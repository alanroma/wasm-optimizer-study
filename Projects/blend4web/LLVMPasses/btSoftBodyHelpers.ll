; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletSoftBody/btSoftBodyHelpers.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletSoftBody/btSoftBodyHelpers.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btVector3 = type { [4 x float] }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btSoftBody = type { %class.btCollisionObject, %class.btAlignedObjectArray, %class.btSoftBodySolver*, %"struct.btSoftBody::Config", %"struct.btSoftBody::SolverState", %"struct.btSoftBody::Pose", i8*, %struct.btSoftBodyWorldInfo*, %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.29, %class.btAlignedObjectArray.33, %class.btAlignedObjectArray.37, %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.49, %class.btAlignedObjectArray.53, %class.btAlignedObjectArray.57, %class.btAlignedObjectArray.65, float, [2 x %class.btVector3], i8, %struct.btDbvt, %struct.btDbvt, %struct.btDbvt, %class.btAlignedObjectArray.73, %class.btAlignedObjectArray.77, %class.btTransform, %class.btVector3, float, %class.btAlignedObjectArray.81 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btSoftBodySolver = type opaque
%"struct.btSoftBody::Config" = type { i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%"struct.btSoftBody::SolverState" = type { float, float, float, float, float }
%"struct.btSoftBody::Pose" = type { i8, i8, float, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btVector3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.btSoftBodyWorldInfo = type { float, float, float, float, %class.btVector3, %class.btBroadphaseInterface*, %class.btDispatcher*, %class.btVector3, %struct.btSparseSdf }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btDispatcher = type { i32 (...)** }
%struct.btSparseSdf = type { %class.btAlignedObjectArray.16, float, i32, i32, i32, i32, i32 }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %"struct.btSparseSdf<3>::Cell"**, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%"struct.btSparseSdf<3>::Cell" = type opaque
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, %"struct.btSoftBody::Note"*, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }
%"struct.btSoftBody::Note" = type { %"struct.btSoftBody::Element", i8*, %class.btVector3, i32, [4 x %"struct.btSoftBody::Node"*], [4 x float] }
%"struct.btSoftBody::Element" = type { i8* }
%"struct.btSoftBody::Node" = type <{ %"struct.btSoftBody::Feature", %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, %struct.btDbvtNode*, i8, [3 x i8] }>
%"struct.btSoftBody::Feature" = type { %"struct.btSoftBody::Element", %"struct.btSoftBody::Material"* }
%"struct.btSoftBody::Material" = type { %"struct.btSoftBody::Element", float, float, float, i32 }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.23 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.23 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray.25 = type <{ %class.btAlignedAllocator.26, [3 x i8], i32, i32, %"struct.btSoftBody::Node"*, i8, [3 x i8] }>
%class.btAlignedAllocator.26 = type { i8 }
%class.btAlignedObjectArray.29 = type <{ %class.btAlignedAllocator.30, [3 x i8], i32, i32, %"struct.btSoftBody::Link"*, i8, [3 x i8] }>
%class.btAlignedAllocator.30 = type { i8 }
%"struct.btSoftBody::Link" = type { %"struct.btSoftBody::Feature", %class.btVector3, [2 x %"struct.btSoftBody::Node"*], float, i8, float, float, float }
%class.btAlignedObjectArray.33 = type <{ %class.btAlignedAllocator.34, [3 x i8], i32, i32, %"struct.btSoftBody::Face"*, i8, [3 x i8] }>
%class.btAlignedAllocator.34 = type { i8 }
%"struct.btSoftBody::Face" = type { %"struct.btSoftBody::Feature", [3 x %"struct.btSoftBody::Node"*], %class.btVector3, float, %struct.btDbvtNode* }
%class.btAlignedObjectArray.37 = type <{ %class.btAlignedAllocator.38, [3 x i8], i32, i32, %"struct.btSoftBody::Tetra"*, i8, [3 x i8] }>
%class.btAlignedAllocator.38 = type { i8 }
%"struct.btSoftBody::Tetra" = type { %"struct.btSoftBody::Feature", [4 x %"struct.btSoftBody::Node"*], float, %struct.btDbvtNode*, [4 x %class.btVector3], float, float }
%class.btAlignedObjectArray.41 = type <{ %class.btAlignedAllocator.42, [3 x i8], i32, i32, %"struct.btSoftBody::Anchor"*, i8, [3 x i8] }>
%class.btAlignedAllocator.42 = type { i8 }
%"struct.btSoftBody::Anchor" = type { %"struct.btSoftBody::Node"*, %class.btVector3, %class.btRigidBody*, float, %class.btMatrix3x3, %class.btVector3, float }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.44, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.44 = type <{ %class.btAlignedAllocator.45, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.45 = type { i8 }
%class.btTypedConstraint = type opaque
%class.btAlignedObjectArray.49 = type <{ %class.btAlignedAllocator.50, [3 x i8], i32, i32, %"struct.btSoftBody::RContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.50 = type { i8 }
%"struct.btSoftBody::RContact" = type { %"struct.btSoftBody::sCti", %"struct.btSoftBody::Node"*, %class.btMatrix3x3, %class.btVector3, float, float, float }
%"struct.btSoftBody::sCti" = type { %class.btCollisionObject*, %class.btVector3, float }
%class.btAlignedObjectArray.53 = type <{ %class.btAlignedAllocator.54, [3 x i8], i32, i32, %"struct.btSoftBody::SContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.54 = type { i8 }
%"struct.btSoftBody::SContact" = type { %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Face"*, %class.btVector3, %class.btVector3, float, float, [2 x float] }
%class.btAlignedObjectArray.57 = type <{ %class.btAlignedAllocator.58, [3 x i8], i32, i32, %"struct.btSoftBody::Joint"**, i8, [3 x i8] }>
%class.btAlignedAllocator.58 = type { i8 }
%"struct.btSoftBody::Joint" = type <{ i32 (...)**, [2 x %"struct.btSoftBody::Body"], [2 x %class.btVector3], float, float, float, %class.btVector3, %class.btVector3, %class.btMatrix3x3, i8, [3 x i8] }>
%"struct.btSoftBody::Body" = type { %"struct.btSoftBody::Cluster"*, %class.btRigidBody*, %class.btCollisionObject* }
%"struct.btSoftBody::Cluster" = type { %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.60, %class.btAlignedObjectArray.8, %class.btTransform, float, float, %class.btMatrix3x3, %class.btMatrix3x3, %class.btVector3, [2 x %class.btVector3], [2 x %class.btVector3], i32, i32, %class.btVector3, %class.btVector3, %struct.btDbvtNode*, float, float, float, float, float, float, i8, i8, i32 }
%class.btAlignedObjectArray.60 = type <{ %class.btAlignedAllocator.61, [3 x i8], i32, i32, %"struct.btSoftBody::Node"**, i8, [3 x i8] }>
%class.btAlignedAllocator.61 = type { i8 }
%class.btAlignedObjectArray.65 = type <{ %class.btAlignedAllocator.66, [3 x i8], i32, i32, %"struct.btSoftBody::Material"**, i8, [3 x i8] }>
%class.btAlignedAllocator.66 = type { i8 }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.69 }
%class.btAlignedObjectArray.69 = type <{ %class.btAlignedAllocator.70, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.70 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.73 = type <{ %class.btAlignedAllocator.74, [3 x i8], i32, i32, %"struct.btSoftBody::Cluster"**, i8, [3 x i8] }>
%class.btAlignedAllocator.74 = type { i8 }
%class.btAlignedObjectArray.77 = type <{ %class.btAlignedAllocator.78, [3 x i8], i32, i32, i8*, i8, [3 x i8] }>
%class.btAlignedAllocator.78 = type { i8 }
%class.btAlignedObjectArray.81 = type <{ %class.btAlignedAllocator.82, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.82 = type { i8 }
%class.btIDebugDraw = type { i32 (...)** }
%class.btConvexHullComputer = type { %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.81 }
%class.btAlignedObjectArray.85 = type <{ %class.btAlignedAllocator.86, [3 x i8], i32, i32, %"class.btConvexHullComputer::Edge"*, i8, [3 x i8] }>
%class.btAlignedAllocator.86 = type { i8 }
%"class.btConvexHullComputer::Edge" = type { i32, i32, i32 }
%"struct.btSoftBody::LJoint" = type { %"struct.btSoftBody::Joint.base", [2 x %class.btVector3] }
%"struct.btSoftBody::Joint.base" = type <{ i32 (...)**, [2 x %"struct.btSoftBody::Body"], [2 x %class.btVector3], float, float, float, %class.btVector3, %class.btVector3, %class.btMatrix3x3, i8 }>
%class.LinkDeps_t = type { i32, %class.LinkDeps_t* }
%class.HullDesc = type { i32, i32, %class.btVector3*, i32, float, i32, i32 }
%class.HullResult = type { i8, i32, %class.btAlignedObjectArray.8, i32, i32, %class.btAlignedObjectArray.89 }
%class.btAlignedObjectArray.89 = type <{ %class.btAlignedAllocator.90, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.90 = type { i8 }
%class.HullLibrary = type { %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.81 }
%class.btAlignedObjectArray.93 = type <{ %class.btAlignedAllocator.94, [3 x i8], i32, i32, %class.btHullTriangle**, i8, [3 x i8] }>
%class.btAlignedAllocator.94 = type { i8 }
%class.btHullTriangle = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK20btAlignedObjectArrayIPN10btSoftBody7ClusterEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPN10btSoftBody7ClusterEEixEi = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZNK20btAlignedObjectArrayIPN10btSoftBody4NodeEE4sizeEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPN10btSoftBody4NodeEEixEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZN20btConvexHullComputerC2Ev = comdat any

$_ZN20btConvexHullComputer7computeEPKfiiff = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi = comdat any

$_ZNK20btConvexHullComputer4Edge17getNextEdgeOfFaceEv = comdat any

$_ZNK20btConvexHullComputer4Edge15getSourceVertexEv = comdat any

$_ZNK20btConvexHullComputer4Edge15getTargetVertexEv = comdat any

$_ZN20btConvexHullComputerD2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody4LinkEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody4LinkEEixEi = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody8RContactEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody8RContactEEixEi = comdat any

$_Z5btDotRK9btVector3S1_ = comdat any

$_Z7btCrossRK9btVector3S1_ = comdat any

$_ZNK9btVector37minAxisEv = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody4FaceEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody4FaceEEixEi = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody5TetraEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody5TetraEEixEi = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody6AnchorEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody6AnchorEEixEi = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody4NoteEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody4NoteEEixEi = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK20btAlignedObjectArrayIPN10btSoftBody5JointEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPN10btSoftBody5JointEEixEi = comdat any

$_ZNK10btSoftBody4Body5xformEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZN10btSoftBody4LinknaEm = comdat any

$_ZN10btSoftBody4LinkC2Ev = comdat any

$_ZN10btSoftBody4LinkdaEPv = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZN9btVector3naEm = comdat any

$_Z4lerpRK9btVector3S1_RKf = comdat any

$_ZN17btCollisionObjectnwEm = comdat any

$_ZN9btVector3daEPv = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_Z5btMaxIiERKT_S2_S2_ = comdat any

$_ZN20btAlignedObjectArrayIbEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIbE6resizeEiRKb = comdat any

$_ZN20btAlignedObjectArrayIbEixEi = comdat any

$_ZN20btAlignedObjectArrayIbED2Ev = comdat any

$_ZN8HullDescC2E8HullFlagjPK9btVector3j = comdat any

$_ZN10HullResultC2Ev = comdat any

$_ZN11HullLibraryC2Ev = comdat any

$_ZN20btAlignedObjectArrayIjEixEi = comdat any

$_ZN11HullLibraryD2Ev = comdat any

$_ZN10HullResultD2Ev = comdat any

$_ZN9btVector34setXEf = comdat any

$_ZN9btVector34setYEf = comdat any

$_ZN9btVector34setZEf = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZNK20btConvexHullComputer4Edge19getNextEdgeOfVertexEv = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEED2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_ = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZN11btTransform11getIdentityEv = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZN11btMatrix3x311getIdentityEv = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZNK10btDbvtNode10isinternalEv = comdat any

$_ZNK12btDbvtAabbMm6CenterEv = comdat any

$_ZNK12btDbvtAabbMm7ExtentsEv = comdat any

$_ZNK10btDbvtNode6isleafEv = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN10btSoftBody7FeatureC2Ev = comdat any

$_ZN10btSoftBody7ElementC2Ev = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector34lerpERKS_RKf = comdat any

$_Z5btCosf = comdat any

$_Z5btSinf = comdat any

$_ZN20btAlignedObjectArrayIjEC2Ev = comdat any

$_ZN18btAlignedAllocatorIjLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIjE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleEC2Ev = comdat any

$_ZN18btAlignedAllocatorIP14btHullTriangleLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIjED2Ev = comdat any

$_ZN20btAlignedObjectArrayIjE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIjE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIjE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIjE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorIbLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIbE4initEv = comdat any

$_ZN20btAlignedObjectArrayIbE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIbE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIbE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIbE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIbLj16EE10deallocateEPb = comdat any

$_ZN20btAlignedObjectArrayIbE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIbE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIbE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIbE4copyEiiPb = comdat any

$_ZN18btAlignedAllocatorIbLj16EE8allocateEiPPKb = comdat any

$_ZZNK10btSoftBody4Body5xformEvE8identity = comdat any

$_ZGVZNK10btSoftBody4Body5xformEvE8identity = comdat any

$_ZZN11btTransform11getIdentityEvE17identityTransform = comdat any

$_ZGVZN11btTransform11getIdentityEvE17identityTransform = comdat any

$_ZZN11btMatrix3x311getIdentityEvE14identityMatrix = comdat any

$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis = internal global [3 x %class.btVector3] zeroinitializer, align 16
@_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis = internal global i32 0, align 4
@.str = private unnamed_addr constant [9 x i8] c" M(%.2f)\00", align 1
@.str.1 = private unnamed_addr constant [9 x i8] c" A(%.2f)\00", align 1
@_ZZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDrawE4ascl = internal constant float 1.000000e+01, align 4
@_ZZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDrawE4nscl = internal constant float 0x3FB99999A0000000, align 4
@.str.2 = private unnamed_addr constant [12 x i8] c"%d %d %d %d\00", align 1
@.str.3 = private unnamed_addr constant [12 x i8] c"%d %f %f %f\00", align 1
@.str.4 = private unnamed_addr constant [9 x i8] c"%d %d %d\00", align 1
@.str.5 = private unnamed_addr constant [15 x i8] c"%d %d %d %d %d\00", align 1
@.str.6 = private unnamed_addr constant [13 x i8] c"Nodes:  %u\0D\0A\00", align 1
@.str.7 = private unnamed_addr constant [13 x i8] c"Links:  %u\0D\0A\00", align 1
@.str.8 = private unnamed_addr constant [13 x i8] c"Faces:  %u\0D\0A\00", align 1
@.str.9 = private unnamed_addr constant [13 x i8] c"Tetras: %u\0D\0A\00", align 1
@_ZZNK10btSoftBody4Body5xformEvE8identity = linkonce_odr hidden global %class.btTransform zeroinitializer, comdat, align 4
@_ZGVZNK10btSoftBody4Body5xformEvE8identity = linkonce_odr hidden global i32 0, comdat, align 4
@_ZZN11btTransform11getIdentityEvE17identityTransform = linkonce_odr hidden global %class.btTransform zeroinitializer, comdat, align 4
@_ZGVZN11btTransform11getIdentityEvE17identityTransform = linkonce_odr hidden global i32 0, comdat, align 4
@_ZZN11btMatrix3x311getIdentityEvE14identityMatrix = linkonce_odr hidden global %class.btMatrix3x3 zeroinitializer, comdat, align 4
@_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix = linkonce_odr hidden global i32 0, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btSoftBodyHelpers.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawi(%class.btSoftBody* %psb, %class.btIDebugDraw* %idraw, i32 %drawflags) #2 {
entry:
  %psb.addr = alloca %class.btSoftBody*, align 4
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %drawflags.addr = alloca i32, align 4
  %scl = alloca float, align 4
  %nscl = alloca float, align 4
  %lcolor = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ncolor = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ccolor = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %nj = alloca i32, align 4
  %color = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca float, align 4
  %vertices = alloca %class.btAlignedObjectArray.8, align 4
  %ref.tmp34 = alloca %class.btVector3, align 4
  %computer = alloca %class.btConvexHullComputer, align 4
  %stride = alloca i32, align 4
  %count = alloca i32, align 4
  %shrink = alloca float, align 4
  %shrinkClamp = alloca float, align 4
  %i50 = alloca i32, align 4
  %face = alloca i32, align 4
  %firstEdge = alloca %"class.btConvexHullComputer::Edge"*, align 4
  %edge = alloca %"class.btConvexHullComputer::Edge"*, align 4
  %v0 = alloca i32, align 4
  %v1 = alloca i32, align 4
  %v2 = alloca i32, align 4
  %n = alloca %"struct.btSoftBody::Node"*, align 4
  %ref.tmp92 = alloca %class.btVector3, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  %ref.tmp95 = alloca float, align 4
  %ref.tmp96 = alloca float, align 4
  %ref.tmp98 = alloca %class.btVector3, align 4
  %ref.tmp100 = alloca %class.btVector3, align 4
  %ref.tmp101 = alloca float, align 4
  %ref.tmp102 = alloca float, align 4
  %ref.tmp104 = alloca %class.btVector3, align 4
  %ref.tmp105 = alloca float, align 4
  %ref.tmp106 = alloca float, align 4
  %ref.tmp107 = alloca float, align 4
  %ref.tmp111 = alloca %class.btVector3, align 4
  %ref.tmp113 = alloca %class.btVector3, align 4
  %ref.tmp114 = alloca float, align 4
  %ref.tmp115 = alloca float, align 4
  %ref.tmp117 = alloca %class.btVector3, align 4
  %ref.tmp119 = alloca %class.btVector3, align 4
  %ref.tmp120 = alloca float, align 4
  %ref.tmp121 = alloca float, align 4
  %ref.tmp123 = alloca %class.btVector3, align 4
  %ref.tmp124 = alloca float, align 4
  %ref.tmp125 = alloca float, align 4
  %ref.tmp126 = alloca float, align 4
  %ref.tmp130 = alloca %class.btVector3, align 4
  %ref.tmp132 = alloca %class.btVector3, align 4
  %ref.tmp133 = alloca float, align 4
  %ref.tmp134 = alloca float, align 4
  %ref.tmp136 = alloca %class.btVector3, align 4
  %ref.tmp138 = alloca %class.btVector3, align 4
  %ref.tmp139 = alloca float, align 4
  %ref.tmp140 = alloca float, align 4
  %ref.tmp142 = alloca %class.btVector3, align 4
  %ref.tmp143 = alloca float, align 4
  %ref.tmp144 = alloca float, align 4
  %ref.tmp145 = alloca float, align 4
  %l = alloca %"struct.btSoftBody::Link"*, align 4
  %n186 = alloca %"struct.btSoftBody::Node"*, align 4
  %d = alloca %class.btVector3, align 4
  %ref.tmp197 = alloca %class.btVector3, align 4
  %ref.tmp202 = alloca %class.btVector3, align 4
  %ref.tmp204 = alloca %class.btVector3, align 4
  %ref.tmp205 = alloca float, align 4
  %ref.tmp216 = alloca float, align 4
  %ref.tmp217 = alloca float, align 4
  %ref.tmp218 = alloca float, align 4
  %ref.tmp220 = alloca float, align 4
  %ref.tmp221 = alloca float, align 4
  %ref.tmp222 = alloca float, align 4
  %ref.tmp224 = alloca float, align 4
  %ref.tmp225 = alloca float, align 4
  %ref.tmp226 = alloca float, align 4
  %c = alloca %"struct.btSoftBody::RContact"*, align 4
  %o = alloca %class.btVector3, align 4
  %ref.tmp235 = alloca %class.btVector3, align 4
  %ref.tmp236 = alloca float, align 4
  %x = alloca %class.btVector3, align 4
  %ref.tmp243 = alloca %class.btVector3, align 4
  %y = alloca %class.btVector3, align 4
  %ref.tmp250 = alloca %class.btVector3, align 4
  %ref.tmp253 = alloca %class.btVector3, align 4
  %ref.tmp254 = alloca %class.btVector3, align 4
  %ref.tmp255 = alloca %class.btVector3, align 4
  %ref.tmp256 = alloca %class.btVector3, align 4
  %ref.tmp259 = alloca %class.btVector3, align 4
  %ref.tmp260 = alloca %class.btVector3, align 4
  %ref.tmp261 = alloca %class.btVector3, align 4
  %ref.tmp262 = alloca %class.btVector3, align 4
  %ref.tmp265 = alloca %class.btVector3, align 4
  %ref.tmp266 = alloca %class.btVector3, align 4
  %ref.tmp267 = alloca %class.btVector3, align 4
  %ref.tmp270 = alloca float, align 4
  %ref.tmp271 = alloca %class.btVector3, align 4
  %ref.tmp272 = alloca float, align 4
  %ref.tmp273 = alloca float, align 4
  %ref.tmp274 = alloca float, align 4
  %scl285 = alloca float, align 4
  %alp = alloca float, align 4
  %col = alloca %class.btVector3, align 4
  %ref.tmp286 = alloca float, align 4
  %ref.tmp287 = alloca float, align 4
  %ref.tmp288 = alloca float, align 4
  %f = alloca %"struct.btSoftBody::Face"*, align 4
  %x302 = alloca [3 x %class.btVector3], align 16
  %c313 = alloca %class.btVector3, align 4
  %ref.tmp314 = alloca %class.btVector3, align 4
  %ref.tmp315 = alloca %class.btVector3, align 4
  %ref.tmp319 = alloca float, align 4
  %ref.tmp320 = alloca %class.btVector3, align 4
  %ref.tmp321 = alloca %class.btVector3, align 4
  %ref.tmp322 = alloca %class.btVector3, align 4
  %ref.tmp324 = alloca %class.btVector3, align 4
  %ref.tmp325 = alloca %class.btVector3, align 4
  %ref.tmp326 = alloca %class.btVector3, align 4
  %ref.tmp328 = alloca %class.btVector3, align 4
  %ref.tmp329 = alloca %class.btVector3, align 4
  %ref.tmp330 = alloca %class.btVector3, align 4
  %scl341 = alloca float, align 4
  %alp342 = alloca float, align 4
  %col343 = alloca %class.btVector3, align 4
  %ref.tmp344 = alloca float, align 4
  %ref.tmp345 = alloca float, align 4
  %ref.tmp346 = alloca float, align 4
  %i348 = alloca i32, align 4
  %t = alloca %"struct.btSoftBody::Tetra"*, align 4
  %x361 = alloca [4 x %class.btVector3], align 16
  %c378 = alloca %class.btVector3, align 4
  %ref.tmp379 = alloca %class.btVector3, align 4
  %ref.tmp380 = alloca %class.btVector3, align 4
  %ref.tmp381 = alloca %class.btVector3, align 4
  %ref.tmp386 = alloca float, align 4
  %ref.tmp387 = alloca %class.btVector3, align 4
  %ref.tmp388 = alloca %class.btVector3, align 4
  %ref.tmp389 = alloca %class.btVector3, align 4
  %ref.tmp391 = alloca %class.btVector3, align 4
  %ref.tmp392 = alloca %class.btVector3, align 4
  %ref.tmp393 = alloca %class.btVector3, align 4
  %ref.tmp395 = alloca %class.btVector3, align 4
  %ref.tmp396 = alloca %class.btVector3, align 4
  %ref.tmp397 = alloca %class.btVector3, align 4
  %ref.tmp401 = alloca %class.btVector3, align 4
  %ref.tmp402 = alloca %class.btVector3, align 4
  %ref.tmp403 = alloca %class.btVector3, align 4
  %ref.tmp405 = alloca %class.btVector3, align 4
  %ref.tmp406 = alloca %class.btVector3, align 4
  %ref.tmp407 = alloca %class.btVector3, align 4
  %ref.tmp409 = alloca %class.btVector3, align 4
  %ref.tmp410 = alloca %class.btVector3, align 4
  %ref.tmp411 = alloca %class.btVector3, align 4
  %ref.tmp415 = alloca %class.btVector3, align 4
  %ref.tmp416 = alloca %class.btVector3, align 4
  %ref.tmp417 = alloca %class.btVector3, align 4
  %ref.tmp419 = alloca %class.btVector3, align 4
  %ref.tmp420 = alloca %class.btVector3, align 4
  %ref.tmp421 = alloca %class.btVector3, align 4
  %ref.tmp423 = alloca %class.btVector3, align 4
  %ref.tmp424 = alloca %class.btVector3, align 4
  %ref.tmp425 = alloca %class.btVector3, align 4
  %ref.tmp429 = alloca %class.btVector3, align 4
  %ref.tmp430 = alloca %class.btVector3, align 4
  %ref.tmp431 = alloca %class.btVector3, align 4
  %ref.tmp433 = alloca %class.btVector3, align 4
  %ref.tmp434 = alloca %class.btVector3, align 4
  %ref.tmp435 = alloca %class.btVector3, align 4
  %ref.tmp437 = alloca %class.btVector3, align 4
  %ref.tmp438 = alloca %class.btVector3, align 4
  %ref.tmp439 = alloca %class.btVector3, align 4
  %a = alloca %"struct.btSoftBody::Anchor"*, align 4
  %q = alloca %class.btVector3, align 4
  %ref.tmp460 = alloca %class.btVector3, align 4
  %ref.tmp461 = alloca float, align 4
  %ref.tmp462 = alloca float, align 4
  %ref.tmp463 = alloca float, align 4
  %ref.tmp465 = alloca %class.btVector3, align 4
  %ref.tmp466 = alloca float, align 4
  %ref.tmp467 = alloca float, align 4
  %ref.tmp468 = alloca float, align 4
  %ref.tmp472 = alloca %class.btVector3, align 4
  %ref.tmp473 = alloca float, align 4
  %ref.tmp474 = alloca float, align 4
  %ref.tmp475 = alloca float, align 4
  %n487 = alloca %"struct.btSoftBody::Node"*, align 4
  %ref.tmp499 = alloca %class.btVector3, align 4
  %ref.tmp500 = alloca float, align 4
  %ref.tmp501 = alloca float, align 4
  %ref.tmp502 = alloca float, align 4
  %n516 = alloca %"struct.btSoftBody::Note"*, align 4
  %p = alloca %class.btVector3, align 4
  %j520 = alloca i32, align 4
  %ref.tmp524 = alloca %class.btVector3, align 4
  %pj = alloca %"struct.btSoftBody::Joint"*, align 4
  %pjl = alloca %"struct.btSoftBody::LJoint"*, align 4
  %a0 = alloca %class.btVector3, align 4
  %a1 = alloca %class.btVector3, align 4
  %ref.tmp575 = alloca %class.btVector3, align 4
  %ref.tmp576 = alloca float, align 4
  %ref.tmp577 = alloca float, align 4
  %ref.tmp578 = alloca float, align 4
  %ref.tmp586 = alloca %class.btVector3, align 4
  %ref.tmp587 = alloca float, align 4
  %ref.tmp588 = alloca float, align 4
  %ref.tmp589 = alloca float, align 4
  %ref.tmp593 = alloca %class.btVector3, align 4
  %ref.tmp594 = alloca float, align 4
  %ref.tmp595 = alloca float, align 4
  %ref.tmp596 = alloca float, align 4
  %ref.tmp598 = alloca %class.btVector3, align 4
  %ref.tmp599 = alloca float, align 4
  %ref.tmp600 = alloca float, align 4
  %ref.tmp601 = alloca float, align 4
  %o0 = alloca %class.btVector3, align 4
  %o1 = alloca %class.btVector3, align 4
  %a0612 = alloca %class.btVector3, align 4
  %a1619 = alloca %class.btVector3, align 4
  %ref.tmp626 = alloca %class.btVector3, align 4
  %ref.tmp627 = alloca %class.btVector3, align 4
  %ref.tmp628 = alloca float, align 4
  %ref.tmp629 = alloca %class.btVector3, align 4
  %ref.tmp630 = alloca float, align 4
  %ref.tmp631 = alloca float, align 4
  %ref.tmp632 = alloca float, align 4
  %ref.tmp636 = alloca %class.btVector3, align 4
  %ref.tmp637 = alloca %class.btVector3, align 4
  %ref.tmp638 = alloca float, align 4
  %ref.tmp639 = alloca %class.btVector3, align 4
  %ref.tmp640 = alloca float, align 4
  %ref.tmp641 = alloca float, align 4
  %ref.tmp642 = alloca float, align 4
  %ref.tmp646 = alloca %class.btVector3, align 4
  %ref.tmp647 = alloca %class.btVector3, align 4
  %ref.tmp648 = alloca float, align 4
  %ref.tmp649 = alloca %class.btVector3, align 4
  %ref.tmp650 = alloca float, align 4
  %ref.tmp651 = alloca float, align 4
  %ref.tmp652 = alloca float, align 4
  %ref.tmp656 = alloca %class.btVector3, align 4
  %ref.tmp657 = alloca %class.btVector3, align 4
  %ref.tmp658 = alloca float, align 4
  %ref.tmp659 = alloca %class.btVector3, align 4
  %ref.tmp660 = alloca float, align 4
  %ref.tmp661 = alloca float, align 4
  %ref.tmp662 = alloca float, align 4
  store %class.btSoftBody* %psb, %class.btSoftBody** %psb.addr, align 4
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4
  store i32 %drawflags, i32* %drawflags.addr, align 4
  store float 0x3FB99999A0000000, float* %scl, align 4
  store float 5.000000e-01, float* %nscl, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %lcolor, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  store float 1.000000e+00, float* %ref.tmp3, align 4
  store float 1.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ncolor, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  store float 1.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 0.000000e+00, float* %ref.tmp9, align 4
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ccolor, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %0 = load i32, i32* %drawflags.addr, align 4
  %and = and i32 %0, 256
  %cmp = icmp ne i32 0, %and
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  call void @srand(i32 1806)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc75, %if.then
  %1 = load i32, i32* %i, align 4
  %2 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_clusters = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %2, i32 0, i32 24
  %call11 = call i32 @_ZNK20btAlignedObjectArrayIPN10btSoftBody7ClusterEE4sizeEv(%class.btAlignedObjectArray.73* %m_clusters)
  %cmp12 = icmp slt i32 %1, %call11
  br i1 %cmp12, label %for.body, label %for.end77

for.body:                                         ; preds = %for.cond
  %3 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_clusters13 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %3, i32 0, i32 24
  %4 = load i32, i32* %i, align 4
  %call14 = call nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Cluster"** @_ZN20btAlignedObjectArrayIPN10btSoftBody7ClusterEEixEi(%class.btAlignedObjectArray.73* %m_clusters13, i32 %4)
  %5 = load %"struct.btSoftBody::Cluster"*, %"struct.btSoftBody::Cluster"** %call14, align 4
  %m_collide = getelementptr inbounds %"struct.btSoftBody::Cluster", %"struct.btSoftBody::Cluster"* %5, i32 0, i32 23
  %6 = load i8, i8* %m_collide, align 1
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %if.then15, label %if.end

if.then15:                                        ; preds = %for.body
  %call17 = call i32 @rand()
  %conv = sitofp i32 %call17 to float
  %div = fdiv float %conv, 0x41E0000000000000
  store float %div, float* %ref.tmp16, align 4
  %call19 = call i32 @rand()
  %conv20 = sitofp i32 %call19 to float
  %div21 = fdiv float %conv20, 0x41E0000000000000
  store float %div21, float* %ref.tmp18, align 4
  %call23 = call i32 @rand()
  %conv24 = sitofp i32 %call23 to float
  %div25 = fdiv float %conv24, 0x41E0000000000000
  store float %div25, float* %ref.tmp22, align 4
  %call26 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %color, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp22)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* %color)
  store float 7.500000e-01, float* %ref.tmp29, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp29)
  %7 = bitcast %class.btVector3* %color to i8*
  %8 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %call30 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %vertices)
  %9 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_clusters31 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %9, i32 0, i32 24
  %10 = load i32, i32* %i, align 4
  %call32 = call nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Cluster"** @_ZN20btAlignedObjectArrayIPN10btSoftBody7ClusterEEixEi(%class.btAlignedObjectArray.73* %m_clusters31, i32 %10)
  %11 = load %"struct.btSoftBody::Cluster"*, %"struct.btSoftBody::Cluster"** %call32, align 4
  %m_nodes = getelementptr inbounds %"struct.btSoftBody::Cluster", %"struct.btSoftBody::Cluster"* %11, i32 0, i32 1
  %call33 = call i32 @_ZNK20btAlignedObjectArrayIPN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.60* %m_nodes)
  %call35 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp34)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %vertices, i32 %call33, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp34)
  store i32 0, i32* %j, align 4
  %call36 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %vertices)
  store i32 %call36, i32* %nj, align 4
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc, %if.then15
  %12 = load i32, i32* %j, align 4
  %13 = load i32, i32* %nj, align 4
  %cmp38 = icmp slt i32 %12, %13
  br i1 %cmp38, label %for.body39, label %for.end

for.body39:                                       ; preds = %for.cond37
  %14 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_clusters40 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %14, i32 0, i32 24
  %15 = load i32, i32* %i, align 4
  %call41 = call nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Cluster"** @_ZN20btAlignedObjectArrayIPN10btSoftBody7ClusterEEixEi(%class.btAlignedObjectArray.73* %m_clusters40, i32 %15)
  %16 = load %"struct.btSoftBody::Cluster"*, %"struct.btSoftBody::Cluster"** %call41, align 4
  %m_nodes42 = getelementptr inbounds %"struct.btSoftBody::Cluster", %"struct.btSoftBody::Cluster"* %16, i32 0, i32 1
  %17 = load i32, i32* %j, align 4
  %call43 = call nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Node"** @_ZN20btAlignedObjectArrayIPN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.60* %m_nodes42, i32 %17)
  %18 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %call43, align 4
  %m_x = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %18, i32 0, i32 1
  %19 = load i32, i32* %j, align 4
  %call44 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vertices, i32 %19)
  %20 = bitcast %class.btVector3* %call44 to i8*
  %21 = bitcast %class.btVector3* %m_x to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body39
  %22 = load i32, i32* %j, align 4
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond37

for.end:                                          ; preds = %for.cond37
  %call45 = call %class.btConvexHullComputer* @_ZN20btConvexHullComputerC2Ev(%class.btConvexHullComputer* %computer)
  store i32 16, i32* %stride, align 4
  %call46 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %vertices)
  store i32 %call46, i32* %count, align 4
  store float 0.000000e+00, float* %shrink, align 4
  store float 0.000000e+00, float* %shrinkClamp, align 4
  %call47 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vertices, i32 0)
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %call47)
  %23 = load i32, i32* %stride, align 4
  %24 = load i32, i32* %count, align 4
  %25 = load float, float* %shrink, align 4
  %26 = load float, float* %shrinkClamp, align 4
  %call49 = call float @_ZN20btConvexHullComputer7computeEPKfiiff(%class.btConvexHullComputer* %computer, float* %call48, i32 %23, i32 %24, float %25, float %26)
  store i32 0, i32* %i50, align 4
  br label %for.cond51

for.cond51:                                       ; preds = %for.inc70, %for.end
  %27 = load i32, i32* %i50, align 4
  %faces = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %computer, i32 0, i32 2
  %call52 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.81* %faces)
  %cmp53 = icmp slt i32 %27, %call52
  br i1 %cmp53, label %for.body54, label %for.end72

for.body54:                                       ; preds = %for.cond51
  %faces55 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %computer, i32 0, i32 2
  %28 = load i32, i32* %i50, align 4
  %call56 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.81* %faces55, i32 %28)
  %29 = load i32, i32* %call56, align 4
  store i32 %29, i32* %face, align 4
  %edges = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %computer, i32 0, i32 1
  %30 = load i32, i32* %face, align 4
  %call57 = call nonnull align 4 dereferenceable(12) %"class.btConvexHullComputer::Edge"* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi(%class.btAlignedObjectArray.85* %edges, i32 %30)
  store %"class.btConvexHullComputer::Edge"* %call57, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4
  %31 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4
  %call58 = call %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge17getNextEdgeOfFaceEv(%"class.btConvexHullComputer::Edge"* %31)
  store %"class.btConvexHullComputer::Edge"* %call58, %"class.btConvexHullComputer::Edge"** %edge, align 4
  %32 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4
  %call59 = call i32 @_ZNK20btConvexHullComputer4Edge15getSourceVertexEv(%"class.btConvexHullComputer::Edge"* %32)
  store i32 %call59, i32* %v0, align 4
  %33 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4
  %call60 = call i32 @_ZNK20btConvexHullComputer4Edge15getTargetVertexEv(%"class.btConvexHullComputer::Edge"* %33)
  store i32 %call60, i32* %v1, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %for.body54
  %34 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %edge, align 4
  %35 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4
  %cmp61 = icmp ne %"class.btConvexHullComputer::Edge"* %34, %35
  br i1 %cmp61, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %36 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %edge, align 4
  %call62 = call i32 @_ZNK20btConvexHullComputer4Edge15getTargetVertexEv(%"class.btConvexHullComputer::Edge"* %36)
  store i32 %call62, i32* %v2, align 4
  %37 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %vertices63 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %computer, i32 0, i32 0
  %38 = load i32, i32* %v0, align 4
  %call64 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vertices63, i32 %38)
  %vertices65 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %computer, i32 0, i32 0
  %39 = load i32, i32* %v1, align 4
  %call66 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vertices65, i32 %39)
  %vertices67 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %computer, i32 0, i32 0
  %40 = load i32, i32* %v2, align 4
  %call68 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vertices67, i32 %40)
  %41 = bitcast %class.btIDebugDraw* %37 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)***
  %vtable = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*** %41, align 4
  %vfn = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vtable, i64 9
  %42 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vfn, align 4
  call void %42(%class.btIDebugDraw* %37, %class.btVector3* nonnull align 4 dereferenceable(16) %call64, %class.btVector3* nonnull align 4 dereferenceable(16) %call66, %class.btVector3* nonnull align 4 dereferenceable(16) %call68, %class.btVector3* nonnull align 4 dereferenceable(16) %color, float 1.000000e+00)
  %43 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %edge, align 4
  %call69 = call %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge17getNextEdgeOfFaceEv(%"class.btConvexHullComputer::Edge"* %43)
  store %"class.btConvexHullComputer::Edge"* %call69, %"class.btConvexHullComputer::Edge"** %edge, align 4
  %44 = load i32, i32* %v1, align 4
  store i32 %44, i32* %v0, align 4
  %45 = load i32, i32* %v2, align 4
  store i32 %45, i32* %v1, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %for.inc70

for.inc70:                                        ; preds = %while.end
  %46 = load i32, i32* %i50, align 4
  %inc71 = add nsw i32 %46, 1
  store i32 %inc71, i32* %i50, align 4
  br label %for.cond51

for.end72:                                        ; preds = %for.cond51
  %call73 = call %class.btConvexHullComputer* @_ZN20btConvexHullComputerD2Ev(%class.btConvexHullComputer* %computer) #5
  %call74 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %vertices) #5
  br label %if.end

if.end:                                           ; preds = %for.end72, %for.body
  br label %for.inc75

for.inc75:                                        ; preds = %if.end
  %47 = load i32, i32* %i, align 4
  %inc76 = add nsw i32 %47, 1
  store i32 %inc76, i32* %i, align 4
  br label %for.cond

for.end77:                                        ; preds = %for.cond
  br label %if.end447

if.else:                                          ; preds = %entry
  %48 = load i32, i32* %drawflags.addr, align 4
  %and78 = and i32 %48, 1
  %cmp79 = icmp ne i32 0, %and78
  br i1 %cmp79, label %if.then80, label %if.end152

if.then80:                                        ; preds = %if.else
  store i32 0, i32* %i, align 4
  br label %for.cond81

for.cond81:                                       ; preds = %for.inc149, %if.then80
  %49 = load i32, i32* %i, align 4
  %50 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_nodes82 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %50, i32 0, i32 9
  %call83 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.25* %m_nodes82)
  %cmp84 = icmp slt i32 %49, %call83
  br i1 %cmp84, label %for.body85, label %for.end151

for.body85:                                       ; preds = %for.cond81
  %51 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_nodes86 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %51, i32 0, i32 9
  %52 = load i32, i32* %i, align 4
  %call87 = call nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.25* %m_nodes86, i32 %52)
  store %"struct.btSoftBody::Node"* %call87, %"struct.btSoftBody::Node"** %n, align 4
  %53 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4
  %54 = bitcast %"struct.btSoftBody::Node"* %53 to %"struct.btSoftBody::Feature"*
  %m_material = getelementptr inbounds %"struct.btSoftBody::Feature", %"struct.btSoftBody::Feature"* %54, i32 0, i32 1
  %55 = load %"struct.btSoftBody::Material"*, %"struct.btSoftBody::Material"** %m_material, align 4
  %m_flags = getelementptr inbounds %"struct.btSoftBody::Material", %"struct.btSoftBody::Material"* %55, i32 0, i32 4
  %56 = load i32, i32* %m_flags, align 4
  %and88 = and i32 %56, 1
  %cmp89 = icmp eq i32 0, %and88
  br i1 %cmp89, label %if.then90, label %if.end91

if.then90:                                        ; preds = %for.body85
  br label %for.inc149

if.end91:                                         ; preds = %for.body85
  %57 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %58 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4
  %m_x93 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %58, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp95, align 4
  store float 0.000000e+00, float* %ref.tmp96, align 4
  %call97 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp94, float* nonnull align 4 dereferenceable(4) %scl, float* nonnull align 4 dereferenceable(4) %ref.tmp95, float* nonnull align 4 dereferenceable(4) %ref.tmp96)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp92, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x93, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp94)
  %59 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4
  %m_x99 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %59, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp101, align 4
  store float 0.000000e+00, float* %ref.tmp102, align 4
  %call103 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp100, float* nonnull align 4 dereferenceable(4) %scl, float* nonnull align 4 dereferenceable(4) %ref.tmp101, float* nonnull align 4 dereferenceable(4) %ref.tmp102)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp98, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x99, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp100)
  store float 1.000000e+00, float* %ref.tmp105, align 4
  store float 0.000000e+00, float* %ref.tmp106, align 4
  store float 0.000000e+00, float* %ref.tmp107, align 4
  %call108 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp104, float* nonnull align 4 dereferenceable(4) %ref.tmp105, float* nonnull align 4 dereferenceable(4) %ref.tmp106, float* nonnull align 4 dereferenceable(4) %ref.tmp107)
  %60 = bitcast %class.btIDebugDraw* %57 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable109 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %60, align 4
  %vfn110 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable109, i64 4
  %61 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn110, align 4
  call void %61(%class.btIDebugDraw* %57, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp92, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp98, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp104)
  %62 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %63 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4
  %m_x112 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %63, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp114, align 4
  store float 0.000000e+00, float* %ref.tmp115, align 4
  %call116 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp113, float* nonnull align 4 dereferenceable(4) %ref.tmp114, float* nonnull align 4 dereferenceable(4) %scl, float* nonnull align 4 dereferenceable(4) %ref.tmp115)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp111, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x112, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp113)
  %64 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4
  %m_x118 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %64, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp120, align 4
  store float 0.000000e+00, float* %ref.tmp121, align 4
  %call122 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp119, float* nonnull align 4 dereferenceable(4) %ref.tmp120, float* nonnull align 4 dereferenceable(4) %scl, float* nonnull align 4 dereferenceable(4) %ref.tmp121)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp117, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x118, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp119)
  store float 0.000000e+00, float* %ref.tmp124, align 4
  store float 1.000000e+00, float* %ref.tmp125, align 4
  store float 0.000000e+00, float* %ref.tmp126, align 4
  %call127 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp123, float* nonnull align 4 dereferenceable(4) %ref.tmp124, float* nonnull align 4 dereferenceable(4) %ref.tmp125, float* nonnull align 4 dereferenceable(4) %ref.tmp126)
  %65 = bitcast %class.btIDebugDraw* %62 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable128 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %65, align 4
  %vfn129 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable128, i64 4
  %66 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn129, align 4
  call void %66(%class.btIDebugDraw* %62, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp111, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp117, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp123)
  %67 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %68 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4
  %m_x131 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %68, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp133, align 4
  store float 0.000000e+00, float* %ref.tmp134, align 4
  %call135 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp132, float* nonnull align 4 dereferenceable(4) %ref.tmp133, float* nonnull align 4 dereferenceable(4) %ref.tmp134, float* nonnull align 4 dereferenceable(4) %scl)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp130, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x131, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp132)
  %69 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4
  %m_x137 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %69, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp139, align 4
  store float 0.000000e+00, float* %ref.tmp140, align 4
  %call141 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp138, float* nonnull align 4 dereferenceable(4) %ref.tmp139, float* nonnull align 4 dereferenceable(4) %ref.tmp140, float* nonnull align 4 dereferenceable(4) %scl)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp136, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x137, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp138)
  store float 0.000000e+00, float* %ref.tmp143, align 4
  store float 0.000000e+00, float* %ref.tmp144, align 4
  store float 1.000000e+00, float* %ref.tmp145, align 4
  %call146 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp142, float* nonnull align 4 dereferenceable(4) %ref.tmp143, float* nonnull align 4 dereferenceable(4) %ref.tmp144, float* nonnull align 4 dereferenceable(4) %ref.tmp145)
  %70 = bitcast %class.btIDebugDraw* %67 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable147 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %70, align 4
  %vfn148 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable147, i64 4
  %71 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn148, align 4
  call void %71(%class.btIDebugDraw* %67, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp130, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp136, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp142)
  br label %for.inc149

for.inc149:                                       ; preds = %if.end91, %if.then90
  %72 = load i32, i32* %i, align 4
  %inc150 = add nsw i32 %72, 1
  store i32 %inc150, i32* %i, align 4
  br label %for.cond81

for.end151:                                       ; preds = %for.cond81
  br label %if.end152

if.end152:                                        ; preds = %for.end151, %if.else
  %73 = load i32, i32* %drawflags.addr, align 4
  %and153 = and i32 %73, 2
  %cmp154 = icmp ne i32 0, %and153
  br i1 %cmp154, label %if.then155, label %if.end177

if.then155:                                       ; preds = %if.end152
  store i32 0, i32* %i, align 4
  br label %for.cond156

for.cond156:                                      ; preds = %for.inc174, %if.then155
  %74 = load i32, i32* %i, align 4
  %75 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_links = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %75, i32 0, i32 10
  %call157 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4LinkEE4sizeEv(%class.btAlignedObjectArray.29* %m_links)
  %cmp158 = icmp slt i32 %74, %call157
  br i1 %cmp158, label %for.body159, label %for.end176

for.body159:                                      ; preds = %for.cond156
  %76 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_links160 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %76, i32 0, i32 10
  %77 = load i32, i32* %i, align 4
  %call161 = call nonnull align 4 dereferenceable(52) %"struct.btSoftBody::Link"* @_ZN20btAlignedObjectArrayIN10btSoftBody4LinkEEixEi(%class.btAlignedObjectArray.29* %m_links160, i32 %77)
  store %"struct.btSoftBody::Link"* %call161, %"struct.btSoftBody::Link"** %l, align 4
  %78 = load %"struct.btSoftBody::Link"*, %"struct.btSoftBody::Link"** %l, align 4
  %79 = bitcast %"struct.btSoftBody::Link"* %78 to %"struct.btSoftBody::Feature"*
  %m_material162 = getelementptr inbounds %"struct.btSoftBody::Feature", %"struct.btSoftBody::Feature"* %79, i32 0, i32 1
  %80 = load %"struct.btSoftBody::Material"*, %"struct.btSoftBody::Material"** %m_material162, align 4
  %m_flags163 = getelementptr inbounds %"struct.btSoftBody::Material", %"struct.btSoftBody::Material"* %80, i32 0, i32 4
  %81 = load i32, i32* %m_flags163, align 4
  %and164 = and i32 %81, 1
  %cmp165 = icmp eq i32 0, %and164
  br i1 %cmp165, label %if.then166, label %if.end167

if.then166:                                       ; preds = %for.body159
  br label %for.inc174

if.end167:                                        ; preds = %for.body159
  %82 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %83 = load %"struct.btSoftBody::Link"*, %"struct.btSoftBody::Link"** %l, align 4
  %m_n = getelementptr inbounds %"struct.btSoftBody::Link", %"struct.btSoftBody::Link"* %83, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x %"struct.btSoftBody::Node"*], [2 x %"struct.btSoftBody::Node"*]* %m_n, i32 0, i32 0
  %84 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx, align 4
  %m_x168 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %84, i32 0, i32 1
  %85 = load %"struct.btSoftBody::Link"*, %"struct.btSoftBody::Link"** %l, align 4
  %m_n169 = getelementptr inbounds %"struct.btSoftBody::Link", %"struct.btSoftBody::Link"* %85, i32 0, i32 2
  %arrayidx170 = getelementptr inbounds [2 x %"struct.btSoftBody::Node"*], [2 x %"struct.btSoftBody::Node"*]* %m_n169, i32 0, i32 1
  %86 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx170, align 4
  %m_x171 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %86, i32 0, i32 1
  %87 = bitcast %class.btIDebugDraw* %82 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable172 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %87, align 4
  %vfn173 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable172, i64 4
  %88 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn173, align 4
  call void %88(%class.btIDebugDraw* %82, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x168, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x171, %class.btVector3* nonnull align 4 dereferenceable(16) %lcolor)
  br label %for.inc174

for.inc174:                                       ; preds = %if.end167, %if.then166
  %89 = load i32, i32* %i, align 4
  %inc175 = add nsw i32 %89, 1
  store i32 %inc175, i32* %i, align 4
  br label %for.cond156

for.end176:                                       ; preds = %for.cond156
  br label %if.end177

if.end177:                                        ; preds = %for.end176, %if.end152
  %90 = load i32, i32* %drawflags.addr, align 4
  %and178 = and i32 %90, 16
  %cmp179 = icmp ne i32 0, %and178
  br i1 %cmp179, label %if.then180, label %if.end211

if.then180:                                       ; preds = %if.end177
  store i32 0, i32* %i, align 4
  br label %for.cond181

for.cond181:                                      ; preds = %for.inc208, %if.then180
  %91 = load i32, i32* %i, align 4
  %92 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_nodes182 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %92, i32 0, i32 9
  %call183 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.25* %m_nodes182)
  %cmp184 = icmp slt i32 %91, %call183
  br i1 %cmp184, label %for.body185, label %for.end210

for.body185:                                      ; preds = %for.cond181
  %93 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_nodes187 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %93, i32 0, i32 9
  %94 = load i32, i32* %i, align 4
  %call188 = call nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.25* %m_nodes187, i32 %94)
  store %"struct.btSoftBody::Node"* %call188, %"struct.btSoftBody::Node"** %n186, align 4
  %95 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n186, align 4
  %96 = bitcast %"struct.btSoftBody::Node"* %95 to %"struct.btSoftBody::Feature"*
  %m_material189 = getelementptr inbounds %"struct.btSoftBody::Feature", %"struct.btSoftBody::Feature"* %96, i32 0, i32 1
  %97 = load %"struct.btSoftBody::Material"*, %"struct.btSoftBody::Material"** %m_material189, align 4
  %m_flags190 = getelementptr inbounds %"struct.btSoftBody::Material", %"struct.btSoftBody::Material"* %97, i32 0, i32 4
  %98 = load i32, i32* %m_flags190, align 4
  %and191 = and i32 %98, 1
  %cmp192 = icmp eq i32 0, %and191
  br i1 %cmp192, label %if.then193, label %if.end194

if.then193:                                       ; preds = %for.body185
  br label %for.inc208

if.end194:                                        ; preds = %for.body185
  %99 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n186, align 4
  %m_n195 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %99, i32 0, i32 5
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %d, %class.btVector3* nonnull align 4 dereferenceable(16) %m_n195, float* nonnull align 4 dereferenceable(4) %nscl)
  %100 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %101 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n186, align 4
  %m_x196 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %101, i32 0, i32 1
  %102 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n186, align 4
  %m_x198 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %102, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp197, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x198, %class.btVector3* nonnull align 4 dereferenceable(16) %d)
  %103 = bitcast %class.btIDebugDraw* %100 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable199 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %103, align 4
  %vfn200 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable199, i64 4
  %104 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn200, align 4
  call void %104(%class.btIDebugDraw* %100, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x196, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp197, %class.btVector3* nonnull align 4 dereferenceable(16) %ncolor)
  %105 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %106 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n186, align 4
  %m_x201 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %106, i32 0, i32 1
  %107 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n186, align 4
  %m_x203 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %107, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp202, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x203, %class.btVector3* nonnull align 4 dereferenceable(16) %d)
  store float 5.000000e-01, float* %ref.tmp205, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp204, %class.btVector3* nonnull align 4 dereferenceable(16) %ncolor, float* nonnull align 4 dereferenceable(4) %ref.tmp205)
  %108 = bitcast %class.btIDebugDraw* %105 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable206 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %108, align 4
  %vfn207 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable206, i64 4
  %109 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn207, align 4
  call void %109(%class.btIDebugDraw* %105, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x201, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp202, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp204)
  br label %for.inc208

for.inc208:                                       ; preds = %if.end194, %if.then193
  %110 = load i32, i32* %i, align 4
  %inc209 = add nsw i32 %110, 1
  store i32 %inc209, i32* %i, align 4
  br label %for.cond181

for.end210:                                       ; preds = %for.cond181
  br label %if.end211

if.end211:                                        ; preds = %for.end210, %if.end177
  %111 = load i32, i32* %drawflags.addr, align 4
  %and212 = and i32 %111, 32
  %cmp213 = icmp ne i32 0, %and212
  br i1 %cmp213, label %if.then214, label %if.end281

if.then214:                                       ; preds = %if.end211
  %112 = load atomic i8, i8* bitcast (i32* @_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis to i8*) acquire, align 4
  %113 = and i8 %112, 1
  %guard.uninitialized = icmp eq i8 %113, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %if.then214
  %114 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis) #5
  %tobool215 = icmp ne i32 %114, 0
  br i1 %tobool215, label %init, label %init.end

init:                                             ; preds = %init.check
  store float 1.000000e+00, float* %ref.tmp216, align 4
  store float 0.000000e+00, float* %ref.tmp217, align 4
  store float 0.000000e+00, float* %ref.tmp218, align 4
  %call219 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis, i32 0, i32 0), float* nonnull align 4 dereferenceable(4) %ref.tmp216, float* nonnull align 4 dereferenceable(4) %ref.tmp217, float* nonnull align 4 dereferenceable(4) %ref.tmp218)
  store float 0.000000e+00, float* %ref.tmp220, align 4
  store float 1.000000e+00, float* %ref.tmp221, align 4
  store float 0.000000e+00, float* %ref.tmp222, align 4
  %call223 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis, i32 0, i32 1), float* nonnull align 4 dereferenceable(4) %ref.tmp220, float* nonnull align 4 dereferenceable(4) %ref.tmp221, float* nonnull align 4 dereferenceable(4) %ref.tmp222)
  store float 0.000000e+00, float* %ref.tmp224, align 4
  store float 0.000000e+00, float* %ref.tmp225, align 4
  store float 1.000000e+00, float* %ref.tmp226, align 4
  %call227 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis, i32 0, i32 2), float* nonnull align 4 dereferenceable(4) %ref.tmp224, float* nonnull align 4 dereferenceable(4) %ref.tmp225, float* nonnull align 4 dereferenceable(4) %ref.tmp226)
  call void @__cxa_guard_release(i32* @_ZGVZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis) #5
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %if.then214
  store i32 0, i32* %i, align 4
  br label %for.cond228

for.cond228:                                      ; preds = %for.inc278, %init.end
  %115 = load i32, i32* %i, align 4
  %116 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_rcontacts = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %116, i32 0, i32 14
  %call229 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody8RContactEE4sizeEv(%class.btAlignedObjectArray.49* %m_rcontacts)
  %cmp230 = icmp slt i32 %115, %call229
  br i1 %cmp230, label %for.body231, label %for.end280

for.body231:                                      ; preds = %for.cond228
  %117 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_rcontacts232 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %117, i32 0, i32 14
  %118 = load i32, i32* %i, align 4
  %call233 = call nonnull align 4 dereferenceable(104) %"struct.btSoftBody::RContact"* @_ZN20btAlignedObjectArrayIN10btSoftBody8RContactEEixEi(%class.btAlignedObjectArray.49* %m_rcontacts232, i32 %118)
  store %"struct.btSoftBody::RContact"* %call233, %"struct.btSoftBody::RContact"** %c, align 4
  %119 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4
  %m_node = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %119, i32 0, i32 1
  %120 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %m_node, align 4
  %m_x234 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %120, i32 0, i32 1
  %121 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4
  %m_cti = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %121, i32 0, i32 0
  %m_normal = getelementptr inbounds %"struct.btSoftBody::sCti", %"struct.btSoftBody::sCti"* %m_cti, i32 0, i32 1
  %122 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4
  %m_node237 = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %122, i32 0, i32 1
  %123 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %m_node237, align 4
  %m_x238 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %123, i32 0, i32 1
  %124 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4
  %m_cti239 = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %124, i32 0, i32 0
  %m_normal240 = getelementptr inbounds %"struct.btSoftBody::sCti", %"struct.btSoftBody::sCti"* %m_cti239, i32 0, i32 1
  %call241 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_x238, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal240)
  %125 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4
  %m_cti242 = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %125, i32 0, i32 0
  %m_offset = getelementptr inbounds %"struct.btSoftBody::sCti", %"struct.btSoftBody::sCti"* %m_cti242, i32 0, i32 2
  %126 = load float, float* %m_offset, align 4
  %add = fadd float %call241, %126
  store float %add, float* %ref.tmp236, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp235, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal, float* nonnull align 4 dereferenceable(4) %ref.tmp236)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %o, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x234, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp235)
  %127 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4
  %m_cti244 = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %127, i32 0, i32 0
  %m_normal245 = getelementptr inbounds %"struct.btSoftBody::sCti", %"struct.btSoftBody::sCti"* %m_cti244, i32 0, i32 1
  %128 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4
  %m_cti246 = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %128, i32 0, i32 0
  %m_normal247 = getelementptr inbounds %"struct.btSoftBody::sCti", %"struct.btSoftBody::sCti"* %m_cti246, i32 0, i32 1
  %call248 = call i32 @_ZNK9btVector37minAxisEv(%class.btVector3* %m_normal247)
  %arrayidx249 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* @_ZZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawiE4axis, i32 0, i32 %call248
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp243, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal245, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx249)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %x, %class.btVector3* %ref.tmp243)
  %129 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4
  %m_cti251 = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %129, i32 0, i32 0
  %m_normal252 = getelementptr inbounds %"struct.btSoftBody::sCti", %"struct.btSoftBody::sCti"* %m_cti251, i32 0, i32 1
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp250, %class.btVector3* nonnull align 4 dereferenceable(16) %x, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal252)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %y, %class.btVector3* %ref.tmp250)
  %130 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp254, %class.btVector3* nonnull align 4 dereferenceable(16) %x, float* nonnull align 4 dereferenceable(4) %nscl)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp253, %class.btVector3* nonnull align 4 dereferenceable(16) %o, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp254)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp256, %class.btVector3* nonnull align 4 dereferenceable(16) %x, float* nonnull align 4 dereferenceable(4) %nscl)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp255, %class.btVector3* nonnull align 4 dereferenceable(16) %o, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp256)
  %131 = bitcast %class.btIDebugDraw* %130 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable257 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %131, align 4
  %vfn258 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable257, i64 4
  %132 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn258, align 4
  call void %132(%class.btIDebugDraw* %130, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp253, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp255, %class.btVector3* nonnull align 4 dereferenceable(16) %ccolor)
  %133 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp260, %class.btVector3* nonnull align 4 dereferenceable(16) %y, float* nonnull align 4 dereferenceable(4) %nscl)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp259, %class.btVector3* nonnull align 4 dereferenceable(16) %o, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp260)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp262, %class.btVector3* nonnull align 4 dereferenceable(16) %y, float* nonnull align 4 dereferenceable(4) %nscl)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp261, %class.btVector3* nonnull align 4 dereferenceable(16) %o, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp262)
  %134 = bitcast %class.btIDebugDraw* %133 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable263 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %134, align 4
  %vfn264 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable263, i64 4
  %135 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn264, align 4
  call void %135(%class.btIDebugDraw* %133, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp259, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp261, %class.btVector3* nonnull align 4 dereferenceable(16) %ccolor)
  %136 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %137 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %c, align 4
  %m_cti268 = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %137, i32 0, i32 0
  %m_normal269 = getelementptr inbounds %"struct.btSoftBody::sCti", %"struct.btSoftBody::sCti"* %m_cti268, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp267, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal269, float* nonnull align 4 dereferenceable(4) %nscl)
  store float 3.000000e+00, float* %ref.tmp270, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp266, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp267, float* nonnull align 4 dereferenceable(4) %ref.tmp270)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp265, %class.btVector3* nonnull align 4 dereferenceable(16) %o, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp266)
  store float 1.000000e+00, float* %ref.tmp272, align 4
  store float 1.000000e+00, float* %ref.tmp273, align 4
  store float 0.000000e+00, float* %ref.tmp274, align 4
  %call275 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp271, float* nonnull align 4 dereferenceable(4) %ref.tmp272, float* nonnull align 4 dereferenceable(4) %ref.tmp273, float* nonnull align 4 dereferenceable(4) %ref.tmp274)
  %138 = bitcast %class.btIDebugDraw* %136 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable276 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %138, align 4
  %vfn277 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable276, i64 4
  %139 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn277, align 4
  call void %139(%class.btIDebugDraw* %136, %class.btVector3* nonnull align 4 dereferenceable(16) %o, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp265, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp271)
  br label %for.inc278

for.inc278:                                       ; preds = %for.body231
  %140 = load i32, i32* %i, align 4
  %inc279 = add nsw i32 %140, 1
  store i32 %inc279, i32* %i, align 4
  br label %for.cond228

for.end280:                                       ; preds = %for.cond228
  br label %if.end281

if.end281:                                        ; preds = %for.end280, %if.end211
  %141 = load i32, i32* %drawflags.addr, align 4
  %and282 = and i32 %141, 4
  %cmp283 = icmp ne i32 0, %and282
  br i1 %cmp283, label %if.then284, label %if.end337

if.then284:                                       ; preds = %if.end281
  store float 0x3FE99999A0000000, float* %scl285, align 4
  store float 1.000000e+00, float* %alp, align 4
  store float 0.000000e+00, float* %ref.tmp286, align 4
  store float 0x3FE6666660000000, float* %ref.tmp287, align 4
  store float 0.000000e+00, float* %ref.tmp288, align 4
  %call289 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %col, float* nonnull align 4 dereferenceable(4) %ref.tmp286, float* nonnull align 4 dereferenceable(4) %ref.tmp287, float* nonnull align 4 dereferenceable(4) %ref.tmp288)
  store i32 0, i32* %i, align 4
  br label %for.cond290

for.cond290:                                      ; preds = %for.inc334, %if.then284
  %142 = load i32, i32* %i, align 4
  %143 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_faces = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %143, i32 0, i32 11
  %call291 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4FaceEE4sizeEv(%class.btAlignedObjectArray.33* %m_faces)
  %cmp292 = icmp slt i32 %142, %call291
  br i1 %cmp292, label %for.body293, label %for.end336

for.body293:                                      ; preds = %for.cond290
  %144 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_faces294 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %144, i32 0, i32 11
  %145 = load i32, i32* %i, align 4
  %call295 = call nonnull align 4 dereferenceable(44) %"struct.btSoftBody::Face"* @_ZN20btAlignedObjectArrayIN10btSoftBody4FaceEEixEi(%class.btAlignedObjectArray.33* %m_faces294, i32 %145)
  store %"struct.btSoftBody::Face"* %call295, %"struct.btSoftBody::Face"** %f, align 4
  %146 = load %"struct.btSoftBody::Face"*, %"struct.btSoftBody::Face"** %f, align 4
  %147 = bitcast %"struct.btSoftBody::Face"* %146 to %"struct.btSoftBody::Feature"*
  %m_material296 = getelementptr inbounds %"struct.btSoftBody::Feature", %"struct.btSoftBody::Feature"* %147, i32 0, i32 1
  %148 = load %"struct.btSoftBody::Material"*, %"struct.btSoftBody::Material"** %m_material296, align 4
  %m_flags297 = getelementptr inbounds %"struct.btSoftBody::Material", %"struct.btSoftBody::Material"* %148, i32 0, i32 4
  %149 = load i32, i32* %m_flags297, align 4
  %and298 = and i32 %149, 1
  %cmp299 = icmp eq i32 0, %and298
  br i1 %cmp299, label %if.then300, label %if.end301

if.then300:                                       ; preds = %for.body293
  br label %for.inc334

if.end301:                                        ; preds = %for.body293
  %arrayinit.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %x302, i32 0, i32 0
  %150 = load %"struct.btSoftBody::Face"*, %"struct.btSoftBody::Face"** %f, align 4
  %m_n303 = getelementptr inbounds %"struct.btSoftBody::Face", %"struct.btSoftBody::Face"* %150, i32 0, i32 1
  %arrayidx304 = getelementptr inbounds [3 x %"struct.btSoftBody::Node"*], [3 x %"struct.btSoftBody::Node"*]* %m_n303, i32 0, i32 0
  %151 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx304, align 4
  %m_x305 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %151, i32 0, i32 1
  %152 = bitcast %class.btVector3* %arrayinit.begin to i8*
  %153 = bitcast %class.btVector3* %m_x305 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %152, i8* align 4 %153, i32 16, i1 false)
  %arrayinit.element = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin, i32 1
  %154 = load %"struct.btSoftBody::Face"*, %"struct.btSoftBody::Face"** %f, align 4
  %m_n306 = getelementptr inbounds %"struct.btSoftBody::Face", %"struct.btSoftBody::Face"* %154, i32 0, i32 1
  %arrayidx307 = getelementptr inbounds [3 x %"struct.btSoftBody::Node"*], [3 x %"struct.btSoftBody::Node"*]* %m_n306, i32 0, i32 1
  %155 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx307, align 4
  %m_x308 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %155, i32 0, i32 1
  %156 = bitcast %class.btVector3* %arrayinit.element to i8*
  %157 = bitcast %class.btVector3* %m_x308 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %156, i8* align 4 %157, i32 16, i1 false)
  %arrayinit.element309 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element, i32 1
  %158 = load %"struct.btSoftBody::Face"*, %"struct.btSoftBody::Face"** %f, align 4
  %m_n310 = getelementptr inbounds %"struct.btSoftBody::Face", %"struct.btSoftBody::Face"* %158, i32 0, i32 1
  %arrayidx311 = getelementptr inbounds [3 x %"struct.btSoftBody::Node"*], [3 x %"struct.btSoftBody::Node"*]* %m_n310, i32 0, i32 2
  %159 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx311, align 4
  %m_x312 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %159, i32 0, i32 1
  %160 = bitcast %class.btVector3* %arrayinit.element309 to i8*
  %161 = bitcast %class.btVector3* %m_x312 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %160, i8* align 4 %161, i32 16, i1 false)
  %arrayidx316 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %x302, i32 0, i32 0
  %arrayidx317 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %x302, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp315, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx316, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx317)
  %arrayidx318 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %x302, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp314, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp315, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx318)
  store float 3.000000e+00, float* %ref.tmp319, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %c313, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp314, float* nonnull align 4 dereferenceable(4) %ref.tmp319)
  %162 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx323 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %x302, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp322, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx323, %class.btVector3* nonnull align 4 dereferenceable(16) %c313)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp321, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp322, float* nonnull align 4 dereferenceable(4) %scl285)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp320, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp321, %class.btVector3* nonnull align 4 dereferenceable(16) %c313)
  %arrayidx327 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %x302, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp326, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx327, %class.btVector3* nonnull align 4 dereferenceable(16) %c313)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp325, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp326, float* nonnull align 4 dereferenceable(4) %scl285)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp324, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp325, %class.btVector3* nonnull align 4 dereferenceable(16) %c313)
  %arrayidx331 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %x302, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp330, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx331, %class.btVector3* nonnull align 4 dereferenceable(16) %c313)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp329, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp330, float* nonnull align 4 dereferenceable(4) %scl285)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp328, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp329, %class.btVector3* nonnull align 4 dereferenceable(16) %c313)
  %163 = bitcast %class.btIDebugDraw* %162 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)***
  %vtable332 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*** %163, align 4
  %vfn333 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vtable332, i64 9
  %164 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vfn333, align 4
  call void %164(%class.btIDebugDraw* %162, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp320, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp324, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp328, %class.btVector3* nonnull align 4 dereferenceable(16) %col, float 1.000000e+00)
  br label %for.inc334

for.inc334:                                       ; preds = %if.end301, %if.then300
  %165 = load i32, i32* %i, align 4
  %inc335 = add nsw i32 %165, 1
  store i32 %inc335, i32* %i, align 4
  br label %for.cond290

for.end336:                                       ; preds = %for.cond290
  br label %if.end337

if.end337:                                        ; preds = %for.end336, %if.end281
  %166 = load i32, i32* %drawflags.addr, align 4
  %and338 = and i32 %166, 8
  %cmp339 = icmp ne i32 0, %and338
  br i1 %cmp339, label %if.then340, label %if.end446

if.then340:                                       ; preds = %if.end337
  store float 0x3FE99999A0000000, float* %scl341, align 4
  store float 1.000000e+00, float* %alp342, align 4
  store float 0x3FD3333340000000, float* %ref.tmp344, align 4
  store float 0x3FD3333340000000, float* %ref.tmp345, align 4
  store float 0x3FE6666660000000, float* %ref.tmp346, align 4
  %call347 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %col343, float* nonnull align 4 dereferenceable(4) %ref.tmp344, float* nonnull align 4 dereferenceable(4) %ref.tmp345, float* nonnull align 4 dereferenceable(4) %ref.tmp346)
  store i32 0, i32* %i348, align 4
  br label %for.cond349

for.cond349:                                      ; preds = %for.inc443, %if.then340
  %167 = load i32, i32* %i348, align 4
  %168 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_tetras = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %168, i32 0, i32 12
  %call350 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody5TetraEE4sizeEv(%class.btAlignedObjectArray.37* %m_tetras)
  %cmp351 = icmp slt i32 %167, %call350
  br i1 %cmp351, label %for.body352, label %for.end445

for.body352:                                      ; preds = %for.cond349
  %169 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_tetras353 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %169, i32 0, i32 12
  %170 = load i32, i32* %i348, align 4
  %call354 = call nonnull align 4 dereferenceable(104) %"struct.btSoftBody::Tetra"* @_ZN20btAlignedObjectArrayIN10btSoftBody5TetraEEixEi(%class.btAlignedObjectArray.37* %m_tetras353, i32 %170)
  store %"struct.btSoftBody::Tetra"* %call354, %"struct.btSoftBody::Tetra"** %t, align 4
  %171 = load %"struct.btSoftBody::Tetra"*, %"struct.btSoftBody::Tetra"** %t, align 4
  %172 = bitcast %"struct.btSoftBody::Tetra"* %171 to %"struct.btSoftBody::Feature"*
  %m_material355 = getelementptr inbounds %"struct.btSoftBody::Feature", %"struct.btSoftBody::Feature"* %172, i32 0, i32 1
  %173 = load %"struct.btSoftBody::Material"*, %"struct.btSoftBody::Material"** %m_material355, align 4
  %m_flags356 = getelementptr inbounds %"struct.btSoftBody::Material", %"struct.btSoftBody::Material"* %173, i32 0, i32 4
  %174 = load i32, i32* %m_flags356, align 4
  %and357 = and i32 %174, 1
  %cmp358 = icmp eq i32 0, %and357
  br i1 %cmp358, label %if.then359, label %if.end360

if.then359:                                       ; preds = %for.body352
  br label %for.inc443

if.end360:                                        ; preds = %for.body352
  %arrayinit.begin362 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 0
  %175 = load %"struct.btSoftBody::Tetra"*, %"struct.btSoftBody::Tetra"** %t, align 4
  %m_n363 = getelementptr inbounds %"struct.btSoftBody::Tetra", %"struct.btSoftBody::Tetra"* %175, i32 0, i32 1
  %arrayidx364 = getelementptr inbounds [4 x %"struct.btSoftBody::Node"*], [4 x %"struct.btSoftBody::Node"*]* %m_n363, i32 0, i32 0
  %176 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx364, align 4
  %m_x365 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %176, i32 0, i32 1
  %177 = bitcast %class.btVector3* %arrayinit.begin362 to i8*
  %178 = bitcast %class.btVector3* %m_x365 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %177, i8* align 4 %178, i32 16, i1 false)
  %arrayinit.element366 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin362, i32 1
  %179 = load %"struct.btSoftBody::Tetra"*, %"struct.btSoftBody::Tetra"** %t, align 4
  %m_n367 = getelementptr inbounds %"struct.btSoftBody::Tetra", %"struct.btSoftBody::Tetra"* %179, i32 0, i32 1
  %arrayidx368 = getelementptr inbounds [4 x %"struct.btSoftBody::Node"*], [4 x %"struct.btSoftBody::Node"*]* %m_n367, i32 0, i32 1
  %180 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx368, align 4
  %m_x369 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %180, i32 0, i32 1
  %181 = bitcast %class.btVector3* %arrayinit.element366 to i8*
  %182 = bitcast %class.btVector3* %m_x369 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %181, i8* align 4 %182, i32 16, i1 false)
  %arrayinit.element370 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element366, i32 1
  %183 = load %"struct.btSoftBody::Tetra"*, %"struct.btSoftBody::Tetra"** %t, align 4
  %m_n371 = getelementptr inbounds %"struct.btSoftBody::Tetra", %"struct.btSoftBody::Tetra"* %183, i32 0, i32 1
  %arrayidx372 = getelementptr inbounds [4 x %"struct.btSoftBody::Node"*], [4 x %"struct.btSoftBody::Node"*]* %m_n371, i32 0, i32 2
  %184 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx372, align 4
  %m_x373 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %184, i32 0, i32 1
  %185 = bitcast %class.btVector3* %arrayinit.element370 to i8*
  %186 = bitcast %class.btVector3* %m_x373 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %185, i8* align 4 %186, i32 16, i1 false)
  %arrayinit.element374 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element370, i32 1
  %187 = load %"struct.btSoftBody::Tetra"*, %"struct.btSoftBody::Tetra"** %t, align 4
  %m_n375 = getelementptr inbounds %"struct.btSoftBody::Tetra", %"struct.btSoftBody::Tetra"* %187, i32 0, i32 1
  %arrayidx376 = getelementptr inbounds [4 x %"struct.btSoftBody::Node"*], [4 x %"struct.btSoftBody::Node"*]* %m_n375, i32 0, i32 3
  %188 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx376, align 4
  %m_x377 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %188, i32 0, i32 1
  %189 = bitcast %class.btVector3* %arrayinit.element374 to i8*
  %190 = bitcast %class.btVector3* %m_x377 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %189, i8* align 4 %190, i32 16, i1 false)
  %arrayidx382 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 0
  %arrayidx383 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp381, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx382, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx383)
  %arrayidx384 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp380, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp381, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx384)
  %arrayidx385 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 3
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp379, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp380, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx385)
  store float 4.000000e+00, float* %ref.tmp386, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %c378, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp379, float* nonnull align 4 dereferenceable(4) %ref.tmp386)
  %191 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx390 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp389, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx390, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp388, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp389, float* nonnull align 4 dereferenceable(4) %scl341)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp387, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp388, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  %arrayidx394 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp393, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx394, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp392, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp393, float* nonnull align 4 dereferenceable(4) %scl341)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp391, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp392, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  %arrayidx398 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp397, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx398, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp396, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp397, float* nonnull align 4 dereferenceable(4) %scl341)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp395, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp396, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  %192 = bitcast %class.btIDebugDraw* %191 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)***
  %vtable399 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*** %192, align 4
  %vfn400 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vtable399, i64 9
  %193 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vfn400, align 4
  call void %193(%class.btIDebugDraw* %191, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp387, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp391, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp395, %class.btVector3* nonnull align 4 dereferenceable(16) %col343, float 1.000000e+00)
  %194 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx404 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp403, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx404, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp402, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp403, float* nonnull align 4 dereferenceable(4) %scl341)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp401, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp402, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  %arrayidx408 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp407, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx408, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp406, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp407, float* nonnull align 4 dereferenceable(4) %scl341)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp405, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp406, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  %arrayidx412 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 3
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp411, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx412, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp410, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp411, float* nonnull align 4 dereferenceable(4) %scl341)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp409, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp410, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  %195 = bitcast %class.btIDebugDraw* %194 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)***
  %vtable413 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*** %195, align 4
  %vfn414 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vtable413, i64 9
  %196 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vfn414, align 4
  call void %196(%class.btIDebugDraw* %194, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp401, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp405, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp409, %class.btVector3* nonnull align 4 dereferenceable(16) %col343, float 1.000000e+00)
  %197 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx418 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp417, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx418, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp416, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp417, float* nonnull align 4 dereferenceable(4) %scl341)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp415, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp416, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  %arrayidx422 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp421, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx422, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp420, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp421, float* nonnull align 4 dereferenceable(4) %scl341)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp419, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp420, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  %arrayidx426 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 3
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp425, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx426, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp424, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp425, float* nonnull align 4 dereferenceable(4) %scl341)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp423, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp424, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  %198 = bitcast %class.btIDebugDraw* %197 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)***
  %vtable427 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*** %198, align 4
  %vfn428 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vtable427, i64 9
  %199 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vfn428, align 4
  call void %199(%class.btIDebugDraw* %197, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp415, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp419, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp423, %class.btVector3* nonnull align 4 dereferenceable(16) %col343, float 1.000000e+00)
  %200 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx432 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp431, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx432, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp430, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp431, float* nonnull align 4 dereferenceable(4) %scl341)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp429, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp430, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  %arrayidx436 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp435, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx436, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp434, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp435, float* nonnull align 4 dereferenceable(4) %scl341)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp433, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp434, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  %arrayidx440 = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %x361, i32 0, i32 3
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp439, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx440, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp438, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp439, float* nonnull align 4 dereferenceable(4) %scl341)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp437, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp438, %class.btVector3* nonnull align 4 dereferenceable(16) %c378)
  %201 = bitcast %class.btIDebugDraw* %200 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)***
  %vtable441 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*** %201, align 4
  %vfn442 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vtable441, i64 9
  %202 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btVector3*, float)** %vfn442, align 4
  call void %202(%class.btIDebugDraw* %200, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp429, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp433, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp437, %class.btVector3* nonnull align 4 dereferenceable(16) %col343, float 1.000000e+00)
  br label %for.inc443

for.inc443:                                       ; preds = %if.end360, %if.then359
  %203 = load i32, i32* %i348, align 4
  %inc444 = add nsw i32 %203, 1
  store i32 %inc444, i32* %i348, align 4
  br label %for.cond349

for.end445:                                       ; preds = %for.cond349
  br label %if.end446

if.end446:                                        ; preds = %for.end445, %if.end337
  br label %if.end447

if.end447:                                        ; preds = %if.end446, %for.end77
  %204 = load i32, i32* %drawflags.addr, align 4
  %and448 = and i32 %204, 64
  %cmp449 = icmp ne i32 0, %and448
  br i1 %cmp449, label %if.then450, label %if.end508

if.then450:                                       ; preds = %if.end447
  store i32 0, i32* %i, align 4
  br label %for.cond451

for.cond451:                                      ; preds = %for.inc479, %if.then450
  %205 = load i32, i32* %i, align 4
  %206 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_anchors = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %206, i32 0, i32 13
  %call452 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody6AnchorEE4sizeEv(%class.btAlignedObjectArray.41* %m_anchors)
  %cmp453 = icmp slt i32 %205, %call452
  br i1 %cmp453, label %for.body454, label %for.end481

for.body454:                                      ; preds = %for.cond451
  %207 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_anchors455 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %207, i32 0, i32 13
  %208 = load i32, i32* %i, align 4
  %call456 = call nonnull align 4 dereferenceable(96) %"struct.btSoftBody::Anchor"* @_ZN20btAlignedObjectArrayIN10btSoftBody6AnchorEEixEi(%class.btAlignedObjectArray.41* %m_anchors455, i32 %208)
  store %"struct.btSoftBody::Anchor"* %call456, %"struct.btSoftBody::Anchor"** %a, align 4
  %209 = load %"struct.btSoftBody::Anchor"*, %"struct.btSoftBody::Anchor"** %a, align 4
  %m_body = getelementptr inbounds %"struct.btSoftBody::Anchor", %"struct.btSoftBody::Anchor"* %209, i32 0, i32 2
  %210 = load %class.btRigidBody*, %class.btRigidBody** %m_body, align 4
  %211 = bitcast %class.btRigidBody* %210 to %class.btCollisionObject*
  %call457 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %211)
  %212 = load %"struct.btSoftBody::Anchor"*, %"struct.btSoftBody::Anchor"** %a, align 4
  %m_local = getelementptr inbounds %"struct.btSoftBody::Anchor", %"struct.btSoftBody::Anchor"* %212, i32 0, i32 1
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %q, %class.btTransform* %call457, %class.btVector3* nonnull align 4 dereferenceable(16) %m_local)
  %213 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %214 = load %"struct.btSoftBody::Anchor"*, %"struct.btSoftBody::Anchor"** %a, align 4
  %m_node458 = getelementptr inbounds %"struct.btSoftBody::Anchor", %"struct.btSoftBody::Anchor"* %214, i32 0, i32 0
  %215 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %m_node458, align 4
  %m_x459 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %215, i32 0, i32 1
  store float 1.000000e+00, float* %ref.tmp461, align 4
  store float 0.000000e+00, float* %ref.tmp462, align 4
  store float 0.000000e+00, float* %ref.tmp463, align 4
  %call464 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp460, float* nonnull align 4 dereferenceable(4) %ref.tmp461, float* nonnull align 4 dereferenceable(4) %ref.tmp462, float* nonnull align 4 dereferenceable(4) %ref.tmp463)
  call void @_ZL10drawVertexP12btIDebugDrawRK9btVector3fS3_(%class.btIDebugDraw* %213, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x459, float 2.500000e-01, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp460)
  %216 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  store float 0.000000e+00, float* %ref.tmp466, align 4
  store float 1.000000e+00, float* %ref.tmp467, align 4
  store float 0.000000e+00, float* %ref.tmp468, align 4
  %call469 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp465, float* nonnull align 4 dereferenceable(4) %ref.tmp466, float* nonnull align 4 dereferenceable(4) %ref.tmp467, float* nonnull align 4 dereferenceable(4) %ref.tmp468)
  call void @_ZL10drawVertexP12btIDebugDrawRK9btVector3fS3_(%class.btIDebugDraw* %216, %class.btVector3* nonnull align 4 dereferenceable(16) %q, float 2.500000e-01, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp465)
  %217 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %218 = load %"struct.btSoftBody::Anchor"*, %"struct.btSoftBody::Anchor"** %a, align 4
  %m_node470 = getelementptr inbounds %"struct.btSoftBody::Anchor", %"struct.btSoftBody::Anchor"* %218, i32 0, i32 0
  %219 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %m_node470, align 4
  %m_x471 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %219, i32 0, i32 1
  store float 1.000000e+00, float* %ref.tmp473, align 4
  store float 1.000000e+00, float* %ref.tmp474, align 4
  store float 1.000000e+00, float* %ref.tmp475, align 4
  %call476 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp472, float* nonnull align 4 dereferenceable(4) %ref.tmp473, float* nonnull align 4 dereferenceable(4) %ref.tmp474, float* nonnull align 4 dereferenceable(4) %ref.tmp475)
  %220 = bitcast %class.btIDebugDraw* %217 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable477 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %220, align 4
  %vfn478 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable477, i64 4
  %221 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn478, align 4
  call void %221(%class.btIDebugDraw* %217, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x471, %class.btVector3* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp472)
  br label %for.inc479

for.inc479:                                       ; preds = %for.body454
  %222 = load i32, i32* %i, align 4
  %inc480 = add nsw i32 %222, 1
  store i32 %inc480, i32* %i, align 4
  br label %for.cond451

for.end481:                                       ; preds = %for.cond451
  store i32 0, i32* %i, align 4
  br label %for.cond482

for.cond482:                                      ; preds = %for.inc505, %for.end481
  %223 = load i32, i32* %i, align 4
  %224 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_nodes483 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %224, i32 0, i32 9
  %call484 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.25* %m_nodes483)
  %cmp485 = icmp slt i32 %223, %call484
  br i1 %cmp485, label %for.body486, label %for.end507

for.body486:                                      ; preds = %for.cond482
  %225 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_nodes488 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %225, i32 0, i32 9
  %226 = load i32, i32* %i, align 4
  %call489 = call nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.25* %m_nodes488, i32 %226)
  store %"struct.btSoftBody::Node"* %call489, %"struct.btSoftBody::Node"** %n487, align 4
  %227 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n487, align 4
  %228 = bitcast %"struct.btSoftBody::Node"* %227 to %"struct.btSoftBody::Feature"*
  %m_material490 = getelementptr inbounds %"struct.btSoftBody::Feature", %"struct.btSoftBody::Feature"* %228, i32 0, i32 1
  %229 = load %"struct.btSoftBody::Material"*, %"struct.btSoftBody::Material"** %m_material490, align 4
  %m_flags491 = getelementptr inbounds %"struct.btSoftBody::Material", %"struct.btSoftBody::Material"* %229, i32 0, i32 4
  %230 = load i32, i32* %m_flags491, align 4
  %and492 = and i32 %230, 1
  %cmp493 = icmp eq i32 0, %and492
  br i1 %cmp493, label %if.then494, label %if.end495

if.then494:                                       ; preds = %for.body486
  br label %for.inc505

if.end495:                                        ; preds = %for.body486
  %231 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n487, align 4
  %m_im = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %231, i32 0, i32 6
  %232 = load float, float* %m_im, align 4
  %cmp496 = fcmp ole float %232, 0.000000e+00
  br i1 %cmp496, label %if.then497, label %if.end504

if.then497:                                       ; preds = %if.end495
  %233 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %234 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n487, align 4
  %m_x498 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %234, i32 0, i32 1
  store float 1.000000e+00, float* %ref.tmp500, align 4
  store float 0.000000e+00, float* %ref.tmp501, align 4
  store float 0.000000e+00, float* %ref.tmp502, align 4
  %call503 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp499, float* nonnull align 4 dereferenceable(4) %ref.tmp500, float* nonnull align 4 dereferenceable(4) %ref.tmp501, float* nonnull align 4 dereferenceable(4) %ref.tmp502)
  call void @_ZL10drawVertexP12btIDebugDrawRK9btVector3fS3_(%class.btIDebugDraw* %233, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x498, float 2.500000e-01, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp499)
  br label %if.end504

if.end504:                                        ; preds = %if.then497, %if.end495
  br label %for.inc505

for.inc505:                                       ; preds = %if.end504, %if.then494
  %235 = load i32, i32* %i, align 4
  %inc506 = add nsw i32 %235, 1
  store i32 %inc506, i32* %i, align 4
  br label %for.cond482

for.end507:                                       ; preds = %for.cond482
  br label %if.end508

if.end508:                                        ; preds = %for.end507, %if.end447
  %236 = load i32, i32* %drawflags.addr, align 4
  %and509 = and i32 %236, 128
  %cmp510 = icmp ne i32 0, %and509
  br i1 %cmp510, label %if.then511, label %if.end538

if.then511:                                       ; preds = %if.end508
  store i32 0, i32* %i, align 4
  br label %for.cond512

for.cond512:                                      ; preds = %for.inc535, %if.then511
  %237 = load i32, i32* %i, align 4
  %238 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_notes = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %238, i32 0, i32 8
  %call513 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NoteEE4sizeEv(%class.btAlignedObjectArray.20* %m_notes)
  %cmp514 = icmp slt i32 %237, %call513
  br i1 %cmp514, label %for.body515, label %for.end537

for.body515:                                      ; preds = %for.cond512
  %239 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_notes517 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %239, i32 0, i32 8
  %240 = load i32, i32* %i, align 4
  %call518 = call nonnull align 4 dereferenceable(60) %"struct.btSoftBody::Note"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NoteEEixEi(%class.btAlignedObjectArray.20* %m_notes517, i32 %240)
  store %"struct.btSoftBody::Note"* %call518, %"struct.btSoftBody::Note"** %n516, align 4
  %241 = load %"struct.btSoftBody::Note"*, %"struct.btSoftBody::Note"** %n516, align 4
  %m_offset519 = getelementptr inbounds %"struct.btSoftBody::Note", %"struct.btSoftBody::Note"* %241, i32 0, i32 2
  %242 = bitcast %class.btVector3* %p to i8*
  %243 = bitcast %class.btVector3* %m_offset519 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %242, i8* align 4 %243, i32 16, i1 false)
  store i32 0, i32* %j520, align 4
  br label %for.cond521

for.cond521:                                      ; preds = %for.inc530, %for.body515
  %244 = load i32, i32* %j520, align 4
  %245 = load %"struct.btSoftBody::Note"*, %"struct.btSoftBody::Note"** %n516, align 4
  %m_rank = getelementptr inbounds %"struct.btSoftBody::Note", %"struct.btSoftBody::Note"* %245, i32 0, i32 3
  %246 = load i32, i32* %m_rank, align 4
  %cmp522 = icmp slt i32 %244, %246
  br i1 %cmp522, label %for.body523, label %for.end532

for.body523:                                      ; preds = %for.cond521
  %247 = load %"struct.btSoftBody::Note"*, %"struct.btSoftBody::Note"** %n516, align 4
  %m_nodes525 = getelementptr inbounds %"struct.btSoftBody::Note", %"struct.btSoftBody::Note"* %247, i32 0, i32 4
  %248 = load i32, i32* %j520, align 4
  %arrayidx526 = getelementptr inbounds [4 x %"struct.btSoftBody::Node"*], [4 x %"struct.btSoftBody::Node"*]* %m_nodes525, i32 0, i32 %248
  %249 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx526, align 4
  %m_x527 = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %249, i32 0, i32 1
  %250 = load %"struct.btSoftBody::Note"*, %"struct.btSoftBody::Note"** %n516, align 4
  %m_coords = getelementptr inbounds %"struct.btSoftBody::Note", %"struct.btSoftBody::Note"* %250, i32 0, i32 5
  %251 = load i32, i32* %j520, align 4
  %arrayidx528 = getelementptr inbounds [4 x float], [4 x float]* %m_coords, i32 0, i32 %251
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp524, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x527, float* nonnull align 4 dereferenceable(4) %arrayidx528)
  %call529 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %p, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp524)
  br label %for.inc530

for.inc530:                                       ; preds = %for.body523
  %252 = load i32, i32* %j520, align 4
  %inc531 = add nsw i32 %252, 1
  store i32 %inc531, i32* %j520, align 4
  br label %for.cond521

for.end532:                                       ; preds = %for.cond521
  %253 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %254 = load %"struct.btSoftBody::Note"*, %"struct.btSoftBody::Note"** %n516, align 4
  %m_text = getelementptr inbounds %"struct.btSoftBody::Note", %"struct.btSoftBody::Note"* %254, i32 0, i32 1
  %255 = load i8*, i8** %m_text, align 4
  %256 = bitcast %class.btIDebugDraw* %253 to void (%class.btIDebugDraw*, %class.btVector3*, i8*)***
  %vtable533 = load void (%class.btIDebugDraw*, %class.btVector3*, i8*)**, void (%class.btIDebugDraw*, %class.btVector3*, i8*)*** %256, align 4
  %vfn534 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, i8*)*, void (%class.btIDebugDraw*, %class.btVector3*, i8*)** %vtable533, i64 12
  %257 = load void (%class.btIDebugDraw*, %class.btVector3*, i8*)*, void (%class.btIDebugDraw*, %class.btVector3*, i8*)** %vfn534, align 4
  call void %257(%class.btIDebugDraw* %253, %class.btVector3* nonnull align 4 dereferenceable(16) %p, i8* %255)
  br label %for.inc535

for.inc535:                                       ; preds = %for.end532
  %258 = load i32, i32* %i, align 4
  %inc536 = add nsw i32 %258, 1
  store i32 %inc536, i32* %i, align 4
  br label %for.cond512

for.end537:                                       ; preds = %for.cond512
  br label %if.end538

if.end538:                                        ; preds = %for.end537, %if.end508
  %259 = load i32, i32* %drawflags.addr, align 4
  %and539 = and i32 %259, 512
  %cmp540 = icmp ne i32 0, %and539
  br i1 %cmp540, label %if.then541, label %if.end542

if.then541:                                       ; preds = %if.end538
  %260 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %261 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  call void @_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %260, %class.btIDebugDraw* %261, i32 0, i32 -1)
  br label %if.end542

if.end542:                                        ; preds = %if.then541, %if.end538
  %262 = load i32, i32* %drawflags.addr, align 4
  %and543 = and i32 %262, 1024
  %cmp544 = icmp ne i32 0, %and543
  br i1 %cmp544, label %if.then545, label %if.end546

if.then545:                                       ; preds = %if.end542
  %263 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %264 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  call void @_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %263, %class.btIDebugDraw* %264, i32 0, i32 -1)
  br label %if.end546

if.end546:                                        ; preds = %if.then545, %if.end542
  %265 = load i32, i32* %drawflags.addr, align 4
  %and547 = and i32 %265, 2048
  %cmp548 = icmp ne i32 0, %and547
  br i1 %cmp548, label %if.then549, label %if.end550

if.then549:                                       ; preds = %if.end546
  %266 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %267 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  call void @_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %266, %class.btIDebugDraw* %267, i32 0, i32 -1)
  br label %if.end550

if.end550:                                        ; preds = %if.then549, %if.end546
  %268 = load i32, i32* %drawflags.addr, align 4
  %and551 = and i32 %268, 4096
  %cmp552 = icmp ne i32 0, %and551
  br i1 %cmp552, label %if.then553, label %if.end669

if.then553:                                       ; preds = %if.end550
  store i32 0, i32* %i, align 4
  br label %for.cond554

for.cond554:                                      ; preds = %for.inc666, %if.then553
  %269 = load i32, i32* %i, align 4
  %270 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_joints = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %270, i32 0, i32 16
  %call555 = call i32 @_ZNK20btAlignedObjectArrayIPN10btSoftBody5JointEE4sizeEv(%class.btAlignedObjectArray.57* %m_joints)
  %cmp556 = icmp slt i32 %269, %call555
  br i1 %cmp556, label %for.body557, label %for.end668

for.body557:                                      ; preds = %for.cond554
  %271 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_joints558 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %271, i32 0, i32 16
  %272 = load i32, i32* %i, align 4
  %call559 = call nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Joint"** @_ZN20btAlignedObjectArrayIPN10btSoftBody5JointEEixEi(%class.btAlignedObjectArray.57* %m_joints558, i32 %272)
  %273 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %call559, align 4
  store %"struct.btSoftBody::Joint"* %273, %"struct.btSoftBody::Joint"** %pj, align 4
  %274 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4
  %275 = bitcast %"struct.btSoftBody::Joint"* %274 to i32 (%"struct.btSoftBody::Joint"*)***
  %vtable560 = load i32 (%"struct.btSoftBody::Joint"*)**, i32 (%"struct.btSoftBody::Joint"*)*** %275, align 4
  %vfn561 = getelementptr inbounds i32 (%"struct.btSoftBody::Joint"*)*, i32 (%"struct.btSoftBody::Joint"*)** %vtable560, i64 5
  %276 = load i32 (%"struct.btSoftBody::Joint"*)*, i32 (%"struct.btSoftBody::Joint"*)** %vfn561, align 4
  %call562 = call i32 %276(%"struct.btSoftBody::Joint"* %274)
  switch i32 %call562, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb603
  ]

sw.bb:                                            ; preds = %for.body557
  %277 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4
  %278 = bitcast %"struct.btSoftBody::Joint"* %277 to %"struct.btSoftBody::LJoint"*
  store %"struct.btSoftBody::LJoint"* %278, %"struct.btSoftBody::LJoint"** %pjl, align 4
  %279 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4
  %m_bodies = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %279, i32 0, i32 1
  %arrayidx563 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies, i32 0, i32 0
  %call564 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx563)
  %280 = load %"struct.btSoftBody::LJoint"*, %"struct.btSoftBody::LJoint"** %pjl, align 4
  %281 = bitcast %"struct.btSoftBody::LJoint"* %280 to %"struct.btSoftBody::Joint"*
  %m_refs = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %281, i32 0, i32 2
  %arrayidx565 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %m_refs, i32 0, i32 0
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %a0, %class.btTransform* %call564, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx565)
  %282 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4
  %m_bodies566 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %282, i32 0, i32 1
  %arrayidx567 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies566, i32 0, i32 1
  %call568 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx567)
  %283 = load %"struct.btSoftBody::LJoint"*, %"struct.btSoftBody::LJoint"** %pjl, align 4
  %284 = bitcast %"struct.btSoftBody::LJoint"* %283 to %"struct.btSoftBody::Joint"*
  %m_refs569 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %284, i32 0, i32 2
  %arrayidx570 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %m_refs569, i32 0, i32 1
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %a1, %class.btTransform* %call568, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx570)
  %285 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %286 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4
  %m_bodies571 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %286, i32 0, i32 1
  %arrayidx572 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies571, i32 0, i32 0
  %call573 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx572)
  %call574 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call573)
  store float 1.000000e+00, float* %ref.tmp576, align 4
  store float 1.000000e+00, float* %ref.tmp577, align 4
  store float 0.000000e+00, float* %ref.tmp578, align 4
  %call579 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp575, float* nonnull align 4 dereferenceable(4) %ref.tmp576, float* nonnull align 4 dereferenceable(4) %ref.tmp577, float* nonnull align 4 dereferenceable(4) %ref.tmp578)
  %287 = bitcast %class.btIDebugDraw* %285 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable580 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %287, align 4
  %vfn581 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable580, i64 4
  %288 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn581, align 4
  call void %288(%class.btIDebugDraw* %285, %class.btVector3* nonnull align 4 dereferenceable(16) %call574, %class.btVector3* nonnull align 4 dereferenceable(16) %a0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp575)
  %289 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %290 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4
  %m_bodies582 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %290, i32 0, i32 1
  %arrayidx583 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies582, i32 0, i32 1
  %call584 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx583)
  %call585 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call584)
  store float 0.000000e+00, float* %ref.tmp587, align 4
  store float 1.000000e+00, float* %ref.tmp588, align 4
  store float 1.000000e+00, float* %ref.tmp589, align 4
  %call590 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp586, float* nonnull align 4 dereferenceable(4) %ref.tmp587, float* nonnull align 4 dereferenceable(4) %ref.tmp588, float* nonnull align 4 dereferenceable(4) %ref.tmp589)
  %291 = bitcast %class.btIDebugDraw* %289 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable591 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %291, align 4
  %vfn592 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable591, i64 4
  %292 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn592, align 4
  call void %292(%class.btIDebugDraw* %289, %class.btVector3* nonnull align 4 dereferenceable(16) %call585, %class.btVector3* nonnull align 4 dereferenceable(16) %a1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp586)
  %293 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  store float 1.000000e+00, float* %ref.tmp594, align 4
  store float 1.000000e+00, float* %ref.tmp595, align 4
  store float 0.000000e+00, float* %ref.tmp596, align 4
  %call597 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp593, float* nonnull align 4 dereferenceable(4) %ref.tmp594, float* nonnull align 4 dereferenceable(4) %ref.tmp595, float* nonnull align 4 dereferenceable(4) %ref.tmp596)
  call void @_ZL10drawVertexP12btIDebugDrawRK9btVector3fS3_(%class.btIDebugDraw* %293, %class.btVector3* nonnull align 4 dereferenceable(16) %a0, float 2.500000e-01, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp593)
  %294 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  store float 0.000000e+00, float* %ref.tmp599, align 4
  store float 1.000000e+00, float* %ref.tmp600, align 4
  store float 1.000000e+00, float* %ref.tmp601, align 4
  %call602 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp598, float* nonnull align 4 dereferenceable(4) %ref.tmp599, float* nonnull align 4 dereferenceable(4) %ref.tmp600, float* nonnull align 4 dereferenceable(4) %ref.tmp601)
  call void @_ZL10drawVertexP12btIDebugDrawRK9btVector3fS3_(%class.btIDebugDraw* %294, %class.btVector3* nonnull align 4 dereferenceable(16) %a1, float 2.500000e-01, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp598)
  br label %sw.epilog

sw.bb603:                                         ; preds = %for.body557
  %295 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4
  %m_bodies604 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %295, i32 0, i32 1
  %arrayidx605 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies604, i32 0, i32 0
  %call606 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx605)
  %call607 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call606)
  %296 = bitcast %class.btVector3* %o0 to i8*
  %297 = bitcast %class.btVector3* %call607 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %296, i8* align 4 %297, i32 16, i1 false)
  %298 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4
  %m_bodies608 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %298, i32 0, i32 1
  %arrayidx609 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies608, i32 0, i32 1
  %call610 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx609)
  %call611 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %call610)
  %299 = bitcast %class.btVector3* %o1 to i8*
  %300 = bitcast %class.btVector3* %call611 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %299, i8* align 4 %300, i32 16, i1 false)
  %301 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4
  %m_bodies613 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %301, i32 0, i32 1
  %arrayidx614 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies613, i32 0, i32 0
  %call615 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx614)
  %call616 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call615)
  %302 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4
  %m_refs617 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %302, i32 0, i32 2
  %arrayidx618 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %m_refs617, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %a0612, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call616, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx618)
  %303 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4
  %m_bodies620 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %303, i32 0, i32 1
  %arrayidx621 = getelementptr inbounds [2 x %"struct.btSoftBody::Body"], [2 x %"struct.btSoftBody::Body"]* %m_bodies620, i32 0, i32 1
  %call622 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %arrayidx621)
  %call623 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call622)
  %304 = load %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %pj, align 4
  %m_refs624 = getelementptr inbounds %"struct.btSoftBody::Joint", %"struct.btSoftBody::Joint"* %304, i32 0, i32 2
  %arrayidx625 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %m_refs624, i32 0, i32 1
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %a1619, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call623, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx625)
  %305 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  store float 1.000000e+01, float* %ref.tmp628, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp627, %class.btVector3* nonnull align 4 dereferenceable(16) %a0612, float* nonnull align 4 dereferenceable(4) %ref.tmp628)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp626, %class.btVector3* nonnull align 4 dereferenceable(16) %o0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp627)
  store float 1.000000e+00, float* %ref.tmp630, align 4
  store float 1.000000e+00, float* %ref.tmp631, align 4
  store float 0.000000e+00, float* %ref.tmp632, align 4
  %call633 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp629, float* nonnull align 4 dereferenceable(4) %ref.tmp630, float* nonnull align 4 dereferenceable(4) %ref.tmp631, float* nonnull align 4 dereferenceable(4) %ref.tmp632)
  %306 = bitcast %class.btIDebugDraw* %305 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable634 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %306, align 4
  %vfn635 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable634, i64 4
  %307 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn635, align 4
  call void %307(%class.btIDebugDraw* %305, %class.btVector3* nonnull align 4 dereferenceable(16) %o0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp626, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp629)
  %308 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  store float 1.000000e+01, float* %ref.tmp638, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp637, %class.btVector3* nonnull align 4 dereferenceable(16) %a1619, float* nonnull align 4 dereferenceable(4) %ref.tmp638)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp636, %class.btVector3* nonnull align 4 dereferenceable(16) %o0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp637)
  store float 1.000000e+00, float* %ref.tmp640, align 4
  store float 1.000000e+00, float* %ref.tmp641, align 4
  store float 0.000000e+00, float* %ref.tmp642, align 4
  %call643 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp639, float* nonnull align 4 dereferenceable(4) %ref.tmp640, float* nonnull align 4 dereferenceable(4) %ref.tmp641, float* nonnull align 4 dereferenceable(4) %ref.tmp642)
  %309 = bitcast %class.btIDebugDraw* %308 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable644 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %309, align 4
  %vfn645 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable644, i64 4
  %310 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn645, align 4
  call void %310(%class.btIDebugDraw* %308, %class.btVector3* nonnull align 4 dereferenceable(16) %o0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp636, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp639)
  %311 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  store float 1.000000e+01, float* %ref.tmp648, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp647, %class.btVector3* nonnull align 4 dereferenceable(16) %a0612, float* nonnull align 4 dereferenceable(4) %ref.tmp648)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp646, %class.btVector3* nonnull align 4 dereferenceable(16) %o1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp647)
  store float 0.000000e+00, float* %ref.tmp650, align 4
  store float 1.000000e+00, float* %ref.tmp651, align 4
  store float 1.000000e+00, float* %ref.tmp652, align 4
  %call653 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp649, float* nonnull align 4 dereferenceable(4) %ref.tmp650, float* nonnull align 4 dereferenceable(4) %ref.tmp651, float* nonnull align 4 dereferenceable(4) %ref.tmp652)
  %312 = bitcast %class.btIDebugDraw* %311 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable654 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %312, align 4
  %vfn655 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable654, i64 4
  %313 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn655, align 4
  call void %313(%class.btIDebugDraw* %311, %class.btVector3* nonnull align 4 dereferenceable(16) %o1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp646, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp649)
  %314 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  store float 1.000000e+01, float* %ref.tmp658, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp657, %class.btVector3* nonnull align 4 dereferenceable(16) %a1619, float* nonnull align 4 dereferenceable(4) %ref.tmp658)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp656, %class.btVector3* nonnull align 4 dereferenceable(16) %o1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp657)
  store float 0.000000e+00, float* %ref.tmp660, align 4
  store float 1.000000e+00, float* %ref.tmp661, align 4
  store float 1.000000e+00, float* %ref.tmp662, align 4
  %call663 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp659, float* nonnull align 4 dereferenceable(4) %ref.tmp660, float* nonnull align 4 dereferenceable(4) %ref.tmp661, float* nonnull align 4 dereferenceable(4) %ref.tmp662)
  %315 = bitcast %class.btIDebugDraw* %314 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable664 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %315, align 4
  %vfn665 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable664, i64 4
  %316 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn665, align 4
  call void %316(%class.btIDebugDraw* %314, %class.btVector3* nonnull align 4 dereferenceable(16) %o1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp656, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp659)
  br label %sw.epilog

sw.default:                                       ; preds = %for.body557
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb603, %sw.bb
  br label %for.inc666

for.inc666:                                       ; preds = %sw.epilog
  %317 = load i32, i32* %i, align 4
  %inc667 = add nsw i32 %317, 1
  store i32 %inc667, i32* %i, align 4
  br label %for.cond554

for.end668:                                       ; preds = %for.cond554
  br label %if.end669

if.end669:                                        ; preds = %for.end668, %if.end550
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

declare void @srand(i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN10btSoftBody7ClusterEE4sizeEv(%class.btAlignedObjectArray.73* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.73*, align 4
  store %class.btAlignedObjectArray.73* %this, %class.btAlignedObjectArray.73** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.73*, %class.btAlignedObjectArray.73** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.73, %class.btAlignedObjectArray.73* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Cluster"** @_ZN20btAlignedObjectArrayIPN10btSoftBody7ClusterEEixEi(%class.btAlignedObjectArray.73* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.73*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.73* %this, %class.btAlignedObjectArray.73** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.73*, %class.btAlignedObjectArray.73** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.73, %class.btAlignedObjectArray.73* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Cluster"**, %"struct.btSoftBody::Cluster"*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Cluster"*, %"struct.btSoftBody::Cluster"** %0, i32 %1
  ret %"struct.btSoftBody::Cluster"** %arrayidx
}

declare i32 @rand() #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %nrm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %nrm to i8*
  %1 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %nrm)
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.9* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %5 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %14 = load %class.btVector3*, %class.btVector3** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 %15
  %16 = bitcast %class.btVector3* %arrayidx10 to i8*
  %call11 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %16)
  %17 = bitcast i8* %call11 to %class.btVector3*
  %18 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4
  %19 = bitcast %class.btVector3* %17 to i8*
  %20 = bitcast %class.btVector3* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.60* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.60*, align 4
  store %class.btAlignedObjectArray.60* %this, %class.btAlignedObjectArray.60** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.60*, %class.btAlignedObjectArray.60** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.60, %class.btAlignedObjectArray.60* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Node"** @_ZN20btAlignedObjectArrayIPN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.60* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.60*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.60* %this, %class.btAlignedObjectArray.60** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.60*, %class.btAlignedObjectArray.60** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.60, %class.btAlignedObjectArray.60* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Node"**, %"struct.btSoftBody::Node"*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %0, i32 %1
  ret %"struct.btSoftBody::Node"** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btConvexHullComputer* @_ZN20btConvexHullComputerC2Ev(%class.btConvexHullComputer* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexHullComputer*, align 4
  store %class.btConvexHullComputer* %this, %class.btConvexHullComputer** %this.addr, align 4
  %this1 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %this.addr, align 4
  %vertices = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %vertices)
  %edges = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEC2Ev(%class.btAlignedObjectArray.85* %edges)
  %faces = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.81* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.81* %faces)
  ret %class.btConvexHullComputer* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZN20btConvexHullComputer7computeEPKfiiff(%class.btConvexHullComputer* %this, float* %coords, i32 %stride, i32 %count, float %shrink, float %shrinkClamp) #2 comdat {
entry:
  %this.addr = alloca %class.btConvexHullComputer*, align 4
  %coords.addr = alloca float*, align 4
  %stride.addr = alloca i32, align 4
  %count.addr = alloca i32, align 4
  %shrink.addr = alloca float, align 4
  %shrinkClamp.addr = alloca float, align 4
  store %class.btConvexHullComputer* %this, %class.btConvexHullComputer** %this.addr, align 4
  store float* %coords, float** %coords.addr, align 4
  store i32 %stride, i32* %stride.addr, align 4
  store i32 %count, i32* %count.addr, align 4
  store float %shrink, float* %shrink.addr, align 4
  store float %shrinkClamp, float* %shrinkClamp.addr, align 4
  %this1 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %this.addr, align 4
  %0 = load float*, float** %coords.addr, align 4
  %1 = bitcast float* %0 to i8*
  %2 = load i32, i32* %stride.addr, align 4
  %3 = load i32, i32* %count.addr, align 4
  %4 = load float, float* %shrink.addr, align 4
  %5 = load float, float* %shrinkClamp.addr, align 4
  %call = call float @_ZN20btConvexHullComputer7computeEPKvbiiff(%class.btConvexHullComputer* %this1, i8* %1, i1 zeroext false, i32 %2, i32 %3, float %4, float %5)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.81* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.81* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.btConvexHullComputer::Edge"* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi(%class.btAlignedObjectArray.85* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %0, i32 %1
  ret %"class.btConvexHullComputer::Edge"* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge17getNextEdgeOfFaceEv(%"class.btConvexHullComputer::Edge"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 1
  %0 = load i32, i32* %reverse, align 4
  %add.ptr = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 %0
  %call = call %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge19getNextEdgeOfVertexEv(%"class.btConvexHullComputer::Edge"* %add.ptr)
  ret %"class.btConvexHullComputer::Edge"* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btConvexHullComputer4Edge15getSourceVertexEv(%"class.btConvexHullComputer::Edge"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 1
  %0 = load i32, i32* %reverse, align 4
  %add.ptr = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 %0
  %targetVertex = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %add.ptr, i32 0, i32 2
  %1 = load i32, i32* %targetVertex, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btConvexHullComputer4Edge15getTargetVertexEv(%"class.btConvexHullComputer::Edge"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %targetVertex = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 2
  %0 = load i32, i32* %targetVertex, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexHullComputer* @_ZN20btConvexHullComputerD2Ev(%class.btConvexHullComputer* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexHullComputer*, align 4
  store %class.btConvexHullComputer* %this, %class.btConvexHullComputer** %this.addr, align 4
  %this1 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %this.addr, align 4
  %faces = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 2
  %call = call %class.btAlignedObjectArray.81* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.81* %faces) #5
  %edges = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEED2Ev(%class.btAlignedObjectArray.85* %edges) #5
  %vertices = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 0
  %call3 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %vertices) #5
  ret %class.btConvexHullComputer* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.25* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.25*, align 4
  store %class.btAlignedObjectArray.25* %this, %class.btAlignedObjectArray.25** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.25*, %class.btAlignedObjectArray.25** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.25* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.25*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.25* %this, %class.btAlignedObjectArray.25** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.25*, %class.btAlignedObjectArray.25** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %0, i32 %1
  ret %"struct.btSoftBody::Node"* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4LinkEE4sizeEv(%class.btAlignedObjectArray.29* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.29*, align 4
  store %class.btAlignedObjectArray.29* %this, %class.btAlignedObjectArray.29** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.29*, %class.btAlignedObjectArray.29** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.29, %class.btAlignedObjectArray.29* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(52) %"struct.btSoftBody::Link"* @_ZN20btAlignedObjectArrayIN10btSoftBody4LinkEEixEi(%class.btAlignedObjectArray.29* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.29*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.29* %this, %class.btAlignedObjectArray.29** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.29*, %class.btAlignedObjectArray.29** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.29, %class.btAlignedObjectArray.29* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Link"*, %"struct.btSoftBody::Link"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Link", %"struct.btSoftBody::Link"* %0, i32 %1
  ret %"struct.btSoftBody::Link"* %arrayidx
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #5

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody8RContactEE4sizeEv(%class.btAlignedObjectArray.49* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.49*, align 4
  store %class.btAlignedObjectArray.49* %this, %class.btAlignedObjectArray.49** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.49*, %class.btAlignedObjectArray.49** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.49, %class.btAlignedObjectArray.49* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(104) %"struct.btSoftBody::RContact"* @_ZN20btAlignedObjectArrayIN10btSoftBody8RContactEEixEi(%class.btAlignedObjectArray.49* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.49*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.49* %this, %class.btAlignedObjectArray.49** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.49*, %class.btAlignedObjectArray.49** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.49, %class.btAlignedObjectArray.49* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::RContact"*, %"struct.btSoftBody::RContact"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::RContact", %"struct.btSoftBody::RContact"* %0, i32 %1
  ret %"struct.btSoftBody::RContact"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #1 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z7btCrossRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVector37minAxisEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 0
  %2 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 0, i32 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 1
  %5 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 1, i32 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4FaceEE4sizeEv(%class.btAlignedObjectArray.33* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.33*, align 4
  store %class.btAlignedObjectArray.33* %this, %class.btAlignedObjectArray.33** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.33*, %class.btAlignedObjectArray.33** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.33, %class.btAlignedObjectArray.33* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(44) %"struct.btSoftBody::Face"* @_ZN20btAlignedObjectArrayIN10btSoftBody4FaceEEixEi(%class.btAlignedObjectArray.33* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.33*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.33* %this, %class.btAlignedObjectArray.33** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.33*, %class.btAlignedObjectArray.33** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.33, %class.btAlignedObjectArray.33* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Face"*, %"struct.btSoftBody::Face"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Face", %"struct.btSoftBody::Face"* %0, i32 %1
  ret %"struct.btSoftBody::Face"* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  %2 = load float, float* %1, align 4
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody5TetraEE4sizeEv(%class.btAlignedObjectArray.37* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.37*, align 4
  store %class.btAlignedObjectArray.37* %this, %class.btAlignedObjectArray.37** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.37*, %class.btAlignedObjectArray.37** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.37, %class.btAlignedObjectArray.37* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(104) %"struct.btSoftBody::Tetra"* @_ZN20btAlignedObjectArrayIN10btSoftBody5TetraEEixEi(%class.btAlignedObjectArray.37* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.37*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.37* %this, %class.btAlignedObjectArray.37** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.37*, %class.btAlignedObjectArray.37** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.37, %class.btAlignedObjectArray.37* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Tetra"*, %"struct.btSoftBody::Tetra"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Tetra", %"struct.btSoftBody::Tetra"* %0, i32 %1
  ret %"struct.btSoftBody::Tetra"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody6AnchorEE4sizeEv(%class.btAlignedObjectArray.41* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(96) %"struct.btSoftBody::Anchor"* @_ZN20btAlignedObjectArrayIN10btSoftBody6AnchorEEixEi(%class.btAlignedObjectArray.41* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Anchor"*, %"struct.btSoftBody::Anchor"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Anchor", %"struct.btSoftBody::Anchor"* %0, i32 %1
  ret %"struct.btSoftBody::Anchor"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZL10drawVertexP12btIDebugDrawRK9btVector3fS3_(%class.btIDebugDraw* %idraw, %class.btVector3* nonnull align 4 dereferenceable(16) %x, float %s, %class.btVector3* nonnull align 4 dereferenceable(16) %c) #2 {
entry:
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float, align 4
  %c.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  store float %s, float* %s.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp1, float* nonnull align 4 dereferenceable(4) %s.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  %2 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %s.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %3 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %4 = bitcast %class.btIDebugDraw* %0 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable, i64 4
  %5 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %5(%class.btIDebugDraw* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %6 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %7 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  store float 0.000000e+00, float* %ref.tmp11, align 4
  store float 0.000000e+00, float* %ref.tmp12, align 4
  %call13 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %s.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  %8 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  store float 0.000000e+00, float* %ref.tmp16, align 4
  store float 0.000000e+00, float* %ref.tmp17, align 4
  %call18 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %s.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15)
  %9 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %10 = bitcast %class.btIDebugDraw* %6 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable19 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %10, align 4
  %vfn20 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable19, i64 4
  %11 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn20, align 4
  call void %11(%class.btIDebugDraw* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %12 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %13 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  store float 0.000000e+00, float* %ref.tmp23, align 4
  store float 0.000000e+00, float* %ref.tmp24, align 4
  %call25 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %s.addr)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp22)
  %14 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  store float 0.000000e+00, float* %ref.tmp28, align 4
  store float 0.000000e+00, float* %ref.tmp29, align 4
  %call30 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %s.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27)
  %15 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %16 = bitcast %class.btIDebugDraw* %12 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable31 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %16, align 4
  %vfn32 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable31, i64 4
  %17 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn32, align 4
  call void %17(%class.btIDebugDraw* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %15)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NoteEE4sizeEv(%class.btAlignedObjectArray.20* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(60) %"struct.btSoftBody::Note"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NoteEEixEi(%class.btAlignedObjectArray.20* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Note"*, %"struct.btSoftBody::Note"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Note", %"struct.btSoftBody::Note"* %0, i32 %1
  ret %"struct.btSoftBody::Note"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %psb, %class.btIDebugDraw* %idraw, i32 %mindepth, i32 %maxdepth) #2 {
entry:
  %psb.addr = alloca %class.btSoftBody*, align 4
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %mindepth.addr = alloca i32, align 4
  %maxdepth.addr = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btSoftBody* %psb, %class.btSoftBody** %psb.addr, align 4
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4
  store i32 %mindepth, i32* %mindepth.addr, align 4
  store i32 %maxdepth, i32* %maxdepth.addr, align 4
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %1 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_ndbvt = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %1, i32 0, i32 21
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %m_ndbvt, i32 0, i32 0
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  store float 1.000000e+00, float* %ref.tmp1, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 1.000000e+00, float* %ref.tmp6, align 4
  store float 1.000000e+00, float* %ref.tmp7, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %3 = load i32, i32* %mindepth.addr, align 4
  %4 = load i32, i32* %maxdepth.addr, align 4
  call void @_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii(%class.btIDebugDraw* %0, %struct.btDbvtNode* %2, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4, i32 %3, i32 %4)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %psb, %class.btIDebugDraw* %idraw, i32 %mindepth, i32 %maxdepth) #2 {
entry:
  %psb.addr = alloca %class.btSoftBody*, align 4
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %mindepth.addr = alloca i32, align 4
  %maxdepth.addr = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btSoftBody* %psb, %class.btSoftBody** %psb.addr, align 4
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4
  store i32 %mindepth, i32* %mindepth.addr, align 4
  store i32 %maxdepth, i32* %maxdepth.addr, align 4
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %1 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_fdbvt = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %1, i32 0, i32 22
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %m_fdbvt, i32 0, i32 0
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %3 = load i32, i32* %mindepth.addr, align 4
  %4 = load i32, i32* %maxdepth.addr, align 4
  call void @_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii(%class.btIDebugDraw* %0, %struct.btDbvtNode* %2, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4, i32 %3, i32 %4)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %psb, %class.btIDebugDraw* %idraw, i32 %mindepth, i32 %maxdepth) #2 {
entry:
  %psb.addr = alloca %class.btSoftBody*, align 4
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %mindepth.addr = alloca i32, align 4
  %maxdepth.addr = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btSoftBody* %psb, %class.btSoftBody** %psb.addr, align 4
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4
  store i32 %mindepth, i32* %mindepth.addr, align 4
  store i32 %maxdepth, i32* %maxdepth.addr, align 4
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %1 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_cdbvt = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %1, i32 0, i32 23
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %m_cdbvt, i32 0, i32 0
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %3 = load i32, i32* %mindepth.addr, align 4
  %4 = load i32, i32* %maxdepth.addr, align 4
  call void @_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii(%class.btIDebugDraw* %0, %struct.btDbvtNode* %2, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4, i32 %3, i32 %4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN10btSoftBody5JointEE4sizeEv(%class.btAlignedObjectArray.57* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.57*, align 4
  store %class.btAlignedObjectArray.57* %this, %class.btAlignedObjectArray.57** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.57*, %class.btAlignedObjectArray.57** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.57, %class.btAlignedObjectArray.57* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.btSoftBody::Joint"** @_ZN20btAlignedObjectArrayIPN10btSoftBody5JointEEixEi(%class.btAlignedObjectArray.57* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.57*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.57* %this, %class.btAlignedObjectArray.57** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.57*, %class.btAlignedObjectArray.57** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.57, %class.btAlignedObjectArray.57* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Joint"**, %"struct.btSoftBody::Joint"*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Joint"*, %"struct.btSoftBody::Joint"** %0, i32 %1
  ret %"struct.btSoftBody::Joint"** %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK10btSoftBody4Body5xformEv(%"struct.btSoftBody::Body"* %this) #2 comdat {
entry:
  %retval = alloca %class.btTransform*, align 4
  %this.addr = alloca %"struct.btSoftBody::Body"*, align 4
  store %"struct.btSoftBody::Body"* %this, %"struct.btSoftBody::Body"** %this.addr, align 4
  %this1 = load %"struct.btSoftBody::Body"*, %"struct.btSoftBody::Body"** %this.addr, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZNK10btSoftBody4Body5xformEvE8identity to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZNK10btSoftBody4Body5xformEvE8identity) #5
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransform11getIdentityEv()
  %call2 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* @_ZZNK10btSoftBody4Body5xformEvE8identity, %class.btTransform* nonnull align 4 dereferenceable(64) %call)
  call void @__cxa_guard_release(i32* @_ZGVZNK10btSoftBody4Body5xformEvE8identity) #5
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  %m_collisionObject = getelementptr inbounds %"struct.btSoftBody::Body", %"struct.btSoftBody::Body"* %this1, i32 0, i32 2
  %3 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4
  %tobool3 = icmp ne %class.btCollisionObject* %3, null
  br i1 %tobool3, label %if.then, label %if.end

if.then:                                          ; preds = %init.end
  %m_collisionObject4 = getelementptr inbounds %"struct.btSoftBody::Body", %"struct.btSoftBody::Body"* %this1, i32 0, i32 2
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject4, align 4
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %4)
  store %class.btTransform* %call5, %class.btTransform** %retval, align 4
  br label %return

if.end:                                           ; preds = %init.end
  %m_soft = getelementptr inbounds %"struct.btSoftBody::Body", %"struct.btSoftBody::Body"* %this1, i32 0, i32 0
  %5 = load %"struct.btSoftBody::Cluster"*, %"struct.btSoftBody::Cluster"** %m_soft, align 4
  %tobool6 = icmp ne %"struct.btSoftBody::Cluster"* %5, null
  br i1 %tobool6, label %if.then7, label %if.end9

if.then7:                                         ; preds = %if.end
  %m_soft8 = getelementptr inbounds %"struct.btSoftBody::Body", %"struct.btSoftBody::Body"* %this1, i32 0, i32 0
  %6 = load %"struct.btSoftBody::Cluster"*, %"struct.btSoftBody::Cluster"** %m_soft8, align 4
  %m_framexform = getelementptr inbounds %"struct.btSoftBody::Cluster", %"struct.btSoftBody::Cluster"* %6, i32 0, i32 3
  store %class.btTransform* %m_framexform, %class.btTransform** %retval, align 4
  br label %return

if.end9:                                          ; preds = %if.end
  store %class.btTransform* @_ZZNK10btSoftBody4Body5xformEvE8identity, %class.btTransform** %retval, align 4
  br label %return

return:                                           ; preds = %if.end9, %if.then7, %if.then
  %7 = load %class.btTransform*, %class.btTransform** %retval, align 4
  ret %class.btTransform* %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btSoftBodyHelpers9DrawInfosEP10btSoftBodyP12btIDebugDrawbbb(%class.btSoftBody* %psb, %class.btIDebugDraw* %idraw, i1 zeroext %masses, i1 zeroext %areas, i1 zeroext %0) #2 {
entry:
  %psb.addr = alloca %class.btSoftBody*, align 4
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %masses.addr = alloca i8, align 1
  %areas.addr = alloca i8, align 1
  %.addr = alloca i8, align 1
  %i = alloca i32, align 4
  %n = alloca %"struct.btSoftBody::Node"*, align 4
  %text = alloca [2048 x i8], align 16
  %buff = alloca [1024 x i8], align 16
  store %class.btSoftBody* %psb, %class.btSoftBody** %psb.addr, align 4
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4
  %frombool = zext i1 %masses to i8
  store i8 %frombool, i8* %masses.addr, align 1
  %frombool1 = zext i1 %areas to i8
  store i8 %frombool1, i8* %areas.addr, align 1
  %frombool2 = zext i1 %0 to i8
  store i8 %frombool2, i8* %.addr, align 1
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_nodes = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %2, i32 0, i32 9
  %call = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.25* %m_nodes)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_nodes3 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %3, i32 0, i32 9
  %4 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.25* %m_nodes3, i32 %4)
  store %"struct.btSoftBody::Node"* %call4, %"struct.btSoftBody::Node"** %n, align 4
  %5 = bitcast [2048 x i8]* %text to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %5, i8 0, i32 2048, i1 false)
  %6 = load i8, i8* %masses.addr, align 1
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %arraydecay = getelementptr inbounds [1024 x i8], [1024 x i8]* %buff, i32 0, i32 0
  %7 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4
  %m_im = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %7, i32 0, i32 6
  %8 = load float, float* %m_im, align 4
  %div = fdiv float 1.000000e+00, %8
  %conv = fpext float %div to double
  %call5 = call i32 (i8*, i8*, ...) @sprintf(i8* %arraydecay, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str, i32 0, i32 0), double %conv)
  %arraydecay6 = getelementptr inbounds [2048 x i8], [2048 x i8]* %text, i32 0, i32 0
  %arraydecay7 = getelementptr inbounds [1024 x i8], [1024 x i8]* %buff, i32 0, i32 0
  %call8 = call i8* @strcat(i8* %arraydecay6, i8* %arraydecay7)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %9 = load i8, i8* %areas.addr, align 1
  %tobool9 = trunc i8 %9 to i1
  br i1 %tobool9, label %if.then10, label %if.end17

if.then10:                                        ; preds = %if.end
  %arraydecay11 = getelementptr inbounds [1024 x i8], [1024 x i8]* %buff, i32 0, i32 0
  %10 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4
  %m_area = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %10, i32 0, i32 7
  %11 = load float, float* %m_area, align 4
  %conv12 = fpext float %11 to double
  %call13 = call i32 (i8*, i8*, ...) @sprintf(i8* %arraydecay11, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.1, i32 0, i32 0), double %conv12)
  %arraydecay14 = getelementptr inbounds [2048 x i8], [2048 x i8]* %text, i32 0, i32 0
  %arraydecay15 = getelementptr inbounds [1024 x i8], [1024 x i8]* %buff, i32 0, i32 0
  %call16 = call i8* @strcat(i8* %arraydecay14, i8* %arraydecay15)
  br label %if.end17

if.end17:                                         ; preds = %if.then10, %if.end
  %arrayidx = getelementptr inbounds [2048 x i8], [2048 x i8]* %text, i32 0, i32 0
  %12 = load i8, i8* %arrayidx, align 16
  %tobool18 = icmp ne i8 %12, 0
  br i1 %tobool18, label %if.then19, label %if.end21

if.then19:                                        ; preds = %if.end17
  %13 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %14 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %n, align 4
  %m_x = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %14, i32 0, i32 1
  %arraydecay20 = getelementptr inbounds [2048 x i8], [2048 x i8]* %text, i32 0, i32 0
  %15 = bitcast %class.btIDebugDraw* %13 to void (%class.btIDebugDraw*, %class.btVector3*, i8*)***
  %vtable = load void (%class.btIDebugDraw*, %class.btVector3*, i8*)**, void (%class.btIDebugDraw*, %class.btVector3*, i8*)*** %15, align 4
  %vfn = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, i8*)*, void (%class.btIDebugDraw*, %class.btVector3*, i8*)** %vtable, i64 12
  %16 = load void (%class.btIDebugDraw*, %class.btVector3*, i8*)*, void (%class.btIDebugDraw*, %class.btVector3*, i8*)** %vfn, align 4
  call void %16(%class.btIDebugDraw* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %m_x, i8* %arraydecay20)
  br label %if.end21

if.end21:                                         ; preds = %if.then19, %if.end17
  br label %for.inc

for.inc:                                          ; preds = %if.end21
  %17 = load i32, i32* %i, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #6

declare i32 @sprintf(i8*, i8*, ...) #3

declare i8* @strcat(i8*, i8*) #3

; Function Attrs: noinline optnone
define internal void @_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii(%class.btIDebugDraw* %idraw, %struct.btDbvtNode* %node, i32 %depth, %class.btVector3* nonnull align 4 dereferenceable(16) %ncolor, %class.btVector3* nonnull align 4 dereferenceable(16) %lcolor, i32 %mindepth, i32 %maxdepth) #2 {
entry:
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %node.addr = alloca %struct.btDbvtNode*, align 4
  %depth.addr = alloca i32, align 4
  %ncolor.addr = alloca %class.btVector3*, align 4
  %lcolor.addr = alloca %class.btVector3*, align 4
  %mindepth.addr = alloca i32, align 4
  %maxdepth.addr = alloca i32, align 4
  %scl = alloca float, align 4
  %mi = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %mx = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4
  store %struct.btDbvtNode* %node, %struct.btDbvtNode** %node.addr, align 4
  store i32 %depth, i32* %depth.addr, align 4
  store %class.btVector3* %ncolor, %class.btVector3** %ncolor.addr, align 4
  store %class.btVector3* %lcolor, %class.btVector3** %lcolor.addr, align 4
  store i32 %mindepth, i32* %mindepth.addr, align 4
  store i32 %maxdepth, i32* %maxdepth.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %tobool = icmp ne %struct.btDbvtNode* %0, null
  br i1 %tobool, label %if.then, label %if.end19

if.then:                                          ; preds = %entry
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %1)
  br i1 %call, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %2 = load i32, i32* %depth.addr, align 4
  %3 = load i32, i32* %maxdepth.addr, align 4
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %if.then2, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true
  %4 = load i32, i32* %maxdepth.addr, align 4
  %cmp1 = icmp slt i32 %4, 0
  br i1 %cmp1, label %if.then2, label %if.end

if.then2:                                         ; preds = %lor.lhs.false, %land.lhs.true
  %5 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %7 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %6, i32 0, i32 2
  %childs = bitcast %union.anon.23* %7 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %9 = load i32, i32* %depth.addr, align 4
  %add = add nsw i32 %9, 1
  %10 = load %class.btVector3*, %class.btVector3** %ncolor.addr, align 4
  %11 = load %class.btVector3*, %class.btVector3** %lcolor.addr, align 4
  %12 = load i32, i32* %mindepth.addr, align 4
  %13 = load i32, i32* %maxdepth.addr, align 4
  call void @_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii(%class.btIDebugDraw* %5, %struct.btDbvtNode* %8, i32 %add, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11, i32 %12, i32 %13)
  %14 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %15 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %16 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %15, i32 0, i32 2
  %childs3 = bitcast %union.anon.23* %16 to [2 x %struct.btDbvtNode*]*
  %arrayidx4 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs3, i32 0, i32 1
  %17 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx4, align 4
  %18 = load i32, i32* %depth.addr, align 4
  %add5 = add nsw i32 %18, 1
  %19 = load %class.btVector3*, %class.btVector3** %ncolor.addr, align 4
  %20 = load %class.btVector3*, %class.btVector3** %lcolor.addr, align 4
  %21 = load i32, i32* %mindepth.addr, align 4
  %22 = load i32, i32* %maxdepth.addr, align 4
  call void @_ZL8drawTreeP12btIDebugDrawPK10btDbvtNodeiRK9btVector3S6_ii(%class.btIDebugDraw* %14, %struct.btDbvtNode* %17, i32 %add5, %class.btVector3* nonnull align 4 dereferenceable(16) %19, %class.btVector3* nonnull align 4 dereferenceable(16) %20, i32 %21, i32 %22)
  br label %if.end

if.end:                                           ; preds = %if.then2, %lor.lhs.false, %if.then
  %23 = load i32, i32* %depth.addr, align 4
  %24 = load i32, i32* %mindepth.addr, align 4
  %cmp6 = icmp sge i32 %23, %24
  br i1 %cmp6, label %if.then7, label %if.end18

if.then7:                                         ; preds = %if.end
  %25 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %call8 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %25)
  %26 = zext i1 %call8 to i64
  %cond = select i1 %call8, i32 1, i32 1
  %conv = sitofp i32 %cond to float
  store float %conv, float* %scl, align 4
  %27 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %27, i32 0, i32 0
  call void @_ZNK12btDbvtAabbMm6CenterEv(%class.btVector3* sret align 4 %ref.tmp, %struct.btDbvtAabbMm* %volume)
  %28 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %volume11 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %28, i32 0, i32 0
  call void @_ZNK12btDbvtAabbMm7ExtentsEv(%class.btVector3* sret align 4 %ref.tmp10, %struct.btDbvtAabbMm* %volume11)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %scl)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %29 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %volume13 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %29, i32 0, i32 0
  call void @_ZNK12btDbvtAabbMm6CenterEv(%class.btVector3* sret align 4 %ref.tmp12, %struct.btDbvtAabbMm* %volume13)
  %30 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %volume16 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %30, i32 0, i32 0
  call void @_ZNK12btDbvtAabbMm7ExtentsEv(%class.btVector3* sret align 4 %ref.tmp15, %struct.btDbvtAabbMm* %volume16)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %scl)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %mx, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14)
  %31 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %32 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %call17 = call zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %32)
  br i1 %call17, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then7
  %33 = load %class.btVector3*, %class.btVector3** %lcolor.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.then7
  %34 = load %class.btVector3*, %class.btVector3** %ncolor.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi %class.btVector3* [ %33, %cond.true ], [ %34, %cond.false ]
  call void @_ZL7drawBoxP12btIDebugDrawRK9btVector3S3_S3_(%class.btIDebugDraw* %31, %class.btVector3* nonnull align 4 dereferenceable(16) %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %mx, %class.btVector3* nonnull align 4 dereferenceable(16) %cond-lvalue)
  br label %if.end18

if.end18:                                         ; preds = %cond.end, %if.end
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btSoftBodyHelpers19ReoptimizeLinkOrderEP10btSoftBody(%class.btSoftBody* %psb) #2 {
entry:
  %psb.addr = alloca %class.btSoftBody*, align 4
  %i = alloca i32, align 4
  %nLinks = alloca i32, align 4
  %nNodes = alloca i32, align 4
  %lr = alloca %"struct.btSoftBody::Link"*, align 4
  %ar = alloca i32, align 4
  %br = alloca i32, align 4
  %node0 = alloca %"struct.btSoftBody::Node"*, align 4
  %node1 = alloca %"struct.btSoftBody::Node"*, align 4
  %linkDep = alloca %class.LinkDeps_t*, align 4
  %readyListHead = alloca i32, align 4
  %readyListTail = alloca i32, align 4
  %linkNum = alloca i32, align 4
  %linkDepFrees = alloca i32, align 4
  %depLink = alloca i32, align 4
  %nodeWrittenAt = alloca i32*, align 4
  %linkDepA = alloca i32*, align 4
  %linkDepB = alloca i32*, align 4
  %readyList = alloca i32*, align 4
  %linkDepFreeList = alloca %class.LinkDeps_t*, align 4
  %linkDepListStarts = alloca %class.LinkDeps_t**, align 4
  %linkBuffer = alloca %"struct.btSoftBody::Link"*, align 4
  store %class.btSoftBody* %psb, %class.btSoftBody** %psb.addr, align 4
  %0 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_links = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %0, i32 0, i32 10
  %call = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4LinkEE4sizeEv(%class.btAlignedObjectArray.29* %m_links)
  store i32 %call, i32* %nLinks, align 4
  %1 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_nodes = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %1, i32 0, i32 9
  %call1 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.25* %m_nodes)
  store i32 %call1, i32* %nNodes, align 4
  %2 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_nodes2 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %2, i32 0, i32 9
  %call3 = call nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.25* %m_nodes2, i32 0)
  store %"struct.btSoftBody::Node"* %call3, %"struct.btSoftBody::Node"** %node0, align 4
  %3 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_nodes4 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %3, i32 0, i32 9
  %call5 = call nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZN20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.25* %m_nodes4, i32 1)
  store %"struct.btSoftBody::Node"* %call5, %"struct.btSoftBody::Node"** %node1, align 4
  %4 = load i32, i32* %nNodes, align 4
  %add = add nsw i32 %4, 1
  %5 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %add, i32 4)
  %6 = extractvalue { i32, i1 } %5, 1
  %7 = extractvalue { i32, i1 } %5, 0
  %8 = select i1 %6, i32 -1, i32 %7
  %call6 = call noalias nonnull i8* @_Znam(i32 %8) #10
  %9 = bitcast i8* %call6 to i32*
  store i32* %9, i32** %nodeWrittenAt, align 4
  %10 = load i32, i32* %nLinks, align 4
  %11 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %10, i32 4)
  %12 = extractvalue { i32, i1 } %11, 1
  %13 = extractvalue { i32, i1 } %11, 0
  %14 = select i1 %12, i32 -1, i32 %13
  %call7 = call noalias nonnull i8* @_Znam(i32 %14) #10
  %15 = bitcast i8* %call7 to i32*
  store i32* %15, i32** %linkDepA, align 4
  %16 = load i32, i32* %nLinks, align 4
  %17 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %16, i32 4)
  %18 = extractvalue { i32, i1 } %17, 1
  %19 = extractvalue { i32, i1 } %17, 0
  %20 = select i1 %18, i32 -1, i32 %19
  %call8 = call noalias nonnull i8* @_Znam(i32 %20) #10
  %21 = bitcast i8* %call8 to i32*
  store i32* %21, i32** %linkDepB, align 4
  %22 = load i32, i32* %nLinks, align 4
  %23 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %22, i32 4)
  %24 = extractvalue { i32, i1 } %23, 1
  %25 = extractvalue { i32, i1 } %23, 0
  %26 = select i1 %24, i32 -1, i32 %25
  %call9 = call noalias nonnull i8* @_Znam(i32 %26) #10
  %27 = bitcast i8* %call9 to i32*
  store i32* %27, i32** %readyList, align 4
  %28 = load i32, i32* %nLinks, align 4
  %mul = mul nsw i32 2, %28
  %29 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %mul, i32 8)
  %30 = extractvalue { i32, i1 } %29, 1
  %31 = extractvalue { i32, i1 } %29, 0
  %32 = select i1 %30, i32 -1, i32 %31
  %call10 = call noalias nonnull i8* @_Znam(i32 %32) #10
  %33 = bitcast i8* %call10 to %class.LinkDeps_t*
  store %class.LinkDeps_t* %33, %class.LinkDeps_t** %linkDepFreeList, align 4
  %34 = load i32, i32* %nLinks, align 4
  %35 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %34, i32 4)
  %36 = extractvalue { i32, i1 } %35, 1
  %37 = extractvalue { i32, i1 } %35, 0
  %38 = select i1 %36, i32 -1, i32 %37
  %call11 = call noalias nonnull i8* @_Znam(i32 %38) #10
  %39 = bitcast i8* %call11 to %class.LinkDeps_t**
  store %class.LinkDeps_t** %39, %class.LinkDeps_t*** %linkDepListStarts, align 4
  %40 = load i32, i32* %nLinks, align 4
  %41 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %40, i32 52)
  %42 = extractvalue { i32, i1 } %41, 1
  %43 = extractvalue { i32, i1 } %41, 0
  %44 = select i1 %42, i32 -1, i32 %43
  %call12 = call i8* @_ZN10btSoftBody4LinknaEm(i32 %44)
  %45 = bitcast i8* %call12 to %"struct.btSoftBody::Link"*
  %isempty = icmp eq i32 %40, 0
  br i1 %isempty, label %arrayctor.cont, label %new.ctorloop

new.ctorloop:                                     ; preds = %entry
  %arrayctor.end = getelementptr inbounds %"struct.btSoftBody::Link", %"struct.btSoftBody::Link"* %45, i32 %40
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %new.ctorloop
  %arrayctor.cur = phi %"struct.btSoftBody::Link"* [ %45, %new.ctorloop ], [ %arrayctor.next, %arrayctor.loop ]
  %call13 = call %"struct.btSoftBody::Link"* @_ZN10btSoftBody4LinkC2Ev(%"struct.btSoftBody::Link"* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %"struct.btSoftBody::Link", %"struct.btSoftBody::Link"* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %"struct.btSoftBody::Link"* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %entry, %arrayctor.loop
  store %"struct.btSoftBody::Link"* %45, %"struct.btSoftBody::Link"** %linkBuffer, align 4
  %46 = load %"struct.btSoftBody::Link"*, %"struct.btSoftBody::Link"** %linkBuffer, align 4
  %47 = bitcast %"struct.btSoftBody::Link"* %46 to i8*
  %48 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_links14 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %48, i32 0, i32 10
  %call15 = call nonnull align 4 dereferenceable(52) %"struct.btSoftBody::Link"* @_ZN20btAlignedObjectArrayIN10btSoftBody4LinkEEixEi(%class.btAlignedObjectArray.29* %m_links14, i32 0)
  %49 = bitcast %"struct.btSoftBody::Link"* %call15 to i8*
  %50 = load i32, i32* %nLinks, align 4
  %mul16 = mul i32 52, %50
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %47, i8* align 4 %49, i32 %mul16, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont
  %51 = load i32, i32* %i, align 4
  %52 = load i32, i32* %nNodes, align 4
  %add17 = add nsw i32 %52, 1
  %cmp = icmp slt i32 %51, %add17
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %53 = load i32*, i32** %nodeWrittenAt, align 4
  %54 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %53, i32 %54
  store i32 -1, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %55 = load i32, i32* %i, align 4
  %inc = add nsw i32 %55, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc22, %for.end
  %56 = load i32, i32* %i, align 4
  %57 = load i32, i32* %nLinks, align 4
  %cmp19 = icmp slt i32 %56, %57
  br i1 %cmp19, label %for.body20, label %for.end24

for.body20:                                       ; preds = %for.cond18
  %58 = load %class.LinkDeps_t**, %class.LinkDeps_t*** %linkDepListStarts, align 4
  %59 = load i32, i32* %i, align 4
  %arrayidx21 = getelementptr inbounds %class.LinkDeps_t*, %class.LinkDeps_t** %58, i32 %59
  store %class.LinkDeps_t* null, %class.LinkDeps_t** %arrayidx21, align 4
  br label %for.inc22

for.inc22:                                        ; preds = %for.body20
  %60 = load i32, i32* %i, align 4
  %inc23 = add nsw i32 %60, 1
  store i32 %inc23, i32* %i, align 4
  br label %for.cond18

for.end24:                                        ; preds = %for.cond18
  store i32 0, i32* %linkDepFrees, align 4
  store i32 0, i32* %readyListTail, align 4
  store i32 0, i32* %readyListHead, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond25

for.cond25:                                       ; preds = %for.inc86, %for.end24
  %61 = load i32, i32* %i, align 4
  %62 = load i32, i32* %nLinks, align 4
  %cmp26 = icmp slt i32 %61, %62
  br i1 %cmp26, label %for.body27, label %for.end88

for.body27:                                       ; preds = %for.cond25
  %63 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_links28 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %63, i32 0, i32 10
  %64 = load i32, i32* %i, align 4
  %call29 = call nonnull align 4 dereferenceable(52) %"struct.btSoftBody::Link"* @_ZN20btAlignedObjectArrayIN10btSoftBody4LinkEEixEi(%class.btAlignedObjectArray.29* %m_links28, i32 %64)
  store %"struct.btSoftBody::Link"* %call29, %"struct.btSoftBody::Link"** %lr, align 4
  %65 = load %"struct.btSoftBody::Link"*, %"struct.btSoftBody::Link"** %lr, align 4
  %m_n = getelementptr inbounds %"struct.btSoftBody::Link", %"struct.btSoftBody::Link"* %65, i32 0, i32 2
  %arrayidx30 = getelementptr inbounds [2 x %"struct.btSoftBody::Node"*], [2 x %"struct.btSoftBody::Node"*]* %m_n, i32 0, i32 0
  %66 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx30, align 4
  %67 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %node0, align 4
  %sub.ptr.lhs.cast = ptrtoint %"struct.btSoftBody::Node"* %66 to i32
  %sub.ptr.rhs.cast = ptrtoint %"struct.btSoftBody::Node"* %67 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 104
  %68 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %node1, align 4
  %69 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %node0, align 4
  %sub.ptr.lhs.cast31 = ptrtoint %"struct.btSoftBody::Node"* %68 to i32
  %sub.ptr.rhs.cast32 = ptrtoint %"struct.btSoftBody::Node"* %69 to i32
  %sub.ptr.sub33 = sub i32 %sub.ptr.lhs.cast31, %sub.ptr.rhs.cast32
  %sub.ptr.div34 = sdiv exact i32 %sub.ptr.sub33, 104
  %div = sdiv i32 %sub.ptr.div, %sub.ptr.div34
  store i32 %div, i32* %ar, align 4
  %70 = load %"struct.btSoftBody::Link"*, %"struct.btSoftBody::Link"** %lr, align 4
  %m_n35 = getelementptr inbounds %"struct.btSoftBody::Link", %"struct.btSoftBody::Link"* %70, i32 0, i32 2
  %arrayidx36 = getelementptr inbounds [2 x %"struct.btSoftBody::Node"*], [2 x %"struct.btSoftBody::Node"*]* %m_n35, i32 0, i32 1
  %71 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %arrayidx36, align 4
  %72 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %node0, align 4
  %sub.ptr.lhs.cast37 = ptrtoint %"struct.btSoftBody::Node"* %71 to i32
  %sub.ptr.rhs.cast38 = ptrtoint %"struct.btSoftBody::Node"* %72 to i32
  %sub.ptr.sub39 = sub i32 %sub.ptr.lhs.cast37, %sub.ptr.rhs.cast38
  %sub.ptr.div40 = sdiv exact i32 %sub.ptr.sub39, 104
  %73 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %node1, align 4
  %74 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %node0, align 4
  %sub.ptr.lhs.cast41 = ptrtoint %"struct.btSoftBody::Node"* %73 to i32
  %sub.ptr.rhs.cast42 = ptrtoint %"struct.btSoftBody::Node"* %74 to i32
  %sub.ptr.sub43 = sub i32 %sub.ptr.lhs.cast41, %sub.ptr.rhs.cast42
  %sub.ptr.div44 = sdiv exact i32 %sub.ptr.sub43, 104
  %div45 = sdiv i32 %sub.ptr.div40, %sub.ptr.div44
  store i32 %div45, i32* %br, align 4
  %75 = load i32*, i32** %nodeWrittenAt, align 4
  %76 = load i32, i32* %ar, align 4
  %arrayidx46 = getelementptr inbounds i32, i32* %75, i32 %76
  %77 = load i32, i32* %arrayidx46, align 4
  %cmp47 = icmp sgt i32 %77, -1
  br i1 %cmp47, label %if.then, label %if.else

if.then:                                          ; preds = %for.body27
  %78 = load i32*, i32** %nodeWrittenAt, align 4
  %79 = load i32, i32* %ar, align 4
  %arrayidx48 = getelementptr inbounds i32, i32* %78, i32 %79
  %80 = load i32, i32* %arrayidx48, align 4
  %81 = load i32*, i32** %linkDepA, align 4
  %82 = load i32, i32* %i, align 4
  %arrayidx49 = getelementptr inbounds i32, i32* %81, i32 %82
  store i32 %80, i32* %arrayidx49, align 4
  %83 = load %class.LinkDeps_t*, %class.LinkDeps_t** %linkDepFreeList, align 4
  %84 = load i32, i32* %linkDepFrees, align 4
  %inc50 = add nsw i32 %84, 1
  store i32 %inc50, i32* %linkDepFrees, align 4
  %arrayidx51 = getelementptr inbounds %class.LinkDeps_t, %class.LinkDeps_t* %83, i32 %84
  store %class.LinkDeps_t* %arrayidx51, %class.LinkDeps_t** %linkDep, align 4
  %85 = load i32, i32* %i, align 4
  %86 = load %class.LinkDeps_t*, %class.LinkDeps_t** %linkDep, align 4
  %value = getelementptr inbounds %class.LinkDeps_t, %class.LinkDeps_t* %86, i32 0, i32 0
  store i32 %85, i32* %value, align 4
  %87 = load %class.LinkDeps_t**, %class.LinkDeps_t*** %linkDepListStarts, align 4
  %88 = load i32*, i32** %nodeWrittenAt, align 4
  %89 = load i32, i32* %ar, align 4
  %arrayidx52 = getelementptr inbounds i32, i32* %88, i32 %89
  %90 = load i32, i32* %arrayidx52, align 4
  %arrayidx53 = getelementptr inbounds %class.LinkDeps_t*, %class.LinkDeps_t** %87, i32 %90
  %91 = load %class.LinkDeps_t*, %class.LinkDeps_t** %arrayidx53, align 4
  %92 = load %class.LinkDeps_t*, %class.LinkDeps_t** %linkDep, align 4
  %next = getelementptr inbounds %class.LinkDeps_t, %class.LinkDeps_t* %92, i32 0, i32 1
  store %class.LinkDeps_t* %91, %class.LinkDeps_t** %next, align 4
  %93 = load %class.LinkDeps_t*, %class.LinkDeps_t** %linkDep, align 4
  %94 = load %class.LinkDeps_t**, %class.LinkDeps_t*** %linkDepListStarts, align 4
  %95 = load i32*, i32** %nodeWrittenAt, align 4
  %96 = load i32, i32* %ar, align 4
  %arrayidx54 = getelementptr inbounds i32, i32* %95, i32 %96
  %97 = load i32, i32* %arrayidx54, align 4
  %arrayidx55 = getelementptr inbounds %class.LinkDeps_t*, %class.LinkDeps_t** %94, i32 %97
  store %class.LinkDeps_t* %93, %class.LinkDeps_t** %arrayidx55, align 4
  br label %if.end

if.else:                                          ; preds = %for.body27
  %98 = load i32*, i32** %linkDepA, align 4
  %99 = load i32, i32* %i, align 4
  %arrayidx56 = getelementptr inbounds i32, i32* %98, i32 %99
  store i32 -1, i32* %arrayidx56, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %100 = load i32*, i32** %nodeWrittenAt, align 4
  %101 = load i32, i32* %br, align 4
  %arrayidx57 = getelementptr inbounds i32, i32* %100, i32 %101
  %102 = load i32, i32* %arrayidx57, align 4
  %cmp58 = icmp sgt i32 %102, -1
  br i1 %cmp58, label %if.then59, label %if.else71

if.then59:                                        ; preds = %if.end
  %103 = load i32*, i32** %nodeWrittenAt, align 4
  %104 = load i32, i32* %br, align 4
  %arrayidx60 = getelementptr inbounds i32, i32* %103, i32 %104
  %105 = load i32, i32* %arrayidx60, align 4
  %106 = load i32*, i32** %linkDepB, align 4
  %107 = load i32, i32* %i, align 4
  %arrayidx61 = getelementptr inbounds i32, i32* %106, i32 %107
  store i32 %105, i32* %arrayidx61, align 4
  %108 = load %class.LinkDeps_t*, %class.LinkDeps_t** %linkDepFreeList, align 4
  %109 = load i32, i32* %linkDepFrees, align 4
  %inc62 = add nsw i32 %109, 1
  store i32 %inc62, i32* %linkDepFrees, align 4
  %arrayidx63 = getelementptr inbounds %class.LinkDeps_t, %class.LinkDeps_t* %108, i32 %109
  store %class.LinkDeps_t* %arrayidx63, %class.LinkDeps_t** %linkDep, align 4
  %110 = load i32, i32* %i, align 4
  %add64 = add nsw i32 %110, 1
  %sub = sub nsw i32 0, %add64
  %111 = load %class.LinkDeps_t*, %class.LinkDeps_t** %linkDep, align 4
  %value65 = getelementptr inbounds %class.LinkDeps_t, %class.LinkDeps_t* %111, i32 0, i32 0
  store i32 %sub, i32* %value65, align 4
  %112 = load %class.LinkDeps_t**, %class.LinkDeps_t*** %linkDepListStarts, align 4
  %113 = load i32*, i32** %nodeWrittenAt, align 4
  %114 = load i32, i32* %br, align 4
  %arrayidx66 = getelementptr inbounds i32, i32* %113, i32 %114
  %115 = load i32, i32* %arrayidx66, align 4
  %arrayidx67 = getelementptr inbounds %class.LinkDeps_t*, %class.LinkDeps_t** %112, i32 %115
  %116 = load %class.LinkDeps_t*, %class.LinkDeps_t** %arrayidx67, align 4
  %117 = load %class.LinkDeps_t*, %class.LinkDeps_t** %linkDep, align 4
  %next68 = getelementptr inbounds %class.LinkDeps_t, %class.LinkDeps_t* %117, i32 0, i32 1
  store %class.LinkDeps_t* %116, %class.LinkDeps_t** %next68, align 4
  %118 = load %class.LinkDeps_t*, %class.LinkDeps_t** %linkDep, align 4
  %119 = load %class.LinkDeps_t**, %class.LinkDeps_t*** %linkDepListStarts, align 4
  %120 = load i32*, i32** %nodeWrittenAt, align 4
  %121 = load i32, i32* %br, align 4
  %arrayidx69 = getelementptr inbounds i32, i32* %120, i32 %121
  %122 = load i32, i32* %arrayidx69, align 4
  %arrayidx70 = getelementptr inbounds %class.LinkDeps_t*, %class.LinkDeps_t** %119, i32 %122
  store %class.LinkDeps_t* %118, %class.LinkDeps_t** %arrayidx70, align 4
  br label %if.end73

if.else71:                                        ; preds = %if.end
  %123 = load i32*, i32** %linkDepB, align 4
  %124 = load i32, i32* %i, align 4
  %arrayidx72 = getelementptr inbounds i32, i32* %123, i32 %124
  store i32 -1, i32* %arrayidx72, align 4
  br label %if.end73

if.end73:                                         ; preds = %if.else71, %if.then59
  %125 = load i32*, i32** %linkDepA, align 4
  %126 = load i32, i32* %i, align 4
  %arrayidx74 = getelementptr inbounds i32, i32* %125, i32 %126
  %127 = load i32, i32* %arrayidx74, align 4
  %cmp75 = icmp eq i32 %127, -1
  br i1 %cmp75, label %land.lhs.true, label %if.end83

land.lhs.true:                                    ; preds = %if.end73
  %128 = load i32*, i32** %linkDepB, align 4
  %129 = load i32, i32* %i, align 4
  %arrayidx76 = getelementptr inbounds i32, i32* %128, i32 %129
  %130 = load i32, i32* %arrayidx76, align 4
  %cmp77 = icmp eq i32 %130, -1
  br i1 %cmp77, label %if.then78, label %if.end83

if.then78:                                        ; preds = %land.lhs.true
  %131 = load i32, i32* %i, align 4
  %132 = load i32*, i32** %readyList, align 4
  %133 = load i32, i32* %readyListTail, align 4
  %inc79 = add nsw i32 %133, 1
  store i32 %inc79, i32* %readyListTail, align 4
  %arrayidx80 = getelementptr inbounds i32, i32* %132, i32 %133
  store i32 %131, i32* %arrayidx80, align 4
  %134 = load i32*, i32** %linkDepB, align 4
  %135 = load i32, i32* %i, align 4
  %arrayidx81 = getelementptr inbounds i32, i32* %134, i32 %135
  store i32 -2, i32* %arrayidx81, align 4
  %136 = load i32*, i32** %linkDepA, align 4
  %137 = load i32, i32* %i, align 4
  %arrayidx82 = getelementptr inbounds i32, i32* %136, i32 %137
  store i32 -2, i32* %arrayidx82, align 4
  br label %if.end83

if.end83:                                         ; preds = %if.then78, %land.lhs.true, %if.end73
  %138 = load i32, i32* %i, align 4
  %139 = load i32*, i32** %nodeWrittenAt, align 4
  %140 = load i32, i32* %br, align 4
  %arrayidx84 = getelementptr inbounds i32, i32* %139, i32 %140
  store i32 %138, i32* %arrayidx84, align 4
  %141 = load i32*, i32** %nodeWrittenAt, align 4
  %142 = load i32, i32* %ar, align 4
  %arrayidx85 = getelementptr inbounds i32, i32* %141, i32 %142
  store i32 %138, i32* %arrayidx85, align 4
  br label %for.inc86

for.inc86:                                        ; preds = %if.end83
  %143 = load i32, i32* %i, align 4
  %inc87 = add nsw i32 %143, 1
  store i32 %inc87, i32* %i, align 4
  br label %for.cond25

for.end88:                                        ; preds = %for.cond25
  store i32 0, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.end, %for.end88
  %144 = load i32, i32* %readyListHead, align 4
  %145 = load i32, i32* %readyListTail, align 4
  %cmp89 = icmp ne i32 %144, %145
  br i1 %cmp89, label %while.body, label %while.end120

while.body:                                       ; preds = %while.cond
  %146 = load i32*, i32** %readyList, align 4
  %147 = load i32, i32* %readyListHead, align 4
  %inc90 = add nsw i32 %147, 1
  store i32 %inc90, i32* %readyListHead, align 4
  %arrayidx91 = getelementptr inbounds i32, i32* %146, i32 %147
  %148 = load i32, i32* %arrayidx91, align 4
  store i32 %148, i32* %linkNum, align 4
  %149 = load %"struct.btSoftBody::Link"*, %"struct.btSoftBody::Link"** %linkBuffer, align 4
  %150 = load i32, i32* %linkNum, align 4
  %arrayidx92 = getelementptr inbounds %"struct.btSoftBody::Link", %"struct.btSoftBody::Link"* %149, i32 %150
  %151 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_links93 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %151, i32 0, i32 10
  %152 = load i32, i32* %i, align 4
  %inc94 = add nsw i32 %152, 1
  store i32 %inc94, i32* %i, align 4
  %call95 = call nonnull align 4 dereferenceable(52) %"struct.btSoftBody::Link"* @_ZN20btAlignedObjectArrayIN10btSoftBody4LinkEEixEi(%class.btAlignedObjectArray.29* %m_links93, i32 %152)
  %153 = bitcast %"struct.btSoftBody::Link"* %call95 to i8*
  %154 = bitcast %"struct.btSoftBody::Link"* %arrayidx92 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %153, i8* align 4 %154, i32 52, i1 false)
  %155 = load %class.LinkDeps_t**, %class.LinkDeps_t*** %linkDepListStarts, align 4
  %156 = load i32, i32* %linkNum, align 4
  %arrayidx96 = getelementptr inbounds %class.LinkDeps_t*, %class.LinkDeps_t** %155, i32 %156
  %157 = load %class.LinkDeps_t*, %class.LinkDeps_t** %arrayidx96, align 4
  store %class.LinkDeps_t* %157, %class.LinkDeps_t** %linkDep, align 4
  br label %while.cond97

while.cond97:                                     ; preds = %if.end118, %while.body
  %158 = load %class.LinkDeps_t*, %class.LinkDeps_t** %linkDep, align 4
  %tobool = icmp ne %class.LinkDeps_t* %158, null
  br i1 %tobool, label %while.body98, label %while.end

while.body98:                                     ; preds = %while.cond97
  %159 = load %class.LinkDeps_t*, %class.LinkDeps_t** %linkDep, align 4
  %value99 = getelementptr inbounds %class.LinkDeps_t, %class.LinkDeps_t* %159, i32 0, i32 0
  %160 = load i32, i32* %value99, align 4
  store i32 %160, i32* %depLink, align 4
  %161 = load i32, i32* %depLink, align 4
  %cmp100 = icmp sge i32 %161, 0
  br i1 %cmp100, label %if.then101, label %if.else103

if.then101:                                       ; preds = %while.body98
  %162 = load i32*, i32** %linkDepA, align 4
  %163 = load i32, i32* %depLink, align 4
  %arrayidx102 = getelementptr inbounds i32, i32* %162, i32 %163
  store i32 -1, i32* %arrayidx102, align 4
  br label %if.end107

if.else103:                                       ; preds = %while.body98
  %164 = load i32, i32* %depLink, align 4
  %sub104 = sub nsw i32 0, %164
  %sub105 = sub nsw i32 %sub104, 1
  store i32 %sub105, i32* %depLink, align 4
  %165 = load i32*, i32** %linkDepB, align 4
  %166 = load i32, i32* %depLink, align 4
  %arrayidx106 = getelementptr inbounds i32, i32* %165, i32 %166
  store i32 -1, i32* %arrayidx106, align 4
  br label %if.end107

if.end107:                                        ; preds = %if.else103, %if.then101
  %167 = load i32*, i32** %linkDepA, align 4
  %168 = load i32, i32* %depLink, align 4
  %arrayidx108 = getelementptr inbounds i32, i32* %167, i32 %168
  %169 = load i32, i32* %arrayidx108, align 4
  %cmp109 = icmp eq i32 %169, -1
  br i1 %cmp109, label %land.lhs.true110, label %if.end118

land.lhs.true110:                                 ; preds = %if.end107
  %170 = load i32*, i32** %linkDepB, align 4
  %171 = load i32, i32* %depLink, align 4
  %arrayidx111 = getelementptr inbounds i32, i32* %170, i32 %171
  %172 = load i32, i32* %arrayidx111, align 4
  %cmp112 = icmp eq i32 %172, -1
  br i1 %cmp112, label %if.then113, label %if.end118

if.then113:                                       ; preds = %land.lhs.true110
  %173 = load i32, i32* %depLink, align 4
  %174 = load i32*, i32** %readyList, align 4
  %175 = load i32, i32* %readyListTail, align 4
  %inc114 = add nsw i32 %175, 1
  store i32 %inc114, i32* %readyListTail, align 4
  %arrayidx115 = getelementptr inbounds i32, i32* %174, i32 %175
  store i32 %173, i32* %arrayidx115, align 4
  %176 = load i32*, i32** %linkDepB, align 4
  %177 = load i32, i32* %depLink, align 4
  %arrayidx116 = getelementptr inbounds i32, i32* %176, i32 %177
  store i32 -2, i32* %arrayidx116, align 4
  %178 = load i32*, i32** %linkDepA, align 4
  %179 = load i32, i32* %depLink, align 4
  %arrayidx117 = getelementptr inbounds i32, i32* %178, i32 %179
  store i32 -2, i32* %arrayidx117, align 4
  br label %if.end118

if.end118:                                        ; preds = %if.then113, %land.lhs.true110, %if.end107
  %180 = load %class.LinkDeps_t*, %class.LinkDeps_t** %linkDep, align 4
  %next119 = getelementptr inbounds %class.LinkDeps_t, %class.LinkDeps_t* %180, i32 0, i32 1
  %181 = load %class.LinkDeps_t*, %class.LinkDeps_t** %next119, align 4
  store %class.LinkDeps_t* %181, %class.LinkDeps_t** %linkDep, align 4
  br label %while.cond97

while.end:                                        ; preds = %while.cond97
  br label %while.cond

while.end120:                                     ; preds = %while.cond
  %182 = load i32*, i32** %nodeWrittenAt, align 4
  %isnull = icmp eq i32* %182, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %while.end120
  %183 = bitcast i32* %182 to i8*
  call void @_ZdaPv(i8* %183) #11
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %while.end120
  %184 = load i32*, i32** %linkDepA, align 4
  %isnull121 = icmp eq i32* %184, null
  br i1 %isnull121, label %delete.end123, label %delete.notnull122

delete.notnull122:                                ; preds = %delete.end
  %185 = bitcast i32* %184 to i8*
  call void @_ZdaPv(i8* %185) #11
  br label %delete.end123

delete.end123:                                    ; preds = %delete.notnull122, %delete.end
  %186 = load i32*, i32** %linkDepB, align 4
  %isnull124 = icmp eq i32* %186, null
  br i1 %isnull124, label %delete.end126, label %delete.notnull125

delete.notnull125:                                ; preds = %delete.end123
  %187 = bitcast i32* %186 to i8*
  call void @_ZdaPv(i8* %187) #11
  br label %delete.end126

delete.end126:                                    ; preds = %delete.notnull125, %delete.end123
  %188 = load i32*, i32** %readyList, align 4
  %isnull127 = icmp eq i32* %188, null
  br i1 %isnull127, label %delete.end129, label %delete.notnull128

delete.notnull128:                                ; preds = %delete.end126
  %189 = bitcast i32* %188 to i8*
  call void @_ZdaPv(i8* %189) #11
  br label %delete.end129

delete.end129:                                    ; preds = %delete.notnull128, %delete.end126
  %190 = load %class.LinkDeps_t*, %class.LinkDeps_t** %linkDepFreeList, align 4
  %isnull130 = icmp eq %class.LinkDeps_t* %190, null
  br i1 %isnull130, label %delete.end132, label %delete.notnull131

delete.notnull131:                                ; preds = %delete.end129
  %191 = bitcast %class.LinkDeps_t* %190 to i8*
  call void @_ZdaPv(i8* %191) #11
  br label %delete.end132

delete.end132:                                    ; preds = %delete.notnull131, %delete.end129
  %192 = load %class.LinkDeps_t**, %class.LinkDeps_t*** %linkDepListStarts, align 4
  %isnull133 = icmp eq %class.LinkDeps_t** %192, null
  br i1 %isnull133, label %delete.end135, label %delete.notnull134

delete.notnull134:                                ; preds = %delete.end132
  %193 = bitcast %class.LinkDeps_t** %192 to i8*
  call void @_ZdaPv(i8* %193) #11
  br label %delete.end135

delete.end135:                                    ; preds = %delete.notnull134, %delete.end132
  %194 = load %"struct.btSoftBody::Link"*, %"struct.btSoftBody::Link"** %linkBuffer, align 4
  %isnull136 = icmp eq %"struct.btSoftBody::Link"* %194, null
  br i1 %isnull136, label %delete.end138, label %delete.notnull137

delete.notnull137:                                ; preds = %delete.end135
  %195 = bitcast %"struct.btSoftBody::Link"* %194 to i8*
  call void @_ZN10btSoftBody4LinkdaEPv(i8* %195) #5
  br label %delete.end138

delete.end138:                                    ; preds = %delete.notnull137, %delete.end135
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare { i32, i1 } @llvm.umul.with.overflow.i32(i32, i32) #7

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znam(i32) #8

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN10btSoftBody4LinknaEm(i32 %sizeInBytes) #2 comdat {
entry:
  %sizeInBytes.addr = alloca i32, align 4
  store i32 %sizeInBytes, i32* %sizeInBytes.addr, align 4
  %0 = load i32, i32* %sizeInBytes.addr, align 4
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %0, i32 16)
  ret i8* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btSoftBody::Link"* @_ZN10btSoftBody4LinkC2Ev(%"struct.btSoftBody::Link"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBody::Link"*, align 4
  store %"struct.btSoftBody::Link"* %this, %"struct.btSoftBody::Link"** %this.addr, align 4
  %this1 = load %"struct.btSoftBody::Link"*, %"struct.btSoftBody::Link"** %this.addr, align 4
  %0 = bitcast %"struct.btSoftBody::Link"* %this1 to %"struct.btSoftBody::Feature"*
  %call = call %"struct.btSoftBody::Feature"* @_ZN10btSoftBody7FeatureC2Ev(%"struct.btSoftBody::Feature"* %0)
  %m_c3 = getelementptr inbounds %"struct.btSoftBody::Link", %"struct.btSoftBody::Link"* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_c3)
  ret %"struct.btSoftBody::Link"* %this1
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdaPv(i8*) #9

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10btSoftBody4LinkdaEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDraw(%class.btSoftBody* %psb, %class.btIDebugDraw* %idraw) #2 {
entry:
  %psb.addr = alloca %class.btSoftBody*, align 4
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %com = alloca %class.btVector3, align 4
  %trs = alloca %class.btMatrix3x3, align 4
  %Xaxis = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %Yaxis = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %Zaxis = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp36 = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca %class.btVector3, align 4
  %ref.tmp38 = alloca %class.btVector3, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %i = alloca i32, align 4
  %x = alloca %class.btVector3, align 4
  %ref.tmp47 = alloca %class.btVector3, align 4
  %ref.tmp51 = alloca %class.btVector3, align 4
  %ref.tmp52 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp54 = alloca float, align 4
  store %class.btSoftBody* %psb, %class.btSoftBody** %psb.addr, align 4
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4
  %0 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_pose = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %0, i32 0, i32 5
  %m_bframe = getelementptr inbounds %"struct.btSoftBody::Pose", %"struct.btSoftBody::Pose"* %m_pose, i32 0, i32 1
  %1 = load i8, i8* %m_bframe, align 1
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_pose1 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %2, i32 0, i32 5
  %m_com = getelementptr inbounds %"struct.btSoftBody::Pose", %"struct.btSoftBody::Pose"* %m_pose1, i32 0, i32 5
  %3 = bitcast %class.btVector3* %com to i8*
  %4 = bitcast %class.btVector3* %m_com to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %5 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_pose2 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %5, i32 0, i32 5
  %m_rot = getelementptr inbounds %"struct.btSoftBody::Pose", %"struct.btSoftBody::Pose"* %m_pose2, i32 0, i32 6
  %6 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_pose3 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %6, i32 0, i32 5
  %m_scl = getelementptr inbounds %"struct.btSoftBody::Pose", %"struct.btSoftBody::Pose"* %m_pose3, i32 0, i32 7
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %trs, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_rot, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_scl)
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %trs, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %Xaxis, %class.btVector3* %ref.tmp)
  store float 0.000000e+00, float* %ref.tmp10, align 4
  store float 1.000000e+00, float* %ref.tmp11, align 4
  store float 0.000000e+00, float* %ref.tmp12, align 4
  %call13 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %trs, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %Yaxis, %class.btVector3* %ref.tmp8)
  store float 0.000000e+00, float* %ref.tmp16, align 4
  store float 0.000000e+00, float* %ref.tmp17, align 4
  store float 1.000000e+00, float* %ref.tmp18, align 4
  %call19 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp14, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %trs, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %Zaxis, %class.btVector3* %ref.tmp14)
  %7 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %Xaxis, float* nonnull align 4 dereferenceable(4) @_ZZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDrawE4ascl)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %com, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21)
  store float 1.000000e+00, float* %ref.tmp23, align 4
  store float 0.000000e+00, float* %ref.tmp24, align 4
  store float 0.000000e+00, float* %ref.tmp25, align 4
  %call26 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  %8 = bitcast %class.btIDebugDraw* %7 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %8, align 4
  %vfn = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable, i64 4
  %9 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %9(%class.btIDebugDraw* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %com, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp22)
  %10 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %Yaxis, float* nonnull align 4 dereferenceable(4) @_ZZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDrawE4ascl)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %com, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp28)
  store float 0.000000e+00, float* %ref.tmp30, align 4
  store float 1.000000e+00, float* %ref.tmp31, align 4
  store float 0.000000e+00, float* %ref.tmp32, align 4
  %call33 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp30, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  %11 = bitcast %class.btIDebugDraw* %10 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable34 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %11, align 4
  %vfn35 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable34, i64 4
  %12 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn35, align 4
  call void %12(%class.btIDebugDraw* %10, %class.btVector3* nonnull align 4 dereferenceable(16) %com, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp29)
  %13 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp37, %class.btVector3* nonnull align 4 dereferenceable(16) %Zaxis, float* nonnull align 4 dereferenceable(4) @_ZZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDrawE4ascl)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp36, %class.btVector3* nonnull align 4 dereferenceable(16) %com, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp37)
  store float 0.000000e+00, float* %ref.tmp39, align 4
  store float 0.000000e+00, float* %ref.tmp40, align 4
  store float 1.000000e+00, float* %ref.tmp41, align 4
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  %14 = bitcast %class.btIDebugDraw* %13 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable43 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %14, align 4
  %vfn44 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable43, i64 4
  %15 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn44, align 4
  call void %15(%class.btIDebugDraw* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %com, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp36, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp38)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %16 = load i32, i32* %i, align 4
  %17 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_pose45 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %17, i32 0, i32 5
  %m_pos = getelementptr inbounds %"struct.btSoftBody::Pose", %"struct.btSoftBody::Pose"* %m_pose45, i32 0, i32 3
  %call46 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %m_pos)
  %cmp = icmp slt i32 %16, %call46
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %18 = load %class.btSoftBody*, %class.btSoftBody** %psb.addr, align 4
  %m_pose48 = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %18, i32 0, i32 5
  %m_pos49 = getelementptr inbounds %"struct.btSoftBody::Pose", %"struct.btSoftBody::Pose"* %m_pose48, i32 0, i32 3
  %19 = load i32, i32* %i, align 4
  %call50 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %m_pos49, i32 %19)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp47, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %trs, %class.btVector3* nonnull align 4 dereferenceable(16) %call50)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %x, %class.btVector3* nonnull align 4 dereferenceable(16) %com, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp47)
  %20 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  store float 1.000000e+00, float* %ref.tmp52, align 4
  store float 0.000000e+00, float* %ref.tmp53, align 4
  store float 1.000000e+00, float* %ref.tmp54, align 4
  %call55 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp51, float* nonnull align 4 dereferenceable(4) %ref.tmp52, float* nonnull align 4 dereferenceable(4) %ref.tmp53, float* nonnull align 4 dereferenceable(4) %ref.tmp54)
  call void @_ZL10drawVertexP12btIDebugDrawRK9btVector3fS3_(%class.btIDebugDraw* %20, %class.btVector3* nonnull align 4 dereferenceable(16) %x, float 0x3FB99999A0000000, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp51)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %i, align 4
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btSoftBody* @_ZN17btSoftBodyHelpers10CreateRopeER19btSoftBodyWorldInfoRK9btVector3S4_ii(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %worldInfo, %class.btVector3* nonnull align 4 dereferenceable(16) %from, %class.btVector3* nonnull align 4 dereferenceable(16) %to, i32 %res, i32 %fixeds) #2 {
entry:
  %worldInfo.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %from.addr = alloca %class.btVector3*, align 4
  %to.addr = alloca %class.btVector3*, align 4
  %res.addr = alloca i32, align 4
  %fixeds.addr = alloca i32, align 4
  %r = alloca i32, align 4
  %x = alloca %class.btVector3*, align 4
  %m = alloca float*, align 4
  %i = alloca i32, align 4
  %t = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %psb = alloca %class.btSoftBody*, align 4
  store %struct.btSoftBodyWorldInfo* %worldInfo, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4
  store %class.btVector3* %from, %class.btVector3** %from.addr, align 4
  store %class.btVector3* %to, %class.btVector3** %to.addr, align 4
  store i32 %res, i32* %res.addr, align 4
  store i32 %fixeds, i32* %fixeds.addr, align 4
  %0 = load i32, i32* %res.addr, align 4
  %add = add nsw i32 %0, 2
  store i32 %add, i32* %r, align 4
  %1 = load i32, i32* %r, align 4
  %2 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %1, i32 16)
  %3 = extractvalue { i32, i1 } %2, 1
  %4 = extractvalue { i32, i1 } %2, 0
  %5 = select i1 %3, i32 -1, i32 %4
  %call = call i8* @_ZN9btVector3naEm(i32 %5)
  %6 = bitcast i8* %call to %class.btVector3*
  %isempty = icmp eq i32 %1, 0
  br i1 %isempty, label %arrayctor.cont, label %new.ctorloop

new.ctorloop:                                     ; preds = %entry
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 %1
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %new.ctorloop
  %arrayctor.cur = phi %class.btVector3* [ %6, %new.ctorloop ], [ %arrayctor.next, %arrayctor.loop ]
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %entry, %arrayctor.loop
  store %class.btVector3* %6, %class.btVector3** %x, align 4
  %7 = load i32, i32* %r, align 4
  %8 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %7, i32 4)
  %9 = extractvalue { i32, i1 } %8, 1
  %10 = extractvalue { i32, i1 } %8, 0
  %11 = select i1 %9, i32 -1, i32 %10
  %call2 = call noalias nonnull i8* @_Znam(i32 %11) #10
  %12 = bitcast i8* %call2 to float*
  store float* %12, float** %m, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont
  %13 = load i32, i32* %i, align 4
  %14 = load i32, i32* %r, align 4
  %cmp = icmp slt i32 %13, %14
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load i32, i32* %i, align 4
  %conv = sitofp i32 %15 to float
  %16 = load i32, i32* %r, align 4
  %sub = sub nsw i32 %16, 1
  %conv3 = sitofp i32 %sub to float
  %div = fdiv float %conv, %conv3
  store float %div, float* %t, align 4
  %17 = load %class.btVector3*, %class.btVector3** %from.addr, align 4
  %18 = load %class.btVector3*, %class.btVector3** %to.addr, align 4
  call void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %17, %class.btVector3* nonnull align 4 dereferenceable(16) %18, float* nonnull align 4 dereferenceable(4) %t)
  %19 = load %class.btVector3*, %class.btVector3** %x, align 4
  %20 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %19, i32 %20
  %21 = bitcast %class.btVector3* %arrayidx to i8*
  %22 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  %23 = load float*, float** %m, align 4
  %24 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds float, float* %23, i32 %24
  store float 1.000000e+00, float* %arrayidx4, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %25 = load i32, i32* %i, align 4
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call5 = call i8* @_ZN17btCollisionObjectnwEm(i32 1252)
  %26 = bitcast i8* %call5 to %class.btSoftBody*
  %27 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4
  %28 = load i32, i32* %r, align 4
  %29 = load %class.btVector3*, %class.btVector3** %x, align 4
  %30 = load float*, float** %m, align 4
  %call6 = call %class.btSoftBody* @_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf(%class.btSoftBody* %26, %struct.btSoftBodyWorldInfo* %27, i32 %28, %class.btVector3* %29, float* %30)
  store %class.btSoftBody* %26, %class.btSoftBody** %psb, align 4
  %31 = load i32, i32* %fixeds.addr, align 4
  %and = and i32 %31, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  %32 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %32, i32 0, float 0.000000e+00)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end
  %33 = load i32, i32* %fixeds.addr, align 4
  %and7 = and i32 %33, 2
  %tobool8 = icmp ne i32 %and7, 0
  br i1 %tobool8, label %if.then9, label %if.end11

if.then9:                                         ; preds = %if.end
  %34 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %35 = load i32, i32* %r, align 4
  %sub10 = sub nsw i32 %35, 1
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %34, i32 %sub10, float 0.000000e+00)
  br label %if.end11

if.end11:                                         ; preds = %if.then9, %if.end
  %36 = load %class.btVector3*, %class.btVector3** %x, align 4
  %isnull = icmp eq %class.btVector3* %36, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %if.end11
  %37 = bitcast %class.btVector3* %36 to i8*
  call void @_ZN9btVector3daEPv(i8* %37) #5
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %if.end11
  %38 = load float*, float** %m, align 4
  %isnull12 = icmp eq float* %38, null
  br i1 %isnull12, label %delete.end14, label %delete.notnull13

delete.notnull13:                                 ; preds = %delete.end
  %39 = bitcast float* %38 to i8*
  call void @_ZdaPv(i8* %39) #11
  br label %delete.end14

delete.end14:                                     ; preds = %delete.notnull13, %delete.end
  store i32 1, i32* %i, align 4
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc19, %delete.end14
  %40 = load i32, i32* %i, align 4
  %41 = load i32, i32* %r, align 4
  %cmp16 = icmp slt i32 %40, %41
  br i1 %cmp16, label %for.body17, label %for.end21

for.body17:                                       ; preds = %for.cond15
  %42 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %43 = load i32, i32* %i, align 4
  %sub18 = sub nsw i32 %43, 1
  %44 = load i32, i32* %i, align 4
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %42, i32 %sub18, i32 %44, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %for.inc19

for.inc19:                                        ; preds = %for.body17
  %45 = load i32, i32* %i, align 4
  %inc20 = add nsw i32 %45, 1
  store i32 %inc20, i32* %i, align 4
  br label %for.cond15

for.end21:                                        ; preds = %for.cond15
  %46 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  ret %class.btSoftBody* %46
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN9btVector3naEm(i32 %sizeInBytes) #2 comdat {
entry:
  %sizeInBytes.addr = alloca i32, align 4
  store i32 %sizeInBytes, i32* %sizeInBytes.addr, align 4
  %0 = load i32, i32* %sizeInBytes.addr, align 4
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %0, i32 16)
  ret i8* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, float* nonnull align 4 dereferenceable(4) %t) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %t.addr = alloca float*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  store float* %t, float** %t.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %2 = load float*, float** %t.addr, align 4
  call void @_ZNK9btVector34lerpERKS_RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN17btCollisionObjectnwEm(i32 %sizeInBytes) #2 comdat {
entry:
  %sizeInBytes.addr = alloca i32, align 4
  store i32 %sizeInBytes, i32* %sizeInBytes.addr, align 4
  %0 = load i32, i32* %sizeInBytes.addr, align 4
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %0, i32 16)
  ret i8* %call
}

declare %class.btSoftBody* @_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf(%class.btSoftBody* returned, %struct.btSoftBodyWorldInfo*, i32, %class.btVector3*, float*) unnamed_addr #3

declare void @_ZN10btSoftBody7setMassEif(%class.btSoftBody*, i32, float) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector3daEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody*, i32, i32, %"struct.btSoftBody::Material"*, i1 zeroext) #3

; Function Attrs: noinline optnone
define hidden %class.btSoftBody* @_ZN17btSoftBodyHelpers11CreatePatchER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiib(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %worldInfo, %class.btVector3* nonnull align 4 dereferenceable(16) %corner00, %class.btVector3* nonnull align 4 dereferenceable(16) %corner10, %class.btVector3* nonnull align 4 dereferenceable(16) %corner01, %class.btVector3* nonnull align 4 dereferenceable(16) %corner11, i32 %resx, i32 %resy, i32 %fixeds, i1 zeroext %gendiags) #2 {
entry:
  %retval = alloca %class.btSoftBody*, align 4
  %worldInfo.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %corner00.addr = alloca %class.btVector3*, align 4
  %corner10.addr = alloca %class.btVector3*, align 4
  %corner01.addr = alloca %class.btVector3*, align 4
  %corner11.addr = alloca %class.btVector3*, align 4
  %resx.addr = alloca i32, align 4
  %resy.addr = alloca i32, align 4
  %fixeds.addr = alloca i32, align 4
  %gendiags.addr = alloca i8, align 1
  %rx = alloca i32, align 4
  %ry = alloca i32, align 4
  %tot = alloca i32, align 4
  %x = alloca %class.btVector3*, align 4
  %m = alloca float*, align 4
  %iy = alloca i32, align 4
  %ty = alloca float, align 4
  %py0 = alloca %class.btVector3, align 4
  %py1 = alloca %class.btVector3, align 4
  %ix = alloca i32, align 4
  %tx = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %psb = alloca %class.btSoftBody*, align 4
  %ix54 = alloca i32, align 4
  %idx = alloca i32, align 4
  %mdx = alloca i8, align 1
  %mdy = alloca i8, align 1
  store %struct.btSoftBodyWorldInfo* %worldInfo, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4
  store %class.btVector3* %corner00, %class.btVector3** %corner00.addr, align 4
  store %class.btVector3* %corner10, %class.btVector3** %corner10.addr, align 4
  store %class.btVector3* %corner01, %class.btVector3** %corner01.addr, align 4
  store %class.btVector3* %corner11, %class.btVector3** %corner11.addr, align 4
  store i32 %resx, i32* %resx.addr, align 4
  store i32 %resy, i32* %resy.addr, align 4
  store i32 %fixeds, i32* %fixeds.addr, align 4
  %frombool = zext i1 %gendiags to i8
  store i8 %frombool, i8* %gendiags.addr, align 1
  %0 = load i32, i32* %resx.addr, align 4
  %cmp = icmp slt i32 %0, 2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %resy.addr, align 4
  %cmp1 = icmp slt i32 %1, 2
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store %class.btSoftBody* null, %class.btSoftBody** %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i32, i32* %resx.addr, align 4
  store i32 %2, i32* %rx, align 4
  %3 = load i32, i32* %resy.addr, align 4
  store i32 %3, i32* %ry, align 4
  %4 = load i32, i32* %rx, align 4
  %5 = load i32, i32* %ry, align 4
  %mul = mul nsw i32 %4, %5
  store i32 %mul, i32* %tot, align 4
  %6 = load i32, i32* %tot, align 4
  %7 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %6, i32 16)
  %8 = extractvalue { i32, i1 } %7, 1
  %9 = extractvalue { i32, i1 } %7, 0
  %10 = select i1 %8, i32 -1, i32 %9
  %call = call i8* @_ZN9btVector3naEm(i32 %10)
  %11 = bitcast i8* %call to %class.btVector3*
  %isempty = icmp eq i32 %6, 0
  br i1 %isempty, label %arrayctor.cont, label %new.ctorloop

new.ctorloop:                                     ; preds = %if.end
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 %6
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %new.ctorloop
  %arrayctor.cur = phi %class.btVector3* [ %11, %new.ctorloop ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %if.end, %arrayctor.loop
  store %class.btVector3* %11, %class.btVector3** %x, align 4
  %12 = load i32, i32* %tot, align 4
  %13 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %12, i32 4)
  %14 = extractvalue { i32, i1 } %13, 1
  %15 = extractvalue { i32, i1 } %13, 0
  %16 = select i1 %14, i32 -1, i32 %15
  %call3 = call noalias nonnull i8* @_Znam(i32 %16) #10
  %17 = bitcast i8* %call3 to float*
  store float* %17, float** %m, align 4
  store i32 0, i32* %iy, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc17, %arrayctor.cont
  %18 = load i32, i32* %iy, align 4
  %19 = load i32, i32* %ry, align 4
  %cmp4 = icmp slt i32 %18, %19
  br i1 %cmp4, label %for.body, label %for.end19

for.body:                                         ; preds = %for.cond
  %20 = load i32, i32* %iy, align 4
  %conv = sitofp i32 %20 to float
  %21 = load i32, i32* %ry, align 4
  %sub = sub nsw i32 %21, 1
  %conv5 = sitofp i32 %sub to float
  %div = fdiv float %conv, %conv5
  store float %div, float* %ty, align 4
  %22 = load %class.btVector3*, %class.btVector3** %corner00.addr, align 4
  %23 = load %class.btVector3*, %class.btVector3** %corner01.addr, align 4
  call void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* sret align 4 %py0, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %23, float* nonnull align 4 dereferenceable(4) %ty)
  %24 = load %class.btVector3*, %class.btVector3** %corner10.addr, align 4
  %25 = load %class.btVector3*, %class.btVector3** %corner11.addr, align 4
  call void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* sret align 4 %py1, %class.btVector3* nonnull align 4 dereferenceable(16) %24, %class.btVector3* nonnull align 4 dereferenceable(16) %25, float* nonnull align 4 dereferenceable(4) %ty)
  store i32 0, i32* %ix, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc, %for.body
  %26 = load i32, i32* %ix, align 4
  %27 = load i32, i32* %rx, align 4
  %cmp7 = icmp slt i32 %26, %27
  br i1 %cmp7, label %for.body8, label %for.end

for.body8:                                        ; preds = %for.cond6
  %28 = load i32, i32* %ix, align 4
  %conv9 = sitofp i32 %28 to float
  %29 = load i32, i32* %rx, align 4
  %sub10 = sub nsw i32 %29, 1
  %conv11 = sitofp i32 %sub10 to float
  %div12 = fdiv float %conv9, %conv11
  store float %div12, float* %tx, align 4
  call void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %py0, %class.btVector3* nonnull align 4 dereferenceable(16) %py1, float* nonnull align 4 dereferenceable(4) %tx)
  %30 = load %class.btVector3*, %class.btVector3** %x, align 4
  %31 = load i32, i32* %iy, align 4
  %32 = load i32, i32* %rx, align 4
  %mul13 = mul nsw i32 %31, %32
  %33 = load i32, i32* %ix, align 4
  %add = add nsw i32 %mul13, %33
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %30, i32 %add
  %34 = bitcast %class.btVector3* %arrayidx to i8*
  %35 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 16, i1 false)
  %36 = load float*, float** %m, align 4
  %37 = load i32, i32* %iy, align 4
  %38 = load i32, i32* %rx, align 4
  %mul14 = mul nsw i32 %37, %38
  %39 = load i32, i32* %ix, align 4
  %add15 = add nsw i32 %mul14, %39
  %arrayidx16 = getelementptr inbounds float, float* %36, i32 %add15
  store float 1.000000e+00, float* %arrayidx16, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %40 = load i32, i32* %ix, align 4
  %inc = add nsw i32 %40, 1
  store i32 %inc, i32* %ix, align 4
  br label %for.cond6

for.end:                                          ; preds = %for.cond6
  br label %for.inc17

for.inc17:                                        ; preds = %for.end
  %41 = load i32, i32* %iy, align 4
  %inc18 = add nsw i32 %41, 1
  store i32 %inc18, i32* %iy, align 4
  br label %for.cond

for.end19:                                        ; preds = %for.cond
  %call20 = call i8* @_ZN17btCollisionObjectnwEm(i32 1252)
  %42 = bitcast i8* %call20 to %class.btSoftBody*
  %43 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4
  %44 = load i32, i32* %tot, align 4
  %45 = load %class.btVector3*, %class.btVector3** %x, align 4
  %46 = load float*, float** %m, align 4
  %call21 = call %class.btSoftBody* @_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf(%class.btSoftBody* %42, %struct.btSoftBodyWorldInfo* %43, i32 %44, %class.btVector3* %45, float* %46)
  store %class.btSoftBody* %42, %class.btSoftBody** %psb, align 4
  %47 = load i32, i32* %fixeds.addr, align 4
  %and = and i32 %47, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then22, label %if.end25

if.then22:                                        ; preds = %for.end19
  %48 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %49 = load i32, i32* %rx, align 4
  %mul23 = mul nsw i32 0, %49
  %add24 = add nsw i32 %mul23, 0
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %48, i32 %add24, float 0.000000e+00)
  br label %if.end25

if.end25:                                         ; preds = %if.then22, %for.end19
  %50 = load i32, i32* %fixeds.addr, align 4
  %and26 = and i32 %50, 2
  %tobool27 = icmp ne i32 %and26, 0
  br i1 %tobool27, label %if.then28, label %if.end32

if.then28:                                        ; preds = %if.end25
  %51 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %52 = load i32, i32* %rx, align 4
  %mul29 = mul nsw i32 0, %52
  %53 = load i32, i32* %rx, align 4
  %sub30 = sub nsw i32 %53, 1
  %add31 = add nsw i32 %mul29, %sub30
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %51, i32 %add31, float 0.000000e+00)
  br label %if.end32

if.end32:                                         ; preds = %if.then28, %if.end25
  %54 = load i32, i32* %fixeds.addr, align 4
  %and33 = and i32 %54, 4
  %tobool34 = icmp ne i32 %and33, 0
  br i1 %tobool34, label %if.then35, label %if.end39

if.then35:                                        ; preds = %if.end32
  %55 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %56 = load i32, i32* %ry, align 4
  %sub36 = sub nsw i32 %56, 1
  %57 = load i32, i32* %rx, align 4
  %mul37 = mul nsw i32 %sub36, %57
  %add38 = add nsw i32 %mul37, 0
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %55, i32 %add38, float 0.000000e+00)
  br label %if.end39

if.end39:                                         ; preds = %if.then35, %if.end32
  %58 = load i32, i32* %fixeds.addr, align 4
  %and40 = and i32 %58, 8
  %tobool41 = icmp ne i32 %and40, 0
  br i1 %tobool41, label %if.then42, label %if.end47

if.then42:                                        ; preds = %if.end39
  %59 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %60 = load i32, i32* %ry, align 4
  %sub43 = sub nsw i32 %60, 1
  %61 = load i32, i32* %rx, align 4
  %mul44 = mul nsw i32 %sub43, %61
  %62 = load i32, i32* %rx, align 4
  %sub45 = sub nsw i32 %62, 1
  %add46 = add nsw i32 %mul44, %sub45
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %59, i32 %add46, float 0.000000e+00)
  br label %if.end47

if.end47:                                         ; preds = %if.then42, %if.end39
  %63 = load %class.btVector3*, %class.btVector3** %x, align 4
  %isnull = icmp eq %class.btVector3* %63, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %if.end47
  %64 = bitcast %class.btVector3* %63 to i8*
  call void @_ZN9btVector3daEPv(i8* %64) #5
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %if.end47
  %65 = load float*, float** %m, align 4
  %isnull48 = icmp eq float* %65, null
  br i1 %isnull48, label %delete.end50, label %delete.notnull49

delete.notnull49:                                 ; preds = %delete.end
  %66 = bitcast float* %65 to i8*
  call void @_ZdaPv(i8* %66) #11
  br label %delete.end50

delete.end50:                                     ; preds = %delete.notnull49, %delete.end
  store i32 0, i32* %iy, align 4
  br label %for.cond51

for.cond51:                                       ; preds = %for.inc144, %delete.end50
  %67 = load i32, i32* %iy, align 4
  %68 = load i32, i32* %ry, align 4
  %cmp52 = icmp slt i32 %67, %68
  br i1 %cmp52, label %for.body53, label %for.end146

for.body53:                                       ; preds = %for.cond51
  store i32 0, i32* %ix54, align 4
  br label %for.cond55

for.cond55:                                       ; preds = %for.inc141, %for.body53
  %69 = load i32, i32* %ix54, align 4
  %70 = load i32, i32* %rx, align 4
  %cmp56 = icmp slt i32 %69, %70
  br i1 %cmp56, label %for.body57, label %for.end143

for.body57:                                       ; preds = %for.cond55
  %71 = load i32, i32* %iy, align 4
  %72 = load i32, i32* %rx, align 4
  %mul58 = mul nsw i32 %71, %72
  %73 = load i32, i32* %ix54, align 4
  %add59 = add nsw i32 %mul58, %73
  store i32 %add59, i32* %idx, align 4
  %74 = load i32, i32* %ix54, align 4
  %add60 = add nsw i32 %74, 1
  %75 = load i32, i32* %rx, align 4
  %cmp61 = icmp slt i32 %add60, %75
  %frombool62 = zext i1 %cmp61 to i8
  store i8 %frombool62, i8* %mdx, align 1
  %76 = load i32, i32* %iy, align 4
  %add63 = add nsw i32 %76, 1
  %77 = load i32, i32* %ry, align 4
  %cmp64 = icmp slt i32 %add63, %77
  %frombool65 = zext i1 %cmp64 to i8
  store i8 %frombool65, i8* %mdy, align 1
  %78 = load i8, i8* %mdx, align 1
  %tobool66 = trunc i8 %78 to i1
  br i1 %tobool66, label %if.then67, label %if.end71

if.then67:                                        ; preds = %for.body57
  %79 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %80 = load i32, i32* %idx, align 4
  %81 = load i32, i32* %iy, align 4
  %82 = load i32, i32* %rx, align 4
  %mul68 = mul nsw i32 %81, %82
  %83 = load i32, i32* %ix54, align 4
  %add69 = add nsw i32 %83, 1
  %add70 = add nsw i32 %mul68, %add69
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %79, i32 %80, i32 %add70, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end71

if.end71:                                         ; preds = %if.then67, %for.body57
  %84 = load i8, i8* %mdy, align 1
  %tobool72 = trunc i8 %84 to i1
  br i1 %tobool72, label %if.then73, label %if.end77

if.then73:                                        ; preds = %if.end71
  %85 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %86 = load i32, i32* %idx, align 4
  %87 = load i32, i32* %iy, align 4
  %add74 = add nsw i32 %87, 1
  %88 = load i32, i32* %rx, align 4
  %mul75 = mul nsw i32 %add74, %88
  %89 = load i32, i32* %ix54, align 4
  %add76 = add nsw i32 %mul75, %89
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %85, i32 %86, i32 %add76, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end77

if.end77:                                         ; preds = %if.then73, %if.end71
  %90 = load i8, i8* %mdx, align 1
  %tobool78 = trunc i8 %90 to i1
  br i1 %tobool78, label %land.lhs.true, label %if.end140

land.lhs.true:                                    ; preds = %if.end77
  %91 = load i8, i8* %mdy, align 1
  %tobool79 = trunc i8 %91 to i1
  br i1 %tobool79, label %if.then80, label %if.end140

if.then80:                                        ; preds = %land.lhs.true
  %92 = load i32, i32* %ix54, align 4
  %93 = load i32, i32* %iy, align 4
  %add81 = add nsw i32 %92, %93
  %and82 = and i32 %add81, 1
  %tobool83 = icmp ne i32 %and82, 0
  br i1 %tobool83, label %if.then84, label %if.else

if.then84:                                        ; preds = %if.then80
  %94 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %95 = load i32, i32* %iy, align 4
  %96 = load i32, i32* %rx, align 4
  %mul85 = mul nsw i32 %95, %96
  %97 = load i32, i32* %ix54, align 4
  %add86 = add nsw i32 %mul85, %97
  %98 = load i32, i32* %iy, align 4
  %99 = load i32, i32* %rx, align 4
  %mul87 = mul nsw i32 %98, %99
  %100 = load i32, i32* %ix54, align 4
  %add88 = add nsw i32 %100, 1
  %add89 = add nsw i32 %mul87, %add88
  %101 = load i32, i32* %iy, align 4
  %add90 = add nsw i32 %101, 1
  %102 = load i32, i32* %rx, align 4
  %mul91 = mul nsw i32 %add90, %102
  %103 = load i32, i32* %ix54, align 4
  %add92 = add nsw i32 %103, 1
  %add93 = add nsw i32 %mul91, %add92
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %94, i32 %add86, i32 %add89, i32 %add93, %"struct.btSoftBody::Material"* null)
  %104 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %105 = load i32, i32* %iy, align 4
  %106 = load i32, i32* %rx, align 4
  %mul94 = mul nsw i32 %105, %106
  %107 = load i32, i32* %ix54, align 4
  %add95 = add nsw i32 %mul94, %107
  %108 = load i32, i32* %iy, align 4
  %add96 = add nsw i32 %108, 1
  %109 = load i32, i32* %rx, align 4
  %mul97 = mul nsw i32 %add96, %109
  %110 = load i32, i32* %ix54, align 4
  %add98 = add nsw i32 %110, 1
  %add99 = add nsw i32 %mul97, %add98
  %111 = load i32, i32* %iy, align 4
  %add100 = add nsw i32 %111, 1
  %112 = load i32, i32* %rx, align 4
  %mul101 = mul nsw i32 %add100, %112
  %113 = load i32, i32* %ix54, align 4
  %add102 = add nsw i32 %mul101, %113
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %104, i32 %add95, i32 %add99, i32 %add102, %"struct.btSoftBody::Material"* null)
  %114 = load i8, i8* %gendiags.addr, align 1
  %tobool103 = trunc i8 %114 to i1
  br i1 %tobool103, label %if.then104, label %if.end111

if.then104:                                       ; preds = %if.then84
  %115 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %116 = load i32, i32* %iy, align 4
  %117 = load i32, i32* %rx, align 4
  %mul105 = mul nsw i32 %116, %117
  %118 = load i32, i32* %ix54, align 4
  %add106 = add nsw i32 %mul105, %118
  %119 = load i32, i32* %iy, align 4
  %add107 = add nsw i32 %119, 1
  %120 = load i32, i32* %rx, align 4
  %mul108 = mul nsw i32 %add107, %120
  %121 = load i32, i32* %ix54, align 4
  %add109 = add nsw i32 %121, 1
  %add110 = add nsw i32 %mul108, %add109
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %115, i32 %add106, i32 %add110, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end111

if.end111:                                        ; preds = %if.then104, %if.then84
  br label %if.end139

if.else:                                          ; preds = %if.then80
  %122 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %123 = load i32, i32* %iy, align 4
  %add112 = add nsw i32 %123, 1
  %124 = load i32, i32* %rx, align 4
  %mul113 = mul nsw i32 %add112, %124
  %125 = load i32, i32* %ix54, align 4
  %add114 = add nsw i32 %mul113, %125
  %126 = load i32, i32* %iy, align 4
  %127 = load i32, i32* %rx, align 4
  %mul115 = mul nsw i32 %126, %127
  %128 = load i32, i32* %ix54, align 4
  %add116 = add nsw i32 %mul115, %128
  %129 = load i32, i32* %iy, align 4
  %130 = load i32, i32* %rx, align 4
  %mul117 = mul nsw i32 %129, %130
  %131 = load i32, i32* %ix54, align 4
  %add118 = add nsw i32 %131, 1
  %add119 = add nsw i32 %mul117, %add118
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %122, i32 %add114, i32 %add116, i32 %add119, %"struct.btSoftBody::Material"* null)
  %132 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %133 = load i32, i32* %iy, align 4
  %add120 = add nsw i32 %133, 1
  %134 = load i32, i32* %rx, align 4
  %mul121 = mul nsw i32 %add120, %134
  %135 = load i32, i32* %ix54, align 4
  %add122 = add nsw i32 %mul121, %135
  %136 = load i32, i32* %iy, align 4
  %137 = load i32, i32* %rx, align 4
  %mul123 = mul nsw i32 %136, %137
  %138 = load i32, i32* %ix54, align 4
  %add124 = add nsw i32 %138, 1
  %add125 = add nsw i32 %mul123, %add124
  %139 = load i32, i32* %iy, align 4
  %add126 = add nsw i32 %139, 1
  %140 = load i32, i32* %rx, align 4
  %mul127 = mul nsw i32 %add126, %140
  %141 = load i32, i32* %ix54, align 4
  %add128 = add nsw i32 %141, 1
  %add129 = add nsw i32 %mul127, %add128
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %132, i32 %add122, i32 %add125, i32 %add129, %"struct.btSoftBody::Material"* null)
  %142 = load i8, i8* %gendiags.addr, align 1
  %tobool130 = trunc i8 %142 to i1
  br i1 %tobool130, label %if.then131, label %if.end138

if.then131:                                       ; preds = %if.else
  %143 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %144 = load i32, i32* %iy, align 4
  %145 = load i32, i32* %rx, align 4
  %mul132 = mul nsw i32 %144, %145
  %146 = load i32, i32* %ix54, align 4
  %add133 = add nsw i32 %146, 1
  %add134 = add nsw i32 %mul132, %add133
  %147 = load i32, i32* %iy, align 4
  %add135 = add nsw i32 %147, 1
  %148 = load i32, i32* %rx, align 4
  %mul136 = mul nsw i32 %add135, %148
  %149 = load i32, i32* %ix54, align 4
  %add137 = add nsw i32 %mul136, %149
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %143, i32 %add134, i32 %add137, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end138

if.end138:                                        ; preds = %if.then131, %if.else
  br label %if.end139

if.end139:                                        ; preds = %if.end138, %if.end111
  br label %if.end140

if.end140:                                        ; preds = %if.end139, %land.lhs.true, %if.end77
  br label %for.inc141

for.inc141:                                       ; preds = %if.end140
  %150 = load i32, i32* %ix54, align 4
  %inc142 = add nsw i32 %150, 1
  store i32 %inc142, i32* %ix54, align 4
  br label %for.cond55

for.end143:                                       ; preds = %for.cond55
  br label %for.inc144

for.inc144:                                       ; preds = %for.end143
  %151 = load i32, i32* %iy, align 4
  %inc145 = add nsw i32 %151, 1
  store i32 %inc145, i32* %iy, align 4
  br label %for.cond51

for.end146:                                       ; preds = %for.cond51
  %152 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  store %class.btSoftBody* %152, %class.btSoftBody** %retval, align 4
  br label %return

return:                                           ; preds = %for.end146, %if.then
  %153 = load %class.btSoftBody*, %class.btSoftBody** %retval, align 4
  ret %class.btSoftBody* %153
}

declare void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody*, i32, i32, i32, %"struct.btSoftBody::Material"*) #3

; Function Attrs: noinline optnone
define hidden %class.btSoftBody* @_ZN17btSoftBodyHelpers13CreatePatchUVER19btSoftBodyWorldInfoRK9btVector3S4_S4_S4_iiibPf(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %worldInfo, %class.btVector3* nonnull align 4 dereferenceable(16) %corner00, %class.btVector3* nonnull align 4 dereferenceable(16) %corner10, %class.btVector3* nonnull align 4 dereferenceable(16) %corner01, %class.btVector3* nonnull align 4 dereferenceable(16) %corner11, i32 %resx, i32 %resy, i32 %fixeds, i1 zeroext %gendiags, float* %tex_coords) #2 {
entry:
  %retval = alloca %class.btSoftBody*, align 4
  %worldInfo.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %corner00.addr = alloca %class.btVector3*, align 4
  %corner10.addr = alloca %class.btVector3*, align 4
  %corner01.addr = alloca %class.btVector3*, align 4
  %corner11.addr = alloca %class.btVector3*, align 4
  %resx.addr = alloca i32, align 4
  %resy.addr = alloca i32, align 4
  %fixeds.addr = alloca i32, align 4
  %gendiags.addr = alloca i8, align 1
  %tex_coords.addr = alloca float*, align 4
  %rx = alloca i32, align 4
  %ry = alloca i32, align 4
  %tot = alloca i32, align 4
  %x = alloca %class.btVector3*, align 4
  %m = alloca float*, align 4
  %iy = alloca i32, align 4
  %ty = alloca float, align 4
  %py0 = alloca %class.btVector3, align 4
  %py1 = alloca %class.btVector3, align 4
  %ix = alloca i32, align 4
  %tx = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %psb = alloca %class.btSoftBody*, align 4
  %z = alloca i32, align 4
  %ix98 = alloca i32, align 4
  %mdx = alloca i8, align 1
  %mdy = alloca i8, align 1
  %node00 = alloca i32, align 4
  %node01 = alloca i32, align 4
  %node10 = alloca i32, align 4
  %node11 = alloca i32, align 4
  store %struct.btSoftBodyWorldInfo* %worldInfo, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4
  store %class.btVector3* %corner00, %class.btVector3** %corner00.addr, align 4
  store %class.btVector3* %corner10, %class.btVector3** %corner10.addr, align 4
  store %class.btVector3* %corner01, %class.btVector3** %corner01.addr, align 4
  store %class.btVector3* %corner11, %class.btVector3** %corner11.addr, align 4
  store i32 %resx, i32* %resx.addr, align 4
  store i32 %resy, i32* %resy.addr, align 4
  store i32 %fixeds, i32* %fixeds.addr, align 4
  %frombool = zext i1 %gendiags to i8
  store i8 %frombool, i8* %gendiags.addr, align 1
  store float* %tex_coords, float** %tex_coords.addr, align 4
  %0 = load i32, i32* %resx.addr, align 4
  %cmp = icmp slt i32 %0, 2
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %1 = load i32, i32* %resy.addr, align 4
  %cmp1 = icmp slt i32 %1, 2
  br i1 %cmp1, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store %class.btSoftBody* null, %class.btSoftBody** %retval, align 4
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %2 = load i32, i32* %resx.addr, align 4
  store i32 %2, i32* %rx, align 4
  %3 = load i32, i32* %resy.addr, align 4
  store i32 %3, i32* %ry, align 4
  %4 = load i32, i32* %rx, align 4
  %5 = load i32, i32* %ry, align 4
  %mul = mul nsw i32 %4, %5
  store i32 %mul, i32* %tot, align 4
  %6 = load i32, i32* %tot, align 4
  %7 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %6, i32 16)
  %8 = extractvalue { i32, i1 } %7, 1
  %9 = extractvalue { i32, i1 } %7, 0
  %10 = select i1 %8, i32 -1, i32 %9
  %call = call i8* @_ZN9btVector3naEm(i32 %10)
  %11 = bitcast i8* %call to %class.btVector3*
  %isempty = icmp eq i32 %6, 0
  br i1 %isempty, label %arrayctor.cont, label %new.ctorloop

new.ctorloop:                                     ; preds = %if.end
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 %6
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %new.ctorloop
  %arrayctor.cur = phi %class.btVector3* [ %11, %new.ctorloop ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %if.end, %arrayctor.loop
  store %class.btVector3* %11, %class.btVector3** %x, align 4
  %12 = load i32, i32* %tot, align 4
  %13 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %12, i32 4)
  %14 = extractvalue { i32, i1 } %13, 1
  %15 = extractvalue { i32, i1 } %13, 0
  %16 = select i1 %14, i32 -1, i32 %15
  %call3 = call noalias nonnull i8* @_Znam(i32 %16) #10
  %17 = bitcast i8* %call3 to float*
  store float* %17, float** %m, align 4
  store i32 0, i32* %iy, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc17, %arrayctor.cont
  %18 = load i32, i32* %iy, align 4
  %19 = load i32, i32* %ry, align 4
  %cmp4 = icmp slt i32 %18, %19
  br i1 %cmp4, label %for.body, label %for.end19

for.body:                                         ; preds = %for.cond
  %20 = load i32, i32* %iy, align 4
  %conv = sitofp i32 %20 to float
  %21 = load i32, i32* %ry, align 4
  %sub = sub nsw i32 %21, 1
  %conv5 = sitofp i32 %sub to float
  %div = fdiv float %conv, %conv5
  store float %div, float* %ty, align 4
  %22 = load %class.btVector3*, %class.btVector3** %corner00.addr, align 4
  %23 = load %class.btVector3*, %class.btVector3** %corner01.addr, align 4
  call void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* sret align 4 %py0, %class.btVector3* nonnull align 4 dereferenceable(16) %22, %class.btVector3* nonnull align 4 dereferenceable(16) %23, float* nonnull align 4 dereferenceable(4) %ty)
  %24 = load %class.btVector3*, %class.btVector3** %corner10.addr, align 4
  %25 = load %class.btVector3*, %class.btVector3** %corner11.addr, align 4
  call void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* sret align 4 %py1, %class.btVector3* nonnull align 4 dereferenceable(16) %24, %class.btVector3* nonnull align 4 dereferenceable(16) %25, float* nonnull align 4 dereferenceable(4) %ty)
  store i32 0, i32* %ix, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc, %for.body
  %26 = load i32, i32* %ix, align 4
  %27 = load i32, i32* %rx, align 4
  %cmp7 = icmp slt i32 %26, %27
  br i1 %cmp7, label %for.body8, label %for.end

for.body8:                                        ; preds = %for.cond6
  %28 = load i32, i32* %ix, align 4
  %conv9 = sitofp i32 %28 to float
  %29 = load i32, i32* %rx, align 4
  %sub10 = sub nsw i32 %29, 1
  %conv11 = sitofp i32 %sub10 to float
  %div12 = fdiv float %conv9, %conv11
  store float %div12, float* %tx, align 4
  call void @_Z4lerpRK9btVector3S1_RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %py0, %class.btVector3* nonnull align 4 dereferenceable(16) %py1, float* nonnull align 4 dereferenceable(4) %tx)
  %30 = load %class.btVector3*, %class.btVector3** %x, align 4
  %31 = load i32, i32* %iy, align 4
  %32 = load i32, i32* %rx, align 4
  %mul13 = mul nsw i32 %31, %32
  %33 = load i32, i32* %ix, align 4
  %add = add nsw i32 %mul13, %33
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %30, i32 %add
  %34 = bitcast %class.btVector3* %arrayidx to i8*
  %35 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 16, i1 false)
  %36 = load float*, float** %m, align 4
  %37 = load i32, i32* %iy, align 4
  %38 = load i32, i32* %rx, align 4
  %mul14 = mul nsw i32 %37, %38
  %39 = load i32, i32* %ix, align 4
  %add15 = add nsw i32 %mul14, %39
  %arrayidx16 = getelementptr inbounds float, float* %36, i32 %add15
  store float 1.000000e+00, float* %arrayidx16, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body8
  %40 = load i32, i32* %ix, align 4
  %inc = add nsw i32 %40, 1
  store i32 %inc, i32* %ix, align 4
  br label %for.cond6

for.end:                                          ; preds = %for.cond6
  br label %for.inc17

for.inc17:                                        ; preds = %for.end
  %41 = load i32, i32* %iy, align 4
  %inc18 = add nsw i32 %41, 1
  store i32 %inc18, i32* %iy, align 4
  br label %for.cond

for.end19:                                        ; preds = %for.cond
  %call20 = call i8* @_ZN17btCollisionObjectnwEm(i32 1252)
  %42 = bitcast i8* %call20 to %class.btSoftBody*
  %43 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4
  %44 = load i32, i32* %tot, align 4
  %45 = load %class.btVector3*, %class.btVector3** %x, align 4
  %46 = load float*, float** %m, align 4
  %call21 = call %class.btSoftBody* @_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf(%class.btSoftBody* %42, %struct.btSoftBodyWorldInfo* %43, i32 %44, %class.btVector3* %45, float* %46)
  store %class.btSoftBody* %42, %class.btSoftBody** %psb, align 4
  %47 = load i32, i32* %fixeds.addr, align 4
  %and = and i32 %47, 1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then22, label %if.end25

if.then22:                                        ; preds = %for.end19
  %48 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %49 = load i32, i32* %rx, align 4
  %mul23 = mul nsw i32 0, %49
  %add24 = add nsw i32 %mul23, 0
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %48, i32 %add24, float 0.000000e+00)
  br label %if.end25

if.end25:                                         ; preds = %if.then22, %for.end19
  %50 = load i32, i32* %fixeds.addr, align 4
  %and26 = and i32 %50, 2
  %tobool27 = icmp ne i32 %and26, 0
  br i1 %tobool27, label %if.then28, label %if.end32

if.then28:                                        ; preds = %if.end25
  %51 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %52 = load i32, i32* %rx, align 4
  %mul29 = mul nsw i32 0, %52
  %53 = load i32, i32* %rx, align 4
  %sub30 = sub nsw i32 %53, 1
  %add31 = add nsw i32 %mul29, %sub30
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %51, i32 %add31, float 0.000000e+00)
  br label %if.end32

if.end32:                                         ; preds = %if.then28, %if.end25
  %54 = load i32, i32* %fixeds.addr, align 4
  %and33 = and i32 %54, 4
  %tobool34 = icmp ne i32 %and33, 0
  br i1 %tobool34, label %if.then35, label %if.end39

if.then35:                                        ; preds = %if.end32
  %55 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %56 = load i32, i32* %ry, align 4
  %sub36 = sub nsw i32 %56, 1
  %57 = load i32, i32* %rx, align 4
  %mul37 = mul nsw i32 %sub36, %57
  %add38 = add nsw i32 %mul37, 0
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %55, i32 %add38, float 0.000000e+00)
  br label %if.end39

if.end39:                                         ; preds = %if.then35, %if.end32
  %58 = load i32, i32* %fixeds.addr, align 4
  %and40 = and i32 %58, 8
  %tobool41 = icmp ne i32 %and40, 0
  br i1 %tobool41, label %if.then42, label %if.end47

if.then42:                                        ; preds = %if.end39
  %59 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %60 = load i32, i32* %ry, align 4
  %sub43 = sub nsw i32 %60, 1
  %61 = load i32, i32* %rx, align 4
  %mul44 = mul nsw i32 %sub43, %61
  %62 = load i32, i32* %rx, align 4
  %sub45 = sub nsw i32 %62, 1
  %add46 = add nsw i32 %mul44, %sub45
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %59, i32 %add46, float 0.000000e+00)
  br label %if.end47

if.end47:                                         ; preds = %if.then42, %if.end39
  %63 = load i32, i32* %fixeds.addr, align 4
  %and48 = and i32 %63, 16
  %tobool49 = icmp ne i32 %and48, 0
  br i1 %tobool49, label %if.then50, label %if.end55

if.then50:                                        ; preds = %if.end47
  %64 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %65 = load i32, i32* %rx, align 4
  %mul51 = mul nsw i32 0, %65
  %66 = load i32, i32* %rx, align 4
  %sub52 = sub nsw i32 %66, 1
  %div53 = sdiv i32 %sub52, 2
  %add54 = add nsw i32 %mul51, %div53
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %64, i32 %add54, float 0.000000e+00)
  br label %if.end55

if.end55:                                         ; preds = %if.then50, %if.end47
  %67 = load i32, i32* %fixeds.addr, align 4
  %and56 = and i32 %67, 32
  %tobool57 = icmp ne i32 %and56, 0
  br i1 %tobool57, label %if.then58, label %if.end63

if.then58:                                        ; preds = %if.end55
  %68 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %69 = load i32, i32* %ry, align 4
  %sub59 = sub nsw i32 %69, 1
  %div60 = sdiv i32 %sub59, 2
  %70 = load i32, i32* %rx, align 4
  %mul61 = mul nsw i32 %div60, %70
  %add62 = add nsw i32 %mul61, 0
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %68, i32 %add62, float 0.000000e+00)
  br label %if.end63

if.end63:                                         ; preds = %if.then58, %if.end55
  %71 = load i32, i32* %fixeds.addr, align 4
  %and64 = and i32 %71, 64
  %tobool65 = icmp ne i32 %and64, 0
  br i1 %tobool65, label %if.then66, label %if.end72

if.then66:                                        ; preds = %if.end63
  %72 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %73 = load i32, i32* %ry, align 4
  %sub67 = sub nsw i32 %73, 1
  %div68 = sdiv i32 %sub67, 2
  %74 = load i32, i32* %rx, align 4
  %mul69 = mul nsw i32 %div68, %74
  %75 = load i32, i32* %rx, align 4
  %sub70 = sub nsw i32 %75, 1
  %add71 = add nsw i32 %mul69, %sub70
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %72, i32 %add71, float 0.000000e+00)
  br label %if.end72

if.end72:                                         ; preds = %if.then66, %if.end63
  %76 = load i32, i32* %fixeds.addr, align 4
  %and73 = and i32 %76, 128
  %tobool74 = icmp ne i32 %and73, 0
  br i1 %tobool74, label %if.then75, label %if.end81

if.then75:                                        ; preds = %if.end72
  %77 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %78 = load i32, i32* %ry, align 4
  %sub76 = sub nsw i32 %78, 1
  %79 = load i32, i32* %rx, align 4
  %mul77 = mul nsw i32 %sub76, %79
  %80 = load i32, i32* %rx, align 4
  %sub78 = sub nsw i32 %80, 1
  %div79 = sdiv i32 %sub78, 2
  %add80 = add nsw i32 %mul77, %div79
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %77, i32 %add80, float 0.000000e+00)
  br label %if.end81

if.end81:                                         ; preds = %if.then75, %if.end72
  %81 = load i32, i32* %fixeds.addr, align 4
  %and82 = and i32 %81, 256
  %tobool83 = icmp ne i32 %and82, 0
  br i1 %tobool83, label %if.then84, label %if.end91

if.then84:                                        ; preds = %if.end81
  %82 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %83 = load i32, i32* %ry, align 4
  %sub85 = sub nsw i32 %83, 1
  %div86 = sdiv i32 %sub85, 2
  %84 = load i32, i32* %rx, align 4
  %mul87 = mul nsw i32 %div86, %84
  %85 = load i32, i32* %rx, align 4
  %sub88 = sub nsw i32 %85, 1
  %div89 = sdiv i32 %sub88, 2
  %add90 = add nsw i32 %mul87, %div89
  call void @_ZN10btSoftBody7setMassEif(%class.btSoftBody* %82, i32 %add90, float 0.000000e+00)
  br label %if.end91

if.end91:                                         ; preds = %if.then84, %if.end81
  %86 = load %class.btVector3*, %class.btVector3** %x, align 4
  %isnull = icmp eq %class.btVector3* %86, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %if.end91
  %87 = bitcast %class.btVector3* %86 to i8*
  call void @_ZN9btVector3daEPv(i8* %87) #5
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %if.end91
  %88 = load float*, float** %m, align 4
  %isnull92 = icmp eq float* %88, null
  br i1 %isnull92, label %delete.end94, label %delete.notnull93

delete.notnull93:                                 ; preds = %delete.end
  %89 = bitcast float* %88 to i8*
  call void @_ZdaPv(i8* %89) #11
  br label %delete.end94

delete.end94:                                     ; preds = %delete.notnull93, %delete.end
  store i32 0, i32* %z, align 4
  store i32 0, i32* %iy, align 4
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc179, %delete.end94
  %90 = load i32, i32* %iy, align 4
  %91 = load i32, i32* %ry, align 4
  %cmp96 = icmp slt i32 %90, %91
  br i1 %cmp96, label %for.body97, label %for.end181

for.body97:                                       ; preds = %for.cond95
  store i32 0, i32* %ix98, align 4
  br label %for.cond99

for.cond99:                                       ; preds = %for.inc176, %for.body97
  %92 = load i32, i32* %ix98, align 4
  %93 = load i32, i32* %rx, align 4
  %cmp100 = icmp slt i32 %92, %93
  br i1 %cmp100, label %for.body101, label %for.end178

for.body101:                                      ; preds = %for.cond99
  %94 = load i32, i32* %ix98, align 4
  %add102 = add nsw i32 %94, 1
  %95 = load i32, i32* %rx, align 4
  %cmp103 = icmp slt i32 %add102, %95
  %frombool104 = zext i1 %cmp103 to i8
  store i8 %frombool104, i8* %mdx, align 1
  %96 = load i32, i32* %iy, align 4
  %add105 = add nsw i32 %96, 1
  %97 = load i32, i32* %ry, align 4
  %cmp106 = icmp slt i32 %add105, %97
  %frombool107 = zext i1 %cmp106 to i8
  store i8 %frombool107, i8* %mdy, align 1
  %98 = load i32, i32* %iy, align 4
  %99 = load i32, i32* %rx, align 4
  %mul108 = mul nsw i32 %98, %99
  %100 = load i32, i32* %ix98, align 4
  %add109 = add nsw i32 %mul108, %100
  store i32 %add109, i32* %node00, align 4
  %101 = load i32, i32* %iy, align 4
  %102 = load i32, i32* %rx, align 4
  %mul110 = mul nsw i32 %101, %102
  %103 = load i32, i32* %ix98, align 4
  %add111 = add nsw i32 %103, 1
  %add112 = add nsw i32 %mul110, %add111
  store i32 %add112, i32* %node01, align 4
  %104 = load i32, i32* %iy, align 4
  %add113 = add nsw i32 %104, 1
  %105 = load i32, i32* %rx, align 4
  %mul114 = mul nsw i32 %add113, %105
  %106 = load i32, i32* %ix98, align 4
  %add115 = add nsw i32 %mul114, %106
  store i32 %add115, i32* %node10, align 4
  %107 = load i32, i32* %iy, align 4
  %add116 = add nsw i32 %107, 1
  %108 = load i32, i32* %rx, align 4
  %mul117 = mul nsw i32 %add116, %108
  %109 = load i32, i32* %ix98, align 4
  %add118 = add nsw i32 %109, 1
  %add119 = add nsw i32 %mul117, %add118
  store i32 %add119, i32* %node11, align 4
  %110 = load i8, i8* %mdx, align 1
  %tobool120 = trunc i8 %110 to i1
  br i1 %tobool120, label %if.then121, label %if.end122

if.then121:                                       ; preds = %for.body101
  %111 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %112 = load i32, i32* %node00, align 4
  %113 = load i32, i32* %node01, align 4
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %111, i32 %112, i32 %113, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end122

if.end122:                                        ; preds = %if.then121, %for.body101
  %114 = load i8, i8* %mdy, align 1
  %tobool123 = trunc i8 %114 to i1
  br i1 %tobool123, label %if.then124, label %if.end125

if.then124:                                       ; preds = %if.end122
  %115 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %116 = load i32, i32* %node00, align 4
  %117 = load i32, i32* %node10, align 4
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %115, i32 %116, i32 %117, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end125

if.end125:                                        ; preds = %if.then124, %if.end122
  %118 = load i8, i8* %mdx, align 1
  %tobool126 = trunc i8 %118 to i1
  br i1 %tobool126, label %land.lhs.true, label %if.end175

land.lhs.true:                                    ; preds = %if.end125
  %119 = load i8, i8* %mdy, align 1
  %tobool127 = trunc i8 %119 to i1
  br i1 %tobool127, label %if.then128, label %if.end175

if.then128:                                       ; preds = %land.lhs.true
  %120 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %121 = load i32, i32* %node00, align 4
  %122 = load i32, i32* %node10, align 4
  %123 = load i32, i32* %node11, align 4
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %120, i32 %121, i32 %122, i32 %123, %"struct.btSoftBody::Material"* null)
  %124 = load float*, float** %tex_coords.addr, align 4
  %tobool129 = icmp ne float* %124, null
  br i1 %tobool129, label %if.then130, label %if.end149

if.then130:                                       ; preds = %if.then128
  %125 = load i32, i32* %resx.addr, align 4
  %126 = load i32, i32* %resy.addr, align 4
  %127 = load i32, i32* %ix98, align 4
  %128 = load i32, i32* %iy, align 4
  %call131 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %125, i32 %126, i32 %127, i32 %128, i32 0)
  %129 = load float*, float** %tex_coords.addr, align 4
  %130 = load i32, i32* %z, align 4
  %add132 = add nsw i32 %130, 0
  %arrayidx133 = getelementptr inbounds float, float* %129, i32 %add132
  store float %call131, float* %arrayidx133, align 4
  %131 = load i32, i32* %resx.addr, align 4
  %132 = load i32, i32* %resy.addr, align 4
  %133 = load i32, i32* %ix98, align 4
  %134 = load i32, i32* %iy, align 4
  %call134 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %131, i32 %132, i32 %133, i32 %134, i32 1)
  %135 = load float*, float** %tex_coords.addr, align 4
  %136 = load i32, i32* %z, align 4
  %add135 = add nsw i32 %136, 1
  %arrayidx136 = getelementptr inbounds float, float* %135, i32 %add135
  store float %call134, float* %arrayidx136, align 4
  %137 = load i32, i32* %resx.addr, align 4
  %138 = load i32, i32* %resy.addr, align 4
  %139 = load i32, i32* %ix98, align 4
  %140 = load i32, i32* %iy, align 4
  %call137 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %137, i32 %138, i32 %139, i32 %140, i32 0)
  %141 = load float*, float** %tex_coords.addr, align 4
  %142 = load i32, i32* %z, align 4
  %add138 = add nsw i32 %142, 2
  %arrayidx139 = getelementptr inbounds float, float* %141, i32 %add138
  store float %call137, float* %arrayidx139, align 4
  %143 = load i32, i32* %resx.addr, align 4
  %144 = load i32, i32* %resy.addr, align 4
  %145 = load i32, i32* %ix98, align 4
  %146 = load i32, i32* %iy, align 4
  %call140 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %143, i32 %144, i32 %145, i32 %146, i32 2)
  %147 = load float*, float** %tex_coords.addr, align 4
  %148 = load i32, i32* %z, align 4
  %add141 = add nsw i32 %148, 3
  %arrayidx142 = getelementptr inbounds float, float* %147, i32 %add141
  store float %call140, float* %arrayidx142, align 4
  %149 = load i32, i32* %resx.addr, align 4
  %150 = load i32, i32* %resy.addr, align 4
  %151 = load i32, i32* %ix98, align 4
  %152 = load i32, i32* %iy, align 4
  %call143 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %149, i32 %150, i32 %151, i32 %152, i32 3)
  %153 = load float*, float** %tex_coords.addr, align 4
  %154 = load i32, i32* %z, align 4
  %add144 = add nsw i32 %154, 4
  %arrayidx145 = getelementptr inbounds float, float* %153, i32 %add144
  store float %call143, float* %arrayidx145, align 4
  %155 = load i32, i32* %resx.addr, align 4
  %156 = load i32, i32* %resy.addr, align 4
  %157 = load i32, i32* %ix98, align 4
  %158 = load i32, i32* %iy, align 4
  %call146 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %155, i32 %156, i32 %157, i32 %158, i32 2)
  %159 = load float*, float** %tex_coords.addr, align 4
  %160 = load i32, i32* %z, align 4
  %add147 = add nsw i32 %160, 5
  %arrayidx148 = getelementptr inbounds float, float* %159, i32 %add147
  store float %call146, float* %arrayidx148, align 4
  br label %if.end149

if.end149:                                        ; preds = %if.then130, %if.then128
  %161 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %162 = load i32, i32* %node11, align 4
  %163 = load i32, i32* %node01, align 4
  %164 = load i32, i32* %node00, align 4
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %161, i32 %162, i32 %163, i32 %164, %"struct.btSoftBody::Material"* null)
  %165 = load float*, float** %tex_coords.addr, align 4
  %tobool150 = icmp ne float* %165, null
  br i1 %tobool150, label %if.then151, label %if.end170

if.then151:                                       ; preds = %if.end149
  %166 = load i32, i32* %resx.addr, align 4
  %167 = load i32, i32* %resy.addr, align 4
  %168 = load i32, i32* %ix98, align 4
  %169 = load i32, i32* %iy, align 4
  %call152 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %166, i32 %167, i32 %168, i32 %169, i32 3)
  %170 = load float*, float** %tex_coords.addr, align 4
  %171 = load i32, i32* %z, align 4
  %add153 = add nsw i32 %171, 6
  %arrayidx154 = getelementptr inbounds float, float* %170, i32 %add153
  store float %call152, float* %arrayidx154, align 4
  %172 = load i32, i32* %resx.addr, align 4
  %173 = load i32, i32* %resy.addr, align 4
  %174 = load i32, i32* %ix98, align 4
  %175 = load i32, i32* %iy, align 4
  %call155 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %172, i32 %173, i32 %174, i32 %175, i32 2)
  %176 = load float*, float** %tex_coords.addr, align 4
  %177 = load i32, i32* %z, align 4
  %add156 = add nsw i32 %177, 7
  %arrayidx157 = getelementptr inbounds float, float* %176, i32 %add156
  store float %call155, float* %arrayidx157, align 4
  %178 = load i32, i32* %resx.addr, align 4
  %179 = load i32, i32* %resy.addr, align 4
  %180 = load i32, i32* %ix98, align 4
  %181 = load i32, i32* %iy, align 4
  %call158 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %178, i32 %179, i32 %180, i32 %181, i32 3)
  %182 = load float*, float** %tex_coords.addr, align 4
  %183 = load i32, i32* %z, align 4
  %add159 = add nsw i32 %183, 8
  %arrayidx160 = getelementptr inbounds float, float* %182, i32 %add159
  store float %call158, float* %arrayidx160, align 4
  %184 = load i32, i32* %resx.addr, align 4
  %185 = load i32, i32* %resy.addr, align 4
  %186 = load i32, i32* %ix98, align 4
  %187 = load i32, i32* %iy, align 4
  %call161 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %184, i32 %185, i32 %186, i32 %187, i32 1)
  %188 = load float*, float** %tex_coords.addr, align 4
  %189 = load i32, i32* %z, align 4
  %add162 = add nsw i32 %189, 9
  %arrayidx163 = getelementptr inbounds float, float* %188, i32 %add162
  store float %call161, float* %arrayidx163, align 4
  %190 = load i32, i32* %resx.addr, align 4
  %191 = load i32, i32* %resy.addr, align 4
  %192 = load i32, i32* %ix98, align 4
  %193 = load i32, i32* %iy, align 4
  %call164 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %190, i32 %191, i32 %192, i32 %193, i32 0)
  %194 = load float*, float** %tex_coords.addr, align 4
  %195 = load i32, i32* %z, align 4
  %add165 = add nsw i32 %195, 10
  %arrayidx166 = getelementptr inbounds float, float* %194, i32 %add165
  store float %call164, float* %arrayidx166, align 4
  %196 = load i32, i32* %resx.addr, align 4
  %197 = load i32, i32* %resy.addr, align 4
  %198 = load i32, i32* %ix98, align 4
  %199 = load i32, i32* %iy, align 4
  %call167 = call float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %196, i32 %197, i32 %198, i32 %199, i32 1)
  %200 = load float*, float** %tex_coords.addr, align 4
  %201 = load i32, i32* %z, align 4
  %add168 = add nsw i32 %201, 11
  %arrayidx169 = getelementptr inbounds float, float* %200, i32 %add168
  store float %call167, float* %arrayidx169, align 4
  br label %if.end170

if.end170:                                        ; preds = %if.then151, %if.end149
  %202 = load i8, i8* %gendiags.addr, align 1
  %tobool171 = trunc i8 %202 to i1
  br i1 %tobool171, label %if.then172, label %if.end173

if.then172:                                       ; preds = %if.end170
  %203 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %204 = load i32, i32* %node00, align 4
  %205 = load i32, i32* %node11, align 4
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %203, i32 %204, i32 %205, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end173

if.end173:                                        ; preds = %if.then172, %if.end170
  %206 = load i32, i32* %z, align 4
  %add174 = add nsw i32 %206, 12
  store i32 %add174, i32* %z, align 4
  br label %if.end175

if.end175:                                        ; preds = %if.end173, %land.lhs.true, %if.end125
  br label %for.inc176

for.inc176:                                       ; preds = %if.end175
  %207 = load i32, i32* %ix98, align 4
  %inc177 = add nsw i32 %207, 1
  store i32 %inc177, i32* %ix98, align 4
  br label %for.cond99

for.end178:                                       ; preds = %for.cond99
  br label %for.inc179

for.inc179:                                       ; preds = %for.end178
  %208 = load i32, i32* %iy, align 4
  %inc180 = add nsw i32 %208, 1
  store i32 %inc180, i32* %iy, align 4
  br label %for.cond95

for.end181:                                       ; preds = %for.cond95
  %209 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  store %class.btSoftBody* %209, %class.btSoftBody** %retval, align 4
  br label %return

return:                                           ; preds = %for.end181, %if.then
  %210 = load %class.btSoftBody*, %class.btSoftBody** %retval, align 4
  ret %class.btSoftBody* %210
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZN17btSoftBodyHelpers11CalculateUVEiiiii(i32 %resx, i32 %resy, i32 %ix, i32 %iy, i32 %id) #1 {
entry:
  %resx.addr = alloca i32, align 4
  %resy.addr = alloca i32, align 4
  %ix.addr = alloca i32, align 4
  %iy.addr = alloca i32, align 4
  %id.addr = alloca i32, align 4
  %tc = alloca float, align 4
  store i32 %resx, i32* %resx.addr, align 4
  store i32 %resy, i32* %resy.addr, align 4
  store i32 %ix, i32* %ix.addr, align 4
  store i32 %iy, i32* %iy.addr, align 4
  store i32 %id, i32* %id.addr, align 4
  store float 0.000000e+00, float* %tc, align 4
  %0 = load i32, i32* %id.addr, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %resx.addr, align 4
  %sub = sub nsw i32 %1, 1
  %conv = sitofp i32 %sub to float
  %div = fdiv float 1.000000e+00, %conv
  %2 = load i32, i32* %ix.addr, align 4
  %conv1 = sitofp i32 %2 to float
  %mul = fmul float %div, %conv1
  store float %mul, float* %tc, align 4
  br label %if.end32

if.else:                                          ; preds = %entry
  %3 = load i32, i32* %id.addr, align 4
  %cmp2 = icmp eq i32 %3, 1
  br i1 %cmp2, label %if.then3, label %if.else11

if.then3:                                         ; preds = %if.else
  %4 = load i32, i32* %resy.addr, align 4
  %sub4 = sub nsw i32 %4, 1
  %conv5 = sitofp i32 %sub4 to float
  %div6 = fdiv float 1.000000e+00, %conv5
  %5 = load i32, i32* %resy.addr, align 4
  %sub7 = sub nsw i32 %5, 1
  %6 = load i32, i32* %iy.addr, align 4
  %sub8 = sub nsw i32 %sub7, %6
  %conv9 = sitofp i32 %sub8 to float
  %mul10 = fmul float %div6, %conv9
  store float %mul10, float* %tc, align 4
  br label %if.end31

if.else11:                                        ; preds = %if.else
  %7 = load i32, i32* %id.addr, align 4
  %cmp12 = icmp eq i32 %7, 2
  br i1 %cmp12, label %if.then13, label %if.else22

if.then13:                                        ; preds = %if.else11
  %8 = load i32, i32* %resy.addr, align 4
  %sub14 = sub nsw i32 %8, 1
  %conv15 = sitofp i32 %sub14 to float
  %div16 = fdiv float 1.000000e+00, %conv15
  %9 = load i32, i32* %resy.addr, align 4
  %sub17 = sub nsw i32 %9, 1
  %10 = load i32, i32* %iy.addr, align 4
  %sub18 = sub nsw i32 %sub17, %10
  %sub19 = sub nsw i32 %sub18, 1
  %conv20 = sitofp i32 %sub19 to float
  %mul21 = fmul float %div16, %conv20
  store float %mul21, float* %tc, align 4
  br label %if.end30

if.else22:                                        ; preds = %if.else11
  %11 = load i32, i32* %id.addr, align 4
  %cmp23 = icmp eq i32 %11, 3
  br i1 %cmp23, label %if.then24, label %if.end

if.then24:                                        ; preds = %if.else22
  %12 = load i32, i32* %resx.addr, align 4
  %sub25 = sub nsw i32 %12, 1
  %conv26 = sitofp i32 %sub25 to float
  %div27 = fdiv float 1.000000e+00, %conv26
  %13 = load i32, i32* %ix.addr, align 4
  %add = add nsw i32 %13, 1
  %conv28 = sitofp i32 %add to float
  %mul29 = fmul float %div27, %conv28
  store float %mul29, float* %tc, align 4
  br label %if.end

if.end:                                           ; preds = %if.then24, %if.else22
  br label %if.end30

if.end30:                                         ; preds = %if.end, %if.then13
  br label %if.end31

if.end31:                                         ; preds = %if.end30, %if.then3
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %if.then
  %14 = load float, float* %tc, align 4
  ret float %14
}

; Function Attrs: noinline optnone
define hidden %class.btSoftBody* @_ZN17btSoftBodyHelpers15CreateEllipsoidER19btSoftBodyWorldInfoRK9btVector3S4_i(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %worldInfo, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %radius, i32 %res) #2 {
entry:
  %worldInfo.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %center.addr = alloca %class.btVector3*, align 4
  %radius.addr = alloca %class.btVector3*, align 4
  %res.addr = alloca i32, align 4
  %vtx = alloca %class.btAlignedObjectArray.8, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  store %struct.btSoftBodyWorldInfo* %worldInfo, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4
  store %class.btVector3* %center, %class.btVector3** %center.addr, align 4
  store %class.btVector3* %radius, %class.btVector3** %radius.addr, align 4
  store i32 %res, i32* %res.addr, align 4
  %call = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %vtx)
  %0 = load i32, i32* %res.addr, align 4
  %add = add nsw i32 3, %0
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %vtx, i32 %add, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vtx, i32 0)
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %vtx)
  call void @_ZZN17btSoftBodyHelpers15CreateEllipsoidER19btSoftBodyWorldInfoRK9btVector3S4_iEN10Hammersley8GenerateEPS2_i(%class.btVector3* %call2, i32 %call3)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %vtx)
  %cmp = icmp slt i32 %1, %call4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vtx, i32 %2)
  %3 = load %class.btVector3*, %class.btVector3** %radius.addr, align 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = load %class.btVector3*, %class.btVector3** %center.addr, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %5 = load i32, i32* %i, align 4
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vtx, i32 %5)
  %6 = bitcast %class.btVector3* %call8 to i8*
  %7 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vtx, i32 0)
  %call10 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %vtx)
  %call11 = call %class.btSoftBody* @_ZN17btSoftBodyHelpers20CreateFromConvexHullER19btSoftBodyWorldInfoPK9btVector3ib(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %9, %class.btVector3* %call9, i32 %call10, i1 zeroext true)
  %call12 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %vtx) #5
  ret %class.btSoftBody* %call11
}

; Function Attrs: noinline optnone
define internal void @_ZZN17btSoftBodyHelpers15CreateEllipsoidER19btSoftBodyWorldInfoRK9btVector3S4_iEN10Hammersley8GenerateEPS2_i(%class.btVector3* %x, i32 %n) #2 {
entry:
  %x.addr = alloca %class.btVector3*, align 4
  %n.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %p = alloca float, align 4
  %t = alloca float, align 4
  %j = alloca i32, align 4
  %w = alloca float, align 4
  %a = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc20, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %n.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end21

for.body:                                         ; preds = %for.cond
  store float 5.000000e-01, float* %p, align 4
  store float 0.000000e+00, float* %t, align 4
  %2 = load i32, i32* %i, align 4
  store i32 %2, i32* %j, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %3 = load i32, i32* %j, align 4
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %for.body2, label %for.end

for.body2:                                        ; preds = %for.cond1
  %4 = load i32, i32* %j, align 4
  %and = and i32 %4, 1
  %tobool3 = icmp ne i32 %and, 0
  br i1 %tobool3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body2
  %5 = load float, float* %p, align 4
  %6 = load float, float* %t, align 4
  %add = fadd float %6, %5
  store float %add, float* %t, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body2
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load float, float* %p, align 4
  %conv = fpext float %7 to double
  %mul = fmul double %conv, 5.000000e-01
  %conv4 = fptrunc double %mul to float
  store float %conv4, float* %p, align 4
  %8 = load i32, i32* %j, align 4
  %shr = ashr i32 %8, 1
  store i32 %shr, i32* %j, align 4
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %9 = load float, float* %t, align 4
  %mul5 = fmul float 2.000000e+00, %9
  %sub = fsub float %mul5, 1.000000e+00
  store float %sub, float* %w, align 4
  %10 = load i32, i32* %i, align 4
  %mul6 = mul nsw i32 2, %10
  %conv7 = sitofp i32 %mul6 to float
  %mul8 = fmul float %conv7, 0x400921FB60000000
  %add9 = fadd float 0x400921FB60000000, %mul8
  %11 = load i32, i32* %n.addr, align 4
  %conv10 = sitofp i32 %11 to float
  %div = fdiv float %add9, %conv10
  store float %div, float* %a, align 4
  %12 = load float, float* %w, align 4
  %13 = load float, float* %w, align 4
  %mul11 = fmul float %12, %13
  %sub12 = fsub float 1.000000e+00, %mul11
  %call = call float @_Z6btSqrtf(float %sub12)
  store float %call, float* %s, align 4
  %14 = load float, float* %s, align 4
  %15 = load float, float* %a, align 4
  %call14 = call float @_Z5btCosf(float %15)
  %mul15 = fmul float %14, %call14
  store float %mul15, float* %ref.tmp13, align 4
  %16 = load float, float* %s, align 4
  %17 = load float, float* %a, align 4
  %call17 = call float @_Z5btSinf(float %17)
  %mul18 = fmul float %16, %call17
  store float %mul18, float* %ref.tmp16, align 4
  %call19 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %w)
  %18 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %incdec.ptr = getelementptr inbounds %class.btVector3, %class.btVector3* %18, i32 1
  store %class.btVector3* %incdec.ptr, %class.btVector3** %x.addr, align 4
  %19 = bitcast %class.btVector3* %18 to i8*
  %20 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %for.inc20

for.inc20:                                        ; preds = %for.end
  %21 = load i32, i32* %i, align 4
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end21:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btSoftBody* @_ZN17btSoftBodyHelpers20CreateFromConvexHullER19btSoftBodyWorldInfoPK9btVector3ib(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %worldInfo, %class.btVector3* %vertices, i32 %nvertices, i1 zeroext %randomizeConstraints) #2 {
entry:
  %worldInfo.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %vertices.addr = alloca %class.btVector3*, align 4
  %nvertices.addr = alloca i32, align 4
  %randomizeConstraints.addr = alloca i8, align 1
  %hdsc = alloca %class.HullDesc, align 4
  %hres = alloca %class.HullResult, align 4
  %hlib = alloca %class.HullLibrary, align 4
  %psb = alloca %class.btSoftBody*, align 4
  %i = alloca i32, align 4
  %idx = alloca [3 x i32], align 4
  store %struct.btSoftBodyWorldInfo* %worldInfo, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4
  store %class.btVector3* %vertices, %class.btVector3** %vertices.addr, align 4
  store i32 %nvertices, i32* %nvertices.addr, align 4
  %frombool = zext i1 %randomizeConstraints to i8
  store i8 %frombool, i8* %randomizeConstraints.addr, align 1
  %0 = load i32, i32* %nvertices.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %call = call %class.HullDesc* @_ZN8HullDescC2E8HullFlagjPK9btVector3j(%class.HullDesc* %hdsc, i32 1, i32 %0, %class.btVector3* %1, i32 16)
  %call1 = call %class.HullResult* @_ZN10HullResultC2Ev(%class.HullResult* %hres)
  %call2 = call %class.HullLibrary* @_ZN11HullLibraryC2Ev(%class.HullLibrary* %hlib)
  %2 = load i32, i32* %nvertices.addr, align 4
  %mMaxVertices = getelementptr inbounds %class.HullDesc, %class.HullDesc* %hdsc, i32 0, i32 5
  store i32 %2, i32* %mMaxVertices, align 4
  %call3 = call i32 @_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult(%class.HullLibrary* %hlib, %class.HullDesc* nonnull align 4 dereferenceable(28) %hdsc, %class.HullResult* nonnull align 4 dereferenceable(56) %hres)
  %call4 = call i8* @_ZN17btCollisionObjectnwEm(i32 1252)
  %3 = bitcast i8* %call4 to %class.btSoftBody*
  %4 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4
  %mNumOutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %hres, i32 0, i32 1
  %5 = load i32, i32* %mNumOutputVertices, align 4
  %m_OutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %hres, i32 0, i32 2
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %m_OutputVertices, i32 0)
  %call6 = call %class.btSoftBody* @_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf(%class.btSoftBody* %3, %struct.btSoftBodyWorldInfo* %4, i32 %5, %class.btVector3* %call5, float* null)
  store %class.btSoftBody* %3, %class.btSoftBody** %psb, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4
  %mNumFaces = getelementptr inbounds %class.HullResult, %class.HullResult* %hres, i32 0, i32 3
  %7 = load i32, i32* %mNumFaces, align 4
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %arrayinit.begin = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %m_Indices = getelementptr inbounds %class.HullResult, %class.HullResult* %hres, i32 0, i32 5
  %8 = load i32, i32* %i, align 4
  %mul = mul nsw i32 %8, 3
  %add = add nsw i32 %mul, 0
  %call7 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.89* %m_Indices, i32 %add)
  %9 = load i32, i32* %call7, align 4
  store i32 %9, i32* %arrayinit.begin, align 4
  %arrayinit.element = getelementptr inbounds i32, i32* %arrayinit.begin, i32 1
  %m_Indices8 = getelementptr inbounds %class.HullResult, %class.HullResult* %hres, i32 0, i32 5
  %10 = load i32, i32* %i, align 4
  %mul9 = mul nsw i32 %10, 3
  %add10 = add nsw i32 %mul9, 1
  %call11 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.89* %m_Indices8, i32 %add10)
  %11 = load i32, i32* %call11, align 4
  store i32 %11, i32* %arrayinit.element, align 4
  %arrayinit.element12 = getelementptr inbounds i32, i32* %arrayinit.element, i32 1
  %m_Indices13 = getelementptr inbounds %class.HullResult, %class.HullResult* %hres, i32 0, i32 5
  %12 = load i32, i32* %i, align 4
  %mul14 = mul nsw i32 %12, 3
  %add15 = add nsw i32 %mul14, 2
  %call16 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.89* %m_Indices13, i32 %add15)
  %13 = load i32, i32* %call16, align 4
  store i32 %13, i32* %arrayinit.element12, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %14 = load i32, i32* %arrayidx, align 4
  %arrayidx17 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 1
  %15 = load i32, i32* %arrayidx17, align 4
  %cmp18 = icmp slt i32 %14, %15
  br i1 %cmp18, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %16 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %arrayidx19 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %17 = load i32, i32* %arrayidx19, align 4
  %arrayidx20 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 1
  %18 = load i32, i32* %arrayidx20, align 4
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %16, i32 %17, i32 %18, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %arrayidx21 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 1
  %19 = load i32, i32* %arrayidx21, align 4
  %arrayidx22 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 2
  %20 = load i32, i32* %arrayidx22, align 4
  %cmp23 = icmp slt i32 %19, %20
  br i1 %cmp23, label %if.then24, label %if.end27

if.then24:                                        ; preds = %if.end
  %21 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %arrayidx25 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 1
  %22 = load i32, i32* %arrayidx25, align 4
  %arrayidx26 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 2
  %23 = load i32, i32* %arrayidx26, align 4
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %21, i32 %22, i32 %23, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end27

if.end27:                                         ; preds = %if.then24, %if.end
  %arrayidx28 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 2
  %24 = load i32, i32* %arrayidx28, align 4
  %arrayidx29 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %25 = load i32, i32* %arrayidx29, align 4
  %cmp30 = icmp slt i32 %24, %25
  br i1 %cmp30, label %if.then31, label %if.end34

if.then31:                                        ; preds = %if.end27
  %26 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %arrayidx32 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 2
  %27 = load i32, i32* %arrayidx32, align 4
  %arrayidx33 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %28 = load i32, i32* %arrayidx33, align 4
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %26, i32 %27, i32 %28, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end34

if.end34:                                         ; preds = %if.then31, %if.end27
  %29 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %arrayidx35 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %30 = load i32, i32* %arrayidx35, align 4
  %arrayidx36 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 1
  %31 = load i32, i32* %arrayidx36, align 4
  %arrayidx37 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 2
  %32 = load i32, i32* %arrayidx37, align 4
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %29, i32 %30, i32 %31, i32 %32, %"struct.btSoftBody::Material"* null)
  br label %for.inc

for.inc:                                          ; preds = %if.end34
  %33 = load i32, i32* %i, align 4
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call38 = call i32 @_ZN11HullLibrary13ReleaseResultER10HullResult(%class.HullLibrary* %hlib, %class.HullResult* nonnull align 4 dereferenceable(56) %hres)
  %34 = load i8, i8* %randomizeConstraints.addr, align 1
  %tobool = trunc i8 %34 to i1
  br i1 %tobool, label %if.then39, label %if.end40

if.then39:                                        ; preds = %for.end
  %35 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  call void @_ZN10btSoftBody20randomizeConstraintsEv(%class.btSoftBody* %35)
  br label %if.end40

if.end40:                                         ; preds = %if.then39, %for.end
  %36 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %call41 = call %class.HullLibrary* @_ZN11HullLibraryD2Ev(%class.HullLibrary* %hlib) #5
  %call42 = call %class.HullResult* @_ZN10HullResultD2Ev(%class.HullResult* %hres) #5
  ret %class.btSoftBody* %36
}

; Function Attrs: noinline optnone
define hidden %class.btSoftBody* @_ZN17btSoftBodyHelpers17CreateFromTriMeshER19btSoftBodyWorldInfoPKfPKiib(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %worldInfo, float* %vertices, i32* %triangles, i32 %ntriangles, i1 zeroext %randomizeConstraints) #2 {
entry:
  %worldInfo.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %vertices.addr = alloca float*, align 4
  %triangles.addr = alloca i32*, align 4
  %ntriangles.addr = alloca i32, align 4
  %randomizeConstraints.addr = alloca i8, align 1
  %maxidx = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %ni = alloca i32, align 4
  %chks = alloca %class.btAlignedObjectArray.77, align 4
  %vtx = alloca %class.btAlignedObjectArray.8, align 4
  %ref.tmp = alloca i8, align 1
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %psb = alloca %class.btSoftBody*, align 4
  %idx = alloca [3 x i32], align 4
  %j36 = alloca i32, align 4
  %k = alloca i32, align 4
  store %struct.btSoftBodyWorldInfo* %worldInfo, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4
  store float* %vertices, float** %vertices.addr, align 4
  store i32* %triangles, i32** %triangles.addr, align 4
  store i32 %ntriangles, i32* %ntriangles.addr, align 4
  %frombool = zext i1 %randomizeConstraints to i8
  store i8 %frombool, i8* %randomizeConstraints.addr, align 1
  store i32 0, i32* %maxidx, align 4
  store i32 0, i32* %i, align 4
  %0 = load i32, i32* %ntriangles.addr, align 4
  %mul = mul nsw i32 %0, 3
  store i32 %mul, i32* %ni, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %ni, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %triangles.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %call = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %arrayidx, i32* nonnull align 4 dereferenceable(4) %maxidx)
  %5 = load i32, i32* %call, align 4
  store i32 %5, i32* %maxidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = load i32, i32* %maxidx, align 4
  %inc1 = add nsw i32 %7, 1
  store i32 %inc1, i32* %maxidx, align 4
  %call2 = call %class.btAlignedObjectArray.77* @_ZN20btAlignedObjectArrayIbEC2Ev(%class.btAlignedObjectArray.77* %chks)
  %call3 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %vtx)
  %8 = load i32, i32* %maxidx, align 4
  %9 = load i32, i32* %maxidx, align 4
  %mul4 = mul nsw i32 %8, %9
  store i8 0, i8* %ref.tmp, align 1
  call void @_ZN20btAlignedObjectArrayIbE6resizeEiRKb(%class.btAlignedObjectArray.77* %chks, i32 %mul4, i8* nonnull align 1 dereferenceable(1) %ref.tmp)
  %10 = load i32, i32* %maxidx, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp5)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %vtx, i32 %10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  store i32 0, i32* %i, align 4
  store i32 0, i32* %j, align 4
  %11 = load i32, i32* %maxidx, align 4
  %mul7 = mul nsw i32 %11, 3
  store i32 %mul7, i32* %ni, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc18, %for.end
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %ni, align 4
  %cmp9 = icmp slt i32 %12, %13
  br i1 %cmp9, label %for.body10, label %for.end21

for.body10:                                       ; preds = %for.cond8
  %14 = load float*, float** %vertices.addr, align 4
  %15 = load i32, i32* %i, align 4
  %arrayidx12 = getelementptr inbounds float, float* %14, i32 %15
  %16 = load float*, float** %vertices.addr, align 4
  %17 = load i32, i32* %i, align 4
  %add = add nsw i32 %17, 1
  %arrayidx13 = getelementptr inbounds float, float* %16, i32 %add
  %18 = load float*, float** %vertices.addr, align 4
  %19 = load i32, i32* %i, align 4
  %add14 = add nsw i32 %19, 2
  %arrayidx15 = getelementptr inbounds float, float* %18, i32 %add14
  %call16 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp11, float* nonnull align 4 dereferenceable(4) %arrayidx12, float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %arrayidx15)
  %20 = load i32, i32* %j, align 4
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vtx, i32 %20)
  %21 = bitcast %class.btVector3* %call17 to i8*
  %22 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  br label %for.inc18

for.inc18:                                        ; preds = %for.body10
  %23 = load i32, i32* %j, align 4
  %inc19 = add nsw i32 %23, 1
  store i32 %inc19, i32* %j, align 4
  %24 = load i32, i32* %i, align 4
  %add20 = add nsw i32 %24, 3
  store i32 %add20, i32* %i, align 4
  br label %for.cond8

for.end21:                                        ; preds = %for.cond8
  %call22 = call i8* @_ZN17btCollisionObjectnwEm(i32 1252)
  %25 = bitcast i8* %call22 to %class.btSoftBody*
  %26 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4
  %call23 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %vtx)
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %vtx, i32 0)
  %call25 = call %class.btSoftBody* @_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf(%class.btSoftBody* %25, %struct.btSoftBodyWorldInfo* %26, i32 %call23, %class.btVector3* %call24, float* null)
  store %class.btSoftBody* %25, %class.btSoftBody** %psb, align 4
  store i32 0, i32* %i, align 4
  %27 = load i32, i32* %ntriangles.addr, align 4
  %mul26 = mul nsw i32 %27, 3
  store i32 %mul26, i32* %ni, align 4
  br label %for.cond27

for.cond27:                                       ; preds = %for.inc63, %for.end21
  %28 = load i32, i32* %i, align 4
  %29 = load i32, i32* %ni, align 4
  %cmp28 = icmp slt i32 %28, %29
  br i1 %cmp28, label %for.body29, label %for.end65

for.body29:                                       ; preds = %for.cond27
  %arrayinit.begin = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %30 = load i32*, i32** %triangles.addr, align 4
  %31 = load i32, i32* %i, align 4
  %arrayidx30 = getelementptr inbounds i32, i32* %30, i32 %31
  %32 = load i32, i32* %arrayidx30, align 4
  store i32 %32, i32* %arrayinit.begin, align 4
  %arrayinit.element = getelementptr inbounds i32, i32* %arrayinit.begin, i32 1
  %33 = load i32*, i32** %triangles.addr, align 4
  %34 = load i32, i32* %i, align 4
  %add31 = add nsw i32 %34, 1
  %arrayidx32 = getelementptr inbounds i32, i32* %33, i32 %add31
  %35 = load i32, i32* %arrayidx32, align 4
  store i32 %35, i32* %arrayinit.element, align 4
  %arrayinit.element33 = getelementptr inbounds i32, i32* %arrayinit.element, i32 1
  %36 = load i32*, i32** %triangles.addr, align 4
  %37 = load i32, i32* %i, align 4
  %add34 = add nsw i32 %37, 2
  %arrayidx35 = getelementptr inbounds i32, i32* %36, i32 %add34
  %38 = load i32, i32* %arrayidx35, align 4
  store i32 %38, i32* %arrayinit.element33, align 4
  store i32 2, i32* %j36, align 4
  store i32 0, i32* %k, align 4
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc57, %for.body29
  %39 = load i32, i32* %k, align 4
  %cmp38 = icmp slt i32 %39, 3
  br i1 %cmp38, label %for.body39, label %for.end59

for.body39:                                       ; preds = %for.cond37
  %40 = load i32, i32* %k, align 4
  %arrayidx40 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %40
  %41 = load i32, i32* %arrayidx40, align 4
  %42 = load i32, i32* %maxidx, align 4
  %mul41 = mul nsw i32 %41, %42
  %43 = load i32, i32* %j36, align 4
  %arrayidx42 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %43
  %44 = load i32, i32* %arrayidx42, align 4
  %add43 = add nsw i32 %mul41, %44
  %call44 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.77* %chks, i32 %add43)
  %45 = load i8, i8* %call44, align 1
  %tobool = trunc i8 %45 to i1
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body39
  %46 = load i32, i32* %k, align 4
  %arrayidx45 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %46
  %47 = load i32, i32* %arrayidx45, align 4
  %48 = load i32, i32* %maxidx, align 4
  %mul46 = mul nsw i32 %47, %48
  %49 = load i32, i32* %j36, align 4
  %arrayidx47 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %49
  %50 = load i32, i32* %arrayidx47, align 4
  %add48 = add nsw i32 %mul46, %50
  %call49 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.77* %chks, i32 %add48)
  store i8 1, i8* %call49, align 1
  %51 = load i32, i32* %j36, align 4
  %arrayidx50 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %51
  %52 = load i32, i32* %arrayidx50, align 4
  %53 = load i32, i32* %maxidx, align 4
  %mul51 = mul nsw i32 %52, %53
  %54 = load i32, i32* %k, align 4
  %arrayidx52 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %54
  %55 = load i32, i32* %arrayidx52, align 4
  %add53 = add nsw i32 %mul51, %55
  %call54 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.77* %chks, i32 %add53)
  store i8 1, i8* %call54, align 1
  %56 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %57 = load i32, i32* %j36, align 4
  %arrayidx55 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %57
  %58 = load i32, i32* %arrayidx55, align 4
  %59 = load i32, i32* %k, align 4
  %arrayidx56 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 %59
  %60 = load i32, i32* %arrayidx56, align 4
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %56, i32 %58, i32 %60, %"struct.btSoftBody::Material"* null, i1 zeroext false)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body39
  br label %for.inc57

for.inc57:                                        ; preds = %if.end
  %61 = load i32, i32* %k, align 4
  %inc58 = add nsw i32 %61, 1
  store i32 %inc58, i32* %k, align 4
  store i32 %61, i32* %j36, align 4
  br label %for.cond37

for.end59:                                        ; preds = %for.cond37
  %62 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %arrayidx60 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 0
  %63 = load i32, i32* %arrayidx60, align 4
  %arrayidx61 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 1
  %64 = load i32, i32* %arrayidx61, align 4
  %arrayidx62 = getelementptr inbounds [3 x i32], [3 x i32]* %idx, i32 0, i32 2
  %65 = load i32, i32* %arrayidx62, align 4
  call void @_ZN10btSoftBody10appendFaceEiiiPNS_8MaterialE(%class.btSoftBody* %62, i32 %63, i32 %64, i32 %65, %"struct.btSoftBody::Material"* null)
  br label %for.inc63

for.inc63:                                        ; preds = %for.end59
  %66 = load i32, i32* %i, align 4
  %add64 = add nsw i32 %66, 3
  store i32 %add64, i32* %i, align 4
  br label %for.cond27

for.end65:                                        ; preds = %for.cond27
  %67 = load i8, i8* %randomizeConstraints.addr, align 1
  %tobool66 = trunc i8 %67 to i1
  br i1 %tobool66, label %if.then67, label %if.end68

if.then67:                                        ; preds = %for.end65
  %68 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  call void @_ZN10btSoftBody20randomizeConstraintsEv(%class.btSoftBody* %68)
  br label %if.end68

if.end68:                                         ; preds = %if.then67, %for.end65
  %69 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %call69 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %vtx) #5
  %call70 = call %class.btAlignedObjectArray.77* @_ZN20btAlignedObjectArrayIbED2Ev(%class.btAlignedObjectArray.77* %chks) #5
  ret %class.btSoftBody* %69
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %a, i32* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca i32*, align 4
  %b.addr = alloca i32*, align 4
  store i32* %a, i32** %a.addr, align 4
  store i32* %b, i32** %b.addr, align 4
  %0 = load i32*, i32** %a.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %b.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp sgt i32 %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32*, i32** %a.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load i32*, i32** %b.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %4, %cond.true ], [ %5, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.77* @_ZN20btAlignedObjectArrayIbEC2Ev(%class.btAlignedObjectArray.77* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.78* @_ZN18btAlignedAllocatorIbLj16EEC2Ev(%class.btAlignedAllocator.78* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIbE4initEv(%class.btAlignedObjectArray.77* %this1)
  ret %class.btAlignedObjectArray.77* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE6resizeEiRKb(%class.btAlignedObjectArray.77* %this, i32 %newsize, i8* nonnull align 1 dereferenceable(1) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i8*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store i8* %fillData, i8** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.77* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  %5 = load i8*, i8** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIbE7reserveEi(%class.btAlignedObjectArray.77* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  %14 = load i8*, i8** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds i8, i8* %14, i32 %15
  %16 = load i8*, i8** %fillData.addr, align 4
  %17 = load i8, i8* %16, align 1
  %tobool = trunc i8 %17 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %arrayidx10, align 1
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %18 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %18, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %19 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 2
  store i32 %19, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.77* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  %0 = load i8*, i8** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 %1
  ret i8* %arrayidx
}

declare void @_ZN10btSoftBody20randomizeConstraintsEv(%class.btSoftBody*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.77* @_ZN20btAlignedObjectArrayIbED2Ev(%class.btAlignedObjectArray.77* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIbE5clearEv(%class.btAlignedObjectArray.77* %this1)
  ret %class.btAlignedObjectArray.77* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.HullDesc* @_ZN8HullDescC2E8HullFlagjPK9btVector3j(%class.HullDesc* returned %this, i32 %flag, i32 %vcount, %class.btVector3* %vertices, i32 %stride) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.HullDesc*, align 4
  %flag.addr = alloca i32, align 4
  %vcount.addr = alloca i32, align 4
  %vertices.addr = alloca %class.btVector3*, align 4
  %stride.addr = alloca i32, align 4
  store %class.HullDesc* %this, %class.HullDesc** %this.addr, align 4
  store i32 %flag, i32* %flag.addr, align 4
  store i32 %vcount, i32* %vcount.addr, align 4
  store %class.btVector3* %vertices, %class.btVector3** %vertices.addr, align 4
  store i32 %stride, i32* %stride.addr, align 4
  %this1 = load %class.HullDesc*, %class.HullDesc** %this.addr, align 4
  %0 = load i32, i32* %flag.addr, align 4
  %mFlags = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 0
  store i32 %0, i32* %mFlags, align 4
  %1 = load i32, i32* %vcount.addr, align 4
  %mVcount = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 1
  store i32 %1, i32* %mVcount, align 4
  %2 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %mVertices = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 2
  store %class.btVector3* %2, %class.btVector3** %mVertices, align 4
  %3 = load i32, i32* %stride.addr, align 4
  %mVertexStride = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 3
  store i32 %3, i32* %mVertexStride, align 4
  %mNormalEpsilon = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 4
  store float 0x3F50624DE0000000, float* %mNormalEpsilon, align 4
  %mMaxVertices = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 5
  store i32 4096, i32* %mMaxVertices, align 4
  ret %class.HullDesc* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.HullResult* @_ZN10HullResultC2Ev(%class.HullResult* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.HullResult*, align 4
  store %class.HullResult* %this, %class.HullResult** %this.addr, align 4
  %this1 = load %class.HullResult*, %class.HullResult** %this.addr, align 4
  %m_OutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 2
  %call = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %m_OutputVertices)
  %m_Indices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 5
  %call2 = call %class.btAlignedObjectArray.89* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.89* %m_Indices)
  %mPolygons = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 0
  store i8 1, i8* %mPolygons, align 4
  %mNumOutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 1
  store i32 0, i32* %mNumOutputVertices, align 4
  %mNumFaces = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 3
  store i32 0, i32* %mNumFaces, align 4
  %mNumIndices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 4
  store i32 0, i32* %mNumIndices, align 4
  ret %class.HullResult* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.HullLibrary* @_ZN11HullLibraryC2Ev(%class.HullLibrary* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.93* @_ZN20btAlignedObjectArrayIP14btHullTriangleEC2Ev(%class.btAlignedObjectArray.93* %m_tris)
  %m_vertexIndexMapping = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.81* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.81* %m_vertexIndexMapping)
  ret %class.HullLibrary* %this1
}

declare i32 @_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult(%class.HullLibrary*, %class.HullDesc* nonnull align 4 dereferenceable(28), %class.HullResult* nonnull align 4 dereferenceable(56)) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.89* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

declare i32 @_ZN11HullLibrary13ReleaseResultER10HullResult(%class.HullLibrary*, %class.HullResult* nonnull align 4 dereferenceable(56)) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.HullLibrary* @_ZN11HullLibraryD2Ev(%class.HullLibrary* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %m_vertexIndexMapping = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray.81* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.81* %m_vertexIndexMapping) #5
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call2 = call %class.btAlignedObjectArray.93* @_ZN20btAlignedObjectArrayIP14btHullTriangleED2Ev(%class.btAlignedObjectArray.93* %m_tris) #5
  ret %class.HullLibrary* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.HullResult* @_ZN10HullResultD2Ev(%class.HullResult* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.HullResult*, align 4
  store %class.HullResult* %this, %class.HullResult** %this.addr, align 4
  %this1 = load %class.HullResult*, %class.HullResult** %this.addr, align 4
  %m_Indices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray.89* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.89* %m_Indices) #5
  %m_OutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %m_OutputVertices) #5
  ret %class.HullResult* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btSoftBody* @_ZN17btSoftBodyHelpers20CreateFromTetGenDataER19btSoftBodyWorldInfoPKcS3_S3_bbb(%struct.btSoftBodyWorldInfo* nonnull align 4 dereferenceable(100) %worldInfo, i8* %ele, i8* %face, i8* %node, i1 zeroext %bfacelinks, i1 zeroext %btetralinks, i1 zeroext %bfacesfromtetras) #2 {
entry:
  %worldInfo.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %ele.addr = alloca i8*, align 4
  %face.addr = alloca i8*, align 4
  %node.addr = alloca i8*, align 4
  %bfacelinks.addr = alloca i8, align 1
  %btetralinks.addr = alloca i8, align 1
  %bfacesfromtetras.addr = alloca i8, align 1
  %pos = alloca %class.btAlignedObjectArray.8, align 4
  %nnode = alloca i32, align 4
  %ndims = alloca i32, align 4
  %nattrb = alloca i32, align 4
  %hasbounds = alloca i32, align 4
  %result = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %index = alloca i32, align 4
  %x = alloca float, align 4
  %y = alloca float, align 4
  %z = alloca float, align 4
  %psb = alloca %class.btSoftBody*, align 4
  %ntetra = alloca i32, align 4
  %ncorner = alloca i32, align 4
  %neattrb = alloca i32, align 4
  %i21 = alloca i32, align 4
  %index25 = alloca i32, align 4
  %ni = alloca [4 x i32], align 16
  store %struct.btSoftBodyWorldInfo* %worldInfo, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4
  store i8* %ele, i8** %ele.addr, align 4
  store i8* %face, i8** %face.addr, align 4
  store i8* %node, i8** %node.addr, align 4
  %frombool = zext i1 %bfacelinks to i8
  store i8 %frombool, i8* %bfacelinks.addr, align 1
  %frombool1 = zext i1 %btetralinks to i8
  store i8 %frombool1, i8* %btetralinks.addr, align 1
  %frombool2 = zext i1 %bfacesfromtetras to i8
  store i8 %frombool2, i8* %bfacesfromtetras.addr, align 1
  %call = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.8* %pos)
  store i32 0, i32* %nnode, align 4
  store i32 0, i32* %ndims, align 4
  store i32 0, i32* %nattrb, align 4
  store i32 0, i32* %hasbounds, align 4
  %0 = load i8*, i8** %node.addr, align 4
  %call3 = call i32 (i8*, i8*, ...) @sscanf(i8* %0, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32* %nnode, i32* %ndims, i32* %nattrb, i32* %hasbounds)
  store i32 %call3, i32* %result, align 4
  %1 = load i8*, i8** %node.addr, align 4
  %call4 = call i32 (i8*, i8*, ...) @sscanf(i8* %1, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.2, i32 0, i32 0), i32* %nnode, i32* %ndims, i32* %nattrb, i32* %hasbounds)
  store i32 %call4, i32* %result, align 4
  %2 = load i8*, i8** %node.addr, align 4
  %call5 = call i32 @_ZL8nextLinePKc(i8* %2)
  %3 = load i8*, i8** %node.addr, align 4
  %add.ptr = getelementptr inbounds i8, i8* %3, i32 %call5
  store i8* %add.ptr, i8** %node.addr, align 4
  %4 = load i32, i32* %nnode, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray.8* %pos, i32 %4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %i, align 4
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %pos)
  %cmp = icmp slt i32 %5, %call7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %index, align 4
  %6 = load i8*, i8** %node.addr, align 4
  %call8 = call i32 (i8*, i8*, ...) @sscanf(i8* %6, i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.3, i32 0, i32 0), i32* %index, float* %x, float* %y, float* %z)
  %7 = load i8*, i8** %node.addr, align 4
  %call9 = call i32 @_ZL8nextLinePKc(i8* %7)
  %8 = load i8*, i8** %node.addr, align 4
  %add.ptr10 = getelementptr inbounds i8, i8* %8, i32 %call9
  store i8* %add.ptr10, i8** %node.addr, align 4
  %9 = load i32, i32* %index, align 4
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %pos, i32 %9)
  %10 = load float, float* %x, align 4
  call void @_ZN9btVector34setXEf(%class.btVector3* %call11, float %10)
  %11 = load i32, i32* %index, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %pos, i32 %11)
  %12 = load float, float* %y, align 4
  call void @_ZN9btVector34setYEf(%class.btVector3* %call12, float %12)
  %13 = load i32, i32* %index, align 4
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %pos, i32 %13)
  %14 = load float, float* %z, align 4
  call void @_ZN9btVector34setZEf(%class.btVector3* %call13, float %14)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call14 = call i8* @_ZN17btCollisionObjectnwEm(i32 1252)
  %16 = bitcast i8* %call14 to %class.btSoftBody*
  %17 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %worldInfo.addr, align 4
  %18 = load i32, i32* %nnode, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.8* %pos, i32 0)
  %call16 = call %class.btSoftBody* @_ZN10btSoftBodyC1EP19btSoftBodyWorldInfoiPK9btVector3PKf(%class.btSoftBody* %16, %struct.btSoftBodyWorldInfo* %17, i32 %18, %class.btVector3* %call15, float* null)
  store %class.btSoftBody* %16, %class.btSoftBody** %psb, align 4
  %19 = load i8*, i8** %ele.addr, align 4
  %tobool = icmp ne i8* %19, null
  br i1 %tobool, label %land.lhs.true, label %if.end54

land.lhs.true:                                    ; preds = %for.end
  %20 = load i8*, i8** %ele.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %20, i32 0
  %21 = load i8, i8* %arrayidx, align 1
  %tobool17 = icmp ne i8 %21, 0
  br i1 %tobool17, label %if.then, label %if.end54

if.then:                                          ; preds = %land.lhs.true
  store i32 0, i32* %ntetra, align 4
  store i32 0, i32* %ncorner, align 4
  store i32 0, i32* %neattrb, align 4
  %22 = load i8*, i8** %ele.addr, align 4
  %call18 = call i32 (i8*, i8*, ...) @sscanf(i8* %22, i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.4, i32 0, i32 0), i32* %ntetra, i32* %ncorner, i32* %neattrb)
  %23 = load i8*, i8** %ele.addr, align 4
  %call19 = call i32 @_ZL8nextLinePKc(i8* %23)
  %24 = load i8*, i8** %ele.addr, align 4
  %add.ptr20 = getelementptr inbounds i8, i8* %24, i32 %call19
  store i8* %add.ptr20, i8** %ele.addr, align 4
  store i32 0, i32* %i21, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc51, %if.then
  %25 = load i32, i32* %i21, align 4
  %26 = load i32, i32* %ntetra, align 4
  %cmp23 = icmp slt i32 %25, %26
  br i1 %cmp23, label %for.body24, label %for.end53

for.body24:                                       ; preds = %for.cond22
  store i32 0, i32* %index25, align 4
  %27 = load i8*, i8** %ele.addr, align 4
  %arrayidx26 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 0
  %arrayidx27 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 1
  %arrayidx28 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 2
  %arrayidx29 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 3
  %call30 = call i32 (i8*, i8*, ...) @sscanf(i8* %27, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.5, i32 0, i32 0), i32* %index25, i32* %arrayidx26, i32* %arrayidx27, i32* %arrayidx28, i32* %arrayidx29)
  %28 = load i8*, i8** %ele.addr, align 4
  %call31 = call i32 @_ZL8nextLinePKc(i8* %28)
  %29 = load i8*, i8** %ele.addr, align 4
  %add.ptr32 = getelementptr inbounds i8, i8* %29, i32 %call31
  store i8* %add.ptr32, i8** %ele.addr, align 4
  %30 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %arrayidx33 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 0
  %31 = load i32, i32* %arrayidx33, align 16
  %arrayidx34 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 1
  %32 = load i32, i32* %arrayidx34, align 4
  %arrayidx35 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 2
  %33 = load i32, i32* %arrayidx35, align 8
  %arrayidx36 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 3
  %34 = load i32, i32* %arrayidx36, align 4
  call void @_ZN10btSoftBody11appendTetraEiiiiPNS_8MaterialE(%class.btSoftBody* %30, i32 %31, i32 %32, i32 %33, i32 %34, %"struct.btSoftBody::Material"* null)
  %35 = load i8, i8* %btetralinks.addr, align 1
  %tobool37 = trunc i8 %35 to i1
  br i1 %tobool37, label %if.then38, label %if.end

if.then38:                                        ; preds = %for.body24
  %36 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %arrayidx39 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 0
  %37 = load i32, i32* %arrayidx39, align 16
  %arrayidx40 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 1
  %38 = load i32, i32* %arrayidx40, align 4
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %36, i32 %37, i32 %38, %"struct.btSoftBody::Material"* null, i1 zeroext true)
  %39 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %arrayidx41 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 1
  %40 = load i32, i32* %arrayidx41, align 4
  %arrayidx42 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 2
  %41 = load i32, i32* %arrayidx42, align 8
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %39, i32 %40, i32 %41, %"struct.btSoftBody::Material"* null, i1 zeroext true)
  %42 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %arrayidx43 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 2
  %43 = load i32, i32* %arrayidx43, align 8
  %arrayidx44 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 0
  %44 = load i32, i32* %arrayidx44, align 16
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %42, i32 %43, i32 %44, %"struct.btSoftBody::Material"* null, i1 zeroext true)
  %45 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %arrayidx45 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 0
  %46 = load i32, i32* %arrayidx45, align 16
  %arrayidx46 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 3
  %47 = load i32, i32* %arrayidx46, align 4
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %45, i32 %46, i32 %47, %"struct.btSoftBody::Material"* null, i1 zeroext true)
  %48 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %arrayidx47 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 1
  %49 = load i32, i32* %arrayidx47, align 4
  %arrayidx48 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 3
  %50 = load i32, i32* %arrayidx48, align 4
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %48, i32 %49, i32 %50, %"struct.btSoftBody::Material"* null, i1 zeroext true)
  %51 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %arrayidx49 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 2
  %52 = load i32, i32* %arrayidx49, align 8
  %arrayidx50 = getelementptr inbounds [4 x i32], [4 x i32]* %ni, i32 0, i32 3
  %53 = load i32, i32* %arrayidx50, align 4
  call void @_ZN10btSoftBody10appendLinkEiiPNS_8MaterialEb(%class.btSoftBody* %51, i32 %52, i32 %53, %"struct.btSoftBody::Material"* null, i1 zeroext true)
  br label %if.end

if.end:                                           ; preds = %if.then38, %for.body24
  br label %for.inc51

for.inc51:                                        ; preds = %if.end
  %54 = load i32, i32* %i21, align 4
  %inc52 = add nsw i32 %54, 1
  store i32 %inc52, i32* %i21, align 4
  br label %for.cond22

for.end53:                                        ; preds = %for.cond22
  br label %if.end54

if.end54:                                         ; preds = %for.end53, %land.lhs.true, %for.end
  %55 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %m_nodes = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %55, i32 0, i32 9
  %call55 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.25* %m_nodes)
  %call56 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.6, i32 0, i32 0), i32 %call55)
  %56 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %m_links = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %56, i32 0, i32 10
  %call57 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4LinkEE4sizeEv(%class.btAlignedObjectArray.29* %m_links)
  %call58 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.7, i32 0, i32 0), i32 %call57)
  %57 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %m_faces = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %57, i32 0, i32 11
  %call59 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4FaceEE4sizeEv(%class.btAlignedObjectArray.33* %m_faces)
  %call60 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.8, i32 0, i32 0), i32 %call59)
  %58 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %m_tetras = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %58, i32 0, i32 12
  %call61 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody5TetraEE4sizeEv(%class.btAlignedObjectArray.37* %m_tetras)
  %call62 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str.9, i32 0, i32 0), i32 %call61)
  %59 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %call63 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.8* %pos) #5
  ret %class.btSoftBody* %59
}

declare i32 @sscanf(i8*, i8*, ...) #3

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZL8nextLinePKc(i8* %buffer) #1 {
entry:
  %buffer.addr = alloca i8*, align 4
  %numBytesRead = alloca i32, align 4
  store i8* %buffer, i8** %buffer.addr, align 4
  store i32 0, i32* %numBytesRead, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i8*, i8** %buffer.addr, align 4
  %1 = load i8, i8* %0, align 1
  %conv = sext i8 %1 to i32
  %cmp = icmp ne i32 %conv, 10
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i8*, i8** %buffer.addr, align 4
  %incdec.ptr = getelementptr inbounds i8, i8* %2, i32 1
  store i8* %incdec.ptr, i8** %buffer.addr, align 4
  %3 = load i32, i32* %numBytesRead, align 4
  %inc = add nsw i32 %3, 1
  store i32 %inc, i32* %numBytesRead, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %4 = load i8*, i8** %buffer.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 0
  %5 = load i8, i8* %arrayidx, align 1
  %conv1 = sext i8 %5 to i32
  %cmp2 = icmp eq i32 %conv1, 10
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  %6 = load i8*, i8** %buffer.addr, align 4
  %incdec.ptr3 = getelementptr inbounds i8, i8* %6, i32 1
  store i8* %incdec.ptr3, i8** %buffer.addr, align 4
  %7 = load i32, i32* %numBytesRead, align 4
  %inc4 = add nsw i32 %7, 1
  store i32 %inc4, i32* %numBytesRead, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end
  %8 = load i32, i32* %numBytesRead, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector34setXEf(%class.btVector3* %this, float %_x) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float %_x, float* %_x.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_x.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector34setYEf(%class.btVector3* %this, float %_y) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_y.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float %_y, float* %_y.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_y.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector34setZEf(%class.btVector3* %this, float %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_z.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float %_z, float* %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_z.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  store float %0, float* %arrayidx, align 4
  ret void
}

declare void @_ZN10btSoftBody11appendTetraEiiiiPNS_8MaterialE(%class.btSoftBody*, i32, i32, i32, i32, %"struct.btSoftBody::Material"*) #3

declare i32 @printf(i8*, ...) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEC2Ev(%class.btAlignedObjectArray.85* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.86* @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EEC2Ev(%class.btAlignedAllocator.86* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv(%class.btAlignedObjectArray.85* %this1)
  ret %class.btAlignedObjectArray.85* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.81* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.81* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.82* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.82* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.81* %this1)
  ret %class.btAlignedObjectArray.81* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.86* @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EEC2Ev(%class.btAlignedAllocator.86* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.86*, align 4
  store %class.btAlignedAllocator.86* %this, %class.btAlignedAllocator.86** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.86*, %class.btAlignedAllocator.86** %this.addr, align 4
  ret %class.btAlignedAllocator.86* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv(%class.btAlignedObjectArray.85* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  store %"class.btConvexHullComputer::Edge"* null, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.82* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.82* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.82*, align 4
  store %class.btAlignedAllocator.82* %this, %class.btAlignedAllocator.82** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.82*, %class.btAlignedAllocator.82** %this.addr, align 4
  ret %class.btAlignedAllocator.82* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.81* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

declare float @_ZN20btConvexHullComputer7computeEPKvbiiff(%class.btConvexHullComputer*, i8*, i1 zeroext, i32, i32, float, float) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge19getNextEdgeOfVertexEv(%"class.btConvexHullComputer::Edge"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %next = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 0
  %0 = load i32, i32* %next, align 4
  %add.ptr = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 %0
  ret %"class.btConvexHullComputer::Edge"* %add.ptr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.81* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.81* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.81* %this1)
  ret %class.btAlignedObjectArray.81* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.85* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEED2Ev(%class.btAlignedObjectArray.85* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv(%class.btAlignedObjectArray.85* %this1)
  ret %class.btAlignedObjectArray.85* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.81* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.81* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.81* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.81* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.81* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.81* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.81* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.81*, align 4
  store %class.btAlignedObjectArray.81* %this, %class.btAlignedObjectArray.81** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.81*, %class.btAlignedObjectArray.81** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.82* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.81* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.82* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.82*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.82* %this, %class.btAlignedAllocator.82** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.82*, %class.btAlignedAllocator.82** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv(%class.btAlignedObjectArray.85* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.85* %this1)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii(%class.btAlignedObjectArray.85* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv(%class.btAlignedObjectArray.85* %this1)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv(%class.btAlignedObjectArray.85* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii(%class.btAlignedObjectArray.85* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %3 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.85* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv(%class.btAlignedObjectArray.85* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.85*, align 4
  store %class.btAlignedObjectArray.85* %this, %class.btAlignedObjectArray.85** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.85*, %class.btAlignedObjectArray.85** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %tobool = icmp ne %"class.btConvexHullComputer::Edge"* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  %2 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_(%class.btAlignedAllocator.86* %m_allocator, %"class.btConvexHullComputer::Edge"* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.85, %class.btAlignedObjectArray.85* %this1, i32 0, i32 4
  store %"class.btConvexHullComputer::Edge"* null, %"class.btConvexHullComputer::Edge"** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_(%class.btAlignedAllocator.86* %this, %"class.btConvexHullComputer::Edge"* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.86*, align 4
  %ptr.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %class.btAlignedAllocator.86* %this, %class.btAlignedAllocator.86** %this.addr, align 4
  store %"class.btConvexHullComputer::Edge"* %ptr, %"class.btConvexHullComputer::Edge"** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.86*, %class.btAlignedAllocator.86** %this.addr, align 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %ptr.addr, align 4
  %1 = bitcast %"class.btConvexHullComputer::Edge"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransform11getIdentityEv() #2 comdat {
entry:
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN11btTransform11getIdentityEvE17identityTransform to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN11btTransform11getIdentityEvE17identityTransform) #5
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x311getIdentityEv()
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %call5 = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* @_ZZN11btTransform11getIdentityEvE17identityTransform, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  call void @__cxa_guard_release(i32* @_ZGVZN11btTransform11getIdentityEvE17identityTransform) #5
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  ret %class.btTransform* @_ZZN11btTransform11getIdentityEvE17identityTransform
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x311getIdentityEv() #2 comdat {
entry:
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix) #5
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 1.000000e+00, float* %ref.tmp4, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 1.000000e+00, float* %ref.tmp8, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* @_ZZN11btMatrix3x311getIdentityEvE14identityMatrix, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  call void @__cxa_guard_release(i32* @_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix) #5
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  ret %class.btMatrix3x3* @_ZZN11btMatrix3x311getIdentityEvE14identityMatrix
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this1)
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btDbvtAabbMm6CenterEv(%class.btVector3* noalias sret align 4 %agg.result, %struct.btDbvtAabbMm* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %mx)
  store float 2.000000e+00, float* %ref.tmp2, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btDbvtAabbMm7ExtentsEv(%class.btVector3* noalias sret align 4 %agg.result, %struct.btDbvtAabbMm* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %mx, %class.btVector3* nonnull align 4 dereferenceable(16) %mi)
  store float 2.000000e+00, float* %ref.tmp2, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZL7drawBoxP12btIDebugDrawRK9btVector3S3_S3_(%class.btIDebugDraw* %idraw, %class.btVector3* nonnull align 4 dereferenceable(16) %mins, %class.btVector3* nonnull align 4 dereferenceable(16) %maxs, %class.btVector3* nonnull align 4 dereferenceable(16) %color) #2 {
entry:
  %idraw.addr = alloca %class.btIDebugDraw*, align 4
  %mins.addr = alloca %class.btVector3*, align 4
  %maxs.addr = alloca %class.btVector3*, align 4
  %color.addr = alloca %class.btVector3*, align 4
  %c = alloca [8 x %class.btVector3], align 16
  store %class.btIDebugDraw* %idraw, %class.btIDebugDraw** %idraw.addr, align 4
  store %class.btVector3* %mins, %class.btVector3** %mins.addr, align 4
  store %class.btVector3* %maxs, %class.btVector3** %maxs.addr, align 4
  store %class.btVector3* %color, %class.btVector3** %color.addr, align 4
  %arrayinit.begin = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %0)
  %1 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %1)
  %2 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %2)
  %call3 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.begin, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call1, float* nonnull align 4 dereferenceable(4) %call2)
  %arrayinit.element = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin, i32 1
  %3 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %3)
  %4 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %5)
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call5, float* nonnull align 4 dereferenceable(4) %call6)
  %arrayinit.element8 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element, i32 1
  %6 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %6)
  %7 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %7)
  %8 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %8)
  %call12 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element8, float* nonnull align 4 dereferenceable(4) %call9, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call11)
  %arrayinit.element13 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element8, i32 1
  %9 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %9)
  %10 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %10)
  %11 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %11)
  %call17 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element13, float* nonnull align 4 dereferenceable(4) %call14, float* nonnull align 4 dereferenceable(4) %call15, float* nonnull align 4 dereferenceable(4) %call16)
  %arrayinit.element18 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element13, i32 1
  %12 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %12)
  %13 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %13)
  %14 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %14)
  %call22 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element18, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call20, float* nonnull align 4 dereferenceable(4) %call21)
  %arrayinit.element23 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element18, i32 1
  %15 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %15)
  %16 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %16)
  %17 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %17)
  %call27 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element23, float* nonnull align 4 dereferenceable(4) %call24, float* nonnull align 4 dereferenceable(4) %call25, float* nonnull align 4 dereferenceable(4) %call26)
  %arrayinit.element28 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element23, i32 1
  %18 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %18)
  %19 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %19)
  %20 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %20)
  %call32 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element28, float* nonnull align 4 dereferenceable(4) %call29, float* nonnull align 4 dereferenceable(4) %call30, float* nonnull align 4 dereferenceable(4) %call31)
  %arrayinit.element33 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element28, i32 1
  %21 = load %class.btVector3*, %class.btVector3** %mins.addr, align 4
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %21)
  %22 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %22)
  %23 = load %class.btVector3*, %class.btVector3** %maxs.addr, align 4
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %23)
  %call37 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element33, float* nonnull align 4 dereferenceable(4) %call34, float* nonnull align 4 dereferenceable(4) %call35, float* nonnull align 4 dereferenceable(4) %call36)
  %24 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 0
  %arrayidx38 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 1
  %25 = load %class.btVector3*, %class.btVector3** %color.addr, align 4
  %26 = bitcast %class.btIDebugDraw* %24 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %26, align 4
  %vfn = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable, i64 4
  %27 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %27(%class.btIDebugDraw* %24, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx38, %class.btVector3* nonnull align 4 dereferenceable(16) %25)
  %28 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx39 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 1
  %arrayidx40 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 2
  %29 = load %class.btVector3*, %class.btVector3** %color.addr, align 4
  %30 = bitcast %class.btIDebugDraw* %28 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable41 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %30, align 4
  %vfn42 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable41, i64 4
  %31 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn42, align 4
  call void %31(%class.btIDebugDraw* %28, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx39, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx40, %class.btVector3* nonnull align 4 dereferenceable(16) %29)
  %32 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx43 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 2
  %arrayidx44 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 3
  %33 = load %class.btVector3*, %class.btVector3** %color.addr, align 4
  %34 = bitcast %class.btIDebugDraw* %32 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable45 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %34, align 4
  %vfn46 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable45, i64 4
  %35 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn46, align 4
  call void %35(%class.btIDebugDraw* %32, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx43, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx44, %class.btVector3* nonnull align 4 dereferenceable(16) %33)
  %36 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx47 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 3
  %arrayidx48 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 0
  %37 = load %class.btVector3*, %class.btVector3** %color.addr, align 4
  %38 = bitcast %class.btIDebugDraw* %36 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable49 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %38, align 4
  %vfn50 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable49, i64 4
  %39 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn50, align 4
  call void %39(%class.btIDebugDraw* %36, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx47, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx48, %class.btVector3* nonnull align 4 dereferenceable(16) %37)
  %40 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx51 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 4
  %arrayidx52 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 5
  %41 = load %class.btVector3*, %class.btVector3** %color.addr, align 4
  %42 = bitcast %class.btIDebugDraw* %40 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable53 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %42, align 4
  %vfn54 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable53, i64 4
  %43 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn54, align 4
  call void %43(%class.btIDebugDraw* %40, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx51, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx52, %class.btVector3* nonnull align 4 dereferenceable(16) %41)
  %44 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx55 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 5
  %arrayidx56 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 6
  %45 = load %class.btVector3*, %class.btVector3** %color.addr, align 4
  %46 = bitcast %class.btIDebugDraw* %44 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable57 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %46, align 4
  %vfn58 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable57, i64 4
  %47 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn58, align 4
  call void %47(%class.btIDebugDraw* %44, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx55, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx56, %class.btVector3* nonnull align 4 dereferenceable(16) %45)
  %48 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx59 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 6
  %arrayidx60 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 7
  %49 = load %class.btVector3*, %class.btVector3** %color.addr, align 4
  %50 = bitcast %class.btIDebugDraw* %48 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable61 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %50, align 4
  %vfn62 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable61, i64 4
  %51 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn62, align 4
  call void %51(%class.btIDebugDraw* %48, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx59, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx60, %class.btVector3* nonnull align 4 dereferenceable(16) %49)
  %52 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx63 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 7
  %arrayidx64 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 4
  %53 = load %class.btVector3*, %class.btVector3** %color.addr, align 4
  %54 = bitcast %class.btIDebugDraw* %52 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable65 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %54, align 4
  %vfn66 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable65, i64 4
  %55 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn66, align 4
  call void %55(%class.btIDebugDraw* %52, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx63, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx64, %class.btVector3* nonnull align 4 dereferenceable(16) %53)
  %56 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx67 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 0
  %arrayidx68 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 4
  %57 = load %class.btVector3*, %class.btVector3** %color.addr, align 4
  %58 = bitcast %class.btIDebugDraw* %56 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable69 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %58, align 4
  %vfn70 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable69, i64 4
  %59 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn70, align 4
  call void %59(%class.btIDebugDraw* %56, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx67, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx68, %class.btVector3* nonnull align 4 dereferenceable(16) %57)
  %60 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx71 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 1
  %arrayidx72 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 5
  %61 = load %class.btVector3*, %class.btVector3** %color.addr, align 4
  %62 = bitcast %class.btIDebugDraw* %60 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable73 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %62, align 4
  %vfn74 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable73, i64 4
  %63 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn74, align 4
  call void %63(%class.btIDebugDraw* %60, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx71, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx72, %class.btVector3* nonnull align 4 dereferenceable(16) %61)
  %64 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx75 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 2
  %arrayidx76 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 6
  %65 = load %class.btVector3*, %class.btVector3** %color.addr, align 4
  %66 = bitcast %class.btIDebugDraw* %64 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable77 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %66, align 4
  %vfn78 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable77, i64 4
  %67 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn78, align 4
  call void %67(%class.btIDebugDraw* %64, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx75, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx76, %class.btVector3* nonnull align 4 dereferenceable(16) %65)
  %68 = load %class.btIDebugDraw*, %class.btIDebugDraw** %idraw.addr, align 4
  %arrayidx79 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 3
  %arrayidx80 = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %c, i32 0, i32 7
  %69 = load %class.btVector3*, %class.btVector3** %color.addr, align 4
  %70 = bitcast %class.btIDebugDraw* %68 to void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)***
  %vtable81 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)**, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*** %70, align 4
  %vfn82 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vtable81, i64 4
  %71 = load void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)*, void (%class.btIDebugDraw*, %class.btVector3*, %class.btVector3*, %class.btVector3*)** %vfn82, align 4
  call void %71(%class.btIDebugDraw* %68, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx79, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx80, %class.btVector3* nonnull align 4 dereferenceable(16) %69)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %this1, i32 0, i32 2
  %childs = bitcast %union.anon.23* %0 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %cmp = icmp eq %struct.btDbvtNode* %1, null
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btSoftBody::Feature"* @_ZN10btSoftBody7FeatureC2Ev(%"struct.btSoftBody::Feature"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBody::Feature"*, align 4
  store %"struct.btSoftBody::Feature"* %this, %"struct.btSoftBody::Feature"** %this.addr, align 4
  %this1 = load %"struct.btSoftBody::Feature"*, %"struct.btSoftBody::Feature"** %this.addr, align 4
  %0 = bitcast %"struct.btSoftBody::Feature"* %this1 to %"struct.btSoftBody::Element"*
  %call = call %"struct.btSoftBody::Element"* @_ZN10btSoftBody7ElementC2Ev(%"struct.btSoftBody::Element"* %0)
  ret %"struct.btSoftBody::Feature"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSoftBody::Element"* @_ZN10btSoftBody7ElementC2Ev(%"struct.btSoftBody::Element"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSoftBody::Element"*, align 4
  store %"struct.btSoftBody::Element"* %this, %"struct.btSoftBody::Element"** %this.addr, align 4
  %this1 = load %"struct.btSoftBody::Element"*, %"struct.btSoftBody::Element"** %this.addr, align 4
  %m_tag = getelementptr inbounds %"struct.btSoftBody::Element", %"struct.btSoftBody::Element"* %this1, i32 0, i32 0
  store i8* null, i8** %m_tag, align 4
  ret %"struct.btSoftBody::Element"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34lerpERKS_RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %t) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %t.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %t, float** %t.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 0
  %3 = load float, float* %arrayidx5, align 4
  %sub = fsub float %2, %3
  %4 = load float*, float** %t.addr, align 4
  %5 = load float, float* %4, align 4
  %mul = fmul float %sub, %5
  %add = fadd float %0, %mul
  store float %add, float* %ref.tmp, align 4
  %m_floats7 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %6 = load float, float* %arrayidx8, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 1
  %8 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 1
  %9 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %8, %9
  %10 = load float*, float** %t.addr, align 4
  %11 = load float, float* %10, align 4
  %mul14 = fmul float %sub13, %11
  %add15 = fadd float %6, %mul14
  store float %add15, float* %ref.tmp6, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %12 = load float, float* %arrayidx18, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats19 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [4 x float], [4 x float]* %m_floats19, i32 0, i32 2
  %14 = load float, float* %arrayidx20, align 4
  %m_floats21 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 2
  %15 = load float, float* %arrayidx22, align 4
  %sub23 = fsub float %14, %15
  %16 = load float*, float** %t.addr, align 4
  %17 = load float, float* %16, align 4
  %mul24 = fmul float %sub23, %17
  %add25 = fadd float %12, %mul24
  store float %add25, float* %ref.tmp16, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp16)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btCosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btSinf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #7

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #7

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.89* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.89* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.90* @_ZN18btAlignedAllocatorIjLj16EEC2Ev(%class.btAlignedAllocator.90* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.89* %this1)
  ret %class.btAlignedObjectArray.89* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.90* @_ZN18btAlignedAllocatorIjLj16EEC2Ev(%class.btAlignedAllocator.90* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.90*, align 4
  store %class.btAlignedAllocator.90* %this, %class.btAlignedAllocator.90** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.90*, %class.btAlignedAllocator.90** %this.addr, align 4
  ret %class.btAlignedAllocator.90* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.89* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.93* @_ZN20btAlignedObjectArrayIP14btHullTriangleEC2Ev(%class.btAlignedObjectArray.93* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.94* @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EEC2Ev(%class.btAlignedAllocator.94* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv(%class.btAlignedObjectArray.93* %this1)
  ret %class.btAlignedObjectArray.93* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.94* @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EEC2Ev(%class.btAlignedAllocator.94* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.94*, align 4
  store %class.btAlignedAllocator.94* %this, %class.btAlignedAllocator.94** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.94*, %class.btAlignedAllocator.94** %this.addr, align 4
  ret %class.btAlignedAllocator.94* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv(%class.btAlignedObjectArray.93* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  store %class.btHullTriangle** null, %class.btHullTriangle*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.93* @_ZN20btAlignedObjectArrayIP14btHullTriangleED2Ev(%class.btAlignedObjectArray.93* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE5clearEv(%class.btAlignedObjectArray.93* %this1)
  ret %class.btAlignedObjectArray.93* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE5clearEv(%class.btAlignedObjectArray.93* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.93* %this1)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii(%class.btAlignedObjectArray.93* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv(%class.btAlignedObjectArray.93* %this1)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv(%class.btAlignedObjectArray.93* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii(%class.btAlignedObjectArray.93* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  %3 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btHullTriangle*, %class.btHullTriangle** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.93* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv(%class.btAlignedObjectArray.93* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.93*, align 4
  store %class.btAlignedObjectArray.93* %this, %class.btAlignedObjectArray.93** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.93*, %class.btAlignedObjectArray.93** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  %0 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data, align 4
  %tobool = icmp ne %class.btHullTriangle** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  %2 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_(%class.btAlignedAllocator.94* %m_allocator, %class.btHullTriangle** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.93, %class.btAlignedObjectArray.93* %this1, i32 0, i32 4
  store %class.btHullTriangle** null, %class.btHullTriangle*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_(%class.btAlignedAllocator.94* %this, %class.btHullTriangle** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.94*, align 4
  %ptr.addr = alloca %class.btHullTriangle**, align 4
  store %class.btAlignedAllocator.94* %this, %class.btAlignedAllocator.94** %this.addr, align 4
  store %class.btHullTriangle** %ptr, %class.btHullTriangle*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.94*, %class.btAlignedAllocator.94** %this.addr, align 4
  %0 = load %class.btHullTriangle**, %class.btHullTriangle*** %ptr.addr, align 4
  %1 = bitcast %class.btHullTriangle** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.89* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.89* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.89* %this1)
  ret %class.btAlignedObjectArray.89* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.89* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.89* %this1)
  call void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.89* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.89* %this1)
  call void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.89* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.89* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.89* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.89* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.89*, align 4
  store %class.btAlignedObjectArray.89* %this, %class.btAlignedObjectArray.89** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.89*, %class.btAlignedObjectArray.89** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj(%class.btAlignedAllocator.90* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.89, %class.btAlignedObjectArray.89* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj(%class.btAlignedAllocator.90* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.90*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.90* %this, %class.btAlignedAllocator.90** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.90*, %class.btAlignedAllocator.90** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.9* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  ret %class.btAlignedAllocator.9* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.8* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.9* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.9* %this, %class.btVector3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %2, %class.btVector3** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %3 = load %class.btVector3*, %class.btVector3** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, %class.btVector3* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %class.btVector3* %4, %class.btVector3** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.9* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, %class.btVector3* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %7 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %9 = bitcast %class.btVector3* %6 to i8*
  %10 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.9* %this, i32 %n, %class.btVector3** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.78* @_ZN18btAlignedAllocatorIbLj16EEC2Ev(%class.btAlignedAllocator.78* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.78*, align 4
  store %class.btAlignedAllocator.78* %this, %class.btAlignedAllocator.78** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.78*, %class.btAlignedAllocator.78** %this.addr, align 4
  ret %class.btAlignedAllocator.78* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE4initEv(%class.btAlignedObjectArray.77* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  store i8* null, i8** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE5clearEv(%class.btAlignedObjectArray.77* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.77* %this1)
  call void @_ZN20btAlignedObjectArrayIbE7destroyEii(%class.btAlignedObjectArray.77* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIbE10deallocateEv(%class.btAlignedObjectArray.77* %this1)
  call void @_ZN20btAlignedObjectArrayIbE4initEv(%class.btAlignedObjectArray.77* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE7destroyEii(%class.btAlignedObjectArray.77* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  %3 = load i8*, i8** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.77* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE10deallocateEv(%class.btAlignedObjectArray.77* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  %0 = load i8*, i8** %m_data, align 4
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  %2 = load i8*, i8** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIbLj16EE10deallocateEPb(%class.btAlignedAllocator.78* %m_allocator, i8* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  store i8* null, i8** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIbLj16EE10deallocateEPb(%class.btAlignedAllocator.78* %this, i8* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.78*, align 4
  %ptr.addr = alloca i8*, align 4
  store %class.btAlignedAllocator.78* %this, %class.btAlignedAllocator.78** %this.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.78*, %class.btAlignedAllocator.78** %this.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE7reserveEi(%class.btAlignedObjectArray.77* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i8*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIbE8capacityEv(%class.btAlignedObjectArray.77* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIbE8allocateEi(%class.btAlignedObjectArray.77* %this1, i32 %1)
  store i8* %call2, i8** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.77* %this1)
  %2 = load i8*, i8** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIbE4copyEiiPb(%class.btAlignedObjectArray.77* %this1, i32 0, i32 %call3, i8* %2)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.77* %this1)
  call void @_ZN20btAlignedObjectArrayIbE7destroyEii(%class.btAlignedObjectArray.77* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIbE10deallocateEv(%class.btAlignedObjectArray.77* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %3 = load i8*, i8** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  store i8* %3, i8** %m_data, align 4
  %4 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 3
  store i32 %4, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIbE8capacityEv(%class.btAlignedObjectArray.77* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIbE8allocateEi(%class.btAlignedObjectArray.77* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i8* @_ZN18btAlignedAllocatorIbLj16EE8allocateEiPPKb(%class.btAlignedAllocator.78* %m_allocator, i32 %1, i8** null)
  store i8* %call, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i8*, i8** %retval, align 4
  ret i8* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIbE4copyEiiPb(%class.btAlignedObjectArray.77* %this, i32 %start, i32 %end, i8* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.77*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.77* %this, %class.btAlignedObjectArray.77** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i8* %dest, i8** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.77*, %class.btAlignedObjectArray.77** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i8*, i8** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 %4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.77, %class.btAlignedObjectArray.77* %this1, i32 0, i32 4
  %5 = load i8*, i8** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx2, align 1
  %tobool = trunc i8 %7 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %arrayidx, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN18btAlignedAllocatorIbLj16EE8allocateEiPPKb(%class.btAlignedAllocator.78* %this, i32 %n, i8** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.78*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i8**, align 4
  store %class.btAlignedAllocator.78* %this, %class.btAlignedAllocator.78** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i8** %hint, i8*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.78*, %class.btAlignedAllocator.78** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 1, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  ret i8* %call
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btSoftBodyHelpers.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind }
attributes #6 = { argmemonly nounwind willreturn writeonly }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #10 = { builtin allocsize(0) }
attributes #11 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1048575}
