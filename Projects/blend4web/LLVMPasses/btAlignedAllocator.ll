; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/LinearMath/btAlignedAllocator.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/LinearMath/btAlignedAllocator.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%struct.btConvertPointerSizeT = type { %union.anon.0 }
%union.anon.0 = type { i8* }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_Z14btAlignPointerIcEPT_S1_m = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@gNumAlignedAllocs = hidden global i32 0, align 4
@gNumAlignedFree = hidden global i32 0, align 4
@gTotalBytesAlignedAllocs = hidden global i32 0, align 4
@_ZL17sAlignedAllocFunc = internal global i8* (i32, i32)* @_ZL21btAlignedAllocDefaultmi, align 4
@_ZL16sAlignedFreeFunc = internal global void (i8*)* @_ZL20btAlignedFreeDefaultPv, align 4
@_ZL10sAllocFunc = internal global i8* (i32)* @_ZL14btAllocDefaultm, align 4
@_ZL9sFreeFunc = internal global void (i8*)* @_ZL13btFreeDefaultPv, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btAlignedAllocator.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z30btAlignedAllocSetCustomAlignedPFPvmiEPFvS_E(i8* (i32, i32)* %allocFunc, void (i8*)* %freeFunc) #1 {
entry:
  %allocFunc.addr = alloca i8* (i32, i32)*, align 4
  %freeFunc.addr = alloca void (i8*)*, align 4
  store i8* (i32, i32)* %allocFunc, i8* (i32, i32)** %allocFunc.addr, align 4
  store void (i8*)* %freeFunc, void (i8*)** %freeFunc.addr, align 4
  %0 = load i8* (i32, i32)*, i8* (i32, i32)** %allocFunc.addr, align 4
  %tobool = icmp ne i8* (i32, i32)* %0, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i8* (i32, i32)*, i8* (i32, i32)** %allocFunc.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* (i32, i32)* [ %1, %cond.true ], [ @_ZL21btAlignedAllocDefaultmi, %cond.false ]
  store i8* (i32, i32)* %cond, i8* (i32, i32)** @_ZL17sAlignedAllocFunc, align 4
  %2 = load void (i8*)*, void (i8*)** %freeFunc.addr, align 4
  %tobool1 = icmp ne void (i8*)* %2, null
  br i1 %tobool1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.end
  %3 = load void (i8*)*, void (i8*)** %freeFunc.addr, align 4
  br label %cond.end4

cond.false3:                                      ; preds = %cond.end
  br label %cond.end4

cond.end4:                                        ; preds = %cond.false3, %cond.true2
  %cond5 = phi void (i8*)* [ %3, %cond.true2 ], [ @_ZL20btAlignedFreeDefaultPv, %cond.false3 ]
  store void (i8*)* %cond5, void (i8*)** @_ZL16sAlignedFreeFunc, align 4
  ret void
}

; Function Attrs: noinline optnone
define internal i8* @_ZL21btAlignedAllocDefaultmi(i32 %size, i32 %alignment) #2 {
entry:
  %size.addr = alloca i32, align 4
  %alignment.addr = alloca i32, align 4
  %ret = alloca i8*, align 4
  %real = alloca i8*, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 %alignment, i32* %alignment.addr, align 4
  %0 = load i8* (i32)*, i8* (i32)** @_ZL10sAllocFunc, align 4
  %1 = load i32, i32* %size.addr, align 4
  %add = add i32 %1, 4
  %2 = load i32, i32* %alignment.addr, align 4
  %sub = sub nsw i32 %2, 1
  %add1 = add i32 %add, %sub
  %call = call i8* %0(i32 %add1)
  store i8* %call, i8** %real, align 4
  %3 = load i8*, i8** %real, align 4
  %tobool = icmp ne i8* %3, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load i8*, i8** %real, align 4
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 4
  %5 = load i32, i32* %alignment.addr, align 4
  %call2 = call i8* @_Z14btAlignPointerIcEPT_S1_m(i8* %add.ptr, i32 %5)
  store i8* %call2, i8** %ret, align 4
  %6 = load i8*, i8** %real, align 4
  %7 = load i8*, i8** %ret, align 4
  %8 = bitcast i8* %7 to i8**
  %add.ptr3 = getelementptr inbounds i8*, i8** %8, i32 -1
  store i8* %6, i8** %add.ptr3, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %9 = load i8*, i8** %real, align 4
  store i8* %9, i8** %ret, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %10 = load i8*, i8** %ret, align 4
  ret i8* %10
}

; Function Attrs: noinline optnone
define internal void @_ZL20btAlignedFreeDefaultPv(i8* %ptr) #2 {
entry:
  %ptr.addr = alloca i8*, align 4
  %real = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %ptr.addr, align 4
  %2 = bitcast i8* %1 to i8**
  %add.ptr = getelementptr inbounds i8*, i8** %2, i32 -1
  %3 = load i8*, i8** %add.ptr, align 4
  store i8* %3, i8** %real, align 4
  %4 = load void (i8*)*, void (i8*)** @_ZL9sFreeFunc, align 4
  %5 = load i8*, i8** %real, align 4
  call void %4(i8* %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z23btAlignedAllocSetCustomPFPvmEPFvS_E(i8* (i32)* %allocFunc, void (i8*)* %freeFunc) #1 {
entry:
  %allocFunc.addr = alloca i8* (i32)*, align 4
  %freeFunc.addr = alloca void (i8*)*, align 4
  store i8* (i32)* %allocFunc, i8* (i32)** %allocFunc.addr, align 4
  store void (i8*)* %freeFunc, void (i8*)** %freeFunc.addr, align 4
  %0 = load i8* (i32)*, i8* (i32)** %allocFunc.addr, align 4
  %tobool = icmp ne i8* (i32)* %0, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i8* (i32)*, i8* (i32)** %allocFunc.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* (i32)* [ %1, %cond.true ], [ @_ZL14btAllocDefaultm, %cond.false ]
  store i8* (i32)* %cond, i8* (i32)** @_ZL10sAllocFunc, align 4
  %2 = load void (i8*)*, void (i8*)** %freeFunc.addr, align 4
  %tobool1 = icmp ne void (i8*)* %2, null
  br i1 %tobool1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.end
  %3 = load void (i8*)*, void (i8*)** %freeFunc.addr, align 4
  br label %cond.end4

cond.false3:                                      ; preds = %cond.end
  br label %cond.end4

cond.end4:                                        ; preds = %cond.false3, %cond.true2
  %cond5 = phi void (i8*)* [ %3, %cond.true2 ], [ @_ZL13btFreeDefaultPv, %cond.false3 ]
  store void (i8*)* %cond5, void (i8*)** @_ZL9sFreeFunc, align 4
  ret void
}

; Function Attrs: noinline optnone
define internal i8* @_ZL14btAllocDefaultm(i32 %size) #2 {
entry:
  %size.addr = alloca i32, align 4
  store i32 %size, i32* %size.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %call = call i8* @malloc(i32 %0)
  ret i8* %call
}

; Function Attrs: noinline optnone
define internal void @_ZL13btFreeDefaultPv(i8* %ptr) #2 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @free(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden i8* @_Z22btAlignedAllocInternalmi(i32 %size, i32 %alignment) #2 {
entry:
  %size.addr = alloca i32, align 4
  %alignment.addr = alloca i32, align 4
  %ptr = alloca i8*, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 %alignment, i32* %alignment.addr, align 4
  %0 = load i32, i32* @gNumAlignedAllocs, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @gNumAlignedAllocs, align 4
  %1 = load i8* (i32, i32)*, i8* (i32, i32)** @_ZL17sAlignedAllocFunc, align 4
  %2 = load i32, i32* %size.addr, align 4
  %3 = load i32, i32* %alignment.addr, align 4
  %call = call i8* %1(i32 %2, i32 %3)
  store i8* %call, i8** %ptr, align 4
  %4 = load i8*, i8** %ptr, align 4
  ret i8* %4
}

; Function Attrs: noinline optnone
define hidden void @_Z21btAlignedFreeInternalPv(i8* %ptr) #2 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* @gNumAlignedFree, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* @gNumAlignedFree, align 4
  %2 = load void (i8*)*, void (i8*)** @_ZL16sAlignedFreeFunc, align 4
  %3 = load i8*, i8** %ptr.addr, align 4
  call void %2(i8* %3)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_Z14btAlignPointerIcEPT_S1_m(i8* %unalignedPtr, i32 %alignment) #1 comdat {
entry:
  %unalignedPtr.addr = alloca i8*, align 4
  %alignment.addr = alloca i32, align 4
  %converter = alloca %struct.btConvertPointerSizeT, align 4
  %bit_mask = alloca i32, align 4
  store i8* %unalignedPtr, i8** %unalignedPtr.addr, align 4
  store i32 %alignment, i32* %alignment.addr, align 4
  %0 = load i32, i32* %alignment.addr, align 4
  %sub = sub i32 %0, 1
  %neg = xor i32 %sub, -1
  store i32 %neg, i32* %bit_mask, align 4
  %1 = load i8*, i8** %unalignedPtr.addr, align 4
  %2 = getelementptr inbounds %struct.btConvertPointerSizeT, %struct.btConvertPointerSizeT* %converter, i32 0, i32 0
  %ptr = bitcast %union.anon.0* %2 to i8**
  store i8* %1, i8** %ptr, align 4
  %3 = load i32, i32* %alignment.addr, align 4
  %sub1 = sub i32 %3, 1
  %4 = getelementptr inbounds %struct.btConvertPointerSizeT, %struct.btConvertPointerSizeT* %converter, i32 0, i32 0
  %integer = bitcast %union.anon.0* %4 to i32*
  %5 = load i32, i32* %integer, align 4
  %add = add i32 %5, %sub1
  store i32 %add, i32* %integer, align 4
  %6 = load i32, i32* %bit_mask, align 4
  %7 = getelementptr inbounds %struct.btConvertPointerSizeT, %struct.btConvertPointerSizeT* %converter, i32 0, i32 0
  %integer2 = bitcast %union.anon.0* %7 to i32*
  %8 = load i32, i32* %integer2, align 4
  %and = and i32 %8, %6
  store i32 %and, i32* %integer2, align 4
  %9 = getelementptr inbounds %struct.btConvertPointerSizeT, %struct.btConvertPointerSizeT* %converter, i32 0, i32 0
  %ptr3 = bitcast %union.anon.0* %9 to i8**
  %10 = load i8*, i8** %ptr3, align 4
  ret i8* %10
}

declare i8* @malloc(i32) #3

declare void @free(i8*) #3

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btAlignedAllocator.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
