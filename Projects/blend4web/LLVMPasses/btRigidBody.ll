; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Dynamics/btRigidBody.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Dynamics/btRigidBody.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.0, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.3, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.3 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%"struct.btRigidBody::btRigidBodyConstructionInfo" = type { float, %class.btMotionState*, %class.btTransform, %class.btCollisionShape*, %class.btVector3, float, float, float, float, float, float, float, float, i8, float, float, float, float }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btSerializer = type { i32 (...)** }
%struct.btRigidBodyFloatData = type { %struct.btCollisionObjectFloatData, %struct.btMatrix3x3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, i32 }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btVector3FloatData = type { [4 x float] }
%class.btChunk = type { i32, i32, i8*, i32, i32 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev = comdat any

$_ZN11btRigidBody27btRigidBodyConstructionInfoC2EfP13btMotionStateP16btCollisionShapeRK9btVector3 = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN9btVector37setZeroEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_ = comdat any

$_ZN11btRigidBody14getMotionStateEv = comdat any

$_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_ = comdat any

$_ZNK11btRigidBody17getCollisionShapeEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_Z9btClampedIfERKT_S2_S2_S2_ = comdat any

$_Z5btPowff = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZNK17btCollisionObject25isStaticOrKinematicObjectEv = comdat any

$_ZN11btRigidBody17applyCentralForceERK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x36scaledERK9btVector3 = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btRigidBody18getAngularVelocityEv = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_Z6btSqrtf = comdat any

$_ZNK11btTransform11getRotationEv = comdat any

$_Z10quatRotateRK12btQuaternionRK9btVector3 = comdat any

$_ZNK12btQuaternion7inverseEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_ = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZplRK11btMatrix3x3S1_ = comdat any

$_ZmlRK11btMatrix3x3RKf = comdat any

$_ZmiRK11btMatrix3x3S1_ = comdat any

$_ZNK11btMatrix3x37solve33ERK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_Z12evalEulerEqnRK9btVector3S1_S1_fRK11btMatrix3x3 = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_Z17evalEulerEqnDerivRK9btVector3S1_fRK11btMatrix3x3 = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZNK11btMatrix3x311getRotationER12btQuaternion = comdat any

$_ZNK17btCollisionObject17isKinematicObjectEv = comdat any

$_ZNK11btRigidBody17getLinearVelocityEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_ = comdat any

$_ZN17btTypedConstraint13getRigidBodyAEv = comdat any

$_ZN17btTypedConstraint13getRigidBodyBEv = comdat any

$_ZN17btCollisionObject23setIgnoreCollisionCheckEPKS_b = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE6removeERKS1_ = comdat any

$_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

$_ZN11btRigidBodyD2Ev = comdat any

$_ZN11btRigidBodyD0Ev = comdat any

$_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape = comdat any

$_ZNK17btCollisionObject24checkCollideWithOverrideEPKS_ = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_Z5btSinf = comdat any

$_Z5btCosf = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZmlRK12btQuaternionS1_ = comdat any

$_ZN12btQuaternion9normalizeEv = comdat any

$_ZN11btTransform11setRotationERK12btQuaternion = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK12btQuaternion6lengthEv = comdat any

$_ZN12btQuaterniondVERKf = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN12btQuaternionmLERKf = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf = comdat any

$_ZNK11btMatrix3x37inverseEv = comdat any

$_ZNK12btQuaternion8getAngleEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZNK11btMatrix3x35cofacEiiii = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_Z6btAcosf = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZmlRK12btQuaternionRK9btVector3 = comdat any

$_ZN12btQuaternionmLERKS_ = comdat any

$_ZNK10btQuadWord4getXEv = comdat any

$_ZNK10btQuadWord4getYEv = comdat any

$_ZNK10btQuadWord4getZEv = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZNK11btMatrix3x39getColumnEi = comdat any

$_Z5btDotRK9btVector3S1_ = comdat any

$_Z7btCrossRK9btVector3S1_ = comdat any

$_Z6btFabsf = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectE9push_backERKS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectE6removeERKS2_ = comdat any

$_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4copyEiiPS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPK17btCollisionObjectLj16EE8allocateEiPPKS2_ = comdat any

$_ZN18btAlignedAllocatorIPK17btCollisionObjectLj16EE10deallocateEPS2_ = comdat any

$_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE16findLinearSearchERKS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectE13removeAtIndexEi = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectE8pop_backEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_ = comdat any

$_ZN17btCollisionObjectdlEPv = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE13removeAtIndexEi = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE8pop_backEv = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@gDeactivationTime = hidden global float 2.000000e+00, align 4
@gDisableDeactivation = hidden global i8 0, align 1
@_ZTV11btRigidBody = hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI11btRigidBody to i8*), i8* bitcast (%class.btRigidBody* (%class.btRigidBody*)* @_ZN11btRigidBodyD2Ev to i8*), i8* bitcast (void (%class.btRigidBody*)* @_ZN11btRigidBodyD0Ev to i8*), i8* bitcast (void (%class.btCollisionObject*, %class.btCollisionShape*)* @_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape to i8*), i8* bitcast (i1 (%class.btCollisionObject*, %class.btCollisionObject*)* @_ZNK17btCollisionObject24checkCollideWithOverrideEPKS_ to i8*), i8* bitcast (i32 (%class.btRigidBody*)* @_ZNK11btRigidBody28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btRigidBody*, i8*, %class.btSerializer*)* @_ZNK11btRigidBody9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btRigidBody*, %class.btSerializer*)* @_ZNK11btRigidBody21serializeSingleObjectEP12btSerializer to i8*)] }, align 4
@_ZL8uniqueId = internal global i32 0, align 4
@.str = private unnamed_addr constant [21 x i8] c"btRigidBodyFloatData\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS11btRigidBody = hidden constant [14 x i8] c"11btRigidBody\00", align 1
@_ZTI17btCollisionObject = external constant i8*
@_ZTI11btRigidBody = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([14 x i8], [14 x i8]* @_ZTS11btRigidBody, i32 0, i32 0), i8* bitcast (i8** @_ZTI17btCollisionObject to i8*) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btRigidBody.cpp, i8* null }]

@_ZN11btRigidBodyC1ERKNS_27btRigidBodyConstructionInfoE = hidden unnamed_addr alias %class.btRigidBody* (%class.btRigidBody*, %"struct.btRigidBody::btRigidBodyConstructionInfo"*), %class.btRigidBody* (%class.btRigidBody*, %"struct.btRigidBody::btRigidBodyConstructionInfo"*)* @_ZN11btRigidBodyC2ERKNS_27btRigidBodyConstructionInfoE
@_ZN11btRigidBodyC1EfP13btMotionStateP16btCollisionShapeRK9btVector3 = hidden unnamed_addr alias %class.btRigidBody* (%class.btRigidBody*, float, %class.btMotionState*, %class.btCollisionShape*, %class.btVector3*), %class.btRigidBody* (%class.btRigidBody*, float, %class.btMotionState*, %class.btCollisionShape*, %class.btVector3*)* @_ZN11btRigidBodyC2EfP13btMotionStateP16btCollisionShapeRK9btVector3

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btRigidBody* @_ZN11btRigidBodyC2ERKNS_27btRigidBodyConstructionInfoE(%class.btRigidBody* returned %this, %"struct.btRigidBody::btRigidBodyConstructionInfo"* nonnull align 4 dereferenceable(144) %constructionInfo) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %constructionInfo.addr = alloca %"struct.btRigidBody::btRigidBodyConstructionInfo"*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %"struct.btRigidBody::btRigidBodyConstructionInfo"* %constructionInfo, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call = call %class.btCollisionObject* @_ZN17btCollisionObjectC2Ev(%class.btCollisionObject* %0)
  %1 = bitcast %class.btRigidBody* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV11btRigidBody, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  %call2 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_invInertiaTensorWorld)
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearVelocity)
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularVelocity)
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearFactor)
  %m_gravity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 6
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_gravity)
  %m_gravity_acceleration = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 7
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_gravity_acceleration)
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_invInertiaLocal)
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_totalForce)
  %m_totalTorque = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 10
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_totalTorque)
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call11 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray.0* %m_constraintRefs)
  %m_deltaLinearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 24
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_deltaLinearVelocity)
  %m_deltaAngularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 25
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_deltaAngularVelocity)
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularFactor)
  %m_invMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 27
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_invMass)
  %m_pushVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 28
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_pushVelocity)
  %m_turnVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 29
  %call17 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_turnVelocity)
  %2 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  call void @_ZN11btRigidBody14setupRigidBodyERKNS_27btRigidBodyConstructionInfoE(%class.btRigidBody* %this1, %"struct.btRigidBody::btRigidBodyConstructionInfo"* nonnull align 4 dereferenceable(144) %2)
  ret %class.btRigidBody* %this1
}

declare %class.btCollisionObject* @_ZN17btCollisionObjectC2Ev(%class.btCollisionObject* returned) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btRigidBody14setupRigidBodyERKNS_27btRigidBodyConstructionInfoE(%class.btRigidBody* %this, %"struct.btRigidBody::btRigidBodyConstructionInfo"* nonnull align 4 dereferenceable(144) %constructionInfo) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %constructionInfo.addr = alloca %"struct.btRigidBody::btRigidBodyConstructionInfo"*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %ref.tmp42 = alloca float, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp50 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %"struct.btRigidBody::btRigidBodyConstructionInfo"* %constructionInfo, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 24
  store i32 2, i32* %m_internalType, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_linearVelocity, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_angularVelocity, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  store float 1.000000e+00, float* %ref.tmp7, align 4
  store float 1.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_angularFactor, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  store float 1.000000e+00, float* %ref.tmp10, align 4
  store float 1.000000e+00, float* %ref.tmp11, align 4
  store float 1.000000e+00, float* %ref.tmp12, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_linearFactor, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %m_gravity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %ref.tmp13, align 4
  store float 0.000000e+00, float* %ref.tmp14, align 4
  store float 0.000000e+00, float* %ref.tmp15, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_gravity, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %m_gravity_acceleration = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 7
  store float 0.000000e+00, float* %ref.tmp16, align 4
  store float 0.000000e+00, float* %ref.tmp17, align 4
  store float 0.000000e+00, float* %ref.tmp18, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_gravity_acceleration, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18)
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  store float 0.000000e+00, float* %ref.tmp19, align 4
  store float 0.000000e+00, float* %ref.tmp20, align 4
  store float 0.000000e+00, float* %ref.tmp21, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_totalForce, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  %m_totalTorque = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 10
  store float 0.000000e+00, float* %ref.tmp22, align 4
  store float 0.000000e+00, float* %ref.tmp23, align 4
  store float 0.000000e+00, float* %ref.tmp24, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_totalTorque, float* nonnull align 4 dereferenceable(4) %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  %1 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_linearDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %1, i32 0, i32 5
  %2 = load float, float* %m_linearDamping, align 4
  %3 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_angularDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %3, i32 0, i32 6
  %4 = load float, float* %m_angularDamping, align 4
  call void @_ZN11btRigidBody10setDampingEff(%class.btRigidBody* %this1, float %2, float %4)
  %5 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_linearSleepingThreshold = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %5, i32 0, i32 11
  %6 = load float, float* %m_linearSleepingThreshold, align 4
  %m_linearSleepingThreshold25 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 18
  store float %6, float* %m_linearSleepingThreshold25, align 4
  %7 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_angularSleepingThreshold = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %7, i32 0, i32 12
  %8 = load float, float* %m_angularSleepingThreshold, align 4
  %m_angularSleepingThreshold26 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 19
  store float %8, float* %m_angularSleepingThreshold26, align 4
  %9 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_motionState = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %9, i32 0, i32 1
  %10 = load %class.btMotionState*, %class.btMotionState** %m_motionState, align 4
  %m_optionalMotionState = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 20
  store %class.btMotionState* %10, %class.btMotionState** %m_optionalMotionState, align 4
  %m_contactSolverType = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 30
  store i32 0, i32* %m_contactSolverType, align 4
  %m_frictionSolverType = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 31
  store i32 0, i32* %m_frictionSolverType, align 4
  %11 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_additionalDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %11, i32 0, i32 13
  %12 = load i8, i8* %m_additionalDamping, align 4
  %tobool = trunc i8 %12 to i1
  %m_additionalDamping27 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 13
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %m_additionalDamping27, align 4
  %13 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_additionalDampingFactor = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %13, i32 0, i32 14
  %14 = load float, float* %m_additionalDampingFactor, align 4
  %m_additionalDampingFactor28 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 14
  store float %14, float* %m_additionalDampingFactor28, align 4
  %15 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_additionalLinearDampingThresholdSqr = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %15, i32 0, i32 15
  %16 = load float, float* %m_additionalLinearDampingThresholdSqr, align 4
  %m_additionalLinearDampingThresholdSqr29 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 15
  store float %16, float* %m_additionalLinearDampingThresholdSqr29, align 4
  %17 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_additionalAngularDampingThresholdSqr = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %17, i32 0, i32 16
  %18 = load float, float* %m_additionalAngularDampingThresholdSqr, align 4
  %m_additionalAngularDampingThresholdSqr30 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 16
  store float %18, float* %m_additionalAngularDampingThresholdSqr30, align 4
  %19 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_additionalAngularDampingFactor = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %19, i32 0, i32 17
  %20 = load float, float* %m_additionalAngularDampingFactor, align 4
  %m_additionalAngularDampingFactor31 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 17
  store float %20, float* %m_additionalAngularDampingFactor31, align 4
  %m_optionalMotionState32 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 20
  %21 = load %class.btMotionState*, %class.btMotionState** %m_optionalMotionState32, align 4
  %tobool33 = icmp ne %class.btMotionState* %21, null
  br i1 %tobool33, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_optionalMotionState34 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 20
  %22 = load %class.btMotionState*, %class.btMotionState** %m_optionalMotionState34, align 4
  %23 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %23, i32 0, i32 1
  %24 = bitcast %class.btMotionState* %22 to void (%class.btMotionState*, %class.btTransform*)***
  %vtable = load void (%class.btMotionState*, %class.btTransform*)**, void (%class.btMotionState*, %class.btTransform*)*** %24, align 4
  %vfn = getelementptr inbounds void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vtable, i64 2
  %25 = load void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vfn, align 4
  call void %25(%class.btMotionState* %22, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform)
  br label %if.end

if.else:                                          ; preds = %entry
  %26 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_startWorldTransform = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %26, i32 0, i32 2
  %27 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform35 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %27, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_worldTransform35, %class.btTransform* nonnull align 4 dereferenceable(64) %m_startWorldTransform)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %28 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform36 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %28, i32 0, i32 1
  %29 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationWorldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %29, i32 0, i32 2
  %call37 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_interpolationWorldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform36)
  %30 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationLinearVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %30, i32 0, i32 3
  store float 0.000000e+00, float* %ref.tmp38, align 4
  store float 0.000000e+00, float* %ref.tmp39, align 4
  store float 0.000000e+00, float* %ref.tmp40, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_interpolationLinearVelocity, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  %31 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationAngularVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %31, i32 0, i32 4
  store float 0.000000e+00, float* %ref.tmp41, align 4
  store float 0.000000e+00, float* %ref.tmp42, align 4
  store float 0.000000e+00, float* %ref.tmp43, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_interpolationAngularVelocity, float* nonnull align 4 dereferenceable(4) %ref.tmp41, float* nonnull align 4 dereferenceable(4) %ref.tmp42, float* nonnull align 4 dereferenceable(4) %ref.tmp43)
  %32 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_friction = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %32, i32 0, i32 7
  %33 = load float, float* %m_friction, align 4
  %34 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_friction44 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %34, i32 0, i32 18
  store float %33, float* %m_friction44, align 4
  %35 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_rollingFriction = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %35, i32 0, i32 8
  %36 = load float, float* %m_rollingFriction, align 4
  %37 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_rollingFriction45 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %37, i32 0, i32 20
  store float %36, float* %m_rollingFriction45, align 4
  %38 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_spinningFriction = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %38, i32 0, i32 9
  %39 = load float, float* %m_spinningFriction, align 4
  %40 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_spinningFriction46 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %40, i32 0, i32 21
  store float %39, float* %m_spinningFriction46, align 4
  %41 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_restitution = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %41, i32 0, i32 10
  %42 = load float, float* %m_restitution, align 4
  %43 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_restitution47 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %43, i32 0, i32 19
  store float %42, float* %m_restitution47, align 4
  %44 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %45 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_collisionShape = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %45, i32 0, i32 3
  %46 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4
  %47 = bitcast %class.btCollisionObject* %44 to void (%class.btCollisionObject*, %class.btCollisionShape*)***
  %vtable48 = load void (%class.btCollisionObject*, %class.btCollisionShape*)**, void (%class.btCollisionObject*, %class.btCollisionShape*)*** %47, align 4
  %vfn49 = getelementptr inbounds void (%class.btCollisionObject*, %class.btCollisionShape*)*, void (%class.btCollisionObject*, %class.btCollisionShape*)** %vtable48, i64 2
  %48 = load void (%class.btCollisionObject*, %class.btCollisionShape*)*, void (%class.btCollisionObject*, %class.btCollisionShape*)** %vfn49, align 4
  call void %48(%class.btCollisionObject* %44, %class.btCollisionShape* %46)
  %49 = load i32, i32* @_ZL8uniqueId, align 4
  %inc = add nsw i32 %49, 1
  store i32 %inc, i32* @_ZL8uniqueId, align 4
  %m_debugBodyId = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 23
  store i32 %49, i32* %m_debugBodyId, align 4
  %50 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_mass = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %50, i32 0, i32 0
  %51 = load float, float* %m_mass, align 4
  %52 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %constructionInfo.addr, align 4
  %m_localInertia = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %52, i32 0, i32 4
  call void @_ZN11btRigidBody12setMassPropsEfRK9btVector3(%class.btRigidBody* %this1, float %51, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localInertia)
  call void @_ZN11btRigidBody19updateInertiaTensorEv(%class.btRigidBody* %this1)
  %m_rigidbodyFlags = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 22
  store i32 8, i32* %m_rigidbodyFlags, align 4
  %m_deltaLinearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 24
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_deltaLinearVelocity)
  %m_deltaAngularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 25
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_deltaAngularVelocity)
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %m_linearFactor51 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp50, float* nonnull align 4 dereferenceable(4) %m_inverseMass, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor51)
  %m_invMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 27
  %53 = bitcast %class.btVector3* %m_invMass to i8*
  %54 = bitcast %class.btVector3* %ref.tmp50 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %53, i8* align 4 %54, i32 16, i1 false)
  %m_pushVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 28
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_pushVelocity)
  %m_turnVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 29
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_turnVelocity)
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btRigidBody* @_ZN11btRigidBodyC2EfP13btMotionStateP16btCollisionShapeRK9btVector3(%class.btRigidBody* returned %this, float %mass, %class.btMotionState* %motionState, %class.btCollisionShape* %collisionShape, %class.btVector3* nonnull align 4 dereferenceable(16) %localInertia) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %mass.addr = alloca float, align 4
  %motionState.addr = alloca %class.btMotionState*, align 4
  %collisionShape.addr = alloca %class.btCollisionShape*, align 4
  %localInertia.addr = alloca %class.btVector3*, align 4
  %cinfo = alloca %"struct.btRigidBody::btRigidBodyConstructionInfo", align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store float %mass, float* %mass.addr, align 4
  store %class.btMotionState* %motionState, %class.btMotionState** %motionState.addr, align 4
  store %class.btCollisionShape* %collisionShape, %class.btCollisionShape** %collisionShape.addr, align 4
  store %class.btVector3* %localInertia, %class.btVector3** %localInertia.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call = call %class.btCollisionObject* @_ZN17btCollisionObjectC2Ev(%class.btCollisionObject* %0)
  %1 = bitcast %class.btRigidBody* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV11btRigidBody, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  %call2 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_invInertiaTensorWorld)
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearVelocity)
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularVelocity)
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearFactor)
  %m_gravity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 6
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_gravity)
  %m_gravity_acceleration = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 7
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_gravity_acceleration)
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_invInertiaLocal)
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_totalForce)
  %m_totalTorque = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 10
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_totalTorque)
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call11 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray.0* %m_constraintRefs)
  %m_deltaLinearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 24
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_deltaLinearVelocity)
  %m_deltaAngularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 25
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_deltaAngularVelocity)
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_angularFactor)
  %m_invMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 27
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_invMass)
  %m_pushVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 28
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_pushVelocity)
  %m_turnVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 29
  %call17 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_turnVelocity)
  %2 = load float, float* %mass.addr, align 4
  %3 = load %class.btMotionState*, %class.btMotionState** %motionState.addr, align 4
  %4 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %localInertia.addr, align 4
  %call18 = call %"struct.btRigidBody::btRigidBodyConstructionInfo"* @_ZN11btRigidBody27btRigidBodyConstructionInfoC2EfP13btMotionStateP16btCollisionShapeRK9btVector3(%"struct.btRigidBody::btRigidBodyConstructionInfo"* %cinfo, float %2, %class.btMotionState* %3, %class.btCollisionShape* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  call void @_ZN11btRigidBody14setupRigidBodyERKNS_27btRigidBodyConstructionInfoE(%class.btRigidBody* %this1, %"struct.btRigidBody::btRigidBodyConstructionInfo"* nonnull align 4 dereferenceable(144) %cinfo)
  ret %class.btRigidBody* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btRigidBody::btRigidBodyConstructionInfo"* @_ZN11btRigidBody27btRigidBodyConstructionInfoC2EfP13btMotionStateP16btCollisionShapeRK9btVector3(%"struct.btRigidBody::btRigidBodyConstructionInfo"* returned %this, float %mass, %class.btMotionState* %motionState, %class.btCollisionShape* %collisionShape, %class.btVector3* nonnull align 4 dereferenceable(16) %localInertia) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btRigidBody::btRigidBodyConstructionInfo"*, align 4
  %mass.addr = alloca float, align 4
  %motionState.addr = alloca %class.btMotionState*, align 4
  %collisionShape.addr = alloca %class.btCollisionShape*, align 4
  %localInertia.addr = alloca %class.btVector3*, align 4
  store %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %this.addr, align 4
  store float %mass, float* %mass.addr, align 4
  store %class.btMotionState* %motionState, %class.btMotionState** %motionState.addr, align 4
  store %class.btCollisionShape* %collisionShape, %class.btCollisionShape** %collisionShape.addr, align 4
  store %class.btVector3* %localInertia, %class.btVector3** %localInertia.addr, align 4
  %this1 = load %"struct.btRigidBody::btRigidBodyConstructionInfo"*, %"struct.btRigidBody::btRigidBodyConstructionInfo"** %this.addr, align 4
  %m_mass = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 0
  %0 = load float, float* %mass.addr, align 4
  store float %0, float* %m_mass, align 4
  %m_motionState = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 1
  %1 = load %class.btMotionState*, %class.btMotionState** %motionState.addr, align 4
  store %class.btMotionState* %1, %class.btMotionState** %m_motionState, align 4
  %m_startWorldTransform = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 2
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_startWorldTransform)
  %m_collisionShape = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 3
  %2 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4
  store %class.btCollisionShape* %2, %class.btCollisionShape** %m_collisionShape, align 4
  %m_localInertia = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %localInertia.addr, align 4
  %4 = bitcast %class.btVector3* %m_localInertia to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_linearDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_linearDamping, align 4
  %m_angularDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_angularDamping, align 4
  %m_friction = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 7
  store float 5.000000e-01, float* %m_friction, align 4
  %m_rollingFriction = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 8
  store float 0.000000e+00, float* %m_rollingFriction, align 4
  %m_spinningFriction = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 9
  store float 0.000000e+00, float* %m_spinningFriction, align 4
  %m_restitution = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 10
  store float 0.000000e+00, float* %m_restitution, align 4
  %m_linearSleepingThreshold = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 11
  store float 0x3FE99999A0000000, float* %m_linearSleepingThreshold, align 4
  %m_angularSleepingThreshold = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 12
  store float 1.000000e+00, float* %m_angularSleepingThreshold, align 4
  %m_additionalDamping = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 13
  store i8 0, i8* %m_additionalDamping, align 4
  %m_additionalDampingFactor = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 14
  store float 0x3F747AE140000000, float* %m_additionalDampingFactor, align 4
  %m_additionalLinearDampingThresholdSqr = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 15
  store float 0x3F847AE140000000, float* %m_additionalLinearDampingThresholdSqr, align 4
  %m_additionalAngularDampingThresholdSqr = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 16
  store float 0x3F847AE140000000, float* %m_additionalAngularDampingThresholdSqr, align 4
  %m_additionalAngularDampingFactor = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 17
  store float 0x3F847AE140000000, float* %m_additionalAngularDampingFactor, align 4
  %m_startWorldTransform2 = getelementptr inbounds %"struct.btRigidBody::btRigidBodyConstructionInfo", %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1, i32 0, i32 2
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %m_startWorldTransform2)
  ret %"struct.btRigidBody::btRigidBodyConstructionInfo"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btRigidBody10setDampingEff(%class.btRigidBody* %this, float %lin_damping, float %ang_damping) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %lin_damping.addr = alloca float, align 4
  %ang_damping.addr = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store float %lin_damping, float* %lin_damping.addr, align 4
  store float %ang_damping, float* %ang_damping.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_Z9btClampedIfERKT_S2_S2_S2_(float* nonnull align 4 dereferenceable(4) %lin_damping.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %0 = load float, float* %call, align 4
  %m_linearDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 11
  store float %0, float* %m_linearDamping, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 1.000000e+00, float* %ref.tmp4, align 4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_Z9btClampedIfERKT_S2_S2_S2_(float* nonnull align 4 dereferenceable(4) %ang_damping.addr, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %1 = load float, float* %call5, align 4
  %m_angularDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 12
  store float %1, float* %m_angularDamping, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btRigidBody12setMassPropsEfRK9btVector3(%class.btRigidBody* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store float %mass, float* %mass.addr, align 4
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = load float, float* %mass.addr, align 4
  %cmp = fcmp oeq float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %1, i32 0, i32 12
  %2 = load i32, i32* %m_collisionFlags, align 4
  %or = or i32 %2, 1
  store i32 %or, i32* %m_collisionFlags, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  store float 0.000000e+00, float* %m_inverseMass, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_collisionFlags2 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %3, i32 0, i32 12
  %4 = load i32, i32* %m_collisionFlags2, align 4
  %and = and i32 %4, -2
  store i32 %and, i32* %m_collisionFlags2, align 4
  %5 = load float, float* %mass.addr, align 4
  %div = fdiv float 1.000000e+00, %5
  %m_inverseMass3 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  store float %div, float* %m_inverseMass3, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %m_gravity_acceleration = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 7
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, float* nonnull align 4 dereferenceable(4) %mass.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %m_gravity_acceleration)
  %m_gravity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 6
  %6 = bitcast %class.btVector3* %m_gravity to i8*
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  %8 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %8)
  %9 = load float, float* %call, align 4
  %cmp5 = fcmp une float %9, 0.000000e+00
  br i1 %cmp5, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %10 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %10)
  %11 = load float, float* %call6, align 4
  %div7 = fdiv float 1.000000e+00, %11
  br label %cond.end

cond.false:                                       ; preds = %if.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %div7, %cond.true ], [ 0.000000e+00, %cond.false ]
  store float %cond, float* %ref.tmp4, align 4
  %12 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %12)
  %13 = load float, float* %call9, align 4
  %cmp10 = fcmp une float %13, 0.000000e+00
  br i1 %cmp10, label %cond.true11, label %cond.false14

cond.true11:                                      ; preds = %cond.end
  %14 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %14)
  %15 = load float, float* %call12, align 4
  %div13 = fdiv float 1.000000e+00, %15
  br label %cond.end15

cond.false14:                                     ; preds = %cond.end
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false14, %cond.true11
  %cond16 = phi float [ %div13, %cond.true11 ], [ 0.000000e+00, %cond.false14 ]
  store float %cond16, float* %ref.tmp8, align 4
  %16 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %16)
  %17 = load float, float* %call18, align 4
  %cmp19 = fcmp une float %17, 0.000000e+00
  br i1 %cmp19, label %cond.true20, label %cond.false23

cond.true20:                                      ; preds = %cond.end15
  %18 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %18)
  %19 = load float, float* %call21, align 4
  %div22 = fdiv float 1.000000e+00, %19
  br label %cond.end24

cond.false23:                                     ; preds = %cond.end15
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false23, %cond.true20
  %cond25 = phi float [ %div22, %cond.true20 ], [ 0.000000e+00, %cond.false23 ]
  store float %cond25, float* %ref.tmp17, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_invInertiaLocal, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  %m_inverseMass27 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor, float* nonnull align 4 dereferenceable(4) %m_inverseMass27)
  %m_invMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 27
  %20 = bitcast %class.btVector3* %m_invMass to i8*
  %21 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btRigidBody19updateInertiaTensorEv(%class.btRigidBody* %this) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp2 = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btMatrix3x3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_worldTransform)
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  call void @_ZNK11btMatrix3x36scaledERK9btVector3(%class.btMatrix3x3* sret align 4 %ref.tmp2, %class.btMatrix3x3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %m_invInertiaLocal)
  %1 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform4 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %1, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_worldTransform4)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp3, %class.btMatrix3x3* %call5)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp3)
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_invInertiaTensorWorld, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector37setZeroEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define hidden void @_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform(%class.btRigidBody* %this, float %timeStep, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTransform) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %timeStep.addr = alloca float, align 4
  %predictedTransform.addr = alloca %class.btTransform*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  store %class.btTransform* %predictedTransform, %class.btTransform** %predictedTransform.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %1 = load float, float* %timeStep.addr, align 4
  %2 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4
  call void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularVelocity, float %1, %class.btTransform* nonnull align 4 dereferenceable(64) %2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_(%class.btTransform* nonnull align 4 dereferenceable(64) %curTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %linvel, %class.btVector3* nonnull align 4 dereferenceable(16) %angvel, float %timeStep, %class.btTransform* nonnull align 4 dereferenceable(64) %predictedTransform) #2 comdat {
entry:
  %curTrans.addr = alloca %class.btTransform*, align 4
  %linvel.addr = alloca %class.btVector3*, align 4
  %angvel.addr = alloca %class.btVector3*, align 4
  %timeStep.addr = alloca float, align 4
  %predictedTransform.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %axis = alloca %class.btVector3, align 4
  %fAngle = alloca float, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca float, align 4
  %dorn = alloca %class.btQuaternion, align 4
  %ref.tmp24 = alloca float, align 4
  %orn0 = alloca %class.btQuaternion, align 4
  %predictedOrn = alloca %class.btQuaternion, align 4
  store %class.btTransform* %curTrans, %class.btTransform** %curTrans.addr, align 4
  store %class.btVector3* %linvel, %class.btVector3** %linvel.addr, align 4
  store %class.btVector3* %angvel, %class.btVector3** %angvel.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  store %class.btTransform* %predictedTransform, %class.btTransform** %predictedTransform.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %curTrans.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %1)
  %2 = load %class.btVector3*, %class.btVector3** %linvel.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis)
  %3 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %call3 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %3)
  store float %call3, float* %fAngle, align 4
  %4 = load float, float* %fAngle, align 4
  %5 = load float, float* %timeStep.addr, align 4
  %mul = fmul float %4, %5
  %cmp = fcmp ogt float %mul, 0x3FE921FB60000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load float, float* %timeStep.addr, align 4
  %div = fdiv float 0x3FE921FB60000000, %6
  store float %div, float* %fAngle, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load float, float* %fAngle, align 4
  %cmp4 = fcmp olt float %7, 0x3F50624DE0000000
  br i1 %cmp4, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.end
  %8 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %9 = load float, float* %timeStep.addr, align 4
  %mul8 = fmul float 5.000000e-01, %9
  %10 = load float, float* %timeStep.addr, align 4
  %11 = load float, float* %timeStep.addr, align 4
  %mul9 = fmul float %10, %11
  %12 = load float, float* %timeStep.addr, align 4
  %mul10 = fmul float %mul9, %12
  %mul11 = fmul float %mul10, 0x3F95555560000000
  %13 = load float, float* %fAngle, align 4
  %mul12 = fmul float %mul11, %13
  %14 = load float, float* %fAngle, align 4
  %mul13 = fmul float %mul12, %14
  %sub = fsub float %mul8, %mul13
  store float %sub, float* %ref.tmp7, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %8, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %15 = bitcast %class.btVector3* %axis to i8*
  %16 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  br label %if.end20

if.else:                                          ; preds = %if.end
  %17 = load %class.btVector3*, %class.btVector3** %angvel.addr, align 4
  %18 = load float, float* %fAngle, align 4
  %mul16 = fmul float 5.000000e-01, %18
  %19 = load float, float* %timeStep.addr, align 4
  %mul17 = fmul float %mul16, %19
  %call18 = call float @_Z5btSinf(float %mul17)
  %20 = load float, float* %fAngle, align 4
  %div19 = fdiv float %call18, %20
  store float %div19, float* %ref.tmp15, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %17, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %21 = bitcast %class.btVector3* %axis to i8*
  %22 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  br label %if.end20

if.end20:                                         ; preds = %if.else, %if.then5
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %axis)
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %axis)
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %axis)
  %23 = load float, float* %fAngle, align 4
  %24 = load float, float* %timeStep.addr, align 4
  %mul25 = fmul float %23, %24
  %mul26 = fmul float %mul25, 5.000000e-01
  %call27 = call float @_Z5btCosf(float %mul26)
  store float %call27, float* %ref.tmp24, align 4
  %call28 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %dorn, float* nonnull align 4 dereferenceable(4) %call21, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  %25 = load %class.btTransform*, %class.btTransform** %curTrans.addr, align 4
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %orn0, %class.btTransform* %25)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %predictedOrn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %dorn, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn0)
  %call29 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %predictedOrn)
  %26 = load %class.btTransform*, %class.btTransform** %predictedTransform.addr, align 4
  call void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %26, %class.btQuaternion* nonnull align 4 dereferenceable(16) %predictedOrn)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btRigidBody18saveKinematicStateEf(%class.btRigidBody* %this, float %timeStep) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %timeStep.addr = alloca float, align 4
  %linVel = alloca %class.btVector3, align 4
  %angVel = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = load float, float* %timeStep.addr, align 4
  %cmp = fcmp une float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end12

if.then:                                          ; preds = %entry
  %call = call %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %this1)
  %tobool = icmp ne %class.btMotionState* %call, null
  br i1 %tobool, label %if.then2, label %if.end

if.then2:                                         ; preds = %if.then
  %call3 = call %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %this1)
  %1 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %1, i32 0, i32 1
  %2 = bitcast %class.btMotionState* %call3 to void (%class.btMotionState*, %class.btTransform*)***
  %vtable = load void (%class.btMotionState*, %class.btTransform*)**, void (%class.btMotionState*, %class.btTransform*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vtable, i64 2
  %3 = load void (%class.btMotionState*, %class.btTransform*)*, void (%class.btMotionState*, %class.btTransform*)** %vfn, align 4
  call void %3(%class.btMotionState* %call3, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform)
  br label %if.end

if.end:                                           ; preds = %if.then2, %if.then
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %linVel)
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %angVel)
  %4 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationWorldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %4, i32 0, i32 2
  %5 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform6 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %5, i32 0, i32 1
  %6 = load float, float* %timeStep.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  call void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %m_interpolationWorldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform6, float %6, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %m_angularVelocity)
  %m_linearVelocity7 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %7 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationLinearVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %7, i32 0, i32 3
  %8 = bitcast %class.btVector3* %m_interpolationLinearVelocity to i8*
  %9 = bitcast %class.btVector3* %m_linearVelocity7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %m_angularVelocity8 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %10 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationAngularVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %10, i32 0, i32 4
  %11 = bitcast %class.btVector3* %m_interpolationAngularVelocity to i8*
  %12 = bitcast %class.btVector3* %m_angularVelocity8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false)
  %13 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform9 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %13, i32 0, i32 1
  %14 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationWorldTransform10 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %14, i32 0, i32 2
  %call11 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_interpolationWorldTransform10, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform9)
  br label %if.end12

if.end12:                                         ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btMotionState* @_ZN11btRigidBody14getMotionStateEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_optionalMotionState = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 20
  %0 = load %class.btMotionState*, %class.btMotionState** %m_optionalMotionState, align 4
  ret %class.btMotionState* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN15btTransformUtil17calculateVelocityERK11btTransformS2_fR9btVector3S4_(%class.btTransform* nonnull align 4 dereferenceable(64) %transform0, %class.btTransform* nonnull align 4 dereferenceable(64) %transform1, float %timeStep, %class.btVector3* nonnull align 4 dereferenceable(16) %linVel, %class.btVector3* nonnull align 4 dereferenceable(16) %angVel) #2 comdat {
entry:
  %transform0.addr = alloca %class.btTransform*, align 4
  %transform1.addr = alloca %class.btTransform*, align 4
  %timeStep.addr = alloca float, align 4
  %linVel.addr = alloca %class.btVector3*, align 4
  %angVel.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %axis = alloca %class.btVector3, align 4
  %angle = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btTransform* %transform0, %class.btTransform** %transform0.addr, align 4
  store %class.btTransform* %transform1, %class.btTransform** %transform1.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  store %class.btVector3* %linVel, %class.btVector3** %linVel.addr, align 4
  store %class.btVector3* %angVel, %class.btVector3** %angVel.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %0)
  %1 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %1)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  %2 = load %class.btVector3*, %class.btVector3** %linVel.addr, align 4
  %3 = bitcast %class.btVector3* %2 to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis)
  %5 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4
  %6 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4
  call void @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf(%class.btTransform* nonnull align 4 dereferenceable(64) %5, %class.btTransform* nonnull align 4 dereferenceable(64) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle)
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %timeStep.addr)
  %7 = load %class.btVector3*, %class.btVector3** %angVel.addr, align 4
  %8 = bitcast %class.btVector3* %7 to i8*
  %9 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK11btRigidBody7getAabbER9btVector3S1_(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %call = call %class.btCollisionShape* @_ZNK11btRigidBody17getCollisionShapeEv(%class.btRigidBody* %this1)
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %3 = bitcast %class.btCollisionShape* %call to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %4 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btCollisionShape* %call, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK11btRigidBody17getCollisionShapeEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 9
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4
  ret %class.btCollisionShape* %1
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btRigidBody10setGravityERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %acceleration) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %acceleration.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %acceleration, %class.btVector3** %acceleration.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4
  %cmp = fcmp une float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %acceleration.addr, align 4
  %m_inverseMass3 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %2 = load float, float* %m_inverseMass3, align 4
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp2, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %m_gravity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 6
  %3 = bitcast %class.btVector3* %m_gravity to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load %class.btVector3*, %class.btVector3** %acceleration.addr, align 4
  %m_gravity_acceleration = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 7
  %6 = bitcast %class.btVector3* %m_gravity_acceleration to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_Z9btClampedIfERKT_S2_S2_S2_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %lb, float* nonnull align 4 dereferenceable(4) %ub) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %lb.addr = alloca float*, align 4
  %ub.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %lb, float** %lb.addr, align 4
  store float* %ub, float** %ub.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %lb.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load float*, float** %lb.addr, align 4
  br label %cond.end4

cond.false:                                       ; preds = %entry
  %5 = load float*, float** %ub.addr, align 4
  %6 = load float, float* %5, align 4
  %7 = load float*, float** %a.addr, align 4
  %8 = load float, float* %7, align 4
  %cmp1 = fcmp olt float %6, %8
  br i1 %cmp1, label %cond.true2, label %cond.false3

cond.true2:                                       ; preds = %cond.false
  %9 = load float*, float** %ub.addr, align 4
  br label %cond.end

cond.false3:                                      ; preds = %cond.false
  %10 = load float*, float** %a.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false3, %cond.true2
  %cond-lvalue = phi float* [ %9, %cond.true2 ], [ %10, %cond.false3 ]
  br label %cond.end4

cond.end4:                                        ; preds = %cond.end, %cond.true
  %cond-lvalue5 = phi float* [ %4, %cond.true ], [ %cond-lvalue, %cond.end ]
  ret float* %cond-lvalue5
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btRigidBody12applyDampingEf(%class.btRigidBody* %this, float %timeStep) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %timeStep.addr = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %speed = alloca float, align 4
  %dampVel = alloca float, align 4
  %dir = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %angSpeed = alloca float, align 4
  %angDampVel = alloca float, align 4
  %dir42 = alloca %class.btVector3, align 4
  %ref.tmp44 = alloca %class.btVector3, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp50 = alloca float, align 4
  %ref.tmp51 = alloca float, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 11
  %0 = load float, float* %m_linearDamping, align 4
  %sub = fsub float 1.000000e+00, %0
  %1 = load float, float* %timeStep.addr, align 4
  %call = call float @_Z5btPowff(float %sub, float %1)
  store float %call, float* %ref.tmp, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_linearVelocity, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %m_angularDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 12
  %2 = load float, float* %m_angularDamping, align 4
  %sub4 = fsub float 1.000000e+00, %2
  %3 = load float, float* %timeStep.addr, align 4
  %call5 = call float @_Z5btPowff(float %sub4, float %3)
  store float %call5, float* %ref.tmp3, align 4
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_angularVelocity, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %m_additionalDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 13
  %4 = load i8, i8* %m_additionalDamping, align 4
  %tobool = trunc i8 %4 to i1
  br i1 %tobool, label %if.then, label %if.end54

if.then:                                          ; preds = %entry
  %m_angularVelocity7 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call8 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_angularVelocity7)
  %m_additionalAngularDampingThresholdSqr = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 16
  %5 = load float, float* %m_additionalAngularDampingThresholdSqr, align 4
  %cmp = fcmp olt float %call8, %5
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %m_linearVelocity9 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call10 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_linearVelocity9)
  %m_additionalLinearDampingThresholdSqr = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 15
  %6 = load float, float* %m_additionalLinearDampingThresholdSqr, align 4
  %cmp11 = fcmp olt float %call10, %6
  br i1 %cmp11, label %if.then12, label %if.end

if.then12:                                        ; preds = %land.lhs.true
  %m_additionalDampingFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 14
  %m_angularVelocity13 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_angularVelocity13, float* nonnull align 4 dereferenceable(4) %m_additionalDampingFactor)
  %m_additionalDampingFactor15 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 14
  %m_linearVelocity16 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_linearVelocity16, float* nonnull align 4 dereferenceable(4) %m_additionalDampingFactor15)
  br label %if.end

if.end:                                           ; preds = %if.then12, %land.lhs.true, %if.then
  %m_linearVelocity18 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call19 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_linearVelocity18)
  store float %call19, float* %speed, align 4
  %7 = load float, float* %speed, align 4
  %m_linearDamping20 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 11
  %8 = load float, float* %m_linearDamping20, align 4
  %cmp21 = fcmp olt float %7, %8
  br i1 %cmp21, label %if.then22, label %if.end34

if.then22:                                        ; preds = %if.end
  store float 0x3F747AE140000000, float* %dampVel, align 4
  %9 = load float, float* %speed, align 4
  %10 = load float, float* %dampVel, align 4
  %cmp23 = fcmp ogt float %9, %10
  br i1 %cmp23, label %if.then24, label %if.else

if.then24:                                        ; preds = %if.then22
  %m_linearVelocity25 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %dir, %class.btVector3* %m_linearVelocity25)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %dir, float* nonnull align 4 dereferenceable(4) %dampVel)
  %m_linearVelocity27 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call28 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %m_linearVelocity27, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp26)
  br label %if.end33

if.else:                                          ; preds = %if.then22
  %m_linearVelocity29 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %ref.tmp30, align 4
  store float 0.000000e+00, float* %ref.tmp31, align 4
  store float 0.000000e+00, float* %ref.tmp32, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_linearVelocity29, float* nonnull align 4 dereferenceable(4) %ref.tmp30, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  br label %if.end33

if.end33:                                         ; preds = %if.else, %if.then24
  br label %if.end34

if.end34:                                         ; preds = %if.end33, %if.end
  %m_angularVelocity35 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call36 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_angularVelocity35)
  store float %call36, float* %angSpeed, align 4
  %11 = load float, float* %angSpeed, align 4
  %m_angularDamping37 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 12
  %12 = load float, float* %m_angularDamping37, align 4
  %cmp38 = fcmp olt float %11, %12
  br i1 %cmp38, label %if.then39, label %if.end53

if.then39:                                        ; preds = %if.end34
  store float 0x3F747AE140000000, float* %angDampVel, align 4
  %13 = load float, float* %angSpeed, align 4
  %14 = load float, float* %angDampVel, align 4
  %cmp40 = fcmp ogt float %13, %14
  br i1 %cmp40, label %if.then41, label %if.else47

if.then41:                                        ; preds = %if.then39
  %m_angularVelocity43 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %dir42, %class.btVector3* %m_angularVelocity43)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp44, %class.btVector3* nonnull align 4 dereferenceable(16) %dir42, float* nonnull align 4 dereferenceable(4) %angDampVel)
  %m_angularVelocity45 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %m_angularVelocity45, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp44)
  br label %if.end52

if.else47:                                        ; preds = %if.then39
  %m_angularVelocity48 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  store float 0.000000e+00, float* %ref.tmp49, align 4
  store float 0.000000e+00, float* %ref.tmp50, align 4
  store float 0.000000e+00, float* %ref.tmp51, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_angularVelocity48, float* nonnull align 4 dereferenceable(4) %ref.tmp49, float* nonnull align 4 dereferenceable(4) %ref.tmp50, float* nonnull align 4 dereferenceable(4) %ref.tmp51)
  br label %if.end52

if.end52:                                         ; preds = %if.else47, %if.then41
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.end34
  br label %if.end54

if.end54:                                         ; preds = %if.end53, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btPowff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %2 = call float @llvm.pow.f32(float %0, float %1)
  ret float %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %nrm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %nrm to i8*
  %1 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %nrm)
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btRigidBody12applyGravityEv(%class.btRigidBody* %this) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %0)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %m_gravity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 6
  call void @_ZN11btRigidBody17applyCentralForceERK9btVector3(%class.btRigidBody* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_gravity)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 3
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btRigidBody17applyCentralForceERK9btVector3(%class.btRigidBody* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %force) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %force.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btVector3* %force, %class.btVector3** %force.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %force.addr, align 4
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_linearFactor)
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_totalForce, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btRigidBody18proceedToTransformERK11btTransform(%class.btRigidBody* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %newTrans) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %newTrans.addr = alloca %class.btTransform*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btTransform* %newTrans, %class.btTransform** %newTrans.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %newTrans.addr, align 4
  call void @_ZN11btRigidBody24setCenterOfMassTransformERK11btTransform(%class.btRigidBody* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btRigidBody24setCenterOfMassTransformERK11btTransform(%class.btRigidBody* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %xform) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %xform.addr = alloca %class.btTransform*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btTransform* %xform, %class.btTransform** %xform.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call = call zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %0)
  br i1 %call, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %1, i32 0, i32 1
  %2 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationWorldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %2, i32 0, i32 2
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_interpolationWorldTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_worldTransform)
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load %class.btTransform*, %class.btTransform** %xform.addr, align 4
  %4 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationWorldTransform3 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %4, i32 0, i32 2
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_interpolationWorldTransform3, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %this1)
  %5 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationLinearVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %5, i32 0, i32 3
  %6 = bitcast %class.btVector3* %m_interpolationLinearVelocity to i8*
  %7 = bitcast %class.btVector3* %call5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this1)
  %8 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_interpolationAngularVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %8, i32 0, i32 4
  %9 = bitcast %class.btVector3* %m_interpolationAngularVelocity to i8*
  %10 = bitcast %class.btVector3* %call6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %11 = load %class.btTransform*, %class.btTransform** %xform.addr, align 4
  %12 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform7 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %12, i32 0, i32 1
  %call8 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_worldTransform7, %class.btTransform* nonnull align 4 dereferenceable(64) %11)
  call void @_ZN11btRigidBody19updateInertiaTensorEv(%class.btRigidBody* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x36scaledERK9btVector3(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %s) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %s.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %s, %class.btVector3** %s.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4
  %2 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %2)
  %3 = load float, float* %call2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %4 = load float, float* %call6, align 4
  %5 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %5)
  %6 = load float, float* %call7, align 4
  %mul8 = fmul float %4, %6
  store float %mul8, float* %ref.tmp3, align 4
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 0
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx11)
  %7 = load float, float* %call12, align 4
  %8 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %8)
  %9 = load float, float* %call13, align 4
  %mul14 = fmul float %7, %9
  store float %mul14, float* %ref.tmp9, align 4
  %m_el16 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el16, i32 0, i32 1
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx17)
  %10 = load float, float* %call18, align 4
  %11 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %11)
  %12 = load float, float* %call19, align 4
  %mul20 = fmul float %10, %12
  store float %mul20, float* %ref.tmp15, align 4
  %m_el22 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el22, i32 0, i32 1
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx23)
  %13 = load float, float* %call24, align 4
  %14 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %14)
  %15 = load float, float* %call25, align 4
  %mul26 = fmul float %13, %15
  store float %mul26, float* %ref.tmp21, align 4
  %m_el28 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx29 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el28, i32 0, i32 1
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx29)
  %16 = load float, float* %call30, align 4
  %17 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %17)
  %18 = load float, float* %call31, align 4
  %mul32 = fmul float %16, %18
  store float %mul32, float* %ref.tmp27, align 4
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx35)
  %19 = load float, float* %call36, align 4
  %20 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %20)
  %21 = load float, float* %call37, align 4
  %mul38 = fmul float %19, %21
  store float %mul38, float* %ref.tmp33, align 4
  %m_el40 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx41 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el40, i32 0, i32 2
  %call42 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx41)
  %22 = load float, float* %call42, align 4
  %23 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %23)
  %24 = load float, float* %call43, align 4
  %mul44 = fmul float %22, %24
  store float %mul44, float* %ref.tmp39, align 4
  %m_el46 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx47 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el46, i32 0, i32 2
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx47)
  %25 = load float, float* %call48, align 4
  %26 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %26)
  %27 = load float, float* %call49, align 4
  %mul50 = fmul float %25, %27
  store float %mul50, float* %ref.tmp45, align 4
  %call51 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp45)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK11btRigidBody15getLocalInertiaEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btRigidBody* %this) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %inertia = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %agg.result)
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  %0 = bitcast %class.btVector3* %inertia to i8*
  %1 = bitcast %class.btVector3* %m_invInertiaLocal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %inertia)
  %2 = load float, float* %call2, align 4
  %cmp = fcmp une float %2, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %inertia)
  %3 = load float, float* %call3, align 4
  %div = fdiv float 1.000000e+00, %3
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %div, %cond.true ], [ 0.000000e+00, %cond.false ]
  store float %cond, float* %ref.tmp, align 4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %inertia)
  %4 = load float, float* %call5, align 4
  %cmp6 = fcmp une float %4, 0.000000e+00
  br i1 %cmp6, label %cond.true7, label %cond.false10

cond.true7:                                       ; preds = %cond.end
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %inertia)
  %5 = load float, float* %call8, align 4
  %div9 = fdiv float 1.000000e+00, %5
  br label %cond.end11

cond.false10:                                     ; preds = %cond.end
  br label %cond.end11

cond.end11:                                       ; preds = %cond.false10, %cond.true7
  %cond12 = phi float [ %div9, %cond.true7 ], [ 0.000000e+00, %cond.false10 ]
  store float %cond12, float* %ref.tmp4, align 4
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %inertia)
  %6 = load float, float* %call14, align 4
  %cmp15 = fcmp une float %6, 0.000000e+00
  br i1 %cmp15, label %cond.true16, label %cond.false19

cond.true16:                                      ; preds = %cond.end11
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %inertia)
  %7 = load float, float* %call17, align 4
  %div18 = fdiv float 1.000000e+00, %7
  br label %cond.end20

cond.false19:                                     ; preds = %cond.end11
  br label %cond.end20

cond.end20:                                       ; preds = %cond.false19, %cond.true16
  %cond21 = phi float [ %div18, %cond.true16 ], [ 0.000000e+00, %cond.false19 ]
  store float %cond21, float* %ref.tmp13, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK11btRigidBody30computeGyroscopicForceExplicitEf(%class.btVector3* noalias sret align 4 %agg.result, %class.btRigidBody* %this, float %maxGyroscopicForce) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %maxGyroscopicForce.addr = alloca float, align 4
  %inertiaLocal = alloca %class.btVector3, align 4
  %inertiaTensorWorld = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btMatrix3x3, align 4
  %tmp = alloca %class.btVector3, align 4
  %l2 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store float %maxGyroscopicForce, float* %maxGyroscopicForce.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  call void @_ZNK11btRigidBody15getLocalInertiaEv(%class.btVector3* sret align 4 %inertiaLocal, %class.btRigidBody* %this1)
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %0)
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call)
  call void @_ZNK11btMatrix3x36scaledERK9btVector3(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call2, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaLocal)
  %1 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %1)
  %call5 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call4)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp3, %class.btMatrix3x3* %call5)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %inertiaTensorWorld, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp3)
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this1)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inertiaTensorWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this1)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %tmp)
  %call8 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %agg.result)
  store float %call8, float* %l2, align 4
  %2 = load float, float* %l2, align 4
  %3 = load float, float* %maxGyroscopicForce.addr, align 4
  %4 = load float, float* %maxGyroscopicForce.addr, align 4
  %mul = fmul float %3, %4
  %cmp = fcmp ogt float %2, %mul
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load float, float* %l2, align 4
  %call10 = call float @_Z6btSqrtf(float %5)
  %div = fdiv float 1.000000e+00, %call10
  %6 = load float, float* %maxGyroscopicForce.addr, align 4
  %mul11 = fmul float %div, %6
  store float %mul11, float* %ref.tmp9, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  ret %class.btVector3* %m_angularVelocity
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK11btRigidBody37computeGyroscopicImpulseImplicit_BodyEf(%class.btVector3* noalias sret align 4 %agg.result, %class.btRigidBody* %this, float %step) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %step.addr = alloca float, align 4
  %idl = alloca %class.btVector3, align 4
  %omega1 = alloca %class.btVector3, align 4
  %q = alloca %class.btQuaternion, align 4
  %omegab = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  %Ib = alloca %class.btMatrix3x3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ibo = alloca %class.btVector3, align 4
  %f = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %skew0 = alloca %class.btMatrix3x3, align 4
  %om = alloca %class.btVector3, align 4
  %skew1 = alloca %class.btMatrix3x3, align 4
  %J = alloca %class.btMatrix3x3, align 4
  %ref.tmp22 = alloca %class.btMatrix3x3, align 4
  %ref.tmp23 = alloca %class.btMatrix3x3, align 4
  %ref.tmp24 = alloca %class.btMatrix3x3, align 4
  %omega_div = alloca %class.btVector3, align 4
  %ref.tmp25 = alloca %class.btVector3, align 4
  %omega2 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store float %step, float* %step.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  call void @_ZNK11btRigidBody15getLocalInertiaEv(%class.btVector3* sret align 4 %idl, %class.btRigidBody* %this1)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this1)
  %0 = bitcast %class.btVector3* %omega1 to i8*
  %1 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %2 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %2)
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %q, %class.btTransform* %call2)
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %q)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %omegab, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %omega1)
  %call3 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %Ib)
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %idl)
  store float 0.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %idl)
  store float 0.000000e+00, float* %ref.tmp9, align 4
  store float 0.000000e+00, float* %ref.tmp10, align 4
  store float 0.000000e+00, float* %ref.tmp11, align 4
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %idl)
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %Ib, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %call8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %call12)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ibo, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %Ib, %class.btVector3* nonnull align 4 dereferenceable(16) %omegab)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* %omegab, %class.btVector3* nonnull align 4 dereferenceable(16) %ibo)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %f, float* nonnull align 4 dereferenceable(4) %step.addr, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp13)
  %call14 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %skew0)
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %skew0, i32 0)
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %skew0, i32 1)
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %skew0, i32 2)
  call void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %omegab, %class.btVector3* %call15, %class.btVector3* %call16, %class.btVector3* %call17)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %om, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %Ib, %class.btVector3* nonnull align 4 dereferenceable(16) %omegab)
  %call18 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %skew1)
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %skew1, i32 0)
  %call20 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %skew1, i32 1)
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %skew1, i32 2)
  call void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %om, %class.btVector3* %call19, %class.btVector3* %call20, %class.btVector3* %call21)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp24, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %skew0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %Ib)
  call void @_ZmiRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp23, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp24, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %skew1)
  call void @_ZmlRK11btMatrix3x3RKf(%class.btMatrix3x3* sret align 4 %ref.tmp22, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %step.addr)
  call void @_ZplRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %J, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %Ib, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp22)
  call void @_ZNK11btMatrix3x37solve33ERK9btVector3(%class.btVector3* sret align 4 %omega_div, %class.btMatrix3x3* %J, %class.btVector3* nonnull align 4 dereferenceable(16) %f)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp25, %class.btVector3* nonnull align 4 dereferenceable(16) %omegab, %class.btVector3* nonnull align 4 dereferenceable(16) %omega_div)
  %3 = bitcast %class.btVector3* %omegab to i8*
  %4 = bitcast %class.btVector3* %ref.tmp25 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %omega2, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %omegab)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %omega2, %class.btVector3* nonnull align 4 dereferenceable(16) %omega1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %rotation.addr = alloca %class.btQuaternion*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %q = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  store %class.btQuaternion* %rotation, %class.btQuaternion** %rotation.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  call void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* sret align 4 %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %2)
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %3)
  %4 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %4)
  %5 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %5)
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call1, float* nonnull align 4 dereferenceable(4) %call2, float* nonnull align 4 dereferenceable(4) %call3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats3 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %3 = load float, float* %arrayidx4, align 4
  %fneg5 = fneg float %3
  store float %fneg5, float* %ref.tmp2, align 4
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %4, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %5 = load float, float* %arrayidx8, align 4
  %fneg9 = fneg float %5
  store float %fneg9, float* %ref.tmp6, align 4
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats10 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 3
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %this, %class.btVector3* %v0, %class.btVector3* %v1, %class.btVector3* %v2) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this1)
  %1 = load float, float* %call, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp2, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this1)
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %call3)
  %2 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this1)
  store float 0.000000e+00, float* %ref.tmp5, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this1)
  %3 = load float, float* %call7, align 4
  %fneg8 = fneg float %3
  store float %fneg8, float* %ref.tmp6, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %2, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %4 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this1)
  %5 = load float, float* %call10, align 4
  %fneg11 = fneg float %5
  store float %fneg11, float* %ref.tmp9, align 4
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this1)
  store float 0.000000e+00, float* %ref.tmp13, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %4, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %call12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call)
  %arrayidx = getelementptr inbounds float, float* %call1, i32 0
  %2 = load float, float* %arrayidx, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 0
  %4 = load float, float* %arrayidx4, align 4
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %5, i32 0)
  %call7 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call6)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 1
  %6 = load float, float* %arrayidx8, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 0)
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call9)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  %8 = load float, float* %arrayidx11, align 4
  %add12 = fadd float %6, %8
  store float %add12, float* %ref.tmp5, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call15 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call14)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 2
  %10 = load float, float* %arrayidx16, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %11, i32 0)
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 2
  %12 = load float, float* %arrayidx19, align 4
  %add20 = fadd float %10, %12
  store float %add20, float* %ref.tmp13, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %13, i32 1)
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call22)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 0
  %14 = load float, float* %arrayidx24, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call26 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call25)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 0
  %16 = load float, float* %arrayidx27, align 4
  %add28 = fadd float %14, %16
  store float %add28, float* %ref.tmp21, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %17, i32 1)
  %call31 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call30)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 1
  %18 = load float, float* %arrayidx32, align 4
  %19 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call33 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %19, i32 1)
  %call34 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call33)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 1
  %20 = load float, float* %arrayidx35, align 4
  %add36 = fadd float %18, %20
  store float %add36, float* %ref.tmp29, align 4
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call38 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 1)
  %call39 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call38)
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 2
  %22 = load float, float* %arrayidx40, align 4
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call41 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %23, i32 1)
  %call42 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call41)
  %arrayidx43 = getelementptr inbounds float, float* %call42, i32 2
  %24 = load float, float* %arrayidx43, align 4
  %add44 = fadd float %22, %24
  store float %add44, float* %ref.tmp37, align 4
  %25 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %25, i32 2)
  %call47 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call46)
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 0
  %26 = load float, float* %arrayidx48, align 4
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call49 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call49)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 0
  %28 = load float, float* %arrayidx51, align 4
  %add52 = fadd float %26, %28
  store float %add52, float* %ref.tmp45, align 4
  %29 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call54 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %29, i32 2)
  %call55 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call54)
  %arrayidx56 = getelementptr inbounds float, float* %call55, i32 1
  %30 = load float, float* %arrayidx56, align 4
  %31 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %31, i32 2)
  %call58 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call57)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 1
  %32 = load float, float* %arrayidx59, align 4
  %add60 = fadd float %30, %32
  store float %add60, float* %ref.tmp53, align 4
  %33 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call62 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %33, i32 2)
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call62)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 2
  %34 = load float, float* %arrayidx64, align 4
  %35 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call65 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %35, i32 2)
  %call66 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call65)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 2
  %36 = load float, float* %arrayidx67, align 4
  %add68 = fadd float %34, %36
  store float %add68, float* %ref.tmp61, align 4
  %call69 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp45, float* nonnull align 4 dereferenceable(4) %ref.tmp53, float* nonnull align 4 dereferenceable(4) %ref.tmp61)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RKf(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, float* nonnull align 4 dereferenceable(4) %k) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %k.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store float* %k, float** %k.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call)
  %2 = load float, float* %call1, align 4
  %3 = load float*, float** %k.addr, align 4
  %4 = load float, float* %3, align 4
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %5, i32 0)
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call3)
  %6 = load float, float* %call4, align 4
  %7 = load float*, float** %k.addr, align 4
  %8 = load float, float* %7, align 4
  %mul5 = fmul float %6, %8
  store float %mul5, float* %ref.tmp2, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call7)
  %10 = load float, float* %call8, align 4
  %11 = load float*, float** %k.addr, align 4
  %12 = load float, float* %11, align 4
  %mul9 = fmul float %10, %12
  store float %mul9, float* %ref.tmp6, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %13, i32 1)
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call11)
  %14 = load float, float* %call12, align 4
  %15 = load float*, float** %k.addr, align 4
  %16 = load float, float* %15, align 4
  %mul13 = fmul float %14, %16
  store float %mul13, float* %ref.tmp10, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %17, i32 1)
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call15)
  %18 = load float, float* %call16, align 4
  %19 = load float*, float** %k.addr, align 4
  %20 = load float, float* %19, align 4
  %mul17 = fmul float %18, %20
  store float %mul17, float* %ref.tmp14, align 4
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 1)
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call19)
  %22 = load float, float* %call20, align 4
  %23 = load float*, float** %k.addr, align 4
  %24 = load float, float* %23, align 4
  %mul21 = fmul float %22, %24
  store float %mul21, float* %ref.tmp18, align 4
  %25 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %25, i32 2)
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call23)
  %26 = load float, float* %call24, align 4
  %27 = load float*, float** %k.addr, align 4
  %28 = load float, float* %27, align 4
  %mul25 = fmul float %26, %28
  store float %mul25, float* %ref.tmp22, align 4
  %29 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %29, i32 2)
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call27)
  %30 = load float, float* %call28, align 4
  %31 = load float*, float** %k.addr, align 4
  %32 = load float, float* %31, align 4
  %mul29 = fmul float %30, %32
  store float %mul29, float* %ref.tmp26, align 4
  %33 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call31 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %33, i32 2)
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call31)
  %34 = load float, float* %call32, align 4
  %35 = load float*, float** %k.addr, align 4
  %36 = load float, float* %35, align 4
  %mul33 = fmul float %34, %36
  store float %mul33, float* %ref.tmp30, align 4
  %call34 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp30)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call)
  %arrayidx = getelementptr inbounds float, float* %call1, i32 0
  %2 = load float, float* %arrayidx, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 0
  %4 = load float, float* %arrayidx4, align 4
  %sub = fsub float %2, %4
  store float %sub, float* %ref.tmp, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %5, i32 0)
  %call7 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call6)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 1
  %6 = load float, float* %arrayidx8, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 0)
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call9)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  %8 = load float, float* %arrayidx11, align 4
  %sub12 = fsub float %6, %8
  store float %sub12, float* %ref.tmp5, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call15 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call14)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 2
  %10 = load float, float* %arrayidx16, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %11, i32 0)
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 2
  %12 = load float, float* %arrayidx19, align 4
  %sub20 = fsub float %10, %12
  store float %sub20, float* %ref.tmp13, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %13, i32 1)
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call22)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 0
  %14 = load float, float* %arrayidx24, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call26 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call25)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 0
  %16 = load float, float* %arrayidx27, align 4
  %sub28 = fsub float %14, %16
  store float %sub28, float* %ref.tmp21, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %17, i32 1)
  %call31 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call30)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 1
  %18 = load float, float* %arrayidx32, align 4
  %19 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call33 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %19, i32 1)
  %call34 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call33)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 1
  %20 = load float, float* %arrayidx35, align 4
  %sub36 = fsub float %18, %20
  store float %sub36, float* %ref.tmp29, align 4
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call38 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 1)
  %call39 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call38)
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 2
  %22 = load float, float* %arrayidx40, align 4
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call41 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %23, i32 1)
  %call42 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call41)
  %arrayidx43 = getelementptr inbounds float, float* %call42, i32 2
  %24 = load float, float* %arrayidx43, align 4
  %sub44 = fsub float %22, %24
  store float %sub44, float* %ref.tmp37, align 4
  %25 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %25, i32 2)
  %call47 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call46)
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 0
  %26 = load float, float* %arrayidx48, align 4
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call49 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call49)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 0
  %28 = load float, float* %arrayidx51, align 4
  %sub52 = fsub float %26, %28
  store float %sub52, float* %ref.tmp45, align 4
  %29 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call54 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %29, i32 2)
  %call55 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call54)
  %arrayidx56 = getelementptr inbounds float, float* %call55, i32 1
  %30 = load float, float* %arrayidx56, align 4
  %31 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %31, i32 2)
  %call58 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call57)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 1
  %32 = load float, float* %arrayidx59, align 4
  %sub60 = fsub float %30, %32
  store float %sub60, float* %ref.tmp53, align 4
  %33 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call62 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %33, i32 2)
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call62)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 2
  %34 = load float, float* %arrayidx64, align 4
  %35 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call65 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %35, i32 2)
  %call66 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call65)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 2
  %36 = load float, float* %arrayidx67, align 4
  %sub68 = fsub float %34, %36
  store float %sub68, float* %ref.tmp61, align 4
  %call69 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp45, float* nonnull align 4 dereferenceable(4) %ref.tmp53, float* nonnull align 4 dereferenceable(4) %ref.tmp61)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x37solve33ERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %b) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %col1 = alloca %class.btVector3, align 4
  %col2 = alloca %class.btVector3, align 4
  %col3 = alloca %class.btVector3, align 4
  %det = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %col1, %class.btMatrix3x3* %this1, i32 0)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %col2, %class.btMatrix3x3* %this1, i32 1)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %col3, %class.btMatrix3x3* %this1, i32 2)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %col2, %class.btVector3* nonnull align 4 dereferenceable(16) %col3)
  %call = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %col1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  store float %call, float* %det, align 4
  %0 = load float, float* %det, align 4
  %call2 = call float @_Z6btFabsf(float %0)
  %cmp = fcmp ogt float %call2, 0x3E80000000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load float, float* %det, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %det, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %agg.result)
  %2 = load float, float* %det, align 4
  %3 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %col2, %class.btVector3* nonnull align 4 dereferenceable(16) %col3)
  %call5 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %mul = fmul float %2, %call5
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx = getelementptr inbounds float, float* %call6, i32 0
  store float %mul, float* %arrayidx, align 4
  %4 = load float, float* %det, align 4
  %5 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %col3)
  %call8 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %col1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %mul9 = fmul float %4, %call8
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  store float %mul9, float* %arrayidx11, align 4
  %6 = load float, float* %det, align 4
  %7 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %col2, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  %call13 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %col1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12)
  %mul14 = fmul float %6, %call13
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 2
  store float %mul14, float* %arrayidx16, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK11btRigidBody38computeGyroscopicImpulseImplicit_WorldEf(%class.btVector3* noalias sret align 4 %agg.result, %class.btRigidBody* %this, float %step) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %step.addr = alloca float, align 4
  %inertiaLocal = alloca %class.btVector3, align 4
  %w0 = alloca %class.btVector3, align 4
  %I = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btMatrix3x3, align 4
  %ref.tmp5 = alloca %class.btMatrix3x3, align 4
  %w1 = alloca %class.btVector3, align 4
  %fw = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %dfw = alloca %class.btMatrix3x3, align 4
  %dw = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store float %step, float* %step.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  call void @_ZNK11btRigidBody15getLocalInertiaEv(%class.btVector3* sret align 4 %inertiaLocal, %class.btRigidBody* %this1)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this1)
  %0 = bitcast %class.btVector3* %w0 to i8*
  %1 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %call2 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %I)
  %2 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %m_worldTransform)
  call void @_ZNK11btMatrix3x36scaledERK9btVector3(%class.btMatrix3x3* sret align 4 %ref.tmp3, %class.btMatrix3x3* %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %inertiaLocal)
  %3 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform6 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %3, i32 0, i32 1
  %call7 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %m_worldTransform6)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp5, %class.btMatrix3x3* %call7)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp3, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp5)
  %call8 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %I, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp)
  %4 = bitcast %class.btVector3* %w1 to i8*
  %5 = bitcast %class.btVector3* %w0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  store float 0.000000e+00, float* %ref.tmp10, align 4
  store float 0.000000e+00, float* %ref.tmp11, align 4
  store float 0.000000e+00, float* %ref.tmp12, align 4
  %call13 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %6 = load float, float* %step.addr, align 4
  call void @_Z12evalEulerEqnRK9btVector3S1_S1_fRK11btMatrix3x3(%class.btVector3* sret align 4 %fw, %class.btVector3* nonnull align 4 dereferenceable(16) %w1, %class.btVector3* nonnull align 4 dereferenceable(16) %w0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9, float %6, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %I)
  %7 = load float, float* %step.addr, align 4
  call void @_Z17evalEulerEqnDerivRK9btVector3S1_fRK11btMatrix3x3(%class.btMatrix3x3* sret align 4 %dfw, %class.btVector3* nonnull align 4 dereferenceable(16) %w1, %class.btVector3* nonnull align 4 dereferenceable(16) %w0, float %7, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %I)
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %dw)
  call void @_ZNK11btMatrix3x37solve33ERK9btVector3(%class.btVector3* sret align 4 %ref.tmp15, %class.btMatrix3x3* %dfw, %class.btVector3* nonnull align 4 dereferenceable(16) %fw)
  %8 = bitcast %class.btVector3* %dw to i8*
  %9 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %w1, %class.btVector3* nonnull align 4 dereferenceable(16) %dw)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %w1, %class.btVector3* nonnull align 4 dereferenceable(16) %w0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z12evalEulerEqnRK9btVector3S1_S1_fRK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %w1, %class.btVector3* nonnull align 4 dereferenceable(16) %w0, %class.btVector3* nonnull align 4 dereferenceable(16) %T, float %dt, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %I) #2 comdat {
entry:
  %w1.addr = alloca %class.btVector3*, align 4
  %w0.addr = alloca %class.btVector3*, align 4
  %T.addr = alloca %class.btVector3*, align 4
  %dt.addr = alloca float, align 4
  %I.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  store %class.btVector3* %w1, %class.btVector3** %w1.addr, align 4
  store %class.btVector3* %w0, %class.btVector3** %w0.addr, align 4
  store %class.btVector3* %T, %class.btVector3** %T.addr, align 4
  store float %dt, float* %dt.addr, align 4
  store %class.btMatrix3x3* %I, %class.btMatrix3x3** %I.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %I.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %w1.addr, align 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btVector3*, %class.btVector3** %w1.addr, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %I.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %w1.addr, align 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %dt.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %5 = load %class.btVector3*, %class.btVector3** %T.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %5, float* nonnull align 4 dereferenceable(4) %dt.addr)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %I.addr, align 4
  %7 = load %class.btVector3*, %class.btVector3** %w0.addr, align 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp7, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z17evalEulerEqnDerivRK9btVector3S1_fRK11btMatrix3x3(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %w1, %class.btVector3* nonnull align 4 dereferenceable(16) %w0, float %dt, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %I) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %w1.addr = alloca %class.btVector3*, align 4
  %w0.addr = alloca %class.btVector3*, align 4
  %dt.addr = alloca float, align 4
  %I.addr = alloca %class.btMatrix3x3*, align 4
  %w1x = alloca %class.btMatrix3x3, align 4
  %Iw1x = alloca %class.btMatrix3x3, align 4
  %Iwi = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp8 = alloca %class.btMatrix3x3, align 4
  %ref.tmp9 = alloca %class.btMatrix3x3, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btVector3* %w1, %class.btVector3** %w1.addr, align 4
  store %class.btVector3* %w0, %class.btVector3** %w0.addr, align 4
  store float %dt, float* %dt.addr, align 4
  store %class.btMatrix3x3* %I, %class.btMatrix3x3** %I.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %w1x)
  %call1 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %Iw1x)
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %I.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %w1.addr, align 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %Iwi, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %3 = load %class.btVector3*, %class.btVector3** %w1.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %w1x, i32 0)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %w1x, i32 1)
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %w1x, i32 2)
  call void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %3, %class.btVector3* %call2, %class.btVector3* %call3, %class.btVector3* %call4)
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %Iw1x, i32 0)
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %Iw1x, i32 1)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %Iw1x, i32 2)
  call void @_ZNK9btVector322getSkewSymmetricMatrixEPS_S0_S0_(%class.btVector3* %Iwi, %class.btVector3* %call5, %class.btVector3* %call6, %class.btVector3* %call7)
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %I.addr, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %I.addr, align 4
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp9, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %w1x, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %5)
  call void @_ZmiRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp8, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp9, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %Iw1x)
  call void @_ZmlRK11btMatrix3x3RKf(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %dt.addr)
  call void @_ZplRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %4, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btRigidBody19integrateVelocitiesEf(%class.btRigidBody* %this, float %step) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %step.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %angvel = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store float %step, float* %step.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %0)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %if.end15

if.end:                                           ; preds = %entry
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %1 = load float, float* %m_inverseMass, align 4
  %2 = load float, float* %step.addr, align 4
  %mul = fmul float %1, %2
  store float %mul, float* %ref.tmp2, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_totalForce, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_linearVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  %m_totalTorque = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 10
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp5, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_invInertiaTensorWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %m_totalTorque)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %step.addr)
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_angularVelocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %m_angularVelocity7 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call8 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_angularVelocity7)
  store float %call8, float* %angvel, align 4
  %3 = load float, float* %angvel, align 4
  %4 = load float, float* %step.addr, align 4
  %mul9 = fmul float %3, %4
  %cmp = fcmp ogt float %mul9, 0x3FF921FB60000000
  br i1 %cmp, label %if.then10, label %if.end15

if.then10:                                        ; preds = %if.end
  %5 = load float, float* %step.addr, align 4
  %div = fdiv float 0x3FF921FB60000000, %5
  %6 = load float, float* %angvel, align 4
  %div12 = fdiv float %div, %6
  store float %div12, float* %ref.tmp11, align 4
  %m_angularVelocity13 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_angularVelocity13, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  br label %if.end15

if.end15:                                         ; preds = %if.then, %if.then10, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK11btRigidBody14getOrientationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btRigidBody* %this) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %m_worldTransform)
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %call2, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trace = alloca float, align 4
  %temp = alloca [4 x float], align 16
  %s = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s64 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx3)
  %1 = load float, float* %call4, align 4
  %add = fadd float %0, %1
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx6)
  %2 = load float, float* %call7, align 4
  %add8 = fadd float %add, %2
  store float %add8, float* %trace, align 4
  %3 = load float, float* %trace, align 4
  %cmp = fcmp ogt float %3, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load float, float* %trace, align 4
  %add9 = fadd float %4, 1.000000e+00
  %call10 = call float @_Z6btSqrtf(float %add9)
  store float %call10, float* %s, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %5, 5.000000e-01
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul, float* %arrayidx11, align 4
  %6 = load float, float* %s, align 4
  %div = fdiv float 5.000000e-01, %6
  store float %div, float* %s, align 4
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %7 = load float, float* %call14, align 4
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx16)
  %8 = load float, float* %call17, align 4
  %sub = fsub float %7, %8
  %9 = load float, float* %s, align 4
  %mul18 = fmul float %sub, %9
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  store float %mul18, float* %arrayidx19, align 16
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %10 = load float, float* %call22, align 4
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx24)
  %11 = load float, float* %call25, align 4
  %sub26 = fsub float %10, %11
  %12 = load float, float* %s, align 4
  %mul27 = fmul float %sub26, %12
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  store float %mul27, float* %arrayidx28, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %13 = load float, float* %call31, align 4
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 0
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx33)
  %14 = load float, float* %call34, align 4
  %sub35 = fsub float %13, %14
  %15 = load float, float* %s, align 4
  %mul36 = fmul float %sub35, %15
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  store float %mul36, float* %arrayidx37, align 8
  br label %if.end

if.else:                                          ; preds = %entry
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 0
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx39)
  %16 = load float, float* %call40, align 4
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 1
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx42)
  %17 = load float, float* %call43, align 4
  %cmp44 = fcmp olt float %16, %17
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx46)
  %18 = load float, float* %call47, align 4
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 2
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx49)
  %19 = load float, float* %call50, align 4
  %cmp51 = fcmp olt float %18, %19
  %20 = zext i1 %cmp51 to i64
  %cond = select i1 %cmp51, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 0
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %21 = load float, float* %call54, align 4
  %m_el55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el55, i32 0, i32 2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx56)
  %22 = load float, float* %call57, align 4
  %cmp58 = fcmp olt float %21, %22
  %23 = zext i1 %cmp58 to i64
  %cond59 = select i1 %cmp58, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond60 = phi i32 [ %cond, %cond.true ], [ %cond59, %cond.false ]
  store i32 %cond60, i32* %i, align 4
  %24 = load i32, i32* %i, align 4
  %add61 = add nsw i32 %24, 1
  %rem = srem i32 %add61, 3
  store i32 %rem, i32* %j, align 4
  %25 = load i32, i32* %i, align 4
  %add62 = add nsw i32 %25, 2
  %rem63 = srem i32 %add62, 3
  store i32 %rem63, i32* %k, align 4
  %m_el65 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %26 = load i32, i32* %i, align 4
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el65, i32 0, i32 %26
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx66)
  %27 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %27
  %28 = load float, float* %arrayidx68, align 4
  %m_el69 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %29 = load i32, i32* %j, align 4
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el69, i32 0, i32 %29
  %call71 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx70)
  %30 = load i32, i32* %j, align 4
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %30
  %31 = load float, float* %arrayidx72, align 4
  %sub73 = fsub float %28, %31
  %m_el74 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %32 = load i32, i32* %k, align 4
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el74, i32 0, i32 %32
  %call76 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx75)
  %33 = load i32, i32* %k, align 4
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %33
  %34 = load float, float* %arrayidx77, align 4
  %sub78 = fsub float %sub73, %34
  %add79 = fadd float %sub78, 1.000000e+00
  %call80 = call float @_Z6btSqrtf(float %add79)
  store float %call80, float* %s64, align 4
  %35 = load float, float* %s64, align 4
  %mul81 = fmul float %35, 5.000000e-01
  %36 = load i32, i32* %i, align 4
  %arrayidx82 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %36
  store float %mul81, float* %arrayidx82, align 4
  %37 = load float, float* %s64, align 4
  %div83 = fdiv float 5.000000e-01, %37
  store float %div83, float* %s64, align 4
  %m_el84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %38 = load i32, i32* %k, align 4
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el84, i32 0, i32 %38
  %call86 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx85)
  %39 = load i32, i32* %j, align 4
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %39
  %40 = load float, float* %arrayidx87, align 4
  %m_el88 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %41 = load i32, i32* %j, align 4
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el88, i32 0, i32 %41
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx89)
  %42 = load i32, i32* %k, align 4
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 %42
  %43 = load float, float* %arrayidx91, align 4
  %sub92 = fsub float %40, %43
  %44 = load float, float* %s64, align 4
  %mul93 = fmul float %sub92, %44
  %arrayidx94 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul93, float* %arrayidx94, align 4
  %m_el95 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %45 = load i32, i32* %j, align 4
  %arrayidx96 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el95, i32 0, i32 %45
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %46 = load i32, i32* %i, align 4
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %46
  %47 = load float, float* %arrayidx98, align 4
  %m_el99 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %48 = load i32, i32* %i, align 4
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el99, i32 0, i32 %48
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx100)
  %49 = load i32, i32* %j, align 4
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 %49
  %50 = load float, float* %arrayidx102, align 4
  %add103 = fadd float %47, %50
  %51 = load float, float* %s64, align 4
  %mul104 = fmul float %add103, %51
  %52 = load i32, i32* %j, align 4
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %52
  store float %mul104, float* %arrayidx105, align 4
  %m_el106 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %53 = load i32, i32* %k, align 4
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el106, i32 0, i32 %53
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx107)
  %54 = load i32, i32* %i, align 4
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 %54
  %55 = load float, float* %arrayidx109, align 4
  %m_el110 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %56 = load i32, i32* %i, align 4
  %arrayidx111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el110, i32 0, i32 %56
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx111)
  %57 = load i32, i32* %k, align 4
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 %57
  %58 = load float, float* %arrayidx113, align 4
  %add114 = fadd float %55, %58
  %59 = load float, float* %s64, align 4
  %mul115 = fmul float %add114, %59
  %60 = load i32, i32* %k, align 4
  %arrayidx116 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %60
  store float %mul115, float* %arrayidx116, align 4
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %61 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %62 = bitcast %class.btQuaternion* %61 to %class.btQuadWord*
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  %arrayidx120 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %62, float* nonnull align 4 dereferenceable(4) %arrayidx117, float* nonnull align 4 dereferenceable(4) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx119, float* nonnull align 4 dereferenceable(4) %arrayidx120)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 2
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  ret %class.btVector3* %m_linearVelocity
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btRigidBody16addConstraintRefEP17btTypedConstraint(%class.btRigidBody* %this, %class.btTypedConstraint* %c) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %c.addr = alloca %class.btTypedConstraint*, align 4
  %index = alloca i32, align 4
  %colObjA = alloca %class.btCollisionObject*, align 4
  %colObjB = alloca %class.btCollisionObject*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btTypedConstraint* %c, %class.btTypedConstraint** %c.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_(%class.btAlignedObjectArray.0* %m_constraintRefs, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %c.addr)
  store i32 %call, i32* %index, align 4
  %0 = load i32, i32* %index, align 4
  %m_constraintRefs2 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %m_constraintRefs2)
  %cmp = icmp eq i32 %0, %call3
  br i1 %cmp, label %if.then, label %if.end9

if.then:                                          ; preds = %entry
  %m_constraintRefs4 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_(%class.btAlignedObjectArray.0* %m_constraintRefs4, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %c.addr)
  %1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %c.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %1)
  %2 = bitcast %class.btRigidBody* %call5 to %class.btCollisionObject*
  store %class.btCollisionObject* %2, %class.btCollisionObject** %colObjA, align 4
  %3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %c.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %3)
  %4 = bitcast %class.btRigidBody* %call6 to %class.btCollisionObject*
  store %class.btCollisionObject* %4, %class.btCollisionObject** %colObjB, align 4
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %colObjA, align 4
  %6 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %cmp7 = icmp eq %class.btCollisionObject* %5, %6
  br i1 %cmp7, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.then
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %colObjA, align 4
  %8 = load %class.btCollisionObject*, %class.btCollisionObject** %colObjB, align 4
  call void @_ZN17btCollisionObject23setIgnoreCollisionCheckEPKS_b(%class.btCollisionObject* %7, %class.btCollisionObject* %8, i1 zeroext true)
  br label %if.end

if.else:                                          ; preds = %if.then
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %colObjB, align 4
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %colObjA, align 4
  call void @_ZN17btCollisionObject23setIgnoreCollisionCheckEPKS_b(%class.btCollisionObject* %9, %class.btCollisionObject* %10, i1 zeroext true)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then8
  br label %if.end9

if.end9:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_(%class.btAlignedObjectArray.0* %this, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %key) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %key.addr = alloca %class.btTypedConstraint**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store %class.btTypedConstraint** %key, %class.btTypedConstraint*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %1 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %1, i32 %2
  %3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx, align 4
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %key.addr, align 4
  %5 = load %class.btTypedConstraint*, %class.btTypedConstraint** %4, align 4
  %cmp3 = icmp eq %class.btTypedConstraint* %3, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  store i32 %6, i32* %index, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %8 = load i32, i32* %index, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_(%class.btAlignedObjectArray.0* %this, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %class.btTypedConstraint**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store %class.btTypedConstraint** %_Val, %class.btTypedConstraint*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %1 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %1, i32 %2
  %3 = bitcast %class.btTypedConstraint** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btTypedConstraint**
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %_Val.addr, align 4
  %6 = load %class.btTypedConstraint*, %class.btTypedConstraint** %5, align 4
  store %class.btTypedConstraint* %6, %class.btTypedConstraint** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN17btCollisionObject23setIgnoreCollisionCheckEPKS_b(%class.btCollisionObject* %this, %class.btCollisionObject* %co, i1 zeroext %ignoreCollisionCheck) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %co.addr = alloca %class.btCollisionObject*, align 4
  %ignoreCollisionCheck.addr = alloca i8, align 1
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store %class.btCollisionObject* %co, %class.btCollisionObject** %co.addr, align 4
  %frombool = zext i1 %ignoreCollisionCheck to i8
  store i8 %frombool, i8* %ignoreCollisionCheck.addr, align 1
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load i8, i8* %ignoreCollisionCheck.addr, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_objectsWithoutCollisionCheck = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 32
  call void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE9push_backERKS2_(%class.btAlignedObjectArray* %m_objectsWithoutCollisionCheck, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %co.addr)
  br label %if.end

if.else:                                          ; preds = %entry
  %m_objectsWithoutCollisionCheck2 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 32
  call void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE6removeERKS2_(%class.btAlignedObjectArray* %m_objectsWithoutCollisionCheck2, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %co.addr)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %m_objectsWithoutCollisionCheck3 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 32
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_objectsWithoutCollisionCheck3)
  %cmp = icmp sgt i32 %call, 0
  %conv = zext i1 %cmp to i32
  %m_checkCollideWith = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 31
  store i32 %conv, i32* %m_checkCollideWith, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN11btRigidBody19removeConstraintRefEP17btTypedConstraint(%class.btRigidBody* %this, %class.btTypedConstraint* %c) #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %c.addr = alloca %class.btTypedConstraint*, align 4
  %index = alloca i32, align 4
  %colObjA = alloca %class.btCollisionObject*, align 4
  %colObjB = alloca %class.btCollisionObject*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btTypedConstraint* %c, %class.btTypedConstraint** %c.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_(%class.btAlignedObjectArray.0* %m_constraintRefs, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %c.addr)
  store i32 %call, i32* %index, align 4
  %0 = load i32, i32* %index, align 4
  %m_constraintRefs2 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %m_constraintRefs2)
  %cmp = icmp slt i32 %0, %call3
  br i1 %cmp, label %if.then, label %if.end9

if.then:                                          ; preds = %entry
  %m_constraintRefs4 = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6removeERKS1_(%class.btAlignedObjectArray.0* %m_constraintRefs4, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %c.addr)
  %1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %c.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %1)
  %2 = bitcast %class.btRigidBody* %call5 to %class.btCollisionObject*
  store %class.btCollisionObject* %2, %class.btCollisionObject** %colObjA, align 4
  %3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %c.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %3)
  %4 = bitcast %class.btRigidBody* %call6 to %class.btCollisionObject*
  store %class.btCollisionObject* %4, %class.btCollisionObject** %colObjB, align 4
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %colObjA, align 4
  %6 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %cmp7 = icmp eq %class.btCollisionObject* %5, %6
  br i1 %cmp7, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.then
  %7 = load %class.btCollisionObject*, %class.btCollisionObject** %colObjA, align 4
  %8 = load %class.btCollisionObject*, %class.btCollisionObject** %colObjB, align 4
  call void @_ZN17btCollisionObject23setIgnoreCollisionCheckEPKS_b(%class.btCollisionObject* %7, %class.btCollisionObject* %8, i1 zeroext false)
  br label %if.end

if.else:                                          ; preds = %if.then
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %colObjB, align 4
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %colObjA, align 4
  call void @_ZN17btCollisionObject23setIgnoreCollisionCheckEPKS_b(%class.btCollisionObject* %9, %class.btCollisionObject* %10, i1 zeroext false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then8
  br label %if.end9

if.end9:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6removeERKS1_(%class.btAlignedObjectArray.0* %this, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %key) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %key.addr = alloca %class.btTypedConstraint**, align 4
  %findIndex = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store %class.btTypedConstraint** %key, %class.btTypedConstraint*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %key.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_(%class.btAlignedObjectArray.0* %this1, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %0)
  store i32 %call, i32* %findIndex, align 4
  %1 = load i32, i32* %findIndex, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE13removeAtIndexEi(%class.btAlignedObjectArray.0* %this1, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK11btRigidBody28calculateSerializeBufferSizeEv(%class.btRigidBody* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %sz = alloca i32, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  store i32 496, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden i8* @_ZNK11btRigidBody9serializeEPvP12btSerializer(%class.btRigidBody* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %rbd = alloca %struct.btRigidBodyFloatData*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btRigidBodyFloatData*
  store %struct.btRigidBodyFloatData* %1, %struct.btRigidBodyFloatData** %rbd, align 4
  %2 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %3 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_collisionObjectData = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %3, i32 0, i32 0
  %4 = bitcast %struct.btCollisionObjectFloatData* %m_collisionObjectData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK17btCollisionObject9serializeEPvP12btSerializer(%class.btCollisionObject* %2, i8* %4, %class.btSerializer* %5)
  %m_invInertiaTensorWorld = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 1
  %6 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_invInertiaTensorWorld2 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %6, i32 0, i32 1
  call void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_invInertiaTensorWorld, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_invInertiaTensorWorld2)
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  %7 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_linearVelocity3 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %7, i32 0, i32 2
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_linearVelocity, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearVelocity3)
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  %8 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_angularVelocity4 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %8, i32 0, i32 3
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_angularVelocity, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_angularVelocity4)
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %9 = load float, float* %m_inverseMass, align 4
  %10 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_inverseMass5 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %10, i32 0, i32 11
  store float %9, float* %m_inverseMass5, align 4
  %m_angularFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 26
  %11 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_angularFactor6 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %11, i32 0, i32 4
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_angularFactor, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_angularFactor6)
  %m_linearFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 5
  %12 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_linearFactor7 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %12, i32 0, i32 5
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_linearFactor, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_linearFactor7)
  %m_gravity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 6
  %13 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_gravity8 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %13, i32 0, i32 6
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_gravity, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_gravity8)
  %m_gravity_acceleration = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 7
  %14 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_gravity_acceleration9 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %14, i32 0, i32 7
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_gravity_acceleration, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_gravity_acceleration9)
  %m_invInertiaLocal = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 8
  %15 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_invInertiaLocal10 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %15, i32 0, i32 8
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_invInertiaLocal, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_invInertiaLocal10)
  %m_totalForce = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 9
  %16 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_totalForce11 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %16, i32 0, i32 9
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_totalForce, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_totalForce11)
  %m_totalTorque = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 10
  %17 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_totalTorque12 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %17, i32 0, i32 10
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_totalTorque, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_totalTorque12)
  %m_linearDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 11
  %18 = load float, float* %m_linearDamping, align 4
  %19 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_linearDamping13 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %19, i32 0, i32 12
  store float %18, float* %m_linearDamping13, align 4
  %m_angularDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 12
  %20 = load float, float* %m_angularDamping, align 4
  %21 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_angularDamping14 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %21, i32 0, i32 13
  store float %20, float* %m_angularDamping14, align 4
  %m_additionalDamping = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 13
  %22 = load i8, i8* %m_additionalDamping, align 4
  %tobool = trunc i8 %22 to i1
  %conv = zext i1 %tobool to i32
  %23 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_additionalDamping15 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %23, i32 0, i32 20
  store i32 %conv, i32* %m_additionalDamping15, align 4
  %m_additionalDampingFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 14
  %24 = load float, float* %m_additionalDampingFactor, align 4
  %25 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_additionalDampingFactor16 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %25, i32 0, i32 14
  store float %24, float* %m_additionalDampingFactor16, align 4
  %m_additionalLinearDampingThresholdSqr = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 15
  %26 = load float, float* %m_additionalLinearDampingThresholdSqr, align 4
  %27 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_additionalLinearDampingThresholdSqr17 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %27, i32 0, i32 15
  store float %26, float* %m_additionalLinearDampingThresholdSqr17, align 4
  %m_additionalAngularDampingThresholdSqr = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 16
  %28 = load float, float* %m_additionalAngularDampingThresholdSqr, align 4
  %29 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_additionalAngularDampingThresholdSqr18 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %29, i32 0, i32 16
  store float %28, float* %m_additionalAngularDampingThresholdSqr18, align 4
  %m_additionalAngularDampingFactor = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 17
  %30 = load float, float* %m_additionalAngularDampingFactor, align 4
  %31 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_additionalAngularDampingFactor19 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %31, i32 0, i32 17
  store float %30, float* %m_additionalAngularDampingFactor19, align 4
  %m_linearSleepingThreshold = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 18
  %32 = load float, float* %m_linearSleepingThreshold, align 4
  %33 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_linearSleepingThreshold20 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %33, i32 0, i32 18
  store float %32, float* %m_linearSleepingThreshold20, align 4
  %m_angularSleepingThreshold = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 19
  %34 = load float, float* %m_angularSleepingThreshold, align 4
  %35 = load %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData** %rbd, align 4
  %m_angularSleepingThreshold21 = getelementptr inbounds %struct.btRigidBodyFloatData, %struct.btRigidBodyFloatData* %35, i32 0, i32 19
  store float %34, float* %m_angularSleepingThreshold21, align 4
  ret i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str, i32 0, i32 0)
}

declare i8* @_ZNK17btCollisionObject9serializeEPvP12btSerializer(%class.btCollisionObject*, i8*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %1
  %2 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %2, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %3
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK11btRigidBody21serializeSingleObjectEP12btSerializer(%class.btRigidBody* %this, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %chunk = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %1 = bitcast %class.btRigidBody* %this1 to i32 (%class.btRigidBody*)***
  %vtable = load i32 (%class.btRigidBody*)**, i32 (%class.btRigidBody*)*** %1, align 4
  %vfn = getelementptr inbounds i32 (%class.btRigidBody*)*, i32 (%class.btRigidBody*)** %vtable, i64 4
  %2 = load i32 (%class.btRigidBody*)*, i32 (%class.btRigidBody*)** %vfn, align 4
  %call = call i32 %2(%class.btRigidBody* %this1)
  %3 = bitcast %class.btSerializer* %0 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable2 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %3, align 4
  %vfn3 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable2, i64 4
  %4 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn3, align 4
  %call4 = call %class.btChunk* %4(%class.btSerializer* %0, i32 %call, i32 1)
  store %class.btChunk* %call4, %class.btChunk** %chunk, align 4
  %5 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %5, i32 0, i32 2
  %6 = load i8*, i8** %m_oldPtr, align 4
  %7 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %8 = bitcast %class.btRigidBody* %this1 to i8* (%class.btRigidBody*, i8*, %class.btSerializer*)***
  %vtable5 = load i8* (%class.btRigidBody*, i8*, %class.btSerializer*)**, i8* (%class.btRigidBody*, i8*, %class.btSerializer*)*** %8, align 4
  %vfn6 = getelementptr inbounds i8* (%class.btRigidBody*, i8*, %class.btSerializer*)*, i8* (%class.btRigidBody*, i8*, %class.btSerializer*)** %vtable5, i64 5
  %9 = load i8* (%class.btRigidBody*, i8*, %class.btSerializer*)*, i8* (%class.btRigidBody*, i8*, %class.btSerializer*)** %vfn6, align 4
  %call7 = call i8* %9(%class.btRigidBody* %this1, i8* %6, %class.btSerializer* %7)
  store i8* %call7, i8** %structType, align 4
  %10 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %11 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %12 = load i8*, i8** %structType, align 4
  %13 = bitcast %class.btRigidBody* %this1 to i8*
  %14 = bitcast %class.btSerializer* %10 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable8 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %14, align 4
  %vfn9 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable8, i64 5
  %15 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn9, align 4
  call void %15(%class.btSerializer* %10, %class.btChunk* %11, i8* %12, i32 1497645650, i8* %13)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btRigidBody* @_ZN11btRigidBodyD2Ev(%class.btRigidBody* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV11btRigidBody, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.0* %m_constraintRefs) #8
  %1 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call2 = call %class.btCollisionObject* @_ZN17btCollisionObjectD2Ev(%class.btCollisionObject* %1) #8
  ret %class.btRigidBody* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btRigidBodyD0Ev(%class.btRigidBody* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %call = call %class.btRigidBody* @_ZN11btRigidBodyD2Ev(%class.btRigidBody* %this1) #8
  %0 = bitcast %class.btRigidBody* %this1 to i8*
  call void @_ZN17btCollisionObjectdlEPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape(%class.btCollisionObject* %this, %class.btCollisionShape* %collisionShape) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %collisionShape.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store %class.btCollisionShape* %collisionShape, %class.btCollisionShape** %collisionShape.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 33
  %0 = load i32, i32* %m_updateRevision, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  store %class.btCollisionShape* %1, %class.btCollisionShape** %m_collisionShape, align 4
  %2 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4
  %m_rootCollisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 11
  store %class.btCollisionShape* %2, %class.btCollisionShape** %m_rootCollisionShape, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject24checkCollideWithOverrideEPKS_(%class.btCollisionObject* %this, %class.btCollisionObject* %co) unnamed_addr #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btCollisionObject*, align 4
  %co.addr = alloca %class.btCollisionObject*, align 4
  %index = alloca i32, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store %class.btCollisionObject* %co, %class.btCollisionObject** %co.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_objectsWithoutCollisionCheck = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 32
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE16findLinearSearchERKS2_(%class.btAlignedObjectArray* %m_objectsWithoutCollisionCheck, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %co.addr)
  store i32 %call, i32* %index, align 4
  %0 = load i32, i32* %index, align 4
  %m_objectsWithoutCollisionCheck2 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 32
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_objectsWithoutCollisionCheck2)
  %cmp = icmp slt i32 %0, %call3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %1 = load i1, i1* %retval, align 1
  ret i1 %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btSinf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btCosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float*, float** %_z.addr, align 4
  %4 = load float*, float** %_w.addr, align 4
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q2) #2 comdat {
entry:
  %q1.addr = alloca %class.btQuaternion*, align 4
  %q2.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  store %class.btQuaternion* %q1, %class.btQuaternion** %q1.addr, align 4
  store %class.btQuaternion* %q2, %class.btQuaternion** %q2.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call1, align 4
  %mul = fmul float %2, %5
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %7)
  %8 = load float, float* %call2, align 4
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %10)
  %11 = load float, float* %call3, align 4
  %mul4 = fmul float %8, %11
  %add = fadd float %mul, %mul4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %13)
  %14 = load float, float* %call5, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call6, align 4
  %mul7 = fmul float %14, %17
  %add8 = fadd float %add, %mul7
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %22 = bitcast %class.btQuaternion* %21 to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %22)
  %23 = load float, float* %call10, align 4
  %mul11 = fmul float %20, %23
  %sub = fsub float %add8, %mul11
  store float %sub, float* %ref.tmp, align 4
  %24 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %25 = bitcast %class.btQuaternion* %24 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %25)
  %26 = load float, float* %call13, align 4
  %27 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %28 = bitcast %class.btQuaternion* %27 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %28)
  %29 = load float, float* %call14, align 4
  %mul15 = fmul float %26, %29
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %31)
  %32 = load float, float* %call16, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %34)
  %35 = load float, float* %call17, align 4
  %mul18 = fmul float %32, %35
  %add19 = fadd float %mul15, %mul18
  %36 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %37 = bitcast %class.btQuaternion* %36 to %class.btQuadWord*
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %37)
  %38 = load float, float* %call20, align 4
  %39 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %40 = bitcast %class.btQuaternion* %39 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %40)
  %41 = load float, float* %call21, align 4
  %mul22 = fmul float %38, %41
  %add23 = fadd float %add19, %mul22
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %43)
  %44 = load float, float* %call24, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %46)
  %47 = load float, float* %call25, align 4
  %mul26 = fmul float %44, %47
  %sub27 = fsub float %add23, %mul26
  store float %sub27, float* %ref.tmp12, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %49)
  %50 = load float, float* %call29, align 4
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %52)
  %53 = load float, float* %call30, align 4
  %mul31 = fmul float %50, %53
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %55)
  %56 = load float, float* %call32, align 4
  %57 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %58 = bitcast %class.btQuaternion* %57 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %58)
  %59 = load float, float* %call33, align 4
  %mul34 = fmul float %56, %59
  %add35 = fadd float %mul31, %mul34
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %61)
  %62 = load float, float* %call36, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %64)
  %65 = load float, float* %call37, align 4
  %mul38 = fmul float %62, %65
  %add39 = fadd float %add35, %mul38
  %66 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %67 = bitcast %class.btQuaternion* %66 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %67)
  %68 = load float, float* %call40, align 4
  %69 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %70 = bitcast %class.btQuaternion* %69 to %class.btQuadWord*
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %70)
  %71 = load float, float* %call41, align 4
  %mul42 = fmul float %68, %71
  %sub43 = fsub float %add39, %mul42
  store float %sub43, float* %ref.tmp28, align 4
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %73)
  %74 = load float, float* %call45, align 4
  %75 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %76 = bitcast %class.btQuaternion* %75 to %class.btQuadWord*
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %76)
  %77 = load float, float* %call46, align 4
  %mul47 = fmul float %74, %77
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %79)
  %80 = load float, float* %call48, align 4
  %81 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %82 = bitcast %class.btQuaternion* %81 to %class.btQuadWord*
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %82)
  %83 = load float, float* %call49, align 4
  %mul50 = fmul float %80, %83
  %sub51 = fsub float %mul47, %mul50
  %84 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %85 = bitcast %class.btQuaternion* %84 to %class.btQuadWord*
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %85)
  %86 = load float, float* %call52, align 4
  %87 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %88 = bitcast %class.btQuaternion* %87 to %class.btQuadWord*
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %88)
  %89 = load float, float* %call53, align 4
  %mul54 = fmul float %86, %89
  %sub55 = fsub float %sub51, %mul54
  %90 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %91 = bitcast %class.btQuaternion* %90 to %class.btQuadWord*
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %91)
  %92 = load float, float* %call56, align 4
  %93 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %94 = bitcast %class.btQuaternion* %93 to %class.btQuadWord*
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %94)
  %95 = load float, float* %call57, align 4
  %mul58 = fmul float %92, %95
  %sub59 = fsub float %sub55, %mul58
  store float %sub59, float* %ref.tmp44, align 4
  %call60 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btQuaternion* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setRotationERK12btQuaternion(%class.btTransform* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #5

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion6lengthEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaterniondVERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btQuaternion* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKf(%class.btQuaternion* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %3 = load float, float* %arrayidx, align 4
  %mul = fmul float %3, %1
  store float %mul, float* %arrayidx, align 4
  %4 = load float*, float** %s.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %7 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %7, %5
  store float %mul4, float* %arrayidx3, align 4
  %8 = load float*, float** %s.addr, align 4
  %9 = load float, float* %8, align 4
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats5 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %11 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %11, %9
  store float %mul7, float* %arrayidx6, align 4
  %12 = load float*, float** %s.addr, align 4
  %13 = load float, float* %12, align 4
  %14 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats8 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %14, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 3
  %15 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %15, %13
  store float %mul10, float* %arrayidx9, align 4
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %0)
  store float %call, float* %d, align 4
  %1 = load float, float* %d, align 4
  %div = fdiv float 2.000000e+00, %1
  store float %div, float* %s, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call2, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %4, %5
  store float %mul, float* %xs, align 4
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call3, align 4
  %9 = load float, float* %s, align 4
  %mul4 = fmul float %8, %9
  store float %mul4, float* %ys, align 4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load float, float* %s, align 4
  %mul6 = fmul float %12, %13
  store float %mul6, float* %zs, align 4
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %15)
  %16 = load float, float* %call7, align 4
  %17 = load float, float* %xs, align 4
  %mul8 = fmul float %16, %17
  store float %mul8, float* %wx, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load float, float* %ys, align 4
  %mul10 = fmul float %20, %21
  store float %mul10, float* %wy, align 4
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %23)
  %24 = load float, float* %call11, align 4
  %25 = load float, float* %zs, align 4
  %mul12 = fmul float %24, %25
  store float %mul12, float* %wz, align 4
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4
  %29 = load float, float* %xs, align 4
  %mul14 = fmul float %28, %29
  store float %mul14, float* %xx, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %31)
  %32 = load float, float* %call15, align 4
  %33 = load float, float* %ys, align 4
  %mul16 = fmul float %32, %33
  store float %mul16, float* %xy, align 4
  %34 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %35 = bitcast %class.btQuaternion* %34 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %35)
  %36 = load float, float* %call17, align 4
  %37 = load float, float* %zs, align 4
  %mul18 = fmul float %36, %37
  store float %mul18, float* %xz, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %39)
  %40 = load float, float* %call19, align 4
  %41 = load float, float* %ys, align 4
  %mul20 = fmul float %40, %41
  store float %mul20, float* %yy, align 4
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %43)
  %44 = load float, float* %call21, align 4
  %45 = load float, float* %zs, align 4
  %mul22 = fmul float %44, %45
  store float %mul22, float* %yz, align 4
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call23, align 4
  %49 = load float, float* %zs, align 4
  %mul24 = fmul float %48, %49
  store float %mul24, float* %zz, align 4
  %50 = load float, float* %yy, align 4
  %51 = load float, float* %zz, align 4
  %add = fadd float %50, %51
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4
  %52 = load float, float* %xy, align 4
  %53 = load float, float* %wz, align 4
  %sub26 = fsub float %52, %53
  store float %sub26, float* %ref.tmp25, align 4
  %54 = load float, float* %xz, align 4
  %55 = load float, float* %wy, align 4
  %add28 = fadd float %54, %55
  store float %add28, float* %ref.tmp27, align 4
  %56 = load float, float* %xy, align 4
  %57 = load float, float* %wz, align 4
  %add30 = fadd float %56, %57
  store float %add30, float* %ref.tmp29, align 4
  %58 = load float, float* %xx, align 4
  %59 = load float, float* %zz, align 4
  %add32 = fadd float %58, %59
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4
  %60 = load float, float* %yz, align 4
  %61 = load float, float* %wx, align 4
  %sub35 = fsub float %60, %61
  store float %sub35, float* %ref.tmp34, align 4
  %62 = load float, float* %xz, align 4
  %63 = load float, float* %wy, align 4
  %sub37 = fsub float %62, %63
  store float %sub37, float* %ref.tmp36, align 4
  %64 = load float, float* %yz, align 4
  %65 = load float, float* %wx, align 4
  %add39 = fadd float %64, %65
  store float %add39, float* %ref.tmp38, align 4
  %66 = load float, float* %xx, align 4
  %67 = load float, float* %yy, align 4
  %add41 = fadd float %66, %67
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  %2 = load float, float* %1, align 4
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf(%class.btTransform* nonnull align 4 dereferenceable(64) %transform0, %class.btTransform* nonnull align 4 dereferenceable(64) %transform1, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %angle) #2 comdat {
entry:
  %transform0.addr = alloca %class.btTransform*, align 4
  %transform1.addr = alloca %class.btTransform*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %angle.addr = alloca float*, align 4
  %dmat = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %dorn = alloca %class.btQuaternion, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %len = alloca float, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  store %class.btTransform* %transform0, %class.btTransform** %transform0.addr, align 4
  store %class.btTransform* %transform1, %class.btTransform** %transform1.addr, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4
  store float* %angle, float** %angle.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transform1.addr, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %0)
  %1 = load %class.btTransform*, %class.btTransform** %transform0.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %1)
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call1)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %dmat, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp)
  %call2 = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %dorn)
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %dmat, %class.btQuaternion* nonnull align 4 dereferenceable(16) %dorn)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternion9normalizeEv(%class.btQuaternion* %dorn)
  %call4 = call float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %dorn)
  %2 = load float*, float** %angle.addr, align 4
  store float %call4, float* %2, align 4
  %3 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %4)
  %5 = bitcast %class.btQuaternion* %dorn to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %5)
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %call6, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call8)
  %6 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %7 = bitcast %class.btVector3* %6 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %9)
  %arrayidx = getelementptr inbounds float, float* %call10, i32 3
  store float 0.000000e+00, float* %arrayidx, align 4
  %10 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call11 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %10)
  store float %call11, float* %len, align 4
  %11 = load float, float* %len, align 4
  %cmp = fcmp olt float %11, 0x3D10000000000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store float 1.000000e+00, float* %ref.tmp13, align 4
  store float 0.000000e+00, float* %ref.tmp14, align 4
  store float 0.000000e+00, float* %ref.tmp15, align 4
  %call16 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %12 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %13 = bitcast %class.btVector3* %12 to i8*
  %14 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %15 = load float, float* %len, align 4
  %call18 = call float @_Z6btSqrtf(float %15)
  store float %call18, float* %ref.tmp17, align 4
  %16 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %16, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %co = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %det = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %call = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 1, i32 2, i32 2)
  store float %call, float* %ref.tmp, align 4
  %call3 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 2, i32 2, i32 0)
  store float %call3, float* %ref.tmp2, align 4
  %call5 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 0, i32 2, i32 1)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %co, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this1, i32 0)
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %co)
  store float %call8, float* %det, align 4
  %1 = load float, float* %det, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %s, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %co)
  %2 = load float, float* %call10, align 4
  %3 = load float, float* %s, align 4
  %mul = fmul float %2, %3
  store float %mul, float* %ref.tmp9, align 4
  %call12 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 2, i32 1)
  %4 = load float, float* %s, align 4
  %mul13 = fmul float %call12, %4
  store float %mul13, float* %ref.tmp11, align 4
  %call15 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 1, i32 2)
  %5 = load float, float* %s, align 4
  %mul16 = fmul float %call15, %5
  store float %mul16, float* %ref.tmp14, align 4
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %co)
  %6 = load float, float* %call18, align 4
  %7 = load float, float* %s, align 4
  %mul19 = fmul float %6, %7
  store float %mul19, float* %ref.tmp17, align 4
  %call21 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 2, i32 2)
  %8 = load float, float* %s, align 4
  %mul22 = fmul float %call21, %8
  store float %mul22, float* %ref.tmp20, align 4
  %call24 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 1, i32 0)
  %9 = load float, float* %s, align 4
  %mul25 = fmul float %call24, %9
  store float %mul25, float* %ref.tmp23, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %co)
  %10 = load float, float* %call27, align 4
  %11 = load float, float* %s, align 4
  %mul28 = fmul float %10, %11
  store float %mul28, float* %ref.tmp26, align 4
  %call30 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 2, i32 0)
  %12 = load float, float* %s, align 4
  %mul31 = fmul float %call30, %12
  store float %mul31, float* %ref.tmp29, align 4
  %call33 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 1, i32 1)
  %13 = load float, float* %s, align 4
  %mul34 = fmul float %call33, %13
  store float %mul34, float* %ref.tmp32, align 4
  %call35 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion8getAngleEv(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %s = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %1 = load float, float* %arrayidx, align 4
  %call = call float @_Z6btAcosf(float %1)
  %mul = fmul float 2.000000e+00, %call
  store float %mul, float* %s, align 4
  %2 = load float, float* %s, align 4
  ret float %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this, i32 %r1, i32 %c1, i32 %r2, i32 %c2) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %r1.addr = alloca i32, align 4
  %c1.addr = alloca i32, align 4
  %r2.addr = alloca i32, align 4
  %c2.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %r1, i32* %r1.addr, align 4
  store i32 %c1, i32* %c1.addr, align 4
  store i32 %r2, i32* %r2.addr, align 4
  store i32 %c2, i32* %c2.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %r1.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %1 = load i32, i32* %c1.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx2, align 4
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %r2.addr, align 4
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 %3
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %4 = load i32, i32* %c2.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %4
  %5 = load float, float* %arrayidx6, align 4
  %mul = fmul float %2, %5
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %6 = load i32, i32* %r1.addr, align 4
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 %6
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %7 = load i32, i32* %c2.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %7
  %8 = load float, float* %arrayidx10, align 4
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %9 = load i32, i32* %r2.addr, align 4
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 %9
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx12)
  %10 = load i32, i32* %c1.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %10
  %11 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %8, %11
  %sub = fsub float %mul, %mul15
  ret float %sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btAcosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %cmp = fcmp olt float %0, -1.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float -1.000000e+00, float* %x.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load float, float* %x.addr, align 4
  %cmp1 = fcmp ogt float %1, 1.000000e+00
  br i1 %cmp1, label %if.then2, label %if.end3

if.then2:                                         ; preds = %if.end
  store float 1.000000e+00, float* %x.addr, align 4
  br label %if.end3

if.end3:                                          ; preds = %if.then2, %if.end
  %2 = load float, float* %x.addr, align 4
  %call = call float @acosf(float %2) #9
  ret float %call
}

; Function Attrs: nounwind readnone
declare float @acosf(float) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.pow.f32(float, float) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %w) #2 comdat {
entry:
  %q.addr = alloca %class.btQuaternion*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %3)
  %4 = load float, float* %call1, align 4
  %mul = fmul float %2, %4
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4
  %8 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %8)
  %9 = load float, float* %call3, align 4
  %mul4 = fmul float %7, %9
  %add = fadd float %mul, %mul4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %13)
  %14 = load float, float* %call6, align 4
  %mul7 = fmul float %12, %14
  %sub = fsub float %add, %mul7
  store float %sub, float* %ref.tmp, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %16)
  %17 = load float, float* %call9, align 4
  %18 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %18)
  %19 = load float, float* %call10, align 4
  %mul11 = fmul float %17, %19
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %21)
  %22 = load float, float* %call12, align 4
  %23 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %23)
  %24 = load float, float* %call13, align 4
  %mul14 = fmul float %22, %24
  %add15 = fadd float %mul11, %mul14
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %26)
  %27 = load float, float* %call16, align 4
  %28 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %28)
  %29 = load float, float* %call17, align 4
  %mul18 = fmul float %27, %29
  %sub19 = fsub float %add15, %mul18
  store float %sub19, float* %ref.tmp8, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call21, align 4
  %33 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %33)
  %34 = load float, float* %call22, align 4
  %mul23 = fmul float %32, %34
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call24, align 4
  %38 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %38)
  %39 = load float, float* %call25, align 4
  %mul26 = fmul float %37, %39
  %add27 = fadd float %mul23, %mul26
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %41)
  %42 = load float, float* %call28, align 4
  %43 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %43)
  %44 = load float, float* %call29, align 4
  %mul30 = fmul float %42, %44
  %sub31 = fsub float %add27, %mul30
  store float %sub31, float* %ref.tmp20, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call33, align 4
  %fneg = fneg float %47
  %48 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %48)
  %49 = load float, float* %call34, align 4
  %mul35 = fmul float %fneg, %49
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call36, align 4
  %53 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %53)
  %54 = load float, float* %call37, align 4
  %mul38 = fmul float %52, %54
  %sub39 = fsub float %mul35, %mul38
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %56)
  %57 = load float, float* %call40, align 4
  %58 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %58)
  %59 = load float, float* %call41, align 4
  %mul42 = fmul float %57, %59
  %sub43 = fsub float %sub39, %mul42
  store float %sub43, float* %ref.tmp32, align 4
  %call44 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp58 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %2 = load float, float* %arrayidx, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call, align 4
  %mul = fmul float %2, %5
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %7 = load float, float* %arrayidx3, align 4
  %8 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %9 = bitcast %class.btQuaternion* %8 to %class.btQuadWord*
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %9, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 3
  %10 = load float, float* %arrayidx5, align 4
  %mul6 = fmul float %7, %10
  %add = fadd float %mul, %mul6
  %11 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %11, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %12 = load float, float* %arrayidx8, align 4
  %13 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %14 = bitcast %class.btQuaternion* %13 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %14)
  %15 = load float, float* %call9, align 4
  %mul10 = fmul float %12, %15
  %add11 = fadd float %add, %mul10
  %16 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats12 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %16, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %17 = load float, float* %arrayidx13, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %19)
  %20 = load float, float* %call14, align 4
  %mul15 = fmul float %17, %20
  %sub = fsub float %add11, %mul15
  store float %sub, float* %ref.tmp, align 4
  %21 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats17 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %21, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 3
  %22 = load float, float* %arrayidx18, align 4
  %23 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %24 = bitcast %class.btQuaternion* %23 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %24)
  %25 = load float, float* %call19, align 4
  %mul20 = fmul float %22, %25
  %26 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats21 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %26, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 1
  %27 = load float, float* %arrayidx22, align 4
  %28 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %29 = bitcast %class.btQuaternion* %28 to %class.btQuadWord*
  %m_floats23 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %29, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 3
  %30 = load float, float* %arrayidx24, align 4
  %mul25 = fmul float %27, %30
  %add26 = fadd float %mul20, %mul25
  %31 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats27 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %31, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 2
  %32 = load float, float* %arrayidx28, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %34)
  %35 = load float, float* %call29, align 4
  %mul30 = fmul float %32, %35
  %add31 = fadd float %add26, %mul30
  %36 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats32 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %36, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [4 x float], [4 x float]* %m_floats32, i32 0, i32 0
  %37 = load float, float* %arrayidx33, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %39)
  %40 = load float, float* %call34, align 4
  %mul35 = fmul float %37, %40
  %sub36 = fsub float %add31, %mul35
  store float %sub36, float* %ref.tmp16, align 4
  %41 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats38 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %41, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [4 x float], [4 x float]* %m_floats38, i32 0, i32 3
  %42 = load float, float* %arrayidx39, align 4
  %43 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %44 = bitcast %class.btQuaternion* %43 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %44)
  %45 = load float, float* %call40, align 4
  %mul41 = fmul float %42, %45
  %46 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats42 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %46, i32 0, i32 0
  %arrayidx43 = getelementptr inbounds [4 x float], [4 x float]* %m_floats42, i32 0, i32 2
  %47 = load float, float* %arrayidx43, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %m_floats44 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %49, i32 0, i32 0
  %arrayidx45 = getelementptr inbounds [4 x float], [4 x float]* %m_floats44, i32 0, i32 3
  %50 = load float, float* %arrayidx45, align 4
  %mul46 = fmul float %47, %50
  %add47 = fadd float %mul41, %mul46
  %51 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats48 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %51, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [4 x float], [4 x float]* %m_floats48, i32 0, i32 0
  %52 = load float, float* %arrayidx49, align 4
  %53 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %54 = bitcast %class.btQuaternion* %53 to %class.btQuadWord*
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %54)
  %55 = load float, float* %call50, align 4
  %mul51 = fmul float %52, %55
  %add52 = fadd float %add47, %mul51
  %56 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats53 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %56, i32 0, i32 0
  %arrayidx54 = getelementptr inbounds [4 x float], [4 x float]* %m_floats53, i32 0, i32 1
  %57 = load float, float* %arrayidx54, align 4
  %58 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %59 = bitcast %class.btQuaternion* %58 to %class.btQuadWord*
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %59)
  %60 = load float, float* %call55, align 4
  %mul56 = fmul float %57, %60
  %sub57 = fsub float %add52, %mul56
  store float %sub57, float* %ref.tmp37, align 4
  %61 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats59 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %61, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [4 x float], [4 x float]* %m_floats59, i32 0, i32 3
  %62 = load float, float* %arrayidx60, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %m_floats61 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %64, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [4 x float], [4 x float]* %m_floats61, i32 0, i32 3
  %65 = load float, float* %arrayidx62, align 4
  %mul63 = fmul float %62, %65
  %66 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats64 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %66, i32 0, i32 0
  %arrayidx65 = getelementptr inbounds [4 x float], [4 x float]* %m_floats64, i32 0, i32 0
  %67 = load float, float* %arrayidx65, align 4
  %68 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %69 = bitcast %class.btQuaternion* %68 to %class.btQuadWord*
  %call66 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %69)
  %70 = load float, float* %call66, align 4
  %mul67 = fmul float %67, %70
  %sub68 = fsub float %mul63, %mul67
  %71 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats69 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %71, i32 0, i32 0
  %arrayidx70 = getelementptr inbounds [4 x float], [4 x float]* %m_floats69, i32 0, i32 1
  %72 = load float, float* %arrayidx70, align 4
  %73 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %74 = bitcast %class.btQuaternion* %73 to %class.btQuadWord*
  %call71 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %74)
  %75 = load float, float* %call71, align 4
  %mul72 = fmul float %72, %75
  %sub73 = fsub float %sub68, %mul72
  %76 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats74 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %76, i32 0, i32 0
  %arrayidx75 = getelementptr inbounds [4 x float], [4 x float]* %m_floats74, i32 0, i32 2
  %77 = load float, float* %arrayidx75, align 4
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %79)
  %80 = load float, float* %call76, align 4
  %mul77 = fmul float %77, %80
  %sub78 = fsub float %sub73, %mul77
  store float %sub78, float* %ref.tmp58, align 4
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp58)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %2 = load i32, i32* %i.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #1 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z7btCrossRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE9push_backERKS2_(%class.btAlignedObjectArray* %this, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btCollisionObject**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btCollisionObject** %_Val, %class.btCollisionObject*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %1, i32 %2
  %3 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btCollisionObject**
  %5 = load %class.btCollisionObject**, %class.btCollisionObject*** %_Val.addr, align 4
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %5, align 4
  store %class.btCollisionObject* %6, %class.btCollisionObject** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE6removeERKS2_(%class.btAlignedObjectArray* %this, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %key) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %key.addr = alloca %class.btCollisionObject**, align 4
  %findIndex = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btCollisionObject** %key, %class.btCollisionObject*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %key.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE16findLinearSearchERKS2_(%class.btAlignedObjectArray* %this1, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %0)
  store i32 %call, i32* %findIndex, align 4
  %1 = load i32, i32* %findIndex, align 4
  call void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE13removeAtIndexEi(%class.btAlignedObjectArray* %this1, i32 %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btCollisionObject**
  store %class.btCollisionObject** %2, %class.btCollisionObject*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4copyEiiPS2_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btCollisionObject** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** %4, %class.btCollisionObject*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btCollisionObject** @_ZN18btAlignedAllocatorIPK17btCollisionObjectLj16EE8allocateEiPPKS2_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btCollisionObject*** null)
  %2 = bitcast %class.btCollisionObject** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4copyEiiPS2_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btCollisionObject** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btCollisionObject**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btCollisionObject** %dest, %class.btCollisionObject*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %3, i32 %4
  %5 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btCollisionObject**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %7, i32 %8
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx2, align 4
  store %class.btCollisionObject* %9, %class.btCollisionObject** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %tobool = icmp ne %class.btCollisionObject** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIPK17btCollisionObjectLj16EE10deallocateEPS2_(%class.btAlignedAllocator* %m_allocator, %class.btCollisionObject** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionObject** @_ZN18btAlignedAllocatorIPK17btCollisionObjectLj16EE8allocateEiPPKS2_(%class.btAlignedAllocator* %this, i32 %n, %class.btCollisionObject*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btCollisionObject***, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btCollisionObject*** %hint, %class.btCollisionObject**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btCollisionObject**
  ret %class.btCollisionObject** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPK17btCollisionObjectLj16EE10deallocateEPS2_(%class.btAlignedAllocator* %this, %class.btCollisionObject** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.btCollisionObject** %ptr, %class.btCollisionObject*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %ptr.addr, align 4
  %1 = bitcast %class.btCollisionObject** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE16findLinearSearchERKS2_(%class.btAlignedObjectArray* %this, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %key) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %key.addr = alloca %class.btCollisionObject**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btCollisionObject** %key, %class.btCollisionObject*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %1, i32 %2
  %3 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx, align 4
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %key.addr, align 4
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %4, align 4
  %cmp3 = icmp eq %class.btCollisionObject* %3, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  store i32 %6, i32* %index, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %8 = load i32, i32* %index, align 4
  ret i32 %8
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE13removeAtIndexEi(%class.btAlignedObjectArray* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %index.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %index.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE4swapEii(%class.btAlignedObjectArray* %this1, i32 %1, i32 %sub)
  call void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE8pop_backEv(%class.btAlignedObjectArray* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE4swapEii(%class.btAlignedObjectArray* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btCollisionObject*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %0, i32 %1
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx, align 4
  store %class.btCollisionObject* %2, %class.btCollisionObject** %temp, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data2, align 4
  %4 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %3, i32 %4
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx3, align 4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %6 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %6, i32 %7
  store %class.btCollisionObject* %5, %class.btCollisionObject** %arrayidx5, align 4
  %8 = load %class.btCollisionObject*, %class.btCollisionObject** %temp, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %9 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data6, align 4
  %10 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %9, i32 %10
  store %class.btCollisionObject* %8, %class.btCollisionObject** %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE8pop_backEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %1, i32 %2
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
declare %class.btCollisionObject* @_ZN17btCollisionObjectD2Ev(%class.btCollisionObject* returned) unnamed_addr #7

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %tobool = icmp ne %class.btTypedConstraint** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %m_allocator, %class.btTypedConstraint** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %this, %class.btTypedConstraint** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %class.btTypedConstraint** %ptr, %class.btTypedConstraint*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %ptr.addr, align 4
  %1 = bitcast %class.btTypedConstraint** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btCollisionObjectdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btTypedConstraint**
  store %class.btTypedConstraint** %2, %class.btTypedConstraint*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %class.btTypedConstraint** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btTypedConstraint** %4, %class.btTypedConstraint*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btTypedConstraint** @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %class.btTypedConstraint*** null)
  %2 = bitcast %class.btTypedConstraint** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %class.btTypedConstraint** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btTypedConstraint**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btTypedConstraint** %dest, %class.btTypedConstraint*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %3, i32 %4
  %5 = bitcast %class.btTypedConstraint** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btTypedConstraint**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %7, i32 %8
  %9 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx2, align 4
  store %class.btTypedConstraint* %9, %class.btTypedConstraint** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTypedConstraint** @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.1* %this, i32 %n, %class.btTypedConstraint*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btTypedConstraint***, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btTypedConstraint*** %hint, %class.btTypedConstraint**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btTypedConstraint**
  ret %class.btTypedConstraint** %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE13removeAtIndexEi(%class.btAlignedObjectArray.0* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %index.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %index.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii(%class.btAlignedObjectArray.0* %this1, i32 %1, i32 %sub)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8pop_backEv(%class.btAlignedObjectArray.0* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4swapEii(%class.btAlignedObjectArray.0* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btTypedConstraint*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %0, i32 %1
  %2 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx, align 4
  store %class.btTypedConstraint* %2, %class.btTypedConstraint** %temp, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data2, align 4
  %4 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %3, i32 %4
  %5 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx3, align 4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %6 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %6, i32 %7
  store %class.btTypedConstraint* %5, %class.btTypedConstraint** %arrayidx5, align 4
  %8 = load %class.btTypedConstraint*, %class.btTypedConstraint** %temp, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %9 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data6, align 4
  %10 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %9, i32 %10
  store %class.btTypedConstraint* %8, %class.btTypedConstraint** %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8pop_backEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %1 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %1, i32 %2
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btRigidBody.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
