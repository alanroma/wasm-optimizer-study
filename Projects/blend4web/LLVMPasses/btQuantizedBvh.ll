; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/BroadphaseCollision/btQuantizedBvh.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/BroadphaseCollision/btQuantizedBvh.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btQuantizedBvh = type { i32 (...)**, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, i8, [3 x i8], %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, i32, %class.btAlignedObjectArray.4, i32 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btOptimizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btOptimizedBvhNode = type { %class.btVector3, %class.btVector3, i32, i32, i32, [20 x i8] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btQuantizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btQuantizedBvhNode = type { [3 x i16], [3 x i16], i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btBvhSubtreeInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btBvhSubtreeInfo = type { [3 x i16], [3 x i16], i32, i32, [3 x i32] }
%class.btNodeOverlapCallback = type { i32 (...)** }
%struct.btQuantizedBvhFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeFloatData*, %struct.btQuantizedBvhNodeData*, %struct.btBvhSubtreeInfoData*, i32, i32 }
%struct.btVector3FloatData = type { [4 x float] }
%struct.btOptimizedBvhNodeFloatData = type { %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32, i32, [4 x i8] }
%struct.btQuantizedBvhNodeData = type { [3 x i16], [3 x i16], i32 }
%struct.btBvhSubtreeInfoData = type { i32, i32, [3 x i16], [3 x i16] }
%struct.btQuantizedBvhDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, i32, %struct.btOptimizedBvhNodeDoubleData*, %struct.btQuantizedBvhNodeData*, i32, i32, %struct.btBvhSubtreeInfoData* }
%struct.btVector3DoubleData = type { [4 x double] }
%struct.btOptimizedBvhNodeDoubleData = type { %struct.btVector3DoubleData, %struct.btVector3DoubleData, i32, i32, i32, [4 x i8] }
%class.btSerializer = type { i32 (...)** }
%class.btChunk = type { i32, i32, i8*, i32, i32 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEC2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_ = comdat any

$_ZN16btBvhSubtreeInfoC2Ev = comdat any

$_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi = comdat any

$_ZNK18btQuantizedBvhNode10isLeafNodeEv = comdat any

$_ZNK18btQuantizedBvhNode14getEscapeIndexEv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZdvRK9btVector3S1_ = comdat any

$_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i = comdat any

$_ZNK14btQuantizedBvh10unQuantizeEPKt = comdat any

$_ZN9btVector36setMinERKS_ = comdat any

$_ZN9btVector36setMaxERKS_ = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoED2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeED2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeED2Ev = comdat any

$_ZN14btQuantizedBvhdlEPv = comdat any

$_ZN14btQuantizedBvh22setInternalNodeAabbMinEiRK9btVector3 = comdat any

$_ZN14btQuantizedBvh22setInternalNodeAabbMaxEiRK9btVector3 = comdat any

$_ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_ = comdat any

$_ZNK14btQuantizedBvh10getAabbMinEi = comdat any

$_ZNK14btQuantizedBvh10getAabbMaxEi = comdat any

$_ZN14btQuantizedBvh26setInternalNodeEscapeIndexEii = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZNK14btQuantizedBvh17quantizeWithClampEPtRK9btVector3i = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi = comdat any

$_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi = comdat any

$_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_ = comdat any

$_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_ = comdat any

$_ZNK18btQuantizedBvhNode9getPartIdEv = comdat any

$_ZNK18btQuantizedBvhNode16getTriangleIndexEv = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff = comdat any

$_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi = comdat any

$_ZN14btQuantizedBvhnwEmPv = comdat any

$_Z12btSwapEndiani = comdat any

$_Z19btSwapVector3EndianRK9btVector3RS_ = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE20initializeFromBufferEPvii = comdat any

$_Z12btSwapEndiant = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE20initializeFromBufferEPvii = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi = comdat any

$_Z21btUnSwapVector3EndianR9btVector3 = comdat any

$_ZN9btVector316deSerializeFloatERK18btVector3FloatData = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE6resizeEiRKS0_ = comdat any

$_ZN18btOptimizedBvhNodeC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6resizeEiRKS0_ = comdat any

$_ZN9btVector317deSerializeDoubleERK19btVector3DoubleData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

$_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv = comdat any

$_ZNK14btQuantizedBvh31calculateSerializeBufferSizeNewEv = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_ZNK9btVector31wEv = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_Z8btSelectjii = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_Z12btSwapEndianj = comdat any

$_Z18btSwapScalarEndianRKfRf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE4initEv = comdat any

$_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE4initEv = comdat any

$_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE4initEv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi = comdat any

$_ZN18btQuantizedBvhNodenwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE9allocSizeEi = comdat any

$_ZN16btBvhSubtreeInfonwEmPv = comdat any

$_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi = comdat any

$_ZN18btOptimizedBvhNodenwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE8allocateEiPPKS0_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV14btQuantizedBvh = hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI14btQuantizedBvh to i8*), i8* bitcast (%class.btQuantizedBvh* (%class.btQuantizedBvh*)* @_ZN14btQuantizedBvhD1Ev to i8*), i8* bitcast (void (%class.btQuantizedBvh*)* @_ZN14btQuantizedBvhD0Ev to i8*), i8* bitcast (i1 (%class.btQuantizedBvh*, i8*, i32, i1)* @_ZNK14btQuantizedBvh9serializeEPvjb to i8*), i8* bitcast (i32 (%class.btQuantizedBvh*)* @_ZNK14btQuantizedBvh31calculateSerializeBufferSizeNewEv to i8*), i8* bitcast (i8* (%class.btQuantizedBvh*, i8*, %class.btSerializer*)* @_ZNK14btQuantizedBvh9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btQuantizedBvh*, %struct.btQuantizedBvhFloatData*)* @_ZN14btQuantizedBvh16deSerializeFloatER23btQuantizedBvhFloatData to i8*), i8* bitcast (void (%class.btQuantizedBvh*, %struct.btQuantizedBvhDoubleData*)* @_ZN14btQuantizedBvh17deSerializeDoubleER24btQuantizedBvhDoubleData to i8*)] }, align 4
@maxIterations = hidden global i32 0, align 4
@.str = private unnamed_addr constant [23 x i8] c"btOptimizedBvhNodeData\00", align 1
@.str.1 = private unnamed_addr constant [23 x i8] c"btQuantizedBvhNodeData\00", align 1
@.str.2 = private unnamed_addr constant [21 x i8] c"btBvhSubtreeInfoData\00", align 1
@.str.3 = private unnamed_addr constant [24 x i8] c"btQuantizedBvhFloatData\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS14btQuantizedBvh = hidden constant [17 x i8] c"14btQuantizedBvh\00", align 1
@_ZTI14btQuantizedBvh = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([17 x i8], [17 x i8]* @_ZTS14btQuantizedBvh, i32 0, i32 0) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btQuantizedBvh.cpp, i8* null }]

@_ZN14btQuantizedBvhC1Ev = hidden unnamed_addr alias %class.btQuantizedBvh* (%class.btQuantizedBvh*), %class.btQuantizedBvh* (%class.btQuantizedBvh*)* @_ZN14btQuantizedBvhC2Ev
@_ZN14btQuantizedBvhD1Ev = hidden unnamed_addr alias %class.btQuantizedBvh* (%class.btQuantizedBvh*), %class.btQuantizedBvh* (%class.btQuantizedBvh*)* @_ZN14btQuantizedBvhD2Ev
@_ZN14btQuantizedBvhC1ERS_b = hidden unnamed_addr alias %class.btQuantizedBvh* (%class.btQuantizedBvh*, %class.btQuantizedBvh*, i1), %class.btQuantizedBvh* (%class.btQuantizedBvh*, %class.btQuantizedBvh*, i1)* @_ZN14btQuantizedBvhC2ERS_b

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btQuantizedBvh* @_ZN14btQuantizedBvhC2Ev(%class.btQuantizedBvh* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast %class.btQuantizedBvh* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV14btQuantizedBvh, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bvhAabbMin)
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bvhAabbMax)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bvhQuantization)
  %m_bulletVersion = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 4
  store i32 286, i32* %m_bulletVersion, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  store i8 0, i8* %m_useQuantization, align 4
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %call4 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEC2Ev(%class.btAlignedObjectArray* %m_leafNodes)
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call5 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEC2Ev(%class.btAlignedObjectArray* %m_contiguousNodes)
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %call6 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEC2Ev(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes)
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call7 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEC2Ev(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes)
  %m_traversalMode = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 12
  store i32 0, i32* %m_traversalMode, align 4
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call8 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEC2Ev(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %m_subtreeHeaderCount = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  store i32 0, i32* %m_subtreeHeaderCount, align 4
  %m_bvhAabbMin9 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  store float 0xC7EFFFFFE0000000, float* %ref.tmp, align 4
  store float 0xC7EFFFFFE0000000, float* %ref.tmp10, align 4
  store float 0xC7EFFFFFE0000000, float* %ref.tmp11, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_bvhAabbMin9, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %m_bvhAabbMax12 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  store float 0x47EFFFFFE0000000, float* %ref.tmp13, align 4
  store float 0x47EFFFFFE0000000, float* %ref.tmp14, align 4
  store float 0x47EFFFFFE0000000, float* %ref.tmp15, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_bvhAabbMax12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  ret %class.btQuantizedBvh* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btQuantizedBvh13buildInternalEv(%class.btQuantizedBvh* %this) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %numLeafNodes = alloca i32, align 4
  %ref.tmp = alloca %struct.btQuantizedBvhNode, align 4
  %subtree = alloca %class.btBvhSubtreeInfo*, align 4
  %ref.tmp9 = alloca %class.btBvhSubtreeInfo, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  store i8 1, i8* %m_useQuantization, align 4
  store i32 0, i32* %numLeafNodes, align 4
  %m_useQuantization2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization2, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes)
  store i32 %call, i32* %numLeafNodes, align 4
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %1 = load i32, i32* %numLeafNodes, align 4
  %mul = mul nsw i32 2, %1
  %2 = bitcast %struct.btQuantizedBvhNode* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %2, i8 0, i32 16, i1 false)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %mul, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %ref.tmp)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  store i32 0, i32* %m_curNodeIndex, align 4
  %3 = load i32, i32* %numLeafNodes, align 4
  call void @_ZN14btQuantizedBvh9buildTreeEii(%class.btQuantizedBvh* %this1, i32 0, i32 %3)
  %m_useQuantization3 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %4 = load i8, i8* %m_useQuantization3, align 4
  %tobool4 = trunc i8 %4 to i1
  br i1 %tobool4, label %land.lhs.true, label %if.end20

land.lhs.true:                                    ; preds = %if.end
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %tobool6 = icmp ne i32 %call5, 0
  br i1 %tobool6, label %if.end20, label %if.then7

if.then7:                                         ; preds = %land.lhs.true
  %m_SubtreeHeaders8 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call10 = call %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* %ref.tmp9)
  %call11 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_(%class.btAlignedObjectArray.4* %m_SubtreeHeaders8, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %ref.tmp9)
  store %class.btBvhSubtreeInfo* %call11, %class.btBvhSubtreeInfo** %subtree, align 4
  %5 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_quantizedContiguousNodes12 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call13 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes12, i32 0)
  call void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %5, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %call13)
  %6 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %6, i32 0, i32 2
  store i32 0, i32* %m_rootNodeIndex, align 4
  %m_quantizedContiguousNodes14 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call15 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes14, i32 0)
  %call16 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %call15)
  br i1 %call16, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then7
  br label %cond.end

cond.false:                                       ; preds = %if.then7
  %m_quantizedContiguousNodes17 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call18 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes17, i32 0)
  %call19 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %call18)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 1, %cond.true ], [ %call19, %cond.false ]
  %7 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %7, i32 0, i32 3
  store i32 %cond, i32* %m_subtreeSize, align 4
  br label %if.end20

if.end20:                                         ; preds = %cond.end, %land.lhs.true, %if.end
  %m_SubtreeHeaders21 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call22 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders21)
  %m_subtreeHeaderCount = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  store i32 %call22, i32* %m_subtreeHeaderCount, align 4
  %m_quantizedLeafNodes23 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes23)
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv(%class.btAlignedObjectArray* %m_leafNodes)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %this, i32 %newsize, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btQuantizedBvhNode* %fillData, %struct.btQuantizedBvhNode** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %5 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %14 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %14, i32 %15
  %16 = bitcast %struct.btQuantizedBvhNode* %arrayidx10 to i8*
  %call11 = call i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 16, i8* %16)
  %17 = bitcast i8* %call11 to %struct.btQuantizedBvhNode*
  %18 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %fillData.addr, align 4
  %19 = bitcast %struct.btQuantizedBvhNode* %17 to i8*
  %20 = bitcast %struct.btQuantizedBvhNode* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #3

; Function Attrs: noinline optnone
define hidden void @_ZN14btQuantizedBvh9buildTreeEii(%class.btQuantizedBvh* %this, i32 %startIndex, i32 %endIndex) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %splitAxis = alloca i32, align 4
  %splitIndex = alloca i32, align 4
  %i = alloca i32, align 4
  %numIndices = alloca i32, align 4
  %curIndex = alloca i32, align 4
  %internalNodeIndex = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %leftChildNodexIndex = alloca i32, align 4
  %rightChildNodexIndex = alloca i32, align 4
  %escapeIndex = alloca i32, align 4
  %sizeQuantizedNode = alloca i32, align 4
  %treeSizeInBytes = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i32 %startIndex, i32* %startIndex.addr, align 4
  store i32 %endIndex, i32* %endIndex.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = load i32, i32* %endIndex.addr, align 4
  %1 = load i32, i32* %startIndex.addr, align 4
  %sub = sub nsw i32 %0, %1
  store i32 %sub, i32* %numIndices, align 4
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %2 = load i32, i32* %m_curNodeIndex, align 4
  store i32 %2, i32* %curIndex, align 4
  %3 = load i32, i32* %numIndices, align 4
  %cmp = icmp eq i32 %3, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_curNodeIndex2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %4 = load i32, i32* %m_curNodeIndex2, align 4
  %5 = load i32, i32* %startIndex.addr, align 4
  call void @_ZN14btQuantizedBvh30assignInternalNodeFromLeafNodeEii(%class.btQuantizedBvh* %this1, i32 %4, i32 %5)
  %m_curNodeIndex3 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %6 = load i32, i32* %m_curNodeIndex3, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %m_curNodeIndex3, align 4
  br label %return

if.end:                                           ; preds = %entry
  %7 = load i32, i32* %startIndex.addr, align 4
  %8 = load i32, i32* %endIndex.addr, align 4
  %call = call i32 @_ZN14btQuantizedBvh17calcSplittingAxisEii(%class.btQuantizedBvh* %this1, i32 %7, i32 %8)
  store i32 %call, i32* %splitAxis, align 4
  %9 = load i32, i32* %startIndex.addr, align 4
  %10 = load i32, i32* %endIndex.addr, align 4
  %11 = load i32, i32* %splitAxis, align 4
  %call4 = call i32 @_ZN14btQuantizedBvh25sortAndCalcSplittingIndexEiii(%class.btQuantizedBvh* %this1, i32 %9, i32 %10, i32 %11)
  store i32 %call4, i32* %splitIndex, align 4
  %m_curNodeIndex5 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %12 = load i32, i32* %m_curNodeIndex5, align 4
  store i32 %12, i32* %internalNodeIndex, align 4
  %m_curNodeIndex6 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %13 = load i32, i32* %m_curNodeIndex6, align 4
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  call void @_ZN14btQuantizedBvh22setInternalNodeAabbMinEiRK9btVector3(%class.btQuantizedBvh* %this1, i32 %13, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax)
  %m_curNodeIndex7 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %14 = load i32, i32* %m_curNodeIndex7, align 4
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZN14btQuantizedBvh22setInternalNodeAabbMaxEiRK9btVector3(%class.btQuantizedBvh* %this1, i32 %14, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin)
  %15 = load i32, i32* %startIndex.addr, align 4
  store i32 %15, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %16 = load i32, i32* %i, align 4
  %17 = load i32, i32* %endIndex.addr, align 4
  %cmp8 = icmp slt i32 %16, %17
  br i1 %cmp8, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_curNodeIndex9 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %18 = load i32, i32* %m_curNodeIndex9, align 4
  %19 = load i32, i32* %i, align 4
  call void @_ZNK14btQuantizedBvh10getAabbMinEi(%class.btVector3* sret align 4 %ref.tmp, %class.btQuantizedBvh* %this1, i32 %19)
  %20 = load i32, i32* %i, align 4
  call void @_ZNK14btQuantizedBvh10getAabbMaxEi(%class.btVector3* sret align 4 %ref.tmp10, %class.btQuantizedBvh* %this1, i32 %20)
  call void @_ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_(%class.btQuantizedBvh* %this1, i32 %18, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %i, align 4
  %inc11 = add nsw i32 %21, 1
  store i32 %inc11, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_curNodeIndex12 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %22 = load i32, i32* %m_curNodeIndex12, align 4
  %inc13 = add nsw i32 %22, 1
  store i32 %inc13, i32* %m_curNodeIndex12, align 4
  %m_curNodeIndex14 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %23 = load i32, i32* %m_curNodeIndex14, align 4
  store i32 %23, i32* %leftChildNodexIndex, align 4
  %24 = load i32, i32* %startIndex.addr, align 4
  %25 = load i32, i32* %splitIndex, align 4
  call void @_ZN14btQuantizedBvh9buildTreeEii(%class.btQuantizedBvh* %this1, i32 %24, i32 %25)
  %m_curNodeIndex15 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %26 = load i32, i32* %m_curNodeIndex15, align 4
  store i32 %26, i32* %rightChildNodexIndex, align 4
  %27 = load i32, i32* %splitIndex, align 4
  %28 = load i32, i32* %endIndex.addr, align 4
  call void @_ZN14btQuantizedBvh9buildTreeEii(%class.btQuantizedBvh* %this1, i32 %27, i32 %28)
  %m_curNodeIndex16 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %29 = load i32, i32* %m_curNodeIndex16, align 4
  %30 = load i32, i32* %curIndex, align 4
  %sub17 = sub nsw i32 %29, %30
  store i32 %sub17, i32* %escapeIndex, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %31 = load i8, i8* %m_useQuantization, align 4
  %tobool = trunc i8 %31 to i1
  br i1 %tobool, label %if.then18, label %if.else

if.then18:                                        ; preds = %for.end
  store i32 16, i32* %sizeQuantizedNode, align 4
  %32 = load i32, i32* %escapeIndex, align 4
  %mul = mul nsw i32 %32, 16
  store i32 %mul, i32* %treeSizeInBytes, align 4
  %33 = load i32, i32* %treeSizeInBytes, align 4
  %cmp19 = icmp sgt i32 %33, 2048
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %if.then18
  %34 = load i32, i32* %leftChildNodexIndex, align 4
  %35 = load i32, i32* %rightChildNodexIndex, align 4
  call void @_ZN14btQuantizedBvh20updateSubtreeHeadersEii(%class.btQuantizedBvh* %this1, i32 %34, i32 %35)
  br label %if.end21

if.end21:                                         ; preds = %if.then20, %if.then18
  br label %if.end22

if.else:                                          ; preds = %for.end
  br label %if.end22

if.end22:                                         ; preds = %if.else, %if.end21
  %36 = load i32, i32* %internalNodeIndex, align 4
  %37 = load i32, i32* %escapeIndex, align 4
  call void @_ZN14btQuantizedBvh26setInternalNodeEscapeIndexEii(%class.btQuantizedBvh* %this1, i32 %36, i32 %37)
  br label %return

return:                                           ; preds = %if.end22, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_(%class.btAlignedObjectArray.4* %this, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %fillValue) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %fillValue.addr = alloca %class.btBvhSubtreeInfo*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store %class.btBvhSubtreeInfo* %fillValue, %class.btBvhSubtreeInfo** %fillValue.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE9allocSizeEi(%class.btAlignedObjectArray.4* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %1 = load i32, i32* %m_size, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4
  %3 = load i32, i32* %sz, align 4
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %2, i32 %3
  %4 = bitcast %class.btBvhSubtreeInfo* %arrayidx to i8*
  %call5 = call i8* @_ZN16btBvhSubtreeInfonwEmPv(i32 32, i8* %4)
  %5 = bitcast i8* %call5 to %class.btBvhSubtreeInfo*
  %6 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %fillValue.addr, align 4
  %7 = bitcast %class.btBvhSubtreeInfo* %5 to i8*
  %8 = bitcast %class.btBvhSubtreeInfo* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 32, i1 false)
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %9 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data6, align 4
  %10 = load i32, i32* %sz, align 4
  %arrayidx7 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %9, i32 %10
  ret %class.btBvhSubtreeInfo* %arrayidx7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBvhSubtreeInfo*, align 4
  store %class.btBvhSubtreeInfo* %this, %class.btBvhSubtreeInfo** %this.addr, align 4
  %this1 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %this.addr, align 4
  ret %class.btBvhSubtreeInfo* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %this, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %quantizedNode) #1 comdat {
entry:
  %this.addr = alloca %class.btBvhSubtreeInfo*, align 4
  %quantizedNode.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btBvhSubtreeInfo* %this, %class.btBvhSubtreeInfo** %this.addr, align 4
  store %struct.btQuantizedBvhNode* %quantizedNode, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4
  %this1 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %this.addr, align 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %1 = load i16, i16* %arrayidx, align 4
  %m_quantizedAabbMin2 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin2, i32 0, i32 0
  store i16 %1, i16* %arrayidx3, align 4
  %2 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4
  %m_quantizedAabbMin4 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %2, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin4, i32 0, i32 1
  %3 = load i16, i16* %arrayidx5, align 2
  %m_quantizedAabbMin6 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin6, i32 0, i32 1
  store i16 %3, i16* %arrayidx7, align 2
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4
  %m_quantizedAabbMin8 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %4, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin8, i32 0, i32 2
  %5 = load i16, i16* %arrayidx9, align 4
  %m_quantizedAabbMin10 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin10, i32 0, i32 2
  store i16 %5, i16* %arrayidx11, align 4
  %6 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %6, i32 0, i32 1
  %arrayidx12 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %7 = load i16, i16* %arrayidx12, align 2
  %m_quantizedAabbMax13 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 1
  %arrayidx14 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax13, i32 0, i32 0
  store i16 %7, i16* %arrayidx14, align 2
  %8 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4
  %m_quantizedAabbMax15 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %8, i32 0, i32 1
  %arrayidx16 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax15, i32 0, i32 1
  %9 = load i16, i16* %arrayidx16, align 2
  %m_quantizedAabbMax17 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 1
  %arrayidx18 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax17, i32 0, i32 1
  store i16 %9, i16* %arrayidx18, align 2
  %10 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %quantizedNode.addr, align 4
  %m_quantizedAabbMax19 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %10, i32 0, i32 1
  %arrayidx20 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax19, i32 0, i32 2
  %11 = load i16, i16* %arrayidx20, align 2
  %m_quantizedAabbMax21 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %this1, i32 0, i32 1
  %arrayidx22 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax21, i32 0, i32 2
  store i16 %11, i16* %arrayidx22, align 2
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %0, i32 %1
  ret %struct.btQuantizedBvhNode* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4
  %cmp = icmp sge i32 %0, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4
  %sub = sub nsw i32 0, %0
  ret i32 %sub
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btQuantizedBvh21setQuantizationValuesERK9btVector3S2_f(%class.btQuantizedBvh* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhAabbMax, float %quantizationMargin) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %bvhAabbMin.addr = alloca %class.btVector3*, align 4
  %bvhAabbMax.addr = alloca %class.btVector3*, align 4
  %quantizationMargin.addr = alloca float, align 4
  %clampValue = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %aabbSize = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %vecIn = alloca [3 x i16], align 2
  %v = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %ref.tmp36 = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca %class.btVector3, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store %class.btVector3* %bvhAabbMin, %class.btVector3** %bvhAabbMin.addr, align 4
  store %class.btVector3* %bvhAabbMax, %class.btVector3** %bvhAabbMax.addr, align 4
  store float %quantizationMargin, float* %quantizationMargin.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %clampValue, float* nonnull align 4 dereferenceable(4) %quantizationMargin.addr, float* nonnull align 4 dereferenceable(4) %quantizationMargin.addr, float* nonnull align 4 dereferenceable(4) %quantizationMargin.addr)
  %0 = load %class.btVector3*, %class.btVector3** %bvhAabbMin.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %clampValue)
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_bvhAabbMin to i8*
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %bvhAabbMax.addr, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %clampValue)
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %4 = bitcast %class.btVector3* %m_bvhAabbMax to i8*
  %5 = bitcast %class.btVector3* %ref.tmp2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_bvhAabbMax3 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %m_bvhAabbMin4 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %aabbSize, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin4)
  store float 6.553300e+04, float* %ref.tmp7, align 4
  store float 6.553300e+04, float* %ref.tmp8, align 4
  store float 6.553300e+04, float* %ref.tmp9, align 4
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbSize)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %6 = bitcast %class.btVector3* %m_bvhQuantization to i8*
  %7 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  store i8 1, i8* %m_useQuantization, align 4
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %v)
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %vecIn, i32 0, i32 0
  %m_bvhAabbMin12 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin12, i32 0)
  %arraydecay14 = getelementptr inbounds [3 x i16], [3 x i16]* %vecIn, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh10unQuantizeEPKt(%class.btVector3* sret align 4 %ref.tmp13, %class.btQuantizedBvh* %this1, i16* %arraydecay14)
  %8 = bitcast %class.btVector3* %v to i8*
  %9 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %m_bvhAabbMin15 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btVector3* nonnull align 4 dereferenceable(16) %clampValue)
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %m_bvhAabbMin15, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp16)
  %m_bvhAabbMax18 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %m_bvhAabbMin19 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax18, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin19)
  %10 = bitcast %class.btVector3* %aabbSize to i8*
  %11 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  store float 6.553300e+04, float* %ref.tmp22, align 4
  store float 6.553300e+04, float* %ref.tmp23, align 4
  store float 6.553300e+04, float* %ref.tmp24, align 4
  %call25 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24)
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbSize)
  %m_bvhQuantization26 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %12 = bitcast %class.btVector3* %m_bvhQuantization26 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  %arraydecay27 = getelementptr inbounds [3 x i16], [3 x i16]* %vecIn, i32 0, i32 0
  %m_bvhAabbMax28 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay27, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax28, i32 1)
  %arraydecay30 = getelementptr inbounds [3 x i16], [3 x i16]* %vecIn, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh10unQuantizeEPKt(%class.btVector3* sret align 4 %ref.tmp29, %class.btQuantizedBvh* %this1, i16* %arraydecay30)
  %14 = bitcast %class.btVector3* %v to i8*
  %15 = bitcast %class.btVector3* %ref.tmp29 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false)
  %m_bvhAabbMax31 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btVector3* nonnull align 4 dereferenceable(16) %clampValue)
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %m_bvhAabbMax31, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32)
  %m_bvhAabbMax34 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %m_bvhAabbMin35 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp33, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax34, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin35)
  %16 = bitcast %class.btVector3* %aabbSize to i8*
  %17 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  store float 6.553300e+04, float* %ref.tmp38, align 4
  store float 6.553300e+04, float* %ref.tmp39, align 4
  store float 6.553300e+04, float* %ref.tmp40, align 4
  %call41 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp36, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp37, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbSize)
  %m_bvhQuantization42 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %18 = bitcast %class.btVector3* %m_bvhQuantization42 to i8*
  %19 = bitcast %class.btVector3* %ref.tmp36 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %div = fdiv float %1, %3
  store float %div, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %div8 = fdiv float %5, %7
  store float %div8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %div14 = fdiv float %9, %11
  store float %div14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this, i16* %out, %class.btVector3* nonnull align 4 dereferenceable(16) %point, i32 %isMax) #2 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %out.addr = alloca i16*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %isMax.addr = alloca i32, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i16* %out, i16** %out.addr, align 4
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4
  store i32 %isMax, i32* %isMax.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhQuantization)
  %1 = load i32, i32* %isMax.addr, align 4
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %v)
  %2 = load float, float* %call, align 4
  %add = fadd float %2, 1.000000e+00
  %conv = fptoui float %add to i16
  %conv2 = zext i16 %conv to i32
  %or = or i32 %conv2, 1
  %conv3 = trunc i32 %or to i16
  %3 = load i16*, i16** %out.addr, align 4
  %arrayidx = getelementptr inbounds i16, i16* %3, i32 0
  store i16 %conv3, i16* %arrayidx, align 2
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %v)
  %4 = load float, float* %call4, align 4
  %add5 = fadd float %4, 1.000000e+00
  %conv6 = fptoui float %add5 to i16
  %conv7 = zext i16 %conv6 to i32
  %or8 = or i32 %conv7, 1
  %conv9 = trunc i32 %or8 to i16
  %5 = load i16*, i16** %out.addr, align 4
  %arrayidx10 = getelementptr inbounds i16, i16* %5, i32 1
  store i16 %conv9, i16* %arrayidx10, align 2
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %v)
  %6 = load float, float* %call11, align 4
  %add12 = fadd float %6, 1.000000e+00
  %conv13 = fptoui float %add12 to i16
  %conv14 = zext i16 %conv13 to i32
  %or15 = or i32 %conv14, 1
  %conv16 = trunc i32 %or15 to i16
  %7 = load i16*, i16** %out.addr, align 4
  %arrayidx17 = getelementptr inbounds i16, i16* %7, i32 2
  store i16 %conv16, i16* %arrayidx17, align 2
  br label %if.end

if.else:                                          ; preds = %entry
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %v)
  %8 = load float, float* %call18, align 4
  %conv19 = fptoui float %8 to i16
  %conv20 = zext i16 %conv19 to i32
  %and = and i32 %conv20, 65534
  %conv21 = trunc i32 %and to i16
  %9 = load i16*, i16** %out.addr, align 4
  %arrayidx22 = getelementptr inbounds i16, i16* %9, i32 0
  store i16 %conv21, i16* %arrayidx22, align 2
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %v)
  %10 = load float, float* %call23, align 4
  %conv24 = fptoui float %10 to i16
  %conv25 = zext i16 %conv24 to i32
  %and26 = and i32 %conv25, 65534
  %conv27 = trunc i32 %and26 to i16
  %11 = load i16*, i16** %out.addr, align 4
  %arrayidx28 = getelementptr inbounds i16, i16* %11, i32 1
  store i16 %conv27, i16* %arrayidx28, align 2
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %v)
  %12 = load float, float* %call29, align 4
  %conv30 = fptoui float %12 to i16
  %conv31 = zext i16 %conv30 to i32
  %and32 = and i32 %conv31, 65534
  %conv33 = trunc i32 %and32 to i16
  %13 = load i16*, i16** %out.addr, align 4
  %arrayidx34 = getelementptr inbounds i16, i16* %13, i32 2
  store i16 %conv33, i16* %arrayidx34, align 2
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK14btQuantizedBvh10unQuantizeEPKt(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuantizedBvh* %this, i16* %vecIn) #2 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %vecIn.addr = alloca i16*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i16* %vecIn, i16** %vecIn.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %agg.result)
  %0 = load i16*, i16** %vecIn.addr, align 4
  %arrayidx = getelementptr inbounds i16, i16* %0, i32 0
  %1 = load i16, i16* %arrayidx, align 2
  %conv = uitofp i16 %1 to float
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_bvhQuantization)
  %2 = load float, float* %call2, align 4
  %div = fdiv float %conv, %2
  store float %div, float* %ref.tmp, align 4
  %3 = load i16*, i16** %vecIn.addr, align 4
  %arrayidx4 = getelementptr inbounds i16, i16* %3, i32 1
  %4 = load i16, i16* %arrayidx4, align 2
  %conv5 = uitofp i16 %4 to float
  %m_bvhQuantization6 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %m_bvhQuantization6)
  %5 = load float, float* %call7, align 4
  %div8 = fdiv float %conv5, %5
  store float %div8, float* %ref.tmp3, align 4
  %6 = load i16*, i16** %vecIn.addr, align 4
  %arrayidx10 = getelementptr inbounds i16, i16* %6, i32 2
  %7 = load i16, i16* %arrayidx10, align 2
  %conv11 = uitofp i16 %7 to float
  %m_bvhQuantization12 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %m_bvhQuantization12)
  %8 = load float, float* %call13, align 4
  %div14 = fdiv float %conv11, %8
  store float %div14, float* %ref.tmp9, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector36setMinERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector36setMaxERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btQuantizedBvh* @_ZN14btQuantizedBvhD2Ev(%class.btQuantizedBvh* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = bitcast %class.btQuantizedBvh* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV14btQuantizedBvh, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoED2Ev(%class.btAlignedObjectArray.4* %m_SubtreeHeaders) #7
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeED2Ev(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes) #7
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %call3 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeED2Ev(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes) #7
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call4 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeED2Ev(%class.btAlignedObjectArray* %m_contiguousNodes) #7
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %call5 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeED2Ev(%class.btAlignedObjectArray* %m_leafNodes) #7
  ret %class.btQuantizedBvh* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN14btQuantizedBvhD0Ev(%class.btQuantizedBvh* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %call = call %class.btQuantizedBvh* @_ZN14btQuantizedBvhD1Ev(%class.btQuantizedBvh* %this1) #7
  %0 = bitcast %class.btQuantizedBvh* %this1 to i8*
  call void @_ZN14btQuantizedBvhdlEPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN14btQuantizedBvhdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btQuantizedBvh30assignInternalNodeFromLeafNodeEii(%class.btQuantizedBvh* %this, i32 %internalNode, i32 %leafNodeIndex) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %internalNode.addr = alloca i32, align 4
  %leafNodeIndex.addr = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i32 %internalNode, i32* %internalNode.addr, align 4
  store i32 %leafNodeIndex, i32* %leafNodeIndex.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %1 = load i32, i32* %leafNodeIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes, i32 %1)
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %2 = load i32, i32* %internalNode.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %2)
  %3 = bitcast %struct.btQuantizedBvhNode* %call2 to i8*
  %4 = bitcast %struct.btQuantizedBvhNode* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %5 = load i32, i32* %leafNodeIndex.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_leafNodes, i32 %5)
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %6 = load i32, i32* %internalNode.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %6)
  %7 = bitcast %struct.btOptimizedBvhNode* %call4 to i8*
  %8 = bitcast %struct.btOptimizedBvhNode* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 64, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN14btQuantizedBvh17calcSplittingAxisEii(%class.btQuantizedBvh* %this, i32 %startIndex, i32 %endIndex) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %means = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %variance = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %numIndices = alloca i32, align 4
  %center = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %center18 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %diff2 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca float, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i32 %startIndex, i32* %startIndex.addr, align 4
  store i32 %endIndex, i32* %endIndex.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %variance, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %0 = load i32, i32* %endIndex.addr, align 4
  %1 = load i32, i32* %startIndex.addr, align 4
  %sub = sub nsw i32 %0, %1
  store i32 %sub, i32* %numIndices, align 4
  %2 = load i32, i32* %startIndex.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %endIndex.addr, align 4
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store float 5.000000e-01, float* %ref.tmp8, align 4
  %5 = load i32, i32* %i, align 4
  call void @_ZNK14btQuantizedBvh10getAabbMaxEi(%class.btVector3* sret align 4 %ref.tmp10, %class.btQuantizedBvh* %this1, i32 %5)
  %6 = load i32, i32* %i, align 4
  call void @_ZNK14btQuantizedBvh10getAabbMinEi(%class.btVector3* sret align 4 %ref.tmp11, %class.btQuantizedBvh* %this1, i32 %6)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center, float* nonnull align 4 dereferenceable(4) %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %means, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %8 = load i32, i32* %numIndices, align 4
  %conv = sitofp i32 %8 to float
  %div = fdiv float 1.000000e+00, %conv
  store float %div, float* %ref.tmp13, align 4
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  %9 = load i32, i32* %startIndex.addr, align 4
  store i32 %9, i32* %i, align 4
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc25, %for.end
  %10 = load i32, i32* %i, align 4
  %11 = load i32, i32* %endIndex.addr, align 4
  %cmp16 = icmp slt i32 %10, %11
  br i1 %cmp16, label %for.body17, label %for.end27

for.body17:                                       ; preds = %for.cond15
  store float 5.000000e-01, float* %ref.tmp19, align 4
  %12 = load i32, i32* %i, align 4
  call void @_ZNK14btQuantizedBvh10getAabbMaxEi(%class.btVector3* sret align 4 %ref.tmp21, %class.btQuantizedBvh* %this1, i32 %12)
  %13 = load i32, i32* %i, align 4
  call void @_ZNK14btQuantizedBvh10getAabbMinEi(%class.btVector3* sret align 4 %ref.tmp22, %class.btQuantizedBvh* %this1, i32 %13)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp22)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center18, float* nonnull align 4 dereferenceable(4) %ref.tmp19, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp20)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %diff2, %class.btVector3* nonnull align 4 dereferenceable(16) %center18, %class.btVector3* nonnull align 4 dereferenceable(16) %means)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp23, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2)
  %14 = bitcast %class.btVector3* %diff2 to i8*
  %15 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false)
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %variance, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2)
  br label %for.inc25

for.inc25:                                        ; preds = %for.body17
  %16 = load i32, i32* %i, align 4
  %inc26 = add nsw i32 %16, 1
  store i32 %inc26, i32* %i, align 4
  br label %for.cond15

for.end27:                                        ; preds = %for.cond15
  %17 = load i32, i32* %numIndices, align 4
  %conv29 = sitofp i32 %17 to float
  %sub30 = fsub float %conv29, 1.000000e+00
  %div31 = fdiv float 1.000000e+00, %sub30
  store float %div31, float* %ref.tmp28, align 4
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %variance, float* nonnull align 4 dereferenceable(4) %ref.tmp28)
  %call33 = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %variance)
  ret i32 %call33
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN14btQuantizedBvh25sortAndCalcSplittingIndexEiii(%class.btQuantizedBvh* %this, i32 %startIndex, i32 %endIndex, i32 %splitAxis) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %splitAxis.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %splitIndex = alloca i32, align 4
  %numIndices = alloca i32, align 4
  %splitValue = alloca float, align 4
  %means = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %center = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca float, align 4
  %center15 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %ref.tmp18 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %rangeBalancedIndices = alloca i32, align 4
  %unbalanced = alloca i8, align 1
  %unbal = alloca i8, align 1
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i32 %startIndex, i32* %startIndex.addr, align 4
  store i32 %endIndex, i32* %endIndex.addr, align 4
  store i32 %splitAxis, i32* %splitAxis.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = load i32, i32* %startIndex.addr, align 4
  store i32 %0, i32* %splitIndex, align 4
  %1 = load i32, i32* %endIndex.addr, align 4
  %2 = load i32, i32* %startIndex.addr, align 4
  %sub = sub nsw i32 %1, %2
  store i32 %sub, i32* %numIndices, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %3 = load i32, i32* %startIndex.addr, align 4
  store i32 %3, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4
  %5 = load i32, i32* %endIndex.addr, align 4
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store float 5.000000e-01, float* %ref.tmp4, align 4
  %6 = load i32, i32* %i, align 4
  call void @_ZNK14btQuantizedBvh10getAabbMaxEi(%class.btVector3* sret align 4 %ref.tmp6, %class.btQuantizedBvh* %this1, i32 %6)
  %7 = load i32, i32* %i, align 4
  call void @_ZNK14btQuantizedBvh10getAabbMinEi(%class.btVector3* sret align 4 %ref.tmp7, %class.btQuantizedBvh* %this1, i32 %7)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center, float* nonnull align 4 dereferenceable(4) %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %means, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load i32, i32* %numIndices, align 4
  %conv = sitofp i32 %9 to float
  %div = fdiv float 1.000000e+00, %conv
  store float %div, float* %ref.tmp9, align 4
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %means)
  %10 = load i32, i32* %splitAxis.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call11, i32 %10
  %11 = load float, float* %arrayidx, align 4
  store float %11, float* %splitValue, align 4
  %12 = load i32, i32* %startIndex.addr, align 4
  store i32 %12, i32* %i, align 4
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc24, %for.end
  %13 = load i32, i32* %i, align 4
  %14 = load i32, i32* %endIndex.addr, align 4
  %cmp13 = icmp slt i32 %13, %14
  br i1 %cmp13, label %for.body14, label %for.end26

for.body14:                                       ; preds = %for.cond12
  store float 5.000000e-01, float* %ref.tmp16, align 4
  %15 = load i32, i32* %i, align 4
  call void @_ZNK14btQuantizedBvh10getAabbMaxEi(%class.btVector3* sret align 4 %ref.tmp18, %class.btQuantizedBvh* %this1, i32 %15)
  %16 = load i32, i32* %i, align 4
  call void @_ZNK14btQuantizedBvh10getAabbMinEi(%class.btVector3* sret align 4 %ref.tmp19, %class.btQuantizedBvh* %this1, i32 %16)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp18, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp19)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp17)
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center15)
  %17 = load i32, i32* %splitAxis.addr, align 4
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 %17
  %18 = load float, float* %arrayidx21, align 4
  %19 = load float, float* %splitValue, align 4
  %cmp22 = fcmp ogt float %18, %19
  br i1 %cmp22, label %if.then, label %if.end

if.then:                                          ; preds = %for.body14
  %20 = load i32, i32* %i, align 4
  %21 = load i32, i32* %splitIndex, align 4
  call void @_ZN14btQuantizedBvh13swapLeafNodesEii(%class.btQuantizedBvh* %this1, i32 %20, i32 %21)
  %22 = load i32, i32* %splitIndex, align 4
  %inc23 = add nsw i32 %22, 1
  store i32 %inc23, i32* %splitIndex, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body14
  br label %for.inc24

for.inc24:                                        ; preds = %if.end
  %23 = load i32, i32* %i, align 4
  %inc25 = add nsw i32 %23, 1
  store i32 %inc25, i32* %i, align 4
  br label %for.cond12

for.end26:                                        ; preds = %for.cond12
  %24 = load i32, i32* %numIndices, align 4
  %div27 = sdiv i32 %24, 3
  store i32 %div27, i32* %rangeBalancedIndices, align 4
  %25 = load i32, i32* %splitIndex, align 4
  %26 = load i32, i32* %startIndex.addr, align 4
  %27 = load i32, i32* %rangeBalancedIndices, align 4
  %add = add nsw i32 %26, %27
  %cmp28 = icmp sle i32 %25, %add
  br i1 %cmp28, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %for.end26
  %28 = load i32, i32* %splitIndex, align 4
  %29 = load i32, i32* %endIndex.addr, align 4
  %sub29 = sub nsw i32 %29, 1
  %30 = load i32, i32* %rangeBalancedIndices, align 4
  %sub30 = sub nsw i32 %sub29, %30
  %cmp31 = icmp sge i32 %28, %sub30
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %for.end26
  %31 = phi i1 [ true, %for.end26 ], [ %cmp31, %lor.rhs ]
  %frombool = zext i1 %31 to i8
  store i8 %frombool, i8* %unbalanced, align 1
  %32 = load i8, i8* %unbalanced, align 1
  %tobool = trunc i8 %32 to i1
  br i1 %tobool, label %if.then32, label %if.end34

if.then32:                                        ; preds = %lor.end
  %33 = load i32, i32* %startIndex.addr, align 4
  %34 = load i32, i32* %numIndices, align 4
  %shr = ashr i32 %34, 1
  %add33 = add nsw i32 %33, %shr
  store i32 %add33, i32* %splitIndex, align 4
  br label %if.end34

if.end34:                                         ; preds = %if.then32, %lor.end
  %35 = load i32, i32* %splitIndex, align 4
  %36 = load i32, i32* %startIndex.addr, align 4
  %cmp35 = icmp eq i32 %35, %36
  br i1 %cmp35, label %lor.end38, label %lor.rhs36

lor.rhs36:                                        ; preds = %if.end34
  %37 = load i32, i32* %splitIndex, align 4
  %38 = load i32, i32* %endIndex.addr, align 4
  %cmp37 = icmp eq i32 %37, %38
  br label %lor.end38

lor.end38:                                        ; preds = %lor.rhs36, %if.end34
  %39 = phi i1 [ true, %if.end34 ], [ %cmp37, %lor.rhs36 ]
  %frombool39 = zext i1 %39 to i8
  store i8 %frombool39, i8* %unbal, align 1
  %40 = load i32, i32* %splitIndex, align 4
  ret i32 %40
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN14btQuantizedBvh22setInternalNodeAabbMinEiRK9btVector3(%class.btQuantizedBvh* %this, i32 %nodeIndex, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin) #2 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeIndex.addr = alloca i32, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i32 %nodeIndex, i32* %nodeIndex.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %1 = load i32, i32* %nodeIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %1)
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i32 0)
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %4 = load i32, i32* %nodeIndex.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %4)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call2, i32 0, i32 0
  %5 = bitcast %class.btVector3* %m_aabbMinOrg to i8*
  %6 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN14btQuantizedBvh22setInternalNodeAabbMaxEiRK9btVector3(%class.btQuantizedBvh* %this, i32 %nodeIndex, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #2 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeIndex.addr = alloca i32, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i32 %nodeIndex, i32* %nodeIndex.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %1 = load i32, i32* %nodeIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %1)
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i32 1)
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %4 = load i32, i32* %nodeIndex.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %4)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call2, i32 0, i32 1
  %5 = bitcast %class.btVector3* %m_aabbMaxOrg to i8*
  %6 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN14btQuantizedBvh21mergeInternalNodeAabbEiRK9btVector3S2_(%class.btQuantizedBvh* %this, i32 %nodeIndex, %class.btVector3* nonnull align 4 dereferenceable(16) %newAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %newAabbMax) #2 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeIndex.addr = alloca i32, align 4
  %newAabbMin.addr = alloca %class.btVector3*, align 4
  %newAabbMax.addr = alloca %class.btVector3*, align 4
  %quantizedAabbMin = alloca [3 x i16], align 2
  %quantizedAabbMax = alloca [3 x i16], align 2
  %i = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i32 %nodeIndex, i32* %nodeIndex.addr, align 4
  store %class.btVector3* %newAabbMin, %class.btVector3** %newAabbMin.addr, align 4
  store %class.btVector3* %newAabbMax, %class.btVector3** %newAabbMax.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedAabbMin, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %newAabbMin.addr, align 4
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %1, i32 0)
  %arraydecay2 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedAabbMax, i32 0, i32 0
  %2 = load %class.btVector3*, %class.btVector3** %newAabbMax.addr, align 4
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay2, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i32 1)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %3, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %4 = load i32, i32* %nodeIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %4)
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call, i32 0, i32 0
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 %5
  %6 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %6 to i32
  %7 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedAabbMin, i32 0, i32 %7
  %8 = load i16, i16* %arrayidx3, align 2
  %conv4 = zext i16 %8 to i32
  %cmp5 = icmp sgt i32 %conv, %conv4
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedAabbMin, i32 0, i32 %9
  %10 = load i16, i16* %arrayidx7, align 2
  %m_quantizedContiguousNodes8 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %11 = load i32, i32* %nodeIndex.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes8, i32 %11)
  %m_quantizedAabbMin10 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call9, i32 0, i32 0
  %12 = load i32, i32* %i, align 4
  %arrayidx11 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin10, i32 0, i32 %12
  store i16 %10, i16* %arrayidx11, align 2
  br label %if.end

if.end:                                           ; preds = %if.then6, %for.body
  %m_quantizedContiguousNodes12 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %13 = load i32, i32* %nodeIndex.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes12, i32 %13)
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call13, i32 0, i32 1
  %14 = load i32, i32* %i, align 4
  %arrayidx14 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 %14
  %15 = load i16, i16* %arrayidx14, align 2
  %conv15 = zext i16 %15 to i32
  %16 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedAabbMax, i32 0, i32 %16
  %17 = load i16, i16* %arrayidx16, align 2
  %conv17 = zext i16 %17 to i32
  %cmp18 = icmp slt i32 %conv15, %conv17
  br i1 %cmp18, label %if.then19, label %if.end25

if.then19:                                        ; preds = %if.end
  %18 = load i32, i32* %i, align 4
  %arrayidx20 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedAabbMax, i32 0, i32 %18
  %19 = load i16, i16* %arrayidx20, align 2
  %m_quantizedContiguousNodes21 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %20 = load i32, i32* %nodeIndex.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes21, i32 %20)
  %m_quantizedAabbMax23 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call22, i32 0, i32 1
  %21 = load i32, i32* %i, align 4
  %arrayidx24 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax23, i32 0, i32 %21
  store i16 %19, i16* %arrayidx24, align 2
  br label %if.end25

if.end25:                                         ; preds = %if.then19, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end25
  %22 = load i32, i32* %i, align 4
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end29

if.else:                                          ; preds = %entry
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %23 = load i32, i32* %nodeIndex.addr, align 4
  %call26 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %23)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call26, i32 0, i32 0
  %24 = load %class.btVector3*, %class.btVector3** %newAabbMin.addr, align 4
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %m_aabbMinOrg, %class.btVector3* nonnull align 4 dereferenceable(16) %24)
  %m_contiguousNodes27 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %25 = load i32, i32* %nodeIndex.addr, align 4
  %call28 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes27, i32 %25)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call28, i32 0, i32 1
  %26 = load %class.btVector3*, %class.btVector3** %newAabbMax.addr, align 4
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %m_aabbMaxOrg, %class.btVector3* nonnull align 4 dereferenceable(16) %26)
  br label %if.end29

if.end29:                                         ; preds = %if.else, %for.end
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK14btQuantizedBvh10getAabbMinEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuantizedBvh* %this, i32 %nodeIndex) #2 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeIndex.addr = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i32 %nodeIndex, i32* %nodeIndex.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %1 = load i32, i32* %nodeIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes, i32 %1)
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh10unQuantizeEPKt(%class.btVector3* sret align 4 %agg.result, %class.btQuantizedBvh* %this1, i16* %arrayidx)
  br label %return

if.end:                                           ; preds = %entry
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %2 = load i32, i32* %nodeIndex.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_leafNodes, i32 %2)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call2, i32 0, i32 0
  %3 = bitcast %class.btVector3* %agg.result to i8*
  %4 = bitcast %class.btVector3* %m_aabbMinOrg to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK14btQuantizedBvh10getAabbMaxEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuantizedBvh* %this, i32 %nodeIndex) #2 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeIndex.addr = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i32 %nodeIndex, i32* %nodeIndex.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %1 = load i32, i32* %nodeIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes, i32 %1)
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh10unQuantizeEPKt(%class.btVector3* sret align 4 %agg.result, %class.btQuantizedBvh* %this1, i16* %arrayidx)
  br label %return

if.end:                                           ; preds = %entry
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %2 = load i32, i32* %nodeIndex.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_leafNodes, i32 %2)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call2, i32 0, i32 1
  %3 = bitcast %class.btVector3* %agg.result to i8*
  %4 = bitcast %class.btVector3* %m_aabbMaxOrg to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btQuantizedBvh20updateSubtreeHeadersEii(%class.btQuantizedBvh* %this, i32 %leftChildNodexIndex, i32 %rightChildNodexIndex) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %leftChildNodexIndex.addr = alloca i32, align 4
  %rightChildNodexIndex.addr = alloca i32, align 4
  %leftChildNode = alloca %struct.btQuantizedBvhNode*, align 4
  %leftSubTreeSize = alloca i32, align 4
  %leftSubTreeSizeInBytes = alloca i32, align 4
  %rightChildNode = alloca %struct.btQuantizedBvhNode*, align 4
  %rightSubTreeSize = alloca i32, align 4
  %rightSubTreeSizeInBytes = alloca i32, align 4
  %subtree = alloca %class.btBvhSubtreeInfo*, align 4
  %ref.tmp = alloca %class.btBvhSubtreeInfo, align 4
  %subtree17 = alloca %class.btBvhSubtreeInfo*, align 4
  %ref.tmp19 = alloca %class.btBvhSubtreeInfo, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i32 %leftChildNodexIndex, i32* %leftChildNodexIndex.addr, align 4
  store i32 %rightChildNodexIndex, i32* %rightChildNodexIndex.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %0 = load i32, i32* %leftChildNodexIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %0)
  store %struct.btQuantizedBvhNode* %call, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  %1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  %call2 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %1)
  br i1 %call2, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  %call3 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %2)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ 1, %cond.true ], [ %call3, %cond.false ]
  store i32 %cond, i32* %leftSubTreeSize, align 4
  %3 = load i32, i32* %leftSubTreeSize, align 4
  %mul = mul nsw i32 %3, 16
  store i32 %mul, i32* %leftSubTreeSizeInBytes, align 4
  %m_quantizedContiguousNodes4 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %4 = load i32, i32* %rightChildNodexIndex.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes4, i32 %4)
  store %struct.btQuantizedBvhNode* %call5, %struct.btQuantizedBvhNode** %rightChildNode, align 4
  %5 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4
  %call6 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %5)
  br i1 %call6, label %cond.true7, label %cond.false8

cond.true7:                                       ; preds = %cond.end
  br label %cond.end10

cond.false8:                                      ; preds = %cond.end
  %6 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4
  %call9 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %6)
  br label %cond.end10

cond.end10:                                       ; preds = %cond.false8, %cond.true7
  %cond11 = phi i32 [ 1, %cond.true7 ], [ %call9, %cond.false8 ]
  store i32 %cond11, i32* %rightSubTreeSize, align 4
  %7 = load i32, i32* %rightSubTreeSize, align 4
  %mul12 = mul nsw i32 %7, 16
  store i32 %mul12, i32* %rightSubTreeSizeInBytes, align 4
  %8 = load i32, i32* %leftSubTreeSizeInBytes, align 4
  %cmp = icmp sle i32 %8, 2048
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end10
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call13 = call %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* %ref.tmp)
  %call14 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_(%class.btAlignedObjectArray.4* %m_SubtreeHeaders, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %ref.tmp)
  store %class.btBvhSubtreeInfo* %call14, %class.btBvhSubtreeInfo** %subtree, align 4
  %9 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %10 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  call void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %9, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %10)
  %11 = load i32, i32* %leftChildNodexIndex.addr, align 4
  %12 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %12, i32 0, i32 2
  store i32 %11, i32* %m_rootNodeIndex, align 4
  %13 = load i32, i32* %leftSubTreeSize, align 4
  %14 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %14, i32 0, i32 3
  store i32 %13, i32* %m_subtreeSize, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %cond.end10
  %15 = load i32, i32* %rightSubTreeSizeInBytes, align 4
  %cmp15 = icmp sle i32 %15, 2048
  br i1 %cmp15, label %if.then16, label %if.end24

if.then16:                                        ; preds = %if.end
  %m_SubtreeHeaders18 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call20 = call %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* %ref.tmp19)
  %call21 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6expandERKS0_(%class.btAlignedObjectArray.4* %m_SubtreeHeaders18, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %ref.tmp19)
  store %class.btBvhSubtreeInfo* %call21, %class.btBvhSubtreeInfo** %subtree17, align 4
  %16 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree17, align 4
  %17 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4
  call void @_ZN16btBvhSubtreeInfo23setAabbFromQuantizeNodeERK18btQuantizedBvhNode(%class.btBvhSubtreeInfo* %16, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %17)
  %18 = load i32, i32* %rightChildNodexIndex.addr, align 4
  %19 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree17, align 4
  %m_rootNodeIndex22 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %19, i32 0, i32 2
  store i32 %18, i32* %m_rootNodeIndex22, align 4
  %20 = load i32, i32* %rightSubTreeSize, align 4
  %21 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree17, align 4
  %m_subtreeSize23 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %21, i32 0, i32 3
  store i32 %20, i32* %m_subtreeSize23, align 4
  br label %if.end24

if.end24:                                         ; preds = %if.then16, %if.end
  %m_SubtreeHeaders25 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call26 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders25)
  %m_subtreeHeaderCount = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  store i32 %call26, i32* %m_subtreeHeaderCount, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN14btQuantizedBvh26setInternalNodeEscapeIndexEii(%class.btQuantizedBvh* %this, i32 %nodeIndex, i32 %escapeIndex) #2 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeIndex.addr = alloca i32, align 4
  %escapeIndex.addr = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i32 %nodeIndex, i32* %nodeIndex.addr, align 4
  store i32 %escapeIndex, i32* %escapeIndex.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %escapeIndex.addr, align 4
  %sub = sub nsw i32 0, %1
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %2 = load i32, i32* %nodeIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %2)
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call, i32 0, i32 2
  store i32 %sub, i32* %m_escapeIndexOrTriangleIndex, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = load i32, i32* %escapeIndex.addr, align 4
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %4 = load i32, i32* %nodeIndex.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %4)
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call2, i32 0, i32 2
  store i32 %3, i32* %m_escapeIndex, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btQuantizedBvh13swapLeafNodesEii(%class.btQuantizedBvh* %this, i32 %i, i32 %splitIndex) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %i.addr = alloca i32, align 4
  %splitIndex.addr = alloca i32, align 4
  %tmp = alloca %struct.btQuantizedBvhNode, align 4
  %tmp8 = alloca %struct.btOptimizedBvhNode, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  store i32 %splitIndex, i32* %splitIndex.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %1 = load i32, i32* %i.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes, i32 %1)
  %2 = bitcast %struct.btQuantizedBvhNode* %tmp to i8*
  %3 = bitcast %struct.btQuantizedBvhNode* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %m_quantizedLeafNodes2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %4 = load i32, i32* %splitIndex.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes2, i32 %4)
  %m_quantizedLeafNodes4 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %5 = load i32, i32* %i.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes4, i32 %5)
  %6 = bitcast %struct.btQuantizedBvhNode* %call5 to i8*
  %7 = bitcast %struct.btQuantizedBvhNode* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_quantizedLeafNodes6 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %8 = load i32, i32* %splitIndex.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes6, i32 %8)
  %9 = bitcast %struct.btQuantizedBvhNode* %call7 to i8*
  %10 = bitcast %struct.btQuantizedBvhNode* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %11 = load i32, i32* %i.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_leafNodes, i32 %11)
  %12 = bitcast %struct.btOptimizedBvhNode* %tmp8 to i8*
  %13 = bitcast %struct.btOptimizedBvhNode* %call9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 64, i1 false)
  %m_leafNodes10 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %14 = load i32, i32* %splitIndex.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_leafNodes10, i32 %14)
  %m_leafNodes12 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %15 = load i32, i32* %i.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_leafNodes12, i32 %15)
  %16 = bitcast %struct.btOptimizedBvhNode* %call13 to i8*
  %17 = bitcast %struct.btOptimizedBvhNode* %call11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 64, i1 false)
  %m_leafNodes14 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %18 = load i32, i32* %splitIndex.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_leafNodes14, i32 %18)
  %19 = bitcast %struct.btOptimizedBvhNode* %call15 to i8*
  %20 = bitcast %struct.btOptimizedBvhNode* %tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 64, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: noinline optnone
define hidden void @_ZNK14btQuantizedBvh26reportAabbOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %quantizedQueryAabbMin = alloca [3 x i16], align 2
  %quantizedQueryAabbMax = alloca [3 x i16], align 2
  %rootNode = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  call void @_ZNK14btQuantizedBvh17quantizeWithClampEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %1, i32 0)
  %arraydecay2 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZNK14btQuantizedBvh17quantizeWithClampEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay2, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i32 1)
  %m_traversalMode = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 12
  %3 = load i32, i32* %m_traversalMode, align 4
  switch i32 %3, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb5
    i32 2, label %sw.bb8
  ]

sw.bb:                                            ; preds = %if.then
  %4 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %arraydecay3 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  %arraydecay4 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %5 = load i32, i32* %m_curNodeIndex, align 4
  call void @_ZNK14btQuantizedBvh26walkStacklessQuantizedTreeEP21btNodeOverlapCallbackPtS2_ii(%class.btQuantizedBvh* %this1, %class.btNodeOverlapCallback* %4, i16* %arraydecay3, i16* %arraydecay4, i32 0, i32 %5)
  br label %sw.epilog

sw.bb5:                                           ; preds = %if.then
  %6 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %arraydecay6 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  %arraydecay7 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh39walkStacklessQuantizedTreeCacheFriendlyEP21btNodeOverlapCallbackPtS2_(%class.btQuantizedBvh* %this1, %class.btNodeOverlapCallback* %6, i16* %arraydecay6, i16* %arraydecay7)
  br label %sw.epilog

sw.bb8:                                           ; preds = %if.then
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 0)
  store %struct.btQuantizedBvhNode* %call, %struct.btQuantizedBvhNode** %rootNode, align 4
  %7 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %8 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %arraydecay9 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  %arraydecay10 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_(%class.btQuantizedBvh* %this1, %struct.btQuantizedBvhNode* %7, %class.btNodeOverlapCallback* %8, i16* %arraydecay9, i16* %arraydecay10)
  br label %sw.epilog

sw.default:                                       ; preds = %if.then
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb8, %sw.bb5, %sw.bb
  br label %if.end

if.else:                                          ; preds = %entry
  %9 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %10 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZNK14btQuantizedBvh17walkStacklessTreeEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh* %this1, %class.btNodeOverlapCallback* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11)
  br label %if.end

if.end:                                           ; preds = %if.else, %sw.epilog
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK14btQuantizedBvh17quantizeWithClampEPtRK9btVector3i(%class.btQuantizedBvh* %this, i16* %out, %class.btVector3* nonnull align 4 dereferenceable(16) %point2, i32 %isMax) #2 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %out.addr = alloca i16*, align 4
  %point2.addr = alloca %class.btVector3*, align 4
  %isMax.addr = alloca i32, align 4
  %clampedPoint = alloca %class.btVector3, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i16* %out, i16** %out.addr, align 4
  store %class.btVector3* %point2, %class.btVector3** %point2.addr, align 4
  store i32 %isMax, i32* %isMax.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %point2.addr, align 4
  %1 = bitcast %class.btVector3* %clampedPoint to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %clampedPoint, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin)
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %clampedPoint, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax)
  %3 = load i16*, i16** %out.addr, align 4
  %4 = load i32, i32* %isMax.addr, align 4
  call void @_ZNK14btQuantizedBvh8quantizeEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedPoint, i32 %4)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK14btQuantizedBvh26walkStacklessQuantizedTreeEP21btNodeOverlapCallbackPtS2_ii(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, i16* %quantizedQueryAabbMin, i16* %quantizedQueryAabbMax, i32 %startNodeIndex, i32 %endNodeIndex) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %quantizedQueryAabbMin.addr = alloca i16*, align 4
  %quantizedQueryAabbMax.addr = alloca i16*, align 4
  %startNodeIndex.addr = alloca i32, align 4
  %endNodeIndex.addr = alloca i32, align 4
  %curIndex = alloca i32, align 4
  %walkIterations = alloca i32, align 4
  %subTreeSize = alloca i32, align 4
  %rootNode = alloca %struct.btQuantizedBvhNode*, align 4
  %escapeIndex = alloca i32, align 4
  %isLeafNode = alloca i8, align 1
  %aabbOverlap = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  store i16* %quantizedQueryAabbMin, i16** %quantizedQueryAabbMin.addr, align 4
  store i16* %quantizedQueryAabbMax, i16** %quantizedQueryAabbMax.addr, align 4
  store i32 %startNodeIndex, i32* %startNodeIndex.addr, align 4
  store i32 %endNodeIndex, i32* %endNodeIndex.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = load i32, i32* %startNodeIndex.addr, align 4
  store i32 %0, i32* %curIndex, align 4
  store i32 0, i32* %walkIterations, align 4
  %1 = load i32, i32* %endNodeIndex.addr, align 4
  %2 = load i32, i32* %startNodeIndex.addr, align 4
  %sub = sub nsw i32 %1, %2
  store i32 %sub, i32* %subTreeSize, align 4
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %3 = load i32, i32* %startNodeIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %3)
  store %struct.btQuantizedBvhNode* %call, %struct.btQuantizedBvhNode** %rootNode, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end13, %entry
  %4 = load i32, i32* %curIndex, align 4
  %5 = load i32, i32* %endNodeIndex.addr, align 4
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = load i32, i32* %walkIterations, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %walkIterations, align 4
  %7 = load i16*, i16** %quantizedQueryAabbMin.addr, align 4
  %8 = load i16*, i16** %quantizedQueryAabbMax.addr, align 4
  %9 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %9, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %10 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %10, i32 0, i32 1
  %arraydecay2 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %call3 = call i32 @_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_(i16* %7, i16* %8, i16* %arraydecay, i16* %arraydecay2)
  store i32 %call3, i32* %aabbOverlap, align 4
  %11 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %call4 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %11)
  %frombool = zext i1 %call4 to i8
  store i8 %frombool, i8* %isLeafNode, align 1
  %12 = load i8, i8* %isLeafNode, align 1
  %tobool = trunc i8 %12 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %while.body
  %13 = load i32, i32* %aabbOverlap, align 4
  %tobool5 = icmp ne i32 %13, 0
  br i1 %tobool5, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %14 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %15 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %call6 = call i32 @_ZNK18btQuantizedBvhNode9getPartIdEv(%struct.btQuantizedBvhNode* %15)
  %16 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %call7 = call i32 @_ZNK18btQuantizedBvhNode16getTriangleIndexEv(%struct.btQuantizedBvhNode* %16)
  %17 = bitcast %class.btNodeOverlapCallback* %14 to void (%class.btNodeOverlapCallback*, i32, i32)***
  %vtable = load void (%class.btNodeOverlapCallback*, i32, i32)**, void (%class.btNodeOverlapCallback*, i32, i32)*** %17, align 4
  %vfn = getelementptr inbounds void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vtable, i64 2
  %18 = load void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vfn, align 4
  call void %18(%class.btNodeOverlapCallback* %14, i32 %call6, i32 %call7)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %while.body
  %19 = load i32, i32* %aabbOverlap, align 4
  %cmp8 = icmp ne i32 %19, 0
  br i1 %cmp8, label %if.then10, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %20 = load i8, i8* %isLeafNode, align 1
  %tobool9 = trunc i8 %20 to i1
  br i1 %tobool9, label %if.then10, label %if.else

if.then10:                                        ; preds = %lor.lhs.false, %if.end
  %21 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %incdec.ptr = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %21, i32 1
  store %struct.btQuantizedBvhNode* %incdec.ptr, %struct.btQuantizedBvhNode** %rootNode, align 4
  %22 = load i32, i32* %curIndex, align 4
  %inc11 = add nsw i32 %22, 1
  store i32 %inc11, i32* %curIndex, align 4
  br label %if.end13

if.else:                                          ; preds = %lor.lhs.false
  %23 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %call12 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %23)
  store i32 %call12, i32* %escapeIndex, align 4
  %24 = load i32, i32* %escapeIndex, align 4
  %25 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %add.ptr = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %25, i32 %24
  store %struct.btQuantizedBvhNode* %add.ptr, %struct.btQuantizedBvhNode** %rootNode, align 4
  %26 = load i32, i32* %escapeIndex, align 4
  %27 = load i32, i32* %curIndex, align 4
  %add = add nsw i32 %27, %26
  store i32 %add, i32* %curIndex, align 4
  br label %if.end13

if.end13:                                         ; preds = %if.else, %if.then10
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %28 = load i32, i32* @maxIterations, align 4
  %29 = load i32, i32* %walkIterations, align 4
  %cmp14 = icmp slt i32 %28, %29
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %while.end
  %30 = load i32, i32* %walkIterations, align 4
  store i32 %30, i32* @maxIterations, align 4
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %while.end
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK14btQuantizedBvh39walkStacklessQuantizedTreeCacheFriendlyEP21btNodeOverlapCallbackPtS2_(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, i16* %quantizedQueryAabbMin, i16* %quantizedQueryAabbMax) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %quantizedQueryAabbMin.addr = alloca i16*, align 4
  %quantizedQueryAabbMax.addr = alloca i16*, align 4
  %i = alloca i32, align 4
  %subtree = alloca %class.btBvhSubtreeInfo*, align 4
  %overlap = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  store i16* %quantizedQueryAabbMin, i16** %quantizedQueryAabbMin.addr, align 4
  store i16* %quantizedQueryAabbMax, i16** %quantizedQueryAabbMax.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_SubtreeHeaders2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %1 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders2, i32 %1)
  store %class.btBvhSubtreeInfo* %call3, %class.btBvhSubtreeInfo** %subtree, align 4
  %2 = load i16*, i16** %quantizedQueryAabbMin.addr, align 4
  %3 = load i16*, i16** %quantizedQueryAabbMax.addr, align 4
  %4 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_quantizedAabbMin = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %4, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %5 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_quantizedAabbMax = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %5, i32 0, i32 1
  %arraydecay4 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %call5 = call i32 @_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_(i16* %2, i16* %3, i16* %arraydecay, i16* %arraydecay4)
  store i32 %call5, i32* %overlap, align 4
  %6 = load i32, i32* %overlap, align 4
  %cmp6 = icmp ne i32 %6, 0
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %7 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %8 = load i16*, i16** %quantizedQueryAabbMin.addr, align 4
  %9 = load i16*, i16** %quantizedQueryAabbMax.addr, align 4
  %10 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %10, i32 0, i32 2
  %11 = load i32, i32* %m_rootNodeIndex, align 4
  %12 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_rootNodeIndex7 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %12, i32 0, i32 2
  %13 = load i32, i32* %m_rootNodeIndex7, align 4
  %14 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %subtree, align 4
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %14, i32 0, i32 3
  %15 = load i32, i32* %m_subtreeSize, align 4
  %add = add nsw i32 %13, %15
  call void @_ZNK14btQuantizedBvh26walkStacklessQuantizedTreeEP21btNodeOverlapCallbackPtS2_ii(%class.btQuantizedBvh* %this1, %class.btNodeOverlapCallback* %7, i16* %8, i16* %9, i32 %11, i32 %add)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %16 = load i32, i32* %i, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %0, i32 %1
  ret %struct.btQuantizedBvhNode* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_(%class.btQuantizedBvh* %this, %struct.btQuantizedBvhNode* %currentNode, %class.btNodeOverlapCallback* %nodeCallback, i16* %quantizedQueryAabbMin, i16* %quantizedQueryAabbMax) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %currentNode.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %quantizedQueryAabbMin.addr = alloca i16*, align 4
  %quantizedQueryAabbMax.addr = alloca i16*, align 4
  %isLeafNode = alloca i8, align 1
  %aabbOverlap = alloca i32, align 4
  %leftChildNode = alloca %struct.btQuantizedBvhNode*, align 4
  %rightChildNode = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store %struct.btQuantizedBvhNode* %currentNode, %struct.btQuantizedBvhNode** %currentNode.addr, align 4
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  store i16* %quantizedQueryAabbMin, i16** %quantizedQueryAabbMin.addr, align 4
  store i16* %quantizedQueryAabbMax, i16** %quantizedQueryAabbMax.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = load i16*, i16** %quantizedQueryAabbMin.addr, align 4
  %1 = load i16*, i16** %quantizedQueryAabbMax.addr, align 4
  %2 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %currentNode.addr, align 4
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %2, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %3 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %currentNode.addr, align 4
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %3, i32 0, i32 1
  %arraydecay2 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %call = call i32 @_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_(i16* %0, i16* %1, i16* %arraydecay, i16* %arraydecay2)
  store i32 %call, i32* %aabbOverlap, align 4
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %currentNode.addr, align 4
  %call3 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %4)
  %frombool = zext i1 %call3 to i8
  store i8 %frombool, i8* %isLeafNode, align 1
  %5 = load i32, i32* %aabbOverlap, align 4
  %cmp = icmp ne i32 %5, 0
  br i1 %cmp, label %if.then, label %if.end11

if.then:                                          ; preds = %entry
  %6 = load i8, i8* %isLeafNode, align 1
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.then
  %7 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %8 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %currentNode.addr, align 4
  %call5 = call i32 @_ZNK18btQuantizedBvhNode9getPartIdEv(%struct.btQuantizedBvhNode* %8)
  %9 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %currentNode.addr, align 4
  %call6 = call i32 @_ZNK18btQuantizedBvhNode16getTriangleIndexEv(%struct.btQuantizedBvhNode* %9)
  %10 = bitcast %class.btNodeOverlapCallback* %7 to void (%class.btNodeOverlapCallback*, i32, i32)***
  %vtable = load void (%class.btNodeOverlapCallback*, i32, i32)**, void (%class.btNodeOverlapCallback*, i32, i32)*** %10, align 4
  %vfn = getelementptr inbounds void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vtable, i64 2
  %11 = load void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vfn, align 4
  call void %11(%class.btNodeOverlapCallback* %7, i32 %call5, i32 %call6)
  br label %if.end

if.else:                                          ; preds = %if.then
  %12 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %currentNode.addr, align 4
  %add.ptr = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %12, i32 1
  store %struct.btQuantizedBvhNode* %add.ptr, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  %13 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  %14 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %15 = load i16*, i16** %quantizedQueryAabbMin.addr, align 4
  %16 = load i16*, i16** %quantizedQueryAabbMax.addr, align 4
  call void @_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_(%class.btQuantizedBvh* %this1, %struct.btQuantizedBvhNode* %13, %class.btNodeOverlapCallback* %14, i16* %15, i16* %16)
  %17 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  %call7 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %17)
  br i1 %call7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %18 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  %add.ptr8 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %18, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %19 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  %20 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %leftChildNode, align 4
  %call9 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %20)
  %add.ptr10 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %19, i32 %call9
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btQuantizedBvhNode* [ %add.ptr8, %cond.true ], [ %add.ptr10, %cond.false ]
  store %struct.btQuantizedBvhNode* %cond, %struct.btQuantizedBvhNode** %rightChildNode, align 4
  %21 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rightChildNode, align 4
  %22 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %23 = load i16*, i16** %quantizedQueryAabbMin.addr, align 4
  %24 = load i16*, i16** %quantizedQueryAabbMax.addr, align 4
  call void @_ZNK14btQuantizedBvh42walkRecursiveQuantizedTreeAgainstQueryAabbEPK18btQuantizedBvhNodeP21btNodeOverlapCallbackPtS5_(%class.btQuantizedBvh* %this1, %struct.btQuantizedBvhNode* %21, %class.btNodeOverlapCallback* %22, i16* %23, i16* %24)
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then4
  br label %if.end11

if.end11:                                         ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK14btQuantizedBvh17walkStacklessTreeEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %rootNode = alloca %struct.btOptimizedBvhNode*, align 4
  %escapeIndex = alloca i32, align 4
  %curIndex = alloca i32, align 4
  %walkIterations = alloca i32, align 4
  %isLeafNode = alloca i8, align 1
  %aabbOverlap = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes, i32 0)
  store %struct.btOptimizedBvhNode* %call, %struct.btOptimizedBvhNode** %rootNode, align 4
  store i32 0, i32* %curIndex, align 4
  store i32 0, i32* %walkIterations, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end10, %entry
  %0 = load i32, i32* %curIndex, align 4
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %1 = load i32, i32* %m_curNodeIndex, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load i32, i32* %walkIterations, align 4
  %inc = add nsw i32 %2, 1
  store i32 %inc, i32* %walkIterations, align 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %5 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %5, i32 0, i32 0
  %6 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %6, i32 0, i32 1
  %call2 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMinOrg, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMaxOrg)
  %conv = zext i1 %call2 to i32
  store i32 %conv, i32* %aabbOverlap, align 4
  %7 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %7, i32 0, i32 2
  %8 = load i32, i32* %m_escapeIndex, align 4
  %cmp3 = icmp eq i32 %8, -1
  %frombool = zext i1 %cmp3 to i8
  store i8 %frombool, i8* %isLeafNode, align 1
  %9 = load i8, i8* %isLeafNode, align 1
  %tobool = trunc i8 %9 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %while.body
  %10 = load i32, i32* %aabbOverlap, align 4
  %cmp4 = icmp ne i32 %10, 0
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %11 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %12 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %12, i32 0, i32 3
  %13 = load i32, i32* %m_subPart, align 4
  %14 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %14, i32 0, i32 4
  %15 = load i32, i32* %m_triangleIndex, align 4
  %16 = bitcast %class.btNodeOverlapCallback* %11 to void (%class.btNodeOverlapCallback*, i32, i32)***
  %vtable = load void (%class.btNodeOverlapCallback*, i32, i32)**, void (%class.btNodeOverlapCallback*, i32, i32)*** %16, align 4
  %vfn = getelementptr inbounds void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vtable, i64 2
  %17 = load void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vfn, align 4
  call void %17(%class.btNodeOverlapCallback* %11, i32 %13, i32 %15)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %while.body
  %18 = load i32, i32* %aabbOverlap, align 4
  %cmp5 = icmp ne i32 %18, 0
  br i1 %cmp5, label %if.then7, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %19 = load i8, i8* %isLeafNode, align 1
  %tobool6 = trunc i8 %19 to i1
  br i1 %tobool6, label %if.then7, label %if.else

if.then7:                                         ; preds = %lor.lhs.false, %if.end
  %20 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %incdec.ptr = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %20, i32 1
  store %struct.btOptimizedBvhNode* %incdec.ptr, %struct.btOptimizedBvhNode** %rootNode, align 4
  %21 = load i32, i32* %curIndex, align 4
  %inc8 = add nsw i32 %21, 1
  store i32 %inc8, i32* %curIndex, align 4
  br label %if.end10

if.else:                                          ; preds = %lor.lhs.false
  %22 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %m_escapeIndex9 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %22, i32 0, i32 2
  %23 = load i32, i32* %m_escapeIndex9, align 4
  store i32 %23, i32* %escapeIndex, align 4
  %24 = load i32, i32* %escapeIndex, align 4
  %25 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %add.ptr = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %25, i32 %24
  store %struct.btOptimizedBvhNode* %add.ptr, %struct.btOptimizedBvhNode** %rootNode, align 4
  %26 = load i32, i32* %escapeIndex, align 4
  %27 = load i32, i32* %curIndex, align 4
  %add = add nsw i32 %27, %26
  store i32 %add, i32* %curIndex, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.else, %if.then7
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %28 = load i32, i32* @maxIterations, align 4
  %29 = load i32, i32* %walkIterations, align 4
  %cmp11 = icmp slt i32 %28, %29
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %while.end
  %30 = load i32, i32* %walkIterations, align 4
  store i32 %30, i32* @maxIterations, align 4
  br label %if.end13

if.end13:                                         ; preds = %if.then12, %while.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %0, i32 %1
  ret %struct.btOptimizedBvhNode* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin2, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax2) #1 comdat {
entry:
  %aabbMin1.addr = alloca %class.btVector3*, align 4
  %aabbMax1.addr = alloca %class.btVector3*, align 4
  %aabbMin2.addr = alloca %class.btVector3*, align 4
  %aabbMax2.addr = alloca %class.btVector3*, align 4
  %overlap = alloca i8, align 1
  store %class.btVector3* %aabbMin1, %class.btVector3** %aabbMin1.addr, align 4
  store %class.btVector3* %aabbMax1, %class.btVector3** %aabbMax1.addr, align 4
  store %class.btVector3* %aabbMin2, %class.btVector3** %aabbMin2.addr, align 4
  store %class.btVector3* %aabbMax2, %class.btVector3** %aabbMax2.addr, align 4
  store i8 1, i8* %overlap, align 1
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call3, align 4
  %cmp4 = fcmp olt float %5, %7
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %lor.lhs.false, %entry
  br label %cond.end

cond.false:                                       ; preds = %lor.lhs.false
  %8 = load i8, i8* %overlap, align 1
  %tobool = trunc i8 %8 to i1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i1 [ false, %cond.true ], [ %tobool, %cond.false ]
  %frombool = zext i1 %cond to i8
  store i8 %frombool, i8* %overlap, align 1
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %9)
  %10 = load float, float* %call5, align 4
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %11)
  %12 = load float, float* %call6, align 4
  %cmp7 = fcmp ogt float %10, %12
  br i1 %cmp7, label %cond.true12, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %cond.end
  %13 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %13)
  %14 = load float, float* %call9, align 4
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %15)
  %16 = load float, float* %call10, align 4
  %cmp11 = fcmp olt float %14, %16
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %lor.lhs.false8, %cond.end
  br label %cond.end15

cond.false13:                                     ; preds = %lor.lhs.false8
  %17 = load i8, i8* %overlap, align 1
  %tobool14 = trunc i8 %17 to i1
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false13, %cond.true12
  %cond16 = phi i1 [ false, %cond.true12 ], [ %tobool14, %cond.false13 ]
  %frombool17 = zext i1 %cond16 to i8
  store i8 %frombool17, i8* %overlap, align 1
  %18 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %18)
  %19 = load float, float* %call18, align 4
  %20 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %20)
  %21 = load float, float* %call19, align 4
  %cmp20 = fcmp ogt float %19, %21
  br i1 %cmp20, label %cond.true25, label %lor.lhs.false21

lor.lhs.false21:                                  ; preds = %cond.end15
  %22 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %22)
  %23 = load float, float* %call22, align 4
  %24 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %24)
  %25 = load float, float* %call23, align 4
  %cmp24 = fcmp olt float %23, %25
  br i1 %cmp24, label %cond.true25, label %cond.false26

cond.true25:                                      ; preds = %lor.lhs.false21, %cond.end15
  br label %cond.end28

cond.false26:                                     ; preds = %lor.lhs.false21
  %26 = load i8, i8* %overlap, align 1
  %tobool27 = trunc i8 %26 to i1
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false26, %cond.true25
  %cond29 = phi i1 [ false, %cond.true25 ], [ %tobool27, %cond.false26 ]
  %frombool30 = zext i1 %cond29 to i8
  store i8 %frombool30, i8* %overlap, align 1
  %27 = load i8, i8* %overlap, align 1
  %tobool31 = trunc i8 %27 to i1
  ret i1 %tobool31
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_(i16* %aabbMin1, i16* %aabbMax1, i16* %aabbMin2, i16* %aabbMax2) #2 comdat {
entry:
  %aabbMin1.addr = alloca i16*, align 4
  %aabbMax1.addr = alloca i16*, align 4
  %aabbMin2.addr = alloca i16*, align 4
  %aabbMax2.addr = alloca i16*, align 4
  store i16* %aabbMin1, i16** %aabbMin1.addr, align 4
  store i16* %aabbMax1, i16** %aabbMax1.addr, align 4
  store i16* %aabbMin2, i16** %aabbMin2.addr, align 4
  store i16* %aabbMax2, i16** %aabbMax2.addr, align 4
  %0 = load i16*, i16** %aabbMin1.addr, align 4
  %arrayidx = getelementptr inbounds i16, i16* %0, i32 0
  %1 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %1 to i32
  %2 = load i16*, i16** %aabbMax2.addr, align 4
  %arrayidx1 = getelementptr inbounds i16, i16* %2, i32 0
  %3 = load i16, i16* %arrayidx1, align 2
  %conv2 = zext i16 %3 to i32
  %cmp = icmp sle i32 %conv, %conv2
  %conv3 = zext i1 %cmp to i32
  %4 = load i16*, i16** %aabbMax1.addr, align 4
  %arrayidx4 = getelementptr inbounds i16, i16* %4, i32 0
  %5 = load i16, i16* %arrayidx4, align 2
  %conv5 = zext i16 %5 to i32
  %6 = load i16*, i16** %aabbMin2.addr, align 4
  %arrayidx6 = getelementptr inbounds i16, i16* %6, i32 0
  %7 = load i16, i16* %arrayidx6, align 2
  %conv7 = zext i16 %7 to i32
  %cmp8 = icmp sge i32 %conv5, %conv7
  %conv9 = zext i1 %cmp8 to i32
  %and = and i32 %conv3, %conv9
  %8 = load i16*, i16** %aabbMin1.addr, align 4
  %arrayidx10 = getelementptr inbounds i16, i16* %8, i32 2
  %9 = load i16, i16* %arrayidx10, align 2
  %conv11 = zext i16 %9 to i32
  %10 = load i16*, i16** %aabbMax2.addr, align 4
  %arrayidx12 = getelementptr inbounds i16, i16* %10, i32 2
  %11 = load i16, i16* %arrayidx12, align 2
  %conv13 = zext i16 %11 to i32
  %cmp14 = icmp sle i32 %conv11, %conv13
  %conv15 = zext i1 %cmp14 to i32
  %and16 = and i32 %and, %conv15
  %12 = load i16*, i16** %aabbMax1.addr, align 4
  %arrayidx17 = getelementptr inbounds i16, i16* %12, i32 2
  %13 = load i16, i16* %arrayidx17, align 2
  %conv18 = zext i16 %13 to i32
  %14 = load i16*, i16** %aabbMin2.addr, align 4
  %arrayidx19 = getelementptr inbounds i16, i16* %14, i32 2
  %15 = load i16, i16* %arrayidx19, align 2
  %conv20 = zext i16 %15 to i32
  %cmp21 = icmp sge i32 %conv18, %conv20
  %conv22 = zext i1 %cmp21 to i32
  %and23 = and i32 %and16, %conv22
  %16 = load i16*, i16** %aabbMin1.addr, align 4
  %arrayidx24 = getelementptr inbounds i16, i16* %16, i32 1
  %17 = load i16, i16* %arrayidx24, align 2
  %conv25 = zext i16 %17 to i32
  %18 = load i16*, i16** %aabbMax2.addr, align 4
  %arrayidx26 = getelementptr inbounds i16, i16* %18, i32 1
  %19 = load i16, i16* %arrayidx26, align 2
  %conv27 = zext i16 %19 to i32
  %cmp28 = icmp sle i32 %conv25, %conv27
  %conv29 = zext i1 %cmp28 to i32
  %and30 = and i32 %and23, %conv29
  %20 = load i16*, i16** %aabbMax1.addr, align 4
  %arrayidx31 = getelementptr inbounds i16, i16* %20, i32 1
  %21 = load i16, i16* %arrayidx31, align 2
  %conv32 = zext i16 %21 to i32
  %22 = load i16*, i16** %aabbMin2.addr, align 4
  %arrayidx33 = getelementptr inbounds i16, i16* %22, i32 1
  %23 = load i16, i16* %arrayidx33, align 2
  %conv34 = zext i16 %23 to i32
  %cmp35 = icmp sge i32 %conv32, %conv34
  %conv36 = zext i1 %cmp35 to i32
  %and37 = and i32 %and30, %conv36
  %call = call i32 @_Z8btSelectjii(i32 %and37, i32 1, i32 0)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK18btQuantizedBvhNode9getPartIdEv(%struct.btQuantizedBvhNode* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4
  %shr = ashr i32 %0, 21
  ret i32 %shr
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK18btQuantizedBvhNode16getTriangleIndexEv(%struct.btQuantizedBvhNode* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %x = alloca i32, align 4
  %y = alloca i32, align 4
  store %struct.btQuantizedBvhNode* %this, %struct.btQuantizedBvhNode** %this.addr, align 4
  %this1 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %this.addr, align 4
  store i32 0, i32* %x, align 4
  %0 = load i32, i32* %x, align 4
  %and = and i32 %0, 0
  %neg = xor i32 %and, -1
  %shl = shl i32 %neg, 21
  store i32 %shl, i32* %y, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %this1, i32 0, i32 2
  %1 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4
  %2 = load i32, i32* %y, align 4
  %neg2 = xor i32 %2, -1
  %and3 = and i32 %1, %neg2
  ret i32 %and3
}

; Function Attrs: noinline optnone
define hidden void @_ZNK14btQuantizedBvh27walkStacklessTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %raySource, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTarget, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 %startNodeIndex, i32 %endNodeIndex) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %raySource.addr = alloca %class.btVector3*, align 4
  %rayTarget.addr = alloca %class.btVector3*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %startNodeIndex.addr = alloca i32, align 4
  %endNodeIndex.addr = alloca i32, align 4
  %rootNode = alloca %struct.btOptimizedBvhNode*, align 4
  %escapeIndex = alloca i32, align 4
  %curIndex = alloca i32, align 4
  %walkIterations = alloca i32, align 4
  %isLeafNode = alloca i8, align 1
  %aabbOverlap = alloca i32, align 4
  %rayBoxOverlap = alloca i32, align 4
  %lambda_max = alloca float, align 4
  %rayAabbMin = alloca %class.btVector3, align 4
  %rayAabbMax = alloca %class.btVector3, align 4
  %rayDir = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %rayDirectionInverse = alloca %class.btVector3, align 4
  %sign = alloca [3 x i32], align 4
  %bounds = alloca [2 x %class.btVector3], align 16
  %param = alloca float, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  store %class.btVector3* %raySource, %class.btVector3** %raySource.addr, align 4
  store %class.btVector3* %rayTarget, %class.btVector3** %rayTarget.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store i32 %startNodeIndex, i32* %startNodeIndex.addr, align 4
  store i32 %endNodeIndex, i32* %endNodeIndex.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes, i32 0)
  store %struct.btOptimizedBvhNode* %call, %struct.btOptimizedBvhNode** %rootNode, align 4
  store i32 0, i32* %curIndex, align 4
  store i32 0, i32* %walkIterations, align 4
  store i32 0, i32* %aabbOverlap, align 4
  store i32 0, i32* %rayBoxOverlap, align 4
  store float 1.000000e+00, float* %lambda_max, align 4
  %0 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  %1 = bitcast %class.btVector3* %rayAabbMin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  %4 = bitcast %class.btVector3* %rayAabbMax to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %rayAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  %7 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %rayAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  %8 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %rayAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %9 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %rayAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %10 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4
  %11 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rayDir, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11)
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %rayDir)
  %12 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4
  %13 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13)
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %rayDir, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  store float %call5, float* %lambda_max, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %rayDirectionInverse)
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx = getelementptr inbounds float, float* %call7, i32 0
  %14 = load float, float* %arrayidx, align 4
  %cmp = fcmp oeq float %14, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  %15 = load float, float* %arrayidx9, align 4
  %div = fdiv float 1.000000e+00, %15
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ 0x43ABC16D60000000, %cond.true ], [ %div, %cond.false ]
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirectionInverse)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 0
  store float %cond, float* %arrayidx11, align 4
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  %16 = load float, float* %arrayidx13, align 4
  %cmp14 = fcmp oeq float %16, 0.000000e+00
  br i1 %cmp14, label %cond.true15, label %cond.false16

cond.true15:                                      ; preds = %cond.end
  br label %cond.end20

cond.false16:                                     ; preds = %cond.end
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  %17 = load float, float* %arrayidx18, align 4
  %div19 = fdiv float 1.000000e+00, %17
  br label %cond.end20

cond.end20:                                       ; preds = %cond.false16, %cond.true15
  %cond21 = phi float [ 0x43ABC16D60000000, %cond.true15 ], [ %div19, %cond.false16 ]
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirectionInverse)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 1
  store float %cond21, float* %arrayidx23, align 4
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 2
  %18 = load float, float* %arrayidx25, align 4
  %cmp26 = fcmp oeq float %18, 0.000000e+00
  br i1 %cmp26, label %cond.true27, label %cond.false28

cond.true27:                                      ; preds = %cond.end20
  br label %cond.end32

cond.false28:                                     ; preds = %cond.end20
  %call29 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 2
  %19 = load float, float* %arrayidx30, align 4
  %div31 = fdiv float 1.000000e+00, %19
  br label %cond.end32

cond.end32:                                       ; preds = %cond.false28, %cond.true27
  %cond33 = phi float [ 0x43ABC16D60000000, %cond.true27 ], [ %div31, %cond.false28 ]
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirectionInverse)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 2
  store float %cond33, float* %arrayidx35, align 4
  %arrayinit.begin = getelementptr inbounds [3 x i32], [3 x i32]* %sign, i32 0, i32 0
  %call36 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirectionInverse)
  %arrayidx37 = getelementptr inbounds float, float* %call36, i32 0
  %20 = load float, float* %arrayidx37, align 4
  %conv = fpext float %20 to double
  %cmp38 = fcmp olt double %conv, 0.000000e+00
  %conv39 = zext i1 %cmp38 to i32
  store i32 %conv39, i32* %arrayinit.begin, align 4
  %arrayinit.element = getelementptr inbounds i32, i32* %arrayinit.begin, i32 1
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirectionInverse)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 1
  %21 = load float, float* %arrayidx41, align 4
  %conv42 = fpext float %21 to double
  %cmp43 = fcmp olt double %conv42, 0.000000e+00
  %conv44 = zext i1 %cmp43 to i32
  store i32 %conv44, i32* %arrayinit.element, align 4
  %arrayinit.element45 = getelementptr inbounds i32, i32* %arrayinit.element, i32 1
  %call46 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirectionInverse)
  %arrayidx47 = getelementptr inbounds float, float* %call46, i32 2
  %22 = load float, float* %arrayidx47, align 4
  %conv48 = fpext float %22 to double
  %cmp49 = fcmp olt double %conv48, 0.000000e+00
  %conv50 = zext i1 %cmp49 to i32
  store i32 %conv50, i32* %arrayinit.element45, align 4
  %array.begin = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 2
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %cond.end32
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %cond.end32 ], [ %arrayctor.next, %arrayctor.loop ]
  %call51 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  br label %while.cond

while.cond:                                       ; preds = %if.end78, %arrayctor.cont
  %23 = load i32, i32* %curIndex, align 4
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %24 = load i32, i32* %m_curNodeIndex, align 4
  %cmp52 = icmp slt i32 %23, %24
  br i1 %cmp52, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  store float 1.000000e+00, float* %param, align 4
  %25 = load i32, i32* %walkIterations, align 4
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %walkIterations, align 4
  %26 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %26, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %27 = bitcast %class.btVector3* %arrayidx53 to i8*
  %28 = bitcast %class.btVector3* %m_aabbMinOrg to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %27, i8* align 4 %28, i32 16, i1 false)
  %29 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %29, i32 0, i32 1
  %arrayidx54 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 1
  %30 = bitcast %class.btVector3* %arrayidx54 to i8*
  %31 = bitcast %class.btVector3* %m_aabbMaxOrg to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %30, i8* align 4 %31, i32 16, i1 false)
  %32 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %arrayidx55 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %call56 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %arrayidx55, %class.btVector3* nonnull align 4 dereferenceable(16) %32)
  %33 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %arrayidx57 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 1
  %call58 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %arrayidx57, %class.btVector3* nonnull align 4 dereferenceable(16) %33)
  %34 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %m_aabbMinOrg59 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %34, i32 0, i32 0
  %35 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %m_aabbMaxOrg60 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %35, i32 0, i32 1
  %call61 = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %rayAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %rayAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMinOrg59, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMaxOrg60)
  %conv62 = zext i1 %call61 to i32
  store i32 %conv62, i32* %aabbOverlap, align 4
  %36 = load i32, i32* %aabbOverlap, align 4
  %tobool = icmp ne i32 %36, 0
  br i1 %tobool, label %cond.true63, label %cond.false66

cond.true63:                                      ; preds = %while.body
  %37 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  %arraydecay = getelementptr inbounds [3 x i32], [3 x i32]* %sign, i32 0, i32 0
  %arraydecay64 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %38 = load float, float* %lambda_max, align 4
  %call65 = call zeroext i1 @_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff(%class.btVector3* nonnull align 4 dereferenceable(16) %37, %class.btVector3* nonnull align 4 dereferenceable(16) %rayDirectionInverse, i32* %arraydecay, %class.btVector3* %arraydecay64, float* nonnull align 4 dereferenceable(4) %param, float 0.000000e+00, float %38)
  br label %cond.end67

cond.false66:                                     ; preds = %while.body
  br label %cond.end67

cond.end67:                                       ; preds = %cond.false66, %cond.true63
  %cond68 = phi i1 [ %call65, %cond.true63 ], [ false, %cond.false66 ]
  %conv69 = zext i1 %cond68 to i32
  store i32 %conv69, i32* %rayBoxOverlap, align 4
  %39 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %39, i32 0, i32 2
  %40 = load i32, i32* %m_escapeIndex, align 4
  %cmp70 = icmp eq i32 %40, -1
  %frombool = zext i1 %cmp70 to i8
  store i8 %frombool, i8* %isLeafNode, align 1
  %41 = load i8, i8* %isLeafNode, align 1
  %tobool71 = trunc i8 %41 to i1
  br i1 %tobool71, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %cond.end67
  %42 = load i32, i32* %rayBoxOverlap, align 4
  %cmp72 = icmp ne i32 %42, 0
  br i1 %cmp72, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %43 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %44 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %44, i32 0, i32 3
  %45 = load i32, i32* %m_subPart, align 4
  %46 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %46, i32 0, i32 4
  %47 = load i32, i32* %m_triangleIndex, align 4
  %48 = bitcast %class.btNodeOverlapCallback* %43 to void (%class.btNodeOverlapCallback*, i32, i32)***
  %vtable = load void (%class.btNodeOverlapCallback*, i32, i32)**, void (%class.btNodeOverlapCallback*, i32, i32)*** %48, align 4
  %vfn = getelementptr inbounds void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vtable, i64 2
  %49 = load void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vfn, align 4
  call void %49(%class.btNodeOverlapCallback* %43, i32 %45, i32 %47)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %cond.end67
  %50 = load i32, i32* %rayBoxOverlap, align 4
  %cmp73 = icmp ne i32 %50, 0
  br i1 %cmp73, label %if.then75, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end
  %51 = load i8, i8* %isLeafNode, align 1
  %tobool74 = trunc i8 %51 to i1
  br i1 %tobool74, label %if.then75, label %if.else

if.then75:                                        ; preds = %lor.lhs.false, %if.end
  %52 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %incdec.ptr = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %52, i32 1
  store %struct.btOptimizedBvhNode* %incdec.ptr, %struct.btOptimizedBvhNode** %rootNode, align 4
  %53 = load i32, i32* %curIndex, align 4
  %inc76 = add nsw i32 %53, 1
  store i32 %inc76, i32* %curIndex, align 4
  br label %if.end78

if.else:                                          ; preds = %lor.lhs.false
  %54 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %m_escapeIndex77 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %54, i32 0, i32 2
  %55 = load i32, i32* %m_escapeIndex77, align 4
  store i32 %55, i32* %escapeIndex, align 4
  %56 = load i32, i32* %escapeIndex, align 4
  %57 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %rootNode, align 4
  %add.ptr = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %57, i32 %56
  store %struct.btOptimizedBvhNode* %add.ptr, %struct.btOptimizedBvhNode** %rootNode, align 4
  %58 = load i32, i32* %escapeIndex, align 4
  %59 = load i32, i32* %curIndex, align 4
  %add = add nsw i32 %59, %58
  store i32 %add, i32* %curIndex, align 4
  br label %if.end78

if.end78:                                         ; preds = %if.else, %if.then75
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %60 = load i32, i32* @maxIterations, align 4
  %61 = load i32, i32* %walkIterations, align 4
  %cmp79 = icmp slt i32 %60, %61
  br i1 %cmp79, label %if.then80, label %if.end81

if.then80:                                        ; preds = %while.end
  %62 = load i32, i32* %walkIterations, align 4
  store i32 %62, i32* @maxIterations, align 4
  br label %if.end81

if.end81:                                         ; preds = %if.then80, %while.end
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff(%class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayInvDirection, i32* %raySign, %class.btVector3* %bounds, float* nonnull align 4 dereferenceable(4) %tmin, float %lambda_min, float %lambda_max) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayInvDirection.addr = alloca %class.btVector3*, align 4
  %raySign.addr = alloca i32*, align 4
  %bounds.addr = alloca %class.btVector3*, align 4
  %tmin.addr = alloca float*, align 4
  %lambda_min.addr = alloca float, align 4
  %lambda_max.addr = alloca float, align 4
  %tmax = alloca float, align 4
  %tymin = alloca float, align 4
  %tymax = alloca float, align 4
  %tzmin = alloca float, align 4
  %tzmax = alloca float, align 4
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4
  store %class.btVector3* %rayInvDirection, %class.btVector3** %rayInvDirection.addr, align 4
  store i32* %raySign, i32** %raySign.addr, align 4
  store %class.btVector3* %bounds, %class.btVector3** %bounds.addr, align 4
  store float* %tmin, float** %tmin.addr, align 4
  store float %lambda_min, float* %lambda_min.addr, align 4
  store float %lambda_max, float* %lambda_max.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4
  %1 = load i32*, i32** %raySign.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 0
  %2 = load i32, i32* %arrayidx, align 4
  %arrayidx1 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %arrayidx1)
  %3 = load float, float* %call, align 4
  %4 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4
  %sub = fsub float %3, %5
  %6 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call3, align 4
  %mul = fmul float %sub, %7
  %8 = load float*, float** %tmin.addr, align 4
  store float %mul, float* %8, align 4
  %9 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4
  %10 = load i32*, i32** %raySign.addr, align 4
  %arrayidx4 = getelementptr inbounds i32, i32* %10, i32 0
  %11 = load i32, i32* %arrayidx4, align 4
  %sub5 = sub i32 1, %11
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 %sub5
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %arrayidx6)
  %12 = load float, float* %call7, align 4
  %13 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %13)
  %14 = load float, float* %call8, align 4
  %sub9 = fsub float %12, %14
  %15 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %15)
  %16 = load float, float* %call10, align 4
  %mul11 = fmul float %sub9, %16
  store float %mul11, float* %tmax, align 4
  %17 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4
  %18 = load i32*, i32** %raySign.addr, align 4
  %arrayidx12 = getelementptr inbounds i32, i32* %18, i32 1
  %19 = load i32, i32* %arrayidx12, align 4
  %arrayidx13 = getelementptr inbounds %class.btVector3, %class.btVector3* %17, i32 %19
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %arrayidx13)
  %20 = load float, float* %call14, align 4
  %21 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %21)
  %22 = load float, float* %call15, align 4
  %sub16 = fsub float %20, %22
  %23 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %23)
  %24 = load float, float* %call17, align 4
  %mul18 = fmul float %sub16, %24
  store float %mul18, float* %tymin, align 4
  %25 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4
  %26 = load i32*, i32** %raySign.addr, align 4
  %arrayidx19 = getelementptr inbounds i32, i32* %26, i32 1
  %27 = load i32, i32* %arrayidx19, align 4
  %sub20 = sub i32 1, %27
  %arrayidx21 = getelementptr inbounds %class.btVector3, %class.btVector3* %25, i32 %sub20
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %arrayidx21)
  %28 = load float, float* %call22, align 4
  %29 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %29)
  %30 = load float, float* %call23, align 4
  %sub24 = fsub float %28, %30
  %31 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %31)
  %32 = load float, float* %call25, align 4
  %mul26 = fmul float %sub24, %32
  store float %mul26, float* %tymax, align 4
  %33 = load float*, float** %tmin.addr, align 4
  %34 = load float, float* %33, align 4
  %35 = load float, float* %tymax, align 4
  %cmp = fcmp ogt float %34, %35
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %36 = load float, float* %tymin, align 4
  %37 = load float, float* %tmax, align 4
  %cmp27 = fcmp ogt float %36, %37
  br i1 %cmp27, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %38 = load float, float* %tymin, align 4
  %39 = load float*, float** %tmin.addr, align 4
  %40 = load float, float* %39, align 4
  %cmp28 = fcmp ogt float %38, %40
  br i1 %cmp28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.end
  %41 = load float, float* %tymin, align 4
  %42 = load float*, float** %tmin.addr, align 4
  store float %41, float* %42, align 4
  br label %if.end30

if.end30:                                         ; preds = %if.then29, %if.end
  %43 = load float, float* %tymax, align 4
  %44 = load float, float* %tmax, align 4
  %cmp31 = fcmp olt float %43, %44
  br i1 %cmp31, label %if.then32, label %if.end33

if.then32:                                        ; preds = %if.end30
  %45 = load float, float* %tymax, align 4
  store float %45, float* %tmax, align 4
  br label %if.end33

if.end33:                                         ; preds = %if.then32, %if.end30
  %46 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4
  %47 = load i32*, i32** %raySign.addr, align 4
  %arrayidx34 = getelementptr inbounds i32, i32* %47, i32 2
  %48 = load i32, i32* %arrayidx34, align 4
  %arrayidx35 = getelementptr inbounds %class.btVector3, %class.btVector3* %46, i32 %48
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %arrayidx35)
  %49 = load float, float* %call36, align 4
  %50 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %50)
  %51 = load float, float* %call37, align 4
  %sub38 = fsub float %49, %51
  %52 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4
  %call39 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %52)
  %53 = load float, float* %call39, align 4
  %mul40 = fmul float %sub38, %53
  store float %mul40, float* %tzmin, align 4
  %54 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4
  %55 = load i32*, i32** %raySign.addr, align 4
  %arrayidx41 = getelementptr inbounds i32, i32* %55, i32 2
  %56 = load i32, i32* %arrayidx41, align 4
  %sub42 = sub i32 1, %56
  %arrayidx43 = getelementptr inbounds %class.btVector3, %class.btVector3* %54, i32 %sub42
  %call44 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %arrayidx43)
  %57 = load float, float* %call44, align 4
  %58 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %58)
  %59 = load float, float* %call45, align 4
  %sub46 = fsub float %57, %59
  %60 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %60)
  %61 = load float, float* %call47, align 4
  %mul48 = fmul float %sub46, %61
  store float %mul48, float* %tzmax, align 4
  %62 = load float*, float** %tmin.addr, align 4
  %63 = load float, float* %62, align 4
  %64 = load float, float* %tzmax, align 4
  %cmp49 = fcmp ogt float %63, %64
  br i1 %cmp49, label %if.then52, label %lor.lhs.false50

lor.lhs.false50:                                  ; preds = %if.end33
  %65 = load float, float* %tzmin, align 4
  %66 = load float, float* %tmax, align 4
  %cmp51 = fcmp ogt float %65, %66
  br i1 %cmp51, label %if.then52, label %if.end53

if.then52:                                        ; preds = %lor.lhs.false50, %if.end33
  store i1 false, i1* %retval, align 1
  br label %return

if.end53:                                         ; preds = %lor.lhs.false50
  %67 = load float, float* %tzmin, align 4
  %68 = load float*, float** %tmin.addr, align 4
  %69 = load float, float* %68, align 4
  %cmp54 = fcmp ogt float %67, %69
  br i1 %cmp54, label %if.then55, label %if.end56

if.then55:                                        ; preds = %if.end53
  %70 = load float, float* %tzmin, align 4
  %71 = load float*, float** %tmin.addr, align 4
  store float %70, float* %71, align 4
  br label %if.end56

if.end56:                                         ; preds = %if.then55, %if.end53
  %72 = load float, float* %tzmax, align 4
  %73 = load float, float* %tmax, align 4
  %cmp57 = fcmp olt float %72, %73
  br i1 %cmp57, label %if.then58, label %if.end59

if.then58:                                        ; preds = %if.end56
  %74 = load float, float* %tzmax, align 4
  store float %74, float* %tmax, align 4
  br label %if.end59

if.end59:                                         ; preds = %if.then58, %if.end56
  %75 = load float*, float** %tmin.addr, align 4
  %76 = load float, float* %75, align 4
  %77 = load float, float* %lambda_max.addr, align 4
  %cmp60 = fcmp olt float %76, %77
  br i1 %cmp60, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end59
  %78 = load float, float* %tmax, align 4
  %79 = load float, float* %lambda_min.addr, align 4
  %cmp61 = fcmp ogt float %78, %79
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end59
  %80 = phi i1 [ false, %if.end59 ], [ %cmp61, %land.rhs ]
  store i1 %80, i1* %retval, align 1
  br label %return

return:                                           ; preds = %land.end, %if.then52, %if.then
  %81 = load i1, i1* %retval, align 1
  ret i1 %81
}

; Function Attrs: noinline optnone
define hidden void @_ZNK14btQuantizedBvh36walkStacklessQuantizedTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %raySource, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTarget, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 %startNodeIndex, i32 %endNodeIndex) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %raySource.addr = alloca %class.btVector3*, align 4
  %rayTarget.addr = alloca %class.btVector3*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %startNodeIndex.addr = alloca i32, align 4
  %endNodeIndex.addr = alloca i32, align 4
  %curIndex = alloca i32, align 4
  %walkIterations = alloca i32, align 4
  %subTreeSize = alloca i32, align 4
  %rootNode = alloca %struct.btQuantizedBvhNode*, align 4
  %escapeIndex = alloca i32, align 4
  %isLeafNode = alloca i8, align 1
  %boxBoxOverlap = alloca i32, align 4
  %rayBoxOverlap = alloca i32, align 4
  %lambda_max = alloca float, align 4
  %rayDirection = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %sign = alloca [3 x i32], align 4
  %rayAabbMin = alloca %class.btVector3, align 4
  %rayAabbMax = alloca %class.btVector3, align 4
  %quantizedQueryAabbMin = alloca [3 x i16], align 2
  %quantizedQueryAabbMax = alloca [3 x i16], align 2
  %param = alloca float, align 4
  %bounds = alloca [2 x %class.btVector3], align 16
  %ref.tmp59 = alloca %class.btVector3, align 4
  %ref.tmp63 = alloca %class.btVector3, align 4
  %normal = alloca %class.btVector3, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  store %class.btVector3* %raySource, %class.btVector3** %raySource.addr, align 4
  store %class.btVector3* %rayTarget, %class.btVector3** %rayTarget.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store i32 %startNodeIndex, i32* %startNodeIndex.addr, align 4
  store i32 %endNodeIndex, i32* %endNodeIndex.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = load i32, i32* %startNodeIndex.addr, align 4
  store i32 %0, i32* %curIndex, align 4
  store i32 0, i32* %walkIterations, align 4
  %1 = load i32, i32* %endNodeIndex.addr, align 4
  %2 = load i32, i32* %startNodeIndex.addr, align 4
  %sub = sub nsw i32 %1, %2
  store i32 %sub, i32* %subTreeSize, align 4
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %3 = load i32, i32* %startNodeIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %3)
  store %struct.btQuantizedBvhNode* %call, %struct.btQuantizedBvhNode** %rootNode, align 4
  store i32 0, i32* %boxBoxOverlap, align 4
  store i32 0, i32* %rayBoxOverlap, align 4
  store float 1.000000e+00, float* %lambda_max, align 4
  %4 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rayDirection, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %rayDirection)
  %6 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4
  %7 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %rayDirection, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  store float %call3, float* %lambda_max, align 4
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx = getelementptr inbounds float, float* %call4, i32 0
  %8 = load float, float* %arrayidx, align 4
  %cmp = fcmp oeq float %8, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  %9 = load float, float* %arrayidx6, align 4
  %div = fdiv float 1.000000e+00, %9
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ 0x43ABC16D60000000, %cond.true ], [ %div, %cond.false ]
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 0
  store float %cond, float* %arrayidx8, align 4
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 1
  %10 = load float, float* %arrayidx10, align 4
  %cmp11 = fcmp oeq float %10, 0.000000e+00
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %cond.end
  br label %cond.end17

cond.false13:                                     ; preds = %cond.end
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 1
  %11 = load float, float* %arrayidx15, align 4
  %div16 = fdiv float 1.000000e+00, %11
  br label %cond.end17

cond.end17:                                       ; preds = %cond.false13, %cond.true12
  %cond18 = phi float [ 0x43ABC16D60000000, %cond.true12 ], [ %div16, %cond.false13 ]
  %call19 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  store float %cond18, float* %arrayidx20, align 4
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 2
  %12 = load float, float* %arrayidx22, align 4
  %cmp23 = fcmp oeq float %12, 0.000000e+00
  br i1 %cmp23, label %cond.true24, label %cond.false25

cond.true24:                                      ; preds = %cond.end17
  br label %cond.end29

cond.false25:                                     ; preds = %cond.end17
  %call26 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 2
  %13 = load float, float* %arrayidx27, align 4
  %div28 = fdiv float 1.000000e+00, %13
  br label %cond.end29

cond.end29:                                       ; preds = %cond.false25, %cond.true24
  %cond30 = phi float [ 0x43ABC16D60000000, %cond.true24 ], [ %div28, %cond.false25 ]
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 2
  store float %cond30, float* %arrayidx32, align 4
  %arrayinit.begin = getelementptr inbounds [3 x i32], [3 x i32]* %sign, i32 0, i32 0
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 0
  %14 = load float, float* %arrayidx34, align 4
  %conv = fpext float %14 to double
  %cmp35 = fcmp olt double %conv, 0.000000e+00
  %conv36 = zext i1 %cmp35 to i32
  store i32 %conv36, i32* %arrayinit.begin, align 4
  %arrayinit.element = getelementptr inbounds i32, i32* %arrayinit.begin, i32 1
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %15 = load float, float* %arrayidx38, align 4
  %conv39 = fpext float %15 to double
  %cmp40 = fcmp olt double %conv39, 0.000000e+00
  %conv41 = zext i1 %cmp40 to i32
  store i32 %conv41, i32* %arrayinit.element, align 4
  %arrayinit.element42 = getelementptr inbounds i32, i32* %arrayinit.element, i32 1
  %call43 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDirection)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 2
  %16 = load float, float* %arrayidx44, align 4
  %conv45 = fpext float %16 to double
  %cmp46 = fcmp olt double %conv45, 0.000000e+00
  %conv47 = zext i1 %cmp46 to i32
  store i32 %conv47, i32* %arrayinit.element42, align 4
  %17 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  %18 = bitcast %class.btVector3* %rayAabbMin to i8*
  %19 = bitcast %class.btVector3* %17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false)
  %20 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  %21 = bitcast %class.btVector3* %rayAabbMax to i8*
  %22 = bitcast %class.btVector3* %20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  %23 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %rayAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %23)
  %24 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %rayAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %24)
  %25 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %call48 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %rayAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %25)
  %26 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %call49 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %rayAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %26)
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh17quantizeWithClampEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %rayAabbMin, i32 0)
  %arraydecay50 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh17quantizeWithClampEPtRK9btVector3i(%class.btQuantizedBvh* %this1, i16* %arraydecay50, %class.btVector3* nonnull align 4 dereferenceable(16) %rayAabbMax, i32 1)
  br label %while.cond

while.cond:                                       ; preds = %if.end87, %cond.end29
  %27 = load i32, i32* %curIndex, align 4
  %28 = load i32, i32* %endNodeIndex.addr, align 4
  %cmp51 = icmp slt i32 %27, %28
  br i1 %cmp51, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %29 = load i32, i32* %walkIterations, align 4
  %inc = add nsw i32 %29, 1
  store i32 %inc, i32* %walkIterations, align 4
  store float 1.000000e+00, float* %param, align 4
  store i32 0, i32* %rayBoxOverlap, align 4
  %arraydecay52 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMin, i32 0, i32 0
  %arraydecay53 = getelementptr inbounds [3 x i16], [3 x i16]* %quantizedQueryAabbMax, i32 0, i32 0
  %30 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %30, i32 0, i32 0
  %arraydecay54 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %31 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %31, i32 0, i32 1
  %arraydecay55 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %call56 = call i32 @_Z37testQuantizedAabbAgainstQuantizedAabbPKtS0_S0_S0_(i16* %arraydecay52, i16* %arraydecay53, i16* %arraydecay54, i16* %arraydecay55)
  store i32 %call56, i32* %boxBoxOverlap, align 4
  %32 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %call57 = call zeroext i1 @_ZNK18btQuantizedBvhNode10isLeafNodeEv(%struct.btQuantizedBvhNode* %32)
  %frombool = zext i1 %call57 to i8
  store i8 %frombool, i8* %isLeafNode, align 1
  %33 = load i32, i32* %boxBoxOverlap, align 4
  %tobool = icmp ne i32 %33, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %array.begin = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 2
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %if.then
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %if.then ], [ %arrayctor.next, %arrayctor.loop ]
  %call58 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %34 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %m_quantizedAabbMin60 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %34, i32 0, i32 0
  %arraydecay61 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin60, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh10unQuantizeEPKt(%class.btVector3* sret align 4 %ref.tmp59, %class.btQuantizedBvh* %this1, i16* %arraydecay61)
  %arrayidx62 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %35 = bitcast %class.btVector3* %arrayidx62 to i8*
  %36 = bitcast %class.btVector3* %ref.tmp59 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %35, i8* align 4 %36, i32 16, i1 false)
  %37 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %m_quantizedAabbMax64 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %37, i32 0, i32 1
  %arraydecay65 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax64, i32 0, i32 0
  call void @_ZNK14btQuantizedBvh10unQuantizeEPKt(%class.btVector3* sret align 4 %ref.tmp63, %class.btQuantizedBvh* %this1, i16* %arraydecay65)
  %arrayidx66 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 1
  %38 = bitcast %class.btVector3* %arrayidx66 to i8*
  %39 = bitcast %class.btVector3* %ref.tmp63 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %38, i8* align 4 %39, i32 16, i1 false)
  %40 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %arrayidx67 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %call68 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %arrayidx67, %class.btVector3* nonnull align 4 dereferenceable(16) %40)
  %41 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %arrayidx69 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 1
  %call70 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %arrayidx69, %class.btVector3* nonnull align 4 dereferenceable(16) %41)
  %call71 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normal)
  %42 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  %arraydecay72 = getelementptr inbounds [3 x i32], [3 x i32]* %sign, i32 0, i32 0
  %arraydecay73 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %43 = load float, float* %lambda_max, align 4
  %call74 = call zeroext i1 @_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff(%class.btVector3* nonnull align 4 dereferenceable(16) %42, %class.btVector3* nonnull align 4 dereferenceable(16) %rayDirection, i32* %arraydecay72, %class.btVector3* %arraydecay73, float* nonnull align 4 dereferenceable(4) %param, float 0.000000e+00, float %43)
  %conv75 = zext i1 %call74 to i32
  store i32 %conv75, i32* %rayBoxOverlap, align 4
  br label %if.end

if.end:                                           ; preds = %arrayctor.cont, %while.body
  %44 = load i8, i8* %isLeafNode, align 1
  %tobool76 = trunc i8 %44 to i1
  br i1 %tobool76, label %land.lhs.true, label %if.end81

land.lhs.true:                                    ; preds = %if.end
  %45 = load i32, i32* %rayBoxOverlap, align 4
  %tobool77 = icmp ne i32 %45, 0
  br i1 %tobool77, label %if.then78, label %if.end81

if.then78:                                        ; preds = %land.lhs.true
  %46 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %47 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %call79 = call i32 @_ZNK18btQuantizedBvhNode9getPartIdEv(%struct.btQuantizedBvhNode* %47)
  %48 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %call80 = call i32 @_ZNK18btQuantizedBvhNode16getTriangleIndexEv(%struct.btQuantizedBvhNode* %48)
  %49 = bitcast %class.btNodeOverlapCallback* %46 to void (%class.btNodeOverlapCallback*, i32, i32)***
  %vtable = load void (%class.btNodeOverlapCallback*, i32, i32)**, void (%class.btNodeOverlapCallback*, i32, i32)*** %49, align 4
  %vfn = getelementptr inbounds void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vtable, i64 2
  %50 = load void (%class.btNodeOverlapCallback*, i32, i32)*, void (%class.btNodeOverlapCallback*, i32, i32)** %vfn, align 4
  call void %50(%class.btNodeOverlapCallback* %46, i32 %call79, i32 %call80)
  br label %if.end81

if.end81:                                         ; preds = %if.then78, %land.lhs.true, %if.end
  %51 = load i32, i32* %rayBoxOverlap, align 4
  %cmp82 = icmp ne i32 %51, 0
  br i1 %cmp82, label %if.then84, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end81
  %52 = load i8, i8* %isLeafNode, align 1
  %tobool83 = trunc i8 %52 to i1
  br i1 %tobool83, label %if.then84, label %if.else

if.then84:                                        ; preds = %lor.lhs.false, %if.end81
  %53 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %incdec.ptr = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %53, i32 1
  store %struct.btQuantizedBvhNode* %incdec.ptr, %struct.btQuantizedBvhNode** %rootNode, align 4
  %54 = load i32, i32* %curIndex, align 4
  %inc85 = add nsw i32 %54, 1
  store i32 %inc85, i32* %curIndex, align 4
  br label %if.end87

if.else:                                          ; preds = %lor.lhs.false
  %55 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %call86 = call i32 @_ZNK18btQuantizedBvhNode14getEscapeIndexEv(%struct.btQuantizedBvhNode* %55)
  store i32 %call86, i32* %escapeIndex, align 4
  %56 = load i32, i32* %escapeIndex, align 4
  %57 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %rootNode, align 4
  %add.ptr = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %57, i32 %56
  store %struct.btQuantizedBvhNode* %add.ptr, %struct.btQuantizedBvhNode** %rootNode, align 4
  %58 = load i32, i32* %escapeIndex, align 4
  %59 = load i32, i32* %curIndex, align 4
  %add = add nsw i32 %59, %58
  store i32 %add, i32* %curIndex, align 4
  br label %if.end87

if.end87:                                         ; preds = %if.else, %if.then84
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %60 = load i32, i32* @maxIterations, align 4
  %61 = load i32, i32* %walkIterations, align 4
  %cmp88 = icmp slt i32 %60, %61
  br i1 %cmp88, label %if.then89, label %if.end90

if.then89:                                        ; preds = %while.end
  %62 = load i32, i32* %walkIterations, align 4
  store i32 %62, i32* @maxIterations, align 4
  br label %if.end90

if.end90:                                         ; preds = %if.then89, %while.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %0, i32 %1
  ret %class.btBvhSubtreeInfo* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZNK14btQuantizedBvh25reportRayOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %raySource, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTarget) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %raySource.addr = alloca %class.btVector3*, align 4
  %rayTarget.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  store %class.btVector3* %raySource, %class.btVector3** %raySource.addr, align 4
  store %class.btVector3* %rayTarget, %class.btVector3** %rayTarget.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  call void @_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_(%class.btQuantizedBvh* %this1, %class.btNodeOverlapCallback* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK14btQuantizedBvh29reportBoxCastOverlappingNodexEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_(%class.btQuantizedBvh* %this, %class.btNodeOverlapCallback* %nodeCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %raySource, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTarget, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %nodeCallback.addr = alloca %class.btNodeOverlapCallback*, align 4
  %raySource.addr = alloca %class.btVector3*, align 4
  %rayTarget.addr = alloca %class.btVector3*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store %class.btNodeOverlapCallback* %nodeCallback, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  store %class.btVector3* %raySource, %class.btVector3** %raySource.addr, align 4
  store %class.btVector3* %rayTarget, %class.btVector3** %rayTarget.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_useQuantization, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %6 = load i32, i32* %m_curNodeIndex, align 4
  call void @_ZNK14btQuantizedBvh36walkStacklessQuantizedTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii(%class.btQuantizedBvh* %this1, %class.btNodeOverlapCallback* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5, i32 0, i32 %6)
  br label %if.end

if.else:                                          ; preds = %entry
  %7 = load %class.btNodeOverlapCallback*, %class.btNodeOverlapCallback** %nodeCallback.addr, align 4
  %8 = load %class.btVector3*, %class.btVector3** %raySource.addr, align 4
  %9 = load %class.btVector3*, %class.btVector3** %rayTarget.addr, align 4
  %10 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %m_curNodeIndex2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %12 = load i32, i32* %m_curNodeIndex2, align 4
  call void @_ZNK14btQuantizedBvh27walkStacklessTreeAgainstRayEP21btNodeOverlapCallbackRK9btVector3S4_S4_S4_ii(%class.btQuantizedBvh* %this1, %class.btNodeOverlapCallback* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11, i32 0, i32 %12)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %0, i32 %1
  ret %struct.btOptimizedBvhNode* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZN14btQuantizedBvh32getAlignmentSerializationPaddingEv() #1 {
entry:
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK14btQuantizedBvh28calculateSerializeBufferSizeEv(%class.btQuantizedBvh* %this) #1 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %baseSize = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %call = call i32 @_ZN14btQuantizedBvh32getAlignmentSerializationPaddingEv()
  %add = add i32 172, %call
  store i32 %add, i32* %baseSize, align 4
  %m_subtreeHeaderCount = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %0 = load i32, i32* %m_subtreeHeaderCount, align 4
  %mul = mul i32 32, %0
  %1 = load i32, i32* %baseSize, align 4
  %add2 = add i32 %1, %mul
  store i32 %add2, i32* %baseSize, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %2 = load i8, i8* %m_useQuantization, align 4
  %tobool = trunc i8 %2 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %baseSize, align 4
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %4 = load i32, i32* %m_curNodeIndex, align 4
  %mul3 = mul i32 %4, 16
  %add4 = add i32 %3, %mul3
  store i32 %add4, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %5 = load i32, i32* %baseSize, align 4
  %m_curNodeIndex5 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %6 = load i32, i32* %m_curNodeIndex5, align 4
  %mul6 = mul i32 %6, 64
  %add7 = add i32 %5, %mul6
  store i32 %add7, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZNK14btQuantizedBvh9serializeEPvjb(%class.btQuantizedBvh* %this, i8* %o_alignedDataBuffer, i32 %0, i1 zeroext %i_swapEndian) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %o_alignedDataBuffer.addr = alloca i8*, align 4
  %.addr = alloca i32, align 4
  %i_swapEndian.addr = alloca i8, align 1
  %targetBvh = alloca %class.btQuantizedBvh*, align 4
  %nodeData = alloca i8*, align 4
  %sizeToAdd = alloca i32, align 4
  %nodeCount = alloca i32, align 4
  %nodeIndex = alloca i32, align 4
  %nodeIndex94 = alloca i32, align 4
  %nodeIndex161 = alloca i32, align 4
  %nodeIndex197 = alloca i32, align 4
  %i = alloca i32, align 4
  %i319 = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i8* %o_alignedDataBuffer, i8** %o_alignedDataBuffer.addr, align 4
  store i32 %0, i32* %.addr, align 4
  %frombool = zext i1 %i_swapEndian to i8
  store i8 %frombool, i8* %i_swapEndian.addr, align 1
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %m_subtreeHeaderCount = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  store i32 %call, i32* %m_subtreeHeaderCount, align 4
  %1 = load i8*, i8** %o_alignedDataBuffer.addr, align 4
  %2 = bitcast i8* %1 to %class.btQuantizedBvh*
  store %class.btQuantizedBvh* %2, %class.btQuantizedBvh** %targetBvh, align 4
  %3 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %4 = bitcast %class.btQuantizedBvh* %3 to i8*
  %call2 = call i8* @_ZN14btQuantizedBvhnwEmPv(i32 172, i8* %4)
  %5 = bitcast i8* %call2 to %class.btQuantizedBvh*
  %call3 = call %class.btQuantizedBvh* @_ZN14btQuantizedBvhC1Ev(%class.btQuantizedBvh* %5)
  %6 = load i8, i8* %i_swapEndian.addr, align 1
  %tobool = trunc i8 %6 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %7 = load i32, i32* %m_curNodeIndex, align 4
  %call4 = call i32 @_Z12btSwapEndiani(i32 %7)
  %8 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_curNodeIndex5 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %8, i32 0, i32 5
  store i32 %call4, i32* %m_curNodeIndex5, align 4
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %9 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_bvhAabbMin6 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %9, i32 0, i32 1
  call void @_Z19btSwapVector3EndianRK9btVector3RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin6)
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %10 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_bvhAabbMax7 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %10, i32 0, i32 2
  call void @_Z19btSwapVector3EndianRK9btVector3RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax7)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %11 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_bvhQuantization8 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %11, i32 0, i32 3
  call void @_Z19btSwapVector3EndianRK9btVector3RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhQuantization, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhQuantization8)
  %m_traversalMode = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 12
  %12 = load i32, i32* %m_traversalMode, align 4
  %call9 = call i32 @_Z12btSwapEndiani(i32 %12)
  %13 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_traversalMode10 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %13, i32 0, i32 12
  store i32 %call9, i32* %m_traversalMode10, align 4
  %m_subtreeHeaderCount11 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %14 = load i32, i32* %m_subtreeHeaderCount11, align 4
  %call12 = call i32 @_Z12btSwapEndiani(i32 %14)
  %15 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_subtreeHeaderCount13 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %15, i32 0, i32 14
  store i32 %call12, i32* %m_subtreeHeaderCount13, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %m_curNodeIndex14 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %16 = load i32, i32* %m_curNodeIndex14, align 4
  %17 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_curNodeIndex15 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %17, i32 0, i32 5
  store i32 %16, i32* %m_curNodeIndex15, align 4
  %m_bvhAabbMin16 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %18 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_bvhAabbMin17 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %18, i32 0, i32 1
  %19 = bitcast %class.btVector3* %m_bvhAabbMin17 to i8*
  %20 = bitcast %class.btVector3* %m_bvhAabbMin16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  %m_bvhAabbMax18 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %21 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_bvhAabbMax19 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %21, i32 0, i32 2
  %22 = bitcast %class.btVector3* %m_bvhAabbMax19 to i8*
  %23 = bitcast %class.btVector3* %m_bvhAabbMax18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false)
  %m_bvhQuantization20 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %24 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_bvhQuantization21 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %24, i32 0, i32 3
  %25 = bitcast %class.btVector3* %m_bvhQuantization21 to i8*
  %26 = bitcast %class.btVector3* %m_bvhQuantization20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false)
  %m_traversalMode22 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 12
  %27 = load i32, i32* %m_traversalMode22, align 4
  %28 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_traversalMode23 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %28, i32 0, i32 12
  store i32 %27, i32* %m_traversalMode23, align 4
  %m_subtreeHeaderCount24 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %29 = load i32, i32* %m_subtreeHeaderCount24, align 4
  %30 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_subtreeHeaderCount25 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %30, i32 0, i32 14
  store i32 %29, i32* %m_subtreeHeaderCount25, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %31 = load i8, i8* %m_useQuantization, align 4
  %tobool26 = trunc i8 %31 to i1
  %32 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_useQuantization27 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %32, i32 0, i32 6
  %frombool28 = zext i1 %tobool26 to i8
  store i8 %frombool28, i8* %m_useQuantization27, align 4
  %33 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %34 = bitcast %class.btQuantizedBvh* %33 to i8*
  store i8* %34, i8** %nodeData, align 4
  %35 = load i8*, i8** %nodeData, align 4
  %add.ptr = getelementptr inbounds i8, i8* %35, i32 172
  store i8* %add.ptr, i8** %nodeData, align 4
  store i32 0, i32* %sizeToAdd, align 4
  %36 = load i32, i32* %sizeToAdd, align 4
  %37 = load i8*, i8** %nodeData, align 4
  %add.ptr29 = getelementptr inbounds i8, i8* %37, i32 %36
  store i8* %add.ptr29, i8** %nodeData, align 4
  %m_curNodeIndex30 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %38 = load i32, i32* %m_curNodeIndex30, align 4
  store i32 %38, i32* %nodeCount, align 4
  %m_useQuantization31 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %39 = load i8, i8* %m_useQuantization31, align 4
  %tobool32 = trunc i8 %39 to i1
  br i1 %tobool32, label %if.then33, label %if.else158

if.then33:                                        ; preds = %if.end
  %40 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %40, i32 0, i32 11
  %41 = load i8*, i8** %nodeData, align 4
  %42 = load i32, i32* %nodeCount, align 4
  %43 = load i32, i32* %nodeCount, align 4
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i8* %41, i32 %42, i32 %43)
  %44 = load i8, i8* %i_swapEndian.addr, align 1
  %tobool34 = trunc i8 %44 to i1
  br i1 %tobool34, label %if.then35, label %if.else93

if.then35:                                        ; preds = %if.then33
  store i32 0, i32* %nodeIndex, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then35
  %45 = load i32, i32* %nodeIndex, align 4
  %46 = load i32, i32* %nodeCount, align 4
  %cmp = icmp slt i32 %45, %46
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_quantizedContiguousNodes36 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %47 = load i32, i32* %nodeIndex, align 4
  %call37 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes36, i32 %47)
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call37, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %48 = load i16, i16* %arrayidx, align 4
  %call38 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %48)
  %49 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes39 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %49, i32 0, i32 11
  %50 = load i32, i32* %nodeIndex, align 4
  %call40 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes39, i32 %50)
  %m_quantizedAabbMin41 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call40, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin41, i32 0, i32 0
  store i16 %call38, i16* %arrayidx42, align 4
  %m_quantizedContiguousNodes43 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %51 = load i32, i32* %nodeIndex, align 4
  %call44 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes43, i32 %51)
  %m_quantizedAabbMin45 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call44, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin45, i32 0, i32 1
  %52 = load i16, i16* %arrayidx46, align 2
  %call47 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %52)
  %53 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes48 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %53, i32 0, i32 11
  %54 = load i32, i32* %nodeIndex, align 4
  %call49 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes48, i32 %54)
  %m_quantizedAabbMin50 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call49, i32 0, i32 0
  %arrayidx51 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin50, i32 0, i32 1
  store i16 %call47, i16* %arrayidx51, align 2
  %m_quantizedContiguousNodes52 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %55 = load i32, i32* %nodeIndex, align 4
  %call53 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes52, i32 %55)
  %m_quantizedAabbMin54 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call53, i32 0, i32 0
  %arrayidx55 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin54, i32 0, i32 2
  %56 = load i16, i16* %arrayidx55, align 4
  %call56 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %56)
  %57 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes57 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %57, i32 0, i32 11
  %58 = load i32, i32* %nodeIndex, align 4
  %call58 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes57, i32 %58)
  %m_quantizedAabbMin59 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call58, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin59, i32 0, i32 2
  store i16 %call56, i16* %arrayidx60, align 4
  %m_quantizedContiguousNodes61 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %59 = load i32, i32* %nodeIndex, align 4
  %call62 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes61, i32 %59)
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call62, i32 0, i32 1
  %arrayidx63 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %60 = load i16, i16* %arrayidx63, align 2
  %call64 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %60)
  %61 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes65 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %61, i32 0, i32 11
  %62 = load i32, i32* %nodeIndex, align 4
  %call66 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes65, i32 %62)
  %m_quantizedAabbMax67 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call66, i32 0, i32 1
  %arrayidx68 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax67, i32 0, i32 0
  store i16 %call64, i16* %arrayidx68, align 2
  %m_quantizedContiguousNodes69 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %63 = load i32, i32* %nodeIndex, align 4
  %call70 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes69, i32 %63)
  %m_quantizedAabbMax71 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call70, i32 0, i32 1
  %arrayidx72 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax71, i32 0, i32 1
  %64 = load i16, i16* %arrayidx72, align 2
  %call73 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %64)
  %65 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes74 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %65, i32 0, i32 11
  %66 = load i32, i32* %nodeIndex, align 4
  %call75 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes74, i32 %66)
  %m_quantizedAabbMax76 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call75, i32 0, i32 1
  %arrayidx77 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax76, i32 0, i32 1
  store i16 %call73, i16* %arrayidx77, align 2
  %m_quantizedContiguousNodes78 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %67 = load i32, i32* %nodeIndex, align 4
  %call79 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes78, i32 %67)
  %m_quantizedAabbMax80 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call79, i32 0, i32 1
  %arrayidx81 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax80, i32 0, i32 2
  %68 = load i16, i16* %arrayidx81, align 2
  %call82 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %68)
  %69 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes83 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %69, i32 0, i32 11
  %70 = load i32, i32* %nodeIndex, align 4
  %call84 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes83, i32 %70)
  %m_quantizedAabbMax85 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call84, i32 0, i32 1
  %arrayidx86 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax85, i32 0, i32 2
  store i16 %call82, i16* %arrayidx86, align 2
  %m_quantizedContiguousNodes87 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %71 = load i32, i32* %nodeIndex, align 4
  %call88 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes87, i32 %71)
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call88, i32 0, i32 2
  %72 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4
  %call89 = call i32 @_Z12btSwapEndiani(i32 %72)
  %73 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes90 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %73, i32 0, i32 11
  %74 = load i32, i32* %nodeIndex, align 4
  %call91 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes90, i32 %74)
  %m_escapeIndexOrTriangleIndex92 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call91, i32 0, i32 2
  store i32 %call89, i32* %m_escapeIndexOrTriangleIndex92, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %75 = load i32, i32* %nodeIndex, align 4
  %inc = add nsw i32 %75, 1
  store i32 %inc, i32* %nodeIndex, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end155

if.else93:                                        ; preds = %if.then33
  store i32 0, i32* %nodeIndex94, align 4
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc152, %if.else93
  %76 = load i32, i32* %nodeIndex94, align 4
  %77 = load i32, i32* %nodeCount, align 4
  %cmp96 = icmp slt i32 %76, %77
  br i1 %cmp96, label %for.body97, label %for.end154

for.body97:                                       ; preds = %for.cond95
  %m_quantizedContiguousNodes98 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %78 = load i32, i32* %nodeIndex94, align 4
  %call99 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes98, i32 %78)
  %m_quantizedAabbMin100 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call99, i32 0, i32 0
  %arrayidx101 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin100, i32 0, i32 0
  %79 = load i16, i16* %arrayidx101, align 4
  %80 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes102 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %80, i32 0, i32 11
  %81 = load i32, i32* %nodeIndex94, align 4
  %call103 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes102, i32 %81)
  %m_quantizedAabbMin104 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call103, i32 0, i32 0
  %arrayidx105 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin104, i32 0, i32 0
  store i16 %79, i16* %arrayidx105, align 4
  %m_quantizedContiguousNodes106 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %82 = load i32, i32* %nodeIndex94, align 4
  %call107 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes106, i32 %82)
  %m_quantizedAabbMin108 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call107, i32 0, i32 0
  %arrayidx109 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin108, i32 0, i32 1
  %83 = load i16, i16* %arrayidx109, align 2
  %84 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes110 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %84, i32 0, i32 11
  %85 = load i32, i32* %nodeIndex94, align 4
  %call111 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes110, i32 %85)
  %m_quantizedAabbMin112 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call111, i32 0, i32 0
  %arrayidx113 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin112, i32 0, i32 1
  store i16 %83, i16* %arrayidx113, align 2
  %m_quantizedContiguousNodes114 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %86 = load i32, i32* %nodeIndex94, align 4
  %call115 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes114, i32 %86)
  %m_quantizedAabbMin116 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call115, i32 0, i32 0
  %arrayidx117 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin116, i32 0, i32 2
  %87 = load i16, i16* %arrayidx117, align 4
  %88 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes118 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %88, i32 0, i32 11
  %89 = load i32, i32* %nodeIndex94, align 4
  %call119 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes118, i32 %89)
  %m_quantizedAabbMin120 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call119, i32 0, i32 0
  %arrayidx121 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin120, i32 0, i32 2
  store i16 %87, i16* %arrayidx121, align 4
  %m_quantizedContiguousNodes122 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %90 = load i32, i32* %nodeIndex94, align 4
  %call123 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes122, i32 %90)
  %m_quantizedAabbMax124 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call123, i32 0, i32 1
  %arrayidx125 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax124, i32 0, i32 0
  %91 = load i16, i16* %arrayidx125, align 2
  %92 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes126 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %92, i32 0, i32 11
  %93 = load i32, i32* %nodeIndex94, align 4
  %call127 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes126, i32 %93)
  %m_quantizedAabbMax128 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call127, i32 0, i32 1
  %arrayidx129 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax128, i32 0, i32 0
  store i16 %91, i16* %arrayidx129, align 2
  %m_quantizedContiguousNodes130 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %94 = load i32, i32* %nodeIndex94, align 4
  %call131 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes130, i32 %94)
  %m_quantizedAabbMax132 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call131, i32 0, i32 1
  %arrayidx133 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax132, i32 0, i32 1
  %95 = load i16, i16* %arrayidx133, align 2
  %96 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes134 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %96, i32 0, i32 11
  %97 = load i32, i32* %nodeIndex94, align 4
  %call135 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes134, i32 %97)
  %m_quantizedAabbMax136 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call135, i32 0, i32 1
  %arrayidx137 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax136, i32 0, i32 1
  store i16 %95, i16* %arrayidx137, align 2
  %m_quantizedContiguousNodes138 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %98 = load i32, i32* %nodeIndex94, align 4
  %call139 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes138, i32 %98)
  %m_quantizedAabbMax140 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call139, i32 0, i32 1
  %arrayidx141 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax140, i32 0, i32 2
  %99 = load i16, i16* %arrayidx141, align 2
  %100 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes142 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %100, i32 0, i32 11
  %101 = load i32, i32* %nodeIndex94, align 4
  %call143 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes142, i32 %101)
  %m_quantizedAabbMax144 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call143, i32 0, i32 1
  %arrayidx145 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax144, i32 0, i32 2
  store i16 %99, i16* %arrayidx145, align 2
  %m_quantizedContiguousNodes146 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %102 = load i32, i32* %nodeIndex94, align 4
  %call147 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes146, i32 %102)
  %m_escapeIndexOrTriangleIndex148 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call147, i32 0, i32 2
  %103 = load i32, i32* %m_escapeIndexOrTriangleIndex148, align 4
  %104 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes149 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %104, i32 0, i32 11
  %105 = load i32, i32* %nodeIndex94, align 4
  %call150 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes149, i32 %105)
  %m_escapeIndexOrTriangleIndex151 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call150, i32 0, i32 2
  store i32 %103, i32* %m_escapeIndexOrTriangleIndex151, align 4
  br label %for.inc152

for.inc152:                                       ; preds = %for.body97
  %106 = load i32, i32* %nodeIndex94, align 4
  %inc153 = add nsw i32 %106, 1
  store i32 %inc153, i32* %nodeIndex94, align 4
  br label %for.cond95

for.end154:                                       ; preds = %for.cond95
  br label %if.end155

if.end155:                                        ; preds = %for.end154, %for.end
  %107 = load i32, i32* %nodeCount, align 4
  %mul = mul i32 16, %107
  %108 = load i8*, i8** %nodeData, align 4
  %add.ptr156 = getelementptr inbounds i8, i8* %108, i32 %mul
  store i8* %add.ptr156, i8** %nodeData, align 4
  %109 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_quantizedContiguousNodes157 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %109, i32 0, i32 11
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes157, i8* null, i32 0, i32 0)
  br label %if.end238

if.else158:                                       ; preds = %if.end
  %110 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %110, i32 0, i32 9
  %111 = load i8*, i8** %nodeData, align 4
  %112 = load i32, i32* %nodeCount, align 4
  %113 = load i32, i32* %nodeCount, align 4
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray* %m_contiguousNodes, i8* %111, i32 %112, i32 %113)
  %114 = load i8, i8* %i_swapEndian.addr, align 1
  %tobool159 = trunc i8 %114 to i1
  br i1 %tobool159, label %if.then160, label %if.else196

if.then160:                                       ; preds = %if.else158
  store i32 0, i32* %nodeIndex161, align 4
  br label %for.cond162

for.cond162:                                      ; preds = %for.inc193, %if.then160
  %115 = load i32, i32* %nodeIndex161, align 4
  %116 = load i32, i32* %nodeCount, align 4
  %cmp163 = icmp slt i32 %115, %116
  br i1 %cmp163, label %for.body164, label %for.end195

for.body164:                                      ; preds = %for.cond162
  %m_contiguousNodes165 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %117 = load i32, i32* %nodeIndex161, align 4
  %call166 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes165, i32 %117)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call166, i32 0, i32 0
  %118 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_contiguousNodes167 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %118, i32 0, i32 9
  %119 = load i32, i32* %nodeIndex161, align 4
  %call168 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes167, i32 %119)
  %m_aabbMinOrg169 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call168, i32 0, i32 0
  call void @_Z19btSwapVector3EndianRK9btVector3RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMinOrg, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMinOrg169)
  %m_contiguousNodes170 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %120 = load i32, i32* %nodeIndex161, align 4
  %call171 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes170, i32 %120)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call171, i32 0, i32 1
  %121 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_contiguousNodes172 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %121, i32 0, i32 9
  %122 = load i32, i32* %nodeIndex161, align 4
  %call173 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes172, i32 %122)
  %m_aabbMaxOrg174 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call173, i32 0, i32 1
  call void @_Z19btSwapVector3EndianRK9btVector3RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMaxOrg, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMaxOrg174)
  %m_contiguousNodes175 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %123 = load i32, i32* %nodeIndex161, align 4
  %call176 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes175, i32 %123)
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call176, i32 0, i32 2
  %124 = load i32, i32* %m_escapeIndex, align 4
  %call177 = call i32 @_Z12btSwapEndiani(i32 %124)
  %125 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_contiguousNodes178 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %125, i32 0, i32 9
  %126 = load i32, i32* %nodeIndex161, align 4
  %call179 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes178, i32 %126)
  %m_escapeIndex180 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call179, i32 0, i32 2
  store i32 %call177, i32* %m_escapeIndex180, align 4
  %m_contiguousNodes181 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %127 = load i32, i32* %nodeIndex161, align 4
  %call182 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes181, i32 %127)
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call182, i32 0, i32 3
  %128 = load i32, i32* %m_subPart, align 4
  %call183 = call i32 @_Z12btSwapEndiani(i32 %128)
  %129 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_contiguousNodes184 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %129, i32 0, i32 9
  %130 = load i32, i32* %nodeIndex161, align 4
  %call185 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes184, i32 %130)
  %m_subPart186 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call185, i32 0, i32 3
  store i32 %call183, i32* %m_subPart186, align 4
  %m_contiguousNodes187 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %131 = load i32, i32* %nodeIndex161, align 4
  %call188 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes187, i32 %131)
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call188, i32 0, i32 4
  %132 = load i32, i32* %m_triangleIndex, align 4
  %call189 = call i32 @_Z12btSwapEndiani(i32 %132)
  %133 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_contiguousNodes190 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %133, i32 0, i32 9
  %134 = load i32, i32* %nodeIndex161, align 4
  %call191 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes190, i32 %134)
  %m_triangleIndex192 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call191, i32 0, i32 4
  store i32 %call189, i32* %m_triangleIndex192, align 4
  br label %for.inc193

for.inc193:                                       ; preds = %for.body164
  %135 = load i32, i32* %nodeIndex161, align 4
  %inc194 = add nsw i32 %135, 1
  store i32 %inc194, i32* %nodeIndex161, align 4
  br label %for.cond162

for.end195:                                       ; preds = %for.cond162
  br label %if.end234

if.else196:                                       ; preds = %if.else158
  store i32 0, i32* %nodeIndex197, align 4
  br label %for.cond198

for.cond198:                                      ; preds = %for.inc231, %if.else196
  %136 = load i32, i32* %nodeIndex197, align 4
  %137 = load i32, i32* %nodeCount, align 4
  %cmp199 = icmp slt i32 %136, %137
  br i1 %cmp199, label %for.body200, label %for.end233

for.body200:                                      ; preds = %for.cond198
  %m_contiguousNodes201 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %138 = load i32, i32* %nodeIndex197, align 4
  %call202 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes201, i32 %138)
  %m_aabbMinOrg203 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call202, i32 0, i32 0
  %139 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_contiguousNodes204 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %139, i32 0, i32 9
  %140 = load i32, i32* %nodeIndex197, align 4
  %call205 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes204, i32 %140)
  %m_aabbMinOrg206 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call205, i32 0, i32 0
  %141 = bitcast %class.btVector3* %m_aabbMinOrg206 to i8*
  %142 = bitcast %class.btVector3* %m_aabbMinOrg203 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %141, i8* align 4 %142, i32 16, i1 false)
  %m_contiguousNodes207 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %143 = load i32, i32* %nodeIndex197, align 4
  %call208 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes207, i32 %143)
  %m_aabbMaxOrg209 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call208, i32 0, i32 1
  %144 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_contiguousNodes210 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %144, i32 0, i32 9
  %145 = load i32, i32* %nodeIndex197, align 4
  %call211 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes210, i32 %145)
  %m_aabbMaxOrg212 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call211, i32 0, i32 1
  %146 = bitcast %class.btVector3* %m_aabbMaxOrg212 to i8*
  %147 = bitcast %class.btVector3* %m_aabbMaxOrg209 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %146, i8* align 4 %147, i32 16, i1 false)
  %m_contiguousNodes213 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %148 = load i32, i32* %nodeIndex197, align 4
  %call214 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes213, i32 %148)
  %m_escapeIndex215 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call214, i32 0, i32 2
  %149 = load i32, i32* %m_escapeIndex215, align 4
  %150 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_contiguousNodes216 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %150, i32 0, i32 9
  %151 = load i32, i32* %nodeIndex197, align 4
  %call217 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes216, i32 %151)
  %m_escapeIndex218 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call217, i32 0, i32 2
  store i32 %149, i32* %m_escapeIndex218, align 4
  %m_contiguousNodes219 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %152 = load i32, i32* %nodeIndex197, align 4
  %call220 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes219, i32 %152)
  %m_subPart221 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call220, i32 0, i32 3
  %153 = load i32, i32* %m_subPart221, align 4
  %154 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_contiguousNodes222 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %154, i32 0, i32 9
  %155 = load i32, i32* %nodeIndex197, align 4
  %call223 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes222, i32 %155)
  %m_subPart224 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call223, i32 0, i32 3
  store i32 %153, i32* %m_subPart224, align 4
  %m_contiguousNodes225 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %156 = load i32, i32* %nodeIndex197, align 4
  %call226 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes225, i32 %156)
  %m_triangleIndex227 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call226, i32 0, i32 4
  %157 = load i32, i32* %m_triangleIndex227, align 4
  %158 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_contiguousNodes228 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %158, i32 0, i32 9
  %159 = load i32, i32* %nodeIndex197, align 4
  %call229 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes228, i32 %159)
  %m_triangleIndex230 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call229, i32 0, i32 4
  store i32 %157, i32* %m_triangleIndex230, align 4
  br label %for.inc231

for.inc231:                                       ; preds = %for.body200
  %160 = load i32, i32* %nodeIndex197, align 4
  %inc232 = add nsw i32 %160, 1
  store i32 %inc232, i32* %nodeIndex197, align 4
  br label %for.cond198

for.end233:                                       ; preds = %for.cond198
  br label %if.end234

if.end234:                                        ; preds = %for.end233, %for.end195
  %161 = load i32, i32* %nodeCount, align 4
  %mul235 = mul i32 64, %161
  %162 = load i8*, i8** %nodeData, align 4
  %add.ptr236 = getelementptr inbounds i8, i8* %162, i32 %mul235
  store i8* %add.ptr236, i8** %nodeData, align 4
  %163 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_contiguousNodes237 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %163, i32 0, i32 9
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray* %m_contiguousNodes237, i8* null, i32 0, i32 0)
  br label %if.end238

if.end238:                                        ; preds = %if.end234, %if.end155
  store i32 0, i32* %sizeToAdd, align 4
  %164 = load i32, i32* %sizeToAdd, align 4
  %165 = load i8*, i8** %nodeData, align 4
  %add.ptr239 = getelementptr inbounds i8, i8* %165, i32 %164
  store i8* %add.ptr239, i8** %nodeData, align 4
  %166 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders240 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %166, i32 0, i32 13
  %167 = load i8*, i8** %nodeData, align 4
  %m_subtreeHeaderCount241 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %168 = load i32, i32* %m_subtreeHeaderCount241, align 4
  %m_subtreeHeaderCount242 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %169 = load i32, i32* %m_subtreeHeaderCount242, align 4
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE20initializeFromBufferEPvii(%class.btAlignedObjectArray.4* %m_SubtreeHeaders240, i8* %167, i32 %168, i32 %169)
  %170 = load i8, i8* %i_swapEndian.addr, align 1
  %tobool243 = trunc i8 %170 to i1
  br i1 %tobool243, label %if.then244, label %if.else318

if.then244:                                       ; preds = %if.end238
  store i32 0, i32* %i, align 4
  br label %for.cond245

for.cond245:                                      ; preds = %for.inc315, %if.then244
  %171 = load i32, i32* %i, align 4
  %m_subtreeHeaderCount246 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %172 = load i32, i32* %m_subtreeHeaderCount246, align 4
  %cmp247 = icmp slt i32 %171, %172
  br i1 %cmp247, label %for.body248, label %for.end317

for.body248:                                      ; preds = %for.cond245
  %m_SubtreeHeaders249 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %173 = load i32, i32* %i, align 4
  %call250 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders249, i32 %173)
  %m_quantizedAabbMin251 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call250, i32 0, i32 0
  %arrayidx252 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin251, i32 0, i32 0
  %174 = load i16, i16* %arrayidx252, align 4
  %call253 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %174)
  %175 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders254 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %175, i32 0, i32 13
  %176 = load i32, i32* %i, align 4
  %call255 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders254, i32 %176)
  %m_quantizedAabbMin256 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call255, i32 0, i32 0
  %arrayidx257 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin256, i32 0, i32 0
  store i16 %call253, i16* %arrayidx257, align 4
  %m_SubtreeHeaders258 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %177 = load i32, i32* %i, align 4
  %call259 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders258, i32 %177)
  %m_quantizedAabbMin260 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call259, i32 0, i32 0
  %arrayidx261 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin260, i32 0, i32 1
  %178 = load i16, i16* %arrayidx261, align 2
  %call262 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %178)
  %179 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders263 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %179, i32 0, i32 13
  %180 = load i32, i32* %i, align 4
  %call264 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders263, i32 %180)
  %m_quantizedAabbMin265 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call264, i32 0, i32 0
  %arrayidx266 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin265, i32 0, i32 1
  store i16 %call262, i16* %arrayidx266, align 2
  %m_SubtreeHeaders267 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %181 = load i32, i32* %i, align 4
  %call268 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders267, i32 %181)
  %m_quantizedAabbMin269 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call268, i32 0, i32 0
  %arrayidx270 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin269, i32 0, i32 2
  %182 = load i16, i16* %arrayidx270, align 4
  %call271 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %182)
  %183 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders272 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %183, i32 0, i32 13
  %184 = load i32, i32* %i, align 4
  %call273 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders272, i32 %184)
  %m_quantizedAabbMin274 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call273, i32 0, i32 0
  %arrayidx275 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin274, i32 0, i32 2
  store i16 %call271, i16* %arrayidx275, align 4
  %m_SubtreeHeaders276 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %185 = load i32, i32* %i, align 4
  %call277 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders276, i32 %185)
  %m_quantizedAabbMax278 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call277, i32 0, i32 1
  %arrayidx279 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax278, i32 0, i32 0
  %186 = load i16, i16* %arrayidx279, align 2
  %call280 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %186)
  %187 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders281 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %187, i32 0, i32 13
  %188 = load i32, i32* %i, align 4
  %call282 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders281, i32 %188)
  %m_quantizedAabbMax283 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call282, i32 0, i32 1
  %arrayidx284 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax283, i32 0, i32 0
  store i16 %call280, i16* %arrayidx284, align 2
  %m_SubtreeHeaders285 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %189 = load i32, i32* %i, align 4
  %call286 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders285, i32 %189)
  %m_quantizedAabbMax287 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call286, i32 0, i32 1
  %arrayidx288 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax287, i32 0, i32 1
  %190 = load i16, i16* %arrayidx288, align 2
  %call289 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %190)
  %191 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders290 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %191, i32 0, i32 13
  %192 = load i32, i32* %i, align 4
  %call291 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders290, i32 %192)
  %m_quantizedAabbMax292 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call291, i32 0, i32 1
  %arrayidx293 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax292, i32 0, i32 1
  store i16 %call289, i16* %arrayidx293, align 2
  %m_SubtreeHeaders294 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %193 = load i32, i32* %i, align 4
  %call295 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders294, i32 %193)
  %m_quantizedAabbMax296 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call295, i32 0, i32 1
  %arrayidx297 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax296, i32 0, i32 2
  %194 = load i16, i16* %arrayidx297, align 2
  %call298 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %194)
  %195 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders299 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %195, i32 0, i32 13
  %196 = load i32, i32* %i, align 4
  %call300 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders299, i32 %196)
  %m_quantizedAabbMax301 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call300, i32 0, i32 1
  %arrayidx302 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax301, i32 0, i32 2
  store i16 %call298, i16* %arrayidx302, align 2
  %m_SubtreeHeaders303 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %197 = load i32, i32* %i, align 4
  %call304 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders303, i32 %197)
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call304, i32 0, i32 2
  %198 = load i32, i32* %m_rootNodeIndex, align 4
  %call305 = call i32 @_Z12btSwapEndiani(i32 %198)
  %199 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders306 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %199, i32 0, i32 13
  %200 = load i32, i32* %i, align 4
  %call307 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders306, i32 %200)
  %m_rootNodeIndex308 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call307, i32 0, i32 2
  store i32 %call305, i32* %m_rootNodeIndex308, align 4
  %m_SubtreeHeaders309 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %201 = load i32, i32* %i, align 4
  %call310 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders309, i32 %201)
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call310, i32 0, i32 3
  %202 = load i32, i32* %m_subtreeSize, align 4
  %call311 = call i32 @_Z12btSwapEndiani(i32 %202)
  %203 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders312 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %203, i32 0, i32 13
  %204 = load i32, i32* %i, align 4
  %call313 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders312, i32 %204)
  %m_subtreeSize314 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call313, i32 0, i32 3
  store i32 %call311, i32* %m_subtreeSize314, align 4
  br label %for.inc315

for.inc315:                                       ; preds = %for.body248
  %205 = load i32, i32* %i, align 4
  %inc316 = add nsw i32 %205, 1
  store i32 %inc316, i32* %i, align 4
  br label %for.cond245

for.end317:                                       ; preds = %for.cond245
  br label %if.end398

if.else318:                                       ; preds = %if.end238
  store i32 0, i32* %i319, align 4
  br label %for.cond320

for.cond320:                                      ; preds = %for.inc395, %if.else318
  %206 = load i32, i32* %i319, align 4
  %m_subtreeHeaderCount321 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %207 = load i32, i32* %m_subtreeHeaderCount321, align 4
  %cmp322 = icmp slt i32 %206, %207
  br i1 %cmp322, label %for.body323, label %for.end397

for.body323:                                      ; preds = %for.cond320
  %m_SubtreeHeaders324 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %208 = load i32, i32* %i319, align 4
  %call325 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders324, i32 %208)
  %m_quantizedAabbMin326 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call325, i32 0, i32 0
  %arrayidx327 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin326, i32 0, i32 0
  %209 = load i16, i16* %arrayidx327, align 4
  %210 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders328 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %210, i32 0, i32 13
  %211 = load i32, i32* %i319, align 4
  %call329 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders328, i32 %211)
  %m_quantizedAabbMin330 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call329, i32 0, i32 0
  %arrayidx331 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin330, i32 0, i32 0
  store i16 %209, i16* %arrayidx331, align 4
  %m_SubtreeHeaders332 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %212 = load i32, i32* %i319, align 4
  %call333 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders332, i32 %212)
  %m_quantizedAabbMin334 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call333, i32 0, i32 0
  %arrayidx335 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin334, i32 0, i32 1
  %213 = load i16, i16* %arrayidx335, align 2
  %214 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders336 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %214, i32 0, i32 13
  %215 = load i32, i32* %i319, align 4
  %call337 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders336, i32 %215)
  %m_quantizedAabbMin338 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call337, i32 0, i32 0
  %arrayidx339 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin338, i32 0, i32 1
  store i16 %213, i16* %arrayidx339, align 2
  %m_SubtreeHeaders340 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %216 = load i32, i32* %i319, align 4
  %call341 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders340, i32 %216)
  %m_quantizedAabbMin342 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call341, i32 0, i32 0
  %arrayidx343 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin342, i32 0, i32 2
  %217 = load i16, i16* %arrayidx343, align 4
  %218 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders344 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %218, i32 0, i32 13
  %219 = load i32, i32* %i319, align 4
  %call345 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders344, i32 %219)
  %m_quantizedAabbMin346 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call345, i32 0, i32 0
  %arrayidx347 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin346, i32 0, i32 2
  store i16 %217, i16* %arrayidx347, align 4
  %m_SubtreeHeaders348 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %220 = load i32, i32* %i319, align 4
  %call349 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders348, i32 %220)
  %m_quantizedAabbMax350 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call349, i32 0, i32 1
  %arrayidx351 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax350, i32 0, i32 0
  %221 = load i16, i16* %arrayidx351, align 2
  %222 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders352 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %222, i32 0, i32 13
  %223 = load i32, i32* %i319, align 4
  %call353 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders352, i32 %223)
  %m_quantizedAabbMax354 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call353, i32 0, i32 1
  %arrayidx355 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax354, i32 0, i32 0
  store i16 %221, i16* %arrayidx355, align 2
  %m_SubtreeHeaders356 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %224 = load i32, i32* %i319, align 4
  %call357 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders356, i32 %224)
  %m_quantizedAabbMax358 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call357, i32 0, i32 1
  %arrayidx359 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax358, i32 0, i32 1
  %225 = load i16, i16* %arrayidx359, align 2
  %226 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders360 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %226, i32 0, i32 13
  %227 = load i32, i32* %i319, align 4
  %call361 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders360, i32 %227)
  %m_quantizedAabbMax362 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call361, i32 0, i32 1
  %arrayidx363 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax362, i32 0, i32 1
  store i16 %225, i16* %arrayidx363, align 2
  %m_SubtreeHeaders364 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %228 = load i32, i32* %i319, align 4
  %call365 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders364, i32 %228)
  %m_quantizedAabbMax366 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call365, i32 0, i32 1
  %arrayidx367 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax366, i32 0, i32 2
  %229 = load i16, i16* %arrayidx367, align 2
  %230 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders368 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %230, i32 0, i32 13
  %231 = load i32, i32* %i319, align 4
  %call369 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders368, i32 %231)
  %m_quantizedAabbMax370 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call369, i32 0, i32 1
  %arrayidx371 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax370, i32 0, i32 2
  store i16 %229, i16* %arrayidx371, align 2
  %m_SubtreeHeaders372 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %232 = load i32, i32* %i319, align 4
  %call373 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders372, i32 %232)
  %m_rootNodeIndex374 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call373, i32 0, i32 2
  %233 = load i32, i32* %m_rootNodeIndex374, align 4
  %234 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders375 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %234, i32 0, i32 13
  %235 = load i32, i32* %i319, align 4
  %call376 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders375, i32 %235)
  %m_rootNodeIndex377 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call376, i32 0, i32 2
  store i32 %233, i32* %m_rootNodeIndex377, align 4
  %m_SubtreeHeaders378 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %236 = load i32, i32* %i319, align 4
  %call379 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders378, i32 %236)
  %m_subtreeSize380 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call379, i32 0, i32 3
  %237 = load i32, i32* %m_subtreeSize380, align 4
  %238 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders381 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %238, i32 0, i32 13
  %239 = load i32, i32* %i319, align 4
  %call382 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders381, i32 %239)
  %m_subtreeSize383 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call382, i32 0, i32 3
  store i32 %237, i32* %m_subtreeSize383, align 4
  %240 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders384 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %240, i32 0, i32 13
  %241 = load i32, i32* %i319, align 4
  %call385 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders384, i32 %241)
  %m_padding = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call385, i32 0, i32 4
  %arrayidx386 = getelementptr inbounds [3 x i32], [3 x i32]* %m_padding, i32 0, i32 0
  store i32 0, i32* %arrayidx386, align 4
  %242 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders387 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %242, i32 0, i32 13
  %243 = load i32, i32* %i319, align 4
  %call388 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders387, i32 %243)
  %m_padding389 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call388, i32 0, i32 4
  %arrayidx390 = getelementptr inbounds [3 x i32], [3 x i32]* %m_padding389, i32 0, i32 1
  store i32 0, i32* %arrayidx390, align 4
  %244 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders391 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %244, i32 0, i32 13
  %245 = load i32, i32* %i319, align 4
  %call392 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders391, i32 %245)
  %m_padding393 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call392, i32 0, i32 4
  %arrayidx394 = getelementptr inbounds [3 x i32], [3 x i32]* %m_padding393, i32 0, i32 2
  store i32 0, i32* %arrayidx394, align 4
  br label %for.inc395

for.inc395:                                       ; preds = %for.body323
  %246 = load i32, i32* %i319, align 4
  %inc396 = add nsw i32 %246, 1
  store i32 %inc396, i32* %i319, align 4
  br label %for.cond320

for.end397:                                       ; preds = %for.cond320
  br label %if.end398

if.end398:                                        ; preds = %for.end397, %for.end317
  %m_subtreeHeaderCount399 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 14
  %247 = load i32, i32* %m_subtreeHeaderCount399, align 4
  %mul400 = mul i32 32, %247
  %248 = load i8*, i8** %nodeData, align 4
  %add.ptr401 = getelementptr inbounds i8, i8* %248, i32 %mul400
  store i8* %add.ptr401, i8** %nodeData, align 4
  %249 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %targetBvh, align 4
  %m_SubtreeHeaders402 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %249, i32 0, i32 13
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE20initializeFromBufferEPvii(%class.btAlignedObjectArray.4* %m_SubtreeHeaders402, i8* null, i32 0, i32 0)
  %250 = load i8*, i8** %o_alignedDataBuffer.addr, align 4
  %251 = bitcast i8* %250 to i8**
  store i8* null, i8** %251, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN14btQuantizedBvhnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z12btSwapEndiani(i32 %val) #2 comdat {
entry:
  %val.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4
  %0 = load i32, i32* %val.addr, align 4
  %call = call i32 @_Z12btSwapEndianj(i32 %0)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z19btSwapVector3EndianRK9btVector3RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %sourceVec, %class.btVector3* nonnull align 4 dereferenceable(16) %destVec) #2 comdat {
entry:
  %sourceVec.addr = alloca %class.btVector3*, align 4
  %destVec.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %sourceVec, %class.btVector3** %sourceVec.addr, align 4
  store %class.btVector3* %destVec, %class.btVector3** %destVec.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %class.btVector3*, %class.btVector3** %sourceVec.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %1)
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %2
  %3 = load %class.btVector3*, %class.btVector3** %destVec.addr, align 4
  %call1 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %3)
  %4 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call1, i32 %4
  call void @_Z18btSwapScalarEndianRKfRf(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray.0* %this, i8* %buffer, i32 %size, i32 %capacity) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %capacity.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i8* %buffer, i8** %buffer.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 %capacity, i32* %capacity.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE5clearEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 0, i8* %m_ownsMemory, align 4
  %0 = load i8*, i8** %buffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btQuantizedBvhNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* %1, %struct.btQuantizedBvhNode** %m_data, align 4
  %2 = load i32, i32* %size.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %2, i32* %m_size, align 4
  %3 = load i32, i32* %capacity.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %3, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i16 @_Z12btSwapEndiant(i16 zeroext %val) #1 comdat {
entry:
  %val.addr = alloca i16, align 2
  store i16 %val, i16* %val.addr, align 2
  %0 = load i16, i16* %val.addr, align 2
  %conv = zext i16 %0 to i32
  %and = and i32 %conv, 65280
  %shr = ashr i32 %and, 8
  %1 = load i16, i16* %val.addr, align 2
  %conv1 = zext i16 %1 to i32
  %and2 = and i32 %conv1, 255
  %shl = shl i32 %and2, 8
  %or = or i32 %shr, %shl
  %conv3 = trunc i32 %or to i16
  ret i16 %conv3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray* %this, i8* %buffer, i32 %size, i32 %capacity) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %capacity.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i8* %buffer, i8** %buffer.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 %capacity, i32* %capacity.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE5clearEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 0, i8* %m_ownsMemory, align 4
  %0 = load i8*, i8** %buffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btOptimizedBvhNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btOptimizedBvhNode* %1, %struct.btOptimizedBvhNode** %m_data, align 4
  %2 = load i32, i32* %size.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %2, i32* %m_size, align 4
  %3 = load i32, i32* %capacity.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %3, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE20initializeFromBufferEPvii(%class.btAlignedObjectArray.4* %this, i8* %buffer, i32 %size, i32 %capacity) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %buffer.addr = alloca i8*, align 4
  %size.addr = alloca i32, align 4
  %capacity.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i8* %buffer, i8** %buffer.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  store i32 %capacity, i32* %capacity.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE5clearEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 0, i8* %m_ownsMemory, align 4
  %0 = load i8*, i8** %buffer.addr, align 4
  %1 = bitcast i8* %0 to %class.btBvhSubtreeInfo*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btBvhSubtreeInfo* %1, %class.btBvhSubtreeInfo** %m_data, align 4
  %2 = load i32, i32* %size.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %2, i32* %m_size, align 4
  %3 = load i32, i32* %capacity.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %3, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %0, i32 %1
  ret %class.btBvhSubtreeInfo* %arrayidx
}

; Function Attrs: noinline optnone
define hidden %class.btQuantizedBvh* @_ZN14btQuantizedBvh18deSerializeInPlaceEPvjb(i8* %i_alignedDataBuffer, i32 %i_dataBufferSize, i1 zeroext %i_swapEndian) #2 {
entry:
  %retval = alloca %class.btQuantizedBvh*, align 4
  %i_alignedDataBuffer.addr = alloca i8*, align 4
  %i_dataBufferSize.addr = alloca i32, align 4
  %i_swapEndian.addr = alloca i8, align 1
  %bvh = alloca %class.btQuantizedBvh*, align 4
  %calculatedBufSize = alloca i32, align 4
  %nodeData = alloca i8*, align 4
  %sizeToAdd = alloca i32, align 4
  %nodeCount = alloca i32, align 4
  %nodeIndex = alloca i32, align 4
  %nodeIndex82 = alloca i32, align 4
  %i = alloca i32, align 4
  store i8* %i_alignedDataBuffer, i8** %i_alignedDataBuffer.addr, align 4
  store i32 %i_dataBufferSize, i32* %i_dataBufferSize.addr, align 4
  %frombool = zext i1 %i_swapEndian to i8
  store i8 %frombool, i8* %i_swapEndian.addr, align 1
  %0 = load i8*, i8** %i_alignedDataBuffer.addr, align 4
  %cmp = icmp eq i8* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %class.btQuantizedBvh* null, %class.btQuantizedBvh** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i8*, i8** %i_alignedDataBuffer.addr, align 4
  %2 = bitcast i8* %1 to %class.btQuantizedBvh*
  store %class.btQuantizedBvh* %2, %class.btQuantizedBvh** %bvh, align 4
  %3 = load i8, i8* %i_swapEndian.addr, align 1
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %if.then1, label %if.end7

if.then1:                                         ; preds = %if.end
  %4 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %4, i32 0, i32 5
  %5 = load i32, i32* %m_curNodeIndex, align 4
  %call = call i32 @_Z12btSwapEndiani(i32 %5)
  %6 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_curNodeIndex2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %6, i32 0, i32 5
  store i32 %call, i32* %m_curNodeIndex2, align 4
  %7 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %7, i32 0, i32 1
  call void @_Z21btUnSwapVector3EndianR9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMin)
  %8 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %8, i32 0, i32 2
  call void @_Z21btUnSwapVector3EndianR9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhAabbMax)
  %9 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %9, i32 0, i32 3
  call void @_Z21btUnSwapVector3EndianR9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhQuantization)
  %10 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_traversalMode = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %10, i32 0, i32 12
  %11 = load i32, i32* %m_traversalMode, align 4
  %call3 = call i32 @_Z12btSwapEndiani(i32 %11)
  %12 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_traversalMode4 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %12, i32 0, i32 12
  store i32 %call3, i32* %m_traversalMode4, align 4
  %13 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_subtreeHeaderCount = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %13, i32 0, i32 14
  %14 = load i32, i32* %m_subtreeHeaderCount, align 4
  %call5 = call i32 @_Z12btSwapEndiani(i32 %14)
  %15 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_subtreeHeaderCount6 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %15, i32 0, i32 14
  store i32 %call5, i32* %m_subtreeHeaderCount6, align 4
  br label %if.end7

if.end7:                                          ; preds = %if.then1, %if.end
  %16 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %call8 = call i32 @_ZNK14btQuantizedBvh28calculateSerializeBufferSizeEv(%class.btQuantizedBvh* %16)
  store i32 %call8, i32* %calculatedBufSize, align 4
  %17 = load i32, i32* %calculatedBufSize, align 4
  %18 = load i32, i32* %i_dataBufferSize.addr, align 4
  %cmp9 = icmp ugt i32 %17, %18
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end7
  store %class.btQuantizedBvh* null, %class.btQuantizedBvh** %retval, align 4
  br label %return

if.end11:                                         ; preds = %if.end7
  %19 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %20 = bitcast %class.btQuantizedBvh* %19 to i8*
  store i8* %20, i8** %nodeData, align 4
  %21 = load i8*, i8** %nodeData, align 4
  %add.ptr = getelementptr inbounds i8, i8* %21, i32 172
  store i8* %add.ptr, i8** %nodeData, align 4
  store i32 0, i32* %sizeToAdd, align 4
  %22 = load i32, i32* %sizeToAdd, align 4
  %23 = load i8*, i8** %nodeData, align 4
  %add.ptr12 = getelementptr inbounds i8, i8* %23, i32 %22
  store i8* %add.ptr12, i8** %nodeData, align 4
  %24 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_curNodeIndex13 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %24, i32 0, i32 5
  %25 = load i32, i32* %m_curNodeIndex13, align 4
  store i32 %25, i32* %nodeCount, align 4
  %26 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %27 = bitcast %class.btQuantizedBvh* %26 to i8*
  %call14 = call i8* @_ZN14btQuantizedBvhnwEmPv(i32 172, i8* %27)
  %28 = bitcast i8* %call14 to %class.btQuantizedBvh*
  %29 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %call15 = call %class.btQuantizedBvh* @_ZN14btQuantizedBvhC1ERS_b(%class.btQuantizedBvh* %28, %class.btQuantizedBvh* nonnull align 4 dereferenceable(172) %29, i1 zeroext false)
  %30 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %30, i32 0, i32 6
  %31 = load i8, i8* %m_useQuantization, align 4
  %tobool16 = trunc i8 %31 to i1
  br i1 %tobool16, label %if.then17, label %if.else

if.then17:                                        ; preds = %if.end11
  %32 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %32, i32 0, i32 11
  %33 = load i8*, i8** %nodeData, align 4
  %34 = load i32, i32* %nodeCount, align 4
  %35 = load i32, i32* %nodeCount, align 4
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i8* %33, i32 %34, i32 %35)
  %36 = load i8, i8* %i_swapEndian.addr, align 1
  %tobool18 = trunc i8 %36 to i1
  br i1 %tobool18, label %if.then19, label %if.end78

if.then19:                                        ; preds = %if.then17
  store i32 0, i32* %nodeIndex, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then19
  %37 = load i32, i32* %nodeIndex, align 4
  %38 = load i32, i32* %nodeCount, align 4
  %cmp20 = icmp slt i32 %37, %38
  br i1 %cmp20, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %39 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes21 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %39, i32 0, i32 11
  %40 = load i32, i32* %nodeIndex, align 4
  %call22 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes21, i32 %40)
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call22, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %41 = load i16, i16* %arrayidx, align 4
  %call23 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %41)
  %42 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes24 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %42, i32 0, i32 11
  %43 = load i32, i32* %nodeIndex, align 4
  %call25 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes24, i32 %43)
  %m_quantizedAabbMin26 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call25, i32 0, i32 0
  %arrayidx27 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin26, i32 0, i32 0
  store i16 %call23, i16* %arrayidx27, align 4
  %44 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes28 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %44, i32 0, i32 11
  %45 = load i32, i32* %nodeIndex, align 4
  %call29 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes28, i32 %45)
  %m_quantizedAabbMin30 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call29, i32 0, i32 0
  %arrayidx31 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin30, i32 0, i32 1
  %46 = load i16, i16* %arrayidx31, align 2
  %call32 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %46)
  %47 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes33 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %47, i32 0, i32 11
  %48 = load i32, i32* %nodeIndex, align 4
  %call34 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes33, i32 %48)
  %m_quantizedAabbMin35 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call34, i32 0, i32 0
  %arrayidx36 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin35, i32 0, i32 1
  store i16 %call32, i16* %arrayidx36, align 2
  %49 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes37 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %49, i32 0, i32 11
  %50 = load i32, i32* %nodeIndex, align 4
  %call38 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes37, i32 %50)
  %m_quantizedAabbMin39 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call38, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin39, i32 0, i32 2
  %51 = load i16, i16* %arrayidx40, align 4
  %call41 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %51)
  %52 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes42 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %52, i32 0, i32 11
  %53 = load i32, i32* %nodeIndex, align 4
  %call43 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes42, i32 %53)
  %m_quantizedAabbMin44 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call43, i32 0, i32 0
  %arrayidx45 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin44, i32 0, i32 2
  store i16 %call41, i16* %arrayidx45, align 4
  %54 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes46 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %54, i32 0, i32 11
  %55 = load i32, i32* %nodeIndex, align 4
  %call47 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes46, i32 %55)
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call47, i32 0, i32 1
  %arrayidx48 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %56 = load i16, i16* %arrayidx48, align 2
  %call49 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %56)
  %57 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes50 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %57, i32 0, i32 11
  %58 = load i32, i32* %nodeIndex, align 4
  %call51 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes50, i32 %58)
  %m_quantizedAabbMax52 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call51, i32 0, i32 1
  %arrayidx53 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax52, i32 0, i32 0
  store i16 %call49, i16* %arrayidx53, align 2
  %59 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes54 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %59, i32 0, i32 11
  %60 = load i32, i32* %nodeIndex, align 4
  %call55 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes54, i32 %60)
  %m_quantizedAabbMax56 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call55, i32 0, i32 1
  %arrayidx57 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax56, i32 0, i32 1
  %61 = load i16, i16* %arrayidx57, align 2
  %call58 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %61)
  %62 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes59 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %62, i32 0, i32 11
  %63 = load i32, i32* %nodeIndex, align 4
  %call60 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes59, i32 %63)
  %m_quantizedAabbMax61 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call60, i32 0, i32 1
  %arrayidx62 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax61, i32 0, i32 1
  store i16 %call58, i16* %arrayidx62, align 2
  %64 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes63 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %64, i32 0, i32 11
  %65 = load i32, i32* %nodeIndex, align 4
  %call64 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes63, i32 %65)
  %m_quantizedAabbMax65 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call64, i32 0, i32 1
  %arrayidx66 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax65, i32 0, i32 2
  %66 = load i16, i16* %arrayidx66, align 2
  %call67 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %66)
  %67 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes68 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %67, i32 0, i32 11
  %68 = load i32, i32* %nodeIndex, align 4
  %call69 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes68, i32 %68)
  %m_quantizedAabbMax70 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call69, i32 0, i32 1
  %arrayidx71 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax70, i32 0, i32 2
  store i16 %call67, i16* %arrayidx71, align 2
  %69 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes72 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %69, i32 0, i32 11
  %70 = load i32, i32* %nodeIndex, align 4
  %call73 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes72, i32 %70)
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call73, i32 0, i32 2
  %71 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4
  %call74 = call i32 @_Z12btSwapEndiani(i32 %71)
  %72 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_quantizedContiguousNodes75 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %72, i32 0, i32 11
  %73 = load i32, i32* %nodeIndex, align 4
  %call76 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes75, i32 %73)
  %m_escapeIndexOrTriangleIndex77 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call76, i32 0, i32 2
  store i32 %call74, i32* %m_escapeIndexOrTriangleIndex77, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %74 = load i32, i32* %nodeIndex, align 4
  %inc = add nsw i32 %74, 1
  store i32 %inc, i32* %nodeIndex, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end78

if.end78:                                         ; preds = %for.end, %if.then17
  %75 = load i32, i32* %nodeCount, align 4
  %mul = mul i32 16, %75
  %76 = load i8*, i8** %nodeData, align 4
  %add.ptr79 = getelementptr inbounds i8, i8* %76, i32 %mul
  store i8* %add.ptr79, i8** %nodeData, align 4
  br label %if.end114

if.else:                                          ; preds = %if.end11
  %77 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %77, i32 0, i32 9
  %78 = load i8*, i8** %nodeData, align 4
  %79 = load i32, i32* %nodeCount, align 4
  %80 = load i32, i32* %nodeCount, align 4
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE20initializeFromBufferEPvii(%class.btAlignedObjectArray* %m_contiguousNodes, i8* %78, i32 %79, i32 %80)
  %81 = load i8, i8* %i_swapEndian.addr, align 1
  %tobool80 = trunc i8 %81 to i1
  br i1 %tobool80, label %if.then81, label %if.end111

if.then81:                                        ; preds = %if.else
  store i32 0, i32* %nodeIndex82, align 4
  br label %for.cond83

for.cond83:                                       ; preds = %for.inc108, %if.then81
  %82 = load i32, i32* %nodeIndex82, align 4
  %83 = load i32, i32* %nodeCount, align 4
  %cmp84 = icmp slt i32 %82, %83
  br i1 %cmp84, label %for.body85, label %for.end110

for.body85:                                       ; preds = %for.cond83
  %84 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_contiguousNodes86 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %84, i32 0, i32 9
  %85 = load i32, i32* %nodeIndex82, align 4
  %call87 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes86, i32 %85)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call87, i32 0, i32 0
  call void @_Z21btUnSwapVector3EndianR9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMinOrg)
  %86 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_contiguousNodes88 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %86, i32 0, i32 9
  %87 = load i32, i32* %nodeIndex82, align 4
  %call89 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes88, i32 %87)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call89, i32 0, i32 1
  call void @_Z21btUnSwapVector3EndianR9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMaxOrg)
  %88 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_contiguousNodes90 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %88, i32 0, i32 9
  %89 = load i32, i32* %nodeIndex82, align 4
  %call91 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes90, i32 %89)
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call91, i32 0, i32 2
  %90 = load i32, i32* %m_escapeIndex, align 4
  %call92 = call i32 @_Z12btSwapEndiani(i32 %90)
  %91 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_contiguousNodes93 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %91, i32 0, i32 9
  %92 = load i32, i32* %nodeIndex82, align 4
  %call94 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes93, i32 %92)
  %m_escapeIndex95 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call94, i32 0, i32 2
  store i32 %call92, i32* %m_escapeIndex95, align 4
  %93 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_contiguousNodes96 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %93, i32 0, i32 9
  %94 = load i32, i32* %nodeIndex82, align 4
  %call97 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes96, i32 %94)
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call97, i32 0, i32 3
  %95 = load i32, i32* %m_subPart, align 4
  %call98 = call i32 @_Z12btSwapEndiani(i32 %95)
  %96 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_contiguousNodes99 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %96, i32 0, i32 9
  %97 = load i32, i32* %nodeIndex82, align 4
  %call100 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes99, i32 %97)
  %m_subPart101 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call100, i32 0, i32 3
  store i32 %call98, i32* %m_subPart101, align 4
  %98 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_contiguousNodes102 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %98, i32 0, i32 9
  %99 = load i32, i32* %nodeIndex82, align 4
  %call103 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes102, i32 %99)
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call103, i32 0, i32 4
  %100 = load i32, i32* %m_triangleIndex, align 4
  %call104 = call i32 @_Z12btSwapEndiani(i32 %100)
  %101 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_contiguousNodes105 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %101, i32 0, i32 9
  %102 = load i32, i32* %nodeIndex82, align 4
  %call106 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes105, i32 %102)
  %m_triangleIndex107 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call106, i32 0, i32 4
  store i32 %call104, i32* %m_triangleIndex107, align 4
  br label %for.inc108

for.inc108:                                       ; preds = %for.body85
  %103 = load i32, i32* %nodeIndex82, align 4
  %inc109 = add nsw i32 %103, 1
  store i32 %inc109, i32* %nodeIndex82, align 4
  br label %for.cond83

for.end110:                                       ; preds = %for.cond83
  br label %if.end111

if.end111:                                        ; preds = %for.end110, %if.else
  %104 = load i32, i32* %nodeCount, align 4
  %mul112 = mul i32 64, %104
  %105 = load i8*, i8** %nodeData, align 4
  %add.ptr113 = getelementptr inbounds i8, i8* %105, i32 %mul112
  store i8* %add.ptr113, i8** %nodeData, align 4
  br label %if.end114

if.end114:                                        ; preds = %if.end111, %if.end78
  store i32 0, i32* %sizeToAdd, align 4
  %106 = load i32, i32* %sizeToAdd, align 4
  %107 = load i8*, i8** %nodeData, align 4
  %add.ptr115 = getelementptr inbounds i8, i8* %107, i32 %106
  store i8* %add.ptr115, i8** %nodeData, align 4
  %108 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %108, i32 0, i32 13
  %109 = load i8*, i8** %nodeData, align 4
  %110 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_subtreeHeaderCount116 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %110, i32 0, i32 14
  %111 = load i32, i32* %m_subtreeHeaderCount116, align 4
  %112 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_subtreeHeaderCount117 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %112, i32 0, i32 14
  %113 = load i32, i32* %m_subtreeHeaderCount117, align 4
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE20initializeFromBufferEPvii(%class.btAlignedObjectArray.4* %m_SubtreeHeaders, i8* %109, i32 %111, i32 %113)
  %114 = load i8, i8* %i_swapEndian.addr, align 1
  %tobool118 = trunc i8 %114 to i1
  br i1 %tobool118, label %if.then119, label %if.end193

if.then119:                                       ; preds = %if.end114
  store i32 0, i32* %i, align 4
  br label %for.cond120

for.cond120:                                      ; preds = %for.inc190, %if.then119
  %115 = load i32, i32* %i, align 4
  %116 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_subtreeHeaderCount121 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %116, i32 0, i32 14
  %117 = load i32, i32* %m_subtreeHeaderCount121, align 4
  %cmp122 = icmp slt i32 %115, %117
  br i1 %cmp122, label %for.body123, label %for.end192

for.body123:                                      ; preds = %for.cond120
  %118 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders124 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %118, i32 0, i32 13
  %119 = load i32, i32* %i, align 4
  %call125 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders124, i32 %119)
  %m_quantizedAabbMin126 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call125, i32 0, i32 0
  %arrayidx127 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin126, i32 0, i32 0
  %120 = load i16, i16* %arrayidx127, align 4
  %call128 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %120)
  %121 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders129 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %121, i32 0, i32 13
  %122 = load i32, i32* %i, align 4
  %call130 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders129, i32 %122)
  %m_quantizedAabbMin131 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call130, i32 0, i32 0
  %arrayidx132 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin131, i32 0, i32 0
  store i16 %call128, i16* %arrayidx132, align 4
  %123 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders133 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %123, i32 0, i32 13
  %124 = load i32, i32* %i, align 4
  %call134 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders133, i32 %124)
  %m_quantizedAabbMin135 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call134, i32 0, i32 0
  %arrayidx136 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin135, i32 0, i32 1
  %125 = load i16, i16* %arrayidx136, align 2
  %call137 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %125)
  %126 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders138 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %126, i32 0, i32 13
  %127 = load i32, i32* %i, align 4
  %call139 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders138, i32 %127)
  %m_quantizedAabbMin140 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call139, i32 0, i32 0
  %arrayidx141 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin140, i32 0, i32 1
  store i16 %call137, i16* %arrayidx141, align 2
  %128 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders142 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %128, i32 0, i32 13
  %129 = load i32, i32* %i, align 4
  %call143 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders142, i32 %129)
  %m_quantizedAabbMin144 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call143, i32 0, i32 0
  %arrayidx145 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin144, i32 0, i32 2
  %130 = load i16, i16* %arrayidx145, align 4
  %call146 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %130)
  %131 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders147 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %131, i32 0, i32 13
  %132 = load i32, i32* %i, align 4
  %call148 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders147, i32 %132)
  %m_quantizedAabbMin149 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call148, i32 0, i32 0
  %arrayidx150 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin149, i32 0, i32 2
  store i16 %call146, i16* %arrayidx150, align 4
  %133 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders151 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %133, i32 0, i32 13
  %134 = load i32, i32* %i, align 4
  %call152 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders151, i32 %134)
  %m_quantizedAabbMax153 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call152, i32 0, i32 1
  %arrayidx154 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax153, i32 0, i32 0
  %135 = load i16, i16* %arrayidx154, align 2
  %call155 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %135)
  %136 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders156 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %136, i32 0, i32 13
  %137 = load i32, i32* %i, align 4
  %call157 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders156, i32 %137)
  %m_quantizedAabbMax158 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call157, i32 0, i32 1
  %arrayidx159 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax158, i32 0, i32 0
  store i16 %call155, i16* %arrayidx159, align 2
  %138 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders160 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %138, i32 0, i32 13
  %139 = load i32, i32* %i, align 4
  %call161 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders160, i32 %139)
  %m_quantizedAabbMax162 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call161, i32 0, i32 1
  %arrayidx163 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax162, i32 0, i32 1
  %140 = load i16, i16* %arrayidx163, align 2
  %call164 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %140)
  %141 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders165 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %141, i32 0, i32 13
  %142 = load i32, i32* %i, align 4
  %call166 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders165, i32 %142)
  %m_quantizedAabbMax167 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call166, i32 0, i32 1
  %arrayidx168 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax167, i32 0, i32 1
  store i16 %call164, i16* %arrayidx168, align 2
  %143 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders169 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %143, i32 0, i32 13
  %144 = load i32, i32* %i, align 4
  %call170 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders169, i32 %144)
  %m_quantizedAabbMax171 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call170, i32 0, i32 1
  %arrayidx172 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax171, i32 0, i32 2
  %145 = load i16, i16* %arrayidx172, align 2
  %call173 = call zeroext i16 @_Z12btSwapEndiant(i16 zeroext %145)
  %146 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders174 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %146, i32 0, i32 13
  %147 = load i32, i32* %i, align 4
  %call175 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders174, i32 %147)
  %m_quantizedAabbMax176 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call175, i32 0, i32 1
  %arrayidx177 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax176, i32 0, i32 2
  store i16 %call173, i16* %arrayidx177, align 2
  %148 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders178 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %148, i32 0, i32 13
  %149 = load i32, i32* %i, align 4
  %call179 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders178, i32 %149)
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call179, i32 0, i32 2
  %150 = load i32, i32* %m_rootNodeIndex, align 4
  %call180 = call i32 @_Z12btSwapEndiani(i32 %150)
  %151 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders181 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %151, i32 0, i32 13
  %152 = load i32, i32* %i, align 4
  %call182 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders181, i32 %152)
  %m_rootNodeIndex183 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call182, i32 0, i32 2
  store i32 %call180, i32* %m_rootNodeIndex183, align 4
  %153 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders184 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %153, i32 0, i32 13
  %154 = load i32, i32* %i, align 4
  %call185 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders184, i32 %154)
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call185, i32 0, i32 3
  %155 = load i32, i32* %m_subtreeSize, align 4
  %call186 = call i32 @_Z12btSwapEndiani(i32 %155)
  %156 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  %m_SubtreeHeaders187 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %156, i32 0, i32 13
  %157 = load i32, i32* %i, align 4
  %call188 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders187, i32 %157)
  %m_subtreeSize189 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call188, i32 0, i32 3
  store i32 %call186, i32* %m_subtreeSize189, align 4
  br label %for.inc190

for.inc190:                                       ; preds = %for.body123
  %158 = load i32, i32* %i, align 4
  %inc191 = add nsw i32 %158, 1
  store i32 %inc191, i32* %i, align 4
  br label %for.cond120

for.end192:                                       ; preds = %for.cond120
  br label %if.end193

if.end193:                                        ; preds = %for.end192, %if.end114
  %159 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %bvh, align 4
  store %class.btQuantizedBvh* %159, %class.btQuantizedBvh** %retval, align 4
  br label %return

return:                                           ; preds = %if.end193, %if.then10, %if.then
  %160 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %retval, align 4
  ret %class.btQuantizedBvh* %160
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z21btUnSwapVector3EndianR9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %vector) #2 comdat {
entry:
  %vector.addr = alloca %class.btVector3*, align 4
  %swappedVec = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %vector, %class.btVector3** %vector.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %swappedVec)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %class.btVector3*, %class.btVector3** %vector.addr, align 4
  %call1 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %1)
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call1, i32 %2
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %swappedVec)
  %3 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 %3
  call void @_Z18btSwapScalarEndianRKfRf(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %5 = load %class.btVector3*, %class.btVector3** %vector.addr, align 4
  %6 = bitcast %class.btVector3* %5 to i8*
  %7 = bitcast %class.btVector3* %swappedVec to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btQuantizedBvh* @_ZN14btQuantizedBvhC2ERS_b(%class.btQuantizedBvh* returned %this, %class.btQuantizedBvh* nonnull align 4 dereferenceable(172) %self, i1 zeroext %0) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %self.addr = alloca %class.btQuantizedBvh*, align 4
  %.addr = alloca i8, align 1
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store %class.btQuantizedBvh* %self, %class.btQuantizedBvh** %self.addr, align 4
  %frombool = zext i1 %0 to i8
  store i8 %frombool, i8* %.addr, align 1
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %1 = bitcast %class.btQuantizedBvh* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV14btQuantizedBvh, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %2 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %self.addr, align 4
  %m_bvhAabbMin2 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %2, i32 0, i32 1
  %3 = bitcast %class.btVector3* %m_bvhAabbMin to i8*
  %4 = bitcast %class.btVector3* %m_bvhAabbMin2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %5 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %self.addr, align 4
  %m_bvhAabbMax3 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %5, i32 0, i32 2
  %6 = bitcast %class.btVector3* %m_bvhAabbMax to i8*
  %7 = bitcast %class.btVector3* %m_bvhAabbMax3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %8 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %self.addr, align 4
  %m_bvhQuantization4 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %8, i32 0, i32 3
  %9 = bitcast %class.btVector3* %m_bvhQuantization to i8*
  %10 = bitcast %class.btVector3* %m_bvhQuantization4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %m_bulletVersion = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 4
  store i32 286, i32* %m_bulletVersion, align 4
  %m_leafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 8
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEC2Ev(%class.btAlignedObjectArray* %m_leafNodes)
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call5 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEC2Ev(%class.btAlignedObjectArray* %m_contiguousNodes)
  %m_quantizedLeafNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 10
  %call6 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEC2Ev(%class.btAlignedObjectArray.0* %m_quantizedLeafNodes)
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call7 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEC2Ev(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes)
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call8 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEC2Ev(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  ret %class.btQuantizedBvh* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btQuantizedBvh16deSerializeFloatER23btQuantizedBvhFloatData(%class.btQuantizedBvh* %this, %struct.btQuantizedBvhFloatData* nonnull align 4 dereferenceable(84) %quantizedBvhFloatData) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %quantizedBvhFloatData.addr = alloca %struct.btQuantizedBvhFloatData*, align 4
  %numElem = alloca i32, align 4
  %ref.tmp = alloca %struct.btOptimizedBvhNode, align 4
  %memPtr = alloca %struct.btOptimizedBvhNodeFloatData*, align 4
  %i = alloca i32, align 4
  %numElem23 = alloca i32, align 4
  %ref.tmp24 = alloca %struct.btQuantizedBvhNode, align 4
  %memPtr27 = alloca %struct.btQuantizedBvhNodeData*, align 4
  %i28 = alloca i32, align 4
  %numElem74 = alloca i32, align 4
  %ref.tmp75 = alloca %class.btBvhSubtreeInfo, align 4
  %memPtr79 = alloca %struct.btBvhSubtreeInfoData*, align 4
  %i80 = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store %struct.btQuantizedBvhFloatData* %quantizedBvhFloatData, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %0 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4
  %m_bvhAabbMax2 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %0, i32 0, i32 1
  call void @_ZN9btVector316deSerializeFloatERK18btVector3FloatData(%class.btVector3* %m_bvhAabbMax, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_bvhAabbMax2)
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %1 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4
  %m_bvhAabbMin3 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %1, i32 0, i32 0
  call void @_ZN9btVector316deSerializeFloatERK18btVector3FloatData(%class.btVector3* %m_bvhAabbMin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_bvhAabbMin3)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %2 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4
  %m_bvhQuantization4 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %2, i32 0, i32 2
  call void @_ZN9btVector316deSerializeFloatERK18btVector3FloatData(%class.btVector3* %m_bvhQuantization, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_bvhQuantization4)
  %3 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4
  %m_curNodeIndex = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %3, i32 0, i32 3
  %4 = load i32, i32* %m_curNodeIndex, align 4
  %m_curNodeIndex5 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  store i32 %4, i32* %m_curNodeIndex5, align 4
  %5 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4
  %m_useQuantization = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %5, i32 0, i32 4
  %6 = load i32, i32* %m_useQuantization, align 4
  %cmp = icmp ne i32 %6, 0
  %m_useQuantization6 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %m_useQuantization6, align 4
  %7 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4
  %m_numContiguousLeafNodes = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %7, i32 0, i32 5
  %8 = load i32, i32* %m_numContiguousLeafNodes, align 4
  store i32 %8, i32* %numElem, align 4
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %9 = load i32, i32* %numElem, align 4
  %10 = bitcast %struct.btOptimizedBvhNode* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %10, i8 0, i32 64, i1 false)
  %call = call %struct.btOptimizedBvhNode* @_ZN18btOptimizedBvhNodeC2Ev(%struct.btOptimizedBvhNode* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %9, %struct.btOptimizedBvhNode* nonnull align 4 dereferenceable(64) %ref.tmp)
  %11 = load i32, i32* %numElem, align 4
  %tobool = icmp ne i32 %11, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %12 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4
  %m_contiguousNodesPtr = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %12, i32 0, i32 7
  %13 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %m_contiguousNodesPtr, align 4
  store %struct.btOptimizedBvhNodeFloatData* %13, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %14 = load i32, i32* %i, align 4
  %15 = load i32, i32* %numElem, align 4
  %cmp7 = icmp slt i32 %14, %15
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_contiguousNodes8 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %16 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes8, i32 %16)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call9, i32 0, i32 1
  %17 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  %m_aabbMaxOrg10 = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %17, i32 0, i32 1
  call void @_ZN9btVector316deSerializeFloatERK18btVector3FloatData(%class.btVector3* %m_aabbMaxOrg, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_aabbMaxOrg10)
  %m_contiguousNodes11 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %18 = load i32, i32* %i, align 4
  %call12 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes11, i32 %18)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call12, i32 0, i32 0
  %19 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  %m_aabbMinOrg13 = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %19, i32 0, i32 0
  call void @_ZN9btVector316deSerializeFloatERK18btVector3FloatData(%class.btVector3* %m_aabbMinOrg, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_aabbMinOrg13)
  %20 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %20, i32 0, i32 2
  %21 = load i32, i32* %m_escapeIndex, align 4
  %m_contiguousNodes14 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %22 = load i32, i32* %i, align 4
  %call15 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes14, i32 %22)
  %m_escapeIndex16 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call15, i32 0, i32 2
  store i32 %21, i32* %m_escapeIndex16, align 4
  %23 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %23, i32 0, i32 3
  %24 = load i32, i32* %m_subPart, align 4
  %m_contiguousNodes17 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %25 = load i32, i32* %i, align 4
  %call18 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes17, i32 %25)
  %m_subPart19 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call18, i32 0, i32 3
  store i32 %24, i32* %m_subPart19, align 4
  %26 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %26, i32 0, i32 4
  %27 = load i32, i32* %m_triangleIndex, align 4
  %m_contiguousNodes20 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %28 = load i32, i32* %i, align 4
  %call21 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes20, i32 %28)
  %m_triangleIndex22 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call21, i32 0, i32 4
  store i32 %27, i32* %m_triangleIndex22, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %29 = load i32, i32* %i, align 4
  %inc = add nsw i32 %29, 1
  store i32 %inc, i32* %i, align 4
  %30 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  %incdec.ptr = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %30, i32 1
  store %struct.btOptimizedBvhNodeFloatData* %incdec.ptr, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  %31 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4
  %m_numQuantizedContiguousNodes = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %31, i32 0, i32 6
  %32 = load i32, i32* %m_numQuantizedContiguousNodes, align 4
  store i32 %32, i32* %numElem23, align 4
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %33 = load i32, i32* %numElem23, align 4
  %34 = bitcast %struct.btQuantizedBvhNode* %ref.tmp24 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %34, i8 0, i32 16, i1 false)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %33, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %ref.tmp24)
  %35 = load i32, i32* %numElem23, align 4
  %tobool25 = icmp ne i32 %35, 0
  br i1 %tobool25, label %if.then26, label %if.end72

if.then26:                                        ; preds = %if.end
  %36 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4
  %m_quantizedContiguousNodesPtr = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %36, i32 0, i32 8
  %37 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %m_quantizedContiguousNodesPtr, align 4
  store %struct.btQuantizedBvhNodeData* %37, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  store i32 0, i32* %i28, align 4
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc68, %if.then26
  %38 = load i32, i32* %i28, align 4
  %39 = load i32, i32* %numElem23, align 4
  %cmp30 = icmp slt i32 %38, %39
  br i1 %cmp30, label %for.body31, label %for.end71

for.body31:                                       ; preds = %for.cond29
  %40 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %40, i32 0, i32 2
  %41 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4
  %m_quantizedContiguousNodes32 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %42 = load i32, i32* %i28, align 4
  %call33 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes32, i32 %42)
  %m_escapeIndexOrTriangleIndex34 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call33, i32 0, i32 2
  store i32 %41, i32* %m_escapeIndexOrTriangleIndex34, align 4
  %43 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %43, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %44 = load i16, i16* %arrayidx, align 2
  %m_quantizedContiguousNodes35 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %45 = load i32, i32* %i28, align 4
  %call36 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes35, i32 %45)
  %m_quantizedAabbMax37 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call36, i32 0, i32 1
  %arrayidx38 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax37, i32 0, i32 0
  store i16 %44, i16* %arrayidx38, align 2
  %46 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %m_quantizedAabbMax39 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %46, i32 0, i32 1
  %arrayidx40 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax39, i32 0, i32 1
  %47 = load i16, i16* %arrayidx40, align 2
  %m_quantizedContiguousNodes41 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %48 = load i32, i32* %i28, align 4
  %call42 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes41, i32 %48)
  %m_quantizedAabbMax43 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call42, i32 0, i32 1
  %arrayidx44 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax43, i32 0, i32 1
  store i16 %47, i16* %arrayidx44, align 2
  %49 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %m_quantizedAabbMax45 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %49, i32 0, i32 1
  %arrayidx46 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax45, i32 0, i32 2
  %50 = load i16, i16* %arrayidx46, align 2
  %m_quantizedContiguousNodes47 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %51 = load i32, i32* %i28, align 4
  %call48 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes47, i32 %51)
  %m_quantizedAabbMax49 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call48, i32 0, i32 1
  %arrayidx50 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax49, i32 0, i32 2
  store i16 %50, i16* %arrayidx50, align 2
  %52 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %52, i32 0, i32 0
  %arrayidx51 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %53 = load i16, i16* %arrayidx51, align 4
  %m_quantizedContiguousNodes52 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %54 = load i32, i32* %i28, align 4
  %call53 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes52, i32 %54)
  %m_quantizedAabbMin54 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call53, i32 0, i32 0
  %arrayidx55 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin54, i32 0, i32 0
  store i16 %53, i16* %arrayidx55, align 4
  %55 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %m_quantizedAabbMin56 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %55, i32 0, i32 0
  %arrayidx57 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin56, i32 0, i32 1
  %56 = load i16, i16* %arrayidx57, align 2
  %m_quantizedContiguousNodes58 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %57 = load i32, i32* %i28, align 4
  %call59 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes58, i32 %57)
  %m_quantizedAabbMin60 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call59, i32 0, i32 0
  %arrayidx61 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin60, i32 0, i32 1
  store i16 %56, i16* %arrayidx61, align 2
  %58 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %m_quantizedAabbMin62 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %58, i32 0, i32 0
  %arrayidx63 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin62, i32 0, i32 2
  %59 = load i16, i16* %arrayidx63, align 4
  %m_quantizedContiguousNodes64 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %60 = load i32, i32* %i28, align 4
  %call65 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes64, i32 %60)
  %m_quantizedAabbMin66 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call65, i32 0, i32 0
  %arrayidx67 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin66, i32 0, i32 2
  store i16 %59, i16* %arrayidx67, align 4
  br label %for.inc68

for.inc68:                                        ; preds = %for.body31
  %61 = load i32, i32* %i28, align 4
  %inc69 = add nsw i32 %61, 1
  store i32 %inc69, i32* %i28, align 4
  %62 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %incdec.ptr70 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %62, i32 1
  store %struct.btQuantizedBvhNodeData* %incdec.ptr70, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  br label %for.cond29

for.end71:                                        ; preds = %for.cond29
  br label %if.end72

if.end72:                                         ; preds = %for.end71, %if.end
  %63 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4
  %m_traversalMode = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %63, i32 0, i32 10
  %64 = load i32, i32* %m_traversalMode, align 4
  %m_traversalMode73 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 12
  store i32 %64, i32* %m_traversalMode73, align 4
  %65 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4
  %m_numSubtreeHeaders = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %65, i32 0, i32 11
  %66 = load i32, i32* %m_numSubtreeHeaders, align 4
  store i32 %66, i32* %numElem74, align 4
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %67 = load i32, i32* %numElem74, align 4
  %call76 = call %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* %ref.tmp75)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6resizeEiRKS0_(%class.btAlignedObjectArray.4* %m_SubtreeHeaders, i32 %67, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %ref.tmp75)
  %68 = load i32, i32* %numElem74, align 4
  %tobool77 = icmp ne i32 %68, 0
  br i1 %tobool77, label %if.then78, label %if.end130

if.then78:                                        ; preds = %if.end72
  %69 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedBvhFloatData.addr, align 4
  %m_subTreeInfoPtr = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %69, i32 0, i32 9
  %70 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %m_subTreeInfoPtr, align 4
  store %struct.btBvhSubtreeInfoData* %70, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  store i32 0, i32* %i80, align 4
  br label %for.cond81

for.cond81:                                       ; preds = %for.inc126, %if.then78
  %71 = load i32, i32* %i80, align 4
  %72 = load i32, i32* %numElem74, align 4
  %cmp82 = icmp slt i32 %71, %72
  br i1 %cmp82, label %for.body83, label %for.end129

for.body83:                                       ; preds = %for.cond81
  %73 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_quantizedAabbMax84 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %73, i32 0, i32 3
  %arrayidx85 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax84, i32 0, i32 0
  %74 = load i16, i16* %arrayidx85, align 2
  %m_SubtreeHeaders86 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %75 = load i32, i32* %i80, align 4
  %call87 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders86, i32 %75)
  %m_quantizedAabbMax88 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call87, i32 0, i32 1
  %arrayidx89 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax88, i32 0, i32 0
  store i16 %74, i16* %arrayidx89, align 2
  %76 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_quantizedAabbMax90 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %76, i32 0, i32 3
  %arrayidx91 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax90, i32 0, i32 1
  %77 = load i16, i16* %arrayidx91, align 2
  %m_SubtreeHeaders92 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %78 = load i32, i32* %i80, align 4
  %call93 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders92, i32 %78)
  %m_quantizedAabbMax94 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call93, i32 0, i32 1
  %arrayidx95 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax94, i32 0, i32 1
  store i16 %77, i16* %arrayidx95, align 2
  %79 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_quantizedAabbMax96 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %79, i32 0, i32 3
  %arrayidx97 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax96, i32 0, i32 2
  %80 = load i16, i16* %arrayidx97, align 2
  %m_SubtreeHeaders98 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %81 = load i32, i32* %i80, align 4
  %call99 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders98, i32 %81)
  %m_quantizedAabbMax100 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call99, i32 0, i32 1
  %arrayidx101 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax100, i32 0, i32 2
  store i16 %80, i16* %arrayidx101, align 2
  %82 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_quantizedAabbMin102 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %82, i32 0, i32 2
  %arrayidx103 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin102, i32 0, i32 0
  %83 = load i16, i16* %arrayidx103, align 4
  %m_SubtreeHeaders104 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %84 = load i32, i32* %i80, align 4
  %call105 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders104, i32 %84)
  %m_quantizedAabbMin106 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call105, i32 0, i32 0
  %arrayidx107 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin106, i32 0, i32 0
  store i16 %83, i16* %arrayidx107, align 4
  %85 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_quantizedAabbMin108 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %85, i32 0, i32 2
  %arrayidx109 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin108, i32 0, i32 1
  %86 = load i16, i16* %arrayidx109, align 2
  %m_SubtreeHeaders110 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %87 = load i32, i32* %i80, align 4
  %call111 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders110, i32 %87)
  %m_quantizedAabbMin112 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call111, i32 0, i32 0
  %arrayidx113 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin112, i32 0, i32 1
  store i16 %86, i16* %arrayidx113, align 2
  %88 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_quantizedAabbMin114 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %88, i32 0, i32 2
  %arrayidx115 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin114, i32 0, i32 2
  %89 = load i16, i16* %arrayidx115, align 4
  %m_SubtreeHeaders116 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %90 = load i32, i32* %i80, align 4
  %call117 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders116, i32 %90)
  %m_quantizedAabbMin118 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call117, i32 0, i32 0
  %arrayidx119 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin118, i32 0, i32 2
  store i16 %89, i16* %arrayidx119, align 4
  %91 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_rootNodeIndex = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %91, i32 0, i32 0
  %92 = load i32, i32* %m_rootNodeIndex, align 4
  %m_SubtreeHeaders120 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %93 = load i32, i32* %i80, align 4
  %call121 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders120, i32 %93)
  %m_rootNodeIndex122 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call121, i32 0, i32 2
  store i32 %92, i32* %m_rootNodeIndex122, align 4
  %94 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_subtreeSize = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %94, i32 0, i32 1
  %95 = load i32, i32* %m_subtreeSize, align 4
  %m_SubtreeHeaders123 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %96 = load i32, i32* %i80, align 4
  %call124 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders123, i32 %96)
  %m_subtreeSize125 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call124, i32 0, i32 3
  store i32 %95, i32* %m_subtreeSize125, align 4
  br label %for.inc126

for.inc126:                                       ; preds = %for.body83
  %97 = load i32, i32* %i80, align 4
  %inc127 = add nsw i32 %97, 1
  store i32 %inc127, i32* %i80, align 4
  %98 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %incdec.ptr128 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %98, i32 1
  store %struct.btBvhSubtreeInfoData* %incdec.ptr128, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  br label %for.cond81

for.end129:                                       ; preds = %for.cond81
  br label %if.end130

if.end130:                                        ; preds = %for.end129, %if.end72
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector316deSerializeFloatERK18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataIn) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataIn.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataIn, %struct.btVector3FloatData** %dataIn.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataIn.addr, align 4
  %m_floats = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %1, i32 0, i32 0
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %2
  %3 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %3, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %struct.btOptimizedBvhNode* nonnull align 4 dereferenceable(64) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btOptimizedBvhNode*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btOptimizedBvhNode* %fillData, %struct.btOptimizedBvhNode** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %14, i32 %15
  %16 = bitcast %struct.btOptimizedBvhNode* %arrayidx10 to i8*
  %call11 = call i8* @_ZN18btOptimizedBvhNodenwEmPv(i32 64, i8* %16)
  %17 = bitcast i8* %call11 to %struct.btOptimizedBvhNode*
  %18 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %fillData.addr, align 4
  %19 = bitcast %struct.btOptimizedBvhNode* %17 to i8*
  %20 = bitcast %struct.btOptimizedBvhNode* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 64, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btOptimizedBvhNode* @_ZN18btOptimizedBvhNodeC2Ev(%struct.btOptimizedBvhNode* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btOptimizedBvhNode*, align 4
  store %struct.btOptimizedBvhNode* %this, %struct.btOptimizedBvhNode** %this.addr, align 4
  %this1 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %this.addr, align 4
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMinOrg)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMaxOrg)
  ret %struct.btOptimizedBvhNode* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6resizeEiRKS0_(%class.btAlignedObjectArray.4* %this, i32 %newsize, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btBvhSubtreeInfo*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btBvhSubtreeInfo* %fillData, %class.btBvhSubtreeInfo** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %5 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %14 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %14, i32 %15
  %16 = bitcast %class.btBvhSubtreeInfo* %arrayidx10 to i8*
  %call11 = call i8* @_ZN16btBvhSubtreeInfonwEmPv(i32 32, i8* %16)
  %17 = bitcast i8* %call11 to %class.btBvhSubtreeInfo*
  %18 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %fillData.addr, align 4
  %19 = bitcast %class.btBvhSubtreeInfo* %17 to i8*
  %20 = bitcast %class.btBvhSubtreeInfo* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 32, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btQuantizedBvh17deSerializeDoubleER24btQuantizedBvhDoubleData(%class.btQuantizedBvh* %this, %struct.btQuantizedBvhDoubleData* nonnull align 8 dereferenceable(136) %quantizedBvhDoubleData) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %quantizedBvhDoubleData.addr = alloca %struct.btQuantizedBvhDoubleData*, align 4
  %numElem = alloca i32, align 4
  %ref.tmp = alloca %struct.btOptimizedBvhNode, align 4
  %memPtr = alloca %struct.btOptimizedBvhNodeDoubleData*, align 4
  %i = alloca i32, align 4
  %numElem23 = alloca i32, align 4
  %ref.tmp24 = alloca %struct.btQuantizedBvhNode, align 4
  %memPtr27 = alloca %struct.btQuantizedBvhNodeData*, align 4
  %i28 = alloca i32, align 4
  %numElem74 = alloca i32, align 4
  %ref.tmp75 = alloca %class.btBvhSubtreeInfo, align 4
  %memPtr79 = alloca %struct.btBvhSubtreeInfoData*, align 4
  %i80 = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store %struct.btQuantizedBvhDoubleData* %quantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %0 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4
  %m_bvhAabbMax2 = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %0, i32 0, i32 1
  call void @_ZN9btVector317deSerializeDoubleERK19btVector3DoubleData(%class.btVector3* %m_bvhAabbMax, %struct.btVector3DoubleData* nonnull align 8 dereferenceable(32) %m_bvhAabbMax2)
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %1 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4
  %m_bvhAabbMin3 = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %1, i32 0, i32 0
  call void @_ZN9btVector317deSerializeDoubleERK19btVector3DoubleData(%class.btVector3* %m_bvhAabbMin, %struct.btVector3DoubleData* nonnull align 8 dereferenceable(32) %m_bvhAabbMin3)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %2 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4
  %m_bvhQuantization4 = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %2, i32 0, i32 2
  call void @_ZN9btVector317deSerializeDoubleERK19btVector3DoubleData(%class.btVector3* %m_bvhQuantization, %struct.btVector3DoubleData* nonnull align 8 dereferenceable(32) %m_bvhQuantization4)
  %3 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4
  %m_curNodeIndex = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %3, i32 0, i32 3
  %4 = load i32, i32* %m_curNodeIndex, align 8
  %m_curNodeIndex5 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  store i32 %4, i32* %m_curNodeIndex5, align 4
  %5 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4
  %m_useQuantization = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %5, i32 0, i32 4
  %6 = load i32, i32* %m_useQuantization, align 4
  %cmp = icmp ne i32 %6, 0
  %m_useQuantization6 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %m_useQuantization6, align 4
  %7 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4
  %m_numContiguousLeafNodes = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %7, i32 0, i32 5
  %8 = load i32, i32* %m_numContiguousLeafNodes, align 8
  store i32 %8, i32* %numElem, align 4
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %9 = load i32, i32* %numElem, align 4
  %10 = bitcast %struct.btOptimizedBvhNode* %ref.tmp to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %10, i8 0, i32 64, i1 false)
  %call = call %struct.btOptimizedBvhNode* @_ZN18btOptimizedBvhNodeC2Ev(%struct.btOptimizedBvhNode* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray* %m_contiguousNodes, i32 %9, %struct.btOptimizedBvhNode* nonnull align 4 dereferenceable(64) %ref.tmp)
  %11 = load i32, i32* %numElem, align 4
  %tobool = icmp ne i32 %11, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %12 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4
  %m_contiguousNodesPtr = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %12, i32 0, i32 7
  %13 = load %struct.btOptimizedBvhNodeDoubleData*, %struct.btOptimizedBvhNodeDoubleData** %m_contiguousNodesPtr, align 8
  store %struct.btOptimizedBvhNodeDoubleData* %13, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %14 = load i32, i32* %i, align 4
  %15 = load i32, i32* %numElem, align 4
  %cmp7 = icmp slt i32 %14, %15
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_contiguousNodes8 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %16 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes8, i32 %16)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call9, i32 0, i32 1
  %17 = load %struct.btOptimizedBvhNodeDoubleData*, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4
  %m_aabbMaxOrg10 = getelementptr inbounds %struct.btOptimizedBvhNodeDoubleData, %struct.btOptimizedBvhNodeDoubleData* %17, i32 0, i32 1
  call void @_ZN9btVector317deSerializeDoubleERK19btVector3DoubleData(%class.btVector3* %m_aabbMaxOrg, %struct.btVector3DoubleData* nonnull align 8 dereferenceable(32) %m_aabbMaxOrg10)
  %m_contiguousNodes11 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %18 = load i32, i32* %i, align 4
  %call12 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes11, i32 %18)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call12, i32 0, i32 0
  %19 = load %struct.btOptimizedBvhNodeDoubleData*, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4
  %m_aabbMinOrg13 = getelementptr inbounds %struct.btOptimizedBvhNodeDoubleData, %struct.btOptimizedBvhNodeDoubleData* %19, i32 0, i32 0
  call void @_ZN9btVector317deSerializeDoubleERK19btVector3DoubleData(%class.btVector3* %m_aabbMinOrg, %struct.btVector3DoubleData* nonnull align 8 dereferenceable(32) %m_aabbMinOrg13)
  %20 = load %struct.btOptimizedBvhNodeDoubleData*, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNodeDoubleData, %struct.btOptimizedBvhNodeDoubleData* %20, i32 0, i32 2
  %21 = load i32, i32* %m_escapeIndex, align 8
  %m_contiguousNodes14 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %22 = load i32, i32* %i, align 4
  %call15 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes14, i32 %22)
  %m_escapeIndex16 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call15, i32 0, i32 2
  store i32 %21, i32* %m_escapeIndex16, align 4
  %23 = load %struct.btOptimizedBvhNodeDoubleData*, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNodeDoubleData, %struct.btOptimizedBvhNodeDoubleData* %23, i32 0, i32 3
  %24 = load i32, i32* %m_subPart, align 4
  %m_contiguousNodes17 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %25 = load i32, i32* %i, align 4
  %call18 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes17, i32 %25)
  %m_subPart19 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call18, i32 0, i32 3
  store i32 %24, i32* %m_subPart19, align 4
  %26 = load %struct.btOptimizedBvhNodeDoubleData*, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNodeDoubleData, %struct.btOptimizedBvhNodeDoubleData* %26, i32 0, i32 4
  %27 = load i32, i32* %m_triangleIndex, align 8
  %m_contiguousNodes20 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %28 = load i32, i32* %i, align 4
  %call21 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes20, i32 %28)
  %m_triangleIndex22 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call21, i32 0, i32 4
  store i32 %27, i32* %m_triangleIndex22, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %29 = load i32, i32* %i, align 4
  %inc = add nsw i32 %29, 1
  store i32 %inc, i32* %i, align 4
  %30 = load %struct.btOptimizedBvhNodeDoubleData*, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4
  %incdec.ptr = getelementptr inbounds %struct.btOptimizedBvhNodeDoubleData, %struct.btOptimizedBvhNodeDoubleData* %30, i32 1
  store %struct.btOptimizedBvhNodeDoubleData* %incdec.ptr, %struct.btOptimizedBvhNodeDoubleData** %memPtr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  %31 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4
  %m_numQuantizedContiguousNodes = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %31, i32 0, i32 6
  %32 = load i32, i32* %m_numQuantizedContiguousNodes, align 4
  store i32 %32, i32* %numElem23, align 4
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %33 = load i32, i32* %numElem23, align 4
  %34 = bitcast %struct.btQuantizedBvhNode* %ref.tmp24 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %34, i8 0, i32 16, i1 false)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes, i32 %33, %struct.btQuantizedBvhNode* nonnull align 4 dereferenceable(16) %ref.tmp24)
  %35 = load i32, i32* %numElem23, align 4
  %tobool25 = icmp ne i32 %35, 0
  br i1 %tobool25, label %if.then26, label %if.end72

if.then26:                                        ; preds = %if.end
  %36 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4
  %m_quantizedContiguousNodesPtr = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %36, i32 0, i32 8
  %37 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %m_quantizedContiguousNodesPtr, align 4
  store %struct.btQuantizedBvhNodeData* %37, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  store i32 0, i32* %i28, align 4
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc68, %if.then26
  %38 = load i32, i32* %i28, align 4
  %39 = load i32, i32* %numElem23, align 4
  %cmp30 = icmp slt i32 %38, %39
  br i1 %cmp30, label %for.body31, label %for.end71

for.body31:                                       ; preds = %for.cond29
  %40 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %40, i32 0, i32 2
  %41 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4
  %m_quantizedContiguousNodes32 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %42 = load i32, i32* %i28, align 4
  %call33 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes32, i32 %42)
  %m_escapeIndexOrTriangleIndex34 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call33, i32 0, i32 2
  store i32 %41, i32* %m_escapeIndexOrTriangleIndex34, align 4
  %43 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %43, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %44 = load i16, i16* %arrayidx, align 2
  %m_quantizedContiguousNodes35 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %45 = load i32, i32* %i28, align 4
  %call36 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes35, i32 %45)
  %m_quantizedAabbMax37 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call36, i32 0, i32 1
  %arrayidx38 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax37, i32 0, i32 0
  store i16 %44, i16* %arrayidx38, align 2
  %46 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %m_quantizedAabbMax39 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %46, i32 0, i32 1
  %arrayidx40 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax39, i32 0, i32 1
  %47 = load i16, i16* %arrayidx40, align 2
  %m_quantizedContiguousNodes41 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %48 = load i32, i32* %i28, align 4
  %call42 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes41, i32 %48)
  %m_quantizedAabbMax43 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call42, i32 0, i32 1
  %arrayidx44 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax43, i32 0, i32 1
  store i16 %47, i16* %arrayidx44, align 2
  %49 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %m_quantizedAabbMax45 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %49, i32 0, i32 1
  %arrayidx46 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax45, i32 0, i32 2
  %50 = load i16, i16* %arrayidx46, align 2
  %m_quantizedContiguousNodes47 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %51 = load i32, i32* %i28, align 4
  %call48 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes47, i32 %51)
  %m_quantizedAabbMax49 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call48, i32 0, i32 1
  %arrayidx50 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax49, i32 0, i32 2
  store i16 %50, i16* %arrayidx50, align 2
  %52 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %52, i32 0, i32 0
  %arrayidx51 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %53 = load i16, i16* %arrayidx51, align 4
  %m_quantizedContiguousNodes52 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %54 = load i32, i32* %i28, align 4
  %call53 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes52, i32 %54)
  %m_quantizedAabbMin54 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call53, i32 0, i32 0
  %arrayidx55 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin54, i32 0, i32 0
  store i16 %53, i16* %arrayidx55, align 4
  %55 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %m_quantizedAabbMin56 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %55, i32 0, i32 0
  %arrayidx57 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin56, i32 0, i32 1
  %56 = load i16, i16* %arrayidx57, align 2
  %m_quantizedContiguousNodes58 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %57 = load i32, i32* %i28, align 4
  %call59 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes58, i32 %57)
  %m_quantizedAabbMin60 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call59, i32 0, i32 0
  %arrayidx61 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin60, i32 0, i32 1
  store i16 %56, i16* %arrayidx61, align 2
  %58 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %m_quantizedAabbMin62 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %58, i32 0, i32 0
  %arrayidx63 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin62, i32 0, i32 2
  %59 = load i16, i16* %arrayidx63, align 4
  %m_quantizedContiguousNodes64 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %60 = load i32, i32* %i28, align 4
  %call65 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes64, i32 %60)
  %m_quantizedAabbMin66 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call65, i32 0, i32 0
  %arrayidx67 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin66, i32 0, i32 2
  store i16 %59, i16* %arrayidx67, align 4
  br label %for.inc68

for.inc68:                                        ; preds = %for.body31
  %61 = load i32, i32* %i28, align 4
  %inc69 = add nsw i32 %61, 1
  store i32 %inc69, i32* %i28, align 4
  %62 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  %incdec.ptr70 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %62, i32 1
  store %struct.btQuantizedBvhNodeData* %incdec.ptr70, %struct.btQuantizedBvhNodeData** %memPtr27, align 4
  br label %for.cond29

for.end71:                                        ; preds = %for.cond29
  br label %if.end72

if.end72:                                         ; preds = %for.end71, %if.end
  %63 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4
  %m_traversalMode = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %63, i32 0, i32 9
  %64 = load i32, i32* %m_traversalMode, align 8
  %m_traversalMode73 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 12
  store i32 %64, i32* %m_traversalMode73, align 4
  %65 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4
  %m_numSubtreeHeaders = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %65, i32 0, i32 10
  %66 = load i32, i32* %m_numSubtreeHeaders, align 4
  store i32 %66, i32* %numElem74, align 4
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %67 = load i32, i32* %numElem74, align 4
  %call76 = call %class.btBvhSubtreeInfo* @_ZN16btBvhSubtreeInfoC2Ev(%class.btBvhSubtreeInfo* %ref.tmp75)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE6resizeEiRKS0_(%class.btAlignedObjectArray.4* %m_SubtreeHeaders, i32 %67, %class.btBvhSubtreeInfo* nonnull align 4 dereferenceable(32) %ref.tmp75)
  %68 = load i32, i32* %numElem74, align 4
  %tobool77 = icmp ne i32 %68, 0
  br i1 %tobool77, label %if.then78, label %if.end130

if.then78:                                        ; preds = %if.end72
  %69 = load %struct.btQuantizedBvhDoubleData*, %struct.btQuantizedBvhDoubleData** %quantizedBvhDoubleData.addr, align 4
  %m_subTreeInfoPtr = getelementptr inbounds %struct.btQuantizedBvhDoubleData, %struct.btQuantizedBvhDoubleData* %69, i32 0, i32 11
  %70 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %m_subTreeInfoPtr, align 8
  store %struct.btBvhSubtreeInfoData* %70, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  store i32 0, i32* %i80, align 4
  br label %for.cond81

for.cond81:                                       ; preds = %for.inc126, %if.then78
  %71 = load i32, i32* %i80, align 4
  %72 = load i32, i32* %numElem74, align 4
  %cmp82 = icmp slt i32 %71, %72
  br i1 %cmp82, label %for.body83, label %for.end129

for.body83:                                       ; preds = %for.cond81
  %73 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_quantizedAabbMax84 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %73, i32 0, i32 3
  %arrayidx85 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax84, i32 0, i32 0
  %74 = load i16, i16* %arrayidx85, align 2
  %m_SubtreeHeaders86 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %75 = load i32, i32* %i80, align 4
  %call87 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders86, i32 %75)
  %m_quantizedAabbMax88 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call87, i32 0, i32 1
  %arrayidx89 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax88, i32 0, i32 0
  store i16 %74, i16* %arrayidx89, align 2
  %76 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_quantizedAabbMax90 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %76, i32 0, i32 3
  %arrayidx91 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax90, i32 0, i32 1
  %77 = load i16, i16* %arrayidx91, align 2
  %m_SubtreeHeaders92 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %78 = load i32, i32* %i80, align 4
  %call93 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders92, i32 %78)
  %m_quantizedAabbMax94 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call93, i32 0, i32 1
  %arrayidx95 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax94, i32 0, i32 1
  store i16 %77, i16* %arrayidx95, align 2
  %79 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_quantizedAabbMax96 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %79, i32 0, i32 3
  %arrayidx97 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax96, i32 0, i32 2
  %80 = load i16, i16* %arrayidx97, align 2
  %m_SubtreeHeaders98 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %81 = load i32, i32* %i80, align 4
  %call99 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders98, i32 %81)
  %m_quantizedAabbMax100 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call99, i32 0, i32 1
  %arrayidx101 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax100, i32 0, i32 2
  store i16 %80, i16* %arrayidx101, align 2
  %82 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_quantizedAabbMin102 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %82, i32 0, i32 2
  %arrayidx103 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin102, i32 0, i32 0
  %83 = load i16, i16* %arrayidx103, align 4
  %m_SubtreeHeaders104 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %84 = load i32, i32* %i80, align 4
  %call105 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders104, i32 %84)
  %m_quantizedAabbMin106 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call105, i32 0, i32 0
  %arrayidx107 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin106, i32 0, i32 0
  store i16 %83, i16* %arrayidx107, align 4
  %85 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_quantizedAabbMin108 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %85, i32 0, i32 2
  %arrayidx109 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin108, i32 0, i32 1
  %86 = load i16, i16* %arrayidx109, align 2
  %m_SubtreeHeaders110 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %87 = load i32, i32* %i80, align 4
  %call111 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders110, i32 %87)
  %m_quantizedAabbMin112 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call111, i32 0, i32 0
  %arrayidx113 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin112, i32 0, i32 1
  store i16 %86, i16* %arrayidx113, align 2
  %88 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_quantizedAabbMin114 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %88, i32 0, i32 2
  %arrayidx115 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin114, i32 0, i32 2
  %89 = load i16, i16* %arrayidx115, align 4
  %m_SubtreeHeaders116 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %90 = load i32, i32* %i80, align 4
  %call117 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders116, i32 %90)
  %m_quantizedAabbMin118 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call117, i32 0, i32 0
  %arrayidx119 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin118, i32 0, i32 2
  store i16 %89, i16* %arrayidx119, align 4
  %91 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_rootNodeIndex = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %91, i32 0, i32 0
  %92 = load i32, i32* %m_rootNodeIndex, align 4
  %m_SubtreeHeaders120 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %93 = load i32, i32* %i80, align 4
  %call121 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders120, i32 %93)
  %m_rootNodeIndex122 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call121, i32 0, i32 2
  store i32 %92, i32* %m_rootNodeIndex122, align 4
  %94 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %m_subtreeSize = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %94, i32 0, i32 1
  %95 = load i32, i32* %m_subtreeSize, align 4
  %m_SubtreeHeaders123 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %96 = load i32, i32* %i80, align 4
  %call124 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders123, i32 %96)
  %m_subtreeSize125 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call124, i32 0, i32 3
  store i32 %95, i32* %m_subtreeSize125, align 4
  br label %for.inc126

for.inc126:                                       ; preds = %for.body83
  %97 = load i32, i32* %i80, align 4
  %inc127 = add nsw i32 %97, 1
  store i32 %inc127, i32* %i80, align 4
  %98 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  %incdec.ptr128 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %98, i32 1
  store %struct.btBvhSubtreeInfoData* %incdec.ptr128, %struct.btBvhSubtreeInfoData** %memPtr79, align 4
  br label %for.cond81

for.end129:                                       ; preds = %for.cond81
  br label %if.end130

if.end130:                                        ; preds = %for.end129, %if.end72
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector317deSerializeDoubleERK19btVector3DoubleData(%class.btVector3* %this, %struct.btVector3DoubleData* nonnull align 8 dereferenceable(32) %dataIn) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataIn.addr = alloca %struct.btVector3DoubleData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3DoubleData* %dataIn, %struct.btVector3DoubleData** %dataIn.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %struct.btVector3DoubleData*, %struct.btVector3DoubleData** %dataIn.addr, align 4
  %m_floats = getelementptr inbounds %struct.btVector3DoubleData, %struct.btVector3DoubleData* %1, i32 0, i32 0
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x double], [4 x double]* %m_floats, i32 0, i32 %2
  %3 = load double, double* %arrayidx, align 8
  %conv = fptrunc double %3 to float
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %conv, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden i8* @_ZNK14btQuantizedBvh9serializeEPvP12btSerializer(%class.btQuantizedBvh* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %quantizedData = alloca %struct.btQuantizedBvhFloatData*, align 4
  %sz = alloca i32, align 4
  %numElem = alloca i32, align 4
  %chunk = alloca %class.btChunk*, align 4
  %memPtr = alloca %struct.btOptimizedBvhNodeFloatData*, align 4
  %i = alloca i32, align 4
  %sz55 = alloca i32, align 4
  %numElem56 = alloca i32, align 4
  %chunk59 = alloca %class.btChunk*, align 4
  %memPtr63 = alloca %struct.btQuantizedBvhNodeData*, align 4
  %i65 = alloca i32, align 4
  %sz131 = alloca i32, align 4
  %numElem132 = alloca i32, align 4
  %chunk135 = alloca %class.btChunk*, align 4
  %memPtr139 = alloca %struct.btBvhSubtreeInfoData*, align 4
  %i141 = alloca i32, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btQuantizedBvhFloatData*
  store %struct.btQuantizedBvhFloatData* %1, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_bvhAabbMax = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 2
  %2 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_bvhAabbMax2 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %2, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_bvhAabbMax, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_bvhAabbMax2)
  %m_bvhAabbMin = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 1
  %3 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_bvhAabbMin3 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %3, i32 0, i32 0
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_bvhAabbMin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_bvhAabbMin3)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 3
  %4 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_bvhQuantization4 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %4, i32 0, i32 2
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_bvhQuantization, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_bvhQuantization4)
  %m_curNodeIndex = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 5
  %5 = load i32, i32* %m_curNodeIndex, align 4
  %6 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_curNodeIndex5 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %6, i32 0, i32 3
  store i32 %5, i32* %m_curNodeIndex5, align 4
  %m_useQuantization = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 6
  %7 = load i8, i8* %m_useQuantization, align 4
  %tobool = trunc i8 %7 to i1
  %conv = zext i1 %tobool to i32
  %8 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_useQuantization6 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %8, i32 0, i32 4
  store i32 %conv, i32* %m_useQuantization6, align 4
  %m_contiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %m_contiguousNodes)
  %9 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_numContiguousLeafNodes = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %9, i32 0, i32 5
  store i32 %call, i32* %m_numContiguousLeafNodes, align 4
  %m_contiguousNodes7 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call8 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %m_contiguousNodes7)
  %tobool9 = icmp ne i32 %call8, 0
  br i1 %tobool9, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %10 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %m_contiguousNodes10 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call11 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes10, i32 0)
  %11 = bitcast %struct.btOptimizedBvhNode* %call11 to i8*
  %12 = bitcast %class.btSerializer* %10 to i8* (%class.btSerializer*, i8*)***
  %vtable = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %12, align 4
  %vfn = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable, i64 7
  %13 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn, align 4
  %call12 = call i8* %13(%class.btSerializer* %10, i8* %11)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i8* [ %call12, %cond.true ], [ null, %cond.false ]
  %14 = bitcast i8* %cond to %struct.btOptimizedBvhNodeFloatData*
  %15 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_contiguousNodesPtr = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %15, i32 0, i32 7
  store %struct.btOptimizedBvhNodeFloatData* %14, %struct.btOptimizedBvhNodeFloatData** %m_contiguousNodesPtr, align 4
  %16 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_contiguousNodesPtr13 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %16, i32 0, i32 7
  %17 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %m_contiguousNodesPtr13, align 4
  %tobool14 = icmp ne %struct.btOptimizedBvhNodeFloatData* %17, null
  br i1 %tobool14, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end
  store i32 48, i32* %sz, align 4
  %m_contiguousNodes15 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call16 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %m_contiguousNodes15)
  store i32 %call16, i32* %numElem, align 4
  %18 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %19 = load i32, i32* %sz, align 4
  %20 = load i32, i32* %numElem, align 4
  %21 = bitcast %class.btSerializer* %18 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable17 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %21, align 4
  %vfn18 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable17, i64 4
  %22 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn18, align 4
  %call19 = call %class.btChunk* %22(%class.btSerializer* %18, i32 %19, i32 %20)
  store %class.btChunk* %call19, %class.btChunk** %chunk, align 4
  %23 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %23, i32 0, i32 2
  %24 = load i8*, i8** %m_oldPtr, align 4
  %25 = bitcast i8* %24 to %struct.btOptimizedBvhNodeFloatData*
  store %struct.btOptimizedBvhNodeFloatData* %25, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %26 = load i32, i32* %i, align 4
  %27 = load i32, i32* %numElem, align 4
  %cmp = icmp slt i32 %26, %27
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_contiguousNodes20 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %28 = load i32, i32* %i, align 4
  %call21 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes20, i32 %28)
  %m_aabbMaxOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call21, i32 0, i32 1
  %29 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  %m_aabbMaxOrg22 = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %29, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_aabbMaxOrg, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_aabbMaxOrg22)
  %m_contiguousNodes23 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %30 = load i32, i32* %i, align 4
  %call24 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes23, i32 %30)
  %m_aabbMinOrg = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call24, i32 0, i32 0
  %31 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  %m_aabbMinOrg25 = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %31, i32 0, i32 0
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_aabbMinOrg, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_aabbMinOrg25)
  %m_contiguousNodes26 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %32 = load i32, i32* %i, align 4
  %call27 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes26, i32 %32)
  %m_escapeIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call27, i32 0, i32 2
  %33 = load i32, i32* %m_escapeIndex, align 4
  %34 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  %m_escapeIndex28 = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %34, i32 0, i32 2
  store i32 %33, i32* %m_escapeIndex28, align 4
  %m_contiguousNodes29 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %35 = load i32, i32* %i, align 4
  %call30 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes29, i32 %35)
  %m_subPart = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call30, i32 0, i32 3
  %36 = load i32, i32* %m_subPart, align 4
  %37 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  %m_subPart31 = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %37, i32 0, i32 3
  store i32 %36, i32* %m_subPart31, align 4
  %m_contiguousNodes32 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %38 = load i32, i32* %i, align 4
  %call33 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes32, i32 %38)
  %m_triangleIndex = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %call33, i32 0, i32 4
  %39 = load i32, i32* %m_triangleIndex, align 4
  %40 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  %m_triangleIndex34 = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %40, i32 0, i32 4
  store i32 %39, i32* %m_triangleIndex34, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %41 = load i32, i32* %i, align 4
  %inc = add nsw i32 %41, 1
  store i32 %inc, i32* %i, align 4
  %42 = load %struct.btOptimizedBvhNodeFloatData*, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  %incdec.ptr = getelementptr inbounds %struct.btOptimizedBvhNodeFloatData, %struct.btOptimizedBvhNodeFloatData* %42, i32 1
  store %struct.btOptimizedBvhNodeFloatData* %incdec.ptr, %struct.btOptimizedBvhNodeFloatData** %memPtr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %43 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %44 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %m_contiguousNodes35 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 9
  %call36 = call nonnull align 4 dereferenceable(64) %struct.btOptimizedBvhNode* @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeEixEi(%class.btAlignedObjectArray* %m_contiguousNodes35, i32 0)
  %45 = bitcast %struct.btOptimizedBvhNode* %call36 to i8*
  %46 = bitcast %class.btSerializer* %43 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable37 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %46, align 4
  %vfn38 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable37, i64 5
  %47 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn38, align 4
  call void %47(%class.btSerializer* %43, %class.btChunk* %44, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str, i32 0, i32 0), i32 1497453121, i8* %45)
  br label %if.end

if.end:                                           ; preds = %for.end, %cond.end
  %m_quantizedContiguousNodes = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call39 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes)
  %48 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_numQuantizedContiguousNodes = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %48, i32 0, i32 6
  store i32 %call39, i32* %m_numQuantizedContiguousNodes, align 4
  %m_quantizedContiguousNodes40 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call41 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes40)
  %tobool42 = icmp ne i32 %call41, 0
  br i1 %tobool42, label %cond.true43, label %cond.false49

cond.true43:                                      ; preds = %if.end
  %49 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %m_quantizedContiguousNodes44 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call45 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes44, i32 0)
  %50 = bitcast %struct.btQuantizedBvhNode* %call45 to i8*
  %51 = bitcast %class.btSerializer* %49 to i8* (%class.btSerializer*, i8*)***
  %vtable46 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %51, align 4
  %vfn47 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable46, i64 7
  %52 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn47, align 4
  %call48 = call i8* %52(%class.btSerializer* %49, i8* %50)
  br label %cond.end50

cond.false49:                                     ; preds = %if.end
  br label %cond.end50

cond.end50:                                       ; preds = %cond.false49, %cond.true43
  %cond51 = phi i8* [ %call48, %cond.true43 ], [ null, %cond.false49 ]
  %53 = bitcast i8* %cond51 to %struct.btQuantizedBvhNodeData*
  %54 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_quantizedContiguousNodesPtr = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %54, i32 0, i32 8
  store %struct.btQuantizedBvhNodeData* %53, %struct.btQuantizedBvhNodeData** %m_quantizedContiguousNodesPtr, align 4
  %55 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_quantizedContiguousNodesPtr52 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %55, i32 0, i32 8
  %56 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %m_quantizedContiguousNodesPtr52, align 4
  %tobool53 = icmp ne %struct.btQuantizedBvhNodeData* %56, null
  br i1 %tobool53, label %if.then54, label %if.end113

if.then54:                                        ; preds = %cond.end50
  store i32 16, i32* %sz55, align 4
  %m_quantizedContiguousNodes57 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call58 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes57)
  store i32 %call58, i32* %numElem56, align 4
  %57 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %58 = load i32, i32* %sz55, align 4
  %59 = load i32, i32* %numElem56, align 4
  %60 = bitcast %class.btSerializer* %57 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable60 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %60, align 4
  %vfn61 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable60, i64 4
  %61 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn61, align 4
  %call62 = call %class.btChunk* %61(%class.btSerializer* %57, i32 %58, i32 %59)
  store %class.btChunk* %call62, %class.btChunk** %chunk59, align 4
  %62 = load %class.btChunk*, %class.btChunk** %chunk59, align 4
  %m_oldPtr64 = getelementptr inbounds %class.btChunk, %class.btChunk* %62, i32 0, i32 2
  %63 = load i8*, i8** %m_oldPtr64, align 4
  %64 = bitcast i8* %63 to %struct.btQuantizedBvhNodeData*
  store %struct.btQuantizedBvhNodeData* %64, %struct.btQuantizedBvhNodeData** %memPtr63, align 4
  store i32 0, i32* %i65, align 4
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc105, %if.then54
  %65 = load i32, i32* %i65, align 4
  %66 = load i32, i32* %numElem56, align 4
  %cmp67 = icmp slt i32 %65, %66
  br i1 %cmp67, label %for.body68, label %for.end108

for.body68:                                       ; preds = %for.cond66
  %m_quantizedContiguousNodes69 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %67 = load i32, i32* %i65, align 4
  %call70 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes69, i32 %67)
  %m_escapeIndexOrTriangleIndex = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call70, i32 0, i32 2
  %68 = load i32, i32* %m_escapeIndexOrTriangleIndex, align 4
  %69 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4
  %m_escapeIndexOrTriangleIndex71 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %69, i32 0, i32 2
  store i32 %68, i32* %m_escapeIndexOrTriangleIndex71, align 4
  %m_quantizedContiguousNodes72 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %70 = load i32, i32* %i65, align 4
  %call73 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes72, i32 %70)
  %m_quantizedAabbMax = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call73, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %71 = load i16, i16* %arrayidx, align 2
  %72 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4
  %m_quantizedAabbMax74 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %72, i32 0, i32 1
  %arrayidx75 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax74, i32 0, i32 0
  store i16 %71, i16* %arrayidx75, align 2
  %m_quantizedContiguousNodes76 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %73 = load i32, i32* %i65, align 4
  %call77 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes76, i32 %73)
  %m_quantizedAabbMax78 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call77, i32 0, i32 1
  %arrayidx79 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax78, i32 0, i32 1
  %74 = load i16, i16* %arrayidx79, align 2
  %75 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4
  %m_quantizedAabbMax80 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %75, i32 0, i32 1
  %arrayidx81 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax80, i32 0, i32 1
  store i16 %74, i16* %arrayidx81, align 2
  %m_quantizedContiguousNodes82 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %76 = load i32, i32* %i65, align 4
  %call83 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes82, i32 %76)
  %m_quantizedAabbMax84 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call83, i32 0, i32 1
  %arrayidx85 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax84, i32 0, i32 2
  %77 = load i16, i16* %arrayidx85, align 2
  %78 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4
  %m_quantizedAabbMax86 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %78, i32 0, i32 1
  %arrayidx87 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax86, i32 0, i32 2
  store i16 %77, i16* %arrayidx87, align 2
  %m_quantizedContiguousNodes88 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %79 = load i32, i32* %i65, align 4
  %call89 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes88, i32 %79)
  %m_quantizedAabbMin = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call89, i32 0, i32 0
  %arrayidx90 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %80 = load i16, i16* %arrayidx90, align 4
  %81 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4
  %m_quantizedAabbMin91 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %81, i32 0, i32 0
  %arrayidx92 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin91, i32 0, i32 0
  store i16 %80, i16* %arrayidx92, align 4
  %m_quantizedContiguousNodes93 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %82 = load i32, i32* %i65, align 4
  %call94 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes93, i32 %82)
  %m_quantizedAabbMin95 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call94, i32 0, i32 0
  %arrayidx96 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin95, i32 0, i32 1
  %83 = load i16, i16* %arrayidx96, align 2
  %84 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4
  %m_quantizedAabbMin97 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %84, i32 0, i32 0
  %arrayidx98 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin97, i32 0, i32 1
  store i16 %83, i16* %arrayidx98, align 2
  %m_quantizedContiguousNodes99 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %85 = load i32, i32* %i65, align 4
  %call100 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes99, i32 %85)
  %m_quantizedAabbMin101 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %call100, i32 0, i32 0
  %arrayidx102 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin101, i32 0, i32 2
  %86 = load i16, i16* %arrayidx102, align 4
  %87 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4
  %m_quantizedAabbMin103 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %87, i32 0, i32 0
  %arrayidx104 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin103, i32 0, i32 2
  store i16 %86, i16* %arrayidx104, align 4
  br label %for.inc105

for.inc105:                                       ; preds = %for.body68
  %88 = load i32, i32* %i65, align 4
  %inc106 = add nsw i32 %88, 1
  store i32 %inc106, i32* %i65, align 4
  %89 = load %struct.btQuantizedBvhNodeData*, %struct.btQuantizedBvhNodeData** %memPtr63, align 4
  %incdec.ptr107 = getelementptr inbounds %struct.btQuantizedBvhNodeData, %struct.btQuantizedBvhNodeData* %89, i32 1
  store %struct.btQuantizedBvhNodeData* %incdec.ptr107, %struct.btQuantizedBvhNodeData** %memPtr63, align 4
  br label %for.cond66

for.end108:                                       ; preds = %for.cond66
  %90 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %91 = load %class.btChunk*, %class.btChunk** %chunk59, align 4
  %m_quantizedContiguousNodes109 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 11
  %call110 = call nonnull align 4 dereferenceable(16) %struct.btQuantizedBvhNode* @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeEixEi(%class.btAlignedObjectArray.0* %m_quantizedContiguousNodes109, i32 0)
  %92 = bitcast %struct.btQuantizedBvhNode* %call110 to i8*
  %93 = bitcast %class.btSerializer* %90 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable111 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %93, align 4
  %vfn112 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable111, i64 5
  %94 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn112, align 4
  call void %94(%class.btSerializer* %90, %class.btChunk* %91, i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str.1, i32 0, i32 0), i32 1497453121, i8* %92)
  br label %if.end113

if.end113:                                        ; preds = %for.end108, %cond.end50
  %m_traversalMode = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 12
  %95 = load i32, i32* %m_traversalMode, align 4
  %96 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_traversalMode114 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %96, i32 0, i32 10
  store i32 %95, i32* %m_traversalMode114, align 4
  %m_SubtreeHeaders = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call115 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders)
  %97 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_numSubtreeHeaders = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %97, i32 0, i32 11
  store i32 %call115, i32* %m_numSubtreeHeaders, align 4
  %m_SubtreeHeaders116 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call117 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders116)
  %tobool118 = icmp ne i32 %call117, 0
  br i1 %tobool118, label %cond.true119, label %cond.false125

cond.true119:                                     ; preds = %if.end113
  %98 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %m_SubtreeHeaders120 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call121 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders120, i32 0)
  %99 = bitcast %class.btBvhSubtreeInfo* %call121 to i8*
  %100 = bitcast %class.btSerializer* %98 to i8* (%class.btSerializer*, i8*)***
  %vtable122 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %100, align 4
  %vfn123 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable122, i64 7
  %101 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn123, align 4
  %call124 = call i8* %101(%class.btSerializer* %98, i8* %99)
  br label %cond.end126

cond.false125:                                    ; preds = %if.end113
  br label %cond.end126

cond.end126:                                      ; preds = %cond.false125, %cond.true119
  %cond127 = phi i8* [ %call124, %cond.true119 ], [ null, %cond.false125 ]
  %102 = bitcast i8* %cond127 to %struct.btBvhSubtreeInfoData*
  %103 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_subTreeInfoPtr = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %103, i32 0, i32 9
  store %struct.btBvhSubtreeInfoData* %102, %struct.btBvhSubtreeInfoData** %m_subTreeInfoPtr, align 4
  %104 = load %struct.btQuantizedBvhFloatData*, %struct.btQuantizedBvhFloatData** %quantizedData, align 4
  %m_subTreeInfoPtr128 = getelementptr inbounds %struct.btQuantizedBvhFloatData, %struct.btQuantizedBvhFloatData* %104, i32 0, i32 9
  %105 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %m_subTreeInfoPtr128, align 4
  %tobool129 = icmp ne %struct.btBvhSubtreeInfoData* %105, null
  br i1 %tobool129, label %if.then130, label %if.end195

if.then130:                                       ; preds = %cond.end126
  store i32 20, i32* %sz131, align 4
  %m_SubtreeHeaders133 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call134 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %m_SubtreeHeaders133)
  store i32 %call134, i32* %numElem132, align 4
  %106 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %107 = load i32, i32* %sz131, align 4
  %108 = load i32, i32* %numElem132, align 4
  %109 = bitcast %class.btSerializer* %106 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable136 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %109, align 4
  %vfn137 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable136, i64 4
  %110 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn137, align 4
  %call138 = call %class.btChunk* %110(%class.btSerializer* %106, i32 %107, i32 %108)
  store %class.btChunk* %call138, %class.btChunk** %chunk135, align 4
  %111 = load %class.btChunk*, %class.btChunk** %chunk135, align 4
  %m_oldPtr140 = getelementptr inbounds %class.btChunk, %class.btChunk* %111, i32 0, i32 2
  %112 = load i8*, i8** %m_oldPtr140, align 4
  %113 = bitcast i8* %112 to %struct.btBvhSubtreeInfoData*
  store %struct.btBvhSubtreeInfoData* %113, %struct.btBvhSubtreeInfoData** %memPtr139, align 4
  store i32 0, i32* %i141, align 4
  br label %for.cond142

for.cond142:                                      ; preds = %for.inc187, %if.then130
  %114 = load i32, i32* %i141, align 4
  %115 = load i32, i32* %numElem132, align 4
  %cmp143 = icmp slt i32 %114, %115
  br i1 %cmp143, label %for.body144, label %for.end190

for.body144:                                      ; preds = %for.cond142
  %m_SubtreeHeaders145 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %116 = load i32, i32* %i141, align 4
  %call146 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders145, i32 %116)
  %m_quantizedAabbMax147 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call146, i32 0, i32 1
  %arrayidx148 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax147, i32 0, i32 0
  %117 = load i16, i16* %arrayidx148, align 2
  %118 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr139, align 4
  %m_quantizedAabbMax149 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %118, i32 0, i32 3
  %arrayidx150 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax149, i32 0, i32 0
  store i16 %117, i16* %arrayidx150, align 2
  %m_SubtreeHeaders151 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %119 = load i32, i32* %i141, align 4
  %call152 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders151, i32 %119)
  %m_quantizedAabbMax153 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call152, i32 0, i32 1
  %arrayidx154 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax153, i32 0, i32 1
  %120 = load i16, i16* %arrayidx154, align 2
  %121 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr139, align 4
  %m_quantizedAabbMax155 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %121, i32 0, i32 3
  %arrayidx156 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax155, i32 0, i32 1
  store i16 %120, i16* %arrayidx156, align 2
  %m_SubtreeHeaders157 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %122 = load i32, i32* %i141, align 4
  %call158 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders157, i32 %122)
  %m_quantizedAabbMax159 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call158, i32 0, i32 1
  %arrayidx160 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax159, i32 0, i32 2
  %123 = load i16, i16* %arrayidx160, align 2
  %124 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr139, align 4
  %m_quantizedAabbMax161 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %124, i32 0, i32 3
  %arrayidx162 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax161, i32 0, i32 2
  store i16 %123, i16* %arrayidx162, align 2
  %m_SubtreeHeaders163 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %125 = load i32, i32* %i141, align 4
  %call164 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders163, i32 %125)
  %m_quantizedAabbMin165 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call164, i32 0, i32 0
  %arrayidx166 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin165, i32 0, i32 0
  %126 = load i16, i16* %arrayidx166, align 4
  %127 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr139, align 4
  %m_quantizedAabbMin167 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %127, i32 0, i32 2
  %arrayidx168 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin167, i32 0, i32 0
  store i16 %126, i16* %arrayidx168, align 4
  %m_SubtreeHeaders169 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %128 = load i32, i32* %i141, align 4
  %call170 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders169, i32 %128)
  %m_quantizedAabbMin171 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call170, i32 0, i32 0
  %arrayidx172 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin171, i32 0, i32 1
  %129 = load i16, i16* %arrayidx172, align 2
  %130 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr139, align 4
  %m_quantizedAabbMin173 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %130, i32 0, i32 2
  %arrayidx174 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin173, i32 0, i32 1
  store i16 %129, i16* %arrayidx174, align 2
  %m_SubtreeHeaders175 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %131 = load i32, i32* %i141, align 4
  %call176 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders175, i32 %131)
  %m_quantizedAabbMin177 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call176, i32 0, i32 0
  %arrayidx178 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin177, i32 0, i32 2
  %132 = load i16, i16* %arrayidx178, align 4
  %133 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr139, align 4
  %m_quantizedAabbMin179 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %133, i32 0, i32 2
  %arrayidx180 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin179, i32 0, i32 2
  store i16 %132, i16* %arrayidx180, align 4
  %m_SubtreeHeaders181 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %134 = load i32, i32* %i141, align 4
  %call182 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders181, i32 %134)
  %m_rootNodeIndex = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call182, i32 0, i32 2
  %135 = load i32, i32* %m_rootNodeIndex, align 4
  %136 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr139, align 4
  %m_rootNodeIndex183 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %136, i32 0, i32 0
  store i32 %135, i32* %m_rootNodeIndex183, align 4
  %m_SubtreeHeaders184 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %137 = load i32, i32* %i141, align 4
  %call185 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders184, i32 %137)
  %m_subtreeSize = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %call185, i32 0, i32 3
  %138 = load i32, i32* %m_subtreeSize, align 4
  %139 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr139, align 4
  %m_subtreeSize186 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %139, i32 0, i32 1
  store i32 %138, i32* %m_subtreeSize186, align 4
  br label %for.inc187

for.inc187:                                       ; preds = %for.body144
  %140 = load i32, i32* %i141, align 4
  %inc188 = add nsw i32 %140, 1
  store i32 %inc188, i32* %i141, align 4
  %141 = load %struct.btBvhSubtreeInfoData*, %struct.btBvhSubtreeInfoData** %memPtr139, align 4
  %incdec.ptr189 = getelementptr inbounds %struct.btBvhSubtreeInfoData, %struct.btBvhSubtreeInfoData* %141, i32 1
  store %struct.btBvhSubtreeInfoData* %incdec.ptr189, %struct.btBvhSubtreeInfoData** %memPtr139, align 4
  br label %for.cond142

for.end190:                                       ; preds = %for.cond142
  %142 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %143 = load %class.btChunk*, %class.btChunk** %chunk135, align 4
  %m_SubtreeHeaders191 = getelementptr inbounds %class.btQuantizedBvh, %class.btQuantizedBvh* %this1, i32 0, i32 13
  %call192 = call nonnull align 4 dereferenceable(32) %class.btBvhSubtreeInfo* @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoEixEi(%class.btAlignedObjectArray.4* %m_SubtreeHeaders191, i32 0)
  %144 = bitcast %class.btBvhSubtreeInfo* %call192 to i8*
  %145 = bitcast %class.btSerializer* %142 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable193 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %145, align 4
  %vfn194 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable193, i64 5
  %146 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn194, align 4
  call void %146(%class.btSerializer* %142, %class.btChunk* %143, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i32 0, i32 0), i32 1497453121, i8* %144)
  br label %if.end195

if.end195:                                        ; preds = %for.end190, %cond.end126
  ret i8* getelementptr inbounds ([24 x i8], [24 x i8]* @.str.3, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK14btQuantizedBvh31calculateSerializeBufferSizeNewEv(%class.btQuantizedBvh* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvh*, align 4
  store %class.btQuantizedBvh* %this, %class.btQuantizedBvh** %this.addr, align 4
  %this1 = load %class.btQuantizedBvh*, %class.btQuantizedBvh** %this.addr, align 4
  ret i32 84
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %b.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %a.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_Z8btSelectjii(i32 %condition, i32 %valueIfConditionNonZero, i32 %valueIfConditionZero) #1 comdat {
entry:
  %condition.addr = alloca i32, align 4
  %valueIfConditionNonZero.addr = alloca i32, align 4
  %valueIfConditionZero.addr = alloca i32, align 4
  %testNz = alloca i32, align 4
  %testEqz = alloca i32, align 4
  store i32 %condition, i32* %condition.addr, align 4
  store i32 %valueIfConditionNonZero, i32* %valueIfConditionNonZero.addr, align 4
  store i32 %valueIfConditionZero, i32* %valueIfConditionZero.addr, align 4
  %0 = load i32, i32* %condition.addr, align 4
  %1 = load i32, i32* %condition.addr, align 4
  %sub = sub nsw i32 0, %1
  %or = or i32 %0, %sub
  %shr = ashr i32 %or, 31
  store i32 %shr, i32* %testNz, align 4
  %2 = load i32, i32* %testNz, align 4
  %neg = xor i32 %2, -1
  store i32 %neg, i32* %testEqz, align 4
  %3 = load i32, i32* %valueIfConditionNonZero.addr, align 4
  %4 = load i32, i32* %testNz, align 4
  %and = and i32 %3, %4
  %5 = load i32, i32* %valueIfConditionZero.addr, align 4
  %6 = load i32, i32* %testEqz, align 4
  %and1 = and i32 %5, %6
  %or2 = or i32 %and, %and1
  ret i32 %or2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_Z12btSwapEndianj(i32 %val) #1 comdat {
entry:
  %val.addr = alloca i32, align 4
  store i32 %val, i32* %val.addr, align 4
  %0 = load i32, i32* %val.addr, align 4
  %and = and i32 %0, -16777216
  %shr = lshr i32 %and, 24
  %1 = load i32, i32* %val.addr, align 4
  %and1 = and i32 %1, 16711680
  %shr2 = lshr i32 %and1, 8
  %or = or i32 %shr, %shr2
  %2 = load i32, i32* %val.addr, align 4
  %and3 = and i32 %2, 65280
  %shl = shl i32 %and3, 8
  %or4 = or i32 %or, %shl
  %3 = load i32, i32* %val.addr, align 4
  %and5 = and i32 %3, 255
  %shl6 = shl i32 %and5, 24
  %or7 = or i32 %or4, %shl6
  ret i32 %or7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z18btSwapScalarEndianRKfRf(float* nonnull align 4 dereferenceable(4) %sourceVal, float* nonnull align 4 dereferenceable(4) %destVal) #1 comdat {
entry:
  %sourceVal.addr = alloca float*, align 4
  %destVal.addr = alloca float*, align 4
  %dest = alloca i8*, align 4
  %src = alloca i8*, align 4
  store float* %sourceVal, float** %sourceVal.addr, align 4
  store float* %destVal, float** %destVal.addr, align 4
  %0 = load float*, float** %destVal.addr, align 4
  %1 = bitcast float* %0 to i8*
  store i8* %1, i8** %dest, align 4
  %2 = load float*, float** %sourceVal.addr, align 4
  %3 = bitcast float* %2 to i8*
  store i8* %3, i8** %src, align 4
  %4 = load i8*, i8** %src, align 4
  %arrayidx = getelementptr inbounds i8, i8* %4, i32 3
  %5 = load i8, i8* %arrayidx, align 1
  %6 = load i8*, i8** %dest, align 4
  %arrayidx1 = getelementptr inbounds i8, i8* %6, i32 0
  store i8 %5, i8* %arrayidx1, align 1
  %7 = load i8*, i8** %src, align 4
  %arrayidx2 = getelementptr inbounds i8, i8* %7, i32 2
  %8 = load i8, i8* %arrayidx2, align 1
  %9 = load i8*, i8** %dest, align 4
  %arrayidx3 = getelementptr inbounds i8, i8* %9, i32 1
  store i8 %8, i8* %arrayidx3, align 1
  %10 = load i8*, i8** %src, align 4
  %arrayidx4 = getelementptr inbounds i8, i8* %10, i32 1
  %11 = load i8, i8* %arrayidx4, align 1
  %12 = load i8*, i8** %dest, align 4
  %arrayidx5 = getelementptr inbounds i8, i8* %12, i32 2
  store i8 %11, i8* %arrayidx5, align 1
  %13 = load i8*, i8** %src, align 4
  %arrayidx6 = getelementptr inbounds i8, i8* %13, i32 0
  %14 = load i8, i8* %arrayidx6, align 1
  %15 = load i8*, i8** %dest, align 4
  %arrayidx7 = getelementptr inbounds i8, i8* %15, i32 3
  store i8 %14, i8* %arrayidx7, align 1
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btOptimizedBvhNode* null, %struct.btOptimizedBvhNode** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* null, %struct.btQuantizedBvhNode** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE4initEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btBvhSubtreeInfo* null, %class.btBvhSubtreeInfo** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE5clearEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4
  %tobool = icmp ne %class.btBvhSubtreeInfo* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.5* %m_allocator, %class.btBvhSubtreeInfo* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btBvhSubtreeInfo* null, %class.btBvhSubtreeInfo** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.5* %this, %class.btBvhSubtreeInfo* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca %class.btBvhSubtreeInfo*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store %class.btBvhSubtreeInfo* %ptr, %class.btBvhSubtreeInfo** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %ptr.addr, align 4
  %1 = bitcast %class.btBvhSubtreeInfo* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btQuantizedBvhNode*
  store %struct.btQuantizedBvhNode* %2, %struct.btQuantizedBvhNode** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %3 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %struct.btQuantizedBvhNode* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* %4, %struct.btQuantizedBvhNode** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btQuantizedBvhNode* @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %struct.btQuantizedBvhNode** null)
  %2 = bitcast %struct.btQuantizedBvhNode* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI18btQuantizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %struct.btQuantizedBvhNode* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btQuantizedBvhNode*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btQuantizedBvhNode* %dest, %struct.btQuantizedBvhNode** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %3, i32 %4
  %5 = bitcast %struct.btQuantizedBvhNode* %arrayidx to i8*
  %call = call i8* @_ZN18btQuantizedBvhNodenwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %struct.btQuantizedBvhNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %7, i32 %8
  %9 = bitcast %struct.btQuantizedBvhNode* %6 to i8*
  %10 = bitcast %struct.btQuantizedBvhNode* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btQuantizedBvhNode, %struct.btQuantizedBvhNode* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btQuantizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data, align 4
  %tobool = icmp ne %struct.btQuantizedBvhNode* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.btQuantizedBvhNode* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btQuantizedBvhNode* null, %struct.btQuantizedBvhNode** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btQuantizedBvhNode* @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, %struct.btQuantizedBvhNode** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btQuantizedBvhNode**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btQuantizedBvhNode** %hint, %struct.btQuantizedBvhNode*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btQuantizedBvhNode*
  ret %struct.btQuantizedBvhNode* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI18btQuantizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.btQuantizedBvhNode* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.btQuantizedBvhNode*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %struct.btQuantizedBvhNode* %ptr, %struct.btQuantizedBvhNode** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.btQuantizedBvhNode*, %struct.btQuantizedBvhNode** %ptr.addr, align 4
  %1 = bitcast %struct.btQuantizedBvhNode* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btBvhSubtreeInfo*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btBvhSubtreeInfo*
  store %class.btBvhSubtreeInfo* %2, %class.btBvhSubtreeInfo** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %3 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, %class.btBvhSubtreeInfo* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btBvhSubtreeInfo* %4, %class.btBvhSubtreeInfo** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE9allocSizeEi(%class.btAlignedObjectArray.4* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN16btBvhSubtreeInfonwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI16btBvhSubtreeInfoE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btBvhSubtreeInfo* @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.5* %m_allocator, i32 %1, %class.btBvhSubtreeInfo** null)
  %2 = bitcast %class.btBvhSubtreeInfo* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI16btBvhSubtreeInfoE4copyEiiPS0_(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, %class.btBvhSubtreeInfo* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btBvhSubtreeInfo*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btBvhSubtreeInfo* %dest, %class.btBvhSubtreeInfo** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %3, i32 %4
  %5 = bitcast %class.btBvhSubtreeInfo* %arrayidx to i8*
  %call = call i8* @_ZN16btBvhSubtreeInfonwEmPv(i32 32, i8* %5)
  %6 = bitcast i8* %call to %class.btBvhSubtreeInfo*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %7 = load %class.btBvhSubtreeInfo*, %class.btBvhSubtreeInfo** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btBvhSubtreeInfo, %class.btBvhSubtreeInfo* %7, i32 %8
  %9 = bitcast %class.btBvhSubtreeInfo* %6 to i8*
  %10 = bitcast %class.btBvhSubtreeInfo* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 32, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btBvhSubtreeInfo* @_ZN18btAlignedAllocatorI16btBvhSubtreeInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.5* %this, i32 %n, %class.btBvhSubtreeInfo** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btBvhSubtreeInfo**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btBvhSubtreeInfo** %hint, %class.btBvhSubtreeInfo*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 32, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btBvhSubtreeInfo*
  ret %class.btBvhSubtreeInfo* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4
  %tobool = icmp ne %struct.btOptimizedBvhNode* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btOptimizedBvhNode* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btOptimizedBvhNode* null, %struct.btOptimizedBvhNode** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btOptimizedBvhNode* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btOptimizedBvhNode*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %struct.btOptimizedBvhNode* %ptr, %struct.btOptimizedBvhNode** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %ptr.addr, align 4
  %1 = bitcast %struct.btOptimizedBvhNode* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btOptimizedBvhNode*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btOptimizedBvhNode*
  store %struct.btOptimizedBvhNode* %2, %struct.btOptimizedBvhNode** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btOptimizedBvhNode* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btOptimizedBvhNode* %4, %struct.btOptimizedBvhNode** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN18btOptimizedBvhNodenwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI18btOptimizedBvhNodeE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btOptimizedBvhNode* @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btOptimizedBvhNode** null)
  %2 = bitcast %struct.btOptimizedBvhNode* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI18btOptimizedBvhNodeE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btOptimizedBvhNode* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btOptimizedBvhNode*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btOptimizedBvhNode* %dest, %struct.btOptimizedBvhNode** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %3, i32 %4
  %5 = bitcast %struct.btOptimizedBvhNode* %arrayidx to i8*
  %call = call i8* @_ZN18btOptimizedBvhNodenwEmPv(i32 64, i8* %5)
  %6 = bitcast i8* %call to %struct.btOptimizedBvhNode*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %struct.btOptimizedBvhNode*, %struct.btOptimizedBvhNode** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btOptimizedBvhNode, %struct.btOptimizedBvhNode* %7, i32 %8
  %9 = bitcast %struct.btOptimizedBvhNode* %6 to i8*
  %10 = bitcast %struct.btOptimizedBvhNode* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 64, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btOptimizedBvhNode* @_ZN18btAlignedAllocatorI18btOptimizedBvhNodeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btOptimizedBvhNode** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btOptimizedBvhNode**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btOptimizedBvhNode** %hint, %struct.btOptimizedBvhNode*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 64, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btOptimizedBvhNode*
  ret %struct.btOptimizedBvhNode* %1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btQuantizedBvh.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn writeonly }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
