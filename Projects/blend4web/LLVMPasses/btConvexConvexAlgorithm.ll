; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btConvexConvexAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btConvexConvexAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%"struct.btConvexConvexAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, %class.btConvexPenetrationDepthSolver*, i32, i32 }
%struct.btCollisionAlgorithmCreateFunc.base = type <{ i32 (...)**, i8 }>
%class.btConvexPenetrationDepthSolver = type { i32 (...)** }
%class.btConvexConvexAlgorithm = type { %class.btActivatingCollisionAlgorithm, %class.btConvexPenetrationDepthSolver*, %class.btAlignedObjectArray, %class.btAlignedObjectArray, i8, %class.btPersistentManifold*, i8, i32, i32 }
%class.btActivatingCollisionAlgorithm = type { %class.btCollisionAlgorithm }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.0, %union.anon.1, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.0 = type { float }
%union.anon.1 = type { float }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.2, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btAlignedObjectArray.2 = type <{ %class.btAlignedAllocator.3, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.3 = type { i8 }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%struct.btCollisionAlgorithmCreateFunc = type <{ i32 (...)**, i8, [3 x i8] }>
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32, float }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCapsuleShape = type { %class.btConvexInternalShape, i32 }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btSphereShape = type { %class.btConvexInternalShape }
%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput" = type { %class.btTransform, %class.btTransform, float }
%class.btVoronoiSimplexSolver = type <{ i32, [5 x %class.btVector3], [5 x %class.btVector3], [5 x %class.btVector3], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, i8, [3 x i8], %struct.btSubSimplexClosestResult, i8, [3 x i8] }>
%struct.btSubSimplexClosestResult = type <{ %class.btVector3, %struct.btUsageBitfield, [2 x i8], [4 x float], i8, [3 x i8] }>
%struct.btUsageBitfield = type { i8, i8 }
%class.btGjkPairDetector = type { %struct.btDiscreteCollisionDetectorInterface, %class.btVector3, %class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, i32, i32, float, float, i8, float, i32, i32, i32, i32, i32 }
%struct.btDiscreteCollisionDetectorInterface = type { i32 (...)** }
%struct.btDummyResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result" }
%struct.btWithoutMarginResult = type <{ %"struct.btDiscreteCollisionDetectorInterface::Result", %"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3, float, float, float, i8, [3 x i8] }>
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexPolyhedron = type opaque
%class.btTriangleShape = type { %class.btPolyhedralConvexShape, [3 x %class.btVector3] }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%struct.btPerturbedContactResult = type { %class.btManifoldResult, %class.btManifoldResult*, %class.btTransform, %class.btTransform, %class.btTransform, i8, %class.btIDebugDraw* }
%"struct.btConvexCast::CastResult" = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, float, %class.btIDebugDraw*, float }
%class.btGjkConvexCast = type { %class.btConvexCast, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape* }
%class.btConvexCast = type { i32 (...)** }
%class.btAlignedObjectArray.6 = type <{ %class.btAlignedAllocator.7, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.7 = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN30btCollisionAlgorithmCreateFuncC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold = comdat any

$_ZNK24btCollisionObjectWrapper17getCollisionShapeEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZNK14btCapsuleShape13getHalfHeightEv = comdat any

$_ZNK14btCapsuleShape9getRadiusEv = comdat any

$_ZNK14btCapsuleShape9getUpAxisEv = comdat any

$_ZNK24btCollisionObjectWrapper17getWorldTransformEv = comdat any

$_ZN16btManifoldResult20refreshContactPointsEv = comdat any

$_ZNK13btSphereShape9getRadiusEv = comdat any

$_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev = comdat any

$_ZN22btVoronoiSimplexSolverC2Ev = comdat any

$_ZN17btGjkPairDetector13setMinkowskiAEPK13btConvexShape = comdat any

$_ZN17btGjkPairDetector13setMinkowskiBEPK13btConvexShape = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZNK16btCollisionShape12isPolyhedralEv = comdat any

$_ZNK23btPolyhedralConvexShape19getConvexPolyhedronEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_ = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZNK17btGjkPairDetector23getCachedSeparatingAxisEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK17btGjkPairDetector27getCachedSeparatingDistanceEv = comdat any

$_ZN16btManifoldResult21getPersistentManifoldEv = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_ = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN12btQuaternionC2ERK9btVector3RKf = comdat any

$_ZN11btTransform8setBasisERK11btMatrix3x3 = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZmlRK12btQuaternionS1_ = comdat any

$_ZNK12btQuaternion7inverseEv = comdat any

$_ZN11btMatrix3x3C2ERK12btQuaternion = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZN24btPerturbedContactResultC2EP16btManifoldResultRK11btTransformS4_S4_bP12btIDebugDraw = comdat any

$_ZN24btPerturbedContactResultD2Ev = comdat any

$_ZN17btGjkPairDetectorD2Ev = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN17btCollisionObject30getInterpolationWorldTransformEv = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZNK17btCollisionObject27getCcdSquareMotionThresholdEv = comdat any

$_ZN17btCollisionObject17getCollisionShapeEv = comdat any

$_ZNK17btCollisionObject23getCcdSweptSphereRadiusEv = comdat any

$_ZN13btSphereShapeC2Ef = comdat any

$_ZN12btConvexCast10CastResultC2Ev = comdat any

$_ZNK17btCollisionObject14getHitFractionEv = comdat any

$_ZN17btCollisionObject14setHitFractionEf = comdat any

$_ZN15btGjkConvexCastD2Ev = comdat any

$_ZN12btConvexCast10CastResultD2Ev = comdat any

$_ZN13btSphereShapeD2Ev = comdat any

$_ZN23btConvexConvexAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN23btConvexConvexAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE = comdat any

$_ZN30btCollisionAlgorithmCreateFuncD2Ev = comdat any

$_ZN30btCollisionAlgorithmCreateFuncD0Ev = comdat any

$_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_ = comdat any

$_ZNK11btMatrix3x39getColumnEi = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_Z6btSqrtf = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_Z5btDotRK9btVector3S1_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZN25btSubSimplexClosestResultC2Ev = comdat any

$_ZN15btUsageBitfieldC2Ev = comdat any

$_ZN15btUsageBitfield5resetEv = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN17btBroadphaseProxy12isPolyhedralEi = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZN12btQuaternion11setRotationERK9btVector3RKf = comdat any

$_Z5btSinf = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_Z5btCosf = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN16btManifoldResultC2Ev = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN24btPerturbedContactResultD0Ev = comdat any

$_ZN16btManifoldResult20setShapeIdentifiersAEii = comdat any

$_ZN16btManifoldResult20setShapeIdentifiersBEii = comdat any

$_ZN24btPerturbedContactResult15addContactPointERK9btVector3S2_f = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN16btManifoldResultD2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD2Ev = comdat any

$_ZN9btVector34setXEf = comdat any

$_ZN12btConvexCast10CastResult9DebugDrawEf = comdat any

$_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform = comdat any

$_ZN12btConvexCast10CastResult13reportFailureEii = comdat any

$_ZN12btConvexCast10CastResultD0Ev = comdat any

$_ZN21btConvexInternalShapeD2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_Z6btFabsf = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi = comdat any

$_ZTS30btCollisionAlgorithmCreateFunc = comdat any

$_ZTI30btCollisionAlgorithmCreateFunc = comdat any

$_ZTV30btCollisionAlgorithmCreateFunc = comdat any

$_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTV24btPerturbedContactResult = comdat any

$_ZTS24btPerturbedContactResult = comdat any

$_ZTI24btPerturbedContactResult = comdat any

$_ZTVN12btConvexCast10CastResultE = comdat any

$_ZTSN12btConvexCast10CastResultE = comdat any

$_ZTIN12btConvexCast10CastResultE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTVN23btConvexConvexAlgorithm10CreateFuncE = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN23btConvexConvexAlgorithm10CreateFuncE to i8*), i8* bitcast (%"struct.btConvexConvexAlgorithm::CreateFunc"* (%"struct.btConvexConvexAlgorithm::CreateFunc"*)* @_ZN23btConvexConvexAlgorithm10CreateFuncD1Ev to i8*), i8* bitcast (void (%"struct.btConvexConvexAlgorithm::CreateFunc"*)* @_ZN23btConvexConvexAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btConvexConvexAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN23btConvexConvexAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, align 4
@_ZTV23btConvexConvexAlgorithm = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btConvexConvexAlgorithm to i8*), i8* bitcast (%class.btConvexConvexAlgorithm* (%class.btConvexConvexAlgorithm*)* @_ZN23btConvexConvexAlgorithmD1Ev to i8*), i8* bitcast (void (%class.btConvexConvexAlgorithm*)* @_ZN23btConvexConvexAlgorithmD0Ev to i8*), i8* bitcast (void (%class.btConvexConvexAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (float (%class.btConvexConvexAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN23btConvexConvexAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (void (%class.btConvexConvexAlgorithm*, %class.btAlignedObjectArray.6*)* @_ZN23btConvexConvexAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE to i8*)] }, align 4
@gContactBreakingThreshold = external global float, align 4
@disableCcd = hidden global i8 0, align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTSN23btConvexConvexAlgorithm10CreateFuncE = hidden constant [40 x i8] c"N23btConvexConvexAlgorithm10CreateFuncE\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS30btCollisionAlgorithmCreateFunc = linkonce_odr hidden constant [33 x i8] c"30btCollisionAlgorithmCreateFunc\00", comdat, align 1
@_ZTI30btCollisionAlgorithmCreateFunc = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btCollisionAlgorithmCreateFunc, i32 0, i32 0) }, comdat, align 4
@_ZTIN23btConvexConvexAlgorithm10CreateFuncE = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([40 x i8], [40 x i8]* @_ZTSN23btConvexConvexAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, align 4
@_ZTS23btConvexConvexAlgorithm = hidden constant [26 x i8] c"23btConvexConvexAlgorithm\00", align 1
@_ZTI30btActivatingCollisionAlgorithm = external constant i8*
@_ZTI23btConvexConvexAlgorithm = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btConvexConvexAlgorithm, i32 0, i32 0), i8* bitcast (i8** @_ZTI30btActivatingCollisionAlgorithm to i8*) }, align 4
@_ZTV30btCollisionAlgorithmCreateFunc = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_ to i8*)] }, comdat, align 4
@_ZTVZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultE13btDummyResult = internal unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultE13btDummyResult to i8*), i8* bitcast (%struct.btDummyResult* (%struct.btDummyResult*)* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN13btDummyResultD2Ev to i8*), i8* bitcast (void (%struct.btDummyResult*)* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN13btDummyResultD0Ev to i8*), i8* bitcast (void (%struct.btDummyResult*, i32, i32)* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN13btDummyResult20setShapeIdentifiersAEii to i8*), i8* bitcast (void (%struct.btDummyResult*, i32, i32)* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN13btDummyResult20setShapeIdentifiersBEii to i8*), i8* bitcast (void (%struct.btDummyResult*, %class.btVector3*, %class.btVector3*, float)* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN13btDummyResult15addContactPointERK9btVector3SB_f to i8*)] }, align 4
@_ZTSZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultE13btDummyResult = internal constant [133 x i8] c"ZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultE13btDummyResult\00", align 1
@_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant [48 x i8] c"N36btDiscreteCollisionDetectorInterface6ResultE\00", comdat, align 1
@_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([48 x i8], [48 x i8]* @_ZTSN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, i32 0) }, comdat, align 4
@_ZTIZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultE13btDummyResult = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([133 x i8], [133 x i8]* @_ZTSZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultE13btDummyResult, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*) }, align 4
@_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*), i8* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to i8*), i8* bitcast (void (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultE21btWithoutMarginResult = internal unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultE21btWithoutMarginResult to i8*), i8* bitcast (%struct.btWithoutMarginResult* (%struct.btWithoutMarginResult*)* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN21btWithoutMarginResultD2Ev to i8*), i8* bitcast (void (%struct.btWithoutMarginResult*)* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN21btWithoutMarginResultD0Ev to i8*), i8* bitcast (void (%struct.btWithoutMarginResult*, i32, i32)* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN21btWithoutMarginResult20setShapeIdentifiersAEii to i8*), i8* bitcast (void (%struct.btWithoutMarginResult*, i32, i32)* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN21btWithoutMarginResult20setShapeIdentifiersBEii to i8*), i8* bitcast (void (%struct.btWithoutMarginResult*, %class.btVector3*, %class.btVector3*, float)* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN21btWithoutMarginResult15addContactPointERK9btVector3SB_f to i8*)] }, align 4
@_ZTSZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultE21btWithoutMarginResult = internal constant [141 x i8] c"ZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultE21btWithoutMarginResult\00", align 1
@_ZTIZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultE21btWithoutMarginResult = internal constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([141 x i8], [141 x i8]* @_ZTSZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultE21btWithoutMarginResult, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*) }, align 4
@_ZTV24btPerturbedContactResult = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI24btPerturbedContactResult to i8*), i8* bitcast (%struct.btPerturbedContactResult* (%struct.btPerturbedContactResult*)* @_ZN24btPerturbedContactResultD2Ev to i8*), i8* bitcast (void (%struct.btPerturbedContactResult*)* @_ZN24btPerturbedContactResultD0Ev to i8*), i8* bitcast (void (%class.btManifoldResult*, i32, i32)* @_ZN16btManifoldResult20setShapeIdentifiersAEii to i8*), i8* bitcast (void (%class.btManifoldResult*, i32, i32)* @_ZN16btManifoldResult20setShapeIdentifiersBEii to i8*), i8* bitcast (void (%struct.btPerturbedContactResult*, %class.btVector3*, %class.btVector3*, float)* @_ZN24btPerturbedContactResult15addContactPointERK9btVector3S2_f to i8*)] }, comdat, align 4
@_ZTS24btPerturbedContactResult = linkonce_odr hidden constant [27 x i8] c"24btPerturbedContactResult\00", comdat, align 1
@_ZTI16btManifoldResult = external constant i8*
@_ZTI24btPerturbedContactResult = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @_ZTS24btPerturbedContactResult, i32 0, i32 0), i8* bitcast (i8** @_ZTI16btManifoldResult to i8*) }, comdat, align 4
@_ZTV16btManifoldResult = external unnamed_addr constant { [7 x i8*] }, align 4
@_ZTV13btSphereShape = external unnamed_addr constant { [25 x i8*] }, align 4
@_ZTVN12btConvexCast10CastResultE = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN12btConvexCast10CastResultE to i8*), i8* bitcast (void (%"struct.btConvexCast::CastResult"*, float)* @_ZN12btConvexCast10CastResult9DebugDrawEf to i8*), i8* bitcast (void (%"struct.btConvexCast::CastResult"*, %class.btTransform*)* @_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform to i8*), i8* bitcast (void (%"struct.btConvexCast::CastResult"*, i32, i32)* @_ZN12btConvexCast10CastResult13reportFailureEii to i8*), i8* bitcast (%"struct.btConvexCast::CastResult"* (%"struct.btConvexCast::CastResult"*)* @_ZN12btConvexCast10CastResultD2Ev to i8*), i8* bitcast (void (%"struct.btConvexCast::CastResult"*)* @_ZN12btConvexCast10CastResultD0Ev to i8*)] }, comdat, align 4
@_ZTSN12btConvexCast10CastResultE = linkonce_odr hidden constant [29 x i8] c"N12btConvexCast10CastResultE\00", comdat, align 1
@_ZTIN12btConvexCast10CastResultE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([29 x i8], [29 x i8]* @_ZTSN12btConvexCast10CastResultE, i32 0, i32 0) }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btConvexConvexAlgorithm.cpp, i8* null }]

@_ZN23btConvexConvexAlgorithm10CreateFuncC1EP30btConvexPenetrationDepthSolver = hidden unnamed_addr alias %"struct.btConvexConvexAlgorithm::CreateFunc"* (%"struct.btConvexConvexAlgorithm::CreateFunc"*, %class.btConvexPenetrationDepthSolver*), %"struct.btConvexConvexAlgorithm::CreateFunc"* (%"struct.btConvexConvexAlgorithm::CreateFunc"*, %class.btConvexPenetrationDepthSolver*)* @_ZN23btConvexConvexAlgorithm10CreateFuncC2EP30btConvexPenetrationDepthSolver
@_ZN23btConvexConvexAlgorithm10CreateFuncD1Ev = hidden unnamed_addr alias %"struct.btConvexConvexAlgorithm::CreateFunc"* (%"struct.btConvexConvexAlgorithm::CreateFunc"*), %"struct.btConvexConvexAlgorithm::CreateFunc"* (%"struct.btConvexConvexAlgorithm::CreateFunc"*)* @_ZN23btConvexConvexAlgorithm10CreateFuncD2Ev
@_ZN23btConvexConvexAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_P30btConvexPenetrationDepthSolverii = hidden unnamed_addr alias %class.btConvexConvexAlgorithm* (%class.btConvexConvexAlgorithm*, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btConvexPenetrationDepthSolver*, i32, i32), %class.btConvexConvexAlgorithm* (%class.btConvexConvexAlgorithm*, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btConvexPenetrationDepthSolver*, i32, i32)* @_ZN23btConvexConvexAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_P30btConvexPenetrationDepthSolverii
@_ZN23btConvexConvexAlgorithmD1Ev = hidden unnamed_addr alias %class.btConvexConvexAlgorithm* (%class.btConvexConvexAlgorithm*), %class.btConvexConvexAlgorithm* (%class.btConvexConvexAlgorithm*)* @_ZN23btConvexConvexAlgorithmD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %"struct.btConvexConvexAlgorithm::CreateFunc"* @_ZN23btConvexConvexAlgorithm10CreateFuncC2EP30btConvexPenetrationDepthSolver(%"struct.btConvexConvexAlgorithm::CreateFunc"* returned %this, %class.btConvexPenetrationDepthSolver* %pdSolver) unnamed_addr #2 {
entry:
  %this.addr = alloca %"struct.btConvexConvexAlgorithm::CreateFunc"*, align 4
  %pdSolver.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  store %"struct.btConvexConvexAlgorithm::CreateFunc"* %this, %"struct.btConvexConvexAlgorithm::CreateFunc"** %this.addr, align 4
  store %class.btConvexPenetrationDepthSolver* %pdSolver, %class.btConvexPenetrationDepthSolver** %pdSolver.addr, align 4
  %this1 = load %"struct.btConvexConvexAlgorithm::CreateFunc"*, %"struct.btConvexConvexAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btConvexConvexAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btConvexConvexAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN23btConvexConvexAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_numPerturbationIterations = getelementptr inbounds %"struct.btConvexConvexAlgorithm::CreateFunc", %"struct.btConvexConvexAlgorithm::CreateFunc"* %this1, i32 0, i32 2
  store i32 0, i32* %m_numPerturbationIterations, align 4
  %m_minimumPointsPerturbationThreshold = getelementptr inbounds %"struct.btConvexConvexAlgorithm::CreateFunc", %"struct.btConvexConvexAlgorithm::CreateFunc"* %this1, i32 0, i32 3
  store i32 3, i32* %m_minimumPointsPerturbationThreshold, align 4
  %2 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %pdSolver.addr, align 4
  %m_pdSolver = getelementptr inbounds %"struct.btConvexConvexAlgorithm::CreateFunc", %"struct.btConvexConvexAlgorithm::CreateFunc"* %this1, i32 0, i32 1
  store %class.btConvexPenetrationDepthSolver* %2, %class.btConvexPenetrationDepthSolver** %m_pdSolver, align 4
  ret %"struct.btConvexConvexAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %0 = bitcast %struct.btCollisionAlgorithmCreateFunc* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV30btCollisionAlgorithmCreateFunc, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %this1, i32 0, i32 1
  store i8 0, i8* %m_swapped, align 4
  ret %struct.btCollisionAlgorithmCreateFunc* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %"struct.btConvexConvexAlgorithm::CreateFunc"* @_ZN23btConvexConvexAlgorithm10CreateFuncD2Ev(%"struct.btConvexConvexAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %"struct.btConvexConvexAlgorithm::CreateFunc"*, align 4
  store %"struct.btConvexConvexAlgorithm::CreateFunc"* %this, %"struct.btConvexConvexAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btConvexConvexAlgorithm::CreateFunc"*, %"struct.btConvexConvexAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btConvexConvexAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #9
  ret %"struct.btConvexConvexAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN23btConvexConvexAlgorithm10CreateFuncD0Ev(%"struct.btConvexConvexAlgorithm::CreateFunc"* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %"struct.btConvexConvexAlgorithm::CreateFunc"*, align 4
  store %"struct.btConvexConvexAlgorithm::CreateFunc"* %this, %"struct.btConvexConvexAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btConvexConvexAlgorithm::CreateFunc"*, %"struct.btConvexConvexAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btConvexConvexAlgorithm::CreateFunc"* @_ZN23btConvexConvexAlgorithm10CreateFuncD1Ev(%"struct.btConvexConvexAlgorithm::CreateFunc"* %this1) #9
  %0 = bitcast %"struct.btConvexConvexAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #3

; Function Attrs: noinline optnone
define hidden %class.btConvexConvexAlgorithm* @_ZN23btConvexConvexAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_P30btConvexPenetrationDepthSolverii(%class.btConvexConvexAlgorithm* returned %this, %class.btPersistentManifold* %mf, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %class.btConvexPenetrationDepthSolver* %pdSolver, i32 %numPerturbationIterations, i32 %minimumPointsPerturbationThreshold) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvexConvexAlgorithm*, align 4
  %mf.addr = alloca %class.btPersistentManifold*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %pdSolver.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  %numPerturbationIterations.addr = alloca i32, align 4
  %minimumPointsPerturbationThreshold.addr = alloca i32, align 4
  store %class.btConvexConvexAlgorithm* %this, %class.btConvexConvexAlgorithm** %this.addr, align 4
  store %class.btPersistentManifold* %mf, %class.btPersistentManifold** %mf.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  store %class.btConvexPenetrationDepthSolver* %pdSolver, %class.btConvexPenetrationDepthSolver** %pdSolver.addr, align 4
  store i32 %numPerturbationIterations, i32* %numPerturbationIterations.addr, align 4
  store i32 %minimumPointsPerturbationThreshold, i32* %minimumPointsPerturbationThreshold.addr, align 4
  %this1 = load %class.btConvexConvexAlgorithm*, %class.btConvexConvexAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btConvexConvexAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* %0, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %1, %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper* %3)
  %4 = bitcast %class.btConvexConvexAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV23btConvexConvexAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4
  %m_pdSolver = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 1
  %5 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %pdSolver.addr, align 4
  store %class.btConvexPenetrationDepthSolver* %5, %class.btConvexPenetrationDepthSolver** %m_pdSolver, align 4
  %worldVertsB1 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %worldVertsB1)
  %worldVertsB2 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 3
  %call3 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %worldVertsB2)
  %m_ownManifold = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 4
  store i8 0, i8* %m_ownManifold, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 5
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %mf.addr, align 4
  store %class.btPersistentManifold* %6, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %m_lowLevelOfDetail = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 6
  store i8 0, i8* %m_lowLevelOfDetail, align 4
  %m_numPerturbationIterations = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 7
  %7 = load i32, i32* %numPerturbationIterations.addr, align 4
  store i32 %7, i32* %m_numPerturbationIterations, align 4
  %m_minimumPointsPerturbationThreshold = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 8
  %8 = load i32, i32* %minimumPointsPerturbationThreshold.addr, align 4
  store i32 %8, i32* %m_minimumPointsPerturbationThreshold, align 4
  ret %class.btConvexConvexAlgorithm* %this1
}

declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btConvexConvexAlgorithm* @_ZN23btConvexConvexAlgorithmD2Ev(%class.btConvexConvexAlgorithm* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btConvexConvexAlgorithm*, align 4
  %this.addr = alloca %class.btConvexConvexAlgorithm*, align 4
  store %class.btConvexConvexAlgorithm* %this, %class.btConvexConvexAlgorithm** %this.addr, align 4
  %this1 = load %class.btConvexConvexAlgorithm*, %class.btConvexConvexAlgorithm** %this.addr, align 4
  store %class.btConvexConvexAlgorithm* %this1, %class.btConvexConvexAlgorithm** %retval, align 4
  %0 = bitcast %class.btConvexConvexAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV23btConvexConvexAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_ownManifold = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 4
  %1 = load i8, i8* %m_ownManifold, align 4
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %m_manifoldPtr = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 5
  %2 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %tobool2 = icmp ne %class.btPersistentManifold* %2, null
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %3 = bitcast %class.btConvexConvexAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %3, i32 0, i32 1
  %4 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %m_manifoldPtr4 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 5
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr4, align 4
  %6 = bitcast %class.btDispatcher* %4 to void (%class.btDispatcher*, %class.btPersistentManifold*)***
  %vtable = load void (%class.btDispatcher*, %class.btPersistentManifold*)**, void (%class.btDispatcher*, %class.btPersistentManifold*)*** %6, align 4
  %vfn = getelementptr inbounds void (%class.btDispatcher*, %class.btPersistentManifold*)*, void (%class.btDispatcher*, %class.btPersistentManifold*)** %vtable, i64 4
  %7 = load void (%class.btDispatcher*, %class.btPersistentManifold*)*, void (%class.btDispatcher*, %class.btPersistentManifold*)** %vfn, align 4
  call void %7(%class.btDispatcher* %4, %class.btPersistentManifold* %5)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  br label %if.end5

if.end5:                                          ; preds = %if.end, %entry
  %worldVertsB2 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 3
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %worldVertsB2) #9
  %worldVertsB1 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 2
  %call6 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %worldVertsB1) #9
  %8 = bitcast %class.btConvexConvexAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %call7 = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* %8) #9
  %9 = load %class.btConvexConvexAlgorithm*, %class.btConvexConvexAlgorithm** %retval, align 4
  ret %class.btConvexConvexAlgorithm* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* returned) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN23btConvexConvexAlgorithmD0Ev(%class.btConvexConvexAlgorithm* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvexConvexAlgorithm*, align 4
  store %class.btConvexConvexAlgorithm* %this, %class.btConvexConvexAlgorithm** %this.addr, align 4
  %this1 = load %class.btConvexConvexAlgorithm*, %class.btConvexConvexAlgorithm** %this.addr, align 4
  %call = call %class.btConvexConvexAlgorithm* @_ZN23btConvexConvexAlgorithmD1Ev(%class.btConvexConvexAlgorithm* %this1) #9
  %0 = bitcast %class.btConvexConvexAlgorithm* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN23btConvexConvexAlgorithm19setLowLevelOfDetailEb(%class.btConvexConvexAlgorithm* %this, i1 zeroext %useLowLevel) #1 {
entry:
  %this.addr = alloca %class.btConvexConvexAlgorithm*, align 4
  %useLowLevel.addr = alloca i8, align 1
  store %class.btConvexConvexAlgorithm* %this, %class.btConvexConvexAlgorithm** %this.addr, align 4
  %frombool = zext i1 %useLowLevel to i8
  store i8 %frombool, i8* %useLowLevel.addr, align 1
  %this1 = load %class.btConvexConvexAlgorithm*, %class.btConvexConvexAlgorithm** %this.addr, align 4
  %0 = load i8, i8* %useLowLevel.addr, align 1
  %tobool = trunc i8 %0 to i1
  %m_lowLevelOfDetail = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 6
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %m_lowLevelOfDetail, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btConvexConvexAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvexConvexAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %min0 = alloca %class.btConvexShape*, align 4
  %min1 = alloca %class.btConvexShape*, align 4
  %normalOnB = alloca %class.btVector3, align 4
  %pointOnBWorld = alloca %class.btVector3, align 4
  %capsuleA = alloca %class.btCapsuleShape*, align 4
  %capsuleB = alloca %class.btCapsuleShape*, align 4
  %threshold = alloca float, align 4
  %dist = alloca float, align 4
  %capsuleA37 = alloca %class.btCapsuleShape*, align 4
  %capsuleB38 = alloca %class.btSphereShape*, align 4
  %threshold39 = alloca float, align 4
  %dist42 = alloca float, align 4
  %capsuleA62 = alloca %class.btSphereShape*, align 4
  %capsuleB63 = alloca %class.btCapsuleShape*, align 4
  %threshold64 = alloca float, align 4
  %dist67 = alloca float, align 4
  %input = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", align 4
  %simplexSolver = alloca %class.btVoronoiSimplexSolver, align 4
  %gjkPairDetector = alloca %class.btGjkPairDetector, align 4
  %dummy = alloca %struct.btDummyResult, align 4
  %min0Margin = alloca float, align 4
  %min1Margin = alloca float, align 4
  %withoutMargin = alloca %struct.btWithoutMarginResult, align 4
  %polyhedronA = alloca %class.btPolyhedralConvexShape*, align 4
  %polyhedronB = alloca %class.btPolyhedralConvexShape*, align 4
  %threshold126 = alloca float, align 4
  %minDist = alloca float, align 4
  %sepNormalWorldSpace = alloca %class.btVector3, align 4
  %foundSepAxis = alloca i8, align 1
  %ref.tmp = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %vertices = alloca %class.btAlignedObjectArray, align 4
  %tri = alloca %class.btTriangleShape*, align 4
  %ref.tmp162 = alloca %class.btVector3, align 4
  %ref.tmp164 = alloca %class.btVector3, align 4
  %ref.tmp168 = alloca %class.btVector3, align 4
  %threshold172 = alloca float, align 4
  %sepNormalWorldSpace175 = alloca %class.btVector3, align 4
  %minDist177 = alloca float, align 4
  %maxDist = alloca float, align 4
  %foundSepAxis178 = alloca i8, align 1
  %l2 = alloca float, align 4
  %ref.tmp184 = alloca %class.btVector3, align 4
  %ref.tmp186 = alloca float, align 4
  %ref.tmp200 = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %v0 = alloca %class.btVector3, align 4
  %v1 = alloca %class.btVector3, align 4
  %sepNormalWorldSpace227 = alloca %class.btVector3, align 4
  %l2229 = alloca float, align 4
  %ref.tmp234 = alloca %class.btVector3, align 4
  %ref.tmp236 = alloca float, align 4
  %perturbeA = alloca i8, align 1
  %angleLimit = alloca float, align 4
  %perturbeAngle = alloca float, align 4
  %radiusA = alloca float, align 4
  %radiusB = alloca float, align 4
  %unPerturbedTransform = alloca %class.btTransform, align 4
  %perturbeRot = alloca %class.btQuaternion, align 4
  %iterationAngle = alloca float, align 4
  %rotq = alloca %class.btQuaternion, align 4
  %ref.tmp276 = alloca %class.btMatrix3x3, align 4
  %ref.tmp277 = alloca %class.btMatrix3x3, align 4
  %ref.tmp278 = alloca %class.btQuaternion, align 4
  %ref.tmp279 = alloca %class.btQuaternion, align 4
  %ref.tmp280 = alloca %class.btQuaternion, align 4
  %ref.tmp292 = alloca %class.btMatrix3x3, align 4
  %ref.tmp293 = alloca %class.btMatrix3x3, align 4
  %ref.tmp294 = alloca %class.btQuaternion, align 4
  %ref.tmp295 = alloca %class.btQuaternion, align 4
  %ref.tmp296 = alloca %class.btQuaternion, align 4
  %perturbedResultOut = alloca %struct.btPerturbedContactResult, align 4
  store %class.btConvexConvexAlgorithm* %this, %class.btConvexConvexAlgorithm** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4
  %this1 = load %class.btConvexConvexAlgorithm*, %class.btConvexConvexAlgorithm** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 5
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %tobool = icmp ne %class.btPersistentManifold* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %1 = bitcast %class.btConvexConvexAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %1, i32 0, i32 1
  %2 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %3)
  %4 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call2 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %4)
  %5 = bitcast %class.btDispatcher* %2 to %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable = load %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)**, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*** %5, align 4
  %vfn = getelementptr inbounds %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable, i64 3
  %6 = load %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn, align 4
  %call3 = call %class.btPersistentManifold* %6(%class.btDispatcher* %2, %class.btCollisionObject* %call, %class.btCollisionObject* %call2)
  %m_manifoldPtr4 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 5
  store %class.btPersistentManifold* %call3, %class.btPersistentManifold** %m_manifoldPtr4, align 4
  %m_ownManifold = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 4
  store i8 1, i8* %m_ownManifold, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_manifoldPtr5 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 5
  %8 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr5, align 4
  call void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %7, %class.btPersistentManifold* %8)
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call6 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %9)
  %10 = bitcast %class.btCollisionShape* %call6 to %class.btConvexShape*
  store %class.btConvexShape* %10, %class.btConvexShape** %min0, align 4
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call7 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %11)
  %12 = bitcast %class.btCollisionShape* %call7 to %class.btConvexShape*
  store %class.btConvexShape* %12, %class.btConvexShape** %min1, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normalOnB)
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pointOnBWorld)
  %13 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  %14 = bitcast %class.btConvexShape* %13 to %class.btCollisionShape*
  %call10 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %14)
  %cmp = icmp eq i32 %call10, 10
  br i1 %cmp, label %land.lhs.true, label %if.end30

land.lhs.true:                                    ; preds = %if.end
  %15 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  %16 = bitcast %class.btConvexShape* %15 to %class.btCollisionShape*
  %call11 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %16)
  %cmp12 = icmp eq i32 %call11, 10
  br i1 %cmp12, label %if.then13, label %if.end30

if.then13:                                        ; preds = %land.lhs.true
  %17 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  %18 = bitcast %class.btConvexShape* %17 to %class.btCapsuleShape*
  store %class.btCapsuleShape* %18, %class.btCapsuleShape** %capsuleA, align 4
  %19 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  %20 = bitcast %class.btConvexShape* %19 to %class.btCapsuleShape*
  store %class.btCapsuleShape* %20, %class.btCapsuleShape** %capsuleB, align 4
  %m_manifoldPtr14 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 5
  %21 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr14, align 4
  %call15 = call float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold* %21)
  store float %call15, float* %threshold, align 4
  %22 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleA, align 4
  %call16 = call float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %22)
  %23 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleA, align 4
  %call17 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %23)
  %24 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleB, align 4
  %call18 = call float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %24)
  %25 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleB, align 4
  %call19 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %25)
  %26 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleA, align 4
  %call20 = call i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %26)
  %27 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleB, align 4
  %call21 = call i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %27)
  %28 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %28)
  %29 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %29)
  %30 = load float, float* %threshold, align 4
  %call24 = call float @_ZL22capsuleCapsuleDistanceR9btVector3S0_ffffiiRK11btTransformS3_f(%class.btVector3* nonnull align 4 dereferenceable(16) %normalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %pointOnBWorld, float %call16, float %call17, float %call18, float %call19, i32 %call20, i32 %call21, %class.btTransform* nonnull align 4 dereferenceable(64) %call22, %class.btTransform* nonnull align 4 dereferenceable(64) %call23, float %30)
  store float %call24, float* %dist, align 4
  %31 = load float, float* %dist, align 4
  %32 = load float, float* %threshold, align 4
  %cmp25 = fcmp olt float %31, %32
  br i1 %cmp25, label %if.then26, label %if.end29

if.then26:                                        ; preds = %if.then13
  %33 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %34 = load float, float* %dist, align 4
  %35 = bitcast %class.btManifoldResult* %33 to void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)***
  %vtable27 = load void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*** %35, align 4
  %vfn28 = getelementptr inbounds void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)** %vtable27, i64 4
  %36 = load void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)** %vfn28, align 4
  call void %36(%class.btManifoldResult* %33, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %pointOnBWorld, float %34)
  br label %if.end29

if.end29:                                         ; preds = %if.then26, %if.then13
  %37 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  call void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %37)
  br label %if.end318

if.end30:                                         ; preds = %land.lhs.true, %if.end
  %38 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  %39 = bitcast %class.btConvexShape* %38 to %class.btCollisionShape*
  %call31 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %39)
  %cmp32 = icmp eq i32 %call31, 10
  br i1 %cmp32, label %land.lhs.true33, label %if.end55

land.lhs.true33:                                  ; preds = %if.end30
  %40 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  %41 = bitcast %class.btConvexShape* %40 to %class.btCollisionShape*
  %call34 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %41)
  %cmp35 = icmp eq i32 %call34, 8
  br i1 %cmp35, label %if.then36, label %if.end55

if.then36:                                        ; preds = %land.lhs.true33
  %42 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  %43 = bitcast %class.btConvexShape* %42 to %class.btCapsuleShape*
  store %class.btCapsuleShape* %43, %class.btCapsuleShape** %capsuleA37, align 4
  %44 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  %45 = bitcast %class.btConvexShape* %44 to %class.btSphereShape*
  store %class.btSphereShape* %45, %class.btSphereShape** %capsuleB38, align 4
  %m_manifoldPtr40 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 5
  %46 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr40, align 4
  %call41 = call float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold* %46)
  store float %call41, float* %threshold39, align 4
  %47 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleA37, align 4
  %call43 = call float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %47)
  %48 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleA37, align 4
  %call44 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %48)
  %49 = load %class.btSphereShape*, %class.btSphereShape** %capsuleB38, align 4
  %call45 = call float @_ZNK13btSphereShape9getRadiusEv(%class.btSphereShape* %49)
  %50 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleA37, align 4
  %call46 = call i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %50)
  %51 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call47 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %51)
  %52 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call48 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %52)
  %53 = load float, float* %threshold39, align 4
  %call49 = call float @_ZL22capsuleCapsuleDistanceR9btVector3S0_ffffiiRK11btTransformS3_f(%class.btVector3* nonnull align 4 dereferenceable(16) %normalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %pointOnBWorld, float %call43, float %call44, float 0.000000e+00, float %call45, i32 %call46, i32 1, %class.btTransform* nonnull align 4 dereferenceable(64) %call47, %class.btTransform* nonnull align 4 dereferenceable(64) %call48, float %53)
  store float %call49, float* %dist42, align 4
  %54 = load float, float* %dist42, align 4
  %55 = load float, float* %threshold39, align 4
  %cmp50 = fcmp olt float %54, %55
  br i1 %cmp50, label %if.then51, label %if.end54

if.then51:                                        ; preds = %if.then36
  %56 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %57 = load float, float* %dist42, align 4
  %58 = bitcast %class.btManifoldResult* %56 to void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)***
  %vtable52 = load void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*** %58, align 4
  %vfn53 = getelementptr inbounds void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)** %vtable52, i64 4
  %59 = load void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)** %vfn53, align 4
  call void %59(%class.btManifoldResult* %56, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %pointOnBWorld, float %57)
  br label %if.end54

if.end54:                                         ; preds = %if.then51, %if.then36
  %60 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  call void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %60)
  br label %if.end318

if.end55:                                         ; preds = %land.lhs.true33, %if.end30
  %61 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  %62 = bitcast %class.btConvexShape* %61 to %class.btCollisionShape*
  %call56 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %62)
  %cmp57 = icmp eq i32 %call56, 8
  br i1 %cmp57, label %land.lhs.true58, label %if.end80

land.lhs.true58:                                  ; preds = %if.end55
  %63 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  %64 = bitcast %class.btConvexShape* %63 to %class.btCollisionShape*
  %call59 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %64)
  %cmp60 = icmp eq i32 %call59, 10
  br i1 %cmp60, label %if.then61, label %if.end80

if.then61:                                        ; preds = %land.lhs.true58
  %65 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  %66 = bitcast %class.btConvexShape* %65 to %class.btSphereShape*
  store %class.btSphereShape* %66, %class.btSphereShape** %capsuleA62, align 4
  %67 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  %68 = bitcast %class.btConvexShape* %67 to %class.btCapsuleShape*
  store %class.btCapsuleShape* %68, %class.btCapsuleShape** %capsuleB63, align 4
  %m_manifoldPtr65 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 5
  %69 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr65, align 4
  %call66 = call float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold* %69)
  store float %call66, float* %threshold64, align 4
  %70 = load %class.btSphereShape*, %class.btSphereShape** %capsuleA62, align 4
  %call68 = call float @_ZNK13btSphereShape9getRadiusEv(%class.btSphereShape* %70)
  %71 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleB63, align 4
  %call69 = call float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %71)
  %72 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleB63, align 4
  %call70 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %72)
  %73 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleB63, align 4
  %call71 = call i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %73)
  %74 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call72 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %74)
  %75 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call73 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %75)
  %76 = load float, float* %threshold64, align 4
  %call74 = call float @_ZL22capsuleCapsuleDistanceR9btVector3S0_ffffiiRK11btTransformS3_f(%class.btVector3* nonnull align 4 dereferenceable(16) %normalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %pointOnBWorld, float 0.000000e+00, float %call68, float %call69, float %call70, i32 1, i32 %call71, %class.btTransform* nonnull align 4 dereferenceable(64) %call72, %class.btTransform* nonnull align 4 dereferenceable(64) %call73, float %76)
  store float %call74, float* %dist67, align 4
  %77 = load float, float* %dist67, align 4
  %78 = load float, float* %threshold64, align 4
  %cmp75 = fcmp olt float %77, %78
  br i1 %cmp75, label %if.then76, label %if.end79

if.then76:                                        ; preds = %if.then61
  %79 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %80 = load float, float* %dist67, align 4
  %81 = bitcast %class.btManifoldResult* %79 to void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)***
  %vtable77 = load void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*** %81, align 4
  %vfn78 = getelementptr inbounds void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)** %vtable77, i64 4
  %82 = load void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)** %vfn78, align 4
  call void %82(%class.btManifoldResult* %79, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %pointOnBWorld, float %80)
  br label %if.end79

if.end79:                                         ; preds = %if.then76, %if.then61
  %83 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  call void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %83)
  br label %if.end318

if.end80:                                         ; preds = %land.lhs.true58, %if.end55
  %call81 = call %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* @_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev(%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input)
  %call82 = call %class.btVoronoiSimplexSolver* @_ZN22btVoronoiSimplexSolverC2Ev(%class.btVoronoiSimplexSolver* %simplexSolver)
  %84 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  %85 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  %m_pdSolver = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 1
  %86 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_pdSolver, align 4
  %call83 = call %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* %gjkPairDetector, %class.btConvexShape* %84, %class.btConvexShape* %85, %class.btVoronoiSimplexSolver* %simplexSolver, %class.btConvexPenetrationDepthSolver* %86)
  %87 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  call void @_ZN17btGjkPairDetector13setMinkowskiAEPK13btConvexShape(%class.btGjkPairDetector* %gjkPairDetector, %class.btConvexShape* %87)
  %88 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  call void @_ZN17btGjkPairDetector13setMinkowskiBEPK13btConvexShape(%class.btGjkPairDetector* %gjkPairDetector, %class.btConvexShape* %88)
  %89 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  %90 = bitcast %class.btConvexShape* %89 to float (%class.btConvexShape*)***
  %vtable84 = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %90, align 4
  %vfn85 = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable84, i64 12
  %91 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn85, align 4
  %call86 = call float %91(%class.btConvexShape* %89)
  %92 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  %93 = bitcast %class.btConvexShape* %92 to float (%class.btConvexShape*)***
  %vtable87 = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %93, align 4
  %vfn88 = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable87, i64 12
  %94 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn88, align 4
  %call89 = call float %94(%class.btConvexShape* %92)
  %add = fadd float %call86, %call89
  %m_manifoldPtr90 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 5
  %95 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr90, align 4
  %call91 = call float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold* %95)
  %add92 = fadd float %add, %call91
  %96 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_closestPointDistanceThreshold = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %96, i32 0, i32 8
  %97 = load float, float* %m_closestPointDistanceThreshold, align 4
  %add93 = fadd float %add92, %97
  %m_maximumDistanceSquared = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 2
  store float %add93, float* %m_maximumDistanceSquared, align 4
  %m_maximumDistanceSquared94 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 2
  %98 = load float, float* %m_maximumDistanceSquared94, align 4
  %m_maximumDistanceSquared95 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 2
  %99 = load float, float* %m_maximumDistanceSquared95, align 4
  %mul = fmul float %99, %98
  store float %mul, float* %m_maximumDistanceSquared95, align 4
  %100 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call96 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %100)
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 0
  %call97 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformA, %class.btTransform* nonnull align 4 dereferenceable(64) %call96)
  %101 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call98 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %101)
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 1
  %call99 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformB, %class.btTransform* nonnull align 4 dereferenceable(64) %call98)
  %102 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  %103 = bitcast %class.btConvexShape* %102 to %class.btCollisionShape*
  %call100 = call zeroext i1 @_ZNK16btCollisionShape12isPolyhedralEv(%class.btCollisionShape* %103)
  br i1 %call100, label %land.lhs.true101, label %if.end217

land.lhs.true101:                                 ; preds = %if.end80
  %104 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  %105 = bitcast %class.btConvexShape* %104 to %class.btCollisionShape*
  %call102 = call zeroext i1 @_ZNK16btCollisionShape12isPolyhedralEv(%class.btCollisionShape* %105)
  br i1 %call102, label %if.then103, label %if.end217

if.then103:                                       ; preds = %land.lhs.true101
  %call104 = call %struct.btDummyResult* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN13btDummyResultC2Ev(%struct.btDummyResult* %dummy) #9
  %106 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  %107 = bitcast %class.btConvexShape* %106 to %class.btCollisionShape*
  %call105 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %107)
  %cmp106 = icmp eq i32 %call105, 0
  br i1 %cmp106, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then103
  br label %cond.end

cond.false:                                       ; preds = %if.then103
  %108 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  %109 = bitcast %class.btConvexShape* %108 to float (%class.btConvexShape*)***
  %vtable107 = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %109, align 4
  %vfn108 = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable107, i64 12
  %110 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn108, align 4
  %call109 = call float %110(%class.btConvexShape* %108)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ 0.000000e+00, %cond.true ], [ %call109, %cond.false ]
  store float %cond, float* %min0Margin, align 4
  %111 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  %112 = bitcast %class.btConvexShape* %111 to %class.btCollisionShape*
  %call110 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %112)
  %cmp111 = icmp eq i32 %call110, 0
  br i1 %cmp111, label %cond.true112, label %cond.false113

cond.true112:                                     ; preds = %cond.end
  br label %cond.end117

cond.false113:                                    ; preds = %cond.end
  %113 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  %114 = bitcast %class.btConvexShape* %113 to float (%class.btConvexShape*)***
  %vtable114 = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %114, align 4
  %vfn115 = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable114, i64 12
  %115 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn115, align 4
  %call116 = call float %115(%class.btConvexShape* %113)
  br label %cond.end117

cond.end117:                                      ; preds = %cond.false113, %cond.true112
  %cond118 = phi float [ 0.000000e+00, %cond.true112 ], [ %call116, %cond.false113 ]
  store float %cond118, float* %min1Margin, align 4
  %116 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %117 = bitcast %class.btManifoldResult* %116 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %118 = load float, float* %min0Margin, align 4
  %119 = load float, float* %min1Margin, align 4
  %call119 = call %struct.btWithoutMarginResult* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN21btWithoutMarginResultC2EPN36btDiscreteCollisionDetectorInterface6ResultEff(%struct.btWithoutMarginResult* %withoutMargin, %"struct.btDiscreteCollisionDetectorInterface::Result"* %117, float %118, float %119)
  %120 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  %121 = bitcast %class.btConvexShape* %120 to %class.btPolyhedralConvexShape*
  store %class.btPolyhedralConvexShape* %121, %class.btPolyhedralConvexShape** %polyhedronA, align 4
  %122 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  %123 = bitcast %class.btConvexShape* %122 to %class.btPolyhedralConvexShape*
  store %class.btPolyhedralConvexShape* %123, %class.btPolyhedralConvexShape** %polyhedronB, align 4
  %124 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %polyhedronA, align 4
  %call120 = call %class.btConvexPolyhedron* @_ZNK23btPolyhedralConvexShape19getConvexPolyhedronEv(%class.btPolyhedralConvexShape* %124)
  %tobool121 = icmp ne %class.btConvexPolyhedron* %call120, null
  br i1 %tobool121, label %land.lhs.true122, label %if.else154

land.lhs.true122:                                 ; preds = %cond.end117
  %125 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %polyhedronB, align 4
  %call123 = call %class.btConvexPolyhedron* @_ZNK23btPolyhedralConvexShape19getConvexPolyhedronEv(%class.btPolyhedralConvexShape* %125)
  %tobool124 = icmp ne %class.btConvexPolyhedron* %call123, null
  br i1 %tobool124, label %if.then125, label %if.else154

if.then125:                                       ; preds = %land.lhs.true122
  %m_manifoldPtr127 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 5
  %126 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr127, align 4
  %call128 = call float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold* %126)
  store float %call128, float* %threshold126, align 4
  store float 0xC6293E5940000000, float* %minDist, align 4
  %call129 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %sepNormalWorldSpace)
  store i8 1, i8* %foundSepAxis, align 1
  %127 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %m_enableSatConvex = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %127, i32 0, i32 6
  %128 = load i8, i8* %m_enableSatConvex, align 4
  %tobool130 = trunc i8 %128 to i1
  br i1 %tobool130, label %if.then131, label %if.else

if.then131:                                       ; preds = %if.then125
  %129 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %polyhedronA, align 4
  %call132 = call %class.btConvexPolyhedron* @_ZNK23btPolyhedralConvexShape19getConvexPolyhedronEv(%class.btPolyhedralConvexShape* %129)
  %130 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %polyhedronB, align 4
  %call133 = call %class.btConvexPolyhedron* @_ZNK23btPolyhedralConvexShape19getConvexPolyhedronEv(%class.btPolyhedralConvexShape* %130)
  %131 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call134 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %131)
  %132 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call135 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %132)
  %133 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %134 = bitcast %class.btManifoldResult* %133 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call136 = call zeroext i1 @_ZN27btPolyhedralContactClipping18findSeparatingAxisERK18btConvexPolyhedronS2_RK11btTransformS5_R9btVector3RN36btDiscreteCollisionDetectorInterface6ResultE(%class.btConvexPolyhedron* nonnull align 1 %call132, %class.btConvexPolyhedron* nonnull align 1 %call133, %class.btTransform* nonnull align 4 dereferenceable(64) %call134, %class.btTransform* nonnull align 4 dereferenceable(64) %call135, %class.btVector3* nonnull align 4 dereferenceable(16) %sepNormalWorldSpace, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %134)
  %frombool = zext i1 %call136 to i8
  store i8 %frombool, i8* %foundSepAxis, align 1
  br label %if.end140

if.else:                                          ; preds = %if.then125
  %135 = bitcast %struct.btWithoutMarginResult* %withoutMargin to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %136 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %m_debugDraw = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %136, i32 0, i32 5
  %137 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDraw, align 4
  call void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector* %gjkPairDetector, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %135, %class.btIDebugDraw* %137, i1 zeroext false)
  %m_reportedNormalOnWorld = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %withoutMargin, i32 0, i32 2
  %138 = bitcast %class.btVector3* %sepNormalWorldSpace to i8*
  %139 = bitcast %class.btVector3* %m_reportedNormalOnWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %138, i8* align 4 %139, i32 16, i1 false)
  %m_reportedDistance = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %withoutMargin, i32 0, i32 5
  %140 = load float, float* %m_reportedDistance, align 4
  store float %140, float* %minDist, align 4
  %m_foundResult = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %withoutMargin, i32 0, i32 6
  %141 = load i8, i8* %m_foundResult, align 4
  %tobool137 = trunc i8 %141 to i1
  br i1 %tobool137, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.else
  %142 = load float, float* %minDist, align 4
  %cmp138 = fcmp olt float %142, 0.000000e+00
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.else
  %143 = phi i1 [ false, %if.else ], [ %cmp138, %land.rhs ]
  %frombool139 = zext i1 %143 to i8
  store i8 %frombool139, i8* %foundSepAxis, align 1
  br label %if.end140

if.end140:                                        ; preds = %land.end, %if.then131
  %144 = load i8, i8* %foundSepAxis, align 1
  %tobool141 = trunc i8 %144 to i1
  br i1 %tobool141, label %if.then142, label %if.end149

if.then142:                                       ; preds = %if.end140
  %worldVertsB1 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 2
  %call143 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %worldVertsB1, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %145 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %polyhedronA, align 4
  %call144 = call %class.btConvexPolyhedron* @_ZNK23btPolyhedralConvexShape19getConvexPolyhedronEv(%class.btPolyhedralConvexShape* %145)
  %146 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %polyhedronB, align 4
  %call145 = call %class.btConvexPolyhedron* @_ZNK23btPolyhedralConvexShape19getConvexPolyhedronEv(%class.btPolyhedralConvexShape* %146)
  %147 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call146 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %147)
  %148 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call147 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %148)
  %149 = load float, float* %minDist, align 4
  %150 = load float, float* %threshold126, align 4
  %sub = fsub float %149, %150
  %151 = load float, float* %threshold126, align 4
  %worldVertsB1148 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 2
  %worldVertsB2 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 3
  %152 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %153 = bitcast %class.btManifoldResult* %152 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  call void @_ZN27btPolyhedralContactClipping19clipHullAgainstHullERK9btVector3RK18btConvexPolyhedronS5_RK11btTransformS8_ffR20btAlignedObjectArrayIS0_ESB_RN36btDiscreteCollisionDetectorInterface6ResultE(%class.btVector3* nonnull align 4 dereferenceable(16) %sepNormalWorldSpace, %class.btConvexPolyhedron* nonnull align 1 %call144, %class.btConvexPolyhedron* nonnull align 1 %call145, %class.btTransform* nonnull align 4 dereferenceable(64) %call146, %class.btTransform* nonnull align 4 dereferenceable(64) %call147, float %sub, float %151, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %worldVertsB1148, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %worldVertsB2, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %153)
  br label %if.end149

if.end149:                                        ; preds = %if.then142, %if.end140
  %m_ownManifold150 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 4
  %154 = load i8, i8* %m_ownManifold150, align 4
  %tobool151 = trunc i8 %154 to i1
  br i1 %tobool151, label %if.then152, label %if.end153

if.then152:                                       ; preds = %if.end149
  %155 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  call void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %155)
  br label %if.end153

if.end153:                                        ; preds = %if.then152, %if.end149
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.else154:                                       ; preds = %land.lhs.true122, %cond.end117
  %156 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %polyhedronA, align 4
  %call155 = call %class.btConvexPolyhedron* @_ZNK23btPolyhedralConvexShape19getConvexPolyhedronEv(%class.btPolyhedralConvexShape* %156)
  %tobool156 = icmp ne %class.btConvexPolyhedron* %call155, null
  br i1 %tobool156, label %land.lhs.true157, label %if.end212

land.lhs.true157:                                 ; preds = %if.else154
  %157 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %polyhedronB, align 4
  %158 = bitcast %class.btPolyhedralConvexShape* %157 to %class.btCollisionShape*
  %call158 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %158)
  %cmp159 = icmp eq i32 %call158, 1
  br i1 %cmp159, label %if.then160, label %if.end212

if.then160:                                       ; preds = %land.lhs.true157
  %call161 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %vertices)
  %159 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %polyhedronB, align 4
  %160 = bitcast %class.btPolyhedralConvexShape* %159 to %class.btTriangleShape*
  store %class.btTriangleShape* %160, %class.btTriangleShape** %tri, align 4
  %161 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call163 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %161)
  %162 = load %class.btTriangleShape*, %class.btTriangleShape** %tri, align 4
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %162, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp162, %class.btTransform* %call163, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %vertices, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp162)
  %163 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call165 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %163)
  %164 = load %class.btTriangleShape*, %class.btTriangleShape** %tri, align 4
  %m_vertices1166 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %164, i32 0, i32 1
  %arrayidx167 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1166, i32 0, i32 1
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp164, %class.btTransform* %call165, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx167)
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %vertices, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp164)
  %165 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call169 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %165)
  %166 = load %class.btTriangleShape*, %class.btTriangleShape** %tri, align 4
  %m_vertices1170 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %166, i32 0, i32 1
  %arrayidx171 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1170, i32 0, i32 2
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp168, %class.btTransform* %call169, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx171)
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %vertices, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp168)
  %m_manifoldPtr173 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 5
  %167 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr173, align 4
  %call174 = call float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold* %167)
  store float %call174, float* %threshold172, align 4
  %call176 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %sepNormalWorldSpace175)
  store float 0xC6293E5940000000, float* %minDist177, align 4
  %168 = load float, float* %threshold172, align 4
  store float %168, float* %maxDist, align 4
  store i8 0, i8* %foundSepAxis178, align 1
  %169 = bitcast %struct.btDummyResult* %dummy to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %170 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %m_debugDraw179 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %170, i32 0, i32 5
  %171 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDraw179, align 4
  call void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector* %gjkPairDetector, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %169, %class.btIDebugDraw* %171, i1 zeroext false)
  %call180 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK17btGjkPairDetector23getCachedSeparatingAxisEv(%class.btGjkPairDetector* %gjkPairDetector)
  %call181 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %call180)
  store float %call181, float* %l2, align 4
  %172 = load float, float* %l2, align 4
  %cmp182 = fcmp ogt float %172, 0x3E80000000000000
  br i1 %cmp182, label %if.then183, label %if.end196

if.then183:                                       ; preds = %if.then160
  %call185 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK17btGjkPairDetector23getCachedSeparatingAxisEv(%class.btGjkPairDetector* %gjkPairDetector)
  %173 = load float, float* %l2, align 4
  %div = fdiv float 1.000000e+00, %173
  store float %div, float* %ref.tmp186, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp184, %class.btVector3* nonnull align 4 dereferenceable(16) %call185, float* nonnull align 4 dereferenceable(4) %ref.tmp186)
  %174 = bitcast %class.btVector3* %sepNormalWorldSpace175 to i8*
  %175 = bitcast %class.btVector3* %ref.tmp184 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %174, i8* align 4 %175, i32 16, i1 false)
  %call187 = call float @_ZNK17btGjkPairDetector27getCachedSeparatingDistanceEv(%class.btGjkPairDetector* %gjkPairDetector)
  %176 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  %177 = bitcast %class.btConvexShape* %176 to float (%class.btConvexShape*)***
  %vtable188 = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %177, align 4
  %vfn189 = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable188, i64 12
  %178 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn189, align 4
  %call190 = call float %178(%class.btConvexShape* %176)
  %sub191 = fsub float %call187, %call190
  %179 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  %180 = bitcast %class.btConvexShape* %179 to float (%class.btConvexShape*)***
  %vtable192 = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %180, align 4
  %vfn193 = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable192, i64 12
  %181 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn193, align 4
  %call194 = call float %181(%class.btConvexShape* %179)
  %sub195 = fsub float %sub191, %call194
  store float %sub195, float* %minDist177, align 4
  store i8 1, i8* %foundSepAxis178, align 1
  br label %if.end196

if.end196:                                        ; preds = %if.then183, %if.then160
  %182 = load i8, i8* %foundSepAxis178, align 1
  %tobool197 = trunc i8 %182 to i1
  br i1 %tobool197, label %if.then198, label %if.end206

if.then198:                                       ; preds = %if.end196
  %worldVertsB2199 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 3
  %call201 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp200)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %worldVertsB2199, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp200)
  %183 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %polyhedronA, align 4
  %call202 = call %class.btConvexPolyhedron* @_ZNK23btPolyhedralConvexShape19getConvexPolyhedronEv(%class.btPolyhedralConvexShape* %183)
  %184 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call203 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %184)
  %worldVertsB2204 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 3
  %185 = load float, float* %minDist177, align 4
  %186 = load float, float* %threshold172, align 4
  %sub205 = fsub float %185, %186
  %187 = load float, float* %maxDist, align 4
  %188 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %189 = bitcast %class.btManifoldResult* %188 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  call void @_ZN27btPolyhedralContactClipping19clipFaceAgainstHullERK9btVector3RK18btConvexPolyhedronRK11btTransformR20btAlignedObjectArrayIS0_ESB_ffRN36btDiscreteCollisionDetectorInterface6ResultE(%class.btVector3* nonnull align 4 dereferenceable(16) %sepNormalWorldSpace175, %class.btConvexPolyhedron* nonnull align 1 %call202, %class.btTransform* nonnull align 4 dereferenceable(64) %call203, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %vertices, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %worldVertsB2204, float %sub205, float %187, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %189)
  br label %if.end206

if.end206:                                        ; preds = %if.then198, %if.end196
  %m_ownManifold207 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 4
  %190 = load i8, i8* %m_ownManifold207, align 4
  %tobool208 = trunc i8 %190 to i1
  br i1 %tobool208, label %if.then209, label %if.end210

if.then209:                                       ; preds = %if.end206
  %191 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  call void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %191)
  br label %if.end210

if.end210:                                        ; preds = %if.then209, %if.end206
  store i32 1, i32* %cleanup.dest.slot, align 4
  %call211 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %vertices) #9
  br label %cleanup

if.end212:                                        ; preds = %land.lhs.true157, %if.else154
  br label %if.end213

if.end213:                                        ; preds = %if.end212
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end213, %if.end210, %if.end153
  %call214 = call %struct.btWithoutMarginResult* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN21btWithoutMarginResultD2Ev(%struct.btWithoutMarginResult* %withoutMargin) #9
  %call216 = call %struct.btDummyResult* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN13btDummyResultD2Ev(%struct.btDummyResult* %dummy) #9
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %cleanup311 [
    i32 0, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup
  br label %if.end217

if.end217:                                        ; preds = %cleanup.cont, %land.lhs.true101, %if.end80
  %192 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %193 = bitcast %class.btManifoldResult* %192 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %194 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %m_debugDraw218 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %194, i32 0, i32 5
  %195 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDraw218, align 4
  call void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector* %gjkPairDetector, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %193, %class.btIDebugDraw* %195, i1 zeroext false)
  %m_numPerturbationIterations = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 7
  %196 = load i32, i32* %m_numPerturbationIterations, align 4
  %tobool219 = icmp ne i32 %196, 0
  br i1 %tobool219, label %land.lhs.true220, label %if.end310

land.lhs.true220:                                 ; preds = %if.end217
  %197 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %call221 = call %class.btPersistentManifold* @_ZN16btManifoldResult21getPersistentManifoldEv(%class.btManifoldResult* %197)
  %call222 = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %call221)
  %m_minimumPointsPerturbationThreshold = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 8
  %198 = load i32, i32* %m_minimumPointsPerturbationThreshold, align 4
  %cmp223 = icmp slt i32 %call222, %198
  br i1 %cmp223, label %if.then224, label %if.end310

if.then224:                                       ; preds = %land.lhs.true220
  %call225 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %v0)
  %call226 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %v1)
  %call228 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %sepNormalWorldSpace227)
  %call230 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK17btGjkPairDetector23getCachedSeparatingAxisEv(%class.btGjkPairDetector* %gjkPairDetector)
  %call231 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %call230)
  store float %call231, float* %l2229, align 4
  %199 = load float, float* %l2229, align 4
  %cmp232 = fcmp ogt float %199, 0x3E80000000000000
  br i1 %cmp232, label %if.then233, label %if.end309

if.then233:                                       ; preds = %if.then224
  %call235 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK17btGjkPairDetector23getCachedSeparatingAxisEv(%class.btGjkPairDetector* %gjkPairDetector)
  %200 = load float, float* %l2229, align 4
  %div237 = fdiv float 1.000000e+00, %200
  store float %div237, float* %ref.tmp236, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp234, %class.btVector3* nonnull align 4 dereferenceable(16) %call235, float* nonnull align 4 dereferenceable(4) %ref.tmp236)
  %201 = bitcast %class.btVector3* %sepNormalWorldSpace227 to i8*
  %202 = bitcast %class.btVector3* %ref.tmp234 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %201, i8* align 4 %202, i32 16, i1 false)
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %sepNormalWorldSpace227, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1)
  store i8 1, i8* %perturbeA, align 1
  store float 0x3FD921FB60000000, float* %angleLimit, align 4
  %203 = load %class.btConvexShape*, %class.btConvexShape** %min0, align 4
  %204 = bitcast %class.btConvexShape* %203 to %class.btCollisionShape*
  %205 = bitcast %class.btCollisionShape* %204 to float (%class.btCollisionShape*)***
  %vtable238 = load float (%class.btCollisionShape*)**, float (%class.btCollisionShape*)*** %205, align 4
  %vfn239 = getelementptr inbounds float (%class.btCollisionShape*)*, float (%class.btCollisionShape*)** %vtable238, i64 4
  %206 = load float (%class.btCollisionShape*)*, float (%class.btCollisionShape*)** %vfn239, align 4
  %call240 = call float %206(%class.btCollisionShape* %204)
  store float %call240, float* %radiusA, align 4
  %207 = load %class.btConvexShape*, %class.btConvexShape** %min1, align 4
  %208 = bitcast %class.btConvexShape* %207 to %class.btCollisionShape*
  %209 = bitcast %class.btCollisionShape* %208 to float (%class.btCollisionShape*)***
  %vtable241 = load float (%class.btCollisionShape*)**, float (%class.btCollisionShape*)*** %209, align 4
  %vfn242 = getelementptr inbounds float (%class.btCollisionShape*)*, float (%class.btCollisionShape*)** %vtable241, i64 4
  %210 = load float (%class.btCollisionShape*)*, float (%class.btCollisionShape*)** %vfn242, align 4
  %call243 = call float %210(%class.btCollisionShape* %208)
  store float %call243, float* %radiusB, align 4
  %211 = load float, float* %radiusA, align 4
  %212 = load float, float* %radiusB, align 4
  %cmp244 = fcmp olt float %211, %212
  br i1 %cmp244, label %if.then245, label %if.else247

if.then245:                                       ; preds = %if.then233
  %213 = load float, float* @gContactBreakingThreshold, align 4
  %214 = load float, float* %radiusA, align 4
  %div246 = fdiv float %213, %214
  store float %div246, float* %perturbeAngle, align 4
  store i8 1, i8* %perturbeA, align 1
  br label %if.end249

if.else247:                                       ; preds = %if.then233
  %215 = load float, float* @gContactBreakingThreshold, align 4
  %216 = load float, float* %radiusB, align 4
  %div248 = fdiv float %215, %216
  store float %div248, float* %perturbeAngle, align 4
  store i8 0, i8* %perturbeA, align 1
  br label %if.end249

if.end249:                                        ; preds = %if.else247, %if.then245
  %217 = load float, float* %perturbeAngle, align 4
  %cmp250 = fcmp ogt float %217, 0x3FD921FB60000000
  br i1 %cmp250, label %if.then251, label %if.end252

if.then251:                                       ; preds = %if.end249
  store float 0x3FD921FB60000000, float* %perturbeAngle, align 4
  br label %if.end252

if.end252:                                        ; preds = %if.then251, %if.end249
  %call253 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %unPerturbedTransform)
  %218 = load i8, i8* %perturbeA, align 1
  %tobool254 = trunc i8 %218 to i1
  br i1 %tobool254, label %if.then255, label %if.else258

if.then255:                                       ; preds = %if.end252
  %m_transformA256 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 0
  %call257 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %unPerturbedTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transformA256)
  br label %if.end261

if.else258:                                       ; preds = %if.end252
  %m_transformB259 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 1
  %call260 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %unPerturbedTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transformB259)
  br label %if.end261

if.end261:                                        ; preds = %if.else258, %if.then255
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end261
  %219 = load i32, i32* %i, align 4
  %m_numPerturbationIterations262 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 7
  %220 = load i32, i32* %m_numPerturbationIterations262, align 4
  %cmp263 = icmp slt i32 %219, %220
  br i1 %cmp263, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call264 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %v0)
  %cmp265 = fcmp ogt float %call264, 0x3E80000000000000
  br i1 %cmp265, label %if.then266, label %if.end308

if.then266:                                       ; preds = %for.body
  %call267 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %perturbeRot, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, float* nonnull align 4 dereferenceable(4) %perturbeAngle)
  %221 = load i32, i32* %i, align 4
  %conv = sitofp i32 %221 to float
  %m_numPerturbationIterations268 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 7
  %222 = load i32, i32* %m_numPerturbationIterations268, align 4
  %conv269 = sitofp i32 %222 to float
  %div270 = fdiv float 0x401921FB60000000, %conv269
  %mul271 = fmul float %conv, %div270
  store float %mul271, float* %iterationAngle, align 4
  %call272 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %rotq, %class.btVector3* nonnull align 4 dereferenceable(16) %sepNormalWorldSpace227, float* nonnull align 4 dereferenceable(4) %iterationAngle)
  %223 = load i8, i8* %perturbeA, align 1
  %tobool273 = trunc i8 %223 to i1
  br i1 %tobool273, label %if.then274, label %if.else287

if.then274:                                       ; preds = %if.then266
  %m_transformA275 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 0
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp280, %class.btQuaternion* %rotq)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp279, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp280, %class.btQuaternion* nonnull align 4 dereferenceable(16) %perturbeRot)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp278, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp279, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotq)
  %call281 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %ref.tmp277, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp278)
  %224 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call282 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %224)
  %call283 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call282)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp276, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp277, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call283)
  call void @_ZN11btTransform8setBasisERK11btMatrix3x3(%class.btTransform* %m_transformA275, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp276)
  %225 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call284 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %225)
  %m_transformB285 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 1
  %call286 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformB285, %class.btTransform* nonnull align 4 dereferenceable(64) %call284)
  br label %if.end300

if.else287:                                       ; preds = %if.then266
  %226 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call288 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %226)
  %m_transformA289 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 0
  %call290 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformA289, %class.btTransform* nonnull align 4 dereferenceable(64) %call288)
  %m_transformB291 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 1
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp296, %class.btQuaternion* %rotq)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp295, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp296, %class.btQuaternion* nonnull align 4 dereferenceable(16) %perturbeRot)
  call void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* sret align 4 %ref.tmp294, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp295, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotq)
  %call297 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %ref.tmp293, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp294)
  %227 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call298 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %227)
  %call299 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call298)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp292, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp293, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call299)
  call void @_ZN11btTransform8setBasisERK11btMatrix3x3(%class.btTransform* %m_transformB291, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp292)
  br label %if.end300

if.end300:                                        ; preds = %if.else287, %if.then274
  %228 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_transformA301 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 0
  %m_transformB302 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 1
  %229 = load i8, i8* %perturbeA, align 1
  %tobool303 = trunc i8 %229 to i1
  %230 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %m_debugDraw304 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %230, i32 0, i32 5
  %231 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDraw304, align 4
  %call305 = call %struct.btPerturbedContactResult* @_ZN24btPerturbedContactResultC2EP16btManifoldResultRK11btTransformS4_S4_bP12btIDebugDraw(%struct.btPerturbedContactResult* %perturbedResultOut, %class.btManifoldResult* %228, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transformA301, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transformB302, %class.btTransform* nonnull align 4 dereferenceable(64) %unPerturbedTransform, i1 zeroext %tobool303, %class.btIDebugDraw* %231)
  %232 = bitcast %struct.btPerturbedContactResult* %perturbedResultOut to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %233 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %m_debugDraw306 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %233, i32 0, i32 5
  %234 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDraw306, align 4
  call void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector* %gjkPairDetector, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %232, %class.btIDebugDraw* %234, i1 zeroext false)
  %call307 = call %struct.btPerturbedContactResult* @_ZN24btPerturbedContactResultD2Ev(%struct.btPerturbedContactResult* %perturbedResultOut) #9
  br label %if.end308

if.end308:                                        ; preds = %if.end300, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end308
  %235 = load i32, i32* %i, align 4
  %inc = add nsw i32 %235, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end309

if.end309:                                        ; preds = %for.end, %if.then224
  br label %if.end310

if.end310:                                        ; preds = %if.end309, %land.lhs.true220, %if.end217
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup311

cleanup311:                                       ; preds = %if.end310, %cleanup
  %call312 = call %class.btGjkPairDetector* @_ZN17btGjkPairDetectorD2Ev(%class.btGjkPairDetector* %gjkPairDetector) #9
  %cleanup.dest313 = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest313, label %unreachable [
    i32 0, label %cleanup.cont314
    i32 1, label %if.end318
  ]

cleanup.cont314:                                  ; preds = %cleanup311
  %m_ownManifold315 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 4
  %236 = load i8, i8* %m_ownManifold315, align 4
  %tobool316 = trunc i8 %236 to i1
  br i1 %tobool316, label %if.then317, label %if.end318

if.then317:                                       ; preds = %cleanup.cont314
  %237 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  call void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %237)
  br label %if.end318

if.end318:                                        ; preds = %if.end29, %if.end54, %if.end79, %cleanup311, %if.then317, %cleanup.cont314
  ret void

unreachable:                                      ; preds = %cleanup311
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %this, %class.btPersistentManifold* %manifoldPtr) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  store %class.btPersistentManifold* %manifoldPtr, %class.btPersistentManifold** %manifoldPtr.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifoldPtr.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  store %class.btPersistentManifold* %0, %class.btPersistentManifold** %m_manifoldPtr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_shape, align 4
  ret %class.btCollisionShape* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4
  ret i32 %0
}

declare float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold*) #4

; Function Attrs: noinline optnone
define internal float @_ZL22capsuleCapsuleDistanceR9btVector3S0_ffffiiRK11btTransformS3_f(%class.btVector3* nonnull align 4 dereferenceable(16) %normalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %pointOnB, float %capsuleLengthA, float %capsuleRadiusA, float %capsuleLengthB, float %capsuleRadiusB, i32 %capsuleAxisA, i32 %capsuleAxisB, %class.btTransform* nonnull align 4 dereferenceable(64) %transformA, %class.btTransform* nonnull align 4 dereferenceable(64) %transformB, float %distanceThreshold) #2 {
entry:
  %retval = alloca float, align 4
  %normalOnB.addr = alloca %class.btVector3*, align 4
  %pointOnB.addr = alloca %class.btVector3*, align 4
  %capsuleLengthA.addr = alloca float, align 4
  %capsuleRadiusA.addr = alloca float, align 4
  %capsuleLengthB.addr = alloca float, align 4
  %capsuleRadiusB.addr = alloca float, align 4
  %capsuleAxisA.addr = alloca i32, align 4
  %capsuleAxisB.addr = alloca i32, align 4
  %transformA.addr = alloca %class.btTransform*, align 4
  %transformB.addr = alloca %class.btTransform*, align 4
  %distanceThreshold.addr = alloca float, align 4
  %directionA = alloca %class.btVector3, align 4
  %translationA = alloca %class.btVector3, align 4
  %directionB = alloca %class.btVector3, align 4
  %translationB = alloca %class.btVector3, align 4
  %translation = alloca %class.btVector3, align 4
  %ptsVector = alloca %class.btVector3, align 4
  %offsetA = alloca %class.btVector3, align 4
  %offsetB = alloca %class.btVector3, align 4
  %tA = alloca float, align 4
  %tB = alloca float, align 4
  %distance = alloca float, align 4
  %lenSqr = alloca float, align 4
  %q = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  store %class.btVector3* %normalOnB, %class.btVector3** %normalOnB.addr, align 4
  store %class.btVector3* %pointOnB, %class.btVector3** %pointOnB.addr, align 4
  store float %capsuleLengthA, float* %capsuleLengthA.addr, align 4
  store float %capsuleRadiusA, float* %capsuleRadiusA.addr, align 4
  store float %capsuleLengthB, float* %capsuleLengthB.addr, align 4
  store float %capsuleRadiusB, float* %capsuleRadiusB.addr, align 4
  store i32 %capsuleAxisA, i32* %capsuleAxisA.addr, align 4
  store i32 %capsuleAxisB, i32* %capsuleAxisB.addr, align 4
  store %class.btTransform* %transformA, %class.btTransform** %transformA.addr, align 4
  store %class.btTransform* %transformB, %class.btTransform** %transformB.addr, align 4
  store float %distanceThreshold, float* %distanceThreshold.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transformA.addr, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %0)
  %1 = load i32, i32* %capsuleAxisA.addr, align 4
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %directionA, %class.btMatrix3x3* %call, i32 %1)
  %2 = load %class.btTransform*, %class.btTransform** %transformA.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %2)
  %3 = bitcast %class.btVector3* %translationA to i8*
  %4 = bitcast %class.btVector3* %call1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %5 = load %class.btTransform*, %class.btTransform** %transformB.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %5)
  %6 = load i32, i32* %capsuleAxisB.addr, align 4
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %directionB, %class.btMatrix3x3* %call2, i32 %6)
  %7 = load %class.btTransform*, %class.btTransform** %transformB.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %7)
  %8 = bitcast %class.btVector3* %translationB to i8*
  %9 = bitcast %class.btVector3* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %translation, %class.btVector3* nonnull align 4 dereferenceable(16) %translationB, %class.btVector3* nonnull align 4 dereferenceable(16) %translationA)
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ptsVector)
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %offsetA)
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %offsetB)
  %10 = load float, float* %capsuleLengthA.addr, align 4
  %11 = load float, float* %capsuleLengthB.addr, align 4
  call void @_ZL21segmentsClosestPointsR9btVector3S0_S0_RfS1_RKS_S3_fS3_f(%class.btVector3* nonnull align 4 dereferenceable(16) %ptsVector, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetA, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetB, float* nonnull align 4 dereferenceable(4) %tA, float* nonnull align 4 dereferenceable(4) %tB, %class.btVector3* nonnull align 4 dereferenceable(16) %translation, %class.btVector3* nonnull align 4 dereferenceable(16) %directionA, float %10, %class.btVector3* nonnull align 4 dereferenceable(16) %directionB, float %11)
  %call7 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ptsVector)
  %12 = load float, float* %capsuleRadiusA.addr, align 4
  %sub = fsub float %call7, %12
  %13 = load float, float* %capsuleRadiusB.addr, align 4
  %sub8 = fsub float %sub, %13
  store float %sub8, float* %distance, align 4
  %14 = load float, float* %distance, align 4
  %15 = load float, float* %distanceThreshold.addr, align 4
  %cmp = fcmp ogt float %14, %15
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %16 = load float, float* %distance, align 4
  store float %16, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %call9 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ptsVector)
  store float %call9, float* %lenSqr, align 4
  %17 = load float, float* %lenSqr, align 4
  %cmp10 = fcmp ole float %17, 0x3D10000000000000
  br i1 %cmp10, label %if.then11, label %if.else

if.then11:                                        ; preds = %if.end
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %q)
  %18 = load %class.btVector3*, %class.btVector3** %normalOnB.addr, align 4
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %directionA, %class.btVector3* nonnull align 4 dereferenceable(16) %18, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  br label %if.end15

if.else:                                          ; preds = %if.end
  %19 = load float, float* %lenSqr, align 4
  %call14 = call float @_Z6btSqrtf(float %19)
  %div = fdiv float 1.000000e+00, %call14
  %fneg = fneg float %div
  store float %fneg, float* %ref.tmp13, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ptsVector, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  %20 = load %class.btVector3*, %class.btVector3** %normalOnB.addr, align 4
  %21 = bitcast %class.btVector3* %20 to i8*
  %22 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  br label %if.end15

if.end15:                                         ; preds = %if.else, %if.then11
  %23 = load %class.btTransform*, %class.btTransform** %transformB.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %23)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %call18, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetB)
  %24 = load %class.btVector3*, %class.btVector3** %normalOnB.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp19, %class.btVector3* nonnull align 4 dereferenceable(16) %24, float* nonnull align 4 dereferenceable(4) %capsuleRadiusB.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp19)
  %25 = load %class.btVector3*, %class.btVector3** %pointOnB.addr, align 4
  %26 = bitcast %class.btVector3* %25 to i8*
  %27 = bitcast %class.btVector3* %ref.tmp16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false)
  %28 = load float, float* %distance, align 4
  store float %28, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end15, %if.then
  %29 = load float, float* %retval, align 4
  ret float %29
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %0, i32 0, i32 2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_implicitShapeDimensions)
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_upAxis, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx, align 4
  ret float %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  %radiusAxis = alloca i32, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_upAxis, align 4
  %add = add nsw i32 %0, 2
  %rem = srem i32 %add, 3
  store i32 %rem, i32* %radiusAxis, align 4
  %1 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %1, i32 0, i32 2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_implicitShapeDimensions)
  %2 = load i32, i32* %radiusAxis, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %2
  %3 = load float, float* %arrayidx, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_upAxis, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %0 = load %class.btTransform*, %class.btTransform** %m_worldTransform, align 4
  ret %class.btTransform* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %isSwapped = alloca i8, align 1
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %call = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %if.end20

if.end:                                           ; preds = %entry
  %m_manifoldPtr2 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr2, align 4
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %1)
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4
  %call4 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %2)
  %cmp = icmp ne %class.btCollisionObject* %call3, %call4
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %isSwapped, align 1
  %3 = load i8, i8* %isSwapped, align 1
  %tobool5 = trunc i8 %3 to i1
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.end
  %m_manifoldPtr7 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr7, align 4
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4
  %call8 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %5)
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call8)
  %m_body0Wrap10 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap10, align 4
  %call11 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %6)
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call11)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %4, %class.btTransform* nonnull align 4 dereferenceable(64) %call9, %class.btTransform* nonnull align 4 dereferenceable(64) %call12)
  br label %if.end20

if.else:                                          ; preds = %if.end
  %m_manifoldPtr13 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr13, align 4
  %m_body0Wrap14 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap14, align 4
  %call15 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %8)
  %call16 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call15)
  %m_body1Wrap17 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap17, align 4
  %call18 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %9)
  %call19 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call18)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %7, %class.btTransform* nonnull align 4 dereferenceable(64) %call16, %class.btTransform* nonnull align 4 dereferenceable(64) %call19)
  br label %if.end20

if.end20:                                         ; preds = %if.then, %if.else, %if.then6
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK13btSphereShape9getRadiusEv(%class.btSphereShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btSphereShape*, align 4
  store %class.btSphereShape* %this, %class.btSphereShape** %this.addr, align 4
  %this1 = load %class.btSphereShape*, %class.btSphereShape** %this.addr, align 4
  %0 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %0, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_implicitShapeDimensions)
  %1 = load float, float* %call, align 4
  %2 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %2, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling)
  %3 = load float, float* %call2, align 4
  %mul = fmul float %1, %3
  ret float %mul
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* @_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev(%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %this.addr, align 4
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 0
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transformA)
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 1
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transformB)
  %m_maximumDistanceSquared = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 2
  store float 0x43ABC16D60000000, float* %m_maximumDistanceSquared, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btVoronoiSimplexSolver* @_ZN22btVoronoiSimplexSolverC2Ev(%class.btVoronoiSimplexSolver* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btVoronoiSimplexSolver*, align 4
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  store %class.btVoronoiSimplexSolver* %this1, %class.btVoronoiSimplexSolver** %retval, align 4
  %m_simplexVectorW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexVectorW, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 5
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_simplexPointsP = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 2
  %array.begin2 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsP, i32 0, i32 0
  %arrayctor.end3 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin2, i32 5
  br label %arrayctor.loop4

arrayctor.loop4:                                  ; preds = %arrayctor.loop4, %arrayctor.cont
  %arrayctor.cur5 = phi %class.btVector3* [ %array.begin2, %arrayctor.cont ], [ %arrayctor.next7, %arrayctor.loop4 ]
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur5)
  %arrayctor.next7 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur5, i32 1
  %arrayctor.done8 = icmp eq %class.btVector3* %arrayctor.next7, %arrayctor.end3
  br i1 %arrayctor.done8, label %arrayctor.cont9, label %arrayctor.loop4

arrayctor.cont9:                                  ; preds = %arrayctor.loop4
  %m_simplexPointsQ = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 3
  %array.begin10 = getelementptr inbounds [5 x %class.btVector3], [5 x %class.btVector3]* %m_simplexPointsQ, i32 0, i32 0
  %arrayctor.end11 = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin10, i32 5
  br label %arrayctor.loop12

arrayctor.loop12:                                 ; preds = %arrayctor.loop12, %arrayctor.cont9
  %arrayctor.cur13 = phi %class.btVector3* [ %array.begin10, %arrayctor.cont9 ], [ %arrayctor.next15, %arrayctor.loop12 ]
  %call14 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur13)
  %arrayctor.next15 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur13, i32 1
  %arrayctor.done16 = icmp eq %class.btVector3* %arrayctor.next15, %arrayctor.end11
  br i1 %arrayctor.done16, label %arrayctor.cont17, label %arrayctor.loop12

arrayctor.cont17:                                 ; preds = %arrayctor.loop12
  %m_cachedP1 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 4
  %call18 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_cachedP1)
  %m_cachedP2 = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 5
  %call19 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_cachedP2)
  %m_cachedV = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 6
  %call20 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_cachedV)
  %m_lastW = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 7
  %call21 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lastW)
  %m_equalVertexThreshold = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 8
  store float 0x3F1A36E2E0000000, float* %m_equalVertexThreshold, align 4
  %m_cachedBC = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 11
  %call22 = call %struct.btSubSimplexClosestResult* @_ZN25btSubSimplexClosestResultC2Ev(%struct.btSubSimplexClosestResult* %m_cachedBC)
  %0 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %retval, align 4
  ret %class.btVoronoiSimplexSolver* %0
}

declare %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* returned, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btGjkPairDetector13setMinkowskiAEPK13btConvexShape(%class.btGjkPairDetector* %this, %class.btConvexShape* %minkA) #1 comdat {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  %minkA.addr = alloca %class.btConvexShape*, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  store %class.btConvexShape* %minkA, %class.btConvexShape** %minkA.addr, align 4
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = load %class.btConvexShape*, %class.btConvexShape** %minkA.addr, align 4
  %m_minkowskiA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 4
  store %class.btConvexShape* %0, %class.btConvexShape** %m_minkowskiA, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btGjkPairDetector13setMinkowskiBEPK13btConvexShape(%class.btGjkPairDetector* %this, %class.btConvexShape* %minkB) #1 comdat {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  %minkB.addr = alloca %class.btConvexShape*, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  store %class.btConvexShape* %minkB, %class.btConvexShape** %minkB.addr, align 4
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = load %class.btConvexShape*, %class.btConvexShape** %minkB.addr, align 4
  %m_minkowskiB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 5
  store %class.btConvexShape* %0, %class.btConvexShape** %m_minkowskiB, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionShape12isPolyhedralEv(%class.btCollisionShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %call = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this1)
  %call2 = call zeroext i1 @_ZN17btBroadphaseProxy12isPolyhedralEi(i32 %call)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define internal %struct.btDummyResult* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN13btDummyResultC2Ev(%struct.btDummyResult* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btDummyResult*, align 4
  store %struct.btDummyResult* %this, %struct.btDummyResult** %this.addr, align 4
  %this1 = load %struct.btDummyResult*, %struct.btDummyResult** %this.addr, align 4
  %0 = bitcast %struct.btDummyResult* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #9
  %1 = bitcast %struct.btDummyResult* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultE13btDummyResult, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %struct.btDummyResult* %this1
}

; Function Attrs: noinline optnone
define internal %struct.btWithoutMarginResult* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN21btWithoutMarginResultC2EPN36btDiscreteCollisionDetectorInterface6ResultEff(%struct.btWithoutMarginResult* returned %this, %"struct.btDiscreteCollisionDetectorInterface::Result"* %result, float %marginOnA, float %marginOnB) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.btWithoutMarginResult*, align 4
  %result.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %marginOnA.addr = alloca float, align 4
  %marginOnB.addr = alloca float, align 4
  store %struct.btWithoutMarginResult* %this, %struct.btWithoutMarginResult** %this.addr, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %result, %"struct.btDiscreteCollisionDetectorInterface::Result"** %result.addr, align 4
  store float %marginOnA, float* %marginOnA.addr, align 4
  store float %marginOnB, float* %marginOnB.addr, align 4
  %this1 = load %struct.btWithoutMarginResult*, %struct.btWithoutMarginResult** %this.addr, align 4
  %0 = bitcast %struct.btWithoutMarginResult* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #9
  %1 = bitcast %struct.btWithoutMarginResult* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultE21btWithoutMarginResult, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_originalResult = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 1
  %2 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %result.addr, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %2, %"struct.btDiscreteCollisionDetectorInterface::Result"** %m_originalResult, align 4
  %m_reportedNormalOnWorld = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 2
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_reportedNormalOnWorld)
  %m_marginOnA = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 3
  %3 = load float, float* %marginOnA.addr, align 4
  store float %3, float* %m_marginOnA, align 4
  %m_marginOnB = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 4
  %4 = load float, float* %marginOnB.addr, align 4
  store float %4, float* %m_marginOnB, align 4
  %m_foundResult = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 6
  store i8 0, i8* %m_foundResult, align 4
  ret %struct.btWithoutMarginResult* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexPolyhedron* @_ZNK23btPolyhedralConvexShape19getConvexPolyhedronEv(%class.btPolyhedralConvexShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  %m_polyhedron = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron, align 4
  ret %class.btConvexPolyhedron* %0
}

declare zeroext i1 @_ZN27btPolyhedralContactClipping18findSeparatingAxisERK18btConvexPolyhedronS2_RK11btTransformS5_R9btVector3RN36btDiscreteCollisionDetectorInterface6ResultE(%class.btConvexPolyhedron* nonnull align 1, %class.btConvexPolyhedron* nonnull align 1, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4)) #4

declare void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132), %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4), %class.btIDebugDraw*, i1 zeroext) unnamed_addr #4

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %class.btVector3*, %class.btVector3** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 %15
  %16 = bitcast %class.btVector3* %arrayidx10 to i8*
  %call11 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %16)
  %17 = bitcast i8* %call11 to %class.btVector3*
  %18 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4
  %19 = bitcast %class.btVector3* %17 to i8*
  %20 = bitcast %class.btVector3* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

declare void @_ZN27btPolyhedralContactClipping19clipHullAgainstHullERK9btVector3RK18btConvexPolyhedronS5_RK11btTransformS8_ffR20btAlignedObjectArrayIS0_ESB_RN36btDiscreteCollisionDetectorInterface6ResultE(%class.btVector3* nonnull align 4 dereferenceable(16), %class.btConvexPolyhedron* nonnull align 1, %class.btConvexPolyhedron* nonnull align 1, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), float, float, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17), %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4)) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btVector3* %_Val, %class.btVector3** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 %2
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %3)
  %4 = bitcast i8* %call5 to %class.btVector3*
  %5 = load %class.btVector3*, %class.btVector3** %_Val.addr, align 4
  %6 = bitcast %class.btVector3* %4 to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK17btGjkPairDetector23getCachedSeparatingAxisEv(%class.btGjkPairDetector* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %m_cachedSeparatingAxis = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  ret %class.btVector3* %m_cachedSeparatingAxis
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK17btGjkPairDetector27getCachedSeparatingDistanceEv(%class.btGjkPairDetector* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %m_cachedSeparatingDistance = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 11
  %0 = load float, float* %m_cachedSeparatingDistance, align 4
  ret float %0
}

declare void @_ZN27btPolyhedralContactClipping19clipFaceAgainstHullERK9btVector3RK18btConvexPolyhedronRK11btTransformR20btAlignedObjectArrayIS0_ESB_ffRN36btDiscreteCollisionDetectorInterface6ResultE(%class.btVector3* nonnull align 4 dereferenceable(16), %class.btConvexPolyhedron* nonnull align 1, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17), float, float, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4)) #4

; Function Attrs: noinline nounwind optnone
define internal %struct.btWithoutMarginResult* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN21btWithoutMarginResultD2Ev(%struct.btWithoutMarginResult* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btWithoutMarginResult*, align 4
  store %struct.btWithoutMarginResult* %this, %struct.btWithoutMarginResult** %this.addr, align 4
  %this1 = load %struct.btWithoutMarginResult*, %struct.btWithoutMarginResult** %this.addr, align 4
  %0 = bitcast %struct.btWithoutMarginResult* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #9
  ret %struct.btWithoutMarginResult* %this1
}

; Function Attrs: noinline nounwind optnone
define internal %struct.btDummyResult* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN13btDummyResultD2Ev(%struct.btDummyResult* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btDummyResult*, align 4
  store %struct.btDummyResult* %this, %struct.btDummyResult** %this.addr, align 4
  %this1 = load %struct.btDummyResult*, %struct.btDummyResult** %this.addr, align 4
  %0 = bitcast %struct.btDummyResult* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #9
  ret %struct.btDummyResult* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPersistentManifold* @_ZN16btManifoldResult21getPersistentManifoldEv(%class.btManifoldResult* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  ret %class.btPersistentManifold* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %n.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  %a = alloca float, align 4
  %k = alloca float, align 4
  %a42 = alloca float, align 4
  %k54 = alloca float, align 4
  store %class.btVector3* %n, %class.btVector3** %n.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 2
  %1 = load float, float* %arrayidx, align 4
  %call1 = call float @_Z6btFabsf(float %1)
  %cmp = fcmp ogt float %call1, 0x3FE6A09E60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %4 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %mul = fmul float %3, %5
  %6 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %7 = load float, float* %arrayidx7, align 4
  %8 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %9 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %7, %9
  %add = fadd float %mul, %mul10
  store float %add, float* %a, align 4
  %10 = load float, float* %a, align 4
  %call11 = call float @_Z6btSqrtf(float %10)
  %div = fdiv float 1.000000e+00, %call11
  store float %div, float* %k, align 4
  %11 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float 0.000000e+00, float* %arrayidx13, align 4
  %12 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %12)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %13 = load float, float* %arrayidx15, align 4
  %fneg = fneg float %13
  %14 = load float, float* %k, align 4
  %mul16 = fmul float %fneg, %14
  %15 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  store float %mul16, float* %arrayidx18, align 4
  %16 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %16)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %17 = load float, float* %arrayidx20, align 4
  %18 = load float, float* %k, align 4
  %mul21 = fmul float %17, %18
  %19 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %19)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 2
  store float %mul21, float* %arrayidx23, align 4
  %20 = load float, float* %a, align 4
  %21 = load float, float* %k, align 4
  %mul24 = fmul float %20, %21
  %22 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %22)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %mul24, float* %arrayidx26, align 4
  %23 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %23)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  %24 = load float, float* %arrayidx28, align 4
  %fneg29 = fneg float %24
  %25 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %25)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %26 = load float, float* %arrayidx31, align 4
  %mul32 = fmul float %fneg29, %26
  %27 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float %mul32, float* %arrayidx34, align 4
  %28 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %28)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %29 = load float, float* %arrayidx36, align 4
  %30 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %30)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %31 = load float, float* %arrayidx38, align 4
  %mul39 = fmul float %29, %31
  %32 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  store float %mul39, float* %arrayidx41, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %33 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %33)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 0
  %34 = load float, float* %arrayidx44, align 4
  %35 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %35)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %36 = load float, float* %arrayidx46, align 4
  %mul47 = fmul float %34, %36
  %37 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call48 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %37)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 1
  %38 = load float, float* %arrayidx49, align 4
  %39 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %39)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %40 = load float, float* %arrayidx51, align 4
  %mul52 = fmul float %38, %40
  %add53 = fadd float %mul47, %mul52
  store float %add53, float* %a42, align 4
  %41 = load float, float* %a42, align 4
  %call55 = call float @_Z6btSqrtf(float %41)
  %div56 = fdiv float 1.000000e+00, %call55
  store float %div56, float* %k54, align 4
  %42 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %43 = load float, float* %arrayidx58, align 4
  %fneg59 = fneg float %43
  %44 = load float, float* %k54, align 4
  %mul60 = fmul float %fneg59, %44
  %45 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %45)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 0
  store float %mul60, float* %arrayidx62, align 4
  %46 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %46)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 0
  %47 = load float, float* %arrayidx64, align 4
  %48 = load float, float* %k54, align 4
  %mul65 = fmul float %47, %48
  %49 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %49)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %mul65, float* %arrayidx67, align 4
  %50 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %50)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  store float 0.000000e+00, float* %arrayidx69, align 4
  %51 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %51)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 2
  %52 = load float, float* %arrayidx71, align 4
  %fneg72 = fneg float %52
  %53 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %53)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 1
  %54 = load float, float* %arrayidx74, align 4
  %mul75 = fmul float %fneg72, %54
  %55 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  store float %mul75, float* %arrayidx77, align 4
  %56 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %56)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 2
  %57 = load float, float* %arrayidx79, align 4
  %58 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %58)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %59 = load float, float* %arrayidx81, align 4
  %mul82 = fmul float %57, %59
  %60 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %60)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  store float %mul82, float* %arrayidx84, align 4
  %61 = load float, float* %a42, align 4
  %62 = load float, float* %k54, align 4
  %mul85 = fmul float %61, %62
  %63 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %63)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  store float %mul85, float* %arrayidx87, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_axis, float* nonnull align 4 dereferenceable(4) %_angle) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btVector3* %_axis, %class.btVector3** %_axis.addr, align 4
  store float* %_angle, float** %_angle.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  %1 = load %class.btVector3*, %class.btVector3** %_axis.addr, align 4
  %2 = load float*, float** %_angle.addr, align 4
  call void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %2)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btTransform8setBasisERK11btMatrix3x3(%class.btTransform* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %basis) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %basis.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %basis, %class.btMatrix3x3** %basis.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %basis.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionS1_(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q2) #2 comdat {
entry:
  %q1.addr = alloca %class.btQuaternion*, align 4
  %q2.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  store %class.btQuaternion* %q1, %class.btQuaternion** %q1.addr, align 4
  store %class.btQuaternion* %q2, %class.btQuaternion** %q2.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call1, align 4
  %mul = fmul float %2, %5
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %7)
  %8 = load float, float* %call2, align 4
  %9 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %10 = bitcast %class.btQuaternion* %9 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %10)
  %11 = load float, float* %call3, align 4
  %mul4 = fmul float %8, %11
  %add = fadd float %mul, %mul4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %13)
  %14 = load float, float* %call5, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %16)
  %17 = load float, float* %call6, align 4
  %mul7 = fmul float %14, %17
  %add8 = fadd float %add, %mul7
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %22 = bitcast %class.btQuaternion* %21 to %class.btQuadWord*
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %22)
  %23 = load float, float* %call10, align 4
  %mul11 = fmul float %20, %23
  %sub = fsub float %add8, %mul11
  store float %sub, float* %ref.tmp, align 4
  %24 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %25 = bitcast %class.btQuaternion* %24 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %25)
  %26 = load float, float* %call13, align 4
  %27 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %28 = bitcast %class.btQuaternion* %27 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %28)
  %29 = load float, float* %call14, align 4
  %mul15 = fmul float %26, %29
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %31)
  %32 = load float, float* %call16, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %34)
  %35 = load float, float* %call17, align 4
  %mul18 = fmul float %32, %35
  %add19 = fadd float %mul15, %mul18
  %36 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %37 = bitcast %class.btQuaternion* %36 to %class.btQuadWord*
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %37)
  %38 = load float, float* %call20, align 4
  %39 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %40 = bitcast %class.btQuaternion* %39 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %40)
  %41 = load float, float* %call21, align 4
  %mul22 = fmul float %38, %41
  %add23 = fadd float %add19, %mul22
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %43)
  %44 = load float, float* %call24, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %46)
  %47 = load float, float* %call25, align 4
  %mul26 = fmul float %44, %47
  %sub27 = fsub float %add23, %mul26
  store float %sub27, float* %ref.tmp12, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %49)
  %50 = load float, float* %call29, align 4
  %51 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %52 = bitcast %class.btQuaternion* %51 to %class.btQuadWord*
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %52)
  %53 = load float, float* %call30, align 4
  %mul31 = fmul float %50, %53
  %54 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %55 = bitcast %class.btQuaternion* %54 to %class.btQuadWord*
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %55)
  %56 = load float, float* %call32, align 4
  %57 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %58 = bitcast %class.btQuaternion* %57 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %58)
  %59 = load float, float* %call33, align 4
  %mul34 = fmul float %56, %59
  %add35 = fadd float %mul31, %mul34
  %60 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %61 = bitcast %class.btQuaternion* %60 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %61)
  %62 = load float, float* %call36, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %64)
  %65 = load float, float* %call37, align 4
  %mul38 = fmul float %62, %65
  %add39 = fadd float %add35, %mul38
  %66 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %67 = bitcast %class.btQuaternion* %66 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %67)
  %68 = load float, float* %call40, align 4
  %69 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %70 = bitcast %class.btQuaternion* %69 to %class.btQuadWord*
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %70)
  %71 = load float, float* %call41, align 4
  %mul42 = fmul float %68, %71
  %sub43 = fsub float %add39, %mul42
  store float %sub43, float* %ref.tmp28, align 4
  %72 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %73 = bitcast %class.btQuaternion* %72 to %class.btQuadWord*
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %73)
  %74 = load float, float* %call45, align 4
  %75 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %76 = bitcast %class.btQuaternion* %75 to %class.btQuadWord*
  %call46 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %76)
  %77 = load float, float* %call46, align 4
  %mul47 = fmul float %74, %77
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %79)
  %80 = load float, float* %call48, align 4
  %81 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %82 = bitcast %class.btQuaternion* %81 to %class.btQuadWord*
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %82)
  %83 = load float, float* %call49, align 4
  %mul50 = fmul float %80, %83
  %sub51 = fsub float %mul47, %mul50
  %84 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %85 = bitcast %class.btQuaternion* %84 to %class.btQuadWord*
  %call52 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %85)
  %86 = load float, float* %call52, align 4
  %87 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %88 = bitcast %class.btQuaternion* %87 to %class.btQuadWord*
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %88)
  %89 = load float, float* %call53, align 4
  %mul54 = fmul float %86, %89
  %sub55 = fsub float %sub51, %mul54
  %90 = load %class.btQuaternion*, %class.btQuaternion** %q1.addr, align 4
  %91 = bitcast %class.btQuaternion* %90 to %class.btQuadWord*
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %91)
  %92 = load float, float* %call56, align 4
  %93 = load %class.btQuaternion*, %class.btQuaternion** %q2.addr, align 4
  %94 = bitcast %class.btQuaternion* %93 to %class.btQuadWord*
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %94)
  %95 = load float, float* %call57, align 4
  %mul58 = fmul float %92, %95
  %sub59 = fsub float %sub55, %mul58
  store float %sub59, float* %ref.tmp44, align 4
  %call60 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp44)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats3 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %3 = load float, float* %arrayidx4, align 4
  %fneg5 = fneg float %3
  store float %fneg5, float* %ref.tmp2, align 4
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %4, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %5 = load float, float* %arrayidx8, align 4
  %fneg9 = fneg float %5
  store float %fneg9, float* %ref.tmp6, align 4
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats10 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 3
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btPerturbedContactResult* @_ZN24btPerturbedContactResultC2EP16btManifoldResultRK11btTransformS4_S4_bP12btIDebugDraw(%struct.btPerturbedContactResult* returned %this, %class.btManifoldResult* %originalResult, %class.btTransform* nonnull align 4 dereferenceable(64) %transformA, %class.btTransform* nonnull align 4 dereferenceable(64) %transformB, %class.btTransform* nonnull align 4 dereferenceable(64) %unPerturbedTransform, i1 zeroext %perturbA, %class.btIDebugDraw* %debugDrawer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btPerturbedContactResult*, align 4
  %originalResult.addr = alloca %class.btManifoldResult*, align 4
  %transformA.addr = alloca %class.btTransform*, align 4
  %transformB.addr = alloca %class.btTransform*, align 4
  %unPerturbedTransform.addr = alloca %class.btTransform*, align 4
  %perturbA.addr = alloca i8, align 1
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  store %struct.btPerturbedContactResult* %this, %struct.btPerturbedContactResult** %this.addr, align 4
  store %class.btManifoldResult* %originalResult, %class.btManifoldResult** %originalResult.addr, align 4
  store %class.btTransform* %transformA, %class.btTransform** %transformA.addr, align 4
  store %class.btTransform* %transformB, %class.btTransform** %transformB.addr, align 4
  store %class.btTransform* %unPerturbedTransform, %class.btTransform** %unPerturbedTransform.addr, align 4
  %frombool = zext i1 %perturbA to i8
  store i8 %frombool, i8* %perturbA.addr, align 1
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %struct.btPerturbedContactResult*, %struct.btPerturbedContactResult** %this.addr, align 4
  %0 = bitcast %struct.btPerturbedContactResult* %this1 to %class.btManifoldResult*
  %call = call %class.btManifoldResult* @_ZN16btManifoldResultC2Ev(%class.btManifoldResult* %0)
  %1 = bitcast %struct.btPerturbedContactResult* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV24btPerturbedContactResult, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_originalManifoldResult = getelementptr inbounds %struct.btPerturbedContactResult, %struct.btPerturbedContactResult* %this1, i32 0, i32 1
  %2 = load %class.btManifoldResult*, %class.btManifoldResult** %originalResult.addr, align 4
  store %class.btManifoldResult* %2, %class.btManifoldResult** %m_originalManifoldResult, align 4
  %m_transformA = getelementptr inbounds %struct.btPerturbedContactResult, %struct.btPerturbedContactResult* %this1, i32 0, i32 2
  %3 = load %class.btTransform*, %class.btTransform** %transformA.addr, align 4
  %call2 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_transformA, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  %m_transformB = getelementptr inbounds %struct.btPerturbedContactResult, %struct.btPerturbedContactResult* %this1, i32 0, i32 3
  %4 = load %class.btTransform*, %class.btTransform** %transformB.addr, align 4
  %call3 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_transformB, %class.btTransform* nonnull align 4 dereferenceable(64) %4)
  %m_unPerturbedTransform = getelementptr inbounds %struct.btPerturbedContactResult, %struct.btPerturbedContactResult* %this1, i32 0, i32 4
  %5 = load %class.btTransform*, %class.btTransform** %unPerturbedTransform.addr, align 4
  %call4 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_unPerturbedTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %5)
  %m_perturbA = getelementptr inbounds %struct.btPerturbedContactResult, %struct.btPerturbedContactResult* %this1, i32 0, i32 5
  %6 = load i8, i8* %perturbA.addr, align 1
  %tobool = trunc i8 %6 to i1
  %frombool5 = zext i1 %tobool to i8
  store i8 %frombool5, i8* %m_perturbA, align 4
  %m_debugDrawer = getelementptr inbounds %struct.btPerturbedContactResult, %struct.btPerturbedContactResult* %this1, i32 0, i32 6
  %7 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  store %class.btIDebugDraw* %7, %class.btIDebugDraw** %m_debugDrawer, align 4
  ret %struct.btPerturbedContactResult* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btPerturbedContactResult* @_ZN24btPerturbedContactResultD2Ev(%struct.btPerturbedContactResult* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btPerturbedContactResult*, align 4
  store %struct.btPerturbedContactResult* %this, %struct.btPerturbedContactResult** %this.addr, align 4
  %this1 = load %struct.btPerturbedContactResult*, %struct.btPerturbedContactResult** %this.addr, align 4
  %0 = bitcast %struct.btPerturbedContactResult* %this1 to %class.btManifoldResult*
  %call = call %class.btManifoldResult* @_ZN16btManifoldResultD2Ev(%class.btManifoldResult* %0) #9
  ret %struct.btPerturbedContactResult* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGjkPairDetector* @_ZN17btGjkPairDetectorD2Ev(%class.btGjkPairDetector* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = bitcast %class.btGjkPairDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #9
  ret %class.btGjkPairDetector* %this1
}

; Function Attrs: noinline optnone
define hidden float @_ZN23btConvexConvexAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult(%class.btConvexConvexAlgorithm* %this, %class.btCollisionObject* %col0, %class.btCollisionObject* %col1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #2 {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %class.btConvexConvexAlgorithm*, align 4
  %col0.addr = alloca %class.btCollisionObject*, align 4
  %col1.addr = alloca %class.btCollisionObject*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %resultFraction = alloca float, align 4
  %squareMot0 = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %squareMot1 = alloca float, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %convex0 = alloca %class.btConvexShape*, align 4
  %sphere1 = alloca %class.btSphereShape, align 4
  %result = alloca %"struct.btConvexCast::CastResult", align 4
  %voronoiSimplex = alloca %class.btVoronoiSimplexSolver, align 4
  %ccd1 = alloca %class.btGjkConvexCast, align 4
  %convex1 = alloca %class.btConvexShape*, align 4
  %sphere0 = alloca %class.btSphereShape, align 4
  %result52 = alloca %"struct.btConvexCast::CastResult", align 4
  %voronoiSimplex54 = alloca %class.btVoronoiSimplexSolver, align 4
  %ccd156 = alloca %class.btGjkConvexCast, align 4
  store %class.btConvexConvexAlgorithm* %this, %class.btConvexConvexAlgorithm** %this.addr, align 4
  store %class.btCollisionObject* %col0, %class.btCollisionObject** %col0.addr, align 4
  store %class.btCollisionObject* %col1, %class.btCollisionObject** %col1.addr, align 4
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4
  %this1 = load %class.btConvexConvexAlgorithm*, %class.btConvexConvexAlgorithm** %this.addr, align 4
  %0 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store float 1.000000e+00, float* %resultFraction, align 4
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %col0.addr, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %1)
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call)
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %col0.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %2)
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call3)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call2, %class.btVector3* nonnull align 4 dereferenceable(16) %call4)
  %call5 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  store float %call5, float* %squareMot0, align 4
  %3 = load %class.btCollisionObject*, %class.btCollisionObject** %col1.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %3)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call7)
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %col1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %4)
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %call9)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %call8, %class.btVector3* nonnull align 4 dereferenceable(16) %call10)
  %call11 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp6)
  store float %call11, float* %squareMot1, align 4
  %5 = load float, float* %squareMot0, align 4
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %col0.addr, align 4
  %call12 = call float @_ZNK17btCollisionObject27getCcdSquareMotionThresholdEv(%class.btCollisionObject* %6)
  %cmp = fcmp olt float %5, %call12
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %7 = load float, float* %squareMot1, align 4
  %8 = load %class.btCollisionObject*, %class.btCollisionObject** %col1.addr, align 4
  %call13 = call float @_ZNK17btCollisionObject27getCcdSquareMotionThresholdEv(%class.btCollisionObject* %8)
  %cmp14 = fcmp olt float %7, %call13
  br i1 %cmp14, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %9 = load float, float* %resultFraction, align 4
  store float %9, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %10 = load i8, i8* @disableCcd, align 1
  %tobool = trunc i8 %10 to i1
  br i1 %tobool, label %if.then15, label %if.end16

if.then15:                                        ; preds = %if.end
  store float 1.000000e+00, float* %retval, align 4
  br label %return

if.end16:                                         ; preds = %if.end
  %11 = load %class.btCollisionObject*, %class.btCollisionObject** %col0.addr, align 4
  %call17 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %11)
  %12 = bitcast %class.btCollisionShape* %call17 to %class.btConvexShape*
  store %class.btConvexShape* %12, %class.btConvexShape** %convex0, align 4
  %13 = load %class.btCollisionObject*, %class.btCollisionObject** %col1.addr, align 4
  %call18 = call float @_ZNK17btCollisionObject23getCcdSweptSphereRadiusEv(%class.btCollisionObject* %13)
  %call19 = call %class.btSphereShape* @_ZN13btSphereShapeC2Ef(%class.btSphereShape* %sphere1, float %call18)
  %call20 = call %"struct.btConvexCast::CastResult"* @_ZN12btConvexCast10CastResultC2Ev(%"struct.btConvexCast::CastResult"* %result)
  %call21 = call %class.btVoronoiSimplexSolver* @_ZN22btVoronoiSimplexSolverC2Ev(%class.btVoronoiSimplexSolver* %voronoiSimplex)
  %14 = load %class.btConvexShape*, %class.btConvexShape** %convex0, align 4
  %15 = bitcast %class.btSphereShape* %sphere1 to %class.btConvexShape*
  %call22 = call %class.btGjkConvexCast* @_ZN15btGjkConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver(%class.btGjkConvexCast* %ccd1, %class.btConvexShape* %14, %class.btConvexShape* %15, %class.btVoronoiSimplexSolver* %voronoiSimplex)
  %16 = load %class.btCollisionObject*, %class.btCollisionObject** %col0.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %16)
  %17 = load %class.btCollisionObject*, %class.btCollisionObject** %col0.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %17)
  %18 = load %class.btCollisionObject*, %class.btCollisionObject** %col1.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %18)
  %19 = load %class.btCollisionObject*, %class.btCollisionObject** %col1.addr, align 4
  %call26 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %19)
  %call27 = call zeroext i1 @_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE(%class.btGjkConvexCast* %ccd1, %class.btTransform* nonnull align 4 dereferenceable(64) %call23, %class.btTransform* nonnull align 4 dereferenceable(64) %call24, %class.btTransform* nonnull align 4 dereferenceable(64) %call25, %class.btTransform* nonnull align 4 dereferenceable(64) %call26, %"struct.btConvexCast::CastResult"* nonnull align 4 dereferenceable(176) %result)
  br i1 %call27, label %if.then28, label %if.end45

if.then28:                                        ; preds = %if.end16
  %20 = load %class.btCollisionObject*, %class.btCollisionObject** %col0.addr, align 4
  %call29 = call float @_ZNK17btCollisionObject14getHitFractionEv(%class.btCollisionObject* %20)
  %m_fraction = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %result, i32 0, i32 5
  %21 = load float, float* %m_fraction, align 4
  %cmp30 = fcmp ogt float %call29, %21
  br i1 %cmp30, label %if.then31, label %if.end33

if.then31:                                        ; preds = %if.then28
  %22 = load %class.btCollisionObject*, %class.btCollisionObject** %col0.addr, align 4
  %m_fraction32 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %result, i32 0, i32 5
  %23 = load float, float* %m_fraction32, align 4
  call void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %22, float %23)
  br label %if.end33

if.end33:                                         ; preds = %if.then31, %if.then28
  %24 = load %class.btCollisionObject*, %class.btCollisionObject** %col1.addr, align 4
  %call34 = call float @_ZNK17btCollisionObject14getHitFractionEv(%class.btCollisionObject* %24)
  %m_fraction35 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %result, i32 0, i32 5
  %25 = load float, float* %m_fraction35, align 4
  %cmp36 = fcmp ogt float %call34, %25
  br i1 %cmp36, label %if.then37, label %if.end39

if.then37:                                        ; preds = %if.end33
  %26 = load %class.btCollisionObject*, %class.btCollisionObject** %col1.addr, align 4
  %m_fraction38 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %result, i32 0, i32 5
  %27 = load float, float* %m_fraction38, align 4
  call void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %26, float %27)
  br label %if.end39

if.end39:                                         ; preds = %if.then37, %if.end33
  %28 = load float, float* %resultFraction, align 4
  %m_fraction40 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %result, i32 0, i32 5
  %29 = load float, float* %m_fraction40, align 4
  %cmp41 = fcmp ogt float %28, %29
  br i1 %cmp41, label %if.then42, label %if.end44

if.then42:                                        ; preds = %if.end39
  %m_fraction43 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %result, i32 0, i32 5
  %30 = load float, float* %m_fraction43, align 4
  store float %30, float* %resultFraction, align 4
  br label %if.end44

if.end44:                                         ; preds = %if.then42, %if.end39
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %if.end16
  %call46 = call %class.btGjkConvexCast* @_ZN15btGjkConvexCastD2Ev(%class.btGjkConvexCast* %ccd1) #9
  %call47 = call %"struct.btConvexCast::CastResult"* @_ZN12btConvexCast10CastResultD2Ev(%"struct.btConvexCast::CastResult"* %result) #9
  %call48 = call %class.btSphereShape* @_ZN13btSphereShapeD2Ev(%class.btSphereShape* %sphere1) #9
  %31 = load %class.btCollisionObject*, %class.btCollisionObject** %col1.addr, align 4
  %call49 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %31)
  %32 = bitcast %class.btCollisionShape* %call49 to %class.btConvexShape*
  store %class.btConvexShape* %32, %class.btConvexShape** %convex1, align 4
  %33 = load %class.btCollisionObject*, %class.btCollisionObject** %col0.addr, align 4
  %call50 = call float @_ZNK17btCollisionObject23getCcdSweptSphereRadiusEv(%class.btCollisionObject* %33)
  %call51 = call %class.btSphereShape* @_ZN13btSphereShapeC2Ef(%class.btSphereShape* %sphere0, float %call50)
  %call53 = call %"struct.btConvexCast::CastResult"* @_ZN12btConvexCast10CastResultC2Ev(%"struct.btConvexCast::CastResult"* %result52)
  %call55 = call %class.btVoronoiSimplexSolver* @_ZN22btVoronoiSimplexSolverC2Ev(%class.btVoronoiSimplexSolver* %voronoiSimplex54)
  %34 = bitcast %class.btSphereShape* %sphere0 to %class.btConvexShape*
  %35 = load %class.btConvexShape*, %class.btConvexShape** %convex1, align 4
  %call57 = call %class.btGjkConvexCast* @_ZN15btGjkConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver(%class.btGjkConvexCast* %ccd156, %class.btConvexShape* %34, %class.btConvexShape* %35, %class.btVoronoiSimplexSolver* %voronoiSimplex54)
  %36 = load %class.btCollisionObject*, %class.btCollisionObject** %col0.addr, align 4
  %call58 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %36)
  %37 = load %class.btCollisionObject*, %class.btCollisionObject** %col0.addr, align 4
  %call59 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %37)
  %38 = load %class.btCollisionObject*, %class.btCollisionObject** %col1.addr, align 4
  %call60 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %38)
  %39 = load %class.btCollisionObject*, %class.btCollisionObject** %col1.addr, align 4
  %call61 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %39)
  %call62 = call zeroext i1 @_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE(%class.btGjkConvexCast* %ccd156, %class.btTransform* nonnull align 4 dereferenceable(64) %call58, %class.btTransform* nonnull align 4 dereferenceable(64) %call59, %class.btTransform* nonnull align 4 dereferenceable(64) %call60, %class.btTransform* nonnull align 4 dereferenceable(64) %call61, %"struct.btConvexCast::CastResult"* nonnull align 4 dereferenceable(176) %result52)
  br i1 %call62, label %if.then63, label %if.end81

if.then63:                                        ; preds = %if.end45
  %40 = load %class.btCollisionObject*, %class.btCollisionObject** %col0.addr, align 4
  %call64 = call float @_ZNK17btCollisionObject14getHitFractionEv(%class.btCollisionObject* %40)
  %m_fraction65 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %result52, i32 0, i32 5
  %41 = load float, float* %m_fraction65, align 4
  %cmp66 = fcmp ogt float %call64, %41
  br i1 %cmp66, label %if.then67, label %if.end69

if.then67:                                        ; preds = %if.then63
  %42 = load %class.btCollisionObject*, %class.btCollisionObject** %col0.addr, align 4
  %m_fraction68 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %result52, i32 0, i32 5
  %43 = load float, float* %m_fraction68, align 4
  call void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %42, float %43)
  br label %if.end69

if.end69:                                         ; preds = %if.then67, %if.then63
  %44 = load %class.btCollisionObject*, %class.btCollisionObject** %col1.addr, align 4
  %call70 = call float @_ZNK17btCollisionObject14getHitFractionEv(%class.btCollisionObject* %44)
  %m_fraction71 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %result52, i32 0, i32 5
  %45 = load float, float* %m_fraction71, align 4
  %cmp72 = fcmp ogt float %call70, %45
  br i1 %cmp72, label %if.then73, label %if.end75

if.then73:                                        ; preds = %if.end69
  %46 = load %class.btCollisionObject*, %class.btCollisionObject** %col1.addr, align 4
  %m_fraction74 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %result52, i32 0, i32 5
  %47 = load float, float* %m_fraction74, align 4
  call void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %46, float %47)
  br label %if.end75

if.end75:                                         ; preds = %if.then73, %if.end69
  %48 = load float, float* %resultFraction, align 4
  %m_fraction76 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %result52, i32 0, i32 5
  %49 = load float, float* %m_fraction76, align 4
  %cmp77 = fcmp ogt float %48, %49
  br i1 %cmp77, label %if.then78, label %if.end80

if.then78:                                        ; preds = %if.end75
  %m_fraction79 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %result52, i32 0, i32 5
  %50 = load float, float* %m_fraction79, align 4
  store float %50, float* %resultFraction, align 4
  br label %if.end80

if.end80:                                         ; preds = %if.then78, %if.end75
  br label %if.end81

if.end81:                                         ; preds = %if.end80, %if.end45
  %call82 = call %class.btGjkConvexCast* @_ZN15btGjkConvexCastD2Ev(%class.btGjkConvexCast* %ccd156) #9
  %call83 = call %"struct.btConvexCast::CastResult"* @_ZN12btConvexCast10CastResultD2Ev(%"struct.btConvexCast::CastResult"* %result52) #9
  %call84 = call %class.btSphereShape* @_ZN13btSphereShapeD2Ev(%class.btSphereShape* %sphere0) #9
  %51 = load float, float* %resultFraction, align 4
  store float %51, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end81, %if.then15, %if.then
  %52 = load float, float* %retval, align 4
  ret float %52
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject30getInterpolationWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_interpolationWorldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 2
  ret %class.btTransform* %m_interpolationWorldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK17btCollisionObject27getCcdSquareMotionThresholdEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_ccdMotionThreshold = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 30
  %0 = load float, float* %m_ccdMotionThreshold, align 4
  %m_ccdMotionThreshold2 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 30
  %1 = load float, float* %m_ccdMotionThreshold2, align 4
  %mul = fmul float %0, %1
  ret float %mul
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4
  ret %class.btCollisionShape* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK17btCollisionObject23getCcdSweptSphereRadiusEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_ccdSweptSphereRadius = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 29
  %0 = load float, float* %m_ccdSweptSphereRadius, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btSphereShape* @_ZN13btSphereShapeC2Ef(%class.btSphereShape* returned %this, float %radius) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btSphereShape*, align 4
  %radius.addr = alloca float, align 4
  store %class.btSphereShape* %this, %class.btSphereShape** %this.addr, align 4
  store float %radius, float* %radius.addr, align 4
  %this1 = load %class.btSphereShape*, %class.btSphereShape** %this.addr, align 4
  %0 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* %0)
  %1 = bitcast %class.btSphereShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV13btSphereShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %2 = bitcast %class.btSphereShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 8, i32* %m_shapeType, align 4
  %3 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %3, i32 0, i32 2
  %4 = load float, float* %radius.addr, align 4
  call void @_ZN9btVector34setXEf(%class.btVector3* %m_implicitShapeDimensions, float %4)
  %5 = load float, float* %radius.addr, align 4
  %6 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %6, i32 0, i32 3
  store float %5, float* %m_collisionMargin, align 4
  ret %class.btSphereShape* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btConvexCast::CastResult"* @_ZN12btConvexCast10CastResultC2Ev(%"struct.btConvexCast::CastResult"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  store %"struct.btConvexCast::CastResult"* %this, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  %this1 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  %0 = bitcast %"struct.btConvexCast::CastResult"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN12btConvexCast10CastResultE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_hitTransformA = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %this1, i32 0, i32 1
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_hitTransformA)
  %m_hitTransformB = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %this1, i32 0, i32 2
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_hitTransformB)
  %m_normal = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normal)
  %m_hitPoint = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitPoint)
  %m_fraction = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %this1, i32 0, i32 5
  store float 0x43ABC16D60000000, float* %m_fraction, align 4
  %m_debugDrawer = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %this1, i32 0, i32 6
  store %class.btIDebugDraw* null, %class.btIDebugDraw** %m_debugDrawer, align 4
  %m_allowedPenetration = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %this1, i32 0, i32 7
  store float 0.000000e+00, float* %m_allowedPenetration, align 4
  ret %"struct.btConvexCast::CastResult"* %this1
}

declare %class.btGjkConvexCast* @_ZN15btGjkConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver(%class.btGjkConvexCast* returned, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*) unnamed_addr #4

declare zeroext i1 @_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE(%class.btGjkConvexCast*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %"struct.btConvexCast::CastResult"* nonnull align 4 dereferenceable(176)) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK17btCollisionObject14getHitFractionEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_hitFraction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 28
  %0 = load float, float* %m_hitFraction, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %this, float %hitFraction) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %hitFraction.addr = alloca float, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store float %hitFraction, float* %hitFraction.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load float, float* %hitFraction.addr, align 4
  %m_hitFraction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 28
  store float %0, float* %m_hitFraction, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGjkConvexCast* @_ZN15btGjkConvexCastD2Ev(%class.btGjkConvexCast* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGjkConvexCast*, align 4
  store %class.btGjkConvexCast* %this, %class.btGjkConvexCast** %this.addr, align 4
  %this1 = load %class.btGjkConvexCast*, %class.btGjkConvexCast** %this.addr, align 4
  %0 = bitcast %class.btGjkConvexCast* %this1 to %class.btConvexCast*
  %call = call %class.btConvexCast* @_ZN12btConvexCastD2Ev(%class.btConvexCast* %0) #9
  ret %class.btGjkConvexCast* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btConvexCast::CastResult"* @_ZN12btConvexCast10CastResultD2Ev(%"struct.btConvexCast::CastResult"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  store %"struct.btConvexCast::CastResult"* %this, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  %this1 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  ret %"struct.btConvexCast::CastResult"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btSphereShape* @_ZN13btSphereShapeD2Ev(%class.btSphereShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSphereShape*, align 4
  store %class.btSphereShape* %this, %class.btSphereShape** %this.addr, align 4
  %this1 = load %class.btSphereShape*, %class.btSphereShape** %this.addr, align 4
  %0 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeD2Ev(%class.btConvexInternalShape* %0) #9
  ret %class.btSphereShape* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN23btConvexConvexAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btConvexConvexAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConvexAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btConvexConvexAlgorithm::CreateFunc"* %this, %"struct.btConvexConvexAlgorithm::CreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %"struct.btConvexConvexAlgorithm::CreateFunc"*, %"struct.btConvexConvexAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %3 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %3(%class.btDispatcher* %1, i32 72)
  store i8* %call, i8** %mem, align 4
  %4 = load i8*, i8** %mem, align 4
  %5 = bitcast i8* %4 to %class.btConvexConvexAlgorithm*
  %6 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_manifold = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %6, i32 0, i32 1
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifold, align 4
  %8 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %m_pdSolver = getelementptr inbounds %"struct.btConvexConvexAlgorithm::CreateFunc", %"struct.btConvexConvexAlgorithm::CreateFunc"* %this1, i32 0, i32 1
  %11 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_pdSolver, align 4
  %m_numPerturbationIterations = getelementptr inbounds %"struct.btConvexConvexAlgorithm::CreateFunc", %"struct.btConvexConvexAlgorithm::CreateFunc"* %this1, i32 0, i32 2
  %12 = load i32, i32* %m_numPerturbationIterations, align 4
  %m_minimumPointsPerturbationThreshold = getelementptr inbounds %"struct.btConvexConvexAlgorithm::CreateFunc", %"struct.btConvexConvexAlgorithm::CreateFunc"* %this1, i32 0, i32 3
  %13 = load i32, i32* %m_minimumPointsPerturbationThreshold, align 4
  %call2 = call %class.btConvexConvexAlgorithm* @_ZN23btConvexConvexAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_P30btConvexPenetrationDepthSolverii(%class.btConvexConvexAlgorithm* %5, %class.btPersistentManifold* %7, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %8, %struct.btCollisionObjectWrapper* %9, %struct.btCollisionObjectWrapper* %10, %class.btConvexPenetrationDepthSolver* %11, i32 %12, i32 %13)
  %14 = bitcast %class.btConvexConvexAlgorithm* %5 to %class.btCollisionAlgorithm*
  ret %class.btCollisionAlgorithm* %14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN23btConvexConvexAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE(%class.btConvexConvexAlgorithm* %this, %class.btAlignedObjectArray.6* nonnull align 4 dereferenceable(17) %manifoldArray) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexConvexAlgorithm*, align 4
  %manifoldArray.addr = alloca %class.btAlignedObjectArray.6*, align 4
  store %class.btConvexConvexAlgorithm* %this, %class.btConvexConvexAlgorithm** %this.addr, align 4
  store %class.btAlignedObjectArray.6* %manifoldArray, %class.btAlignedObjectArray.6** %manifoldArray.addr, align 4
  %this1 = load %class.btConvexConvexAlgorithm*, %class.btConvexConvexAlgorithm** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 5
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %tobool = icmp ne %class.btPersistentManifold* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %m_ownManifold = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 4
  %1 = load i8, i8* %m_ownManifold, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %2 = load %class.btAlignedObjectArray.6*, %class.btAlignedObjectArray.6** %manifoldArray.addr, align 4
  %m_manifoldPtr3 = getelementptr inbounds %class.btConvexConvexAlgorithm, %class.btConvexConvexAlgorithm* %this1, i32 0, i32 5
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.6* %2, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %m_manifoldPtr3)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  ret %struct.btCollisionAlgorithmCreateFunc* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btCollisionAlgorithmCreateFuncD0Ev(%struct.btCollisionAlgorithmCreateFunc* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %this1) #9
  %0 = bitcast %struct.btCollisionAlgorithmCreateFunc* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_(%struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %0, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  %.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %0, %struct.btCollisionAlgorithmConstructionInfo** %.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  ret %class.btCollisionAlgorithm* null
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %2 = load i32, i32* %i.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define internal void @_ZL21segmentsClosestPointsR9btVector3S0_S0_RfS1_RKS_S3_fS3_f(%class.btVector3* nonnull align 4 dereferenceable(16) %ptsVector, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetA, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetB, float* nonnull align 4 dereferenceable(4) %tA, float* nonnull align 4 dereferenceable(4) %tB, %class.btVector3* nonnull align 4 dereferenceable(16) %translation, %class.btVector3* nonnull align 4 dereferenceable(16) %dirA, float %hlenA, %class.btVector3* nonnull align 4 dereferenceable(16) %dirB, float %hlenB) #2 {
entry:
  %ptsVector.addr = alloca %class.btVector3*, align 4
  %offsetA.addr = alloca %class.btVector3*, align 4
  %offsetB.addr = alloca %class.btVector3*, align 4
  %tA.addr = alloca float*, align 4
  %tB.addr = alloca float*, align 4
  %translation.addr = alloca %class.btVector3*, align 4
  %dirA.addr = alloca %class.btVector3*, align 4
  %hlenA.addr = alloca float, align 4
  %dirB.addr = alloca %class.btVector3*, align 4
  %hlenB.addr = alloca float, align 4
  %dirA_dot_dirB = alloca float, align 4
  %dirA_dot_trans = alloca float, align 4
  %dirB_dot_trans = alloca float, align 4
  %denom = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp45 = alloca %class.btVector3, align 4
  %ref.tmp46 = alloca %class.btVector3, align 4
  %ref.tmp47 = alloca %class.btVector3, align 4
  store %class.btVector3* %ptsVector, %class.btVector3** %ptsVector.addr, align 4
  store %class.btVector3* %offsetA, %class.btVector3** %offsetA.addr, align 4
  store %class.btVector3* %offsetB, %class.btVector3** %offsetB.addr, align 4
  store float* %tA, float** %tA.addr, align 4
  store float* %tB, float** %tB.addr, align 4
  store %class.btVector3* %translation, %class.btVector3** %translation.addr, align 4
  store %class.btVector3* %dirA, %class.btVector3** %dirA.addr, align 4
  store float %hlenA, float* %hlenA.addr, align 4
  store %class.btVector3* %dirB, %class.btVector3** %dirB.addr, align 4
  store float %hlenB, float* %hlenB.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %dirA.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %dirB.addr, align 4
  %call = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %dirA_dot_dirB, align 4
  %2 = load %class.btVector3*, %class.btVector3** %dirA.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %translation.addr, align 4
  %call1 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call1, float* %dirA_dot_trans, align 4
  %4 = load %class.btVector3*, %class.btVector3** %dirB.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %translation.addr, align 4
  %call2 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call2, float* %dirB_dot_trans, align 4
  %6 = load float, float* %dirA_dot_dirB, align 4
  %7 = load float, float* %dirA_dot_dirB, align 4
  %mul = fmul float %6, %7
  %sub = fsub float 1.000000e+00, %mul
  store float %sub, float* %denom, align 4
  %8 = load float, float* %denom, align 4
  %cmp = fcmp oeq float %8, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %9 = load float*, float** %tA.addr, align 4
  store float 0.000000e+00, float* %9, align 4
  br label %if.end12

if.else:                                          ; preds = %entry
  %10 = load float, float* %dirA_dot_trans, align 4
  %11 = load float, float* %dirB_dot_trans, align 4
  %12 = load float, float* %dirA_dot_dirB, align 4
  %mul3 = fmul float %11, %12
  %sub4 = fsub float %10, %mul3
  %13 = load float, float* %denom, align 4
  %div = fdiv float %sub4, %13
  %14 = load float*, float** %tA.addr, align 4
  store float %div, float* %14, align 4
  %15 = load float*, float** %tA.addr, align 4
  %16 = load float, float* %15, align 4
  %17 = load float, float* %hlenA.addr, align 4
  %fneg = fneg float %17
  %cmp5 = fcmp olt float %16, %fneg
  br i1 %cmp5, label %if.then6, label %if.else8

if.then6:                                         ; preds = %if.else
  %18 = load float, float* %hlenA.addr, align 4
  %fneg7 = fneg float %18
  %19 = load float*, float** %tA.addr, align 4
  store float %fneg7, float* %19, align 4
  br label %if.end11

if.else8:                                         ; preds = %if.else
  %20 = load float*, float** %tA.addr, align 4
  %21 = load float, float* %20, align 4
  %22 = load float, float* %hlenA.addr, align 4
  %cmp9 = fcmp ogt float %21, %22
  br i1 %cmp9, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.else8
  %23 = load float, float* %hlenA.addr, align 4
  %24 = load float*, float** %tA.addr, align 4
  store float %23, float* %24, align 4
  br label %if.end

if.end:                                           ; preds = %if.then10, %if.else8
  br label %if.end11

if.end11:                                         ; preds = %if.end, %if.then6
  br label %if.end12

if.end12:                                         ; preds = %if.end11, %if.then
  %25 = load float*, float** %tA.addr, align 4
  %26 = load float, float* %25, align 4
  %27 = load float, float* %dirA_dot_dirB, align 4
  %mul13 = fmul float %26, %27
  %28 = load float, float* %dirB_dot_trans, align 4
  %sub14 = fsub float %mul13, %28
  %29 = load float*, float** %tB.addr, align 4
  store float %sub14, float* %29, align 4
  %30 = load float*, float** %tB.addr, align 4
  %31 = load float, float* %30, align 4
  %32 = load float, float* %hlenB.addr, align 4
  %fneg15 = fneg float %32
  %cmp16 = fcmp olt float %31, %fneg15
  br i1 %cmp16, label %if.then17, label %if.else29

if.then17:                                        ; preds = %if.end12
  %33 = load float, float* %hlenB.addr, align 4
  %fneg18 = fneg float %33
  %34 = load float*, float** %tB.addr, align 4
  store float %fneg18, float* %34, align 4
  %35 = load float*, float** %tB.addr, align 4
  %36 = load float, float* %35, align 4
  %37 = load float, float* %dirA_dot_dirB, align 4
  %mul19 = fmul float %36, %37
  %38 = load float, float* %dirA_dot_trans, align 4
  %add = fadd float %mul19, %38
  %39 = load float*, float** %tA.addr, align 4
  store float %add, float* %39, align 4
  %40 = load float*, float** %tA.addr, align 4
  %41 = load float, float* %40, align 4
  %42 = load float, float* %hlenA.addr, align 4
  %fneg20 = fneg float %42
  %cmp21 = fcmp olt float %41, %fneg20
  br i1 %cmp21, label %if.then22, label %if.else24

if.then22:                                        ; preds = %if.then17
  %43 = load float, float* %hlenA.addr, align 4
  %fneg23 = fneg float %43
  %44 = load float*, float** %tA.addr, align 4
  store float %fneg23, float* %44, align 4
  br label %if.end28

if.else24:                                        ; preds = %if.then17
  %45 = load float*, float** %tA.addr, align 4
  %46 = load float, float* %45, align 4
  %47 = load float, float* %hlenA.addr, align 4
  %cmp25 = fcmp ogt float %46, %47
  br i1 %cmp25, label %if.then26, label %if.end27

if.then26:                                        ; preds = %if.else24
  %48 = load float, float* %hlenA.addr, align 4
  %49 = load float*, float** %tA.addr, align 4
  store float %48, float* %49, align 4
  br label %if.end27

if.end27:                                         ; preds = %if.then26, %if.else24
  br label %if.end28

if.end28:                                         ; preds = %if.end27, %if.then22
  br label %if.end44

if.else29:                                        ; preds = %if.end12
  %50 = load float*, float** %tB.addr, align 4
  %51 = load float, float* %50, align 4
  %52 = load float, float* %hlenB.addr, align 4
  %cmp30 = fcmp ogt float %51, %52
  br i1 %cmp30, label %if.then31, label %if.end43

if.then31:                                        ; preds = %if.else29
  %53 = load float, float* %hlenB.addr, align 4
  %54 = load float*, float** %tB.addr, align 4
  store float %53, float* %54, align 4
  %55 = load float*, float** %tB.addr, align 4
  %56 = load float, float* %55, align 4
  %57 = load float, float* %dirA_dot_dirB, align 4
  %mul32 = fmul float %56, %57
  %58 = load float, float* %dirA_dot_trans, align 4
  %add33 = fadd float %mul32, %58
  %59 = load float*, float** %tA.addr, align 4
  store float %add33, float* %59, align 4
  %60 = load float*, float** %tA.addr, align 4
  %61 = load float, float* %60, align 4
  %62 = load float, float* %hlenA.addr, align 4
  %fneg34 = fneg float %62
  %cmp35 = fcmp olt float %61, %fneg34
  br i1 %cmp35, label %if.then36, label %if.else38

if.then36:                                        ; preds = %if.then31
  %63 = load float, float* %hlenA.addr, align 4
  %fneg37 = fneg float %63
  %64 = load float*, float** %tA.addr, align 4
  store float %fneg37, float* %64, align 4
  br label %if.end42

if.else38:                                        ; preds = %if.then31
  %65 = load float*, float** %tA.addr, align 4
  %66 = load float, float* %65, align 4
  %67 = load float, float* %hlenA.addr, align 4
  %cmp39 = fcmp ogt float %66, %67
  br i1 %cmp39, label %if.then40, label %if.end41

if.then40:                                        ; preds = %if.else38
  %68 = load float, float* %hlenA.addr, align 4
  %69 = load float*, float** %tA.addr, align 4
  store float %68, float* %69, align 4
  br label %if.end41

if.end41:                                         ; preds = %if.then40, %if.else38
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %if.then36
  br label %if.end43

if.end43:                                         ; preds = %if.end42, %if.else29
  br label %if.end44

if.end44:                                         ; preds = %if.end43, %if.end28
  %70 = load %class.btVector3*, %class.btVector3** %dirA.addr, align 4
  %71 = load float*, float** %tA.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %70, float* nonnull align 4 dereferenceable(4) %71)
  %72 = load %class.btVector3*, %class.btVector3** %offsetA.addr, align 4
  %73 = bitcast %class.btVector3* %72 to i8*
  %74 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %73, i8* align 4 %74, i32 16, i1 false)
  %75 = load %class.btVector3*, %class.btVector3** %dirB.addr, align 4
  %76 = load float*, float** %tB.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp45, %class.btVector3* nonnull align 4 dereferenceable(16) %75, float* nonnull align 4 dereferenceable(4) %76)
  %77 = load %class.btVector3*, %class.btVector3** %offsetB.addr, align 4
  %78 = bitcast %class.btVector3* %77 to i8*
  %79 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %78, i8* align 4 %79, i32 16, i1 false)
  %80 = load %class.btVector3*, %class.btVector3** %translation.addr, align 4
  %81 = load %class.btVector3*, %class.btVector3** %offsetA.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp47, %class.btVector3* nonnull align 4 dereferenceable(16) %80, %class.btVector3* nonnull align 4 dereferenceable(16) %81)
  %82 = load %class.btVector3*, %class.btVector3** %offsetB.addr, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp46, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp47, %class.btVector3* nonnull align 4 dereferenceable(16) %82)
  %83 = load %class.btVector3*, %class.btVector3** %ptsVector.addr, align 4
  %84 = bitcast %class.btVector3* %83 to i8*
  %85 = bitcast %class.btVector3* %ref.tmp46 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %84, i8* align 4 %85, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4
  ret %class.btCollisionObject* %0
}

declare void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64)) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btSubSimplexClosestResult* @_ZN25btSubSimplexClosestResultC2Ev(%struct.btSubSimplexClosestResult* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btSubSimplexClosestResult*, align 4
  store %struct.btSubSimplexClosestResult* %this, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %this1 = load %struct.btSubSimplexClosestResult*, %struct.btSubSimplexClosestResult** %this.addr, align 4
  %m_closestPointOnSimplex = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_closestPointOnSimplex)
  %m_usedVertices = getelementptr inbounds %struct.btSubSimplexClosestResult, %struct.btSubSimplexClosestResult* %this1, i32 0, i32 1
  %call2 = call %struct.btUsageBitfield* @_ZN15btUsageBitfieldC2Ev(%struct.btUsageBitfield* %m_usedVertices)
  ret %struct.btSubSimplexClosestResult* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btUsageBitfield* @_ZN15btUsageBitfieldC2Ev(%struct.btUsageBitfield* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btUsageBitfield*, align 4
  store %struct.btUsageBitfield* %this, %struct.btUsageBitfield** %this.addr, align 4
  %this1 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %this.addr, align 4
  call void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %this1)
  ret %struct.btUsageBitfield* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btUsageBitfield5resetEv(%struct.btUsageBitfield* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btUsageBitfield*, align 4
  store %struct.btUsageBitfield* %this, %struct.btUsageBitfield** %this.addr, align 4
  %this1 = load %struct.btUsageBitfield*, %struct.btUsageBitfield** %this.addr, align 4
  %0 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load = load i8, i8* %0, align 2
  %bf.clear = and i8 %bf.load, -2
  store i8 %bf.clear, i8* %0, align 2
  %1 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load2 = load i8, i8* %1, align 2
  %bf.clear3 = and i8 %bf.load2, -3
  store i8 %bf.clear3, i8* %1, align 2
  %2 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load4 = load i8, i8* %2, align 2
  %bf.clear5 = and i8 %bf.load4, -5
  store i8 %bf.clear5, i8* %2, align 2
  %3 = bitcast %struct.btUsageBitfield* %this1 to i8*
  %bf.load6 = load i8, i8* %3, align 2
  %bf.clear7 = and i8 %bf.load6, -9
  store i8 %bf.clear7, i8* %3, align 2
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy12isPolyhedralEi(i32 %proxyType) #1 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4
  %0 = load i32, i32* %proxyType.addr, align 4
  %cmp = icmp slt i32 %0, 7
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %0 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN13btDummyResultD0Ev(%struct.btDummyResult* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btDummyResult*, align 4
  store %struct.btDummyResult* %this, %struct.btDummyResult** %this.addr, align 4
  %this1 = load %struct.btDummyResult*, %struct.btDummyResult** %this.addr, align 4
  %call = call %struct.btDummyResult* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN13btDummyResultD2Ev(%struct.btDummyResult* %this1) #9
  %0 = bitcast %struct.btDummyResult* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN13btDummyResult20setShapeIdentifiersAEii(%struct.btDummyResult* %this, i32 %partId0, i32 %index0) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btDummyResult*, align 4
  %partId0.addr = alloca i32, align 4
  %index0.addr = alloca i32, align 4
  store %struct.btDummyResult* %this, %struct.btDummyResult** %this.addr, align 4
  store i32 %partId0, i32* %partId0.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  %this1 = load %struct.btDummyResult*, %struct.btDummyResult** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN13btDummyResult20setShapeIdentifiersBEii(%struct.btDummyResult* %this, i32 %partId1, i32 %index1) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btDummyResult*, align 4
  %partId1.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  store %struct.btDummyResult* %this, %struct.btDummyResult** %this.addr, align 4
  store i32 %partId1, i32* %partId1.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %struct.btDummyResult*, %struct.btDummyResult** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN13btDummyResult15addContactPointERK9btVector3SB_f(%struct.btDummyResult* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnBInWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInWorld, float %depth) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btDummyResult*, align 4
  %normalOnBInWorld.addr = alloca %class.btVector3*, align 4
  %pointInWorld.addr = alloca %class.btVector3*, align 4
  %depth.addr = alloca float, align 4
  store %struct.btDummyResult* %this, %struct.btDummyResult** %this.addr, align 4
  store %class.btVector3* %normalOnBInWorld, %class.btVector3** %normalOnBInWorld.addr, align 4
  store %class.btVector3* %pointInWorld, %class.btVector3** %pointInWorld.addr, align 4
  store float %depth, float* %depth.addr, align 4
  %this1 = load %struct.btDummyResult*, %struct.btDummyResult** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #8

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN21btWithoutMarginResultD0Ev(%struct.btWithoutMarginResult* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btWithoutMarginResult*, align 4
  store %struct.btWithoutMarginResult* %this, %struct.btWithoutMarginResult** %this.addr, align 4
  %this1 = load %struct.btWithoutMarginResult*, %struct.btWithoutMarginResult** %this.addr, align 4
  %call = call %struct.btWithoutMarginResult* @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN21btWithoutMarginResultD2Ev(%struct.btWithoutMarginResult* %this1) #9
  %0 = bitcast %struct.btWithoutMarginResult* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN21btWithoutMarginResult20setShapeIdentifiersAEii(%struct.btWithoutMarginResult* %this, i32 %partId0, i32 %index0) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btWithoutMarginResult*, align 4
  %partId0.addr = alloca i32, align 4
  %index0.addr = alloca i32, align 4
  store %struct.btWithoutMarginResult* %this, %struct.btWithoutMarginResult** %this.addr, align 4
  store i32 %partId0, i32* %partId0.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  %this1 = load %struct.btWithoutMarginResult*, %struct.btWithoutMarginResult** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN21btWithoutMarginResult20setShapeIdentifiersBEii(%struct.btWithoutMarginResult* %this, i32 %partId1, i32 %index1) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btWithoutMarginResult*, align 4
  %partId1.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  store %struct.btWithoutMarginResult* %this, %struct.btWithoutMarginResult** %this.addr, align 4
  store i32 %partId1, i32* %partId1.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %struct.btWithoutMarginResult*, %struct.btWithoutMarginResult** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZZN23btConvexConvexAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResultEN21btWithoutMarginResult15addContactPointERK9btVector3SB_f(%struct.btWithoutMarginResult* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnBInWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInWorldOrg, float %depthOrg) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.btWithoutMarginResult*, align 4
  %normalOnBInWorld.addr = alloca %class.btVector3*, align 4
  %pointInWorldOrg.addr = alloca %class.btVector3*, align 4
  %depthOrg.addr = alloca float, align 4
  %adjustedPointB = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %struct.btWithoutMarginResult* %this, %struct.btWithoutMarginResult** %this.addr, align 4
  store %class.btVector3* %normalOnBInWorld, %class.btVector3** %normalOnBInWorld.addr, align 4
  store %class.btVector3* %pointInWorldOrg, %class.btVector3** %pointInWorldOrg.addr, align 4
  store float %depthOrg, float* %depthOrg.addr, align 4
  %this1 = load %struct.btWithoutMarginResult*, %struct.btWithoutMarginResult** %this.addr, align 4
  %0 = load float, float* %depthOrg.addr, align 4
  %m_reportedDistance = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 5
  store float %0, float* %m_reportedDistance, align 4
  %1 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4
  %m_reportedNormalOnWorld = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 2
  %2 = bitcast %class.btVector3* %m_reportedNormalOnWorld to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %4 = load %class.btVector3*, %class.btVector3** %pointInWorldOrg.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4
  %m_marginOnB = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %5, float* nonnull align 4 dereferenceable(4) %m_marginOnB)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %adjustedPointB, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %6 = load float, float* %depthOrg.addr, align 4
  %m_marginOnA = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 3
  %7 = load float, float* %m_marginOnA, align 4
  %m_marginOnB2 = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 4
  %8 = load float, float* %m_marginOnB2, align 4
  %add = fadd float %7, %8
  %add3 = fadd float %6, %add
  %m_reportedDistance4 = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 5
  store float %add3, float* %m_reportedDistance4, align 4
  %m_reportedDistance5 = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 5
  %9 = load float, float* %m_reportedDistance5, align 4
  %cmp = fcmp olt float %9, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_foundResult = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 6
  store i8 1, i8* %m_foundResult, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_originalResult = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 1
  %10 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %m_originalResult, align 4
  %11 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4
  %m_reportedDistance6 = getelementptr inbounds %struct.btWithoutMarginResult, %struct.btWithoutMarginResult* %this1, i32 0, i32 5
  %12 = load float, float* %m_reportedDistance6, align 4
  %13 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %10 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %13, align 4
  %vfn = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable, i64 4
  %14 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn, align 4
  call void %14(%"struct.btDiscreteCollisionDetectorInterface::Result"* %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %adjustedPointB, float %12)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %_angle) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4
  store float* %_angle, float** %_angle.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %0)
  store float %call, float* %d, align 4
  %1 = load float*, float** %_angle.addr, align 4
  %2 = load float, float* %1, align 4
  %mul = fmul float %2, 5.000000e-01
  %call2 = call float @_Z5btSinf(float %mul)
  %3 = load float, float* %d, align 4
  %div = fdiv float %call2, %3
  store float %div, float* %s, align 4
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %5 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %5)
  %6 = load float, float* %call3, align 4
  %7 = load float, float* %s, align 4
  %mul4 = fmul float %6, %7
  store float %mul4, float* %ref.tmp, align 4
  %8 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %8)
  %9 = load float, float* %call6, align 4
  %10 = load float, float* %s, align 4
  %mul7 = fmul float %9, %10
  store float %mul7, float* %ref.tmp5, align 4
  %11 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %11)
  %12 = load float, float* %call9, align 4
  %13 = load float, float* %s, align 4
  %mul10 = fmul float %12, %13
  store float %mul10, float* %ref.tmp8, align 4
  %14 = load float*, float** %_angle.addr, align 4
  %15 = load float, float* %14, align 4
  %mul12 = fmul float %15, 5.000000e-01
  %call13 = call float @_Z5btCosf(float %mul12)
  store float %call13, float* %ref.tmp11, align 4
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %4, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btSinf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btCosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #7

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float*, float** %_z.addr, align 4
  %4 = load float*, float** %_w.addr, align 4
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %0)
  store float %call, float* %d, align 4
  %1 = load float, float* %d, align 4
  %div = fdiv float 2.000000e+00, %1
  store float %div, float* %s, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call2, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %4, %5
  store float %mul, float* %xs, align 4
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call3, align 4
  %9 = load float, float* %s, align 4
  %mul4 = fmul float %8, %9
  store float %mul4, float* %ys, align 4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load float, float* %s, align 4
  %mul6 = fmul float %12, %13
  store float %mul6, float* %zs, align 4
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %15)
  %16 = load float, float* %call7, align 4
  %17 = load float, float* %xs, align 4
  %mul8 = fmul float %16, %17
  store float %mul8, float* %wx, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load float, float* %ys, align 4
  %mul10 = fmul float %20, %21
  store float %mul10, float* %wy, align 4
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %23)
  %24 = load float, float* %call11, align 4
  %25 = load float, float* %zs, align 4
  %mul12 = fmul float %24, %25
  store float %mul12, float* %wz, align 4
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4
  %29 = load float, float* %xs, align 4
  %mul14 = fmul float %28, %29
  store float %mul14, float* %xx, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %31)
  %32 = load float, float* %call15, align 4
  %33 = load float, float* %ys, align 4
  %mul16 = fmul float %32, %33
  store float %mul16, float* %xy, align 4
  %34 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %35 = bitcast %class.btQuaternion* %34 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %35)
  %36 = load float, float* %call17, align 4
  %37 = load float, float* %zs, align 4
  %mul18 = fmul float %36, %37
  store float %mul18, float* %xz, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %39)
  %40 = load float, float* %call19, align 4
  %41 = load float, float* %ys, align 4
  %mul20 = fmul float %40, %41
  store float %mul20, float* %yy, align 4
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %43)
  %44 = load float, float* %call21, align 4
  %45 = load float, float* %zs, align 4
  %mul22 = fmul float %44, %45
  store float %mul22, float* %yz, align 4
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call23, align 4
  %49 = load float, float* %zs, align 4
  %mul24 = fmul float %48, %49
  store float %mul24, float* %zz, align 4
  %50 = load float, float* %yy, align 4
  %51 = load float, float* %zz, align 4
  %add = fadd float %50, %51
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4
  %52 = load float, float* %xy, align 4
  %53 = load float, float* %wz, align 4
  %sub26 = fsub float %52, %53
  store float %sub26, float* %ref.tmp25, align 4
  %54 = load float, float* %xz, align 4
  %55 = load float, float* %wy, align 4
  %add28 = fadd float %54, %55
  store float %add28, float* %ref.tmp27, align 4
  %56 = load float, float* %xy, align 4
  %57 = load float, float* %wz, align 4
  %add30 = fadd float %56, %57
  store float %add30, float* %ref.tmp29, align 4
  %58 = load float, float* %xx, align 4
  %59 = load float, float* %zz, align 4
  %add32 = fadd float %58, %59
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4
  %60 = load float, float* %yz, align 4
  %61 = load float, float* %wx, align 4
  %sub35 = fsub float %60, %61
  store float %sub35, float* %ref.tmp34, align 4
  %62 = load float, float* %xz, align 4
  %63 = load float, float* %wy, align 4
  %sub37 = fsub float %62, %63
  store float %sub37, float* %ref.tmp36, align 4
  %64 = load float, float* %yz, align 4
  %65 = load float, float* %wx, align 4
  %add39 = fadd float %64, %65
  store float %add39, float* %ref.tmp38, align 4
  %66 = load float, float* %xx, align 4
  %67 = load float, float* %yy, align 4
  %add41 = fadd float %66, %67
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btManifoldResult* @_ZN16btManifoldResultC2Ev(%class.btManifoldResult* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = bitcast %class.btManifoldResult* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #9
  %1 = bitcast %class.btManifoldResult* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV16btManifoldResult, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_closestPointDistanceThreshold = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 8
  store float 0.000000e+00, float* %m_closestPointDistanceThreshold, align 4
  ret %class.btManifoldResult* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN24btPerturbedContactResultD0Ev(%struct.btPerturbedContactResult* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btPerturbedContactResult*, align 4
  store %struct.btPerturbedContactResult* %this, %struct.btPerturbedContactResult** %this.addr, align 4
  %this1 = load %struct.btPerturbedContactResult*, %struct.btPerturbedContactResult** %this.addr, align 4
  %call = call %struct.btPerturbedContactResult* @_ZN24btPerturbedContactResultD2Ev(%struct.btPerturbedContactResult* %this1) #9
  %0 = bitcast %struct.btPerturbedContactResult* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btManifoldResult20setShapeIdentifiersAEii(%class.btManifoldResult* %this, i32 %partId0, i32 %index0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %partId0.addr = alloca i32, align 4
  %index0.addr = alloca i32, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  store i32 %partId0, i32* %partId0.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load i32, i32* %partId0.addr, align 4
  %m_partId0 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 4
  store i32 %0, i32* %m_partId0, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %m_index0 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 6
  store i32 %1, i32* %m_index0, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btManifoldResult20setShapeIdentifiersBEii(%class.btManifoldResult* %this, i32 %partId1, i32 %index1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %partId1.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  store i32 %partId1, i32* %partId1.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load i32, i32* %partId1.addr, align 4
  %m_partId1 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 5
  store i32 %0, i32* %m_partId1, align 4
  %1 = load i32, i32* %index1.addr, align 4
  %m_index1 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 7
  store i32 %1, i32* %m_index1, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN24btPerturbedContactResult15addContactPointERK9btVector3S2_f(%struct.btPerturbedContactResult* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnBInWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInWorld, float %orgDepth) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btPerturbedContactResult*, align 4
  %normalOnBInWorld.addr = alloca %class.btVector3*, align 4
  %pointInWorld.addr = alloca %class.btVector3*, align 4
  %orgDepth.addr = alloca float, align 4
  %endPt = alloca %class.btVector3, align 4
  %startPt = alloca %class.btVector3, align 4
  %newDepth = alloca float, align 4
  %newNormal = alloca %class.btVector3, align 4
  %endPtOrg = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btTransform, align 4
  %ref.tmp6 = alloca %class.btTransform, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btTransform, align 4
  %ref.tmp16 = alloca %class.btTransform, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  store %struct.btPerturbedContactResult* %this, %struct.btPerturbedContactResult** %this.addr, align 4
  store %class.btVector3* %normalOnBInWorld, %class.btVector3** %normalOnBInWorld.addr, align 4
  store %class.btVector3* %pointInWorld, %class.btVector3** %pointInWorld.addr, align 4
  store float %orgDepth, float* %orgDepth.addr, align 4
  %this1 = load %struct.btPerturbedContactResult*, %struct.btPerturbedContactResult** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %endPt)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %startPt)
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %newNormal)
  %m_perturbA = getelementptr inbounds %struct.btPerturbedContactResult, %struct.btPerturbedContactResult* %this1, i32 0, i32 5
  %0 = load i8, i8* %m_perturbA, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %class.btVector3*, %class.btVector3** %pointInWorld.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %2, float* nonnull align 4 dereferenceable(4) %orgDepth.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %endPtOrg, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %m_unPerturbedTransform = getelementptr inbounds %struct.btPerturbedContactResult, %struct.btPerturbedContactResult* %this1, i32 0, i32 4
  %m_transformA = getelementptr inbounds %struct.btPerturbedContactResult, %struct.btPerturbedContactResult* %this1, i32 0, i32 2
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp6, %class.btTransform* %m_transformA)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp5, %class.btTransform* %m_unPerturbedTransform, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp6)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, %class.btTransform* %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %endPtOrg)
  %3 = bitcast %class.btVector3* %endPt to i8*
  %4 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %5 = load %class.btVector3*, %class.btVector3** %pointInWorld.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %endPt, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  %6 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  store float %call8, float* %newDepth, align 4
  %7 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %7, float* nonnull align 4 dereferenceable(4) %newDepth)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %endPt, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  %8 = bitcast %class.btVector3* %startPt to i8*
  %9 = bitcast %class.btVector3* %ref.tmp9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %entry
  %10 = load %class.btVector3*, %class.btVector3** %pointInWorld.addr, align 4
  %11 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %11, float* nonnull align 4 dereferenceable(4) %orgDepth.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12)
  %12 = bitcast %class.btVector3* %endPt to i8*
  %13 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  %m_unPerturbedTransform15 = getelementptr inbounds %struct.btPerturbedContactResult, %struct.btPerturbedContactResult* %this1, i32 0, i32 4
  %m_transformB = getelementptr inbounds %struct.btPerturbedContactResult, %struct.btPerturbedContactResult* %this1, i32 0, i32 3
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp16, %class.btTransform* %m_transformB)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp14, %class.btTransform* %m_unPerturbedTransform15, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp16)
  %14 = load %class.btVector3*, %class.btVector3** %pointInWorld.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp13, %class.btTransform* %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %14)
  %15 = bitcast %class.btVector3* %startPt to i8*
  %16 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %endPt, %class.btVector3* nonnull align 4 dereferenceable(16) %startPt)
  %17 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4
  %call18 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp17, %class.btVector3* nonnull align 4 dereferenceable(16) %17)
  store float %call18, float* %newDepth, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %m_originalManifoldResult = getelementptr inbounds %struct.btPerturbedContactResult, %struct.btPerturbedContactResult* %this1, i32 0, i32 1
  %18 = load %class.btManifoldResult*, %class.btManifoldResult** %m_originalManifoldResult, align 4
  %19 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4
  %20 = load float, float* %newDepth, align 4
  %21 = bitcast %class.btManifoldResult* %18 to void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)***
  %vtable = load void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*** %21, align 4
  %vfn = getelementptr inbounds void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)** %vtable, i64 4
  %22 = load void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)** %vfn, align 4
  call void %22(%class.btManifoldResult* %18, %class.btVector3* nonnull align 4 dereferenceable(16) %19, %class.btVector3* nonnull align 4 dereferenceable(16) %startPt, float %20)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btManifoldResult* @_ZN16btManifoldResultD2Ev(%class.btManifoldResult* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = bitcast %class.btManifoldResult* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #9
  ret %class.btManifoldResult* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

declare %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector34setXEf(%class.btVector3* %this, float %_x) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float %_x, float* %_x.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_x.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN12btConvexCast10CastResult9DebugDrawEf(%"struct.btConvexCast::CastResult"* %this, float %fraction) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  %fraction.addr = alloca float, align 4
  store %"struct.btConvexCast::CastResult"* %this, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  store float %fraction, float* %fraction.addr, align 4
  %this1 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform(%"struct.btConvexCast::CastResult"* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  store %"struct.btConvexCast::CastResult"* %this, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4
  %this1 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN12btConvexCast10CastResult13reportFailureEii(%"struct.btConvexCast::CastResult"* %this, i32 %errNo, i32 %numIterations) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  %errNo.addr = alloca i32, align 4
  %numIterations.addr = alloca i32, align 4
  store %"struct.btConvexCast::CastResult"* %this, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  store i32 %errNo, i32* %errNo.addr, align 4
  store i32 %numIterations, i32* %numIterations.addr, align 4
  %this1 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN12btConvexCast10CastResultD0Ev(%"struct.btConvexCast::CastResult"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  store %"struct.btConvexCast::CastResult"* %this, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  %this1 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %this.addr, align 4
  %call = call %"struct.btConvexCast::CastResult"* @_ZN12btConvexCast10CastResultD2Ev(%"struct.btConvexCast::CastResult"* %this1) #9
  %0 = bitcast %"struct.btConvexCast::CastResult"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: nounwind
declare %class.btConvexCast* @_ZN12btConvexCastD2Ev(%class.btConvexCast* returned) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexInternalShape* @_ZN21btConvexInternalShapeD2Ev(%class.btConvexInternalShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %class.btConvexInternalShape* %this1 to %class.btConvexShape*
  %call = call %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* %0) #9
  ret %class.btConvexInternalShape* %this1
}

; Function Attrs: nounwind
declare %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned) unnamed_addr #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.6* %this, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.6*, align 4
  %_Val.addr = alloca %class.btPersistentManifold**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.6* %this, %class.btAlignedObjectArray.6** %this.addr, align 4
  store %class.btPersistentManifold** %_Val, %class.btPersistentManifold*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.6*, %class.btAlignedObjectArray.6** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.6* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.6* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.6* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.6* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.6* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 4
  %1 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %1, i32 %2
  %3 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btPersistentManifold**
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %_Val.addr, align 4
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %5, align 4
  store %class.btPersistentManifold* %6, %class.btPersistentManifold** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.6* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.6*, align 4
  store %class.btAlignedObjectArray.6* %this, %class.btAlignedObjectArray.6** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.6*, %class.btAlignedObjectArray.6** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.6* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.6*, align 4
  store %class.btAlignedObjectArray.6* %this, %class.btAlignedObjectArray.6** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.6*, %class.btAlignedObjectArray.6** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.6* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.6*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.6* %this, %class.btAlignedObjectArray.6** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.6*, %class.btAlignedObjectArray.6** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.6* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.6* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %2, %class.btPersistentManifold*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.6* %this1)
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.6* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.6* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.6* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.6* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %4, %class.btPersistentManifold*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.6* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.6*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.6* %this, %class.btAlignedObjectArray.6** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.6*, %class.btAlignedObjectArray.6** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.6* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.6*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.6* %this, %class.btAlignedObjectArray.6** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.6*, %class.btAlignedObjectArray.6** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.7* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.6* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.6*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.6* %this, %class.btAlignedObjectArray.6** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.6*, %class.btAlignedObjectArray.6** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  %5 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 4
  %7 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %7, i32 %8
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4
  store %class.btPersistentManifold* %9, %class.btPersistentManifold** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.6* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.6*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.6* %this, %class.btAlignedObjectArray.6** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.6*, %class.btAlignedObjectArray.6** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.6* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.6*, align 4
  store %class.btAlignedObjectArray.6* %this, %class.btAlignedObjectArray.6** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.6*, %class.btAlignedObjectArray.6** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.7* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.6, %class.btAlignedObjectArray.6* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.7* %this, i32 %n, %class.btPersistentManifold*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.7*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.7* %this, %class.btAlignedAllocator.7** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.7*, %class.btAlignedAllocator.7** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.7* %this, %class.btPersistentManifold** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.7*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.7* %this, %class.btAlignedAllocator.7** %this.addr, align 4
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.7*, %class.btAlignedAllocator.7** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.btVector3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %2, %class.btVector3** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %class.btVector3*, %class.btVector3** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btVector3* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* %4, %class.btVector3** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btVector3* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %9 = bitcast %class.btVector3* %6 to i8*
  %10 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.btVector3** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btConvexConvexAlgorithm.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { cold noreturn nounwind }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }
attributes #11 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
