; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/LinearMath/btPolarDecomposition.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/LinearMath/btPolarDecomposition.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btPolarDecomposition = type { float, i32 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK11btMatrix3x37inverseEv = comdat any

$_Z11btFuzzyZerof = comdat any

$_Z5btPowff = comdat any

$_ZmlRK11btMatrix3x3RKf = comdat any

$_ZplRK11btMatrix3x3S1_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZN11btMatrix3x3pLERKS_ = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btMatrix3x35cofacEiiii = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_Z5btMaxIfERKT_S2_S2_ = comdat any

$_Z6btFabsf = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZZ14polarDecomposeRK11btMatrix3x3RS_S2_E5polar = internal global %class.btPolarDecomposition zeroinitializer, align 4
@_ZGVZ14polarDecomposeRK11btMatrix3x3RS_S2_E5polar = internal global i32 0, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btPolarDecomposition.cpp, i8* null }]

@_ZN20btPolarDecompositionC1Efj = hidden unnamed_addr alias %class.btPolarDecomposition* (%class.btPolarDecomposition*, float, i32), %class.btPolarDecomposition* (%class.btPolarDecomposition*, float, i32)* @_ZN20btPolarDecompositionC2Efj

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btPolarDecomposition* @_ZN20btPolarDecompositionC2Efj(%class.btPolarDecomposition* returned %this, float %tolerance, i32 %maxIterations) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btPolarDecomposition*, align 4
  %tolerance.addr = alloca float, align 4
  %maxIterations.addr = alloca i32, align 4
  store %class.btPolarDecomposition* %this, %class.btPolarDecomposition** %this.addr, align 4
  store float %tolerance, float* %tolerance.addr, align 4
  store i32 %maxIterations, i32* %maxIterations.addr, align 4
  %this1 = load %class.btPolarDecomposition*, %class.btPolarDecomposition** %this.addr, align 4
  %m_tolerance = getelementptr inbounds %class.btPolarDecomposition, %class.btPolarDecomposition* %this1, i32 0, i32 0
  %0 = load float, float* %tolerance.addr, align 4
  store float %0, float* %m_tolerance, align 4
  %m_maxIterations = getelementptr inbounds %class.btPolarDecomposition, %class.btPolarDecomposition* %this1, i32 0, i32 1
  %1 = load i32, i32* %maxIterations.addr, align 4
  store i32 %1, i32* %m_maxIterations, align 4
  ret %class.btPolarDecomposition* %this1
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK20btPolarDecomposition9decomposeERK11btMatrix3x3RS0_S3_(%class.btPolarDecomposition* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %a, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %u, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %h) #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btPolarDecomposition*, align 4
  %a.addr = alloca %class.btMatrix3x3*, align 4
  %u.addr = alloca %class.btMatrix3x3*, align 4
  %h.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %i = alloca i32, align 4
  %h_1 = alloca float, align 4
  %h_inf = alloca float, align 4
  %u_1 = alloca float, align 4
  %u_inf = alloca float, align 4
  %h_norm = alloca float, align 4
  %u_norm = alloca float, align 4
  %gamma = alloca float, align 4
  %inv_gamma = alloca float, align 4
  %delta = alloca %class.btMatrix3x3, align 4
  %ref.tmp12 = alloca %class.btMatrix3x3, align 4
  %ref.tmp13 = alloca %class.btMatrix3x3, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca %class.btMatrix3x3, align 4
  %ref.tmp16 = alloca %class.btMatrix3x3, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp19 = alloca %class.btMatrix3x3, align 4
  %ref.tmp25 = alloca %class.btMatrix3x3, align 4
  %ref.tmp26 = alloca %class.btMatrix3x3, align 4
  %ref.tmp28 = alloca %class.btMatrix3x3, align 4
  %ref.tmp29 = alloca %class.btMatrix3x3, align 4
  %ref.tmp30 = alloca %class.btMatrix3x3, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca %class.btMatrix3x3, align 4
  %ref.tmp35 = alloca %class.btMatrix3x3, align 4
  %ref.tmp37 = alloca %class.btMatrix3x3, align 4
  %ref.tmp38 = alloca %class.btMatrix3x3, align 4
  %ref.tmp39 = alloca %class.btMatrix3x3, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btPolarDecomposition* %this, %class.btPolarDecomposition** %this.addr, align 4
  store %class.btMatrix3x3* %a, %class.btMatrix3x3** %a.addr, align 4
  store %class.btMatrix3x3* %u, %class.btMatrix3x3** %u.addr, align 4
  store %class.btMatrix3x3* %h, %class.btMatrix3x3** %h.addr, align 4
  %this1 = load %class.btPolarDecomposition*, %class.btPolarDecomposition** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %u.addr, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %2)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %h.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %3, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4
  %m_maxIterations = getelementptr inbounds %class.btPolarDecomposition, %class.btPolarDecomposition* %this1, i32 0, i32 1
  %5 = load i32, i32* %m_maxIterations, align 4
  %cmp = icmp ult i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %h.addr, align 4
  %call3 = call float @_ZN12_GLOBAL__N_17p1_normERK11btMatrix3x3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %6)
  store float %call3, float* %h_1, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %h.addr, align 4
  %call4 = call float @_ZN12_GLOBAL__N_19pinf_normERK11btMatrix3x3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %7)
  store float %call4, float* %h_inf, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %u.addr, align 4
  %call5 = call float @_ZN12_GLOBAL__N_17p1_normERK11btMatrix3x3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %8)
  store float %call5, float* %u_1, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %u.addr, align 4
  %call6 = call float @_ZN12_GLOBAL__N_19pinf_normERK11btMatrix3x3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %9)
  store float %call6, float* %u_inf, align 4
  %10 = load float, float* %h_1, align 4
  %11 = load float, float* %h_inf, align 4
  %mul = fmul float %10, %11
  store float %mul, float* %h_norm, align 4
  %12 = load float, float* %u_1, align 4
  %13 = load float, float* %u_inf, align 4
  %mul7 = fmul float %12, %13
  store float %mul7, float* %u_norm, align 4
  %14 = load float, float* %h_norm, align 4
  %call8 = call zeroext i1 @_Z11btFuzzyZerof(float %14)
  br i1 %call8, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %15 = load float, float* %u_norm, align 4
  %call9 = call zeroext i1 @_Z11btFuzzyZerof(float %15)
  br i1 %call9, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %for.body
  br label %for.end

if.end:                                           ; preds = %lor.lhs.false
  %16 = load float, float* %h_norm, align 4
  %17 = load float, float* %u_norm, align 4
  %div = fdiv float %16, %17
  %call10 = call float @_Z5btPowff(float %div, float 2.500000e-01)
  store float %call10, float* %gamma, align 4
  %18 = load float, float* %gamma, align 4
  %div11 = fdiv float 1.000000e+00, %18
  store float %div11, float* %inv_gamma, align 4
  %19 = load %class.btMatrix3x3*, %class.btMatrix3x3** %u.addr, align 4
  %20 = load float, float* %gamma, align 4
  %sub = fsub float %20, 2.000000e+00
  store float %sub, float* %ref.tmp14, align 4
  call void @_ZmlRK11btMatrix3x3RKf(%class.btMatrix3x3* sret align 4 %ref.tmp13, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %19, float* nonnull align 4 dereferenceable(4) %ref.tmp14)
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %h.addr, align 4
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp16, %class.btMatrix3x3* %21)
  call void @_ZmlRK11btMatrix3x3RKf(%class.btMatrix3x3* sret align 4 %ref.tmp15, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %inv_gamma)
  call void @_ZplRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp12, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp13, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp15)
  store float 5.000000e-01, float* %ref.tmp17, align 4
  call void @_ZmlRK11btMatrix3x3RKf(%class.btMatrix3x3* sret align 4 %delta, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  %22 = load %class.btMatrix3x3*, %class.btMatrix3x3** %u.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3pLERKS_(%class.btMatrix3x3* %22, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %delta)
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %u.addr, align 4
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %ref.tmp19, %class.btMatrix3x3* %23)
  %24 = load %class.btMatrix3x3*, %class.btMatrix3x3** %h.addr, align 4
  %call20 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %24, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp19)
  %call21 = call float @_ZN12_GLOBAL__N_17p1_normERK11btMatrix3x3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %delta)
  %m_tolerance = getelementptr inbounds %class.btPolarDecomposition, %class.btPolarDecomposition* %this1, i32 0, i32 0
  %25 = load float, float* %m_tolerance, align 4
  %26 = load float, float* %u_1, align 4
  %mul22 = fmul float %25, %26
  %cmp23 = fcmp ole float %call21, %mul22
  br i1 %cmp23, label %if.then24, label %if.end33

if.then24:                                        ; preds = %if.end
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %u.addr, align 4
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp26, %class.btMatrix3x3* %27)
  %28 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp25, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp26, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %28)
  %29 = load %class.btMatrix3x3*, %class.btMatrix3x3** %h.addr, align 4
  %call27 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %29, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp25)
  %30 = load %class.btMatrix3x3*, %class.btMatrix3x3** %h.addr, align 4
  %31 = load %class.btMatrix3x3*, %class.btMatrix3x3** %h.addr, align 4
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp30, %class.btMatrix3x3* %31)
  call void @_ZplRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp29, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %30, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp30)
  store float 5.000000e-01, float* %ref.tmp31, align 4
  call void @_ZmlRK11btMatrix3x3RKf(%class.btMatrix3x3* sret align 4 %ref.tmp28, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31)
  %32 = load %class.btMatrix3x3*, %class.btMatrix3x3** %h.addr, align 4
  %call32 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %32, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp28)
  %33 = load i32, i32* %i, align 4
  store i32 %33, i32* %retval, align 4
  br label %return

if.end33:                                         ; preds = %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end33
  %34 = load i32, i32* %i, align 4
  %inc = add i32 %34, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %35 = load %class.btMatrix3x3*, %class.btMatrix3x3** %u.addr, align 4
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp35, %class.btMatrix3x3* %35)
  %36 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp34, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp35, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %36)
  %37 = load %class.btMatrix3x3*, %class.btMatrix3x3** %h.addr, align 4
  %call36 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %37, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp34)
  %38 = load %class.btMatrix3x3*, %class.btMatrix3x3** %h.addr, align 4
  %39 = load %class.btMatrix3x3*, %class.btMatrix3x3** %h.addr, align 4
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp39, %class.btMatrix3x3* %39)
  call void @_ZplRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp38, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %38, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp39)
  store float 5.000000e-01, float* %ref.tmp40, align 4
  call void @_ZmlRK11btMatrix3x3RKf(%class.btMatrix3x3* sret align 4 %ref.tmp37, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  %40 = load %class.btMatrix3x3*, %class.btMatrix3x3** %h.addr, align 4
  %call41 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %40, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp37)
  %m_maxIterations42 = getelementptr inbounds %class.btPolarDecomposition, %class.btPolarDecomposition* %this1, i32 0, i32 1
  %41 = load i32, i32* %m_maxIterations42, align 4
  store i32 %41, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then24
  %42 = load i32, i32* %retval, align 4
  ret i32 %42
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %co = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %det = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %call = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 1, i32 2, i32 2)
  store float %call, float* %ref.tmp, align 4
  %call3 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 2, i32 2, i32 0)
  store float %call3, float* %ref.tmp2, align 4
  %call5 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 0, i32 2, i32 1)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %co, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this1, i32 0)
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %co)
  store float %call8, float* %det, align 4
  %1 = load float, float* %det, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %s, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %co)
  %2 = load float, float* %call10, align 4
  %3 = load float, float* %s, align 4
  %mul = fmul float %2, %3
  store float %mul, float* %ref.tmp9, align 4
  %call12 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 2, i32 1)
  %4 = load float, float* %s, align 4
  %mul13 = fmul float %call12, %4
  store float %mul13, float* %ref.tmp11, align 4
  %call15 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 1, i32 2)
  %5 = load float, float* %s, align 4
  %mul16 = fmul float %call15, %5
  store float %mul16, float* %ref.tmp14, align 4
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %co)
  %6 = load float, float* %call18, align 4
  %7 = load float, float* %s, align 4
  %mul19 = fmul float %6, %7
  store float %mul19, float* %ref.tmp17, align 4
  %call21 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 2, i32 2)
  %8 = load float, float* %s, align 4
  %mul22 = fmul float %call21, %8
  store float %mul22, float* %ref.tmp20, align 4
  %call24 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 1, i32 0)
  %9 = load float, float* %s, align 4
  %mul25 = fmul float %call24, %9
  store float %mul25, float* %ref.tmp23, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %co)
  %10 = load float, float* %call27, align 4
  %11 = load float, float* %s, align 4
  %mul28 = fmul float %10, %11
  store float %mul28, float* %ref.tmp26, align 4
  %call30 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 2, i32 0)
  %12 = load float, float* %s, align 4
  %mul31 = fmul float %call30, %12
  store float %mul31, float* %ref.tmp29, align 4
  %call33 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 1, i32 1)
  %13 = load float, float* %s, align 4
  %mul34 = fmul float %call33, %13
  store float %mul34, float* %ref.tmp32, align 4
  %call35 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  ret void
}

; Function Attrs: noinline optnone
define internal float @_ZN12_GLOBAL__N_17p1_normERK11btMatrix3x3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %a) #2 {
entry:
  %a.addr = alloca %class.btMatrix3x3*, align 4
  %sum0 = alloca float, align 4
  %sum1 = alloca float, align 4
  %sum2 = alloca float, align 4
  store %class.btMatrix3x3* %a, %class.btMatrix3x3** %a.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  %call = call float @_ZN12_GLOBAL__N_114abs_column_sumERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0, i32 0)
  store float %call, float* %sum0, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  %call1 = call float @_ZN12_GLOBAL__N_114abs_column_sumERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %1, i32 1)
  store float %call1, float* %sum1, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  %call2 = call float @_ZN12_GLOBAL__N_114abs_column_sumERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %2, i32 2)
  store float %call2, float* %sum2, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %sum0, float* nonnull align 4 dereferenceable(4) %sum1)
  %call4 = call nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %call3, float* nonnull align 4 dereferenceable(4) %sum2)
  %3 = load float, float* %call4, align 4
  ret float %3
}

; Function Attrs: noinline optnone
define internal float @_ZN12_GLOBAL__N_19pinf_normERK11btMatrix3x3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %a) #2 {
entry:
  %a.addr = alloca %class.btMatrix3x3*, align 4
  %sum0 = alloca float, align 4
  %sum1 = alloca float, align 4
  %sum2 = alloca float, align 4
  store %class.btMatrix3x3* %a, %class.btMatrix3x3** %a.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  %call = call float @_ZN12_GLOBAL__N_111abs_row_sumERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0, i32 0)
  store float %call, float* %sum0, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  %call1 = call float @_ZN12_GLOBAL__N_111abs_row_sumERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %1, i32 1)
  store float %call1, float* %sum1, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  %call2 = call float @_ZN12_GLOBAL__N_111abs_row_sumERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %2, i32 2)
  store float %call2, float* %sum2, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %sum0, float* nonnull align 4 dereferenceable(4) %sum1)
  %call4 = call nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %call3, float* nonnull align 4 dereferenceable(4) %sum2)
  %3 = load float, float* %call4, align 4
  ret float %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_Z11btFuzzyZerof(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %call = call float @_Z6btFabsf(float %0)
  %cmp = fcmp olt float %call, 0x3E80000000000000
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btPowff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %2 = call float @llvm.pow.f32(float %0, float %1)
  ret float %2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RKf(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, float* nonnull align 4 dereferenceable(4) %k) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %k.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store float* %k, float** %k.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call)
  %2 = load float, float* %call1, align 4
  %3 = load float*, float** %k.addr, align 4
  %4 = load float, float* %3, align 4
  %mul = fmul float %2, %4
  store float %mul, float* %ref.tmp, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %5, i32 0)
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call3)
  %6 = load float, float* %call4, align 4
  %7 = load float*, float** %k.addr, align 4
  %8 = load float, float* %7, align 4
  %mul5 = fmul float %6, %8
  store float %mul5, float* %ref.tmp2, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call7)
  %10 = load float, float* %call8, align 4
  %11 = load float*, float** %k.addr, align 4
  %12 = load float, float* %11, align 4
  %mul9 = fmul float %10, %12
  store float %mul9, float* %ref.tmp6, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %13, i32 1)
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call11)
  %14 = load float, float* %call12, align 4
  %15 = load float*, float** %k.addr, align 4
  %16 = load float, float* %15, align 4
  %mul13 = fmul float %14, %16
  store float %mul13, float* %ref.tmp10, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %17, i32 1)
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call15)
  %18 = load float, float* %call16, align 4
  %19 = load float*, float** %k.addr, align 4
  %20 = load float, float* %19, align 4
  %mul17 = fmul float %18, %20
  store float %mul17, float* %ref.tmp14, align 4
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 1)
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call19)
  %22 = load float, float* %call20, align 4
  %23 = load float*, float** %k.addr, align 4
  %24 = load float, float* %23, align 4
  %mul21 = fmul float %22, %24
  store float %mul21, float* %ref.tmp18, align 4
  %25 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %25, i32 2)
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call23)
  %26 = load float, float* %call24, align 4
  %27 = load float*, float** %k.addr, align 4
  %28 = load float, float* %27, align 4
  %mul25 = fmul float %26, %28
  store float %mul25, float* %ref.tmp22, align 4
  %29 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %29, i32 2)
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call27)
  %30 = load float, float* %call28, align 4
  %31 = load float*, float** %k.addr, align 4
  %32 = load float, float* %31, align 4
  %mul29 = fmul float %30, %32
  store float %mul29, float* %ref.tmp26, align 4
  %33 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call31 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %33, i32 2)
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call31)
  %34 = load float, float* %call32, align 4
  %35 = load float*, float** %k.addr, align 4
  %36 = load float, float* %35, align 4
  %mul33 = fmul float %34, %36
  store float %mul33, float* %ref.tmp30, align 4
  %call34 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp22, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp30)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call)
  %arrayidx = getelementptr inbounds float, float* %call1, i32 0
  %2 = load float, float* %arrayidx, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 0)
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 0
  %4 = load float, float* %arrayidx4, align 4
  %add = fadd float %2, %4
  store float %add, float* %ref.tmp, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %5, i32 0)
  %call7 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call6)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 1
  %6 = load float, float* %arrayidx8, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %7, i32 0)
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call9)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  %8 = load float, float* %arrayidx11, align 4
  %add12 = fadd float %6, %8
  store float %add12, float* %ref.tmp5, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %9, i32 0)
  %call15 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call14)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 2
  %10 = load float, float* %arrayidx16, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %11, i32 0)
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 2
  %12 = load float, float* %arrayidx19, align 4
  %add20 = fadd float %10, %12
  store float %add20, float* %ref.tmp13, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %13, i32 1)
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call22)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 0
  %14 = load float, float* %arrayidx24, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %15, i32 1)
  %call26 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call25)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 0
  %16 = load float, float* %arrayidx27, align 4
  %add28 = fadd float %14, %16
  store float %add28, float* %ref.tmp21, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call30 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %17, i32 1)
  %call31 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call30)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 1
  %18 = load float, float* %arrayidx32, align 4
  %19 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call33 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %19, i32 1)
  %call34 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call33)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 1
  %20 = load float, float* %arrayidx35, align 4
  %add36 = fadd float %18, %20
  store float %add36, float* %ref.tmp29, align 4
  %21 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call38 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %21, i32 1)
  %call39 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call38)
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 2
  %22 = load float, float* %arrayidx40, align 4
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call41 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %23, i32 1)
  %call42 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call41)
  %arrayidx43 = getelementptr inbounds float, float* %call42, i32 2
  %24 = load float, float* %arrayidx43, align 4
  %add44 = fadd float %22, %24
  store float %add44, float* %ref.tmp37, align 4
  %25 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %25, i32 2)
  %call47 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call46)
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 0
  %26 = load float, float* %arrayidx48, align 4
  %27 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call49 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %27, i32 2)
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call49)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 0
  %28 = load float, float* %arrayidx51, align 4
  %add52 = fadd float %26, %28
  store float %add52, float* %ref.tmp45, align 4
  %29 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call54 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %29, i32 2)
  %call55 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call54)
  %arrayidx56 = getelementptr inbounds float, float* %call55, i32 1
  %30 = load float, float* %arrayidx56, align 4
  %31 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %31, i32 2)
  %call58 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call57)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 1
  %32 = load float, float* %arrayidx59, align 4
  %add60 = fadd float %30, %32
  store float %add60, float* %ref.tmp53, align 4
  %33 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call62 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %33, i32 2)
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call62)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 2
  %34 = load float, float* %arrayidx64, align 4
  %35 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %call65 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %35, i32 2)
  %call66 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call65)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 2
  %36 = load float, float* %arrayidx67, align 4
  %add68 = fadd float %34, %36
  store float %add68, float* %ref.tmp61, align 4
  %call69 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp45, float* nonnull align 4 dereferenceable(4) %ref.tmp53, float* nonnull align 4 dereferenceable(4) %ref.tmp61)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3pLERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp57 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ref.tmp77 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx)
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 0
  %0 = load float, float* %arrayidx2, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  %2 = load float, float* %arrayidx6, align 4
  %add = fadd float %0, %2
  store float %add, float* %ref.tmp, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx9)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  %3 = load float, float* %arrayidx11, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %4, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 0
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx13)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 1
  %5 = load float, float* %arrayidx15, align 4
  %add16 = fadd float %3, %5
  store float %add16, float* %ref.tmp7, align 4
  %m_el18 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx19 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el18, i32 0, i32 0
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx19)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 2
  %6 = load float, float* %arrayidx21, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %m_el22 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %7, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el22, i32 0, i32 0
  %call24 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx23)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 2
  %8 = load float, float* %arrayidx25, align 4
  %add26 = fadd float %6, %8
  store float %add26, float* %ref.tmp17, align 4
  %m_el28 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx29 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el28, i32 0, i32 1
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx29)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 0
  %9 = load float, float* %arrayidx31, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %10, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 1
  %call34 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx33)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 0
  %11 = load float, float* %arrayidx35, align 4
  %add36 = fadd float %9, %11
  store float %add36, float* %ref.tmp27, align 4
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 1
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx39)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 1
  %12 = load float, float* %arrayidx41, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %m_el42 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %13, i32 0, i32 0
  %arrayidx43 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el42, i32 0, i32 1
  %call44 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx43)
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 1
  %14 = load float, float* %arrayidx45, align 4
  %add46 = fadd float %12, %14
  store float %add46, float* %ref.tmp37, align 4
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 1
  %call50 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx49)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 2
  %15 = load float, float* %arrayidx51, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %16, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 1
  %call54 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx53)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 2
  %17 = load float, float* %arrayidx55, align 4
  %add56 = fadd float %15, %17
  store float %add56, float* %ref.tmp47, align 4
  %m_el58 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx59 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el58, i32 0, i32 2
  %call60 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx59)
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 0
  %18 = load float, float* %arrayidx61, align 4
  %19 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %m_el62 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %19, i32 0, i32 0
  %arrayidx63 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el62, i32 0, i32 2
  %call64 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx63)
  %arrayidx65 = getelementptr inbounds float, float* %call64, i32 0
  %20 = load float, float* %arrayidx65, align 4
  %add66 = fadd float %18, %20
  store float %add66, float* %ref.tmp57, align 4
  %m_el68 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx69 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el68, i32 0, i32 2
  %call70 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx69)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 1
  %21 = load float, float* %arrayidx71, align 4
  %22 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %m_el72 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %22, i32 0, i32 0
  %arrayidx73 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el72, i32 0, i32 2
  %call74 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx73)
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 1
  %23 = load float, float* %arrayidx75, align 4
  %add76 = fadd float %21, %23
  store float %add76, float* %ref.tmp67, align 4
  %m_el78 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx79 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el78, i32 0, i32 2
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx79)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 2
  %24 = load float, float* %arrayidx81, align 4
  %25 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %m_el82 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %25, i32 0, i32 0
  %arrayidx83 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el82, i32 0, i32 2
  %call84 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx83)
  %arrayidx85 = getelementptr inbounds float, float* %call84, i32 2
  %26 = load float, float* %arrayidx85, align 4
  %add86 = fadd float %24, %26
  store float %add86, float* %ref.tmp77, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp47, float* nonnull align 4 dereferenceable(4) %ref.tmp57, float* nonnull align 4 dereferenceable(4) %ref.tmp67, float* nonnull align 4 dereferenceable(4) %ref.tmp77)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK20btPolarDecomposition13maxIterationsEv(%class.btPolarDecomposition* %this) #1 {
entry:
  %this.addr = alloca %class.btPolarDecomposition*, align 4
  store %class.btPolarDecomposition* %this, %class.btPolarDecomposition** %this.addr, align 4
  %this1 = load %class.btPolarDecomposition*, %class.btPolarDecomposition** %this.addr, align 4
  %m_maxIterations = getelementptr inbounds %class.btPolarDecomposition, %class.btPolarDecomposition* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_maxIterations, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden i32 @_Z14polarDecomposeRK11btMatrix3x3RS_S2_(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %a, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %u, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %h) #2 {
entry:
  %a.addr = alloca %class.btMatrix3x3*, align 4
  %u.addr = alloca %class.btMatrix3x3*, align 4
  %h.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %a, %class.btMatrix3x3** %a.addr, align 4
  store %class.btMatrix3x3* %u, %class.btMatrix3x3** %u.addr, align 4
  store %class.btMatrix3x3* %h, %class.btMatrix3x3** %h.addr, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZ14polarDecomposeRK11btMatrix3x3RS_S2_E5polar to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZ14polarDecomposeRK11btMatrix3x3RS_S2_E5polar) #3
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %call = call %class.btPolarDecomposition* @_ZN20btPolarDecompositionC1Efj(%class.btPolarDecomposition* @_ZZ14polarDecomposeRK11btMatrix3x3RS_S2_E5polar, float 0x3F1A36E2E0000000, i32 16)
  call void @__cxa_guard_release(i32* @_ZGVZ14polarDecomposeRK11btMatrix3x3RS_S2_E5polar) #3
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %u.addr, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %h.addr, align 4
  %call1 = call i32 @_ZNK20btPolarDecomposition9decomposeERK11btMatrix3x3RS0_S3_(%class.btPolarDecomposition* @_ZZ14polarDecomposeRK11btMatrix3x3RS_S2_E5polar, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %3, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %4, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %5)
  ret i32 %call1
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #3

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this, i32 %r1, i32 %c1, i32 %r2, i32 %c2) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %r1.addr = alloca i32, align 4
  %c1.addr = alloca i32, align 4
  %r2.addr = alloca i32, align 4
  %c2.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %r1, i32* %r1.addr, align 4
  store i32 %c1, i32* %c1.addr, align 4
  store i32 %r2, i32* %r2.addr, align 4
  store i32 %c2, i32* %c2.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %r1.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %1 = load i32, i32* %c1.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx2, align 4
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %r2.addr, align 4
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 %3
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %4 = load i32, i32* %c2.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %4
  %5 = load float, float* %arrayidx6, align 4
  %mul = fmul float %2, %5
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %6 = load i32, i32* %r1.addr, align 4
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 %6
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %7 = load i32, i32* %c2.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %7
  %8 = load float, float* %arrayidx10, align 4
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %9 = load i32, i32* %r2.addr, align 4
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 %9
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx12)
  %10 = load i32, i32* %c1.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %10
  %11 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %8, %11
  %sub = fsub float %mul, %mul15
  ret float %sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define internal float @_ZN12_GLOBAL__N_114abs_column_sumERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %a, i32 %i) #2 {
entry:
  %a.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %a, %class.btMatrix3x3** %a.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call)
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call1, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %call2 = call float @_Z6btFabsf(float %2)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 1)
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call3)
  %4 = load i32, i32* %i.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 %4
  %5 = load float, float* %arrayidx5, align 4
  %call6 = call float @_Z6btFabsf(float %5)
  %add = fadd float %call2, %call6
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 2)
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call7)
  %7 = load i32, i32* %i.addr, align 4
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 %7
  %8 = load float, float* %arrayidx9, align 4
  %call10 = call float @_Z6btFabsf(float %8)
  %add11 = fadd float %add, %call10
  ret float %add11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_Z5btMaxIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load float*, float** %a.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load float*, float** %b.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi float* [ %4, %cond.true ], [ %5, %cond.false ]
  ret float* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #5

; Function Attrs: noinline nounwind optnone
define internal float @_ZN12_GLOBAL__N_111abs_row_sumERK11btMatrix3x3i(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %a, i32 %i) #1 {
entry:
  %a.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %a, %class.btMatrix3x3** %a.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  %1 = load i32, i32* %i.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 %1)
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call)
  %arrayidx = getelementptr inbounds float, float* %call1, i32 0
  %2 = load float, float* %arrayidx, align 4
  %call2 = call float @_Z6btFabsf(float %2)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  %4 = load i32, i32* %i.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 %4)
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call3)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %call6 = call float @_Z6btFabsf(float %5)
  %add = fadd float %call2, %call6
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %a.addr, align 4
  %7 = load i32, i32* %i.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 %7)
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call7)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %8 = load float, float* %arrayidx9, align 4
  %call10 = call float @_Z6btFabsf(float %8)
  %add11 = fadd float %add, %call10
  ret float %add11
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.pow.f32(float, float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btPolarDecomposition.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind readnone speculatable willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1048575}
