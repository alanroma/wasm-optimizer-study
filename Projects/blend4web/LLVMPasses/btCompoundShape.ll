; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btCompoundShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btCompoundShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btCompoundShape = type { %class.btCollisionShape, %class.btAlignedObjectArray, %class.btVector3, %class.btVector3, %struct.btDbvt*, i32, float, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btCompoundShapeChild*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btCompoundShapeChild = type { %class.btTransform, %class.btCollisionShape*, i32, float, %struct.btDbvtNode* }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.0 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.0 = type { [2 x %struct.btDbvtNode*] }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.1 }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btVector3 = type { [4 x float] }
%class.btSerializer = type { i32 (...)** }
%struct.btCompoundShapeData = type { %struct.btCollisionShapeData, %struct.btCompoundShapeChildData*, i32, float }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btCompoundShapeChildData = type { %struct.btTransformFloatData, %struct.btCollisionShapeData*, i32, float }
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btVector3FloatData = type { [4 x float] }
%class.btChunk = type { i32, i32, i8*, i32, i32 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN16btCollisionShapeC2Ev = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildEC2Ev = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildED2Ev = comdat any

$_ZN16btCollisionShapeD2Ev = comdat any

$_ZN15btCompoundShapedlEPv = comdat any

$_ZN20btCompoundShapeChildC2Ev = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_ = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE4swapEii = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE8pop_backEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x38absoluteEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN11btMatrix3x311diagonalizeERS_fi = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZN15btCompoundShape17getChildTransformEi = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZdvRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZNK11btTransform14serializeFloatER20btTransformFloatData = comdat any

$_ZNK15btCompoundShape15getLocalScalingEv = comdat any

$_ZNK15btCompoundShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN15btCompoundShape9setMarginEf = comdat any

$_ZNK15btCompoundShape9getMarginEv = comdat any

$_ZNK15btCompoundShape28calculateSerializeBufferSizeEv = comdat any

$_ZN12btDbvtAabbMmC2Ev = comdat any

$_Z6btFabsf = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_Z6btSqrtf = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZNK11btMatrix3x314serializeFloatER20btMatrix3x3FloatData = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE4initEv = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btCompoundShapeChildnwEmPv = comdat any

$_ZN20btCompoundShapeChildC2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9allocSizeEi = comdat any

$_ZN20btCompoundShapeChildaSERKS_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV15btCompoundShape = hidden unnamed_addr constant { [20 x i8*] } { [20 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btCompoundShape to i8*), i8* bitcast (%class.btCompoundShape* (%class.btCompoundShape*)* @_ZN15btCompoundShapeD1Ev to i8*), i8* bitcast (void (%class.btCompoundShape*)* @_ZN15btCompoundShapeD0Ev to i8*), i8* bitcast (void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK15btCompoundShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btCompoundShape*, %class.btVector3*)* @_ZN15btCompoundShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btCompoundShape*)* @_ZNK15btCompoundShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btCompoundShape*, float, %class.btVector3*)* @_ZNK15btCompoundShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btCompoundShape*)* @_ZNK15btCompoundShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btCompoundShape*, float)* @_ZN15btCompoundShape9setMarginEf to i8*), i8* bitcast (float (%class.btCompoundShape*)* @_ZNK15btCompoundShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCompoundShape*)* @_ZNK15btCompoundShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCompoundShape*, i8*, %class.btSerializer*)* @_ZNK15btCompoundShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btCompoundShape*, %class.btCollisionShape*)* @_ZN15btCompoundShape16removeChildShapeEP16btCollisionShape to i8*), i8* bitcast (void (%class.btCompoundShape*)* @_ZN15btCompoundShape20recalculateLocalAabbEv to i8*)] }, align 4
@.str = private unnamed_addr constant [25 x i8] c"btCompoundShapeChildData\00", align 1
@.str.1 = private unnamed_addr constant [20 x i8] c"btCompoundShapeData\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS15btCompoundShape = hidden constant [18 x i8] c"15btCompoundShape\00", align 1
@_ZTI16btCollisionShape = external constant i8*
@_ZTI15btCompoundShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btCompoundShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI16btCollisionShape to i8*) }, align 4
@_ZTV16btCollisionShape = external unnamed_addr constant { [18 x i8*] }, align 4
@.str.2 = private unnamed_addr constant [9 x i8] c"Compound\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btCompoundShape.cpp, i8* null }]

@_ZN15btCompoundShapeC1Ebi = hidden unnamed_addr alias %class.btCompoundShape* (%class.btCompoundShape*, i1, i32), %class.btCompoundShape* (%class.btCompoundShape*, i1, i32)* @_ZN15btCompoundShapeC2Ebi
@_ZN15btCompoundShapeD1Ev = hidden unnamed_addr alias %class.btCompoundShape* (%class.btCompoundShape*), %class.btCompoundShape* (%class.btCompoundShape*)* @_ZN15btCompoundShapeD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btCompoundShape* @_ZN15btCompoundShapeC2Ebi(%class.btCompoundShape* returned %this, i1 zeroext %enableDynamicAabbTree, i32 %initialChildCapacity) unnamed_addr #2 {
entry:
  %retval = alloca %class.btCompoundShape*, align 4
  %this.addr = alloca %class.btCompoundShape*, align 4
  %enableDynamicAabbTree.addr = alloca i8, align 1
  %initialChildCapacity.addr = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %mem = alloca i8*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  %frombool = zext i1 %enableDynamicAabbTree to i8
  store i8 %frombool, i8* %enableDynamicAabbTree.addr, align 1
  store i32 %initialChildCapacity, i32* %initialChildCapacity.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  store %class.btCompoundShape* %this1, %class.btCompoundShape** %retval, align 4
  %0 = bitcast %class.btCompoundShape* %this1 to %class.btCollisionShape*
  %call = call %class.btCollisionShape* @_ZN16btCollisionShapeC2Ev(%class.btCollisionShape* %0)
  %1 = bitcast %class.btCompoundShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [20 x i8*] }, { [20 x i8*] }* @_ZTV15btCompoundShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEC2Ev(%class.btAlignedObjectArray* %m_children)
  %m_localAabbMin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  store float 0x43ABC16D60000000, float* %ref.tmp, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp3, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_localAabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %m_localAabbMax = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  store float 0xC3ABC16D60000000, float* %ref.tmp6, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp7, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp8, align 4
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_localAabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  store %struct.btDbvt* null, %struct.btDbvt** %m_dynamicAabbTree, align 4
  %m_updateRevision = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 5
  store i32 1, i32* %m_updateRevision, align 4
  %m_collisionMargin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_collisionMargin, align 4
  %m_localScaling = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 7
  store float 1.000000e+00, float* %ref.tmp10, align 4
  store float 1.000000e+00, float* %ref.tmp11, align 4
  store float 1.000000e+00, float* %ref.tmp12, align 4
  %call13 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_localScaling, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %2 = bitcast %class.btCompoundShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 31, i32* %m_shapeType, align 4
  %3 = load i8, i8* %enableDynamicAabbTree.addr, align 1
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call14 = call i8* @_Z22btAlignedAllocInternalmi(i32 40, i32 16)
  store i8* %call14, i8** %mem, align 4
  %4 = load i8*, i8** %mem, align 4
  %5 = bitcast i8* %4 to %struct.btDbvt*
  %call15 = call %struct.btDbvt* @_ZN6btDbvtC1Ev(%struct.btDbvt* %5)
  %m_dynamicAabbTree16 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  store %struct.btDbvt* %5, %struct.btDbvt** %m_dynamicAabbTree16, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_children17 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %6 = load i32, i32* %initialChildCapacity.addr, align 4
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7reserveEi(%class.btAlignedObjectArray* %m_children17, i32 %6)
  %7 = load %class.btCompoundShape*, %class.btCompoundShape** %retval, align 4
  ret %class.btCompoundShape* %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZN16btCollisionShapeC2Ev(%class.btCollisionShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast %class.btCollisionShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV16btCollisionShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  store i32 35, i32* %m_shapeType, align 4
  %m_userPointer = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 2
  store i8* null, i8** %m_userPointer, align 4
  %m_userIndex = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 3
  store i32 -1, i32* %m_userIndex, align 4
  ret %class.btCollisionShape* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

declare %struct.btDbvt* @_ZN6btDbvtC1Ev(%struct.btDbvt* returned) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btCompoundShapeChild*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btCompoundShapeChild*
  store %struct.btCompoundShapeChild* %2, %struct.btCompoundShapeChild** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btCompoundShapeChild* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btCompoundShapeChild* %4, %struct.btCompoundShapeChild** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btCompoundShape* @_ZN15btCompoundShapeD2Ev(%class.btCompoundShape* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btCompoundShape*, align 4
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  store %class.btCompoundShape* %this1, %class.btCompoundShape** %retval, align 4
  %0 = bitcast %class.btCompoundShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [20 x i8*] }, { [20 x i8*] }* @_ZTV15btCompoundShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %1 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree, align 4
  %tobool = icmp ne %struct.btDbvt* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_dynamicAabbTree2 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %2 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree2, align 4
  %call = call %struct.btDbvt* @_ZN6btDbvtD1Ev(%struct.btDbvt* %2) #7
  %m_dynamicAabbTree3 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %3 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree3, align 4
  %4 = bitcast %struct.btDbvt* %3 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call4 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildED2Ev(%class.btAlignedObjectArray* %m_children) #7
  %5 = bitcast %class.btCompoundShape* %this1 to %class.btCollisionShape*
  %call5 = call %class.btCollisionShape* @_ZN16btCollisionShapeD2Ev(%class.btCollisionShape* %5) #7
  %6 = load %class.btCompoundShape*, %class.btCompoundShape** %retval, align 4
  ret %class.btCompoundShape* %6
}

; Function Attrs: nounwind
declare %struct.btDbvt* @_ZN6btDbvtD1Ev(%struct.btDbvt* returned) unnamed_addr #4

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZN16btCollisionShapeD2Ev(%class.btCollisionShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  ret %class.btCollisionShape* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN15btCompoundShapeD0Ev(%class.btCompoundShape* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %call = call %class.btCompoundShape* @_ZN15btCompoundShapeD1Ev(%class.btCompoundShape* %this1) #7
  %0 = bitcast %class.btCompoundShape* %this1 to i8*
  call void @_ZN15btCompoundShapedlEPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btCompoundShapedlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN15btCompoundShape13addChildShapeERK11btTransformP16btCollisionShape(%class.btCompoundShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %localTransform, %class.btCollisionShape* %shape) #2 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %localTransform.addr = alloca %class.btTransform*, align 4
  %shape.addr = alloca %class.btCollisionShape*, align 4
  %child = alloca %struct.btCompoundShapeChild, align 4
  %localAabbMin = alloca %class.btVector3, align 4
  %localAabbMax = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %bounds = alloca %struct.btDbvtAabbMm, align 4
  %index = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store %class.btTransform* %localTransform, %class.btTransform** %localTransform.addr, align 4
  store %class.btCollisionShape* %shape, %class.btCollisionShape** %shape.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_updateRevision, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4
  %call = call %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildC2Ev(%struct.btCompoundShapeChild* %child)
  %m_node = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %child, i32 0, i32 4
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %m_node, align 4
  %1 = load %class.btTransform*, %class.btTransform** %localTransform.addr, align 4
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %child, i32 0, i32 0
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transform, %class.btTransform* nonnull align 4 dereferenceable(64) %1)
  %2 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %child, i32 0, i32 1
  store %class.btCollisionShape* %2, %class.btCollisionShape** %m_childShape, align 4
  %3 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4
  %call3 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %3)
  %m_childShapeType = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %child, i32 0, i32 2
  store i32 %call3, i32* %m_childShapeType, align 4
  %4 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4
  %5 = bitcast %class.btCollisionShape* %4 to float (%class.btCollisionShape*)***
  %vtable = load float (%class.btCollisionShape*)**, float (%class.btCollisionShape*)*** %5, align 4
  %vfn = getelementptr inbounds float (%class.btCollisionShape*)*, float (%class.btCollisionShape*)** %vtable, i64 12
  %6 = load float (%class.btCollisionShape*)*, float (%class.btCollisionShape*)** %vfn, align 4
  %call4 = call float %6(%class.btCollisionShape* %4)
  %m_childMargin = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %child, i32 0, i32 3
  store float %call4, float* %m_childMargin, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMin)
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMax)
  %7 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4
  %8 = load %class.btTransform*, %class.btTransform** %localTransform.addr, align 4
  %9 = bitcast %class.btCollisionShape* %7 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable7 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %9, align 4
  %vfn8 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable7, i64 2
  %10 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn8, align 4
  call void %10(%class.btCollisionShape* %7, %class.btTransform* nonnull align 4 dereferenceable(64) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %11 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %11, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_localAabbMin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMin)
  %12 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call9, i32 %12
  %13 = load float, float* %arrayidx, align 4
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMin)
  %14 = load i32, i32* %i, align 4
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 %14
  %15 = load float, float* %arrayidx11, align 4
  %cmp12 = fcmp ogt float %13, %15
  br i1 %cmp12, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMin)
  %16 = load i32, i32* %i, align 4
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %16
  %17 = load float, float* %arrayidx14, align 4
  %m_localAabbMin15 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMin15)
  %18 = load i32, i32* %i, align 4
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 %18
  store float %17, float* %arrayidx17, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %m_localAabbMax = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMax)
  %19 = load i32, i32* %i, align 4
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 %19
  %20 = load float, float* %arrayidx19, align 4
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMax)
  %21 = load i32, i32* %i, align 4
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 %21
  %22 = load float, float* %arrayidx21, align 4
  %cmp22 = fcmp olt float %20, %22
  br i1 %cmp22, label %if.then23, label %if.end29

if.then23:                                        ; preds = %if.end
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMax)
  %23 = load i32, i32* %i, align 4
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 %23
  %24 = load float, float* %arrayidx25, align 4
  %m_localAabbMax26 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMax26)
  %25 = load i32, i32* %i, align 4
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 %25
  store float %24, float* %arrayidx28, align 4
  br label %if.end29

if.end29:                                         ; preds = %if.then23, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end29
  %26 = load i32, i32* %i, align 4
  %inc30 = add nsw i32 %26, 1
  store i32 %inc30, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %27 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree, align 4
  %tobool = icmp ne %struct.btDbvt* %27, null
  br i1 %tobool, label %if.then31, label %if.end36

if.then31:                                        ; preds = %for.end
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %bounds, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call32 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  store i32 %call32, i32* %index, align 4
  %m_dynamicAabbTree33 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %28 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree33, align 4
  %29 = load i32, i32* %index, align 4
  %30 = inttoptr i32 %29 to i8*
  %call34 = call %struct.btDbvtNode* @_ZN6btDbvt6insertERK12btDbvtAabbMmPv(%struct.btDbvt* %28, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %bounds, i8* %30)
  %m_node35 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %child, i32 0, i32 4
  store %struct.btDbvtNode* %call34, %struct.btDbvtNode** %m_node35, align 4
  br label %if.end36

if.end36:                                         ; preds = %if.then31, %for.end
  %m_children37 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_(%class.btAlignedObjectArray* %m_children37, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %child)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildC2Ev(%struct.btCompoundShapeChild* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btCompoundShapeChild*, align 4
  store %struct.btCompoundShapeChild* %this, %struct.btCompoundShapeChild** %this.addr, align 4
  %this1 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %this.addr, align 4
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %this1, i32 0, i32 0
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transform)
  ret %struct.btCompoundShapeChild* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %mx) #2 comdat {
entry:
  %mi.addr = alloca %class.btVector3*, align 4
  %mx.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %mi, %class.btVector3** %mi.addr, align 4
  store %class.btVector3* %mx, %class.btVector3** %mx.addr, align 4
  %call = call %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* %agg.result)
  %0 = load %class.btVector3*, %class.btVector3** %mi.addr, align 4
  %mi1 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 0
  %1 = bitcast %class.btVector3* %mi1 to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %mx.addr, align 4
  %mx2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 1
  %4 = bitcast %class.btVector3* %mx2 to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

declare %struct.btDbvtNode* @_ZN6btDbvt6insertERK12btDbvtAabbMmPv(%struct.btDbvt*, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32), i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9push_backERKS0_(%class.btAlignedObjectArray* %this, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %struct.btCompoundShapeChild*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %struct.btCompoundShapeChild* %_Val, %struct.btCompoundShapeChild** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %1, i32 %2
  %3 = bitcast %struct.btCompoundShapeChild* %arrayidx to i8*
  %call5 = call i8* @_ZN20btCompoundShapeChildnwEmPv(i32 80, i8* %3)
  %4 = bitcast i8* %call5 to %struct.btCompoundShapeChild*
  %5 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %_Val.addr, align 4
  %call6 = call %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildC2ERKS_(%struct.btCompoundShapeChild* %4, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %5)
  %m_size7 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %6 = load i32, i32* %m_size7, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %m_size7, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN15btCompoundShape20updateChildTransformEiRK11btTransformb(%class.btCompoundShape* %this, i32 %childIndex, %class.btTransform* nonnull align 4 dereferenceable(64) %newChildTransform, i1 zeroext %shouldRecalculateLocalAabb) #2 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %childIndex.addr = alloca i32, align 4
  %newChildTransform.addr = alloca %class.btTransform*, align 4
  %shouldRecalculateLocalAabb.addr = alloca i8, align 1
  %localAabbMin = alloca %class.btVector3, align 4
  %localAabbMax = alloca %class.btVector3, align 4
  %bounds = alloca %struct.btDbvtAabbMm, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store i32 %childIndex, i32* %childIndex.addr, align 4
  store %class.btTransform* %newChildTransform, %class.btTransform** %newChildTransform.addr, align 4
  %frombool = zext i1 %shouldRecalculateLocalAabb to i8
  store i8 %frombool, i8* %shouldRecalculateLocalAabb.addr, align 1
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %newChildTransform.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %1 = load i32, i32* %childIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children, i32 %1)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 0
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transform, %class.btTransform* nonnull align 4 dereferenceable(64) %0)
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %2 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree, align 4
  %tobool = icmp ne %struct.btDbvt* %2, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMin)
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMax)
  %m_children5 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %3 = load i32, i32* %childIndex.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children5, i32 %3)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call6, i32 0, i32 1
  %4 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4
  %5 = load %class.btTransform*, %class.btTransform** %newChildTransform.addr, align 4
  %6 = bitcast %class.btCollisionShape* %4 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %6, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %7 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %7(%class.btCollisionShape* %4, %class.btTransform* nonnull align 4 dereferenceable(64) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %bounds, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  %m_dynamicAabbTree7 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %8 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree7, align 4
  %m_children8 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %9 = load i32, i32* %childIndex.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children8, i32 %9)
  %m_node = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call9, i32 0, i32 4
  %10 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_node, align 4
  call void @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm(%struct.btDbvt* %8, %struct.btDbvtNode* %10, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %bounds)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load i8, i8* %shouldRecalculateLocalAabb.addr, align 1
  %tobool10 = trunc i8 %11 to i1
  br i1 %tobool10, label %if.then11, label %if.end14

if.then11:                                        ; preds = %if.end
  %12 = bitcast %class.btCompoundShape* %this1 to void (%class.btCompoundShape*)***
  %vtable12 = load void (%class.btCompoundShape*)**, void (%class.btCompoundShape*)*** %12, align 4
  %vfn13 = getelementptr inbounds void (%class.btCompoundShape*)*, void (%class.btCompoundShape*)** %vtable12, i64 17
  %13 = load void (%class.btCompoundShape*)*, void (%class.btCompoundShape*)** %vfn13, align 4
  call void %13(%class.btCompoundShape* %this1)
  br label %if.end14

if.end14:                                         ; preds = %if.then11, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %0, i32 %1
  ret %struct.btCompoundShapeChild* %arrayidx
}

declare void @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm(%struct.btDbvt*, %struct.btDbvtNode*, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32)) #3

; Function Attrs: noinline optnone
define hidden void @_ZN15btCompoundShape23removeChildShapeByIndexEi(%class.btCompoundShape* %this, i32 %childShapeIndex) #2 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %childShapeIndex.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store i32 %childShapeIndex, i32* %childShapeIndex.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_updateRevision, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %1 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree, align 4
  %tobool = icmp ne %struct.btDbvt* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_dynamicAabbTree2 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %2 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree2, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %3 = load i32, i32* %childShapeIndex.addr, align 4
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children, i32 %3)
  %m_node = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 4
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_node, align 4
  call void @_ZN6btDbvt6removeEP10btDbvtNode(%struct.btDbvt* %2, %struct.btDbvtNode* %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_children3 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %5 = load i32, i32* %childShapeIndex.addr, align 4
  %m_children4 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children4)
  %sub = sub nsw i32 %call5, 1
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE4swapEii(%class.btAlignedObjectArray* %m_children3, i32 %5, i32 %sub)
  %m_dynamicAabbTree6 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %6 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree6, align 4
  %tobool7 = icmp ne %struct.btDbvt* %6, null
  br i1 %tobool7, label %if.then8, label %if.end12

if.then8:                                         ; preds = %if.end
  %7 = load i32, i32* %childShapeIndex.addr, align 4
  %m_children9 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %8 = load i32, i32* %childShapeIndex.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children9, i32 %8)
  %m_node11 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call10, i32 0, i32 4
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_node11, align 4
  %10 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %9, i32 0, i32 2
  %dataAsInt = bitcast %union.anon.0* %10 to i32*
  store i32 %7, i32* %dataAsInt, align 4
  br label %if.end12

if.end12:                                         ; preds = %if.then8, %if.end
  %m_children13 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE8pop_backEv(%class.btAlignedObjectArray* %m_children13)
  ret void
}

declare void @_ZN6btDbvt6removeEP10btDbvtNode(%struct.btDbvt*, %struct.btDbvtNode*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE4swapEii(%class.btAlignedObjectArray* %this, i32 %index0, i32 %index1) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.btCompoundShapeChild, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %0, i32 %1
  %call = call %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildC2ERKS_(%struct.btCompoundShapeChild* %temp, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %arrayidx)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data2, align 4
  %3 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %2, i32 %3
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data4, align 4
  %5 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %4, i32 %5
  %call6 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildaSERKS_(%struct.btCompoundShapeChild* %arrayidx5, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %arrayidx3)
  %m_data7 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %6 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data7, align 4
  %7 = load i32, i32* %index1.addr, align 4
  %arrayidx8 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %6, i32 %7
  %call9 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildaSERKS_(%struct.btCompoundShapeChild* %arrayidx8, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %temp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE8pop_backEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %1, i32 %2
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN15btCompoundShape16removeChildShapeEP16btCollisionShape(%class.btCompoundShape* %this, %class.btCollisionShape* %shape) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %shape.addr = alloca %class.btCollisionShape*, align 4
  %i = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store %class.btCollisionShape* %shape, %class.btCollisionShape** %shape.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_updateRevision, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  %sub = sub nsw i32 %call, 1
  store i32 %sub, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp sge i32 %1, 0
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_children2 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %2 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children2, i32 %2)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call3, i32 0, i32 1
  %3 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4
  %4 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4
  %cmp4 = icmp eq %class.btCollisionShape* %3, %4
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  call void @_ZN15btCompoundShape23removeChildShapeByIndexEi(%class.btCompoundShape* %this1, i32 %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %6 = load i32, i32* %i, align 4
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = bitcast %class.btCompoundShape* %this1 to void (%class.btCompoundShape*)***
  %vtable = load void (%class.btCompoundShape*)**, void (%class.btCompoundShape*)*** %7, align 4
  %vfn = getelementptr inbounds void (%class.btCompoundShape*)*, void (%class.btCompoundShape*)** %vtable, i64 17
  %8 = load void (%class.btCompoundShape*)*, void (%class.btCompoundShape*)** %vfn, align 4
  call void %8(%class.btCompoundShape* %this1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN15btCompoundShape20recalculateLocalAabbEv(%class.btCompoundShape* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %j = alloca i32, align 4
  %localAabbMin = alloca %class.btVector3, align 4
  %localAabbMax = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp2, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp3, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %m_localAabbMin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  %0 = bitcast %class.btVector3* %m_localAabbMin to i8*
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  store float 0xC3ABC16D60000000, float* %ref.tmp6, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp7, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp8, align 4
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  %m_localAabbMax = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %2 = bitcast %class.btVector3* %m_localAabbMax to i8*
  %3 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc43, %entry
  %4 = load i32, i32* %j, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call10 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  %cmp = icmp slt i32 %4, %call10
  br i1 %cmp, label %for.body, label %for.end45

for.body:                                         ; preds = %for.cond
  %call11 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMin)
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMax)
  %m_children13 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %5 = load i32, i32* %j, align 4
  %call14 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children13, i32 %5)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call14, i32 0, i32 1
  %6 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4
  %m_children15 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %7 = load i32, i32* %j, align 4
  %call16 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children15, i32 %7)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call16, i32 0, i32 0
  %8 = bitcast %class.btCollisionShape* %6 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %8, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %9 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %9(%class.btCollisionShape* %6, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transform, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  store i32 0, i32* %i, align 4
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc, %for.body
  %10 = load i32, i32* %i, align 4
  %cmp18 = icmp slt i32 %10, 3
  br i1 %cmp18, label %for.body19, label %for.end

for.body19:                                       ; preds = %for.cond17
  %m_localAabbMin20 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMin20)
  %11 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call21, i32 %11
  %12 = load float, float* %arrayidx, align 4
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMin)
  %13 = load i32, i32* %i, align 4
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 %13
  %14 = load float, float* %arrayidx23, align 4
  %cmp24 = fcmp ogt float %12, %14
  br i1 %cmp24, label %if.then, label %if.end

if.then:                                          ; preds = %for.body19
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMin)
  %15 = load i32, i32* %i, align 4
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 %15
  %16 = load float, float* %arrayidx26, align 4
  %m_localAabbMin27 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMin27)
  %17 = load i32, i32* %i, align 4
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 %17
  store float %16, float* %arrayidx29, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body19
  %m_localAabbMax30 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMax30)
  %18 = load i32, i32* %i, align 4
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 %18
  %19 = load float, float* %arrayidx32, align 4
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMax)
  %20 = load i32, i32* %i, align 4
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 %20
  %21 = load float, float* %arrayidx34, align 4
  %cmp35 = fcmp olt float %19, %21
  br i1 %cmp35, label %if.then36, label %if.end42

if.then36:                                        ; preds = %if.end
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAabbMax)
  %22 = load i32, i32* %i, align 4
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 %22
  %23 = load float, float* %arrayidx38, align 4
  %m_localAabbMax39 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMax39)
  %24 = load i32, i32* %i, align 4
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 %24
  store float %23, float* %arrayidx41, align 4
  br label %if.end42

if.end42:                                         ; preds = %if.then36, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end42
  %25 = load i32, i32* %i, align 4
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond17

for.end:                                          ; preds = %for.cond17
  br label %for.inc43

for.inc43:                                        ; preds = %for.end
  %26 = load i32, i32* %j, align 4
  %inc44 = add nsw i32 %26, 1
  store i32 %inc44, i32* %j, align 4
  br label %for.cond

for.end45:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: noinline optnone
define hidden void @_ZNK15btCompoundShape7getAabbERK11btTransformR9btVector3S4_(%class.btCompoundShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %localHalfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %localCenter = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center = alloca %class.btVector3, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  store float 5.000000e-01, float* %ref.tmp, align 4
  %m_localAabbMax = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %m_localAabbMin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMin)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localHalfExtents, float* nonnull align 4 dereferenceable(4) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  store float 5.000000e-01, float* %ref.tmp3, align 4
  %m_localAabbMax5 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 3
  %m_localAabbMin6 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMax5, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMin6)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 0.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %localHalfExtents, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  store float 0.000000e+00, float* %ref.tmp10, align 4
  store float 0.000000e+00, float* %ref.tmp11, align 4
  store float 0.000000e+00, float* %ref.tmp12, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %localCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %0 = bitcast %class.btCompoundShape* %this1 to float (%class.btCompoundShape*)***
  %vtable = load float (%class.btCompoundShape*)**, float (%class.btCompoundShape*)*** %0, align 4
  %vfn = getelementptr inbounds float (%class.btCompoundShape*)*, float (%class.btCompoundShape*)** %vtable, i64 12
  %1 = load float (%class.btCompoundShape*)*, float (%class.btCompoundShape*)** %vfn, align 4
  %call15 = call float %1(%class.btCompoundShape* %this1)
  store float %call15, float* %ref.tmp14, align 4
  %2 = bitcast %class.btCompoundShape* %this1 to float (%class.btCompoundShape*)***
  %vtable17 = load float (%class.btCompoundShape*)**, float (%class.btCompoundShape*)*** %2, align 4
  %vfn18 = getelementptr inbounds float (%class.btCompoundShape*)*, float (%class.btCompoundShape*)** %vtable17, i64 12
  %3 = load float (%class.btCompoundShape*)*, float (%class.btCompoundShape*)** %vfn18, align 4
  %call19 = call float %3(%class.btCompoundShape* %this1)
  store float %call19, float* %ref.tmp16, align 4
  %4 = bitcast %class.btCompoundShape* %this1 to float (%class.btCompoundShape*)***
  %vtable21 = load float (%class.btCompoundShape*)**, float (%class.btCompoundShape*)*** %4, align 4
  %vfn22 = getelementptr inbounds float (%class.btCompoundShape*)*, float (%class.btCompoundShape*)** %vtable21, i64 12
  %5 = load float (%class.btCompoundShape*)*, float (%class.btCompoundShape*)** %vfn22, align 4
  %call23 = call float %5(%class.btCompoundShape* %this1)
  store float %call23, float* %ref.tmp20, align 4
  %call24 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp20)
  %call25 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp13)
  %6 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %call26 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %6)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call26)
  %7 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %center, %class.btTransform* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %localCenter)
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call28 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent, %class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %call27, %class.btVector3* nonnull align 4 dereferenceable(16) %call28, %class.btVector3* nonnull align 4 dereferenceable(16) %call29)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %8 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %9 = bitcast %class.btVector3* %8 to i8*
  %10 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %12 = bitcast %class.btVector3* %11 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4
  %call2 = call float @_Z6btFabsf(float %1)
  store float %call2, float* %ref.tmp, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %2 = load float, float* %call6, align 4
  %call7 = call float @_Z6btFabsf(float %2)
  store float %call7, float* %ref.tmp3, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx10)
  %3 = load float, float* %call11, align 4
  %call12 = call float @_Z6btFabsf(float %3)
  store float %call12, float* %ref.tmp8, align 4
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx15)
  %4 = load float, float* %call16, align 4
  %call17 = call float @_Z6btFabsf(float %4)
  store float %call17, float* %ref.tmp13, align 4
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx20)
  %5 = load float, float* %call21, align 4
  %call22 = call float @_Z6btFabsf(float %5)
  store float %call22, float* %ref.tmp18, align 4
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx25)
  %6 = load float, float* %call26, align 4
  %call27 = call float @_Z6btFabsf(float %6)
  store float %call27, float* %ref.tmp23, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %7 = load float, float* %call31, align 4
  %call32 = call float @_Z6btFabsf(float %7)
  store float %call32, float* %ref.tmp28, align 4
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx35)
  %8 = load float, float* %call36, align 4
  %call37 = call float @_Z6btFabsf(float %8)
  store float %call37, float* %ref.tmp33, align 4
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx40)
  %9 = load float, float* %call41, align 4
  %call42 = call float @_Z6btFabsf(float %9)
  store float %call42, float* %ref.tmp38, align 4
  %call43 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZNK15btCompoundShape21calculateLocalInertiaEfR9btVector3(%class.btCompoundShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %ident = alloca %class.btTransform, align 4
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %halfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %lx = alloca float, align 4
  %ly = alloca float, align 4
  %lz = alloca float, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store float %mass, float* %mass.addr, align 4
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %ident)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %ident)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  %0 = bitcast %class.btCompoundShape* %this1 to void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %0, align 4
  %vfn = getelementptr inbounds void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %1 = load void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCompoundShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %1(%class.btCompoundShape* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %ident, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin)
  store float 5.000000e-01, float* %ref.tmp4, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %halfExtents)
  %2 = load float, float* %call5, align 4
  %mul = fmul float 2.000000e+00, %2
  store float %mul, float* %lx, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %halfExtents)
  %3 = load float, float* %call6, align 4
  %mul7 = fmul float 2.000000e+00, %3
  store float %mul7, float* %ly, align 4
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %halfExtents)
  %4 = load float, float* %call8, align 4
  %mul9 = fmul float 2.000000e+00, %4
  store float %mul9, float* %lz, align 4
  %5 = load float, float* %mass.addr, align 4
  %div = fdiv float %5, 1.200000e+01
  %6 = load float, float* %ly, align 4
  %7 = load float, float* %ly, align 4
  %mul10 = fmul float %6, %7
  %8 = load float, float* %lz, align 4
  %9 = load float, float* %lz, align 4
  %mul11 = fmul float %8, %9
  %add = fadd float %mul10, %mul11
  %mul12 = fmul float %div, %add
  %10 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %10)
  %arrayidx = getelementptr inbounds float, float* %call13, i32 0
  store float %mul12, float* %arrayidx, align 4
  %11 = load float, float* %mass.addr, align 4
  %div14 = fdiv float %11, 1.200000e+01
  %12 = load float, float* %lx, align 4
  %13 = load float, float* %lx, align 4
  %mul15 = fmul float %12, %13
  %14 = load float, float* %lz, align 4
  %15 = load float, float* %lz, align 4
  %mul16 = fmul float %14, %15
  %add17 = fadd float %mul15, %mul16
  %mul18 = fmul float %div14, %add17
  %16 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %call19 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %16)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  store float %mul18, float* %arrayidx20, align 4
  %17 = load float, float* %mass.addr, align 4
  %div21 = fdiv float %17, 1.200000e+01
  %18 = load float, float* %lx, align 4
  %19 = load float, float* %lx, align 4
  %mul22 = fmul float %18, %19
  %20 = load float, float* %ly, align 4
  %21 = load float, float* %ly, align 4
  %mul23 = fmul float %20, %21
  %add24 = fadd float %mul22, %mul23
  %mul25 = fmul float %div21, %add24
  %22 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %call26 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %22)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 2
  store float %mul25, float* %arrayidx27, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZNK15btCompoundShape31calculatePrincipalAxisTransformEPfR11btTransformR9btVector3(%class.btCompoundShape* %this, float* %masses, %class.btTransform* nonnull align 4 dereferenceable(64) %principal, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) #2 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %masses.addr = alloca float*, align 4
  %principal.addr = alloca %class.btTransform*, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %n = alloca i32, align 4
  %totalMass = alloca float, align 4
  %center = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %k = alloca i32, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %tensor = alloca %class.btMatrix3x3, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %i = alloca %class.btVector3, align 4
  %t = alloca %class.btTransform*, align 4
  %o = alloca %class.btVector3, align 4
  %j = alloca %class.btMatrix3x3, align 4
  %ref.tmp46 = alloca %class.btMatrix3x3, align 4
  %o2 = alloca float, align 4
  %ref.tmp60 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %ref.tmp63 = alloca float, align 4
  %ref.tmp64 = alloca float, align 4
  %ref.tmp66 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ref.tmp68 = alloca %class.btVector3, align 4
  %ref.tmp69 = alloca float, align 4
  %ref.tmp73 = alloca %class.btVector3, align 4
  %ref.tmp74 = alloca float, align 4
  %ref.tmp79 = alloca %class.btVector3, align 4
  %ref.tmp80 = alloca float, align 4
  %ref.tmp85 = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca %class.btVector3, align 4
  %ref.tmp95 = alloca %class.btVector3, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store float* %masses, float** %masses.addr, align 4
  store %class.btTransform* %principal, %class.btTransform** %principal.addr, align 4
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  store i32 %call, i32* %n, align 4
  store float 0.000000e+00, float* %totalMass, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %center, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  store i32 0, i32* %k, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %k, align 4
  %1 = load i32, i32* %n, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_children6 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %2 = load i32, i32* %k, align 4
  %call7 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children6, i32 %2)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call7, i32 0, i32 0
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %m_transform)
  %3 = load float*, float** %masses.addr, align 4
  %4 = load i32, i32* %k, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %call8, float* nonnull align 4 dereferenceable(4) %arrayidx)
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %center, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %5 = load float*, float** %masses.addr, align 4
  %6 = load i32, i32* %k, align 4
  %arrayidx10 = getelementptr inbounds float, float* %5, i32 %6
  %7 = load float, float* %arrayidx10, align 4
  %8 = load float, float* %totalMass, align 4
  %add = fadd float %8, %7
  store float %add, float* %totalMass, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %k, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %k, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %center, float* nonnull align 4 dereferenceable(4) %totalMass)
  %10 = load %class.btTransform*, %class.btTransform** %principal.addr, align 4
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %10, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  store float 0.000000e+00, float* %ref.tmp12, align 4
  store float 0.000000e+00, float* %ref.tmp13, align 4
  store float 0.000000e+00, float* %ref.tmp14, align 4
  store float 0.000000e+00, float* %ref.tmp15, align 4
  store float 0.000000e+00, float* %ref.tmp16, align 4
  store float 0.000000e+00, float* %ref.tmp17, align 4
  store float 0.000000e+00, float* %ref.tmp18, align 4
  store float 0.000000e+00, float* %ref.tmp19, align 4
  store float 0.000000e+00, float* %ref.tmp20, align 4
  %call21 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %tensor, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20)
  store i32 0, i32* %k, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc100, %for.end
  %11 = load i32, i32* %k, align 4
  %12 = load i32, i32* %n, align 4
  %cmp23 = icmp slt i32 %11, %12
  br i1 %cmp23, label %for.body24, label %for.end102

for.body24:                                       ; preds = %for.cond22
  %call25 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %i)
  %m_children26 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %13 = load i32, i32* %k, align 4
  %call27 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children26, i32 %13)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call27, i32 0, i32 1
  %14 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4
  %15 = load float*, float** %masses.addr, align 4
  %16 = load i32, i32* %k, align 4
  %arrayidx28 = getelementptr inbounds float, float* %15, i32 %16
  %17 = load float, float* %arrayidx28, align 4
  %18 = bitcast %class.btCollisionShape* %14 to void (%class.btCollisionShape*, float, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, float, %class.btVector3*)**, void (%class.btCollisionShape*, float, %class.btVector3*)*** %18, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, float, %class.btVector3*)*, void (%class.btCollisionShape*, float, %class.btVector3*)** %vtable, i64 8
  %19 = load void (%class.btCollisionShape*, float, %class.btVector3*)*, void (%class.btCollisionShape*, float, %class.btVector3*)** %vfn, align 4
  call void %19(%class.btCollisionShape* %14, float %17, %class.btVector3* nonnull align 4 dereferenceable(16) %i)
  %m_children29 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %20 = load i32, i32* %k, align 4
  %call30 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children29, i32 %20)
  %m_transform31 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call30, i32 0, i32 0
  store %class.btTransform* %m_transform31, %class.btTransform** %t, align 4
  %21 = load %class.btTransform*, %class.btTransform** %t, align 4
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %21)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %o, %class.btVector3* nonnull align 4 dereferenceable(16) %call32, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  %22 = load %class.btTransform*, %class.btTransform** %t, align 4
  %call33 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %22)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %j, %class.btMatrix3x3* %call33)
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %i)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 0
  %call36 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 0)
  %call37 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %call36, float* nonnull align 4 dereferenceable(4) %arrayidx35)
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %i)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 1
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 1)
  %call41 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %call40, float* nonnull align 4 dereferenceable(4) %arrayidx39)
  %call42 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %i)
  %arrayidx43 = getelementptr inbounds float, float* %call42, i32 2
  %call44 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 2)
  %call45 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %call44, float* nonnull align 4 dereferenceable(4) %arrayidx43)
  %23 = load %class.btTransform*, %class.btTransform** %t, align 4
  %call47 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %23)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp46, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call47, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %j)
  %call48 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %j, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp46)
  %call49 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 0)
  %call50 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 0)
  %call51 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call50, %class.btVector3* nonnull align 4 dereferenceable(16) %call49)
  %call52 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 1)
  %call53 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 1)
  %call54 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call53, %class.btVector3* nonnull align 4 dereferenceable(16) %call52)
  %call55 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 2)
  %call56 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 2)
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call56, %class.btVector3* nonnull align 4 dereferenceable(16) %call55)
  %call58 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %o)
  store float %call58, float* %o2, align 4
  %call59 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 0)
  store float 0.000000e+00, float* %ref.tmp60, align 4
  store float 0.000000e+00, float* %ref.tmp61, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call59, float* nonnull align 4 dereferenceable(4) %o2, float* nonnull align 4 dereferenceable(4) %ref.tmp60, float* nonnull align 4 dereferenceable(4) %ref.tmp61)
  %call62 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 1)
  store float 0.000000e+00, float* %ref.tmp63, align 4
  store float 0.000000e+00, float* %ref.tmp64, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call62, float* nonnull align 4 dereferenceable(4) %ref.tmp63, float* nonnull align 4 dereferenceable(4) %o2, float* nonnull align 4 dereferenceable(4) %ref.tmp64)
  %call65 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 2)
  store float 0.000000e+00, float* %ref.tmp66, align 4
  store float 0.000000e+00, float* %ref.tmp67, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %call65, float* nonnull align 4 dereferenceable(4) %ref.tmp66, float* nonnull align 4 dereferenceable(4) %ref.tmp67, float* nonnull align 4 dereferenceable(4) %o2)
  %call70 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %o)
  %24 = load float, float* %call70, align 4
  %fneg = fneg float %24
  store float %fneg, float* %ref.tmp69, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp68, %class.btVector3* nonnull align 4 dereferenceable(16) %o, float* nonnull align 4 dereferenceable(4) %ref.tmp69)
  %call71 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 0)
  %call72 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call71, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp68)
  %call75 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %o)
  %25 = load float, float* %call75, align 4
  %fneg76 = fneg float %25
  store float %fneg76, float* %ref.tmp74, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp73, %class.btVector3* nonnull align 4 dereferenceable(16) %o, float* nonnull align 4 dereferenceable(4) %ref.tmp74)
  %call77 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 1)
  %call78 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call77, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp73)
  %call81 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %o)
  %26 = load float, float* %call81, align 4
  %fneg82 = fneg float %26
  store float %fneg82, float* %ref.tmp80, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp79, %class.btVector3* nonnull align 4 dereferenceable(16) %o, float* nonnull align 4 dereferenceable(4) %ref.tmp80)
  %call83 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 2)
  %call84 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call83, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp79)
  %27 = load float*, float** %masses.addr, align 4
  %28 = load i32, i32* %k, align 4
  %arrayidx86 = getelementptr inbounds float, float* %27, i32 %28
  %call87 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 0)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp85, float* nonnull align 4 dereferenceable(4) %arrayidx86, %class.btVector3* nonnull align 4 dereferenceable(16) %call87)
  %call88 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 0)
  %call89 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call88, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp85)
  %29 = load float*, float** %masses.addr, align 4
  %30 = load i32, i32* %k, align 4
  %arrayidx91 = getelementptr inbounds float, float* %29, i32 %30
  %call92 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 1)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp90, float* nonnull align 4 dereferenceable(4) %arrayidx91, %class.btVector3* nonnull align 4 dereferenceable(16) %call92)
  %call93 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 1)
  %call94 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call93, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp90)
  %31 = load float*, float** %masses.addr, align 4
  %32 = load i32, i32* %k, align 4
  %arrayidx96 = getelementptr inbounds float, float* %31, i32 %32
  %call97 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %j, i32 2)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp95, float* nonnull align 4 dereferenceable(4) %arrayidx96, %class.btVector3* nonnull align 4 dereferenceable(16) %call97)
  %call98 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 2)
  %call99 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %call98, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp95)
  br label %for.inc100

for.inc100:                                       ; preds = %for.body24
  %33 = load i32, i32* %k, align 4
  %inc101 = add nsw i32 %33, 1
  store i32 %inc101, i32* %k, align 4
  br label %for.cond22

for.end102:                                       ; preds = %for.cond22
  %34 = load %class.btTransform*, %class.btTransform** %principal.addr, align 4
  %call103 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %34)
  call void @_ZN11btMatrix3x311diagonalizeERS_fi(%class.btMatrix3x3* %tensor, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call103, float 0x3EE4F8B580000000, i32 20)
  %35 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %call104 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 0)
  %call105 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call104)
  %arrayidx106 = getelementptr inbounds float, float* %call105, i32 0
  %call107 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 1)
  %call108 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call107)
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 1
  %call110 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %tensor, i32 2)
  %call111 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call110)
  %arrayidx112 = getelementptr inbounds float, float* %call111, i32 2
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %35, float* nonnull align 4 dereferenceable(4) %arrayidx106, float* nonnull align 4 dereferenceable(4) %arrayidx109, float* nonnull align 4 dereferenceable(4) %arrayidx112)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %0, i32 %1
  ret %struct.btCompoundShapeChild* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311diagonalizeERS_fi(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %rot, float %threshold, i32 %maxSteps) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %rot.addr = alloca %class.btMatrix3x3*, align 4
  %threshold.addr = alloca float, align 4
  %maxSteps.addr = alloca i32, align 4
  %step = alloca i32, align 4
  %p = alloca i32, align 4
  %q = alloca i32, align 4
  %r = alloca i32, align 4
  %max = alloca float, align 4
  %v = alloca float, align 4
  %t = alloca float, align 4
  %mpq = alloca float, align 4
  %theta = alloca float, align 4
  %theta2 = alloca float, align 4
  %cos = alloca float, align 4
  %sin = alloca float, align 4
  %mrp = alloca float, align 4
  %mrq = alloca float, align 4
  %i = alloca i32, align 4
  %row = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %rot, %class.btMatrix3x3** %rot.addr, align 4
  store float %threshold, float* %threshold.addr, align 4
  store i32 %maxSteps, i32* %maxSteps.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot.addr, align 4
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %0)
  %1 = load i32, i32* %maxSteps.addr, align 4
  store i32 %1, i32* %step, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc149, %entry
  %2 = load i32, i32* %step, align 4
  %cmp = icmp sgt i32 %2, 0
  br i1 %cmp, label %for.body, label %for.end150

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %p, align 4
  store i32 1, i32* %q, align 4
  store i32 2, i32* %r, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx)
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 1
  %3 = load float, float* %arrayidx2, align 4
  %call3 = call float @_Z6btFabsf(float %3)
  store float %call3, float* %max, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %4 = load float, float* %arrayidx7, align 4
  %call8 = call float @_Z6btFabsf(float %4)
  store float %call8, float* %v, align 4
  %5 = load float, float* %v, align 4
  %6 = load float, float* %max, align 4
  %cmp9 = fcmp ogt float %5, %6
  br i1 %cmp9, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 2, i32* %q, align 4
  store i32 1, i32* %r, align 4
  %7 = load float, float* %v, align 4
  store float %7, float* %max, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 1
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 2
  %8 = load float, float* %arrayidx13, align 4
  %call14 = call float @_Z6btFabsf(float %8)
  store float %call14, float* %v, align 4
  %9 = load float, float* %v, align 4
  %10 = load float, float* %max, align 4
  %cmp15 = fcmp ogt float %9, %10
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %if.end
  store i32 1, i32* %p, align 4
  store i32 2, i32* %q, align 4
  store i32 0, i32* %r, align 4
  %11 = load float, float* %v, align 4
  store float %11, float* %max, align 4
  br label %if.end17

if.end17:                                         ; preds = %if.then16, %if.end
  %12 = load float, float* %threshold.addr, align 4
  %m_el18 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx19 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el18, i32 0, i32 0
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx19)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 0
  %13 = load float, float* %arrayidx21, align 4
  %call22 = call float @_Z6btFabsf(float %13)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 1
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 1
  %14 = load float, float* %arrayidx26, align 4
  %call27 = call float @_Z6btFabsf(float %14)
  %add = fadd float %call22, %call27
  %m_el28 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx29 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el28, i32 0, i32 2
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx29)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %15 = load float, float* %arrayidx31, align 4
  %call32 = call float @_Z6btFabsf(float %15)
  %add33 = fadd float %add, %call32
  %mul = fmul float %12, %add33
  store float %mul, float* %t, align 4
  %16 = load float, float* %max, align 4
  %17 = load float, float* %t, align 4
  %cmp34 = fcmp ole float %16, %17
  br i1 %cmp34, label %if.then35, label %if.end40

if.then35:                                        ; preds = %if.end17
  %18 = load float, float* %max, align 4
  %19 = load float, float* %t, align 4
  %mul36 = fmul float 0x3E80000000000000, %19
  %cmp37 = fcmp ole float %18, %mul36
  br i1 %cmp37, label %if.then38, label %if.end39

if.then38:                                        ; preds = %if.then35
  br label %for.end150

if.end39:                                         ; preds = %if.then35
  store i32 1, i32* %step, align 4
  br label %if.end40

if.end40:                                         ; preds = %if.end39, %if.end17
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %20 = load i32, i32* %p, align 4
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 %20
  %call43 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx42)
  %21 = load i32, i32* %q, align 4
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 %21
  %22 = load float, float* %arrayidx44, align 4
  store float %22, float* %mpq, align 4
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %23 = load i32, i32* %q, align 4
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 %23
  %call47 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx46)
  %24 = load i32, i32* %q, align 4
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 %24
  %25 = load float, float* %arrayidx48, align 4
  %m_el49 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %26 = load i32, i32* %p, align 4
  %arrayidx50 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el49, i32 0, i32 %26
  %call51 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx50)
  %27 = load i32, i32* %p, align 4
  %arrayidx52 = getelementptr inbounds float, float* %call51, i32 %27
  %28 = load float, float* %arrayidx52, align 4
  %sub = fsub float %25, %28
  %29 = load float, float* %mpq, align 4
  %mul53 = fmul float 2.000000e+00, %29
  %div = fdiv float %sub, %mul53
  store float %div, float* %theta, align 4
  %30 = load float, float* %theta, align 4
  %31 = load float, float* %theta, align 4
  %mul54 = fmul float %30, %31
  store float %mul54, float* %theta2, align 4
  %32 = load float, float* %theta2, align 4
  %33 = load float, float* %theta2, align 4
  %mul55 = fmul float %32, %33
  %cmp56 = fcmp olt float %mul55, 0x4194000000000000
  br i1 %cmp56, label %if.then57, label %if.else

if.then57:                                        ; preds = %if.end40
  %34 = load float, float* %theta, align 4
  %cmp58 = fcmp oge float %34, 0.000000e+00
  br i1 %cmp58, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then57
  %35 = load float, float* %theta, align 4
  %36 = load float, float* %theta2, align 4
  %add59 = fadd float 1.000000e+00, %36
  %call60 = call float @_Z6btSqrtf(float %add59)
  %add61 = fadd float %35, %call60
  %div62 = fdiv float 1.000000e+00, %add61
  br label %cond.end

cond.false:                                       ; preds = %if.then57
  %37 = load float, float* %theta, align 4
  %38 = load float, float* %theta2, align 4
  %add63 = fadd float 1.000000e+00, %38
  %call64 = call float @_Z6btSqrtf(float %add63)
  %sub65 = fsub float %37, %call64
  %div66 = fdiv float 1.000000e+00, %sub65
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %div62, %cond.true ], [ %div66, %cond.false ]
  store float %cond, float* %t, align 4
  %39 = load float, float* %t, align 4
  %40 = load float, float* %t, align 4
  %mul67 = fmul float %39, %40
  %add68 = fadd float 1.000000e+00, %mul67
  %call69 = call float @_Z6btSqrtf(float %add68)
  %div70 = fdiv float 1.000000e+00, %call69
  store float %div70, float* %cos, align 4
  %41 = load float, float* %cos, align 4
  %42 = load float, float* %t, align 4
  %mul71 = fmul float %41, %42
  store float %mul71, float* %sin, align 4
  br label %if.end80

if.else:                                          ; preds = %if.end40
  %43 = load float, float* %theta, align 4
  %44 = load float, float* %theta2, align 4
  %div72 = fdiv float 5.000000e-01, %44
  %add73 = fadd float 2.000000e+00, %div72
  %mul74 = fmul float %43, %add73
  %div75 = fdiv float 1.000000e+00, %mul74
  store float %div75, float* %t, align 4
  %45 = load float, float* %t, align 4
  %mul76 = fmul float 5.000000e-01, %45
  %46 = load float, float* %t, align 4
  %mul77 = fmul float %mul76, %46
  %sub78 = fsub float 1.000000e+00, %mul77
  store float %sub78, float* %cos, align 4
  %47 = load float, float* %cos, align 4
  %48 = load float, float* %t, align 4
  %mul79 = fmul float %47, %48
  store float %mul79, float* %sin, align 4
  br label %if.end80

if.end80:                                         ; preds = %if.else, %cond.end
  %m_el81 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %49 = load i32, i32* %q, align 4
  %arrayidx82 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el81, i32 0, i32 %49
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx82)
  %50 = load i32, i32* %p, align 4
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 %50
  store float 0.000000e+00, float* %arrayidx84, align 4
  %m_el85 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %51 = load i32, i32* %p, align 4
  %arrayidx86 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el85, i32 0, i32 %51
  %call87 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx86)
  %52 = load i32, i32* %q, align 4
  %arrayidx88 = getelementptr inbounds float, float* %call87, i32 %52
  store float 0.000000e+00, float* %arrayidx88, align 4
  %53 = load float, float* %t, align 4
  %54 = load float, float* %mpq, align 4
  %mul89 = fmul float %53, %54
  %m_el90 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %55 = load i32, i32* %p, align 4
  %arrayidx91 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el90, i32 0, i32 %55
  %call92 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx91)
  %56 = load i32, i32* %p, align 4
  %arrayidx93 = getelementptr inbounds float, float* %call92, i32 %56
  %57 = load float, float* %arrayidx93, align 4
  %sub94 = fsub float %57, %mul89
  store float %sub94, float* %arrayidx93, align 4
  %58 = load float, float* %t, align 4
  %59 = load float, float* %mpq, align 4
  %mul95 = fmul float %58, %59
  %m_el96 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %60 = load i32, i32* %q, align 4
  %arrayidx97 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el96, i32 0, i32 %60
  %call98 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx97)
  %61 = load i32, i32* %q, align 4
  %arrayidx99 = getelementptr inbounds float, float* %call98, i32 %61
  %62 = load float, float* %arrayidx99, align 4
  %add100 = fadd float %62, %mul95
  store float %add100, float* %arrayidx99, align 4
  %m_el101 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %63 = load i32, i32* %r, align 4
  %arrayidx102 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el101, i32 0, i32 %63
  %call103 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx102)
  %64 = load i32, i32* %p, align 4
  %arrayidx104 = getelementptr inbounds float, float* %call103, i32 %64
  %65 = load float, float* %arrayidx104, align 4
  store float %65, float* %mrp, align 4
  %m_el105 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %66 = load i32, i32* %r, align 4
  %arrayidx106 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el105, i32 0, i32 %66
  %call107 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx106)
  %67 = load i32, i32* %q, align 4
  %arrayidx108 = getelementptr inbounds float, float* %call107, i32 %67
  %68 = load float, float* %arrayidx108, align 4
  store float %68, float* %mrq, align 4
  %69 = load float, float* %cos, align 4
  %70 = load float, float* %mrp, align 4
  %mul109 = fmul float %69, %70
  %71 = load float, float* %sin, align 4
  %72 = load float, float* %mrq, align 4
  %mul110 = fmul float %71, %72
  %sub111 = fsub float %mul109, %mul110
  %m_el112 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %73 = load i32, i32* %p, align 4
  %arrayidx113 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el112, i32 0, i32 %73
  %call114 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx113)
  %74 = load i32, i32* %r, align 4
  %arrayidx115 = getelementptr inbounds float, float* %call114, i32 %74
  store float %sub111, float* %arrayidx115, align 4
  %m_el116 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %75 = load i32, i32* %r, align 4
  %arrayidx117 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el116, i32 0, i32 %75
  %call118 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx117)
  %76 = load i32, i32* %p, align 4
  %arrayidx119 = getelementptr inbounds float, float* %call118, i32 %76
  store float %sub111, float* %arrayidx119, align 4
  %77 = load float, float* %cos, align 4
  %78 = load float, float* %mrq, align 4
  %mul120 = fmul float %77, %78
  %79 = load float, float* %sin, align 4
  %80 = load float, float* %mrp, align 4
  %mul121 = fmul float %79, %80
  %add122 = fadd float %mul120, %mul121
  %m_el123 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %81 = load i32, i32* %q, align 4
  %arrayidx124 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el123, i32 0, i32 %81
  %call125 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx124)
  %82 = load i32, i32* %r, align 4
  %arrayidx126 = getelementptr inbounds float, float* %call125, i32 %82
  store float %add122, float* %arrayidx126, align 4
  %m_el127 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %83 = load i32, i32* %r, align 4
  %arrayidx128 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el127, i32 0, i32 %83
  %call129 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx128)
  %84 = load i32, i32* %q, align 4
  %arrayidx130 = getelementptr inbounds float, float* %call129, i32 %84
  store float %add122, float* %arrayidx130, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond131

for.cond131:                                      ; preds = %for.inc, %if.end80
  %85 = load i32, i32* %i, align 4
  %cmp132 = icmp slt i32 %85, 3
  br i1 %cmp132, label %for.body133, label %for.end

for.body133:                                      ; preds = %for.cond131
  %86 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot.addr, align 4
  %87 = load i32, i32* %i, align 4
  %call134 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %86, i32 %87)
  store %class.btVector3* %call134, %class.btVector3** %row, align 4
  %88 = load %class.btVector3*, %class.btVector3** %row, align 4
  %call135 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %88)
  %89 = load i32, i32* %p, align 4
  %arrayidx136 = getelementptr inbounds float, float* %call135, i32 %89
  %90 = load float, float* %arrayidx136, align 4
  store float %90, float* %mrp, align 4
  %91 = load %class.btVector3*, %class.btVector3** %row, align 4
  %call137 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %91)
  %92 = load i32, i32* %q, align 4
  %arrayidx138 = getelementptr inbounds float, float* %call137, i32 %92
  %93 = load float, float* %arrayidx138, align 4
  store float %93, float* %mrq, align 4
  %94 = load float, float* %cos, align 4
  %95 = load float, float* %mrp, align 4
  %mul139 = fmul float %94, %95
  %96 = load float, float* %sin, align 4
  %97 = load float, float* %mrq, align 4
  %mul140 = fmul float %96, %97
  %sub141 = fsub float %mul139, %mul140
  %98 = load %class.btVector3*, %class.btVector3** %row, align 4
  %call142 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %98)
  %99 = load i32, i32* %p, align 4
  %arrayidx143 = getelementptr inbounds float, float* %call142, i32 %99
  store float %sub141, float* %arrayidx143, align 4
  %100 = load float, float* %cos, align 4
  %101 = load float, float* %mrq, align 4
  %mul144 = fmul float %100, %101
  %102 = load float, float* %sin, align 4
  %103 = load float, float* %mrp, align 4
  %mul145 = fmul float %102, %103
  %add146 = fadd float %mul144, %mul145
  %104 = load %class.btVector3*, %class.btVector3** %row, align 4
  %call147 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %104)
  %105 = load i32, i32* %q, align 4
  %arrayidx148 = getelementptr inbounds float, float* %call147, i32 %105
  store float %add146, float* %arrayidx148, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body133
  %106 = load i32, i32* %i, align 4
  %inc = add nsw i32 %106, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond131

for.end:                                          ; preds = %for.cond131
  br label %for.inc149

for.inc149:                                       ; preds = %for.end
  %107 = load i32, i32* %step, align 4
  %dec = add nsw i32 %107, -1
  store i32 %dec, i32* %step, align 4
  br label %for.cond

for.end150:                                       ; preds = %if.then38, %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define hidden void @_ZN15btCompoundShape15setLocalScalingERK9btVector3(%class.btCompoundShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %childTrans = alloca %class.btTransform, align 4
  %childScale = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %this1, i32 %1)
  %call3 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %childTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %call2)
  %m_children4 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %2 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children4, i32 %2)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call5, i32 0, i32 1
  %3 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4
  %4 = bitcast %class.btCollisionShape* %3 to %class.btVector3* (%class.btCollisionShape*)***
  %vtable = load %class.btVector3* (%class.btCollisionShape*)**, %class.btVector3* (%class.btCollisionShape*)*** %4, align 4
  %vfn = getelementptr inbounds %class.btVector3* (%class.btCollisionShape*)*, %class.btVector3* (%class.btCollisionShape*)** %vtable, i64 7
  %5 = load %class.btVector3* (%class.btCollisionShape*)*, %class.btVector3* (%class.btCollisionShape*)** %vfn, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* %5(%class.btCollisionShape* %3)
  %6 = bitcast %class.btVector3* %childScale to i8*
  %7 = bitcast %class.btVector3* %call6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %8 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %childScale, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %m_localScaling = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 7
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %9 = bitcast %class.btVector3* %childScale to i8*
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %m_children8 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %11 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children8, i32 %11)
  %m_childShape10 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call9, i32 0, i32 1
  %12 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape10, align 4
  %13 = bitcast %class.btCollisionShape* %12 to void (%class.btCollisionShape*, %class.btVector3*)***
  %vtable11 = load void (%class.btCollisionShape*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btVector3*)*** %13, align 4
  %vfn12 = getelementptr inbounds void (%class.btCollisionShape*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btVector3*)** %vtable11, i64 6
  %14 = load void (%class.btCollisionShape*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btVector3*)** %vfn12, align 4
  call void %14(%class.btCollisionShape* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %childScale)
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %childTrans)
  %15 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %call15, %class.btVector3* nonnull align 4 dereferenceable(16) %15)
  %m_localScaling16 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 7
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling16)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %childTrans, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp13)
  %16 = load i32, i32* %i, align 4
  call void @_ZN15btCompoundShape20updateChildTransformEiRK11btTransformb(%class.btCompoundShape* %this1, i32 %16, %class.btTransform* nonnull align 4 dereferenceable(64) %childTrans, i1 zeroext false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %i, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %18 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  %m_localScaling17 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 7
  %19 = bitcast %class.btVector3* %m_localScaling17 to i8*
  %20 = bitcast %class.btVector3* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  %21 = bitcast %class.btCompoundShape* %this1 to void (%class.btCompoundShape*)***
  %vtable18 = load void (%class.btCompoundShape*)**, void (%class.btCompoundShape*)*** %21, align 4
  %vfn19 = getelementptr inbounds void (%class.btCompoundShape*)*, void (%class.btCompoundShape*)** %vtable18, i64 17
  %22 = load void (%class.btCompoundShape*)*, void (%class.btCompoundShape*)** %vfn19, align 4
  call void %22(%class.btCompoundShape* %this1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN15btCompoundShape17getChildTransformEi(%class.btCompoundShape* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children, i32 %0)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call, i32 0, i32 0
  ret %class.btTransform* %m_transform
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %div = fdiv float %1, %3
  store float %div, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %div8 = fdiv float %5, %7
  store float %div8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %div14 = fdiv float %9, %11
  store float %div14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define hidden void @_ZN15btCompoundShape26createAabbTreeFromChildrenEv(%class.btCompoundShape* %this) #2 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %mem = alloca i8*, align 4
  %index = alloca i32, align 4
  %child = alloca %struct.btCompoundShapeChild*, align 4
  %localAabbMin = alloca %class.btVector3, align 4
  %localAabbMax = alloca %class.btVector3, align 4
  %bounds = alloca %struct.btDbvtAabbMm, align 4
  %index2 = alloca i32, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_dynamicAabbTree = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %0 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree, align 4
  %tobool = icmp ne %struct.btDbvt* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 40, i32 16)
  store i8* %call, i8** %mem, align 4
  %1 = load i8*, i8** %mem, align 4
  %2 = bitcast i8* %1 to %struct.btDbvt*
  %call2 = call %struct.btDbvt* @_ZN6btDbvtC1Ev(%struct.btDbvt* %2)
  %m_dynamicAabbTree3 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  store %struct.btDbvt* %2, %struct.btDbvt** %m_dynamicAabbTree3, align 4
  store i32 0, i32* %index, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %index, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  %cmp = icmp slt i32 %3, %call4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_children5 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %4 = load i32, i32* %index, align 4
  %call6 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children5, i32 %4)
  store %struct.btCompoundShapeChild* %call6, %struct.btCompoundShapeChild** %child, align 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMin)
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAabbMax)
  %5 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %child, align 4
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %5, i32 0, i32 1
  %6 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4
  %7 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %child, align 4
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %7, i32 0, i32 0
  %8 = bitcast %class.btCollisionShape* %6 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %8, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %9 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %9(%class.btCollisionShape* %6, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transform, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %bounds, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax)
  %10 = load i32, i32* %index, align 4
  store i32 %10, i32* %index2, align 4
  %m_dynamicAabbTree9 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 4
  %11 = load %struct.btDbvt*, %struct.btDbvt** %m_dynamicAabbTree9, align 4
  %12 = load i32, i32* %index2, align 4
  %13 = inttoptr i32 %12 to i8*
  %call10 = call %struct.btDbvtNode* @_ZN6btDbvt6insertERK12btDbvtAabbMmPv(%struct.btDbvt* %11, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %bounds, i8* %13)
  %14 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %child, align 4
  %m_node = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %14, i32 0, i32 4
  store %struct.btDbvtNode* %call10, %struct.btDbvtNode** %m_node, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %index, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %index, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden i8* @_ZNK15btCompoundShape9serializeEPvP12btSerializer(%class.btCompoundShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btCompoundShapeData*, align 4
  %chunk = alloca %class.btChunk*, align 4
  %memPtr = alloca %struct.btCompoundShapeChildData*, align 4
  %i = alloca i32, align 4
  %chunk29 = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btCompoundShapeData*
  store %struct.btCompoundShapeData* %1, %struct.btCompoundShapeData** %shapeData, align 4
  %2 = bitcast %class.btCompoundShape* %this1 to %class.btCollisionShape*
  %3 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4
  %m_collisionShapeData = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %3, i32 0, i32 0
  %4 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %2, i8* %4, %class.btSerializer* %5)
  %m_collisionMargin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 6
  %6 = load float, float* %m_collisionMargin, align 4
  %7 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4
  %m_collisionMargin2 = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %7, i32 0, i32 3
  store float %6, float* %m_collisionMargin2, align 4
  %m_children = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %m_children)
  %8 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4
  %m_numChildShapes = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %8, i32 0, i32 2
  store i32 %call3, i32* %m_numChildShapes, align 4
  %9 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4
  %m_childShapePtr = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %9, i32 0, i32 1
  store %struct.btCompoundShapeChildData* null, %struct.btCompoundShapeChildData** %m_childShapePtr, align 4
  %10 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4
  %m_numChildShapes4 = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %10, i32 0, i32 2
  %11 = load i32, i32* %m_numChildShapes4, align 4
  %tobool = icmp ne i32 %11, 0
  br i1 %tobool, label %if.then, label %if.end60

if.then:                                          ; preds = %entry
  %12 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %13 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4
  %m_numChildShapes5 = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %13, i32 0, i32 2
  %14 = load i32, i32* %m_numChildShapes5, align 4
  %15 = bitcast %class.btSerializer* %12 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %15, align 4
  %vfn = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable, i64 4
  %16 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn, align 4
  %call6 = call %class.btChunk* %16(%class.btSerializer* %12, i32 76, i32 %14)
  store %class.btChunk* %call6, %class.btChunk** %chunk, align 4
  %17 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %17, i32 0, i32 2
  %18 = load i8*, i8** %m_oldPtr, align 4
  %19 = bitcast i8* %18 to %struct.btCompoundShapeChildData*
  store %struct.btCompoundShapeChildData* %19, %struct.btCompoundShapeChildData** %memPtr, align 4
  %20 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %21 = load %struct.btCompoundShapeChildData*, %struct.btCompoundShapeChildData** %memPtr, align 4
  %22 = bitcast %struct.btCompoundShapeChildData* %21 to i8*
  %23 = bitcast %class.btSerializer* %20 to i8* (%class.btSerializer*, i8*)***
  %vtable7 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %23, align 4
  %vfn8 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable7, i64 7
  %24 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn8, align 4
  %call9 = call i8* %24(%class.btSerializer* %20, i8* %22)
  %25 = bitcast i8* %call9 to %struct.btCompoundShapeChildData*
  %26 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4
  %m_childShapePtr10 = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %26, i32 0, i32 1
  store %struct.btCompoundShapeChildData* %25, %struct.btCompoundShapeChildData** %m_childShapePtr10, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %27 = load i32, i32* %i, align 4
  %28 = load %struct.btCompoundShapeData*, %struct.btCompoundShapeData** %shapeData, align 4
  %m_numChildShapes11 = getelementptr inbounds %struct.btCompoundShapeData, %struct.btCompoundShapeData* %28, i32 0, i32 2
  %29 = load i32, i32* %m_numChildShapes11, align 4
  %cmp = icmp slt i32 %27, %29
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_children12 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %30 = load i32, i32* %i, align 4
  %call13 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children12, i32 %30)
  %m_childMargin = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call13, i32 0, i32 3
  %31 = load float, float* %m_childMargin, align 4
  %32 = load %struct.btCompoundShapeChildData*, %struct.btCompoundShapeChildData** %memPtr, align 4
  %m_childMargin14 = getelementptr inbounds %struct.btCompoundShapeChildData, %struct.btCompoundShapeChildData* %32, i32 0, i32 3
  store float %31, float* %m_childMargin14, align 4
  %33 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %m_children15 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %34 = load i32, i32* %i, align 4
  %call16 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children15, i32 %34)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call16, i32 0, i32 1
  %35 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape, align 4
  %36 = bitcast %class.btCollisionShape* %35 to i8*
  %37 = bitcast %class.btSerializer* %33 to i8* (%class.btSerializer*, i8*)***
  %vtable17 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %37, align 4
  %vfn18 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable17, i64 7
  %38 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn18, align 4
  %call19 = call i8* %38(%class.btSerializer* %33, i8* %36)
  %39 = bitcast i8* %call19 to %struct.btCollisionShapeData*
  %40 = load %struct.btCompoundShapeChildData*, %struct.btCompoundShapeChildData** %memPtr, align 4
  %m_childShape20 = getelementptr inbounds %struct.btCompoundShapeChildData, %struct.btCompoundShapeChildData* %40, i32 0, i32 1
  store %struct.btCollisionShapeData* %39, %struct.btCollisionShapeData** %m_childShape20, align 4
  %41 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %m_children21 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %42 = load i32, i32* %i, align 4
  %call22 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children21, i32 %42)
  %m_childShape23 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call22, i32 0, i32 1
  %43 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape23, align 4
  %44 = bitcast %class.btCollisionShape* %43 to i8*
  %45 = bitcast %class.btSerializer* %41 to i8* (%class.btSerializer*, i8*)***
  %vtable24 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %45, align 4
  %vfn25 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable24, i64 6
  %46 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn25, align 4
  %call26 = call i8* %46(%class.btSerializer* %41, i8* %44)
  %tobool27 = icmp ne i8* %call26, null
  br i1 %tobool27, label %if.end, label %if.then28

if.then28:                                        ; preds = %for.body
  %47 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %m_children30 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %48 = load i32, i32* %i, align 4
  %call31 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children30, i32 %48)
  %m_childShape32 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call31, i32 0, i32 1
  %49 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape32, align 4
  %50 = bitcast %class.btCollisionShape* %49 to i32 (%class.btCollisionShape*)***
  %vtable33 = load i32 (%class.btCollisionShape*)**, i32 (%class.btCollisionShape*)*** %50, align 4
  %vfn34 = getelementptr inbounds i32 (%class.btCollisionShape*)*, i32 (%class.btCollisionShape*)** %vtable33, i64 13
  %51 = load i32 (%class.btCollisionShape*)*, i32 (%class.btCollisionShape*)** %vfn34, align 4
  %call35 = call i32 %51(%class.btCollisionShape* %49)
  %52 = bitcast %class.btSerializer* %47 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable36 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %52, align 4
  %vfn37 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable36, i64 4
  %53 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn37, align 4
  %call38 = call %class.btChunk* %53(%class.btSerializer* %47, i32 %call35, i32 1)
  store %class.btChunk* %call38, %class.btChunk** %chunk29, align 4
  %m_children39 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %54 = load i32, i32* %i, align 4
  %call40 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children39, i32 %54)
  %m_childShape41 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call40, i32 0, i32 1
  %55 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape41, align 4
  %56 = load %class.btChunk*, %class.btChunk** %chunk29, align 4
  %m_oldPtr42 = getelementptr inbounds %class.btChunk, %class.btChunk* %56, i32 0, i32 2
  %57 = load i8*, i8** %m_oldPtr42, align 4
  %58 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %59 = bitcast %class.btCollisionShape* %55 to i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)***
  %vtable43 = load i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)**, i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)*** %59, align 4
  %vfn44 = getelementptr inbounds i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)*, i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)** %vtable43, i64 14
  %60 = load i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)*, i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)** %vfn44, align 4
  %call45 = call i8* %60(%class.btCollisionShape* %55, i8* %57, %class.btSerializer* %58)
  store i8* %call45, i8** %structType, align 4
  %61 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %62 = load %class.btChunk*, %class.btChunk** %chunk29, align 4
  %63 = load i8*, i8** %structType, align 4
  %m_children46 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %64 = load i32, i32* %i, align 4
  %call47 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children46, i32 %64)
  %m_childShape48 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call47, i32 0, i32 1
  %65 = load %class.btCollisionShape*, %class.btCollisionShape** %m_childShape48, align 4
  %66 = bitcast %class.btCollisionShape* %65 to i8*
  %67 = bitcast %class.btSerializer* %61 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable49 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %67, align 4
  %vfn50 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable49, i64 5
  %68 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn50, align 4
  call void %68(%class.btSerializer* %61, %class.btChunk* %62, i8* %63, i32 1346455635, i8* %66)
  br label %if.end

if.end:                                           ; preds = %if.then28, %for.body
  %m_children51 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %69 = load i32, i32* %i, align 4
  %call52 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children51, i32 %69)
  %m_childShapeType = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call52, i32 0, i32 2
  %70 = load i32, i32* %m_childShapeType, align 4
  %71 = load %struct.btCompoundShapeChildData*, %struct.btCompoundShapeChildData** %memPtr, align 4
  %m_childShapeType53 = getelementptr inbounds %struct.btCompoundShapeChildData, %struct.btCompoundShapeChildData* %71, i32 0, i32 2
  store i32 %70, i32* %m_childShapeType53, align 4
  %m_children54 = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 1
  %72 = load i32, i32* %i, align 4
  %call55 = call nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildEixEi(%class.btAlignedObjectArray* %m_children54, i32 %72)
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %call55, i32 0, i32 0
  %73 = load %struct.btCompoundShapeChildData*, %struct.btCompoundShapeChildData** %memPtr, align 4
  %m_transform56 = getelementptr inbounds %struct.btCompoundShapeChildData, %struct.btCompoundShapeChildData* %73, i32 0, i32 0
  call void @_ZNK11btTransform14serializeFloatER20btTransformFloatData(%class.btTransform* %m_transform, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_transform56)
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %74 = load i32, i32* %i, align 4
  %inc = add nsw i32 %74, 1
  store i32 %inc, i32* %i, align 4
  %75 = load %struct.btCompoundShapeChildData*, %struct.btCompoundShapeChildData** %memPtr, align 4
  %incdec.ptr = getelementptr inbounds %struct.btCompoundShapeChildData, %struct.btCompoundShapeChildData* %75, i32 1
  store %struct.btCompoundShapeChildData* %incdec.ptr, %struct.btCompoundShapeChildData** %memPtr, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %76 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %77 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %78 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %m_oldPtr57 = getelementptr inbounds %class.btChunk, %class.btChunk* %78, i32 0, i32 2
  %79 = load i8*, i8** %m_oldPtr57, align 4
  %80 = bitcast %class.btSerializer* %76 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable58 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %80, align 4
  %vfn59 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable58, i64 5
  %81 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn59, align 4
  call void %81(%class.btSerializer* %76, %class.btChunk* %77, i8* getelementptr inbounds ([25 x i8], [25 x i8]* @.str, i32 0, i32 0), i32 1497453121, i8* %79)
  br label %if.end60

if.end60:                                         ; preds = %for.end, %entry
  ret i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.1, i32 0, i32 0)
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform14serializeFloatER20btTransformFloatData(%class.btTransform* %this, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %dataOut.addr = alloca %struct.btTransformFloatData*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %struct.btTransformFloatData* %dataOut, %struct.btTransformFloatData** %dataOut.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_basis2 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %0, i32 0, i32 0
  call void @_ZNK11btMatrix3x314serializeFloatER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_basis, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_origin3 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %1, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_origin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_origin3)
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #3

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #3

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btCompoundShape15getLocalScalingEv(%class.btCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 7
  ret %class.btVector3* %m_localScaling
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK15btCompoundShape7getNameEv(%class.btCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str.2, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btCompoundShape9setMarginEf(%class.btCompoundShape* %this, float %margin) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 6
  store float %0, float* %m_collisionMargin, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK15btCompoundShape9getMarginEv(%class.btCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btCompoundShape, %class.btCompoundShape* %this1, i32 0, i32 6
  %0 = load float, float* %m_collisionMargin, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK15btCompoundShape28calculateSerializeBufferSizeEv(%class.btCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCompoundShape*, align 4
  store %class.btCompoundShape* %this, %class.btCompoundShape** %this.addr, align 4
  %this1 = load %class.btCompoundShape*, %class.btCompoundShape** %this.addr, align 4
  ret i32 24
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mi)
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mx)
  ret %struct.btDbvtAabbMm* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x314serializeFloatER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %1
  %2 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %2, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %3
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btCompoundShapeChild* null, %struct.btCompoundShapeChild** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4
  %tobool = icmp ne %struct.btCompoundShapeChild* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btCompoundShapeChild* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btCompoundShapeChild* null, %struct.btCompoundShapeChild** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btCompoundShapeChild* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btCompoundShapeChild*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %struct.btCompoundShapeChild* %ptr, %struct.btCompoundShapeChild** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %ptr.addr, align 4
  %1 = bitcast %struct.btCompoundShapeChild* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btCompoundShapeChild* @_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btCompoundShapeChild** null)
  %2 = bitcast %struct.btCompoundShapeChild* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI20btCompoundShapeChildE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btCompoundShapeChild* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btCompoundShapeChild*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btCompoundShapeChild* %dest, %struct.btCompoundShapeChild** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %3, i32 %4
  %5 = bitcast %struct.btCompoundShapeChild* %arrayidx to i8*
  %call = call i8* @_ZN20btCompoundShapeChildnwEmPv(i32 80, i8* %5)
  %6 = bitcast i8* %call to %struct.btCompoundShapeChild*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %7, i32 %8
  %call3 = call %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildC2ERKS_(%struct.btCompoundShapeChild* %6, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btCompoundShapeChild* @_ZN18btAlignedAllocatorI20btCompoundShapeChildLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btCompoundShapeChild** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btCompoundShapeChild**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btCompoundShapeChild** %hint, %struct.btCompoundShapeChild*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 80, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btCompoundShapeChild*
  ret %struct.btCompoundShapeChild* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN20btCompoundShapeChildnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildC2ERKS_(%struct.btCompoundShapeChild* returned %this, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btCompoundShapeChild*, align 4
  %.addr = alloca %struct.btCompoundShapeChild*, align 4
  store %struct.btCompoundShapeChild* %this, %struct.btCompoundShapeChild** %this.addr, align 4
  store %struct.btCompoundShapeChild* %0, %struct.btCompoundShapeChild** %.addr, align 4
  %this1 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %this.addr, align 4
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %this1, i32 0, i32 0
  %1 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %.addr, align 4
  %m_transform2 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %1, i32 0, i32 0
  %call = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_transform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transform2)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %this1, i32 0, i32 1
  %2 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %.addr, align 4
  %m_childShape3 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %2, i32 0, i32 1
  %3 = bitcast %class.btCollisionShape** %m_childShape to i8*
  %4 = bitcast %class.btCollisionShape** %m_childShape3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %3, i8* align 4 %4, i64 16, i1 false)
  ret %struct.btCompoundShapeChild* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i64, i1 immarg) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI20btCompoundShapeChildE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(80) %struct.btCompoundShapeChild* @_ZN20btCompoundShapeChildaSERKS_(%struct.btCompoundShapeChild* %this, %struct.btCompoundShapeChild* nonnull align 4 dereferenceable(80) %0) #2 comdat {
entry:
  %this.addr = alloca %struct.btCompoundShapeChild*, align 4
  %.addr = alloca %struct.btCompoundShapeChild*, align 4
  store %struct.btCompoundShapeChild* %this, %struct.btCompoundShapeChild** %this.addr, align 4
  store %struct.btCompoundShapeChild* %0, %struct.btCompoundShapeChild** %.addr, align 4
  %this1 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %this.addr, align 4
  %m_transform = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %this1, i32 0, i32 0
  %1 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %.addr, align 4
  %m_transform2 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transform, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transform2)
  %m_childShape = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %this1, i32 0, i32 1
  %2 = load %struct.btCompoundShapeChild*, %struct.btCompoundShapeChild** %.addr, align 4
  %m_childShape3 = getelementptr inbounds %struct.btCompoundShapeChild, %struct.btCompoundShapeChild* %2, i32 0, i32 1
  %3 = bitcast %class.btCollisionShape** %m_childShape to i8*
  %4 = bitcast %class.btCollisionShape** %m_childShape3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* align 4 %3, i8* align 4 %4, i64 16, i1 false)
  ret %struct.btCompoundShapeChild* %this1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btCompoundShape.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
