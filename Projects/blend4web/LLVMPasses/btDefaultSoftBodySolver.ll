; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletSoftBody/btDefaultSoftBodySolver.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletSoftBody/btDefaultSoftBodySolver.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btDefaultSoftBodySolver = type { %class.btSoftBodySolver, i8, [3 x i8], %class.btAlignedObjectArray }
%class.btSoftBodySolver = type { i32 (...)**, i32, i32, float }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btSoftBody**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btSoftBody = type { %class.btCollisionObject, %class.btAlignedObjectArray.0, %class.btSoftBodySolver*, %"struct.btSoftBody::Config", %"struct.btSoftBody::SolverState", %"struct.btSoftBody::Pose", i8*, %struct.btSoftBodyWorldInfo*, %class.btAlignedObjectArray.23, %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.32, %class.btAlignedObjectArray.36, %class.btAlignedObjectArray.40, %class.btAlignedObjectArray.44, %class.btAlignedObjectArray.52, %class.btAlignedObjectArray.56, %class.btAlignedObjectArray.60, %class.btAlignedObjectArray.68, float, [2 x %class.btVector3], i8, %struct.btDbvt, %struct.btDbvt, %struct.btDbvt, %class.btAlignedObjectArray.76, %class.btAlignedObjectArray.80, %class.btTransform, %class.btVector3, float, %class.btAlignedObjectArray.84 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.0, i32, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%"struct.btSoftBody::Config" = type { i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.7, %class.btAlignedObjectArray.7 }
%class.btAlignedObjectArray.3 = type <{ %class.btAlignedAllocator.4, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.4 = type { i8 }
%class.btAlignedObjectArray.7 = type <{ %class.btAlignedAllocator.8, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.8 = type { i8 }
%"struct.btSoftBody::SolverState" = type { float, float, float, float, float }
%"struct.btSoftBody::Pose" = type { i8, i8, float, %class.btAlignedObjectArray.11, %class.btAlignedObjectArray.15, %class.btVector3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3 }
%class.btAlignedObjectArray.11 = type <{ %class.btAlignedAllocator.12, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.12 = type { i8 }
%class.btAlignedObjectArray.15 = type <{ %class.btAlignedAllocator.16, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.16 = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btSoftBodyWorldInfo = type { float, float, float, float, %class.btVector3, %class.btBroadphaseInterface*, %class.btDispatcher*, %class.btVector3, %struct.btSparseSdf }
%class.btBroadphaseInterface = type opaque
%class.btDispatcher = type opaque
%struct.btSparseSdf = type { %class.btAlignedObjectArray.19, float, i32, i32, i32, i32, i32 }
%class.btAlignedObjectArray.19 = type <{ %class.btAlignedAllocator.20, [3 x i8], i32, i32, %"struct.btSparseSdf<3>::Cell"**, i8, [3 x i8] }>
%class.btAlignedAllocator.20 = type { i8 }
%"struct.btSparseSdf<3>::Cell" = type opaque
%class.btAlignedObjectArray.23 = type <{ %class.btAlignedAllocator.24, [3 x i8], i32, i32, %"struct.btSoftBody::Note"*, i8, [3 x i8] }>
%class.btAlignedAllocator.24 = type { i8 }
%"struct.btSoftBody::Note" = type { %"struct.btSoftBody::Element", i8*, %class.btVector3, i32, [4 x %"struct.btSoftBody::Node"*], [4 x float] }
%"struct.btSoftBody::Element" = type { i8* }
%"struct.btSoftBody::Node" = type <{ %"struct.btSoftBody::Feature", %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, %struct.btDbvtNode*, i8, [3 x i8] }>
%"struct.btSoftBody::Feature" = type { %"struct.btSoftBody::Element", %"struct.btSoftBody::Material"* }
%"struct.btSoftBody::Material" = type { %"struct.btSoftBody::Element", float, float, float, i32 }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.26 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.26 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray.28 = type <{ %class.btAlignedAllocator.29, [3 x i8], i32, i32, %"struct.btSoftBody::Node"*, i8, [3 x i8] }>
%class.btAlignedAllocator.29 = type { i8 }
%class.btAlignedObjectArray.32 = type <{ %class.btAlignedAllocator.33, [3 x i8], i32, i32, %"struct.btSoftBody::Link"*, i8, [3 x i8] }>
%class.btAlignedAllocator.33 = type { i8 }
%"struct.btSoftBody::Link" = type { %"struct.btSoftBody::Feature", %class.btVector3, [2 x %"struct.btSoftBody::Node"*], float, i8, float, float, float }
%class.btAlignedObjectArray.36 = type <{ %class.btAlignedAllocator.37, [3 x i8], i32, i32, %"struct.btSoftBody::Face"*, i8, [3 x i8] }>
%class.btAlignedAllocator.37 = type { i8 }
%"struct.btSoftBody::Face" = type { %"struct.btSoftBody::Feature", [3 x %"struct.btSoftBody::Node"*], %class.btVector3, float, %struct.btDbvtNode* }
%class.btAlignedObjectArray.40 = type <{ %class.btAlignedAllocator.41, [3 x i8], i32, i32, %"struct.btSoftBody::Tetra"*, i8, [3 x i8] }>
%class.btAlignedAllocator.41 = type { i8 }
%"struct.btSoftBody::Tetra" = type { %"struct.btSoftBody::Feature", [4 x %"struct.btSoftBody::Node"*], float, %struct.btDbvtNode*, [4 x %class.btVector3], float, float }
%class.btAlignedObjectArray.44 = type <{ %class.btAlignedAllocator.45, [3 x i8], i32, i32, %"struct.btSoftBody::Anchor"*, i8, [3 x i8] }>
%class.btAlignedAllocator.45 = type { i8 }
%"struct.btSoftBody::Anchor" = type { %"struct.btSoftBody::Node"*, %class.btVector3, %class.btRigidBody*, float, %class.btMatrix3x3, %class.btVector3, float }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.47, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.47 = type <{ %class.btAlignedAllocator.48, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.48 = type { i8 }
%class.btTypedConstraint = type opaque
%class.btAlignedObjectArray.52 = type <{ %class.btAlignedAllocator.53, [3 x i8], i32, i32, %"struct.btSoftBody::RContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.53 = type { i8 }
%"struct.btSoftBody::RContact" = type { %"struct.btSoftBody::sCti", %"struct.btSoftBody::Node"*, %class.btMatrix3x3, %class.btVector3, float, float, float }
%"struct.btSoftBody::sCti" = type { %class.btCollisionObject*, %class.btVector3, float }
%class.btAlignedObjectArray.56 = type <{ %class.btAlignedAllocator.57, [3 x i8], i32, i32, %"struct.btSoftBody::SContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.57 = type { i8 }
%"struct.btSoftBody::SContact" = type { %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Face"*, %class.btVector3, %class.btVector3, float, float, [2 x float] }
%class.btAlignedObjectArray.60 = type <{ %class.btAlignedAllocator.61, [3 x i8], i32, i32, %"struct.btSoftBody::Joint"**, i8, [3 x i8] }>
%class.btAlignedAllocator.61 = type { i8 }
%"struct.btSoftBody::Joint" = type <{ i32 (...)**, [2 x %"struct.btSoftBody::Body"], [2 x %class.btVector3], float, float, float, %class.btVector3, %class.btVector3, %class.btMatrix3x3, i8, [3 x i8] }>
%"struct.btSoftBody::Body" = type { %"struct.btSoftBody::Cluster"*, %class.btRigidBody*, %class.btCollisionObject* }
%"struct.btSoftBody::Cluster" = type { %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.63, %class.btAlignedObjectArray.11, %class.btTransform, float, float, %class.btMatrix3x3, %class.btMatrix3x3, %class.btVector3, [2 x %class.btVector3], [2 x %class.btVector3], i32, i32, %class.btVector3, %class.btVector3, %struct.btDbvtNode*, float, float, float, float, float, float, i8, i8, i32 }
%class.btAlignedObjectArray.63 = type <{ %class.btAlignedAllocator.64, [3 x i8], i32, i32, %"struct.btSoftBody::Node"**, i8, [3 x i8] }>
%class.btAlignedAllocator.64 = type { i8 }
%class.btAlignedObjectArray.68 = type <{ %class.btAlignedAllocator.69, [3 x i8], i32, i32, %"struct.btSoftBody::Material"**, i8, [3 x i8] }>
%class.btAlignedAllocator.69 = type { i8 }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.72 }
%class.btAlignedObjectArray.72 = type <{ %class.btAlignedAllocator.73, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.73 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.76 = type <{ %class.btAlignedAllocator.77, [3 x i8], i32, i32, %"struct.btSoftBody::Cluster"**, i8, [3 x i8] }>
%class.btAlignedAllocator.77 = type { i8 }
%class.btAlignedObjectArray.80 = type <{ %class.btAlignedAllocator.81, [3 x i8], i32, i32, i8*, i8, [3 x i8] }>
%class.btAlignedAllocator.81 = type { i8 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.84 = type <{ %class.btAlignedAllocator.85, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.85 = type { i8 }
%class.btVertexBufferDescriptor = type { i32 (...)**, i8, i8, i32, i32, i32, i32 }
%class.btCPUVertexBufferDescriptor = type { %class.btVertexBufferDescriptor, float* }
%struct.btCollisionObjectWrapper = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN16btSoftBodySolverC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE13copyFromArrayERKS2_ = comdat any

$_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi = comdat any

$_ZNK17btCollisionObject8isActiveEv = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK23btDefaultSoftBodySolver13getSolverTypeEv = comdat any

$_ZN16btSoftBodySolver29setNumberOfPositionIterationsEi = comdat any

$_ZN16btSoftBodySolver29getNumberOfPositionIterationsEv = comdat any

$_ZN16btSoftBodySolver29setNumberOfVelocityIterationsEi = comdat any

$_ZN16btSoftBodySolver29getNumberOfVelocityIterationsEv = comdat any

$_ZN16btSoftBodySolverD2Ev = comdat any

$_ZN16btSoftBodySolverD0Ev = comdat any

$_ZNK17btCollisionObject18getActivationStateEv = comdat any

$_ZN18btAlignedAllocatorIP10btSoftBodyLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE6resizeEiRKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP10btSoftBodyE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE8allocateEi = comdat any

$_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE8allocateEiPPKS1_ = comdat any

$_ZTS16btSoftBodySolver = comdat any

$_ZTI16btSoftBodySolver = comdat any

$_ZTV16btSoftBodySolver = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV23btDefaultSoftBodySolver = hidden unnamed_addr constant { [18 x i8*] } { [18 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btDefaultSoftBodySolver to i8*), i8* bitcast (%class.btDefaultSoftBodySolver* (%class.btDefaultSoftBodySolver*)* @_ZN23btDefaultSoftBodySolverD1Ev to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*)* @_ZN23btDefaultSoftBodySolverD0Ev to i8*), i8* bitcast (i32 (%class.btDefaultSoftBodySolver*)* @_ZNK23btDefaultSoftBodySolver13getSolverTypeEv to i8*), i8* bitcast (i1 (%class.btDefaultSoftBodySolver*)* @_ZN23btDefaultSoftBodySolver16checkInitializedEv to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*, %class.btAlignedObjectArray*, i1)* @_ZN23btDefaultSoftBodySolver8optimizeER20btAlignedObjectArrayIP10btSoftBodyEb to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*, i1)* @_ZN23btDefaultSoftBodySolver20copyBackToSoftBodiesEb to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*, float)* @_ZN23btDefaultSoftBodySolver13predictMotionEf to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*, float)* @_ZN23btDefaultSoftBodySolver16solveConstraintsEf to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*)* @_ZN23btDefaultSoftBodySolver16updateSoftBodiesEv to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*, %class.btSoftBody*, %struct.btCollisionObjectWrapper*)* @_ZN23btDefaultSoftBodySolver16processCollisionEP10btSoftBodyPK24btCollisionObjectWrapper to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*, %class.btSoftBody*, %class.btSoftBody*)* @_ZN23btDefaultSoftBodySolver16processCollisionEP10btSoftBodyS1_ to i8*), i8* bitcast (void (%class.btSoftBodySolver*, i32)* @_ZN16btSoftBodySolver29setNumberOfPositionIterationsEi to i8*), i8* bitcast (i32 (%class.btSoftBodySolver*)* @_ZN16btSoftBodySolver29getNumberOfPositionIterationsEv to i8*), i8* bitcast (void (%class.btSoftBodySolver*, i32)* @_ZN16btSoftBodySolver29setNumberOfVelocityIterationsEi to i8*), i8* bitcast (i32 (%class.btSoftBodySolver*)* @_ZN16btSoftBodySolver29getNumberOfVelocityIterationsEv to i8*), i8* bitcast (void (%class.btDefaultSoftBodySolver*, %class.btSoftBody*, %class.btVertexBufferDescriptor*)* @_ZN23btDefaultSoftBodySolver26copySoftBodyToVertexBufferEPK10btSoftBodyP24btVertexBufferDescriptor to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS23btDefaultSoftBodySolver = hidden constant [26 x i8] c"23btDefaultSoftBodySolver\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS16btSoftBodySolver = linkonce_odr hidden constant [19 x i8] c"16btSoftBodySolver\00", comdat, align 1
@_ZTI16btSoftBodySolver = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btSoftBodySolver, i32 0, i32 0) }, comdat, align 4
@_ZTI23btDefaultSoftBodySolver = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btDefaultSoftBodySolver, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI16btSoftBodySolver to i8*) }, align 4
@_ZTV16btSoftBodySolver = linkonce_odr hidden unnamed_addr constant { [17 x i8*] } { [17 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI16btSoftBodySolver to i8*), i8* bitcast (%class.btSoftBodySolver* (%class.btSoftBodySolver*)* @_ZN16btSoftBodySolverD2Ev to i8*), i8* bitcast (void (%class.btSoftBodySolver*)* @_ZN16btSoftBodySolverD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btSoftBodySolver*, i32)* @_ZN16btSoftBodySolver29setNumberOfPositionIterationsEi to i8*), i8* bitcast (i32 (%class.btSoftBodySolver*)* @_ZN16btSoftBodySolver29getNumberOfPositionIterationsEv to i8*), i8* bitcast (void (%class.btSoftBodySolver*, i32)* @_ZN16btSoftBodySolver29setNumberOfVelocityIterationsEi to i8*), i8* bitcast (i32 (%class.btSoftBodySolver*)* @_ZN16btSoftBodySolver29getNumberOfVelocityIterationsEv to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btDefaultSoftBodySolver.cpp, i8* null }]

@_ZN23btDefaultSoftBodySolverC1Ev = hidden unnamed_addr alias %class.btDefaultSoftBodySolver* (%class.btDefaultSoftBodySolver*), %class.btDefaultSoftBodySolver* (%class.btDefaultSoftBodySolver*)* @_ZN23btDefaultSoftBodySolverC2Ev
@_ZN23btDefaultSoftBodySolverD1Ev = hidden unnamed_addr alias %class.btDefaultSoftBodySolver* (%class.btDefaultSoftBodySolver*), %class.btDefaultSoftBodySolver* (%class.btDefaultSoftBodySolver*)* @_ZN23btDefaultSoftBodySolverD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btDefaultSoftBodySolver* @_ZN23btDefaultSoftBodySolverC2Ev(%class.btDefaultSoftBodySolver* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %0 = bitcast %class.btDefaultSoftBodySolver* %this1 to %class.btSoftBodySolver*
  %call = call %class.btSoftBodySolver* @_ZN16btSoftBodySolverC2Ev(%class.btSoftBodySolver* %0)
  %1 = bitcast %class.btDefaultSoftBodySolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV23btDefaultSoftBodySolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_softBodySet = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP10btSoftBodyEC2Ev(%class.btAlignedObjectArray* %m_softBodySet)
  %m_updateSolverConstants = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 1
  store i8 1, i8* %m_updateSolverConstants, align 4
  ret %class.btDefaultSoftBodySolver* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btSoftBodySolver* @_ZN16btSoftBodySolverC2Ev(%class.btSoftBodySolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  %0 = bitcast %class.btSoftBodySolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [17 x i8*] }, { [17 x i8*] }* @_ZTV16btSoftBodySolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_numberOfPositionIterations = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 1
  store i32 10, i32* %m_numberOfPositionIterations, align 4
  %m_timeScale = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 3
  store float 1.000000e+00, float* %m_timeScale, align 4
  %m_numberOfVelocityIterations = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 2
  store i32 0, i32* %m_numberOfVelocityIterations, align 4
  %m_numberOfPositionIterations2 = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 1
  store i32 5, i32* %m_numberOfPositionIterations2, align 4
  ret %class.btSoftBodySolver* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP10btSoftBodyEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btDefaultSoftBodySolver* @_ZN23btDefaultSoftBodySolverD2Ev(%class.btDefaultSoftBodySolver* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %0 = bitcast %class.btDefaultSoftBodySolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV23btDefaultSoftBodySolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_softBodySet = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP10btSoftBodyED2Ev(%class.btAlignedObjectArray* %m_softBodySet) #7
  %1 = bitcast %class.btDefaultSoftBodySolver* %this1 to %class.btSoftBodySolver*
  %call2 = call %class.btSoftBodySolver* @_ZN16btSoftBodySolverD2Ev(%class.btSoftBodySolver* %1) #7
  ret %class.btDefaultSoftBodySolver* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP10btSoftBodyED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN23btDefaultSoftBodySolverD0Ev(%class.btDefaultSoftBodySolver* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %call = call %class.btDefaultSoftBodySolver* @_ZN23btDefaultSoftBodySolverD1Ev(%class.btDefaultSoftBodySolver* %this1) #7
  %0 = bitcast %class.btDefaultSoftBodySolver* %this1 to i8*
  call void @_ZdlPv(i8* %0) #8
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN23btDefaultSoftBodySolver20copyBackToSoftBodiesEb(%class.btDefaultSoftBodySolver* %this, i1 zeroext %bMove) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %bMove.addr = alloca i8, align 1
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %frombool = zext i1 %bMove to i8
  store i8 %frombool, i8* %bMove.addr, align 1
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDefaultSoftBodySolver8optimizeER20btAlignedObjectArrayIP10btSoftBodyEb(%class.btDefaultSoftBodySolver* %this, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %softBodies, i1 zeroext %forceUpdate) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %softBodies.addr = alloca %class.btAlignedObjectArray*, align 4
  %forceUpdate.addr = alloca i8, align 1
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4
  store %class.btAlignedObjectArray* %softBodies, %class.btAlignedObjectArray** %softBodies.addr, align 4
  %frombool = zext i1 %forceUpdate to i8
  store i8 %frombool, i8* %forceUpdate.addr, align 1
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %m_softBodySet = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %0 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %softBodies.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE13copyFromArrayERKS2_(%class.btAlignedObjectArray* %m_softBodySet, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE13copyFromArrayERKS2_(%class.btAlignedObjectArray* %this, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %otherArray) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca %class.btSoftBody*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btAlignedObjectArray* %otherArray, %class.btAlignedObjectArray** %otherArray.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %otherArray.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %0)
  store i32 %call, i32* %otherSize, align 4
  %1 = load i32, i32* %otherSize, align 4
  store %class.btSoftBody* null, %class.btSoftBody** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE6resizeEiRKS1_(%class.btAlignedObjectArray* %this1, i32 %1, %class.btSoftBody** nonnull align 4 dereferenceable(4) %ref.tmp)
  %2 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %otherArray.addr, align 4
  %3 = load i32, i32* %otherSize, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4
  call void @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_(%class.btAlignedObjectArray* %2, i32 0, i32 %3, %class.btSoftBody** %4)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDefaultSoftBodySolver16updateSoftBodiesEv(%class.btDefaultSoftBodySolver* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %i = alloca i32, align 4
  %psb = alloca %class.btSoftBody*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_softBodySet = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %m_softBodySet)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_softBodySet2 = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %1 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btSoftBody** @_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi(%class.btAlignedObjectArray* %m_softBodySet2, i32 %1)
  %2 = load %class.btSoftBody*, %class.btSoftBody** %call3, align 4
  store %class.btSoftBody* %2, %class.btSoftBody** %psb, align 4
  %3 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %4 = bitcast %class.btSoftBody* %3 to %class.btCollisionObject*
  %call4 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %4)
  br i1 %call4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %5 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  call void @_ZN10btSoftBody15integrateMotionEv(%class.btSoftBody* %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btSoftBody** @_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %0, i32 %1
  ret %class.btSoftBody** %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp = icmp ne i32 %call, 2
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %call2 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp3 = icmp ne i32 %call2, 5
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %0 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %0
}

declare void @_ZN10btSoftBody15integrateMotionEv(%class.btSoftBody*) #4

; Function Attrs: noinline nounwind optnone
define hidden zeroext i1 @_ZN23btDefaultSoftBodySolver16checkInitializedEv(%class.btDefaultSoftBodySolver* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDefaultSoftBodySolver16solveConstraintsEf(%class.btDefaultSoftBodySolver* %this, float %solverdt) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %solverdt.addr = alloca float, align 4
  %i = alloca i32, align 4
  %psb = alloca %class.btSoftBody*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4
  store float %solverdt, float* %solverdt.addr, align 4
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_softBodySet = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %m_softBodySet)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_softBodySet2 = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %1 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btSoftBody** @_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi(%class.btAlignedObjectArray* %m_softBodySet2, i32 %1)
  %2 = load %class.btSoftBody*, %class.btSoftBody** %call3, align 4
  store %class.btSoftBody* %2, %class.btSoftBody** %psb, align 4
  %3 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %4 = bitcast %class.btSoftBody* %3 to %class.btCollisionObject*
  %call4 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %4)
  br i1 %call4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %5 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  call void @_ZN10btSoftBody16solveConstraintsEv(%class.btSoftBody* %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

declare void @_ZN10btSoftBody16solveConstraintsEv(%class.btSoftBody*) #4

; Function Attrs: noinline optnone
define hidden void @_ZN23btDefaultSoftBodySolver26copySoftBodyToVertexBufferEPK10btSoftBodyP24btVertexBufferDescriptor(%class.btDefaultSoftBodySolver* %this, %class.btSoftBody* %softBody, %class.btVertexBufferDescriptor* %vertexBuffer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %softBody.addr = alloca %class.btSoftBody*, align 4
  %vertexBuffer.addr = alloca %class.btVertexBufferDescriptor*, align 4
  %clothVertices = alloca %class.btAlignedObjectArray.28*, align 4
  %numVertices = alloca i32, align 4
  %cpuVertexBuffer = alloca %class.btCPUVertexBufferDescriptor*, align 4
  %basePointer = alloca float*, align 4
  %vertexOffset = alloca i32, align 4
  %vertexStride = alloca i32, align 4
  %vertexPointer = alloca float*, align 4
  %vertexIndex = alloca i32, align 4
  %position = alloca %class.btVector3, align 4
  %normalOffset = alloca i32, align 4
  %normalStride = alloca i32, align 4
  %normalPointer = alloca float*, align 4
  %vertexIndex36 = alloca i32, align 4
  %normal = alloca %class.btVector3, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4
  store %class.btSoftBody* %softBody, %class.btSoftBody** %softBody.addr, align 4
  store %class.btVertexBufferDescriptor* %vertexBuffer, %class.btVertexBufferDescriptor** %vertexBuffer.addr, align 4
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %0 = load %class.btVertexBufferDescriptor*, %class.btVertexBufferDescriptor** %vertexBuffer.addr, align 4
  %1 = bitcast %class.btVertexBufferDescriptor* %0 to i32 (%class.btVertexBufferDescriptor*)***
  %vtable = load i32 (%class.btVertexBufferDescriptor*)**, i32 (%class.btVertexBufferDescriptor*)*** %1, align 4
  %vfn = getelementptr inbounds i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vtable, i64 4
  %2 = load i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vfn, align 4
  %call = call i32 %2(%class.btVertexBufferDescriptor* %0)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end52

if.then:                                          ; preds = %entry
  %3 = load %class.btSoftBody*, %class.btSoftBody** %softBody.addr, align 4
  %m_nodes = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %3, i32 0, i32 9
  store %class.btAlignedObjectArray.28* %m_nodes, %class.btAlignedObjectArray.28** %clothVertices, align 4
  %4 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %clothVertices, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.28* %4)
  store i32 %call2, i32* %numVertices, align 4
  %5 = load %class.btVertexBufferDescriptor*, %class.btVertexBufferDescriptor** %vertexBuffer.addr, align 4
  %6 = bitcast %class.btVertexBufferDescriptor* %5 to %class.btCPUVertexBufferDescriptor*
  store %class.btCPUVertexBufferDescriptor* %6, %class.btCPUVertexBufferDescriptor** %cpuVertexBuffer, align 4
  %7 = load %class.btCPUVertexBufferDescriptor*, %class.btCPUVertexBufferDescriptor** %cpuVertexBuffer, align 4
  %8 = bitcast %class.btCPUVertexBufferDescriptor* %7 to float* (%class.btCPUVertexBufferDescriptor*)***
  %vtable3 = load float* (%class.btCPUVertexBufferDescriptor*)**, float* (%class.btCPUVertexBufferDescriptor*)*** %8, align 4
  %vfn4 = getelementptr inbounds float* (%class.btCPUVertexBufferDescriptor*)*, float* (%class.btCPUVertexBufferDescriptor*)** %vtable3, i64 9
  %9 = load float* (%class.btCPUVertexBufferDescriptor*)*, float* (%class.btCPUVertexBufferDescriptor*)** %vfn4, align 4
  %call5 = call float* %9(%class.btCPUVertexBufferDescriptor* %7)
  store float* %call5, float** %basePointer, align 4
  %10 = load %class.btVertexBufferDescriptor*, %class.btVertexBufferDescriptor** %vertexBuffer.addr, align 4
  %11 = bitcast %class.btVertexBufferDescriptor* %10 to i1 (%class.btVertexBufferDescriptor*)***
  %vtable6 = load i1 (%class.btVertexBufferDescriptor*)**, i1 (%class.btVertexBufferDescriptor*)*** %11, align 4
  %vfn7 = getelementptr inbounds i1 (%class.btVertexBufferDescriptor*)*, i1 (%class.btVertexBufferDescriptor*)** %vtable6, i64 2
  %12 = load i1 (%class.btVertexBufferDescriptor*)*, i1 (%class.btVertexBufferDescriptor*)** %vfn7, align 4
  %call8 = call zeroext i1 %12(%class.btVertexBufferDescriptor* %10)
  br i1 %call8, label %if.then9, label %if.end

if.then9:                                         ; preds = %if.then
  %13 = load %class.btCPUVertexBufferDescriptor*, %class.btCPUVertexBufferDescriptor** %cpuVertexBuffer, align 4
  %14 = bitcast %class.btCPUVertexBufferDescriptor* %13 to %class.btVertexBufferDescriptor*
  %15 = bitcast %class.btVertexBufferDescriptor* %14 to i32 (%class.btVertexBufferDescriptor*)***
  %vtable10 = load i32 (%class.btVertexBufferDescriptor*)**, i32 (%class.btVertexBufferDescriptor*)*** %15, align 4
  %vfn11 = getelementptr inbounds i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vtable10, i64 5
  %16 = load i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vfn11, align 4
  %call12 = call i32 %16(%class.btVertexBufferDescriptor* %14)
  store i32 %call12, i32* %vertexOffset, align 4
  %17 = load %class.btCPUVertexBufferDescriptor*, %class.btCPUVertexBufferDescriptor** %cpuVertexBuffer, align 4
  %18 = bitcast %class.btCPUVertexBufferDescriptor* %17 to %class.btVertexBufferDescriptor*
  %19 = bitcast %class.btVertexBufferDescriptor* %18 to i32 (%class.btVertexBufferDescriptor*)***
  %vtable13 = load i32 (%class.btVertexBufferDescriptor*)**, i32 (%class.btVertexBufferDescriptor*)*** %19, align 4
  %vfn14 = getelementptr inbounds i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vtable13, i64 6
  %20 = load i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vfn14, align 4
  %call15 = call i32 %20(%class.btVertexBufferDescriptor* %18)
  store i32 %call15, i32* %vertexStride, align 4
  %21 = load float*, float** %basePointer, align 4
  %22 = load i32, i32* %vertexOffset, align 4
  %add.ptr = getelementptr inbounds float, float* %21, i32 %22
  store float* %add.ptr, float** %vertexPointer, align 4
  store i32 0, i32* %vertexIndex, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then9
  %23 = load i32, i32* %vertexIndex, align 4
  %24 = load i32, i32* %numVertices, align 4
  %cmp16 = icmp slt i32 %23, %24
  br i1 %cmp16, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %25 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %clothVertices, align 4
  %26 = load i32, i32* %vertexIndex, align 4
  %call17 = call nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.28* %25, i32 %26)
  %m_x = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %call17, i32 0, i32 1
  %27 = bitcast %class.btVector3* %position to i8*
  %28 = bitcast %class.btVector3* %m_x to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false)
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %position)
  %29 = load float, float* %call18, align 4
  %30 = load float*, float** %vertexPointer, align 4
  %add.ptr19 = getelementptr inbounds float, float* %30, i32 0
  store float %29, float* %add.ptr19, align 4
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %position)
  %31 = load float, float* %call20, align 4
  %32 = load float*, float** %vertexPointer, align 4
  %add.ptr21 = getelementptr inbounds float, float* %32, i32 1
  store float %31, float* %add.ptr21, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %position)
  %33 = load float, float* %call22, align 4
  %34 = load float*, float** %vertexPointer, align 4
  %add.ptr23 = getelementptr inbounds float, float* %34, i32 2
  store float %33, float* %add.ptr23, align 4
  %35 = load i32, i32* %vertexStride, align 4
  %36 = load float*, float** %vertexPointer, align 4
  %add.ptr24 = getelementptr inbounds float, float* %36, i32 %35
  store float* %add.ptr24, float** %vertexPointer, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %37 = load i32, i32* %vertexIndex, align 4
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %vertexIndex, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %if.then
  %38 = load %class.btVertexBufferDescriptor*, %class.btVertexBufferDescriptor** %vertexBuffer.addr, align 4
  %39 = bitcast %class.btVertexBufferDescriptor* %38 to i1 (%class.btVertexBufferDescriptor*)***
  %vtable25 = load i1 (%class.btVertexBufferDescriptor*)**, i1 (%class.btVertexBufferDescriptor*)*** %39, align 4
  %vfn26 = getelementptr inbounds i1 (%class.btVertexBufferDescriptor*)*, i1 (%class.btVertexBufferDescriptor*)** %vtable25, i64 3
  %40 = load i1 (%class.btVertexBufferDescriptor*)*, i1 (%class.btVertexBufferDescriptor*)** %vfn26, align 4
  %call27 = call zeroext i1 %40(%class.btVertexBufferDescriptor* %38)
  br i1 %call27, label %if.then28, label %if.end51

if.then28:                                        ; preds = %if.end
  %41 = load %class.btCPUVertexBufferDescriptor*, %class.btCPUVertexBufferDescriptor** %cpuVertexBuffer, align 4
  %42 = bitcast %class.btCPUVertexBufferDescriptor* %41 to %class.btVertexBufferDescriptor*
  %43 = bitcast %class.btVertexBufferDescriptor* %42 to i32 (%class.btVertexBufferDescriptor*)***
  %vtable29 = load i32 (%class.btVertexBufferDescriptor*)**, i32 (%class.btVertexBufferDescriptor*)*** %43, align 4
  %vfn30 = getelementptr inbounds i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vtable29, i64 7
  %44 = load i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vfn30, align 4
  %call31 = call i32 %44(%class.btVertexBufferDescriptor* %42)
  store i32 %call31, i32* %normalOffset, align 4
  %45 = load %class.btCPUVertexBufferDescriptor*, %class.btCPUVertexBufferDescriptor** %cpuVertexBuffer, align 4
  %46 = bitcast %class.btCPUVertexBufferDescriptor* %45 to %class.btVertexBufferDescriptor*
  %47 = bitcast %class.btVertexBufferDescriptor* %46 to i32 (%class.btVertexBufferDescriptor*)***
  %vtable32 = load i32 (%class.btVertexBufferDescriptor*)**, i32 (%class.btVertexBufferDescriptor*)*** %47, align 4
  %vfn33 = getelementptr inbounds i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vtable32, i64 8
  %48 = load i32 (%class.btVertexBufferDescriptor*)*, i32 (%class.btVertexBufferDescriptor*)** %vfn33, align 4
  %call34 = call i32 %48(%class.btVertexBufferDescriptor* %46)
  store i32 %call34, i32* %normalStride, align 4
  %49 = load float*, float** %basePointer, align 4
  %50 = load i32, i32* %normalOffset, align 4
  %add.ptr35 = getelementptr inbounds float, float* %49, i32 %50
  store float* %add.ptr35, float** %normalPointer, align 4
  store i32 0, i32* %vertexIndex36, align 4
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc48, %if.then28
  %51 = load i32, i32* %vertexIndex36, align 4
  %52 = load i32, i32* %numVertices, align 4
  %cmp38 = icmp slt i32 %51, %52
  br i1 %cmp38, label %for.body39, label %for.end50

for.body39:                                       ; preds = %for.cond37
  %53 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %clothVertices, align 4
  %54 = load i32, i32* %vertexIndex36, align 4
  %call40 = call nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.28* %53, i32 %54)
  %m_n = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %call40, i32 0, i32 5
  %55 = bitcast %class.btVector3* %normal to i8*
  %56 = bitcast %class.btVector3* %m_n to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %55, i8* align 4 %56, i32 16, i1 false)
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %normal)
  %57 = load float, float* %call41, align 4
  %58 = load float*, float** %normalPointer, align 4
  %add.ptr42 = getelementptr inbounds float, float* %58, i32 0
  store float %57, float* %add.ptr42, align 4
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %normal)
  %59 = load float, float* %call43, align 4
  %60 = load float*, float** %normalPointer, align 4
  %add.ptr44 = getelementptr inbounds float, float* %60, i32 1
  store float %59, float* %add.ptr44, align 4
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %normal)
  %61 = load float, float* %call45, align 4
  %62 = load float*, float** %normalPointer, align 4
  %add.ptr46 = getelementptr inbounds float, float* %62, i32 2
  store float %61, float* %add.ptr46, align 4
  %63 = load i32, i32* %normalStride, align 4
  %64 = load float*, float** %normalPointer, align 4
  %add.ptr47 = getelementptr inbounds float, float* %64, i32 %63
  store float* %add.ptr47, float** %normalPointer, align 4
  br label %for.inc48

for.inc48:                                        ; preds = %for.body39
  %65 = load i32, i32* %vertexIndex36, align 4
  %inc49 = add nsw i32 %65, 1
  store i32 %inc49, i32* %vertexIndex36, align 4
  br label %for.cond37

for.end50:                                        ; preds = %for.cond37
  br label %if.end51

if.end51:                                         ; preds = %for.end50, %if.end
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEE4sizeEv(%class.btAlignedObjectArray.28* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(101) %"struct.btSoftBody::Node"* @_ZNK20btAlignedObjectArrayIN10btSoftBody4NodeEEixEi(%class.btAlignedObjectArray.28* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.28*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.28* %this, %class.btAlignedObjectArray.28** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.28*, %class.btAlignedObjectArray.28** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.28, %class.btAlignedObjectArray.28* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Node"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Node", %"struct.btSoftBody::Node"* %0, i32 %1
  ret %"struct.btSoftBody::Node"* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN23btDefaultSoftBodySolver16processCollisionEP10btSoftBodyS1_(%class.btDefaultSoftBodySolver* %this, %class.btSoftBody* %softBody, %class.btSoftBody* %otherSoftBody) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %softBody.addr = alloca %class.btSoftBody*, align 4
  %otherSoftBody.addr = alloca %class.btSoftBody*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4
  store %class.btSoftBody* %softBody, %class.btSoftBody** %softBody.addr, align 4
  store %class.btSoftBody* %otherSoftBody, %class.btSoftBody** %otherSoftBody.addr, align 4
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %0 = load %class.btSoftBody*, %class.btSoftBody** %softBody.addr, align 4
  %1 = load %class.btSoftBody*, %class.btSoftBody** %otherSoftBody.addr, align 4
  call void @_ZN10btSoftBody23defaultCollisionHandlerEPS_(%class.btSoftBody* %0, %class.btSoftBody* %1)
  ret void
}

declare void @_ZN10btSoftBody23defaultCollisionHandlerEPS_(%class.btSoftBody*, %class.btSoftBody*) #4

; Function Attrs: noinline optnone
define hidden void @_ZN23btDefaultSoftBodySolver16processCollisionEP10btSoftBodyPK24btCollisionObjectWrapper(%class.btDefaultSoftBodySolver* %this, %class.btSoftBody* %softBody, %struct.btCollisionObjectWrapper* %collisionObjectWrap) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %softBody.addr = alloca %class.btSoftBody*, align 4
  %collisionObjectWrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4
  store %class.btSoftBody* %softBody, %class.btSoftBody** %softBody.addr, align 4
  store %struct.btCollisionObjectWrapper* %collisionObjectWrap, %struct.btCollisionObjectWrapper** %collisionObjectWrap.addr, align 4
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %0 = load %class.btSoftBody*, %class.btSoftBody** %softBody.addr, align 4
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %collisionObjectWrap.addr, align 4
  call void @_ZN10btSoftBody23defaultCollisionHandlerEPK24btCollisionObjectWrapper(%class.btSoftBody* %0, %struct.btCollisionObjectWrapper* %1)
  ret void
}

declare void @_ZN10btSoftBody23defaultCollisionHandlerEPK24btCollisionObjectWrapper(%class.btSoftBody*, %struct.btCollisionObjectWrapper*) #4

; Function Attrs: noinline optnone
define hidden void @_ZN23btDefaultSoftBodySolver13predictMotionEf(%class.btDefaultSoftBodySolver* %this, float %timeStep) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  %timeStep.addr = alloca float, align 4
  %i = alloca i32, align 4
  %psb = alloca %class.btSoftBody*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_softBodySet = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %m_softBodySet)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_softBodySet2 = getelementptr inbounds %class.btDefaultSoftBodySolver, %class.btDefaultSoftBodySolver* %this1, i32 0, i32 3
  %1 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btSoftBody** @_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi(%class.btAlignedObjectArray* %m_softBodySet2, i32 %1)
  %2 = load %class.btSoftBody*, %class.btSoftBody** %call3, align 4
  store %class.btSoftBody* %2, %class.btSoftBody** %psb, align 4
  %3 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %4 = bitcast %class.btSoftBody* %3 to %class.btCollisionObject*
  %call4 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %4)
  br i1 %call4, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %5 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %6 = load float, float* %timeStep.addr, align 4
  call void @_ZN10btSoftBody13predictMotionEf(%class.btSoftBody* %5, float %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

declare void @_ZN10btSoftBody13predictMotionEf(%class.btSoftBody*, float) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK23btDefaultSoftBodySolver13getSolverTypeEv(%class.btDefaultSoftBodySolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDefaultSoftBodySolver*, align 4
  store %class.btDefaultSoftBodySolver* %this, %class.btDefaultSoftBodySolver** %this.addr, align 4
  %this1 = load %class.btDefaultSoftBodySolver*, %class.btDefaultSoftBodySolver** %this.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btSoftBodySolver29setNumberOfPositionIterationsEi(%class.btSoftBodySolver* %this, i32 %iterations) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  %iterations.addr = alloca i32, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4
  store i32 %iterations, i32* %iterations.addr, align 4
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  %0 = load i32, i32* %iterations.addr, align 4
  %m_numberOfPositionIterations = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 1
  store i32 %0, i32* %m_numberOfPositionIterations, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN16btSoftBodySolver29getNumberOfPositionIterationsEv(%class.btSoftBodySolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  %m_numberOfPositionIterations = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_numberOfPositionIterations, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btSoftBodySolver29setNumberOfVelocityIterationsEi(%class.btSoftBodySolver* %this, i32 %iterations) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  %iterations.addr = alloca i32, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4
  store i32 %iterations, i32* %iterations.addr, align 4
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  %0 = load i32, i32* %iterations.addr, align 4
  %m_numberOfVelocityIterations = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 2
  store i32 %0, i32* %m_numberOfVelocityIterations, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN16btSoftBodySolver29getNumberOfVelocityIterationsEv(%class.btSoftBodySolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  %m_numberOfVelocityIterations = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_numberOfVelocityIterations, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btSoftBodySolver* @_ZN16btSoftBodySolverD2Ev(%class.btSoftBodySolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  ret %class.btSoftBodySolver* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btSoftBodySolverD0Ev(%class.btSoftBodySolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  call void @llvm.trap() #9
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  %0 = load i32, i32* %m_activationState1, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btSoftBody** null, %class.btSoftBody*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4
  %tobool = icmp ne %class.btSoftBody** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btSoftBody**, %class.btSoftBody*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %class.btSoftBody** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btSoftBody** null, %class.btSoftBody*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %class.btSoftBody** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btSoftBody**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.btSoftBody** %ptr, %class.btSoftBody*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btSoftBody**, %class.btSoftBody*** %ptr.addr, align 4
  %1 = bitcast %class.btSoftBody** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE6resizeEiRKS1_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btSoftBody** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btSoftBody**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btSoftBody** %fillData, %class.btSoftBody*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %class.btSoftBody**, %class.btSoftBody*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %14, i32 %15
  %16 = bitcast %class.btSoftBody** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btSoftBody**
  %18 = load %class.btSoftBody**, %class.btSoftBody*** %fillData.addr, align 4
  %19 = load %class.btSoftBody*, %class.btSoftBody** %18, align 4
  store %class.btSoftBody* %19, %class.btSoftBody** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btSoftBody** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btSoftBody**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btSoftBody** %dest, %class.btSoftBody*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btSoftBody**, %class.btSoftBody*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %3, i32 %4
  %5 = bitcast %class.btSoftBody** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btSoftBody**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %7, i32 %8
  %9 = load %class.btSoftBody*, %class.btSoftBody** %arrayidx2, align 4
  store %class.btSoftBody* %9, %class.btSoftBody** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btSoftBody**, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP10btSoftBodyE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btSoftBody**
  store %class.btSoftBody** %2, %class.btSoftBody*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %class.btSoftBody**, %class.btSoftBody*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btSoftBody** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btSoftBody**, %class.btSoftBody*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btSoftBody** %4, %class.btSoftBody*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP10btSoftBodyE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btSoftBody** @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btSoftBody*** null)
  %2 = bitcast %class.btSoftBody** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btSoftBody** @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %this, i32 %n, %class.btSoftBody*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btSoftBody***, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btSoftBody*** %hint, %class.btSoftBody**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btSoftBody**
  ret %class.btSoftBody** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #4

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btDefaultSoftBodySolver.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { cold noreturn nounwind }
attributes #7 = { nounwind }
attributes #8 = { builtin nounwind }
attributes #9 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
