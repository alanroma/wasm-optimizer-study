; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btInternalEdgeUtility.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btInternalEdgeUtility.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btBvhTriangleMeshShape = type <{ %class.btTriangleMeshShape, %class.btOptimizedBvh*, %struct.btTriangleInfoMap*, i8, i8, [11 x i8], [3 x i8] }>
%class.btTriangleMeshShape = type { %class.btConcaveShape, %class.btVector3, %class.btVector3, %class.btStridingMeshInterface* }
%class.btConcaveShape = type { %class.btCollisionShape, float }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btVector3 = type { [4 x float] }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btOptimizedBvh = type { %class.btQuantizedBvh }
%class.btQuantizedBvh = type { i32 (...)**, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, i8, [3 x i8], %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, i32, %class.btAlignedObjectArray.4, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btOptimizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btOptimizedBvhNode = type { %class.btVector3, %class.btVector3, i32, i32, i32, [20 x i8] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btQuantizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btQuantizedBvhNode = type { [3 x i16], [3 x i16], i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btBvhSubtreeInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btBvhSubtreeInfo = type { [3 x i16], [3 x i16], i32, i32, [3 x i32] }
%struct.btTriangleInfoMap = type { i32 (...)**, %class.btHashMap, float, float, float, float, float, float }
%class.btHashMap = type { %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %struct.btTriangleInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.btTriangleInfo = type { i32, float, float, float }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btHashInt*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.btHashInt = type { i32 }
%struct.btConnectivityProcessor = type { %class.btTriangleCallback, i32, i32, %class.btVector3*, %struct.btTriangleInfoMap* }
%class.btTriangleCallback = type { i32 (...)** }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.20, %union.anon.21, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.20 = type { float }
%union.anon.21 = type { float }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.22, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.22 = type <{ %class.btAlignedAllocator.23, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.23 = type { i8 }
%class.btTriangleShape = type { %class.btPolyhedralConvexShape, [3 x %class.btVector3] }
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btConvexPolyhedron = type opaque
%class.btScaledBvhTriangleMeshShape = type { %class.btConcaveShape, %class.btVector3, %class.btBvhTriangleMeshShape* }
%class.btSerializer = type { i32 (...)** }
%struct.btConvexInternalShapeData = type { %struct.btCollisionShapeData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN22btBvhTriangleMeshShape18getTriangleInfoMapEv = comdat any

$_ZN22btBvhTriangleMeshShape18setTriangleInfoMapEP17btTriangleInfoMap = comdat any

$_ZN19btTriangleMeshShape16getMeshInterfaceEv = comdat any

$_ZNK23btStridingMeshInterface10getScalingEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN9btVector36setMinERKS_ = comdat any

$_ZN9btVector36setMaxERKS_ = comdat any

$_ZN23btConnectivityProcessorC2Ev = comdat any

$_ZN23btConnectivityProcessorD2Ev = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector39fuzzyZeroEv = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZN12btQuaternionC2ERK9btVector3RKf = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERK12btQuaternion = comdat any

$_ZNK24btCollisionObjectWrapper17getCollisionShapeEv = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZNK17btCollisionObject17getCollisionShapeEv = comdat any

$_ZN28btScaledBvhTriangleMeshShape13getChildShapeEv = comdat any

$_ZN9btHashMapI9btHashInt14btTriangleInfoE4findERKS0_ = comdat any

$_ZN9btHashIntC2Ei = comdat any

$_ZNK15btTriangleShape10calcNormalER9btVector3 = comdat any

$_ZNK24btCollisionObjectWrapper17getWorldTransformEv = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_Z10quatRotateRK12btQuaternionRK9btVector3 = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK11btTransform8invXformERK9btVector3 = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_ZNK9btVector31wEv = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

$_ZN18btTriangleCallbackC2Ev = comdat any

$_ZN23btConnectivityProcessorD0Ev = comdat any

$_ZN23btConnectivityProcessor15processTriangleEP9btVector3ii = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN14btTriangleInfoC2Ev = comdat any

$_ZN9btHashMapI9btHashInt14btTriangleInfoE6insertERKS0_RKS1_ = comdat any

$_ZN15btTriangleShapeC2ERK9btVector3S2_S2_ = comdat any

$_ZN15btTriangleShapeD2Ev = comdat any

$_ZNK9btHashInt7getHashEv = comdat any

$_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv = comdat any

$_ZNK9btHashMapI9btHashInt14btTriangleInfoE9findIndexERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI14btTriangleInfoEixEi = comdat any

$_ZNK20btAlignedObjectArrayI14btTriangleInfoE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI14btTriangleInfoE9push_backERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btHashIntE9push_backERKS0_ = comdat any

$_ZN9btHashMapI9btHashInt14btTriangleInfoE10growTablesERKS0_ = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIiEixEi = comdat any

$_ZNK9btHashInt6equalsERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI9btHashIntEixEi = comdat any

$_ZNK9btHashInt7getUid1Ev = comdat any

$_ZN20btAlignedObjectArrayI14btTriangleInfoE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI14btTriangleInfoE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI14btTriangleInfoE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI14btTriangleInfoE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI14btTriangleInfoE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI14btTriangleInfoE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI14btTriangleInfoLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI14btTriangleInfoLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI9btHashIntE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI9btHashIntE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btHashIntE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI9btHashIntE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI9btHashIntE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btHashIntE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btHashIntE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btHashIntE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btHashIntLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI9btHashIntLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZN20btAlignedObjectArrayI9btHashIntEixEi = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN15btTriangleShapeD0Ev = comdat any

$_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_ = comdat any

$_ZNK21btConvexInternalShape15getLocalScalingEv = comdat any

$_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3 = comdat any

$_ZNK15btTriangleShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN21btConvexInternalShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9getMarginEv = comdat any

$_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK21btConvexInternalShape9serializeEPvP12btSerializer = comdat any

$_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3 = comdat any

$_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i = comdat any

$_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZNK15btTriangleShape14getNumVerticesEv = comdat any

$_ZNK15btTriangleShape11getNumEdgesEv = comdat any

$_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_ = comdat any

$_ZNK15btTriangleShape9getVertexEiR9btVector3 = comdat any

$_ZNK15btTriangleShape12getNumPlanesEv = comdat any

$_ZNK15btTriangleShape8getPlaneER9btVector3S1_i = comdat any

$_ZNK15btTriangleShape8isInsideERK9btVector3f = comdat any

$_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_ = comdat any

$_ZN15btTriangleShapedlEPv = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z7btAtan2ff = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_ZN12btQuaternion11setRotationERK9btVector3RKf = comdat any

$_Z5btSinf = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_Z5btCosf = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_Z6btSqrtf = comdat any

$_ZmlRK12btQuaternionRK9btVector3 = comdat any

$_ZNK12btQuaternion7inverseEv = comdat any

$_ZN12btQuaternionmLERKS_ = comdat any

$_ZNK10btQuadWord4getXEv = comdat any

$_ZNK10btQuadWord4getYEv = comdat any

$_ZNK10btQuadWord4getZEv = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZTV23btConnectivityProcessor = comdat any

$_ZTS23btConnectivityProcessor = comdat any

$_ZTI23btConnectivityProcessor = comdat any

$_ZTV15btTriangleShape = comdat any

$_ZTS15btTriangleShape = comdat any

$_ZTI15btTriangleShape = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV23btConnectivityProcessor = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btConnectivityProcessor to i8*), i8* bitcast (%struct.btConnectivityProcessor* (%struct.btConnectivityProcessor*)* @_ZN23btConnectivityProcessorD2Ev to i8*), i8* bitcast (void (%struct.btConnectivityProcessor*)* @_ZN23btConnectivityProcessorD0Ev to i8*), i8* bitcast (void (%struct.btConnectivityProcessor*, %class.btVector3*, i32, i32)* @_ZN23btConnectivityProcessor15processTriangleEP9btVector3ii to i8*)] }, comdat, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS23btConnectivityProcessor = linkonce_odr hidden constant [26 x i8] c"23btConnectivityProcessor\00", comdat, align 1
@_ZTI18btTriangleCallback = external constant i8*
@_ZTI23btConnectivityProcessor = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btConnectivityProcessor, i32 0, i32 0), i8* bitcast (i8** @_ZTI18btTriangleCallback to i8*) }, comdat, align 4
@_ZTV18btTriangleCallback = external unnamed_addr constant { [5 x i8*] }, align 4
@__const._ZN23btConnectivityProcessor15processTriangleEP9btVector3ii.sharedVertsA = private unnamed_addr constant [3 x i32] [i32 -1, i32 -1, i32 -1], align 4
@__const._ZN23btConnectivityProcessor15processTriangleEP9btVector3ii.sharedVertsB = private unnamed_addr constant [3 x i32] [i32 -1, i32 -1, i32 -1], align 4
@_ZTV15btTriangleShape = linkonce_odr hidden unnamed_addr constant { [34 x i8*] } { [34 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btTriangleShape to i8*), i8* bitcast (%class.btTriangleShape* (%class.btTriangleShape*)* @_ZN15btTriangleShapeD2Ev to i8*), i8* bitcast (void (%class.btTriangleShape*)* @_ZN15btTriangleShapeD0Ev to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btVector3*)* @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btTriangleShape*, float, %class.btVector3*)* @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btTriangleShape*)* @_ZNK15btTriangleShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexInternalShape*, i8*, %class.btSerializer*)* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btTriangleShape*, %class.btVector3*)* @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*, %class.btVector3*, %class.btVector3*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_ to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*)* @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape14getNumVerticesEv to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape11getNumEdgesEv to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_ to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*)* @_ZNK15btTriangleShape9getVertexEiR9btVector3 to i8*), i8* bitcast (i32 (%class.btTriangleShape*)* @_ZNK15btTriangleShape12getNumPlanesEv to i8*), i8* bitcast (void (%class.btTriangleShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i to i8*), i8* bitcast (i1 (%class.btTriangleShape*, %class.btVector3*, float)* @_ZNK15btTriangleShape8isInsideERK9btVector3f to i8*), i8* bitcast (void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)* @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_ to i8*)] }, comdat, align 4
@_ZTS15btTriangleShape = linkonce_odr hidden constant [18 x i8] c"15btTriangleShape\00", comdat, align 1
@_ZTI23btPolyhedralConvexShape = external constant i8*
@_ZTI15btTriangleShape = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btTriangleShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI23btPolyhedralConvexShape to i8*) }, comdat, align 4
@.str = private unnamed_addr constant [9 x i8] c"Triangle\00", align 1
@.str.1 = private unnamed_addr constant [26 x i8] c"btConvexInternalShapeData\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btInternalEdgeUtility.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden void @_Z26btGenerateInternalEdgeInfoP22btBvhTriangleMeshShapeP17btTriangleInfoMap(%class.btBvhTriangleMeshShape* %trimeshShape, %struct.btTriangleInfoMap* %triangleInfoMap) #2 {
entry:
  %trimeshShape.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %triangleInfoMap.addr = alloca %struct.btTriangleInfoMap*, align 4
  %meshInterface = alloca %class.btStridingMeshInterface*, align 4
  %meshScaling = alloca %class.btVector3*, align 4
  %partId = alloca i32, align 4
  %vertexbase = alloca i8*, align 4
  %numverts = alloca i32, align 4
  %type = alloca i32, align 4
  %stride = alloca i32, align 4
  %indexbase = alloca i8*, align 4
  %indexstride = alloca i32, align 4
  %numfaces = alloca i32, align 4
  %indicestype = alloca i32, align 4
  %triangleVerts = alloca [3 x %class.btVector3], align 16
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %triangleIndex = alloca i32, align 4
  %gfxbase = alloca i32*, align 4
  %j = alloca i32, align 4
  %graphicsindex = alloca i32, align 4
  %graphicsbase = alloca float*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %graphicsbase35 = alloca double*, align 4
  %ref.tmp38 = alloca %class.btVector3, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp51 = alloca float, align 4
  %ref.tmp60 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %ref.tmp62 = alloca float, align 4
  %ref.tmp63 = alloca float, align 4
  %ref.tmp64 = alloca float, align 4
  %ref.tmp65 = alloca float, align 4
  %connectivityProcessor = alloca %struct.btConnectivityProcessor, align 4
  store %class.btBvhTriangleMeshShape* %trimeshShape, %class.btBvhTriangleMeshShape** %trimeshShape.addr, align 4
  store %struct.btTriangleInfoMap* %triangleInfoMap, %struct.btTriangleInfoMap** %triangleInfoMap.addr, align 4
  %0 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %trimeshShape.addr, align 4
  %call = call %struct.btTriangleInfoMap* @_ZN22btBvhTriangleMeshShape18getTriangleInfoMapEv(%class.btBvhTriangleMeshShape* %0)
  %tobool = icmp ne %struct.btTriangleInfoMap* %call, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %for.end81

if.end:                                           ; preds = %entry
  %1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %trimeshShape.addr, align 4
  %2 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMap.addr, align 4
  call void @_ZN22btBvhTriangleMeshShape18setTriangleInfoMapEP17btTriangleInfoMap(%class.btBvhTriangleMeshShape* %1, %struct.btTriangleInfoMap* %2)
  %3 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %trimeshShape.addr, align 4
  %4 = bitcast %class.btBvhTriangleMeshShape* %3 to %class.btTriangleMeshShape*
  %call1 = call %class.btStridingMeshInterface* @_ZN19btTriangleMeshShape16getMeshInterfaceEv(%class.btTriangleMeshShape* %4)
  store %class.btStridingMeshInterface* %call1, %class.btStridingMeshInterface** %meshInterface, align 4
  %5 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %5)
  store %class.btVector3* %call2, %class.btVector3** %meshScaling, align 4
  store i32 0, i32* %partId, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc79, %if.end
  %6 = load i32, i32* %partId, align 4
  %7 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface, align 4
  %8 = bitcast %class.btStridingMeshInterface* %7 to i32 (%class.btStridingMeshInterface*)***
  %vtable = load i32 (%class.btStridingMeshInterface*)**, i32 (%class.btStridingMeshInterface*)*** %8, align 4
  %vfn = getelementptr inbounds i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vtable, i64 7
  %9 = load i32 (%class.btStridingMeshInterface*)*, i32 (%class.btStridingMeshInterface*)** %vfn, align 4
  %call3 = call i32 %9(%class.btStridingMeshInterface* %7)
  %cmp = icmp slt i32 %6, %call3
  br i1 %cmp, label %for.body, label %for.end81

for.body:                                         ; preds = %for.cond
  store i8* null, i8** %vertexbase, align 4
  store i32 0, i32* %numverts, align 4
  store i32 2, i32* %type, align 4
  store i32 0, i32* %stride, align 4
  store i8* null, i8** %indexbase, align 4
  store i32 0, i32* %indexstride, align 4
  store i32 0, i32* %numfaces, align 4
  store i32 2, i32* %indicestype, align 4
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %for.body
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %for.body ], [ %arrayctor.next, %arrayctor.loop ]
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %10 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface, align 4
  %11 = load i32, i32* %partId, align 4
  %12 = bitcast %class.btStridingMeshInterface* %10 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable5 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %12, align 4
  %vfn6 = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable5, i64 4
  %13 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn6, align 4
  call void %13(%class.btStridingMeshInterface* %10, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numfaces, i32* nonnull align 4 dereferenceable(4) %indicestype, i32 %11)
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  store i32 0, i32* %triangleIndex, align 4
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc77, %arrayctor.cont
  %14 = load i32, i32* %triangleIndex, align 4
  %15 = load i32, i32* %numfaces, align 4
  %cmp10 = icmp slt i32 %14, %15
  br i1 %cmp10, label %for.body11, label %for.end78

for.body11:                                       ; preds = %for.cond9
  %16 = load i8*, i8** %indexbase, align 4
  %17 = load i32, i32* %triangleIndex, align 4
  %18 = load i32, i32* %indexstride, align 4
  %mul = mul nsw i32 %17, %18
  %add.ptr = getelementptr inbounds i8, i8* %16, i32 %mul
  %19 = bitcast i8* %add.ptr to i32*
  store i32* %19, i32** %gfxbase, align 4
  store i32 2, i32* %j, align 4
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc, %for.body11
  %20 = load i32, i32* %j, align 4
  %cmp13 = icmp sge i32 %20, 0
  br i1 %cmp13, label %for.body14, label %for.end

for.body14:                                       ; preds = %for.cond12
  %21 = load i32, i32* %indicestype, align 4
  %cmp15 = icmp eq i32 %21, 3
  br i1 %cmp15, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body14
  %22 = load i32*, i32** %gfxbase, align 4
  %23 = bitcast i32* %22 to i16*
  %24 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds i16, i16* %23, i32 %24
  %25 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %25 to i32
  br label %cond.end

cond.false:                                       ; preds = %for.body14
  %26 = load i32*, i32** %gfxbase, align 4
  %27 = load i32, i32* %j, align 4
  %arrayidx16 = getelementptr inbounds i32, i32* %26, i32 %27
  %28 = load i32, i32* %arrayidx16, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %conv, %cond.true ], [ %28, %cond.false ]
  store i32 %cond, i32* %graphicsindex, align 4
  %29 = load i32, i32* %type, align 4
  %cmp17 = icmp eq i32 %29, 0
  br i1 %cmp17, label %if.then18, label %if.else

if.then18:                                        ; preds = %cond.end
  %30 = load i8*, i8** %vertexbase, align 4
  %31 = load i32, i32* %graphicsindex, align 4
  %32 = load i32, i32* %stride, align 4
  %mul19 = mul nsw i32 %31, %32
  %add.ptr20 = getelementptr inbounds i8, i8* %30, i32 %mul19
  %33 = bitcast i8* %add.ptr20 to float*
  store float* %33, float** %graphicsbase, align 4
  %34 = load float*, float** %graphicsbase, align 4
  %arrayidx22 = getelementptr inbounds float, float* %34, i32 0
  %35 = load float, float* %arrayidx22, align 4
  %36 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %36)
  %37 = load float, float* %call23, align 4
  %mul24 = fmul float %35, %37
  store float %mul24, float* %ref.tmp21, align 4
  %38 = load float*, float** %graphicsbase, align 4
  %arrayidx26 = getelementptr inbounds float, float* %38, i32 1
  %39 = load float, float* %arrayidx26, align 4
  %40 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %40)
  %41 = load float, float* %call27, align 4
  %mul28 = fmul float %39, %41
  store float %mul28, float* %ref.tmp25, align 4
  %42 = load float*, float** %graphicsbase, align 4
  %arrayidx30 = getelementptr inbounds float, float* %42, i32 2
  %43 = load float, float* %arrayidx30, align 4
  %44 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %44)
  %45 = load float, float* %call31, align 4
  %mul32 = fmul float %43, %45
  store float %mul32, float* %ref.tmp29, align 4
  %call33 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp29)
  %46 = load i32, i32* %j, align 4
  %arrayidx34 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 %46
  %47 = bitcast %class.btVector3* %arrayidx34 to i8*
  %48 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %47, i8* align 4 %48, i32 16, i1 false)
  br label %if.end59

if.else:                                          ; preds = %cond.end
  %49 = load i8*, i8** %vertexbase, align 4
  %50 = load i32, i32* %graphicsindex, align 4
  %51 = load i32, i32* %stride, align 4
  %mul36 = mul nsw i32 %50, %51
  %add.ptr37 = getelementptr inbounds i8, i8* %49, i32 %mul36
  %52 = bitcast i8* %add.ptr37 to double*
  store double* %52, double** %graphicsbase35, align 4
  %53 = load double*, double** %graphicsbase35, align 4
  %arrayidx40 = getelementptr inbounds double, double* %53, i32 0
  %54 = load double, double* %arrayidx40, align 8
  %55 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %55)
  %56 = load float, float* %call41, align 4
  %conv42 = fpext float %56 to double
  %mul43 = fmul double %54, %conv42
  %conv44 = fptrunc double %mul43 to float
  store float %conv44, float* %ref.tmp39, align 4
  %57 = load double*, double** %graphicsbase35, align 4
  %arrayidx46 = getelementptr inbounds double, double* %57, i32 1
  %58 = load double, double* %arrayidx46, align 8
  %59 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %59)
  %60 = load float, float* %call47, align 4
  %conv48 = fpext float %60 to double
  %mul49 = fmul double %58, %conv48
  %conv50 = fptrunc double %mul49 to float
  store float %conv50, float* %ref.tmp45, align 4
  %61 = load double*, double** %graphicsbase35, align 4
  %arrayidx52 = getelementptr inbounds double, double* %61, i32 2
  %62 = load double, double* %arrayidx52, align 8
  %63 = load %class.btVector3*, %class.btVector3** %meshScaling, align 4
  %call53 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %63)
  %64 = load float, float* %call53, align 4
  %conv54 = fpext float %64 to double
  %mul55 = fmul double %62, %conv54
  %conv56 = fptrunc double %mul55 to float
  store float %conv56, float* %ref.tmp51, align 4
  %call57 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp45, float* nonnull align 4 dereferenceable(4) %ref.tmp51)
  %65 = load i32, i32* %j, align 4
  %arrayidx58 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 %65
  %66 = bitcast %class.btVector3* %arrayidx58 to i8*
  %67 = bitcast %class.btVector3* %ref.tmp38 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %66, i8* align 4 %67, i32 16, i1 false)
  br label %if.end59

if.end59:                                         ; preds = %if.else, %if.then18
  br label %for.inc

for.inc:                                          ; preds = %if.end59
  %68 = load i32, i32* %j, align 4
  %dec = add nsw i32 %68, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond12

for.end:                                          ; preds = %for.cond12
  store float 0x43ABC16D60000000, float* %ref.tmp60, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp61, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp62, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp60, float* nonnull align 4 dereferenceable(4) %ref.tmp61, float* nonnull align 4 dereferenceable(4) %ref.tmp62)
  store float 0xC3ABC16D60000000, float* %ref.tmp63, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp64, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp65, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %aabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp63, float* nonnull align 4 dereferenceable(4) %ref.tmp64, float* nonnull align 4 dereferenceable(4) %ref.tmp65)
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 0
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx66)
  %arrayidx67 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 0
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx67)
  %arrayidx68 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 1
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx68)
  %arrayidx69 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 1
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx69)
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 2
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx70)
  %arrayidx71 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 2
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx71)
  %call72 = call %struct.btConnectivityProcessor* @_ZN23btConnectivityProcessorC2Ev(%struct.btConnectivityProcessor* %connectivityProcessor) #9
  %69 = load i32, i32* %partId, align 4
  %m_partIdA = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %connectivityProcessor, i32 0, i32 1
  store i32 %69, i32* %m_partIdA, align 4
  %70 = load i32, i32* %triangleIndex, align 4
  %m_triangleIndexA = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %connectivityProcessor, i32 0, i32 2
  store i32 %70, i32* %m_triangleIndexA, align 4
  %arrayidx73 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %triangleVerts, i32 0, i32 0
  %m_triangleVerticesA = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %connectivityProcessor, i32 0, i32 3
  store %class.btVector3* %arrayidx73, %class.btVector3** %m_triangleVerticesA, align 4
  %71 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMap.addr, align 4
  %m_triangleInfoMap = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %connectivityProcessor, i32 0, i32 4
  store %struct.btTriangleInfoMap* %71, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4
  %72 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %trimeshShape.addr, align 4
  %73 = bitcast %struct.btConnectivityProcessor* %connectivityProcessor to %class.btTriangleCallback*
  %74 = bitcast %class.btBvhTriangleMeshShape* %72 to void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable74 = load void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*** %74, align 4
  %vfn75 = getelementptr inbounds void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vtable74, i64 16
  %75 = load void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btBvhTriangleMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vfn75, align 4
  call void %75(%class.btBvhTriangleMeshShape* %72, %class.btTriangleCallback* %73, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  %call76 = call %struct.btConnectivityProcessor* @_ZN23btConnectivityProcessorD2Ev(%struct.btConnectivityProcessor* %connectivityProcessor) #9
  br label %for.inc77

for.inc77:                                        ; preds = %for.end
  %76 = load i32, i32* %triangleIndex, align 4
  %inc = add nsw i32 %76, 1
  store i32 %inc, i32* %triangleIndex, align 4
  br label %for.cond9

for.end78:                                        ; preds = %for.cond9
  br label %for.inc79

for.inc79:                                        ; preds = %for.end78
  %77 = load i32, i32* %partId, align 4
  %inc80 = add nsw i32 %77, 1
  store i32 %inc80, i32* %partId, align 4
  br label %for.cond

for.end81:                                        ; preds = %if.then, %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btTriangleInfoMap* @_ZN22btBvhTriangleMeshShape18getTriangleInfoMapEv(%class.btBvhTriangleMeshShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %m_triangleInfoMap = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  %0 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4
  ret %struct.btTriangleInfoMap* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN22btBvhTriangleMeshShape18setTriangleInfoMapEP17btTriangleInfoMap(%class.btBvhTriangleMeshShape* %this, %struct.btTriangleInfoMap* %triangleInfoMap) #1 comdat {
entry:
  %this.addr = alloca %class.btBvhTriangleMeshShape*, align 4
  %triangleInfoMap.addr = alloca %struct.btTriangleInfoMap*, align 4
  store %class.btBvhTriangleMeshShape* %this, %class.btBvhTriangleMeshShape** %this.addr, align 4
  store %struct.btTriangleInfoMap* %triangleInfoMap, %struct.btTriangleInfoMap** %triangleInfoMap.addr, align 4
  %this1 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %this.addr, align 4
  %0 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMap.addr, align 4
  %m_triangleInfoMap = getelementptr inbounds %class.btBvhTriangleMeshShape, %class.btBvhTriangleMeshShape* %this1, i32 0, i32 2
  store %struct.btTriangleInfoMap* %0, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btStridingMeshInterface* @_ZN19btTriangleMeshShape16getMeshInterfaceEv(%class.btTriangleMeshShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleMeshShape*, align 4
  store %class.btTriangleMeshShape* %this, %class.btTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btTriangleMeshShape*, %class.btTriangleMeshShape** %this.addr, align 4
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %this1, i32 0, i32 3
  %0 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  ret %class.btStridingMeshInterface* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btStridingMeshInterface10getScalingEv(%class.btStridingMeshInterface* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %m_scaling = getelementptr inbounds %class.btStridingMeshInterface, %class.btStridingMeshInterface* %this1, i32 0, i32 1
  ret %class.btVector3* %m_scaling
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector36setMinERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector36setMaxERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btConnectivityProcessor* @_ZN23btConnectivityProcessorC2Ev(%struct.btConnectivityProcessor* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btConnectivityProcessor*, align 4
  store %struct.btConnectivityProcessor* %this, %struct.btConnectivityProcessor** %this.addr, align 4
  %this1 = load %struct.btConnectivityProcessor*, %struct.btConnectivityProcessor** %this.addr, align 4
  %0 = bitcast %struct.btConnectivityProcessor* %this1 to %class.btTriangleCallback*
  %call = call %class.btTriangleCallback* @_ZN18btTriangleCallbackC2Ev(%class.btTriangleCallback* %0) #9
  %1 = bitcast %struct.btConnectivityProcessor* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV23btConnectivityProcessor, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %struct.btConnectivityProcessor* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btConnectivityProcessor* @_ZN23btConnectivityProcessorD2Ev(%struct.btConnectivityProcessor* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btConnectivityProcessor*, align 4
  store %struct.btConnectivityProcessor* %this, %struct.btConnectivityProcessor** %this.addr, align 4
  %this1 = load %struct.btConnectivityProcessor*, %struct.btConnectivityProcessor** %this.addr, align 4
  %0 = bitcast %struct.btConnectivityProcessor* %this1 to %class.btTriangleCallback*
  %call = call %class.btTriangleCallback* @_ZN18btTriangleCallbackD2Ev(%class.btTriangleCallback* %0) #9
  ret %struct.btConnectivityProcessor* %this1
}

; Function Attrs: noinline optnone
define hidden void @_Z27btNearestPointInLineSegmentRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %point, %class.btVector3* nonnull align 4 dereferenceable(16) %line0, %class.btVector3* nonnull align 4 dereferenceable(16) %line1, %class.btVector3* nonnull align 4 dereferenceable(16) %nearestPoint) #2 {
entry:
  %point.addr = alloca %class.btVector3*, align 4
  %line0.addr = alloca %class.btVector3*, align 4
  %line1.addr = alloca %class.btVector3*, align 4
  %nearestPoint.addr = alloca %class.btVector3*, align 4
  %lineDelta = alloca %class.btVector3, align 4
  %delta = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4
  store %class.btVector3* %line0, %class.btVector3** %line0.addr, align 4
  store %class.btVector3* %line1, %class.btVector3** %line1.addr, align 4
  store %class.btVector3* %nearestPoint, %class.btVector3** %nearestPoint.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %line1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %line0.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %lineDelta, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %call = call zeroext i1 @_ZNK9btVector39fuzzyZeroEv(%class.btVector3* %lineDelta)
  br i1 %call, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %line0.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %nearestPoint.addr, align 4
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  br label %if.end10

if.else:                                          ; preds = %entry
  %6 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %7 = load %class.btVector3*, %class.btVector3** %line0.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %lineDelta)
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %lineDelta, %class.btVector3* nonnull align 4 dereferenceable(16) %lineDelta)
  %div = fdiv float %call1, %call2
  store float %div, float* %delta, align 4
  %8 = load float, float* %delta, align 4
  %cmp = fcmp olt float %8, 0.000000e+00
  br i1 %cmp, label %if.then3, label %if.else4

if.then3:                                         ; preds = %if.else
  store float 0.000000e+00, float* %delta, align 4
  br label %if.end7

if.else4:                                         ; preds = %if.else
  %9 = load float, float* %delta, align 4
  %cmp5 = fcmp ogt float %9, 1.000000e+00
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.else4
  store float 1.000000e+00, float* %delta, align 4
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.else4
  br label %if.end7

if.end7:                                          ; preds = %if.end, %if.then3
  %10 = load %class.btVector3*, %class.btVector3** %line0.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %lineDelta, float* nonnull align 4 dereferenceable(4) %delta)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %11 = load %class.btVector3*, %class.btVector3** %nearestPoint.addr, align 4
  %12 = bitcast %class.btVector3* %11 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  br label %if.end10

if.end10:                                         ; preds = %if.end7, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK9btVector39fuzzyZeroEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %cmp = fcmp olt float %call, 0x3D10000000000000
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_Z13btClampNormalRK9btVector3S1_S1_fRS_(%class.btVector3* nonnull align 4 dereferenceable(16) %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal_org, %class.btVector3* nonnull align 4 dereferenceable(16) %localContactNormalOnB, float %correctedEdgeAngle, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedLocalNormal) #2 {
entry:
  %retval = alloca i1, align 1
  %edge.addr = alloca %class.btVector3*, align 4
  %tri_normal_org.addr = alloca %class.btVector3*, align 4
  %localContactNormalOnB.addr = alloca %class.btVector3*, align 4
  %correctedEdgeAngle.addr = alloca float, align 4
  %clampedLocalNormal.addr = alloca %class.btVector3*, align 4
  %tri_normal = alloca %class.btVector3, align 4
  %edgeCross = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %curAngle = alloca float, align 4
  %diffAngle = alloca float, align 4
  %rotation = alloca %class.btQuaternion, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btMatrix3x3, align 4
  %diffAngle13 = alloca float, align 4
  %rotation15 = alloca %class.btQuaternion, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %ref.tmp18 = alloca %class.btMatrix3x3, align 4
  store %class.btVector3* %edge, %class.btVector3** %edge.addr, align 4
  store %class.btVector3* %tri_normal_org, %class.btVector3** %tri_normal_org.addr, align 4
  store %class.btVector3* %localContactNormalOnB, %class.btVector3** %localContactNormalOnB.addr, align 4
  store float %correctedEdgeAngle, float* %correctedEdgeAngle.addr, align 4
  store %class.btVector3* %clampedLocalNormal, %class.btVector3** %clampedLocalNormal.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %tri_normal_org.addr, align 4
  %1 = bitcast %class.btVector3* %tri_normal to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %edge.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %ref.tmp)
  %4 = bitcast %class.btVector3* %edgeCross to i8*
  %5 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btVector3*, %class.btVector3** %localContactNormalOnB.addr, align 4
  %call1 = call float @_ZL10btGetAngleRK9btVector3S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %edgeCross, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  store float %call1, float* %curAngle, align 4
  %7 = load float, float* %correctedEdgeAngle.addr, align 4
  %cmp = fcmp olt float %7, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end8

if.then:                                          ; preds = %entry
  %8 = load float, float* %curAngle, align 4
  %9 = load float, float* %correctedEdgeAngle.addr, align 4
  %cmp2 = fcmp olt float %8, %9
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %10 = load float, float* %correctedEdgeAngle.addr, align 4
  %11 = load float, float* %curAngle, align 4
  %sub = fsub float %10, %11
  store float %sub, float* %diffAngle, align 4
  %12 = load %class.btVector3*, %class.btVector3** %edge.addr, align 4
  %call4 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %rotation, %class.btVector3* nonnull align 4 dereferenceable(16) %12, float* nonnull align 4 dereferenceable(4) %diffAngle)
  %call7 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %ref.tmp6, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation)
  %13 = load %class.btVector3*, %class.btVector3** %localContactNormalOnB.addr, align 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp5, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %13)
  %14 = load %class.btVector3*, %class.btVector3** %clampedLocalNormal.addr, align 4
  %15 = bitcast %class.btVector3* %14 to i8*
  %16 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %if.then
  br label %if.end8

if.end8:                                          ; preds = %if.end, %entry
  %17 = load float, float* %correctedEdgeAngle.addr, align 4
  %cmp9 = fcmp oge float %17, 0.000000e+00
  br i1 %cmp9, label %if.then10, label %if.end21

if.then10:                                        ; preds = %if.end8
  %18 = load float, float* %curAngle, align 4
  %19 = load float, float* %correctedEdgeAngle.addr, align 4
  %cmp11 = fcmp ogt float %18, %19
  br i1 %cmp11, label %if.then12, label %if.end20

if.then12:                                        ; preds = %if.then10
  %20 = load float, float* %correctedEdgeAngle.addr, align 4
  %21 = load float, float* %curAngle, align 4
  %sub14 = fsub float %20, %21
  store float %sub14, float* %diffAngle13, align 4
  %22 = load %class.btVector3*, %class.btVector3** %edge.addr, align 4
  %call16 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %rotation15, %class.btVector3* nonnull align 4 dereferenceable(16) %22, float* nonnull align 4 dereferenceable(4) %diffAngle13)
  %call19 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %ref.tmp18, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation15)
  %23 = load %class.btVector3*, %class.btVector3** %localContactNormalOnB.addr, align 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp17, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp18, %class.btVector3* nonnull align 4 dereferenceable(16) %23)
  %24 = load %class.btVector3*, %class.btVector3** %clampedLocalNormal.addr, align 4
  %25 = bitcast %class.btVector3* %24 to i8*
  %26 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false)
  store i1 true, i1* %retval, align 1
  br label %return

if.end20:                                         ; preds = %if.then10
  br label %if.end21

if.end21:                                         ; preds = %if.end20, %if.end8
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end21, %if.then12, %if.then3
  %27 = load i1, i1* %retval, align 1
  ret i1 %27
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define internal float @_ZL10btGetAngleRK9btVector3S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %edgeA, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA, %class.btVector3* nonnull align 4 dereferenceable(16) %normalB) #2 {
entry:
  %edgeA.addr = alloca %class.btVector3*, align 4
  %normalA.addr = alloca %class.btVector3*, align 4
  %normalB.addr = alloca %class.btVector3*, align 4
  %refAxis0 = alloca %class.btVector3, align 4
  %refAxis1 = alloca %class.btVector3, align 4
  %swingAxis = alloca %class.btVector3, align 4
  %angle = alloca float, align 4
  store %class.btVector3* %edgeA, %class.btVector3** %edgeA.addr, align 4
  store %class.btVector3* %normalA, %class.btVector3** %normalA.addr, align 4
  store %class.btVector3* %normalB, %class.btVector3** %normalB.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %edgeA.addr, align 4
  %1 = bitcast %class.btVector3* %refAxis0 to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %normalA.addr, align 4
  %4 = bitcast %class.btVector3* %refAxis1 to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btVector3*, %class.btVector3** %normalB.addr, align 4
  %7 = bitcast %class.btVector3* %swingAxis to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %swingAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %refAxis0)
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %swingAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %refAxis1)
  %call2 = call float @_Z7btAtan2ff(float %call, float %call1)
  store float %call2, float* %angle, align 4
  %9 = load float, float* %angle, align 4
  ret float %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_axis, float* nonnull align 4 dereferenceable(4) %_angle) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btVector3* %_axis, %class.btVector3** %_axis.addr, align 4
  store float* %_angle, float** %_angle.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  %1 = load %class.btVector3*, %class.btVector3** %_axis.addr, align 4
  %2 = load float*, float** %_angle.addr, align 4
  call void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1, float* nonnull align 4 dereferenceable(4) %2)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %1
}

; Function Attrs: noinline optnone
define hidden void @_Z28btAdjustInternalEdgeContactsR15btManifoldPointPK24btCollisionObjectWrapperS3_iii(%class.btManifoldPoint* nonnull align 4 dereferenceable(192) %cp, %struct.btCollisionObjectWrapper* %colObj0Wrap, %struct.btCollisionObjectWrapper* %colObj1Wrap, i32 %partId0, i32 %index0, i32 %normalAdjustFlags) #2 {
entry:
  %cp.addr = alloca %class.btManifoldPoint*, align 4
  %colObj0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %colObj1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %partId0.addr = alloca i32, align 4
  %index0.addr = alloca i32, align 4
  %normalAdjustFlags.addr = alloca i32, align 4
  %trimesh = alloca %class.btBvhTriangleMeshShape*, align 4
  %triangleInfoMapPtr = alloca %struct.btTriangleInfoMap*, align 4
  %hash = alloca i32, align 4
  %info = alloca %struct.btTriangleInfo*, align 4
  %ref.tmp = alloca %class.btHashInt, align 4
  %frontFacing = alloca float, align 4
  %tri_shape = alloca %class.btTriangleShape*, align 4
  %v0 = alloca %class.btVector3, align 4
  %v1 = alloca %class.btVector3, align 4
  %v2 = alloca %class.btVector3, align 4
  %red = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %green = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %blue = alloca %class.btVector3, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %white = alloca %class.btVector3, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %black = alloca %class.btVector3, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp48 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %tri_normal = alloca %class.btVector3, align 4
  %nearest = alloca %class.btVector3, align 4
  %contact = alloca %class.btVector3, align 4
  %isNearEdge = alloca i8, align 1
  %numConcaveEdgeHits = alloca i32, align 4
  %numConvexEdgeHits = alloca i32, align 4
  %localContactNormalOnB = alloca %class.btVector3, align 4
  %ref.tmp54 = alloca %class.btMatrix3x3, align 4
  %bestedge = alloca i32, align 4
  %disttobestedge = alloca float, align 4
  %nearest61 = alloca %class.btVector3, align 4
  %len = alloca float, align 4
  %ref.tmp64 = alloca %class.btVector3, align 4
  %nearest74 = alloca %class.btVector3, align 4
  %len77 = alloca float, align 4
  %ref.tmp78 = alloca %class.btVector3, align 4
  %nearest88 = alloca %class.btVector3, align 4
  %len91 = alloca float, align 4
  %ref.tmp92 = alloca %class.btVector3, align 4
  %len103 = alloca float, align 4
  %ref.tmp104 = alloca %class.btVector3, align 4
  %edge = alloca %class.btVector3, align 4
  %isEdgeConvex = alloca i8, align 1
  %swapFactor = alloca float, align 4
  %nA = alloca %class.btVector3, align 4
  %orn = alloca %class.btQuaternion, align 4
  %computedNormalB = alloca %class.btVector3, align 4
  %ref.tmp124 = alloca float, align 4
  %nB = alloca %class.btVector3, align 4
  %NdotA = alloca float, align 4
  %NdotB = alloca float, align 4
  %backFacingNormal = alloca i8, align 1
  %clampedLocalNormal = alloca %class.btVector3, align 4
  %isClamped = alloca i8, align 1
  %ref.tmp139 = alloca %class.btVector3, align 4
  %ref.tmp147 = alloca %class.btVector3, align 4
  %newNormal = alloca %class.btVector3, align 4
  %ref.tmp154 = alloca %class.btVector3, align 4
  %ref.tmp155 = alloca %class.btVector3, align 4
  %ref.tmp157 = alloca %class.btVector3, align 4
  %len173 = alloca float, align 4
  %ref.tmp174 = alloca %class.btVector3, align 4
  %edge181 = alloca %class.btVector3, align 4
  %isEdgeConvex187 = alloca i8, align 1
  %swapFactor192 = alloca float, align 4
  %nA195 = alloca %class.btVector3, align 4
  %orn196 = alloca %class.btQuaternion, align 4
  %computedNormalB199 = alloca %class.btVector3, align 4
  %ref.tmp204 = alloca float, align 4
  %nB207 = alloca %class.btVector3, align 4
  %NdotA208 = alloca float, align 4
  %NdotB210 = alloca float, align 4
  %backFacingNormal212 = alloca i8, align 1
  %localContactNormalOnB225 = alloca %class.btVector3, align 4
  %ref.tmp226 = alloca %class.btMatrix3x3, align 4
  %clampedLocalNormal230 = alloca %class.btVector3, align 4
  %isClamped232 = alloca i8, align 1
  %ref.tmp233 = alloca %class.btVector3, align 4
  %ref.tmp242 = alloca %class.btVector3, align 4
  %newNormal247 = alloca %class.btVector3, align 4
  %ref.tmp251 = alloca %class.btVector3, align 4
  %ref.tmp253 = alloca %class.btVector3, align 4
  %ref.tmp257 = alloca %class.btVector3, align 4
  %len273 = alloca float, align 4
  %ref.tmp274 = alloca %class.btVector3, align 4
  %edge281 = alloca %class.btVector3, align 4
  %isEdgeConvex287 = alloca i8, align 1
  %swapFactor292 = alloca float, align 4
  %nA295 = alloca %class.btVector3, align 4
  %orn296 = alloca %class.btQuaternion, align 4
  %computedNormalB299 = alloca %class.btVector3, align 4
  %ref.tmp304 = alloca float, align 4
  %nB307 = alloca %class.btVector3, align 4
  %NdotA308 = alloca float, align 4
  %NdotB310 = alloca float, align 4
  %backFacingNormal312 = alloca i8, align 1
  %localContactNormalOnB325 = alloca %class.btVector3, align 4
  %ref.tmp326 = alloca %class.btMatrix3x3, align 4
  %clampedLocalNormal330 = alloca %class.btVector3, align 4
  %isClamped332 = alloca i8, align 1
  %ref.tmp333 = alloca %class.btVector3, align 4
  %ref.tmp342 = alloca %class.btVector3, align 4
  %newNormal347 = alloca %class.btVector3, align 4
  %ref.tmp351 = alloca %class.btVector3, align 4
  %ref.tmp353 = alloca %class.btVector3, align 4
  %ref.tmp357 = alloca %class.btVector3, align 4
  %ref.tmp378 = alloca float, align 4
  %ref.tmp381 = alloca %class.btVector3, align 4
  %newNormal386 = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %ref.tmp391 = alloca %class.btVector3, align 4
  %ref.tmp396 = alloca %class.btVector3, align 4
  %ref.tmp398 = alloca %class.btVector3, align 4
  %ref.tmp402 = alloca %class.btVector3, align 4
  store %class.btManifoldPoint* %cp, %class.btManifoldPoint** %cp.addr, align 4
  store %struct.btCollisionObjectWrapper* %colObj0Wrap, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %colObj1Wrap, %struct.btCollisionObjectWrapper** %colObj1Wrap.addr, align 4
  store i32 %partId0, i32* %partId0.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %normalAdjustFlags, i32* %normalAdjustFlags.addr, align 4
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %0)
  %call1 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %call)
  %cmp = icmp ne i32 %call1, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %if.end407

if.end:                                           ; preds = %entry
  store %class.btBvhTriangleMeshShape* null, %class.btBvhTriangleMeshShape** %trimesh, align 4
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call2 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %1)
  %call3 = call %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %call2)
  %call4 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %call3)
  %cmp5 = icmp eq i32 %call4, 22
  br i1 %cmp5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.end
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call7 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %2)
  %call8 = call %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %call7)
  %3 = bitcast %class.btCollisionShape* %call8 to %class.btScaledBvhTriangleMeshShape*
  %call9 = call %class.btBvhTriangleMeshShape* @_ZN28btScaledBvhTriangleMeshShape13getChildShapeEv(%class.btScaledBvhTriangleMeshShape* %3)
  store %class.btBvhTriangleMeshShape* %call9, %class.btBvhTriangleMeshShape** %trimesh, align 4
  br label %if.end12

if.else:                                          ; preds = %if.end
  %4 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call10 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %4)
  %call11 = call %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %call10)
  %5 = bitcast %class.btCollisionShape* %call11 to %class.btBvhTriangleMeshShape*
  store %class.btBvhTriangleMeshShape* %5, %class.btBvhTriangleMeshShape** %trimesh, align 4
  br label %if.end12

if.end12:                                         ; preds = %if.else, %if.then6
  %6 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %trimesh, align 4
  %call13 = call %struct.btTriangleInfoMap* @_ZN22btBvhTriangleMeshShape18getTriangleInfoMapEv(%class.btBvhTriangleMeshShape* %6)
  store %struct.btTriangleInfoMap* %call13, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %7 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %tobool = icmp ne %struct.btTriangleInfoMap* %7, null
  br i1 %tobool, label %if.end15, label %if.then14

if.then14:                                        ; preds = %if.end12
  br label %if.end407

if.end15:                                         ; preds = %if.end12
  %8 = load i32, i32* %partId0.addr, align 4
  %9 = load i32, i32* %index0.addr, align 4
  %call16 = call i32 @_ZL9btGetHashii(i32 %8, i32 %9)
  store i32 %call16, i32* %hash, align 4
  %10 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %11 = bitcast %struct.btTriangleInfoMap* %10 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %11, i32 4
  %12 = bitcast i8* %add.ptr to %class.btHashMap*
  %13 = load i32, i32* %hash, align 4
  %call17 = call %class.btHashInt* @_ZN9btHashIntC2Ei(%class.btHashInt* %ref.tmp, i32 %13)
  %call18 = call %struct.btTriangleInfo* @_ZN9btHashMapI9btHashInt14btTriangleInfoE4findERKS0_(%class.btHashMap* %12, %class.btHashInt* nonnull align 4 dereferenceable(4) %ref.tmp)
  store %struct.btTriangleInfo* %call18, %struct.btTriangleInfo** %info, align 4
  %14 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %tobool19 = icmp ne %struct.btTriangleInfo* %14, null
  br i1 %tobool19, label %if.end21, label %if.then20

if.then20:                                        ; preds = %if.end15
  br label %if.end407

if.end21:                                         ; preds = %if.end15
  %15 = load i32, i32* %normalAdjustFlags.addr, align 4
  %and = and i32 %15, 1
  %cmp22 = icmp eq i32 %and, 0
  %16 = zext i1 %cmp22 to i64
  %cond = select i1 %cmp22, float 1.000000e+00, float -1.000000e+00
  store float %cond, float* %frontFacing, align 4
  %17 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call23 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %17)
  %18 = bitcast %class.btCollisionShape* %call23 to %class.btTriangleShape*
  store %class.btTriangleShape* %18, %class.btTriangleShape** %tri_shape, align 4
  %call24 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %v0)
  %call25 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %v1)
  %call26 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %v2)
  %19 = load %class.btTriangleShape*, %class.btTriangleShape** %tri_shape, align 4
  %20 = bitcast %class.btTriangleShape* %19 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %20, align 4
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable, i64 27
  %21 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn, align 4
  call void %21(%class.btTriangleShape* %19, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %v0)
  %22 = load %class.btTriangleShape*, %class.btTriangleShape** %tri_shape, align 4
  %23 = bitcast %class.btTriangleShape* %22 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable27 = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %23, align 4
  %vfn28 = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable27, i64 27
  %24 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn28, align 4
  call void %24(%class.btTriangleShape* %22, i32 1, %class.btVector3* nonnull align 4 dereferenceable(16) %v1)
  %25 = load %class.btTriangleShape*, %class.btTriangleShape** %tri_shape, align 4
  %26 = bitcast %class.btTriangleShape* %25 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable29 = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %26, align 4
  %vfn30 = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable29, i64 27
  %27 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn30, align 4
  call void %27(%class.btTriangleShape* %25, i32 2, %class.btVector3* nonnull align 4 dereferenceable(16) %v2)
  store float 1.000000e+00, float* %ref.tmp31, align 4
  store float 0.000000e+00, float* %ref.tmp32, align 4
  store float 0.000000e+00, float* %ref.tmp33, align 4
  %call34 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %red, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp33)
  store float 0.000000e+00, float* %ref.tmp35, align 4
  store float 1.000000e+00, float* %ref.tmp36, align 4
  store float 0.000000e+00, float* %ref.tmp37, align 4
  %call38 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %green, float* nonnull align 4 dereferenceable(4) %ref.tmp35, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  store float 0.000000e+00, float* %ref.tmp39, align 4
  store float 0.000000e+00, float* %ref.tmp40, align 4
  store float 1.000000e+00, float* %ref.tmp41, align 4
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %blue, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  store float 1.000000e+00, float* %ref.tmp43, align 4
  store float 1.000000e+00, float* %ref.tmp44, align 4
  store float 1.000000e+00, float* %ref.tmp45, align 4
  %call46 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %white, float* nonnull align 4 dereferenceable(4) %ref.tmp43, float* nonnull align 4 dereferenceable(4) %ref.tmp44, float* nonnull align 4 dereferenceable(4) %ref.tmp45)
  store float 0.000000e+00, float* %ref.tmp47, align 4
  store float 0.000000e+00, float* %ref.tmp48, align 4
  store float 0.000000e+00, float* %ref.tmp49, align 4
  %call50 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %black, float* nonnull align 4 dereferenceable(4) %ref.tmp47, float* nonnull align 4 dereferenceable(4) %ref.tmp48, float* nonnull align 4 dereferenceable(4) %ref.tmp49)
  %call51 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tri_normal)
  %28 = load %class.btTriangleShape*, %class.btTriangleShape** %tri_shape, align 4
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %28, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %call52 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %nearest)
  %29 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_localPointB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %29, i32 0, i32 1
  call void @_Z27btNearestPointInLineSegmentRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointB, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest)
  %30 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_localPointB53 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %30, i32 0, i32 1
  %31 = bitcast %class.btVector3* %contact to i8*
  %32 = bitcast %class.btVector3* %m_localPointB53 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 16, i1 false)
  store i8 0, i8* %isNearEdge, align 1
  store i32 0, i32* %numConcaveEdgeHits, align 4
  store i32 0, i32* %numConvexEdgeHits, align 4
  %33 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call55 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %33)
  %call56 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call55)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp54, %class.btMatrix3x3* %call56)
  %34 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %34, i32 0, i32 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %localContactNormalOnB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp54, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB)
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %localContactNormalOnB)
  store i32 -1, i32* %bestedge, align 4
  store float 0x43ABC16D60000000, float* %disttobestedge, align 4
  %35 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV0V1Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %35, i32 0, i32 1
  %36 = load float, float* %m_edgeV0V1Angle, align 4
  %call58 = call float @_Z6btFabsf(float %36)
  %37 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_maxEdgeAngleThreshold = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %37, i32 0, i32 6
  %38 = load float, float* %m_maxEdgeAngleThreshold, align 4
  %cmp59 = fcmp olt float %call58, %38
  br i1 %cmp59, label %if.then60, label %if.end69

if.then60:                                        ; preds = %if.end21
  %call62 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %nearest61)
  %39 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_localPointB63 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %39, i32 0, i32 1
  call void @_Z27btNearestPointInLineSegmentRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointB63, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest61)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp64, %class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest61)
  %call65 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp64)
  store float %call65, float* %len, align 4
  %40 = load float, float* %len, align 4
  %41 = load float, float* %disttobestedge, align 4
  %cmp66 = fcmp olt float %40, %41
  br i1 %cmp66, label %if.then67, label %if.end68

if.then67:                                        ; preds = %if.then60
  store i32 0, i32* %bestedge, align 4
  %42 = load float, float* %len, align 4
  store float %42, float* %disttobestedge, align 4
  br label %if.end68

if.end68:                                         ; preds = %if.then67, %if.then60
  br label %if.end69

if.end69:                                         ; preds = %if.end68, %if.end21
  %43 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV1V2Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %43, i32 0, i32 2
  %44 = load float, float* %m_edgeV1V2Angle, align 4
  %call70 = call float @_Z6btFabsf(float %44)
  %45 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_maxEdgeAngleThreshold71 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %45, i32 0, i32 6
  %46 = load float, float* %m_maxEdgeAngleThreshold71, align 4
  %cmp72 = fcmp olt float %call70, %46
  br i1 %cmp72, label %if.then73, label %if.end83

if.then73:                                        ; preds = %if.end69
  %call75 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %nearest74)
  %47 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_localPointB76 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %47, i32 0, i32 1
  call void @_Z27btNearestPointInLineSegmentRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointB76, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest74)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp78, %class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest74)
  %call79 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp78)
  store float %call79, float* %len77, align 4
  %48 = load float, float* %len77, align 4
  %49 = load float, float* %disttobestedge, align 4
  %cmp80 = fcmp olt float %48, %49
  br i1 %cmp80, label %if.then81, label %if.end82

if.then81:                                        ; preds = %if.then73
  store i32 1, i32* %bestedge, align 4
  %50 = load float, float* %len77, align 4
  store float %50, float* %disttobestedge, align 4
  br label %if.end82

if.end82:                                         ; preds = %if.then81, %if.then73
  br label %if.end83

if.end83:                                         ; preds = %if.end82, %if.end69
  %51 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV2V0Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %51, i32 0, i32 3
  %52 = load float, float* %m_edgeV2V0Angle, align 4
  %call84 = call float @_Z6btFabsf(float %52)
  %53 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_maxEdgeAngleThreshold85 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %53, i32 0, i32 6
  %54 = load float, float* %m_maxEdgeAngleThreshold85, align 4
  %cmp86 = fcmp olt float %call84, %54
  br i1 %cmp86, label %if.then87, label %if.end97

if.then87:                                        ; preds = %if.end83
  %call89 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %nearest88)
  %55 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_localPointB90 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %55, i32 0, i32 1
  call void @_Z27btNearestPointInLineSegmentRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localPointB90, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest88)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp92, %class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest88)
  %call93 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp92)
  store float %call93, float* %len91, align 4
  %56 = load float, float* %len91, align 4
  %57 = load float, float* %disttobestedge, align 4
  %cmp94 = fcmp olt float %56, %57
  br i1 %cmp94, label %if.then95, label %if.end96

if.then95:                                        ; preds = %if.then87
  store i32 2, i32* %bestedge, align 4
  %58 = load float, float* %len91, align 4
  store float %58, float* %disttobestedge, align 4
  br label %if.end96

if.end96:                                         ; preds = %if.then95, %if.then87
  br label %if.end97

if.end97:                                         ; preds = %if.end96, %if.end83
  %59 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV0V1Angle98 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %59, i32 0, i32 1
  %60 = load float, float* %m_edgeV0V1Angle98, align 4
  %call99 = call float @_Z6btFabsf(float %60)
  %61 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_maxEdgeAngleThreshold100 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %61, i32 0, i32 6
  %62 = load float, float* %m_maxEdgeAngleThreshold100, align 4
  %cmp101 = fcmp olt float %call99, %62
  br i1 %cmp101, label %if.then102, label %if.end167

if.then102:                                       ; preds = %if.end97
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp104, %class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest)
  %call105 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp104)
  store float %call105, float* %len103, align 4
  %63 = load float, float* %len103, align 4
  %64 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_edgeDistanceThreshold = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %64, i32 0, i32 5
  %65 = load float, float* %m_edgeDistanceThreshold, align 4
  %cmp106 = fcmp olt float %63, %65
  br i1 %cmp106, label %if.then107, label %if.end166

if.then107:                                       ; preds = %if.then102
  %66 = load i32, i32* %bestedge, align 4
  %cmp108 = icmp eq i32 %66, 0
  br i1 %cmp108, label %if.then109, label %if.end165

if.then109:                                       ; preds = %if.then107
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1)
  store i8 1, i8* %isNearEdge, align 1
  %67 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV0V1Angle110 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %67, i32 0, i32 1
  %68 = load float, float* %m_edgeV0V1Angle110, align 4
  %cmp111 = fcmp oeq float %68, 0.000000e+00
  br i1 %cmp111, label %if.then112, label %if.else113

if.then112:                                       ; preds = %if.then109
  %69 = load i32, i32* %numConcaveEdgeHits, align 4
  %inc = add nsw i32 %69, 1
  store i32 %inc, i32* %numConcaveEdgeHits, align 4
  br label %if.end164

if.else113:                                       ; preds = %if.then109
  %70 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_flags = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %70, i32 0, i32 0
  %71 = load i32, i32* %m_flags, align 4
  %and114 = and i32 %71, 1
  %tobool115 = icmp ne i32 %and114, 0
  %frombool = zext i1 %tobool115 to i8
  store i8 %frombool, i8* %isEdgeConvex, align 1
  %72 = load i8, i8* %isEdgeConvex, align 1
  %tobool116 = trunc i8 %72 to i1
  %73 = zext i1 %tobool116 to i64
  %cond117 = select i1 %tobool116, float 1.000000e+00, float -1.000000e+00
  store float %cond117, float* %swapFactor, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %nA, float* nonnull align 4 dereferenceable(4) %swapFactor, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %74 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV0V1Angle118 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %74, i32 0, i32 1
  %call119 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %orn, %class.btVector3* nonnull align 4 dereferenceable(16) %edge, float* nonnull align 4 dereferenceable(4) %m_edgeV0V1Angle118)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %computedNormalB, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %75 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_flags120 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %75, i32 0, i32 0
  %76 = load i32, i32* %m_flags120, align 4
  %and121 = and i32 %76, 8
  %tobool122 = icmp ne i32 %and121, 0
  br i1 %tobool122, label %if.then123, label %if.end126

if.then123:                                       ; preds = %if.else113
  store float -1.000000e+00, float* %ref.tmp124, align 4
  %call125 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %computedNormalB, float* nonnull align 4 dereferenceable(4) %ref.tmp124)
  br label %if.end126

if.end126:                                        ; preds = %if.then123, %if.else113
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %nB, float* nonnull align 4 dereferenceable(4) %swapFactor, %class.btVector3* nonnull align 4 dereferenceable(16) %computedNormalB)
  %call127 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %localContactNormalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %nA)
  store float %call127, float* %NdotA, align 4
  %call128 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %localContactNormalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %nB)
  store float %call128, float* %NdotB, align 4
  %77 = load float, float* %NdotA, align 4
  %78 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_convexEpsilon = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %78, i32 0, i32 2
  %79 = load float, float* %m_convexEpsilon, align 4
  %cmp129 = fcmp olt float %77, %79
  br i1 %cmp129, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end126
  %80 = load float, float* %NdotB, align 4
  %81 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_convexEpsilon130 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %81, i32 0, i32 2
  %82 = load float, float* %m_convexEpsilon130, align 4
  %cmp131 = fcmp olt float %80, %82
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end126
  %83 = phi i1 [ false, %if.end126 ], [ %cmp131, %land.rhs ]
  %frombool132 = zext i1 %83 to i8
  store i8 %frombool132, i8* %backFacingNormal, align 1
  %84 = load i8, i8* %backFacingNormal, align 1
  %tobool133 = trunc i8 %84 to i1
  br i1 %tobool133, label %if.then134, label %if.else136

if.then134:                                       ; preds = %land.end
  %85 = load i32, i32* %numConcaveEdgeHits, align 4
  %inc135 = add nsw i32 %85, 1
  store i32 %inc135, i32* %numConcaveEdgeHits, align 4
  br label %if.end163

if.else136:                                       ; preds = %land.end
  %86 = load i32, i32* %numConvexEdgeHits, align 4
  %inc137 = add nsw i32 %86, 1
  store i32 %inc137, i32* %numConvexEdgeHits, align 4
  %call138 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %clampedLocalNormal)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp139, float* nonnull align 4 dereferenceable(4) %swapFactor, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %87 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV0V1Angle140 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %87, i32 0, i32 1
  %88 = load float, float* %m_edgeV0V1Angle140, align 4
  %call141 = call zeroext i1 @_Z13btClampNormalRK9btVector3S1_S1_fRS_(%class.btVector3* nonnull align 4 dereferenceable(16) %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp139, %class.btVector3* nonnull align 4 dereferenceable(16) %localContactNormalOnB, float %88, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedLocalNormal)
  %frombool142 = zext i1 %call141 to i8
  store i8 %frombool142, i8* %isClamped, align 1
  %89 = load i8, i8* %isClamped, align 1
  %tobool143 = trunc i8 %89 to i1
  br i1 %tobool143, label %if.then144, label %if.end162

if.then144:                                       ; preds = %if.else136
  %90 = load i32, i32* %normalAdjustFlags.addr, align 4
  %and145 = and i32 %90, 4
  %cmp146 = icmp ne i32 %and145, 0
  br i1 %cmp146, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.then144
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp147, float* nonnull align 4 dereferenceable(4) %frontFacing, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %call148 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %clampedLocalNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp147)
  %cmp149 = fcmp ogt float %call148, 0.000000e+00
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.then144
  %91 = phi i1 [ true, %if.then144 ], [ %cmp149, %lor.rhs ]
  br i1 %91, label %if.then150, label %if.end161

if.then150:                                       ; preds = %lor.end
  %92 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call151 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %92)
  %call152 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call151)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %newNormal, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call152, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedLocalNormal)
  %93 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB153 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %93, i32 0, i32 4
  %94 = bitcast %class.btVector3* %m_normalWorldOnB153 to i8*
  %95 = bitcast %class.btVector3* %newNormal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %94, i8* align 4 %95, i32 16, i1 false)
  %96 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_positionWorldOnA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %96, i32 0, i32 3
  %97 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB156 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %97, i32 0, i32 4
  %98 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_distance1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %98, i32 0, i32 5
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp155, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB156, float* nonnull align 4 dereferenceable(4) %m_distance1)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp154, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp155)
  %99 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_positionWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %99, i32 0, i32 2
  %100 = bitcast %class.btVector3* %m_positionWorldOnB to i8*
  %101 = bitcast %class.btVector3* %ref.tmp154 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %100, i8* align 4 %101, i32 16, i1 false)
  %102 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call158 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %102)
  %103 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_positionWorldOnB159 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %103, i32 0, i32 2
  call void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* sret align 4 %ref.tmp157, %class.btTransform* %call158, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnB159)
  %104 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_localPointB160 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %104, i32 0, i32 1
  %105 = bitcast %class.btVector3* %m_localPointB160 to i8*
  %106 = bitcast %class.btVector3* %ref.tmp157 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %105, i8* align 4 %106, i32 16, i1 false)
  br label %if.end161

if.end161:                                        ; preds = %if.then150, %lor.end
  br label %if.end162

if.end162:                                        ; preds = %if.end161, %if.else136
  br label %if.end163

if.end163:                                        ; preds = %if.end162, %if.then134
  br label %if.end164

if.end164:                                        ; preds = %if.end163, %if.then112
  br label %if.end165

if.end165:                                        ; preds = %if.end164, %if.then107
  br label %if.end166

if.end166:                                        ; preds = %if.end165, %if.then102
  br label %if.end167

if.end167:                                        ; preds = %if.end166, %if.end97
  call void @_Z27btNearestPointInLineSegmentRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest)
  %107 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV1V2Angle168 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %107, i32 0, i32 2
  %108 = load float, float* %m_edgeV1V2Angle168, align 4
  %call169 = call float @_Z6btFabsf(float %108)
  %109 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_maxEdgeAngleThreshold170 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %109, i32 0, i32 6
  %110 = load float, float* %m_maxEdgeAngleThreshold170, align 4
  %cmp171 = fcmp olt float %call169, %110
  br i1 %cmp171, label %if.then172, label %if.end267

if.then172:                                       ; preds = %if.end167
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp174, %class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest)
  %call175 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp174)
  store float %call175, float* %len173, align 4
  %111 = load float, float* %len173, align 4
  %112 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_edgeDistanceThreshold176 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %112, i32 0, i32 5
  %113 = load float, float* %m_edgeDistanceThreshold176, align 4
  %cmp177 = fcmp olt float %111, %113
  br i1 %cmp177, label %if.then178, label %if.end266

if.then178:                                       ; preds = %if.then172
  %114 = load i32, i32* %bestedge, align 4
  %cmp179 = icmp eq i32 %114, 1
  br i1 %cmp179, label %if.then180, label %if.end265

if.then180:                                       ; preds = %if.then178
  store i8 1, i8* %isNearEdge, align 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge181, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2)
  store i8 1, i8* %isNearEdge, align 1
  %115 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV1V2Angle182 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %115, i32 0, i32 2
  %116 = load float, float* %m_edgeV1V2Angle182, align 4
  %cmp183 = fcmp oeq float %116, 0.000000e+00
  br i1 %cmp183, label %if.then184, label %if.else186

if.then184:                                       ; preds = %if.then180
  %117 = load i32, i32* %numConcaveEdgeHits, align 4
  %inc185 = add nsw i32 %117, 1
  store i32 %inc185, i32* %numConcaveEdgeHits, align 4
  br label %if.end264

if.else186:                                       ; preds = %if.then180
  %118 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_flags188 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %118, i32 0, i32 0
  %119 = load i32, i32* %m_flags188, align 4
  %and189 = and i32 %119, 2
  %cmp190 = icmp ne i32 %and189, 0
  %frombool191 = zext i1 %cmp190 to i8
  store i8 %frombool191, i8* %isEdgeConvex187, align 1
  %120 = load i8, i8* %isEdgeConvex187, align 1
  %tobool193 = trunc i8 %120 to i1
  %121 = zext i1 %tobool193 to i64
  %cond194 = select i1 %tobool193, float 1.000000e+00, float -1.000000e+00
  store float %cond194, float* %swapFactor192, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %nA195, float* nonnull align 4 dereferenceable(4) %swapFactor192, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %122 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV1V2Angle197 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %122, i32 0, i32 2
  %call198 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %orn196, %class.btVector3* nonnull align 4 dereferenceable(16) %edge181, float* nonnull align 4 dereferenceable(4) %m_edgeV1V2Angle197)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %computedNormalB199, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn196, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %123 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_flags200 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %123, i32 0, i32 0
  %124 = load i32, i32* %m_flags200, align 4
  %and201 = and i32 %124, 16
  %tobool202 = icmp ne i32 %and201, 0
  br i1 %tobool202, label %if.then203, label %if.end206

if.then203:                                       ; preds = %if.else186
  store float -1.000000e+00, float* %ref.tmp204, align 4
  %call205 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %computedNormalB199, float* nonnull align 4 dereferenceable(4) %ref.tmp204)
  br label %if.end206

if.end206:                                        ; preds = %if.then203, %if.else186
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %nB207, float* nonnull align 4 dereferenceable(4) %swapFactor192, %class.btVector3* nonnull align 4 dereferenceable(16) %computedNormalB199)
  %call209 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %localContactNormalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %nA195)
  store float %call209, float* %NdotA208, align 4
  %call211 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %localContactNormalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %nB207)
  store float %call211, float* %NdotB210, align 4
  %125 = load float, float* %NdotA208, align 4
  %126 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_convexEpsilon213 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %126, i32 0, i32 2
  %127 = load float, float* %m_convexEpsilon213, align 4
  %cmp214 = fcmp olt float %125, %127
  br i1 %cmp214, label %land.rhs215, label %land.end218

land.rhs215:                                      ; preds = %if.end206
  %128 = load float, float* %NdotB210, align 4
  %129 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_convexEpsilon216 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %129, i32 0, i32 2
  %130 = load float, float* %m_convexEpsilon216, align 4
  %cmp217 = fcmp olt float %128, %130
  br label %land.end218

land.end218:                                      ; preds = %land.rhs215, %if.end206
  %131 = phi i1 [ false, %if.end206 ], [ %cmp217, %land.rhs215 ]
  %frombool219 = zext i1 %131 to i8
  store i8 %frombool219, i8* %backFacingNormal212, align 1
  %132 = load i8, i8* %backFacingNormal212, align 1
  %tobool220 = trunc i8 %132 to i1
  br i1 %tobool220, label %if.then221, label %if.else223

if.then221:                                       ; preds = %land.end218
  %133 = load i32, i32* %numConcaveEdgeHits, align 4
  %inc222 = add nsw i32 %133, 1
  store i32 %inc222, i32* %numConcaveEdgeHits, align 4
  br label %if.end263

if.else223:                                       ; preds = %land.end218
  %134 = load i32, i32* %numConvexEdgeHits, align 4
  %inc224 = add nsw i32 %134, 1
  store i32 %inc224, i32* %numConvexEdgeHits, align 4
  %135 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call227 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %135)
  %call228 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call227)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp226, %class.btMatrix3x3* %call228)
  %136 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB229 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %136, i32 0, i32 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %localContactNormalOnB225, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp226, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB229)
  %call231 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %clampedLocalNormal230)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp233, float* nonnull align 4 dereferenceable(4) %swapFactor192, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %137 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV1V2Angle234 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %137, i32 0, i32 2
  %138 = load float, float* %m_edgeV1V2Angle234, align 4
  %call235 = call zeroext i1 @_Z13btClampNormalRK9btVector3S1_S1_fRS_(%class.btVector3* nonnull align 4 dereferenceable(16) %edge181, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp233, %class.btVector3* nonnull align 4 dereferenceable(16) %localContactNormalOnB225, float %138, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedLocalNormal230)
  %frombool236 = zext i1 %call235 to i8
  store i8 %frombool236, i8* %isClamped232, align 1
  %139 = load i8, i8* %isClamped232, align 1
  %tobool237 = trunc i8 %139 to i1
  br i1 %tobool237, label %if.then238, label %if.end262

if.then238:                                       ; preds = %if.else223
  %140 = load i32, i32* %normalAdjustFlags.addr, align 4
  %and239 = and i32 %140, 4
  %cmp240 = icmp ne i32 %and239, 0
  br i1 %cmp240, label %lor.end245, label %lor.rhs241

lor.rhs241:                                       ; preds = %if.then238
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp242, float* nonnull align 4 dereferenceable(4) %frontFacing, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %call243 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %clampedLocalNormal230, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp242)
  %cmp244 = fcmp ogt float %call243, 0.000000e+00
  br label %lor.end245

lor.end245:                                       ; preds = %lor.rhs241, %if.then238
  %141 = phi i1 [ true, %if.then238 ], [ %cmp244, %lor.rhs241 ]
  br i1 %141, label %if.then246, label %if.end261

if.then246:                                       ; preds = %lor.end245
  %142 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call248 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %142)
  %call249 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call248)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %newNormal247, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call249, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedLocalNormal230)
  %143 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB250 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %143, i32 0, i32 4
  %144 = bitcast %class.btVector3* %m_normalWorldOnB250 to i8*
  %145 = bitcast %class.btVector3* %newNormal247 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %144, i8* align 4 %145, i32 16, i1 false)
  %146 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_positionWorldOnA252 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %146, i32 0, i32 3
  %147 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB254 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %147, i32 0, i32 4
  %148 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_distance1255 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %148, i32 0, i32 5
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp253, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB254, float* nonnull align 4 dereferenceable(4) %m_distance1255)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp251, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnA252, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp253)
  %149 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_positionWorldOnB256 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %149, i32 0, i32 2
  %150 = bitcast %class.btVector3* %m_positionWorldOnB256 to i8*
  %151 = bitcast %class.btVector3* %ref.tmp251 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %150, i8* align 4 %151, i32 16, i1 false)
  %152 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call258 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %152)
  %153 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_positionWorldOnB259 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %153, i32 0, i32 2
  call void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* sret align 4 %ref.tmp257, %class.btTransform* %call258, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnB259)
  %154 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_localPointB260 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %154, i32 0, i32 1
  %155 = bitcast %class.btVector3* %m_localPointB260 to i8*
  %156 = bitcast %class.btVector3* %ref.tmp257 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %155, i8* align 4 %156, i32 16, i1 false)
  br label %if.end261

if.end261:                                        ; preds = %if.then246, %lor.end245
  br label %if.end262

if.end262:                                        ; preds = %if.end261, %if.else223
  br label %if.end263

if.end263:                                        ; preds = %if.end262, %if.then221
  br label %if.end264

if.end264:                                        ; preds = %if.end263, %if.then184
  br label %if.end265

if.end265:                                        ; preds = %if.end264, %if.then178
  br label %if.end266

if.end266:                                        ; preds = %if.end265, %if.then172
  br label %if.end267

if.end267:                                        ; preds = %if.end266, %if.end167
  call void @_Z27btNearestPointInLineSegmentRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest)
  %157 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV2V0Angle268 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %157, i32 0, i32 3
  %158 = load float, float* %m_edgeV2V0Angle268, align 4
  %call269 = call float @_Z6btFabsf(float %158)
  %159 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_maxEdgeAngleThreshold270 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %159, i32 0, i32 6
  %160 = load float, float* %m_maxEdgeAngleThreshold270, align 4
  %cmp271 = fcmp olt float %call269, %160
  br i1 %cmp271, label %if.then272, label %if.end367

if.then272:                                       ; preds = %if.end267
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp274, %class.btVector3* nonnull align 4 dereferenceable(16) %contact, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest)
  %call275 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp274)
  store float %call275, float* %len273, align 4
  %161 = load float, float* %len273, align 4
  %162 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_edgeDistanceThreshold276 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %162, i32 0, i32 5
  %163 = load float, float* %m_edgeDistanceThreshold276, align 4
  %cmp277 = fcmp olt float %161, %163
  br i1 %cmp277, label %if.then278, label %if.end366

if.then278:                                       ; preds = %if.then272
  %164 = load i32, i32* %bestedge, align 4
  %cmp279 = icmp eq i32 %164, 2
  br i1 %cmp279, label %if.then280, label %if.end365

if.then280:                                       ; preds = %if.then278
  store i8 1, i8* %isNearEdge, align 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge281, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, %class.btVector3* nonnull align 4 dereferenceable(16) %v0)
  %165 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV2V0Angle282 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %165, i32 0, i32 3
  %166 = load float, float* %m_edgeV2V0Angle282, align 4
  %cmp283 = fcmp oeq float %166, 0.000000e+00
  br i1 %cmp283, label %if.then284, label %if.else286

if.then284:                                       ; preds = %if.then280
  %167 = load i32, i32* %numConcaveEdgeHits, align 4
  %inc285 = add nsw i32 %167, 1
  store i32 %inc285, i32* %numConcaveEdgeHits, align 4
  br label %if.end364

if.else286:                                       ; preds = %if.then280
  %168 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_flags288 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %168, i32 0, i32 0
  %169 = load i32, i32* %m_flags288, align 4
  %and289 = and i32 %169, 4
  %cmp290 = icmp ne i32 %and289, 0
  %frombool291 = zext i1 %cmp290 to i8
  store i8 %frombool291, i8* %isEdgeConvex287, align 1
  %170 = load i8, i8* %isEdgeConvex287, align 1
  %tobool293 = trunc i8 %170 to i1
  %171 = zext i1 %tobool293 to i64
  %cond294 = select i1 %tobool293, float 1.000000e+00, float -1.000000e+00
  store float %cond294, float* %swapFactor292, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %nA295, float* nonnull align 4 dereferenceable(4) %swapFactor292, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %172 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV2V0Angle297 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %172, i32 0, i32 3
  %call298 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %orn296, %class.btVector3* nonnull align 4 dereferenceable(16) %edge281, float* nonnull align 4 dereferenceable(4) %m_edgeV2V0Angle297)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %computedNormalB299, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn296, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %173 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_flags300 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %173, i32 0, i32 0
  %174 = load i32, i32* %m_flags300, align 4
  %and301 = and i32 %174, 32
  %tobool302 = icmp ne i32 %and301, 0
  br i1 %tobool302, label %if.then303, label %if.end306

if.then303:                                       ; preds = %if.else286
  store float -1.000000e+00, float* %ref.tmp304, align 4
  %call305 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %computedNormalB299, float* nonnull align 4 dereferenceable(4) %ref.tmp304)
  br label %if.end306

if.end306:                                        ; preds = %if.then303, %if.else286
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %nB307, float* nonnull align 4 dereferenceable(4) %swapFactor292, %class.btVector3* nonnull align 4 dereferenceable(16) %computedNormalB299)
  %call309 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %localContactNormalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %nA295)
  store float %call309, float* %NdotA308, align 4
  %call311 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %localContactNormalOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %nB307)
  store float %call311, float* %NdotB310, align 4
  %175 = load float, float* %NdotA308, align 4
  %176 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_convexEpsilon313 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %176, i32 0, i32 2
  %177 = load float, float* %m_convexEpsilon313, align 4
  %cmp314 = fcmp olt float %175, %177
  br i1 %cmp314, label %land.rhs315, label %land.end318

land.rhs315:                                      ; preds = %if.end306
  %178 = load float, float* %NdotB310, align 4
  %179 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %triangleInfoMapPtr, align 4
  %m_convexEpsilon316 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %179, i32 0, i32 2
  %180 = load float, float* %m_convexEpsilon316, align 4
  %cmp317 = fcmp olt float %178, %180
  br label %land.end318

land.end318:                                      ; preds = %land.rhs315, %if.end306
  %181 = phi i1 [ false, %if.end306 ], [ %cmp317, %land.rhs315 ]
  %frombool319 = zext i1 %181 to i8
  store i8 %frombool319, i8* %backFacingNormal312, align 1
  %182 = load i8, i8* %backFacingNormal312, align 1
  %tobool320 = trunc i8 %182 to i1
  br i1 %tobool320, label %if.then321, label %if.else323

if.then321:                                       ; preds = %land.end318
  %183 = load i32, i32* %numConcaveEdgeHits, align 4
  %inc322 = add nsw i32 %183, 1
  store i32 %inc322, i32* %numConcaveEdgeHits, align 4
  br label %if.end363

if.else323:                                       ; preds = %land.end318
  %184 = load i32, i32* %numConvexEdgeHits, align 4
  %inc324 = add nsw i32 %184, 1
  store i32 %inc324, i32* %numConvexEdgeHits, align 4
  %185 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call327 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %185)
  %call328 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call327)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp326, %class.btMatrix3x3* %call328)
  %186 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB329 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %186, i32 0, i32 4
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %localContactNormalOnB325, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp326, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB329)
  %call331 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %clampedLocalNormal330)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp333, float* nonnull align 4 dereferenceable(4) %swapFactor292, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %187 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV2V0Angle334 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %187, i32 0, i32 3
  %188 = load float, float* %m_edgeV2V0Angle334, align 4
  %call335 = call zeroext i1 @_Z13btClampNormalRK9btVector3S1_S1_fRS_(%class.btVector3* nonnull align 4 dereferenceable(16) %edge281, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp333, %class.btVector3* nonnull align 4 dereferenceable(16) %localContactNormalOnB325, float %188, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedLocalNormal330)
  %frombool336 = zext i1 %call335 to i8
  store i8 %frombool336, i8* %isClamped332, align 1
  %189 = load i8, i8* %isClamped332, align 1
  %tobool337 = trunc i8 %189 to i1
  br i1 %tobool337, label %if.then338, label %if.end362

if.then338:                                       ; preds = %if.else323
  %190 = load i32, i32* %normalAdjustFlags.addr, align 4
  %and339 = and i32 %190, 4
  %cmp340 = icmp ne i32 %and339, 0
  br i1 %cmp340, label %lor.end345, label %lor.rhs341

lor.rhs341:                                       ; preds = %if.then338
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp342, float* nonnull align 4 dereferenceable(4) %frontFacing, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %call343 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %clampedLocalNormal330, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp342)
  %cmp344 = fcmp ogt float %call343, 0.000000e+00
  br label %lor.end345

lor.end345:                                       ; preds = %lor.rhs341, %if.then338
  %191 = phi i1 [ true, %if.then338 ], [ %cmp344, %lor.rhs341 ]
  br i1 %191, label %if.then346, label %if.end361

if.then346:                                       ; preds = %lor.end345
  %192 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call348 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %192)
  %call349 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call348)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %newNormal347, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call349, %class.btVector3* nonnull align 4 dereferenceable(16) %clampedLocalNormal330)
  %193 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB350 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %193, i32 0, i32 4
  %194 = bitcast %class.btVector3* %m_normalWorldOnB350 to i8*
  %195 = bitcast %class.btVector3* %newNormal347 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %194, i8* align 4 %195, i32 16, i1 false)
  %196 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_positionWorldOnA352 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %196, i32 0, i32 3
  %197 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB354 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %197, i32 0, i32 4
  %198 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_distance1355 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %198, i32 0, i32 5
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp353, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB354, float* nonnull align 4 dereferenceable(4) %m_distance1355)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp351, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnA352, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp353)
  %199 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_positionWorldOnB356 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %199, i32 0, i32 2
  %200 = bitcast %class.btVector3* %m_positionWorldOnB356 to i8*
  %201 = bitcast %class.btVector3* %ref.tmp351 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %200, i8* align 4 %201, i32 16, i1 false)
  %202 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call358 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %202)
  %203 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_positionWorldOnB359 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %203, i32 0, i32 2
  call void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* sret align 4 %ref.tmp357, %class.btTransform* %call358, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnB359)
  %204 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_localPointB360 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %204, i32 0, i32 1
  %205 = bitcast %class.btVector3* %m_localPointB360 to i8*
  %206 = bitcast %class.btVector3* %ref.tmp357 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %205, i8* align 4 %206, i32 16, i1 false)
  br label %if.end361

if.end361:                                        ; preds = %if.then346, %lor.end345
  br label %if.end362

if.end362:                                        ; preds = %if.end361, %if.else323
  br label %if.end363

if.end363:                                        ; preds = %if.end362, %if.then321
  br label %if.end364

if.end364:                                        ; preds = %if.end363, %if.then284
  br label %if.end365

if.end365:                                        ; preds = %if.end364, %if.then278
  br label %if.end366

if.end366:                                        ; preds = %if.end365, %if.then272
  br label %if.end367

if.end367:                                        ; preds = %if.end366, %if.end267
  %207 = load i8, i8* %isNearEdge, align 1
  %tobool368 = trunc i8 %207 to i1
  br i1 %tobool368, label %if.then369, label %if.end407

if.then369:                                       ; preds = %if.end367
  %208 = load i32, i32* %numConcaveEdgeHits, align 4
  %cmp370 = icmp sgt i32 %208, 0
  br i1 %cmp370, label %if.then371, label %if.end406

if.then371:                                       ; preds = %if.then369
  %209 = load i32, i32* %normalAdjustFlags.addr, align 4
  %and372 = and i32 %209, 2
  %cmp373 = icmp ne i32 %and372, 0
  br i1 %cmp373, label %if.then374, label %if.else385

if.then374:                                       ; preds = %if.then371
  %call375 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %tri_normal, %class.btVector3* nonnull align 4 dereferenceable(16) %localContactNormalOnB)
  %cmp376 = fcmp olt float %call375, 0.000000e+00
  br i1 %cmp376, label %if.then377, label %if.end380

if.then377:                                       ; preds = %if.then374
  store float -1.000000e+00, float* %ref.tmp378, align 4
  %call379 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tri_normal, float* nonnull align 4 dereferenceable(4) %ref.tmp378)
  br label %if.end380

if.end380:                                        ; preds = %if.then377, %if.then374
  %210 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call382 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %210)
  %call383 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call382)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp381, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call383, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal)
  %211 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB384 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %211, i32 0, i32 4
  %212 = bitcast %class.btVector3* %m_normalWorldOnB384 to i8*
  %213 = bitcast %class.btVector3* %ref.tmp381 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %212, i8* align 4 %213, i32 16, i1 false)
  br label %if.end395

if.else385:                                       ; preds = %if.then371
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %newNormal386, %class.btVector3* nonnull align 4 dereferenceable(16) %tri_normal, float* nonnull align 4 dereferenceable(4) %frontFacing)
  %call387 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %newNormal386, %class.btVector3* nonnull align 4 dereferenceable(16) %localContactNormalOnB)
  store float %call387, float* %d, align 4
  %214 = load float, float* %d, align 4
  %cmp388 = fcmp olt float %214, 0.000000e+00
  br i1 %cmp388, label %if.then389, label %if.end390

if.then389:                                       ; preds = %if.else385
  br label %if.end407

if.end390:                                        ; preds = %if.else385
  %215 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call392 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %215)
  %call393 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %call392)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp391, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call393, %class.btVector3* nonnull align 4 dereferenceable(16) %newNormal386)
  %216 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB394 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %216, i32 0, i32 4
  %217 = bitcast %class.btVector3* %m_normalWorldOnB394 to i8*
  %218 = bitcast %class.btVector3* %ref.tmp391 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %217, i8* align 4 %218, i32 16, i1 false)
  br label %if.end395

if.end395:                                        ; preds = %if.end390, %if.end380
  %219 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_positionWorldOnA397 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %219, i32 0, i32 3
  %220 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_normalWorldOnB399 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %220, i32 0, i32 4
  %221 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_distance1400 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %221, i32 0, i32 5
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp398, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normalWorldOnB399, float* nonnull align 4 dereferenceable(4) %m_distance1400)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp396, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnA397, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp398)
  %222 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_positionWorldOnB401 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %222, i32 0, i32 2
  %223 = bitcast %class.btVector3* %m_positionWorldOnB401 to i8*
  %224 = bitcast %class.btVector3* %ref.tmp396 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %223, i8* align 4 %224, i32 16, i1 false)
  %225 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %colObj0Wrap.addr, align 4
  %call403 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %225)
  %226 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_positionWorldOnB404 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %226, i32 0, i32 2
  call void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* sret align 4 %ref.tmp402, %class.btTransform* %call403, %class.btVector3* nonnull align 4 dereferenceable(16) %m_positionWorldOnB404)
  %227 = load %class.btManifoldPoint*, %class.btManifoldPoint** %cp.addr, align 4
  %m_localPointB405 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %227, i32 0, i32 1
  %228 = bitcast %class.btVector3* %m_localPointB405 to i8*
  %229 = bitcast %class.btVector3* %ref.tmp402 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %228, i8* align 4 %229, i32 16, i1 false)
  br label %if.end406

if.end406:                                        ; preds = %if.end395, %if.then369
  br label %if.end407

if.end407:                                        ; preds = %if.then, %if.then14, %if.then20, %if.then389, %if.end406, %if.end367
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_shape, align 4
  ret %class.btCollisionShape* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4
  ret %class.btCollisionShape* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btBvhTriangleMeshShape* @_ZN28btScaledBvhTriangleMeshShape13getChildShapeEv(%class.btScaledBvhTriangleMeshShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btScaledBvhTriangleMeshShape*, align 4
  store %class.btScaledBvhTriangleMeshShape* %this, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %this1 = load %class.btScaledBvhTriangleMeshShape*, %class.btScaledBvhTriangleMeshShape** %this.addr, align 4
  %m_bvhTriMeshShape = getelementptr inbounds %class.btScaledBvhTriangleMeshShape, %class.btScaledBvhTriangleMeshShape* %this1, i32 0, i32 2
  %0 = load %class.btBvhTriangleMeshShape*, %class.btBvhTriangleMeshShape** %m_bvhTriMeshShape, align 4
  ret %class.btBvhTriangleMeshShape* %0
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZL9btGetHashii(i32 %partId, i32 %triangleIndex) #1 {
entry:
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %hash = alloca i32, align 4
  store i32 %partId, i32* %partId.addr, align 4
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4
  %0 = load i32, i32* %partId.addr, align 4
  %shl = shl i32 %0, 21
  %1 = load i32, i32* %triangleIndex.addr, align 4
  %or = or i32 %shl, %1
  store i32 %or, i32* %hash, align 4
  %2 = load i32, i32* %hash, align 4
  ret i32 %2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btTriangleInfo* @_ZN9btHashMapI9btHashInt14btTriangleInfoE4findERKS0_(%class.btHashMap* %this, %class.btHashInt* nonnull align 4 dereferenceable(4) %key) #2 comdat {
entry:
  %retval = alloca %struct.btTriangleInfo*, align 4
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %class.btHashInt*, align 4
  %index = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4
  store %class.btHashInt* %key, %class.btHashInt** %key.addr, align 4
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4
  %call = call i32 @_ZNK9btHashMapI9btHashInt14btTriangleInfoE9findIndexERKS0_(%class.btHashMap* %this1, %class.btHashInt* nonnull align 4 dereferenceable(4) %0)
  store i32 %call, i32* %index, align 4
  %1 = load i32, i32* %index, align 4
  %cmp = icmp eq i32 %1, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %struct.btTriangleInfo* null, %struct.btTriangleInfo** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %2 = load i32, i32* %index, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %struct.btTriangleInfo* @_ZN20btAlignedObjectArrayI14btTriangleInfoEixEi(%class.btAlignedObjectArray.12* %m_valueArray, i32 %2)
  store %struct.btTriangleInfo* %call2, %struct.btTriangleInfo** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %retval, align 4
  ret %struct.btTriangleInfo* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btHashInt* @_ZN9btHashIntC2Ei(%class.btHashInt* returned %this, i32 %uid) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHashInt*, align 4
  %uid.addr = alloca i32, align 4
  store %class.btHashInt* %this, %class.btHashInt** %this.addr, align 4
  store i32 %uid, i32* %uid.addr, align 4
  %this1 = load %class.btHashInt*, %class.btHashInt** %this.addr, align 4
  %m_uid = getelementptr inbounds %class.btHashInt, %class.btHashInt* %this1, i32 0, i32 0
  %0 = load i32, i32* %uid.addr, align 4
  store i32 %0, i32* %m_uid, align 4
  ret %class.btHashInt* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 1
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4)
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 2
  %m_vertices18 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices18, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx9)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %0 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  %2 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %0 = load %class.btTransform*, %class.btTransform** %m_worldTransform, align 4
  ret %class.btTransform* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %rotation.addr = alloca %class.btQuaternion*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %q = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  store %class.btQuaternion* %rotation, %class.btQuaternion** %rotation.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  call void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* sret align 4 %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %2)
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %3)
  %4 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %4)
  %5 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %5)
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call1, float* nonnull align 4 dereferenceable(4) %call2, float* nonnull align 4 dereferenceable(4) %call3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform8invXformERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %inVec) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %inVec.addr = alloca %class.btVector3*, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %inVec, %class.btVector3** %inVec.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %inVec.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %m_basis)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %b.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %a.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTriangleCallback* @_ZN18btTriangleCallbackC2Ev(%class.btTriangleCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleCallback*, align 4
  store %class.btTriangleCallback* %this, %class.btTriangleCallback** %this.addr, align 4
  %this1 = load %class.btTriangleCallback*, %class.btTriangleCallback** %this.addr, align 4
  %0 = bitcast %class.btTriangleCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV18btTriangleCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btTriangleCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btConnectivityProcessorD0Ev(%struct.btConnectivityProcessor* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btConnectivityProcessor*, align 4
  store %struct.btConnectivityProcessor* %this, %struct.btConnectivityProcessor** %this.addr, align 4
  %this1 = load %struct.btConnectivityProcessor*, %struct.btConnectivityProcessor** %this.addr, align 4
  %call = call %struct.btConnectivityProcessor* @_ZN23btConnectivityProcessorD2Ev(%struct.btConnectivityProcessor* %this1) #9
  %0 = bitcast %struct.btConnectivityProcessor* %this1 to i8*
  call void @_ZdlPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN23btConnectivityProcessor15processTriangleEP9btVector3ii(%struct.btConnectivityProcessor* %this, %class.btVector3* %triangle, i32 %partId, i32 %triangleIndex) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btConnectivityProcessor*, align 4
  %triangle.addr = alloca %class.btVector3*, align 4
  %partId.addr = alloca i32, align 4
  %triangleIndex.addr = alloca i32, align 4
  %numshared = alloca i32, align 4
  %sharedVertsA = alloca [3 x i32], align 4
  %sharedVertsB = alloca [3 x i32], align 4
  %crossBSqr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %crossASqr = alloca float, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %tmp = alloca i32, align 4
  %hash = alloca i32, align 4
  %info = alloca %struct.btTriangleInfo*, align 4
  %ref.tmp72 = alloca %class.btHashInt, align 4
  %tmp76 = alloca %struct.btTriangleInfo, align 4
  %ref.tmp80 = alloca %class.btHashInt, align 4
  %ref.tmp84 = alloca %class.btHashInt, align 4
  %sumvertsA = alloca i32, align 4
  %otherIndexA = alloca i32, align 4
  %edge = alloca %class.btVector3, align 4
  %tA = alloca %class.btTriangleShape, align 4
  %otherIndexB = alloca i32, align 4
  %tB = alloca %class.btTriangleShape, align 4
  %normalA = alloca %class.btVector3, align 4
  %normalB = alloca %class.btVector3, align 4
  %edgeCrossA = alloca %class.btVector3, align 4
  %ref.tmp116 = alloca %class.btVector3, align 4
  %tmp118 = alloca %class.btVector3, align 4
  %ref.tmp127 = alloca float, align 4
  %edgeCrossB = alloca %class.btVector3, align 4
  %ref.tmp130 = alloca %class.btVector3, align 4
  %tmp132 = alloca %class.btVector3, align 4
  %ref.tmp139 = alloca float, align 4
  %angle2 = alloca float, align 4
  %ang4 = alloca float, align 4
  %calculatedEdge = alloca %class.btVector3, align 4
  %len2 = alloca float, align 4
  %correctedAngle = alloca float, align 4
  %isConvex = alloca i8, align 1
  %calculatedNormalA = alloca %class.btVector3, align 4
  %dotA = alloca float, align 4
  %edge155 = alloca %class.btVector3, align 4
  %orn = alloca %class.btQuaternion, align 4
  %ref.tmp160 = alloca float, align 4
  %computedNormalB = alloca %class.btVector3, align 4
  %bla = alloca float, align 4
  %ref.tmp166 = alloca float, align 4
  %edge176 = alloca %class.btVector3, align 4
  %orn181 = alloca %class.btQuaternion, align 4
  %ref.tmp182 = alloca float, align 4
  %computedNormalB185 = alloca %class.btVector3, align 4
  %ref.tmp189 = alloca float, align 4
  %edge201 = alloca %class.btVector3, align 4
  %orn206 = alloca %class.btQuaternion, align 4
  %ref.tmp207 = alloca float, align 4
  %computedNormalB210 = alloca %class.btVector3, align 4
  %ref.tmp216 = alloca float, align 4
  store %struct.btConnectivityProcessor* %this, %struct.btConnectivityProcessor** %this.addr, align 4
  store %class.btVector3* %triangle, %class.btVector3** %triangle.addr, align 4
  store i32 %partId, i32* %partId.addr, align 4
  store i32 %triangleIndex, i32* %triangleIndex.addr, align 4
  %this1 = load %struct.btConnectivityProcessor*, %struct.btConnectivityProcessor** %this.addr, align 4
  %m_partIdA = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_partIdA, align 4
  %1 = load i32, i32* %partId.addr, align 4
  %cmp = icmp eq i32 %0, %1
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %m_triangleIndexA = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_triangleIndexA, align 4
  %3 = load i32, i32* %triangleIndex.addr, align 4
  %cmp2 = icmp eq i32 %2, %3
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %sw.epilog227

if.end:                                           ; preds = %land.lhs.true, %entry
  store i32 0, i32* %numshared, align 4
  %4 = bitcast [3 x i32]* %sharedVertsA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 bitcast ([3 x i32]* @__const._ZN23btConnectivityProcessor15processTriangleEP9btVector3ii.sharedVertsA to i8*), i32 12, i1 false)
  %5 = bitcast [3 x i32]* %sharedVertsB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 bitcast ([3 x i32]* @__const._ZN23btConnectivityProcessor15processTriangleEP9btVector3ii.sharedVertsB to i8*), i32 12, i1 false)
  %6 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 1
  %7 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4)
  %8 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 2
  %9 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx7 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  store float %call, float* %crossBSqr, align 4
  %10 = load float, float* %crossBSqr, align 4
  %m_triangleInfoMap = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 4
  %11 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap, align 4
  %m_equalVertexThreshold = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %11, i32 0, i32 4
  %12 = load float, float* %m_equalVertexThreshold, align 4
  %cmp8 = fcmp olt float %10, %12
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.end
  br label %sw.epilog227

if.end10:                                         ; preds = %if.end
  %m_triangleVerticesA = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %13 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA, align 4
  %arrayidx13 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 1
  %m_triangleVerticesA14 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %14 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA14, align 4
  %arrayidx15 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx13, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx15)
  %m_triangleVerticesA17 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %15 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA17, align 4
  %arrayidx18 = getelementptr inbounds %class.btVector3, %class.btVector3* %15, i32 2
  %m_triangleVerticesA19 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %16 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA19, align 4
  %arrayidx20 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx18, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx20)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp16)
  %call21 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp11)
  store float %call21, float* %crossASqr, align 4
  %17 = load float, float* %crossASqr, align 4
  %m_triangleInfoMap22 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 4
  %18 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap22, align 4
  %m_equalVertexThreshold23 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %18, i32 0, i32 4
  %19 = load float, float* %m_equalVertexThreshold23, align 4
  %cmp24 = fcmp olt float %17, %19
  br i1 %cmp24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %if.end10
  br label %sw.epilog227

if.end26:                                         ; preds = %if.end10
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc50, %if.end26
  %20 = load i32, i32* %i, align 4
  %cmp27 = icmp slt i32 %20, 3
  br i1 %cmp27, label %for.body, label %for.end52

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond28

for.cond28:                                       ; preds = %for.inc, %for.body
  %21 = load i32, i32* %j, align 4
  %cmp29 = icmp slt i32 %21, 3
  br i1 %cmp29, label %for.body30, label %for.end

for.body30:                                       ; preds = %for.cond28
  %m_triangleVerticesA32 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %22 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA32, align 4
  %23 = load i32, i32* %i, align 4
  %arrayidx33 = getelementptr inbounds %class.btVector3, %class.btVector3* %22, i32 %23
  %24 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %25 = load i32, i32* %j, align 4
  %arrayidx34 = getelementptr inbounds %class.btVector3, %class.btVector3* %24, i32 %25
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx33, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx34)
  %call35 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp31)
  %m_triangleInfoMap36 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 4
  %26 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap36, align 4
  %m_equalVertexThreshold37 = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %26, i32 0, i32 4
  %27 = load float, float* %m_equalVertexThreshold37, align 4
  %cmp38 = fcmp olt float %call35, %27
  br i1 %cmp38, label %if.then39, label %if.end45

if.then39:                                        ; preds = %for.body30
  %28 = load i32, i32* %i, align 4
  %29 = load i32, i32* %numshared, align 4
  %arrayidx40 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 %29
  store i32 %28, i32* %arrayidx40, align 4
  %30 = load i32, i32* %j, align 4
  %31 = load i32, i32* %numshared, align 4
  %arrayidx41 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 %31
  store i32 %30, i32* %arrayidx41, align 4
  %32 = load i32, i32* %numshared, align 4
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %numshared, align 4
  %33 = load i32, i32* %numshared, align 4
  %cmp42 = icmp sge i32 %33, 3
  br i1 %cmp42, label %if.then43, label %if.end44

if.then43:                                        ; preds = %if.then39
  br label %sw.epilog227

if.end44:                                         ; preds = %if.then39
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %for.body30
  br label %for.inc

for.inc:                                          ; preds = %if.end45
  %34 = load i32, i32* %j, align 4
  %inc46 = add nsw i32 %34, 1
  store i32 %inc46, i32* %j, align 4
  br label %for.cond28

for.end:                                          ; preds = %for.cond28
  %35 = load i32, i32* %numshared, align 4
  %cmp47 = icmp sge i32 %35, 3
  br i1 %cmp47, label %if.then48, label %if.end49

if.then48:                                        ; preds = %for.end
  br label %sw.epilog227

if.end49:                                         ; preds = %for.end
  br label %for.inc50

for.inc50:                                        ; preds = %if.end49
  %36 = load i32, i32* %i, align 4
  %inc51 = add nsw i32 %36, 1
  store i32 %inc51, i32* %i, align 4
  br label %for.cond

for.end52:                                        ; preds = %for.cond
  %37 = load i32, i32* %numshared, align 4
  switch i32 %37, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb53
    i32 2, label %sw.bb54
  ]

sw.bb:                                            ; preds = %for.end52
  br label %sw.epilog227

sw.bb53:                                          ; preds = %for.end52
  br label %sw.epilog227

sw.bb54:                                          ; preds = %for.end52
  %arrayidx55 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 0
  %38 = load i32, i32* %arrayidx55, align 4
  %cmp56 = icmp eq i32 %38, 0
  br i1 %cmp56, label %land.lhs.true57, label %if.end67

land.lhs.true57:                                  ; preds = %sw.bb54
  %arrayidx58 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 1
  %39 = load i32, i32* %arrayidx58, align 4
  %cmp59 = icmp eq i32 %39, 2
  br i1 %cmp59, label %if.then60, label %if.end67

if.then60:                                        ; preds = %land.lhs.true57
  %arrayidx61 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 0
  store i32 2, i32* %arrayidx61, align 4
  %arrayidx62 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 1
  store i32 0, i32* %arrayidx62, align 4
  %arrayidx63 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 1
  %40 = load i32, i32* %arrayidx63, align 4
  store i32 %40, i32* %tmp, align 4
  %arrayidx64 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 0
  %41 = load i32, i32* %arrayidx64, align 4
  %arrayidx65 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 1
  store i32 %41, i32* %arrayidx65, align 4
  %42 = load i32, i32* %tmp, align 4
  %arrayidx66 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 0
  store i32 %42, i32* %arrayidx66, align 4
  br label %if.end67

if.end67:                                         ; preds = %if.then60, %land.lhs.true57, %sw.bb54
  %m_partIdA68 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 1
  %43 = load i32, i32* %m_partIdA68, align 4
  %m_triangleIndexA69 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 2
  %44 = load i32, i32* %m_triangleIndexA69, align 4
  %call70 = call i32 @_ZL9btGetHashii(i32 %43, i32 %44)
  store i32 %call70, i32* %hash, align 4
  %m_triangleInfoMap71 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 4
  %45 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap71, align 4
  %46 = bitcast %struct.btTriangleInfoMap* %45 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %46, i32 4
  %47 = bitcast i8* %add.ptr to %class.btHashMap*
  %48 = load i32, i32* %hash, align 4
  %call73 = call %class.btHashInt* @_ZN9btHashIntC2Ei(%class.btHashInt* %ref.tmp72, i32 %48)
  %call74 = call %struct.btTriangleInfo* @_ZN9btHashMapI9btHashInt14btTriangleInfoE4findERKS0_(%class.btHashMap* %47, %class.btHashInt* nonnull align 4 dereferenceable(4) %ref.tmp72)
  store %struct.btTriangleInfo* %call74, %struct.btTriangleInfo** %info, align 4
  %49 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %tobool = icmp ne %struct.btTriangleInfo* %49, null
  br i1 %tobool, label %if.end87, label %if.then75

if.then75:                                        ; preds = %if.end67
  %call77 = call %struct.btTriangleInfo* @_ZN14btTriangleInfoC2Ev(%struct.btTriangleInfo* %tmp76)
  %m_triangleInfoMap78 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 4
  %50 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap78, align 4
  %51 = bitcast %struct.btTriangleInfoMap* %50 to i8*
  %add.ptr79 = getelementptr inbounds i8, i8* %51, i32 4
  %52 = bitcast i8* %add.ptr79 to %class.btHashMap*
  %53 = load i32, i32* %hash, align 4
  %call81 = call %class.btHashInt* @_ZN9btHashIntC2Ei(%class.btHashInt* %ref.tmp80, i32 %53)
  call void @_ZN9btHashMapI9btHashInt14btTriangleInfoE6insertERKS0_RKS1_(%class.btHashMap* %52, %class.btHashInt* nonnull align 4 dereferenceable(4) %ref.tmp80, %struct.btTriangleInfo* nonnull align 4 dereferenceable(16) %tmp76)
  %m_triangleInfoMap82 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 4
  %54 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap82, align 4
  %55 = bitcast %struct.btTriangleInfoMap* %54 to i8*
  %add.ptr83 = getelementptr inbounds i8, i8* %55, i32 4
  %56 = bitcast i8* %add.ptr83 to %class.btHashMap*
  %57 = load i32, i32* %hash, align 4
  %call85 = call %class.btHashInt* @_ZN9btHashIntC2Ei(%class.btHashInt* %ref.tmp84, i32 %57)
  %call86 = call %struct.btTriangleInfo* @_ZN9btHashMapI9btHashInt14btTriangleInfoE4findERKS0_(%class.btHashMap* %56, %class.btHashInt* nonnull align 4 dereferenceable(4) %ref.tmp84)
  store %struct.btTriangleInfo* %call86, %struct.btTriangleInfo** %info, align 4
  br label %if.end87

if.end87:                                         ; preds = %if.then75, %if.end67
  %arrayidx88 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 0
  %58 = load i32, i32* %arrayidx88, align 4
  %arrayidx89 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 1
  %59 = load i32, i32* %arrayidx89, align 4
  %add = add nsw i32 %58, %59
  store i32 %add, i32* %sumvertsA, align 4
  %60 = load i32, i32* %sumvertsA, align 4
  %sub = sub nsw i32 3, %60
  store i32 %sub, i32* %otherIndexA, align 4
  %m_triangleVerticesA90 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %61 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA90, align 4
  %arrayidx91 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 1
  %62 = load i32, i32* %arrayidx91, align 4
  %arrayidx92 = getelementptr inbounds %class.btVector3, %class.btVector3* %61, i32 %62
  %m_triangleVerticesA93 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %63 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA93, align 4
  %arrayidx94 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 0
  %64 = load i32, i32* %arrayidx94, align 4
  %arrayidx95 = getelementptr inbounds %class.btVector3, %class.btVector3* %63, i32 %64
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx92, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx95)
  %m_triangleVerticesA96 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %65 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA96, align 4
  %arrayidx97 = getelementptr inbounds %class.btVector3, %class.btVector3* %65, i32 0
  %m_triangleVerticesA98 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %66 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA98, align 4
  %arrayidx99 = getelementptr inbounds %class.btVector3, %class.btVector3* %66, i32 1
  %m_triangleVerticesA100 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %67 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA100, align 4
  %arrayidx101 = getelementptr inbounds %class.btVector3, %class.btVector3* %67, i32 2
  %call102 = call %class.btTriangleShape* @_ZN15btTriangleShapeC2ERK9btVector3S2_S2_(%class.btTriangleShape* %tA, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx97, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx99, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx101)
  %arrayidx103 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 0
  %68 = load i32, i32* %arrayidx103, align 4
  %arrayidx104 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 1
  %69 = load i32, i32* %arrayidx104, align 4
  %add105 = add nsw i32 %68, %69
  %sub106 = sub nsw i32 3, %add105
  store i32 %sub106, i32* %otherIndexB, align 4
  %70 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx107 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 1
  %71 = load i32, i32* %arrayidx107, align 4
  %arrayidx108 = getelementptr inbounds %class.btVector3, %class.btVector3* %70, i32 %71
  %72 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx109 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 0
  %73 = load i32, i32* %arrayidx109, align 4
  %arrayidx110 = getelementptr inbounds %class.btVector3, %class.btVector3* %72, i32 %73
  %74 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %75 = load i32, i32* %otherIndexB, align 4
  %arrayidx111 = getelementptr inbounds %class.btVector3, %class.btVector3* %74, i32 %75
  %call112 = call %class.btTriangleShape* @_ZN15btTriangleShapeC2ERK9btVector3S2_S2_(%class.btTriangleShape* %tB, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx108, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx110, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx111)
  %call113 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normalA)
  %call114 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normalB)
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %tA, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA)
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %tB, %class.btVector3* nonnull align 4 dereferenceable(16) %normalB)
  %call115 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %edge)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp116, %class.btVector3* %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA)
  %call117 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %ref.tmp116)
  %76 = bitcast %class.btVector3* %edgeCrossA to i8*
  %77 = bitcast %class.btVector3* %call117 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %76, i8* align 4 %77, i32 16, i1 false)
  %m_triangleVerticesA119 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %78 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA119, align 4
  %79 = load i32, i32* %otherIndexA, align 4
  %arrayidx120 = getelementptr inbounds %class.btVector3, %class.btVector3* %78, i32 %79
  %m_triangleVerticesA121 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %80 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA121, align 4
  %arrayidx122 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsA, i32 0, i32 0
  %81 = load i32, i32* %arrayidx122, align 4
  %arrayidx123 = getelementptr inbounds %class.btVector3, %class.btVector3* %80, i32 %81
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %tmp118, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx120, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx123)
  %call124 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %edgeCrossA, %class.btVector3* nonnull align 4 dereferenceable(16) %tmp118)
  %cmp125 = fcmp olt float %call124, 0.000000e+00
  br i1 %cmp125, label %if.then126, label %if.end129

if.then126:                                       ; preds = %if.end87
  store float -1.000000e+00, float* %ref.tmp127, align 4
  %call128 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %edgeCrossA, float* nonnull align 4 dereferenceable(4) %ref.tmp127)
  br label %if.end129

if.end129:                                        ; preds = %if.then126, %if.end87
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp130, %class.btVector3* %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %normalB)
  %call131 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %ref.tmp130)
  %82 = bitcast %class.btVector3* %edgeCrossB to i8*
  %83 = bitcast %class.btVector3* %call131 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %82, i8* align 4 %83, i32 16, i1 false)
  %84 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %85 = load i32, i32* %otherIndexB, align 4
  %arrayidx133 = getelementptr inbounds %class.btVector3, %class.btVector3* %84, i32 %85
  %86 = load %class.btVector3*, %class.btVector3** %triangle.addr, align 4
  %arrayidx134 = getelementptr inbounds [3 x i32], [3 x i32]* %sharedVertsB, i32 0, i32 0
  %87 = load i32, i32* %arrayidx134, align 4
  %arrayidx135 = getelementptr inbounds %class.btVector3, %class.btVector3* %86, i32 %87
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %tmp132, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx133, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx135)
  %call136 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %edgeCrossB, %class.btVector3* nonnull align 4 dereferenceable(16) %tmp132)
  %cmp137 = fcmp olt float %call136, 0.000000e+00
  br i1 %cmp137, label %if.then138, label %if.end141

if.then138:                                       ; preds = %if.end129
  store float -1.000000e+00, float* %ref.tmp139, align 4
  %call140 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %edgeCrossB, float* nonnull align 4 dereferenceable(4) %ref.tmp139)
  br label %if.end141

if.end141:                                        ; preds = %if.then138, %if.end129
  store float 0.000000e+00, float* %angle2, align 4
  store float 0.000000e+00, float* %ang4, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %calculatedEdge, %class.btVector3* %edgeCrossA, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeCrossB)
  %call142 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %calculatedEdge)
  store float %call142, float* %len2, align 4
  store float 0.000000e+00, float* %correctedAngle, align 4
  store i8 0, i8* %isConvex, align 1
  %88 = load float, float* %len2, align 4
  %m_triangleInfoMap143 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 4
  %89 = load %struct.btTriangleInfoMap*, %struct.btTriangleInfoMap** %m_triangleInfoMap143, align 4
  %m_planarEpsilon = getelementptr inbounds %struct.btTriangleInfoMap, %struct.btTriangleInfoMap* %89, i32 0, i32 3
  %90 = load float, float* %m_planarEpsilon, align 4
  %cmp144 = fcmp olt float %88, %90
  br i1 %cmp144, label %if.then145, label %if.else

if.then145:                                       ; preds = %if.end141
  store float 0.000000e+00, float* %angle2, align 4
  store float 0.000000e+00, float* %ang4, align 4
  br label %if.end153

if.else:                                          ; preds = %if.end141
  %call146 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %calculatedEdge)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %calculatedNormalA, %class.btVector3* %calculatedEdge, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeCrossA)
  %call147 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %calculatedNormalA)
  %call148 = call float @_ZL10btGetAngleRK9btVector3S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %calculatedNormalA, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeCrossA, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeCrossB)
  store float %call148, float* %angle2, align 4
  %91 = load float, float* %angle2, align 4
  %sub149 = fsub float 0x400921FB60000000, %91
  store float %sub149, float* %ang4, align 4
  %call150 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %normalA, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeCrossB)
  store float %call150, float* %dotA, align 4
  %92 = load float, float* %dotA, align 4
  %conv = fpext float %92 to double
  %cmp151 = fcmp olt double %conv, 0.000000e+00
  %frombool = zext i1 %cmp151 to i8
  store i8 %frombool, i8* %isConvex, align 1
  %93 = load i8, i8* %isConvex, align 1
  %tobool152 = trunc i8 %93 to i1
  br i1 %tobool152, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %94 = load float, float* %ang4, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %95 = load float, float* %ang4, align 4
  %fneg = fneg float %95
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %94, %cond.true ], [ %fneg, %cond.false ]
  store float %cond, float* %correctedAngle, align 4
  br label %if.end153

if.end153:                                        ; preds = %cond.end, %if.then145
  %96 = load i32, i32* %sumvertsA, align 4
  switch i32 %96, label %sw.epilog [
    i32 1, label %sw.bb154
    i32 2, label %sw.bb175
    i32 3, label %sw.bb200
  ]

sw.bb154:                                         ; preds = %if.end153
  %m_triangleVerticesA156 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %97 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA156, align 4
  %arrayidx157 = getelementptr inbounds %class.btVector3, %class.btVector3* %97, i32 0
  %m_triangleVerticesA158 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %98 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA158, align 4
  %arrayidx159 = getelementptr inbounds %class.btVector3, %class.btVector3* %98, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge155, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx157, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx159)
  %99 = load float, float* %correctedAngle, align 4
  %fneg161 = fneg float %99
  store float %fneg161, float* %ref.tmp160, align 4
  %call162 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %orn, %class.btVector3* nonnull align 4 dereferenceable(16) %edge155, float* nonnull align 4 dereferenceable(4) %ref.tmp160)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %computedNormalB, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA)
  %call163 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %computedNormalB, %class.btVector3* nonnull align 4 dereferenceable(16) %normalB)
  store float %call163, float* %bla, align 4
  %100 = load float, float* %bla, align 4
  %cmp164 = fcmp olt float %100, 0.000000e+00
  br i1 %cmp164, label %if.then165, label %if.end168

if.then165:                                       ; preds = %sw.bb154
  store float -1.000000e+00, float* %ref.tmp166, align 4
  %call167 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %computedNormalB, float* nonnull align 4 dereferenceable(4) %ref.tmp166)
  %101 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_flags = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %101, i32 0, i32 0
  %102 = load i32, i32* %m_flags, align 4
  %or = or i32 %102, 8
  store i32 %or, i32* %m_flags, align 4
  br label %if.end168

if.end168:                                        ; preds = %if.then165, %sw.bb154
  %103 = load float, float* %correctedAngle, align 4
  %fneg169 = fneg float %103
  %104 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV0V1Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %104, i32 0, i32 1
  store float %fneg169, float* %m_edgeV0V1Angle, align 4
  %105 = load i8, i8* %isConvex, align 1
  %tobool170 = trunc i8 %105 to i1
  br i1 %tobool170, label %if.then171, label %if.end174

if.then171:                                       ; preds = %if.end168
  %106 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_flags172 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %106, i32 0, i32 0
  %107 = load i32, i32* %m_flags172, align 4
  %or173 = or i32 %107, 1
  store i32 %or173, i32* %m_flags172, align 4
  br label %if.end174

if.end174:                                        ; preds = %if.then171, %if.end168
  br label %sw.epilog

sw.bb175:                                         ; preds = %if.end153
  %m_triangleVerticesA177 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %108 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA177, align 4
  %arrayidx178 = getelementptr inbounds %class.btVector3, %class.btVector3* %108, i32 2
  %m_triangleVerticesA179 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %109 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA179, align 4
  %arrayidx180 = getelementptr inbounds %class.btVector3, %class.btVector3* %109, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge176, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx178, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx180)
  %110 = load float, float* %correctedAngle, align 4
  %fneg183 = fneg float %110
  store float %fneg183, float* %ref.tmp182, align 4
  %call184 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %orn181, %class.btVector3* nonnull align 4 dereferenceable(16) %edge176, float* nonnull align 4 dereferenceable(4) %ref.tmp182)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %computedNormalB185, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn181, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA)
  %call186 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %computedNormalB185, %class.btVector3* nonnull align 4 dereferenceable(16) %normalB)
  %cmp187 = fcmp olt float %call186, 0.000000e+00
  br i1 %cmp187, label %if.then188, label %if.end193

if.then188:                                       ; preds = %sw.bb175
  store float -1.000000e+00, float* %ref.tmp189, align 4
  %call190 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %computedNormalB185, float* nonnull align 4 dereferenceable(4) %ref.tmp189)
  %111 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_flags191 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %111, i32 0, i32 0
  %112 = load i32, i32* %m_flags191, align 4
  %or192 = or i32 %112, 32
  store i32 %or192, i32* %m_flags191, align 4
  br label %if.end193

if.end193:                                        ; preds = %if.then188, %sw.bb175
  %113 = load float, float* %correctedAngle, align 4
  %fneg194 = fneg float %113
  %114 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV2V0Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %114, i32 0, i32 3
  store float %fneg194, float* %m_edgeV2V0Angle, align 4
  %115 = load i8, i8* %isConvex, align 1
  %tobool195 = trunc i8 %115 to i1
  br i1 %tobool195, label %if.then196, label %if.end199

if.then196:                                       ; preds = %if.end193
  %116 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_flags197 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %116, i32 0, i32 0
  %117 = load i32, i32* %m_flags197, align 4
  %or198 = or i32 %117, 4
  store i32 %or198, i32* %m_flags197, align 4
  br label %if.end199

if.end199:                                        ; preds = %if.then196, %if.end193
  br label %sw.epilog

sw.bb200:                                         ; preds = %if.end153
  %m_triangleVerticesA202 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %118 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA202, align 4
  %arrayidx203 = getelementptr inbounds %class.btVector3, %class.btVector3* %118, i32 1
  %m_triangleVerticesA204 = getelementptr inbounds %struct.btConnectivityProcessor, %struct.btConnectivityProcessor* %this1, i32 0, i32 3
  %119 = load %class.btVector3*, %class.btVector3** %m_triangleVerticesA204, align 4
  %arrayidx205 = getelementptr inbounds %class.btVector3, %class.btVector3* %119, i32 2
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge201, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx203, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx205)
  %120 = load float, float* %correctedAngle, align 4
  %fneg208 = fneg float %120
  store float %fneg208, float* %ref.tmp207, align 4
  %call209 = call %class.btQuaternion* @_ZN12btQuaternionC2ERK9btVector3RKf(%class.btQuaternion* %orn206, %class.btVector3* nonnull align 4 dereferenceable(16) %edge201, float* nonnull align 4 dereferenceable(4) %ref.tmp207)
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %computedNormalB210, %class.btQuaternion* nonnull align 4 dereferenceable(16) %orn206, %class.btVector3* nonnull align 4 dereferenceable(16) %normalA)
  %call211 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %computedNormalB210, %class.btVector3* nonnull align 4 dereferenceable(16) %normalB)
  %cmp212 = fcmp olt float %call211, 0.000000e+00
  br i1 %cmp212, label %if.then213, label %if.end218

if.then213:                                       ; preds = %sw.bb200
  %121 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_flags214 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %121, i32 0, i32 0
  %122 = load i32, i32* %m_flags214, align 4
  %or215 = or i32 %122, 16
  store i32 %or215, i32* %m_flags214, align 4
  store float -1.000000e+00, float* %ref.tmp216, align 4
  %call217 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %computedNormalB210, float* nonnull align 4 dereferenceable(4) %ref.tmp216)
  br label %if.end218

if.end218:                                        ; preds = %if.then213, %sw.bb200
  %123 = load float, float* %correctedAngle, align 4
  %fneg219 = fneg float %123
  %124 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_edgeV1V2Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %124, i32 0, i32 2
  store float %fneg219, float* %m_edgeV1V2Angle, align 4
  %125 = load i8, i8* %isConvex, align 1
  %tobool220 = trunc i8 %125 to i1
  br i1 %tobool220, label %if.then221, label %if.end224

if.then221:                                       ; preds = %if.end218
  %126 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %info, align 4
  %m_flags222 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %126, i32 0, i32 0
  %127 = load i32, i32* %m_flags222, align 4
  %or223 = or i32 %127, 2
  store i32 %or223, i32* %m_flags222, align 4
  br label %if.end224

if.end224:                                        ; preds = %if.then221, %if.end218
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end153, %if.end224, %if.end199, %if.end174
  %call225 = call %class.btTriangleShape* @_ZN15btTriangleShapeD2Ev(%class.btTriangleShape* %tB) #9
  %call226 = call %class.btTriangleShape* @_ZN15btTriangleShapeD2Ev(%class.btTriangleShape* %tA) #9
  br label %sw.epilog227

sw.default:                                       ; preds = %for.end52
  br label %sw.epilog227

sw.epilog227:                                     ; preds = %if.then, %if.then9, %if.then25, %if.then43, %if.then48, %sw.default, %sw.epilog, %sw.bb53, %sw.bb
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btTriangleInfo* @_ZN14btTriangleInfoC2Ev(%struct.btTriangleInfo* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btTriangleInfo*, align 4
  store %struct.btTriangleInfo* %this, %struct.btTriangleInfo** %this.addr, align 4
  %this1 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %this.addr, align 4
  %m_edgeV0V1Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %this1, i32 0, i32 1
  store float 0x401921FB60000000, float* %m_edgeV0V1Angle, align 4
  %m_edgeV1V2Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %this1, i32 0, i32 2
  store float 0x401921FB60000000, float* %m_edgeV1V2Angle, align 4
  %m_edgeV2V0Angle = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %this1, i32 0, i32 3
  store float 0x401921FB60000000, float* %m_edgeV2V0Angle, align 4
  %m_flags = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %this1, i32 0, i32 0
  store i32 0, i32* %m_flags, align 4
  ret %struct.btTriangleInfo* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btHashMapI9btHashInt14btTriangleInfoE6insertERKS0_RKS1_(%class.btHashMap* %this, %class.btHashInt* nonnull align 4 dereferenceable(4) %key, %struct.btTriangleInfo* nonnull align 4 dereferenceable(16) %value) #2 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %class.btHashInt*, align 4
  %value.addr = alloca %struct.btTriangleInfo*, align 4
  %hash = alloca i32, align 4
  %index = alloca i32, align 4
  %count = alloca i32, align 4
  %oldCapacity = alloca i32, align 4
  %newCapacity = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4
  store %class.btHashInt* %key, %class.btHashInt** %key.addr, align 4
  store %struct.btTriangleInfo* %value, %struct.btTriangleInfo** %value.addr, align 4
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4
  %call = call i32 @_ZNK9btHashInt7getHashEv(%class.btHashInt* %0)
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %m_valueArray)
  %sub = sub nsw i32 %call2, 1
  %and = and i32 %call, %sub
  store i32 %and, i32* %hash, align 4
  %1 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4
  %call3 = call i32 @_ZNK9btHashMapI9btHashInt14btTriangleInfoE9findIndexERKS0_(%class.btHashMap* %this1, %class.btHashInt* nonnull align 4 dereferenceable(4) %1)
  store i32 %call3, i32* %index, align 4
  %2 = load i32, i32* %index, align 4
  %cmp = icmp ne i32 %2, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %value.addr, align 4
  %m_valueArray4 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %4 = load i32, i32* %index, align 4
  %call5 = call nonnull align 4 dereferenceable(16) %struct.btTriangleInfo* @_ZN20btAlignedObjectArrayI14btTriangleInfoEixEi(%class.btAlignedObjectArray.12* %m_valueArray4, i32 %4)
  %5 = bitcast %struct.btTriangleInfo* %call5 to i8*
  %6 = bitcast %struct.btTriangleInfo* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %m_valueArray6 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4sizeEv(%class.btAlignedObjectArray.12* %m_valueArray6)
  store i32 %call7, i32* %count, align 4
  %m_valueArray8 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call9 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %m_valueArray8)
  store i32 %call9, i32* %oldCapacity, align 4
  %m_valueArray10 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %7 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %value.addr, align 4
  call void @_ZN20btAlignedObjectArrayI14btTriangleInfoE9push_backERKS0_(%class.btAlignedObjectArray.12* %m_valueArray10, %struct.btTriangleInfo* nonnull align 4 dereferenceable(16) %7)
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %8 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btHashIntE9push_backERKS0_(%class.btAlignedObjectArray.16* %m_keyArray, %class.btHashInt* nonnull align 4 dereferenceable(4) %8)
  %m_valueArray11 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call12 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %m_valueArray11)
  store i32 %call12, i32* %newCapacity, align 4
  %9 = load i32, i32* %oldCapacity, align 4
  %10 = load i32, i32* %newCapacity, align 4
  %cmp13 = icmp slt i32 %9, %10
  br i1 %cmp13, label %if.then14, label %if.end20

if.then14:                                        ; preds = %if.end
  %11 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4
  call void @_ZN9btHashMapI9btHashInt14btTriangleInfoE10growTablesERKS0_(%class.btHashMap* %this1, %class.btHashInt* nonnull align 4 dereferenceable(4) %11)
  %12 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4
  %call15 = call i32 @_ZNK9btHashInt7getHashEv(%class.btHashInt* %12)
  %m_valueArray16 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call17 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %m_valueArray16)
  %sub18 = sub nsw i32 %call17, 1
  %and19 = and i32 %call15, %sub18
  store i32 %and19, i32* %hash, align 4
  br label %if.end20

if.end20:                                         ; preds = %if.then14, %if.end
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %13 = load i32, i32* %hash, align 4
  %call21 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_hashTable, i32 %13)
  %14 = load i32, i32* %call21, align 4
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %15 = load i32, i32* %count, align 4
  %call22 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_next, i32 %15)
  store i32 %14, i32* %call22, align 4
  %16 = load i32, i32* %count, align 4
  %m_hashTable23 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %17 = load i32, i32* %hash, align 4
  %call24 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_hashTable23, i32 %17)
  store i32 %16, i32* %call24, align 4
  br label %return

return:                                           ; preds = %if.end20, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTriangleShape* @_ZN15btTriangleShapeC2ERK9btVector3S2_S2_(%class.btTriangleShape* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p0, %class.btVector3* nonnull align 4 dereferenceable(16) %p1, %class.btVector3* nonnull align 4 dereferenceable(16) %p2) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btTriangleShape*, align 4
  %this.addr = alloca %class.btTriangleShape*, align 4
  %p0.addr = alloca %class.btVector3*, align 4
  %p1.addr = alloca %class.btVector3*, align 4
  %p2.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  store %class.btVector3* %p0, %class.btVector3** %p0.addr, align 4
  store %class.btVector3* %p1, %class.btVector3** %p1.addr, align 4
  store %class.btVector3* %p2, %class.btVector3** %p2.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  store %class.btTriangleShape* %this1, %class.btTriangleShape** %retval, align 4
  %0 = bitcast %class.btTriangleShape* %this1 to %class.btPolyhedralConvexShape*
  %call = call %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeC2Ev(%class.btPolyhedralConvexShape* %0)
  %1 = bitcast %class.btTriangleShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [34 x i8*] }, { [34 x i8*] }* @_ZTV15btTriangleShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %2 = bitcast %class.btTriangleShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 1, i32* %m_shapeType, align 4
  %3 = load %class.btVector3*, %class.btVector3** %p0.addr, align 4
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 0
  %4 = bitcast %class.btVector3* %arrayidx to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4
  %m_vertices14 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices14, i32 0, i32 1
  %7 = bitcast %class.btVector3* %arrayidx5 to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btVector3*, %class.btVector3** %p2.addr, align 4
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 2
  %10 = bitcast %class.btVector3* %arrayidx7 to i8*
  %11 = bitcast %class.btVector3* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  %12 = load %class.btTriangleShape*, %class.btTriangleShape** %retval, align 4
  ret %class.btTriangleShape* %12
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTriangleShape* @_ZN15btTriangleShapeD2Ev(%class.btTriangleShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btTriangleShape* %this1 to %class.btPolyhedralConvexShape*
  %call = call %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeD2Ev(%class.btPolyhedralConvexShape* %0) #9
  ret %class.btTriangleShape* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btHashInt7getHashEv(%class.btHashInt* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btHashInt*, align 4
  %key = alloca i32, align 4
  store %class.btHashInt* %this, %class.btHashInt** %this.addr, align 4
  %this1 = load %class.btHashInt*, %class.btHashInt** %this.addr, align 4
  %m_uid = getelementptr inbounds %class.btHashInt, %class.btHashInt* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_uid, align 4
  store i32 %0, i32* %key, align 4
  %1 = load i32, i32* %key, align 4
  %shl = shl i32 %1, 15
  %neg = xor i32 %shl, -1
  %2 = load i32, i32* %key, align 4
  %add = add nsw i32 %2, %neg
  store i32 %add, i32* %key, align 4
  %3 = load i32, i32* %key, align 4
  %shr = ashr i32 %3, 10
  %4 = load i32, i32* %key, align 4
  %xor = xor i32 %4, %shr
  store i32 %xor, i32* %key, align 4
  %5 = load i32, i32* %key, align 4
  %shl2 = shl i32 %5, 3
  %6 = load i32, i32* %key, align 4
  %add3 = add nsw i32 %6, %shl2
  store i32 %add3, i32* %key, align 4
  %7 = load i32, i32* %key, align 4
  %shr4 = ashr i32 %7, 6
  %8 = load i32, i32* %key, align 4
  %xor5 = xor i32 %8, %shr4
  store i32 %xor5, i32* %key, align 4
  %9 = load i32, i32* %key, align 4
  %shl6 = shl i32 %9, 11
  %neg7 = xor i32 %shl6, -1
  %10 = load i32, i32* %key, align 4
  %add8 = add nsw i32 %10, %neg7
  store i32 %add8, i32* %key, align 4
  %11 = load i32, i32* %key, align 4
  %shr9 = ashr i32 %11, 16
  %12 = load i32, i32* %key, align 4
  %xor10 = xor i32 %12, %shr9
  store i32 %xor10, i32* %key, align 4
  %13 = load i32, i32* %key, align 4
  ret i32 %13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK9btHashMapI9btHashInt14btTriangleInfoE9findIndexERKS0_(%class.btHashMap* %this, %class.btHashInt* nonnull align 4 dereferenceable(4) %key) #2 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %class.btHashInt*, align 4
  %hash = alloca i32, align 4
  %index = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4
  store %class.btHashInt* %key, %class.btHashInt** %key.addr, align 4
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4
  %call = call i32 @_ZNK9btHashInt7getHashEv(%class.btHashInt* %0)
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %m_valueArray)
  %sub = sub nsw i32 %call2, 1
  %and = and i32 %call, %sub
  store i32 %and, i32* %hash, align 4
  %1 = load i32, i32* %hash, align 4
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %m_hashTable)
  %cmp = icmp uge i32 %1, %call3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %m_hashTable4 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %2 = load i32, i32* %hash, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_hashTable4, i32 %2)
  %3 = load i32, i32* %call5, align 4
  store i32 %3, i32* %index, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %4 = load i32, i32* %index, align 4
  %cmp6 = icmp ne i32 %4, -1
  br i1 %cmp6, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %5 = load %class.btHashInt*, %class.btHashInt** %key.addr, align 4
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %6 = load i32, i32* %index, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %class.btHashInt* @_ZNK20btAlignedObjectArrayI9btHashIntEixEi(%class.btAlignedObjectArray.16* %m_keyArray, i32 %6)
  %call8 = call zeroext i1 @_ZNK9btHashInt6equalsERKS_(%class.btHashInt* %5, %class.btHashInt* nonnull align 4 dereferenceable(4) %call7)
  %conv = zext i1 %call8 to i32
  %cmp9 = icmp eq i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %7 = phi i1 [ false, %while.cond ], [ %cmp9, %land.rhs ]
  br i1 %7, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %8 = load i32, i32* %index, align 4
  %call10 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_next, i32 %8)
  %9 = load i32, i32* %call10, align 4
  store i32 %9, i32* %index, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %10 = load i32, i32* %index, align 4
  store i32 %10, i32* %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.then
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btTriangleInfo* @_ZN20btAlignedObjectArrayI14btTriangleInfoEixEi(%class.btAlignedObjectArray.12* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %0, i32 %1
  ret %struct.btTriangleInfo* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4sizeEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btTriangleInfoE9push_backERKS0_(%class.btAlignedObjectArray.12* %this, %struct.btTriangleInfo* nonnull align 4 dereferenceable(16) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Val.addr = alloca %struct.btTriangleInfo*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store %struct.btTriangleInfo* %_Val, %struct.btTriangleInfo** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI14btTriangleInfoE9allocSizeEi(%class.btAlignedObjectArray.12* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI14btTriangleInfoE7reserveEi(%class.btAlignedObjectArray.12* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %1 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %1, i32 %2
  %3 = bitcast %struct.btTriangleInfo* %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.btTriangleInfo*
  %5 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %_Val.addr, align 4
  %6 = bitcast %struct.btTriangleInfo* %4 to i8*
  %7 = bitcast %struct.btTriangleInfo* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btHashIntE9push_backERKS0_(%class.btAlignedObjectArray.16* %this, %class.btHashInt* nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %_Val.addr = alloca %class.btHashInt*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store %class.btHashInt* %_Val, %class.btHashInt** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btHashIntE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btHashIntE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btHashIntE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btHashIntE9allocSizeEi(%class.btAlignedObjectArray.16* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btHashIntE7reserveEi(%class.btAlignedObjectArray.16* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %1 = load %class.btHashInt*, %class.btHashInt** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btHashInt, %class.btHashInt* %1, i32 %2
  %3 = bitcast %class.btHashInt* %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btHashInt*
  %5 = load %class.btHashInt*, %class.btHashInt** %_Val.addr, align 4
  %6 = bitcast %class.btHashInt* %4 to i8*
  %7 = bitcast %class.btHashInt* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 4, i1 false)
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btHashMapI9btHashInt14btTriangleInfoE10growTablesERKS0_(%class.btHashMap* %this, %class.btHashInt* nonnull align 4 dereferenceable(4) %0) #2 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  %.addr = alloca %class.btHashInt*, align 4
  %newCapacity = alloca i32, align 4
  %curHashtableSize = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  %i = alloca i32, align 4
  %hashValue = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4
  store %class.btHashInt* %0, %class.btHashInt** %.addr, align 4
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %m_valueArray)
  store i32 %call, i32* %newCapacity, align 4
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %m_hashTable)
  %1 = load i32, i32* %newCapacity, align 4
  %cmp = icmp slt i32 %call2, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_hashTable3 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %m_hashTable3)
  store i32 %call4, i32* %curHashtableSize, align 4
  %m_hashTable5 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %2 = load i32, i32* %newCapacity, align 4
  store i32 0, i32* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.8* %m_hashTable5, i32 %2, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %3 = load i32, i32* %newCapacity, align 4
  store i32 0, i32* %ref.tmp6, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.8* %m_next, i32 %3, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %4 = load i32, i32* %i, align 4
  %5 = load i32, i32* %newCapacity, align 4
  %cmp7 = icmp slt i32 %4, %5
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_hashTable8 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %6 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_hashTable8, i32 %6)
  store i32 -1, i32* %call9, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc15, %for.end
  %8 = load i32, i32* %i, align 4
  %9 = load i32, i32* %newCapacity, align 4
  %cmp11 = icmp slt i32 %8, %9
  br i1 %cmp11, label %for.body12, label %for.end17

for.body12:                                       ; preds = %for.cond10
  %m_next13 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %10 = load i32, i32* %i, align 4
  %call14 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_next13, i32 %10)
  store i32 -1, i32* %call14, align 4
  br label %for.inc15

for.inc15:                                        ; preds = %for.body12
  %11 = load i32, i32* %i, align 4
  %inc16 = add nsw i32 %11, 1
  store i32 %inc16, i32* %i, align 4
  br label %for.cond10

for.end17:                                        ; preds = %for.cond10
  store i32 0, i32* %i, align 4
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc31, %for.end17
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %curHashtableSize, align 4
  %cmp19 = icmp slt i32 %12, %13
  br i1 %cmp19, label %for.body20, label %for.end33

for.body20:                                       ; preds = %for.cond18
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %14 = load i32, i32* %i, align 4
  %call21 = call nonnull align 4 dereferenceable(4) %class.btHashInt* @_ZN20btAlignedObjectArrayI9btHashIntEixEi(%class.btAlignedObjectArray.16* %m_keyArray, i32 %14)
  %call22 = call i32 @_ZNK9btHashInt7getHashEv(%class.btHashInt* %call21)
  %m_valueArray23 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call24 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %m_valueArray23)
  %sub = sub nsw i32 %call24, 1
  %and = and i32 %call22, %sub
  store i32 %and, i32* %hashValue, align 4
  %m_hashTable25 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %15 = load i32, i32* %hashValue, align 4
  %call26 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_hashTable25, i32 %15)
  %16 = load i32, i32* %call26, align 4
  %m_next27 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %17 = load i32, i32* %i, align 4
  %call28 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_next27, i32 %17)
  store i32 %16, i32* %call28, align 4
  %18 = load i32, i32* %i, align 4
  %m_hashTable29 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %19 = load i32, i32* %hashValue, align 4
  %call30 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %m_hashTable29, i32 %19)
  store i32 %18, i32* %call30, align 4
  br label %for.inc31

for.inc31:                                        ; preds = %for.body20
  %20 = load i32, i32* %i, align 4
  %inc32 = add nsw i32 %20, 1
  store i32 %inc32, i32* %i, align 4
  br label %for.cond18

for.end33:                                        ; preds = %for.cond18
  br label %if.end

if.end:                                           ; preds = %for.end33, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK9btHashInt6equalsERKS_(%class.btHashInt* %this, %class.btHashInt* nonnull align 4 dereferenceable(4) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btHashInt*, align 4
  %other.addr = alloca %class.btHashInt*, align 4
  store %class.btHashInt* %this, %class.btHashInt** %this.addr, align 4
  store %class.btHashInt* %other, %class.btHashInt** %other.addr, align 4
  %this1 = load %class.btHashInt*, %class.btHashInt** %this.addr, align 4
  %call = call i32 @_ZNK9btHashInt7getUid1Ev(%class.btHashInt* %this1)
  %0 = load %class.btHashInt*, %class.btHashInt** %other.addr, align 4
  %call2 = call i32 @_ZNK9btHashInt7getUid1Ev(%class.btHashInt* %0)
  %cmp = icmp eq i32 %call, %call2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btHashInt* @_ZNK20btAlignedObjectArrayI9btHashIntEixEi(%class.btAlignedObjectArray.16* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %class.btHashInt*, %class.btHashInt** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btHashInt, %class.btHashInt* %0, i32 %1
  ret %class.btHashInt* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btHashInt7getUid1Ev(%class.btHashInt* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btHashInt*, align 4
  store %class.btHashInt* %this, %class.btHashInt** %this.addr, align 4
  %this1 = load %class.btHashInt*, %class.btHashInt** %this.addr, align 4
  %m_uid = getelementptr inbounds %class.btHashInt, %class.btHashInt* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_uid, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btTriangleInfoE7reserveEi(%class.btAlignedObjectArray.12* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btTriangleInfo*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI14btTriangleInfoE8allocateEi(%class.btAlignedObjectArray.12* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btTriangleInfo*
  store %struct.btTriangleInfo* %2, %struct.btTriangleInfo** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %3 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4copyEiiPS0_(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call3, %struct.btTriangleInfo* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI14btTriangleInfoE7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI14btTriangleInfoE10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.btTriangleInfo* %4, %struct.btTriangleInfo** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI14btTriangleInfoE9allocSizeEi(%class.btAlignedObjectArray.12* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI14btTriangleInfoE8allocateEi(%class.btAlignedObjectArray.12* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btTriangleInfo* @_ZN18btAlignedAllocatorI14btTriangleInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %m_allocator, i32 %1, %struct.btTriangleInfo** null)
  %2 = bitcast %struct.btTriangleInfo* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI14btTriangleInfoE4copyEiiPS0_(%class.btAlignedObjectArray.12* %this, i32 %start, i32 %end, %struct.btTriangleInfo* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btTriangleInfo*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btTriangleInfo* %dest, %struct.btTriangleInfo** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %3, i32 %4
  %5 = bitcast %struct.btTriangleInfo* %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btTriangleInfo*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %7 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %7, i32 %8
  %9 = bitcast %struct.btTriangleInfo* %6 to i8*
  %10 = bitcast %struct.btTriangleInfo* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btTriangleInfoE7destroyEii(%class.btAlignedObjectArray.12* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %3 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btTriangleInfo, %struct.btTriangleInfo* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btTriangleInfoE10deallocateEv(%class.btAlignedObjectArray.12* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %m_data, align 4
  %tobool = icmp ne %struct.btTriangleInfo* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI14btTriangleInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %m_allocator, %struct.btTriangleInfo* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.btTriangleInfo* null, %struct.btTriangleInfo** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btTriangleInfo* @_ZN18btAlignedAllocatorI14btTriangleInfoLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %this, i32 %n, %struct.btTriangleInfo** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btTriangleInfo**, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btTriangleInfo** %hint, %struct.btTriangleInfo*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btTriangleInfo*
  ret %struct.btTriangleInfo* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI14btTriangleInfoLj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %this, %struct.btTriangleInfo* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %ptr.addr = alloca %struct.btTriangleInfo*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  store %struct.btTriangleInfo* %ptr, %struct.btTriangleInfo** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load %struct.btTriangleInfo*, %struct.btTriangleInfo** %ptr.addr, align 4
  %1 = bitcast %struct.btTriangleInfo* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btHashIntE4sizeEv(%class.btAlignedObjectArray.16* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btHashIntE8capacityEv(%class.btAlignedObjectArray.16* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btHashIntE7reserveEi(%class.btAlignedObjectArray.16* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btHashInt*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btHashIntE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btHashIntE8allocateEi(%class.btAlignedObjectArray.16* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btHashInt*
  store %class.btHashInt* %2, %class.btHashInt** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btHashIntE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %3 = load %class.btHashInt*, %class.btHashInt** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btHashIntE4copyEiiPS0_(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call3, %class.btHashInt* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btHashIntE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayI9btHashIntE7destroyEii(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btHashIntE10deallocateEv(%class.btAlignedObjectArray.16* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btHashInt*, %class.btHashInt** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %class.btHashInt* %4, %class.btHashInt** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btHashIntE9allocSizeEi(%class.btAlignedObjectArray.16* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btHashIntE8allocateEi(%class.btAlignedObjectArray.16* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btHashInt* @_ZN18btAlignedAllocatorI9btHashIntLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.17* %m_allocator, i32 %1, %class.btHashInt** null)
  %2 = bitcast %class.btHashInt* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btHashIntE4copyEiiPS0_(%class.btAlignedObjectArray.16* %this, i32 %start, i32 %end, %class.btHashInt* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btHashInt*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btHashInt* %dest, %class.btHashInt** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btHashInt*, %class.btHashInt** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btHashInt, %class.btHashInt* %3, i32 %4
  %5 = bitcast %class.btHashInt* %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btHashInt*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %7 = load %class.btHashInt*, %class.btHashInt** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btHashInt, %class.btHashInt* %7, i32 %8
  %9 = bitcast %class.btHashInt* %6 to i8*
  %10 = bitcast %class.btHashInt* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 4, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btHashIntE7destroyEii(%class.btAlignedObjectArray.16* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %3 = load %class.btHashInt*, %class.btHashInt** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btHashInt, %class.btHashInt* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btHashIntE10deallocateEv(%class.btAlignedObjectArray.16* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %class.btHashInt*, %class.btHashInt** %m_data, align 4
  %tobool = icmp ne %class.btHashInt* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %2 = load %class.btHashInt*, %class.btHashInt** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btHashIntLj16EE10deallocateEPS0_(%class.btAlignedAllocator.17* %m_allocator, %class.btHashInt* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store %class.btHashInt* null, %class.btHashInt** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btHashInt* @_ZN18btAlignedAllocatorI9btHashIntLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.17* %this, i32 %n, %class.btHashInt** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btHashInt**, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btHashInt** %hint, %class.btHashInt*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btHashInt*
  ret %class.btHashInt* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btHashIntLj16EE10deallocateEPS0_(%class.btAlignedAllocator.17* %this, %class.btHashInt* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %ptr.addr = alloca %class.btHashInt*, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4
  store %class.btHashInt* %ptr, %class.btHashInt** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load %class.btHashInt*, %class.btHashInt** %ptr.addr, align 4
  %1 = bitcast %class.btHashInt* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.8* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store i32* %fillData, i32** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %5 = load i32*, i32** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %14 = load i32*, i32** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %14, i32 %15
  %16 = bitcast i32* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to i32*
  %18 = load i32*, i32** %fillData.addr, align 4
  %19 = load i32, i32* %18, align 4
  store i32 %19, i32* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btHashInt* @_ZN20btAlignedObjectArrayI9btHashIntEixEi(%class.btAlignedObjectArray.16* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load %class.btHashInt*, %class.btHashInt** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btHashInt, %class.btHashInt* %0, i32 %1
  ret %class.btHashInt* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i32*
  store i32* %2, i32** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %3 = load i32*, i32** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, i32* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i32*, i32** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i32* %4, i32** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.9* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i32* %dest, i32** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = bitcast i32* %arrayidx to i8*
  %6 = bitcast i8* %5 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx2, align 4
  store i32 %9, i32* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.9* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.9* %this, i32 %n, i32** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32** %hint, i32*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.9* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeC2Ev(%class.btPolyhedralConvexShape* returned) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btTriangleShapeD0Ev(%class.btTriangleShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %call = call %class.btTriangleShape* @_ZN15btTriangleShapeD2Ev(%class.btTriangleShape* %this1) #9
  %0 = bitcast %class.btTriangleShape* %this1 to i8*
  call void @_ZN15btTriangleShapedlEPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK15btTriangleShape7getAabbERK11btTransformR9btVector3S4_(%class.btTriangleShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = bitcast %class.btTriangleShape* %this1 to %class.btConvexInternalShape*
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %4 = bitcast %class.btConvexInternalShape* %0 to void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 20
  %5 = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %5(%class.btConvexInternalShape* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #5

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #5

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #5

declare void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape15getLocalScalingEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK15btTriangleShape21calculateLocalInertiaEfR9btVector3(%class.btTriangleShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  store float %mass, float* %mass.addr, align 4
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK15btTriangleShape7getNameEv(%class.btTriangleShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([9 x i8], [9 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %this, float %margin) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  store float %0, float* %m_collisionMargin, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK21btConvexInternalShape9getMarginEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 52
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexInternalShapeData*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btConvexInternalShapeData*
  store %struct.btConvexInternalShapeData* %1, %struct.btConvexInternalShapeData** %shapeData, align 4
  %2 = bitcast %class.btConvexInternalShape* %this1 to %class.btCollisionShape*
  %3 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_collisionShapeData = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %3, i32 0, i32 0
  %4 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %2, i8* %4, %class.btSerializer* %5)
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  %6 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_implicitShapeDimensions2 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %6, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_implicitShapeDimensions, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions2)
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  %7 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_localScaling3 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %7, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %8 = load float, float* %m_collisionMargin, align 4
  %9 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_collisionMargin4 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %9, i32 0, i32 3
  store float %8, float* %m_collisionMargin4, align 4
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.1, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #5

declare void @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3(%class.btVector3* sret align 4, %class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK15btTriangleShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %dir) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %dir.addr = alloca %class.btVector3*, align 4
  %dots = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  store %class.btVector3* %dir, %class.btVector3** %dir.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %m_vertices12 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices12, i32 0, i32 1
  %m_vertices14 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices14, i32 0, i32 2
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %dots, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5)
  %m_vertices16 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %dots)
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices16, i32 0, i32 %call
  %1 = bitcast %class.btVector3* %agg.result to i8*
  %2 = bitcast %class.btVector3* %arrayidx7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK15btTriangleShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btTriangleShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %dir = alloca %class.btVector3*, align 4
  %dots = alloca %class.btVector3, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4
  store i32 %numVectors, i32* %numVectors.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numVectors.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 %3
  store %class.btVector3* %arrayidx, %class.btVector3** %dir, align 4
  %4 = load %class.btVector3*, %class.btVector3** %dir, align 4
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx2 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %m_vertices13 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices13, i32 0, i32 1
  %m_vertices15 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices15, i32 0, i32 2
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %dots, %class.btVector3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6)
  %m_vertices17 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %call = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %dots)
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices17, i32 0, i32 %call
  %5 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx9 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  %7 = bitcast %class.btVector3* %arrayidx9 to i8*
  %8 = bitcast %class.btVector3* %arrayidx8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

declare void @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvexInternalShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK15btTriangleShape36getNumPreferredPenetrationDirectionsEv(%class.btTriangleShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK15btTriangleShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btTriangleShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %1 = load i32, i32* %index.addr, align 4
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float -1.000000e+00, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %2, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare zeroext i1 @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi(%class.btPolyhedralConvexShape*, i32) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK15btTriangleShape14getNumVerticesEv(%class.btTriangleShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK15btTriangleShape11getNumEdgesEv(%class.btTriangleShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK15btTriangleShape7getEdgeEiR9btVector3S1_(%class.btTriangleShape* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %i.addr = alloca i32, align 4
  %pa.addr = alloca %class.btVector3*, align 4
  %pb.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  store %class.btVector3* %pa, %class.btVector3** %pa.addr, align 4
  store %class.btVector3* %pb, %class.btVector3** %pb.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %pa.addr, align 4
  %2 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable, i64 27
  %3 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn, align 4
  call void %3(%class.btTriangleShape* %this1, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %4 = load i32, i32* %i.addr, align 4
  %add = add nsw i32 %4, 1
  %rem = srem i32 %add, 3
  %5 = load %class.btVector3*, %class.btVector3** %pb.addr, align 4
  %6 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*)***
  %vtable2 = load void (%class.btTriangleShape*, i32, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*)*** %6, align 4
  %vfn3 = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vtable2, i64 27
  %7 = load void (%class.btTriangleShape*, i32, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*)** %vfn3, align 4
  call void %7(%class.btTriangleShape* %this1, i32 %rem, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK15btTriangleShape9getVertexEiR9btVector3(%class.btTriangleShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %vert) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %index.addr = alloca i32, align 4
  %vert.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store %class.btVector3* %vert, %class.btVector3** %vert.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 %0
  %1 = load %class.btVector3*, %class.btVector3** %vert.addr, align 4
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK15btTriangleShape12getNumPlanesEv(%class.btTriangleShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  ret i32 1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK15btTriangleShape8getPlaneER9btVector3S1_i(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %planeSupport, i32 %i) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %planeNormal.addr = alloca %class.btVector3*, align 4
  %planeSupport.addr = alloca %class.btVector3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  store %class.btVector3* %planeNormal, %class.btVector3** %planeNormal.addr, align 4
  store %class.btVector3* %planeSupport, %class.btVector3** %planeSupport.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %planeNormal.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %planeSupport.addr, align 4
  %3 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vtable, i64 31
  %4 = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btTriangleShape* %this1, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK15btTriangleShape8isInsideERK9btVector3f(%class.btTriangleShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pt, float %tolerance) unnamed_addr #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btTriangleShape*, align 4
  %pt.addr = alloca %class.btVector3*, align 4
  %tolerance.addr = alloca float, align 4
  %normal = alloca %class.btVector3, align 4
  %dist = alloca float, align 4
  %planeconst = alloca float, align 4
  %i = alloca i32, align 4
  %pa = alloca %class.btVector3, align 4
  %pb = alloca %class.btVector3, align 4
  %edge = alloca %class.btVector3, align 4
  %edgeNormal = alloca %class.btVector3, align 4
  %dist9 = alloca float, align 4
  %edgeConst = alloca float, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  store %class.btVector3* %pt, %class.btVector3** %pt.addr, align 4
  store float %tolerance, float* %tolerance.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normal)
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %0 = load %class.btVector3*, %class.btVector3** %pt.addr, align 4
  %call2 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call2, float* %dist, align 4
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call3, float* %planeconst, align 4
  %1 = load float, float* %planeconst, align 4
  %2 = load float, float* %dist, align 4
  %sub = fsub float %2, %1
  store float %sub, float* %dist, align 4
  %3 = load float, float* %dist, align 4
  %4 = load float, float* %tolerance.addr, align 4
  %fneg = fneg float %4
  %cmp = fcmp oge float %3, %fneg
  br i1 %cmp, label %land.lhs.true, label %if.end16

land.lhs.true:                                    ; preds = %entry
  %5 = load float, float* %dist, align 4
  %6 = load float, float* %tolerance.addr, align 4
  %cmp4 = fcmp ole float %5, %6
  br i1 %cmp4, label %if.then, label %if.end16

if.then:                                          ; preds = %land.lhs.true
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %7 = load i32, i32* %i, align 4
  %cmp5 = icmp slt i32 %7, 3
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pa)
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pb)
  %8 = load i32, i32* %i, align 4
  %9 = bitcast %class.btTriangleShape* %this1 to void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*** %9, align 4
  %vfn = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vtable, i64 26
  %10 = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %10(%class.btTriangleShape* %this1, i32 %8, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %pb, %class.btVector3* nonnull align 4 dereferenceable(16) %pa)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %edgeNormal, %class.btVector3* %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %edgeNormal)
  %11 = load %class.btVector3*, %class.btVector3** %pt.addr, align 4
  %call10 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeNormal)
  store float %call10, float* %dist9, align 4
  %call11 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %edgeNormal)
  store float %call11, float* %edgeConst, align 4
  %12 = load float, float* %edgeConst, align 4
  %13 = load float, float* %dist9, align 4
  %sub12 = fsub float %13, %12
  store float %sub12, float* %dist9, align 4
  %14 = load float, float* %dist9, align 4
  %15 = load float, float* %tolerance.addr, align 4
  %fneg13 = fneg float %15
  %cmp14 = fcmp olt float %14, %fneg13
  br i1 %cmp14, label %if.then15, label %if.end

if.then15:                                        ; preds = %for.body
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %16 = load i32, i32* %i, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

if.end16:                                         ; preds = %land.lhs.true, %entry
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end16, %for.end, %if.then15
  %17 = load i1, i1* %retval, align 1
  ret i1 %17
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK15btTriangleShape16getPlaneEquationEiR9btVector3S1_(%class.btTriangleShape* %this, i32 %i, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %planeSupport) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %i.addr = alloca i32, align 4
  %planeNormal.addr = alloca %class.btVector3*, align 4
  %planeSupport.addr = alloca %class.btVector3*, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  store %class.btVector3* %planeNormal, %class.btVector3** %planeNormal.addr, align 4
  store %class.btVector3* %planeSupport, %class.btVector3** %planeSupport.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %planeNormal.addr, align 4
  call void @_ZNK15btTriangleShape10calcNormalER9btVector3(%class.btTriangleShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %planeSupport.addr, align 4
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btTriangleShapedlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: nounwind
declare %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeD2Ev(%class.btPolyhedralConvexShape* returned) unnamed_addr #6

; Function Attrs: nounwind
declare %class.btTriangleCallback* @_ZN18btTriangleCallbackD2Ev(%class.btTriangleCallback* returned) unnamed_addr #6

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z7btAtan2ff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %call = call float @atan2f(float %0, float %1) #11
  ret float %call
}

; Function Attrs: nounwind readnone
declare float @atan2f(float, float) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btQuaternion11setRotationERK9btVector3RKf(%class.btQuaternion* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, float* nonnull align 4 dereferenceable(4) %_angle) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %_angle.addr = alloca float*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4
  store float* %_angle, float** %_angle.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %0)
  store float %call, float* %d, align 4
  %1 = load float*, float** %_angle.addr, align 4
  %2 = load float, float* %1, align 4
  %mul = fmul float %2, 5.000000e-01
  %call2 = call float @_Z5btSinf(float %mul)
  %3 = load float, float* %d, align 4
  %div = fdiv float %call2, %3
  store float %div, float* %s, align 4
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %5 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %5)
  %6 = load float, float* %call3, align 4
  %7 = load float, float* %s, align 4
  %mul4 = fmul float %6, %7
  store float %mul4, float* %ref.tmp, align 4
  %8 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %8)
  %9 = load float, float* %call6, align 4
  %10 = load float, float* %s, align 4
  %mul7 = fmul float %9, %10
  store float %mul7, float* %ref.tmp5, align 4
  %11 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %11)
  %12 = load float, float* %call9, align 4
  %13 = load float, float* %s, align 4
  %mul10 = fmul float %12, %13
  store float %mul10, float* %ref.tmp8, align 4
  %14 = load float*, float** %_angle.addr, align 4
  %15 = load float, float* %14, align 4
  %mul12 = fmul float %15, 5.000000e-01
  %call13 = call float @_Z5btCosf(float %mul12)
  store float %call13, float* %ref.tmp11, align 4
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %4, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btSinf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btCosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #8

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #8

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %0)
  store float %call, float* %d, align 4
  %1 = load float, float* %d, align 4
  %div = fdiv float 2.000000e+00, %1
  store float %div, float* %s, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call2, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %4, %5
  store float %mul, float* %xs, align 4
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call3, align 4
  %9 = load float, float* %s, align 4
  %mul4 = fmul float %8, %9
  store float %mul4, float* %ys, align 4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load float, float* %s, align 4
  %mul6 = fmul float %12, %13
  store float %mul6, float* %zs, align 4
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %15)
  %16 = load float, float* %call7, align 4
  %17 = load float, float* %xs, align 4
  %mul8 = fmul float %16, %17
  store float %mul8, float* %wx, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load float, float* %ys, align 4
  %mul10 = fmul float %20, %21
  store float %mul10, float* %wy, align 4
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %23)
  %24 = load float, float* %call11, align 4
  %25 = load float, float* %zs, align 4
  %mul12 = fmul float %24, %25
  store float %mul12, float* %wz, align 4
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4
  %29 = load float, float* %xs, align 4
  %mul14 = fmul float %28, %29
  store float %mul14, float* %xx, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %31)
  %32 = load float, float* %call15, align 4
  %33 = load float, float* %ys, align 4
  %mul16 = fmul float %32, %33
  store float %mul16, float* %xy, align 4
  %34 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %35 = bitcast %class.btQuaternion* %34 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %35)
  %36 = load float, float* %call17, align 4
  %37 = load float, float* %zs, align 4
  %mul18 = fmul float %36, %37
  store float %mul18, float* %xz, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %39)
  %40 = load float, float* %call19, align 4
  %41 = load float, float* %ys, align 4
  %mul20 = fmul float %40, %41
  store float %mul20, float* %yy, align 4
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %43)
  %44 = load float, float* %call21, align 4
  %45 = load float, float* %zs, align 4
  %mul22 = fmul float %44, %45
  store float %mul22, float* %yz, align 4
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call23, align 4
  %49 = load float, float* %zs, align 4
  %mul24 = fmul float %48, %49
  store float %mul24, float* %zz, align 4
  %50 = load float, float* %yy, align 4
  %51 = load float, float* %zz, align 4
  %add = fadd float %50, %51
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4
  %52 = load float, float* %xy, align 4
  %53 = load float, float* %wz, align 4
  %sub26 = fsub float %52, %53
  store float %sub26, float* %ref.tmp25, align 4
  %54 = load float, float* %xz, align 4
  %55 = load float, float* %wy, align 4
  %add28 = fadd float %54, %55
  store float %add28, float* %ref.tmp27, align 4
  %56 = load float, float* %xy, align 4
  %57 = load float, float* %wz, align 4
  %add30 = fadd float %56, %57
  store float %add30, float* %ref.tmp29, align 4
  %58 = load float, float* %xx, align 4
  %59 = load float, float* %zz, align 4
  %add32 = fadd float %58, %59
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4
  %60 = load float, float* %yz, align 4
  %61 = load float, float* %wx, align 4
  %sub35 = fsub float %60, %61
  store float %sub35, float* %ref.tmp34, align 4
  %62 = load float, float* %xz, align 4
  %63 = load float, float* %wy, align 4
  %sub37 = fsub float %62, %63
  store float %sub37, float* %ref.tmp36, align 4
  %64 = load float, float* %yz, align 4
  %65 = load float, float* %wx, align 4
  %add39 = fadd float %64, %65
  store float %add39, float* %ref.tmp38, align 4
  %66 = load float, float* %xx, align 4
  %67 = load float, float* %yy, align 4
  %add41 = fadd float %66, %67
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #8

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #8

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %w) #2 comdat {
entry:
  %q.addr = alloca %class.btQuaternion*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %3)
  %4 = load float, float* %call1, align 4
  %mul = fmul float %2, %4
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4
  %8 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %8)
  %9 = load float, float* %call3, align 4
  %mul4 = fmul float %7, %9
  %add = fadd float %mul, %mul4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %13)
  %14 = load float, float* %call6, align 4
  %mul7 = fmul float %12, %14
  %sub = fsub float %add, %mul7
  store float %sub, float* %ref.tmp, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %16)
  %17 = load float, float* %call9, align 4
  %18 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %18)
  %19 = load float, float* %call10, align 4
  %mul11 = fmul float %17, %19
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %21)
  %22 = load float, float* %call12, align 4
  %23 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %23)
  %24 = load float, float* %call13, align 4
  %mul14 = fmul float %22, %24
  %add15 = fadd float %mul11, %mul14
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %26)
  %27 = load float, float* %call16, align 4
  %28 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %28)
  %29 = load float, float* %call17, align 4
  %mul18 = fmul float %27, %29
  %sub19 = fsub float %add15, %mul18
  store float %sub19, float* %ref.tmp8, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call21, align 4
  %33 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %33)
  %34 = load float, float* %call22, align 4
  %mul23 = fmul float %32, %34
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call24, align 4
  %38 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %38)
  %39 = load float, float* %call25, align 4
  %mul26 = fmul float %37, %39
  %add27 = fadd float %mul23, %mul26
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %41)
  %42 = load float, float* %call28, align 4
  %43 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %43)
  %44 = load float, float* %call29, align 4
  %mul30 = fmul float %42, %44
  %sub31 = fsub float %add27, %mul30
  store float %sub31, float* %ref.tmp20, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call33, align 4
  %fneg = fneg float %47
  %48 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %48)
  %49 = load float, float* %call34, align 4
  %mul35 = fmul float %fneg, %49
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call36, align 4
  %53 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %53)
  %54 = load float, float* %call37, align 4
  %mul38 = fmul float %52, %54
  %sub39 = fsub float %mul35, %mul38
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %56)
  %57 = load float, float* %call40, align 4
  %58 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %58)
  %59 = load float, float* %call41, align 4
  %mul42 = fmul float %57, %59
  %sub43 = fsub float %sub39, %mul42
  store float %sub43, float* %ref.tmp32, align 4
  %call44 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats3 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %3 = load float, float* %arrayidx4, align 4
  %fneg5 = fneg float %3
  store float %fneg5, float* %ref.tmp2, align 4
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %4, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %5 = load float, float* %arrayidx8, align 4
  %fneg9 = fneg float %5
  store float %fneg9, float* %ref.tmp6, align 4
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats10 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 3
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp58 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %2 = load float, float* %arrayidx, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call, align 4
  %mul = fmul float %2, %5
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %7 = load float, float* %arrayidx3, align 4
  %8 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %9 = bitcast %class.btQuaternion* %8 to %class.btQuadWord*
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %9, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 3
  %10 = load float, float* %arrayidx5, align 4
  %mul6 = fmul float %7, %10
  %add = fadd float %mul, %mul6
  %11 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %11, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %12 = load float, float* %arrayidx8, align 4
  %13 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %14 = bitcast %class.btQuaternion* %13 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %14)
  %15 = load float, float* %call9, align 4
  %mul10 = fmul float %12, %15
  %add11 = fadd float %add, %mul10
  %16 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats12 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %16, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %17 = load float, float* %arrayidx13, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %19)
  %20 = load float, float* %call14, align 4
  %mul15 = fmul float %17, %20
  %sub = fsub float %add11, %mul15
  store float %sub, float* %ref.tmp, align 4
  %21 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats17 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %21, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 3
  %22 = load float, float* %arrayidx18, align 4
  %23 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %24 = bitcast %class.btQuaternion* %23 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %24)
  %25 = load float, float* %call19, align 4
  %mul20 = fmul float %22, %25
  %26 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats21 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %26, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 1
  %27 = load float, float* %arrayidx22, align 4
  %28 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %29 = bitcast %class.btQuaternion* %28 to %class.btQuadWord*
  %m_floats23 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %29, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 3
  %30 = load float, float* %arrayidx24, align 4
  %mul25 = fmul float %27, %30
  %add26 = fadd float %mul20, %mul25
  %31 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats27 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %31, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 2
  %32 = load float, float* %arrayidx28, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %34)
  %35 = load float, float* %call29, align 4
  %mul30 = fmul float %32, %35
  %add31 = fadd float %add26, %mul30
  %36 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats32 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %36, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [4 x float], [4 x float]* %m_floats32, i32 0, i32 0
  %37 = load float, float* %arrayidx33, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %39)
  %40 = load float, float* %call34, align 4
  %mul35 = fmul float %37, %40
  %sub36 = fsub float %add31, %mul35
  store float %sub36, float* %ref.tmp16, align 4
  %41 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats38 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %41, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [4 x float], [4 x float]* %m_floats38, i32 0, i32 3
  %42 = load float, float* %arrayidx39, align 4
  %43 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %44 = bitcast %class.btQuaternion* %43 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %44)
  %45 = load float, float* %call40, align 4
  %mul41 = fmul float %42, %45
  %46 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats42 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %46, i32 0, i32 0
  %arrayidx43 = getelementptr inbounds [4 x float], [4 x float]* %m_floats42, i32 0, i32 2
  %47 = load float, float* %arrayidx43, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %m_floats44 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %49, i32 0, i32 0
  %arrayidx45 = getelementptr inbounds [4 x float], [4 x float]* %m_floats44, i32 0, i32 3
  %50 = load float, float* %arrayidx45, align 4
  %mul46 = fmul float %47, %50
  %add47 = fadd float %mul41, %mul46
  %51 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats48 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %51, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [4 x float], [4 x float]* %m_floats48, i32 0, i32 0
  %52 = load float, float* %arrayidx49, align 4
  %53 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %54 = bitcast %class.btQuaternion* %53 to %class.btQuadWord*
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %54)
  %55 = load float, float* %call50, align 4
  %mul51 = fmul float %52, %55
  %add52 = fadd float %add47, %mul51
  %56 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats53 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %56, i32 0, i32 0
  %arrayidx54 = getelementptr inbounds [4 x float], [4 x float]* %m_floats53, i32 0, i32 1
  %57 = load float, float* %arrayidx54, align 4
  %58 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %59 = bitcast %class.btQuaternion* %58 to %class.btQuadWord*
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %59)
  %60 = load float, float* %call55, align 4
  %mul56 = fmul float %57, %60
  %sub57 = fsub float %add52, %mul56
  store float %sub57, float* %ref.tmp37, align 4
  %61 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats59 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %61, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [4 x float], [4 x float]* %m_floats59, i32 0, i32 3
  %62 = load float, float* %arrayidx60, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %m_floats61 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %64, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [4 x float], [4 x float]* %m_floats61, i32 0, i32 3
  %65 = load float, float* %arrayidx62, align 4
  %mul63 = fmul float %62, %65
  %66 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats64 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %66, i32 0, i32 0
  %arrayidx65 = getelementptr inbounds [4 x float], [4 x float]* %m_floats64, i32 0, i32 0
  %67 = load float, float* %arrayidx65, align 4
  %68 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %69 = bitcast %class.btQuaternion* %68 to %class.btQuadWord*
  %call66 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %69)
  %70 = load float, float* %call66, align 4
  %mul67 = fmul float %67, %70
  %sub68 = fsub float %mul63, %mul67
  %71 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats69 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %71, i32 0, i32 0
  %arrayidx70 = getelementptr inbounds [4 x float], [4 x float]* %m_floats69, i32 0, i32 1
  %72 = load float, float* %arrayidx70, align 4
  %73 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %74 = bitcast %class.btQuaternion* %73 to %class.btQuadWord*
  %call71 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %74)
  %75 = load float, float* %call71, align 4
  %mul72 = fmul float %72, %75
  %sub73 = fsub float %sub68, %mul72
  %76 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats74 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %76, i32 0, i32 0
  %arrayidx75 = getelementptr inbounds [4 x float], [4 x float]* %m_floats74, i32 0, i32 2
  %77 = load float, float* %arrayidx75, align 4
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %79)
  %80 = load float, float* %call76, align 4
  %mul77 = fmul float %77, %80
  %sub78 = fsub float %sub73, %mul77
  store float %sub78, float* %ref.tmp58, align 4
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp58)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float*, float** %_z.addr, align 4
  %4 = load float*, float** %_w.addr, align 4
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btInternalEdgeUtility.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind readnone speculatable willreturn }
attributes #9 = { nounwind }
attributes #10 = { builtin nounwind }
attributes #11 = { nounwind readnone }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
