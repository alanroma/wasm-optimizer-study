; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btSliderConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btSliderConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btSliderConstraint = type { %class.btTypedConstraint, i8, i8, %class.btTransform, %class.btTransform, i8, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, i8, i8, i32, [3 x %class.btJacobianEntry], [3 x float], [3 x %class.btJacobianEntry], float, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, i8, float, float, float, i8, float, float, float }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.0, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.0 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btJacobianEntry = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.1, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%"struct.btTypedConstraint::btConstraintInfo2" = type { float, float, float*, float*, float*, float*, i32, float*, float*, float*, float*, i32, float }
%class.btAlignedObjectArray.5 = type opaque
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btSerializer = type opaque
%struct.btSliderConstraintData = type { %struct.btTypedConstraintData, %struct.btTransformFloatData, %struct.btTransformFloatData, float, float, float, float, i32, i32 }
%struct.btTypedConstraintData = type { %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData*, i8*, i32, i32, i32, i32, float, float, i32, i32, float, i32 }
%struct.btRigidBodyFloatData = type { %struct.btCollisionObjectFloatData, %struct.btMatrix3x3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, i32 }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type opaque
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btVector3FloatData = type { [4 x float] }
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZNK11btRigidBody24getCenterOfMassTransformEv = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN15btJacobianEntryC2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN18btSliderConstraint16getSolveLinLimitEv = comdat any

$_ZN18btSliderConstraint18getPoweredLinMotorEv = comdat any

$_ZN18btSliderConstraint16getSolveAngLimitEv = comdat any

$_ZN18btSliderConstraint18getPoweredAngMotorEv = comdat any

$_ZNK11btRigidBody17getLinearVelocityEv = comdat any

$_ZNK11btRigidBody10getInvMassEv = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x39getColumnEi = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_Z7btAtan2ff = comdat any

$_Z21btAdjustAngleToLimitsfff = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZNK18btSliderConstraint23getCalculatedTransformAEv = comdat any

$_ZNK18btSliderConstraint23getCalculatedTransformBEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_ = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN18btSliderConstraint11getLinDepthEv = comdat any

$_ZN18btSliderConstraint16getLowerLinLimitEv = comdat any

$_ZN18btSliderConstraint16getUpperLinLimitEv = comdat any

$_ZN18btSliderConstraint25getTargetLinMotorVelocityEv = comdat any

$_ZN18btSliderConstraint19getMaxLinMotorForceEv = comdat any

$_Z6btFabsf = comdat any

$_ZN18btSliderConstraint16getDampingLimLinEv = comdat any

$_ZN18btSliderConstraint17getSoftnessLimLinEv = comdat any

$_ZN18btSliderConstraint11getAngDepthEv = comdat any

$_ZN18btSliderConstraint16getLowerAngLimitEv = comdat any

$_ZN18btSliderConstraint16getUpperAngLimitEv = comdat any

$_ZN18btSliderConstraint25getTargetAngMotorVelocityEv = comdat any

$_ZN18btSliderConstraint19getMaxAngMotorForceEv = comdat any

$_ZN18btSliderConstraint16getDampingLimAngEv = comdat any

$_ZNK11btRigidBody18getAngularVelocityEv = comdat any

$_ZN18btSliderConstraint17getSoftnessLimAngEv = comdat any

$_ZN18btSliderConstraintD2Ev = comdat any

$_ZN18btSliderConstraintD0Ev = comdat any

$_ZN17btTypedConstraint13buildJacobianEv = comdat any

$_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif = comdat any

$_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f = comdat any

$_ZNK18btSliderConstraint28calculateSerializeBufferSizeEv = comdat any

$_ZNK18btSliderConstraint9serializeEPvP12btSerializer = comdat any

$_ZNK18btSliderConstraint8getFlagsEv = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_Z16btNormalizeAnglef = comdat any

$_Z6btFmodff = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN17btTypedConstraintD2Ev = comdat any

$_ZN18btSliderConstraintdlEPv = comdat any

$_ZNK11btTransform9serializeER20btTransformFloatData = comdat any

$_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV18btSliderConstraint = hidden unnamed_addr constant { [14 x i8*] } { [14 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI18btSliderConstraint to i8*), i8* bitcast (%class.btSliderConstraint* (%class.btSliderConstraint*)* @_ZN18btSliderConstraintD2Ev to i8*), i8* bitcast (void (%class.btSliderConstraint*)* @_ZN18btSliderConstraintD0Ev to i8*), i8* bitcast (void (%class.btTypedConstraint*)* @_ZN17btTypedConstraint13buildJacobianEv to i8*), i8* bitcast (void (%class.btTypedConstraint*, %class.btAlignedObjectArray.5*, i32, i32, float)* @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif to i8*), i8* bitcast (void (%class.btSliderConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)* @_ZN18btSliderConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E to i8*), i8* bitcast (void (%class.btSliderConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)* @_ZN18btSliderConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E to i8*), i8* bitcast (void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)* @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f to i8*), i8* bitcast (void (%class.btSliderConstraint*, i32, float, i32)* @_ZN18btSliderConstraint8setParamEifi to i8*), i8* bitcast (float (%class.btSliderConstraint*, i32, i32)* @_ZNK18btSliderConstraint8getParamEii to i8*), i8* bitcast (i32 (%class.btSliderConstraint*)* @_ZNK18btSliderConstraint28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btSliderConstraint*, i8*, %class.btSerializer*)* @_ZNK18btSliderConstraint9serializeEPvP12btSerializer to i8*), i8* bitcast (i32 (%class.btSliderConstraint*)* @_ZNK18btSliderConstraint8getFlagsEv to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS18btSliderConstraint = hidden constant [21 x i8] c"18btSliderConstraint\00", align 1
@_ZTI17btTypedConstraint = external constant i8*
@_ZTI18btSliderConstraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @_ZTS18btSliderConstraint, i32 0, i32 0), i8* bitcast (i8** @_ZTI17btTypedConstraint to i8*) }, align 4
@_ZTV17btTypedConstraint = external unnamed_addr constant { [13 x i8*] }, align 4
@.str = private unnamed_addr constant [23 x i8] c"btSliderConstraintData\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btSliderConstraint.cpp, i8* null }]

@_ZN18btSliderConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b = hidden unnamed_addr alias %class.btSliderConstraint* (%class.btSliderConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*, i1), %class.btSliderConstraint* (%class.btSliderConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*, i1)* @_ZN18btSliderConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
@_ZN18btSliderConstraintC1ER11btRigidBodyRK11btTransformb = hidden unnamed_addr alias %class.btSliderConstraint* (%class.btSliderConstraint*, %class.btRigidBody*, %class.btTransform*, i1), %class.btSliderConstraint* (%class.btSliderConstraint*, %class.btRigidBody*, %class.btTransform*, i1)* @_ZN18btSliderConstraintC2ER11btRigidBodyRK11btTransformb

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN18btSliderConstraint10initParamsEv(%class.btSliderConstraint* %this) #2 {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_lowerLinLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 6
  store float 1.000000e+00, float* %m_lowerLinLimit, align 4
  %m_upperLinLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 7
  store float -1.000000e+00, float* %m_upperLinLimit, align 4
  %m_lowerAngLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 8
  store float 0.000000e+00, float* %m_lowerAngLimit, align 4
  %m_upperAngLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 9
  store float 0.000000e+00, float* %m_upperAngLimit, align 4
  %m_softnessDirLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 10
  store float 1.000000e+00, float* %m_softnessDirLin, align 4
  %m_restitutionDirLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 11
  store float 0x3FE6666660000000, float* %m_restitutionDirLin, align 4
  %m_dampingDirLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 12
  store float 0.000000e+00, float* %m_dampingDirLin, align 4
  %m_cfmDirLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 13
  store float 0.000000e+00, float* %m_cfmDirLin, align 4
  %m_softnessDirAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 14
  store float 1.000000e+00, float* %m_softnessDirAng, align 4
  %m_restitutionDirAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 15
  store float 0x3FE6666660000000, float* %m_restitutionDirAng, align 4
  %m_dampingDirAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 16
  store float 0.000000e+00, float* %m_dampingDirAng, align 4
  %m_cfmDirAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_cfmDirAng, align 4
  %m_softnessOrthoLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 26
  store float 1.000000e+00, float* %m_softnessOrthoLin, align 4
  %m_restitutionOrthoLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 27
  store float 0x3FE6666660000000, float* %m_restitutionOrthoLin, align 4
  %m_dampingOrthoLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 28
  store float 1.000000e+00, float* %m_dampingOrthoLin, align 4
  %m_cfmOrthoLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 29
  store float 0.000000e+00, float* %m_cfmOrthoLin, align 4
  %m_softnessOrthoAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 30
  store float 1.000000e+00, float* %m_softnessOrthoAng, align 4
  %m_restitutionOrthoAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 31
  store float 0x3FE6666660000000, float* %m_restitutionOrthoAng, align 4
  %m_dampingOrthoAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 32
  store float 1.000000e+00, float* %m_dampingOrthoAng, align 4
  %m_cfmOrthoAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 33
  store float 0.000000e+00, float* %m_cfmOrthoAng, align 4
  %m_softnessLimLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 18
  store float 1.000000e+00, float* %m_softnessLimLin, align 4
  %m_restitutionLimLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 19
  store float 0x3FE6666660000000, float* %m_restitutionLimLin, align 4
  %m_dampingLimLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 20
  store float 1.000000e+00, float* %m_dampingLimLin, align 4
  %m_cfmLimLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 21
  store float 0.000000e+00, float* %m_cfmLimLin, align 4
  %m_softnessLimAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 22
  store float 1.000000e+00, float* %m_softnessLimAng, align 4
  %m_restitutionLimAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 23
  store float 0x3FE6666660000000, float* %m_restitutionLimAng, align 4
  %m_dampingLimAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 24
  store float 1.000000e+00, float* %m_dampingLimAng, align 4
  %m_cfmLimAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 25
  store float 0.000000e+00, float* %m_cfmLimAng, align 4
  %m_poweredLinMotor = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 55
  store i8 0, i8* %m_poweredLinMotor, align 4
  %m_targetLinMotorVelocity = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 56
  store float 0.000000e+00, float* %m_targetLinMotorVelocity, align 4
  %m_maxLinMotorForce = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 57
  store float 0.000000e+00, float* %m_maxLinMotorForce, align 4
  %m_accumulatedLinMotorImpulse = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 58
  store float 0.000000e+00, float* %m_accumulatedLinMotorImpulse, align 4
  %m_poweredAngMotor = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 59
  store i8 0, i8* %m_poweredAngMotor, align 4
  %m_targetAngMotorVelocity = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 60
  store float 0.000000e+00, float* %m_targetAngMotorVelocity, align 4
  %m_maxAngMotorForce = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 61
  store float 0.000000e+00, float* %m_maxAngMotorForce, align 4
  %m_accumulatedAngMotorImpulse = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 62
  store float 0.000000e+00, float* %m_accumulatedAngMotorImpulse, align 4
  %m_flags = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  store i32 0, i32* %m_flags, align 4
  %m_flags2 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  store i32 0, i32* %m_flags2, align 4
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 2
  store i8 1, i8* %m_useOffsetForConstraintFrame, align 1
  %0 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %0, i32 0, i32 8
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %1)
  %2 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %2, i32 0, i32 9
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %3)
  call void @_ZN18btSliderConstraint19calculateTransformsERK11btTransformS2_(%class.btSliderConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call3)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN18btSliderConstraint19calculateTransformsERK11btTransformS2_(%class.btSliderConstraint* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB) #2 {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  %ref.tmp3 = alloca %class.btTransform, align 4
  %ref.tmp5 = alloca %class.btTransform, align 4
  %ref.tmp9 = alloca %class.btTransform, align 4
  %ref.tmp17 = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca %class.btVector3, align 4
  %ref.tmp38 = alloca float, align 4
  %normalWorld = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %ref.tmp44 = alloca %class.btVector3, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_useLinearReferenceFrameA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 5
  %0 = load i8, i8* %m_useLinearReferenceFrameA, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 1
  %1 = load i8, i8* %m_useSolveConstraintObsolete, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.else, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  %2 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %m_frameInA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %m_frameInA)
  %m_calculatedTransformA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 41
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_calculatedTransformA, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %3 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %m_frameInB = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp3, %class.btTransform* %3, %class.btTransform* nonnull align 4 dereferenceable(64) %m_frameInB)
  %m_calculatedTransformB = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 42
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_calculatedTransformB, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp3)
  br label %if.end

if.else:                                          ; preds = %lor.lhs.false
  %4 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %m_frameInB6 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp5, %class.btTransform* %4, %class.btTransform* nonnull align 4 dereferenceable(64) %m_frameInB6)
  %m_calculatedTransformA7 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 41
  %call8 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_calculatedTransformA7, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp5)
  %5 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %m_frameInA10 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp9, %class.btTransform* %5, %class.btTransform* nonnull align 4 dereferenceable(64) %m_frameInA10)
  %m_calculatedTransformB11 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 42
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_calculatedTransformB11, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp9)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %m_calculatedTransformA13 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 41
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_calculatedTransformA13)
  %m_realPivotAInW = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 44
  %6 = bitcast %class.btVector3* %m_realPivotAInW to i8*
  %7 = bitcast %class.btVector3* %call14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_calculatedTransformB15 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 42
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_calculatedTransformB15)
  %m_realPivotBInW = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 45
  %8 = bitcast %class.btVector3* %m_realPivotBInW to i8*
  %9 = bitcast %class.btVector3* %call16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %m_calculatedTransformA18 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 41
  %call19 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA18)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp17, %class.btMatrix3x3* %call19, i32 0)
  %m_sliderAxis = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 43
  %10 = bitcast %class.btVector3* %m_sliderAxis to i8*
  %11 = bitcast %class.btVector3* %ref.tmp17 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  %m_useLinearReferenceFrameA20 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 5
  %12 = load i8, i8* %m_useLinearReferenceFrameA20, align 4
  %tobool21 = trunc i8 %12 to i1
  br i1 %tobool21, label %if.then25, label %lor.lhs.false22

lor.lhs.false22:                                  ; preds = %if.end
  %m_useSolveConstraintObsolete23 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 1
  %13 = load i8, i8* %m_useSolveConstraintObsolete23, align 4
  %tobool24 = trunc i8 %13 to i1
  br i1 %tobool24, label %if.then25, label %if.else29

if.then25:                                        ; preds = %lor.lhs.false22, %if.end
  %m_realPivotBInW27 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 45
  %m_realPivotAInW28 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 44
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp26, %class.btVector3* nonnull align 4 dereferenceable(16) %m_realPivotBInW27, %class.btVector3* nonnull align 4 dereferenceable(16) %m_realPivotAInW28)
  %m_delta = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 47
  %14 = bitcast %class.btVector3* %m_delta to i8*
  %15 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false)
  br label %if.end34

if.else29:                                        ; preds = %lor.lhs.false22
  %m_realPivotAInW31 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 44
  %m_realPivotBInW32 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 45
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %m_realPivotAInW31, %class.btVector3* nonnull align 4 dereferenceable(16) %m_realPivotBInW32)
  %m_delta33 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 47
  %16 = bitcast %class.btVector3* %m_delta33 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  br label %if.end34

if.end34:                                         ; preds = %if.else29, %if.then25
  %m_realPivotAInW36 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 44
  %m_sliderAxis39 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 43
  %m_delta40 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 47
  %call41 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_sliderAxis39, %class.btVector3* nonnull align 4 dereferenceable(16) %m_delta40)
  store float %call41, float* %ref.tmp38, align 4
  %m_sliderAxis42 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 43
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp38, %class.btVector3* nonnull align 4 dereferenceable(16) %m_sliderAxis42)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %m_realPivotAInW36, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp37)
  %m_projPivotInW = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 46
  %18 = bitcast %class.btVector3* %m_projPivotInW to i8*
  %19 = bitcast %class.btVector3* %ref.tmp35 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false)
  %call43 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normalWorld)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end34
  %20 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %20, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_calculatedTransformA45 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 41
  %call46 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA45)
  %21 = load i32, i32* %i, align 4
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp44, %class.btMatrix3x3* %call46, i32 %21)
  %22 = bitcast %class.btVector3* %normalWorld to i8*
  %23 = bitcast %class.btVector3* %ref.tmp44 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false)
  %m_delta47 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 47
  %call48 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_delta47, %class.btVector3* nonnull align 4 dereferenceable(16) %normalWorld)
  %m_depth = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 48
  %call49 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_depth)
  %24 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call49, i32 %24
  store float %call48, float* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %25 = load i32, i32* %i, align 4
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline optnone
define hidden %class.btSliderConstraint* @_ZN18btSliderConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b(%class.btSliderConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbA, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbB, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInA, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInB, i1 zeroext %useLinearReferenceFrameA) unnamed_addr #2 {
entry:
  %retval = alloca %class.btSliderConstraint*, align 4
  %this.addr = alloca %class.btSliderConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %frameInA.addr = alloca %class.btTransform*, align 4
  %frameInB.addr = alloca %class.btTransform*, align 4
  %useLinearReferenceFrameA.addr = alloca i8, align 1
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4
  store %class.btTransform* %frameInA, %class.btTransform** %frameInA.addr, align 4
  store %class.btTransform* %frameInB, %class.btTransform** %frameInB.addr, align 4
  %frombool = zext i1 %useLinearReferenceFrameA to i8
  store i8 %frombool, i8* %useLinearReferenceFrameA.addr, align 1
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  store %class.btSliderConstraint* %this1, %class.btSliderConstraint** %retval, align 4
  %0 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %2 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* %0, i32 7, %class.btRigidBody* nonnull align 4 dereferenceable(676) %1, %class.btRigidBody* nonnull align 4 dereferenceable(676) %2)
  %3 = bitcast %class.btSliderConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTV18btSliderConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 1
  store i8 0, i8* %m_useSolveConstraintObsolete, align 4
  %m_frameInA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 3
  %4 = load %class.btTransform*, %class.btTransform** %frameInA.addr, align 4
  %call2 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_frameInA, %class.btTransform* nonnull align 4 dereferenceable(64) %4)
  %m_frameInB = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 4
  %5 = load %class.btTransform*, %class.btTransform** %frameInB.addr, align 4
  %call3 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_frameInB, %class.btTransform* nonnull align 4 dereferenceable(64) %5)
  %m_useLinearReferenceFrameA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 5
  %6 = load i8, i8* %useLinearReferenceFrameA.addr, align 1
  %tobool = trunc i8 %6 to i1
  %frombool4 = zext i1 %tobool to i8
  store i8 %frombool4, i8* %m_useLinearReferenceFrameA, align 4
  %m_jacLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 37
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacLin, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call5 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_jacAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 39
  %array.begin6 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %arrayctor.end7 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin6, i32 3
  br label %arrayctor.loop8

arrayctor.loop8:                                  ; preds = %arrayctor.loop8, %arrayctor.cont
  %arrayctor.cur9 = phi %class.btJacobianEntry* [ %array.begin6, %arrayctor.cont ], [ %arrayctor.next11, %arrayctor.loop8 ]
  %call10 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur9)
  %arrayctor.next11 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur9, i32 1
  %arrayctor.done12 = icmp eq %class.btJacobianEntry* %arrayctor.next11, %arrayctor.end7
  br i1 %arrayctor.done12, label %arrayctor.cont13, label %arrayctor.loop8

arrayctor.cont13:                                 ; preds = %arrayctor.loop8
  %m_calculatedTransformA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 41
  %call14 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_calculatedTransformA)
  %m_calculatedTransformB = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 42
  %call15 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_calculatedTransformB)
  %m_sliderAxis = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 43
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_sliderAxis)
  %m_realPivotAInW = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 44
  %call17 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_realPivotAInW)
  %m_realPivotBInW = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 45
  %call18 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_realPivotBInW)
  %m_projPivotInW = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 46
  %call19 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_projPivotInW)
  %m_delta = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 47
  %call20 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_delta)
  %m_depth = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 48
  %call21 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_depth)
  %m_relPosA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 49
  %call22 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_relPosA)
  %m_relPosB = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 50
  %call23 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_relPosB)
  call void @_ZN18btSliderConstraint10initParamsEv(%class.btSliderConstraint* %this1)
  %7 = load %class.btSliderConstraint*, %class.btSliderConstraint** %retval, align 4
  ret %class.btSliderConstraint* %7
}

declare %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* returned, i32, %class.btRigidBody* nonnull align 4 dereferenceable(676), %class.btRigidBody* nonnull align 4 dereferenceable(676)) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btJacobianEntry*, align 4
  store %class.btJacobianEntry* %this, %class.btJacobianEntry** %this.addr, align 4
  %this1 = load %class.btJacobianEntry*, %class.btJacobianEntry** %this.addr, align 4
  %m_linearJointAxis = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_linearJointAxis)
  %m_aJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aJ)
  %m_bJ = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bJ)
  %m_0MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_0MinvJt)
  %m_1MinvJt = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_1MinvJt)
  ret %class.btJacobianEntry* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btSliderConstraint* @_ZN18btSliderConstraintC2ER11btRigidBodyRK11btTransformb(%class.btSliderConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbB, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInB, i1 zeroext %useLinearReferenceFrameA) unnamed_addr #2 {
entry:
  %retval = alloca %class.btSliderConstraint*, align 4
  %this.addr = alloca %class.btSliderConstraint*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %frameInB.addr = alloca %class.btTransform*, align 4
  %useLinearReferenceFrameA.addr = alloca i8, align 1
  %ref.tmp = alloca %class.btTransform, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4
  store %class.btTransform* %frameInB, %class.btTransform** %frameInB.addr, align 4
  %frombool = zext i1 %useLinearReferenceFrameA to i8
  store i8 %frombool, i8* %useLinearReferenceFrameA.addr, align 1
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  store %class.btSliderConstraint* %this1, %class.btSliderConstraint** %retval, align 4
  %0 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %call = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint12getFixedBodyEv()
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4
  %call2 = call %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* %0, i32 7, %class.btRigidBody* nonnull align 4 dereferenceable(676) %call, %class.btRigidBody* nonnull align 4 dereferenceable(676) %1)
  %2 = bitcast %class.btSliderConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTV18btSliderConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 1
  store i8 0, i8* %m_useSolveConstraintObsolete, align 4
  %m_frameInA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 3
  %call3 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_frameInA)
  %m_frameInB = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 4
  %3 = load %class.btTransform*, %class.btTransform** %frameInB.addr, align 4
  %call4 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %m_frameInB, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  %m_useLinearReferenceFrameA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 5
  %4 = load i8, i8* %useLinearReferenceFrameA.addr, align 1
  %tobool = trunc i8 %4 to i1
  %frombool5 = zext i1 %tobool to i8
  store i8 %frombool5, i8* %m_useLinearReferenceFrameA, align 4
  %m_jacLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 37
  %array.begin = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacLin, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btJacobianEntry* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call6 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btJacobianEntry* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_jacAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 39
  %array.begin7 = getelementptr inbounds [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry]* %m_jacAng, i32 0, i32 0
  %arrayctor.end8 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %array.begin7, i32 3
  br label %arrayctor.loop9

arrayctor.loop9:                                  ; preds = %arrayctor.loop9, %arrayctor.cont
  %arrayctor.cur10 = phi %class.btJacobianEntry* [ %array.begin7, %arrayctor.cont ], [ %arrayctor.next12, %arrayctor.loop9 ]
  %call11 = call %class.btJacobianEntry* @_ZN15btJacobianEntryC2Ev(%class.btJacobianEntry* %arrayctor.cur10)
  %arrayctor.next12 = getelementptr inbounds %class.btJacobianEntry, %class.btJacobianEntry* %arrayctor.cur10, i32 1
  %arrayctor.done13 = icmp eq %class.btJacobianEntry* %arrayctor.next12, %arrayctor.end8
  br i1 %arrayctor.done13, label %arrayctor.cont14, label %arrayctor.loop9

arrayctor.cont14:                                 ; preds = %arrayctor.loop9
  %m_calculatedTransformA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 41
  %call15 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_calculatedTransformA)
  %m_calculatedTransformB = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 42
  %call16 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_calculatedTransformB)
  %m_sliderAxis = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 43
  %call17 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_sliderAxis)
  %m_realPivotAInW = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 44
  %call18 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_realPivotAInW)
  %m_realPivotBInW = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 45
  %call19 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_realPivotBInW)
  %m_projPivotInW = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 46
  %call20 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_projPivotInW)
  %m_delta = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 47
  %call21 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_delta)
  %m_depth = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 48
  %call22 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_depth)
  %m_relPosA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 49
  %call23 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_relPosA)
  %m_relPosB = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 50
  %call24 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_relPosB)
  %5 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %5)
  %m_frameInB26 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 4
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %call25, %class.btTransform* nonnull align 4 dereferenceable(64) %m_frameInB26)
  %m_frameInA27 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 3
  %call28 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_frameInA27, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  call void @_ZN18btSliderConstraint10initParamsEv(%class.btSliderConstraint* %this1)
  %6 = load %class.btSliderConstraint*, %class.btSliderConstraint** %retval, align 4
  ret %class.btSliderConstraint* %6
}

declare nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint12getFixedBodyEv() #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN18btSliderConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E(%class.btSliderConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_useSolveConstraintObsolete = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 1
  %0 = load i8, i8* %m_useSolveConstraintObsolete, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %1, i32 0, i32 0
  store i32 0, i32* %m_numConstraintRows, align 4
  %2 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %2, i32 0, i32 1
  store i32 0, i32* %nub, align 4
  br label %if.end19

if.else:                                          ; preds = %entry
  %3 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows2 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %3, i32 0, i32 0
  store i32 4, i32* %m_numConstraintRows2, align 4
  %4 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub3 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %4, i32 0, i32 1
  store i32 2, i32* %nub3, align 4
  %5 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %5, i32 0, i32 8
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %6)
  %7 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %7, i32 0, i32 9
  %8 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %8)
  call void @_ZN18btSliderConstraint19calculateTransformsERK11btTransformS2_(%class.btSliderConstraint* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call4)
  call void @_ZN18btSliderConstraint13testAngLimitsEv(%class.btSliderConstraint* %this1)
  call void @_ZN18btSliderConstraint13testLinLimitsEv(%class.btSliderConstraint* %this1)
  %call5 = call zeroext i1 @_ZN18btSliderConstraint16getSolveLinLimitEv(%class.btSliderConstraint* %this1)
  br i1 %call5, label %if.then7, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.else
  %call6 = call zeroext i1 @_ZN18btSliderConstraint18getPoweredLinMotorEv(%class.btSliderConstraint* %this1)
  br i1 %call6, label %if.then7, label %if.end

if.then7:                                         ; preds = %lor.lhs.false, %if.else
  %9 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows8 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %9, i32 0, i32 0
  %10 = load i32, i32* %m_numConstraintRows8, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %m_numConstraintRows8, align 4
  %11 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub9 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %11, i32 0, i32 1
  %12 = load i32, i32* %nub9, align 4
  %dec = add nsw i32 %12, -1
  store i32 %dec, i32* %nub9, align 4
  br label %if.end

if.end:                                           ; preds = %if.then7, %lor.lhs.false
  %call10 = call zeroext i1 @_ZN18btSliderConstraint16getSolveAngLimitEv(%class.btSliderConstraint* %this1)
  br i1 %call10, label %if.then13, label %lor.lhs.false11

lor.lhs.false11:                                  ; preds = %if.end
  %call12 = call zeroext i1 @_ZN18btSliderConstraint18getPoweredAngMotorEv(%class.btSliderConstraint* %this1)
  br i1 %call12, label %if.then13, label %if.end18

if.then13:                                        ; preds = %lor.lhs.false11, %if.end
  %13 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows14 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %13, i32 0, i32 0
  %14 = load i32, i32* %m_numConstraintRows14, align 4
  %inc15 = add nsw i32 %14, 1
  store i32 %inc15, i32* %m_numConstraintRows14, align 4
  %15 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub16 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %15, i32 0, i32 1
  %16 = load i32, i32* %nub16, align 4
  %dec17 = add nsw i32 %16, -1
  store i32 %dec17, i32* %nub16, align 4
  br label %if.end18

if.end18:                                         ; preds = %if.then13, %lor.lhs.false11
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN18btSliderConstraint13testAngLimitsEv(%class.btSliderConstraint* %this) #2 {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  %axisA0 = alloca %class.btVector3, align 4
  %axisA1 = alloca %class.btVector3, align 4
  %axisB0 = alloca %class.btVector3, align 4
  %rot = alloca float, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_angDepth = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 53
  store float 0.000000e+00, float* %m_angDepth, align 4
  %m_solveAngLim = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 35
  store i8 0, i8* %m_solveAngLim, align 1
  %m_lowerAngLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 8
  %0 = load float, float* %m_lowerAngLimit, align 4
  %m_upperAngLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 9
  %1 = load float, float* %m_upperAngLimit, align 4
  %cmp = fcmp ole float %0, %1
  br i1 %cmp, label %if.then, label %if.end25

if.then:                                          ; preds = %entry
  %m_calculatedTransformA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 41
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axisA0, %class.btMatrix3x3* %call, i32 1)
  %m_calculatedTransformA2 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 41
  %call3 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformA2)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axisA1, %class.btMatrix3x3* %call3, i32 2)
  %m_calculatedTransformB = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 42
  %call4 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %m_calculatedTransformB)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %axisB0, %class.btMatrix3x3* %call4, i32 1)
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %axisB0, %class.btVector3* nonnull align 4 dereferenceable(16) %axisA1)
  %call6 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %axisB0, %class.btVector3* nonnull align 4 dereferenceable(16) %axisA0)
  %call7 = call float @_Z7btAtan2ff(float %call5, float %call6)
  store float %call7, float* %rot, align 4
  %2 = load float, float* %rot, align 4
  %m_lowerAngLimit8 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 8
  %3 = load float, float* %m_lowerAngLimit8, align 4
  %m_upperAngLimit9 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 9
  %4 = load float, float* %m_upperAngLimit9, align 4
  %call10 = call float @_Z21btAdjustAngleToLimitsfff(float %2, float %3, float %4)
  store float %call10, float* %rot, align 4
  %5 = load float, float* %rot, align 4
  %m_angPos = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 52
  store float %5, float* %m_angPos, align 4
  %6 = load float, float* %rot, align 4
  %m_lowerAngLimit11 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 8
  %7 = load float, float* %m_lowerAngLimit11, align 4
  %cmp12 = fcmp olt float %6, %7
  br i1 %cmp12, label %if.then13, label %if.else

if.then13:                                        ; preds = %if.then
  %8 = load float, float* %rot, align 4
  %m_lowerAngLimit14 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 8
  %9 = load float, float* %m_lowerAngLimit14, align 4
  %sub = fsub float %8, %9
  %m_angDepth15 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 53
  store float %sub, float* %m_angDepth15, align 4
  %m_solveAngLim16 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 35
  store i8 1, i8* %m_solveAngLim16, align 1
  br label %if.end24

if.else:                                          ; preds = %if.then
  %10 = load float, float* %rot, align 4
  %m_upperAngLimit17 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 9
  %11 = load float, float* %m_upperAngLimit17, align 4
  %cmp18 = fcmp ogt float %10, %11
  br i1 %cmp18, label %if.then19, label %if.end

if.then19:                                        ; preds = %if.else
  %12 = load float, float* %rot, align 4
  %m_upperAngLimit20 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 9
  %13 = load float, float* %m_upperAngLimit20, align 4
  %sub21 = fsub float %12, %13
  %m_angDepth22 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 53
  store float %sub21, float* %m_angDepth22, align 4
  %m_solveAngLim23 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 35
  store i8 1, i8* %m_solveAngLim23, align 1
  br label %if.end

if.end:                                           ; preds = %if.then19, %if.else
  br label %if.end24

if.end24:                                         ; preds = %if.end, %if.then13
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN18btSliderConstraint13testLinLimitsEv(%class.btSliderConstraint* %this) #2 {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_solveLinLim = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 34
  store i8 0, i8* %m_solveLinLim, align 4
  %m_depth = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 48
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_depth)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %0 = load float, float* %arrayidx, align 4
  %m_linPos = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 51
  store float %0, float* %m_linPos, align 4
  %m_lowerLinLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 6
  %1 = load float, float* %m_lowerLinLimit, align 4
  %m_upperLinLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 7
  %2 = load float, float* %m_upperLinLimit, align 4
  %cmp = fcmp ole float %1, %2
  br i1 %cmp, label %if.then, label %if.else30

if.then:                                          ; preds = %entry
  %m_depth2 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 48
  %call3 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_depth2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 0
  %3 = load float, float* %arrayidx4, align 4
  %m_upperLinLimit5 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 7
  %4 = load float, float* %m_upperLinLimit5, align 4
  %cmp6 = fcmp ogt float %3, %4
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then
  %m_upperLinLimit8 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 7
  %5 = load float, float* %m_upperLinLimit8, align 4
  %m_depth9 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 48
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_depth9)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 0
  %6 = load float, float* %arrayidx11, align 4
  %sub = fsub float %6, %5
  store float %sub, float* %arrayidx11, align 4
  %m_solveLinLim12 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 34
  store i8 1, i8* %m_solveLinLim12, align 4
  br label %if.end29

if.else:                                          ; preds = %if.then
  %m_depth13 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 48
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_depth13)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 0
  %7 = load float, float* %arrayidx15, align 4
  %m_lowerLinLimit16 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 6
  %8 = load float, float* %m_lowerLinLimit16, align 4
  %cmp17 = fcmp olt float %7, %8
  br i1 %cmp17, label %if.then18, label %if.else25

if.then18:                                        ; preds = %if.else
  %m_lowerLinLimit19 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 6
  %9 = load float, float* %m_lowerLinLimit19, align 4
  %m_depth20 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 48
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_depth20)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 0
  %10 = load float, float* %arrayidx22, align 4
  %sub23 = fsub float %10, %9
  store float %sub23, float* %arrayidx22, align 4
  %m_solveLinLim24 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 34
  store i8 1, i8* %m_solveLinLim24, align 4
  br label %if.end

if.else25:                                        ; preds = %if.else
  %m_depth26 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 48
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_depth26)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  store float 0.000000e+00, float* %arrayidx28, align 4
  br label %if.end

if.end:                                           ; preds = %if.else25, %if.then18
  br label %if.end29

if.end29:                                         ; preds = %if.end, %if.then7
  br label %if.end34

if.else30:                                        ; preds = %entry
  %m_depth31 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 48
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_depth31)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 0
  store float 0.000000e+00, float* %arrayidx33, align 4
  br label %if.end34

if.end34:                                         ; preds = %if.else30, %if.end29
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN18btSliderConstraint16getSolveLinLimitEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_solveLinLim = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 34
  %0 = load i8, i8* %m_solveLinLim, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN18btSliderConstraint18getPoweredLinMotorEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_poweredLinMotor = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 55
  %0 = load i8, i8* %m_poweredLinMotor, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN18btSliderConstraint16getSolveAngLimitEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_solveAngLim = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 35
  %0 = load i8, i8* %m_solveAngLim, align 1
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN18btSliderConstraint18getPoweredAngMotorEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_poweredAngMotor = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 59
  %0 = load i8, i8* %m_poweredAngMotor, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN18btSliderConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E(%class.btSliderConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo1"* %info) #1 {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo1"*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo1"* %info, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %m_numConstraintRows = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %0, i32 0, i32 0
  store i32 6, i32* %m_numConstraintRows, align 4
  %1 = load %"struct.btTypedConstraint::btConstraintInfo1"*, %"struct.btTypedConstraint::btConstraintInfo1"** %info.addr, align 4
  %nub = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo1", %"struct.btTypedConstraint::btConstraintInfo1"* %1, i32 0, i32 1
  store i32 0, i32* %nub, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN18btSliderConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E(%class.btSliderConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %1 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %1, i32 0, i32 8
  %2 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %2)
  %3 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %3, i32 0, i32 9
  %4 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %4)
  %5 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA3 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %5, i32 0, i32 8
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA3, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %6)
  %7 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB5 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %7, i32 0, i32 9
  %8 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB5, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %8)
  %9 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA7 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %9, i32 0, i32 8
  %10 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA7, align 4
  %call8 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %10)
  %11 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB9 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %11, i32 0, i32 9
  %12 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB9, align 4
  %call10 = call float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %12)
  call void @_ZN18btSliderConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_ff(%class.btSliderConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %call, %class.btTransform* nonnull align 4 dereferenceable(64) %call2, %class.btVector3* nonnull align 4 dereferenceable(16) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %call6, float %call8, float %call10)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN18btSliderConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK9btVector3S8_ff(%class.btSliderConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB, float %rbAinvMass, float %rbBinvMass) #2 {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %linVelA.addr = alloca %class.btVector3*, align 4
  %linVelB.addr = alloca %class.btVector3*, align 4
  %rbAinvMass.addr = alloca float, align 4
  %rbBinvMass.addr = alloca float, align 4
  %trA = alloca %class.btTransform*, align 4
  %trB = alloca %class.btTransform*, align 4
  %i = alloca i32, align 4
  %s = alloca i32, align 4
  %signFact = alloca float, align 4
  %ofs = alloca %class.btVector3, align 4
  %miA = alloca float, align 4
  %miB = alloca float, align 4
  %hasStaticBody = alloca i8, align 1
  %miS = alloca float, align 4
  %factA = alloca float, align 4
  %factB = alloca float, align 4
  %ax1 = alloca %class.btVector3, align 4
  %p = alloca %class.btVector3, align 4
  %q = alloca %class.btVector3, align 4
  %ax1A = alloca %class.btVector3, align 4
  %ax1B = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %ref.tmp18 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %currERP = alloca float, align 4
  %k = alloca float, align 4
  %u = alloca %class.btVector3, align 4
  %nrow = alloca i32, align 4
  %srow = alloca i32, align 4
  %limit_err = alloca float, align 4
  %limit = alloca i32, align 4
  %powered = alloca i32, align 4
  %bodyA_trans = alloca %class.btTransform, align 4
  %bodyB_trans = alloca %class.btTransform, align 4
  %s2 = alloca i32, align 4
  %s3 = alloca i32, align 4
  %tmpA = alloca %class.btVector3, align 4
  %ref.tmp106 = alloca float, align 4
  %ref.tmp107 = alloca float, align 4
  %ref.tmp108 = alloca float, align 4
  %tmpB = alloca %class.btVector3, align 4
  %ref.tmp110 = alloca float, align 4
  %ref.tmp111 = alloca float, align 4
  %ref.tmp112 = alloca float, align 4
  %relA = alloca %class.btVector3, align 4
  %ref.tmp114 = alloca float, align 4
  %ref.tmp115 = alloca float, align 4
  %ref.tmp116 = alloca float, align 4
  %relB = alloca %class.btVector3, align 4
  %ref.tmp118 = alloca float, align 4
  %ref.tmp119 = alloca float, align 4
  %ref.tmp120 = alloca float, align 4
  %c = alloca %class.btVector3, align 4
  %ref.tmp122 = alloca float, align 4
  %ref.tmp123 = alloca float, align 4
  %ref.tmp124 = alloca float, align 4
  %ref.tmp129 = alloca %class.btVector3, align 4
  %projB = alloca %class.btVector3, align 4
  %ref.tmp132 = alloca float, align 4
  %orthoB = alloca %class.btVector3, align 4
  %ref.tmp134 = alloca %class.btVector3, align 4
  %projA = alloca %class.btVector3, align 4
  %ref.tmp137 = alloca float, align 4
  %orthoA = alloca %class.btVector3, align 4
  %sliderOffs = alloca float, align 4
  %totalDist = alloca %class.btVector3, align 4
  %ref.tmp142 = alloca %class.btVector3, align 4
  %ref.tmp143 = alloca %class.btVector3, align 4
  %ref.tmp144 = alloca %class.btVector3, align 4
  %ref.tmp145 = alloca %class.btVector3, align 4
  %ref.tmp146 = alloca %class.btVector3, align 4
  %ref.tmp147 = alloca %class.btVector3, align 4
  %ref.tmp148 = alloca %class.btVector3, align 4
  %ref.tmp149 = alloca %class.btVector3, align 4
  %ref.tmp150 = alloca %class.btVector3, align 4
  %len2 = alloca float, align 4
  %ref.tmp154 = alloca float, align 4
  %ref.tmp158 = alloca %class.btVector3, align 4
  %ref.tmp161 = alloca %class.btVector3, align 4
  %ref.tmp162 = alloca %class.btVector3, align 4
  %ref.tmp163 = alloca %class.btVector3, align 4
  %ref.tmp183 = alloca %class.btVector3, align 4
  %ref.tmp184 = alloca %class.btVector3, align 4
  %ref.tmp259 = alloca %class.btVector3, align 4
  %tmp = alloca %class.btVector3, align 4
  %ref.tmp286 = alloca %class.btVector3, align 4
  %rhs = alloca float, align 4
  %ref.tmp442 = alloca %class.btVector3, align 4
  %ref.tmp443 = alloca %class.btVector3, align 4
  %ltd = alloca %class.btVector3, align 4
  %ref.tmp480 = alloca %class.btVector3, align 4
  %lostop = alloca float, align 4
  %histop = alloca float, align 4
  %tag_vel = alloca float, align 4
  %mot_fact = alloca float, align 4
  %bounce = alloca float, align 4
  %vel = alloca float, align 4
  %newc = alloca float, align 4
  %newc632 = alloca float, align 4
  %lostop700 = alloca float, align 4
  %histop702 = alloca float, align 4
  %mot_fact726 = alloca float, align 4
  %bounce782 = alloca float, align 4
  %vel788 = alloca float, align 4
  %newc798 = alloca float, align 4
  %newc812 = alloca float, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store %class.btVector3* %linVelA, %class.btVector3** %linVelA.addr, align 4
  store %class.btVector3* %linVelB, %class.btVector3** %linVelB.addr, align 4
  store float %rbAinvMass, float* %rbAinvMass.addr, align 4
  store float %rbBinvMass, float* %rbBinvMass.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK18btSliderConstraint23getCalculatedTransformAEv(%class.btSliderConstraint* %this1)
  store %class.btTransform* %call, %class.btTransform** %trA, align 4
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK18btSliderConstraint23getCalculatedTransformBEv(%class.btSliderConstraint* %this1)
  store %class.btTransform* %call2, %class.btTransform** %trB, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %0, i32 0, i32 6
  %1 = load i32, i32* %rowskip, align 4
  store i32 %1, i32* %s, align 4
  %m_useLinearReferenceFrameA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 5
  %2 = load i8, i8* %m_useLinearReferenceFrameA, align 4
  %tobool = trunc i8 %2 to i1
  %3 = zext i1 %tobool to i64
  %cond = select i1 %tobool, float 1.000000e+00, float -1.000000e+00
  store float %cond, float* %signFact, align 4
  %4 = load %class.btTransform*, %class.btTransform** %trB, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %4)
  %5 = load %class.btTransform*, %class.btTransform** %trA, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %5)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ofs, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call4)
  %6 = load float, float* %rbAinvMass.addr, align 4
  store float %6, float* %miA, align 4
  %7 = load float, float* %rbBinvMass.addr, align 4
  store float %7, float* %miB, align 4
  %8 = load float, float* %miA, align 4
  %cmp = fcmp olt float %8, 0x3E80000000000000
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %9 = load float, float* %miB, align 4
  %cmp5 = fcmp olt float %9, 0x3E80000000000000
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %10 = phi i1 [ true, %entry ], [ %cmp5, %lor.rhs ]
  %frombool = zext i1 %10 to i8
  store i8 %frombool, i8* %hasStaticBody, align 1
  %11 = load float, float* %miA, align 4
  %12 = load float, float* %miB, align 4
  %add = fadd float %11, %12
  store float %add, float* %miS, align 4
  %13 = load float, float* %miS, align 4
  %cmp6 = fcmp ogt float %13, 0.000000e+00
  br i1 %cmp6, label %if.then, label %if.else

if.then:                                          ; preds = %lor.end
  %14 = load float, float* %miB, align 4
  %15 = load float, float* %miS, align 4
  %div = fdiv float %14, %15
  store float %div, float* %factA, align 4
  br label %if.end

if.else:                                          ; preds = %lor.end
  store float 5.000000e-01, float* %factA, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %16 = load float, float* %factA, align 4
  %sub = fsub float 1.000000e+00, %16
  store float %sub, float* %factB, align 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ax1)
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %p)
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %q)
  %17 = load %class.btTransform*, %class.btTransform** %trA, align 4
  %call10 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %17)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ax1A, %class.btMatrix3x3* %call10, i32 0)
  %18 = load %class.btTransform*, %class.btTransform** %trB, align 4
  %call11 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %18)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ax1B, %class.btMatrix3x3* %call11, i32 0)
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 2
  %19 = load i8, i8* %m_useOffsetForConstraintFrame, align 1
  %tobool12 = trunc i8 %19 to i1
  br i1 %tobool12, label %if.then13, label %if.else17

if.then13:                                        ; preds = %if.end
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1A, float* nonnull align 4 dereferenceable(4) %factA)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1B, float* nonnull align 4 dereferenceable(4) %factB)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15)
  %20 = bitcast %class.btVector3* %ax1 to i8*
  %21 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false)
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %ax1)
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %ax1, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  br label %if.end24

if.else17:                                        ; preds = %if.end
  %22 = load %class.btTransform*, %class.btTransform** %trA, align 4
  %call19 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %22)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp18, %class.btMatrix3x3* %call19, i32 0)
  %23 = bitcast %class.btVector3* %ax1 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false)
  %25 = load %class.btTransform*, %class.btTransform** %trA, align 4
  %call21 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %25)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp20, %class.btMatrix3x3* %call21, i32 1)
  %26 = bitcast %class.btVector3* %p to i8*
  %27 = bitcast %class.btVector3* %ref.tmp20 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false)
  %28 = load %class.btTransform*, %class.btTransform** %trA, align 4
  %call23 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %28)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp22, %class.btMatrix3x3* %call23, i32 2)
  %29 = bitcast %class.btVector3* %q to i8*
  %30 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false)
  br label %if.end24

if.end24:                                         ; preds = %if.else17, %if.then13
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx = getelementptr inbounds float, float* %call25, i32 0
  %31 = load float, float* %arrayidx, align 4
  %32 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %32, i32 0, i32 3
  %33 = load float*, float** %m_J1angularAxis, align 4
  %arrayidx26 = getelementptr inbounds float, float* %33, i32 0
  store float %31, float* %arrayidx26, align 4
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 1
  %34 = load float, float* %arrayidx28, align 4
  %35 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis29 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %35, i32 0, i32 3
  %36 = load float*, float** %m_J1angularAxis29, align 4
  %arrayidx30 = getelementptr inbounds float, float* %36, i32 1
  store float %34, float* %arrayidx30, align 4
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 2
  %37 = load float, float* %arrayidx32, align 4
  %38 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis33 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %38, i32 0, i32 3
  %39 = load float*, float** %m_J1angularAxis33, align 4
  %arrayidx34 = getelementptr inbounds float, float* %39, i32 2
  store float %37, float* %arrayidx34, align 4
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %40 = load float, float* %arrayidx36, align 4
  %41 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis37 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %41, i32 0, i32 3
  %42 = load float*, float** %m_J1angularAxis37, align 4
  %43 = load i32, i32* %s, align 4
  %add38 = add nsw i32 %43, 0
  %arrayidx39 = getelementptr inbounds float, float* %42, i32 %add38
  store float %40, float* %arrayidx39, align 4
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 1
  %44 = load float, float* %arrayidx41, align 4
  %45 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis42 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %45, i32 0, i32 3
  %46 = load float*, float** %m_J1angularAxis42, align 4
  %47 = load i32, i32* %s, align 4
  %add43 = add nsw i32 %47, 1
  %arrayidx44 = getelementptr inbounds float, float* %46, i32 %add43
  store float %44, float* %arrayidx44, align 4
  %call45 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 2
  %48 = load float, float* %arrayidx46, align 4
  %49 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis47 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %49, i32 0, i32 3
  %50 = load float*, float** %m_J1angularAxis47, align 4
  %51 = load i32, i32* %s, align 4
  %add48 = add nsw i32 %51, 2
  %arrayidx49 = getelementptr inbounds float, float* %50, i32 %add48
  store float %48, float* %arrayidx49, align 4
  %call50 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 0
  %52 = load float, float* %arrayidx51, align 4
  %fneg = fneg float %52
  %53 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %53, i32 0, i32 5
  %54 = load float*, float** %m_J2angularAxis, align 4
  %arrayidx52 = getelementptr inbounds float, float* %54, i32 0
  store float %fneg, float* %arrayidx52, align 4
  %call53 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx54 = getelementptr inbounds float, float* %call53, i32 1
  %55 = load float, float* %arrayidx54, align 4
  %fneg55 = fneg float %55
  %56 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis56 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %56, i32 0, i32 5
  %57 = load float*, float** %m_J2angularAxis56, align 4
  %arrayidx57 = getelementptr inbounds float, float* %57, i32 1
  store float %fneg55, float* %arrayidx57, align 4
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 2
  %58 = load float, float* %arrayidx59, align 4
  %fneg60 = fneg float %58
  %59 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis61 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %59, i32 0, i32 5
  %60 = load float*, float** %m_J2angularAxis61, align 4
  %arrayidx62 = getelementptr inbounds float, float* %60, i32 2
  store float %fneg60, float* %arrayidx62, align 4
  %call63 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 0
  %61 = load float, float* %arrayidx64, align 4
  %fneg65 = fneg float %61
  %62 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis66 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %62, i32 0, i32 5
  %63 = load float*, float** %m_J2angularAxis66, align 4
  %64 = load i32, i32* %s, align 4
  %add67 = add nsw i32 %64, 0
  %arrayidx68 = getelementptr inbounds float, float* %63, i32 %add67
  store float %fneg65, float* %arrayidx68, align 4
  %call69 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx70 = getelementptr inbounds float, float* %call69, i32 1
  %65 = load float, float* %arrayidx70, align 4
  %fneg71 = fneg float %65
  %66 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis72 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %66, i32 0, i32 5
  %67 = load float*, float** %m_J2angularAxis72, align 4
  %68 = load i32, i32* %s, align 4
  %add73 = add nsw i32 %68, 1
  %arrayidx74 = getelementptr inbounds float, float* %67, i32 %add73
  store float %fneg71, float* %arrayidx74, align 4
  %call75 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %arrayidx76 = getelementptr inbounds float, float* %call75, i32 2
  %69 = load float, float* %arrayidx76, align 4
  %fneg77 = fneg float %69
  %70 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis78 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %70, i32 0, i32 5
  %71 = load float*, float** %m_J2angularAxis78, align 4
  %72 = load i32, i32* %s, align 4
  %add79 = add nsw i32 %72, 2
  %arrayidx80 = getelementptr inbounds float, float* %71, i32 %add79
  store float %fneg77, float* %arrayidx80, align 4
  %m_flags = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %73 = load i32, i32* %m_flags, align 4
  %and = and i32 %73, 128
  %tobool81 = icmp ne i32 %and, 0
  br i1 %tobool81, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end24
  %m_softnessOrthoAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 30
  %74 = load float, float* %m_softnessOrthoAng, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.end24
  %m_softnessOrthoAng82 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 30
  %75 = load float, float* %m_softnessOrthoAng82, align 4
  %76 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %erp = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %76, i32 0, i32 1
  %77 = load float, float* %erp, align 4
  %mul = fmul float %75, %77
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond83 = phi float [ %74, %cond.true ], [ %mul, %cond.false ]
  store float %cond83, float* %currERP, align 4
  %78 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %78, i32 0, i32 0
  %79 = load float, float* %fps, align 4
  %80 = load float, float* %currERP, align 4
  %mul84 = fmul float %79, %80
  store float %mul84, float* %k, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %u, %class.btVector3* %ax1A, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1B)
  %81 = load float, float* %k, align 4
  %call85 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %u, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %mul86 = fmul float %81, %call85
  %82 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %82, i32 0, i32 7
  %83 = load float*, float** %m_constraintError, align 4
  %arrayidx87 = getelementptr inbounds float, float* %83, i32 0
  store float %mul86, float* %arrayidx87, align 4
  %84 = load float, float* %k, align 4
  %call88 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %u, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  %mul89 = fmul float %84, %call88
  %85 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError90 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %85, i32 0, i32 7
  %86 = load float*, float** %m_constraintError90, align 4
  %87 = load i32, i32* %s, align 4
  %arrayidx91 = getelementptr inbounds float, float* %86, i32 %87
  store float %mul89, float* %arrayidx91, align 4
  %m_flags92 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %88 = load i32, i32* %m_flags92, align 4
  %and93 = and i32 %88, 64
  %tobool94 = icmp ne i32 %and93, 0
  br i1 %tobool94, label %if.then95, label %if.end100

if.then95:                                        ; preds = %cond.end
  %m_cfmOrthoAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 33
  %89 = load float, float* %m_cfmOrthoAng, align 4
  %90 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %90, i32 0, i32 8
  %91 = load float*, float** %cfm, align 4
  %arrayidx96 = getelementptr inbounds float, float* %91, i32 0
  store float %89, float* %arrayidx96, align 4
  %m_cfmOrthoAng97 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 33
  %92 = load float, float* %m_cfmOrthoAng97, align 4
  %93 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm98 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %93, i32 0, i32 8
  %94 = load float*, float** %cfm98, align 4
  %95 = load i32, i32* %s, align 4
  %arrayidx99 = getelementptr inbounds float, float* %94, i32 %95
  store float %92, float* %arrayidx99, align 4
  br label %if.end100

if.end100:                                        ; preds = %if.then95, %cond.end
  store i32 1, i32* %nrow, align 4
  %96 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call101 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %bodyA_trans, %class.btTransform* nonnull align 4 dereferenceable(64) %96)
  %97 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call102 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %bodyB_trans, %class.btTransform* nonnull align 4 dereferenceable(64) %97)
  %98 = load i32, i32* %nrow, align 4
  %inc = add nsw i32 %98, 1
  store i32 %inc, i32* %nrow, align 4
  %99 = load i32, i32* %nrow, align 4
  %100 = load i32, i32* %s, align 4
  %mul103 = mul nsw i32 %99, %100
  store i32 %mul103, i32* %s2, align 4
  %101 = load i32, i32* %nrow, align 4
  %inc104 = add nsw i32 %101, 1
  store i32 %inc104, i32* %nrow, align 4
  %102 = load i32, i32* %nrow, align 4
  %103 = load i32, i32* %s, align 4
  %mul105 = mul nsw i32 %102, %103
  store i32 %mul105, i32* %s3, align 4
  store float 0.000000e+00, float* %ref.tmp106, align 4
  store float 0.000000e+00, float* %ref.tmp107, align 4
  store float 0.000000e+00, float* %ref.tmp108, align 4
  %call109 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %tmpA, float* nonnull align 4 dereferenceable(4) %ref.tmp106, float* nonnull align 4 dereferenceable(4) %ref.tmp107, float* nonnull align 4 dereferenceable(4) %ref.tmp108)
  store float 0.000000e+00, float* %ref.tmp110, align 4
  store float 0.000000e+00, float* %ref.tmp111, align 4
  store float 0.000000e+00, float* %ref.tmp112, align 4
  %call113 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %tmpB, float* nonnull align 4 dereferenceable(4) %ref.tmp110, float* nonnull align 4 dereferenceable(4) %ref.tmp111, float* nonnull align 4 dereferenceable(4) %ref.tmp112)
  store float 0.000000e+00, float* %ref.tmp114, align 4
  store float 0.000000e+00, float* %ref.tmp115, align 4
  store float 0.000000e+00, float* %ref.tmp116, align 4
  %call117 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %relA, float* nonnull align 4 dereferenceable(4) %ref.tmp114, float* nonnull align 4 dereferenceable(4) %ref.tmp115, float* nonnull align 4 dereferenceable(4) %ref.tmp116)
  store float 0.000000e+00, float* %ref.tmp118, align 4
  store float 0.000000e+00, float* %ref.tmp119, align 4
  store float 0.000000e+00, float* %ref.tmp120, align 4
  %call121 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %relB, float* nonnull align 4 dereferenceable(4) %ref.tmp118, float* nonnull align 4 dereferenceable(4) %ref.tmp119, float* nonnull align 4 dereferenceable(4) %ref.tmp120)
  store float 0.000000e+00, float* %ref.tmp122, align 4
  store float 0.000000e+00, float* %ref.tmp123, align 4
  store float 0.000000e+00, float* %ref.tmp124, align 4
  %call125 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %c, float* nonnull align 4 dereferenceable(4) %ref.tmp122, float* nonnull align 4 dereferenceable(4) %ref.tmp123, float* nonnull align 4 dereferenceable(4) %ref.tmp124)
  %m_useOffsetForConstraintFrame126 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 2
  %104 = load i8, i8* %m_useOffsetForConstraintFrame126, align 1
  %tobool127 = trunc i8 %104 to i1
  br i1 %tobool127, label %if.then128, label %if.else258

if.then128:                                       ; preds = %if.end100
  %105 = load %class.btTransform*, %class.btTransform** %trB, align 4
  %call130 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %105)
  %call131 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %bodyB_trans)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp129, %class.btVector3* nonnull align 4 dereferenceable(16) %call130, %class.btVector3* nonnull align 4 dereferenceable(16) %call131)
  %106 = bitcast %class.btVector3* %relB to i8*
  %107 = bitcast %class.btVector3* %ref.tmp129 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %106, i8* align 4 %107, i32 16, i1 false)
  %call133 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  store float %call133, float* %ref.tmp132, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %projB, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1, float* nonnull align 4 dereferenceable(4) %ref.tmp132)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %orthoB, %class.btVector3* nonnull align 4 dereferenceable(16) %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %projB)
  %108 = load %class.btTransform*, %class.btTransform** %trA, align 4
  %call135 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %108)
  %call136 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %bodyA_trans)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp134, %class.btVector3* nonnull align 4 dereferenceable(16) %call135, %class.btVector3* nonnull align 4 dereferenceable(16) %call136)
  %109 = bitcast %class.btVector3* %relA to i8*
  %110 = bitcast %class.btVector3* %ref.tmp134 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %109, i8* align 4 %110, i32 16, i1 false)
  %call138 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  store float %call138, float* %ref.tmp137, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %projA, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1, float* nonnull align 4 dereferenceable(4) %ref.tmp137)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %orthoA, %class.btVector3* nonnull align 4 dereferenceable(16) %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %projA)
  %m_linPos = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 51
  %111 = load float, float* %m_linPos, align 4
  %m_depth = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 48
  %call139 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_depth)
  %arrayidx140 = getelementptr inbounds float, float* %call139, i32 0
  %112 = load float, float* %arrayidx140, align 4
  %sub141 = fsub float %111, %112
  store float %sub141, float* %sliderOffs, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp143, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1, float* nonnull align 4 dereferenceable(4) %sliderOffs)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp142, %class.btVector3* nonnull align 4 dereferenceable(16) %projA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp143)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %totalDist, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp142, %class.btVector3* nonnull align 4 dereferenceable(16) %projB)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp145, %class.btVector3* nonnull align 4 dereferenceable(16) %totalDist, float* nonnull align 4 dereferenceable(4) %factA)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp144, %class.btVector3* nonnull align 4 dereferenceable(16) %orthoA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp145)
  %113 = bitcast %class.btVector3* %relA to i8*
  %114 = bitcast %class.btVector3* %ref.tmp144 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %113, i8* align 4 %114, i32 16, i1 false)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp147, %class.btVector3* nonnull align 4 dereferenceable(16) %totalDist, float* nonnull align 4 dereferenceable(4) %factB)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp146, %class.btVector3* nonnull align 4 dereferenceable(16) %orthoB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp147)
  %115 = bitcast %class.btVector3* %relB to i8*
  %116 = bitcast %class.btVector3* %ref.tmp146 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %115, i8* align 4 %116, i32 16, i1 false)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp149, %class.btVector3* nonnull align 4 dereferenceable(16) %orthoB, float* nonnull align 4 dereferenceable(4) %factA)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp150, %class.btVector3* nonnull align 4 dereferenceable(16) %orthoA, float* nonnull align 4 dereferenceable(4) %factB)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp148, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp149, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp150)
  %117 = bitcast %class.btVector3* %p to i8*
  %118 = bitcast %class.btVector3* %ref.tmp148 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %117, i8* align 4 %118, i32 16, i1 false)
  %call151 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %p)
  store float %call151, float* %len2, align 4
  %119 = load float, float* %len2, align 4
  %cmp152 = fcmp ogt float %119, 0x3E80000000000000
  br i1 %cmp152, label %if.then153, label %if.else157

if.then153:                                       ; preds = %if.then128
  %120 = load float, float* %len2, align 4
  %call155 = call float @_Z6btSqrtf(float %120)
  store float %call155, float* %ref.tmp154, align 4
  %call156 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %p, float* nonnull align 4 dereferenceable(4) %ref.tmp154)
  br label %if.end160

if.else157:                                       ; preds = %if.then128
  %121 = load %class.btTransform*, %class.btTransform** %trA, align 4
  %call159 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %121)
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp158, %class.btMatrix3x3* %call159, i32 1)
  %122 = bitcast %class.btVector3* %p to i8*
  %123 = bitcast %class.btVector3* %ref.tmp158 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %122, i8* align 4 %123, i32 16, i1 false)
  br label %if.end160

if.end160:                                        ; preds = %if.else157, %if.then153
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp161, %class.btVector3* %ax1, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %124 = bitcast %class.btVector3* %q to i8*
  %125 = bitcast %class.btVector3* %ref.tmp161 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %124, i8* align 4 %125, i32 16, i1 false)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp162, %class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %126 = bitcast %class.btVector3* %tmpA to i8*
  %127 = bitcast %class.btVector3* %ref.tmp162 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %126, i8* align 4 %127, i32 16, i1 false)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp163, %class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  %128 = bitcast %class.btVector3* %tmpB to i8*
  %129 = bitcast %class.btVector3* %ref.tmp163 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %128, i8* align 4 %129, i32 16, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end160
  %130 = load i32, i32* %i, align 4
  %cmp164 = icmp slt i32 %130, 3
  br i1 %cmp164, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call165 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpA)
  %131 = load i32, i32* %i, align 4
  %arrayidx166 = getelementptr inbounds float, float* %call165, i32 %131
  %132 = load float, float* %arrayidx166, align 4
  %133 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis167 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %133, i32 0, i32 3
  %134 = load float*, float** %m_J1angularAxis167, align 4
  %135 = load i32, i32* %s2, align 4
  %136 = load i32, i32* %i, align 4
  %add168 = add nsw i32 %135, %136
  %arrayidx169 = getelementptr inbounds float, float* %134, i32 %add168
  store float %132, float* %arrayidx169, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %137 = load i32, i32* %i, align 4
  %inc170 = add nsw i32 %137, 1
  store i32 %inc170, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond171

for.cond171:                                      ; preds = %for.inc180, %for.end
  %138 = load i32, i32* %i, align 4
  %cmp172 = icmp slt i32 %138, 3
  br i1 %cmp172, label %for.body173, label %for.end182

for.body173:                                      ; preds = %for.cond171
  %call174 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpB)
  %139 = load i32, i32* %i, align 4
  %arrayidx175 = getelementptr inbounds float, float* %call174, i32 %139
  %140 = load float, float* %arrayidx175, align 4
  %fneg176 = fneg float %140
  %141 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis177 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %141, i32 0, i32 5
  %142 = load float*, float** %m_J2angularAxis177, align 4
  %143 = load i32, i32* %s2, align 4
  %144 = load i32, i32* %i, align 4
  %add178 = add nsw i32 %143, %144
  %arrayidx179 = getelementptr inbounds float, float* %142, i32 %add178
  store float %fneg176, float* %arrayidx179, align 4
  br label %for.inc180

for.inc180:                                       ; preds = %for.body173
  %145 = load i32, i32* %i, align 4
  %inc181 = add nsw i32 %145, 1
  store i32 %inc181, i32* %i, align 4
  br label %for.cond171

for.end182:                                       ; preds = %for.cond171
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp183, %class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  %146 = bitcast %class.btVector3* %tmpA to i8*
  %147 = bitcast %class.btVector3* %ref.tmp183 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %146, i8* align 4 %147, i32 16, i1 false)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp184, %class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  %148 = bitcast %class.btVector3* %tmpB to i8*
  %149 = bitcast %class.btVector3* %ref.tmp184 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %148, i8* align 4 %149, i32 16, i1 false)
  %150 = load i8, i8* %hasStaticBody, align 1
  %tobool185 = trunc i8 %150 to i1
  br i1 %tobool185, label %land.lhs.true, label %if.end190

land.lhs.true:                                    ; preds = %for.end182
  %call186 = call zeroext i1 @_ZN18btSliderConstraint16getSolveAngLimitEv(%class.btSliderConstraint* %this1)
  br i1 %call186, label %if.then187, label %if.end190

if.then187:                                       ; preds = %land.lhs.true
  %call188 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tmpB, float* nonnull align 4 dereferenceable(4) %factB)
  %call189 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %tmpA, float* nonnull align 4 dereferenceable(4) %factA)
  br label %if.end190

if.end190:                                        ; preds = %if.then187, %land.lhs.true, %for.end182
  store i32 0, i32* %i, align 4
  br label %for.cond191

for.cond191:                                      ; preds = %for.inc199, %if.end190
  %151 = load i32, i32* %i, align 4
  %cmp192 = icmp slt i32 %151, 3
  br i1 %cmp192, label %for.body193, label %for.end201

for.body193:                                      ; preds = %for.cond191
  %call194 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpA)
  %152 = load i32, i32* %i, align 4
  %arrayidx195 = getelementptr inbounds float, float* %call194, i32 %152
  %153 = load float, float* %arrayidx195, align 4
  %154 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis196 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %154, i32 0, i32 3
  %155 = load float*, float** %m_J1angularAxis196, align 4
  %156 = load i32, i32* %s3, align 4
  %157 = load i32, i32* %i, align 4
  %add197 = add nsw i32 %156, %157
  %arrayidx198 = getelementptr inbounds float, float* %155, i32 %add197
  store float %153, float* %arrayidx198, align 4
  br label %for.inc199

for.inc199:                                       ; preds = %for.body193
  %158 = load i32, i32* %i, align 4
  %inc200 = add nsw i32 %158, 1
  store i32 %inc200, i32* %i, align 4
  br label %for.cond191

for.end201:                                       ; preds = %for.cond191
  store i32 0, i32* %i, align 4
  br label %for.cond202

for.cond202:                                      ; preds = %for.inc211, %for.end201
  %159 = load i32, i32* %i, align 4
  %cmp203 = icmp slt i32 %159, 3
  br i1 %cmp203, label %for.body204, label %for.end213

for.body204:                                      ; preds = %for.cond202
  %call205 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpB)
  %160 = load i32, i32* %i, align 4
  %arrayidx206 = getelementptr inbounds float, float* %call205, i32 %160
  %161 = load float, float* %arrayidx206, align 4
  %fneg207 = fneg float %161
  %162 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis208 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %162, i32 0, i32 5
  %163 = load float*, float** %m_J2angularAxis208, align 4
  %164 = load i32, i32* %s3, align 4
  %165 = load i32, i32* %i, align 4
  %add209 = add nsw i32 %164, %165
  %arrayidx210 = getelementptr inbounds float, float* %163, i32 %add209
  store float %fneg207, float* %arrayidx210, align 4
  br label %for.inc211

for.inc211:                                       ; preds = %for.body204
  %166 = load i32, i32* %i, align 4
  %inc212 = add nsw i32 %166, 1
  store i32 %inc212, i32* %i, align 4
  br label %for.cond202

for.end213:                                       ; preds = %for.cond202
  store i32 0, i32* %i, align 4
  br label %for.cond214

for.cond214:                                      ; preds = %for.inc221, %for.end213
  %167 = load i32, i32* %i, align 4
  %cmp215 = icmp slt i32 %167, 3
  br i1 %cmp215, label %for.body216, label %for.end223

for.body216:                                      ; preds = %for.cond214
  %call217 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %168 = load i32, i32* %i, align 4
  %arrayidx218 = getelementptr inbounds float, float* %call217, i32 %168
  %169 = load float, float* %arrayidx218, align 4
  %170 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %170, i32 0, i32 2
  %171 = load float*, float** %m_J1linearAxis, align 4
  %172 = load i32, i32* %s2, align 4
  %173 = load i32, i32* %i, align 4
  %add219 = add nsw i32 %172, %173
  %arrayidx220 = getelementptr inbounds float, float* %171, i32 %add219
  store float %169, float* %arrayidx220, align 4
  br label %for.inc221

for.inc221:                                       ; preds = %for.body216
  %174 = load i32, i32* %i, align 4
  %inc222 = add nsw i32 %174, 1
  store i32 %inc222, i32* %i, align 4
  br label %for.cond214

for.end223:                                       ; preds = %for.cond214
  store i32 0, i32* %i, align 4
  br label %for.cond224

for.cond224:                                      ; preds = %for.inc232, %for.end223
  %175 = load i32, i32* %i, align 4
  %cmp225 = icmp slt i32 %175, 3
  br i1 %cmp225, label %for.body226, label %for.end234

for.body226:                                      ; preds = %for.cond224
  %call227 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %176 = load i32, i32* %i, align 4
  %arrayidx228 = getelementptr inbounds float, float* %call227, i32 %176
  %177 = load float, float* %arrayidx228, align 4
  %178 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis229 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %178, i32 0, i32 2
  %179 = load float*, float** %m_J1linearAxis229, align 4
  %180 = load i32, i32* %s3, align 4
  %181 = load i32, i32* %i, align 4
  %add230 = add nsw i32 %180, %181
  %arrayidx231 = getelementptr inbounds float, float* %179, i32 %add230
  store float %177, float* %arrayidx231, align 4
  br label %for.inc232

for.inc232:                                       ; preds = %for.body226
  %182 = load i32, i32* %i, align 4
  %inc233 = add nsw i32 %182, 1
  store i32 %inc233, i32* %i, align 4
  br label %for.cond224

for.end234:                                       ; preds = %for.cond224
  store i32 0, i32* %i, align 4
  br label %for.cond235

for.cond235:                                      ; preds = %for.inc243, %for.end234
  %183 = load i32, i32* %i, align 4
  %cmp236 = icmp slt i32 %183, 3
  br i1 %cmp236, label %for.body237, label %for.end245

for.body237:                                      ; preds = %for.cond235
  %call238 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %184 = load i32, i32* %i, align 4
  %arrayidx239 = getelementptr inbounds float, float* %call238, i32 %184
  %185 = load float, float* %arrayidx239, align 4
  %fneg240 = fneg float %185
  %186 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %186, i32 0, i32 4
  %187 = load float*, float** %m_J2linearAxis, align 4
  %188 = load i32, i32* %s2, align 4
  %189 = load i32, i32* %i, align 4
  %add241 = add nsw i32 %188, %189
  %arrayidx242 = getelementptr inbounds float, float* %187, i32 %add241
  store float %fneg240, float* %arrayidx242, align 4
  br label %for.inc243

for.inc243:                                       ; preds = %for.body237
  %190 = load i32, i32* %i, align 4
  %inc244 = add nsw i32 %190, 1
  store i32 %inc244, i32* %i, align 4
  br label %for.cond235

for.end245:                                       ; preds = %for.cond235
  store i32 0, i32* %i, align 4
  br label %for.cond246

for.cond246:                                      ; preds = %for.inc255, %for.end245
  %191 = load i32, i32* %i, align 4
  %cmp247 = icmp slt i32 %191, 3
  br i1 %cmp247, label %for.body248, label %for.end257

for.body248:                                      ; preds = %for.cond246
  %call249 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %192 = load i32, i32* %i, align 4
  %arrayidx250 = getelementptr inbounds float, float* %call249, i32 %192
  %193 = load float, float* %arrayidx250, align 4
  %fneg251 = fneg float %193
  %194 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis252 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %194, i32 0, i32 4
  %195 = load float*, float** %m_J2linearAxis252, align 4
  %196 = load i32, i32* %s3, align 4
  %197 = load i32, i32* %i, align 4
  %add253 = add nsw i32 %196, %197
  %arrayidx254 = getelementptr inbounds float, float* %195, i32 %add253
  store float %fneg251, float* %arrayidx254, align 4
  br label %for.inc255

for.inc255:                                       ; preds = %for.body248
  %198 = load i32, i32* %i, align 4
  %inc256 = add nsw i32 %198, 1
  store i32 %inc256, i32* %i, align 4
  br label %for.cond246

for.end257:                                       ; preds = %for.cond246
  br label %if.end357

if.else258:                                       ; preds = %if.end100
  %call260 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %bodyB_trans)
  %call261 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %bodyA_trans)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp259, %class.btVector3* nonnull align 4 dereferenceable(16) %call260, %class.btVector3* nonnull align 4 dereferenceable(16) %call261)
  %199 = bitcast %class.btVector3* %c to i8*
  %200 = bitcast %class.btVector3* %ref.tmp259 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %199, i8* align 4 %200, i32 16, i1 false)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %tmp, %class.btVector3* %c, %class.btVector3* nonnull align 4 dereferenceable(16) %p)
  store i32 0, i32* %i, align 4
  br label %for.cond262

for.cond262:                                      ; preds = %for.inc271, %if.else258
  %201 = load i32, i32* %i, align 4
  %cmp263 = icmp slt i32 %201, 3
  br i1 %cmp263, label %for.body264, label %for.end273

for.body264:                                      ; preds = %for.cond262
  %202 = load float, float* %factA, align 4
  %call265 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %203 = load i32, i32* %i, align 4
  %arrayidx266 = getelementptr inbounds float, float* %call265, i32 %203
  %204 = load float, float* %arrayidx266, align 4
  %mul267 = fmul float %202, %204
  %205 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis268 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %205, i32 0, i32 3
  %206 = load float*, float** %m_J1angularAxis268, align 4
  %207 = load i32, i32* %s2, align 4
  %208 = load i32, i32* %i, align 4
  %add269 = add nsw i32 %207, %208
  %arrayidx270 = getelementptr inbounds float, float* %206, i32 %add269
  store float %mul267, float* %arrayidx270, align 4
  br label %for.inc271

for.inc271:                                       ; preds = %for.body264
  %209 = load i32, i32* %i, align 4
  %inc272 = add nsw i32 %209, 1
  store i32 %inc272, i32* %i, align 4
  br label %for.cond262

for.end273:                                       ; preds = %for.cond262
  store i32 0, i32* %i, align 4
  br label %for.cond274

for.cond274:                                      ; preds = %for.inc283, %for.end273
  %210 = load i32, i32* %i, align 4
  %cmp275 = icmp slt i32 %210, 3
  br i1 %cmp275, label %for.body276, label %for.end285

for.body276:                                      ; preds = %for.cond274
  %211 = load float, float* %factB, align 4
  %call277 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %212 = load i32, i32* %i, align 4
  %arrayidx278 = getelementptr inbounds float, float* %call277, i32 %212
  %213 = load float, float* %arrayidx278, align 4
  %mul279 = fmul float %211, %213
  %214 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis280 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %214, i32 0, i32 5
  %215 = load float*, float** %m_J2angularAxis280, align 4
  %216 = load i32, i32* %s2, align 4
  %217 = load i32, i32* %i, align 4
  %add281 = add nsw i32 %216, %217
  %arrayidx282 = getelementptr inbounds float, float* %215, i32 %add281
  store float %mul279, float* %arrayidx282, align 4
  br label %for.inc283

for.inc283:                                       ; preds = %for.body276
  %218 = load i32, i32* %i, align 4
  %inc284 = add nsw i32 %218, 1
  store i32 %inc284, i32* %i, align 4
  br label %for.cond274

for.end285:                                       ; preds = %for.cond274
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp286, %class.btVector3* %c, %class.btVector3* nonnull align 4 dereferenceable(16) %q)
  %219 = bitcast %class.btVector3* %tmp to i8*
  %220 = bitcast %class.btVector3* %ref.tmp286 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %219, i8* align 4 %220, i32 16, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond287

for.cond287:                                      ; preds = %for.inc296, %for.end285
  %221 = load i32, i32* %i, align 4
  %cmp288 = icmp slt i32 %221, 3
  br i1 %cmp288, label %for.body289, label %for.end298

for.body289:                                      ; preds = %for.cond287
  %222 = load float, float* %factA, align 4
  %call290 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %223 = load i32, i32* %i, align 4
  %arrayidx291 = getelementptr inbounds float, float* %call290, i32 %223
  %224 = load float, float* %arrayidx291, align 4
  %mul292 = fmul float %222, %224
  %225 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis293 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %225, i32 0, i32 3
  %226 = load float*, float** %m_J1angularAxis293, align 4
  %227 = load i32, i32* %s3, align 4
  %228 = load i32, i32* %i, align 4
  %add294 = add nsw i32 %227, %228
  %arrayidx295 = getelementptr inbounds float, float* %226, i32 %add294
  store float %mul292, float* %arrayidx295, align 4
  br label %for.inc296

for.inc296:                                       ; preds = %for.body289
  %229 = load i32, i32* %i, align 4
  %inc297 = add nsw i32 %229, 1
  store i32 %inc297, i32* %i, align 4
  br label %for.cond287

for.end298:                                       ; preds = %for.cond287
  store i32 0, i32* %i, align 4
  br label %for.cond299

for.cond299:                                      ; preds = %for.inc308, %for.end298
  %230 = load i32, i32* %i, align 4
  %cmp300 = icmp slt i32 %230, 3
  br i1 %cmp300, label %for.body301, label %for.end310

for.body301:                                      ; preds = %for.cond299
  %231 = load float, float* %factB, align 4
  %call302 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %232 = load i32, i32* %i, align 4
  %arrayidx303 = getelementptr inbounds float, float* %call302, i32 %232
  %233 = load float, float* %arrayidx303, align 4
  %mul304 = fmul float %231, %233
  %234 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis305 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %234, i32 0, i32 5
  %235 = load float*, float** %m_J2angularAxis305, align 4
  %236 = load i32, i32* %s3, align 4
  %237 = load i32, i32* %i, align 4
  %add306 = add nsw i32 %236, %237
  %arrayidx307 = getelementptr inbounds float, float* %235, i32 %add306
  store float %mul304, float* %arrayidx307, align 4
  br label %for.inc308

for.inc308:                                       ; preds = %for.body301
  %238 = load i32, i32* %i, align 4
  %inc309 = add nsw i32 %238, 1
  store i32 %inc309, i32* %i, align 4
  br label %for.cond299

for.end310:                                       ; preds = %for.cond299
  store i32 0, i32* %i, align 4
  br label %for.cond311

for.cond311:                                      ; preds = %for.inc319, %for.end310
  %239 = load i32, i32* %i, align 4
  %cmp312 = icmp slt i32 %239, 3
  br i1 %cmp312, label %for.body313, label %for.end321

for.body313:                                      ; preds = %for.cond311
  %call314 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %240 = load i32, i32* %i, align 4
  %arrayidx315 = getelementptr inbounds float, float* %call314, i32 %240
  %241 = load float, float* %arrayidx315, align 4
  %242 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis316 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %242, i32 0, i32 2
  %243 = load float*, float** %m_J1linearAxis316, align 4
  %244 = load i32, i32* %s2, align 4
  %245 = load i32, i32* %i, align 4
  %add317 = add nsw i32 %244, %245
  %arrayidx318 = getelementptr inbounds float, float* %243, i32 %add317
  store float %241, float* %arrayidx318, align 4
  br label %for.inc319

for.inc319:                                       ; preds = %for.body313
  %246 = load i32, i32* %i, align 4
  %inc320 = add nsw i32 %246, 1
  store i32 %inc320, i32* %i, align 4
  br label %for.cond311

for.end321:                                       ; preds = %for.cond311
  store i32 0, i32* %i, align 4
  br label %for.cond322

for.cond322:                                      ; preds = %for.inc330, %for.end321
  %247 = load i32, i32* %i, align 4
  %cmp323 = icmp slt i32 %247, 3
  br i1 %cmp323, label %for.body324, label %for.end332

for.body324:                                      ; preds = %for.cond322
  %call325 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %248 = load i32, i32* %i, align 4
  %arrayidx326 = getelementptr inbounds float, float* %call325, i32 %248
  %249 = load float, float* %arrayidx326, align 4
  %250 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis327 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %250, i32 0, i32 2
  %251 = load float*, float** %m_J1linearAxis327, align 4
  %252 = load i32, i32* %s3, align 4
  %253 = load i32, i32* %i, align 4
  %add328 = add nsw i32 %252, %253
  %arrayidx329 = getelementptr inbounds float, float* %251, i32 %add328
  store float %249, float* %arrayidx329, align 4
  br label %for.inc330

for.inc330:                                       ; preds = %for.body324
  %254 = load i32, i32* %i, align 4
  %inc331 = add nsw i32 %254, 1
  store i32 %inc331, i32* %i, align 4
  br label %for.cond322

for.end332:                                       ; preds = %for.cond322
  store i32 0, i32* %i, align 4
  br label %for.cond333

for.cond333:                                      ; preds = %for.inc342, %for.end332
  %255 = load i32, i32* %i, align 4
  %cmp334 = icmp slt i32 %255, 3
  br i1 %cmp334, label %for.body335, label %for.end344

for.body335:                                      ; preds = %for.cond333
  %call336 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %p)
  %256 = load i32, i32* %i, align 4
  %arrayidx337 = getelementptr inbounds float, float* %call336, i32 %256
  %257 = load float, float* %arrayidx337, align 4
  %fneg338 = fneg float %257
  %258 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis339 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %258, i32 0, i32 4
  %259 = load float*, float** %m_J2linearAxis339, align 4
  %260 = load i32, i32* %s2, align 4
  %261 = load i32, i32* %i, align 4
  %add340 = add nsw i32 %260, %261
  %arrayidx341 = getelementptr inbounds float, float* %259, i32 %add340
  store float %fneg338, float* %arrayidx341, align 4
  br label %for.inc342

for.inc342:                                       ; preds = %for.body335
  %262 = load i32, i32* %i, align 4
  %inc343 = add nsw i32 %262, 1
  store i32 %inc343, i32* %i, align 4
  br label %for.cond333

for.end344:                                       ; preds = %for.cond333
  store i32 0, i32* %i, align 4
  br label %for.cond345

for.cond345:                                      ; preds = %for.inc354, %for.end344
  %263 = load i32, i32* %i, align 4
  %cmp346 = icmp slt i32 %263, 3
  br i1 %cmp346, label %for.body347, label %for.end356

for.body347:                                      ; preds = %for.cond345
  %call348 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %q)
  %264 = load i32, i32* %i, align 4
  %arrayidx349 = getelementptr inbounds float, float* %call348, i32 %264
  %265 = load float, float* %arrayidx349, align 4
  %fneg350 = fneg float %265
  %266 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis351 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %266, i32 0, i32 4
  %267 = load float*, float** %m_J2linearAxis351, align 4
  %268 = load i32, i32* %s3, align 4
  %269 = load i32, i32* %i, align 4
  %add352 = add nsw i32 %268, %269
  %arrayidx353 = getelementptr inbounds float, float* %267, i32 %add352
  store float %fneg350, float* %arrayidx353, align 4
  br label %for.inc354

for.inc354:                                       ; preds = %for.body347
  %270 = load i32, i32* %i, align 4
  %inc355 = add nsw i32 %270, 1
  store i32 %inc355, i32* %i, align 4
  br label %for.cond345

for.end356:                                       ; preds = %for.cond345
  br label %if.end357

if.end357:                                        ; preds = %for.end356, %for.end257
  %m_flags358 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %271 = load i32, i32* %m_flags358, align 4
  %and359 = and i32 %271, 32
  %tobool360 = icmp ne i32 %and359, 0
  br i1 %tobool360, label %cond.true361, label %cond.false362

cond.true361:                                     ; preds = %if.end357
  %m_softnessOrthoLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 26
  %272 = load float, float* %m_softnessOrthoLin, align 4
  br label %cond.end366

cond.false362:                                    ; preds = %if.end357
  %m_softnessOrthoLin363 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 26
  %273 = load float, float* %m_softnessOrthoLin363, align 4
  %274 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %erp364 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %274, i32 0, i32 1
  %275 = load float, float* %erp364, align 4
  %mul365 = fmul float %273, %275
  br label %cond.end366

cond.end366:                                      ; preds = %cond.false362, %cond.true361
  %cond367 = phi float [ %272, %cond.true361 ], [ %mul365, %cond.false362 ]
  store float %cond367, float* %currERP, align 4
  %276 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps368 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %276, i32 0, i32 0
  %277 = load float, float* %fps368, align 4
  %278 = load float, float* %currERP, align 4
  %mul369 = fmul float %277, %278
  store float %mul369, float* %k, align 4
  %279 = load float, float* %k, align 4
  %call370 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %p, %class.btVector3* nonnull align 4 dereferenceable(16) %ofs)
  %mul371 = fmul float %279, %call370
  store float %mul371, float* %rhs, align 4
  %280 = load float, float* %rhs, align 4
  %281 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError372 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %281, i32 0, i32 7
  %282 = load float*, float** %m_constraintError372, align 4
  %283 = load i32, i32* %s2, align 4
  %arrayidx373 = getelementptr inbounds float, float* %282, i32 %283
  store float %280, float* %arrayidx373, align 4
  %284 = load float, float* %k, align 4
  %call374 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %q, %class.btVector3* nonnull align 4 dereferenceable(16) %ofs)
  %mul375 = fmul float %284, %call374
  store float %mul375, float* %rhs, align 4
  %285 = load float, float* %rhs, align 4
  %286 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError376 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %286, i32 0, i32 7
  %287 = load float*, float** %m_constraintError376, align 4
  %288 = load i32, i32* %s3, align 4
  %arrayidx377 = getelementptr inbounds float, float* %287, i32 %288
  store float %285, float* %arrayidx377, align 4
  %m_flags378 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %289 = load i32, i32* %m_flags378, align 4
  %and379 = and i32 %289, 16
  %tobool380 = icmp ne i32 %and379, 0
  br i1 %tobool380, label %if.then381, label %if.end387

if.then381:                                       ; preds = %cond.end366
  %m_cfmOrthoLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 29
  %290 = load float, float* %m_cfmOrthoLin, align 4
  %291 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm382 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %291, i32 0, i32 8
  %292 = load float*, float** %cfm382, align 4
  %293 = load i32, i32* %s2, align 4
  %arrayidx383 = getelementptr inbounds float, float* %292, i32 %293
  store float %290, float* %arrayidx383, align 4
  %m_cfmOrthoLin384 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 29
  %294 = load float, float* %m_cfmOrthoLin384, align 4
  %295 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm385 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %295, i32 0, i32 8
  %296 = load float*, float** %cfm385, align 4
  %297 = load i32, i32* %s3, align 4
  %arrayidx386 = getelementptr inbounds float, float* %296, i32 %297
  store float %294, float* %arrayidx386, align 4
  br label %if.end387

if.end387:                                        ; preds = %if.then381, %cond.end366
  store float 0.000000e+00, float* %limit_err, align 4
  store i32 0, i32* %limit, align 4
  %call388 = call zeroext i1 @_ZN18btSliderConstraint16getSolveLinLimitEv(%class.btSliderConstraint* %this1)
  br i1 %call388, label %if.then389, label %if.end394

if.then389:                                       ; preds = %if.end387
  %call390 = call float @_ZN18btSliderConstraint11getLinDepthEv(%class.btSliderConstraint* %this1)
  %298 = load float, float* %signFact, align 4
  %mul391 = fmul float %call390, %298
  store float %mul391, float* %limit_err, align 4
  %299 = load float, float* %limit_err, align 4
  %cmp392 = fcmp ogt float %299, 0.000000e+00
  %300 = zext i1 %cmp392 to i64
  %cond393 = select i1 %cmp392, i32 2, i32 1
  store i32 %cond393, i32* %limit, align 4
  br label %if.end394

if.end394:                                        ; preds = %if.then389, %if.end387
  store i32 0, i32* %powered, align 4
  %call395 = call zeroext i1 @_ZN18btSliderConstraint18getPoweredLinMotorEv(%class.btSliderConstraint* %this1)
  br i1 %call395, label %if.then396, label %if.end397

if.then396:                                       ; preds = %if.end394
  store i32 1, i32* %powered, align 4
  br label %if.end397

if.end397:                                        ; preds = %if.then396, %if.end394
  %301 = load i32, i32* %limit, align 4
  %tobool398 = icmp ne i32 %301, 0
  br i1 %tobool398, label %if.then400, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end397
  %302 = load i32, i32* %powered, align 4
  %tobool399 = icmp ne i32 %302, 0
  br i1 %tobool399, label %if.then400, label %if.end650

if.then400:                                       ; preds = %lor.lhs.false, %if.end397
  %303 = load i32, i32* %nrow, align 4
  %inc401 = add nsw i32 %303, 1
  store i32 %inc401, i32* %nrow, align 4
  %304 = load i32, i32* %nrow, align 4
  %305 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip402 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %305, i32 0, i32 6
  %306 = load i32, i32* %rowskip402, align 4
  %mul403 = mul nsw i32 %304, %306
  store i32 %mul403, i32* %srow, align 4
  %call404 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx405 = getelementptr inbounds float, float* %call404, i32 0
  %307 = load float, float* %arrayidx405, align 4
  %308 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis406 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %308, i32 0, i32 2
  %309 = load float*, float** %m_J1linearAxis406, align 4
  %310 = load i32, i32* %srow, align 4
  %add407 = add nsw i32 %310, 0
  %arrayidx408 = getelementptr inbounds float, float* %309, i32 %add407
  store float %307, float* %arrayidx408, align 4
  %call409 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx410 = getelementptr inbounds float, float* %call409, i32 1
  %311 = load float, float* %arrayidx410, align 4
  %312 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis411 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %312, i32 0, i32 2
  %313 = load float*, float** %m_J1linearAxis411, align 4
  %314 = load i32, i32* %srow, align 4
  %add412 = add nsw i32 %314, 1
  %arrayidx413 = getelementptr inbounds float, float* %313, i32 %add412
  store float %311, float* %arrayidx413, align 4
  %call414 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx415 = getelementptr inbounds float, float* %call414, i32 2
  %315 = load float, float* %arrayidx415, align 4
  %316 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1linearAxis416 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %316, i32 0, i32 2
  %317 = load float*, float** %m_J1linearAxis416, align 4
  %318 = load i32, i32* %srow, align 4
  %add417 = add nsw i32 %318, 2
  %arrayidx418 = getelementptr inbounds float, float* %317, i32 %add417
  store float %315, float* %arrayidx418, align 4
  %call419 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx420 = getelementptr inbounds float, float* %call419, i32 0
  %319 = load float, float* %arrayidx420, align 4
  %fneg421 = fneg float %319
  %320 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis422 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %320, i32 0, i32 4
  %321 = load float*, float** %m_J2linearAxis422, align 4
  %322 = load i32, i32* %srow, align 4
  %add423 = add nsw i32 %322, 0
  %arrayidx424 = getelementptr inbounds float, float* %321, i32 %add423
  store float %fneg421, float* %arrayidx424, align 4
  %call425 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx426 = getelementptr inbounds float, float* %call425, i32 1
  %323 = load float, float* %arrayidx426, align 4
  %fneg427 = fneg float %323
  %324 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis428 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %324, i32 0, i32 4
  %325 = load float*, float** %m_J2linearAxis428, align 4
  %326 = load i32, i32* %srow, align 4
  %add429 = add nsw i32 %326, 1
  %arrayidx430 = getelementptr inbounds float, float* %325, i32 %add429
  store float %fneg427, float* %arrayidx430, align 4
  %call431 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx432 = getelementptr inbounds float, float* %call431, i32 2
  %327 = load float, float* %arrayidx432, align 4
  %fneg433 = fneg float %327
  %328 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2linearAxis434 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %328, i32 0, i32 4
  %329 = load float*, float** %m_J2linearAxis434, align 4
  %330 = load i32, i32* %srow, align 4
  %add435 = add nsw i32 %330, 2
  %arrayidx436 = getelementptr inbounds float, float* %329, i32 %add435
  store float %fneg433, float* %arrayidx436, align 4
  %m_useOffsetForConstraintFrame437 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 2
  %331 = load i8, i8* %m_useOffsetForConstraintFrame437, align 1
  %tobool438 = trunc i8 %331 to i1
  br i1 %tobool438, label %if.then439, label %if.else478

if.then439:                                       ; preds = %if.then400
  %332 = load i8, i8* %hasStaticBody, align 1
  %tobool440 = trunc i8 %332 to i1
  br i1 %tobool440, label %if.end477, label %if.then441

if.then441:                                       ; preds = %if.then439
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp442, %class.btVector3* %relA, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  %333 = bitcast %class.btVector3* %tmpA to i8*
  %334 = bitcast %class.btVector3* %ref.tmp442 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %333, i8* align 4 %334, i32 16, i1 false)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp443, %class.btVector3* %relB, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  %335 = bitcast %class.btVector3* %tmpB to i8*
  %336 = bitcast %class.btVector3* %ref.tmp443 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %335, i8* align 4 %336, i32 16, i1 false)
  %call444 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpA)
  %arrayidx445 = getelementptr inbounds float, float* %call444, i32 0
  %337 = load float, float* %arrayidx445, align 4
  %338 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis446 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %338, i32 0, i32 3
  %339 = load float*, float** %m_J1angularAxis446, align 4
  %340 = load i32, i32* %srow, align 4
  %add447 = add nsw i32 %340, 0
  %arrayidx448 = getelementptr inbounds float, float* %339, i32 %add447
  store float %337, float* %arrayidx448, align 4
  %call449 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpA)
  %arrayidx450 = getelementptr inbounds float, float* %call449, i32 1
  %341 = load float, float* %arrayidx450, align 4
  %342 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis451 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %342, i32 0, i32 3
  %343 = load float*, float** %m_J1angularAxis451, align 4
  %344 = load i32, i32* %srow, align 4
  %add452 = add nsw i32 %344, 1
  %arrayidx453 = getelementptr inbounds float, float* %343, i32 %add452
  store float %341, float* %arrayidx453, align 4
  %call454 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpA)
  %arrayidx455 = getelementptr inbounds float, float* %call454, i32 2
  %345 = load float, float* %arrayidx455, align 4
  %346 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis456 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %346, i32 0, i32 3
  %347 = load float*, float** %m_J1angularAxis456, align 4
  %348 = load i32, i32* %srow, align 4
  %add457 = add nsw i32 %348, 2
  %arrayidx458 = getelementptr inbounds float, float* %347, i32 %add457
  store float %345, float* %arrayidx458, align 4
  %call459 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpB)
  %arrayidx460 = getelementptr inbounds float, float* %call459, i32 0
  %349 = load float, float* %arrayidx460, align 4
  %fneg461 = fneg float %349
  %350 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis462 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %350, i32 0, i32 5
  %351 = load float*, float** %m_J2angularAxis462, align 4
  %352 = load i32, i32* %srow, align 4
  %add463 = add nsw i32 %352, 0
  %arrayidx464 = getelementptr inbounds float, float* %351, i32 %add463
  store float %fneg461, float* %arrayidx464, align 4
  %call465 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpB)
  %arrayidx466 = getelementptr inbounds float, float* %call465, i32 1
  %353 = load float, float* %arrayidx466, align 4
  %fneg467 = fneg float %353
  %354 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis468 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %354, i32 0, i32 5
  %355 = load float*, float** %m_J2angularAxis468, align 4
  %356 = load i32, i32* %srow, align 4
  %add469 = add nsw i32 %356, 1
  %arrayidx470 = getelementptr inbounds float, float* %355, i32 %add469
  store float %fneg467, float* %arrayidx470, align 4
  %call471 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmpB)
  %arrayidx472 = getelementptr inbounds float, float* %call471, i32 2
  %357 = load float, float* %arrayidx472, align 4
  %fneg473 = fneg float %357
  %358 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis474 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %358, i32 0, i32 5
  %359 = load float*, float** %m_J2angularAxis474, align 4
  %360 = load i32, i32* %srow, align 4
  %add475 = add nsw i32 %360, 2
  %arrayidx476 = getelementptr inbounds float, float* %359, i32 %add475
  store float %fneg473, float* %arrayidx476, align 4
  br label %if.end477

if.end477:                                        ; preds = %if.then441, %if.then439
  br label %if.end517

if.else478:                                       ; preds = %if.then400
  %call479 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ltd)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp480, %class.btVector3* %c, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  %361 = bitcast %class.btVector3* %ltd to i8*
  %362 = bitcast %class.btVector3* %ref.tmp480 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %361, i8* align 4 %362, i32 16, i1 false)
  %363 = load float, float* %factA, align 4
  %call481 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ltd)
  %arrayidx482 = getelementptr inbounds float, float* %call481, i32 0
  %364 = load float, float* %arrayidx482, align 4
  %mul483 = fmul float %363, %364
  %365 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis484 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %365, i32 0, i32 3
  %366 = load float*, float** %m_J1angularAxis484, align 4
  %367 = load i32, i32* %srow, align 4
  %add485 = add nsw i32 %367, 0
  %arrayidx486 = getelementptr inbounds float, float* %366, i32 %add485
  store float %mul483, float* %arrayidx486, align 4
  %368 = load float, float* %factA, align 4
  %call487 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ltd)
  %arrayidx488 = getelementptr inbounds float, float* %call487, i32 1
  %369 = load float, float* %arrayidx488, align 4
  %mul489 = fmul float %368, %369
  %370 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis490 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %370, i32 0, i32 3
  %371 = load float*, float** %m_J1angularAxis490, align 4
  %372 = load i32, i32* %srow, align 4
  %add491 = add nsw i32 %372, 1
  %arrayidx492 = getelementptr inbounds float, float* %371, i32 %add491
  store float %mul489, float* %arrayidx492, align 4
  %373 = load float, float* %factA, align 4
  %call493 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ltd)
  %arrayidx494 = getelementptr inbounds float, float* %call493, i32 2
  %374 = load float, float* %arrayidx494, align 4
  %mul495 = fmul float %373, %374
  %375 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis496 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %375, i32 0, i32 3
  %376 = load float*, float** %m_J1angularAxis496, align 4
  %377 = load i32, i32* %srow, align 4
  %add497 = add nsw i32 %377, 2
  %arrayidx498 = getelementptr inbounds float, float* %376, i32 %add497
  store float %mul495, float* %arrayidx498, align 4
  %378 = load float, float* %factB, align 4
  %call499 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ltd)
  %arrayidx500 = getelementptr inbounds float, float* %call499, i32 0
  %379 = load float, float* %arrayidx500, align 4
  %mul501 = fmul float %378, %379
  %380 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis502 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %380, i32 0, i32 5
  %381 = load float*, float** %m_J2angularAxis502, align 4
  %382 = load i32, i32* %srow, align 4
  %add503 = add nsw i32 %382, 0
  %arrayidx504 = getelementptr inbounds float, float* %381, i32 %add503
  store float %mul501, float* %arrayidx504, align 4
  %383 = load float, float* %factB, align 4
  %call505 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ltd)
  %arrayidx506 = getelementptr inbounds float, float* %call505, i32 1
  %384 = load float, float* %arrayidx506, align 4
  %mul507 = fmul float %383, %384
  %385 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis508 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %385, i32 0, i32 5
  %386 = load float*, float** %m_J2angularAxis508, align 4
  %387 = load i32, i32* %srow, align 4
  %add509 = add nsw i32 %387, 1
  %arrayidx510 = getelementptr inbounds float, float* %386, i32 %add509
  store float %mul507, float* %arrayidx510, align 4
  %388 = load float, float* %factB, align 4
  %call511 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ltd)
  %arrayidx512 = getelementptr inbounds float, float* %call511, i32 2
  %389 = load float, float* %arrayidx512, align 4
  %mul513 = fmul float %388, %389
  %390 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis514 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %390, i32 0, i32 5
  %391 = load float*, float** %m_J2angularAxis514, align 4
  %392 = load i32, i32* %srow, align 4
  %add515 = add nsw i32 %392, 2
  %arrayidx516 = getelementptr inbounds float, float* %391, i32 %add515
  store float %mul513, float* %arrayidx516, align 4
  br label %if.end517

if.end517:                                        ; preds = %if.else478, %if.end477
  %call518 = call float @_ZN18btSliderConstraint16getLowerLinLimitEv(%class.btSliderConstraint* %this1)
  store float %call518, float* %lostop, align 4
  %call519 = call float @_ZN18btSliderConstraint16getUpperLinLimitEv(%class.btSliderConstraint* %this1)
  store float %call519, float* %histop, align 4
  %393 = load i32, i32* %limit, align 4
  %tobool520 = icmp ne i32 %393, 0
  br i1 %tobool520, label %land.lhs.true521, label %if.end524

land.lhs.true521:                                 ; preds = %if.end517
  %394 = load float, float* %lostop, align 4
  %395 = load float, float* %histop, align 4
  %cmp522 = fcmp oeq float %394, %395
  br i1 %cmp522, label %if.then523, label %if.end524

if.then523:                                       ; preds = %land.lhs.true521
  store i32 0, i32* %powered, align 4
  br label %if.end524

if.end524:                                        ; preds = %if.then523, %land.lhs.true521, %if.end517
  %396 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError525 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %396, i32 0, i32 7
  %397 = load float*, float** %m_constraintError525, align 4
  %398 = load i32, i32* %srow, align 4
  %arrayidx526 = getelementptr inbounds float, float* %397, i32 %398
  store float 0.000000e+00, float* %arrayidx526, align 4
  %399 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %399, i32 0, i32 9
  %400 = load float*, float** %m_lowerLimit, align 4
  %401 = load i32, i32* %srow, align 4
  %arrayidx527 = getelementptr inbounds float, float* %400, i32 %401
  store float 0.000000e+00, float* %arrayidx527, align 4
  %402 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %402, i32 0, i32 10
  %403 = load float*, float** %m_upperLimit, align 4
  %404 = load i32, i32* %srow, align 4
  %arrayidx528 = getelementptr inbounds float, float* %403, i32 %404
  store float 0.000000e+00, float* %arrayidx528, align 4
  %m_flags529 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %405 = load i32, i32* %m_flags529, align 4
  %and530 = and i32 %405, 512
  %tobool531 = icmp ne i32 %and530, 0
  br i1 %tobool531, label %cond.true532, label %cond.false533

cond.true532:                                     ; preds = %if.end524
  %m_softnessLimLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 18
  %406 = load float, float* %m_softnessLimLin, align 4
  br label %cond.end535

cond.false533:                                    ; preds = %if.end524
  %407 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %erp534 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %407, i32 0, i32 1
  %408 = load float, float* %erp534, align 4
  br label %cond.end535

cond.end535:                                      ; preds = %cond.false533, %cond.true532
  %cond536 = phi float [ %406, %cond.true532 ], [ %408, %cond.false533 ]
  store float %cond536, float* %currERP, align 4
  %409 = load i32, i32* %powered, align 4
  %tobool537 = icmp ne i32 %409, 0
  br i1 %tobool537, label %if.then538, label %if.end570

if.then538:                                       ; preds = %cond.end535
  %m_flags539 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %410 = load i32, i32* %m_flags539, align 4
  %and540 = and i32 %410, 1
  %tobool541 = icmp ne i32 %and540, 0
  br i1 %tobool541, label %if.then542, label %if.end545

if.then542:                                       ; preds = %if.then538
  %m_cfmDirLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 13
  %411 = load float, float* %m_cfmDirLin, align 4
  %412 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm543 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %412, i32 0, i32 8
  %413 = load float*, float** %cfm543, align 4
  %414 = load i32, i32* %srow, align 4
  %arrayidx544 = getelementptr inbounds float, float* %413, i32 %414
  store float %411, float* %arrayidx544, align 4
  br label %if.end545

if.end545:                                        ; preds = %if.then542, %if.then538
  %call546 = call float @_ZN18btSliderConstraint25getTargetLinMotorVelocityEv(%class.btSliderConstraint* %this1)
  store float %call546, float* %tag_vel, align 4
  %415 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_linPos547 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 51
  %416 = load float, float* %m_linPos547, align 4
  %m_lowerLinLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 6
  %417 = load float, float* %m_lowerLinLimit, align 4
  %m_upperLinLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 7
  %418 = load float, float* %m_upperLinLimit, align 4
  %419 = load float, float* %tag_vel, align 4
  %420 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps548 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %420, i32 0, i32 0
  %421 = load float, float* %fps548, align 4
  %422 = load float, float* %currERP, align 4
  %mul549 = fmul float %421, %422
  %call550 = call float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint* %415, float %416, float %417, float %418, float %419, float %mul549)
  store float %call550, float* %mot_fact, align 4
  %423 = load float, float* %signFact, align 4
  %424 = load float, float* %mot_fact, align 4
  %mul551 = fmul float %423, %424
  %call552 = call float @_ZN18btSliderConstraint25getTargetLinMotorVelocityEv(%class.btSliderConstraint* %this1)
  %mul553 = fmul float %mul551, %call552
  %425 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError554 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %425, i32 0, i32 7
  %426 = load float*, float** %m_constraintError554, align 4
  %427 = load i32, i32* %srow, align 4
  %arrayidx555 = getelementptr inbounds float, float* %426, i32 %427
  %428 = load float, float* %arrayidx555, align 4
  %sub556 = fsub float %428, %mul553
  store float %sub556, float* %arrayidx555, align 4
  %call557 = call float @_ZN18btSliderConstraint19getMaxLinMotorForceEv(%class.btSliderConstraint* %this1)
  %fneg558 = fneg float %call557
  %429 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps559 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %429, i32 0, i32 0
  %430 = load float, float* %fps559, align 4
  %div560 = fdiv float %fneg558, %430
  %431 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit561 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %431, i32 0, i32 9
  %432 = load float*, float** %m_lowerLimit561, align 4
  %433 = load i32, i32* %srow, align 4
  %arrayidx562 = getelementptr inbounds float, float* %432, i32 %433
  %434 = load float, float* %arrayidx562, align 4
  %add563 = fadd float %434, %div560
  store float %add563, float* %arrayidx562, align 4
  %call564 = call float @_ZN18btSliderConstraint19getMaxLinMotorForceEv(%class.btSliderConstraint* %this1)
  %435 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps565 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %435, i32 0, i32 0
  %436 = load float, float* %fps565, align 4
  %div566 = fdiv float %call564, %436
  %437 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit567 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %437, i32 0, i32 10
  %438 = load float*, float** %m_upperLimit567, align 4
  %439 = load i32, i32* %srow, align 4
  %arrayidx568 = getelementptr inbounds float, float* %438, i32 %439
  %440 = load float, float* %arrayidx568, align 4
  %add569 = fadd float %440, %div566
  store float %add569, float* %arrayidx568, align 4
  br label %if.end570

if.end570:                                        ; preds = %if.end545, %cond.end535
  %441 = load i32, i32* %limit, align 4
  %tobool571 = icmp ne i32 %441, 0
  br i1 %tobool571, label %if.then572, label %if.end649

if.then572:                                       ; preds = %if.end570
  %442 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps573 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %442, i32 0, i32 0
  %443 = load float, float* %fps573, align 4
  %444 = load float, float* %currERP, align 4
  %mul574 = fmul float %443, %444
  store float %mul574, float* %k, align 4
  %445 = load float, float* %k, align 4
  %446 = load float, float* %limit_err, align 4
  %mul575 = fmul float %445, %446
  %447 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError576 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %447, i32 0, i32 7
  %448 = load float*, float** %m_constraintError576, align 4
  %449 = load i32, i32* %srow, align 4
  %arrayidx577 = getelementptr inbounds float, float* %448, i32 %449
  %450 = load float, float* %arrayidx577, align 4
  %add578 = fadd float %450, %mul575
  store float %add578, float* %arrayidx577, align 4
  %m_flags579 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %451 = load i32, i32* %m_flags579, align 4
  %and580 = and i32 %451, 256
  %tobool581 = icmp ne i32 %and580, 0
  br i1 %tobool581, label %if.then582, label %if.end585

if.then582:                                       ; preds = %if.then572
  %m_cfmLimLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 21
  %452 = load float, float* %m_cfmLimLin, align 4
  %453 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm583 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %453, i32 0, i32 8
  %454 = load float*, float** %cfm583, align 4
  %455 = load i32, i32* %srow, align 4
  %arrayidx584 = getelementptr inbounds float, float* %454, i32 %455
  store float %452, float* %arrayidx584, align 4
  br label %if.end585

if.end585:                                        ; preds = %if.then582, %if.then572
  %456 = load float, float* %lostop, align 4
  %457 = load float, float* %histop, align 4
  %cmp586 = fcmp oeq float %456, %457
  br i1 %cmp586, label %if.then587, label %if.else592

if.then587:                                       ; preds = %if.end585
  %458 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit588 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %458, i32 0, i32 9
  %459 = load float*, float** %m_lowerLimit588, align 4
  %460 = load i32, i32* %srow, align 4
  %arrayidx589 = getelementptr inbounds float, float* %459, i32 %460
  store float 0xC7EFFFFFE0000000, float* %arrayidx589, align 4
  %461 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit590 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %461, i32 0, i32 10
  %462 = load float*, float** %m_upperLimit590, align 4
  %463 = load i32, i32* %srow, align 4
  %arrayidx591 = getelementptr inbounds float, float* %462, i32 %463
  store float 0x47EFFFFFE0000000, float* %arrayidx591, align 4
  br label %if.end605

if.else592:                                       ; preds = %if.end585
  %464 = load i32, i32* %limit, align 4
  %cmp593 = icmp eq i32 %464, 1
  br i1 %cmp593, label %if.then594, label %if.else599

if.then594:                                       ; preds = %if.else592
  %465 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit595 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %465, i32 0, i32 9
  %466 = load float*, float** %m_lowerLimit595, align 4
  %467 = load i32, i32* %srow, align 4
  %arrayidx596 = getelementptr inbounds float, float* %466, i32 %467
  store float 0xC7EFFFFFE0000000, float* %arrayidx596, align 4
  %468 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit597 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %468, i32 0, i32 10
  %469 = load float*, float** %m_upperLimit597, align 4
  %470 = load i32, i32* %srow, align 4
  %arrayidx598 = getelementptr inbounds float, float* %469, i32 %470
  store float 0.000000e+00, float* %arrayidx598, align 4
  br label %if.end604

if.else599:                                       ; preds = %if.else592
  %471 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit600 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %471, i32 0, i32 9
  %472 = load float*, float** %m_lowerLimit600, align 4
  %473 = load i32, i32* %srow, align 4
  %arrayidx601 = getelementptr inbounds float, float* %472, i32 %473
  store float 0.000000e+00, float* %arrayidx601, align 4
  %474 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit602 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %474, i32 0, i32 10
  %475 = load float*, float** %m_upperLimit602, align 4
  %476 = load i32, i32* %srow, align 4
  %arrayidx603 = getelementptr inbounds float, float* %475, i32 %476
  store float 0x47EFFFFFE0000000, float* %arrayidx603, align 4
  br label %if.end604

if.end604:                                        ; preds = %if.else599, %if.then594
  br label %if.end605

if.end605:                                        ; preds = %if.end604, %if.then587
  %call606 = call float @_ZN18btSliderConstraint16getDampingLimLinEv(%class.btSliderConstraint* %this1)
  %sub607 = fsub float 1.000000e+00, %call606
  %call608 = call float @_Z6btFabsf(float %sub607)
  store float %call608, float* %bounce, align 4
  %477 = load float, float* %bounce, align 4
  %cmp609 = fcmp ogt float %477, 0.000000e+00
  br i1 %cmp609, label %if.then610, label %if.end644

if.then610:                                       ; preds = %if.end605
  %478 = load %class.btVector3*, %class.btVector3** %linVelA.addr, align 4
  %call611 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %478, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  store float %call611, float* %vel, align 4
  %479 = load %class.btVector3*, %class.btVector3** %linVelB.addr, align 4
  %call612 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %479, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  %480 = load float, float* %vel, align 4
  %sub613 = fsub float %480, %call612
  store float %sub613, float* %vel, align 4
  %481 = load float, float* %signFact, align 4
  %482 = load float, float* %vel, align 4
  %mul614 = fmul float %482, %481
  store float %mul614, float* %vel, align 4
  %483 = load i32, i32* %limit, align 4
  %cmp615 = icmp eq i32 %483, 1
  br i1 %cmp615, label %if.then616, label %if.else629

if.then616:                                       ; preds = %if.then610
  %484 = load float, float* %vel, align 4
  %cmp617 = fcmp olt float %484, 0.000000e+00
  br i1 %cmp617, label %if.then618, label %if.end628

if.then618:                                       ; preds = %if.then616
  %485 = load float, float* %bounce, align 4
  %fneg619 = fneg float %485
  %486 = load float, float* %vel, align 4
  %mul620 = fmul float %fneg619, %486
  store float %mul620, float* %newc, align 4
  %487 = load float, float* %newc, align 4
  %488 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError621 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %488, i32 0, i32 7
  %489 = load float*, float** %m_constraintError621, align 4
  %490 = load i32, i32* %srow, align 4
  %arrayidx622 = getelementptr inbounds float, float* %489, i32 %490
  %491 = load float, float* %arrayidx622, align 4
  %cmp623 = fcmp ogt float %487, %491
  br i1 %cmp623, label %if.then624, label %if.end627

if.then624:                                       ; preds = %if.then618
  %492 = load float, float* %newc, align 4
  %493 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError625 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %493, i32 0, i32 7
  %494 = load float*, float** %m_constraintError625, align 4
  %495 = load i32, i32* %srow, align 4
  %arrayidx626 = getelementptr inbounds float, float* %494, i32 %495
  store float %492, float* %arrayidx626, align 4
  br label %if.end627

if.end627:                                        ; preds = %if.then624, %if.then618
  br label %if.end628

if.end628:                                        ; preds = %if.end627, %if.then616
  br label %if.end643

if.else629:                                       ; preds = %if.then610
  %496 = load float, float* %vel, align 4
  %cmp630 = fcmp ogt float %496, 0.000000e+00
  br i1 %cmp630, label %if.then631, label %if.end642

if.then631:                                       ; preds = %if.else629
  %497 = load float, float* %bounce, align 4
  %fneg633 = fneg float %497
  %498 = load float, float* %vel, align 4
  %mul634 = fmul float %fneg633, %498
  store float %mul634, float* %newc632, align 4
  %499 = load float, float* %newc632, align 4
  %500 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError635 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %500, i32 0, i32 7
  %501 = load float*, float** %m_constraintError635, align 4
  %502 = load i32, i32* %srow, align 4
  %arrayidx636 = getelementptr inbounds float, float* %501, i32 %502
  %503 = load float, float* %arrayidx636, align 4
  %cmp637 = fcmp olt float %499, %503
  br i1 %cmp637, label %if.then638, label %if.end641

if.then638:                                       ; preds = %if.then631
  %504 = load float, float* %newc632, align 4
  %505 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError639 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %505, i32 0, i32 7
  %506 = load float*, float** %m_constraintError639, align 4
  %507 = load i32, i32* %srow, align 4
  %arrayidx640 = getelementptr inbounds float, float* %506, i32 %507
  store float %504, float* %arrayidx640, align 4
  br label %if.end641

if.end641:                                        ; preds = %if.then638, %if.then631
  br label %if.end642

if.end642:                                        ; preds = %if.end641, %if.else629
  br label %if.end643

if.end643:                                        ; preds = %if.end642, %if.end628
  br label %if.end644

if.end644:                                        ; preds = %if.end643, %if.end605
  %call645 = call float @_ZN18btSliderConstraint17getSoftnessLimLinEv(%class.btSliderConstraint* %this1)
  %508 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError646 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %508, i32 0, i32 7
  %509 = load float*, float** %m_constraintError646, align 4
  %510 = load i32, i32* %srow, align 4
  %arrayidx647 = getelementptr inbounds float, float* %509, i32 %510
  %511 = load float, float* %arrayidx647, align 4
  %mul648 = fmul float %511, %call645
  store float %mul648, float* %arrayidx647, align 4
  br label %if.end649

if.end649:                                        ; preds = %if.end644, %if.end570
  br label %if.end650

if.end650:                                        ; preds = %if.end649, %lor.lhs.false
  store float 0.000000e+00, float* %limit_err, align 4
  store i32 0, i32* %limit, align 4
  %call651 = call zeroext i1 @_ZN18btSliderConstraint16getSolveAngLimitEv(%class.btSliderConstraint* %this1)
  br i1 %call651, label %if.then652, label %if.end656

if.then652:                                       ; preds = %if.end650
  %call653 = call float @_ZN18btSliderConstraint11getAngDepthEv(%class.btSliderConstraint* %this1)
  store float %call653, float* %limit_err, align 4
  %512 = load float, float* %limit_err, align 4
  %cmp654 = fcmp ogt float %512, 0.000000e+00
  %513 = zext i1 %cmp654 to i64
  %cond655 = select i1 %cmp654, i32 1, i32 2
  store i32 %cond655, i32* %limit, align 4
  br label %if.end656

if.end656:                                        ; preds = %if.then652, %if.end650
  store i32 0, i32* %powered, align 4
  %call657 = call zeroext i1 @_ZN18btSliderConstraint18getPoweredAngMotorEv(%class.btSliderConstraint* %this1)
  br i1 %call657, label %if.then658, label %if.end659

if.then658:                                       ; preds = %if.end656
  store i32 1, i32* %powered, align 4
  br label %if.end659

if.end659:                                        ; preds = %if.then658, %if.end656
  %514 = load i32, i32* %limit, align 4
  %tobool660 = icmp ne i32 %514, 0
  br i1 %tobool660, label %if.then663, label %lor.lhs.false661

lor.lhs.false661:                                 ; preds = %if.end659
  %515 = load i32, i32* %powered, align 4
  %tobool662 = icmp ne i32 %515, 0
  br i1 %tobool662, label %if.then663, label %if.end830

if.then663:                                       ; preds = %lor.lhs.false661, %if.end659
  %516 = load i32, i32* %nrow, align 4
  %inc664 = add nsw i32 %516, 1
  store i32 %inc664, i32* %nrow, align 4
  %517 = load i32, i32* %nrow, align 4
  %518 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %rowskip665 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %518, i32 0, i32 6
  %519 = load i32, i32* %rowskip665, align 4
  %mul666 = mul nsw i32 %517, %519
  store i32 %mul666, i32* %srow, align 4
  %call667 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx668 = getelementptr inbounds float, float* %call667, i32 0
  %520 = load float, float* %arrayidx668, align 4
  %521 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis669 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %521, i32 0, i32 3
  %522 = load float*, float** %m_J1angularAxis669, align 4
  %523 = load i32, i32* %srow, align 4
  %add670 = add nsw i32 %523, 0
  %arrayidx671 = getelementptr inbounds float, float* %522, i32 %add670
  store float %520, float* %arrayidx671, align 4
  %call672 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx673 = getelementptr inbounds float, float* %call672, i32 1
  %524 = load float, float* %arrayidx673, align 4
  %525 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis674 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %525, i32 0, i32 3
  %526 = load float*, float** %m_J1angularAxis674, align 4
  %527 = load i32, i32* %srow, align 4
  %add675 = add nsw i32 %527, 1
  %arrayidx676 = getelementptr inbounds float, float* %526, i32 %add675
  store float %524, float* %arrayidx676, align 4
  %call677 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx678 = getelementptr inbounds float, float* %call677, i32 2
  %528 = load float, float* %arrayidx678, align 4
  %529 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J1angularAxis679 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %529, i32 0, i32 3
  %530 = load float*, float** %m_J1angularAxis679, align 4
  %531 = load i32, i32* %srow, align 4
  %add680 = add nsw i32 %531, 2
  %arrayidx681 = getelementptr inbounds float, float* %530, i32 %add680
  store float %528, float* %arrayidx681, align 4
  %call682 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx683 = getelementptr inbounds float, float* %call682, i32 0
  %532 = load float, float* %arrayidx683, align 4
  %fneg684 = fneg float %532
  %533 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis685 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %533, i32 0, i32 5
  %534 = load float*, float** %m_J2angularAxis685, align 4
  %535 = load i32, i32* %srow, align 4
  %add686 = add nsw i32 %535, 0
  %arrayidx687 = getelementptr inbounds float, float* %534, i32 %add686
  store float %fneg684, float* %arrayidx687, align 4
  %call688 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx689 = getelementptr inbounds float, float* %call688, i32 1
  %536 = load float, float* %arrayidx689, align 4
  %fneg690 = fneg float %536
  %537 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis691 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %537, i32 0, i32 5
  %538 = load float*, float** %m_J2angularAxis691, align 4
  %539 = load i32, i32* %srow, align 4
  %add692 = add nsw i32 %539, 1
  %arrayidx693 = getelementptr inbounds float, float* %538, i32 %add692
  store float %fneg690, float* %arrayidx693, align 4
  %call694 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %ax1)
  %arrayidx695 = getelementptr inbounds float, float* %call694, i32 2
  %540 = load float, float* %arrayidx695, align 4
  %fneg696 = fneg float %540
  %541 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_J2angularAxis697 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %541, i32 0, i32 5
  %542 = load float*, float** %m_J2angularAxis697, align 4
  %543 = load i32, i32* %srow, align 4
  %add698 = add nsw i32 %543, 2
  %arrayidx699 = getelementptr inbounds float, float* %542, i32 %add698
  store float %fneg696, float* %arrayidx699, align 4
  %call701 = call float @_ZN18btSliderConstraint16getLowerAngLimitEv(%class.btSliderConstraint* %this1)
  store float %call701, float* %lostop700, align 4
  %call703 = call float @_ZN18btSliderConstraint16getUpperAngLimitEv(%class.btSliderConstraint* %this1)
  store float %call703, float* %histop702, align 4
  %544 = load i32, i32* %limit, align 4
  %tobool704 = icmp ne i32 %544, 0
  br i1 %tobool704, label %land.lhs.true705, label %if.end708

land.lhs.true705:                                 ; preds = %if.then663
  %545 = load float, float* %lostop700, align 4
  %546 = load float, float* %histop702, align 4
  %cmp706 = fcmp oeq float %545, %546
  br i1 %cmp706, label %if.then707, label %if.end708

if.then707:                                       ; preds = %land.lhs.true705
  store i32 0, i32* %powered, align 4
  br label %if.end708

if.end708:                                        ; preds = %if.then707, %land.lhs.true705, %if.then663
  %m_flags709 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %547 = load i32, i32* %m_flags709, align 4
  %and710 = and i32 %547, 2048
  %tobool711 = icmp ne i32 %and710, 0
  br i1 %tobool711, label %cond.true712, label %cond.false713

cond.true712:                                     ; preds = %if.end708
  %m_softnessLimAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 22
  %548 = load float, float* %m_softnessLimAng, align 4
  br label %cond.end715

cond.false713:                                    ; preds = %if.end708
  %549 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %erp714 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %549, i32 0, i32 1
  %550 = load float, float* %erp714, align 4
  br label %cond.end715

cond.end715:                                      ; preds = %cond.false713, %cond.true712
  %cond716 = phi float [ %548, %cond.true712 ], [ %550, %cond.false713 ]
  store float %cond716, float* %currERP, align 4
  %551 = load i32, i32* %powered, align 4
  %tobool717 = icmp ne i32 %551, 0
  br i1 %tobool717, label %if.then718, label %if.end746

if.then718:                                       ; preds = %cond.end715
  %m_flags719 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %552 = load i32, i32* %m_flags719, align 4
  %and720 = and i32 %552, 4
  %tobool721 = icmp ne i32 %and720, 0
  br i1 %tobool721, label %if.then722, label %if.end725

if.then722:                                       ; preds = %if.then718
  %m_cfmDirAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 17
  %553 = load float, float* %m_cfmDirAng, align 4
  %554 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm723 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %554, i32 0, i32 8
  %555 = load float*, float** %cfm723, align 4
  %556 = load i32, i32* %srow, align 4
  %arrayidx724 = getelementptr inbounds float, float* %555, i32 %556
  store float %553, float* %arrayidx724, align 4
  br label %if.end725

if.end725:                                        ; preds = %if.then722, %if.then718
  %557 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_angPos = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 52
  %558 = load float, float* %m_angPos, align 4
  %m_lowerAngLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 8
  %559 = load float, float* %m_lowerAngLimit, align 4
  %m_upperAngLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 9
  %560 = load float, float* %m_upperAngLimit, align 4
  %call727 = call float @_ZN18btSliderConstraint25getTargetAngMotorVelocityEv(%class.btSliderConstraint* %this1)
  %561 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps728 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %561, i32 0, i32 0
  %562 = load float, float* %fps728, align 4
  %563 = load float, float* %currERP, align 4
  %mul729 = fmul float %562, %563
  %call730 = call float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint* %557, float %558, float %559, float %560, float %call727, float %mul729)
  store float %call730, float* %mot_fact726, align 4
  %564 = load float, float* %mot_fact726, align 4
  %call731 = call float @_ZN18btSliderConstraint25getTargetAngMotorVelocityEv(%class.btSliderConstraint* %this1)
  %mul732 = fmul float %564, %call731
  %565 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError733 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %565, i32 0, i32 7
  %566 = load float*, float** %m_constraintError733, align 4
  %567 = load i32, i32* %srow, align 4
  %arrayidx734 = getelementptr inbounds float, float* %566, i32 %567
  store float %mul732, float* %arrayidx734, align 4
  %call735 = call float @_ZN18btSliderConstraint19getMaxAngMotorForceEv(%class.btSliderConstraint* %this1)
  %fneg736 = fneg float %call735
  %568 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps737 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %568, i32 0, i32 0
  %569 = load float, float* %fps737, align 4
  %div738 = fdiv float %fneg736, %569
  %570 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit739 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %570, i32 0, i32 9
  %571 = load float*, float** %m_lowerLimit739, align 4
  %572 = load i32, i32* %srow, align 4
  %arrayidx740 = getelementptr inbounds float, float* %571, i32 %572
  store float %div738, float* %arrayidx740, align 4
  %call741 = call float @_ZN18btSliderConstraint19getMaxAngMotorForceEv(%class.btSliderConstraint* %this1)
  %573 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps742 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %573, i32 0, i32 0
  %574 = load float, float* %fps742, align 4
  %div743 = fdiv float %call741, %574
  %575 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit744 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %575, i32 0, i32 10
  %576 = load float*, float** %m_upperLimit744, align 4
  %577 = load i32, i32* %srow, align 4
  %arrayidx745 = getelementptr inbounds float, float* %576, i32 %577
  store float %div743, float* %arrayidx745, align 4
  br label %if.end746

if.end746:                                        ; preds = %if.end725, %cond.end715
  %578 = load i32, i32* %limit, align 4
  %tobool747 = icmp ne i32 %578, 0
  br i1 %tobool747, label %if.then748, label %if.end829

if.then748:                                       ; preds = %if.end746
  %579 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps749 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %579, i32 0, i32 0
  %580 = load float, float* %fps749, align 4
  %581 = load float, float* %currERP, align 4
  %mul750 = fmul float %580, %581
  store float %mul750, float* %k, align 4
  %582 = load float, float* %k, align 4
  %583 = load float, float* %limit_err, align 4
  %mul751 = fmul float %582, %583
  %584 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError752 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %584, i32 0, i32 7
  %585 = load float*, float** %m_constraintError752, align 4
  %586 = load i32, i32* %srow, align 4
  %arrayidx753 = getelementptr inbounds float, float* %585, i32 %586
  %587 = load float, float* %arrayidx753, align 4
  %add754 = fadd float %587, %mul751
  store float %add754, float* %arrayidx753, align 4
  %m_flags755 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %588 = load i32, i32* %m_flags755, align 4
  %and756 = and i32 %588, 1024
  %tobool757 = icmp ne i32 %and756, 0
  br i1 %tobool757, label %if.then758, label %if.end761

if.then758:                                       ; preds = %if.then748
  %m_cfmLimAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 25
  %589 = load float, float* %m_cfmLimAng, align 4
  %590 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %cfm759 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %590, i32 0, i32 8
  %591 = load float*, float** %cfm759, align 4
  %592 = load i32, i32* %srow, align 4
  %arrayidx760 = getelementptr inbounds float, float* %591, i32 %592
  store float %589, float* %arrayidx760, align 4
  br label %if.end761

if.end761:                                        ; preds = %if.then758, %if.then748
  %593 = load float, float* %lostop700, align 4
  %594 = load float, float* %histop702, align 4
  %cmp762 = fcmp oeq float %593, %594
  br i1 %cmp762, label %if.then763, label %if.else768

if.then763:                                       ; preds = %if.end761
  %595 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit764 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %595, i32 0, i32 9
  %596 = load float*, float** %m_lowerLimit764, align 4
  %597 = load i32, i32* %srow, align 4
  %arrayidx765 = getelementptr inbounds float, float* %596, i32 %597
  store float 0xC7EFFFFFE0000000, float* %arrayidx765, align 4
  %598 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit766 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %598, i32 0, i32 10
  %599 = load float*, float** %m_upperLimit766, align 4
  %600 = load i32, i32* %srow, align 4
  %arrayidx767 = getelementptr inbounds float, float* %599, i32 %600
  store float 0x47EFFFFFE0000000, float* %arrayidx767, align 4
  br label %if.end781

if.else768:                                       ; preds = %if.end761
  %601 = load i32, i32* %limit, align 4
  %cmp769 = icmp eq i32 %601, 1
  br i1 %cmp769, label %if.then770, label %if.else775

if.then770:                                       ; preds = %if.else768
  %602 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit771 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %602, i32 0, i32 9
  %603 = load float*, float** %m_lowerLimit771, align 4
  %604 = load i32, i32* %srow, align 4
  %arrayidx772 = getelementptr inbounds float, float* %603, i32 %604
  store float 0.000000e+00, float* %arrayidx772, align 4
  %605 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit773 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %605, i32 0, i32 10
  %606 = load float*, float** %m_upperLimit773, align 4
  %607 = load i32, i32* %srow, align 4
  %arrayidx774 = getelementptr inbounds float, float* %606, i32 %607
  store float 0x47EFFFFFE0000000, float* %arrayidx774, align 4
  br label %if.end780

if.else775:                                       ; preds = %if.else768
  %608 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_lowerLimit776 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %608, i32 0, i32 9
  %609 = load float*, float** %m_lowerLimit776, align 4
  %610 = load i32, i32* %srow, align 4
  %arrayidx777 = getelementptr inbounds float, float* %609, i32 %610
  store float 0xC7EFFFFFE0000000, float* %arrayidx777, align 4
  %611 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_upperLimit778 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %611, i32 0, i32 10
  %612 = load float*, float** %m_upperLimit778, align 4
  %613 = load i32, i32* %srow, align 4
  %arrayidx779 = getelementptr inbounds float, float* %612, i32 %613
  store float 0.000000e+00, float* %arrayidx779, align 4
  br label %if.end780

if.end780:                                        ; preds = %if.else775, %if.then770
  br label %if.end781

if.end781:                                        ; preds = %if.end780, %if.then763
  %call783 = call float @_ZN18btSliderConstraint16getDampingLimAngEv(%class.btSliderConstraint* %this1)
  %sub784 = fsub float 1.000000e+00, %call783
  %call785 = call float @_Z6btFabsf(float %sub784)
  store float %call785, float* %bounce782, align 4
  %614 = load float, float* %bounce782, align 4
  %cmp786 = fcmp ogt float %614, 0.000000e+00
  br i1 %cmp786, label %if.then787, label %if.end824

if.then787:                                       ; preds = %if.end781
  %615 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %615, i32 0, i32 8
  %616 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call789 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %616)
  %call790 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call789, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  store float %call790, float* %vel788, align 4
  %617 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %617, i32 0, i32 9
  %618 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call791 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %618)
  %call792 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call791, %class.btVector3* nonnull align 4 dereferenceable(16) %ax1)
  %619 = load float, float* %vel788, align 4
  %sub793 = fsub float %619, %call792
  store float %sub793, float* %vel788, align 4
  %620 = load i32, i32* %limit, align 4
  %cmp794 = icmp eq i32 %620, 1
  br i1 %cmp794, label %if.then795, label %if.else809

if.then795:                                       ; preds = %if.then787
  %621 = load float, float* %vel788, align 4
  %cmp796 = fcmp olt float %621, 0.000000e+00
  br i1 %cmp796, label %if.then797, label %if.end808

if.then797:                                       ; preds = %if.then795
  %622 = load float, float* %bounce782, align 4
  %fneg799 = fneg float %622
  %623 = load float, float* %vel788, align 4
  %mul800 = fmul float %fneg799, %623
  store float %mul800, float* %newc798, align 4
  %624 = load float, float* %newc798, align 4
  %625 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError801 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %625, i32 0, i32 7
  %626 = load float*, float** %m_constraintError801, align 4
  %627 = load i32, i32* %srow, align 4
  %arrayidx802 = getelementptr inbounds float, float* %626, i32 %627
  %628 = load float, float* %arrayidx802, align 4
  %cmp803 = fcmp ogt float %624, %628
  br i1 %cmp803, label %if.then804, label %if.end807

if.then804:                                       ; preds = %if.then797
  %629 = load float, float* %newc798, align 4
  %630 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError805 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %630, i32 0, i32 7
  %631 = load float*, float** %m_constraintError805, align 4
  %632 = load i32, i32* %srow, align 4
  %arrayidx806 = getelementptr inbounds float, float* %631, i32 %632
  store float %629, float* %arrayidx806, align 4
  br label %if.end807

if.end807:                                        ; preds = %if.then804, %if.then797
  br label %if.end808

if.end808:                                        ; preds = %if.end807, %if.then795
  br label %if.end823

if.else809:                                       ; preds = %if.then787
  %633 = load float, float* %vel788, align 4
  %cmp810 = fcmp ogt float %633, 0.000000e+00
  br i1 %cmp810, label %if.then811, label %if.end822

if.then811:                                       ; preds = %if.else809
  %634 = load float, float* %bounce782, align 4
  %fneg813 = fneg float %634
  %635 = load float, float* %vel788, align 4
  %mul814 = fmul float %fneg813, %635
  store float %mul814, float* %newc812, align 4
  %636 = load float, float* %newc812, align 4
  %637 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError815 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %637, i32 0, i32 7
  %638 = load float*, float** %m_constraintError815, align 4
  %639 = load i32, i32* %srow, align 4
  %arrayidx816 = getelementptr inbounds float, float* %638, i32 %639
  %640 = load float, float* %arrayidx816, align 4
  %cmp817 = fcmp olt float %636, %640
  br i1 %cmp817, label %if.then818, label %if.end821

if.then818:                                       ; preds = %if.then811
  %641 = load float, float* %newc812, align 4
  %642 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError819 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %642, i32 0, i32 7
  %643 = load float*, float** %m_constraintError819, align 4
  %644 = load i32, i32* %srow, align 4
  %arrayidx820 = getelementptr inbounds float, float* %643, i32 %644
  store float %641, float* %arrayidx820, align 4
  br label %if.end821

if.end821:                                        ; preds = %if.then818, %if.then811
  br label %if.end822

if.end822:                                        ; preds = %if.end821, %if.else809
  br label %if.end823

if.end823:                                        ; preds = %if.end822, %if.end808
  br label %if.end824

if.end824:                                        ; preds = %if.end823, %if.end781
  %call825 = call float @_ZN18btSliderConstraint17getSoftnessLimAngEv(%class.btSliderConstraint* %this1)
  %645 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_constraintError826 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %645, i32 0, i32 7
  %646 = load float*, float** %m_constraintError826, align 4
  %647 = load i32, i32* %srow, align 4
  %arrayidx827 = getelementptr inbounds float, float* %646, i32 %647
  %648 = load float, float* %arrayidx827, align 4
  %mul828 = fmul float %648, %call825
  store float %mul828, float* %arrayidx827, align 4
  br label %if.end829

if.end829:                                        ; preds = %if.end824, %if.end746
  br label %if.end830

if.end830:                                        ; preds = %if.end829, %lor.lhs.false661
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody17getLinearVelocityEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_linearVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 2
  ret %class.btVector3* %m_linearVelocity
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btRigidBody10getInvMassEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_inverseMass = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 4
  %0 = load float, float* %m_inverseMass, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %2 = load i32, i32* %i.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z7btAtan2ff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %call = call float @atan2f(float %0, float %1) #7
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_Z21btAdjustAngleToLimitsfff(float %angleInRadians, float %angleLowerLimitInRadians, float %angleUpperLimitInRadians) #2 comdat {
entry:
  %retval = alloca float, align 4
  %angleInRadians.addr = alloca float, align 4
  %angleLowerLimitInRadians.addr = alloca float, align 4
  %angleUpperLimitInRadians.addr = alloca float, align 4
  %diffLo = alloca float, align 4
  %diffHi = alloca float, align 4
  %diffHi11 = alloca float, align 4
  %diffLo15 = alloca float, align 4
  store float %angleInRadians, float* %angleInRadians.addr, align 4
  store float %angleLowerLimitInRadians, float* %angleLowerLimitInRadians.addr, align 4
  store float %angleUpperLimitInRadians, float* %angleUpperLimitInRadians.addr, align 4
  %0 = load float, float* %angleLowerLimitInRadians.addr, align 4
  %1 = load float, float* %angleUpperLimitInRadians.addr, align 4
  %cmp = fcmp oge float %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load float, float* %angleInRadians.addr, align 4
  store float %2, float* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %3 = load float, float* %angleInRadians.addr, align 4
  %4 = load float, float* %angleLowerLimitInRadians.addr, align 4
  %cmp1 = fcmp olt float %3, %4
  br i1 %cmp1, label %if.then2, label %if.else8

if.then2:                                         ; preds = %if.else
  %5 = load float, float* %angleLowerLimitInRadians.addr, align 4
  %6 = load float, float* %angleInRadians.addr, align 4
  %sub = fsub float %5, %6
  %call = call float @_Z16btNormalizeAnglef(float %sub)
  %call3 = call float @_Z6btFabsf(float %call)
  store float %call3, float* %diffLo, align 4
  %7 = load float, float* %angleUpperLimitInRadians.addr, align 4
  %8 = load float, float* %angleInRadians.addr, align 4
  %sub4 = fsub float %7, %8
  %call5 = call float @_Z16btNormalizeAnglef(float %sub4)
  %call6 = call float @_Z6btFabsf(float %call5)
  store float %call6, float* %diffHi, align 4
  %9 = load float, float* %diffLo, align 4
  %10 = load float, float* %diffHi, align 4
  %cmp7 = fcmp olt float %9, %10
  br i1 %cmp7, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then2
  %11 = load float, float* %angleInRadians.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.then2
  %12 = load float, float* %angleInRadians.addr, align 4
  %add = fadd float %12, 0x401921FB60000000
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %11, %cond.true ], [ %add, %cond.false ]
  store float %cond, float* %retval, align 4
  br label %return

if.else8:                                         ; preds = %if.else
  %13 = load float, float* %angleInRadians.addr, align 4
  %14 = load float, float* %angleUpperLimitInRadians.addr, align 4
  %cmp9 = fcmp ogt float %13, %14
  br i1 %cmp9, label %if.then10, label %if.else25

if.then10:                                        ; preds = %if.else8
  %15 = load float, float* %angleInRadians.addr, align 4
  %16 = load float, float* %angleUpperLimitInRadians.addr, align 4
  %sub12 = fsub float %15, %16
  %call13 = call float @_Z16btNormalizeAnglef(float %sub12)
  %call14 = call float @_Z6btFabsf(float %call13)
  store float %call14, float* %diffHi11, align 4
  %17 = load float, float* %angleInRadians.addr, align 4
  %18 = load float, float* %angleLowerLimitInRadians.addr, align 4
  %sub16 = fsub float %17, %18
  %call17 = call float @_Z16btNormalizeAnglef(float %sub16)
  %call18 = call float @_Z6btFabsf(float %call17)
  store float %call18, float* %diffLo15, align 4
  %19 = load float, float* %diffLo15, align 4
  %20 = load float, float* %diffHi11, align 4
  %cmp19 = fcmp olt float %19, %20
  br i1 %cmp19, label %cond.true20, label %cond.false22

cond.true20:                                      ; preds = %if.then10
  %21 = load float, float* %angleInRadians.addr, align 4
  %sub21 = fsub float %21, 0x401921FB60000000
  br label %cond.end23

cond.false22:                                     ; preds = %if.then10
  %22 = load float, float* %angleInRadians.addr, align 4
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false22, %cond.true20
  %cond24 = phi float [ %sub21, %cond.true20 ], [ %22, %cond.false22 ]
  store float %cond24, float* %retval, align 4
  br label %return

if.else25:                                        ; preds = %if.else8
  %23 = load float, float* %angleInRadians.addr, align 4
  store float %23, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.else25, %cond.end23, %cond.end, %if.then
  %24 = load float, float* %retval, align 4
  ret float %24
}

; Function Attrs: noinline optnone
define hidden void @_ZN18btSliderConstraint11getAncorInAEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btSliderConstraint* %this) #2 {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btTransform, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %agg.result)
  %m_realPivotAInW = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 44
  %m_lowerLinLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 6
  %0 = load float, float* %m_lowerLinLimit, align 4
  %m_upperLinLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 7
  %1 = load float, float* %m_upperLinLimit, align 4
  %add = fadd float %0, %1
  %mul = fmul float %add, 5.000000e-01
  store float %mul, float* %ref.tmp3, align 4
  %m_sliderAxis = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 43
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_sliderAxis)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_realPivotAInW, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %4 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %4, i32 0, i32 8
  %5 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call6 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %5)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp5, %class.btTransform* %call6)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, %class.btTransform* %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %agg.result)
  %6 = bitcast %class.btVector3* %agg.result to i8*
  %7 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN18btSliderConstraint11getAncorInBEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btSliderConstraint* %this) #2 {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %agg.result)
  %m_frameInB = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_frameInB)
  %0 = bitcast %class.btVector3* %agg.result to i8*
  %1 = bitcast %class.btVector3* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK18btSliderConstraint23getCalculatedTransformAEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_calculatedTransformA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 41
  ret %class.btTransform* %m_calculatedTransformA
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK18btSliderConstraint23getCalculatedTransformBEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_calculatedTransformB = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 42
  ret %class.btTransform* %m_calculatedTransformB
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #1 comdat {
entry:
  %n.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  %a = alloca float, align 4
  %k = alloca float, align 4
  %a42 = alloca float, align 4
  %k54 = alloca float, align 4
  store %class.btVector3* %n, %class.btVector3** %n.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 2
  %1 = load float, float* %arrayidx, align 4
  %call1 = call float @_Z6btFabsf(float %1)
  %cmp = fcmp ogt float %call1, 0x3FE6A09E60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %4 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %mul = fmul float %3, %5
  %6 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %7 = load float, float* %arrayidx7, align 4
  %8 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %9 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %7, %9
  %add = fadd float %mul, %mul10
  store float %add, float* %a, align 4
  %10 = load float, float* %a, align 4
  %call11 = call float @_Z6btSqrtf(float %10)
  %div = fdiv float 1.000000e+00, %call11
  store float %div, float* %k, align 4
  %11 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float 0.000000e+00, float* %arrayidx13, align 4
  %12 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %12)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %13 = load float, float* %arrayidx15, align 4
  %fneg = fneg float %13
  %14 = load float, float* %k, align 4
  %mul16 = fmul float %fneg, %14
  %15 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  store float %mul16, float* %arrayidx18, align 4
  %16 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %16)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %17 = load float, float* %arrayidx20, align 4
  %18 = load float, float* %k, align 4
  %mul21 = fmul float %17, %18
  %19 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %19)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 2
  store float %mul21, float* %arrayidx23, align 4
  %20 = load float, float* %a, align 4
  %21 = load float, float* %k, align 4
  %mul24 = fmul float %20, %21
  %22 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %22)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %mul24, float* %arrayidx26, align 4
  %23 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %23)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  %24 = load float, float* %arrayidx28, align 4
  %fneg29 = fneg float %24
  %25 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %25)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %26 = load float, float* %arrayidx31, align 4
  %mul32 = fmul float %fneg29, %26
  %27 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float %mul32, float* %arrayidx34, align 4
  %28 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %28)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %29 = load float, float* %arrayidx36, align 4
  %30 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %30)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %31 = load float, float* %arrayidx38, align 4
  %mul39 = fmul float %29, %31
  %32 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  store float %mul39, float* %arrayidx41, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %33 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %33)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 0
  %34 = load float, float* %arrayidx44, align 4
  %35 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %35)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %36 = load float, float* %arrayidx46, align 4
  %mul47 = fmul float %34, %36
  %37 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call48 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %37)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 1
  %38 = load float, float* %arrayidx49, align 4
  %39 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %39)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %40 = load float, float* %arrayidx51, align 4
  %mul52 = fmul float %38, %40
  %add53 = fadd float %mul47, %mul52
  store float %add53, float* %a42, align 4
  %41 = load float, float* %a42, align 4
  %call55 = call float @_Z6btSqrtf(float %41)
  %div56 = fdiv float 1.000000e+00, %call55
  store float %div56, float* %k54, align 4
  %42 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %43 = load float, float* %arrayidx58, align 4
  %fneg59 = fneg float %43
  %44 = load float, float* %k54, align 4
  %mul60 = fmul float %fneg59, %44
  %45 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %45)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 0
  store float %mul60, float* %arrayidx62, align 4
  %46 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %46)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 0
  %47 = load float, float* %arrayidx64, align 4
  %48 = load float, float* %k54, align 4
  %mul65 = fmul float %47, %48
  %49 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %49)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %mul65, float* %arrayidx67, align 4
  %50 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %50)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  store float 0.000000e+00, float* %arrayidx69, align 4
  %51 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %51)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 2
  %52 = load float, float* %arrayidx71, align 4
  %fneg72 = fneg float %52
  %53 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %53)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 1
  %54 = load float, float* %arrayidx74, align 4
  %mul75 = fmul float %fneg72, %54
  %55 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  store float %mul75, float* %arrayidx77, align 4
  %56 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %56)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 2
  %57 = load float, float* %arrayidx79, align 4
  %58 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %58)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %59 = load float, float* %arrayidx81, align 4
  %mul82 = fmul float %57, %59
  %60 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %60)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  store float %mul82, float* %arrayidx84, align 4
  %61 = load float, float* %a42, align 4
  %62 = load float, float* %k54, align 4
  %mul85 = fmul float %61, %62
  %63 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %63)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  store float %mul85, float* %arrayidx87, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint11getLinDepthEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_depth = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 48
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_depth)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %0 = load float, float* %arrayidx, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint16getLowerLinLimitEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_lowerLinLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 6
  %0 = load float, float* %m_lowerLinLimit, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint16getUpperLinLimitEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_upperLinLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 7
  %0 = load float, float* %m_upperLinLimit, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint25getTargetLinMotorVelocityEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_targetLinMotorVelocity = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 56
  %0 = load float, float* %m_targetLinMotorVelocity, align 4
  ret float %0
}

declare float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint*, float, float, float, float, float) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint19getMaxLinMotorForceEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_maxLinMotorForce = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 57
  %0 = load float, float* %m_maxLinMotorForce, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint16getDampingLimLinEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_dampingLimLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 20
  %0 = load float, float* %m_dampingLimLin, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint17getSoftnessLimLinEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_softnessLimLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 18
  %0 = load float, float* %m_softnessLimLin, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint11getAngDepthEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_angDepth = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 53
  %0 = load float, float* %m_angDepth, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint16getLowerAngLimitEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_lowerAngLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 8
  %0 = load float, float* %m_lowerAngLimit, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint16getUpperAngLimitEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_upperAngLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 9
  %0 = load float, float* %m_upperAngLimit, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint25getTargetAngMotorVelocityEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_targetAngMotorVelocity = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 60
  %0 = load float, float* %m_targetAngMotorVelocity, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint19getMaxAngMotorForceEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_maxAngMotorForce = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 61
  %0 = load float, float* %m_maxAngMotorForce, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint16getDampingLimAngEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_dampingLimAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 24
  %0 = load float, float* %m_dampingLimAng, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btRigidBody18getAngularVelocityEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_angularVelocity = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 3
  ret %class.btVector3* %m_angularVelocity
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN18btSliderConstraint17getSoftnessLimAngEv(%class.btSliderConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_softnessLimAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 22
  %0 = load float, float* %m_softnessLimAng, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN18btSliderConstraint8setParamEifi(%class.btSliderConstraint* %this, i32 %num, float %value, i32 %axis) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  %num.addr = alloca i32, align 4
  %value.addr = alloca float, align 4
  %axis.addr = alloca i32, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  store i32 %num, i32* %num.addr, align 4
  store float %value, float* %value.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %0 = load i32, i32* %num.addr, align 4
  switch i32 %0, label %sw.epilog [
    i32 2, label %sw.bb
    i32 3, label %sw.bb20
    i32 4, label %sw.bb33
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i32, i32* %axis.addr, align 4
  %cmp = icmp slt i32 %1, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %sw.bb
  %2 = load float, float* %value.addr, align 4
  %m_softnessLimLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 18
  store float %2, float* %m_softnessLimLin, align 4
  %m_flags = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %3 = load i32, i32* %m_flags, align 4
  %or = or i32 %3, 512
  store i32 %or, i32* %m_flags, align 4
  br label %if.end19

if.else:                                          ; preds = %sw.bb
  %4 = load i32, i32* %axis.addr, align 4
  %cmp2 = icmp slt i32 %4, 3
  br i1 %cmp2, label %if.then3, label %if.else6

if.then3:                                         ; preds = %if.else
  %5 = load float, float* %value.addr, align 4
  %m_softnessOrthoLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 26
  store float %5, float* %m_softnessOrthoLin, align 4
  %m_flags4 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %6 = load i32, i32* %m_flags4, align 4
  %or5 = or i32 %6, 32
  store i32 %or5, i32* %m_flags4, align 4
  br label %if.end18

if.else6:                                         ; preds = %if.else
  %7 = load i32, i32* %axis.addr, align 4
  %cmp7 = icmp eq i32 %7, 3
  br i1 %cmp7, label %if.then8, label %if.else11

if.then8:                                         ; preds = %if.else6
  %8 = load float, float* %value.addr, align 4
  %m_softnessLimAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 22
  store float %8, float* %m_softnessLimAng, align 4
  %m_flags9 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %9 = load i32, i32* %m_flags9, align 4
  %or10 = or i32 %9, 2048
  store i32 %or10, i32* %m_flags9, align 4
  br label %if.end17

if.else11:                                        ; preds = %if.else6
  %10 = load i32, i32* %axis.addr, align 4
  %cmp12 = icmp slt i32 %10, 6
  br i1 %cmp12, label %if.then13, label %if.else16

if.then13:                                        ; preds = %if.else11
  %11 = load float, float* %value.addr, align 4
  %m_softnessOrthoAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 30
  store float %11, float* %m_softnessOrthoAng, align 4
  %m_flags14 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %12 = load i32, i32* %m_flags14, align 4
  %or15 = or i32 %12, 128
  store i32 %or15, i32* %m_flags14, align 4
  br label %if.end

if.else16:                                        ; preds = %if.else11
  br label %if.end

if.end:                                           ; preds = %if.else16, %if.then13
  br label %if.end17

if.end17:                                         ; preds = %if.end, %if.then8
  br label %if.end18

if.end18:                                         ; preds = %if.end17, %if.then3
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %if.then
  br label %sw.epilog

sw.bb20:                                          ; preds = %entry
  %13 = load i32, i32* %axis.addr, align 4
  %cmp21 = icmp slt i32 %13, 1
  br i1 %cmp21, label %if.then22, label %if.else25

if.then22:                                        ; preds = %sw.bb20
  %14 = load float, float* %value.addr, align 4
  %m_cfmDirLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 13
  store float %14, float* %m_cfmDirLin, align 4
  %m_flags23 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %15 = load i32, i32* %m_flags23, align 4
  %or24 = or i32 %15, 1
  store i32 %or24, i32* %m_flags23, align 4
  br label %if.end32

if.else25:                                        ; preds = %sw.bb20
  %16 = load i32, i32* %axis.addr, align 4
  %cmp26 = icmp eq i32 %16, 3
  br i1 %cmp26, label %if.then27, label %if.else30

if.then27:                                        ; preds = %if.else25
  %17 = load float, float* %value.addr, align 4
  %m_cfmDirAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 17
  store float %17, float* %m_cfmDirAng, align 4
  %m_flags28 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %18 = load i32, i32* %m_flags28, align 4
  %or29 = or i32 %18, 4
  store i32 %or29, i32* %m_flags28, align 4
  br label %if.end31

if.else30:                                        ; preds = %if.else25
  br label %if.end31

if.end31:                                         ; preds = %if.else30, %if.then27
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %if.then22
  br label %sw.epilog

sw.bb33:                                          ; preds = %entry
  %19 = load i32, i32* %axis.addr, align 4
  %cmp34 = icmp slt i32 %19, 1
  br i1 %cmp34, label %if.then35, label %if.else38

if.then35:                                        ; preds = %sw.bb33
  %20 = load float, float* %value.addr, align 4
  %m_cfmLimLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 21
  store float %20, float* %m_cfmLimLin, align 4
  %m_flags36 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %21 = load i32, i32* %m_flags36, align 4
  %or37 = or i32 %21, 256
  store i32 %or37, i32* %m_flags36, align 4
  br label %if.end57

if.else38:                                        ; preds = %sw.bb33
  %22 = load i32, i32* %axis.addr, align 4
  %cmp39 = icmp slt i32 %22, 3
  br i1 %cmp39, label %if.then40, label %if.else43

if.then40:                                        ; preds = %if.else38
  %23 = load float, float* %value.addr, align 4
  %m_cfmOrthoLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 29
  store float %23, float* %m_cfmOrthoLin, align 4
  %m_flags41 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %24 = load i32, i32* %m_flags41, align 4
  %or42 = or i32 %24, 16
  store i32 %or42, i32* %m_flags41, align 4
  br label %if.end56

if.else43:                                        ; preds = %if.else38
  %25 = load i32, i32* %axis.addr, align 4
  %cmp44 = icmp eq i32 %25, 3
  br i1 %cmp44, label %if.then45, label %if.else48

if.then45:                                        ; preds = %if.else43
  %26 = load float, float* %value.addr, align 4
  %m_cfmLimAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 25
  store float %26, float* %m_cfmLimAng, align 4
  %m_flags46 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %27 = load i32, i32* %m_flags46, align 4
  %or47 = or i32 %27, 1024
  store i32 %or47, i32* %m_flags46, align 4
  br label %if.end55

if.else48:                                        ; preds = %if.else43
  %28 = load i32, i32* %axis.addr, align 4
  %cmp49 = icmp slt i32 %28, 6
  br i1 %cmp49, label %if.then50, label %if.else53

if.then50:                                        ; preds = %if.else48
  %29 = load float, float* %value.addr, align 4
  %m_cfmOrthoAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 33
  store float %29, float* %m_cfmOrthoAng, align 4
  %m_flags51 = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %30 = load i32, i32* %m_flags51, align 4
  %or52 = or i32 %30, 64
  store i32 %or52, i32* %m_flags51, align 4
  br label %if.end54

if.else53:                                        ; preds = %if.else48
  br label %if.end54

if.end54:                                         ; preds = %if.else53, %if.then50
  br label %if.end55

if.end55:                                         ; preds = %if.end54, %if.then45
  br label %if.end56

if.end56:                                         ; preds = %if.end55, %if.then40
  br label %if.end57

if.end57:                                         ; preds = %if.end56, %if.then35
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %if.end57, %if.end32, %if.end19
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZNK18btSliderConstraint8getParamEii(%class.btSliderConstraint* %this, i32 %num, i32 %axis) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  %num.addr = alloca i32, align 4
  %axis.addr = alloca i32, align 4
  %retVal = alloca float, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  store i32 %num, i32* %num.addr, align 4
  store i32 %axis, i32* %axis.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  store float 0x47EFFFFFE0000000, float* %retVal, align 4
  %0 = load i32, i32* %num.addr, align 4
  switch i32 %0, label %sw.epilog [
    i32 2, label %sw.bb
    i32 3, label %sw.bb14
    i32 4, label %sw.bb23
  ]

sw.bb:                                            ; preds = %entry
  %1 = load i32, i32* %axis.addr, align 4
  %cmp = icmp slt i32 %1, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %sw.bb
  %m_softnessLimLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 18
  %2 = load float, float* %m_softnessLimLin, align 4
  store float %2, float* %retVal, align 4
  br label %if.end13

if.else:                                          ; preds = %sw.bb
  %3 = load i32, i32* %axis.addr, align 4
  %cmp2 = icmp slt i32 %3, 3
  br i1 %cmp2, label %if.then3, label %if.else4

if.then3:                                         ; preds = %if.else
  %m_softnessOrthoLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 26
  %4 = load float, float* %m_softnessOrthoLin, align 4
  store float %4, float* %retVal, align 4
  br label %if.end12

if.else4:                                         ; preds = %if.else
  %5 = load i32, i32* %axis.addr, align 4
  %cmp5 = icmp eq i32 %5, 3
  br i1 %cmp5, label %if.then6, label %if.else7

if.then6:                                         ; preds = %if.else4
  %m_softnessLimAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 22
  %6 = load float, float* %m_softnessLimAng, align 4
  store float %6, float* %retVal, align 4
  br label %if.end11

if.else7:                                         ; preds = %if.else4
  %7 = load i32, i32* %axis.addr, align 4
  %cmp8 = icmp slt i32 %7, 6
  br i1 %cmp8, label %if.then9, label %if.else10

if.then9:                                         ; preds = %if.else7
  %m_softnessOrthoAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 30
  %8 = load float, float* %m_softnessOrthoAng, align 4
  store float %8, float* %retVal, align 4
  br label %if.end

if.else10:                                        ; preds = %if.else7
  br label %if.end

if.end:                                           ; preds = %if.else10, %if.then9
  br label %if.end11

if.end11:                                         ; preds = %if.end, %if.then6
  br label %if.end12

if.end12:                                         ; preds = %if.end11, %if.then3
  br label %if.end13

if.end13:                                         ; preds = %if.end12, %if.then
  br label %sw.epilog

sw.bb14:                                          ; preds = %entry
  %9 = load i32, i32* %axis.addr, align 4
  %cmp15 = icmp slt i32 %9, 1
  br i1 %cmp15, label %if.then16, label %if.else17

if.then16:                                        ; preds = %sw.bb14
  %m_cfmDirLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 13
  %10 = load float, float* %m_cfmDirLin, align 4
  store float %10, float* %retVal, align 4
  br label %if.end22

if.else17:                                        ; preds = %sw.bb14
  %11 = load i32, i32* %axis.addr, align 4
  %cmp18 = icmp eq i32 %11, 3
  br i1 %cmp18, label %if.then19, label %if.else20

if.then19:                                        ; preds = %if.else17
  %m_cfmDirAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 17
  %12 = load float, float* %m_cfmDirAng, align 4
  store float %12, float* %retVal, align 4
  br label %if.end21

if.else20:                                        ; preds = %if.else17
  br label %if.end21

if.end21:                                         ; preds = %if.else20, %if.then19
  br label %if.end22

if.end22:                                         ; preds = %if.end21, %if.then16
  br label %sw.epilog

sw.bb23:                                          ; preds = %entry
  %13 = load i32, i32* %axis.addr, align 4
  %cmp24 = icmp slt i32 %13, 1
  br i1 %cmp24, label %if.then25, label %if.else26

if.then25:                                        ; preds = %sw.bb23
  %m_cfmLimLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 21
  %14 = load float, float* %m_cfmLimLin, align 4
  store float %14, float* %retVal, align 4
  br label %if.end39

if.else26:                                        ; preds = %sw.bb23
  %15 = load i32, i32* %axis.addr, align 4
  %cmp27 = icmp slt i32 %15, 3
  br i1 %cmp27, label %if.then28, label %if.else29

if.then28:                                        ; preds = %if.else26
  %m_cfmOrthoLin = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 29
  %16 = load float, float* %m_cfmOrthoLin, align 4
  store float %16, float* %retVal, align 4
  br label %if.end38

if.else29:                                        ; preds = %if.else26
  %17 = load i32, i32* %axis.addr, align 4
  %cmp30 = icmp eq i32 %17, 3
  br i1 %cmp30, label %if.then31, label %if.else32

if.then31:                                        ; preds = %if.else29
  %m_cfmLimAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 25
  %18 = load float, float* %m_cfmLimAng, align 4
  store float %18, float* %retVal, align 4
  br label %if.end37

if.else32:                                        ; preds = %if.else29
  %19 = load i32, i32* %axis.addr, align 4
  %cmp33 = icmp slt i32 %19, 6
  br i1 %cmp33, label %if.then34, label %if.else35

if.then34:                                        ; preds = %if.else32
  %m_cfmOrthoAng = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 33
  %20 = load float, float* %m_cfmOrthoAng, align 4
  store float %20, float* %retVal, align 4
  br label %if.end36

if.else35:                                        ; preds = %if.else32
  br label %if.end36

if.end36:                                         ; preds = %if.else35, %if.then34
  br label %if.end37

if.end37:                                         ; preds = %if.end36, %if.then31
  br label %if.end38

if.end38:                                         ; preds = %if.end37, %if.then28
  br label %if.end39

if.end39:                                         ; preds = %if.end38, %if.then25
  br label %sw.epilog

sw.epilog:                                        ; preds = %entry, %if.end39, %if.end22, %if.end13
  %21 = load float, float* %retVal, align 4
  ret float %21
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btSliderConstraint* @_ZN18btSliderConstraintD2Ev(%class.btSliderConstraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %0 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* %0) #8
  ret %class.btSliderConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btSliderConstraintD0Ev(%class.btSliderConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %call = call %class.btSliderConstraint* @_ZN18btSliderConstraintD2Ev(%class.btSliderConstraint* %this1) #8
  %0 = bitcast %class.btSliderConstraint* %this1 to i8*
  call void @_ZN18btSliderConstraintdlEPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint13buildJacobianEv(%class.btTypedConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif(%class.btTypedConstraint* %this, %class.btAlignedObjectArray.5* nonnull align 1 %ca, i32 %solverBodyA, i32 %solverBodyB, float %timeStep) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %ca.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %solverBodyA.addr = alloca i32, align 4
  %solverBodyB.addr = alloca i32, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store %class.btAlignedObjectArray.5* %ca, %class.btAlignedObjectArray.5** %ca.addr, align 4
  store i32 %solverBodyA, i32* %solverBodyA.addr, align 4
  store i32 %solverBodyB, i32* %solverBodyB.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %ca.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f(%class.btTypedConstraint* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, float %2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %.addr = alloca %struct.btSolverBody*, align 4
  %.addr1 = alloca %struct.btSolverBody*, align 4
  %.addr2 = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store %struct.btSolverBody* %0, %struct.btSolverBody** %.addr, align 4
  store %struct.btSolverBody* %1, %struct.btSolverBody** %.addr1, align 4
  store float %2, float* %.addr2, align 4
  %this3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK18btSliderConstraint28calculateSerializeBufferSizeEv(%class.btSliderConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  ret i32 204
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNK18btSliderConstraint9serializeEPvP12btSerializer(%class.btSliderConstraint* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %sliderData = alloca %struct.btSliderConstraintData*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btSliderConstraintData*
  store %struct.btSliderConstraintData* %1, %struct.btSliderConstraintData** %sliderData, align 4
  %2 = bitcast %class.btSliderConstraint* %this1 to %class.btTypedConstraint*
  %3 = load %struct.btSliderConstraintData*, %struct.btSliderConstraintData** %sliderData, align 4
  %m_typeConstraintData = getelementptr inbounds %struct.btSliderConstraintData, %struct.btSliderConstraintData* %3, i32 0, i32 0
  %4 = bitcast %struct.btTypedConstraintData* %m_typeConstraintData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint* %2, i8* %4, %class.btSerializer* %5)
  %m_frameInA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 3
  %6 = load %struct.btSliderConstraintData*, %struct.btSliderConstraintData** %sliderData, align 4
  %m_rbAFrame = getelementptr inbounds %struct.btSliderConstraintData, %struct.btSliderConstraintData* %6, i32 0, i32 1
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_frameInA, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbAFrame)
  %m_frameInB = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 4
  %7 = load %struct.btSliderConstraintData*, %struct.btSliderConstraintData** %sliderData, align 4
  %m_rbBFrame = getelementptr inbounds %struct.btSliderConstraintData, %struct.btSliderConstraintData* %7, i32 0, i32 2
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_frameInB, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbBFrame)
  %m_upperLinLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 7
  %8 = load float, float* %m_upperLinLimit, align 4
  %9 = load %struct.btSliderConstraintData*, %struct.btSliderConstraintData** %sliderData, align 4
  %m_linearUpperLimit = getelementptr inbounds %struct.btSliderConstraintData, %struct.btSliderConstraintData* %9, i32 0, i32 3
  store float %8, float* %m_linearUpperLimit, align 4
  %m_lowerLinLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 6
  %10 = load float, float* %m_lowerLinLimit, align 4
  %11 = load %struct.btSliderConstraintData*, %struct.btSliderConstraintData** %sliderData, align 4
  %m_linearLowerLimit = getelementptr inbounds %struct.btSliderConstraintData, %struct.btSliderConstraintData* %11, i32 0, i32 4
  store float %10, float* %m_linearLowerLimit, align 4
  %m_upperAngLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 9
  %12 = load float, float* %m_upperAngLimit, align 4
  %13 = load %struct.btSliderConstraintData*, %struct.btSliderConstraintData** %sliderData, align 4
  %m_angularUpperLimit = getelementptr inbounds %struct.btSliderConstraintData, %struct.btSliderConstraintData* %13, i32 0, i32 5
  store float %12, float* %m_angularUpperLimit, align 4
  %m_lowerAngLimit = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 8
  %14 = load float, float* %m_lowerAngLimit, align 4
  %15 = load %struct.btSliderConstraintData*, %struct.btSliderConstraintData** %sliderData, align 4
  %m_angularLowerLimit = getelementptr inbounds %struct.btSliderConstraintData, %struct.btSliderConstraintData* %15, i32 0, i32 6
  store float %14, float* %m_angularLowerLimit, align 4
  %m_useLinearReferenceFrameA = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 5
  %16 = load i8, i8* %m_useLinearReferenceFrameA, align 4
  %tobool = trunc i8 %16 to i1
  %conv = zext i1 %tobool to i32
  %17 = load %struct.btSliderConstraintData*, %struct.btSliderConstraintData** %sliderData, align 4
  %m_useLinearReferenceFrameA2 = getelementptr inbounds %struct.btSliderConstraintData, %struct.btSliderConstraintData* %17, i32 0, i32 7
  store i32 %conv, i32* %m_useLinearReferenceFrameA2, align 4
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 2
  %18 = load i8, i8* %m_useOffsetForConstraintFrame, align 1
  %tobool3 = trunc i8 %18 to i1
  %conv4 = zext i1 %tobool3 to i32
  %19 = load %struct.btSliderConstraintData*, %struct.btSliderConstraintData** %sliderData, align 4
  %m_useOffsetForConstraintFrame5 = getelementptr inbounds %struct.btSliderConstraintData, %struct.btSliderConstraintData* %19, i32 0, i32 8
  store i32 %conv4, i32* %m_useOffsetForConstraintFrame5, align 4
  ret i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK18btSliderConstraint8getFlagsEv(%class.btSliderConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSliderConstraint*, align 4
  store %class.btSliderConstraint* %this, %class.btSliderConstraint** %this.addr, align 4
  %this1 = load %class.btSliderConstraint*, %class.btSliderConstraint** %this.addr, align 4
  %m_flags = getelementptr inbounds %class.btSliderConstraint, %class.btSliderConstraint* %this1, i32 0, i32 36
  %0 = load i32, i32* %m_flags, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind readnone
declare float @atan2f(float, float) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_Z16btNormalizeAnglef(float %angleInRadians) #2 comdat {
entry:
  %retval = alloca float, align 4
  %angleInRadians.addr = alloca float, align 4
  store float %angleInRadians, float* %angleInRadians.addr, align 4
  %0 = load float, float* %angleInRadians.addr, align 4
  %call = call float @_Z6btFmodff(float %0, float 0x401921FB60000000)
  store float %call, float* %angleInRadians.addr, align 4
  %1 = load float, float* %angleInRadians.addr, align 4
  %cmp = fcmp olt float %1, 0xC00921FB60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load float, float* %angleInRadians.addr, align 4
  %add = fadd float %2, 0x401921FB60000000
  store float %add, float* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %3 = load float, float* %angleInRadians.addr, align 4
  %cmp1 = fcmp ogt float %3, 0x400921FB60000000
  br i1 %cmp1, label %if.then2, label %if.else3

if.then2:                                         ; preds = %if.else
  %4 = load float, float* %angleInRadians.addr, align 4
  %sub = fsub float %4, 0x401921FB60000000
  store float %sub, float* %retval, align 4
  br label %return

if.else3:                                         ; preds = %if.else
  %5 = load float, float* %angleInRadians.addr, align 4
  store float %5, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.else3, %if.then2, %if.then
  %6 = load float, float* %retval, align 4
  ret float %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFmodff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %fmod = frem float %0, %1
  ret float %fmod
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btTypedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btTypedConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btSliderConstraintdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

declare i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint*, i8*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %this, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %dataOut.addr = alloca %struct.btTransformFloatData*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %struct.btTransformFloatData* %dataOut, %struct.btTransformFloatData** %dataOut.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_basis2 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %0, i32 0, i32 0
  call void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_basis, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_origin3 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %1, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_origin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_origin3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %1
  %2 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %2, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %3
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btSliderConstraint.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind readnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind readnone }
attributes #8 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
