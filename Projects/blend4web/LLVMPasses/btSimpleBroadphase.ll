; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/BroadphaseCollision/btSimpleBroadphase.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/BroadphaseCollision/btSimpleBroadphase.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btSimpleBroadphase = type { %class.btBroadphaseInterface, i32, i32, i32, %struct.btSimpleBroadphaseProxy*, i8*, i32, %class.btOverlappingPairCache*, i8, i32 }
%class.btBroadphaseInterface = type { i32 (...)** }
%struct.btSimpleBroadphaseProxy = type { %struct.btBroadphaseProxy, i32 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btOverlappingPairCallback = type { i32 (...)** }
%class.btHashedOverlappingPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray, %struct.btOverlapFilterCallback*, %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1, %class.btOverlappingPairCallback* }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btBroadphasePair*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.0 }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%union.anon.0 = type { i8* }
%struct.btOverlapFilterCallback = type { i32 (...)** }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%struct.btBroadphaseRayCallback = type { %struct.btBroadphaseAabbCallback, %class.btVector3, [3 x i32], float }
%struct.btBroadphaseAabbCallback = type { i32 (...)** }
%class.btBroadphasePairSortPredicate = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN21btBroadphaseInterfaceC2Ev = comdat any

$_ZN28btHashedOverlappingPairCachenwEmPv = comdat any

$_ZN17btBroadphaseProxynaEmPv = comdat any

$_ZN23btSimpleBroadphaseProxyC2Ev = comdat any

$_ZN23btSimpleBroadphaseProxy11SetNextFreeEi = comdat any

$_ZN18btSimpleBroadphase11allocHandleEv = comdat any

$_ZN17btBroadphaseProxynwEmPv = comdat any

$_ZN23btSimpleBroadphaseProxyC2ERK9btVector3S2_iPvii = comdat any

$_ZN18btSimpleBroadphase10freeHandleEP23btSimpleBroadphaseProxy = comdat any

$_ZNK18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy = comdat any

$_ZN18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy = comdat any

$_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv = comdat any

$_ZN16btBroadphasePairC2Ev = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi = comdat any

$_ZeqRK16btBroadphasePairS1_ = comdat any

$_ZN18btSimpleBroadphase23getOverlappingPairCacheEv = comdat any

$_ZNK18btSimpleBroadphase23getOverlappingPairCacheEv = comdat any

$_ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_ = comdat any

$_ZN18btSimpleBroadphase10printStatsEv = comdat any

$_ZN21btBroadphaseInterfaceD2Ev = comdat any

$_ZN21btBroadphaseInterfaceD0Ev = comdat any

$_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher = comdat any

$_ZN17btBroadphaseProxyC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK23btSimpleBroadphaseProxy11GetNextFreeEv = comdat any

$_ZN17btBroadphaseProxyC2ERK9btVector3S2_Pvii = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii = comdat any

$_ZN16btBroadphasePairC2ERKS_ = comdat any

$_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi = comdat any

$_ZN16btBroadphasePairnwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_ = comdat any

$_ZTS21btBroadphaseInterface = comdat any

$_ZTI21btBroadphaseInterface = comdat any

$_ZTV21btBroadphaseInterface = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV18btSimpleBroadphase = hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI18btSimpleBroadphase to i8*), i8* bitcast (%class.btSimpleBroadphase* (%class.btSimpleBroadphase*)* @_ZN18btSimpleBroadphaseD1Ev to i8*), i8* bitcast (void (%class.btSimpleBroadphase*)* @_ZN18btSimpleBroadphaseD0Ev to i8*), i8* bitcast (%struct.btBroadphaseProxy* (%class.btSimpleBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)* @_ZN18btSimpleBroadphase11createProxyERK9btVector3S2_iPviiP12btDispatcher to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN18btSimpleBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)* @_ZN18btSimpleBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*)* @_ZNK18btSimpleBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_ to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)* @_ZN18btSimpleBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_ to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)* @_ZN18btSimpleBroadphase8aabbTestERK9btVector3S2_R24btBroadphaseAabbCallback to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %class.btDispatcher*)* @_ZN18btSimpleBroadphase25calculateOverlappingPairsEP12btDispatcher to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btSimpleBroadphase*)* @_ZN18btSimpleBroadphase23getOverlappingPairCacheEv to i8*), i8* bitcast (%class.btOverlappingPairCache* (%class.btSimpleBroadphase*)* @_ZNK18btSimpleBroadphase23getOverlappingPairCacheEv to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %class.btVector3*, %class.btVector3*)* @_ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_ to i8*), i8* bitcast (void (%class.btSimpleBroadphase*, %class.btDispatcher*)* @_ZN18btSimpleBroadphase9resetPoolEP12btDispatcher to i8*), i8* bitcast (void (%class.btSimpleBroadphase*)* @_ZN18btSimpleBroadphase10printStatsEv to i8*)] }, align 4
@gOverlappingPairs = external global i32, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS18btSimpleBroadphase = hidden constant [21 x i8] c"18btSimpleBroadphase\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS21btBroadphaseInterface = linkonce_odr hidden constant [24 x i8] c"21btBroadphaseInterface\00", comdat, align 1
@_ZTI21btBroadphaseInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btBroadphaseInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI18btSimpleBroadphase = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @_ZTS18btSimpleBroadphase, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btBroadphaseInterface to i8*) }, align 4
@_ZTV21btBroadphaseInterface = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI21btBroadphaseInterface to i8*), i8* bitcast (%class.btBroadphaseInterface* (%class.btBroadphaseInterface*)* @_ZN21btBroadphaseInterfaceD2Ev to i8*), i8* bitcast (void (%class.btBroadphaseInterface*)* @_ZN21btBroadphaseInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btBroadphaseInterface*, %class.btDispatcher*)* @_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btSimpleBroadphase.cpp, i8* null }]

@_ZN18btSimpleBroadphaseC1EiP22btOverlappingPairCache = hidden unnamed_addr alias %class.btSimpleBroadphase* (%class.btSimpleBroadphase*, i32, %class.btOverlappingPairCache*), %class.btSimpleBroadphase* (%class.btSimpleBroadphase*, i32, %class.btOverlappingPairCache*)* @_ZN18btSimpleBroadphaseC2EiP22btOverlappingPairCache
@_ZN18btSimpleBroadphaseD1Ev = hidden unnamed_addr alias %class.btSimpleBroadphase* (%class.btSimpleBroadphase*), %class.btSimpleBroadphase* (%class.btSimpleBroadphase*)* @_ZN18btSimpleBroadphaseD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN18btSimpleBroadphase8validateEv(%class.btSimpleBroadphase* %this) #1 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc6, %entry
  %0 = load i32, i32* %i, align 4
  %m_numHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_numHandles, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end8

for.body:                                         ; preds = %for.cond
  %2 = load i32, i32* %i, align 4
  %add = add nsw i32 %2, 1
  store i32 %add, i32* %j, align 4
  br label %for.cond2

for.cond2:                                        ; preds = %for.inc, %for.body
  %3 = load i32, i32* %j, align 4
  %m_numHandles3 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 1
  %4 = load i32, i32* %m_numHandles3, align 4
  %cmp4 = icmp slt i32 %3, %4
  br i1 %cmp4, label %for.body5, label %for.end

for.body5:                                        ; preds = %for.cond2
  br label %for.inc

for.inc:                                          ; preds = %for.body5
  %5 = load i32, i32* %j, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond2

for.end:                                          ; preds = %for.cond2
  br label %for.inc6

for.inc6:                                         ; preds = %for.end
  %6 = load i32, i32* %i, align 4
  %inc7 = add nsw i32 %6, 1
  store i32 %inc7, i32* %i, align 4
  br label %for.cond

for.end8:                                         ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btSimpleBroadphase* @_ZN18btSimpleBroadphaseC2EiP22btOverlappingPairCache(%class.btSimpleBroadphase* returned %this, i32 %maxProxies, %class.btOverlappingPairCache* %overlappingPairCache) unnamed_addr #2 {
entry:
  %retval = alloca %class.btSimpleBroadphase*, align 4
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %maxProxies.addr = alloca i32, align 4
  %overlappingPairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %mem = alloca i8*, align 4
  %i = alloca i32, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  store i32 %maxProxies, i32* %maxProxies.addr, align 4
  store %class.btOverlappingPairCache* %overlappingPairCache, %class.btOverlappingPairCache** %overlappingPairCache.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  store %class.btSimpleBroadphase* %this1, %class.btSimpleBroadphase** %retval, align 4
  %0 = bitcast %class.btSimpleBroadphase* %this1 to %class.btBroadphaseInterface*
  %call = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceC2Ev(%class.btBroadphaseInterface* %0) #8
  %1 = bitcast %class.btSimpleBroadphase* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV18btSimpleBroadphase, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_pairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %2 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %overlappingPairCache.addr, align 4
  store %class.btOverlappingPairCache* %2, %class.btOverlappingPairCache** %m_pairCache, align 4
  %m_ownsPairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 8
  store i8 0, i8* %m_ownsPairCache, align 4
  %m_invalidPair = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 9
  store i32 0, i32* %m_invalidPair, align 4
  %3 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %overlappingPairCache.addr, align 4
  %tobool = icmp ne %class.btOverlappingPairCache* %3, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %call2 = call i8* @_Z22btAlignedAllocInternalmi(i32 72, i32 16)
  store i8* %call2, i8** %mem, align 4
  %4 = load i8*, i8** %mem, align 4
  %call3 = call i8* @_ZN28btHashedOverlappingPairCachenwEmPv(i32 72, i8* %4)
  %5 = bitcast i8* %call3 to %class.btHashedOverlappingPairCache*
  %call4 = call %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC1Ev(%class.btHashedOverlappingPairCache* %5)
  %6 = bitcast %class.btHashedOverlappingPairCache* %5 to %class.btOverlappingPairCache*
  %m_pairCache5 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  store %class.btOverlappingPairCache* %6, %class.btOverlappingPairCache** %m_pairCache5, align 4
  %m_ownsPairCache6 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 8
  store i8 1, i8* %m_ownsPairCache6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load i32, i32* %maxProxies.addr, align 4
  %mul = mul i32 52, %7
  %call7 = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %m_pHandlesRawPtr = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 5
  store i8* %call7, i8** %m_pHandlesRawPtr, align 4
  %8 = load i32, i32* %maxProxies.addr, align 4
  %9 = call { i32, i1 } @llvm.umul.with.overflow.i32(i32 %8, i32 52)
  %10 = extractvalue { i32, i1 } %9, 1
  %11 = extractvalue { i32, i1 } %9, 0
  %12 = select i1 %10, i32 -1, i32 %11
  %m_pHandlesRawPtr8 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 5
  %13 = load i8*, i8** %m_pHandlesRawPtr8, align 4
  %call9 = call i8* @_ZN17btBroadphaseProxynaEmPv(i32 %12, i8* %13)
  %14 = bitcast i8* %call9 to %struct.btSimpleBroadphaseProxy*
  %isempty = icmp eq i32 %8, 0
  br i1 %isempty, label %arrayctor.cont, label %new.ctorloop

new.ctorloop:                                     ; preds = %if.end
  %arrayctor.end = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %14, i32 %8
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %new.ctorloop
  %arrayctor.cur = phi %struct.btSimpleBroadphaseProxy* [ %14, %new.ctorloop ], [ %arrayctor.next, %arrayctor.loop ]
  %call10 = call %struct.btSimpleBroadphaseProxy* @_ZN23btSimpleBroadphaseProxyC2Ev(%struct.btSimpleBroadphaseProxy* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %struct.btSimpleBroadphaseProxy* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %if.end, %arrayctor.loop
  %m_pHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  store %struct.btSimpleBroadphaseProxy* %14, %struct.btSimpleBroadphaseProxy** %m_pHandles, align 4
  %15 = load i32, i32* %maxProxies.addr, align 4
  %m_maxHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 2
  store i32 %15, i32* %m_maxHandles, align 4
  %m_numHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 1
  store i32 0, i32* %m_numHandles, align 4
  %m_firstFreeHandle = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 6
  store i32 0, i32* %m_firstFreeHandle, align 4
  %m_LastHandleIndex = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  store i32 -1, i32* %m_LastHandleIndex, align 4
  %m_firstFreeHandle11 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 6
  %16 = load i32, i32* %m_firstFreeHandle11, align 4
  store i32 %16, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %arrayctor.cont
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %maxProxies.addr, align 4
  %cmp = icmp slt i32 %17, %18
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles12 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %19 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles12, align 4
  %20 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %19, i32 %20
  %21 = load i32, i32* %i, align 4
  %add = add nsw i32 %21, 1
  call void @_ZN23btSimpleBroadphaseProxy11SetNextFreeEi(%struct.btSimpleBroadphaseProxy* %arrayidx, i32 %add)
  %22 = load i32, i32* %i, align 4
  %add13 = add nsw i32 %22, 2
  %m_pHandles14 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %23 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles14, align 4
  %24 = load i32, i32* %i, align 4
  %arrayidx15 = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %23, i32 %24
  %25 = bitcast %struct.btSimpleBroadphaseProxy* %arrayidx15 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %25, i32 0, i32 3
  store i32 %add13, i32* %m_uniqueId, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %26 = load i32, i32* %i, align 4
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_pHandles16 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %27 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles16, align 4
  %28 = load i32, i32* %maxProxies.addr, align 4
  %sub = sub nsw i32 %28, 1
  %arrayidx17 = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %27, i32 %sub
  call void @_ZN23btSimpleBroadphaseProxy11SetNextFreeEi(%struct.btSimpleBroadphaseProxy* %arrayidx17, i32 0)
  %29 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %retval, align 4
  ret %class.btSimpleBroadphase* %29
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceC2Ev(%class.btBroadphaseInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  %0 = bitcast %class.btBroadphaseInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV21btBroadphaseInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btBroadphaseInterface* %this1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN28btHashedOverlappingPairCachenwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

declare %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC1Ev(%class.btHashedOverlappingPairCache* returned) unnamed_addr #3

; Function Attrs: nounwind readnone speculatable willreturn
declare { i32, i1 } @llvm.umul.with.overflow.i32(i32, i32) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN17btBroadphaseProxynaEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btSimpleBroadphaseProxy* @_ZN23btSimpleBroadphaseProxyC2Ev(%struct.btSimpleBroadphaseProxy* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %struct.btSimpleBroadphaseProxy* %this, %struct.btSimpleBroadphaseProxy** %this.addr, align 4
  %this1 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %this.addr, align 4
  %0 = bitcast %struct.btSimpleBroadphaseProxy* %this1 to %struct.btBroadphaseProxy*
  %call = call %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2Ev(%struct.btBroadphaseProxy* %0)
  ret %struct.btSimpleBroadphaseProxy* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btSimpleBroadphaseProxy11SetNextFreeEi(%struct.btSimpleBroadphaseProxy* %this, i32 %next) #1 comdat {
entry:
  %this.addr = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %next.addr = alloca i32, align 4
  store %struct.btSimpleBroadphaseProxy* %this, %struct.btSimpleBroadphaseProxy** %this.addr, align 4
  store i32 %next, i32* %next.addr, align 4
  %this1 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %this.addr, align 4
  %0 = load i32, i32* %next.addr, align 4
  %m_nextFree = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %this1, i32 0, i32 1
  store i32 %0, i32* %m_nextFree, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btSimpleBroadphase* @_ZN18btSimpleBroadphaseD2Ev(%class.btSimpleBroadphase* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btSimpleBroadphase*, align 4
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  store %class.btSimpleBroadphase* %this1, %class.btSimpleBroadphase** %retval, align 4
  %0 = bitcast %class.btSimpleBroadphase* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV18btSimpleBroadphase, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_pHandlesRawPtr = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 5
  %1 = load i8*, i8** %m_pHandlesRawPtr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  %m_ownsPairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 8
  %2 = load i8, i8* %m_ownsPairCache, align 4
  %tobool = trunc i8 %2 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_pairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %3 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %4 = bitcast %class.btOverlappingPairCache* %3 to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)***
  %vtable = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)**, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*** %4, align 4
  %vfn = getelementptr inbounds %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vtable, i64 0
  %5 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vfn, align 4
  %call = call %class.btOverlappingPairCache* %5(%class.btOverlappingPairCache* %3) #8
  %m_pairCache2 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %6 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache2, align 4
  %7 = bitcast %class.btOverlappingPairCache* %6 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %7)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %8 = bitcast %class.btSimpleBroadphase* %this1 to %class.btBroadphaseInterface*
  %call3 = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceD2Ev(%class.btBroadphaseInterface* %8) #8
  %9 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %retval, align 4
  ret %class.btSimpleBroadphase* %9
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN18btSimpleBroadphaseD0Ev(%class.btSimpleBroadphase* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %call = call %class.btSimpleBroadphase* @_ZN18btSimpleBroadphaseD1Ev(%class.btSimpleBroadphase* %this1) #8
  %0 = bitcast %class.btSimpleBroadphase* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline optnone
define hidden %struct.btBroadphaseProxy* @_ZN18btSimpleBroadphase11createProxyERK9btVector3S2_iPviiP12btDispatcher(%class.btSimpleBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 %shapeType, i8* %userPtr, i32 %collisionFilterGroup, i32 %collisionFilterMask, %class.btDispatcher* %0) unnamed_addr #2 {
entry:
  %retval = alloca %struct.btBroadphaseProxy*, align 4
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %shapeType.addr = alloca i32, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i32, align 4
  %collisionFilterMask.addr = alloca i32, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %newHandleIndex = alloca i32, align 4
  %proxy = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store i32 %shapeType, i32* %shapeType.addr, align 4
  store i8* %userPtr, i8** %userPtr.addr, align 4
  store i32 %collisionFilterGroup, i32* %collisionFilterGroup.addr, align 4
  store i32 %collisionFilterMask, i32* %collisionFilterMask.addr, align 4
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %m_numHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_numHandles, align 4
  %m_maxHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_maxHandles, align 4
  %cmp = icmp sge i32 %1, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %call = call i32 @_ZN18btSimpleBroadphase11allocHandleEv(%class.btSimpleBroadphase* %this1)
  store i32 %call, i32* %newHandleIndex, align 4
  %m_pHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %3 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles, align 4
  %4 = load i32, i32* %newHandleIndex, align 4
  %arrayidx = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %3, i32 %4
  %5 = bitcast %struct.btSimpleBroadphaseProxy* %arrayidx to i8*
  %call2 = call i8* @_ZN17btBroadphaseProxynwEmPv(i32 52, i8* %5)
  %6 = bitcast i8* %call2 to %struct.btSimpleBroadphaseProxy*
  %7 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %8 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %9 = load i32, i32* %shapeType.addr, align 4
  %10 = load i8*, i8** %userPtr.addr, align 4
  %11 = load i32, i32* %collisionFilterGroup.addr, align 4
  %12 = load i32, i32* %collisionFilterMask.addr, align 4
  %call3 = call %struct.btSimpleBroadphaseProxy* @_ZN23btSimpleBroadphaseProxyC2ERK9btVector3S2_iPvii(%struct.btSimpleBroadphaseProxy* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8, i32 %9, i8* %10, i32 %11, i32 %12)
  store %struct.btSimpleBroadphaseProxy* %6, %struct.btSimpleBroadphaseProxy** %proxy, align 4
  %13 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy, align 4
  %14 = bitcast %struct.btSimpleBroadphaseProxy* %13 to %struct.btBroadphaseProxy*
  store %struct.btBroadphaseProxy* %14, %struct.btBroadphaseProxy** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %15 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %retval, align 4
  ret %struct.btBroadphaseProxy* %15
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZN18btSimpleBroadphase11allocHandleEv(%class.btSimpleBroadphase* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %freeHandle = alloca i32, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %m_firstFreeHandle = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 6
  %0 = load i32, i32* %m_firstFreeHandle, align 4
  store i32 %0, i32* %freeHandle, align 4
  %m_pHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %1 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles, align 4
  %2 = load i32, i32* %freeHandle, align 4
  %arrayidx = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %1, i32 %2
  %call = call i32 @_ZNK23btSimpleBroadphaseProxy11GetNextFreeEv(%struct.btSimpleBroadphaseProxy* %arrayidx)
  %m_firstFreeHandle2 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 6
  store i32 %call, i32* %m_firstFreeHandle2, align 4
  %m_numHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_numHandles, align 4
  %inc = add nsw i32 %3, 1
  store i32 %inc, i32* %m_numHandles, align 4
  %4 = load i32, i32* %freeHandle, align 4
  %m_LastHandleIndex = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  %5 = load i32, i32* %m_LastHandleIndex, align 4
  %cmp = icmp sgt i32 %4, %5
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load i32, i32* %freeHandle, align 4
  %m_LastHandleIndex3 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  store i32 %6, i32* %m_LastHandleIndex3, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = load i32, i32* %freeHandle, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN17btBroadphaseProxynwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btSimpleBroadphaseProxy* @_ZN23btSimpleBroadphaseProxyC2ERK9btVector3S2_iPvii(%struct.btSimpleBroadphaseProxy* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %minpt, %class.btVector3* nonnull align 4 dereferenceable(16) %maxpt, i32 %shapeType, i8* %userPtr, i32 %collisionFilterGroup, i32 %collisionFilterMask) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %minpt.addr = alloca %class.btVector3*, align 4
  %maxpt.addr = alloca %class.btVector3*, align 4
  %shapeType.addr = alloca i32, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i32, align 4
  %collisionFilterMask.addr = alloca i32, align 4
  store %struct.btSimpleBroadphaseProxy* %this, %struct.btSimpleBroadphaseProxy** %this.addr, align 4
  store %class.btVector3* %minpt, %class.btVector3** %minpt.addr, align 4
  store %class.btVector3* %maxpt, %class.btVector3** %maxpt.addr, align 4
  store i32 %shapeType, i32* %shapeType.addr, align 4
  store i8* %userPtr, i8** %userPtr.addr, align 4
  store i32 %collisionFilterGroup, i32* %collisionFilterGroup.addr, align 4
  store i32 %collisionFilterMask, i32* %collisionFilterMask.addr, align 4
  %this1 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %this.addr, align 4
  %0 = bitcast %struct.btSimpleBroadphaseProxy* %this1 to %struct.btBroadphaseProxy*
  %1 = load %class.btVector3*, %class.btVector3** %minpt.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %maxpt.addr, align 4
  %3 = load i8*, i8** %userPtr.addr, align 4
  %4 = load i32, i32* %collisionFilterGroup.addr, align 4
  %5 = load i32, i32* %collisionFilterMask.addr, align 4
  %call = call %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2ERK9btVector3S2_Pvii(%struct.btBroadphaseProxy* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i8* %3, i32 %4, i32 %5)
  ret %struct.btSimpleBroadphaseProxy* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN18btSimpleBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher(%class.btSimpleBroadphase* %this, %struct.btBroadphaseProxy* %proxyOrg, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %proxyOrg.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %proxy0 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxyOrg, %struct.btBroadphaseProxy** %proxyOrg.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxyOrg.addr, align 4
  %1 = bitcast %struct.btBroadphaseProxy* %0 to %struct.btSimpleBroadphaseProxy*
  store %struct.btSimpleBroadphaseProxy* %1, %struct.btSimpleBroadphaseProxy** %proxy0, align 4
  %2 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4
  call void @_ZN18btSimpleBroadphase10freeHandleEP23btSimpleBroadphaseProxy(%class.btSimpleBroadphase* %this1, %struct.btSimpleBroadphaseProxy* %2)
  %m_pairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %3 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %4 = bitcast %class.btOverlappingPairCache* %3 to %class.btOverlappingPairCallback*
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxyOrg.addr, align 4
  %6 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %7 = bitcast %class.btOverlappingPairCallback* %4 to void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %7, align 4
  %vfn = getelementptr inbounds void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 4
  %8 = load void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  call void %8(%class.btOverlappingPairCallback* %4, %struct.btBroadphaseProxy* %5, %class.btDispatcher* %6)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btSimpleBroadphase10freeHandleEP23btSimpleBroadphaseProxy(%class.btSimpleBroadphase* %this, %struct.btSimpleBroadphaseProxy* %proxy) #1 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %proxy.addr = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %handle = alloca i32, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  store %struct.btSimpleBroadphaseProxy* %proxy, %struct.btSimpleBroadphaseProxy** %proxy.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy.addr, align 4
  %m_pHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %1 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles, align 4
  %sub.ptr.lhs.cast = ptrtoint %struct.btSimpleBroadphaseProxy* %0 to i32
  %sub.ptr.rhs.cast = ptrtoint %struct.btSimpleBroadphaseProxy* %1 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  %sub.ptr.div = sdiv exact i32 %sub.ptr.sub, 52
  store i32 %sub.ptr.div, i32* %handle, align 4
  %2 = load i32, i32* %handle, align 4
  %m_LastHandleIndex = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  %3 = load i32, i32* %m_LastHandleIndex, align 4
  %cmp = icmp eq i32 %2, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_LastHandleIndex2 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  %4 = load i32, i32* %m_LastHandleIndex2, align 4
  %dec = add nsw i32 %4, -1
  store i32 %dec, i32* %m_LastHandleIndex2, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy.addr, align 4
  %m_firstFreeHandle = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 6
  %6 = load i32, i32* %m_firstFreeHandle, align 4
  call void @_ZN23btSimpleBroadphaseProxy11SetNextFreeEi(%struct.btSimpleBroadphaseProxy* %5, i32 %6)
  %7 = load i32, i32* %handle, align 4
  %m_firstFreeHandle3 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 6
  store i32 %7, i32* %m_firstFreeHandle3, align 4
  %8 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy.addr, align 4
  %9 = bitcast %struct.btSimpleBroadphaseProxy* %8 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %9, i32 0, i32 0
  store i8* null, i8** %m_clientObject, align 4
  %m_numHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 1
  %10 = load i32, i32* %m_numHandles, align 4
  %dec4 = add nsw i32 %10, -1
  store i32 %dec4, i32* %m_numHandles, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK18btSimpleBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_(%class.btSimpleBroadphase* %this, %struct.btBroadphaseProxy* %proxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %sbp = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %call = call %struct.btSimpleBroadphaseProxy* @_ZNK18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this1, %struct.btBroadphaseProxy* %0)
  store %struct.btSimpleBroadphaseProxy* %call, %struct.btSimpleBroadphaseProxy** %sbp, align 4
  %1 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %sbp, align 4
  %2 = bitcast %struct.btSimpleBroadphaseProxy* %1 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %2, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %4 = bitcast %class.btVector3* %3 to i8*
  %5 = bitcast %class.btVector3* %m_aabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %sbp, align 4
  %7 = bitcast %struct.btSimpleBroadphaseProxy* %6 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %7, i32 0, i32 5
  %8 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %9 = bitcast %class.btVector3* %8 to i8*
  %10 = bitcast %class.btVector3* %m_aabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btSimpleBroadphaseProxy* @_ZNK18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this, %struct.btBroadphaseProxy* %proxy) #1 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy0 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %1 = bitcast %struct.btBroadphaseProxy* %0 to %struct.btSimpleBroadphaseProxy*
  store %struct.btSimpleBroadphaseProxy* %1, %struct.btSimpleBroadphaseProxy** %proxy0, align 4
  %2 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4
  ret %struct.btSimpleBroadphaseProxy* %2
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: noinline optnone
define hidden void @_ZN18btSimpleBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher(%class.btSimpleBroadphase* %this, %struct.btBroadphaseProxy* %proxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btDispatcher* %0) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %sbp = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %call = call %struct.btSimpleBroadphaseProxy* @_ZN18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this1, %struct.btBroadphaseProxy* %1)
  store %struct.btSimpleBroadphaseProxy* %call, %struct.btSimpleBroadphaseProxy** %sbp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %3 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %sbp, align 4
  %4 = bitcast %struct.btSimpleBroadphaseProxy* %3 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %4, i32 0, i32 4
  %5 = bitcast %class.btVector3* %m_aabbMin to i8*
  %6 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %7 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %8 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %sbp, align 4
  %9 = bitcast %struct.btSimpleBroadphaseProxy* %8 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %9, i32 0, i32 5
  %10 = bitcast %class.btVector3* %m_aabbMax to i8*
  %11 = bitcast %class.btVector3* %7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btSimpleBroadphaseProxy* @_ZN18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this, %struct.btBroadphaseProxy* %proxy) #1 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy0 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %1 = bitcast %struct.btBroadphaseProxy* %0 to %struct.btSimpleBroadphaseProxy*
  store %struct.btSimpleBroadphaseProxy* %1, %struct.btSimpleBroadphaseProxy** %proxy0, align 4
  %2 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4
  ret %struct.btSimpleBroadphaseProxy* %2
}

; Function Attrs: noinline optnone
define hidden void @_ZN18btSimpleBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_(%class.btSimpleBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTo, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %rayCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayTo.addr = alloca %class.btVector3*, align 4
  %rayCallback.addr = alloca %struct.btBroadphaseRayCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %proxy = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4
  store %class.btVector3* %rayTo, %class.btVector3** %rayTo.addr, align 4
  store %struct.btBroadphaseRayCallback* %rayCallback, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_LastHandleIndex = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  %1 = load i32, i32* %m_LastHandleIndex, align 4
  %cmp = icmp sle i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %2 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %2, i32 %3
  store %struct.btSimpleBroadphaseProxy* %arrayidx, %struct.btSimpleBroadphaseProxy** %proxy, align 4
  %4 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy, align 4
  %5 = bitcast %struct.btSimpleBroadphaseProxy* %4 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 0
  %6 = load i8*, i8** %m_clientObject, align 4
  %tobool = icmp ne i8* %6, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  br label %for.inc

if.end:                                           ; preds = %for.body
  %7 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  %8 = bitcast %struct.btBroadphaseRayCallback* %7 to %struct.btBroadphaseAabbCallback*
  %9 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy, align 4
  %10 = bitcast %struct.btSimpleBroadphaseProxy* %9 to %struct.btBroadphaseProxy*
  %11 = bitcast %struct.btBroadphaseAabbCallback* %8 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %11, align 4
  %vfn = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %12 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call = call zeroext i1 %12(%struct.btBroadphaseAabbCallback* %8, %struct.btBroadphaseProxy* %10)
  br label %for.inc

for.inc:                                          ; preds = %if.end, %if.then
  %13 = load i32, i32* %i, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN18btSimpleBroadphase8aabbTestERK9btVector3S2_R24btBroadphaseAabbCallback(%class.btSimpleBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %struct.btBroadphaseAabbCallback* nonnull align 4 dereferenceable(4) %callback) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %callback.addr = alloca %struct.btBroadphaseAabbCallback*, align 4
  %i = alloca i32, align 4
  %proxy = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store %struct.btBroadphaseAabbCallback* %callback, %struct.btBroadphaseAabbCallback** %callback.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_LastHandleIndex = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  %1 = load i32, i32* %m_LastHandleIndex, align 4
  %cmp = icmp sle i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %2 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %2, i32 %3
  store %struct.btSimpleBroadphaseProxy* %arrayidx, %struct.btSimpleBroadphaseProxy** %proxy, align 4
  %4 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy, align 4
  %5 = bitcast %struct.btSimpleBroadphaseProxy* %4 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 0
  %6 = load i8*, i8** %m_clientObject, align 4
  %tobool = icmp ne i8* %6, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  br label %for.inc

if.end:                                           ; preds = %for.body
  %7 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %8 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %9 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy, align 4
  %10 = bitcast %struct.btSimpleBroadphaseProxy* %9 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %10, i32 0, i32 4
  %11 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy, align 4
  %12 = bitcast %struct.btSimpleBroadphaseProxy* %11 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %12, i32 0, i32 5
  %call = call zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMax)
  br i1 %call, label %if.then2, label %if.end4

if.then2:                                         ; preds = %if.end
  %13 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %callback.addr, align 4
  %14 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy, align 4
  %15 = bitcast %struct.btSimpleBroadphaseProxy* %14 to %struct.btBroadphaseProxy*
  %16 = bitcast %struct.btBroadphaseAabbCallback* %13 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %16, align 4
  %vfn = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %17 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call3 = call zeroext i1 %17(%struct.btBroadphaseAabbCallback* %13, %struct.btBroadphaseProxy* %15)
  br label %if.end4

if.end4:                                          ; preds = %if.then2, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end4, %if.then
  %18 = load i32, i32* %i, align 4
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_Z20TestAabbAgainstAabb2RK9btVector3S1_S1_S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax1, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin2, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax2) #2 comdat {
entry:
  %aabbMin1.addr = alloca %class.btVector3*, align 4
  %aabbMax1.addr = alloca %class.btVector3*, align 4
  %aabbMin2.addr = alloca %class.btVector3*, align 4
  %aabbMax2.addr = alloca %class.btVector3*, align 4
  %overlap = alloca i8, align 1
  store %class.btVector3* %aabbMin1, %class.btVector3** %aabbMin1.addr, align 4
  store %class.btVector3* %aabbMax1, %class.btVector3** %aabbMax1.addr, align 4
  store %class.btVector3* %aabbMin2, %class.btVector3** %aabbMin2.addr, align 4
  store %class.btVector3* %aabbMax2, %class.btVector3** %aabbMax2.addr, align 4
  store i8 1, i8* %overlap, align 1
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4
  %cmp = fcmp ogt float %1, %3
  br i1 %cmp, label %cond.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call3, align 4
  %cmp4 = fcmp olt float %5, %7
  br i1 %cmp4, label %cond.true, label %cond.false

cond.true:                                        ; preds = %lor.lhs.false, %entry
  br label %cond.end

cond.false:                                       ; preds = %lor.lhs.false
  %8 = load i8, i8* %overlap, align 1
  %tobool = trunc i8 %8 to i1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i1 [ false, %cond.true ], [ %tobool, %cond.false ]
  %frombool = zext i1 %cond to i8
  store i8 %frombool, i8* %overlap, align 1
  %9 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %9)
  %10 = load float, float* %call5, align 4
  %11 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %11)
  %12 = load float, float* %call6, align 4
  %cmp7 = fcmp ogt float %10, %12
  br i1 %cmp7, label %cond.true12, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %cond.end
  %13 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %13)
  %14 = load float, float* %call9, align 4
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %15)
  %16 = load float, float* %call10, align 4
  %cmp11 = fcmp olt float %14, %16
  br i1 %cmp11, label %cond.true12, label %cond.false13

cond.true12:                                      ; preds = %lor.lhs.false8, %cond.end
  br label %cond.end15

cond.false13:                                     ; preds = %lor.lhs.false8
  %17 = load i8, i8* %overlap, align 1
  %tobool14 = trunc i8 %17 to i1
  br label %cond.end15

cond.end15:                                       ; preds = %cond.false13, %cond.true12
  %cond16 = phi i1 [ false, %cond.true12 ], [ %tobool14, %cond.false13 ]
  %frombool17 = zext i1 %cond16 to i8
  store i8 %frombool17, i8* %overlap, align 1
  %18 = load %class.btVector3*, %class.btVector3** %aabbMin1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %18)
  %19 = load float, float* %call18, align 4
  %20 = load %class.btVector3*, %class.btVector3** %aabbMax2.addr, align 4
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %20)
  %21 = load float, float* %call19, align 4
  %cmp20 = fcmp ogt float %19, %21
  br i1 %cmp20, label %cond.true25, label %lor.lhs.false21

lor.lhs.false21:                                  ; preds = %cond.end15
  %22 = load %class.btVector3*, %class.btVector3** %aabbMax1.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %22)
  %23 = load float, float* %call22, align 4
  %24 = load %class.btVector3*, %class.btVector3** %aabbMin2.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %24)
  %25 = load float, float* %call23, align 4
  %cmp24 = fcmp olt float %23, %25
  br i1 %cmp24, label %cond.true25, label %cond.false26

cond.true25:                                      ; preds = %lor.lhs.false21, %cond.end15
  br label %cond.end28

cond.false26:                                     ; preds = %lor.lhs.false21
  %26 = load i8, i8* %overlap, align 1
  %tobool27 = trunc i8 %26 to i1
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false26, %cond.true25
  %cond29 = phi i1 [ false, %cond.true25 ], [ %tobool27, %cond.false26 ]
  %frombool30 = zext i1 %cond29 to i8
  store i8 %frombool30, i8* %overlap, align 1
  %27 = load i8, i8* %overlap, align 1
  %tobool31 = trunc i8 %27 to i1
  ret i1 %tobool31
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN18btSimpleBroadphase11aabbOverlapEP23btSimpleBroadphaseProxyS1_(%struct.btSimpleBroadphaseProxy* %proxy0, %struct.btSimpleBroadphaseProxy* %proxy1) #2 {
entry:
  %proxy0.addr = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %struct.btSimpleBroadphaseProxy* %proxy0, %struct.btSimpleBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btSimpleBroadphaseProxy* %proxy1, %struct.btSimpleBroadphaseProxy** %proxy1.addr, align 4
  %0 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0.addr, align 4
  %1 = bitcast %struct.btSimpleBroadphaseProxy* %0 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %1, i32 0, i32 4
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMin)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1.addr, align 4
  %4 = bitcast %struct.btSimpleBroadphaseProxy* %3 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %4, i32 0, i32 5
  %call1 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMax)
  %arrayidx2 = getelementptr inbounds float, float* %call1, i32 0
  %5 = load float, float* %arrayidx2, align 4
  %cmp = fcmp ole float %2, %5
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %6 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1.addr, align 4
  %7 = bitcast %struct.btSimpleBroadphaseProxy* %6 to %struct.btBroadphaseProxy*
  %m_aabbMin3 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %7, i32 0, i32 4
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMin3)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  %8 = load float, float* %arrayidx5, align 4
  %9 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0.addr, align 4
  %10 = bitcast %struct.btSimpleBroadphaseProxy* %9 to %struct.btBroadphaseProxy*
  %m_aabbMax6 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %10, i32 0, i32 5
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMax6)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 0
  %11 = load float, float* %arrayidx8, align 4
  %cmp9 = fcmp ole float %8, %11
  br i1 %cmp9, label %land.lhs.true10, label %land.end

land.lhs.true10:                                  ; preds = %land.lhs.true
  %12 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0.addr, align 4
  %13 = bitcast %struct.btSimpleBroadphaseProxy* %12 to %struct.btBroadphaseProxy*
  %m_aabbMin11 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %13, i32 0, i32 4
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMin11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  %14 = load float, float* %arrayidx13, align 4
  %15 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1.addr, align 4
  %16 = bitcast %struct.btSimpleBroadphaseProxy* %15 to %struct.btBroadphaseProxy*
  %m_aabbMax14 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %16, i32 0, i32 5
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMax14)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 1
  %17 = load float, float* %arrayidx16, align 4
  %cmp17 = fcmp ole float %14, %17
  br i1 %cmp17, label %land.lhs.true18, label %land.end

land.lhs.true18:                                  ; preds = %land.lhs.true10
  %18 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1.addr, align 4
  %19 = bitcast %struct.btSimpleBroadphaseProxy* %18 to %struct.btBroadphaseProxy*
  %m_aabbMin19 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %19, i32 0, i32 4
  %call20 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMin19)
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 1
  %20 = load float, float* %arrayidx21, align 4
  %21 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0.addr, align 4
  %22 = bitcast %struct.btSimpleBroadphaseProxy* %21 to %struct.btBroadphaseProxy*
  %m_aabbMax22 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %22, i32 0, i32 5
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMax22)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 1
  %23 = load float, float* %arrayidx24, align 4
  %cmp25 = fcmp ole float %20, %23
  br i1 %cmp25, label %land.lhs.true26, label %land.end

land.lhs.true26:                                  ; preds = %land.lhs.true18
  %24 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0.addr, align 4
  %25 = bitcast %struct.btSimpleBroadphaseProxy* %24 to %struct.btBroadphaseProxy*
  %m_aabbMin27 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %25, i32 0, i32 4
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMin27)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 2
  %26 = load float, float* %arrayidx29, align 4
  %27 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1.addr, align 4
  %28 = bitcast %struct.btSimpleBroadphaseProxy* %27 to %struct.btBroadphaseProxy*
  %m_aabbMax30 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %28, i32 0, i32 5
  %call31 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMax30)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 2
  %29 = load float, float* %arrayidx32, align 4
  %cmp33 = fcmp ole float %26, %29
  br i1 %cmp33, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true26
  %30 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1.addr, align 4
  %31 = bitcast %struct.btSimpleBroadphaseProxy* %30 to %struct.btBroadphaseProxy*
  %m_aabbMin34 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %31, i32 0, i32 4
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMin34)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 2
  %32 = load float, float* %arrayidx36, align 4
  %33 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0.addr, align 4
  %34 = bitcast %struct.btSimpleBroadphaseProxy* %33 to %struct.btBroadphaseProxy*
  %m_aabbMax37 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %34, i32 0, i32 5
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_aabbMax37)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 2
  %35 = load float, float* %arrayidx39, align 4
  %cmp40 = fcmp ole float %32, %35
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true26, %land.lhs.true18, %land.lhs.true10, %land.lhs.true, %entry
  %36 = phi i1 [ false, %land.lhs.true26 ], [ false, %land.lhs.true18 ], [ false, %land.lhs.true10 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp40, %land.rhs ]
  ret i1 %36
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN18btSimpleBroadphase25calculateOverlappingPairsEP12btDispatcher(%class.btSimpleBroadphase* %this, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %new_largest_index = alloca i32, align 4
  %proxy0 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %proxy1 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %p0 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %p1 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %overlappingPairArray = alloca %class.btAlignedObjectArray*, align 4
  %ref.tmp = alloca %class.btBroadphasePairSortPredicate, align 1
  %ref.tmp58 = alloca %struct.btBroadphasePair, align 4
  %previousPair = alloca %struct.btBroadphasePair, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %isDuplicate = alloca i8, align 1
  %needsRemoval = alloca i8, align 1
  %hasOverlap = alloca i8, align 1
  %ref.tmp93 = alloca %class.btBroadphasePairSortPredicate, align 1
  %ref.tmp97 = alloca %struct.btBroadphasePair, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %m_numHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_numHandles, align 4
  %cmp = icmp sge i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end101

if.then:                                          ; preds = %entry
  store i32 -1, i32* %new_largest_index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc43, %if.then
  %1 = load i32, i32* %i, align 4
  %m_LastHandleIndex = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  %2 = load i32, i32* %m_LastHandleIndex, align 4
  %cmp2 = icmp sle i32 %1, %2
  br i1 %cmp2, label %for.body, label %for.end45

for.body:                                         ; preds = %for.cond
  %m_pHandles = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %3 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %3, i32 %4
  store %struct.btSimpleBroadphaseProxy* %arrayidx, %struct.btSimpleBroadphaseProxy** %proxy0, align 4
  %5 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4
  %6 = bitcast %struct.btSimpleBroadphaseProxy* %5 to %struct.btBroadphaseProxy*
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %6, i32 0, i32 0
  %7 = load i8*, i8** %m_clientObject, align 4
  %tobool = icmp ne i8* %7, null
  br i1 %tobool, label %if.end, label %if.then3

if.then3:                                         ; preds = %for.body
  br label %for.inc43

if.end:                                           ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  store i32 %8, i32* %new_largest_index, align 4
  %9 = load i32, i32* %i, align 4
  %add = add nsw i32 %9, 1
  store i32 %add, i32* %j, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %if.end
  %10 = load i32, i32* %j, align 4
  %m_LastHandleIndex5 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  %11 = load i32, i32* %m_LastHandleIndex5, align 4
  %cmp6 = icmp sle i32 %10, %11
  br i1 %cmp6, label %for.body7, label %for.end

for.body7:                                        ; preds = %for.cond4
  %m_pHandles8 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 4
  %12 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %m_pHandles8, align 4
  %13 = load i32, i32* %j, align 4
  %arrayidx9 = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %12, i32 %13
  store %struct.btSimpleBroadphaseProxy* %arrayidx9, %struct.btSimpleBroadphaseProxy** %proxy1, align 4
  %14 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1, align 4
  %15 = bitcast %struct.btSimpleBroadphaseProxy* %14 to %struct.btBroadphaseProxy*
  %m_clientObject10 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %15, i32 0, i32 0
  %16 = load i8*, i8** %m_clientObject10, align 4
  %tobool11 = icmp ne i8* %16, null
  br i1 %tobool11, label %if.end13, label %if.then12

if.then12:                                        ; preds = %for.body7
  br label %for.inc

if.end13:                                         ; preds = %for.body7
  %17 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4
  %18 = bitcast %struct.btSimpleBroadphaseProxy* %17 to %struct.btBroadphaseProxy*
  %call = call %struct.btSimpleBroadphaseProxy* @_ZN18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this1, %struct.btBroadphaseProxy* %18)
  store %struct.btSimpleBroadphaseProxy* %call, %struct.btSimpleBroadphaseProxy** %p0, align 4
  %19 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1, align 4
  %20 = bitcast %struct.btSimpleBroadphaseProxy* %19 to %struct.btBroadphaseProxy*
  %call14 = call %struct.btSimpleBroadphaseProxy* @_ZN18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this1, %struct.btBroadphaseProxy* %20)
  store %struct.btSimpleBroadphaseProxy* %call14, %struct.btSimpleBroadphaseProxy** %p1, align 4
  %21 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %p0, align 4
  %22 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %p1, align 4
  %call15 = call zeroext i1 @_ZN18btSimpleBroadphase11aabbOverlapEP23btSimpleBroadphaseProxyS1_(%struct.btSimpleBroadphaseProxy* %21, %struct.btSimpleBroadphaseProxy* %22)
  br i1 %call15, label %if.then16, label %if.else

if.then16:                                        ; preds = %if.end13
  %m_pairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %23 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  %24 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4
  %25 = bitcast %struct.btSimpleBroadphaseProxy* %24 to %struct.btBroadphaseProxy*
  %26 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1, align 4
  %27 = bitcast %struct.btSimpleBroadphaseProxy* %26 to %struct.btBroadphaseProxy*
  %28 = bitcast %class.btOverlappingPairCache* %23 to %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %28, align 4
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 13
  %29 = load %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call17 = call %struct.btBroadphasePair* %29(%class.btOverlappingPairCache* %23, %struct.btBroadphaseProxy* %25, %struct.btBroadphaseProxy* %27)
  %tobool18 = icmp ne %struct.btBroadphasePair* %call17, null
  br i1 %tobool18, label %if.end24, label %if.then19

if.then19:                                        ; preds = %if.then16
  %m_pairCache20 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %30 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache20, align 4
  %31 = bitcast %class.btOverlappingPairCache* %30 to %class.btOverlappingPairCallback*
  %32 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4
  %33 = bitcast %struct.btSimpleBroadphaseProxy* %32 to %struct.btBroadphaseProxy*
  %34 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1, align 4
  %35 = bitcast %struct.btSimpleBroadphaseProxy* %34 to %struct.btBroadphaseProxy*
  %36 = bitcast %class.btOverlappingPairCallback* %31 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable21 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %36, align 4
  %vfn22 = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable21, i64 2
  %37 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn22, align 4
  %call23 = call %struct.btBroadphasePair* %37(%class.btOverlappingPairCallback* %31, %struct.btBroadphaseProxy* %33, %struct.btBroadphaseProxy* %35)
  br label %if.end24

if.end24:                                         ; preds = %if.then19, %if.then16
  br label %if.end42

if.else:                                          ; preds = %if.end13
  %m_pairCache25 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %38 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache25, align 4
  %39 = bitcast %class.btOverlappingPairCache* %38 to i1 (%class.btOverlappingPairCache*)***
  %vtable26 = load i1 (%class.btOverlappingPairCache*)**, i1 (%class.btOverlappingPairCache*)*** %39, align 4
  %vfn27 = getelementptr inbounds i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vtable26, i64 14
  %40 = load i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vfn27, align 4
  %call28 = call zeroext i1 %40(%class.btOverlappingPairCache* %38)
  br i1 %call28, label %if.end41, label %if.then29

if.then29:                                        ; preds = %if.else
  %m_pairCache30 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %41 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache30, align 4
  %42 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4
  %43 = bitcast %struct.btSimpleBroadphaseProxy* %42 to %struct.btBroadphaseProxy*
  %44 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1, align 4
  %45 = bitcast %struct.btSimpleBroadphaseProxy* %44 to %struct.btBroadphaseProxy*
  %46 = bitcast %class.btOverlappingPairCache* %41 to %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable31 = load %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %46, align 4
  %vfn32 = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable31, i64 13
  %47 = load %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCache*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn32, align 4
  %call33 = call %struct.btBroadphasePair* %47(%class.btOverlappingPairCache* %41, %struct.btBroadphaseProxy* %43, %struct.btBroadphaseProxy* %45)
  %tobool34 = icmp ne %struct.btBroadphasePair* %call33, null
  br i1 %tobool34, label %if.then35, label %if.end40

if.then35:                                        ; preds = %if.then29
  %m_pairCache36 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %48 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache36, align 4
  %49 = bitcast %class.btOverlappingPairCache* %48 to %class.btOverlappingPairCallback*
  %50 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy0, align 4
  %51 = bitcast %struct.btSimpleBroadphaseProxy* %50 to %struct.btBroadphaseProxy*
  %52 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %proxy1, align 4
  %53 = bitcast %struct.btSimpleBroadphaseProxy* %52 to %struct.btBroadphaseProxy*
  %54 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %55 = bitcast %class.btOverlappingPairCallback* %49 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable37 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %55, align 4
  %vfn38 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable37, i64 3
  %56 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn38, align 4
  %call39 = call i8* %56(%class.btOverlappingPairCallback* %49, %struct.btBroadphaseProxy* %51, %struct.btBroadphaseProxy* %53, %class.btDispatcher* %54)
  br label %if.end40

if.end40:                                         ; preds = %if.then35, %if.then29
  br label %if.end41

if.end41:                                         ; preds = %if.end40, %if.else
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %if.end24
  br label %for.inc

for.inc:                                          ; preds = %if.end42, %if.then12
  %57 = load i32, i32* %j, align 4
  %inc = add nsw i32 %57, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  br label %for.inc43

for.inc43:                                        ; preds = %for.end, %if.then3
  %58 = load i32, i32* %i, align 4
  %inc44 = add nsw i32 %58, 1
  store i32 %inc44, i32* %i, align 4
  br label %for.cond

for.end45:                                        ; preds = %for.cond
  %59 = load i32, i32* %new_largest_index, align 4
  %m_LastHandleIndex46 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 3
  store i32 %59, i32* %m_LastHandleIndex46, align 4
  %m_ownsPairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 8
  %60 = load i8, i8* %m_ownsPairCache, align 4
  %tobool47 = trunc i8 %60 to i1
  br i1 %tobool47, label %land.lhs.true, label %if.end100

land.lhs.true:                                    ; preds = %for.end45
  %m_pairCache48 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %61 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache48, align 4
  %62 = bitcast %class.btOverlappingPairCache* %61 to i1 (%class.btOverlappingPairCache*)***
  %vtable49 = load i1 (%class.btOverlappingPairCache*)**, i1 (%class.btOverlappingPairCache*)*** %62, align 4
  %vfn50 = getelementptr inbounds i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vtable49, i64 14
  %63 = load i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vfn50, align 4
  %call51 = call zeroext i1 %63(%class.btOverlappingPairCache* %61)
  br i1 %call51, label %if.then52, label %if.end100

if.then52:                                        ; preds = %land.lhs.true
  %m_pairCache53 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %64 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache53, align 4
  %65 = bitcast %class.btOverlappingPairCache* %64 to %class.btAlignedObjectArray* (%class.btOverlappingPairCache*)***
  %vtable54 = load %class.btAlignedObjectArray* (%class.btOverlappingPairCache*)**, %class.btAlignedObjectArray* (%class.btOverlappingPairCache*)*** %65, align 4
  %vfn55 = getelementptr inbounds %class.btAlignedObjectArray* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray* (%class.btOverlappingPairCache*)** %vtable54, i64 7
  %66 = load %class.btAlignedObjectArray* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray* (%class.btOverlappingPairCache*)** %vfn55, align 4
  %call56 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray* %66(%class.btOverlappingPairCache* %64)
  store %class.btAlignedObjectArray* %call56, %class.btAlignedObjectArray** %overlappingPairArray, align 4
  %67 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray* %67, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  %68 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4
  %69 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4
  %call57 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %69)
  %m_invalidPair = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 9
  %70 = load i32, i32* %m_invalidPair, align 4
  %sub = sub nsw i32 %call57, %70
  %call59 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp58)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray* %68, i32 %sub, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp58)
  %m_invalidPair60 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 9
  store i32 0, i32* %m_invalidPair60, align 4
  %call61 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %previousPair)
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond62

for.cond62:                                       ; preds = %for.inc90, %if.then52
  %71 = load i32, i32* %i, align 4
  %72 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4
  %call63 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %72)
  %cmp64 = icmp slt i32 %71, %call63
  br i1 %cmp64, label %for.body65, label %for.end92

for.body65:                                       ; preds = %for.cond62
  %73 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4
  %74 = load i32, i32* %i, align 4
  %call66 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %73, i32 %74)
  store %struct.btBroadphasePair* %call66, %struct.btBroadphasePair** %pair, align 4
  %75 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %call67 = call zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %75, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %previousPair)
  %frombool = zext i1 %call67 to i8
  store i8 %frombool, i8* %isDuplicate, align 1
  %76 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %77 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  %78 = bitcast %struct.btBroadphasePair* %76 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %77, i8* align 4 %78, i32 16, i1 false)
  store i8 0, i8* %needsRemoval, align 1
  %79 = load i8, i8* %isDuplicate, align 1
  %tobool68 = trunc i8 %79 to i1
  br i1 %tobool68, label %if.else78, label %if.then69

if.then69:                                        ; preds = %for.body65
  %80 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy070 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %80, i32 0, i32 0
  %81 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy070, align 4
  %82 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy171 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %82, i32 0, i32 1
  %83 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy171, align 4
  %call72 = call zeroext i1 @_ZN18btSimpleBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_(%class.btSimpleBroadphase* %this1, %struct.btBroadphaseProxy* %81, %struct.btBroadphaseProxy* %83)
  %frombool73 = zext i1 %call72 to i8
  store i8 %frombool73, i8* %hasOverlap, align 1
  %84 = load i8, i8* %hasOverlap, align 1
  %tobool74 = trunc i8 %84 to i1
  br i1 %tobool74, label %if.then75, label %if.else76

if.then75:                                        ; preds = %if.then69
  store i8 0, i8* %needsRemoval, align 1
  br label %if.end77

if.else76:                                        ; preds = %if.then69
  store i8 1, i8* %needsRemoval, align 1
  br label %if.end77

if.end77:                                         ; preds = %if.else76, %if.then75
  br label %if.end79

if.else78:                                        ; preds = %for.body65
  store i8 1, i8* %needsRemoval, align 1
  br label %if.end79

if.end79:                                         ; preds = %if.else78, %if.end77
  %85 = load i8, i8* %needsRemoval, align 1
  %tobool80 = trunc i8 %85 to i1
  br i1 %tobool80, label %if.then81, label %if.end89

if.then81:                                        ; preds = %if.end79
  %m_pairCache82 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %86 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache82, align 4
  %87 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %88 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %89 = bitcast %class.btOverlappingPairCache* %86 to void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable83 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %89, align 4
  %vfn84 = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable83, i64 8
  %90 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn84, align 4
  call void %90(%class.btOverlappingPairCache* %86, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %87, %class.btDispatcher* %88)
  %91 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy085 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %91, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy085, align 4
  %92 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy186 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %92, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy186, align 4
  %m_invalidPair87 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 9
  %93 = load i32, i32* %m_invalidPair87, align 4
  %inc88 = add nsw i32 %93, 1
  store i32 %inc88, i32* %m_invalidPair87, align 4
  %94 = load i32, i32* @gOverlappingPairs, align 4
  %dec = add nsw i32 %94, -1
  store i32 %dec, i32* @gOverlappingPairs, align 4
  br label %if.end89

if.end89:                                         ; preds = %if.then81, %if.end79
  br label %for.inc90

for.inc90:                                        ; preds = %if.end89
  %95 = load i32, i32* %i, align 4
  %inc91 = add nsw i32 %95, 1
  store i32 %inc91, i32* %i, align 4
  br label %for.cond62

for.end92:                                        ; preds = %for.cond62
  %96 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray* %96, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp93)
  %97 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4
  %98 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %overlappingPairArray, align 4
  %call94 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %98)
  %m_invalidPair95 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 9
  %99 = load i32, i32* %m_invalidPair95, align 4
  %sub96 = sub nsw i32 %call94, %99
  %call98 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp97)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray* %97, i32 %sub96, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp97)
  %m_invalidPair99 = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 9
  store i32 0, i32* %m_invalidPair99, align 4
  br label %if.end100

if.end100:                                        ; preds = %for.end92, %land.lhs.true, %for.end45
  br label %if.end101

if.end101:                                        ; preds = %if.end100, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray* %this, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %CompareFunc.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btBroadphasePairSortPredicate* %CompareFunc, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btBroadphasePair*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btBroadphasePair* %fillData, %struct.btBroadphasePair** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end16

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc13, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end15

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %14, i32 %15
  %16 = bitcast %struct.btBroadphasePair* %arrayidx10 to i8*
  %call11 = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %16)
  %17 = bitcast i8* %call11 to %struct.btBroadphasePair*
  %18 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %fillData.addr, align 4
  %call12 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %17, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %18)
  br label %for.inc13

for.inc13:                                        ; preds = %for.body8
  %19 = load i32, i32* %i5, align 4
  %inc14 = add nsw i32 %19, 1
  store i32 %inc14, i32* %i5, align 4
  br label %for.cond6

for.end15:                                        ; preds = %for.cond6
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %20 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %20, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon.0* %0 to i8**
  store i8* null, i8** %m_internalInfo1, align 4
  ret %struct.btBroadphasePair* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  ret %struct.btBroadphasePair* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %a.addr = alloca %struct.btBroadphasePair*, align 4
  %b.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %a, %struct.btBroadphasePair** %a.addr, align 4
  store %struct.btBroadphasePair* %b, %struct.btBroadphasePair** %b.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy01 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 0
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy01, align 4
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %3
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy12 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %6, i32 0, i32 1
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy12, align 4
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %5, %7
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %8 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %8
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN18btSimpleBroadphase15testAabbOverlapEP17btBroadphaseProxyS1_(%class.btSimpleBroadphase* %this, %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy* %proxy1) #2 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %proxy0.addr = alloca %struct.btBroadphaseProxy*, align 4
  %proxy1.addr = alloca %struct.btBroadphaseProxy*, align 4
  %p0 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  %p1 = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy0, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  store %struct.btBroadphaseProxy* %proxy1, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy0.addr, align 4
  %call = call %struct.btSimpleBroadphaseProxy* @_ZN18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this1, %struct.btBroadphaseProxy* %0)
  store %struct.btSimpleBroadphaseProxy* %call, %struct.btSimpleBroadphaseProxy** %p0, align 4
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy1.addr, align 4
  %call2 = call %struct.btSimpleBroadphaseProxy* @_ZN18btSimpleBroadphase23getSimpleProxyFromProxyEP17btBroadphaseProxy(%class.btSimpleBroadphase* %this1, %struct.btBroadphaseProxy* %1)
  store %struct.btSimpleBroadphaseProxy* %call2, %struct.btSimpleBroadphaseProxy** %p1, align 4
  %2 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %p0, align 4
  %3 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %p1, align 4
  %call3 = call zeroext i1 @_ZN18btSimpleBroadphase11aabbOverlapEP23btSimpleBroadphaseProxyS1_(%struct.btSimpleBroadphaseProxy* %2, %struct.btSimpleBroadphaseProxy* %3)
  ret i1 %call3
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN18btSimpleBroadphase9resetPoolEP12btDispatcher(%class.btSimpleBroadphase* %this, %class.btDispatcher* %dispatcher) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN18btSimpleBroadphase23getOverlappingPairCacheEv(%class.btSimpleBroadphase* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  ret %class.btOverlappingPairCache* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZNK18btSimpleBroadphase23getOverlappingPairCacheEv(%class.btSimpleBroadphase* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %m_pairCache = getelementptr inbounds %class.btSimpleBroadphase, %class.btSimpleBroadphase* %this1, i32 0, i32 7
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_pairCache, align 4
  ret %class.btOverlappingPairCache* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK18btSimpleBroadphase17getBroadphaseAabbER9btVector3S1_(%class.btSimpleBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp2, align 4
  store float 0xC3ABC16D60000000, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %1 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp4, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp5, align 4
  store float 0x43ABC16D60000000, float* %ref.tmp6, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %1, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btSimpleBroadphase10printStatsEv(%class.btSimpleBroadphase* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSimpleBroadphase*, align 4
  store %class.btSimpleBroadphase* %this, %class.btSimpleBroadphase** %this.addr, align 4
  %this1 = load %class.btSimpleBroadphase*, %class.btSimpleBroadphase** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceD2Ev(%class.btBroadphaseInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  ret %class.btBroadphaseInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btBroadphaseInterfaceD0Ev(%class.btBroadphaseInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher(%class.btBroadphaseInterface* %this, %class.btDispatcher* %dispatcher) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  ret void
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #7

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2Ev(%struct.btBroadphaseProxy* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseProxy*, align 4
  store %struct.btBroadphaseProxy* %this, %struct.btBroadphaseProxy** %this.addr, align 4
  %this1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %this.addr, align 4
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 0
  store i8* null, i8** %m_clientObject, align 4
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMin)
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 5
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMax)
  ret %struct.btBroadphaseProxy* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK23btSimpleBroadphaseProxy11GetNextFreeEv(%struct.btSimpleBroadphaseProxy* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btSimpleBroadphaseProxy*, align 4
  store %struct.btSimpleBroadphaseProxy* %this, %struct.btSimpleBroadphaseProxy** %this.addr, align 4
  %this1 = load %struct.btSimpleBroadphaseProxy*, %struct.btSimpleBroadphaseProxy** %this.addr, align 4
  %m_nextFree = getelementptr inbounds %struct.btSimpleBroadphaseProxy, %struct.btSimpleBroadphaseProxy* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_nextFree, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2ERK9btVector3S2_Pvii(%struct.btBroadphaseProxy* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i8* %userPtr, i32 %collisionFilterGroup, i32 %collisionFilterMask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i32, align 4
  %collisionFilterMask.addr = alloca i32, align 4
  store %struct.btBroadphaseProxy* %this, %struct.btBroadphaseProxy** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store i8* %userPtr, i8** %userPtr.addr, align 4
  store i32 %collisionFilterGroup, i32* %collisionFilterGroup.addr, align 4
  store i32 %collisionFilterMask, i32* %collisionFilterMask.addr, align 4
  %this1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %this.addr, align 4
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 0
  %0 = load i8*, i8** %userPtr.addr, align 4
  store i8* %0, i8** %m_clientObject, align 4
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 1
  %1 = load i32, i32* %collisionFilterGroup.addr, align 4
  store i32 %1, i32* %m_collisionFilterGroup, align 4
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 2
  %2 = load i32, i32* %collisionFilterMask.addr, align 4
  store i32 %2, i32* %m_collisionFilterMask, align 4
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %4 = bitcast %class.btVector3* %m_aabbMin to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 5
  %6 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %7 = bitcast %class.btVector3* %m_aabbMax to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %struct.btBroadphaseProxy* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %CompareFunc.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btBroadphasePairSortPredicate* %CompareFunc, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  store i32 %lo, i32* %lo.addr, align 4
  store i32 %hi, i32* %hi.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %lo.addr, align 4
  store i32 %0, i32* %i, align 4
  %1 = load i32, i32* %hi.addr, align 4
  store i32 %1, i32* %j, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %3 = load i32, i32* %lo.addr, align 4
  %4 = load i32, i32* %hi.addr, align 4
  %add = add nsw i32 %3, %4
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 %div
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %5 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %6, i32 %7
  %call4 = call zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %5, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx3, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x)
  br i1 %call4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond5

while.cond5:                                      ; preds = %while.body9, %while.end
  %9 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %10 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4
  %11 = load i32, i32* %j, align 4
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %10, i32 %11
  %call8 = call zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %9, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx7)
  br i1 %call8, label %while.body9, label %while.end10

while.body9:                                      ; preds = %while.cond5
  %12 = load i32, i32* %j, align 4
  %dec = add nsw i32 %12, -1
  store i32 %dec, i32* %j, align 4
  br label %while.cond5

while.end10:                                      ; preds = %while.cond5
  %13 = load i32, i32* %i, align 4
  %14 = load i32, i32* %j, align 4
  %cmp = icmp sle i32 %13, %14
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end10
  %15 = load i32, i32* %i, align 4
  %16 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray* %this1, i32 %15, i32 %16)
  %17 = load i32, i32* %i, align 4
  %inc11 = add nsw i32 %17, 1
  store i32 %inc11, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %dec12 = add nsw i32 %18, -1
  store i32 %dec12, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end10
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %19 = load i32, i32* %i, align 4
  %20 = load i32, i32* %j, align 4
  %cmp13 = icmp sle i32 %19, %20
  br i1 %cmp13, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %21 = load i32, i32* %lo.addr, align 4
  %22 = load i32, i32* %j, align 4
  %cmp14 = icmp slt i32 %21, %22
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %do.end
  %23 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %24 = load i32, i32* %lo.addr, align 4
  %25 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %23, i32 %24, i32 %25)
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %do.end
  %26 = load i32, i32* %i, align 4
  %27 = load i32, i32* %hi.addr, align 4
  %cmp17 = icmp slt i32 %26, %27
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end16
  %28 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %29 = load i32, i32* %i, align 4
  %30 = load i32, i32* %hi.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %28, i32 %29, i32 %30)
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %if.end16
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* returned %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %other) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  %other.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4
  store %struct.btBroadphasePair* %other, %struct.btBroadphasePair** %other.addr, align 4
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %m_pProxy13 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 1
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy13, align 4
  store %struct.btBroadphaseProxy* %3, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %m_algorithm4 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 2
  %5 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm4, align 4
  store %class.btCollisionAlgorithm* %5, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %6 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon.0* %6 to i8**
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %8 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 3
  %m_internalInfo15 = bitcast %union.anon.0* %8 to i8**
  %9 = load i8*, i8** %m_internalInfo15, align 4
  store i8* %9, i8** %m_internalInfo1, align 4
  ret %struct.btBroadphasePair* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  %a.addr = alloca %struct.btBroadphasePair*, align 4
  %b.addr = alloca %struct.btBroadphasePair*, align 4
  %uidA0 = alloca i32, align 4
  %uidB0 = alloca i32, align 4
  %uidA1 = alloca i32, align 4
  %uidB1 = alloca i32, align 4
  store %class.btBroadphasePairSortPredicate* %this, %class.btBroadphasePairSortPredicate** %this.addr, align 4
  store %struct.btBroadphasePair* %a, %struct.btBroadphasePair** %a.addr, align 4
  store %struct.btBroadphasePair* %b, %struct.btBroadphasePair** %b.addr, align 4
  %this1 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %tobool = icmp ne %struct.btBroadphaseProxy* %1, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 0
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %3, i32 0, i32 3
  %4 = load i32, i32* %m_uniqueId, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %4, %cond.true ], [ -1, %cond.false ]
  store i32 %cond, i32* %uidA0, align 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy03 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 0, i32 0
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy03, align 4
  %tobool4 = icmp ne %struct.btBroadphaseProxy* %6, null
  br i1 %tobool4, label %cond.true5, label %cond.false8

cond.true5:                                       ; preds = %cond.end
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy06 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 0
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy06, align 4
  %m_uniqueId7 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 3
  %9 = load i32, i32* %m_uniqueId7, align 4
  br label %cond.end9

cond.false8:                                      ; preds = %cond.end
  br label %cond.end9

cond.end9:                                        ; preds = %cond.false8, %cond.true5
  %cond10 = phi i32 [ %9, %cond.true5 ], [ -1, %cond.false8 ]
  store i32 %cond10, i32* %uidB0, align 4
  %10 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %10, i32 0, i32 1
  %11 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %tobool11 = icmp ne %struct.btBroadphaseProxy* %11, null
  br i1 %tobool11, label %cond.true12, label %cond.false15

cond.true12:                                      ; preds = %cond.end9
  %12 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy113 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %12, i32 0, i32 1
  %13 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy113, align 4
  %m_uniqueId14 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %13, i32 0, i32 3
  %14 = load i32, i32* %m_uniqueId14, align 4
  br label %cond.end16

cond.false15:                                     ; preds = %cond.end9
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false15, %cond.true12
  %cond17 = phi i32 [ %14, %cond.true12 ], [ -1, %cond.false15 ]
  store i32 %cond17, i32* %uidA1, align 4
  %15 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy118 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %15, i32 0, i32 1
  %16 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy118, align 4
  %tobool19 = icmp ne %struct.btBroadphaseProxy* %16, null
  br i1 %tobool19, label %cond.true20, label %cond.false23

cond.true20:                                      ; preds = %cond.end16
  %17 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy121 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %17, i32 0, i32 1
  %18 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy121, align 4
  %m_uniqueId22 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %18, i32 0, i32 3
  %19 = load i32, i32* %m_uniqueId22, align 4
  br label %cond.end24

cond.false23:                                     ; preds = %cond.end16
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false23, %cond.true20
  %cond25 = phi i32 [ %19, %cond.true20 ], [ -1, %cond.false23 ]
  store i32 %cond25, i32* %uidB1, align 4
  %20 = load i32, i32* %uidA0, align 4
  %21 = load i32, i32* %uidB0, align 4
  %cmp = icmp sgt i32 %20, %21
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end24
  %22 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy026 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %22, i32 0, i32 0
  %23 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy026, align 4
  %24 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy027 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %24, i32 0, i32 0
  %25 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy027, align 4
  %cmp28 = icmp eq %struct.btBroadphaseProxy* %23, %25
  br i1 %cmp28, label %land.lhs.true, label %lor.rhs

land.lhs.true:                                    ; preds = %lor.lhs.false
  %26 = load i32, i32* %uidA1, align 4
  %27 = load i32, i32* %uidB1, align 4
  %cmp29 = icmp sgt i32 %26, %27
  br i1 %cmp29, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.lhs.true, %lor.lhs.false
  %28 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy030 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %28, i32 0, i32 0
  %29 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy030, align 4
  %30 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy031 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %30, i32 0, i32 0
  %31 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy031, align 4
  %cmp32 = icmp eq %struct.btBroadphaseProxy* %29, %31
  br i1 %cmp32, label %land.lhs.true33, label %land.end

land.lhs.true33:                                  ; preds = %lor.rhs
  %32 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy134 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %32, i32 0, i32 1
  %33 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy134, align 4
  %34 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy135 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %34, i32 0, i32 1
  %35 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy135, align 4
  %cmp36 = icmp eq %struct.btBroadphaseProxy* %33, %35
  br i1 %cmp36, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true33
  %36 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %36, i32 0, i32 2
  %37 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %38 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_algorithm37 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %38, i32 0, i32 2
  %39 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm37, align 4
  %cmp38 = icmp ugt %class.btCollisionAlgorithm* %37, %39
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true33, %lor.rhs
  %40 = phi i1 [ false, %land.lhs.true33 ], [ false, %lor.rhs ], [ %cmp38, %land.rhs ]
  br label %lor.end

lor.end:                                          ; preds = %land.end, %land.lhs.true, %cond.end24
  %41 = phi i1 [ true, %land.lhs.true ], [ true, %cond.end24 ], [ %40, %land.end ]
  ret i1 %41
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray* %this, i32 %index0, i32 %index1) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %temp, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4
  %3 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 %3
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4
  %5 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 %5
  %6 = bitcast %struct.btBroadphasePair* %arrayidx5 to i8*
  %7 = bitcast %struct.btBroadphasePair* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4
  %9 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 %9
  %10 = bitcast %struct.btBroadphasePair* %arrayidx7 to i8*
  %11 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btBroadphasePair*
  store %struct.btBroadphasePair* %2, %struct.btBroadphasePair** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btBroadphasePair* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* %4, %struct.btBroadphasePair** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN16btBroadphasePairnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btBroadphasePair** null)
  %2 = bitcast %struct.btBroadphasePair* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btBroadphasePair* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btBroadphasePair*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btBroadphasePair* %dest, %struct.btBroadphasePair** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 %4
  %5 = bitcast %struct.btBroadphasePair* %arrayidx to i8*
  %call = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %struct.btBroadphasePair*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 %8
  %call3 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %6, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %tobool = icmp ne %struct.btBroadphasePair* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btBroadphasePair* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btBroadphasePair** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btBroadphasePair**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btBroadphasePair** %hint, %struct.btBroadphasePair*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btBroadphasePair*
  ret %struct.btBroadphasePair* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btBroadphasePair* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %struct.btBroadphasePair* %ptr, %struct.btBroadphasePair** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %ptr.addr, align 4
  %1 = bitcast %struct.btBroadphasePair* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btSimpleBroadphase.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind readnone speculatable willreturn }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { cold noreturn nounwind }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
