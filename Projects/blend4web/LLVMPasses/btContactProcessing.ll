; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/Gimpact/btContactProcessing.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/Gimpact/btContactProcessing.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btContactArray = type { %class.btAlignedObjectArray.base, [3 x i8] }
%class.btAlignedObjectArray.base = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.GIM_CONTACT*, i8 }>
%class.btAlignedAllocator = type { i8 }
%class.GIM_CONTACT = type { %class.btVector3, %class.btVector3, float, float, i32, i32 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.CONTACT_KEY_TOKEN*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.CONTACT_KEY_TOKEN = type { i32, i32 }
%class.CONTACT_KEY_TOKEN_COMP = type { i8 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.GIM_CONTACT*, i8, [3 x i8] }>

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE5clearEv = comdat any

$_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9push_backERKS0_ = comdat any

$_ZNK11GIM_CONTACT16calc_key_contactEv = comdat any

$_ZN17CONTACT_KEY_TOKENC2Eji = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9quickSortI22CONTACT_KEY_TOKEN_COMPEEvRKT_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEixEi = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTEixEi = comdat any

$_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv = comdat any

$_Z6btFabsf = comdat any

$_ZN11GIM_CONTACT19interpolate_normalsEP9btVector3i = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENED2Ev = comdat any

$_ZN11GIM_CONTACTC2ERKS_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK20btAlignedObjectArrayI11GIM_CONTACTE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI11GIM_CONTACTLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI11GIM_CONTACTLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI11GIM_CONTACTE4initEv = comdat any

$_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4initEv = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EE8allocateEiPPKS0_ = comdat any

$_ZN17CONTACT_KEY_TOKENC2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvRKT_ii = comdat any

$_ZNK22CONTACT_KEY_TOKEN_COMPclERK17CONTACT_KEY_TOKENS2_ = comdat any

$_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4swapEii = comdat any

$_ZNK17CONTACT_KEY_TOKENltERKS_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btContactProcessing.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btContactArray14merge_contactsERKS_b(%class.btContactArray* %this, %class.btContactArray* nonnull align 4 dereferenceable(17) %contacts, i1 zeroext %normal_contact_average) #2 {
entry:
  %this.addr = alloca %class.btContactArray*, align 4
  %contacts.addr = alloca %class.btContactArray*, align 4
  %normal_contact_average.addr = alloca i8, align 1
  %i = alloca i32, align 4
  %keycontacts = alloca %class.btAlignedObjectArray.0, align 4
  %ref.tmp = alloca %struct.CONTACT_KEY_TOKEN, align 4
  %ref.tmp14 = alloca %class.CONTACT_KEY_TOKEN_COMP, align 1
  %coincident_count = alloca i32, align 4
  %coincident_normals = alloca [8 x %class.btVector3], align 16
  %last_key = alloca i32, align 4
  %key = alloca i32, align 4
  %pcontact = alloca %class.GIM_CONTACT*, align 4
  %scontact = alloca %class.GIM_CONTACT*, align 4
  store %class.btContactArray* %this, %class.btContactArray** %this.addr, align 4
  store %class.btContactArray* %contacts, %class.btContactArray** %contacts.addr, align 4
  %frombool = zext i1 %normal_contact_average to i8
  store i8 %frombool, i8* %normal_contact_average.addr, align 1
  %this1 = load %class.btContactArray*, %class.btContactArray** %this.addr, align 4
  %0 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE5clearEv(%class.btAlignedObjectArray* %0)
  %1 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %2 = bitcast %class.btContactArray* %1 to %class.btAlignedObjectArray*
  %call = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %2)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %4 = bitcast %class.btContactArray* %3 to %class.btAlignedObjectArray*
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %4)
  %cmp3 = icmp eq i32 %call2, 1
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %5 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  %6 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %7 = bitcast %class.btContactArray* %6 to %class.btAlignedObjectArray*
  %call5 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %7, i32 0)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_(%class.btAlignedObjectArray* %5, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %call5)
  br label %return

if.end6:                                          ; preds = %if.end
  %call7 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEC2Ev(%class.btAlignedObjectArray.0* %keycontacts)
  %8 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %9 = bitcast %class.btContactArray* %8 to %class.btAlignedObjectArray*
  %call8 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %9)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7reserveEi(%class.btAlignedObjectArray.0* %keycontacts, i32 %call8)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end6
  %10 = load i32, i32* %i, align 4
  %11 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %12 = bitcast %class.btContactArray* %11 to %class.btAlignedObjectArray*
  %call9 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %12)
  %cmp10 = icmp slt i32 %10, %call9
  br i1 %cmp10, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %14 = bitcast %class.btContactArray* %13 to %class.btAlignedObjectArray*
  %15 = load i32, i32* %i, align 4
  %call11 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %14, i32 %15)
  %call12 = call i32 @_ZNK11GIM_CONTACT16calc_key_contactEv(%class.GIM_CONTACT* %call11)
  %16 = load i32, i32* %i, align 4
  %call13 = call %struct.CONTACT_KEY_TOKEN* @_ZN17CONTACT_KEY_TOKENC2Eji(%struct.CONTACT_KEY_TOKEN* %ref.tmp, i32 %call12, i32 %16)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9push_backERKS0_(%class.btAlignedObjectArray.0* %keycontacts, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %ref.tmp)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %i, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9quickSortI22CONTACT_KEY_TOKEN_COMPEEvRKT_(%class.btAlignedObjectArray.0* %keycontacts, %class.CONTACT_KEY_TOKEN_COMP* nonnull align 1 dereferenceable(1) %ref.tmp14)
  store i32 0, i32* %coincident_count, align 4
  %array.begin = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %coincident_normals, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 8
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %for.end
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %for.end ], [ %arrayctor.next, %arrayctor.loop ]
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %call16 = call nonnull align 4 dereferenceable(8) %struct.CONTACT_KEY_TOKEN* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEixEi(%class.btAlignedObjectArray.0* %keycontacts, i32 0)
  %m_key = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %call16, i32 0, i32 0
  %18 = load i32, i32* %m_key, align 4
  store i32 %18, i32* %last_key, align 4
  store i32 0, i32* %key, align 4
  %19 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  %20 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %21 = bitcast %class.btContactArray* %20 to %class.btAlignedObjectArray*
  %call17 = call nonnull align 4 dereferenceable(8) %struct.CONTACT_KEY_TOKEN* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEixEi(%class.btAlignedObjectArray.0* %keycontacts, i32 0)
  %m_value = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %call17, i32 0, i32 1
  %22 = load i32, i32* %m_value, align 4
  %call18 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %21, i32 %22)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_(%class.btAlignedObjectArray* %19, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %call18)
  %23 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  %call19 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZN20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %23, i32 0)
  store %class.GIM_CONTACT* %call19, %class.GIM_CONTACT** %pcontact, align 4
  store i32 1, i32* %i, align 4
  br label %for.cond20

for.cond20:                                       ; preds = %for.inc57, %arrayctor.cont
  %24 = load i32, i32* %i, align 4
  %call21 = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %keycontacts)
  %cmp22 = icmp slt i32 %24, %call21
  br i1 %cmp22, label %for.body23, label %for.end59

for.body23:                                       ; preds = %for.cond20
  %25 = load i32, i32* %i, align 4
  %call24 = call nonnull align 4 dereferenceable(8) %struct.CONTACT_KEY_TOKEN* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEixEi(%class.btAlignedObjectArray.0* %keycontacts, i32 %25)
  %m_key25 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %call24, i32 0, i32 0
  %26 = load i32, i32* %m_key25, align 4
  store i32 %26, i32* %key, align 4
  %27 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %28 = bitcast %class.btContactArray* %27 to %class.btAlignedObjectArray*
  %29 = load i32, i32* %i, align 4
  %call26 = call nonnull align 4 dereferenceable(8) %struct.CONTACT_KEY_TOKEN* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEixEi(%class.btAlignedObjectArray.0* %keycontacts, i32 %29)
  %m_value27 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %call26, i32 0, i32 1
  %30 = load i32, i32* %m_value27, align 4
  %call28 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %28, i32 %30)
  store %class.GIM_CONTACT* %call28, %class.GIM_CONTACT** %scontact, align 4
  %31 = load i32, i32* %last_key, align 4
  %32 = load i32, i32* %key, align 4
  %cmp29 = icmp eq i32 %31, %32
  br i1 %cmp29, label %if.then30, label %if.else48

if.then30:                                        ; preds = %for.body23
  %33 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %pcontact, align 4
  %m_depth = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %33, i32 0, i32 2
  %34 = load float, float* %m_depth, align 4
  %sub = fsub float %34, 0x3EE4F8B580000000
  %35 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4
  %m_depth31 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %35, i32 0, i32 2
  %36 = load float, float* %m_depth31, align 4
  %cmp32 = fcmp ogt float %sub, %36
  br i1 %cmp32, label %if.then33, label %if.else

if.then33:                                        ; preds = %if.then30
  %37 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4
  %38 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %pcontact, align 4
  %39 = bitcast %class.GIM_CONTACT* %38 to i8*
  %40 = bitcast %class.GIM_CONTACT* %37 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %39, i8* align 4 %40, i32 48, i1 false)
  store i32 0, i32* %coincident_count, align 4
  br label %if.end47

if.else:                                          ; preds = %if.then30
  %41 = load i8, i8* %normal_contact_average.addr, align 1
  %tobool = trunc i8 %41 to i1
  br i1 %tobool, label %if.then34, label %if.end46

if.then34:                                        ; preds = %if.else
  %42 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %pcontact, align 4
  %m_depth35 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %42, i32 0, i32 2
  %43 = load float, float* %m_depth35, align 4
  %44 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4
  %m_depth36 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %44, i32 0, i32 2
  %45 = load float, float* %m_depth36, align 4
  %sub37 = fsub float %43, %45
  %call38 = call float @_Z6btFabsf(float %sub37)
  %cmp39 = fcmp olt float %call38, 0x3EE4F8B580000000
  br i1 %cmp39, label %if.then40, label %if.end45

if.then40:                                        ; preds = %if.then34
  %46 = load i32, i32* %coincident_count, align 4
  %cmp41 = icmp slt i32 %46, 8
  br i1 %cmp41, label %if.then42, label %if.end44

if.then42:                                        ; preds = %if.then40
  %47 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4
  %m_normal = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %47, i32 0, i32 1
  %48 = load i32, i32* %coincident_count, align 4
  %arrayidx = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %coincident_normals, i32 0, i32 %48
  %49 = bitcast %class.btVector3* %arrayidx to i8*
  %50 = bitcast %class.btVector3* %m_normal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %49, i8* align 4 %50, i32 16, i1 false)
  %51 = load i32, i32* %coincident_count, align 4
  %inc43 = add nsw i32 %51, 1
  store i32 %inc43, i32* %coincident_count, align 4
  br label %if.end44

if.end44:                                         ; preds = %if.then42, %if.then40
  br label %if.end45

if.end45:                                         ; preds = %if.end44, %if.then34
  br label %if.end46

if.end46:                                         ; preds = %if.end45, %if.else
  br label %if.end47

if.end47:                                         ; preds = %if.end46, %if.then33
  br label %if.end56

if.else48:                                        ; preds = %for.body23
  %52 = load i8, i8* %normal_contact_average.addr, align 1
  %tobool49 = trunc i8 %52 to i1
  br i1 %tobool49, label %land.lhs.true, label %if.end52

land.lhs.true:                                    ; preds = %if.else48
  %53 = load i32, i32* %coincident_count, align 4
  %cmp50 = icmp sgt i32 %53, 0
  br i1 %cmp50, label %if.then51, label %if.end52

if.then51:                                        ; preds = %land.lhs.true
  %54 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %pcontact, align 4
  %arraydecay = getelementptr inbounds [8 x %class.btVector3], [8 x %class.btVector3]* %coincident_normals, i32 0, i32 0
  %55 = load i32, i32* %coincident_count, align 4
  call void @_ZN11GIM_CONTACT19interpolate_normalsEP9btVector3i(%class.GIM_CONTACT* %54, %class.btVector3* %arraydecay, i32 %55)
  store i32 0, i32* %coincident_count, align 4
  br label %if.end52

if.end52:                                         ; preds = %if.then51, %land.lhs.true, %if.else48
  %56 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  %57 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %scontact, align 4
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_(%class.btAlignedObjectArray* %56, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %57)
  %58 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  %59 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  %call53 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %59)
  %sub54 = sub nsw i32 %call53, 1
  %call55 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZN20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %58, i32 %sub54)
  store %class.GIM_CONTACT* %call55, %class.GIM_CONTACT** %pcontact, align 4
  br label %if.end56

if.end56:                                         ; preds = %if.end52, %if.end47
  %60 = load i32, i32* %key, align 4
  store i32 %60, i32* %last_key, align 4
  br label %for.inc57

for.inc57:                                        ; preds = %if.end56
  %61 = load i32, i32* %i, align 4
  %inc58 = add nsw i32 %61, 1
  store i32 %inc58, i32* %i, align 4
  br label %for.cond20

for.end59:                                        ; preds = %for.cond20
  %call60 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENED2Ev(%class.btAlignedObjectArray.0* %keycontacts) #6
  br label %return

return:                                           ; preds = %for.end59, %if.then4, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_(%class.btAlignedObjectArray* %this, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.GIM_CONTACT*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.GIM_CONTACT* %_Val, %class.GIM_CONTACT** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %1, i32 %2
  %3 = bitcast %class.GIM_CONTACT* %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.GIM_CONTACT*
  %5 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %_Val.addr, align 4
  %call5 = call %class.GIM_CONTACT* @_ZN11GIM_CONTACTC2ERKS_(%class.GIM_CONTACT* %4, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %5)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %6 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %0, i32 %1
  ret %class.GIM_CONTACT* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.CONTACT_KEY_TOKEN*
  store %struct.CONTACT_KEY_TOKEN* %2, %struct.CONTACT_KEY_TOKEN** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %3 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %struct.CONTACT_KEY_TOKEN* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.CONTACT_KEY_TOKEN* %4, %struct.CONTACT_KEY_TOKEN** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9push_backERKS0_(%class.btAlignedObjectArray.0* %this, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store %struct.CONTACT_KEY_TOKEN* %_Val, %struct.CONTACT_KEY_TOKEN** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %1 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %1, i32 %2
  %3 = bitcast %struct.CONTACT_KEY_TOKEN* %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.CONTACT_KEY_TOKEN*
  %5 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %_Val.addr, align 4
  %call5 = call %struct.CONTACT_KEY_TOKEN* @_ZN17CONTACT_KEY_TOKENC2ERKS_(%struct.CONTACT_KEY_TOKEN* %4, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %5)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %6 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK11GIM_CONTACT16calc_key_contactEv(%class.GIM_CONTACT* %this) #2 comdat {
entry:
  %this.addr = alloca %class.GIM_CONTACT*, align 4
  %_coords = alloca [3 x i32], align 4
  %_hash = alloca i32, align 4
  %_uitmp = alloca i32*, align 4
  store %class.GIM_CONTACT* %this, %class.GIM_CONTACT** %this.addr, align 4
  %this1 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %this.addr, align 4
  %arrayinit.begin = getelementptr inbounds [3 x i32], [3 x i32]* %_coords, i32 0, i32 0
  %m_point = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_point)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %0 = load float, float* %arrayidx, align 4
  %mul = fmul float %0, 1.000000e+03
  %add = fadd float %mul, 1.000000e+00
  %conv = fptosi float %add to i32
  store i32 %conv, i32* %arrayinit.begin, align 4
  %arrayinit.element = getelementptr inbounds i32, i32* %arrayinit.begin, i32 1
  %m_point2 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 0
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_point2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 1
  %1 = load float, float* %arrayidx4, align 4
  %mul5 = fmul float %1, 1.333000e+03
  %conv6 = fptosi float %mul5 to i32
  store i32 %conv6, i32* %arrayinit.element, align 4
  %arrayinit.element7 = getelementptr inbounds i32, i32* %arrayinit.element, i32 1
  %m_point8 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 0
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_point8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 2
  %2 = load float, float* %arrayidx10, align 4
  %mul11 = fmul float %2, 2.133000e+03
  %add12 = fadd float %mul11, 3.000000e+00
  %conv13 = fptosi float %add12 to i32
  store i32 %conv13, i32* %arrayinit.element7, align 4
  store i32 0, i32* %_hash, align 4
  %arrayidx14 = getelementptr inbounds [3 x i32], [3 x i32]* %_coords, i32 0, i32 0
  store i32* %arrayidx14, i32** %_uitmp, align 4
  %3 = load i32*, i32** %_uitmp, align 4
  %4 = load i32, i32* %3, align 4
  store i32 %4, i32* %_hash, align 4
  %5 = load i32*, i32** %_uitmp, align 4
  %incdec.ptr = getelementptr inbounds i32, i32* %5, i32 1
  store i32* %incdec.ptr, i32** %_uitmp, align 4
  %6 = load i32*, i32** %_uitmp, align 4
  %7 = load i32, i32* %6, align 4
  %shl = shl i32 %7, 4
  %8 = load i32, i32* %_hash, align 4
  %add15 = add i32 %8, %shl
  store i32 %add15, i32* %_hash, align 4
  %9 = load i32*, i32** %_uitmp, align 4
  %incdec.ptr16 = getelementptr inbounds i32, i32* %9, i32 1
  store i32* %incdec.ptr16, i32** %_uitmp, align 4
  %10 = load i32*, i32** %_uitmp, align 4
  %11 = load i32, i32* %10, align 4
  %shl17 = shl i32 %11, 8
  %12 = load i32, i32* %_hash, align 4
  %add18 = add i32 %12, %shl17
  store i32 %add18, i32* %_hash, align 4
  %13 = load i32, i32* %_hash, align 4
  ret i32 %13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.CONTACT_KEY_TOKEN* @_ZN17CONTACT_KEY_TOKENC2Eji(%struct.CONTACT_KEY_TOKEN* returned %this, i32 %key, i32 %token) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  %key.addr = alloca i32, align 4
  %token.addr = alloca i32, align 4
  store %struct.CONTACT_KEY_TOKEN* %this, %struct.CONTACT_KEY_TOKEN** %this.addr, align 4
  store i32 %key, i32* %key.addr, align 4
  store i32 %token, i32* %token.addr, align 4
  %this1 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %this.addr, align 4
  %0 = load i32, i32* %key.addr, align 4
  %m_key = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %this1, i32 0, i32 0
  store i32 %0, i32* %m_key, align 4
  %1 = load i32, i32* %token.addr, align 4
  %m_value = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %this1, i32 0, i32 1
  store i32 %1, i32* %m_value, align 4
  ret %struct.CONTACT_KEY_TOKEN* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9quickSortI22CONTACT_KEY_TOKEN_COMPEEvRKT_(%class.btAlignedObjectArray.0* %this, %class.CONTACT_KEY_TOKEN_COMP* nonnull align 1 dereferenceable(1) %CompareFunc) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %CompareFunc.addr = alloca %class.CONTACT_KEY_TOKEN_COMP*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store %class.CONTACT_KEY_TOKEN_COMP* %CompareFunc, %class.CONTACT_KEY_TOKEN_COMP** %CompareFunc.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.CONTACT_KEY_TOKEN_COMP*, %class.CONTACT_KEY_TOKEN_COMP** %CompareFunc.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.CONTACT_KEY_TOKEN_COMP* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.CONTACT_KEY_TOKEN* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %0, i32 %1
  ret %struct.CONTACT_KEY_TOKEN* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZN20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %0, i32 %1
  ret %class.GIM_CONTACT* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11GIM_CONTACT19interpolate_normalsEP9btVector3i(%class.GIM_CONTACT* %this, %class.btVector3* %normals, i32 %normal_count) #2 comdat {
entry:
  %this.addr = alloca %class.GIM_CONTACT*, align 4
  %normals.addr = alloca %class.btVector3*, align 4
  %normal_count.addr = alloca i32, align 4
  %vec_sum = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %vec_sum_len = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.GIM_CONTACT* %this, %class.GIM_CONTACT** %this.addr, align 4
  store %class.btVector3* %normals, %class.btVector3** %normals.addr, align 4
  store i32 %normal_count, i32* %normal_count.addr, align 4
  %this1 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %this.addr, align 4
  %m_normal = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 1
  %0 = bitcast %class.btVector3* %vec_sum to i8*
  %1 = bitcast %class.btVector3* %m_normal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %normal_count.addr, align 4
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btVector3*, %class.btVector3** %normals.addr, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %vec_sum, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call2 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vec_sum)
  store float %call2, float* %vec_sum_len, align 4
  %7 = load float, float* %vec_sum_len, align 4
  %cmp3 = fcmp olt float %7, 0x3EE4F8B580000000
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.end
  br label %return

if.end:                                           ; preds = %for.end
  %8 = load float, float* %vec_sum_len, align 4
  %call5 = call float @_Z6btSqrtf(float %8)
  store float %call5, float* %ref.tmp4, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %vec_sum, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %m_normal6 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 1
  %9 = bitcast %class.btVector3* %m_normal6 to i8*
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btContactArray21merge_contacts_uniqueERKS_(%class.btContactArray* %this, %class.btContactArray* nonnull align 4 dereferenceable(17) %contacts) #2 {
entry:
  %this.addr = alloca %class.btContactArray*, align 4
  %contacts.addr = alloca %class.btContactArray*, align 4
  %average_contact = alloca %class.GIM_CONTACT, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %divide_average = alloca float, align 4
  store %class.btContactArray* %this, %class.btContactArray** %this.addr, align 4
  store %class.btContactArray* %contacts, %class.btContactArray** %contacts.addr, align 4
  %this1 = load %class.btContactArray*, %class.btContactArray** %this.addr, align 4
  %0 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE5clearEv(%class.btAlignedObjectArray* %0)
  %1 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %2 = bitcast %class.btContactArray* %1 to %class.btAlignedObjectArray*
  %call = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %2)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %3 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %4 = bitcast %class.btContactArray* %3 to %class.btAlignedObjectArray*
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %4)
  %cmp3 = icmp eq i32 %call2, 1
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %5 = bitcast %class.btContactArray* %this1 to %class.btAlignedObjectArray*
  %6 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %7 = bitcast %class.btContactArray* %6 to %class.btAlignedObjectArray*
  %call5 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %7, i32 0)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9push_backERKS0_(%class.btAlignedObjectArray* %5, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %call5)
  br label %return

if.end6:                                          ; preds = %if.end
  %8 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %9 = bitcast %class.btContactArray* %8 to %class.btAlignedObjectArray*
  %call7 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %9, i32 0)
  %call8 = call %class.GIM_CONTACT* @_ZN11GIM_CONTACTC2ERKS_(%class.GIM_CONTACT* %average_contact, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %call7)
  store i32 1, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end6
  %10 = load i32, i32* %i, align 4
  %11 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %12 = bitcast %class.btContactArray* %11 to %class.btAlignedObjectArray*
  %call9 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %12)
  %cmp10 = icmp slt i32 %10, %call9
  br i1 %cmp10, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %14 = bitcast %class.btContactArray* %13 to %class.btAlignedObjectArray*
  %15 = load i32, i32* %i, align 4
  %call11 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %14, i32 %15)
  %m_point = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %call11, i32 0, i32 0
  %m_point12 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 0
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_point12, %class.btVector3* nonnull align 4 dereferenceable(16) %m_point)
  %16 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %17 = bitcast %class.btContactArray* %16 to %class.btAlignedObjectArray*
  %18 = load i32, i32* %i, align 4
  %call14 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %17, i32 %18)
  %m_normal = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %call14, i32 0, i32 1
  %19 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %20 = bitcast %class.btContactArray* %19 to %class.btAlignedObjectArray*
  %21 = load i32, i32* %i, align 4
  %call15 = call nonnull align 4 dereferenceable(48) %class.GIM_CONTACT* @_ZNK20btAlignedObjectArrayI11GIM_CONTACTEixEi(%class.btAlignedObjectArray* %20, i32 %21)
  %m_depth = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %call15, i32 0, i32 2
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_normal, float* nonnull align 4 dereferenceable(4) %m_depth)
  %m_normal16 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_normal16, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %22 = load i32, i32* %i, align 4
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %23 = load %class.btContactArray*, %class.btContactArray** %contacts.addr, align 4
  %24 = bitcast %class.btContactArray* %23 to %class.btAlignedObjectArray*
  %call18 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %24)
  %conv = sitofp i32 %call18 to float
  %div = fdiv float 1.000000e+00, %conv
  store float %div, float* %divide_average, align 4
  %m_point19 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 0
  %call20 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_point19, float* nonnull align 4 dereferenceable(4) %divide_average)
  %m_normal21 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %m_normal21, float* nonnull align 4 dereferenceable(4) %divide_average)
  %m_normal23 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 1
  %call24 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %m_normal23)
  %m_depth25 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 2
  store float %call24, float* %m_depth25, align 4
  %m_depth26 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 2
  %m_normal27 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %average_contact, i32 0, i32 1
  %call28 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %m_normal27, float* nonnull align 4 dereferenceable(4) %m_depth26)
  br label %return

return:                                           ; preds = %for.end, %if.then4, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.GIM_CONTACT* @_ZN11GIM_CONTACTC2ERKS_(%class.GIM_CONTACT* returned %this, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %contact) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.GIM_CONTACT*, align 4
  %contact.addr = alloca %class.GIM_CONTACT*, align 4
  store %class.GIM_CONTACT* %this, %class.GIM_CONTACT** %this.addr, align 4
  store %class.GIM_CONTACT* %contact, %class.GIM_CONTACT** %contact.addr, align 4
  %this1 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %this.addr, align 4
  %m_point = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 0
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4
  %m_point2 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %0, i32 0, i32 0
  %1 = bitcast %class.btVector3* %m_point to i8*
  %2 = bitcast %class.btVector3* %m_point2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_normal = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 1
  %3 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4
  %m_normal3 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %3, i32 0, i32 1
  %4 = bitcast %class.btVector3* %m_normal to i8*
  %5 = bitcast %class.btVector3* %m_normal3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_depth = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 2
  %6 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4
  %m_depth4 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %6, i32 0, i32 2
  %7 = load float, float* %m_depth4, align 4
  store float %7, float* %m_depth, align 4
  %m_feature1 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 4
  %8 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4
  %m_feature15 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %8, i32 0, i32 4
  %9 = load i32, i32* %m_feature15, align 4
  store i32 %9, i32* %m_feature1, align 4
  %m_feature2 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %this1, i32 0, i32 5
  %10 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %contact.addr, align 4
  %m_feature26 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %10, i32 0, i32 5
  %11 = load i32, i32* %m_feature26, align 4
  store i32 %11, i32* %m_feature2, align 4
  ret %class.GIM_CONTACT* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  %2 = load float, float* %1, align 4
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.GIM_CONTACT*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI11GIM_CONTACTE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.GIM_CONTACT*
  store %class.GIM_CONTACT* %2, %class.GIM_CONTACT** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.GIM_CONTACT* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.GIM_CONTACT* %4, %class.GIM_CONTACT** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI11GIM_CONTACTE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI11GIM_CONTACTE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.GIM_CONTACT* @_ZN18btAlignedAllocatorI11GIM_CONTACTLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.GIM_CONTACT** null)
  %2 = bitcast %class.GIM_CONTACT* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI11GIM_CONTACTE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.GIM_CONTACT* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.GIM_CONTACT*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.GIM_CONTACT* %dest, %class.GIM_CONTACT** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %3, i32 %4
  %5 = bitcast %class.GIM_CONTACT* %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.GIM_CONTACT*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %7, i32 %8
  %call = call %class.GIM_CONTACT* @_ZN11GIM_CONTACTC2ERKS_(%class.GIM_CONTACT* %6, %class.GIM_CONTACT* nonnull align 4 dereferenceable(48) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.GIM_CONTACT, %class.GIM_CONTACT* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data, align 4
  %tobool = icmp ne %class.GIM_CONTACT* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI11GIM_CONTACTLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.GIM_CONTACT* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.GIM_CONTACT* null, %class.GIM_CONTACT** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.GIM_CONTACT* @_ZN18btAlignedAllocatorI11GIM_CONTACTLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.GIM_CONTACT** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.GIM_CONTACT**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.GIM_CONTACT** %hint, %class.GIM_CONTACT*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 48, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.GIM_CONTACT*
  ret %class.GIM_CONTACT* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI11GIM_CONTACTLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.GIM_CONTACT* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.GIM_CONTACT*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.GIM_CONTACT* %ptr, %class.GIM_CONTACT** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.GIM_CONTACT*, %class.GIM_CONTACT** %ptr.addr, align 4
  %1 = bitcast %class.GIM_CONTACT* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11GIM_CONTACTE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.GIM_CONTACT* null, %class.GIM_CONTACT** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.CONTACT_KEY_TOKEN* null, %struct.CONTACT_KEY_TOKEN** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data, align 4
  %tobool = icmp ne %struct.CONTACT_KEY_TOKEN* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.CONTACT_KEY_TOKEN* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.CONTACT_KEY_TOKEN* null, %struct.CONTACT_KEY_TOKEN** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.CONTACT_KEY_TOKEN* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %struct.CONTACT_KEY_TOKEN* %ptr, %struct.CONTACT_KEY_TOKEN** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %ptr.addr, align 4
  %1 = bitcast %struct.CONTACT_KEY_TOKEN* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.CONTACT_KEY_TOKEN* @_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %struct.CONTACT_KEY_TOKEN** null)
  %2 = bitcast %struct.CONTACT_KEY_TOKEN* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %struct.CONTACT_KEY_TOKEN* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.CONTACT_KEY_TOKEN* %dest, %struct.CONTACT_KEY_TOKEN** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %3, i32 %4
  %5 = bitcast %struct.CONTACT_KEY_TOKEN* %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.CONTACT_KEY_TOKEN*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %7, i32 %8
  %call = call %struct.CONTACT_KEY_TOKEN* @_ZN17CONTACT_KEY_TOKENC2ERKS_(%struct.CONTACT_KEY_TOKEN* %6, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.CONTACT_KEY_TOKEN* @_ZN18btAlignedAllocatorI17CONTACT_KEY_TOKENLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, %struct.CONTACT_KEY_TOKEN** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.CONTACT_KEY_TOKEN**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.CONTACT_KEY_TOKEN** %hint, %struct.CONTACT_KEY_TOKEN*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 8, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.CONTACT_KEY_TOKEN*
  ret %struct.CONTACT_KEY_TOKEN* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.CONTACT_KEY_TOKEN* @_ZN17CONTACT_KEY_TOKENC2ERKS_(%struct.CONTACT_KEY_TOKEN* returned %this, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %rtoken) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  %rtoken.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  store %struct.CONTACT_KEY_TOKEN* %this, %struct.CONTACT_KEY_TOKEN** %this.addr, align 4
  store %struct.CONTACT_KEY_TOKEN* %rtoken, %struct.CONTACT_KEY_TOKEN** %rtoken.addr, align 4
  %this1 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %this.addr, align 4
  %0 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %rtoken.addr, align 4
  %m_key = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %0, i32 0, i32 0
  %1 = load i32, i32* %m_key, align 4
  %m_key2 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %this1, i32 0, i32 0
  store i32 %1, i32* %m_key2, align 4
  %2 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %rtoken.addr, align 4
  %m_value = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %2, i32 0, i32 1
  %3 = load i32, i32* %m_value, align 4
  %m_value3 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %this1, i32 0, i32 1
  store i32 %3, i32* %m_value3, align 4
  ret %struct.CONTACT_KEY_TOKEN* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvRKT_ii(%class.btAlignedObjectArray.0* %this, %class.CONTACT_KEY_TOKEN_COMP* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %CompareFunc.addr = alloca %class.CONTACT_KEY_TOKEN_COMP*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %struct.CONTACT_KEY_TOKEN, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store %class.CONTACT_KEY_TOKEN_COMP* %CompareFunc, %class.CONTACT_KEY_TOKEN_COMP** %CompareFunc.addr, align 4
  store i32 %lo, i32* %lo.addr, align 4
  store i32 %hi, i32* %hi.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %lo.addr, align 4
  store i32 %0, i32* %i, align 4
  %1 = load i32, i32* %hi.addr, align 4
  store i32 %1, i32* %j, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data, align 4
  %3 = load i32, i32* %lo.addr, align 4
  %4 = load i32, i32* %hi.addr, align 4
  %add = add nsw i32 %3, %4
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %2, i32 %div
  %call = call %struct.CONTACT_KEY_TOKEN* @_ZN17CONTACT_KEY_TOKENC2ERKS_(%struct.CONTACT_KEY_TOKEN* %x, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %5 = load %class.CONTACT_KEY_TOKEN_COMP*, %class.CONTACT_KEY_TOKEN_COMP** %CompareFunc.addr, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %6 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data2, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %6, i32 %7
  %call4 = call zeroext i1 @_ZNK22CONTACT_KEY_TOKEN_COMPclERK17CONTACT_KEY_TOKENS2_(%class.CONTACT_KEY_TOKEN_COMP* %5, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx3, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %x)
  br i1 %call4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond5

while.cond5:                                      ; preds = %while.body9, %while.end
  %9 = load %class.CONTACT_KEY_TOKEN_COMP*, %class.CONTACT_KEY_TOKEN_COMP** %CompareFunc.addr, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %10 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data6, align 4
  %11 = load i32, i32* %j, align 4
  %arrayidx7 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %10, i32 %11
  %call8 = call zeroext i1 @_ZNK22CONTACT_KEY_TOKEN_COMPclERK17CONTACT_KEY_TOKENS2_(%class.CONTACT_KEY_TOKEN_COMP* %9, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %x, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx7)
  br i1 %call8, label %while.body9, label %while.end10

while.body9:                                      ; preds = %while.cond5
  %12 = load i32, i32* %j, align 4
  %dec = add nsw i32 %12, -1
  store i32 %dec, i32* %j, align 4
  br label %while.cond5

while.end10:                                      ; preds = %while.cond5
  %13 = load i32, i32* %i, align 4
  %14 = load i32, i32* %j, align 4
  %cmp = icmp sle i32 %13, %14
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end10
  %15 = load i32, i32* %i, align 4
  %16 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4swapEii(%class.btAlignedObjectArray.0* %this1, i32 %15, i32 %16)
  %17 = load i32, i32* %i, align 4
  %inc11 = add nsw i32 %17, 1
  store i32 %inc11, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %dec12 = add nsw i32 %18, -1
  store i32 %dec12, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end10
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %19 = load i32, i32* %i, align 4
  %20 = load i32, i32* %j, align 4
  %cmp13 = icmp sle i32 %19, %20
  br i1 %cmp13, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %21 = load i32, i32* %lo.addr, align 4
  %22 = load i32, i32* %j, align 4
  %cmp14 = icmp slt i32 %21, %22
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %do.end
  %23 = load %class.CONTACT_KEY_TOKEN_COMP*, %class.CONTACT_KEY_TOKEN_COMP** %CompareFunc.addr, align 4
  %24 = load i32, i32* %lo.addr, align 4
  %25 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.CONTACT_KEY_TOKEN_COMP* nonnull align 1 dereferenceable(1) %23, i32 %24, i32 %25)
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %do.end
  %26 = load i32, i32* %i, align 4
  %27 = load i32, i32* %hi.addr, align 4
  %cmp17 = icmp slt i32 %26, %27
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end16
  %28 = load %class.CONTACT_KEY_TOKEN_COMP*, %class.CONTACT_KEY_TOKEN_COMP** %CompareFunc.addr, align 4
  %29 = load i32, i32* %i, align 4
  %30 = load i32, i32* %hi.addr, align 4
  call void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE17quickSortInternalI22CONTACT_KEY_TOKEN_COMPEEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.CONTACT_KEY_TOKEN_COMP* nonnull align 1 dereferenceable(1) %28, i32 %29, i32 %30)
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %if.end16
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK22CONTACT_KEY_TOKEN_COMPclERK17CONTACT_KEY_TOKENS2_(%class.CONTACT_KEY_TOKEN_COMP* %this, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %a, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %b) #2 comdat {
entry:
  %this.addr = alloca %class.CONTACT_KEY_TOKEN_COMP*, align 4
  %a.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  %b.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  store %class.CONTACT_KEY_TOKEN_COMP* %this, %class.CONTACT_KEY_TOKEN_COMP** %this.addr, align 4
  store %struct.CONTACT_KEY_TOKEN* %a, %struct.CONTACT_KEY_TOKEN** %a.addr, align 4
  store %struct.CONTACT_KEY_TOKEN* %b, %struct.CONTACT_KEY_TOKEN** %b.addr, align 4
  %this1 = load %class.CONTACT_KEY_TOKEN_COMP*, %class.CONTACT_KEY_TOKEN_COMP** %this.addr, align 4
  %0 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %a.addr, align 4
  %1 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %b.addr, align 4
  %call = call zeroext i1 @_ZNK17CONTACT_KEY_TOKENltERKS_(%struct.CONTACT_KEY_TOKEN* %0, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %1)
  ret i1 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI17CONTACT_KEY_TOKENE4swapEii(%class.btAlignedObjectArray.0* %this, i32 %index0, i32 %index1) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.CONTACT_KEY_TOKEN, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %0, i32 %1
  %call = call %struct.CONTACT_KEY_TOKEN* @_ZN17CONTACT_KEY_TOKENC2ERKS_(%struct.CONTACT_KEY_TOKEN* %temp, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %arrayidx)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data2, align 4
  %3 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %2, i32 %3
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %4 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data4, align 4
  %5 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %4, i32 %5
  %6 = bitcast %struct.CONTACT_KEY_TOKEN* %arrayidx5 to i8*
  %7 = bitcast %struct.CONTACT_KEY_TOKEN* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 8, i1 false)
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %8 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %m_data6, align 4
  %9 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %8, i32 %9
  %10 = bitcast %struct.CONTACT_KEY_TOKEN* %arrayidx7 to i8*
  %11 = bitcast %struct.CONTACT_KEY_TOKEN* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 8, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17CONTACT_KEY_TOKENltERKS_(%struct.CONTACT_KEY_TOKEN* %this, %struct.CONTACT_KEY_TOKEN* nonnull align 4 dereferenceable(8) %other) #1 comdat {
entry:
  %this.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  %other.addr = alloca %struct.CONTACT_KEY_TOKEN*, align 4
  store %struct.CONTACT_KEY_TOKEN* %this, %struct.CONTACT_KEY_TOKEN** %this.addr, align 4
  store %struct.CONTACT_KEY_TOKEN* %other, %struct.CONTACT_KEY_TOKEN** %other.addr, align 4
  %this1 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %this.addr, align 4
  %m_key = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_key, align 4
  %1 = load %struct.CONTACT_KEY_TOKEN*, %struct.CONTACT_KEY_TOKEN** %other.addr, align 4
  %m_key2 = getelementptr inbounds %struct.CONTACT_KEY_TOKEN, %struct.CONTACT_KEY_TOKEN* %1, i32 0, i32 0
  %2 = load i32, i32* %m_key2, align 4
  %cmp = icmp ult i32 %0, %2
  ret i1 %cmp
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btContactProcessing.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind readnone speculatable willreturn }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
