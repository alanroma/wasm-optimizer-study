; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btPolyhedralConvexShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btPolyhedralConvexShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btVector3 = type { [4 x float] }
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btConvexPolyhedron = type { i32 (...)**, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btFace*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btFace = type { %class.btAlignedObjectArray.3, [4 x float] }
%class.btAlignedObjectArray.3 = type <{ %class.btAlignedAllocator.4, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.4 = type { i8 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btConvexHullComputer = type { %class.btAlignedObjectArray, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.3 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %"class.btConvexHullComputer::Edge"*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%"class.btConvexHullComputer::Edge" = type { i32, i32, i32 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %struct.GrahamVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.GrahamVector3 = type { %class.btVector3, float, i32 }
%struct.btAngleCompareFunc = type { %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btPolyhedralConvexAabbCachingShape = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8, [3 x i8] }>
%class.btSerializer = type opaque
%struct.btConvexInternalShapeData = type { %struct.btCollisionShapeData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN21btConvexInternalShapeD2Ev = comdat any

$_ZN18btConvexPolyhedronnwEmPv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6expandERKS0_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btConvexHullComputerC2Ev = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_ = comdat any

$_ZN20btConvexHullComputer7computeEPKfiiff = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayI6btFaceEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE6resizeEiRKS0_ = comdat any

$_ZN6btFaceC2Ev = comdat any

$_ZN6btFaceD2Ev = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi = comdat any

$_ZNK20btConvexHullComputer4Edge15getSourceVertexEv = comdat any

$_ZN20btAlignedObjectArrayI6btFaceEixEi = comdat any

$_ZN20btAlignedObjectArrayIiE9push_backERKi = comdat any

$_ZNK20btConvexHullComputer4Edge15getTargetVertexEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK20btConvexHullComputer4Edge17getNextEdgeOfFaceEv = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN9btVector37setZeroEv = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiE8pop_backEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayIiE6removeERKi = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3EC2Ev = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3EixEi = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E9push_backERKS0_ = comdat any

$_ZN13GrahamVector3C2ERK9btVector3i = comdat any

$_Z22GrahamScanConvexHull2DR20btAlignedObjectArrayI13GrahamVector3ES2_RK9btVector3 = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE9push_backERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3ED2Ev = comdat any

$_ZN6btFaceC2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN20btAlignedObjectArrayI6btFaceED2Ev = comdat any

$_ZN20btConvexHullComputerD2Ev = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector36maxDotEPKS_lRf = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZNK34btPolyhedralConvexAabbCachingShape17getNonvirtualAabbERK11btTransformR9btVector3S4_f = comdat any

$_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_ = comdat any

$_ZNK21btConvexInternalShape15getLocalScalingEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN21btConvexInternalShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9getMarginEv = comdat any

$_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK21btConvexInternalShape9serializeEPvP12btSerializer = comdat any

$_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZN34btPolyhedralConvexAabbCachingShapeD2Ev = comdat any

$_ZN34btPolyhedralConvexAabbCachingShapeD0Ev = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEC2Ev = comdat any

$_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZNK20btConvexHullComputer4Edge19getNextEdgeOfVertexEv = comdat any

$_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_ = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E4swapEii = comdat any

$_Z11btAtan2Fastff = comdat any

$_ZN18btAngleCompareFuncC2ERK9btVector3 = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E17quickSortInternalI18btAngleCompareFuncEEvRKT_ii = comdat any

$_Z7btCrossRK9btVector3S1_ = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E8pop_backEv = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK18btAngleCompareFuncclERK13GrahamVector3S2_ = comdat any

$_ZN20btAlignedObjectArrayIiEC2ERKS0_ = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEED2Ev = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x38absoluteEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZNK20btAlignedObjectArrayI13GrahamVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E9allocSizeEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI13GrahamVector3E4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI13GrahamVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI13GrahamVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN18btAlignedAllocatorI6btFaceLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE4initEv = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI6btFaceLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayI6btFaceE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI6btFaceE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI6btFaceLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE9allocSizeEi = comdat any

$_ZNK20btAlignedObjectArrayIiE16findLinearSearchERKi = comdat any

$_ZN20btAlignedObjectArrayIiE13removeAtIndexEi = comdat any

$_ZN20btAlignedObjectArrayIiE4swapEii = comdat any

$_ZN18btAlignedAllocatorI13GrahamVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E4initEv = comdat any

$_ZN20btAlignedObjectArrayI13GrahamVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE9allocSizeEi = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV23btPolyhedralConvexShape = hidden unnamed_addr constant { [33 x i8*] } { [33 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btPolyhedralConvexShape to i8*), i8* bitcast (%class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD1Ev to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD0Ev to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btVector3*)* @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, float, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexInternalShape*, i8*, %class.btSerializer*)* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btPolyhedralConvexShape*, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*, %class.btVector3*, %class.btVector3*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_ to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, align 4
@_ZTV34btPolyhedralConvexAabbCachingShape = hidden unnamed_addr constant { [33 x i8*] } { [33 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI34btPolyhedralConvexAabbCachingShape to i8*), i8* bitcast (%class.btPolyhedralConvexAabbCachingShape* (%class.btPolyhedralConvexAabbCachingShape*)* @_ZN34btPolyhedralConvexAabbCachingShapeD2Ev to i8*), i8* bitcast (void (%class.btPolyhedralConvexAabbCachingShape*)* @_ZN34btPolyhedralConvexAabbCachingShapeD0Ev to i8*), i8* bitcast (void (%class.btPolyhedralConvexAabbCachingShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btPolyhedralConvexAabbCachingShape*, %class.btVector3*)* @_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, float, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, float)* @_ZN21btConvexInternalShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btConvexInternalShape*, i8*, %class.btSerializer*)* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btConvexInternalShape*, %class.btVector3*)* @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btPolyhedralConvexShape*, %class.btVector3*)* @_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*, %class.btVector3*, %class.btVector3*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_ to i8*), i8* bitcast (void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (i1 (%class.btPolyhedralConvexShape*, i32)* @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, align 4
@_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions = internal global [6 x %class.btVector3] zeroinitializer, align 16
@_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions = internal global i32 0, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS23btPolyhedralConvexShape = hidden constant [26 x i8] c"23btPolyhedralConvexShape\00", align 1
@_ZTI21btConvexInternalShape = external constant i8*
@_ZTI23btPolyhedralConvexShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btPolyhedralConvexShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI21btConvexInternalShape to i8*) }, align 4
@_ZTS34btPolyhedralConvexAabbCachingShape = hidden constant [37 x i8] c"34btPolyhedralConvexAabbCachingShape\00", align 1
@_ZTI34btPolyhedralConvexAabbCachingShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([37 x i8], [37 x i8]* @_ZTS34btPolyhedralConvexAabbCachingShape, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btPolyhedralConvexShape to i8*) }, align 4
@.str = private unnamed_addr constant [26 x i8] c"btConvexInternalShapeData\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btPolyhedralConvexShape.cpp, i8* null }]

@_ZN23btPolyhedralConvexShapeD1Ev = hidden unnamed_addr alias %class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*), %class.btPolyhedralConvexShape* (%class.btPolyhedralConvexShape*)* @_ZN23btPolyhedralConvexShapeD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeC2Ev(%class.btPolyhedralConvexShape* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  %0 = bitcast %class.btPolyhedralConvexShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* %0)
  %1 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [33 x i8*] }, { [33 x i8*] }* @_ZTV23btPolyhedralConvexShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_polyhedron = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  store %class.btConvexPolyhedron* null, %class.btConvexPolyhedron** %m_polyhedron, align 4
  ret %class.btPolyhedralConvexShape* %this1
}

declare %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* returned) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define hidden %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeD2Ev(%class.btPolyhedralConvexShape* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btPolyhedralConvexShape*, align 4
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  store %class.btPolyhedralConvexShape* %this1, %class.btPolyhedralConvexShape** %retval, align 4
  %0 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [33 x i8*] }, { [33 x i8*] }* @_ZTV23btPolyhedralConvexShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_polyhedron = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron, align 4
  %tobool = icmp ne %class.btConvexPolyhedron* %1, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_polyhedron2 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %2 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron2, align 4
  %3 = bitcast %class.btConvexPolyhedron* %2 to %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)***
  %vtable = load %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)**, %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)*** %3, align 4
  %vfn = getelementptr inbounds %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)*, %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)** %vtable, i64 0
  %4 = load %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)*, %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)** %vfn, align 4
  %call = call %class.btConvexPolyhedron* %4(%class.btConvexPolyhedron* %2) #7
  %m_polyhedron3 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %5 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron3, align 4
  %6 = bitcast %class.btConvexPolyhedron* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %7 = bitcast %class.btPolyhedralConvexShape* %this1 to %class.btConvexInternalShape*
  %call4 = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeD2Ev(%class.btConvexInternalShape* %7) #7
  %8 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %retval, align 4
  ret %class.btPolyhedralConvexShape* %8
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexInternalShape* @_ZN21btConvexInternalShapeD2Ev(%class.btConvexInternalShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %class.btConvexInternalShape* %this1 to %class.btConvexShape*
  %call = call %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* %0) #7
  ret %class.btConvexInternalShape* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN23btPolyhedralConvexShapeD0Ev(%class.btPolyhedralConvexShape* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #4

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN23btPolyhedralConvexShape28initializePolyhedralFeaturesEi(%class.btPolyhedralConvexShape* %this, i32 %shiftVerticesByMargin) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  %shiftVerticesByMargin.addr = alloca i32, align 4
  %mem = alloca i8*, align 4
  %orgVertices = alloca %class.btAlignedObjectArray, align 4
  %i = alloca i32, align 4
  %newVertex = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %conv = alloca %class.btConvexHullComputer, align 4
  %planeEquations = alloca %class.btAlignedObjectArray, align 4
  %shiftedPlaneEquations = alloca %class.btAlignedObjectArray, align 4
  %p = alloca i32, align 4
  %plane = alloca %class.btVector3, align 4
  %tmpVertices = alloca %class.btAlignedObjectArray, align 4
  %faceNormals = alloca %class.btAlignedObjectArray, align 4
  %numFaces = alloca i32, align 4
  %ref.tmp48 = alloca %class.btVector3, align 4
  %convexUtil = alloca %class.btConvexHullComputer*, align 4
  %tmpFaces = alloca %class.btAlignedObjectArray.0, align 4
  %ref.tmp51 = alloca %struct.btFace, align 4
  %numVertices = alloca i32, align 4
  %ref.tmp56 = alloca %class.btVector3, align 4
  %p58 = alloca i32, align 4
  %i70 = alloca i32, align 4
  %face = alloca i32, align 4
  %firstEdge = alloca %"class.btConvexHullComputer::Edge"*, align 4
  %edge = alloca %"class.btConvexHullComputer::Edge"*, align 4
  %edges77 = alloca [3 x %class.btVector3], align 16
  %numEdges = alloca i32, align 4
  %src = alloca i32, align 4
  %targ = alloca i32, align 4
  %wa = alloca %class.btVector3, align 4
  %wb = alloca %class.btVector3, align 4
  %newEdge = alloca %class.btVector3, align 4
  %planeEq = alloca float, align 4
  %ref.tmp96 = alloca %class.btVector3, align 4
  %v = alloca i32, align 4
  %eq = alloca float, align 4
  %faceWeldThreshold = alloca float, align 4
  %todoFaces = alloca %class.btAlignedObjectArray.3, align 4
  %i149 = alloca i32, align 4
  %coplanarFaceGroup = alloca %class.btAlignedObjectArray.3, align 4
  %refFace = alloca i32, align 4
  %faceA = alloca %struct.btFace*, align 4
  %faceNormalA = alloca %class.btVector3, align 4
  %j = alloca i32, align 4
  %i176 = alloca i32, align 4
  %faceB = alloca %struct.btFace*, align 4
  %faceNormalB = alloca %class.btVector3, align 4
  %did_merge = alloca i8, align 1
  %orgpoints = alloca %class.btAlignedObjectArray.12, align 4
  %averageFaceNormal = alloca %class.btVector3, align 4
  %ref.tmp196 = alloca float, align 4
  %ref.tmp197 = alloca float, align 4
  %ref.tmp198 = alloca float, align 4
  %i200 = alloca i32, align 4
  %face205 = alloca %struct.btFace*, align 4
  %faceNormal = alloca %class.btVector3, align 4
  %f = alloca i32, align 4
  %orgIndex = alloca i32, align 4
  %pt = alloca %class.btVector3, align 4
  %found = alloca i8, align 1
  %i226 = alloca i32, align 4
  %ref.tmp240 = alloca %struct.GrahamVector3, align 4
  %combinedFace = alloca %struct.btFace, align 4
  %i250 = alloca i32, align 4
  %hull = alloca %class.btAlignedObjectArray.12, align 4
  %i265 = alloca i32, align 4
  %k = alloca i32, align 4
  %reject_merge = alloca i8, align 1
  %i292 = alloca i32, align 4
  %j302 = alloca i32, align 4
  %face307 = alloca %struct.btFace*, align 4
  %is_in_current_group = alloca i8, align 1
  %k309 = alloca i32, align 4
  %v324 = alloca i32, align 4
  %i362 = alloca i32, align 4
  %face367 = alloca %struct.btFace, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4
  store i32 %shiftVerticesByMargin, i32* %shiftVerticesByMargin.addr, align 4
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  %m_polyhedron = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %0 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron, align 4
  %tobool = icmp ne %class.btConvexPolyhedron* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_polyhedron2 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron2, align 4
  %2 = bitcast %class.btConvexPolyhedron* %1 to %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)***
  %vtable = load %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)**, %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)*** %2, align 4
  %vfn = getelementptr inbounds %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)*, %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)** %vtable, i64 0
  %3 = load %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)*, %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)** %vfn, align 4
  %call = call %class.btConvexPolyhedron* %3(%class.btConvexPolyhedron* %1) #7
  %m_polyhedron3 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %4 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron3, align 4
  %5 = bitcast %class.btConvexPolyhedron* %4 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call4 = call i8* @_Z22btAlignedAllocInternalmi(i32 132, i32 16)
  store i8* %call4, i8** %mem, align 4
  %6 = load i8*, i8** %mem, align 4
  %call5 = call i8* @_ZN18btConvexPolyhedronnwEmPv(i32 132, i8* %6)
  %7 = bitcast i8* %call5 to %class.btConvexPolyhedron*
  %call6 = call %class.btConvexPolyhedron* @_ZN18btConvexPolyhedronC1Ev(%class.btConvexPolyhedron* %7)
  %m_polyhedron7 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  store %class.btConvexPolyhedron* %7, %class.btConvexPolyhedron** %m_polyhedron7, align 4
  %call8 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %orgVertices)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %8 = load i32, i32* %i, align 4
  %9 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (%class.btPolyhedralConvexShape*)***
  %vtable9 = load i32 (%class.btPolyhedralConvexShape*)**, i32 (%class.btPolyhedralConvexShape*)*** %9, align 4
  %vfn10 = getelementptr inbounds i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vtable9, i64 24
  %10 = load i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vfn10, align 4
  %call11 = call i32 %10(%class.btPolyhedralConvexShape* %this1)
  %cmp = icmp slt i32 %8, %call11
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3E6expandERKS0_(%class.btAlignedObjectArray* %orgVertices, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  store %class.btVector3* %call13, %class.btVector3** %newVertex, align 4
  %11 = load i32, i32* %i, align 4
  %12 = load %class.btVector3*, %class.btVector3** %newVertex, align 4
  %13 = bitcast %class.btPolyhedralConvexShape* %this1 to void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)***
  %vtable14 = load void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)**, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*** %13, align 4
  %vfn15 = getelementptr inbounds void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)** %vtable14, i64 27
  %14 = load void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)** %vfn15, align 4
  call void %14(%class.btPolyhedralConvexShape* %this1, i32 %11, %class.btVector3* nonnull align 4 dereferenceable(16) %12)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call16 = call %class.btConvexHullComputer* @_ZN20btConvexHullComputerC2Ev(%class.btConvexHullComputer* %conv)
  %16 = load i32, i32* %shiftVerticesByMargin.addr, align 4
  %tobool17 = icmp ne i32 %16, 0
  br i1 %tobool17, label %if.then18, label %if.else

if.then18:                                        ; preds = %for.end
  %call19 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %planeEquations)
  call void @_ZN14btGeometryUtil29getPlaneEquationsFromVerticesER20btAlignedObjectArrayI9btVector3ES3_(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %orgVertices, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %planeEquations)
  %call20 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %shiftedPlaneEquations)
  store i32 0, i32* %p, align 4
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc30, %if.then18
  %17 = load i32, i32* %p, align 4
  %call22 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %planeEquations)
  %cmp23 = icmp slt i32 %17, %call22
  br i1 %cmp23, label %for.body24, label %for.end32

for.body24:                                       ; preds = %for.cond21
  %18 = load i32, i32* %p, align 4
  %call25 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %planeEquations, i32 %18)
  %19 = bitcast %class.btVector3* %plane to i8*
  %20 = bitcast %class.btVector3* %call25 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  %21 = bitcast %class.btPolyhedralConvexShape* %this1 to %class.btConvexInternalShape*
  %22 = bitcast %class.btConvexInternalShape* %21 to float (%class.btConvexInternalShape*)***
  %vtable26 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %22, align 4
  %vfn27 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable26, i64 12
  %23 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn27, align 4
  %call28 = call float %23(%class.btConvexInternalShape* %21)
  %call29 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %plane)
  %arrayidx = getelementptr inbounds float, float* %call29, i32 3
  %24 = load float, float* %arrayidx, align 4
  %sub = fsub float %24, %call28
  store float %sub, float* %arrayidx, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %shiftedPlaneEquations, %class.btVector3* nonnull align 4 dereferenceable(16) %plane)
  br label %for.inc30

for.inc30:                                        ; preds = %for.body24
  %25 = load i32, i32* %p, align 4
  %inc31 = add nsw i32 %25, 1
  store i32 %inc31, i32* %p, align 4
  br label %for.cond21

for.end32:                                        ; preds = %for.cond21
  %call33 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %tmpVertices)
  call void @_ZN14btGeometryUtil29getVerticesFromPlaneEquationsERK20btAlignedObjectArrayI9btVector3ERS2_(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %shiftedPlaneEquations, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %tmpVertices)
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %tmpVertices, i32 0)
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %call34)
  %call36 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %tmpVertices)
  %call37 = call float @_ZN20btConvexHullComputer7computeEPKfiiff(%class.btConvexHullComputer* %conv, float* %call35, i32 16, i32 %call36, float 0.000000e+00, float 0.000000e+00)
  %call38 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %tmpVertices) #7
  %call39 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %shiftedPlaneEquations) #7
  %call40 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %planeEquations) #7
  br label %if.end45

if.else:                                          ; preds = %for.end
  %call41 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %orgVertices, i32 0)
  %call42 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %call41)
  %call43 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %orgVertices)
  %call44 = call float @_ZN20btConvexHullComputer7computeEPKfiiff(%class.btConvexHullComputer* %conv, float* %call42, i32 16, i32 %call43, float 0.000000e+00, float 0.000000e+00)
  br label %if.end45

if.end45:                                         ; preds = %if.else, %for.end32
  %call46 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %faceNormals)
  %faces = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %conv, i32 0, i32 2
  %call47 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %faces)
  store i32 %call47, i32* %numFaces, align 4
  %26 = load i32, i32* %numFaces, align 4
  %call49 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp48)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %faceNormals, i32 %26, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp48)
  store %class.btConvexHullComputer* %conv, %class.btConvexHullComputer** %convexUtil, align 4
  %call50 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceEC2Ev(%class.btAlignedObjectArray.0* %tmpFaces)
  %27 = load i32, i32* %numFaces, align 4
  %28 = bitcast %struct.btFace* %ref.tmp51 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %28, i8 0, i32 36, i1 false)
  %call52 = call %struct.btFace* @_ZN6btFaceC2Ev(%struct.btFace* %ref.tmp51)
  call void @_ZN20btAlignedObjectArrayI6btFaceE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %tmpFaces, i32 %27, %struct.btFace* nonnull align 4 dereferenceable(36) %ref.tmp51)
  %call53 = call %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* %ref.tmp51) #7
  %29 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %convexUtil, align 4
  %vertices = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %29, i32 0, i32 0
  %call54 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %vertices)
  store i32 %call54, i32* %numVertices, align 4
  %m_polyhedron55 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %30 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron55, align 4
  %m_vertices = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %30, i32 0, i32 1
  %31 = load i32, i32* %numVertices, align 4
  %call57 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp56)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %m_vertices, i32 %31, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp56)
  store i32 0, i32* %p58, align 4
  br label %for.cond59

for.cond59:                                       ; preds = %for.inc67, %if.end45
  %32 = load i32, i32* %p58, align 4
  %33 = load i32, i32* %numVertices, align 4
  %cmp60 = icmp slt i32 %32, %33
  br i1 %cmp60, label %for.body61, label %for.end69

for.body61:                                       ; preds = %for.cond59
  %34 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %convexUtil, align 4
  %vertices62 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %34, i32 0, i32 0
  %35 = load i32, i32* %p58, align 4
  %call63 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %vertices62, i32 %35)
  %m_polyhedron64 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %36 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron64, align 4
  %m_vertices65 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %36, i32 0, i32 1
  %37 = load i32, i32* %p58, align 4
  %call66 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices65, i32 %37)
  %38 = bitcast %class.btVector3* %call66 to i8*
  %39 = bitcast %class.btVector3* %call63 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %38, i8* align 4 %39, i32 16, i1 false)
  br label %for.inc67

for.inc67:                                        ; preds = %for.body61
  %40 = load i32, i32* %p58, align 4
  %inc68 = add nsw i32 %40, 1
  store i32 %inc68, i32* %p58, align 4
  br label %for.cond59

for.end69:                                        ; preds = %for.cond59
  store i32 0, i32* %i70, align 4
  br label %for.cond71

for.cond71:                                       ; preds = %for.inc145, %for.end69
  %41 = load i32, i32* %i70, align 4
  %42 = load i32, i32* %numFaces, align 4
  %cmp72 = icmp slt i32 %41, %42
  br i1 %cmp72, label %for.body73, label %for.end147

for.body73:                                       ; preds = %for.cond71
  %43 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %convexUtil, align 4
  %faces74 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %43, i32 0, i32 2
  %44 = load i32, i32* %i70, align 4
  %call75 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %faces74, i32 %44)
  %45 = load i32, i32* %call75, align 4
  store i32 %45, i32* %face, align 4
  %46 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %convexUtil, align 4
  %edges = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %46, i32 0, i32 1
  %47 = load i32, i32* %face, align 4
  %call76 = call nonnull align 4 dereferenceable(12) %"class.btConvexHullComputer::Edge"* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi(%class.btAlignedObjectArray.8* %edges, i32 %47)
  store %"class.btConvexHullComputer::Edge"* %call76, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4
  %48 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4
  store %"class.btConvexHullComputer::Edge"* %48, %"class.btConvexHullComputer::Edge"** %edge, align 4
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %edges77, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %for.body73
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %for.body73 ], [ %arrayctor.next, %arrayctor.loop ]
  %call78 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  store i32 0, i32* %numEdges, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %arrayctor.cont
  %49 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %edge, align 4
  %call79 = call i32 @_ZNK20btConvexHullComputer4Edge15getSourceVertexEv(%"class.btConvexHullComputer::Edge"* %49)
  store i32 %call79, i32* %src, align 4
  %50 = load i32, i32* %i70, align 4
  %call80 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %50)
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %call80, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.3* %m_indices, i32* nonnull align 4 dereferenceable(4) %src)
  %51 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %edge, align 4
  %call81 = call i32 @_ZNK20btConvexHullComputer4Edge15getTargetVertexEv(%"class.btConvexHullComputer::Edge"* %51)
  store i32 %call81, i32* %targ, align 4
  %52 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %convexUtil, align 4
  %vertices82 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %52, i32 0, i32 0
  %53 = load i32, i32* %src, align 4
  %call83 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %vertices82, i32 %53)
  %54 = bitcast %class.btVector3* %wa to i8*
  %55 = bitcast %class.btVector3* %call83 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %54, i8* align 4 %55, i32 16, i1 false)
  %56 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %convexUtil, align 4
  %vertices84 = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %56, i32 0, i32 0
  %57 = load i32, i32* %targ, align 4
  %call85 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %vertices84, i32 %57)
  %58 = bitcast %class.btVector3* %wb to i8*
  %59 = bitcast %class.btVector3* %call85 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %58, i8* align 4 %59, i32 16, i1 false)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %newEdge, %class.btVector3* nonnull align 4 dereferenceable(16) %wb, %class.btVector3* nonnull align 4 dereferenceable(16) %wa)
  %call86 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %newEdge)
  %60 = load i32, i32* %numEdges, align 4
  %cmp87 = icmp slt i32 %60, 2
  br i1 %cmp87, label %if.then88, label %if.end91

if.then88:                                        ; preds = %do.body
  %61 = load i32, i32* %numEdges, align 4
  %inc89 = add nsw i32 %61, 1
  store i32 %inc89, i32* %numEdges, align 4
  %arrayidx90 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %edges77, i32 0, i32 %61
  %62 = bitcast %class.btVector3* %arrayidx90 to i8*
  %63 = bitcast %class.btVector3* %newEdge to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %62, i8* align 4 %63, i32 16, i1 false)
  br label %if.end91

if.end91:                                         ; preds = %if.then88, %do.body
  %64 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %edge, align 4
  %call92 = call %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge17getNextEdgeOfFaceEv(%"class.btConvexHullComputer::Edge"* %64)
  store %"class.btConvexHullComputer::Edge"* %call92, %"class.btConvexHullComputer::Edge"** %edge, align 4
  br label %do.cond

do.cond:                                          ; preds = %if.end91
  %65 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %edge, align 4
  %66 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %firstEdge, align 4
  %cmp93 = icmp ne %"class.btConvexHullComputer::Edge"* %65, %66
  br i1 %cmp93, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  store float 0x46293E5940000000, float* %planeEq, align 4
  %67 = load i32, i32* %numEdges, align 4
  %cmp94 = icmp eq i32 %67, 2
  br i1 %cmp94, label %if.then95, label %if.else119

if.then95:                                        ; preds = %do.end
  %arrayidx97 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %edges77, i32 0, i32 0
  %arrayidx98 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %edges77, i32 0, i32 1
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp96, %class.btVector3* %arrayidx97, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx98)
  %68 = load i32, i32* %i70, align 4
  %call99 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %faceNormals, i32 %68)
  %69 = bitcast %class.btVector3* %call99 to i8*
  %70 = bitcast %class.btVector3* %ref.tmp96 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %69, i8* align 4 %70, i32 16, i1 false)
  %71 = load i32, i32* %i70, align 4
  %call100 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %faceNormals, i32 %71)
  %call101 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %call100)
  %72 = load i32, i32* %i70, align 4
  %call102 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %faceNormals, i32 %72)
  %call103 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %call102)
  %73 = load float, float* %call103, align 4
  %74 = load i32, i32* %i70, align 4
  %call104 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %74)
  %m_plane = getelementptr inbounds %struct.btFace, %struct.btFace* %call104, i32 0, i32 1
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %m_plane, i32 0, i32 0
  store float %73, float* %arrayidx105, align 4
  %75 = load i32, i32* %i70, align 4
  %call106 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %faceNormals, i32 %75)
  %call107 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %call106)
  %76 = load float, float* %call107, align 4
  %77 = load i32, i32* %i70, align 4
  %call108 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %77)
  %m_plane109 = getelementptr inbounds %struct.btFace, %struct.btFace* %call108, i32 0, i32 1
  %arrayidx110 = getelementptr inbounds [4 x float], [4 x float]* %m_plane109, i32 0, i32 1
  store float %76, float* %arrayidx110, align 4
  %78 = load i32, i32* %i70, align 4
  %call111 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %faceNormals, i32 %78)
  %call112 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %call111)
  %79 = load float, float* %call112, align 4
  %80 = load i32, i32* %i70, align 4
  %call113 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %80)
  %m_plane114 = getelementptr inbounds %struct.btFace, %struct.btFace* %call113, i32 0, i32 1
  %arrayidx115 = getelementptr inbounds [4 x float], [4 x float]* %m_plane114, i32 0, i32 2
  store float %79, float* %arrayidx115, align 4
  %81 = load float, float* %planeEq, align 4
  %82 = load i32, i32* %i70, align 4
  %call116 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %82)
  %m_plane117 = getelementptr inbounds %struct.btFace, %struct.btFace* %call116, i32 0, i32 1
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %m_plane117, i32 0, i32 3
  store float %81, float* %arrayidx118, align 4
  br label %if.end121

if.else119:                                       ; preds = %do.end
  %83 = load i32, i32* %i70, align 4
  %call120 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %faceNormals, i32 %83)
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %call120)
  br label %if.end121

if.end121:                                        ; preds = %if.else119, %if.then95
  store i32 0, i32* %v, align 4
  br label %for.cond122

for.cond122:                                      ; preds = %for.inc139, %if.end121
  %84 = load i32, i32* %v, align 4
  %85 = load i32, i32* %i70, align 4
  %call123 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %85)
  %m_indices124 = getelementptr inbounds %struct.btFace, %struct.btFace* %call123, i32 0, i32 0
  %call125 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_indices124)
  %cmp126 = icmp slt i32 %84, %call125
  br i1 %cmp126, label %for.body127, label %for.end141

for.body127:                                      ; preds = %for.cond122
  %m_polyhedron128 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %86 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron128, align 4
  %m_vertices129 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %86, i32 0, i32 1
  %87 = load i32, i32* %i70, align 4
  %call130 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %87)
  %m_indices131 = getelementptr inbounds %struct.btFace, %struct.btFace* %call130, i32 0, i32 0
  %88 = load i32, i32* %v, align 4
  %call132 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices131, i32 %88)
  %89 = load i32, i32* %call132, align 4
  %call133 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices129, i32 %89)
  %90 = load i32, i32* %i70, align 4
  %call134 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %faceNormals, i32 %90)
  %call135 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call133, %class.btVector3* nonnull align 4 dereferenceable(16) %call134)
  store float %call135, float* %eq, align 4
  %91 = load float, float* %planeEq, align 4
  %92 = load float, float* %eq, align 4
  %cmp136 = fcmp ogt float %91, %92
  br i1 %cmp136, label %if.then137, label %if.end138

if.then137:                                       ; preds = %for.body127
  %93 = load float, float* %eq, align 4
  store float %93, float* %planeEq, align 4
  br label %if.end138

if.end138:                                        ; preds = %if.then137, %for.body127
  br label %for.inc139

for.inc139:                                       ; preds = %if.end138
  %94 = load i32, i32* %v, align 4
  %inc140 = add nsw i32 %94, 1
  store i32 %inc140, i32* %v, align 4
  br label %for.cond122

for.end141:                                       ; preds = %for.cond122
  %95 = load float, float* %planeEq, align 4
  %fneg = fneg float %95
  %96 = load i32, i32* %i70, align 4
  %call142 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %96)
  %m_plane143 = getelementptr inbounds %struct.btFace, %struct.btFace* %call142, i32 0, i32 1
  %arrayidx144 = getelementptr inbounds [4 x float], [4 x float]* %m_plane143, i32 0, i32 3
  store float %fneg, float* %arrayidx144, align 4
  br label %for.inc145

for.inc145:                                       ; preds = %for.end141
  %97 = load i32, i32* %i70, align 4
  %inc146 = add nsw i32 %97, 1
  store i32 %inc146, i32* %i70, align 4
  br label %for.cond71

for.end147:                                       ; preds = %for.cond71
  store float 0x3FEFF7CEE0000000, float* %faceWeldThreshold, align 4
  %call148 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* %todoFaces)
  store i32 0, i32* %i149, align 4
  br label %for.cond150

for.cond150:                                      ; preds = %for.inc154, %for.end147
  %98 = load i32, i32* %i149, align 4
  %call151 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %tmpFaces)
  %cmp152 = icmp slt i32 %98, %call151
  br i1 %cmp152, label %for.body153, label %for.end156

for.body153:                                      ; preds = %for.cond150
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.3* %todoFaces, i32* nonnull align 4 dereferenceable(4) %i149)
  br label %for.inc154

for.inc154:                                       ; preds = %for.body153
  %99 = load i32, i32* %i149, align 4
  %inc155 = add nsw i32 %99, 1
  store i32 %inc155, i32* %i149, align 4
  br label %for.cond150

for.end156:                                       ; preds = %for.cond150
  br label %while.cond

while.cond:                                       ; preds = %if.end377, %for.end156
  %call157 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %todoFaces)
  %tobool158 = icmp ne i32 %call157, 0
  br i1 %tobool158, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call159 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* %coplanarFaceGroup)
  %call160 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %todoFaces)
  %sub161 = sub nsw i32 %call160, 1
  %call162 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %todoFaces, i32 %sub161)
  %100 = load i32, i32* %call162, align 4
  store i32 %100, i32* %refFace, align 4
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.3* %coplanarFaceGroup, i32* nonnull align 4 dereferenceable(4) %refFace)
  %101 = load i32, i32* %refFace, align 4
  %call163 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %101)
  store %struct.btFace* %call163, %struct.btFace** %faceA, align 4
  call void @_ZN20btAlignedObjectArrayIiE8pop_backEv(%class.btAlignedObjectArray.3* %todoFaces)
  %102 = load %struct.btFace*, %struct.btFace** %faceA, align 4
  %m_plane164 = getelementptr inbounds %struct.btFace, %struct.btFace* %102, i32 0, i32 1
  %arrayidx165 = getelementptr inbounds [4 x float], [4 x float]* %m_plane164, i32 0, i32 0
  %103 = load %struct.btFace*, %struct.btFace** %faceA, align 4
  %m_plane166 = getelementptr inbounds %struct.btFace, %struct.btFace* %103, i32 0, i32 1
  %arrayidx167 = getelementptr inbounds [4 x float], [4 x float]* %m_plane166, i32 0, i32 1
  %104 = load %struct.btFace*, %struct.btFace** %faceA, align 4
  %m_plane168 = getelementptr inbounds %struct.btFace, %struct.btFace* %104, i32 0, i32 1
  %arrayidx169 = getelementptr inbounds [4 x float], [4 x float]* %m_plane168, i32 0, i32 2
  %call170 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %faceNormalA, float* nonnull align 4 dereferenceable(4) %arrayidx165, float* nonnull align 4 dereferenceable(4) %arrayidx167, float* nonnull align 4 dereferenceable(4) %arrayidx169)
  %call171 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %todoFaces)
  %sub172 = sub nsw i32 %call171, 1
  store i32 %sub172, i32* %j, align 4
  br label %for.cond173

for.cond173:                                      ; preds = %for.inc190, %while.body
  %105 = load i32, i32* %j, align 4
  %cmp174 = icmp sge i32 %105, 0
  br i1 %cmp174, label %for.body175, label %for.end191

for.body175:                                      ; preds = %for.cond173
  %106 = load i32, i32* %j, align 4
  %call177 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %todoFaces, i32 %106)
  %107 = load i32, i32* %call177, align 4
  store i32 %107, i32* %i176, align 4
  %108 = load i32, i32* %i176, align 4
  %call178 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %108)
  store %struct.btFace* %call178, %struct.btFace** %faceB, align 4
  %109 = load %struct.btFace*, %struct.btFace** %faceB, align 4
  %m_plane179 = getelementptr inbounds %struct.btFace, %struct.btFace* %109, i32 0, i32 1
  %arrayidx180 = getelementptr inbounds [4 x float], [4 x float]* %m_plane179, i32 0, i32 0
  %110 = load %struct.btFace*, %struct.btFace** %faceB, align 4
  %m_plane181 = getelementptr inbounds %struct.btFace, %struct.btFace* %110, i32 0, i32 1
  %arrayidx182 = getelementptr inbounds [4 x float], [4 x float]* %m_plane181, i32 0, i32 1
  %111 = load %struct.btFace*, %struct.btFace** %faceB, align 4
  %m_plane183 = getelementptr inbounds %struct.btFace, %struct.btFace* %111, i32 0, i32 1
  %arrayidx184 = getelementptr inbounds [4 x float], [4 x float]* %m_plane183, i32 0, i32 2
  %call185 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %faceNormalB, float* nonnull align 4 dereferenceable(4) %arrayidx180, float* nonnull align 4 dereferenceable(4) %arrayidx182, float* nonnull align 4 dereferenceable(4) %arrayidx184)
  %call186 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %faceNormalA, %class.btVector3* nonnull align 4 dereferenceable(16) %faceNormalB)
  %112 = load float, float* %faceWeldThreshold, align 4
  %cmp187 = fcmp ogt float %call186, %112
  br i1 %cmp187, label %if.then188, label %if.end189

if.then188:                                       ; preds = %for.body175
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.3* %coplanarFaceGroup, i32* nonnull align 4 dereferenceable(4) %i176)
  call void @_ZN20btAlignedObjectArrayIiE6removeERKi(%class.btAlignedObjectArray.3* %todoFaces, i32* nonnull align 4 dereferenceable(4) %i176)
  br label %if.end189

if.end189:                                        ; preds = %if.then188, %for.body175
  br label %for.inc190

for.inc190:                                       ; preds = %if.end189
  %113 = load i32, i32* %j, align 4
  %dec = add nsw i32 %113, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond173

for.end191:                                       ; preds = %for.cond173
  store i8 0, i8* %did_merge, align 1
  %call192 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %coplanarFaceGroup)
  %cmp193 = icmp sgt i32 %call192, 1
  br i1 %cmp193, label %if.then194, label %if.end359

if.then194:                                       ; preds = %for.end191
  %call195 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI13GrahamVector3EC2Ev(%class.btAlignedObjectArray.12* %orgpoints)
  store float 0.000000e+00, float* %ref.tmp196, align 4
  store float 0.000000e+00, float* %ref.tmp197, align 4
  store float 0.000000e+00, float* %ref.tmp198, align 4
  %call199 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %averageFaceNormal, float* nonnull align 4 dereferenceable(4) %ref.tmp196, float* nonnull align 4 dereferenceable(4) %ref.tmp197, float* nonnull align 4 dereferenceable(4) %ref.tmp198)
  store i32 0, i32* %i200, align 4
  br label %for.cond201

for.cond201:                                      ; preds = %for.inc246, %if.then194
  %114 = load i32, i32* %i200, align 4
  %call202 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %coplanarFaceGroup)
  %cmp203 = icmp slt i32 %114, %call202
  br i1 %cmp203, label %for.body204, label %for.end248

for.body204:                                      ; preds = %for.cond201
  %115 = load i32, i32* %i200, align 4
  %call206 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %coplanarFaceGroup, i32 %115)
  %116 = load i32, i32* %call206, align 4
  %call207 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %116)
  store %struct.btFace* %call207, %struct.btFace** %face205, align 4
  %117 = load %struct.btFace*, %struct.btFace** %face205, align 4
  %m_plane208 = getelementptr inbounds %struct.btFace, %struct.btFace* %117, i32 0, i32 1
  %arrayidx209 = getelementptr inbounds [4 x float], [4 x float]* %m_plane208, i32 0, i32 0
  %118 = load %struct.btFace*, %struct.btFace** %face205, align 4
  %m_plane210 = getelementptr inbounds %struct.btFace, %struct.btFace* %118, i32 0, i32 1
  %arrayidx211 = getelementptr inbounds [4 x float], [4 x float]* %m_plane210, i32 0, i32 1
  %119 = load %struct.btFace*, %struct.btFace** %face205, align 4
  %m_plane212 = getelementptr inbounds %struct.btFace, %struct.btFace* %119, i32 0, i32 1
  %arrayidx213 = getelementptr inbounds [4 x float], [4 x float]* %m_plane212, i32 0, i32 2
  %call214 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %faceNormal, float* nonnull align 4 dereferenceable(4) %arrayidx209, float* nonnull align 4 dereferenceable(4) %arrayidx211, float* nonnull align 4 dereferenceable(4) %arrayidx213)
  %call215 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %averageFaceNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %faceNormal)
  store i32 0, i32* %f, align 4
  br label %for.cond216

for.cond216:                                      ; preds = %for.inc243, %for.body204
  %120 = load i32, i32* %f, align 4
  %121 = load %struct.btFace*, %struct.btFace** %face205, align 4
  %m_indices217 = getelementptr inbounds %struct.btFace, %struct.btFace* %121, i32 0, i32 0
  %call218 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_indices217)
  %cmp219 = icmp slt i32 %120, %call218
  br i1 %cmp219, label %for.body220, label %for.end245

for.body220:                                      ; preds = %for.cond216
  %122 = load %struct.btFace*, %struct.btFace** %face205, align 4
  %m_indices221 = getelementptr inbounds %struct.btFace, %struct.btFace* %122, i32 0, i32 0
  %123 = load i32, i32* %f, align 4
  %call222 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices221, i32 %123)
  %124 = load i32, i32* %call222, align 4
  store i32 %124, i32* %orgIndex, align 4
  %m_polyhedron223 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %125 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron223, align 4
  %m_vertices224 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %125, i32 0, i32 1
  %126 = load i32, i32* %orgIndex, align 4
  %call225 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices224, i32 %126)
  %127 = bitcast %class.btVector3* %pt to i8*
  %128 = bitcast %class.btVector3* %call225 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %127, i8* align 4 %128, i32 16, i1 false)
  store i8 0, i8* %found, align 1
  store i32 0, i32* %i226, align 4
  br label %for.cond227

for.cond227:                                      ; preds = %for.inc235, %for.body220
  %129 = load i32, i32* %i226, align 4
  %call228 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %orgpoints)
  %cmp229 = icmp slt i32 %129, %call228
  br i1 %cmp229, label %for.body230, label %for.end237

for.body230:                                      ; preds = %for.cond227
  %130 = load i32, i32* %i226, align 4
  %call231 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %orgpoints, i32 %130)
  %m_orgIndex = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call231, i32 0, i32 2
  %131 = load i32, i32* %m_orgIndex, align 4
  %132 = load i32, i32* %orgIndex, align 4
  %cmp232 = icmp eq i32 %131, %132
  br i1 %cmp232, label %if.then233, label %if.end234

if.then233:                                       ; preds = %for.body230
  store i8 1, i8* %found, align 1
  br label %for.end237

if.end234:                                        ; preds = %for.body230
  br label %for.inc235

for.inc235:                                       ; preds = %if.end234
  %133 = load i32, i32* %i226, align 4
  %inc236 = add nsw i32 %133, 1
  store i32 %inc236, i32* %i226, align 4
  br label %for.cond227

for.end237:                                       ; preds = %if.then233, %for.cond227
  %134 = load i8, i8* %found, align 1
  %tobool238 = trunc i8 %134 to i1
  br i1 %tobool238, label %if.end242, label %if.then239

if.then239:                                       ; preds = %for.end237
  %135 = load i32, i32* %orgIndex, align 4
  %call241 = call %struct.GrahamVector3* @_ZN13GrahamVector3C2ERK9btVector3i(%struct.GrahamVector3* %ref.tmp240, %class.btVector3* nonnull align 4 dereferenceable(16) %pt, i32 %135)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E9push_backERKS0_(%class.btAlignedObjectArray.12* %orgpoints, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %ref.tmp240)
  br label %if.end242

if.end242:                                        ; preds = %if.then239, %for.end237
  br label %for.inc243

for.inc243:                                       ; preds = %if.end242
  %136 = load i32, i32* %f, align 4
  %inc244 = add nsw i32 %136, 1
  store i32 %inc244, i32* %f, align 4
  br label %for.cond216

for.end245:                                       ; preds = %for.cond216
  br label %for.inc246

for.inc246:                                       ; preds = %for.end245
  %137 = load i32, i32* %i200, align 4
  %inc247 = add nsw i32 %137, 1
  store i32 %inc247, i32* %i200, align 4
  br label %for.cond201

for.end248:                                       ; preds = %for.cond201
  %call249 = call %struct.btFace* @_ZN6btFaceC2Ev(%struct.btFace* %combinedFace)
  store i32 0, i32* %i250, align 4
  br label %for.cond251

for.cond251:                                      ; preds = %for.inc260, %for.end248
  %138 = load i32, i32* %i250, align 4
  %cmp252 = icmp slt i32 %138, 4
  br i1 %cmp252, label %for.body253, label %for.end262

for.body253:                                      ; preds = %for.cond251
  %call254 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %coplanarFaceGroup, i32 0)
  %139 = load i32, i32* %call254, align 4
  %call255 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %139)
  %m_plane256 = getelementptr inbounds %struct.btFace, %struct.btFace* %call255, i32 0, i32 1
  %140 = load i32, i32* %i250, align 4
  %arrayidx257 = getelementptr inbounds [4 x float], [4 x float]* %m_plane256, i32 0, i32 %140
  %141 = load float, float* %arrayidx257, align 4
  %m_plane258 = getelementptr inbounds %struct.btFace, %struct.btFace* %combinedFace, i32 0, i32 1
  %142 = load i32, i32* %i250, align 4
  %arrayidx259 = getelementptr inbounds [4 x float], [4 x float]* %m_plane258, i32 0, i32 %142
  store float %141, float* %arrayidx259, align 4
  br label %for.inc260

for.inc260:                                       ; preds = %for.body253
  %143 = load i32, i32* %i250, align 4
  %inc261 = add nsw i32 %143, 1
  store i32 %inc261, i32* %i250, align 4
  br label %for.cond251

for.end262:                                       ; preds = %for.cond251
  %call263 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI13GrahamVector3EC2Ev(%class.btAlignedObjectArray.12* %hull)
  %call264 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %averageFaceNormal)
  call void @_Z22GrahamScanConvexHull2DR20btAlignedObjectArrayI13GrahamVector3ES2_RK9btVector3(%class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %orgpoints, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %hull, %class.btVector3* nonnull align 4 dereferenceable(16) %averageFaceNormal)
  store i32 0, i32* %i265, align 4
  br label %for.cond266

for.cond266:                                      ; preds = %for.inc289, %for.end262
  %144 = load i32, i32* %i265, align 4
  %call267 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %hull)
  %cmp268 = icmp slt i32 %144, %call267
  br i1 %cmp268, label %for.body269, label %for.end291

for.body269:                                      ; preds = %for.cond266
  %m_indices270 = getelementptr inbounds %struct.btFace, %struct.btFace* %combinedFace, i32 0, i32 0
  %145 = load i32, i32* %i265, align 4
  %call271 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %hull, i32 %145)
  %m_orgIndex272 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call271, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.3* %m_indices270, i32* nonnull align 4 dereferenceable(4) %m_orgIndex272)
  store i32 0, i32* %k, align 4
  br label %for.cond273

for.cond273:                                      ; preds = %for.inc286, %for.body269
  %146 = load i32, i32* %k, align 4
  %call274 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %orgpoints)
  %cmp275 = icmp slt i32 %146, %call274
  br i1 %cmp275, label %for.body276, label %for.end288

for.body276:                                      ; preds = %for.cond273
  %147 = load i32, i32* %k, align 4
  %call277 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %orgpoints, i32 %147)
  %m_orgIndex278 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call277, i32 0, i32 2
  %148 = load i32, i32* %m_orgIndex278, align 4
  %149 = load i32, i32* %i265, align 4
  %call279 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %hull, i32 %149)
  %m_orgIndex280 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call279, i32 0, i32 2
  %150 = load i32, i32* %m_orgIndex280, align 4
  %cmp281 = icmp eq i32 %148, %150
  br i1 %cmp281, label %if.then282, label %if.end285

if.then282:                                       ; preds = %for.body276
  %151 = load i32, i32* %k, align 4
  %call283 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %orgpoints, i32 %151)
  %m_orgIndex284 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call283, i32 0, i32 2
  store i32 -1, i32* %m_orgIndex284, align 4
  br label %for.end288

if.end285:                                        ; preds = %for.body276
  br label %for.inc286

for.inc286:                                       ; preds = %if.end285
  %152 = load i32, i32* %k, align 4
  %inc287 = add nsw i32 %152, 1
  store i32 %inc287, i32* %k, align 4
  br label %for.cond273

for.end288:                                       ; preds = %if.then282, %for.cond273
  br label %for.inc289

for.inc289:                                       ; preds = %for.end288
  %153 = load i32, i32* %i265, align 4
  %inc290 = add nsw i32 %153, 1
  store i32 %inc290, i32* %i265, align 4
  br label %for.cond266

for.end291:                                       ; preds = %for.cond266
  store i8 0, i8* %reject_merge, align 1
  store i32 0, i32* %i292, align 4
  br label %for.cond293

for.cond293:                                      ; preds = %for.inc349, %for.end291
  %154 = load i32, i32* %i292, align 4
  %call294 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %orgpoints)
  %cmp295 = icmp slt i32 %154, %call294
  br i1 %cmp295, label %for.body296, label %for.end351

for.body296:                                      ; preds = %for.cond293
  %155 = load i32, i32* %i292, align 4
  %call297 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %orgpoints, i32 %155)
  %m_orgIndex298 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call297, i32 0, i32 2
  %156 = load i32, i32* %m_orgIndex298, align 4
  %cmp299 = icmp eq i32 %156, -1
  br i1 %cmp299, label %if.then300, label %if.end301

if.then300:                                       ; preds = %for.body296
  br label %for.inc349

if.end301:                                        ; preds = %for.body296
  store i32 0, i32* %j302, align 4
  br label %for.cond303

for.cond303:                                      ; preds = %for.inc343, %if.end301
  %157 = load i32, i32* %j302, align 4
  %call304 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %tmpFaces)
  %cmp305 = icmp slt i32 %157, %call304
  br i1 %cmp305, label %for.body306, label %for.end345

for.body306:                                      ; preds = %for.cond303
  %158 = load i32, i32* %j302, align 4
  %call308 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %158)
  store %struct.btFace* %call308, %struct.btFace** %face307, align 4
  store i8 0, i8* %is_in_current_group, align 1
  store i32 0, i32* %k309, align 4
  br label %for.cond310

for.cond310:                                      ; preds = %for.inc318, %for.body306
  %159 = load i32, i32* %k309, align 4
  %call311 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %coplanarFaceGroup)
  %cmp312 = icmp slt i32 %159, %call311
  br i1 %cmp312, label %for.body313, label %for.end320

for.body313:                                      ; preds = %for.cond310
  %160 = load i32, i32* %k309, align 4
  %call314 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %coplanarFaceGroup, i32 %160)
  %161 = load i32, i32* %call314, align 4
  %162 = load i32, i32* %j302, align 4
  %cmp315 = icmp eq i32 %161, %162
  br i1 %cmp315, label %if.then316, label %if.end317

if.then316:                                       ; preds = %for.body313
  store i8 1, i8* %is_in_current_group, align 1
  br label %for.end320

if.end317:                                        ; preds = %for.body313
  br label %for.inc318

for.inc318:                                       ; preds = %if.end317
  %163 = load i32, i32* %k309, align 4
  %inc319 = add nsw i32 %163, 1
  store i32 %inc319, i32* %k309, align 4
  br label %for.cond310

for.end320:                                       ; preds = %if.then316, %for.cond310
  %164 = load i8, i8* %is_in_current_group, align 1
  %tobool321 = trunc i8 %164 to i1
  br i1 %tobool321, label %if.then322, label %if.end323

if.then322:                                       ; preds = %for.end320
  br label %for.inc343

if.end323:                                        ; preds = %for.end320
  store i32 0, i32* %v324, align 4
  br label %for.cond325

for.cond325:                                      ; preds = %for.inc337, %if.end323
  %165 = load i32, i32* %v324, align 4
  %166 = load %struct.btFace*, %struct.btFace** %face307, align 4
  %m_indices326 = getelementptr inbounds %struct.btFace, %struct.btFace* %166, i32 0, i32 0
  %call327 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_indices326)
  %cmp328 = icmp slt i32 %165, %call327
  br i1 %cmp328, label %for.body329, label %for.end339

for.body329:                                      ; preds = %for.cond325
  %167 = load %struct.btFace*, %struct.btFace** %face307, align 4
  %m_indices330 = getelementptr inbounds %struct.btFace, %struct.btFace* %167, i32 0, i32 0
  %168 = load i32, i32* %v324, align 4
  %call331 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices330, i32 %168)
  %169 = load i32, i32* %call331, align 4
  %170 = load i32, i32* %i292, align 4
  %call332 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %orgpoints, i32 %170)
  %m_orgIndex333 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call332, i32 0, i32 2
  %171 = load i32, i32* %m_orgIndex333, align 4
  %cmp334 = icmp eq i32 %169, %171
  br i1 %cmp334, label %if.then335, label %if.end336

if.then335:                                       ; preds = %for.body329
  store i8 1, i8* %reject_merge, align 1
  br label %for.end339

if.end336:                                        ; preds = %for.body329
  br label %for.inc337

for.inc337:                                       ; preds = %if.end336
  %172 = load i32, i32* %v324, align 4
  %inc338 = add nsw i32 %172, 1
  store i32 %inc338, i32* %v324, align 4
  br label %for.cond325

for.end339:                                       ; preds = %if.then335, %for.cond325
  %173 = load i8, i8* %reject_merge, align 1
  %tobool340 = trunc i8 %173 to i1
  br i1 %tobool340, label %if.then341, label %if.end342

if.then341:                                       ; preds = %for.end339
  br label %for.end345

if.end342:                                        ; preds = %for.end339
  br label %for.inc343

for.inc343:                                       ; preds = %if.end342, %if.then322
  %174 = load i32, i32* %j302, align 4
  %inc344 = add nsw i32 %174, 1
  store i32 %inc344, i32* %j302, align 4
  br label %for.cond303

for.end345:                                       ; preds = %if.then341, %for.cond303
  %175 = load i8, i8* %reject_merge, align 1
  %tobool346 = trunc i8 %175 to i1
  br i1 %tobool346, label %if.then347, label %if.end348

if.then347:                                       ; preds = %for.end345
  br label %for.end351

if.end348:                                        ; preds = %for.end345
  br label %for.inc349

for.inc349:                                       ; preds = %if.end348, %if.then300
  %176 = load i32, i32* %i292, align 4
  %inc350 = add nsw i32 %176, 1
  store i32 %inc350, i32* %i292, align 4
  br label %for.cond293

for.end351:                                       ; preds = %if.then347, %for.cond293
  %177 = load i8, i8* %reject_merge, align 1
  %tobool352 = trunc i8 %177 to i1
  br i1 %tobool352, label %if.end355, label %if.then353

if.then353:                                       ; preds = %for.end351
  store i8 1, i8* %did_merge, align 1
  %m_polyhedron354 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %178 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron354, align 4
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %178, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayI6btFaceE9push_backERKS0_(%class.btAlignedObjectArray.0* %m_faces, %struct.btFace* nonnull align 4 dereferenceable(36) %combinedFace)
  br label %if.end355

if.end355:                                        ; preds = %if.then353, %for.end351
  %call356 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI13GrahamVector3ED2Ev(%class.btAlignedObjectArray.12* %hull) #7
  %call357 = call %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* %combinedFace) #7
  %call358 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI13GrahamVector3ED2Ev(%class.btAlignedObjectArray.12* %orgpoints) #7
  br label %if.end359

if.end359:                                        ; preds = %if.end355, %for.end191
  %179 = load i8, i8* %did_merge, align 1
  %tobool360 = trunc i8 %179 to i1
  br i1 %tobool360, label %if.end377, label %if.then361

if.then361:                                       ; preds = %if.end359
  store i32 0, i32* %i362, align 4
  br label %for.cond363

for.cond363:                                      ; preds = %for.inc374, %if.then361
  %180 = load i32, i32* %i362, align 4
  %call364 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %coplanarFaceGroup)
  %cmp365 = icmp slt i32 %180, %call364
  br i1 %cmp365, label %for.body366, label %for.end376

for.body366:                                      ; preds = %for.cond363
  %181 = load i32, i32* %i362, align 4
  %call368 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %coplanarFaceGroup, i32 %181)
  %182 = load i32, i32* %call368, align 4
  %call369 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %tmpFaces, i32 %182)
  %call370 = call %struct.btFace* @_ZN6btFaceC2ERKS_(%struct.btFace* %face367, %struct.btFace* nonnull align 4 dereferenceable(36) %call369)
  %m_polyhedron371 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %183 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron371, align 4
  %m_faces372 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %183, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayI6btFaceE9push_backERKS0_(%class.btAlignedObjectArray.0* %m_faces372, %struct.btFace* nonnull align 4 dereferenceable(36) %face367)
  %call373 = call %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* %face367) #7
  br label %for.inc374

for.inc374:                                       ; preds = %for.body366
  %184 = load i32, i32* %i362, align 4
  %inc375 = add nsw i32 %184, 1
  store i32 %inc375, i32* %i362, align 4
  br label %for.cond363

for.end376:                                       ; preds = %for.cond363
  br label %if.end377

if.end377:                                        ; preds = %for.end376, %if.end359
  %call378 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %coplanarFaceGroup) #7
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %m_polyhedron379 = getelementptr inbounds %class.btPolyhedralConvexShape, %class.btPolyhedralConvexShape* %this1, i32 0, i32 1
  %185 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %m_polyhedron379, align 4
  call void @_ZN18btConvexPolyhedron10initializeEv(%class.btConvexPolyhedron* %185)
  %call380 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %todoFaces) #7
  %call381 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceED2Ev(%class.btAlignedObjectArray.0* %tmpFaces) #7
  %call382 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %faceNormals) #7
  %call383 = call %class.btConvexHullComputer* @_ZN20btConvexHullComputerD2Ev(%class.btConvexHullComputer* %conv) #7
  %call384 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %orgVertices) #7
  ret i1 true
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN18btConvexPolyhedronnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

declare %class.btConvexPolyhedron* @_ZN18btConvexPolyhedronC1Ev(%class.btConvexPolyhedron* returned) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3E6expandERKS0_(%class.btAlignedObjectArray* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %fillValue) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %fillValue.addr = alloca %class.btVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btVector3* %fillValue, %class.btVector3** %fillValue.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %1 = load i32, i32* %m_size, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %3 = load i32, i32* %sz, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 %3
  %4 = bitcast %class.btVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %4)
  %5 = bitcast i8* %call5 to %class.btVector3*
  %6 = load %class.btVector3*, %class.btVector3** %fillValue.addr, align 4
  %7 = bitcast %class.btVector3* %5 to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %9 = load %class.btVector3*, %class.btVector3** %m_data6, align 4
  %10 = load i32, i32* %sz, align 4
  %arrayidx7 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 %10
  ret %class.btVector3* %arrayidx7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btConvexHullComputer* @_ZN20btConvexHullComputerC2Ev(%class.btConvexHullComputer* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexHullComputer*, align 4
  store %class.btConvexHullComputer* %this, %class.btConvexHullComputer** %this.addr, align 4
  %this1 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %this.addr, align 4
  %vertices = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %vertices)
  %edges = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEC2Ev(%class.btAlignedObjectArray.8* %edges)
  %faces = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* %faces)
  ret %class.btConvexHullComputer* %this1
}

declare void @_ZN14btGeometryUtil29getPlaneEquationsFromVerticesER20btAlignedObjectArrayI9btVector3ES3_(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17)) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btVector3* %_Val, %class.btVector3** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 %2
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %3)
  %4 = bitcast i8* %call5 to %class.btVector3*
  %5 = load %class.btVector3*, %class.btVector3** %_Val.addr, align 4
  %6 = bitcast %class.btVector3* %4 to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

declare void @_ZN14btGeometryUtil29getVerticesFromPlaneEquationsERK20btAlignedObjectArrayI9btVector3ERS2_(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17), %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17)) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZN20btConvexHullComputer7computeEPKfiiff(%class.btConvexHullComputer* %this, float* %coords, i32 %stride, i32 %count, float %shrink, float %shrinkClamp) #2 comdat {
entry:
  %this.addr = alloca %class.btConvexHullComputer*, align 4
  %coords.addr = alloca float*, align 4
  %stride.addr = alloca i32, align 4
  %count.addr = alloca i32, align 4
  %shrink.addr = alloca float, align 4
  %shrinkClamp.addr = alloca float, align 4
  store %class.btConvexHullComputer* %this, %class.btConvexHullComputer** %this.addr, align 4
  store float* %coords, float** %coords.addr, align 4
  store i32 %stride, i32* %stride.addr, align 4
  store i32 %count, i32* %count.addr, align 4
  store float %shrink, float* %shrink.addr, align 4
  store float %shrinkClamp, float* %shrinkClamp.addr, align 4
  %this1 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %this.addr, align 4
  %0 = load float*, float** %coords.addr, align 4
  %1 = bitcast float* %0 to i8*
  %2 = load i32, i32* %stride.addr, align 4
  %3 = load i32, i32* %count.addr, align 4
  %4 = load float, float* %shrink.addr, align 4
  %5 = load float, float* %shrinkClamp.addr, align 4
  %call = call float @_ZN20btConvexHullComputer7computeEPKvbiiff(%class.btConvexHullComputer* %this1, i8* %1, i1 zeroext false, i32 %2, i32 %3, float %4, float %5)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %class.btVector3*, %class.btVector3** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 %15
  %16 = bitcast %class.btVector3* %arrayidx10 to i8*
  %call11 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %16)
  %17 = bitcast i8* %call11 to %class.btVector3*
  %18 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4
  %19 = bitcast %class.btVector3* %17 to i8*
  %20 = bitcast %class.btVector3* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI6btFaceLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI6btFaceE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %this, i32 %newsize, %struct.btFace* nonnull align 4 dereferenceable(36) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btFace*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btFace* %fillData, %struct.btFace** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %5 = load %struct.btFace*, %struct.btFace** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %5, i32 %6
  %call3 = call %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* %arrayidx) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end16

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp4 = icmp sgt i32 %8, %9
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI6btFaceE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i6, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %12 = load i32, i32* %i6, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp8 = icmp slt i32 %12, %13
  br i1 %cmp8, label %for.body9, label %for.end15

for.body9:                                        ; preds = %for.cond7
  %m_data10 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %14 = load %struct.btFace*, %struct.btFace** %m_data10, align 4
  %15 = load i32, i32* %i6, align 4
  %arrayidx11 = getelementptr inbounds %struct.btFace, %struct.btFace* %14, i32 %15
  %16 = bitcast %struct.btFace* %arrayidx11 to i8*
  %17 = bitcast i8* %16 to %struct.btFace*
  %18 = load %struct.btFace*, %struct.btFace** %fillData.addr, align 4
  %call12 = call %struct.btFace* @_ZN6btFaceC2ERKS_(%struct.btFace* %17, %struct.btFace* nonnull align 4 dereferenceable(36) %18)
  br label %for.inc13

for.inc13:                                        ; preds = %for.body9
  %19 = load i32, i32* %i6, align 4
  %inc14 = add nsw i32 %19, 1
  store i32 %inc14, i32* %i6, align 4
  br label %for.cond7

for.end15:                                        ; preds = %for.cond7
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %20 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %20, i32* %m_size, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btFace* @_ZN6btFaceC2Ev(%struct.btFace* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btFace*, align 4
  store %struct.btFace* %this, %struct.btFace** %this.addr, align 4
  %this1 = load %struct.btFace*, %struct.btFace** %this.addr, align 4
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* %m_indices)
  ret %struct.btFace* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btFace*, align 4
  store %struct.btFace* %this, %struct.btFace** %this.addr, align 4
  %this1 = load %struct.btFace*, %struct.btFace** %this.addr, align 4
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %m_indices) #7
  ret %struct.btFace* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(12) %"class.btConvexHullComputer::Edge"* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %0, i32 %1
  ret %"class.btConvexHullComputer::Edge"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btConvexHullComputer4Edge15getSourceVertexEv(%"class.btConvexHullComputer::Edge"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 1
  %0 = load i32, i32* %reverse, align 4
  %add.ptr = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 %0
  %targetVertex = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %add.ptr, i32 0, i32 2
  %1 = load i32, i32* %targetVertex, align 4
  ret i32 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btFace*, %struct.btFace** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %0, i32 %1
  ret %struct.btFace* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.3* %this, i32* nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %_Val.addr = alloca i32*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32* %_Val, i32** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.3* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.3* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.3* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %1 = load i32*, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  %3 = bitcast i32* %arrayidx to i8*
  %4 = bitcast i8* %3 to i32*
  %5 = load i32*, i32** %_Val.addr, align 4
  %6 = load i32, i32* %5, align 4
  store i32 %6, i32* %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btConvexHullComputer4Edge15getTargetVertexEv(%"class.btConvexHullComputer::Edge"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %targetVertex = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 2
  %0 = load i32, i32* %targetVertex, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge17getNextEdgeOfFaceEv(%"class.btConvexHullComputer::Edge"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %reverse = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 1
  %0 = load i32, i32* %reverse, align 4
  %add.ptr = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 %0
  %call = call %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge19getNextEdgeOfVertexEv(%"class.btConvexHullComputer::Edge"* %add.ptr)
  ret %"class.btConvexHullComputer::Edge"* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector37setZeroEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.4* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.4* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this1)
  ret %class.btAlignedObjectArray.3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE8pop_backEv(%class.btAlignedObjectArray.3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %1 = load i32*, i32** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6removeERKi(%class.btAlignedObjectArray.3* %this, i32* nonnull align 4 dereferenceable(4) %key) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %key.addr = alloca i32*, align 4
  %findIndex = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32* %key, i32** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32*, i32** %key.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE16findLinearSearchERKi(%class.btAlignedObjectArray.3* %this1, i32* nonnull align 4 dereferenceable(4) %0)
  store i32 %call, i32* %findIndex, align 4
  %1 = load i32, i32* %findIndex, align 4
  call void @_ZN20btAlignedObjectArrayIiE13removeAtIndexEi(%class.btAlignedObjectArray.3* %this1, i32 %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI13GrahamVector3EC2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorI13GrahamVector3Lj16EEC2Ev(%class.btAlignedAllocator.13* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E4initEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %0, i32 %1
  ret %struct.GrahamVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E9push_backERKS0_(%class.btAlignedObjectArray.12* %this, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Val.addr = alloca %struct.GrahamVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store %struct.GrahamVector3* %_Val, %struct.GrahamVector3** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI13GrahamVector3E9allocSizeEi(%class.btAlignedObjectArray.12* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E7reserveEi(%class.btAlignedObjectArray.12* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %1 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %1, i32 %2
  %3 = bitcast %struct.GrahamVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 24, i8* %3)
  %4 = bitcast i8* %call5 to %struct.GrahamVector3*
  %5 = load %struct.GrahamVector3*, %struct.GrahamVector3** %_Val.addr, align 4
  %6 = bitcast %struct.GrahamVector3* %4 to i8*
  %7 = bitcast %struct.GrahamVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 24, i1 false)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.GrahamVector3* @_ZN13GrahamVector3C2ERK9btVector3i(%struct.GrahamVector3* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %org, i32 %orgIndex) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.GrahamVector3*, align 4
  %org.addr = alloca %class.btVector3*, align 4
  %orgIndex.addr = alloca i32, align 4
  store %struct.GrahamVector3* %this, %struct.GrahamVector3** %this.addr, align 4
  store %class.btVector3* %org, %class.btVector3** %org.addr, align 4
  store i32 %orgIndex, i32* %orgIndex.addr, align 4
  %this1 = load %struct.GrahamVector3*, %struct.GrahamVector3** %this.addr, align 4
  %0 = bitcast %struct.GrahamVector3* %this1 to %class.btVector3*
  %1 = load %class.btVector3*, %class.btVector3** %org.addr, align 4
  %2 = bitcast %class.btVector3* %0 to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %m_orgIndex = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %this1, i32 0, i32 2
  %4 = load i32, i32* %orgIndex.addr, align 4
  store i32 %4, i32* %m_orgIndex, align 4
  ret %struct.GrahamVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z22GrahamScanConvexHull2DR20btAlignedObjectArrayI13GrahamVector3ES2_RK9btVector3(%class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %originalPoints, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %hull, %class.btVector3* nonnull align 4 dereferenceable(16) %normalAxis) #2 comdat {
entry:
  %originalPoints.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %hull.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %normalAxis.addr = alloca %class.btVector3*, align 4
  %axis0 = alloca %class.btVector3, align 4
  %axis1 = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  %projL = alloca float, align 4
  %projR = alloca float, align 4
  %i22 = alloca i32, align 4
  %ar = alloca %class.btVector3, align 4
  %ar1 = alloca float, align 4
  %ar0 = alloca float, align 4
  %comp = alloca %struct.btAngleCompareFunc, align 4
  %i46 = alloca i32, align 4
  %isConvex = alloca i8, align 1
  %a = alloca %class.btVector3*, align 4
  %b = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp66 = alloca %class.btVector3, align 4
  %ref.tmp67 = alloca %class.btVector3, align 4
  store %class.btAlignedObjectArray.12* %originalPoints, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  store %class.btAlignedObjectArray.12* %hull, %class.btAlignedObjectArray.12** %hull.addr, align 4
  store %class.btVector3* %normalAxis, %class.btVector3** %normalAxis.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis0)
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %axis1)
  %0 = load %class.btVector3*, %class.btVector3** %normalAxis.addr, align 4
  call void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %axis0, %class.btVector3* nonnull align 4 dereferenceable(16) %axis1)
  %1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %1)
  %cmp = icmp sle i32 %call2, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %2 = load i32, i32* %i, align 4
  %3 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %3)
  %cmp4 = icmp slt i32 %2, %call3
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4
  %5 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %5, i32 0)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E9push_backERKS0_(%class.btAlignedObjectArray.12* %4, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %call5)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %for.end83

if.end:                                           ; preds = %entry
  store i32 0, i32* %i6, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc18, %if.end
  %7 = load i32, i32* %i6, align 4
  %8 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %call8 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %8)
  %cmp9 = icmp slt i32 %7, %call8
  br i1 %cmp9, label %for.body10, label %for.end20

for.body10:                                       ; preds = %for.cond7
  %9 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %10 = load i32, i32* %i6, align 4
  %call11 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %9, i32 %10)
  %11 = bitcast %struct.GrahamVector3* %call11 to %class.btVector3*
  %call12 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %axis0)
  store float %call12, float* %projL, align 4
  %12 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %12, i32 0)
  %13 = bitcast %struct.GrahamVector3* %call13 to %class.btVector3*
  %call14 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %axis0)
  store float %call14, float* %projR, align 4
  %14 = load float, float* %projL, align 4
  %15 = load float, float* %projR, align 4
  %cmp15 = fcmp olt float %14, %15
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %for.body10
  %16 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %17 = load i32, i32* %i6, align 4
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E4swapEii(%class.btAlignedObjectArray.12* %16, i32 0, i32 %17)
  br label %if.end17

if.end17:                                         ; preds = %if.then16, %for.body10
  br label %for.inc18

for.inc18:                                        ; preds = %if.end17
  %18 = load i32, i32* %i6, align 4
  %inc19 = add nsw i32 %18, 1
  store i32 %inc19, i32* %i6, align 4
  br label %for.cond7

for.end20:                                        ; preds = %for.cond7
  %19 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %19, i32 0)
  %m_angle = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call21, i32 0, i32 1
  store float 0xC6293E5940000000, float* %m_angle, align 4
  store i32 1, i32* %i22, align 4
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc40, %for.end20
  %20 = load i32, i32* %i22, align 4
  %21 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %call24 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %21)
  %cmp25 = icmp slt i32 %20, %call24
  br i1 %cmp25, label %for.body26, label %for.end42

for.body26:                                       ; preds = %for.cond23
  %22 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %23 = load i32, i32* %i22, align 4
  %call27 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %22, i32 %23)
  %24 = bitcast %struct.GrahamVector3* %call27 to %class.btVector3*
  %25 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %call28 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %25, i32 0)
  %26 = bitcast %struct.GrahamVector3* %call28 to %class.btVector3*
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ar, %class.btVector3* nonnull align 4 dereferenceable(16) %24, %class.btVector3* nonnull align 4 dereferenceable(16) %26)
  %call29 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %ar)
  store float %call29, float* %ar1, align 4
  %call30 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %axis0, %class.btVector3* nonnull align 4 dereferenceable(16) %ar)
  store float %call30, float* %ar0, align 4
  %27 = load float, float* %ar1, align 4
  %28 = load float, float* %ar1, align 4
  %mul = fmul float %27, %28
  %29 = load float, float* %ar0, align 4
  %30 = load float, float* %ar0, align 4
  %mul31 = fmul float %29, %30
  %add = fadd float %mul, %mul31
  %cmp32 = fcmp olt float %add, 0x3E80000000000000
  br i1 %cmp32, label %if.then33, label %if.else

if.then33:                                        ; preds = %for.body26
  %31 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %32 = load i32, i32* %i22, align 4
  %call34 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %31, i32 %32)
  %m_angle35 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call34, i32 0, i32 1
  store float 0.000000e+00, float* %m_angle35, align 4
  br label %if.end39

if.else:                                          ; preds = %for.body26
  %33 = load float, float* %ar1, align 4
  %34 = load float, float* %ar0, align 4
  %call36 = call float @_Z11btAtan2Fastff(float %33, float %34)
  %35 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %36 = load i32, i32* %i22, align 4
  %call37 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %35, i32 %36)
  %m_angle38 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %call37, i32 0, i32 1
  store float %call36, float* %m_angle38, align 4
  br label %if.end39

if.end39:                                         ; preds = %if.else, %if.then33
  br label %for.inc40

for.inc40:                                        ; preds = %if.end39
  %37 = load i32, i32* %i22, align 4
  %inc41 = add nsw i32 %37, 1
  store i32 %inc41, i32* %i22, align 4
  br label %for.cond23

for.end42:                                        ; preds = %for.cond23
  %38 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %call43 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %38, i32 0)
  %39 = bitcast %struct.GrahamVector3* %call43 to %class.btVector3*
  %call44 = call %struct.btAngleCompareFunc* @_ZN18btAngleCompareFuncC2ERK9btVector3(%struct.btAngleCompareFunc* %comp, %class.btVector3* nonnull align 4 dereferenceable(16) %39)
  %40 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %41 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %call45 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %41)
  %sub = sub nsw i32 %call45, 1
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E17quickSortInternalI18btAngleCompareFuncEEvRKT_ii(%class.btAlignedObjectArray.12* %40, %struct.btAngleCompareFunc* nonnull align 4 dereferenceable(16) %comp, i32 1, i32 %sub)
  store i32 0, i32* %i46, align 4
  br label %for.cond47

for.cond47:                                       ; preds = %for.inc51, %for.end42
  %42 = load i32, i32* %i46, align 4
  %cmp48 = icmp slt i32 %42, 2
  br i1 %cmp48, label %for.body49, label %for.end53

for.body49:                                       ; preds = %for.cond47
  %43 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4
  %44 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %45 = load i32, i32* %i46, align 4
  %call50 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %44, i32 %45)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E9push_backERKS0_(%class.btAlignedObjectArray.12* %43, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %call50)
  br label %for.inc51

for.inc51:                                        ; preds = %for.body49
  %46 = load i32, i32* %i46, align 4
  %inc52 = add nsw i32 %46, 1
  store i32 %inc52, i32* %i46, align 4
  br label %for.cond47

for.end53:                                        ; preds = %for.cond47
  br label %for.cond54

for.cond54:                                       ; preds = %for.inc81, %for.end53
  %47 = load i32, i32* %i46, align 4
  %48 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %call55 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %48)
  %cmp56 = icmp ne i32 %47, %call55
  br i1 %cmp56, label %for.body57, label %for.end83

for.body57:                                       ; preds = %for.cond54
  store i8 0, i8* %isConvex, align 1
  br label %while.cond

while.cond:                                       ; preds = %if.end75, %for.body57
  %49 = load i8, i8* %isConvex, align 1
  %tobool = trunc i8 %49 to i1
  br i1 %tobool, label %land.end, label %land.rhs

land.rhs:                                         ; preds = %while.cond
  %50 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4
  %call58 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %50)
  %cmp59 = icmp sgt i32 %call58, 1
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %51 = phi i1 [ false, %while.cond ], [ %cmp59, %land.rhs ]
  br i1 %51, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %52 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4
  %53 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4
  %call60 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %53)
  %sub61 = sub nsw i32 %call60, 2
  %call62 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %52, i32 %sub61)
  %54 = bitcast %struct.GrahamVector3* %call62 to %class.btVector3*
  store %class.btVector3* %54, %class.btVector3** %a, align 4
  %55 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4
  %56 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4
  %call63 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %56)
  %sub64 = sub nsw i32 %call63, 1
  %call65 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %55, i32 %sub64)
  %57 = bitcast %struct.GrahamVector3* %call65 to %class.btVector3*
  store %class.btVector3* %57, %class.btVector3** %b, align 4
  %58 = load %class.btVector3*, %class.btVector3** %a, align 4
  %59 = load %class.btVector3*, %class.btVector3** %b, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp66, %class.btVector3* nonnull align 4 dereferenceable(16) %58, %class.btVector3* nonnull align 4 dereferenceable(16) %59)
  %60 = load %class.btVector3*, %class.btVector3** %a, align 4
  %61 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %62 = load i32, i32* %i46, align 4
  %call68 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %61, i32 %62)
  %63 = bitcast %struct.GrahamVector3* %call68 to %class.btVector3*
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp67, %class.btVector3* nonnull align 4 dereferenceable(16) %60, %class.btVector3* nonnull align 4 dereferenceable(16) %63)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp66, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp67)
  %64 = load %class.btVector3*, %class.btVector3** %normalAxis.addr, align 4
  %call69 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %64)
  %cmp70 = fcmp ogt float %call69, 0.000000e+00
  %frombool = zext i1 %cmp70 to i8
  store i8 %frombool, i8* %isConvex, align 1
  %65 = load i8, i8* %isConvex, align 1
  %tobool71 = trunc i8 %65 to i1
  br i1 %tobool71, label %if.else73, label %if.then72

if.then72:                                        ; preds = %while.body
  %66 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E8pop_backEv(%class.btAlignedObjectArray.12* %66)
  br label %if.end75

if.else73:                                        ; preds = %while.body
  %67 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4
  %68 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %69 = load i32, i32* %i46, align 4
  %call74 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %68, i32 %69)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E9push_backERKS0_(%class.btAlignedObjectArray.12* %67, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %call74)
  br label %if.end75

if.end75:                                         ; preds = %if.else73, %if.then72
  br label %while.cond

while.end:                                        ; preds = %land.end
  %70 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4
  %call76 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %70)
  %cmp77 = icmp eq i32 %call76, 1
  br i1 %cmp77, label %if.then78, label %if.end80

if.then78:                                        ; preds = %while.end
  %71 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %hull.addr, align 4
  %72 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %originalPoints.addr, align 4
  %73 = load i32, i32* %i46, align 4
  %call79 = call nonnull align 4 dereferenceable(24) %struct.GrahamVector3* @_ZN20btAlignedObjectArrayI13GrahamVector3EixEi(%class.btAlignedObjectArray.12* %72, i32 %73)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E9push_backERKS0_(%class.btAlignedObjectArray.12* %71, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %call79)
  br label %if.end80

if.end80:                                         ; preds = %if.then78, %while.end
  br label %for.inc81

for.inc81:                                        ; preds = %if.end80
  %74 = load i32, i32* %i46, align 4
  %inc82 = add nsw i32 %74, 1
  store i32 %inc82, i32* %i46, align 4
  br label %for.cond54

for.end83:                                        ; preds = %for.end, %for.cond54
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE9push_backERKS0_(%class.btAlignedObjectArray.0* %this, %struct.btFace* nonnull align 4 dereferenceable(36) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %struct.btFace*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store %struct.btFace* %_Val, %struct.btFace** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI6btFaceE9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI6btFaceE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %1 = load %struct.btFace*, %struct.btFace** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %1, i32 %2
  %3 = bitcast %struct.btFace* %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.btFace*
  %5 = load %struct.btFace*, %struct.btFace** %_Val.addr, align 4
  %call5 = call %struct.btFace* @_ZN6btFaceC2ERKS_(%struct.btFace* %4, %struct.btFace* nonnull align 4 dereferenceable(36) %5)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %6 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI13GrahamVector3ED2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E5clearEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btFace* @_ZN6btFaceC2ERKS_(%struct.btFace* returned %this, %struct.btFace* nonnull align 4 dereferenceable(36) %0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btFace*, align 4
  %.addr = alloca %struct.btFace*, align 4
  store %struct.btFace* %this, %struct.btFace** %this.addr, align 4
  store %struct.btFace* %0, %struct.btFace** %.addr, align 4
  %this1 = load %struct.btFace*, %struct.btFace** %this.addr, align 4
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %this1, i32 0, i32 0
  %1 = load %struct.btFace*, %struct.btFace** %.addr, align 4
  %m_indices2 = getelementptr inbounds %struct.btFace, %struct.btFace* %1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2ERKS0_(%class.btAlignedObjectArray.3* %m_indices, %class.btAlignedObjectArray.3* nonnull align 4 dereferenceable(17) %m_indices2)
  %m_plane = getelementptr inbounds %struct.btFace, %struct.btFace* %this1, i32 0, i32 1
  %2 = load %struct.btFace*, %struct.btFace** %.addr, align 4
  %m_plane3 = getelementptr inbounds %struct.btFace, %struct.btFace* %2, i32 0, i32 1
  %3 = bitcast [4 x float]* %m_plane to i8*
  %4 = bitcast [4 x float]* %m_plane3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  ret %struct.btFace* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.3* %this1)
  ret %class.btAlignedObjectArray.3* %this1
}

declare void @_ZN18btConvexPolyhedron10initializeEv(%class.btConvexPolyhedron*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI6btFaceE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexHullComputer* @_ZN20btConvexHullComputerD2Ev(%class.btConvexHullComputer* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexHullComputer*, align 4
  store %class.btConvexHullComputer* %this, %class.btConvexHullComputer** %this.addr, align 4
  %this1 = load %class.btConvexHullComputer*, %class.btConvexHullComputer** %this.addr, align 4
  %faces = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 2
  %call = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %faces) #7
  %edges = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEED2Ev(%class.btAlignedObjectArray.8* %edges) #7
  %vertices = getelementptr inbounds %class.btConvexHullComputer, %class.btConvexHullComputer* %this1, i32 0, i32 0
  %call3 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %vertices) #7
  ret %class.btConvexHullComputer* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK23btPolyhedralConvexShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btPolyhedralConvexShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec0) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  %vec0.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %i = alloca i32, align 4
  %maxDot = alloca float, align 4
  %vec = alloca %class.btVector3, align 4
  %lenSqr = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %rlen = alloca float, align 4
  %vtx = alloca %class.btVector3, align 4
  %newDot = alloca float, align 4
  %k = alloca i32, align 4
  %temp = alloca [128 x %class.btVector3], align 16
  %inner_count = alloca i32, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4
  store %class.btVector3* %vec0, %class.btVector3** %vec0.addr, align 4
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  store float 0xC3ABC16D60000000, float* %maxDot, align 4
  %0 = load %class.btVector3*, %class.btVector3** %vec0.addr, align 4
  %1 = bitcast %class.btVector3* %vec to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %call4 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vec)
  store float %call4, float* %lenSqr, align 4
  %3 = load float, float* %lenSqr, align 4
  %cmp = fcmp olt float %3, 0x3F1A36E2E0000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  br label %if.end

if.else:                                          ; preds = %entry
  %4 = load float, float* %lenSqr, align 4
  %call8 = call float @_Z6btSqrtf(float %4)
  %div = fdiv float 1.000000e+00, %call8
  store float %div, float* %rlen, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %rlen)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %call10 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vtx)
  store i32 0, i32* %k, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc32, %if.end
  %5 = load i32, i32* %k, align 4
  %6 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (%class.btPolyhedralConvexShape*)***
  %vtable = load i32 (%class.btPolyhedralConvexShape*)**, i32 (%class.btPolyhedralConvexShape*)*** %6, align 4
  %vfn = getelementptr inbounds i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vtable, i64 24
  %7 = load i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vfn, align 4
  %call11 = call i32 %7(%class.btPolyhedralConvexShape* %this1)
  %cmp12 = icmp slt i32 %5, %call11
  br i1 %cmp12, label %for.body, label %for.end33

for.body:                                         ; preds = %for.cond
  %array.begin = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 128
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %for.body
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %for.body ], [ %arrayctor.next, %arrayctor.loop ]
  %call13 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %8 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (%class.btPolyhedralConvexShape*)***
  %vtable14 = load i32 (%class.btPolyhedralConvexShape*)**, i32 (%class.btPolyhedralConvexShape*)*** %8, align 4
  %vfn15 = getelementptr inbounds i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vtable14, i64 24
  %9 = load i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vfn15, align 4
  %call16 = call i32 %9(%class.btPolyhedralConvexShape* %this1)
  %10 = load i32, i32* %k, align 4
  %sub = sub nsw i32 %call16, %10
  %cmp17 = icmp slt i32 %sub, 128
  br i1 %cmp17, label %cond.true, label %cond.false

cond.true:                                        ; preds = %arrayctor.cont
  %11 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (%class.btPolyhedralConvexShape*)***
  %vtable18 = load i32 (%class.btPolyhedralConvexShape*)**, i32 (%class.btPolyhedralConvexShape*)*** %11, align 4
  %vfn19 = getelementptr inbounds i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vtable18, i64 24
  %12 = load i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vfn19, align 4
  %call20 = call i32 %12(%class.btPolyhedralConvexShape* %this1)
  %13 = load i32, i32* %k, align 4
  %sub21 = sub nsw i32 %call20, %13
  br label %cond.end

cond.false:                                       ; preds = %arrayctor.cont
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub21, %cond.true ], [ 128, %cond.false ]
  store i32 %cond, i32* %inner_count, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc, %cond.end
  %14 = load i32, i32* %i, align 4
  %15 = load i32, i32* %inner_count, align 4
  %cmp23 = icmp slt i32 %14, %15
  br i1 %cmp23, label %for.body24, label %for.end

for.body24:                                       ; preds = %for.cond22
  %16 = load i32, i32* %i, align 4
  %17 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 %17
  %18 = bitcast %class.btPolyhedralConvexShape* %this1 to void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)***
  %vtable25 = load void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)**, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*** %18, align 4
  %vfn26 = getelementptr inbounds void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)** %vtable25, i64 27
  %19 = load void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)** %vfn26, align 4
  call void %19(%class.btPolyhedralConvexShape* %this1, i32 %16, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body24
  %20 = load i32, i32* %i, align 4
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond22

for.end:                                          ; preds = %for.cond22
  %arraydecay = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 0
  %21 = load i32, i32* %inner_count, align 4
  %call27 = call i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %vec, %class.btVector3* %arraydecay, i32 %21, float* nonnull align 4 dereferenceable(4) %newDot)
  store i32 %call27, i32* %i, align 4
  %22 = load float, float* %newDot, align 4
  %23 = load float, float* %maxDot, align 4
  %cmp28 = fcmp ogt float %22, %23
  br i1 %cmp28, label %if.then29, label %if.end31

if.then29:                                        ; preds = %for.end
  %24 = load float, float* %newDot, align 4
  store float %24, float* %maxDot, align 4
  %25 = load i32, i32* %i, align 4
  %arrayidx30 = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 %25
  %26 = bitcast %class.btVector3* %agg.result to i8*
  %27 = bitcast %class.btVector3* %arrayidx30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 16 %27, i32 16, i1 false)
  br label %if.end31

if.end31:                                         ; preds = %if.then29, %for.end
  br label %for.inc32

for.inc32:                                        ; preds = %if.end31
  %28 = load i32, i32* %k, align 4
  %add = add nsw i32 %28, 128
  store i32 %add, i32* %k, align 4
  br label %for.cond

for.end33:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %this, %class.btVector3* %array, i32 %array_count, float* nonnull align 4 dereferenceable(4) %dotOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %array.addr = alloca %class.btVector3*, align 4
  %array_count.addr = alloca i32, align 4
  %dotOut.addr = alloca float*, align 4
  %maxDot1 = alloca float, align 4
  %i = alloca i32, align 4
  %ptIndex = alloca i32, align 4
  %dot = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %array, %class.btVector3** %array.addr, align 4
  store i32 %array_count, i32* %array_count.addr, align 4
  store float* %dotOut, float** %dotOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store float 0xC7EFFFFFE0000000, float* %maxDot1, align 4
  store i32 0, i32* %i, align 4
  store i32 -1, i32* %ptIndex, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %array_count.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btVector3*, %class.btVector3** %array.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 %3
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  store float %call, float* %dot, align 4
  %4 = load float, float* %dot, align 4
  %5 = load float, float* %maxDot1, align 4
  %cmp2 = fcmp ogt float %4, %5
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load float, float* %dot, align 4
  store float %6, float* %maxDot1, align 4
  %7 = load i32, i32* %i, align 4
  store i32 %7, i32* %ptIndex, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load float, float* %maxDot1, align 4
  %10 = load float*, float** %dotOut.addr, align 4
  store float %9, float* %10, align 4
  %11 = load i32, i32* %ptIndex, align 4
  ret i32 %11
}

; Function Attrs: noinline optnone
define hidden void @_ZNK23btPolyhedralConvexShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btPolyhedralConvexShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %vtx = alloca %class.btVector3, align 4
  %newDot = alloca float, align 4
  %j = alloca i32, align 4
  %vec = alloca %class.btVector3*, align 4
  %k = alloca i32, align 4
  %temp = alloca [128 x %class.btVector3], align 16
  %inner_count = alloca i32, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4
  store i32 %numVectors, i32* %numVectors.addr, align 4
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vtx)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numVectors.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 %3
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 3
  store float 0xC3ABC16D60000000, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc42, %for.end
  %5 = load i32, i32* %j, align 4
  %6 = load i32, i32* %numVectors.addr, align 4
  %cmp5 = icmp slt i32 %5, %6
  br i1 %cmp5, label %for.body6, label %for.end44

for.body6:                                        ; preds = %for.cond4
  %7 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4
  %8 = load i32, i32* %j, align 4
  %arrayidx7 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  store %class.btVector3* %arrayidx7, %class.btVector3** %vec, align 4
  store i32 0, i32* %k, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc40, %for.body6
  %9 = load i32, i32* %k, align 4
  %10 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (%class.btPolyhedralConvexShape*)***
  %vtable = load i32 (%class.btPolyhedralConvexShape*)**, i32 (%class.btPolyhedralConvexShape*)*** %10, align 4
  %vfn = getelementptr inbounds i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vtable, i64 24
  %11 = load i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vfn, align 4
  %call9 = call i32 %11(%class.btPolyhedralConvexShape* %this1)
  %cmp10 = icmp slt i32 %9, %call9
  br i1 %cmp10, label %for.body11, label %for.end41

for.body11:                                       ; preds = %for.cond8
  %array.begin = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 128
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %for.body11
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %for.body11 ], [ %arrayctor.next, %arrayctor.loop ]
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %12 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (%class.btPolyhedralConvexShape*)***
  %vtable13 = load i32 (%class.btPolyhedralConvexShape*)**, i32 (%class.btPolyhedralConvexShape*)*** %12, align 4
  %vfn14 = getelementptr inbounds i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vtable13, i64 24
  %13 = load i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vfn14, align 4
  %call15 = call i32 %13(%class.btPolyhedralConvexShape* %this1)
  %14 = load i32, i32* %k, align 4
  %sub = sub nsw i32 %call15, %14
  %cmp16 = icmp slt i32 %sub, 128
  br i1 %cmp16, label %cond.true, label %cond.false

cond.true:                                        ; preds = %arrayctor.cont
  %15 = bitcast %class.btPolyhedralConvexShape* %this1 to i32 (%class.btPolyhedralConvexShape*)***
  %vtable17 = load i32 (%class.btPolyhedralConvexShape*)**, i32 (%class.btPolyhedralConvexShape*)*** %15, align 4
  %vfn18 = getelementptr inbounds i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vtable17, i64 24
  %16 = load i32 (%class.btPolyhedralConvexShape*)*, i32 (%class.btPolyhedralConvexShape*)** %vfn18, align 4
  %call19 = call i32 %16(%class.btPolyhedralConvexShape* %this1)
  %17 = load i32, i32* %k, align 4
  %sub20 = sub nsw i32 %call19, %17
  br label %cond.end

cond.false:                                       ; preds = %arrayctor.cont
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub20, %cond.true ], [ 128, %cond.false ]
  store i32 %cond, i32* %inner_count, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc27, %cond.end
  %18 = load i32, i32* %i, align 4
  %19 = load i32, i32* %inner_count, align 4
  %cmp22 = icmp slt i32 %18, %19
  br i1 %cmp22, label %for.body23, label %for.end29

for.body23:                                       ; preds = %for.cond21
  %20 = load i32, i32* %i, align 4
  %21 = load i32, i32* %i, align 4
  %arrayidx24 = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 %21
  %22 = bitcast %class.btPolyhedralConvexShape* %this1 to void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)***
  %vtable25 = load void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)**, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*** %22, align 4
  %vfn26 = getelementptr inbounds void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)** %vtable25, i64 27
  %23 = load void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)*, void (%class.btPolyhedralConvexShape*, i32, %class.btVector3*)** %vfn26, align 4
  call void %23(%class.btPolyhedralConvexShape* %this1, i32 %20, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx24)
  br label %for.inc27

for.inc27:                                        ; preds = %for.body23
  %24 = load i32, i32* %i, align 4
  %inc28 = add nsw i32 %24, 1
  store i32 %inc28, i32* %i, align 4
  br label %for.cond21

for.end29:                                        ; preds = %for.cond21
  %25 = load %class.btVector3*, %class.btVector3** %vec, align 4
  %arraydecay = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 0
  %26 = load i32, i32* %inner_count, align 4
  %call30 = call i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %25, %class.btVector3* %arraydecay, i32 %26, float* nonnull align 4 dereferenceable(4) %newDot)
  store i32 %call30, i32* %i, align 4
  %27 = load float, float* %newDot, align 4
  %28 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4
  %29 = load i32, i32* %j, align 4
  %arrayidx31 = getelementptr inbounds %class.btVector3, %class.btVector3* %28, i32 %29
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx31)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 3
  %30 = load float, float* %arrayidx33, align 4
  %cmp34 = fcmp ogt float %27, %30
  br i1 %cmp34, label %if.then, label %if.end

if.then:                                          ; preds = %for.end29
  %31 = load i32, i32* %i, align 4
  %arrayidx35 = getelementptr inbounds [128 x %class.btVector3], [128 x %class.btVector3]* %temp, i32 0, i32 %31
  %32 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4
  %33 = load i32, i32* %j, align 4
  %arrayidx36 = getelementptr inbounds %class.btVector3, %class.btVector3* %32, i32 %33
  %34 = bitcast %class.btVector3* %arrayidx36 to i8*
  %35 = bitcast %class.btVector3* %arrayidx35 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 16 %35, i32 16, i1 false)
  %36 = load float, float* %newDot, align 4
  %37 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4
  %38 = load i32, i32* %j, align 4
  %arrayidx37 = getelementptr inbounds %class.btVector3, %class.btVector3* %37, i32 %38
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx37)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 3
  store float %36, float* %arrayidx39, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.end29
  br label %for.inc40

for.inc40:                                        ; preds = %if.end
  %39 = load i32, i32* %k, align 4
  %add = add nsw i32 %39, 128
  store i32 %add, i32* %k, align 4
  br label %for.cond8

for.end41:                                        ; preds = %for.cond8
  br label %for.inc42

for.inc42:                                        ; preds = %for.end41
  %40 = load i32, i32* %j, align 4
  %inc43 = add nsw i32 %40, 1
  store i32 %inc43, i32* %j, align 4
  br label %for.cond4

for.end44:                                        ; preds = %for.cond4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3(%class.btPolyhedralConvexShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %margin = alloca float, align 4
  %ident = alloca %class.btTransform, align 4
  %aabbMin = alloca %class.btVector3, align 4
  %aabbMax = alloca %class.btVector3, align 4
  %halfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  %lx = alloca float, align 4
  %ly = alloca float, align 4
  %lz = alloca float, align 4
  %x2 = alloca float, align 4
  %y2 = alloca float, align 4
  %z2 = alloca float, align 4
  %scaledmass = alloca float, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  store %class.btPolyhedralConvexShape* %this, %class.btPolyhedralConvexShape** %this.addr, align 4
  store float %mass, float* %mass.addr, align 4
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4
  %this1 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %this.addr, align 4
  %0 = bitcast %class.btPolyhedralConvexShape* %this1 to %class.btConvexInternalShape*
  %1 = bitcast %class.btConvexInternalShape* %0 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %1, align 4
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %2 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call = call float %2(%class.btConvexInternalShape* %0)
  store float %call, float* %margin, align 4
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %ident)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %ident)
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMin)
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %aabbMax)
  %3 = bitcast %class.btPolyhedralConvexShape* %this1 to %class.btConvexInternalShape*
  %4 = bitcast %class.btConvexInternalShape* %3 to void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable5 = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %4, align 4
  %vfn6 = getelementptr inbounds void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable5, i64 2
  %5 = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn6, align 4
  call void %5(%class.btConvexInternalShape* %3, %class.btTransform* nonnull align 4 dereferenceable(64) %ident, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin)
  store float 5.000000e-01, float* %ref.tmp7, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %halfExtents)
  %6 = load float, float* %call8, align 4
  %7 = load float, float* %margin, align 4
  %add = fadd float %6, %7
  %mul = fmul float 2.000000e+00, %add
  store float %mul, float* %lx, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %halfExtents)
  %8 = load float, float* %call9, align 4
  %9 = load float, float* %margin, align 4
  %add10 = fadd float %8, %9
  %mul11 = fmul float 2.000000e+00, %add10
  store float %mul11, float* %ly, align 4
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %halfExtents)
  %10 = load float, float* %call12, align 4
  %11 = load float, float* %margin, align 4
  %add13 = fadd float %10, %11
  %mul14 = fmul float 2.000000e+00, %add13
  store float %mul14, float* %lz, align 4
  %12 = load float, float* %lx, align 4
  %13 = load float, float* %lx, align 4
  %mul15 = fmul float %12, %13
  store float %mul15, float* %x2, align 4
  %14 = load float, float* %ly, align 4
  %15 = load float, float* %ly, align 4
  %mul16 = fmul float %14, %15
  store float %mul16, float* %y2, align 4
  %16 = load float, float* %lz, align 4
  %17 = load float, float* %lz, align 4
  %mul17 = fmul float %16, %17
  store float %mul17, float* %z2, align 4
  %18 = load float, float* %mass.addr, align 4
  %mul18 = fmul float %18, 0x3FB5555540000000
  store float %mul18, float* %scaledmass, align 4
  %19 = load float, float* %y2, align 4
  %20 = load float, float* %z2, align 4
  %add22 = fadd float %19, %20
  store float %add22, float* %ref.tmp21, align 4
  %21 = load float, float* %x2, align 4
  %22 = load float, float* %z2, align 4
  %add24 = fadd float %21, %22
  store float %add24, float* %ref.tmp23, align 4
  %23 = load float, float* %x2, align 4
  %24 = load float, float* %y2, align 4
  %add26 = fadd float %23, %24
  store float %add26, float* %ref.tmp25, align 4
  %call27 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp19, float* nonnull align 4 dereferenceable(4) %scaledmass, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp20)
  %25 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %26 = bitcast %class.btVector3* %25 to i8*
  %27 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN34btPolyhedralConvexAabbCachingShape15setLocalScalingERK9btVector3(%class.btPolyhedralConvexAabbCachingShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %0 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to %class.btConvexInternalShape*
  %1 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  call void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  call void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape* %this1)
  ret void
}

declare void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv(%class.btPolyhedralConvexAabbCachingShape* %this) #2 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp22 = alloca float, align 4
  %_supporting = alloca [6 x %class.btVector3], align 16
  %ref.tmp24 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp35 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp48 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp50 = alloca float, align 4
  %i = alloca i32, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %m_isLocalAabbValid = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 3
  store i8 1, i8* %m_isLocalAabbValid, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions) #7
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([6 x %class.btVector3], [6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, i32 0, i32 0), float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([6 x %class.btVector3], [6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, i32 0, i32 1), float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 0.000000e+00, float* %ref.tmp9, align 4
  store float 1.000000e+00, float* %ref.tmp10, align 4
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([6 x %class.btVector3], [6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, i32 0, i32 2), float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  store float -1.000000e+00, float* %ref.tmp12, align 4
  store float 0.000000e+00, float* %ref.tmp13, align 4
  store float 0.000000e+00, float* %ref.tmp14, align 4
  %call15 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([6 x %class.btVector3], [6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, i32 0, i32 3), float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14)
  store float 0.000000e+00, float* %ref.tmp16, align 4
  store float -1.000000e+00, float* %ref.tmp17, align 4
  store float 0.000000e+00, float* %ref.tmp18, align 4
  %call19 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([6 x %class.btVector3], [6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, i32 0, i32 4), float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18)
  store float 0.000000e+00, float* %ref.tmp20, align 4
  store float 0.000000e+00, float* %ref.tmp21, align 4
  store float -1.000000e+00, float* %ref.tmp22, align 4
  %call23 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([6 x %class.btVector3], [6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, i32 0, i32 5), float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp22)
  call void @__cxa_guard_release(i32* @_ZGVZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions) #7
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  %arrayinit.begin = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 0
  store float 0.000000e+00, float* %ref.tmp24, align 4
  store float 0.000000e+00, float* %ref.tmp25, align 4
  store float 0.000000e+00, float* %ref.tmp26, align 4
  %call27 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.begin, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp26)
  %arrayinit.element = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.begin, i32 1
  store float 0.000000e+00, float* %ref.tmp28, align 4
  store float 0.000000e+00, float* %ref.tmp29, align 4
  store float 0.000000e+00, float* %ref.tmp30, align 4
  %call31 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp30)
  %arrayinit.element32 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element, i32 1
  store float 0.000000e+00, float* %ref.tmp33, align 4
  store float 0.000000e+00, float* %ref.tmp34, align 4
  store float 0.000000e+00, float* %ref.tmp35, align 4
  %call36 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element32, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp35)
  %arrayinit.element37 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element32, i32 1
  store float 0.000000e+00, float* %ref.tmp38, align 4
  store float 0.000000e+00, float* %ref.tmp39, align 4
  store float 0.000000e+00, float* %ref.tmp40, align 4
  %call41 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element37, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  %arrayinit.element42 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element37, i32 1
  store float 0.000000e+00, float* %ref.tmp43, align 4
  store float 0.000000e+00, float* %ref.tmp44, align 4
  store float 0.000000e+00, float* %ref.tmp45, align 4
  %call46 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element42, float* nonnull align 4 dereferenceable(4) %ref.tmp43, float* nonnull align 4 dereferenceable(4) %ref.tmp44, float* nonnull align 4 dereferenceable(4) %ref.tmp45)
  %arrayinit.element47 = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.element42, i32 1
  store float 0.000000e+00, float* %ref.tmp48, align 4
  store float 0.000000e+00, float* %ref.tmp49, align 4
  store float 0.000000e+00, float* %ref.tmp50, align 4
  %call51 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %arrayinit.element47, float* nonnull align 4 dereferenceable(4) %ref.tmp48, float* nonnull align 4 dereferenceable(4) %ref.tmp49, float* nonnull align 4 dereferenceable(4) %ref.tmp50)
  %3 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to %class.btPolyhedralConvexShape*
  %arraydecay = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 0
  %4 = bitcast %class.btPolyhedralConvexShape* %3 to void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)***
  %vtable = load void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)**, void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vtable, i64 19
  %5 = load void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)*, void (%class.btPolyhedralConvexShape*, %class.btVector3*, %class.btVector3*, i32)** %vfn, align 4
  call void %5(%class.btPolyhedralConvexShape* %3, %class.btVector3* getelementptr inbounds ([6 x %class.btVector3], [6 x %class.btVector3]* @_ZZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEvE11_directions, i32 0, i32 0), %class.btVector3* %arraydecay, i32 6)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %init.end
  %6 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %6, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 %7
  %call52 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx)
  %8 = load i32, i32* %i, align 4
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 %8
  %9 = load float, float* %arrayidx53, align 4
  %10 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to %class.btConvexInternalShape*
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %10, i32 0, i32 3
  %11 = load float, float* %m_collisionMargin, align 4
  %add = fadd float %9, %11
  %m_localAabbMax = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 2
  %call54 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMax)
  %12 = load i32, i32* %i, align 4
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 %12
  store float %add, float* %arrayidx55, align 4
  %13 = load i32, i32* %i, align 4
  %add56 = add nsw i32 %13, 3
  %arrayidx57 = getelementptr inbounds [6 x %class.btVector3], [6 x %class.btVector3]* %_supporting, i32 0, i32 %add56
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx57)
  %14 = load i32, i32* %i, align 4
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 %14
  %15 = load float, float* %arrayidx59, align 4
  %16 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to %class.btConvexInternalShape*
  %m_collisionMargin60 = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %16, i32 0, i32 3
  %17 = load float, float* %m_collisionMargin60, align 4
  %sub = fsub float %15, %17
  %m_localAabbMin = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 1
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_localAabbMin)
  %18 = load i32, i32* %i, align 4
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 %18
  store float %sub, float* %arrayidx62, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %19 = load i32, i32* %i, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeC2Ev(%class.btPolyhedralConvexAabbCachingShape* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %0 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to %class.btPolyhedralConvexShape*
  %call = call %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeC2Ev(%class.btPolyhedralConvexShape* %0)
  %1 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [33 x i8*] }, { [33 x i8*] }* @_ZTV34btPolyhedralConvexAabbCachingShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_localAabbMin = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 1
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_localAabbMin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %m_localAabbMax = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 2
  store float -1.000000e+00, float* %ref.tmp5, align 4
  store float -1.000000e+00, float* %ref.tmp6, align 4
  store float -1.000000e+00, float* %ref.tmp7, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_localAabbMax, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %m_isLocalAabbValid = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 3
  store i8 0, i8* %m_isLocalAabbValid, align 4
  ret %class.btPolyhedralConvexAabbCachingShape* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_(%class.btPolyhedralConvexAabbCachingShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %3 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to %class.btConvexInternalShape*
  %4 = bitcast %class.btConvexInternalShape* %3 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %4, align 4
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %5 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call = call float %5(%class.btConvexInternalShape* %3)
  call void @_ZNK34btPolyhedralConvexAabbCachingShape17getNonvirtualAabbERK11btTransformR9btVector3S4_f(%class.btPolyhedralConvexAabbCachingShape* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, float %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK34btPolyhedralConvexAabbCachingShape17getNonvirtualAabbERK11btTransformR9btVector3S4_f(%class.btPolyhedralConvexAabbCachingShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, float %margin) #2 comdat {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %m_localAabbMin = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 1
  %m_localAabbMax = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 2
  %0 = load float, float* %margin.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMax, float %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  ret void
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #7

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #7

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK21btConvexInternalShape7getAabbERK11btTransformR9btVector3S4_(%class.btConvexInternalShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %3 = bitcast %class.btConvexInternalShape* %this1 to void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 20
  %4 = load void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btConvexInternalShape* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #3

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #3

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape15getLocalScalingEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %this, float %margin) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  store float %0, float* %m_collisionMargin, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK21btConvexInternalShape9getMarginEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape28calculateSerializeBufferSizeEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 52
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexInternalShapeData*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btConvexInternalShapeData*
  store %struct.btConvexInternalShapeData* %1, %struct.btConvexInternalShapeData** %shapeData, align 4
  %2 = bitcast %class.btConvexInternalShape* %this1 to %class.btCollisionShape*
  %3 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_collisionShapeData = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %3, i32 0, i32 0
  %4 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %2, i8* %4, %class.btSerializer* %5)
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  %6 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_implicitShapeDimensions2 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %6, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_implicitShapeDimensions, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions2)
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  %7 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_localScaling3 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %7, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %8 = load float, float* %m_collisionMargin, align 4
  %9 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_collisionMargin4 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %9, i32 0, i32 3
  store float %8, float* %m_collisionMargin4, align 4
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #3

declare void @_ZNK21btConvexInternalShape24localGetSupportingVertexERK9btVector3(%class.btVector3* sret align 4, %class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvexInternalShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btConvexInternalShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPolyhedralConvexAabbCachingShape* @_ZN34btPolyhedralConvexAabbCachingShapeD2Ev(%class.btPolyhedralConvexAabbCachingShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %0 = bitcast %class.btPolyhedralConvexAabbCachingShape* %this1 to %class.btPolyhedralConvexShape*
  %call = call %class.btPolyhedralConvexShape* @_ZN23btPolyhedralConvexShapeD2Ev(%class.btPolyhedralConvexShape* %0) #7
  ret %class.btPolyhedralConvexAabbCachingShape* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN34btPolyhedralConvexAabbCachingShapeD0Ev(%class.btPolyhedralConvexAabbCachingShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: nounwind
declare %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned) unnamed_addr #8

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEEC2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EEC2Ev(%class.btAlignedAllocator.9* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EEC2Ev(%class.btAlignedAllocator.9* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  ret %class.btAlignedAllocator.9* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %"class.btConvexHullComputer::Edge"* null, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

declare float @_ZN20btConvexHullComputer7computeEPKvbiiff(%class.btConvexHullComputer*, i8*, i1 zeroext, i32, i32, float, float) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btConvexHullComputer::Edge"* @_ZNK20btConvexHullComputer4Edge19getNextEdgeOfVertexEv(%"class.btConvexHullComputer::Edge"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %"class.btConvexHullComputer::Edge"* %this, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %this1 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %this.addr, align 4
  %next = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 0, i32 0
  %0 = load i32, i32* %next, align 4
  %add.ptr = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %this1, i32 %0
  ret %"class.btConvexHullComputer::Edge"* %add.ptr
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z13btPlaneSpace1I9btVector3EvRKT_RS1_S4_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %n.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %q.addr = alloca %class.btVector3*, align 4
  %a = alloca float, align 4
  %k = alloca float, align 4
  %a42 = alloca float, align 4
  %k54 = alloca float, align 4
  store %class.btVector3* %n, %class.btVector3** %n.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store %class.btVector3* %q, %class.btVector3** %q.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 2
  %1 = load float, float* %arrayidx, align 4
  %call1 = call float @_Z6btFabsf(float %1)
  %cmp = fcmp ogt float %call1, 0x3FE6A09E60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %4 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %mul = fmul float %3, %5
  %6 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  %7 = load float, float* %arrayidx7, align 4
  %8 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %9 = load float, float* %arrayidx9, align 4
  %mul10 = fmul float %7, %9
  %add = fadd float %mul, %mul10
  store float %add, float* %a, align 4
  %10 = load float, float* %a, align 4
  %call11 = call float @_Z6btSqrtf(float %10)
  %div = fdiv float 1.000000e+00, %call11
  store float %div, float* %k, align 4
  %11 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float 0.000000e+00, float* %arrayidx13, align 4
  %12 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %12)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %13 = load float, float* %arrayidx15, align 4
  %fneg = fneg float %13
  %14 = load float, float* %k, align 4
  %mul16 = fmul float %fneg, %14
  %15 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %15)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  store float %mul16, float* %arrayidx18, align 4
  %16 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %16)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %17 = load float, float* %arrayidx20, align 4
  %18 = load float, float* %k, align 4
  %mul21 = fmul float %17, %18
  %19 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %19)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 2
  store float %mul21, float* %arrayidx23, align 4
  %20 = load float, float* %a, align 4
  %21 = load float, float* %k, align 4
  %mul24 = fmul float %20, %21
  %22 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call25 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %22)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 0
  store float %mul24, float* %arrayidx26, align 4
  %23 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call27 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %23)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 0
  %24 = load float, float* %arrayidx28, align 4
  %fneg29 = fneg float %24
  %25 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %25)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %26 = load float, float* %arrayidx31, align 4
  %mul32 = fmul float %fneg29, %26
  %27 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %27)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 1
  store float %mul32, float* %arrayidx34, align 4
  %28 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %28)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %29 = load float, float* %arrayidx36, align 4
  %30 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call37 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %30)
  %arrayidx38 = getelementptr inbounds float, float* %call37, i32 1
  %31 = load float, float* %arrayidx38, align 4
  %mul39 = fmul float %29, %31
  %32 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  store float %mul39, float* %arrayidx41, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %33 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %33)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 0
  %34 = load float, float* %arrayidx44, align 4
  %35 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %35)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 0
  %36 = load float, float* %arrayidx46, align 4
  %mul47 = fmul float %34, %36
  %37 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call48 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %37)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 1
  %38 = load float, float* %arrayidx49, align 4
  %39 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call50 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %39)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %40 = load float, float* %arrayidx51, align 4
  %mul52 = fmul float %38, %40
  %add53 = fadd float %mul47, %mul52
  store float %add53, float* %a42, align 4
  %41 = load float, float* %a42, align 4
  %call55 = call float @_Z6btSqrtf(float %41)
  %div56 = fdiv float 1.000000e+00, %call55
  store float %div56, float* %k54, align 4
  %42 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %43 = load float, float* %arrayidx58, align 4
  %fneg59 = fneg float %43
  %44 = load float, float* %k54, align 4
  %mul60 = fmul float %fneg59, %44
  %45 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %45)
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 0
  store float %mul60, float* %arrayidx62, align 4
  %46 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %46)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 0
  %47 = load float, float* %arrayidx64, align 4
  %48 = load float, float* %k54, align 4
  %mul65 = fmul float %47, %48
  %49 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %49)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 1
  store float %mul65, float* %arrayidx67, align 4
  %50 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %50)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 2
  store float 0.000000e+00, float* %arrayidx69, align 4
  %51 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %51)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 2
  %52 = load float, float* %arrayidx71, align 4
  %fneg72 = fneg float %52
  %53 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %53)
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 1
  %54 = load float, float* %arrayidx74, align 4
  %mul75 = fmul float %fneg72, %54
  %55 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call76 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 0
  store float %mul75, float* %arrayidx77, align 4
  %56 = load %class.btVector3*, %class.btVector3** %n.addr, align 4
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %56)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 2
  %57 = load float, float* %arrayidx79, align 4
  %58 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %call80 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %58)
  %arrayidx81 = getelementptr inbounds float, float* %call80, i32 0
  %59 = load float, float* %arrayidx81, align 4
  %mul82 = fmul float %57, %59
  %60 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call83 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %60)
  %arrayidx84 = getelementptr inbounds float, float* %call83, i32 1
  store float %mul82, float* %arrayidx84, align 4
  %61 = load float, float* %a42, align 4
  %62 = load float, float* %k54, align 4
  %mul85 = fmul float %61, %62
  %63 = load %class.btVector3*, %class.btVector3** %q.addr, align 4
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %63)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 2
  store float %mul85, float* %arrayidx87, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E4swapEii(%class.btAlignedObjectArray.12* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.GrahamVector3, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %0, i32 %1
  %2 = bitcast %struct.GrahamVector3* %temp to i8*
  %3 = bitcast %struct.GrahamVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 24, i1 false)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %4 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data2, align 4
  %5 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %4, i32 %5
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %6 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %6, i32 %7
  %8 = bitcast %struct.GrahamVector3* %arrayidx5 to i8*
  %9 = bitcast %struct.GrahamVector3* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 24, i1 false)
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %10 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data6, align 4
  %11 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %10, i32 %11
  %12 = bitcast %struct.GrahamVector3* %arrayidx7 to i8*
  %13 = bitcast %struct.GrahamVector3* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 24, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z11btAtan2Fastff(float %y, float %x) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  %x.addr = alloca float, align 4
  %coeff_1 = alloca float, align 4
  %coeff_2 = alloca float, align 4
  %abs_y = alloca float, align 4
  %angle = alloca float, align 4
  %r = alloca float, align 4
  %r3 = alloca float, align 4
  store float %y, float* %y.addr, align 4
  store float %x, float* %x.addr, align 4
  store float 0x3FE921FB60000000, float* %coeff_1, align 4
  %0 = load float, float* %coeff_1, align 4
  %mul = fmul float 3.000000e+00, %0
  store float %mul, float* %coeff_2, align 4
  %1 = load float, float* %y.addr, align 4
  %call = call float @_Z6btFabsf(float %1)
  store float %call, float* %abs_y, align 4
  %2 = load float, float* %x.addr, align 4
  %cmp = fcmp oge float %2, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load float, float* %x.addr, align 4
  %4 = load float, float* %abs_y, align 4
  %sub = fsub float %3, %4
  %5 = load float, float* %x.addr, align 4
  %6 = load float, float* %abs_y, align 4
  %add = fadd float %5, %6
  %div = fdiv float %sub, %add
  store float %div, float* %r, align 4
  %7 = load float, float* %coeff_1, align 4
  %8 = load float, float* %coeff_1, align 4
  %9 = load float, float* %r, align 4
  %mul1 = fmul float %8, %9
  %sub2 = fsub float %7, %mul1
  store float %sub2, float* %angle, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %10 = load float, float* %x.addr, align 4
  %11 = load float, float* %abs_y, align 4
  %add4 = fadd float %10, %11
  %12 = load float, float* %abs_y, align 4
  %13 = load float, float* %x.addr, align 4
  %sub5 = fsub float %12, %13
  %div6 = fdiv float %add4, %sub5
  store float %div6, float* %r3, align 4
  %14 = load float, float* %coeff_2, align 4
  %15 = load float, float* %coeff_1, align 4
  %16 = load float, float* %r3, align 4
  %mul7 = fmul float %15, %16
  %sub8 = fsub float %14, %mul7
  store float %sub8, float* %angle, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = load float, float* %y.addr, align 4
  %cmp9 = fcmp olt float %17, 0.000000e+00
  br i1 %cmp9, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %18 = load float, float* %angle, align 4
  %fneg = fneg float %18
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %19 = load float, float* %angle, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %fneg, %cond.true ], [ %19, %cond.false ]
  ret float %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btAngleCompareFunc* @_ZN18btAngleCompareFuncC2ERK9btVector3(%struct.btAngleCompareFunc* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %anchor) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btAngleCompareFunc*, align 4
  %anchor.addr = alloca %class.btVector3*, align 4
  store %struct.btAngleCompareFunc* %this, %struct.btAngleCompareFunc** %this.addr, align 4
  store %class.btVector3* %anchor, %class.btVector3** %anchor.addr, align 4
  %this1 = load %struct.btAngleCompareFunc*, %struct.btAngleCompareFunc** %this.addr, align 4
  %m_anchor = getelementptr inbounds %struct.btAngleCompareFunc, %struct.btAngleCompareFunc* %this1, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %anchor.addr, align 4
  %1 = bitcast %class.btVector3* %m_anchor to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret %struct.btAngleCompareFunc* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E17quickSortInternalI18btAngleCompareFuncEEvRKT_ii(%class.btAlignedObjectArray.12* %this, %struct.btAngleCompareFunc* nonnull align 4 dereferenceable(16) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %CompareFunc.addr = alloca %struct.btAngleCompareFunc*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %struct.GrahamVector3, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store %struct.btAngleCompareFunc* %CompareFunc, %struct.btAngleCompareFunc** %CompareFunc.addr, align 4
  store i32 %lo, i32* %lo.addr, align 4
  store i32 %hi, i32* %hi.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %lo.addr, align 4
  store i32 %0, i32* %i, align 4
  %1 = load i32, i32* %hi.addr, align 4
  store i32 %1, i32* %j, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4
  %3 = load i32, i32* %lo.addr, align 4
  %4 = load i32, i32* %hi.addr, align 4
  %add = add nsw i32 %3, %4
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %2, i32 %div
  %5 = bitcast %struct.GrahamVector3* %x to i8*
  %6 = bitcast %struct.GrahamVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 24, i1 false)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %7 = load %struct.btAngleCompareFunc*, %struct.btAngleCompareFunc** %CompareFunc.addr, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %8 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data2, align 4
  %9 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %8, i32 %9
  %call = call zeroext i1 @_ZNK18btAngleCompareFuncclERK13GrahamVector3S2_(%struct.btAngleCompareFunc* %7, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %arrayidx3, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %x)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond4

while.cond4:                                      ; preds = %while.body8, %while.end
  %11 = load %struct.btAngleCompareFunc*, %struct.btAngleCompareFunc** %CompareFunc.addr, align 4
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %12 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data5, align 4
  %13 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %12, i32 %13
  %call7 = call zeroext i1 @_ZNK18btAngleCompareFuncclERK13GrahamVector3S2_(%struct.btAngleCompareFunc* %11, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %x, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %arrayidx6)
  br i1 %call7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond4
  %14 = load i32, i32* %j, align 4
  %dec = add nsw i32 %14, -1
  store i32 %dec, i32* %j, align 4
  br label %while.cond4

while.end9:                                       ; preds = %while.cond4
  %15 = load i32, i32* %i, align 4
  %16 = load i32, i32* %j, align 4
  %cmp = icmp sle i32 %15, %16
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %17 = load i32, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E4swapEii(%class.btAlignedObjectArray.12* %this1, i32 %17, i32 %18)
  %19 = load i32, i32* %i, align 4
  %inc10 = add nsw i32 %19, 1
  store i32 %inc10, i32* %i, align 4
  %20 = load i32, i32* %j, align 4
  %dec11 = add nsw i32 %20, -1
  store i32 %dec11, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %21 = load i32, i32* %i, align 4
  %22 = load i32, i32* %j, align 4
  %cmp12 = icmp sle i32 %21, %22
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %23 = load i32, i32* %lo.addr, align 4
  %24 = load i32, i32* %j, align 4
  %cmp13 = icmp slt i32 %23, %24
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %25 = load %struct.btAngleCompareFunc*, %struct.btAngleCompareFunc** %CompareFunc.addr, align 4
  %26 = load i32, i32* %lo.addr, align 4
  %27 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E17quickSortInternalI18btAngleCompareFuncEEvRKT_ii(%class.btAlignedObjectArray.12* %this1, %struct.btAngleCompareFunc* nonnull align 4 dereferenceable(16) %25, i32 %26, i32 %27)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %28 = load i32, i32* %i, align 4
  %29 = load i32, i32* %hi.addr, align 4
  %cmp16 = icmp slt i32 %28, %29
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %30 = load %struct.btAngleCompareFunc*, %struct.btAngleCompareFunc** %CompareFunc.addr, align 4
  %31 = load i32, i32* %i, align 4
  %32 = load i32, i32* %hi.addr, align 4
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E17quickSortInternalI18btAngleCompareFuncEEvRKT_ii(%class.btAlignedObjectArray.12* %this1, %struct.btAngleCompareFunc* nonnull align 4 dereferenceable(16) %30, i32 %31, i32 %32)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z7btCrossRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E8pop_backEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %1 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %1, i32 %2
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #9

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK18btAngleCompareFuncclERK13GrahamVector3S2_(%struct.btAngleCompareFunc* %this, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %a, %struct.GrahamVector3* nonnull align 4 dereferenceable(24) %b) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %struct.btAngleCompareFunc*, align 4
  %a.addr = alloca %struct.GrahamVector3*, align 4
  %b.addr = alloca %struct.GrahamVector3*, align 4
  %al = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %bl = alloca float, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  store %struct.btAngleCompareFunc* %this, %struct.btAngleCompareFunc** %this.addr, align 4
  store %struct.GrahamVector3* %a, %struct.GrahamVector3** %a.addr, align 4
  store %struct.GrahamVector3* %b, %struct.GrahamVector3** %b.addr, align 4
  %this1 = load %struct.btAngleCompareFunc*, %struct.btAngleCompareFunc** %this.addr, align 4
  %0 = load %struct.GrahamVector3*, %struct.GrahamVector3** %a.addr, align 4
  %m_angle = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %0, i32 0, i32 1
  %1 = load float, float* %m_angle, align 4
  %2 = load %struct.GrahamVector3*, %struct.GrahamVector3** %b.addr, align 4
  %m_angle2 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %2, i32 0, i32 1
  %3 = load float, float* %m_angle2, align 4
  %cmp = fcmp une float %1, %3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load %struct.GrahamVector3*, %struct.GrahamVector3** %a.addr, align 4
  %m_angle3 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %4, i32 0, i32 1
  %5 = load float, float* %m_angle3, align 4
  %6 = load %struct.GrahamVector3*, %struct.GrahamVector3** %b.addr, align 4
  %m_angle4 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %6, i32 0, i32 1
  %7 = load float, float* %m_angle4, align 4
  %cmp5 = fcmp olt float %5, %7
  store i1 %cmp5, i1* %retval, align 1
  br label %return

if.else:                                          ; preds = %entry
  %8 = load %struct.GrahamVector3*, %struct.GrahamVector3** %a.addr, align 4
  %9 = bitcast %struct.GrahamVector3* %8 to %class.btVector3*
  %m_anchor = getelementptr inbounds %struct.btAngleCompareFunc, %struct.btAngleCompareFunc* %this1, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %m_anchor)
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  store float %call, float* %al, align 4
  %10 = load %struct.GrahamVector3*, %struct.GrahamVector3** %b.addr, align 4
  %11 = bitcast %struct.GrahamVector3* %10 to %class.btVector3*
  %m_anchor7 = getelementptr inbounds %struct.btAngleCompareFunc, %struct.btAngleCompareFunc* %this1, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %m_anchor7)
  %call8 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp6)
  store float %call8, float* %bl, align 4
  %12 = load float, float* %al, align 4
  %13 = load float, float* %bl, align 4
  %cmp9 = fcmp une float %12, %13
  br i1 %cmp9, label %if.then10, label %if.else12

if.then10:                                        ; preds = %if.else
  %14 = load float, float* %al, align 4
  %15 = load float, float* %bl, align 4
  %cmp11 = fcmp olt float %14, %15
  store i1 %cmp11, i1* %retval, align 1
  br label %return

if.else12:                                        ; preds = %if.else
  %16 = load %struct.GrahamVector3*, %struct.GrahamVector3** %a.addr, align 4
  %m_orgIndex = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %16, i32 0, i32 2
  %17 = load i32, i32* %m_orgIndex, align 4
  %18 = load %struct.GrahamVector3*, %struct.GrahamVector3** %b.addr, align 4
  %m_orgIndex13 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %18, i32 0, i32 2
  %19 = load i32, i32* %m_orgIndex13, align 4
  %cmp14 = icmp slt i32 %17, %19
  store i1 %cmp14, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.else12, %if.then10, %if.then
  %20 = load i1, i1* %retval, align 1
  ret i1 %20
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2ERKS0_(%class.btAlignedObjectArray.3* returned %this, %class.btAlignedObjectArray.3* nonnull align 4 dereferenceable(17) %otherArray) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store %class.btAlignedObjectArray.3* %otherArray, %class.btAlignedObjectArray.3** %otherArray.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.4* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.4* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this1)
  %0 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %otherArray.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %0)
  store i32 %call2, i32* %otherSize, align 4
  %1 = load i32, i32* %otherSize, align 4
  store i32 0, i32* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.3* %this1, i32 %1, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %2 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %otherArray.addr, align 4
  %3 = load i32, i32* %otherSize, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %4 = load i32*, i32** %m_data, align 4
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.3* %2, i32 0, i32 %3, i32* %4)
  ret %class.btAlignedObjectArray.3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.4* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  ret %class.btAlignedAllocator.4* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.3* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store i32* %fillData, i32** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %5 = load i32*, i32** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.3* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %14 = load i32*, i32** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %14, i32 %15
  %16 = bitcast i32* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to i32*
  %18 = load i32*, i32** %fillData.addr, align 4
  %19 = load i32, i32* %18, align 4
  store i32 %19, i32* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.3* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i32* %dest, i32** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = bitcast i32* %arrayidx to i8*
  %6 = bitcast i8* %5 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx2, align 4
  store i32 %9, i32* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.3* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.3* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.3* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i32*
  store i32* %2, i32** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %3 = load i32*, i32** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call3, i32* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i32*, i32** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* %4, i32** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.3* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.4* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.4* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.4* %this, i32 %n, i32** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32** %hint, i32*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.4* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEED2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE5clearEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %3 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.btConvexHullComputer::Edge", %"class.btConvexHullComputer::Edge"* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN20btConvexHullComputer4EdgeEE10deallocateEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data, align 4
  %tobool = icmp ne %"class.btConvexHullComputer::Edge"* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_(%class.btAlignedAllocator.9* %m_allocator, %"class.btConvexHullComputer::Edge"* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %"class.btConvexHullComputer::Edge"* null, %"class.btConvexHullComputer::Edge"** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIN20btConvexHullComputer4EdgeELj16EE10deallocateEPS1_(%class.btAlignedAllocator.9* %this, %"class.btConvexHullComputer::Edge"* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca %"class.btConvexHullComputer::Edge"*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store %"class.btConvexHullComputer::Edge"* %ptr, %"class.btConvexHullComputer::Edge"** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load %"class.btConvexHullComputer::Edge"*, %"class.btConvexHullComputer::Edge"** %ptr.addr, align 4
  %1 = bitcast %"class.btConvexHullComputer::Edge"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #9

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax, float %margin, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMinOut, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMaxOut) #2 comdat {
entry:
  %localAabbMin.addr = alloca %class.btVector3*, align 4
  %localAabbMax.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMinOut.addr = alloca %class.btVector3*, align 4
  %aabbMaxOut.addr = alloca %class.btVector3*, align 4
  %localHalfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %localCenter = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center = alloca %class.btVector3, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  store %class.btVector3* %localAabbMin, %class.btVector3** %localAabbMin.addr, align 4
  store %class.btVector3* %localAabbMax, %class.btVector3** %localAabbMax.addr, align 4
  store float %margin, float* %margin.addr, align 4
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4
  store %class.btVector3* %aabbMinOut, %class.btVector3** %aabbMinOut.addr, align 4
  store %class.btVector3* %aabbMaxOut, %class.btVector3** %aabbMaxOut.addr, align 4
  store float 5.000000e-01, float* %ref.tmp, align 4
  %0 = load %class.btVector3*, %class.btVector3** %localAabbMax.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %localAabbMin.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localHalfExtents, float* nonnull align 4 dereferenceable(4) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp2, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  store float 5.000000e-01, float* %ref.tmp4, align 4
  %2 = load %class.btVector3*, %class.btVector3** %localAabbMax.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %localAabbMin.addr, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %4 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %4)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call6)
  %5 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %center, %class.btTransform* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %localCenter)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent, %class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %call8, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %6 = load %class.btVector3*, %class.btVector3** %aabbMinOut.addr, align 4
  %7 = bitcast %class.btVector3* %6 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %9 = load %class.btVector3*, %class.btVector3** %aabbMaxOut.addr, align 4
  %10 = bitcast %class.btVector3* %9 to i8*
  %11 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4
  %call2 = call float @_Z6btFabsf(float %1)
  store float %call2, float* %ref.tmp, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %2 = load float, float* %call6, align 4
  %call7 = call float @_Z6btFabsf(float %2)
  store float %call7, float* %ref.tmp3, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx10)
  %3 = load float, float* %call11, align 4
  %call12 = call float @_Z6btFabsf(float %3)
  store float %call12, float* %ref.tmp8, align 4
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx15)
  %4 = load float, float* %call16, align 4
  %call17 = call float @_Z6btFabsf(float %4)
  store float %call17, float* %ref.tmp13, align 4
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx20)
  %5 = load float, float* %call21, align 4
  %call22 = call float @_Z6btFabsf(float %5)
  store float %call22, float* %ref.tmp18, align 4
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx25)
  %6 = load float, float* %call26, align 4
  %call27 = call float @_Z6btFabsf(float %6)
  store float %call27, float* %ref.tmp23, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %7 = load float, float* %call31, align 4
  %call32 = call float @_Z6btFabsf(float %7)
  store float %call32, float* %ref.tmp28, align 4
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx35)
  %8 = load float, float* %call36, align 4
  %call37 = call float @_Z6btFabsf(float %8)
  store float %call37, float* %ref.tmp33, align 4
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx40)
  %9 = load float, float* %call41, align 4
  %call42 = call float @_Z6btFabsf(float %9)
  store float %call42, float* %ref.tmp38, align 4
  %call43 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E8capacityEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E7reserveEi(%class.btAlignedObjectArray.12* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.GrahamVector3*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI13GrahamVector3E8allocateEi(%class.btAlignedObjectArray.12* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.GrahamVector3*
  store %struct.GrahamVector3* %2, %struct.GrahamVector3** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %3 = load %struct.GrahamVector3*, %struct.GrahamVector3** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI13GrahamVector3E4copyEiiPS0_(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call3, %struct.GrahamVector3* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.GrahamVector3*, %struct.GrahamVector3** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.GrahamVector3* %4, %struct.GrahamVector3** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI13GrahamVector3E9allocSizeEi(%class.btAlignedObjectArray.12* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI13GrahamVector3E8allocateEi(%class.btAlignedObjectArray.12* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.GrahamVector3* @_ZN18btAlignedAllocatorI13GrahamVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %m_allocator, i32 %1, %struct.GrahamVector3** null)
  %2 = bitcast %struct.GrahamVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI13GrahamVector3E4copyEiiPS0_(%class.btAlignedObjectArray.12* %this, i32 %start, i32 %end, %struct.GrahamVector3* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.GrahamVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.GrahamVector3* %dest, %struct.GrahamVector3** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.GrahamVector3*, %struct.GrahamVector3** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %3, i32 %4
  %5 = bitcast %struct.GrahamVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 24, i8* %5)
  %6 = bitcast i8* %call to %struct.GrahamVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %7 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %7, i32 %8
  %9 = bitcast %struct.GrahamVector3* %6 to i8*
  %10 = bitcast %struct.GrahamVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 24, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E7destroyEii(%class.btAlignedObjectArray.12* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %3 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.GrahamVector3, %struct.GrahamVector3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E10deallocateEv(%class.btAlignedObjectArray.12* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data, align 4
  %tobool = icmp ne %struct.GrahamVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load %struct.GrahamVector3*, %struct.GrahamVector3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI13GrahamVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %m_allocator, %struct.GrahamVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.GrahamVector3* null, %struct.GrahamVector3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.GrahamVector3* @_ZN18btAlignedAllocatorI13GrahamVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %this, i32 %n, %struct.GrahamVector3** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.GrahamVector3**, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.GrahamVector3** %hint, %struct.GrahamVector3*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 24, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.GrahamVector3*
  ret %struct.GrahamVector3* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI13GrahamVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %this, %struct.GrahamVector3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %ptr.addr = alloca %struct.GrahamVector3*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  store %struct.GrahamVector3* %ptr, %struct.GrahamVector3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load %struct.GrahamVector3*, %struct.GrahamVector3** %ptr.addr, align 4
  %1 = bitcast %struct.GrahamVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.btVector3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %2, %class.btVector3** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %class.btVector3*, %class.btVector3** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btVector3* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* %4, %class.btVector3** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btVector3* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %9 = bitcast %class.btVector3* %6 to i8*
  %10 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.btVector3** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI6btFaceLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btFace* null, %struct.btFace** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI6btFaceE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI6btFaceE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI6btFaceE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %struct.btFace*, %struct.btFace** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %3, i32 %4
  %call = call %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* %arrayidx) #7
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btFace*, %struct.btFace** %m_data, align 4
  %tobool = icmp ne %struct.btFace* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.btFace*, %struct.btFace** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI6btFaceLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.btFace* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btFace* null, %struct.btFace** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI6btFaceLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.btFace* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.btFace*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %struct.btFace* %ptr, %struct.btFace** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.btFace*, %struct.btFace** %ptr.addr, align 4
  %1 = bitcast %struct.btFace* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btFace*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI6btFaceE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btFace*
  store %struct.btFace* %2, %struct.btFace** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %3 = load %struct.btFace*, %struct.btFace** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI6btFaceE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %struct.btFace* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI6btFaceE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI6btFaceE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btFace*, %struct.btFace** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btFace* %4, %struct.btFace** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI6btFaceE8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI6btFaceE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btFace* @_ZN18btAlignedAllocatorI6btFaceLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %struct.btFace** null)
  %2 = bitcast %struct.btFace* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI6btFaceE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %struct.btFace* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btFace*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btFace* %dest, %struct.btFace** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btFace*, %struct.btFace** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %3, i32 %4
  %5 = bitcast %struct.btFace* %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btFace*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load %struct.btFace*, %struct.btFace** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btFace, %struct.btFace* %7, i32 %8
  %call = call %struct.btFace* @_ZN6btFaceC2ERKS_(%struct.btFace* %6, %struct.btFace* nonnull align 4 dereferenceable(36) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btFace* @_ZN18btAlignedAllocatorI6btFaceLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, %struct.btFace** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btFace**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btFace** %hint, %struct.btFace*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 36, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btFace*
  ret %struct.btFace* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.3* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE16findLinearSearchERKi(%class.btAlignedObjectArray.3* %this, i32* nonnull align 4 dereferenceable(4) %key) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %key.addr = alloca i32*, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32* %key, i32** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  store i32 %call, i32* %index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %1 = load i32*, i32** %m_data, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  %3 = load i32, i32* %arrayidx, align 4
  %4 = load i32*, i32** %key.addr, align 4
  %5 = load i32, i32* %4, align 4
  %cmp3 = icmp eq i32 %3, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  store i32 %6, i32* %index, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %8 = load i32, i32* %index, align 4
  ret i32 %8
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE13removeAtIndexEi(%class.btAlignedObjectArray.3* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %index.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %index.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIiE4swapEii(%class.btAlignedObjectArray.3* %this1, i32 %1, i32 %sub)
  call void @_ZN20btAlignedObjectArrayIiE8pop_backEv(%class.btAlignedObjectArray.3* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4swapEii(%class.btAlignedObjectArray.3* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  %2 = load i32, i32* %arrayidx, align 4
  store i32 %2, i32* %temp, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data2, align 4
  %4 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = load i32, i32* %arrayidx3, align 4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %6 = load i32*, i32** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds i32, i32* %6, i32 %7
  store i32 %5, i32* %arrayidx5, align 4
  %8 = load i32, i32* %temp, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %9 = load i32*, i32** %m_data6, align 4
  %10 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds i32, i32* %9, i32 %10
  store i32 %8, i32* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorI13GrahamVector3Lj16EEC2Ev(%class.btAlignedAllocator.13* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  ret %class.btAlignedAllocator.13* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E4initEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.GrahamVector3* null, %struct.GrahamVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13GrahamVector3E5clearEv(%class.btAlignedObjectArray.12* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI13GrahamVector3E4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI13GrahamVector3E4initEv(%class.btAlignedObjectArray.12* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI6btFaceE9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btPolyhedralConvexShape.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { cold noreturn nounwind }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { argmemonly nounwind willreturn writeonly }
attributes #7 = { nounwind }
attributes #8 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { nounwind readnone speculatable willreturn }
attributes #10 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1048575}
