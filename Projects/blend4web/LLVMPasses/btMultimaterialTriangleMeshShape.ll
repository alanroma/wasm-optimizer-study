; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btMultimaterialTriangleMeshShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btMultimaterialTriangleMeshShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btMaterial = type { float, float, [2 x i32] }
%class.btMultimaterialTriangleMeshShape = type { %class.btBvhTriangleMeshShape.base, [3 x i8], %class.btAlignedObjectArray.20 }
%class.btBvhTriangleMeshShape.base = type <{ %class.btTriangleMeshShape, %class.btOptimizedBvh*, %struct.btTriangleInfoMap*, i8, i8, [11 x i8] }>
%class.btTriangleMeshShape = type { %class.btConcaveShape, %class.btVector3, %class.btVector3, %class.btStridingMeshInterface* }
%class.btConcaveShape = type { %class.btCollisionShape, float }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btVector3 = type { [4 x float] }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btOptimizedBvh = type { %class.btQuantizedBvh }
%class.btQuantizedBvh = type { i32 (...)**, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32, i8, [3 x i8], %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0, i32, %class.btAlignedObjectArray.4, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btOptimizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btOptimizedBvhNode = type { %class.btVector3, %class.btVector3, i32, i32, i32, [20 x i8] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btQuantizedBvhNode*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btQuantizedBvhNode = type { [3 x i16], [3 x i16], i32 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btBvhSubtreeInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btBvhSubtreeInfo = type { [3 x i16], [3 x i16], i32, i32, [3 x i32] }
%struct.btTriangleInfoMap = type { i32 (...)**, %class.btHashMap, float, float, float, float, float, float }
%class.btHashMap = type { %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %struct.btTriangleInfo*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.btTriangleInfo = type { i32, float, float, float }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btHashInt*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.btHashInt = type { i32 }
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, %class.btMaterial**, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }
%class.btTriangleIndexVertexMaterialArray = type { %class.btTriangleIndexVertexArray, %class.btAlignedObjectArray.28 }
%class.btTriangleIndexVertexArray = type { %class.btStridingMeshInterface, %class.btAlignedObjectArray.24, [2 x i32], i32, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.24 = type <{ %class.btAlignedAllocator.25, [3 x i8], i32, i32, %struct.btIndexedMesh*, i8, [3 x i8] }>
%class.btAlignedAllocator.25 = type { i8 }
%struct.btIndexedMesh = type { i32, i8*, i32, i32, i8*, i32, i32, i32 }
%class.btAlignedObjectArray.28 = type <{ %class.btAlignedAllocator.29, [3 x i8], i32, i32, %struct.btMaterialProperties*, i8, [3 x i8] }>
%class.btAlignedAllocator.29 = type { i8 }
%struct.btMaterialProperties = type { i32, i8*, i32, i32, i32, i8*, i32, i32 }

$_ZN18btInfMaskConverterC2Ei = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btMultimaterialTriangleMeshShape.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btMaterial* @_ZN32btMultimaterialTriangleMeshShape21getMaterialPropertiesEii(%class.btMultimaterialTriangleMeshShape* %this, i32 %partID, i32 %triIndex) #2 {
entry:
  %this.addr = alloca %class.btMultimaterialTriangleMeshShape*, align 4
  %partID.addr = alloca i32, align 4
  %triIndex.addr = alloca i32, align 4
  %materialBase = alloca i8*, align 4
  %numMaterials = alloca i32, align 4
  %materialType = alloca i32, align 4
  %materialStride = alloca i32, align 4
  %triangleMaterialBase = alloca i8*, align 4
  %numTriangles = alloca i32, align 4
  %triangleMaterialStride = alloca i32, align 4
  %triangleType = alloca i32, align 4
  %matInd = alloca i32*, align 4
  %matVal = alloca %class.btMaterial*, align 4
  store %class.btMultimaterialTriangleMeshShape* %this, %class.btMultimaterialTriangleMeshShape** %this.addr, align 4
  store i32 %partID, i32* %partID.addr, align 4
  store i32 %triIndex, i32* %triIndex.addr, align 4
  %this1 = load %class.btMultimaterialTriangleMeshShape*, %class.btMultimaterialTriangleMeshShape** %this.addr, align 4
  store i8* null, i8** %materialBase, align 4
  store i8* null, i8** %triangleMaterialBase, align 4
  %0 = bitcast %class.btMultimaterialTriangleMeshShape* %this1 to %class.btTriangleMeshShape*
  %m_meshInterface = getelementptr inbounds %class.btTriangleMeshShape, %class.btTriangleMeshShape* %0, i32 0, i32 3
  %1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %2 = bitcast %class.btStridingMeshInterface* %1 to %class.btTriangleIndexVertexMaterialArray*
  %3 = load i32, i32* %partID.addr, align 4
  %4 = bitcast %class.btTriangleIndexVertexMaterialArray* %2 to void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable = load void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable, i64 16
  %5 = load void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn, align 4
  call void %5(%class.btTriangleIndexVertexMaterialArray* %2, i8** %materialBase, i32* nonnull align 4 dereferenceable(4) %numMaterials, i32* nonnull align 4 dereferenceable(4) %materialType, i32* nonnull align 4 dereferenceable(4) %materialStride, i8** %triangleMaterialBase, i32* nonnull align 4 dereferenceable(4) %numTriangles, i32* nonnull align 4 dereferenceable(4) %triangleMaterialStride, i32* nonnull align 4 dereferenceable(4) %triangleType, i32 %3)
  %6 = load i8*, i8** %triangleMaterialBase, align 4
  %7 = load i32, i32* %triIndex.addr, align 4
  %8 = load i32, i32* %triangleMaterialStride, align 4
  %mul = mul nsw i32 %7, %8
  %arrayidx = getelementptr inbounds i8, i8* %6, i32 %mul
  %9 = bitcast i8* %arrayidx to i32*
  store i32* %9, i32** %matInd, align 4
  %10 = load i8*, i8** %materialBase, align 4
  %11 = load i32*, i32** %matInd, align 4
  %12 = load i32, i32* %11, align 4
  %13 = load i32, i32* %materialStride, align 4
  %mul2 = mul nsw i32 %12, %13
  %arrayidx3 = getelementptr inbounds i8, i8* %10, i32 %mul2
  %14 = bitcast i8* %arrayidx3 to %class.btMaterial*
  store %class.btMaterial* %14, %class.btMaterial** %matVal, align 4
  %15 = load %class.btMaterial*, %class.btMaterial** %matVal, align 4
  ret %class.btMaterial* %15
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btMultimaterialTriangleMeshShape.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
