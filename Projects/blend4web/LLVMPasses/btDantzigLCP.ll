; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/MLCPSolvers/btDantzigLCP.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/MLCPSolvers/btDantzigLCP.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%struct.btLCP = type { i32, i32, i32, i32, i32, float**, float*, float*, float*, float*, float*, float*, float*, float*, float*, float*, i8*, i32*, i32*, i32* }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btDantzigScratchMemory = type { %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, float**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, i8*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_Z9btSetZeroIfEvPT_i = comdat any

$_Z10btLargeDotPKfS0_i = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_Z29btEstimateLDLTAddTLTmpbufSizei = comdat any

$_ZN20btAlignedObjectArrayIPfE6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZN20btAlignedObjectArrayIbE6resizeEiRKb = comdat any

$_ZN20btAlignedObjectArrayIbEixEi = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN20btAlignedObjectArrayIPfEixEi = comdat any

$_ZNK5btLCP6getNubEv = comdat any

$_Z6btFabsf = comdat any

$_ZNK5btLCP12AiC_times_qCEiPf = comdat any

$_ZNK5btLCP12AiN_times_qNEiPf = comdat any

$_ZN5btLCP15transfer_i_to_NEi = comdat any

$_ZNK5btLCP3AiiEi = comdat any

$_ZNK5btLCP4numNEv = comdat any

$_ZNK5btLCP6indexNEi = comdat any

$_ZNK5btLCP4numCEv = comdat any

$_ZNK5btLCP6indexCEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZNK20btAlignedObjectArrayIPfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIPfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIPfE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayIPfE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIPfE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPfLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorIPfLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZNK20btAlignedObjectArrayIbE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIbE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIbE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIbE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIbE4copyEiiPb = comdat any

$_ZN20btAlignedObjectArrayIbE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIbE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIbLj16EE8allocateEiPPKb = comdat any

$_ZN18btAlignedAllocatorIbLj16EE10deallocateEPb = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@s_error = hidden global i8 0, align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btDantzigLCP.cpp, i8* null }]

@_ZN5btLCPC1EiiiPfS0_S0_S0_S0_S0_S0_S0_S0_S0_S0_PbPiS2_S2_PS0_ = hidden unnamed_addr alias %struct.btLCP* (%struct.btLCP*, i32, i32, i32, float*, float*, float*, float*, float*, float*, float*, float*, float*, float*, float*, i8*, i32*, i32*, i32*, float**), %struct.btLCP* (%struct.btLCP*, i32, i32, i32, float*, float*, float*, float*, float*, float*, float*, float*, float*, float*, float*, i8*, i32*, i32*, i32*, float**)* @_ZN5btLCPC2EiiiPfS0_S0_S0_S0_S0_S0_S0_S0_S0_S0_PbPiS2_S2_PS0_

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden void @_Z12btFactorLDLTPfS_ii(float* %A, float* %d, i32 %n, i32 %nskip1) #2 {
entry:
  %A.addr = alloca float*, align 4
  %d.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %nskip1.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %sum = alloca float, align 4
  %ell = alloca float*, align 4
  %dee = alloca float*, align 4
  %dd = alloca float, align 4
  %p1 = alloca float, align 4
  %p2 = alloca float, align 4
  %q1 = alloca float, align 4
  %q2 = alloca float, align 4
  %Z11 = alloca float, align 4
  %m11 = alloca float, align 4
  %Z21 = alloca float, align 4
  %m21 = alloca float, align 4
  %Z22 = alloca float, align 4
  %m22 = alloca float, align 4
  store float* %A, float** %A.addr, align 4
  store float* %d, float** %d.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 %nskip1, i32* %nskip1.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %cmp = icmp slt i32 %0, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %sw.epilog

if.end:                                           ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc134, %if.end
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %n.addr, align 4
  %sub = sub nsw i32 %2, 2
  %cmp1 = icmp sle i32 %1, %sub
  br i1 %cmp1, label %for.body, label %for.end136

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %A.addr, align 4
  %4 = load float*, float** %A.addr, align 4
  %5 = load i32, i32* %i, align 4
  %6 = load i32, i32* %nskip1.addr, align 4
  %mul = mul nsw i32 %5, %6
  %add.ptr = getelementptr inbounds float, float* %4, i32 %mul
  %7 = load i32, i32* %i, align 4
  %8 = load i32, i32* %nskip1.addr, align 4
  call void @_ZL11btSolveL1_2PKfPfii(float* %3, float* %add.ptr, i32 %7, i32 %8)
  store float 0.000000e+00, float* %Z11, align 4
  store float 0.000000e+00, float* %Z21, align 4
  store float 0.000000e+00, float* %Z22, align 4
  %9 = load float*, float** %A.addr, align 4
  %10 = load i32, i32* %i, align 4
  %11 = load i32, i32* %nskip1.addr, align 4
  %mul2 = mul nsw i32 %10, %11
  %add.ptr3 = getelementptr inbounds float, float* %9, i32 %mul2
  store float* %add.ptr3, float** %ell, align 4
  %12 = load float*, float** %d.addr, align 4
  store float* %12, float** %dee, align 4
  %13 = load i32, i32* %i, align 4
  %sub4 = sub nsw i32 %13, 6
  store i32 %sub4, i32* %j, align 4
  br label %for.cond5

for.cond5:                                        ; preds = %for.inc, %for.body
  %14 = load i32, i32* %j, align 4
  %cmp6 = icmp sge i32 %14, 0
  br i1 %cmp6, label %for.body7, label %for.end

for.body7:                                        ; preds = %for.cond5
  %15 = load float*, float** %ell, align 4
  %arrayidx = getelementptr inbounds float, float* %15, i32 0
  %16 = load float, float* %arrayidx, align 4
  store float %16, float* %p1, align 4
  %17 = load float*, float** %ell, align 4
  %18 = load i32, i32* %nskip1.addr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %17, i32 %18
  %19 = load float, float* %arrayidx8, align 4
  store float %19, float* %p2, align 4
  %20 = load float*, float** %dee, align 4
  %arrayidx9 = getelementptr inbounds float, float* %20, i32 0
  %21 = load float, float* %arrayidx9, align 4
  store float %21, float* %dd, align 4
  %22 = load float, float* %p1, align 4
  %23 = load float, float* %dd, align 4
  %mul10 = fmul float %22, %23
  store float %mul10, float* %q1, align 4
  %24 = load float, float* %p2, align 4
  %25 = load float, float* %dd, align 4
  %mul11 = fmul float %24, %25
  store float %mul11, float* %q2, align 4
  %26 = load float, float* %q1, align 4
  %27 = load float*, float** %ell, align 4
  %arrayidx12 = getelementptr inbounds float, float* %27, i32 0
  store float %26, float* %arrayidx12, align 4
  %28 = load float, float* %q2, align 4
  %29 = load float*, float** %ell, align 4
  %30 = load i32, i32* %nskip1.addr, align 4
  %arrayidx13 = getelementptr inbounds float, float* %29, i32 %30
  store float %28, float* %arrayidx13, align 4
  %31 = load float, float* %p1, align 4
  %32 = load float, float* %q1, align 4
  %mul14 = fmul float %31, %32
  store float %mul14, float* %m11, align 4
  %33 = load float, float* %p2, align 4
  %34 = load float, float* %q1, align 4
  %mul15 = fmul float %33, %34
  store float %mul15, float* %m21, align 4
  %35 = load float, float* %p2, align 4
  %36 = load float, float* %q2, align 4
  %mul16 = fmul float %35, %36
  store float %mul16, float* %m22, align 4
  %37 = load float, float* %m11, align 4
  %38 = load float, float* %Z11, align 4
  %add = fadd float %38, %37
  store float %add, float* %Z11, align 4
  %39 = load float, float* %m21, align 4
  %40 = load float, float* %Z21, align 4
  %add17 = fadd float %40, %39
  store float %add17, float* %Z21, align 4
  %41 = load float, float* %m22, align 4
  %42 = load float, float* %Z22, align 4
  %add18 = fadd float %42, %41
  store float %add18, float* %Z22, align 4
  %43 = load float*, float** %ell, align 4
  %arrayidx19 = getelementptr inbounds float, float* %43, i32 1
  %44 = load float, float* %arrayidx19, align 4
  store float %44, float* %p1, align 4
  %45 = load float*, float** %ell, align 4
  %46 = load i32, i32* %nskip1.addr, align 4
  %add20 = add nsw i32 1, %46
  %arrayidx21 = getelementptr inbounds float, float* %45, i32 %add20
  %47 = load float, float* %arrayidx21, align 4
  store float %47, float* %p2, align 4
  %48 = load float*, float** %dee, align 4
  %arrayidx22 = getelementptr inbounds float, float* %48, i32 1
  %49 = load float, float* %arrayidx22, align 4
  store float %49, float* %dd, align 4
  %50 = load float, float* %p1, align 4
  %51 = load float, float* %dd, align 4
  %mul23 = fmul float %50, %51
  store float %mul23, float* %q1, align 4
  %52 = load float, float* %p2, align 4
  %53 = load float, float* %dd, align 4
  %mul24 = fmul float %52, %53
  store float %mul24, float* %q2, align 4
  %54 = load float, float* %q1, align 4
  %55 = load float*, float** %ell, align 4
  %arrayidx25 = getelementptr inbounds float, float* %55, i32 1
  store float %54, float* %arrayidx25, align 4
  %56 = load float, float* %q2, align 4
  %57 = load float*, float** %ell, align 4
  %58 = load i32, i32* %nskip1.addr, align 4
  %add26 = add nsw i32 1, %58
  %arrayidx27 = getelementptr inbounds float, float* %57, i32 %add26
  store float %56, float* %arrayidx27, align 4
  %59 = load float, float* %p1, align 4
  %60 = load float, float* %q1, align 4
  %mul28 = fmul float %59, %60
  store float %mul28, float* %m11, align 4
  %61 = load float, float* %p2, align 4
  %62 = load float, float* %q1, align 4
  %mul29 = fmul float %61, %62
  store float %mul29, float* %m21, align 4
  %63 = load float, float* %p2, align 4
  %64 = load float, float* %q2, align 4
  %mul30 = fmul float %63, %64
  store float %mul30, float* %m22, align 4
  %65 = load float, float* %m11, align 4
  %66 = load float, float* %Z11, align 4
  %add31 = fadd float %66, %65
  store float %add31, float* %Z11, align 4
  %67 = load float, float* %m21, align 4
  %68 = load float, float* %Z21, align 4
  %add32 = fadd float %68, %67
  store float %add32, float* %Z21, align 4
  %69 = load float, float* %m22, align 4
  %70 = load float, float* %Z22, align 4
  %add33 = fadd float %70, %69
  store float %add33, float* %Z22, align 4
  %71 = load float*, float** %ell, align 4
  %arrayidx34 = getelementptr inbounds float, float* %71, i32 2
  %72 = load float, float* %arrayidx34, align 4
  store float %72, float* %p1, align 4
  %73 = load float*, float** %ell, align 4
  %74 = load i32, i32* %nskip1.addr, align 4
  %add35 = add nsw i32 2, %74
  %arrayidx36 = getelementptr inbounds float, float* %73, i32 %add35
  %75 = load float, float* %arrayidx36, align 4
  store float %75, float* %p2, align 4
  %76 = load float*, float** %dee, align 4
  %arrayidx37 = getelementptr inbounds float, float* %76, i32 2
  %77 = load float, float* %arrayidx37, align 4
  store float %77, float* %dd, align 4
  %78 = load float, float* %p1, align 4
  %79 = load float, float* %dd, align 4
  %mul38 = fmul float %78, %79
  store float %mul38, float* %q1, align 4
  %80 = load float, float* %p2, align 4
  %81 = load float, float* %dd, align 4
  %mul39 = fmul float %80, %81
  store float %mul39, float* %q2, align 4
  %82 = load float, float* %q1, align 4
  %83 = load float*, float** %ell, align 4
  %arrayidx40 = getelementptr inbounds float, float* %83, i32 2
  store float %82, float* %arrayidx40, align 4
  %84 = load float, float* %q2, align 4
  %85 = load float*, float** %ell, align 4
  %86 = load i32, i32* %nskip1.addr, align 4
  %add41 = add nsw i32 2, %86
  %arrayidx42 = getelementptr inbounds float, float* %85, i32 %add41
  store float %84, float* %arrayidx42, align 4
  %87 = load float, float* %p1, align 4
  %88 = load float, float* %q1, align 4
  %mul43 = fmul float %87, %88
  store float %mul43, float* %m11, align 4
  %89 = load float, float* %p2, align 4
  %90 = load float, float* %q1, align 4
  %mul44 = fmul float %89, %90
  store float %mul44, float* %m21, align 4
  %91 = load float, float* %p2, align 4
  %92 = load float, float* %q2, align 4
  %mul45 = fmul float %91, %92
  store float %mul45, float* %m22, align 4
  %93 = load float, float* %m11, align 4
  %94 = load float, float* %Z11, align 4
  %add46 = fadd float %94, %93
  store float %add46, float* %Z11, align 4
  %95 = load float, float* %m21, align 4
  %96 = load float, float* %Z21, align 4
  %add47 = fadd float %96, %95
  store float %add47, float* %Z21, align 4
  %97 = load float, float* %m22, align 4
  %98 = load float, float* %Z22, align 4
  %add48 = fadd float %98, %97
  store float %add48, float* %Z22, align 4
  %99 = load float*, float** %ell, align 4
  %arrayidx49 = getelementptr inbounds float, float* %99, i32 3
  %100 = load float, float* %arrayidx49, align 4
  store float %100, float* %p1, align 4
  %101 = load float*, float** %ell, align 4
  %102 = load i32, i32* %nskip1.addr, align 4
  %add50 = add nsw i32 3, %102
  %arrayidx51 = getelementptr inbounds float, float* %101, i32 %add50
  %103 = load float, float* %arrayidx51, align 4
  store float %103, float* %p2, align 4
  %104 = load float*, float** %dee, align 4
  %arrayidx52 = getelementptr inbounds float, float* %104, i32 3
  %105 = load float, float* %arrayidx52, align 4
  store float %105, float* %dd, align 4
  %106 = load float, float* %p1, align 4
  %107 = load float, float* %dd, align 4
  %mul53 = fmul float %106, %107
  store float %mul53, float* %q1, align 4
  %108 = load float, float* %p2, align 4
  %109 = load float, float* %dd, align 4
  %mul54 = fmul float %108, %109
  store float %mul54, float* %q2, align 4
  %110 = load float, float* %q1, align 4
  %111 = load float*, float** %ell, align 4
  %arrayidx55 = getelementptr inbounds float, float* %111, i32 3
  store float %110, float* %arrayidx55, align 4
  %112 = load float, float* %q2, align 4
  %113 = load float*, float** %ell, align 4
  %114 = load i32, i32* %nskip1.addr, align 4
  %add56 = add nsw i32 3, %114
  %arrayidx57 = getelementptr inbounds float, float* %113, i32 %add56
  store float %112, float* %arrayidx57, align 4
  %115 = load float, float* %p1, align 4
  %116 = load float, float* %q1, align 4
  %mul58 = fmul float %115, %116
  store float %mul58, float* %m11, align 4
  %117 = load float, float* %p2, align 4
  %118 = load float, float* %q1, align 4
  %mul59 = fmul float %117, %118
  store float %mul59, float* %m21, align 4
  %119 = load float, float* %p2, align 4
  %120 = load float, float* %q2, align 4
  %mul60 = fmul float %119, %120
  store float %mul60, float* %m22, align 4
  %121 = load float, float* %m11, align 4
  %122 = load float, float* %Z11, align 4
  %add61 = fadd float %122, %121
  store float %add61, float* %Z11, align 4
  %123 = load float, float* %m21, align 4
  %124 = load float, float* %Z21, align 4
  %add62 = fadd float %124, %123
  store float %add62, float* %Z21, align 4
  %125 = load float, float* %m22, align 4
  %126 = load float, float* %Z22, align 4
  %add63 = fadd float %126, %125
  store float %add63, float* %Z22, align 4
  %127 = load float*, float** %ell, align 4
  %arrayidx64 = getelementptr inbounds float, float* %127, i32 4
  %128 = load float, float* %arrayidx64, align 4
  store float %128, float* %p1, align 4
  %129 = load float*, float** %ell, align 4
  %130 = load i32, i32* %nskip1.addr, align 4
  %add65 = add nsw i32 4, %130
  %arrayidx66 = getelementptr inbounds float, float* %129, i32 %add65
  %131 = load float, float* %arrayidx66, align 4
  store float %131, float* %p2, align 4
  %132 = load float*, float** %dee, align 4
  %arrayidx67 = getelementptr inbounds float, float* %132, i32 4
  %133 = load float, float* %arrayidx67, align 4
  store float %133, float* %dd, align 4
  %134 = load float, float* %p1, align 4
  %135 = load float, float* %dd, align 4
  %mul68 = fmul float %134, %135
  store float %mul68, float* %q1, align 4
  %136 = load float, float* %p2, align 4
  %137 = load float, float* %dd, align 4
  %mul69 = fmul float %136, %137
  store float %mul69, float* %q2, align 4
  %138 = load float, float* %q1, align 4
  %139 = load float*, float** %ell, align 4
  %arrayidx70 = getelementptr inbounds float, float* %139, i32 4
  store float %138, float* %arrayidx70, align 4
  %140 = load float, float* %q2, align 4
  %141 = load float*, float** %ell, align 4
  %142 = load i32, i32* %nskip1.addr, align 4
  %add71 = add nsw i32 4, %142
  %arrayidx72 = getelementptr inbounds float, float* %141, i32 %add71
  store float %140, float* %arrayidx72, align 4
  %143 = load float, float* %p1, align 4
  %144 = load float, float* %q1, align 4
  %mul73 = fmul float %143, %144
  store float %mul73, float* %m11, align 4
  %145 = load float, float* %p2, align 4
  %146 = load float, float* %q1, align 4
  %mul74 = fmul float %145, %146
  store float %mul74, float* %m21, align 4
  %147 = load float, float* %p2, align 4
  %148 = load float, float* %q2, align 4
  %mul75 = fmul float %147, %148
  store float %mul75, float* %m22, align 4
  %149 = load float, float* %m11, align 4
  %150 = load float, float* %Z11, align 4
  %add76 = fadd float %150, %149
  store float %add76, float* %Z11, align 4
  %151 = load float, float* %m21, align 4
  %152 = load float, float* %Z21, align 4
  %add77 = fadd float %152, %151
  store float %add77, float* %Z21, align 4
  %153 = load float, float* %m22, align 4
  %154 = load float, float* %Z22, align 4
  %add78 = fadd float %154, %153
  store float %add78, float* %Z22, align 4
  %155 = load float*, float** %ell, align 4
  %arrayidx79 = getelementptr inbounds float, float* %155, i32 5
  %156 = load float, float* %arrayidx79, align 4
  store float %156, float* %p1, align 4
  %157 = load float*, float** %ell, align 4
  %158 = load i32, i32* %nskip1.addr, align 4
  %add80 = add nsw i32 5, %158
  %arrayidx81 = getelementptr inbounds float, float* %157, i32 %add80
  %159 = load float, float* %arrayidx81, align 4
  store float %159, float* %p2, align 4
  %160 = load float*, float** %dee, align 4
  %arrayidx82 = getelementptr inbounds float, float* %160, i32 5
  %161 = load float, float* %arrayidx82, align 4
  store float %161, float* %dd, align 4
  %162 = load float, float* %p1, align 4
  %163 = load float, float* %dd, align 4
  %mul83 = fmul float %162, %163
  store float %mul83, float* %q1, align 4
  %164 = load float, float* %p2, align 4
  %165 = load float, float* %dd, align 4
  %mul84 = fmul float %164, %165
  store float %mul84, float* %q2, align 4
  %166 = load float, float* %q1, align 4
  %167 = load float*, float** %ell, align 4
  %arrayidx85 = getelementptr inbounds float, float* %167, i32 5
  store float %166, float* %arrayidx85, align 4
  %168 = load float, float* %q2, align 4
  %169 = load float*, float** %ell, align 4
  %170 = load i32, i32* %nskip1.addr, align 4
  %add86 = add nsw i32 5, %170
  %arrayidx87 = getelementptr inbounds float, float* %169, i32 %add86
  store float %168, float* %arrayidx87, align 4
  %171 = load float, float* %p1, align 4
  %172 = load float, float* %q1, align 4
  %mul88 = fmul float %171, %172
  store float %mul88, float* %m11, align 4
  %173 = load float, float* %p2, align 4
  %174 = load float, float* %q1, align 4
  %mul89 = fmul float %173, %174
  store float %mul89, float* %m21, align 4
  %175 = load float, float* %p2, align 4
  %176 = load float, float* %q2, align 4
  %mul90 = fmul float %175, %176
  store float %mul90, float* %m22, align 4
  %177 = load float, float* %m11, align 4
  %178 = load float, float* %Z11, align 4
  %add91 = fadd float %178, %177
  store float %add91, float* %Z11, align 4
  %179 = load float, float* %m21, align 4
  %180 = load float, float* %Z21, align 4
  %add92 = fadd float %180, %179
  store float %add92, float* %Z21, align 4
  %181 = load float, float* %m22, align 4
  %182 = load float, float* %Z22, align 4
  %add93 = fadd float %182, %181
  store float %add93, float* %Z22, align 4
  %183 = load float*, float** %ell, align 4
  %add.ptr94 = getelementptr inbounds float, float* %183, i32 6
  store float* %add.ptr94, float** %ell, align 4
  %184 = load float*, float** %dee, align 4
  %add.ptr95 = getelementptr inbounds float, float* %184, i32 6
  store float* %add.ptr95, float** %dee, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body7
  %185 = load i32, i32* %j, align 4
  %sub96 = sub nsw i32 %185, 6
  store i32 %sub96, i32* %j, align 4
  br label %for.cond5

for.end:                                          ; preds = %for.cond5
  %186 = load i32, i32* %j, align 4
  %add97 = add nsw i32 %186, 6
  store i32 %add97, i32* %j, align 4
  br label %for.cond98

for.cond98:                                       ; preds = %for.inc115, %for.end
  %187 = load i32, i32* %j, align 4
  %cmp99 = icmp sgt i32 %187, 0
  br i1 %cmp99, label %for.body100, label %for.end116

for.body100:                                      ; preds = %for.cond98
  %188 = load float*, float** %ell, align 4
  %arrayidx101 = getelementptr inbounds float, float* %188, i32 0
  %189 = load float, float* %arrayidx101, align 4
  store float %189, float* %p1, align 4
  %190 = load float*, float** %ell, align 4
  %191 = load i32, i32* %nskip1.addr, align 4
  %arrayidx102 = getelementptr inbounds float, float* %190, i32 %191
  %192 = load float, float* %arrayidx102, align 4
  store float %192, float* %p2, align 4
  %193 = load float*, float** %dee, align 4
  %arrayidx103 = getelementptr inbounds float, float* %193, i32 0
  %194 = load float, float* %arrayidx103, align 4
  store float %194, float* %dd, align 4
  %195 = load float, float* %p1, align 4
  %196 = load float, float* %dd, align 4
  %mul104 = fmul float %195, %196
  store float %mul104, float* %q1, align 4
  %197 = load float, float* %p2, align 4
  %198 = load float, float* %dd, align 4
  %mul105 = fmul float %197, %198
  store float %mul105, float* %q2, align 4
  %199 = load float, float* %q1, align 4
  %200 = load float*, float** %ell, align 4
  %arrayidx106 = getelementptr inbounds float, float* %200, i32 0
  store float %199, float* %arrayidx106, align 4
  %201 = load float, float* %q2, align 4
  %202 = load float*, float** %ell, align 4
  %203 = load i32, i32* %nskip1.addr, align 4
  %arrayidx107 = getelementptr inbounds float, float* %202, i32 %203
  store float %201, float* %arrayidx107, align 4
  %204 = load float, float* %p1, align 4
  %205 = load float, float* %q1, align 4
  %mul108 = fmul float %204, %205
  store float %mul108, float* %m11, align 4
  %206 = load float, float* %p2, align 4
  %207 = load float, float* %q1, align 4
  %mul109 = fmul float %206, %207
  store float %mul109, float* %m21, align 4
  %208 = load float, float* %p2, align 4
  %209 = load float, float* %q2, align 4
  %mul110 = fmul float %208, %209
  store float %mul110, float* %m22, align 4
  %210 = load float, float* %m11, align 4
  %211 = load float, float* %Z11, align 4
  %add111 = fadd float %211, %210
  store float %add111, float* %Z11, align 4
  %212 = load float, float* %m21, align 4
  %213 = load float, float* %Z21, align 4
  %add112 = fadd float %213, %212
  store float %add112, float* %Z21, align 4
  %214 = load float, float* %m22, align 4
  %215 = load float, float* %Z22, align 4
  %add113 = fadd float %215, %214
  store float %add113, float* %Z22, align 4
  %216 = load float*, float** %ell, align 4
  %incdec.ptr = getelementptr inbounds float, float* %216, i32 1
  store float* %incdec.ptr, float** %ell, align 4
  %217 = load float*, float** %dee, align 4
  %incdec.ptr114 = getelementptr inbounds float, float* %217, i32 1
  store float* %incdec.ptr114, float** %dee, align 4
  br label %for.inc115

for.inc115:                                       ; preds = %for.body100
  %218 = load i32, i32* %j, align 4
  %dec = add nsw i32 %218, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond98

for.end116:                                       ; preds = %for.cond98
  %219 = load float*, float** %ell, align 4
  %arrayidx117 = getelementptr inbounds float, float* %219, i32 0
  %220 = load float, float* %arrayidx117, align 4
  %221 = load float, float* %Z11, align 4
  %sub118 = fsub float %220, %221
  store float %sub118, float* %Z11, align 4
  %222 = load float*, float** %ell, align 4
  %223 = load i32, i32* %nskip1.addr, align 4
  %arrayidx119 = getelementptr inbounds float, float* %222, i32 %223
  %224 = load float, float* %arrayidx119, align 4
  %225 = load float, float* %Z21, align 4
  %sub120 = fsub float %224, %225
  store float %sub120, float* %Z21, align 4
  %226 = load float*, float** %ell, align 4
  %227 = load i32, i32* %nskip1.addr, align 4
  %add121 = add nsw i32 1, %227
  %arrayidx122 = getelementptr inbounds float, float* %226, i32 %add121
  %228 = load float, float* %arrayidx122, align 4
  %229 = load float, float* %Z22, align 4
  %sub123 = fsub float %228, %229
  store float %sub123, float* %Z22, align 4
  %230 = load float*, float** %d.addr, align 4
  %231 = load i32, i32* %i, align 4
  %add.ptr124 = getelementptr inbounds float, float* %230, i32 %231
  store float* %add.ptr124, float** %dee, align 4
  %232 = load float, float* %Z11, align 4
  %div = fdiv float 1.000000e+00, %232
  %233 = load float*, float** %dee, align 4
  %arrayidx125 = getelementptr inbounds float, float* %233, i32 0
  store float %div, float* %arrayidx125, align 4
  store float 0.000000e+00, float* %sum, align 4
  %234 = load float, float* %Z21, align 4
  store float %234, float* %q1, align 4
  %235 = load float, float* %q1, align 4
  %236 = load float*, float** %dee, align 4
  %arrayidx126 = getelementptr inbounds float, float* %236, i32 0
  %237 = load float, float* %arrayidx126, align 4
  %mul127 = fmul float %235, %237
  store float %mul127, float* %q2, align 4
  %238 = load float, float* %q2, align 4
  store float %238, float* %Z21, align 4
  %239 = load float, float* %q1, align 4
  %240 = load float, float* %q2, align 4
  %mul128 = fmul float %239, %240
  %241 = load float, float* %sum, align 4
  %add129 = fadd float %241, %mul128
  store float %add129, float* %sum, align 4
  %242 = load float, float* %Z22, align 4
  %243 = load float, float* %sum, align 4
  %sub130 = fsub float %242, %243
  %div131 = fdiv float 1.000000e+00, %sub130
  %244 = load float*, float** %dee, align 4
  %arrayidx132 = getelementptr inbounds float, float* %244, i32 1
  store float %div131, float* %arrayidx132, align 4
  %245 = load float, float* %Z21, align 4
  %246 = load float*, float** %ell, align 4
  %247 = load i32, i32* %nskip1.addr, align 4
  %arrayidx133 = getelementptr inbounds float, float* %246, i32 %247
  store float %245, float* %arrayidx133, align 4
  br label %for.inc134

for.inc134:                                       ; preds = %for.end116
  %248 = load i32, i32* %i, align 4
  %add135 = add nsw i32 %248, 2
  store i32 %add135, i32* %i, align 4
  br label %for.cond

for.end136:                                       ; preds = %for.cond
  %249 = load i32, i32* %n.addr, align 4
  %250 = load i32, i32* %i, align 4
  %sub137 = sub nsw i32 %249, %250
  switch i32 %sub137, label %sw.epilog [
    i32 0, label %sw.bb
    i32 1, label %sw.bb138
  ]

sw.bb:                                            ; preds = %for.end136
  br label %sw.epilog

sw.bb138:                                         ; preds = %for.end136
  %251 = load float*, float** %A.addr, align 4
  %252 = load float*, float** %A.addr, align 4
  %253 = load i32, i32* %i, align 4
  %254 = load i32, i32* %nskip1.addr, align 4
  %mul139 = mul nsw i32 %253, %254
  %add.ptr140 = getelementptr inbounds float, float* %252, i32 %mul139
  %255 = load i32, i32* %i, align 4
  %256 = load i32, i32* %nskip1.addr, align 4
  call void @_ZL11btSolveL1_1PKfPfii(float* %251, float* %add.ptr140, i32 %255, i32 %256)
  store float 0.000000e+00, float* %Z11, align 4
  %257 = load float*, float** %A.addr, align 4
  %258 = load i32, i32* %i, align 4
  %259 = load i32, i32* %nskip1.addr, align 4
  %mul141 = mul nsw i32 %258, %259
  %add.ptr142 = getelementptr inbounds float, float* %257, i32 %mul141
  store float* %add.ptr142, float** %ell, align 4
  %260 = load float*, float** %d.addr, align 4
  store float* %260, float** %dee, align 4
  %261 = load i32, i32* %i, align 4
  %sub143 = sub nsw i32 %261, 6
  store i32 %sub143, i32* %j, align 4
  br label %for.cond144

for.cond144:                                      ; preds = %for.inc185, %sw.bb138
  %262 = load i32, i32* %j, align 4
  %cmp145 = icmp sge i32 %262, 0
  br i1 %cmp145, label %for.body146, label %for.end187

for.body146:                                      ; preds = %for.cond144
  %263 = load float*, float** %ell, align 4
  %arrayidx147 = getelementptr inbounds float, float* %263, i32 0
  %264 = load float, float* %arrayidx147, align 4
  store float %264, float* %p1, align 4
  %265 = load float*, float** %dee, align 4
  %arrayidx148 = getelementptr inbounds float, float* %265, i32 0
  %266 = load float, float* %arrayidx148, align 4
  store float %266, float* %dd, align 4
  %267 = load float, float* %p1, align 4
  %268 = load float, float* %dd, align 4
  %mul149 = fmul float %267, %268
  store float %mul149, float* %q1, align 4
  %269 = load float, float* %q1, align 4
  %270 = load float*, float** %ell, align 4
  %arrayidx150 = getelementptr inbounds float, float* %270, i32 0
  store float %269, float* %arrayidx150, align 4
  %271 = load float, float* %p1, align 4
  %272 = load float, float* %q1, align 4
  %mul151 = fmul float %271, %272
  store float %mul151, float* %m11, align 4
  %273 = load float, float* %m11, align 4
  %274 = load float, float* %Z11, align 4
  %add152 = fadd float %274, %273
  store float %add152, float* %Z11, align 4
  %275 = load float*, float** %ell, align 4
  %arrayidx153 = getelementptr inbounds float, float* %275, i32 1
  %276 = load float, float* %arrayidx153, align 4
  store float %276, float* %p1, align 4
  %277 = load float*, float** %dee, align 4
  %arrayidx154 = getelementptr inbounds float, float* %277, i32 1
  %278 = load float, float* %arrayidx154, align 4
  store float %278, float* %dd, align 4
  %279 = load float, float* %p1, align 4
  %280 = load float, float* %dd, align 4
  %mul155 = fmul float %279, %280
  store float %mul155, float* %q1, align 4
  %281 = load float, float* %q1, align 4
  %282 = load float*, float** %ell, align 4
  %arrayidx156 = getelementptr inbounds float, float* %282, i32 1
  store float %281, float* %arrayidx156, align 4
  %283 = load float, float* %p1, align 4
  %284 = load float, float* %q1, align 4
  %mul157 = fmul float %283, %284
  store float %mul157, float* %m11, align 4
  %285 = load float, float* %m11, align 4
  %286 = load float, float* %Z11, align 4
  %add158 = fadd float %286, %285
  store float %add158, float* %Z11, align 4
  %287 = load float*, float** %ell, align 4
  %arrayidx159 = getelementptr inbounds float, float* %287, i32 2
  %288 = load float, float* %arrayidx159, align 4
  store float %288, float* %p1, align 4
  %289 = load float*, float** %dee, align 4
  %arrayidx160 = getelementptr inbounds float, float* %289, i32 2
  %290 = load float, float* %arrayidx160, align 4
  store float %290, float* %dd, align 4
  %291 = load float, float* %p1, align 4
  %292 = load float, float* %dd, align 4
  %mul161 = fmul float %291, %292
  store float %mul161, float* %q1, align 4
  %293 = load float, float* %q1, align 4
  %294 = load float*, float** %ell, align 4
  %arrayidx162 = getelementptr inbounds float, float* %294, i32 2
  store float %293, float* %arrayidx162, align 4
  %295 = load float, float* %p1, align 4
  %296 = load float, float* %q1, align 4
  %mul163 = fmul float %295, %296
  store float %mul163, float* %m11, align 4
  %297 = load float, float* %m11, align 4
  %298 = load float, float* %Z11, align 4
  %add164 = fadd float %298, %297
  store float %add164, float* %Z11, align 4
  %299 = load float*, float** %ell, align 4
  %arrayidx165 = getelementptr inbounds float, float* %299, i32 3
  %300 = load float, float* %arrayidx165, align 4
  store float %300, float* %p1, align 4
  %301 = load float*, float** %dee, align 4
  %arrayidx166 = getelementptr inbounds float, float* %301, i32 3
  %302 = load float, float* %arrayidx166, align 4
  store float %302, float* %dd, align 4
  %303 = load float, float* %p1, align 4
  %304 = load float, float* %dd, align 4
  %mul167 = fmul float %303, %304
  store float %mul167, float* %q1, align 4
  %305 = load float, float* %q1, align 4
  %306 = load float*, float** %ell, align 4
  %arrayidx168 = getelementptr inbounds float, float* %306, i32 3
  store float %305, float* %arrayidx168, align 4
  %307 = load float, float* %p1, align 4
  %308 = load float, float* %q1, align 4
  %mul169 = fmul float %307, %308
  store float %mul169, float* %m11, align 4
  %309 = load float, float* %m11, align 4
  %310 = load float, float* %Z11, align 4
  %add170 = fadd float %310, %309
  store float %add170, float* %Z11, align 4
  %311 = load float*, float** %ell, align 4
  %arrayidx171 = getelementptr inbounds float, float* %311, i32 4
  %312 = load float, float* %arrayidx171, align 4
  store float %312, float* %p1, align 4
  %313 = load float*, float** %dee, align 4
  %arrayidx172 = getelementptr inbounds float, float* %313, i32 4
  %314 = load float, float* %arrayidx172, align 4
  store float %314, float* %dd, align 4
  %315 = load float, float* %p1, align 4
  %316 = load float, float* %dd, align 4
  %mul173 = fmul float %315, %316
  store float %mul173, float* %q1, align 4
  %317 = load float, float* %q1, align 4
  %318 = load float*, float** %ell, align 4
  %arrayidx174 = getelementptr inbounds float, float* %318, i32 4
  store float %317, float* %arrayidx174, align 4
  %319 = load float, float* %p1, align 4
  %320 = load float, float* %q1, align 4
  %mul175 = fmul float %319, %320
  store float %mul175, float* %m11, align 4
  %321 = load float, float* %m11, align 4
  %322 = load float, float* %Z11, align 4
  %add176 = fadd float %322, %321
  store float %add176, float* %Z11, align 4
  %323 = load float*, float** %ell, align 4
  %arrayidx177 = getelementptr inbounds float, float* %323, i32 5
  %324 = load float, float* %arrayidx177, align 4
  store float %324, float* %p1, align 4
  %325 = load float*, float** %dee, align 4
  %arrayidx178 = getelementptr inbounds float, float* %325, i32 5
  %326 = load float, float* %arrayidx178, align 4
  store float %326, float* %dd, align 4
  %327 = load float, float* %p1, align 4
  %328 = load float, float* %dd, align 4
  %mul179 = fmul float %327, %328
  store float %mul179, float* %q1, align 4
  %329 = load float, float* %q1, align 4
  %330 = load float*, float** %ell, align 4
  %arrayidx180 = getelementptr inbounds float, float* %330, i32 5
  store float %329, float* %arrayidx180, align 4
  %331 = load float, float* %p1, align 4
  %332 = load float, float* %q1, align 4
  %mul181 = fmul float %331, %332
  store float %mul181, float* %m11, align 4
  %333 = load float, float* %m11, align 4
  %334 = load float, float* %Z11, align 4
  %add182 = fadd float %334, %333
  store float %add182, float* %Z11, align 4
  %335 = load float*, float** %ell, align 4
  %add.ptr183 = getelementptr inbounds float, float* %335, i32 6
  store float* %add.ptr183, float** %ell, align 4
  %336 = load float*, float** %dee, align 4
  %add.ptr184 = getelementptr inbounds float, float* %336, i32 6
  store float* %add.ptr184, float** %dee, align 4
  br label %for.inc185

for.inc185:                                       ; preds = %for.body146
  %337 = load i32, i32* %j, align 4
  %sub186 = sub nsw i32 %337, 6
  store i32 %sub186, i32* %j, align 4
  br label %for.cond144

for.end187:                                       ; preds = %for.cond144
  %338 = load i32, i32* %j, align 4
  %add188 = add nsw i32 %338, 6
  store i32 %add188, i32* %j, align 4
  br label %for.cond189

for.cond189:                                      ; preds = %for.inc200, %for.end187
  %339 = load i32, i32* %j, align 4
  %cmp190 = icmp sgt i32 %339, 0
  br i1 %cmp190, label %for.body191, label %for.end202

for.body191:                                      ; preds = %for.cond189
  %340 = load float*, float** %ell, align 4
  %arrayidx192 = getelementptr inbounds float, float* %340, i32 0
  %341 = load float, float* %arrayidx192, align 4
  store float %341, float* %p1, align 4
  %342 = load float*, float** %dee, align 4
  %arrayidx193 = getelementptr inbounds float, float* %342, i32 0
  %343 = load float, float* %arrayidx193, align 4
  store float %343, float* %dd, align 4
  %344 = load float, float* %p1, align 4
  %345 = load float, float* %dd, align 4
  %mul194 = fmul float %344, %345
  store float %mul194, float* %q1, align 4
  %346 = load float, float* %q1, align 4
  %347 = load float*, float** %ell, align 4
  %arrayidx195 = getelementptr inbounds float, float* %347, i32 0
  store float %346, float* %arrayidx195, align 4
  %348 = load float, float* %p1, align 4
  %349 = load float, float* %q1, align 4
  %mul196 = fmul float %348, %349
  store float %mul196, float* %m11, align 4
  %350 = load float, float* %m11, align 4
  %351 = load float, float* %Z11, align 4
  %add197 = fadd float %351, %350
  store float %add197, float* %Z11, align 4
  %352 = load float*, float** %ell, align 4
  %incdec.ptr198 = getelementptr inbounds float, float* %352, i32 1
  store float* %incdec.ptr198, float** %ell, align 4
  %353 = load float*, float** %dee, align 4
  %incdec.ptr199 = getelementptr inbounds float, float* %353, i32 1
  store float* %incdec.ptr199, float** %dee, align 4
  br label %for.inc200

for.inc200:                                       ; preds = %for.body191
  %354 = load i32, i32* %j, align 4
  %dec201 = add nsw i32 %354, -1
  store i32 %dec201, i32* %j, align 4
  br label %for.cond189

for.end202:                                       ; preds = %for.cond189
  %355 = load float*, float** %ell, align 4
  %arrayidx203 = getelementptr inbounds float, float* %355, i32 0
  %356 = load float, float* %arrayidx203, align 4
  %357 = load float, float* %Z11, align 4
  %sub204 = fsub float %356, %357
  store float %sub204, float* %Z11, align 4
  %358 = load float*, float** %d.addr, align 4
  %359 = load i32, i32* %i, align 4
  %add.ptr205 = getelementptr inbounds float, float* %358, i32 %359
  store float* %add.ptr205, float** %dee, align 4
  %360 = load float, float* %Z11, align 4
  %div206 = fdiv float 1.000000e+00, %360
  %361 = load float*, float** %dee, align 4
  %arrayidx207 = getelementptr inbounds float, float* %361, i32 0
  store float %div206, float* %arrayidx207, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.then, %for.end136, %for.end202, %sw.bb
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZL11btSolveL1_2PKfPfii(float* %L, float* %B, i32 %n, i32 %lskip1) #1 {
entry:
  %L.addr = alloca float*, align 4
  %B.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %lskip1.addr = alloca i32, align 4
  %Z11 = alloca float, align 4
  %m11 = alloca float, align 4
  %Z12 = alloca float, align 4
  %m12 = alloca float, align 4
  %Z21 = alloca float, align 4
  %m21 = alloca float, align 4
  %Z22 = alloca float, align 4
  %m22 = alloca float, align 4
  %p1 = alloca float, align 4
  %q1 = alloca float, align 4
  %p2 = alloca float, align 4
  %q2 = alloca float, align 4
  %ex = alloca float*, align 4
  %ell = alloca float*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store float* %L, float** %L.addr, align 4
  store float* %B, float** %B.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 %lskip1, i32* %lskip1.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc70, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %n.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end72

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %Z11, align 4
  store float 0.000000e+00, float* %Z12, align 4
  store float 0.000000e+00, float* %Z21, align 4
  store float 0.000000e+00, float* %Z22, align 4
  %2 = load float*, float** %L.addr, align 4
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %lskip1.addr, align 4
  %mul = mul nsw i32 %3, %4
  %add.ptr = getelementptr inbounds float, float* %2, i32 %mul
  store float* %add.ptr, float** %ell, align 4
  %5 = load float*, float** %B.addr, align 4
  store float* %5, float** %ex, align 4
  %6 = load i32, i32* %i, align 4
  %sub = sub nsw i32 %6, 2
  store i32 %sub, i32* %j, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %7 = load i32, i32* %j, align 4
  %cmp2 = icmp sge i32 %7, 0
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %8 = load float*, float** %ell, align 4
  %arrayidx = getelementptr inbounds float, float* %8, i32 0
  %9 = load float, float* %arrayidx, align 4
  store float %9, float* %p1, align 4
  %10 = load float*, float** %ex, align 4
  %arrayidx4 = getelementptr inbounds float, float* %10, i32 0
  %11 = load float, float* %arrayidx4, align 4
  store float %11, float* %q1, align 4
  %12 = load float, float* %p1, align 4
  %13 = load float, float* %q1, align 4
  %mul5 = fmul float %12, %13
  store float %mul5, float* %m11, align 4
  %14 = load float*, float** %ex, align 4
  %15 = load i32, i32* %lskip1.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %14, i32 %15
  %16 = load float, float* %arrayidx6, align 4
  store float %16, float* %q2, align 4
  %17 = load float, float* %p1, align 4
  %18 = load float, float* %q2, align 4
  %mul7 = fmul float %17, %18
  store float %mul7, float* %m12, align 4
  %19 = load float*, float** %ell, align 4
  %20 = load i32, i32* %lskip1.addr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %19, i32 %20
  %21 = load float, float* %arrayidx8, align 4
  store float %21, float* %p2, align 4
  %22 = load float, float* %p2, align 4
  %23 = load float, float* %q1, align 4
  %mul9 = fmul float %22, %23
  store float %mul9, float* %m21, align 4
  %24 = load float, float* %p2, align 4
  %25 = load float, float* %q2, align 4
  %mul10 = fmul float %24, %25
  store float %mul10, float* %m22, align 4
  %26 = load float, float* %m11, align 4
  %27 = load float, float* %Z11, align 4
  %add = fadd float %27, %26
  store float %add, float* %Z11, align 4
  %28 = load float, float* %m12, align 4
  %29 = load float, float* %Z12, align 4
  %add11 = fadd float %29, %28
  store float %add11, float* %Z12, align 4
  %30 = load float, float* %m21, align 4
  %31 = load float, float* %Z21, align 4
  %add12 = fadd float %31, %30
  store float %add12, float* %Z21, align 4
  %32 = load float, float* %m22, align 4
  %33 = load float, float* %Z22, align 4
  %add13 = fadd float %33, %32
  store float %add13, float* %Z22, align 4
  %34 = load float*, float** %ell, align 4
  %arrayidx14 = getelementptr inbounds float, float* %34, i32 1
  %35 = load float, float* %arrayidx14, align 4
  store float %35, float* %p1, align 4
  %36 = load float*, float** %ex, align 4
  %arrayidx15 = getelementptr inbounds float, float* %36, i32 1
  %37 = load float, float* %arrayidx15, align 4
  store float %37, float* %q1, align 4
  %38 = load float, float* %p1, align 4
  %39 = load float, float* %q1, align 4
  %mul16 = fmul float %38, %39
  store float %mul16, float* %m11, align 4
  %40 = load float*, float** %ex, align 4
  %41 = load i32, i32* %lskip1.addr, align 4
  %add17 = add nsw i32 1, %41
  %arrayidx18 = getelementptr inbounds float, float* %40, i32 %add17
  %42 = load float, float* %arrayidx18, align 4
  store float %42, float* %q2, align 4
  %43 = load float, float* %p1, align 4
  %44 = load float, float* %q2, align 4
  %mul19 = fmul float %43, %44
  store float %mul19, float* %m12, align 4
  %45 = load float*, float** %ell, align 4
  %46 = load i32, i32* %lskip1.addr, align 4
  %add20 = add nsw i32 1, %46
  %arrayidx21 = getelementptr inbounds float, float* %45, i32 %add20
  %47 = load float, float* %arrayidx21, align 4
  store float %47, float* %p2, align 4
  %48 = load float, float* %p2, align 4
  %49 = load float, float* %q1, align 4
  %mul22 = fmul float %48, %49
  store float %mul22, float* %m21, align 4
  %50 = load float, float* %p2, align 4
  %51 = load float, float* %q2, align 4
  %mul23 = fmul float %50, %51
  store float %mul23, float* %m22, align 4
  %52 = load float*, float** %ell, align 4
  %add.ptr24 = getelementptr inbounds float, float* %52, i32 2
  store float* %add.ptr24, float** %ell, align 4
  %53 = load float*, float** %ex, align 4
  %add.ptr25 = getelementptr inbounds float, float* %53, i32 2
  store float* %add.ptr25, float** %ex, align 4
  %54 = load float, float* %m11, align 4
  %55 = load float, float* %Z11, align 4
  %add26 = fadd float %55, %54
  store float %add26, float* %Z11, align 4
  %56 = load float, float* %m12, align 4
  %57 = load float, float* %Z12, align 4
  %add27 = fadd float %57, %56
  store float %add27, float* %Z12, align 4
  %58 = load float, float* %m21, align 4
  %59 = load float, float* %Z21, align 4
  %add28 = fadd float %59, %58
  store float %add28, float* %Z21, align 4
  %60 = load float, float* %m22, align 4
  %61 = load float, float* %Z22, align 4
  %add29 = fadd float %61, %60
  store float %add29, float* %Z22, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %62 = load i32, i32* %j, align 4
  %sub30 = sub nsw i32 %62, 2
  store i32 %sub30, i32* %j, align 4
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %63 = load i32, i32* %j, align 4
  %add31 = add nsw i32 %63, 2
  store i32 %add31, i32* %j, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc49, %for.end
  %64 = load i32, i32* %j, align 4
  %cmp33 = icmp sgt i32 %64, 0
  br i1 %cmp33, label %for.body34, label %for.end50

for.body34:                                       ; preds = %for.cond32
  %65 = load float*, float** %ell, align 4
  %arrayidx35 = getelementptr inbounds float, float* %65, i32 0
  %66 = load float, float* %arrayidx35, align 4
  store float %66, float* %p1, align 4
  %67 = load float*, float** %ex, align 4
  %arrayidx36 = getelementptr inbounds float, float* %67, i32 0
  %68 = load float, float* %arrayidx36, align 4
  store float %68, float* %q1, align 4
  %69 = load float, float* %p1, align 4
  %70 = load float, float* %q1, align 4
  %mul37 = fmul float %69, %70
  store float %mul37, float* %m11, align 4
  %71 = load float*, float** %ex, align 4
  %72 = load i32, i32* %lskip1.addr, align 4
  %arrayidx38 = getelementptr inbounds float, float* %71, i32 %72
  %73 = load float, float* %arrayidx38, align 4
  store float %73, float* %q2, align 4
  %74 = load float, float* %p1, align 4
  %75 = load float, float* %q2, align 4
  %mul39 = fmul float %74, %75
  store float %mul39, float* %m12, align 4
  %76 = load float*, float** %ell, align 4
  %77 = load i32, i32* %lskip1.addr, align 4
  %arrayidx40 = getelementptr inbounds float, float* %76, i32 %77
  %78 = load float, float* %arrayidx40, align 4
  store float %78, float* %p2, align 4
  %79 = load float, float* %p2, align 4
  %80 = load float, float* %q1, align 4
  %mul41 = fmul float %79, %80
  store float %mul41, float* %m21, align 4
  %81 = load float, float* %p2, align 4
  %82 = load float, float* %q2, align 4
  %mul42 = fmul float %81, %82
  store float %mul42, float* %m22, align 4
  %83 = load float*, float** %ell, align 4
  %add.ptr43 = getelementptr inbounds float, float* %83, i32 1
  store float* %add.ptr43, float** %ell, align 4
  %84 = load float*, float** %ex, align 4
  %add.ptr44 = getelementptr inbounds float, float* %84, i32 1
  store float* %add.ptr44, float** %ex, align 4
  %85 = load float, float* %m11, align 4
  %86 = load float, float* %Z11, align 4
  %add45 = fadd float %86, %85
  store float %add45, float* %Z11, align 4
  %87 = load float, float* %m12, align 4
  %88 = load float, float* %Z12, align 4
  %add46 = fadd float %88, %87
  store float %add46, float* %Z12, align 4
  %89 = load float, float* %m21, align 4
  %90 = load float, float* %Z21, align 4
  %add47 = fadd float %90, %89
  store float %add47, float* %Z21, align 4
  %91 = load float, float* %m22, align 4
  %92 = load float, float* %Z22, align 4
  %add48 = fadd float %92, %91
  store float %add48, float* %Z22, align 4
  br label %for.inc49

for.inc49:                                        ; preds = %for.body34
  %93 = load i32, i32* %j, align 4
  %dec = add nsw i32 %93, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond32

for.end50:                                        ; preds = %for.cond32
  %94 = load float*, float** %ex, align 4
  %arrayidx51 = getelementptr inbounds float, float* %94, i32 0
  %95 = load float, float* %arrayidx51, align 4
  %96 = load float, float* %Z11, align 4
  %sub52 = fsub float %95, %96
  store float %sub52, float* %Z11, align 4
  %97 = load float, float* %Z11, align 4
  %98 = load float*, float** %ex, align 4
  %arrayidx53 = getelementptr inbounds float, float* %98, i32 0
  store float %97, float* %arrayidx53, align 4
  %99 = load float*, float** %ex, align 4
  %100 = load i32, i32* %lskip1.addr, align 4
  %arrayidx54 = getelementptr inbounds float, float* %99, i32 %100
  %101 = load float, float* %arrayidx54, align 4
  %102 = load float, float* %Z12, align 4
  %sub55 = fsub float %101, %102
  store float %sub55, float* %Z12, align 4
  %103 = load float, float* %Z12, align 4
  %104 = load float*, float** %ex, align 4
  %105 = load i32, i32* %lskip1.addr, align 4
  %arrayidx56 = getelementptr inbounds float, float* %104, i32 %105
  store float %103, float* %arrayidx56, align 4
  %106 = load float*, float** %ell, align 4
  %107 = load i32, i32* %lskip1.addr, align 4
  %arrayidx57 = getelementptr inbounds float, float* %106, i32 %107
  %108 = load float, float* %arrayidx57, align 4
  store float %108, float* %p1, align 4
  %109 = load float*, float** %ex, align 4
  %arrayidx58 = getelementptr inbounds float, float* %109, i32 1
  %110 = load float, float* %arrayidx58, align 4
  %111 = load float, float* %Z21, align 4
  %sub59 = fsub float %110, %111
  %112 = load float, float* %p1, align 4
  %113 = load float, float* %Z11, align 4
  %mul60 = fmul float %112, %113
  %sub61 = fsub float %sub59, %mul60
  store float %sub61, float* %Z21, align 4
  %114 = load float, float* %Z21, align 4
  %115 = load float*, float** %ex, align 4
  %arrayidx62 = getelementptr inbounds float, float* %115, i32 1
  store float %114, float* %arrayidx62, align 4
  %116 = load float*, float** %ex, align 4
  %117 = load i32, i32* %lskip1.addr, align 4
  %add63 = add nsw i32 1, %117
  %arrayidx64 = getelementptr inbounds float, float* %116, i32 %add63
  %118 = load float, float* %arrayidx64, align 4
  %119 = load float, float* %Z22, align 4
  %sub65 = fsub float %118, %119
  %120 = load float, float* %p1, align 4
  %121 = load float, float* %Z12, align 4
  %mul66 = fmul float %120, %121
  %sub67 = fsub float %sub65, %mul66
  store float %sub67, float* %Z22, align 4
  %122 = load float, float* %Z22, align 4
  %123 = load float*, float** %ex, align 4
  %124 = load i32, i32* %lskip1.addr, align 4
  %add68 = add nsw i32 1, %124
  %arrayidx69 = getelementptr inbounds float, float* %123, i32 %add68
  store float %122, float* %arrayidx69, align 4
  br label %for.inc70

for.inc70:                                        ; preds = %for.end50
  %125 = load i32, i32* %i, align 4
  %add71 = add nsw i32 %125, 2
  store i32 %add71, i32* %i, align 4
  br label %for.cond

for.end72:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZL11btSolveL1_1PKfPfii(float* %L, float* %B, i32 %n, i32 %lskip1) #1 {
entry:
  %L.addr = alloca float*, align 4
  %B.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %lskip1.addr = alloca i32, align 4
  %Z11 = alloca float, align 4
  %m11 = alloca float, align 4
  %Z21 = alloca float, align 4
  %m21 = alloca float, align 4
  %p1 = alloca float, align 4
  %q1 = alloca float, align 4
  %p2 = alloca float, align 4
  %ex = alloca float*, align 4
  %ell = alloca float*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store float* %L, float** %L.addr, align 4
  store float* %B, float** %B.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 %lskip1, i32* %lskip1.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc44, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %n.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end46

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %Z11, align 4
  store float 0.000000e+00, float* %Z21, align 4
  %2 = load float*, float** %L.addr, align 4
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %lskip1.addr, align 4
  %mul = mul nsw i32 %3, %4
  %add.ptr = getelementptr inbounds float, float* %2, i32 %mul
  store float* %add.ptr, float** %ell, align 4
  %5 = load float*, float** %B.addr, align 4
  store float* %5, float** %ex, align 4
  %6 = load i32, i32* %i, align 4
  %sub = sub nsw i32 %6, 2
  store i32 %sub, i32* %j, align 4
  br label %for.cond1

for.cond1:                                        ; preds = %for.inc, %for.body
  %7 = load i32, i32* %j, align 4
  %cmp2 = icmp sge i32 %7, 0
  br i1 %cmp2, label %for.body3, label %for.end

for.body3:                                        ; preds = %for.cond1
  %8 = load float*, float** %ell, align 4
  %arrayidx = getelementptr inbounds float, float* %8, i32 0
  %9 = load float, float* %arrayidx, align 4
  store float %9, float* %p1, align 4
  %10 = load float*, float** %ex, align 4
  %arrayidx4 = getelementptr inbounds float, float* %10, i32 0
  %11 = load float, float* %arrayidx4, align 4
  store float %11, float* %q1, align 4
  %12 = load float, float* %p1, align 4
  %13 = load float, float* %q1, align 4
  %mul5 = fmul float %12, %13
  store float %mul5, float* %m11, align 4
  %14 = load float*, float** %ell, align 4
  %15 = load i32, i32* %lskip1.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %14, i32 %15
  %16 = load float, float* %arrayidx6, align 4
  store float %16, float* %p2, align 4
  %17 = load float, float* %p2, align 4
  %18 = load float, float* %q1, align 4
  %mul7 = fmul float %17, %18
  store float %mul7, float* %m21, align 4
  %19 = load float, float* %m11, align 4
  %20 = load float, float* %Z11, align 4
  %add = fadd float %20, %19
  store float %add, float* %Z11, align 4
  %21 = load float, float* %m21, align 4
  %22 = load float, float* %Z21, align 4
  %add8 = fadd float %22, %21
  store float %add8, float* %Z21, align 4
  %23 = load float*, float** %ell, align 4
  %arrayidx9 = getelementptr inbounds float, float* %23, i32 1
  %24 = load float, float* %arrayidx9, align 4
  store float %24, float* %p1, align 4
  %25 = load float*, float** %ex, align 4
  %arrayidx10 = getelementptr inbounds float, float* %25, i32 1
  %26 = load float, float* %arrayidx10, align 4
  store float %26, float* %q1, align 4
  %27 = load float, float* %p1, align 4
  %28 = load float, float* %q1, align 4
  %mul11 = fmul float %27, %28
  store float %mul11, float* %m11, align 4
  %29 = load float*, float** %ell, align 4
  %30 = load i32, i32* %lskip1.addr, align 4
  %add12 = add nsw i32 1, %30
  %arrayidx13 = getelementptr inbounds float, float* %29, i32 %add12
  %31 = load float, float* %arrayidx13, align 4
  store float %31, float* %p2, align 4
  %32 = load float, float* %p2, align 4
  %33 = load float, float* %q1, align 4
  %mul14 = fmul float %32, %33
  store float %mul14, float* %m21, align 4
  %34 = load float*, float** %ell, align 4
  %add.ptr15 = getelementptr inbounds float, float* %34, i32 2
  store float* %add.ptr15, float** %ell, align 4
  %35 = load float*, float** %ex, align 4
  %add.ptr16 = getelementptr inbounds float, float* %35, i32 2
  store float* %add.ptr16, float** %ex, align 4
  %36 = load float, float* %m11, align 4
  %37 = load float, float* %Z11, align 4
  %add17 = fadd float %37, %36
  store float %add17, float* %Z11, align 4
  %38 = load float, float* %m21, align 4
  %39 = load float, float* %Z21, align 4
  %add18 = fadd float %39, %38
  store float %add18, float* %Z21, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body3
  %40 = load i32, i32* %j, align 4
  %sub19 = sub nsw i32 %40, 2
  store i32 %sub19, i32* %j, align 4
  br label %for.cond1

for.end:                                          ; preds = %for.cond1
  %41 = load i32, i32* %j, align 4
  %add20 = add nsw i32 %41, 2
  store i32 %add20, i32* %j, align 4
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc33, %for.end
  %42 = load i32, i32* %j, align 4
  %cmp22 = icmp sgt i32 %42, 0
  br i1 %cmp22, label %for.body23, label %for.end34

for.body23:                                       ; preds = %for.cond21
  %43 = load float*, float** %ell, align 4
  %arrayidx24 = getelementptr inbounds float, float* %43, i32 0
  %44 = load float, float* %arrayidx24, align 4
  store float %44, float* %p1, align 4
  %45 = load float*, float** %ex, align 4
  %arrayidx25 = getelementptr inbounds float, float* %45, i32 0
  %46 = load float, float* %arrayidx25, align 4
  store float %46, float* %q1, align 4
  %47 = load float, float* %p1, align 4
  %48 = load float, float* %q1, align 4
  %mul26 = fmul float %47, %48
  store float %mul26, float* %m11, align 4
  %49 = load float*, float** %ell, align 4
  %50 = load i32, i32* %lskip1.addr, align 4
  %arrayidx27 = getelementptr inbounds float, float* %49, i32 %50
  %51 = load float, float* %arrayidx27, align 4
  store float %51, float* %p2, align 4
  %52 = load float, float* %p2, align 4
  %53 = load float, float* %q1, align 4
  %mul28 = fmul float %52, %53
  store float %mul28, float* %m21, align 4
  %54 = load float*, float** %ell, align 4
  %add.ptr29 = getelementptr inbounds float, float* %54, i32 1
  store float* %add.ptr29, float** %ell, align 4
  %55 = load float*, float** %ex, align 4
  %add.ptr30 = getelementptr inbounds float, float* %55, i32 1
  store float* %add.ptr30, float** %ex, align 4
  %56 = load float, float* %m11, align 4
  %57 = load float, float* %Z11, align 4
  %add31 = fadd float %57, %56
  store float %add31, float* %Z11, align 4
  %58 = load float, float* %m21, align 4
  %59 = load float, float* %Z21, align 4
  %add32 = fadd float %59, %58
  store float %add32, float* %Z21, align 4
  br label %for.inc33

for.inc33:                                        ; preds = %for.body23
  %60 = load i32, i32* %j, align 4
  %dec = add nsw i32 %60, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond21

for.end34:                                        ; preds = %for.cond21
  %61 = load float*, float** %ex, align 4
  %arrayidx35 = getelementptr inbounds float, float* %61, i32 0
  %62 = load float, float* %arrayidx35, align 4
  %63 = load float, float* %Z11, align 4
  %sub36 = fsub float %62, %63
  store float %sub36, float* %Z11, align 4
  %64 = load float, float* %Z11, align 4
  %65 = load float*, float** %ex, align 4
  %arrayidx37 = getelementptr inbounds float, float* %65, i32 0
  store float %64, float* %arrayidx37, align 4
  %66 = load float*, float** %ell, align 4
  %67 = load i32, i32* %lskip1.addr, align 4
  %arrayidx38 = getelementptr inbounds float, float* %66, i32 %67
  %68 = load float, float* %arrayidx38, align 4
  store float %68, float* %p1, align 4
  %69 = load float*, float** %ex, align 4
  %arrayidx39 = getelementptr inbounds float, float* %69, i32 1
  %70 = load float, float* %arrayidx39, align 4
  %71 = load float, float* %Z21, align 4
  %sub40 = fsub float %70, %71
  %72 = load float, float* %p1, align 4
  %73 = load float, float* %Z11, align 4
  %mul41 = fmul float %72, %73
  %sub42 = fsub float %sub40, %mul41
  store float %sub42, float* %Z21, align 4
  %74 = load float, float* %Z21, align 4
  %75 = load float*, float** %ex, align 4
  %arrayidx43 = getelementptr inbounds float, float* %75, i32 1
  store float %74, float* %arrayidx43, align 4
  br label %for.inc44

for.inc44:                                        ; preds = %for.end34
  %76 = load i32, i32* %i, align 4
  %add45 = add nsw i32 %76, 2
  store i32 %add45, i32* %i, align 4
  br label %for.cond

for.end46:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z9btSolveL1PKfPfii(float* %L, float* %B, i32 %n, i32 %lskip1) #1 {
entry:
  %L.addr = alloca float*, align 4
  %B.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %lskip1.addr = alloca i32, align 4
  %Z11 = alloca float, align 4
  %Z21 = alloca float, align 4
  %Z31 = alloca float, align 4
  %Z41 = alloca float, align 4
  %p1 = alloca float, align 4
  %q1 = alloca float, align 4
  %p2 = alloca float, align 4
  %p3 = alloca float, align 4
  %p4 = alloca float, align 4
  %ex = alloca float*, align 4
  %ell = alloca float*, align 4
  %lskip2 = alloca i32, align 4
  %lskip3 = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store float* %L, float** %L.addr, align 4
  store float* %B, float** %B.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 %lskip1, i32* %lskip1.addr, align 4
  %0 = load i32, i32* %lskip1.addr, align 4
  %mul = mul nsw i32 2, %0
  store i32 %mul, i32* %lskip2, align 4
  %1 = load i32, i32* %lskip1.addr, align 4
  %mul1 = mul nsw i32 3, %1
  store i32 %mul1, i32* %lskip3, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc251, %entry
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %n.addr, align 4
  %sub = sub nsw i32 %3, 4
  %cmp = icmp sle i32 %2, %sub
  br i1 %cmp, label %for.body, label %for.end253

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %Z11, align 4
  store float 0.000000e+00, float* %Z21, align 4
  store float 0.000000e+00, float* %Z31, align 4
  store float 0.000000e+00, float* %Z41, align 4
  %4 = load float*, float** %L.addr, align 4
  %5 = load i32, i32* %i, align 4
  %6 = load i32, i32* %lskip1.addr, align 4
  %mul2 = mul nsw i32 %5, %6
  %add.ptr = getelementptr inbounds float, float* %4, i32 %mul2
  store float* %add.ptr, float** %ell, align 4
  %7 = load float*, float** %B.addr, align 4
  store float* %7, float** %ex, align 4
  %8 = load i32, i32* %i, align 4
  %sub3 = sub nsw i32 %8, 12
  store i32 %sub3, i32* %j, align 4
  br label %for.cond4

for.cond4:                                        ; preds = %for.inc, %for.body
  %9 = load i32, i32* %j, align 4
  %cmp5 = icmp sge i32 %9, 0
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond4
  %10 = load float*, float** %ell, align 4
  %arrayidx = getelementptr inbounds float, float* %10, i32 0
  %11 = load float, float* %arrayidx, align 4
  store float %11, float* %p1, align 4
  %12 = load float*, float** %ex, align 4
  %arrayidx7 = getelementptr inbounds float, float* %12, i32 0
  %13 = load float, float* %arrayidx7, align 4
  store float %13, float* %q1, align 4
  %14 = load float*, float** %ell, align 4
  %15 = load i32, i32* %lskip1.addr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %14, i32 %15
  %16 = load float, float* %arrayidx8, align 4
  store float %16, float* %p2, align 4
  %17 = load float*, float** %ell, align 4
  %18 = load i32, i32* %lskip2, align 4
  %arrayidx9 = getelementptr inbounds float, float* %17, i32 %18
  %19 = load float, float* %arrayidx9, align 4
  store float %19, float* %p3, align 4
  %20 = load float*, float** %ell, align 4
  %21 = load i32, i32* %lskip3, align 4
  %arrayidx10 = getelementptr inbounds float, float* %20, i32 %21
  %22 = load float, float* %arrayidx10, align 4
  store float %22, float* %p4, align 4
  %23 = load float, float* %p1, align 4
  %24 = load float, float* %q1, align 4
  %mul11 = fmul float %23, %24
  %25 = load float, float* %Z11, align 4
  %add = fadd float %25, %mul11
  store float %add, float* %Z11, align 4
  %26 = load float, float* %p2, align 4
  %27 = load float, float* %q1, align 4
  %mul12 = fmul float %26, %27
  %28 = load float, float* %Z21, align 4
  %add13 = fadd float %28, %mul12
  store float %add13, float* %Z21, align 4
  %29 = load float, float* %p3, align 4
  %30 = load float, float* %q1, align 4
  %mul14 = fmul float %29, %30
  %31 = load float, float* %Z31, align 4
  %add15 = fadd float %31, %mul14
  store float %add15, float* %Z31, align 4
  %32 = load float, float* %p4, align 4
  %33 = load float, float* %q1, align 4
  %mul16 = fmul float %32, %33
  %34 = load float, float* %Z41, align 4
  %add17 = fadd float %34, %mul16
  store float %add17, float* %Z41, align 4
  %35 = load float*, float** %ell, align 4
  %arrayidx18 = getelementptr inbounds float, float* %35, i32 1
  %36 = load float, float* %arrayidx18, align 4
  store float %36, float* %p1, align 4
  %37 = load float*, float** %ex, align 4
  %arrayidx19 = getelementptr inbounds float, float* %37, i32 1
  %38 = load float, float* %arrayidx19, align 4
  store float %38, float* %q1, align 4
  %39 = load float*, float** %ell, align 4
  %40 = load i32, i32* %lskip1.addr, align 4
  %add20 = add nsw i32 1, %40
  %arrayidx21 = getelementptr inbounds float, float* %39, i32 %add20
  %41 = load float, float* %arrayidx21, align 4
  store float %41, float* %p2, align 4
  %42 = load float*, float** %ell, align 4
  %43 = load i32, i32* %lskip2, align 4
  %add22 = add nsw i32 1, %43
  %arrayidx23 = getelementptr inbounds float, float* %42, i32 %add22
  %44 = load float, float* %arrayidx23, align 4
  store float %44, float* %p3, align 4
  %45 = load float*, float** %ell, align 4
  %46 = load i32, i32* %lskip3, align 4
  %add24 = add nsw i32 1, %46
  %arrayidx25 = getelementptr inbounds float, float* %45, i32 %add24
  %47 = load float, float* %arrayidx25, align 4
  store float %47, float* %p4, align 4
  %48 = load float, float* %p1, align 4
  %49 = load float, float* %q1, align 4
  %mul26 = fmul float %48, %49
  %50 = load float, float* %Z11, align 4
  %add27 = fadd float %50, %mul26
  store float %add27, float* %Z11, align 4
  %51 = load float, float* %p2, align 4
  %52 = load float, float* %q1, align 4
  %mul28 = fmul float %51, %52
  %53 = load float, float* %Z21, align 4
  %add29 = fadd float %53, %mul28
  store float %add29, float* %Z21, align 4
  %54 = load float, float* %p3, align 4
  %55 = load float, float* %q1, align 4
  %mul30 = fmul float %54, %55
  %56 = load float, float* %Z31, align 4
  %add31 = fadd float %56, %mul30
  store float %add31, float* %Z31, align 4
  %57 = load float, float* %p4, align 4
  %58 = load float, float* %q1, align 4
  %mul32 = fmul float %57, %58
  %59 = load float, float* %Z41, align 4
  %add33 = fadd float %59, %mul32
  store float %add33, float* %Z41, align 4
  %60 = load float*, float** %ell, align 4
  %arrayidx34 = getelementptr inbounds float, float* %60, i32 2
  %61 = load float, float* %arrayidx34, align 4
  store float %61, float* %p1, align 4
  %62 = load float*, float** %ex, align 4
  %arrayidx35 = getelementptr inbounds float, float* %62, i32 2
  %63 = load float, float* %arrayidx35, align 4
  store float %63, float* %q1, align 4
  %64 = load float*, float** %ell, align 4
  %65 = load i32, i32* %lskip1.addr, align 4
  %add36 = add nsw i32 2, %65
  %arrayidx37 = getelementptr inbounds float, float* %64, i32 %add36
  %66 = load float, float* %arrayidx37, align 4
  store float %66, float* %p2, align 4
  %67 = load float*, float** %ell, align 4
  %68 = load i32, i32* %lskip2, align 4
  %add38 = add nsw i32 2, %68
  %arrayidx39 = getelementptr inbounds float, float* %67, i32 %add38
  %69 = load float, float* %arrayidx39, align 4
  store float %69, float* %p3, align 4
  %70 = load float*, float** %ell, align 4
  %71 = load i32, i32* %lskip3, align 4
  %add40 = add nsw i32 2, %71
  %arrayidx41 = getelementptr inbounds float, float* %70, i32 %add40
  %72 = load float, float* %arrayidx41, align 4
  store float %72, float* %p4, align 4
  %73 = load float, float* %p1, align 4
  %74 = load float, float* %q1, align 4
  %mul42 = fmul float %73, %74
  %75 = load float, float* %Z11, align 4
  %add43 = fadd float %75, %mul42
  store float %add43, float* %Z11, align 4
  %76 = load float, float* %p2, align 4
  %77 = load float, float* %q1, align 4
  %mul44 = fmul float %76, %77
  %78 = load float, float* %Z21, align 4
  %add45 = fadd float %78, %mul44
  store float %add45, float* %Z21, align 4
  %79 = load float, float* %p3, align 4
  %80 = load float, float* %q1, align 4
  %mul46 = fmul float %79, %80
  %81 = load float, float* %Z31, align 4
  %add47 = fadd float %81, %mul46
  store float %add47, float* %Z31, align 4
  %82 = load float, float* %p4, align 4
  %83 = load float, float* %q1, align 4
  %mul48 = fmul float %82, %83
  %84 = load float, float* %Z41, align 4
  %add49 = fadd float %84, %mul48
  store float %add49, float* %Z41, align 4
  %85 = load float*, float** %ell, align 4
  %arrayidx50 = getelementptr inbounds float, float* %85, i32 3
  %86 = load float, float* %arrayidx50, align 4
  store float %86, float* %p1, align 4
  %87 = load float*, float** %ex, align 4
  %arrayidx51 = getelementptr inbounds float, float* %87, i32 3
  %88 = load float, float* %arrayidx51, align 4
  store float %88, float* %q1, align 4
  %89 = load float*, float** %ell, align 4
  %90 = load i32, i32* %lskip1.addr, align 4
  %add52 = add nsw i32 3, %90
  %arrayidx53 = getelementptr inbounds float, float* %89, i32 %add52
  %91 = load float, float* %arrayidx53, align 4
  store float %91, float* %p2, align 4
  %92 = load float*, float** %ell, align 4
  %93 = load i32, i32* %lskip2, align 4
  %add54 = add nsw i32 3, %93
  %arrayidx55 = getelementptr inbounds float, float* %92, i32 %add54
  %94 = load float, float* %arrayidx55, align 4
  store float %94, float* %p3, align 4
  %95 = load float*, float** %ell, align 4
  %96 = load i32, i32* %lskip3, align 4
  %add56 = add nsw i32 3, %96
  %arrayidx57 = getelementptr inbounds float, float* %95, i32 %add56
  %97 = load float, float* %arrayidx57, align 4
  store float %97, float* %p4, align 4
  %98 = load float, float* %p1, align 4
  %99 = load float, float* %q1, align 4
  %mul58 = fmul float %98, %99
  %100 = load float, float* %Z11, align 4
  %add59 = fadd float %100, %mul58
  store float %add59, float* %Z11, align 4
  %101 = load float, float* %p2, align 4
  %102 = load float, float* %q1, align 4
  %mul60 = fmul float %101, %102
  %103 = load float, float* %Z21, align 4
  %add61 = fadd float %103, %mul60
  store float %add61, float* %Z21, align 4
  %104 = load float, float* %p3, align 4
  %105 = load float, float* %q1, align 4
  %mul62 = fmul float %104, %105
  %106 = load float, float* %Z31, align 4
  %add63 = fadd float %106, %mul62
  store float %add63, float* %Z31, align 4
  %107 = load float, float* %p4, align 4
  %108 = load float, float* %q1, align 4
  %mul64 = fmul float %107, %108
  %109 = load float, float* %Z41, align 4
  %add65 = fadd float %109, %mul64
  store float %add65, float* %Z41, align 4
  %110 = load float*, float** %ell, align 4
  %arrayidx66 = getelementptr inbounds float, float* %110, i32 4
  %111 = load float, float* %arrayidx66, align 4
  store float %111, float* %p1, align 4
  %112 = load float*, float** %ex, align 4
  %arrayidx67 = getelementptr inbounds float, float* %112, i32 4
  %113 = load float, float* %arrayidx67, align 4
  store float %113, float* %q1, align 4
  %114 = load float*, float** %ell, align 4
  %115 = load i32, i32* %lskip1.addr, align 4
  %add68 = add nsw i32 4, %115
  %arrayidx69 = getelementptr inbounds float, float* %114, i32 %add68
  %116 = load float, float* %arrayidx69, align 4
  store float %116, float* %p2, align 4
  %117 = load float*, float** %ell, align 4
  %118 = load i32, i32* %lskip2, align 4
  %add70 = add nsw i32 4, %118
  %arrayidx71 = getelementptr inbounds float, float* %117, i32 %add70
  %119 = load float, float* %arrayidx71, align 4
  store float %119, float* %p3, align 4
  %120 = load float*, float** %ell, align 4
  %121 = load i32, i32* %lskip3, align 4
  %add72 = add nsw i32 4, %121
  %arrayidx73 = getelementptr inbounds float, float* %120, i32 %add72
  %122 = load float, float* %arrayidx73, align 4
  store float %122, float* %p4, align 4
  %123 = load float, float* %p1, align 4
  %124 = load float, float* %q1, align 4
  %mul74 = fmul float %123, %124
  %125 = load float, float* %Z11, align 4
  %add75 = fadd float %125, %mul74
  store float %add75, float* %Z11, align 4
  %126 = load float, float* %p2, align 4
  %127 = load float, float* %q1, align 4
  %mul76 = fmul float %126, %127
  %128 = load float, float* %Z21, align 4
  %add77 = fadd float %128, %mul76
  store float %add77, float* %Z21, align 4
  %129 = load float, float* %p3, align 4
  %130 = load float, float* %q1, align 4
  %mul78 = fmul float %129, %130
  %131 = load float, float* %Z31, align 4
  %add79 = fadd float %131, %mul78
  store float %add79, float* %Z31, align 4
  %132 = load float, float* %p4, align 4
  %133 = load float, float* %q1, align 4
  %mul80 = fmul float %132, %133
  %134 = load float, float* %Z41, align 4
  %add81 = fadd float %134, %mul80
  store float %add81, float* %Z41, align 4
  %135 = load float*, float** %ell, align 4
  %arrayidx82 = getelementptr inbounds float, float* %135, i32 5
  %136 = load float, float* %arrayidx82, align 4
  store float %136, float* %p1, align 4
  %137 = load float*, float** %ex, align 4
  %arrayidx83 = getelementptr inbounds float, float* %137, i32 5
  %138 = load float, float* %arrayidx83, align 4
  store float %138, float* %q1, align 4
  %139 = load float*, float** %ell, align 4
  %140 = load i32, i32* %lskip1.addr, align 4
  %add84 = add nsw i32 5, %140
  %arrayidx85 = getelementptr inbounds float, float* %139, i32 %add84
  %141 = load float, float* %arrayidx85, align 4
  store float %141, float* %p2, align 4
  %142 = load float*, float** %ell, align 4
  %143 = load i32, i32* %lskip2, align 4
  %add86 = add nsw i32 5, %143
  %arrayidx87 = getelementptr inbounds float, float* %142, i32 %add86
  %144 = load float, float* %arrayidx87, align 4
  store float %144, float* %p3, align 4
  %145 = load float*, float** %ell, align 4
  %146 = load i32, i32* %lskip3, align 4
  %add88 = add nsw i32 5, %146
  %arrayidx89 = getelementptr inbounds float, float* %145, i32 %add88
  %147 = load float, float* %arrayidx89, align 4
  store float %147, float* %p4, align 4
  %148 = load float, float* %p1, align 4
  %149 = load float, float* %q1, align 4
  %mul90 = fmul float %148, %149
  %150 = load float, float* %Z11, align 4
  %add91 = fadd float %150, %mul90
  store float %add91, float* %Z11, align 4
  %151 = load float, float* %p2, align 4
  %152 = load float, float* %q1, align 4
  %mul92 = fmul float %151, %152
  %153 = load float, float* %Z21, align 4
  %add93 = fadd float %153, %mul92
  store float %add93, float* %Z21, align 4
  %154 = load float, float* %p3, align 4
  %155 = load float, float* %q1, align 4
  %mul94 = fmul float %154, %155
  %156 = load float, float* %Z31, align 4
  %add95 = fadd float %156, %mul94
  store float %add95, float* %Z31, align 4
  %157 = load float, float* %p4, align 4
  %158 = load float, float* %q1, align 4
  %mul96 = fmul float %157, %158
  %159 = load float, float* %Z41, align 4
  %add97 = fadd float %159, %mul96
  store float %add97, float* %Z41, align 4
  %160 = load float*, float** %ell, align 4
  %arrayidx98 = getelementptr inbounds float, float* %160, i32 6
  %161 = load float, float* %arrayidx98, align 4
  store float %161, float* %p1, align 4
  %162 = load float*, float** %ex, align 4
  %arrayidx99 = getelementptr inbounds float, float* %162, i32 6
  %163 = load float, float* %arrayidx99, align 4
  store float %163, float* %q1, align 4
  %164 = load float*, float** %ell, align 4
  %165 = load i32, i32* %lskip1.addr, align 4
  %add100 = add nsw i32 6, %165
  %arrayidx101 = getelementptr inbounds float, float* %164, i32 %add100
  %166 = load float, float* %arrayidx101, align 4
  store float %166, float* %p2, align 4
  %167 = load float*, float** %ell, align 4
  %168 = load i32, i32* %lskip2, align 4
  %add102 = add nsw i32 6, %168
  %arrayidx103 = getelementptr inbounds float, float* %167, i32 %add102
  %169 = load float, float* %arrayidx103, align 4
  store float %169, float* %p3, align 4
  %170 = load float*, float** %ell, align 4
  %171 = load i32, i32* %lskip3, align 4
  %add104 = add nsw i32 6, %171
  %arrayidx105 = getelementptr inbounds float, float* %170, i32 %add104
  %172 = load float, float* %arrayidx105, align 4
  store float %172, float* %p4, align 4
  %173 = load float, float* %p1, align 4
  %174 = load float, float* %q1, align 4
  %mul106 = fmul float %173, %174
  %175 = load float, float* %Z11, align 4
  %add107 = fadd float %175, %mul106
  store float %add107, float* %Z11, align 4
  %176 = load float, float* %p2, align 4
  %177 = load float, float* %q1, align 4
  %mul108 = fmul float %176, %177
  %178 = load float, float* %Z21, align 4
  %add109 = fadd float %178, %mul108
  store float %add109, float* %Z21, align 4
  %179 = load float, float* %p3, align 4
  %180 = load float, float* %q1, align 4
  %mul110 = fmul float %179, %180
  %181 = load float, float* %Z31, align 4
  %add111 = fadd float %181, %mul110
  store float %add111, float* %Z31, align 4
  %182 = load float, float* %p4, align 4
  %183 = load float, float* %q1, align 4
  %mul112 = fmul float %182, %183
  %184 = load float, float* %Z41, align 4
  %add113 = fadd float %184, %mul112
  store float %add113, float* %Z41, align 4
  %185 = load float*, float** %ell, align 4
  %arrayidx114 = getelementptr inbounds float, float* %185, i32 7
  %186 = load float, float* %arrayidx114, align 4
  store float %186, float* %p1, align 4
  %187 = load float*, float** %ex, align 4
  %arrayidx115 = getelementptr inbounds float, float* %187, i32 7
  %188 = load float, float* %arrayidx115, align 4
  store float %188, float* %q1, align 4
  %189 = load float*, float** %ell, align 4
  %190 = load i32, i32* %lskip1.addr, align 4
  %add116 = add nsw i32 7, %190
  %arrayidx117 = getelementptr inbounds float, float* %189, i32 %add116
  %191 = load float, float* %arrayidx117, align 4
  store float %191, float* %p2, align 4
  %192 = load float*, float** %ell, align 4
  %193 = load i32, i32* %lskip2, align 4
  %add118 = add nsw i32 7, %193
  %arrayidx119 = getelementptr inbounds float, float* %192, i32 %add118
  %194 = load float, float* %arrayidx119, align 4
  store float %194, float* %p3, align 4
  %195 = load float*, float** %ell, align 4
  %196 = load i32, i32* %lskip3, align 4
  %add120 = add nsw i32 7, %196
  %arrayidx121 = getelementptr inbounds float, float* %195, i32 %add120
  %197 = load float, float* %arrayidx121, align 4
  store float %197, float* %p4, align 4
  %198 = load float, float* %p1, align 4
  %199 = load float, float* %q1, align 4
  %mul122 = fmul float %198, %199
  %200 = load float, float* %Z11, align 4
  %add123 = fadd float %200, %mul122
  store float %add123, float* %Z11, align 4
  %201 = load float, float* %p2, align 4
  %202 = load float, float* %q1, align 4
  %mul124 = fmul float %201, %202
  %203 = load float, float* %Z21, align 4
  %add125 = fadd float %203, %mul124
  store float %add125, float* %Z21, align 4
  %204 = load float, float* %p3, align 4
  %205 = load float, float* %q1, align 4
  %mul126 = fmul float %204, %205
  %206 = load float, float* %Z31, align 4
  %add127 = fadd float %206, %mul126
  store float %add127, float* %Z31, align 4
  %207 = load float, float* %p4, align 4
  %208 = load float, float* %q1, align 4
  %mul128 = fmul float %207, %208
  %209 = load float, float* %Z41, align 4
  %add129 = fadd float %209, %mul128
  store float %add129, float* %Z41, align 4
  %210 = load float*, float** %ell, align 4
  %arrayidx130 = getelementptr inbounds float, float* %210, i32 8
  %211 = load float, float* %arrayidx130, align 4
  store float %211, float* %p1, align 4
  %212 = load float*, float** %ex, align 4
  %arrayidx131 = getelementptr inbounds float, float* %212, i32 8
  %213 = load float, float* %arrayidx131, align 4
  store float %213, float* %q1, align 4
  %214 = load float*, float** %ell, align 4
  %215 = load i32, i32* %lskip1.addr, align 4
  %add132 = add nsw i32 8, %215
  %arrayidx133 = getelementptr inbounds float, float* %214, i32 %add132
  %216 = load float, float* %arrayidx133, align 4
  store float %216, float* %p2, align 4
  %217 = load float*, float** %ell, align 4
  %218 = load i32, i32* %lskip2, align 4
  %add134 = add nsw i32 8, %218
  %arrayidx135 = getelementptr inbounds float, float* %217, i32 %add134
  %219 = load float, float* %arrayidx135, align 4
  store float %219, float* %p3, align 4
  %220 = load float*, float** %ell, align 4
  %221 = load i32, i32* %lskip3, align 4
  %add136 = add nsw i32 8, %221
  %arrayidx137 = getelementptr inbounds float, float* %220, i32 %add136
  %222 = load float, float* %arrayidx137, align 4
  store float %222, float* %p4, align 4
  %223 = load float, float* %p1, align 4
  %224 = load float, float* %q1, align 4
  %mul138 = fmul float %223, %224
  %225 = load float, float* %Z11, align 4
  %add139 = fadd float %225, %mul138
  store float %add139, float* %Z11, align 4
  %226 = load float, float* %p2, align 4
  %227 = load float, float* %q1, align 4
  %mul140 = fmul float %226, %227
  %228 = load float, float* %Z21, align 4
  %add141 = fadd float %228, %mul140
  store float %add141, float* %Z21, align 4
  %229 = load float, float* %p3, align 4
  %230 = load float, float* %q1, align 4
  %mul142 = fmul float %229, %230
  %231 = load float, float* %Z31, align 4
  %add143 = fadd float %231, %mul142
  store float %add143, float* %Z31, align 4
  %232 = load float, float* %p4, align 4
  %233 = load float, float* %q1, align 4
  %mul144 = fmul float %232, %233
  %234 = load float, float* %Z41, align 4
  %add145 = fadd float %234, %mul144
  store float %add145, float* %Z41, align 4
  %235 = load float*, float** %ell, align 4
  %arrayidx146 = getelementptr inbounds float, float* %235, i32 9
  %236 = load float, float* %arrayidx146, align 4
  store float %236, float* %p1, align 4
  %237 = load float*, float** %ex, align 4
  %arrayidx147 = getelementptr inbounds float, float* %237, i32 9
  %238 = load float, float* %arrayidx147, align 4
  store float %238, float* %q1, align 4
  %239 = load float*, float** %ell, align 4
  %240 = load i32, i32* %lskip1.addr, align 4
  %add148 = add nsw i32 9, %240
  %arrayidx149 = getelementptr inbounds float, float* %239, i32 %add148
  %241 = load float, float* %arrayidx149, align 4
  store float %241, float* %p2, align 4
  %242 = load float*, float** %ell, align 4
  %243 = load i32, i32* %lskip2, align 4
  %add150 = add nsw i32 9, %243
  %arrayidx151 = getelementptr inbounds float, float* %242, i32 %add150
  %244 = load float, float* %arrayidx151, align 4
  store float %244, float* %p3, align 4
  %245 = load float*, float** %ell, align 4
  %246 = load i32, i32* %lskip3, align 4
  %add152 = add nsw i32 9, %246
  %arrayidx153 = getelementptr inbounds float, float* %245, i32 %add152
  %247 = load float, float* %arrayidx153, align 4
  store float %247, float* %p4, align 4
  %248 = load float, float* %p1, align 4
  %249 = load float, float* %q1, align 4
  %mul154 = fmul float %248, %249
  %250 = load float, float* %Z11, align 4
  %add155 = fadd float %250, %mul154
  store float %add155, float* %Z11, align 4
  %251 = load float, float* %p2, align 4
  %252 = load float, float* %q1, align 4
  %mul156 = fmul float %251, %252
  %253 = load float, float* %Z21, align 4
  %add157 = fadd float %253, %mul156
  store float %add157, float* %Z21, align 4
  %254 = load float, float* %p3, align 4
  %255 = load float, float* %q1, align 4
  %mul158 = fmul float %254, %255
  %256 = load float, float* %Z31, align 4
  %add159 = fadd float %256, %mul158
  store float %add159, float* %Z31, align 4
  %257 = load float, float* %p4, align 4
  %258 = load float, float* %q1, align 4
  %mul160 = fmul float %257, %258
  %259 = load float, float* %Z41, align 4
  %add161 = fadd float %259, %mul160
  store float %add161, float* %Z41, align 4
  %260 = load float*, float** %ell, align 4
  %arrayidx162 = getelementptr inbounds float, float* %260, i32 10
  %261 = load float, float* %arrayidx162, align 4
  store float %261, float* %p1, align 4
  %262 = load float*, float** %ex, align 4
  %arrayidx163 = getelementptr inbounds float, float* %262, i32 10
  %263 = load float, float* %arrayidx163, align 4
  store float %263, float* %q1, align 4
  %264 = load float*, float** %ell, align 4
  %265 = load i32, i32* %lskip1.addr, align 4
  %add164 = add nsw i32 10, %265
  %arrayidx165 = getelementptr inbounds float, float* %264, i32 %add164
  %266 = load float, float* %arrayidx165, align 4
  store float %266, float* %p2, align 4
  %267 = load float*, float** %ell, align 4
  %268 = load i32, i32* %lskip2, align 4
  %add166 = add nsw i32 10, %268
  %arrayidx167 = getelementptr inbounds float, float* %267, i32 %add166
  %269 = load float, float* %arrayidx167, align 4
  store float %269, float* %p3, align 4
  %270 = load float*, float** %ell, align 4
  %271 = load i32, i32* %lskip3, align 4
  %add168 = add nsw i32 10, %271
  %arrayidx169 = getelementptr inbounds float, float* %270, i32 %add168
  %272 = load float, float* %arrayidx169, align 4
  store float %272, float* %p4, align 4
  %273 = load float, float* %p1, align 4
  %274 = load float, float* %q1, align 4
  %mul170 = fmul float %273, %274
  %275 = load float, float* %Z11, align 4
  %add171 = fadd float %275, %mul170
  store float %add171, float* %Z11, align 4
  %276 = load float, float* %p2, align 4
  %277 = load float, float* %q1, align 4
  %mul172 = fmul float %276, %277
  %278 = load float, float* %Z21, align 4
  %add173 = fadd float %278, %mul172
  store float %add173, float* %Z21, align 4
  %279 = load float, float* %p3, align 4
  %280 = load float, float* %q1, align 4
  %mul174 = fmul float %279, %280
  %281 = load float, float* %Z31, align 4
  %add175 = fadd float %281, %mul174
  store float %add175, float* %Z31, align 4
  %282 = load float, float* %p4, align 4
  %283 = load float, float* %q1, align 4
  %mul176 = fmul float %282, %283
  %284 = load float, float* %Z41, align 4
  %add177 = fadd float %284, %mul176
  store float %add177, float* %Z41, align 4
  %285 = load float*, float** %ell, align 4
  %arrayidx178 = getelementptr inbounds float, float* %285, i32 11
  %286 = load float, float* %arrayidx178, align 4
  store float %286, float* %p1, align 4
  %287 = load float*, float** %ex, align 4
  %arrayidx179 = getelementptr inbounds float, float* %287, i32 11
  %288 = load float, float* %arrayidx179, align 4
  store float %288, float* %q1, align 4
  %289 = load float*, float** %ell, align 4
  %290 = load i32, i32* %lskip1.addr, align 4
  %add180 = add nsw i32 11, %290
  %arrayidx181 = getelementptr inbounds float, float* %289, i32 %add180
  %291 = load float, float* %arrayidx181, align 4
  store float %291, float* %p2, align 4
  %292 = load float*, float** %ell, align 4
  %293 = load i32, i32* %lskip2, align 4
  %add182 = add nsw i32 11, %293
  %arrayidx183 = getelementptr inbounds float, float* %292, i32 %add182
  %294 = load float, float* %arrayidx183, align 4
  store float %294, float* %p3, align 4
  %295 = load float*, float** %ell, align 4
  %296 = load i32, i32* %lskip3, align 4
  %add184 = add nsw i32 11, %296
  %arrayidx185 = getelementptr inbounds float, float* %295, i32 %add184
  %297 = load float, float* %arrayidx185, align 4
  store float %297, float* %p4, align 4
  %298 = load float, float* %p1, align 4
  %299 = load float, float* %q1, align 4
  %mul186 = fmul float %298, %299
  %300 = load float, float* %Z11, align 4
  %add187 = fadd float %300, %mul186
  store float %add187, float* %Z11, align 4
  %301 = load float, float* %p2, align 4
  %302 = load float, float* %q1, align 4
  %mul188 = fmul float %301, %302
  %303 = load float, float* %Z21, align 4
  %add189 = fadd float %303, %mul188
  store float %add189, float* %Z21, align 4
  %304 = load float, float* %p3, align 4
  %305 = load float, float* %q1, align 4
  %mul190 = fmul float %304, %305
  %306 = load float, float* %Z31, align 4
  %add191 = fadd float %306, %mul190
  store float %add191, float* %Z31, align 4
  %307 = load float, float* %p4, align 4
  %308 = load float, float* %q1, align 4
  %mul192 = fmul float %307, %308
  %309 = load float, float* %Z41, align 4
  %add193 = fadd float %309, %mul192
  store float %add193, float* %Z41, align 4
  %310 = load float*, float** %ell, align 4
  %add.ptr194 = getelementptr inbounds float, float* %310, i32 12
  store float* %add.ptr194, float** %ell, align 4
  %311 = load float*, float** %ex, align 4
  %add.ptr195 = getelementptr inbounds float, float* %311, i32 12
  store float* %add.ptr195, float** %ex, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body6
  %312 = load i32, i32* %j, align 4
  %sub196 = sub nsw i32 %312, 12
  store i32 %sub196, i32* %j, align 4
  br label %for.cond4

for.end:                                          ; preds = %for.cond4
  %313 = load i32, i32* %j, align 4
  %add197 = add nsw i32 %313, 12
  store i32 %add197, i32* %j, align 4
  br label %for.cond198

for.cond198:                                      ; preds = %for.inc216, %for.end
  %314 = load i32, i32* %j, align 4
  %cmp199 = icmp sgt i32 %314, 0
  br i1 %cmp199, label %for.body200, label %for.end217

for.body200:                                      ; preds = %for.cond198
  %315 = load float*, float** %ell, align 4
  %arrayidx201 = getelementptr inbounds float, float* %315, i32 0
  %316 = load float, float* %arrayidx201, align 4
  store float %316, float* %p1, align 4
  %317 = load float*, float** %ex, align 4
  %arrayidx202 = getelementptr inbounds float, float* %317, i32 0
  %318 = load float, float* %arrayidx202, align 4
  store float %318, float* %q1, align 4
  %319 = load float*, float** %ell, align 4
  %320 = load i32, i32* %lskip1.addr, align 4
  %arrayidx203 = getelementptr inbounds float, float* %319, i32 %320
  %321 = load float, float* %arrayidx203, align 4
  store float %321, float* %p2, align 4
  %322 = load float*, float** %ell, align 4
  %323 = load i32, i32* %lskip2, align 4
  %arrayidx204 = getelementptr inbounds float, float* %322, i32 %323
  %324 = load float, float* %arrayidx204, align 4
  store float %324, float* %p3, align 4
  %325 = load float*, float** %ell, align 4
  %326 = load i32, i32* %lskip3, align 4
  %arrayidx205 = getelementptr inbounds float, float* %325, i32 %326
  %327 = load float, float* %arrayidx205, align 4
  store float %327, float* %p4, align 4
  %328 = load float, float* %p1, align 4
  %329 = load float, float* %q1, align 4
  %mul206 = fmul float %328, %329
  %330 = load float, float* %Z11, align 4
  %add207 = fadd float %330, %mul206
  store float %add207, float* %Z11, align 4
  %331 = load float, float* %p2, align 4
  %332 = load float, float* %q1, align 4
  %mul208 = fmul float %331, %332
  %333 = load float, float* %Z21, align 4
  %add209 = fadd float %333, %mul208
  store float %add209, float* %Z21, align 4
  %334 = load float, float* %p3, align 4
  %335 = load float, float* %q1, align 4
  %mul210 = fmul float %334, %335
  %336 = load float, float* %Z31, align 4
  %add211 = fadd float %336, %mul210
  store float %add211, float* %Z31, align 4
  %337 = load float, float* %p4, align 4
  %338 = load float, float* %q1, align 4
  %mul212 = fmul float %337, %338
  %339 = load float, float* %Z41, align 4
  %add213 = fadd float %339, %mul212
  store float %add213, float* %Z41, align 4
  %340 = load float*, float** %ell, align 4
  %add.ptr214 = getelementptr inbounds float, float* %340, i32 1
  store float* %add.ptr214, float** %ell, align 4
  %341 = load float*, float** %ex, align 4
  %add.ptr215 = getelementptr inbounds float, float* %341, i32 1
  store float* %add.ptr215, float** %ex, align 4
  br label %for.inc216

for.inc216:                                       ; preds = %for.body200
  %342 = load i32, i32* %j, align 4
  %dec = add nsw i32 %342, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond198

for.end217:                                       ; preds = %for.cond198
  %343 = load float*, float** %ex, align 4
  %arrayidx218 = getelementptr inbounds float, float* %343, i32 0
  %344 = load float, float* %arrayidx218, align 4
  %345 = load float, float* %Z11, align 4
  %sub219 = fsub float %344, %345
  store float %sub219, float* %Z11, align 4
  %346 = load float, float* %Z11, align 4
  %347 = load float*, float** %ex, align 4
  %arrayidx220 = getelementptr inbounds float, float* %347, i32 0
  store float %346, float* %arrayidx220, align 4
  %348 = load float*, float** %ell, align 4
  %349 = load i32, i32* %lskip1.addr, align 4
  %arrayidx221 = getelementptr inbounds float, float* %348, i32 %349
  %350 = load float, float* %arrayidx221, align 4
  store float %350, float* %p1, align 4
  %351 = load float*, float** %ex, align 4
  %arrayidx222 = getelementptr inbounds float, float* %351, i32 1
  %352 = load float, float* %arrayidx222, align 4
  %353 = load float, float* %Z21, align 4
  %sub223 = fsub float %352, %353
  %354 = load float, float* %p1, align 4
  %355 = load float, float* %Z11, align 4
  %mul224 = fmul float %354, %355
  %sub225 = fsub float %sub223, %mul224
  store float %sub225, float* %Z21, align 4
  %356 = load float, float* %Z21, align 4
  %357 = load float*, float** %ex, align 4
  %arrayidx226 = getelementptr inbounds float, float* %357, i32 1
  store float %356, float* %arrayidx226, align 4
  %358 = load float*, float** %ell, align 4
  %359 = load i32, i32* %lskip2, align 4
  %arrayidx227 = getelementptr inbounds float, float* %358, i32 %359
  %360 = load float, float* %arrayidx227, align 4
  store float %360, float* %p1, align 4
  %361 = load float*, float** %ell, align 4
  %362 = load i32, i32* %lskip2, align 4
  %add228 = add nsw i32 1, %362
  %arrayidx229 = getelementptr inbounds float, float* %361, i32 %add228
  %363 = load float, float* %arrayidx229, align 4
  store float %363, float* %p2, align 4
  %364 = load float*, float** %ex, align 4
  %arrayidx230 = getelementptr inbounds float, float* %364, i32 2
  %365 = load float, float* %arrayidx230, align 4
  %366 = load float, float* %Z31, align 4
  %sub231 = fsub float %365, %366
  %367 = load float, float* %p1, align 4
  %368 = load float, float* %Z11, align 4
  %mul232 = fmul float %367, %368
  %sub233 = fsub float %sub231, %mul232
  %369 = load float, float* %p2, align 4
  %370 = load float, float* %Z21, align 4
  %mul234 = fmul float %369, %370
  %sub235 = fsub float %sub233, %mul234
  store float %sub235, float* %Z31, align 4
  %371 = load float, float* %Z31, align 4
  %372 = load float*, float** %ex, align 4
  %arrayidx236 = getelementptr inbounds float, float* %372, i32 2
  store float %371, float* %arrayidx236, align 4
  %373 = load float*, float** %ell, align 4
  %374 = load i32, i32* %lskip3, align 4
  %arrayidx237 = getelementptr inbounds float, float* %373, i32 %374
  %375 = load float, float* %arrayidx237, align 4
  store float %375, float* %p1, align 4
  %376 = load float*, float** %ell, align 4
  %377 = load i32, i32* %lskip3, align 4
  %add238 = add nsw i32 1, %377
  %arrayidx239 = getelementptr inbounds float, float* %376, i32 %add238
  %378 = load float, float* %arrayidx239, align 4
  store float %378, float* %p2, align 4
  %379 = load float*, float** %ell, align 4
  %380 = load i32, i32* %lskip3, align 4
  %add240 = add nsw i32 2, %380
  %arrayidx241 = getelementptr inbounds float, float* %379, i32 %add240
  %381 = load float, float* %arrayidx241, align 4
  store float %381, float* %p3, align 4
  %382 = load float*, float** %ex, align 4
  %arrayidx242 = getelementptr inbounds float, float* %382, i32 3
  %383 = load float, float* %arrayidx242, align 4
  %384 = load float, float* %Z41, align 4
  %sub243 = fsub float %383, %384
  %385 = load float, float* %p1, align 4
  %386 = load float, float* %Z11, align 4
  %mul244 = fmul float %385, %386
  %sub245 = fsub float %sub243, %mul244
  %387 = load float, float* %p2, align 4
  %388 = load float, float* %Z21, align 4
  %mul246 = fmul float %387, %388
  %sub247 = fsub float %sub245, %mul246
  %389 = load float, float* %p3, align 4
  %390 = load float, float* %Z31, align 4
  %mul248 = fmul float %389, %390
  %sub249 = fsub float %sub247, %mul248
  store float %sub249, float* %Z41, align 4
  %391 = load float, float* %Z41, align 4
  %392 = load float*, float** %ex, align 4
  %arrayidx250 = getelementptr inbounds float, float* %392, i32 3
  store float %391, float* %arrayidx250, align 4
  br label %for.inc251

for.inc251:                                       ; preds = %for.end217
  %393 = load i32, i32* %i, align 4
  %add252 = add nsw i32 %393, 4
  store i32 %add252, i32* %i, align 4
  br label %for.cond

for.end253:                                       ; preds = %for.cond
  br label %for.cond254

for.cond254:                                      ; preds = %for.inc332, %for.end253
  %394 = load i32, i32* %i, align 4
  %395 = load i32, i32* %n.addr, align 4
  %cmp255 = icmp slt i32 %394, %395
  br i1 %cmp255, label %for.body256, label %for.end333

for.body256:                                      ; preds = %for.cond254
  store float 0.000000e+00, float* %Z11, align 4
  %396 = load float*, float** %L.addr, align 4
  %397 = load i32, i32* %i, align 4
  %398 = load i32, i32* %lskip1.addr, align 4
  %mul257 = mul nsw i32 %397, %398
  %add.ptr258 = getelementptr inbounds float, float* %396, i32 %mul257
  store float* %add.ptr258, float** %ell, align 4
  %399 = load float*, float** %B.addr, align 4
  store float* %399, float** %ex, align 4
  %400 = load i32, i32* %i, align 4
  %sub259 = sub nsw i32 %400, 12
  store i32 %sub259, i32* %j, align 4
  br label %for.cond260

for.cond260:                                      ; preds = %for.inc313, %for.body256
  %401 = load i32, i32* %j, align 4
  %cmp261 = icmp sge i32 %401, 0
  br i1 %cmp261, label %for.body262, label %for.end315

for.body262:                                      ; preds = %for.cond260
  %402 = load float*, float** %ell, align 4
  %arrayidx263 = getelementptr inbounds float, float* %402, i32 0
  %403 = load float, float* %arrayidx263, align 4
  store float %403, float* %p1, align 4
  %404 = load float*, float** %ex, align 4
  %arrayidx264 = getelementptr inbounds float, float* %404, i32 0
  %405 = load float, float* %arrayidx264, align 4
  store float %405, float* %q1, align 4
  %406 = load float, float* %p1, align 4
  %407 = load float, float* %q1, align 4
  %mul265 = fmul float %406, %407
  %408 = load float, float* %Z11, align 4
  %add266 = fadd float %408, %mul265
  store float %add266, float* %Z11, align 4
  %409 = load float*, float** %ell, align 4
  %arrayidx267 = getelementptr inbounds float, float* %409, i32 1
  %410 = load float, float* %arrayidx267, align 4
  store float %410, float* %p1, align 4
  %411 = load float*, float** %ex, align 4
  %arrayidx268 = getelementptr inbounds float, float* %411, i32 1
  %412 = load float, float* %arrayidx268, align 4
  store float %412, float* %q1, align 4
  %413 = load float, float* %p1, align 4
  %414 = load float, float* %q1, align 4
  %mul269 = fmul float %413, %414
  %415 = load float, float* %Z11, align 4
  %add270 = fadd float %415, %mul269
  store float %add270, float* %Z11, align 4
  %416 = load float*, float** %ell, align 4
  %arrayidx271 = getelementptr inbounds float, float* %416, i32 2
  %417 = load float, float* %arrayidx271, align 4
  store float %417, float* %p1, align 4
  %418 = load float*, float** %ex, align 4
  %arrayidx272 = getelementptr inbounds float, float* %418, i32 2
  %419 = load float, float* %arrayidx272, align 4
  store float %419, float* %q1, align 4
  %420 = load float, float* %p1, align 4
  %421 = load float, float* %q1, align 4
  %mul273 = fmul float %420, %421
  %422 = load float, float* %Z11, align 4
  %add274 = fadd float %422, %mul273
  store float %add274, float* %Z11, align 4
  %423 = load float*, float** %ell, align 4
  %arrayidx275 = getelementptr inbounds float, float* %423, i32 3
  %424 = load float, float* %arrayidx275, align 4
  store float %424, float* %p1, align 4
  %425 = load float*, float** %ex, align 4
  %arrayidx276 = getelementptr inbounds float, float* %425, i32 3
  %426 = load float, float* %arrayidx276, align 4
  store float %426, float* %q1, align 4
  %427 = load float, float* %p1, align 4
  %428 = load float, float* %q1, align 4
  %mul277 = fmul float %427, %428
  %429 = load float, float* %Z11, align 4
  %add278 = fadd float %429, %mul277
  store float %add278, float* %Z11, align 4
  %430 = load float*, float** %ell, align 4
  %arrayidx279 = getelementptr inbounds float, float* %430, i32 4
  %431 = load float, float* %arrayidx279, align 4
  store float %431, float* %p1, align 4
  %432 = load float*, float** %ex, align 4
  %arrayidx280 = getelementptr inbounds float, float* %432, i32 4
  %433 = load float, float* %arrayidx280, align 4
  store float %433, float* %q1, align 4
  %434 = load float, float* %p1, align 4
  %435 = load float, float* %q1, align 4
  %mul281 = fmul float %434, %435
  %436 = load float, float* %Z11, align 4
  %add282 = fadd float %436, %mul281
  store float %add282, float* %Z11, align 4
  %437 = load float*, float** %ell, align 4
  %arrayidx283 = getelementptr inbounds float, float* %437, i32 5
  %438 = load float, float* %arrayidx283, align 4
  store float %438, float* %p1, align 4
  %439 = load float*, float** %ex, align 4
  %arrayidx284 = getelementptr inbounds float, float* %439, i32 5
  %440 = load float, float* %arrayidx284, align 4
  store float %440, float* %q1, align 4
  %441 = load float, float* %p1, align 4
  %442 = load float, float* %q1, align 4
  %mul285 = fmul float %441, %442
  %443 = load float, float* %Z11, align 4
  %add286 = fadd float %443, %mul285
  store float %add286, float* %Z11, align 4
  %444 = load float*, float** %ell, align 4
  %arrayidx287 = getelementptr inbounds float, float* %444, i32 6
  %445 = load float, float* %arrayidx287, align 4
  store float %445, float* %p1, align 4
  %446 = load float*, float** %ex, align 4
  %arrayidx288 = getelementptr inbounds float, float* %446, i32 6
  %447 = load float, float* %arrayidx288, align 4
  store float %447, float* %q1, align 4
  %448 = load float, float* %p1, align 4
  %449 = load float, float* %q1, align 4
  %mul289 = fmul float %448, %449
  %450 = load float, float* %Z11, align 4
  %add290 = fadd float %450, %mul289
  store float %add290, float* %Z11, align 4
  %451 = load float*, float** %ell, align 4
  %arrayidx291 = getelementptr inbounds float, float* %451, i32 7
  %452 = load float, float* %arrayidx291, align 4
  store float %452, float* %p1, align 4
  %453 = load float*, float** %ex, align 4
  %arrayidx292 = getelementptr inbounds float, float* %453, i32 7
  %454 = load float, float* %arrayidx292, align 4
  store float %454, float* %q1, align 4
  %455 = load float, float* %p1, align 4
  %456 = load float, float* %q1, align 4
  %mul293 = fmul float %455, %456
  %457 = load float, float* %Z11, align 4
  %add294 = fadd float %457, %mul293
  store float %add294, float* %Z11, align 4
  %458 = load float*, float** %ell, align 4
  %arrayidx295 = getelementptr inbounds float, float* %458, i32 8
  %459 = load float, float* %arrayidx295, align 4
  store float %459, float* %p1, align 4
  %460 = load float*, float** %ex, align 4
  %arrayidx296 = getelementptr inbounds float, float* %460, i32 8
  %461 = load float, float* %arrayidx296, align 4
  store float %461, float* %q1, align 4
  %462 = load float, float* %p1, align 4
  %463 = load float, float* %q1, align 4
  %mul297 = fmul float %462, %463
  %464 = load float, float* %Z11, align 4
  %add298 = fadd float %464, %mul297
  store float %add298, float* %Z11, align 4
  %465 = load float*, float** %ell, align 4
  %arrayidx299 = getelementptr inbounds float, float* %465, i32 9
  %466 = load float, float* %arrayidx299, align 4
  store float %466, float* %p1, align 4
  %467 = load float*, float** %ex, align 4
  %arrayidx300 = getelementptr inbounds float, float* %467, i32 9
  %468 = load float, float* %arrayidx300, align 4
  store float %468, float* %q1, align 4
  %469 = load float, float* %p1, align 4
  %470 = load float, float* %q1, align 4
  %mul301 = fmul float %469, %470
  %471 = load float, float* %Z11, align 4
  %add302 = fadd float %471, %mul301
  store float %add302, float* %Z11, align 4
  %472 = load float*, float** %ell, align 4
  %arrayidx303 = getelementptr inbounds float, float* %472, i32 10
  %473 = load float, float* %arrayidx303, align 4
  store float %473, float* %p1, align 4
  %474 = load float*, float** %ex, align 4
  %arrayidx304 = getelementptr inbounds float, float* %474, i32 10
  %475 = load float, float* %arrayidx304, align 4
  store float %475, float* %q1, align 4
  %476 = load float, float* %p1, align 4
  %477 = load float, float* %q1, align 4
  %mul305 = fmul float %476, %477
  %478 = load float, float* %Z11, align 4
  %add306 = fadd float %478, %mul305
  store float %add306, float* %Z11, align 4
  %479 = load float*, float** %ell, align 4
  %arrayidx307 = getelementptr inbounds float, float* %479, i32 11
  %480 = load float, float* %arrayidx307, align 4
  store float %480, float* %p1, align 4
  %481 = load float*, float** %ex, align 4
  %arrayidx308 = getelementptr inbounds float, float* %481, i32 11
  %482 = load float, float* %arrayidx308, align 4
  store float %482, float* %q1, align 4
  %483 = load float, float* %p1, align 4
  %484 = load float, float* %q1, align 4
  %mul309 = fmul float %483, %484
  %485 = load float, float* %Z11, align 4
  %add310 = fadd float %485, %mul309
  store float %add310, float* %Z11, align 4
  %486 = load float*, float** %ell, align 4
  %add.ptr311 = getelementptr inbounds float, float* %486, i32 12
  store float* %add.ptr311, float** %ell, align 4
  %487 = load float*, float** %ex, align 4
  %add.ptr312 = getelementptr inbounds float, float* %487, i32 12
  store float* %add.ptr312, float** %ex, align 4
  br label %for.inc313

for.inc313:                                       ; preds = %for.body262
  %488 = load i32, i32* %j, align 4
  %sub314 = sub nsw i32 %488, 12
  store i32 %sub314, i32* %j, align 4
  br label %for.cond260

for.end315:                                       ; preds = %for.cond260
  %489 = load i32, i32* %j, align 4
  %add316 = add nsw i32 %489, 12
  store i32 %add316, i32* %j, align 4
  br label %for.cond317

for.cond317:                                      ; preds = %for.inc326, %for.end315
  %490 = load i32, i32* %j, align 4
  %cmp318 = icmp sgt i32 %490, 0
  br i1 %cmp318, label %for.body319, label %for.end328

for.body319:                                      ; preds = %for.cond317
  %491 = load float*, float** %ell, align 4
  %arrayidx320 = getelementptr inbounds float, float* %491, i32 0
  %492 = load float, float* %arrayidx320, align 4
  store float %492, float* %p1, align 4
  %493 = load float*, float** %ex, align 4
  %arrayidx321 = getelementptr inbounds float, float* %493, i32 0
  %494 = load float, float* %arrayidx321, align 4
  store float %494, float* %q1, align 4
  %495 = load float, float* %p1, align 4
  %496 = load float, float* %q1, align 4
  %mul322 = fmul float %495, %496
  %497 = load float, float* %Z11, align 4
  %add323 = fadd float %497, %mul322
  store float %add323, float* %Z11, align 4
  %498 = load float*, float** %ell, align 4
  %add.ptr324 = getelementptr inbounds float, float* %498, i32 1
  store float* %add.ptr324, float** %ell, align 4
  %499 = load float*, float** %ex, align 4
  %add.ptr325 = getelementptr inbounds float, float* %499, i32 1
  store float* %add.ptr325, float** %ex, align 4
  br label %for.inc326

for.inc326:                                       ; preds = %for.body319
  %500 = load i32, i32* %j, align 4
  %dec327 = add nsw i32 %500, -1
  store i32 %dec327, i32* %j, align 4
  br label %for.cond317

for.end328:                                       ; preds = %for.cond317
  %501 = load float*, float** %ex, align 4
  %arrayidx329 = getelementptr inbounds float, float* %501, i32 0
  %502 = load float, float* %arrayidx329, align 4
  %503 = load float, float* %Z11, align 4
  %sub330 = fsub float %502, %503
  store float %sub330, float* %Z11, align 4
  %504 = load float, float* %Z11, align 4
  %505 = load float*, float** %ex, align 4
  %arrayidx331 = getelementptr inbounds float, float* %505, i32 0
  store float %504, float* %arrayidx331, align 4
  br label %for.inc332

for.inc332:                                       ; preds = %for.end328
  %506 = load i32, i32* %i, align 4
  %inc = add nsw i32 %506, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond254

for.end333:                                       ; preds = %for.cond254
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z10btSolveL1TPKfPfii(float* %L, float* %B, i32 %n, i32 %lskip1) #1 {
entry:
  %L.addr = alloca float*, align 4
  %B.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %lskip1.addr = alloca i32, align 4
  %Z11 = alloca float, align 4
  %m11 = alloca float, align 4
  %Z21 = alloca float, align 4
  %m21 = alloca float, align 4
  %Z31 = alloca float, align 4
  %m31 = alloca float, align 4
  %Z41 = alloca float, align 4
  %m41 = alloca float, align 4
  %p1 = alloca float, align 4
  %q1 = alloca float, align 4
  %p2 = alloca float, align 4
  %p3 = alloca float, align 4
  %p4 = alloca float, align 4
  %ex = alloca float*, align 4
  %ell = alloca float*, align 4
  %lskip2 = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store float* %L, float** %L.addr, align 4
  store float* %B, float** %B.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 %lskip1, i32* %lskip1.addr, align 4
  %0 = load float*, float** %L.addr, align 4
  %1 = load i32, i32* %n.addr, align 4
  %sub = sub nsw i32 %1, 1
  %2 = load i32, i32* %lskip1.addr, align 4
  %add = add nsw i32 %2, 1
  %mul = mul nsw i32 %sub, %add
  %add.ptr = getelementptr inbounds float, float* %0, i32 %mul
  store float* %add.ptr, float** %L.addr, align 4
  %3 = load float*, float** %B.addr, align 4
  %4 = load i32, i32* %n.addr, align 4
  %add.ptr1 = getelementptr inbounds float, float* %3, i32 %4
  %add.ptr2 = getelementptr inbounds float, float* %add.ptr1, i32 -1
  store float* %add.ptr2, float** %B.addr, align 4
  %5 = load i32, i32* %lskip1.addr, align 4
  %sub3 = sub nsw i32 0, %5
  store i32 %sub3, i32* %lskip1.addr, align 4
  %6 = load i32, i32* %lskip1.addr, align 4
  %mul4 = mul nsw i32 2, %6
  store i32 %mul4, i32* %lskip2, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc122, %entry
  %7 = load i32, i32* %i, align 4
  %8 = load i32, i32* %n.addr, align 4
  %sub5 = sub nsw i32 %8, 4
  %cmp = icmp sle i32 %7, %sub5
  br i1 %cmp, label %for.body, label %for.end124

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %Z11, align 4
  store float 0.000000e+00, float* %Z21, align 4
  store float 0.000000e+00, float* %Z31, align 4
  store float 0.000000e+00, float* %Z41, align 4
  %9 = load float*, float** %L.addr, align 4
  %10 = load i32, i32* %i, align 4
  %idx.neg = sub i32 0, %10
  %add.ptr6 = getelementptr inbounds float, float* %9, i32 %idx.neg
  store float* %add.ptr6, float** %ell, align 4
  %11 = load float*, float** %B.addr, align 4
  store float* %11, float** %ex, align 4
  %12 = load i32, i32* %i, align 4
  %sub7 = sub nsw i32 %12, 4
  store i32 %sub7, i32* %j, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %13 = load i32, i32* %j, align 4
  %cmp9 = icmp sge i32 %13, 0
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %14 = load float*, float** %ell, align 4
  %arrayidx = getelementptr inbounds float, float* %14, i32 0
  %15 = load float, float* %arrayidx, align 4
  store float %15, float* %p1, align 4
  %16 = load float*, float** %ex, align 4
  %arrayidx11 = getelementptr inbounds float, float* %16, i32 0
  %17 = load float, float* %arrayidx11, align 4
  store float %17, float* %q1, align 4
  %18 = load float*, float** %ell, align 4
  %arrayidx12 = getelementptr inbounds float, float* %18, i32 -1
  %19 = load float, float* %arrayidx12, align 4
  store float %19, float* %p2, align 4
  %20 = load float*, float** %ell, align 4
  %arrayidx13 = getelementptr inbounds float, float* %20, i32 -2
  %21 = load float, float* %arrayidx13, align 4
  store float %21, float* %p3, align 4
  %22 = load float*, float** %ell, align 4
  %arrayidx14 = getelementptr inbounds float, float* %22, i32 -3
  %23 = load float, float* %arrayidx14, align 4
  store float %23, float* %p4, align 4
  %24 = load float, float* %p1, align 4
  %25 = load float, float* %q1, align 4
  %mul15 = fmul float %24, %25
  store float %mul15, float* %m11, align 4
  %26 = load float, float* %p2, align 4
  %27 = load float, float* %q1, align 4
  %mul16 = fmul float %26, %27
  store float %mul16, float* %m21, align 4
  %28 = load float, float* %p3, align 4
  %29 = load float, float* %q1, align 4
  %mul17 = fmul float %28, %29
  store float %mul17, float* %m31, align 4
  %30 = load float, float* %p4, align 4
  %31 = load float, float* %q1, align 4
  %mul18 = fmul float %30, %31
  store float %mul18, float* %m41, align 4
  %32 = load i32, i32* %lskip1.addr, align 4
  %33 = load float*, float** %ell, align 4
  %add.ptr19 = getelementptr inbounds float, float* %33, i32 %32
  store float* %add.ptr19, float** %ell, align 4
  %34 = load float, float* %m11, align 4
  %35 = load float, float* %Z11, align 4
  %add20 = fadd float %35, %34
  store float %add20, float* %Z11, align 4
  %36 = load float, float* %m21, align 4
  %37 = load float, float* %Z21, align 4
  %add21 = fadd float %37, %36
  store float %add21, float* %Z21, align 4
  %38 = load float, float* %m31, align 4
  %39 = load float, float* %Z31, align 4
  %add22 = fadd float %39, %38
  store float %add22, float* %Z31, align 4
  %40 = load float, float* %m41, align 4
  %41 = load float, float* %Z41, align 4
  %add23 = fadd float %41, %40
  store float %add23, float* %Z41, align 4
  %42 = load float*, float** %ell, align 4
  %arrayidx24 = getelementptr inbounds float, float* %42, i32 0
  %43 = load float, float* %arrayidx24, align 4
  store float %43, float* %p1, align 4
  %44 = load float*, float** %ex, align 4
  %arrayidx25 = getelementptr inbounds float, float* %44, i32 -1
  %45 = load float, float* %arrayidx25, align 4
  store float %45, float* %q1, align 4
  %46 = load float*, float** %ell, align 4
  %arrayidx26 = getelementptr inbounds float, float* %46, i32 -1
  %47 = load float, float* %arrayidx26, align 4
  store float %47, float* %p2, align 4
  %48 = load float*, float** %ell, align 4
  %arrayidx27 = getelementptr inbounds float, float* %48, i32 -2
  %49 = load float, float* %arrayidx27, align 4
  store float %49, float* %p3, align 4
  %50 = load float*, float** %ell, align 4
  %arrayidx28 = getelementptr inbounds float, float* %50, i32 -3
  %51 = load float, float* %arrayidx28, align 4
  store float %51, float* %p4, align 4
  %52 = load float, float* %p1, align 4
  %53 = load float, float* %q1, align 4
  %mul29 = fmul float %52, %53
  store float %mul29, float* %m11, align 4
  %54 = load float, float* %p2, align 4
  %55 = load float, float* %q1, align 4
  %mul30 = fmul float %54, %55
  store float %mul30, float* %m21, align 4
  %56 = load float, float* %p3, align 4
  %57 = load float, float* %q1, align 4
  %mul31 = fmul float %56, %57
  store float %mul31, float* %m31, align 4
  %58 = load float, float* %p4, align 4
  %59 = load float, float* %q1, align 4
  %mul32 = fmul float %58, %59
  store float %mul32, float* %m41, align 4
  %60 = load i32, i32* %lskip1.addr, align 4
  %61 = load float*, float** %ell, align 4
  %add.ptr33 = getelementptr inbounds float, float* %61, i32 %60
  store float* %add.ptr33, float** %ell, align 4
  %62 = load float, float* %m11, align 4
  %63 = load float, float* %Z11, align 4
  %add34 = fadd float %63, %62
  store float %add34, float* %Z11, align 4
  %64 = load float, float* %m21, align 4
  %65 = load float, float* %Z21, align 4
  %add35 = fadd float %65, %64
  store float %add35, float* %Z21, align 4
  %66 = load float, float* %m31, align 4
  %67 = load float, float* %Z31, align 4
  %add36 = fadd float %67, %66
  store float %add36, float* %Z31, align 4
  %68 = load float, float* %m41, align 4
  %69 = load float, float* %Z41, align 4
  %add37 = fadd float %69, %68
  store float %add37, float* %Z41, align 4
  %70 = load float*, float** %ell, align 4
  %arrayidx38 = getelementptr inbounds float, float* %70, i32 0
  %71 = load float, float* %arrayidx38, align 4
  store float %71, float* %p1, align 4
  %72 = load float*, float** %ex, align 4
  %arrayidx39 = getelementptr inbounds float, float* %72, i32 -2
  %73 = load float, float* %arrayidx39, align 4
  store float %73, float* %q1, align 4
  %74 = load float*, float** %ell, align 4
  %arrayidx40 = getelementptr inbounds float, float* %74, i32 -1
  %75 = load float, float* %arrayidx40, align 4
  store float %75, float* %p2, align 4
  %76 = load float*, float** %ell, align 4
  %arrayidx41 = getelementptr inbounds float, float* %76, i32 -2
  %77 = load float, float* %arrayidx41, align 4
  store float %77, float* %p3, align 4
  %78 = load float*, float** %ell, align 4
  %arrayidx42 = getelementptr inbounds float, float* %78, i32 -3
  %79 = load float, float* %arrayidx42, align 4
  store float %79, float* %p4, align 4
  %80 = load float, float* %p1, align 4
  %81 = load float, float* %q1, align 4
  %mul43 = fmul float %80, %81
  store float %mul43, float* %m11, align 4
  %82 = load float, float* %p2, align 4
  %83 = load float, float* %q1, align 4
  %mul44 = fmul float %82, %83
  store float %mul44, float* %m21, align 4
  %84 = load float, float* %p3, align 4
  %85 = load float, float* %q1, align 4
  %mul45 = fmul float %84, %85
  store float %mul45, float* %m31, align 4
  %86 = load float, float* %p4, align 4
  %87 = load float, float* %q1, align 4
  %mul46 = fmul float %86, %87
  store float %mul46, float* %m41, align 4
  %88 = load i32, i32* %lskip1.addr, align 4
  %89 = load float*, float** %ell, align 4
  %add.ptr47 = getelementptr inbounds float, float* %89, i32 %88
  store float* %add.ptr47, float** %ell, align 4
  %90 = load float, float* %m11, align 4
  %91 = load float, float* %Z11, align 4
  %add48 = fadd float %91, %90
  store float %add48, float* %Z11, align 4
  %92 = load float, float* %m21, align 4
  %93 = load float, float* %Z21, align 4
  %add49 = fadd float %93, %92
  store float %add49, float* %Z21, align 4
  %94 = load float, float* %m31, align 4
  %95 = load float, float* %Z31, align 4
  %add50 = fadd float %95, %94
  store float %add50, float* %Z31, align 4
  %96 = load float, float* %m41, align 4
  %97 = load float, float* %Z41, align 4
  %add51 = fadd float %97, %96
  store float %add51, float* %Z41, align 4
  %98 = load float*, float** %ell, align 4
  %arrayidx52 = getelementptr inbounds float, float* %98, i32 0
  %99 = load float, float* %arrayidx52, align 4
  store float %99, float* %p1, align 4
  %100 = load float*, float** %ex, align 4
  %arrayidx53 = getelementptr inbounds float, float* %100, i32 -3
  %101 = load float, float* %arrayidx53, align 4
  store float %101, float* %q1, align 4
  %102 = load float*, float** %ell, align 4
  %arrayidx54 = getelementptr inbounds float, float* %102, i32 -1
  %103 = load float, float* %arrayidx54, align 4
  store float %103, float* %p2, align 4
  %104 = load float*, float** %ell, align 4
  %arrayidx55 = getelementptr inbounds float, float* %104, i32 -2
  %105 = load float, float* %arrayidx55, align 4
  store float %105, float* %p3, align 4
  %106 = load float*, float** %ell, align 4
  %arrayidx56 = getelementptr inbounds float, float* %106, i32 -3
  %107 = load float, float* %arrayidx56, align 4
  store float %107, float* %p4, align 4
  %108 = load float, float* %p1, align 4
  %109 = load float, float* %q1, align 4
  %mul57 = fmul float %108, %109
  store float %mul57, float* %m11, align 4
  %110 = load float, float* %p2, align 4
  %111 = load float, float* %q1, align 4
  %mul58 = fmul float %110, %111
  store float %mul58, float* %m21, align 4
  %112 = load float, float* %p3, align 4
  %113 = load float, float* %q1, align 4
  %mul59 = fmul float %112, %113
  store float %mul59, float* %m31, align 4
  %114 = load float, float* %p4, align 4
  %115 = load float, float* %q1, align 4
  %mul60 = fmul float %114, %115
  store float %mul60, float* %m41, align 4
  %116 = load i32, i32* %lskip1.addr, align 4
  %117 = load float*, float** %ell, align 4
  %add.ptr61 = getelementptr inbounds float, float* %117, i32 %116
  store float* %add.ptr61, float** %ell, align 4
  %118 = load float*, float** %ex, align 4
  %add.ptr62 = getelementptr inbounds float, float* %118, i32 -4
  store float* %add.ptr62, float** %ex, align 4
  %119 = load float, float* %m11, align 4
  %120 = load float, float* %Z11, align 4
  %add63 = fadd float %120, %119
  store float %add63, float* %Z11, align 4
  %121 = load float, float* %m21, align 4
  %122 = load float, float* %Z21, align 4
  %add64 = fadd float %122, %121
  store float %add64, float* %Z21, align 4
  %123 = load float, float* %m31, align 4
  %124 = load float, float* %Z31, align 4
  %add65 = fadd float %124, %123
  store float %add65, float* %Z31, align 4
  %125 = load float, float* %m41, align 4
  %126 = load float, float* %Z41, align 4
  %add66 = fadd float %126, %125
  store float %add66, float* %Z41, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %127 = load i32, i32* %j, align 4
  %sub67 = sub nsw i32 %127, 4
  store i32 %sub67, i32* %j, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %128 = load i32, i32* %j, align 4
  %add68 = add nsw i32 %128, 4
  store i32 %add68, i32* %j, align 4
  br label %for.cond69

for.cond69:                                       ; preds = %for.inc87, %for.end
  %129 = load i32, i32* %j, align 4
  %cmp70 = icmp sgt i32 %129, 0
  br i1 %cmp70, label %for.body71, label %for.end88

for.body71:                                       ; preds = %for.cond69
  %130 = load float*, float** %ell, align 4
  %arrayidx72 = getelementptr inbounds float, float* %130, i32 0
  %131 = load float, float* %arrayidx72, align 4
  store float %131, float* %p1, align 4
  %132 = load float*, float** %ex, align 4
  %arrayidx73 = getelementptr inbounds float, float* %132, i32 0
  %133 = load float, float* %arrayidx73, align 4
  store float %133, float* %q1, align 4
  %134 = load float*, float** %ell, align 4
  %arrayidx74 = getelementptr inbounds float, float* %134, i32 -1
  %135 = load float, float* %arrayidx74, align 4
  store float %135, float* %p2, align 4
  %136 = load float*, float** %ell, align 4
  %arrayidx75 = getelementptr inbounds float, float* %136, i32 -2
  %137 = load float, float* %arrayidx75, align 4
  store float %137, float* %p3, align 4
  %138 = load float*, float** %ell, align 4
  %arrayidx76 = getelementptr inbounds float, float* %138, i32 -3
  %139 = load float, float* %arrayidx76, align 4
  store float %139, float* %p4, align 4
  %140 = load float, float* %p1, align 4
  %141 = load float, float* %q1, align 4
  %mul77 = fmul float %140, %141
  store float %mul77, float* %m11, align 4
  %142 = load float, float* %p2, align 4
  %143 = load float, float* %q1, align 4
  %mul78 = fmul float %142, %143
  store float %mul78, float* %m21, align 4
  %144 = load float, float* %p3, align 4
  %145 = load float, float* %q1, align 4
  %mul79 = fmul float %144, %145
  store float %mul79, float* %m31, align 4
  %146 = load float, float* %p4, align 4
  %147 = load float, float* %q1, align 4
  %mul80 = fmul float %146, %147
  store float %mul80, float* %m41, align 4
  %148 = load i32, i32* %lskip1.addr, align 4
  %149 = load float*, float** %ell, align 4
  %add.ptr81 = getelementptr inbounds float, float* %149, i32 %148
  store float* %add.ptr81, float** %ell, align 4
  %150 = load float*, float** %ex, align 4
  %add.ptr82 = getelementptr inbounds float, float* %150, i32 -1
  store float* %add.ptr82, float** %ex, align 4
  %151 = load float, float* %m11, align 4
  %152 = load float, float* %Z11, align 4
  %add83 = fadd float %152, %151
  store float %add83, float* %Z11, align 4
  %153 = load float, float* %m21, align 4
  %154 = load float, float* %Z21, align 4
  %add84 = fadd float %154, %153
  store float %add84, float* %Z21, align 4
  %155 = load float, float* %m31, align 4
  %156 = load float, float* %Z31, align 4
  %add85 = fadd float %156, %155
  store float %add85, float* %Z31, align 4
  %157 = load float, float* %m41, align 4
  %158 = load float, float* %Z41, align 4
  %add86 = fadd float %158, %157
  store float %add86, float* %Z41, align 4
  br label %for.inc87

for.inc87:                                        ; preds = %for.body71
  %159 = load i32, i32* %j, align 4
  %dec = add nsw i32 %159, -1
  store i32 %dec, i32* %j, align 4
  br label %for.cond69

for.end88:                                        ; preds = %for.cond69
  %160 = load float*, float** %ex, align 4
  %arrayidx89 = getelementptr inbounds float, float* %160, i32 0
  %161 = load float, float* %arrayidx89, align 4
  %162 = load float, float* %Z11, align 4
  %sub90 = fsub float %161, %162
  store float %sub90, float* %Z11, align 4
  %163 = load float, float* %Z11, align 4
  %164 = load float*, float** %ex, align 4
  %arrayidx91 = getelementptr inbounds float, float* %164, i32 0
  store float %163, float* %arrayidx91, align 4
  %165 = load float*, float** %ell, align 4
  %arrayidx92 = getelementptr inbounds float, float* %165, i32 -1
  %166 = load float, float* %arrayidx92, align 4
  store float %166, float* %p1, align 4
  %167 = load float*, float** %ex, align 4
  %arrayidx93 = getelementptr inbounds float, float* %167, i32 -1
  %168 = load float, float* %arrayidx93, align 4
  %169 = load float, float* %Z21, align 4
  %sub94 = fsub float %168, %169
  %170 = load float, float* %p1, align 4
  %171 = load float, float* %Z11, align 4
  %mul95 = fmul float %170, %171
  %sub96 = fsub float %sub94, %mul95
  store float %sub96, float* %Z21, align 4
  %172 = load float, float* %Z21, align 4
  %173 = load float*, float** %ex, align 4
  %arrayidx97 = getelementptr inbounds float, float* %173, i32 -1
  store float %172, float* %arrayidx97, align 4
  %174 = load float*, float** %ell, align 4
  %arrayidx98 = getelementptr inbounds float, float* %174, i32 -2
  %175 = load float, float* %arrayidx98, align 4
  store float %175, float* %p1, align 4
  %176 = load float*, float** %ell, align 4
  %177 = load i32, i32* %lskip1.addr, align 4
  %add99 = add nsw i32 -2, %177
  %arrayidx100 = getelementptr inbounds float, float* %176, i32 %add99
  %178 = load float, float* %arrayidx100, align 4
  store float %178, float* %p2, align 4
  %179 = load float*, float** %ex, align 4
  %arrayidx101 = getelementptr inbounds float, float* %179, i32 -2
  %180 = load float, float* %arrayidx101, align 4
  %181 = load float, float* %Z31, align 4
  %sub102 = fsub float %180, %181
  %182 = load float, float* %p1, align 4
  %183 = load float, float* %Z11, align 4
  %mul103 = fmul float %182, %183
  %sub104 = fsub float %sub102, %mul103
  %184 = load float, float* %p2, align 4
  %185 = load float, float* %Z21, align 4
  %mul105 = fmul float %184, %185
  %sub106 = fsub float %sub104, %mul105
  store float %sub106, float* %Z31, align 4
  %186 = load float, float* %Z31, align 4
  %187 = load float*, float** %ex, align 4
  %arrayidx107 = getelementptr inbounds float, float* %187, i32 -2
  store float %186, float* %arrayidx107, align 4
  %188 = load float*, float** %ell, align 4
  %arrayidx108 = getelementptr inbounds float, float* %188, i32 -3
  %189 = load float, float* %arrayidx108, align 4
  store float %189, float* %p1, align 4
  %190 = load float*, float** %ell, align 4
  %191 = load i32, i32* %lskip1.addr, align 4
  %add109 = add nsw i32 -3, %191
  %arrayidx110 = getelementptr inbounds float, float* %190, i32 %add109
  %192 = load float, float* %arrayidx110, align 4
  store float %192, float* %p2, align 4
  %193 = load float*, float** %ell, align 4
  %194 = load i32, i32* %lskip2, align 4
  %add111 = add nsw i32 -3, %194
  %arrayidx112 = getelementptr inbounds float, float* %193, i32 %add111
  %195 = load float, float* %arrayidx112, align 4
  store float %195, float* %p3, align 4
  %196 = load float*, float** %ex, align 4
  %arrayidx113 = getelementptr inbounds float, float* %196, i32 -3
  %197 = load float, float* %arrayidx113, align 4
  %198 = load float, float* %Z41, align 4
  %sub114 = fsub float %197, %198
  %199 = load float, float* %p1, align 4
  %200 = load float, float* %Z11, align 4
  %mul115 = fmul float %199, %200
  %sub116 = fsub float %sub114, %mul115
  %201 = load float, float* %p2, align 4
  %202 = load float, float* %Z21, align 4
  %mul117 = fmul float %201, %202
  %sub118 = fsub float %sub116, %mul117
  %203 = load float, float* %p3, align 4
  %204 = load float, float* %Z31, align 4
  %mul119 = fmul float %203, %204
  %sub120 = fsub float %sub118, %mul119
  store float %sub120, float* %Z41, align 4
  %205 = load float, float* %Z41, align 4
  %206 = load float*, float** %ex, align 4
  %arrayidx121 = getelementptr inbounds float, float* %206, i32 -3
  store float %205, float* %arrayidx121, align 4
  br label %for.inc122

for.inc122:                                       ; preds = %for.end88
  %207 = load i32, i32* %i, align 4
  %add123 = add nsw i32 %207, 4
  store i32 %add123, i32* %i, align 4
  br label %for.cond

for.end124:                                       ; preds = %for.cond
  br label %for.cond125

for.cond125:                                      ; preds = %for.inc174, %for.end124
  %208 = load i32, i32* %i, align 4
  %209 = load i32, i32* %n.addr, align 4
  %cmp126 = icmp slt i32 %208, %209
  br i1 %cmp126, label %for.body127, label %for.end175

for.body127:                                      ; preds = %for.cond125
  store float 0.000000e+00, float* %Z11, align 4
  %210 = load float*, float** %L.addr, align 4
  %211 = load i32, i32* %i, align 4
  %idx.neg128 = sub i32 0, %211
  %add.ptr129 = getelementptr inbounds float, float* %210, i32 %idx.neg128
  store float* %add.ptr129, float** %ell, align 4
  %212 = load float*, float** %B.addr, align 4
  store float* %212, float** %ex, align 4
  %213 = load i32, i32* %i, align 4
  %sub130 = sub nsw i32 %213, 4
  store i32 %sub130, i32* %j, align 4
  br label %for.cond131

for.cond131:                                      ; preds = %for.inc155, %for.body127
  %214 = load i32, i32* %j, align 4
  %cmp132 = icmp sge i32 %214, 0
  br i1 %cmp132, label %for.body133, label %for.end157

for.body133:                                      ; preds = %for.cond131
  %215 = load float*, float** %ell, align 4
  %arrayidx134 = getelementptr inbounds float, float* %215, i32 0
  %216 = load float, float* %arrayidx134, align 4
  store float %216, float* %p1, align 4
  %217 = load float*, float** %ex, align 4
  %arrayidx135 = getelementptr inbounds float, float* %217, i32 0
  %218 = load float, float* %arrayidx135, align 4
  store float %218, float* %q1, align 4
  %219 = load float, float* %p1, align 4
  %220 = load float, float* %q1, align 4
  %mul136 = fmul float %219, %220
  store float %mul136, float* %m11, align 4
  %221 = load i32, i32* %lskip1.addr, align 4
  %222 = load float*, float** %ell, align 4
  %add.ptr137 = getelementptr inbounds float, float* %222, i32 %221
  store float* %add.ptr137, float** %ell, align 4
  %223 = load float, float* %m11, align 4
  %224 = load float, float* %Z11, align 4
  %add138 = fadd float %224, %223
  store float %add138, float* %Z11, align 4
  %225 = load float*, float** %ell, align 4
  %arrayidx139 = getelementptr inbounds float, float* %225, i32 0
  %226 = load float, float* %arrayidx139, align 4
  store float %226, float* %p1, align 4
  %227 = load float*, float** %ex, align 4
  %arrayidx140 = getelementptr inbounds float, float* %227, i32 -1
  %228 = load float, float* %arrayidx140, align 4
  store float %228, float* %q1, align 4
  %229 = load float, float* %p1, align 4
  %230 = load float, float* %q1, align 4
  %mul141 = fmul float %229, %230
  store float %mul141, float* %m11, align 4
  %231 = load i32, i32* %lskip1.addr, align 4
  %232 = load float*, float** %ell, align 4
  %add.ptr142 = getelementptr inbounds float, float* %232, i32 %231
  store float* %add.ptr142, float** %ell, align 4
  %233 = load float, float* %m11, align 4
  %234 = load float, float* %Z11, align 4
  %add143 = fadd float %234, %233
  store float %add143, float* %Z11, align 4
  %235 = load float*, float** %ell, align 4
  %arrayidx144 = getelementptr inbounds float, float* %235, i32 0
  %236 = load float, float* %arrayidx144, align 4
  store float %236, float* %p1, align 4
  %237 = load float*, float** %ex, align 4
  %arrayidx145 = getelementptr inbounds float, float* %237, i32 -2
  %238 = load float, float* %arrayidx145, align 4
  store float %238, float* %q1, align 4
  %239 = load float, float* %p1, align 4
  %240 = load float, float* %q1, align 4
  %mul146 = fmul float %239, %240
  store float %mul146, float* %m11, align 4
  %241 = load i32, i32* %lskip1.addr, align 4
  %242 = load float*, float** %ell, align 4
  %add.ptr147 = getelementptr inbounds float, float* %242, i32 %241
  store float* %add.ptr147, float** %ell, align 4
  %243 = load float, float* %m11, align 4
  %244 = load float, float* %Z11, align 4
  %add148 = fadd float %244, %243
  store float %add148, float* %Z11, align 4
  %245 = load float*, float** %ell, align 4
  %arrayidx149 = getelementptr inbounds float, float* %245, i32 0
  %246 = load float, float* %arrayidx149, align 4
  store float %246, float* %p1, align 4
  %247 = load float*, float** %ex, align 4
  %arrayidx150 = getelementptr inbounds float, float* %247, i32 -3
  %248 = load float, float* %arrayidx150, align 4
  store float %248, float* %q1, align 4
  %249 = load float, float* %p1, align 4
  %250 = load float, float* %q1, align 4
  %mul151 = fmul float %249, %250
  store float %mul151, float* %m11, align 4
  %251 = load i32, i32* %lskip1.addr, align 4
  %252 = load float*, float** %ell, align 4
  %add.ptr152 = getelementptr inbounds float, float* %252, i32 %251
  store float* %add.ptr152, float** %ell, align 4
  %253 = load float*, float** %ex, align 4
  %add.ptr153 = getelementptr inbounds float, float* %253, i32 -4
  store float* %add.ptr153, float** %ex, align 4
  %254 = load float, float* %m11, align 4
  %255 = load float, float* %Z11, align 4
  %add154 = fadd float %255, %254
  store float %add154, float* %Z11, align 4
  br label %for.inc155

for.inc155:                                       ; preds = %for.body133
  %256 = load i32, i32* %j, align 4
  %sub156 = sub nsw i32 %256, 4
  store i32 %sub156, i32* %j, align 4
  br label %for.cond131

for.end157:                                       ; preds = %for.cond131
  %257 = load i32, i32* %j, align 4
  %add158 = add nsw i32 %257, 4
  store i32 %add158, i32* %j, align 4
  br label %for.cond159

for.cond159:                                      ; preds = %for.inc168, %for.end157
  %258 = load i32, i32* %j, align 4
  %cmp160 = icmp sgt i32 %258, 0
  br i1 %cmp160, label %for.body161, label %for.end170

for.body161:                                      ; preds = %for.cond159
  %259 = load float*, float** %ell, align 4
  %arrayidx162 = getelementptr inbounds float, float* %259, i32 0
  %260 = load float, float* %arrayidx162, align 4
  store float %260, float* %p1, align 4
  %261 = load float*, float** %ex, align 4
  %arrayidx163 = getelementptr inbounds float, float* %261, i32 0
  %262 = load float, float* %arrayidx163, align 4
  store float %262, float* %q1, align 4
  %263 = load float, float* %p1, align 4
  %264 = load float, float* %q1, align 4
  %mul164 = fmul float %263, %264
  store float %mul164, float* %m11, align 4
  %265 = load i32, i32* %lskip1.addr, align 4
  %266 = load float*, float** %ell, align 4
  %add.ptr165 = getelementptr inbounds float, float* %266, i32 %265
  store float* %add.ptr165, float** %ell, align 4
  %267 = load float*, float** %ex, align 4
  %add.ptr166 = getelementptr inbounds float, float* %267, i32 -1
  store float* %add.ptr166, float** %ex, align 4
  %268 = load float, float* %m11, align 4
  %269 = load float, float* %Z11, align 4
  %add167 = fadd float %269, %268
  store float %add167, float* %Z11, align 4
  br label %for.inc168

for.inc168:                                       ; preds = %for.body161
  %270 = load i32, i32* %j, align 4
  %dec169 = add nsw i32 %270, -1
  store i32 %dec169, i32* %j, align 4
  br label %for.cond159

for.end170:                                       ; preds = %for.cond159
  %271 = load float*, float** %ex, align 4
  %arrayidx171 = getelementptr inbounds float, float* %271, i32 0
  %272 = load float, float* %arrayidx171, align 4
  %273 = load float, float* %Z11, align 4
  %sub172 = fsub float %272, %273
  store float %sub172, float* %Z11, align 4
  %274 = load float, float* %Z11, align 4
  %275 = load float*, float** %ex, align 4
  %arrayidx173 = getelementptr inbounds float, float* %275, i32 0
  store float %274, float* %arrayidx173, align 4
  br label %for.inc174

for.inc174:                                       ; preds = %for.end170
  %276 = load i32, i32* %i, align 4
  %inc = add nsw i32 %276, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond125

for.end175:                                       ; preds = %for.cond125
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z13btVectorScalePfPKfi(float* %a, float* %d, i32 %n) #1 {
entry:
  %a.addr = alloca float*, align 4
  %d.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store float* %a, float** %a.addr, align 4
  store float* %d, float** %d.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %n.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load float*, float** %d.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %2, i32 %3
  %4 = load float, float* %arrayidx, align 4
  %5 = load float*, float** %a.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx1 = getelementptr inbounds float, float* %5, i32 %6
  %7 = load float, float* %arrayidx1, align 4
  %mul = fmul float %7, %4
  store float %mul, float* %arrayidx1, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z11btSolveLDLTPKfS0_Pfii(float* %L, float* %d, float* %b, i32 %n, i32 %nskip) #1 {
entry:
  %L.addr = alloca float*, align 4
  %d.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %nskip.addr = alloca i32, align 4
  store float* %L, float** %L.addr, align 4
  store float* %d, float** %d.addr, align 4
  store float* %b, float** %b.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 %nskip, i32* %nskip.addr, align 4
  %0 = load float*, float** %L.addr, align 4
  %1 = load float*, float** %b.addr, align 4
  %2 = load i32, i32* %n.addr, align 4
  %3 = load i32, i32* %nskip.addr, align 4
  call void @_Z9btSolveL1PKfPfii(float* %0, float* %1, i32 %2, i32 %3)
  %4 = load float*, float** %b.addr, align 4
  %5 = load float*, float** %d.addr, align 4
  %6 = load i32, i32* %n.addr, align 4
  call void @_Z13btVectorScalePfPKfi(float* %4, float* %5, i32 %6)
  %7 = load float*, float** %L.addr, align 4
  %8 = load float*, float** %b.addr, align 4
  %9 = load i32, i32* %n.addr, align 4
  %10 = load i32, i32* %nskip.addr, align 4
  call void @_Z10btSolveL1TPKfPfii(float* %7, float* %8, i32 %9, i32 %10)
  ret void
}

; Function Attrs: noinline optnone
define hidden %struct.btLCP* @_ZN5btLCPC2EiiiPfS0_S0_S0_S0_S0_S0_S0_S0_S0_S0_PbPiS2_S2_PS0_(%struct.btLCP* returned %this, i32 %_n, i32 %_nskip, i32 %_nub, float* %_Adata, float* %_x, float* %_b, float* %_w, float* %_lo, float* %_hi, float* %l, float* %_d, float* %_Dell, float* %_ell, float* %_tmp, i8* %_state, i32* %_findex, i32* %p, i32* %c, float** %Arows) unnamed_addr #2 {
entry:
  %retval = alloca %struct.btLCP*, align 4
  %this.addr = alloca %struct.btLCP*, align 4
  %_n.addr = alloca i32, align 4
  %_nskip.addr = alloca i32, align 4
  %_nub.addr = alloca i32, align 4
  %_Adata.addr = alloca float*, align 4
  %_x.addr = alloca float*, align 4
  %_b.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  %_lo.addr = alloca float*, align 4
  %_hi.addr = alloca float*, align 4
  %l.addr = alloca float*, align 4
  %_d.addr = alloca float*, align 4
  %_Dell.addr = alloca float*, align 4
  %_ell.addr = alloca float*, align 4
  %_tmp.addr = alloca float*, align 4
  %_state.addr = alloca i8*, align 4
  %_findex.addr = alloca i32*, align 4
  %p.addr = alloca i32*, align 4
  %c.addr = alloca i32*, align 4
  %Arows.addr = alloca float**, align 4
  %aptr = alloca float*, align 4
  %A = alloca float**, align 4
  %n = alloca i32, align 4
  %nskip = alloca i32, align 4
  %k = alloca i32, align 4
  %p7 = alloca i32*, align 4
  %n9 = alloca i32, align 4
  %k11 = alloca i32, align 4
  %findex = alloca i32*, align 4
  %lo = alloca float*, align 4
  %hi = alloca float*, align 4
  %n22 = alloca i32, align 4
  %k24 = alloca i32, align 4
  %nub = alloca i32, align 4
  %Lrow = alloca float*, align 4
  %nskip56 = alloca i32, align 4
  %j = alloca i32, align 4
  %C = alloca i32*, align 4
  %k79 = alloca i32, align 4
  %nub92 = alloca i32, align 4
  %findex94 = alloca i32*, align 4
  %num_at_end = alloca i32, align 4
  %k96 = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store i32 %_n, i32* %_n.addr, align 4
  store i32 %_nskip, i32* %_nskip.addr, align 4
  store i32 %_nub, i32* %_nub.addr, align 4
  store float* %_Adata, float** %_Adata.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_b, float** %_b.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  store float* %_lo, float** %_lo.addr, align 4
  store float* %_hi, float** %_hi.addr, align 4
  store float* %l, float** %l.addr, align 4
  store float* %_d, float** %_d.addr, align 4
  store float* %_Dell, float** %_Dell.addr, align 4
  store float* %_ell, float** %_ell.addr, align 4
  store float* %_tmp, float** %_tmp.addr, align 4
  store i8* %_state, i8** %_state.addr, align 4
  store i32* %_findex, i32** %_findex.addr, align 4
  store i32* %p, i32** %p.addr, align 4
  store i32* %c, i32** %c.addr, align 4
  store float** %Arows, float*** %Arows.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  store %struct.btLCP* %this1, %struct.btLCP** %retval, align 4
  %m_n = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %0 = load i32, i32* %_n.addr, align 4
  store i32 %0, i32* %m_n, align 4
  %m_nskip = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %1 = load i32, i32* %_nskip.addr, align 4
  store i32 %1, i32* %m_nskip, align 4
  %m_nub = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %2 = load i32, i32* %_nub.addr, align 4
  store i32 %2, i32* %m_nub, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  store i32 0, i32* %m_nC, align 4
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  store i32 0, i32* %m_nN, align 4
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %3 = load float**, float*** %Arows.addr, align 4
  store float** %3, float*** %m_A, align 4
  %m_x = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %4 = load float*, float** %_x.addr, align 4
  store float* %4, float** %m_x, align 4
  %m_b = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 7
  %5 = load float*, float** %_b.addr, align 4
  store float* %5, float** %m_b, align 4
  %m_w = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %6 = load float*, float** %_w.addr, align 4
  store float* %6, float** %m_w, align 4
  %m_lo = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 9
  %7 = load float*, float** %_lo.addr, align 4
  store float* %7, float** %m_lo, align 4
  %m_hi = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 10
  %8 = load float*, float** %_hi.addr, align 4
  store float* %8, float** %m_hi, align 4
  %m_L = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %9 = load float*, float** %l.addr, align 4
  store float* %9, float** %m_L, align 4
  %m_d = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %10 = load float*, float** %_d.addr, align 4
  store float* %10, float** %m_d, align 4
  %m_Dell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %11 = load float*, float** %_Dell.addr, align 4
  store float* %11, float** %m_Dell, align 4
  %m_ell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 14
  %12 = load float*, float** %_ell.addr, align 4
  store float* %12, float** %m_ell, align 4
  %m_tmp = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %13 = load float*, float** %_tmp.addr, align 4
  store float* %13, float** %m_tmp, align 4
  %m_state = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 16
  %14 = load i8*, i8** %_state.addr, align 4
  store i8* %14, i8** %m_state, align 4
  %m_findex = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 17
  %15 = load i32*, i32** %_findex.addr, align 4
  store i32* %15, i32** %m_findex, align 4
  %m_p = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %16 = load i32*, i32** %p.addr, align 4
  store i32* %16, i32** %m_p, align 4
  %m_C = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %17 = load i32*, i32** %c.addr, align 4
  store i32* %17, i32** %m_C, align 4
  %m_x2 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %18 = load float*, float** %m_x2, align 4
  %m_n3 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %19 = load i32, i32* %m_n3, align 4
  call void @_Z9btSetZeroIfEvPT_i(float* %18, i32 %19)
  %20 = load float*, float** %_Adata.addr, align 4
  store float* %20, float** %aptr, align 4
  %m_A4 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %21 = load float**, float*** %m_A4, align 4
  store float** %21, float*** %A, align 4
  %m_n5 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %22 = load i32, i32* %m_n5, align 4
  store i32 %22, i32* %n, align 4
  %m_nskip6 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %23 = load i32, i32* %m_nskip6, align 4
  store i32 %23, i32* %nskip, align 4
  store i32 0, i32* %k, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %24 = load i32, i32* %k, align 4
  %25 = load i32, i32* %n, align 4
  %cmp = icmp slt i32 %24, %25
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %26 = load float*, float** %aptr, align 4
  %27 = load float**, float*** %A, align 4
  %28 = load i32, i32* %k, align 4
  %arrayidx = getelementptr inbounds float*, float** %27, i32 %28
  store float* %26, float** %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %29 = load i32, i32* %nskip, align 4
  %30 = load float*, float** %aptr, align 4
  %add.ptr = getelementptr inbounds float, float* %30, i32 %29
  store float* %add.ptr, float** %aptr, align 4
  %31 = load i32, i32* %k, align 4
  %inc = add nsw i32 %31, 1
  store i32 %inc, i32* %k, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_p8 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %32 = load i32*, i32** %m_p8, align 4
  store i32* %32, i32** %p7, align 4
  %m_n10 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %33 = load i32, i32* %m_n10, align 4
  store i32 %33, i32* %n9, align 4
  store i32 0, i32* %k11, align 4
  br label %for.cond12

for.cond12:                                       ; preds = %for.inc16, %for.end
  %34 = load i32, i32* %k11, align 4
  %35 = load i32, i32* %n9, align 4
  %cmp13 = icmp slt i32 %34, %35
  br i1 %cmp13, label %for.body14, label %for.end18

for.body14:                                       ; preds = %for.cond12
  %36 = load i32, i32* %k11, align 4
  %37 = load i32*, i32** %p7, align 4
  %38 = load i32, i32* %k11, align 4
  %arrayidx15 = getelementptr inbounds i32, i32* %37, i32 %38
  store i32 %36, i32* %arrayidx15, align 4
  br label %for.inc16

for.inc16:                                        ; preds = %for.body14
  %39 = load i32, i32* %k11, align 4
  %inc17 = add nsw i32 %39, 1
  store i32 %inc17, i32* %k11, align 4
  br label %for.cond12

for.end18:                                        ; preds = %for.cond12
  %m_findex19 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 17
  %40 = load i32*, i32** %m_findex19, align 4
  store i32* %40, i32** %findex, align 4
  %m_lo20 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 9
  %41 = load float*, float** %m_lo20, align 4
  store float* %41, float** %lo, align 4
  %m_hi21 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 10
  %42 = load float*, float** %m_hi21, align 4
  store float* %42, float** %hi, align 4
  %m_n23 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %43 = load i32, i32* %m_n23, align 4
  store i32 %43, i32* %n22, align 4
  %m_nub25 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %44 = load i32, i32* %m_nub25, align 4
  store i32 %44, i32* %k24, align 4
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc48, %for.end18
  %45 = load i32, i32* %k24, align 4
  %46 = load i32, i32* %n22, align 4
  %cmp27 = icmp slt i32 %45, %46
  br i1 %cmp27, label %for.body28, label %for.end50

for.body28:                                       ; preds = %for.cond26
  %47 = load i32*, i32** %findex, align 4
  %tobool = icmp ne i32* %47, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body28
  %48 = load i32*, i32** %findex, align 4
  %49 = load i32, i32* %k24, align 4
  %arrayidx29 = getelementptr inbounds i32, i32* %48, i32 %49
  %50 = load i32, i32* %arrayidx29, align 4
  %cmp30 = icmp sge i32 %50, 0
  br i1 %cmp30, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %for.inc48

if.end:                                           ; preds = %land.lhs.true, %for.body28
  %51 = load float*, float** %lo, align 4
  %52 = load i32, i32* %k24, align 4
  %arrayidx31 = getelementptr inbounds float, float* %51, i32 %52
  %53 = load float, float* %arrayidx31, align 4
  %54 = load float, float* getelementptr inbounds (%struct.btInfMaskConverter, %struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 0, i32 0, i32 0), align 4
  %fneg = fneg float %54
  %cmp32 = fcmp oeq float %53, %fneg
  br i1 %cmp32, label %land.lhs.true33, label %if.end47

land.lhs.true33:                                  ; preds = %if.end
  %55 = load float*, float** %hi, align 4
  %56 = load i32, i32* %k24, align 4
  %arrayidx34 = getelementptr inbounds float, float* %55, i32 %56
  %57 = load float, float* %arrayidx34, align 4
  %58 = load float, float* getelementptr inbounds (%struct.btInfMaskConverter, %struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 0, i32 0, i32 0), align 4
  %cmp35 = fcmp oeq float %57, %58
  br i1 %cmp35, label %if.then36, label %if.end47

if.then36:                                        ; preds = %land.lhs.true33
  %m_A37 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %59 = load float**, float*** %m_A37, align 4
  %m_x38 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %60 = load float*, float** %m_x38, align 4
  %m_b39 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 7
  %61 = load float*, float** %m_b39, align 4
  %m_w40 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %62 = load float*, float** %m_w40, align 4
  %63 = load float*, float** %lo, align 4
  %64 = load float*, float** %hi, align 4
  %m_p41 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %65 = load i32*, i32** %m_p41, align 4
  %m_state42 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 16
  %66 = load i8*, i8** %m_state42, align 4
  %67 = load i32*, i32** %findex, align 4
  %68 = load i32, i32* %n22, align 4
  %m_nub43 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %69 = load i32, i32* %m_nub43, align 4
  %70 = load i32, i32* %k24, align 4
  %m_nskip44 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %71 = load i32, i32* %m_nskip44, align 4
  call void @_ZL13btSwapProblemPPfS_S_S_S_S_PiPbS1_iiiii(float** %59, float* %60, float* %61, float* %62, float* %63, float* %64, i32* %65, i8* %66, i32* %67, i32 %68, i32 %69, i32 %70, i32 %71, i32 0)
  %m_nub45 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %72 = load i32, i32* %m_nub45, align 4
  %inc46 = add nsw i32 %72, 1
  store i32 %inc46, i32* %m_nub45, align 4
  br label %if.end47

if.end47:                                         ; preds = %if.then36, %land.lhs.true33, %if.end
  br label %for.inc48

for.inc48:                                        ; preds = %if.end47, %if.then
  %73 = load i32, i32* %k24, align 4
  %inc49 = add nsw i32 %73, 1
  store i32 %inc49, i32* %k24, align 4
  br label %for.cond26

for.end50:                                        ; preds = %for.cond26
  %m_nub51 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %74 = load i32, i32* %m_nub51, align 4
  %cmp52 = icmp sgt i32 %74, 0
  br i1 %cmp52, label %if.then53, label %if.end88

if.then53:                                        ; preds = %for.end50
  %m_nub54 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %75 = load i32, i32* %m_nub54, align 4
  store i32 %75, i32* %nub, align 4
  %m_L55 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %76 = load float*, float** %m_L55, align 4
  store float* %76, float** %Lrow, align 4
  %m_nskip57 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %77 = load i32, i32* %m_nskip57, align 4
  store i32 %77, i32* %nskip56, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond58

for.cond58:                                       ; preds = %for.inc63, %if.then53
  %78 = load i32, i32* %j, align 4
  %79 = load i32, i32* %nub, align 4
  %cmp59 = icmp slt i32 %78, %79
  br i1 %cmp59, label %for.body60, label %for.end66

for.body60:                                       ; preds = %for.cond58
  %80 = load float*, float** %Lrow, align 4
  %81 = bitcast float* %80 to i8*
  %m_A61 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %82 = load float**, float*** %m_A61, align 4
  %83 = load i32, i32* %j, align 4
  %arrayidx62 = getelementptr inbounds float*, float** %82, i32 %83
  %84 = load float*, float** %arrayidx62, align 4
  %85 = bitcast float* %84 to i8*
  %86 = load i32, i32* %j, align 4
  %add = add nsw i32 %86, 1
  %mul = mul i32 %add, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %81, i8* align 4 %85, i32 %mul, i1 false)
  br label %for.inc63

for.inc63:                                        ; preds = %for.body60
  %87 = load i32, i32* %nskip56, align 4
  %88 = load float*, float** %Lrow, align 4
  %add.ptr64 = getelementptr inbounds float, float* %88, i32 %87
  store float* %add.ptr64, float** %Lrow, align 4
  %89 = load i32, i32* %j, align 4
  %inc65 = add nsw i32 %89, 1
  store i32 %inc65, i32* %j, align 4
  br label %for.cond58

for.end66:                                        ; preds = %for.cond58
  %m_L67 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %90 = load float*, float** %m_L67, align 4
  %m_d68 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %91 = load float*, float** %m_d68, align 4
  %92 = load i32, i32* %nub, align 4
  %m_nskip69 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %93 = load i32, i32* %m_nskip69, align 4
  call void @_Z12btFactorLDLTPfS_ii(float* %90, float* %91, i32 %92, i32 %93)
  %m_x70 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %94 = load float*, float** %m_x70, align 4
  %95 = bitcast float* %94 to i8*
  %m_b71 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 7
  %96 = load float*, float** %m_b71, align 4
  %97 = bitcast float* %96 to i8*
  %98 = load i32, i32* %nub, align 4
  %mul72 = mul i32 %98, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %95, i8* align 4 %97, i32 %mul72, i1 false)
  %m_L73 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %99 = load float*, float** %m_L73, align 4
  %m_d74 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %100 = load float*, float** %m_d74, align 4
  %m_x75 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %101 = load float*, float** %m_x75, align 4
  %102 = load i32, i32* %nub, align 4
  %m_nskip76 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %103 = load i32, i32* %m_nskip76, align 4
  call void @_Z11btSolveLDLTPKfS0_Pfii(float* %99, float* %100, float* %101, i32 %102, i32 %103)
  %m_w77 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %104 = load float*, float** %m_w77, align 4
  %105 = load i32, i32* %nub, align 4
  call void @_Z9btSetZeroIfEvPT_i(float* %104, i32 %105)
  %m_C78 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %106 = load i32*, i32** %m_C78, align 4
  store i32* %106, i32** %C, align 4
  store i32 0, i32* %k79, align 4
  br label %for.cond80

for.cond80:                                       ; preds = %for.inc84, %for.end66
  %107 = load i32, i32* %k79, align 4
  %108 = load i32, i32* %nub, align 4
  %cmp81 = icmp slt i32 %107, %108
  br i1 %cmp81, label %for.body82, label %for.end86

for.body82:                                       ; preds = %for.cond80
  %109 = load i32, i32* %k79, align 4
  %110 = load i32*, i32** %C, align 4
  %111 = load i32, i32* %k79, align 4
  %arrayidx83 = getelementptr inbounds i32, i32* %110, i32 %111
  store i32 %109, i32* %arrayidx83, align 4
  br label %for.inc84

for.inc84:                                        ; preds = %for.body82
  %112 = load i32, i32* %k79, align 4
  %inc85 = add nsw i32 %112, 1
  store i32 %inc85, i32* %k79, align 4
  br label %for.cond80

for.end86:                                        ; preds = %for.cond80
  %113 = load i32, i32* %nub, align 4
  %m_nC87 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  store i32 %113, i32* %m_nC87, align 4
  br label %if.end88

if.end88:                                         ; preds = %for.end86, %for.end50
  %m_findex89 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 17
  %114 = load i32*, i32** %m_findex89, align 4
  %tobool90 = icmp ne i32* %114, null
  br i1 %tobool90, label %if.then91, label %if.end121

if.then91:                                        ; preds = %if.end88
  %m_nub93 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %115 = load i32, i32* %m_nub93, align 4
  store i32 %115, i32* %nub92, align 4
  %m_findex95 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 17
  %116 = load i32*, i32** %m_findex95, align 4
  store i32* %116, i32** %findex94, align 4
  store i32 0, i32* %num_at_end, align 4
  %m_n97 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %117 = load i32, i32* %m_n97, align 4
  %sub = sub nsw i32 %117, 1
  store i32 %sub, i32* %k96, align 4
  br label %for.cond98

for.cond98:                                       ; preds = %for.inc119, %if.then91
  %118 = load i32, i32* %k96, align 4
  %119 = load i32, i32* %nub92, align 4
  %cmp99 = icmp sge i32 %118, %119
  br i1 %cmp99, label %for.body100, label %for.end120

for.body100:                                      ; preds = %for.cond98
  %120 = load i32*, i32** %findex94, align 4
  %121 = load i32, i32* %k96, align 4
  %arrayidx101 = getelementptr inbounds i32, i32* %120, i32 %121
  %122 = load i32, i32* %arrayidx101, align 4
  %cmp102 = icmp sge i32 %122, 0
  br i1 %cmp102, label %if.then103, label %if.end118

if.then103:                                       ; preds = %for.body100
  %m_A104 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %123 = load float**, float*** %m_A104, align 4
  %m_x105 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %124 = load float*, float** %m_x105, align 4
  %m_b106 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 7
  %125 = load float*, float** %m_b106, align 4
  %m_w107 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %126 = load float*, float** %m_w107, align 4
  %m_lo108 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 9
  %127 = load float*, float** %m_lo108, align 4
  %m_hi109 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 10
  %128 = load float*, float** %m_hi109, align 4
  %m_p110 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %129 = load i32*, i32** %m_p110, align 4
  %m_state111 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 16
  %130 = load i8*, i8** %m_state111, align 4
  %131 = load i32*, i32** %findex94, align 4
  %m_n112 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %132 = load i32, i32* %m_n112, align 4
  %133 = load i32, i32* %k96, align 4
  %m_n113 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %134 = load i32, i32* %m_n113, align 4
  %sub114 = sub nsw i32 %134, 1
  %135 = load i32, i32* %num_at_end, align 4
  %sub115 = sub nsw i32 %sub114, %135
  %m_nskip116 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %136 = load i32, i32* %m_nskip116, align 4
  call void @_ZL13btSwapProblemPPfS_S_S_S_S_PiPbS1_iiiii(float** %123, float* %124, float* %125, float* %126, float* %127, float* %128, i32* %129, i8* %130, i32* %131, i32 %132, i32 %133, i32 %sub115, i32 %136, i32 1)
  %137 = load i32, i32* %num_at_end, align 4
  %inc117 = add nsw i32 %137, 1
  store i32 %inc117, i32* %num_at_end, align 4
  br label %if.end118

if.end118:                                        ; preds = %if.then103, %for.body100
  br label %for.inc119

for.inc119:                                       ; preds = %if.end118
  %138 = load i32, i32* %k96, align 4
  %dec = add nsw i32 %138, -1
  store i32 %dec, i32* %k96, align 4
  br label %for.cond98

for.end120:                                       ; preds = %for.cond98
  br label %if.end121

if.end121:                                        ; preds = %for.end120, %if.end88
  %139 = load %struct.btLCP*, %struct.btLCP** %retval, align 4
  ret %struct.btLCP* %139
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z9btSetZeroIfEvPT_i(float* %a, i32 %n) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %acurr = alloca float*, align 4
  %ncurr = alloca i32, align 4
  store float* %a, float** %a.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  store float* %0, float** %acurr, align 4
  %1 = load i32, i32* %n.addr, align 4
  store i32 %1, i32* %ncurr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %2 = load i32, i32* %ncurr, align 4
  %cmp = icmp ugt i32 %2, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %3 = load float*, float** %acurr, align 4
  %incdec.ptr = getelementptr inbounds float, float* %3, i32 1
  store float* %incdec.ptr, float** %acurr, align 4
  store float 0.000000e+00, float* %3, align 4
  %4 = load i32, i32* %ncurr, align 4
  %dec = add i32 %4, -1
  store i32 %dec, i32* %ncurr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZL13btSwapProblemPPfS_S_S_S_S_PiPbS1_iiiii(float** %A, float* %x, float* %b, float* %w, float* %lo, float* %hi, i32* %p, i8* %state, i32* %findex, i32 %n, i32 %i1, i32 %i2, i32 %nskip, i32 %do_fast_row_swaps) #2 {
entry:
  %A.addr = alloca float**, align 4
  %x.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %w.addr = alloca float*, align 4
  %lo.addr = alloca float*, align 4
  %hi.addr = alloca float*, align 4
  %p.addr = alloca i32*, align 4
  %state.addr = alloca i8*, align 4
  %findex.addr = alloca i32*, align 4
  %n.addr = alloca i32, align 4
  %i1.addr = alloca i32, align 4
  %i2.addr = alloca i32, align 4
  %nskip.addr = alloca i32, align 4
  %do_fast_row_swaps.addr = alloca i32, align 4
  %tmpr = alloca float, align 4
  %tmpi = alloca i32, align 4
  %tmpb = alloca i8, align 1
  store float** %A, float*** %A.addr, align 4
  store float* %x, float** %x.addr, align 4
  store float* %b, float** %b.addr, align 4
  store float* %w, float** %w.addr, align 4
  store float* %lo, float** %lo.addr, align 4
  store float* %hi, float** %hi.addr, align 4
  store i32* %p, i32** %p.addr, align 4
  store i8* %state, i8** %state.addr, align 4
  store i32* %findex, i32** %findex.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 %i1, i32* %i1.addr, align 4
  store i32 %i2, i32* %i2.addr, align 4
  store i32 %nskip, i32* %nskip.addr, align 4
  store i32 %do_fast_row_swaps, i32* %do_fast_row_swaps.addr, align 4
  %0 = load i32, i32* %i1.addr, align 4
  %1 = load i32, i32* %i2.addr, align 4
  %cmp = icmp eq i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %if.end38

if.end:                                           ; preds = %entry
  %2 = load float**, float*** %A.addr, align 4
  %3 = load i32, i32* %n.addr, align 4
  %4 = load i32, i32* %i1.addr, align 4
  %5 = load i32, i32* %i2.addr, align 4
  %6 = load i32, i32* %nskip.addr, align 4
  %7 = load i32, i32* %do_fast_row_swaps.addr, align 4
  call void @_ZL17btSwapRowsAndColsPPfiiiii(float** %2, i32 %3, i32 %4, i32 %5, i32 %6, i32 %7)
  %8 = load float*, float** %x.addr, align 4
  %9 = load i32, i32* %i1.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  %10 = load float, float* %arrayidx, align 4
  store float %10, float* %tmpr, align 4
  %11 = load float*, float** %x.addr, align 4
  %12 = load i32, i32* %i2.addr, align 4
  %arrayidx1 = getelementptr inbounds float, float* %11, i32 %12
  %13 = load float, float* %arrayidx1, align 4
  %14 = load float*, float** %x.addr, align 4
  %15 = load i32, i32* %i1.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %14, i32 %15
  store float %13, float* %arrayidx2, align 4
  %16 = load float, float* %tmpr, align 4
  %17 = load float*, float** %x.addr, align 4
  %18 = load i32, i32* %i2.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %17, i32 %18
  store float %16, float* %arrayidx3, align 4
  %19 = load float*, float** %b.addr, align 4
  %20 = load i32, i32* %i1.addr, align 4
  %arrayidx4 = getelementptr inbounds float, float* %19, i32 %20
  %21 = load float, float* %arrayidx4, align 4
  store float %21, float* %tmpr, align 4
  %22 = load float*, float** %b.addr, align 4
  %23 = load i32, i32* %i2.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %22, i32 %23
  %24 = load float, float* %arrayidx5, align 4
  %25 = load float*, float** %b.addr, align 4
  %26 = load i32, i32* %i1.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %25, i32 %26
  store float %24, float* %arrayidx6, align 4
  %27 = load float, float* %tmpr, align 4
  %28 = load float*, float** %b.addr, align 4
  %29 = load i32, i32* %i2.addr, align 4
  %arrayidx7 = getelementptr inbounds float, float* %28, i32 %29
  store float %27, float* %arrayidx7, align 4
  %30 = load float*, float** %w.addr, align 4
  %31 = load i32, i32* %i1.addr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %30, i32 %31
  %32 = load float, float* %arrayidx8, align 4
  store float %32, float* %tmpr, align 4
  %33 = load float*, float** %w.addr, align 4
  %34 = load i32, i32* %i2.addr, align 4
  %arrayidx9 = getelementptr inbounds float, float* %33, i32 %34
  %35 = load float, float* %arrayidx9, align 4
  %36 = load float*, float** %w.addr, align 4
  %37 = load i32, i32* %i1.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %36, i32 %37
  store float %35, float* %arrayidx10, align 4
  %38 = load float, float* %tmpr, align 4
  %39 = load float*, float** %w.addr, align 4
  %40 = load i32, i32* %i2.addr, align 4
  %arrayidx11 = getelementptr inbounds float, float* %39, i32 %40
  store float %38, float* %arrayidx11, align 4
  %41 = load float*, float** %lo.addr, align 4
  %42 = load i32, i32* %i1.addr, align 4
  %arrayidx12 = getelementptr inbounds float, float* %41, i32 %42
  %43 = load float, float* %arrayidx12, align 4
  store float %43, float* %tmpr, align 4
  %44 = load float*, float** %lo.addr, align 4
  %45 = load i32, i32* %i2.addr, align 4
  %arrayidx13 = getelementptr inbounds float, float* %44, i32 %45
  %46 = load float, float* %arrayidx13, align 4
  %47 = load float*, float** %lo.addr, align 4
  %48 = load i32, i32* %i1.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %47, i32 %48
  store float %46, float* %arrayidx14, align 4
  %49 = load float, float* %tmpr, align 4
  %50 = load float*, float** %lo.addr, align 4
  %51 = load i32, i32* %i2.addr, align 4
  %arrayidx15 = getelementptr inbounds float, float* %50, i32 %51
  store float %49, float* %arrayidx15, align 4
  %52 = load float*, float** %hi.addr, align 4
  %53 = load i32, i32* %i1.addr, align 4
  %arrayidx16 = getelementptr inbounds float, float* %52, i32 %53
  %54 = load float, float* %arrayidx16, align 4
  store float %54, float* %tmpr, align 4
  %55 = load float*, float** %hi.addr, align 4
  %56 = load i32, i32* %i2.addr, align 4
  %arrayidx17 = getelementptr inbounds float, float* %55, i32 %56
  %57 = load float, float* %arrayidx17, align 4
  %58 = load float*, float** %hi.addr, align 4
  %59 = load i32, i32* %i1.addr, align 4
  %arrayidx18 = getelementptr inbounds float, float* %58, i32 %59
  store float %57, float* %arrayidx18, align 4
  %60 = load float, float* %tmpr, align 4
  %61 = load float*, float** %hi.addr, align 4
  %62 = load i32, i32* %i2.addr, align 4
  %arrayidx19 = getelementptr inbounds float, float* %61, i32 %62
  store float %60, float* %arrayidx19, align 4
  %63 = load i32*, i32** %p.addr, align 4
  %64 = load i32, i32* %i1.addr, align 4
  %arrayidx20 = getelementptr inbounds i32, i32* %63, i32 %64
  %65 = load i32, i32* %arrayidx20, align 4
  store i32 %65, i32* %tmpi, align 4
  %66 = load i32*, i32** %p.addr, align 4
  %67 = load i32, i32* %i2.addr, align 4
  %arrayidx21 = getelementptr inbounds i32, i32* %66, i32 %67
  %68 = load i32, i32* %arrayidx21, align 4
  %69 = load i32*, i32** %p.addr, align 4
  %70 = load i32, i32* %i1.addr, align 4
  %arrayidx22 = getelementptr inbounds i32, i32* %69, i32 %70
  store i32 %68, i32* %arrayidx22, align 4
  %71 = load i32, i32* %tmpi, align 4
  %72 = load i32*, i32** %p.addr, align 4
  %73 = load i32, i32* %i2.addr, align 4
  %arrayidx23 = getelementptr inbounds i32, i32* %72, i32 %73
  store i32 %71, i32* %arrayidx23, align 4
  %74 = load i8*, i8** %state.addr, align 4
  %75 = load i32, i32* %i1.addr, align 4
  %arrayidx24 = getelementptr inbounds i8, i8* %74, i32 %75
  %76 = load i8, i8* %arrayidx24, align 1
  %tobool = trunc i8 %76 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %tmpb, align 1
  %77 = load i8*, i8** %state.addr, align 4
  %78 = load i32, i32* %i2.addr, align 4
  %arrayidx25 = getelementptr inbounds i8, i8* %77, i32 %78
  %79 = load i8, i8* %arrayidx25, align 1
  %tobool26 = trunc i8 %79 to i1
  %80 = load i8*, i8** %state.addr, align 4
  %81 = load i32, i32* %i1.addr, align 4
  %arrayidx27 = getelementptr inbounds i8, i8* %80, i32 %81
  %frombool28 = zext i1 %tobool26 to i8
  store i8 %frombool28, i8* %arrayidx27, align 1
  %82 = load i8, i8* %tmpb, align 1
  %tobool29 = trunc i8 %82 to i1
  %83 = load i8*, i8** %state.addr, align 4
  %84 = load i32, i32* %i2.addr, align 4
  %arrayidx30 = getelementptr inbounds i8, i8* %83, i32 %84
  %frombool31 = zext i1 %tobool29 to i8
  store i8 %frombool31, i8* %arrayidx30, align 1
  %85 = load i32*, i32** %findex.addr, align 4
  %tobool32 = icmp ne i32* %85, null
  br i1 %tobool32, label %if.then33, label %if.end38

if.then33:                                        ; preds = %if.end
  %86 = load i32*, i32** %findex.addr, align 4
  %87 = load i32, i32* %i1.addr, align 4
  %arrayidx34 = getelementptr inbounds i32, i32* %86, i32 %87
  %88 = load i32, i32* %arrayidx34, align 4
  store i32 %88, i32* %tmpi, align 4
  %89 = load i32*, i32** %findex.addr, align 4
  %90 = load i32, i32* %i2.addr, align 4
  %arrayidx35 = getelementptr inbounds i32, i32* %89, i32 %90
  %91 = load i32, i32* %arrayidx35, align 4
  %92 = load i32*, i32** %findex.addr, align 4
  %93 = load i32, i32* %i1.addr, align 4
  %arrayidx36 = getelementptr inbounds i32, i32* %92, i32 %93
  store i32 %91, i32* %arrayidx36, align 4
  %94 = load i32, i32* %tmpi, align 4
  %95 = load i32*, i32** %findex.addr, align 4
  %96 = load i32, i32* %i2.addr, align 4
  %arrayidx37 = getelementptr inbounds i32, i32* %95, i32 %96
  store i32 %94, i32* %arrayidx37, align 4
  br label %if.end38

if.end38:                                         ; preds = %if.then, %if.then33, %if.end
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline optnone
define hidden void @_ZN5btLCP15transfer_i_to_CEi(%struct.btLCP* %this, i32 %i) #2 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  %nC = alloca i32, align 4
  %Ltgt = alloca float*, align 4
  %ell = alloca float*, align 4
  %j = alloca i32, align 4
  %nC5 = alloca i32, align 4
  %nC20 = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_nC, align 4
  %cmp = icmp sgt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_nC2 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %1 = load i32, i32* %m_nC2, align 4
  store i32 %1, i32* %nC, align 4
  %m_L = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %2 = load float*, float** %m_L, align 4
  %3 = load i32, i32* %nC, align 4
  %m_nskip = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %4 = load i32, i32* %m_nskip, align 4
  %mul = mul nsw i32 %3, %4
  %add.ptr = getelementptr inbounds float, float* %2, i32 %mul
  store float* %add.ptr, float** %Ltgt, align 4
  %m_ell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 14
  %5 = load float*, float** %m_ell, align 4
  store float* %5, float** %ell, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %6 = load i32, i32* %j, align 4
  %7 = load i32, i32* %nC, align 4
  %cmp3 = icmp slt i32 %6, %7
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load float*, float** %ell, align 4
  %9 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds float, float* %8, i32 %9
  %10 = load float, float* %arrayidx, align 4
  %11 = load float*, float** %Ltgt, align 4
  %12 = load i32, i32* %j, align 4
  %arrayidx4 = getelementptr inbounds float, float* %11, i32 %12
  store float %10, float* %arrayidx4, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %13 = load i32, i32* %j, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_nC6 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %14 = load i32, i32* %m_nC6, align 4
  store i32 %14, i32* %nC5, align 4
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %15 = load float**, float*** %m_A, align 4
  %16 = load i32, i32* %i.addr, align 4
  %arrayidx7 = getelementptr inbounds float*, float** %15, i32 %16
  %17 = load float*, float** %arrayidx7, align 4
  %18 = load i32, i32* %i.addr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %17, i32 %18
  %19 = load float, float* %arrayidx8, align 4
  %m_ell9 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 14
  %20 = load float*, float** %m_ell9, align 4
  %m_Dell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %21 = load float*, float** %m_Dell, align 4
  %22 = load i32, i32* %nC5, align 4
  %call = call float @_Z10btLargeDotPKfS0_i(float* %20, float* %21, i32 %22)
  %sub = fsub float %19, %call
  %div = fdiv float 1.000000e+00, %sub
  %m_d = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %23 = load float*, float** %m_d, align 4
  %24 = load i32, i32* %nC5, align 4
  %arrayidx10 = getelementptr inbounds float, float* %23, i32 %24
  store float %div, float* %arrayidx10, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %m_A11 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %25 = load float**, float*** %m_A11, align 4
  %26 = load i32, i32* %i.addr, align 4
  %arrayidx12 = getelementptr inbounds float*, float** %25, i32 %26
  %27 = load float*, float** %arrayidx12, align 4
  %28 = load i32, i32* %i.addr, align 4
  %arrayidx13 = getelementptr inbounds float, float* %27, i32 %28
  %29 = load float, float* %arrayidx13, align 4
  %div14 = fdiv float 1.000000e+00, %29
  %m_d15 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %30 = load float*, float** %m_d15, align 4
  %arrayidx16 = getelementptr inbounds float, float* %30, i32 0
  store float %div14, float* %arrayidx16, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %for.end
  %m_A17 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %31 = load float**, float*** %m_A17, align 4
  %m_x = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %32 = load float*, float** %m_x, align 4
  %m_b = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 7
  %33 = load float*, float** %m_b, align 4
  %m_w = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %34 = load float*, float** %m_w, align 4
  %m_lo = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 9
  %35 = load float*, float** %m_lo, align 4
  %m_hi = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 10
  %36 = load float*, float** %m_hi, align 4
  %m_p = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %37 = load i32*, i32** %m_p, align 4
  %m_state = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 16
  %38 = load i8*, i8** %m_state, align 4
  %m_findex = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 17
  %39 = load i32*, i32** %m_findex, align 4
  %m_n = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %40 = load i32, i32* %m_n, align 4
  %m_nC18 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %41 = load i32, i32* %m_nC18, align 4
  %42 = load i32, i32* %i.addr, align 4
  %m_nskip19 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %43 = load i32, i32* %m_nskip19, align 4
  call void @_ZL13btSwapProblemPPfS_S_S_S_S_PiPbS1_iiiii(float** %31, float* %32, float* %33, float* %34, float* %35, float* %36, i32* %37, i8* %38, i32* %39, i32 %40, i32 %41, i32 %42, i32 %43, i32 1)
  %m_nC21 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %44 = load i32, i32* %m_nC21, align 4
  store i32 %44, i32* %nC20, align 4
  %45 = load i32, i32* %nC20, align 4
  %m_C = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %46 = load i32*, i32** %m_C, align 4
  %47 = load i32, i32* %nC20, align 4
  %arrayidx22 = getelementptr inbounds i32, i32* %46, i32 %47
  store i32 %45, i32* %arrayidx22, align 4
  %48 = load i32, i32* %nC20, align 4
  %add = add nsw i32 %48, 1
  %m_nC23 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  store i32 %add, i32* %m_nC23, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z10btLargeDotPKfS0_i(float* %a, float* %b, i32 %n) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %p0 = alloca float, align 4
  %q0 = alloca float, align 4
  %m0 = alloca float, align 4
  %p1 = alloca float, align 4
  %q1 = alloca float, align 4
  %m1 = alloca float, align 4
  %sum = alloca float, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store float 0.000000e+00, float* %sum, align 4
  %0 = load i32, i32* %n.addr, align 4
  %sub = sub nsw i32 %0, 2
  store i32 %sub, i32* %n.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32, i32* %n.addr, align 4
  %cmp = icmp sge i32 %1, 0
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %2 = load float*, float** %a.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %2, i32 0
  %3 = load float, float* %arrayidx, align 4
  store float %3, float* %p0, align 4
  %4 = load float*, float** %b.addr, align 4
  %arrayidx1 = getelementptr inbounds float, float* %4, i32 0
  %5 = load float, float* %arrayidx1, align 4
  store float %5, float* %q0, align 4
  %6 = load float, float* %p0, align 4
  %7 = load float, float* %q0, align 4
  %mul = fmul float %6, %7
  store float %mul, float* %m0, align 4
  %8 = load float*, float** %a.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %8, i32 1
  %9 = load float, float* %arrayidx2, align 4
  store float %9, float* %p1, align 4
  %10 = load float*, float** %b.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %10, i32 1
  %11 = load float, float* %arrayidx3, align 4
  store float %11, float* %q1, align 4
  %12 = load float, float* %p1, align 4
  %13 = load float, float* %q1, align 4
  %mul4 = fmul float %12, %13
  store float %mul4, float* %m1, align 4
  %14 = load float, float* %m0, align 4
  %15 = load float, float* %sum, align 4
  %add = fadd float %15, %14
  store float %add, float* %sum, align 4
  %16 = load float, float* %m1, align 4
  %17 = load float, float* %sum, align 4
  %add5 = fadd float %17, %16
  store float %add5, float* %sum, align 4
  %18 = load float*, float** %a.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %18, i32 2
  store float* %add.ptr, float** %a.addr, align 4
  %19 = load float*, float** %b.addr, align 4
  %add.ptr6 = getelementptr inbounds float, float* %19, i32 2
  store float* %add.ptr6, float** %b.addr, align 4
  %20 = load i32, i32* %n.addr, align 4
  %sub7 = sub nsw i32 %20, 2
  store i32 %sub7, i32* %n.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %21 = load i32, i32* %n.addr, align 4
  %add8 = add nsw i32 %21, 2
  store i32 %add8, i32* %n.addr, align 4
  br label %while.cond9

while.cond9:                                      ; preds = %while.body11, %while.end
  %22 = load i32, i32* %n.addr, align 4
  %cmp10 = icmp sgt i32 %22, 0
  br i1 %cmp10, label %while.body11, label %while.end15

while.body11:                                     ; preds = %while.cond9
  %23 = load float*, float** %a.addr, align 4
  %24 = load float, float* %23, align 4
  %25 = load float*, float** %b.addr, align 4
  %26 = load float, float* %25, align 4
  %mul12 = fmul float %24, %26
  %27 = load float, float* %sum, align 4
  %add13 = fadd float %27, %mul12
  store float %add13, float* %sum, align 4
  %28 = load float*, float** %a.addr, align 4
  %incdec.ptr = getelementptr inbounds float, float* %28, i32 1
  store float* %incdec.ptr, float** %a.addr, align 4
  %29 = load float*, float** %b.addr, align 4
  %incdec.ptr14 = getelementptr inbounds float, float* %29, i32 1
  store float* %incdec.ptr14, float** %b.addr, align 4
  %30 = load i32, i32* %n.addr, align 4
  %dec = add nsw i32 %30, -1
  store i32 %dec, i32* %n.addr, align 4
  br label %while.cond9

while.end15:                                      ; preds = %while.cond9
  %31 = load float, float* %sum, align 4
  ret float %31
}

; Function Attrs: noinline optnone
define hidden void @_ZN5btLCP22transfer_i_from_N_to_CEi(%struct.btLCP* %this, i32 %i) #2 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  %aptr = alloca float*, align 4
  %Dell = alloca float*, align 4
  %C = alloca i32*, align 4
  %nub = alloca i32, align 4
  %j = alloca i32, align 4
  %nC = alloca i32, align 4
  %nC17 = alloca i32, align 4
  %Ltgt = alloca float*, align 4
  %ell = alloca float*, align 4
  %Dell21 = alloca float*, align 4
  %d = alloca float*, align 4
  %j23 = alloca i32, align 4
  %nC35 = alloca i32, align 4
  %nC53 = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_nC, align 4
  %cmp = icmp sgt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %1 = load float**, float*** %m_A, align 4
  %2 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds float*, float** %1, i32 %2
  %3 = load float*, float** %arrayidx, align 4
  store float* %3, float** %aptr, align 4
  %m_Dell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %4 = load float*, float** %m_Dell, align 4
  store float* %4, float** %Dell, align 4
  %m_C = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %5 = load i32*, i32** %m_C, align 4
  store i32* %5, i32** %C, align 4
  %m_nub = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %6 = load i32, i32* %m_nub, align 4
  store i32 %6, i32* %nub, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %7 = load i32, i32* %j, align 4
  %8 = load i32, i32* %nub, align 4
  %cmp2 = icmp slt i32 %7, %8
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load float*, float** %aptr, align 4
  %10 = load i32, i32* %j, align 4
  %arrayidx3 = getelementptr inbounds float, float* %9, i32 %10
  %11 = load float, float* %arrayidx3, align 4
  %12 = load float*, float** %Dell, align 4
  %13 = load i32, i32* %j, align 4
  %arrayidx4 = getelementptr inbounds float, float* %12, i32 %13
  store float %11, float* %arrayidx4, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %j, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_nC5 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %15 = load i32, i32* %m_nC5, align 4
  store i32 %15, i32* %nC, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %for.end
  %16 = load i32, i32* %j, align 4
  %17 = load i32, i32* %nC, align 4
  %cmp7 = icmp slt i32 %16, %17
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %18 = load float*, float** %aptr, align 4
  %19 = load i32*, i32** %C, align 4
  %20 = load i32, i32* %j, align 4
  %arrayidx9 = getelementptr inbounds i32, i32* %19, i32 %20
  %21 = load i32, i32* %arrayidx9, align 4
  %arrayidx10 = getelementptr inbounds float, float* %18, i32 %21
  %22 = load float, float* %arrayidx10, align 4
  %23 = load float*, float** %Dell, align 4
  %24 = load i32, i32* %j, align 4
  %arrayidx11 = getelementptr inbounds float, float* %23, i32 %24
  store float %22, float* %arrayidx11, align 4
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %25 = load i32, i32* %j, align 4
  %inc13 = add nsw i32 %25, 1
  store i32 %inc13, i32* %j, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  %m_L = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %26 = load float*, float** %m_L, align 4
  %m_Dell15 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %27 = load float*, float** %m_Dell15, align 4
  %m_nC16 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %28 = load i32, i32* %m_nC16, align 4
  %m_nskip = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %29 = load i32, i32* %m_nskip, align 4
  call void @_Z9btSolveL1PKfPfii(float* %26, float* %27, i32 %28, i32 %29)
  %m_nC18 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %30 = load i32, i32* %m_nC18, align 4
  store i32 %30, i32* %nC17, align 4
  %m_L19 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %31 = load float*, float** %m_L19, align 4
  %32 = load i32, i32* %nC17, align 4
  %m_nskip20 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %33 = load i32, i32* %m_nskip20, align 4
  %mul = mul nsw i32 %32, %33
  %add.ptr = getelementptr inbounds float, float* %31, i32 %mul
  store float* %add.ptr, float** %Ltgt, align 4
  %m_ell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 14
  %34 = load float*, float** %m_ell, align 4
  store float* %34, float** %ell, align 4
  %m_Dell22 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %35 = load float*, float** %m_Dell22, align 4
  store float* %35, float** %Dell21, align 4
  %m_d = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %36 = load float*, float** %m_d, align 4
  store float* %36, float** %d, align 4
  store i32 0, i32* %j23, align 4
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc32, %for.end14
  %37 = load i32, i32* %j23, align 4
  %38 = load i32, i32* %nC17, align 4
  %cmp25 = icmp slt i32 %37, %38
  br i1 %cmp25, label %for.body26, label %for.end34

for.body26:                                       ; preds = %for.cond24
  %39 = load float*, float** %Dell21, align 4
  %40 = load i32, i32* %j23, align 4
  %arrayidx27 = getelementptr inbounds float, float* %39, i32 %40
  %41 = load float, float* %arrayidx27, align 4
  %42 = load float*, float** %d, align 4
  %43 = load i32, i32* %j23, align 4
  %arrayidx28 = getelementptr inbounds float, float* %42, i32 %43
  %44 = load float, float* %arrayidx28, align 4
  %mul29 = fmul float %41, %44
  %45 = load float*, float** %ell, align 4
  %46 = load i32, i32* %j23, align 4
  %arrayidx30 = getelementptr inbounds float, float* %45, i32 %46
  store float %mul29, float* %arrayidx30, align 4
  %47 = load float*, float** %Ltgt, align 4
  %48 = load i32, i32* %j23, align 4
  %arrayidx31 = getelementptr inbounds float, float* %47, i32 %48
  store float %mul29, float* %arrayidx31, align 4
  br label %for.inc32

for.inc32:                                        ; preds = %for.body26
  %49 = load i32, i32* %j23, align 4
  %inc33 = add nsw i32 %49, 1
  store i32 %inc33, i32* %j23, align 4
  br label %for.cond24

for.end34:                                        ; preds = %for.cond24
  %m_nC36 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %50 = load i32, i32* %m_nC36, align 4
  store i32 %50, i32* %nC35, align 4
  %m_A37 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %51 = load float**, float*** %m_A37, align 4
  %52 = load i32, i32* %i.addr, align 4
  %arrayidx38 = getelementptr inbounds float*, float** %51, i32 %52
  %53 = load float*, float** %arrayidx38, align 4
  %54 = load i32, i32* %i.addr, align 4
  %arrayidx39 = getelementptr inbounds float, float* %53, i32 %54
  %55 = load float, float* %arrayidx39, align 4
  %m_ell40 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 14
  %56 = load float*, float** %m_ell40, align 4
  %m_Dell41 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %57 = load float*, float** %m_Dell41, align 4
  %58 = load i32, i32* %nC35, align 4
  %call = call float @_Z10btLargeDotPKfS0_i(float* %56, float* %57, i32 %58)
  %sub = fsub float %55, %call
  %div = fdiv float 1.000000e+00, %sub
  %m_d42 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %59 = load float*, float** %m_d42, align 4
  %60 = load i32, i32* %nC35, align 4
  %arrayidx43 = getelementptr inbounds float, float* %59, i32 %60
  store float %div, float* %arrayidx43, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %m_A44 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %61 = load float**, float*** %m_A44, align 4
  %62 = load i32, i32* %i.addr, align 4
  %arrayidx45 = getelementptr inbounds float*, float** %61, i32 %62
  %63 = load float*, float** %arrayidx45, align 4
  %64 = load i32, i32* %i.addr, align 4
  %arrayidx46 = getelementptr inbounds float, float* %63, i32 %64
  %65 = load float, float* %arrayidx46, align 4
  %div47 = fdiv float 1.000000e+00, %65
  %m_d48 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %66 = load float*, float** %m_d48, align 4
  %arrayidx49 = getelementptr inbounds float, float* %66, i32 0
  store float %div47, float* %arrayidx49, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %for.end34
  %m_A50 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %67 = load float**, float*** %m_A50, align 4
  %m_x = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %68 = load float*, float** %m_x, align 4
  %m_b = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 7
  %69 = load float*, float** %m_b, align 4
  %m_w = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %70 = load float*, float** %m_w, align 4
  %m_lo = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 9
  %71 = load float*, float** %m_lo, align 4
  %m_hi = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 10
  %72 = load float*, float** %m_hi, align 4
  %m_p = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %73 = load i32*, i32** %m_p, align 4
  %m_state = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 16
  %74 = load i8*, i8** %m_state, align 4
  %m_findex = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 17
  %75 = load i32*, i32** %m_findex, align 4
  %m_n = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %76 = load i32, i32* %m_n, align 4
  %m_nC51 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %77 = load i32, i32* %m_nC51, align 4
  %78 = load i32, i32* %i.addr, align 4
  %m_nskip52 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %79 = load i32, i32* %m_nskip52, align 4
  call void @_ZL13btSwapProblemPPfS_S_S_S_S_PiPbS1_iiiii(float** %67, float* %68, float* %69, float* %70, float* %71, float* %72, i32* %73, i8* %74, i32* %75, i32 %76, i32 %77, i32 %78, i32 %79, i32 1)
  %m_nC54 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %80 = load i32, i32* %m_nC54, align 4
  store i32 %80, i32* %nC53, align 4
  %81 = load i32, i32* %nC53, align 4
  %m_C55 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %82 = load i32*, i32** %m_C55, align 4
  %83 = load i32, i32* %nC53, align 4
  %arrayidx56 = getelementptr inbounds i32, i32* %82, i32 %83
  store i32 %81, i32* %arrayidx56, align 4
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %84 = load i32, i32* %m_nN, align 4
  %dec = add nsw i32 %84, -1
  store i32 %dec, i32* %m_nN, align 4
  %85 = load i32, i32* %nC53, align 4
  %add = add nsw i32 %85, 1
  %m_nC57 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  store i32 %add, i32* %m_nC57, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_Z14btRemoveRowColPfiii(float* %A, i32 %n, i32 %nskip, i32 %r) #1 {
entry:
  %A.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %nskip.addr = alloca i32, align 4
  %r.addr = alloca i32, align 4
  %move_size = alloca i32, align 4
  %Adst = alloca float*, align 4
  %i = alloca i32, align 4
  %Asrc = alloca float*, align 4
  %cpy_size = alloca i32, align 4
  %Adst9 = alloca float*, align 4
  %i12 = alloca i32, align 4
  %Asrc17 = alloca float*, align 4
  %cpy_size23 = alloca i32, align 4
  %Adst27 = alloca float*, align 4
  %i30 = alloca i32, align 4
  %Asrc35 = alloca float*, align 4
  store float* %A, float** %A.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 %nskip, i32* %nskip.addr, align 4
  store i32 %r, i32* %r.addr, align 4
  %0 = load i32, i32* %r.addr, align 4
  %1 = load i32, i32* %n.addr, align 4
  %sub = sub nsw i32 %1, 1
  %cmp = icmp sge i32 %0, %sub
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %for.end41

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %r.addr, align 4
  %cmp1 = icmp sgt i32 %2, 0
  br i1 %cmp1, label %if.then2, label %if.end22

if.then2:                                         ; preds = %if.end
  %3 = load i32, i32* %n.addr, align 4
  %4 = load i32, i32* %r.addr, align 4
  %sub3 = sub nsw i32 %3, %4
  %sub4 = sub nsw i32 %sub3, 1
  %mul = mul i32 %sub4, 4
  store i32 %mul, i32* %move_size, align 4
  %5 = load float*, float** %A.addr, align 4
  %6 = load i32, i32* %r.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %5, i32 %6
  store float* %add.ptr, float** %Adst, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then2
  %7 = load i32, i32* %i, align 4
  %8 = load i32, i32* %r.addr, align 4
  %cmp5 = icmp slt i32 %7, %8
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load float*, float** %Adst, align 4
  %add.ptr6 = getelementptr inbounds float, float* %9, i32 1
  store float* %add.ptr6, float** %Asrc, align 4
  %10 = load float*, float** %Adst, align 4
  %11 = bitcast float* %10 to i8*
  %12 = load float*, float** %Asrc, align 4
  %13 = bitcast float* %12 to i8*
  %14 = load i32, i32* %move_size, align 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %13, i32 %14, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %nskip.addr, align 4
  %16 = load float*, float** %Adst, align 4
  %add.ptr7 = getelementptr inbounds float, float* %16, i32 %15
  store float* %add.ptr7, float** %Adst, align 4
  %17 = load i32, i32* %i, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %18 = load i32, i32* %r.addr, align 4
  %mul8 = mul i32 %18, 4
  store i32 %mul8, i32* %cpy_size, align 4
  %19 = load float*, float** %A.addr, align 4
  %20 = load i32, i32* %r.addr, align 4
  %21 = load i32, i32* %nskip.addr, align 4
  %mul10 = mul nsw i32 %20, %21
  %add.ptr11 = getelementptr inbounds float, float* %19, i32 %mul10
  store float* %add.ptr11, float** %Adst9, align 4
  %22 = load i32, i32* %r.addr, align 4
  store i32 %22, i32* %i12, align 4
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc19, %for.end
  %23 = load i32, i32* %i12, align 4
  %24 = load i32, i32* %n.addr, align 4
  %sub14 = sub nsw i32 %24, 1
  %cmp15 = icmp slt i32 %23, %sub14
  br i1 %cmp15, label %for.body16, label %for.end21

for.body16:                                       ; preds = %for.cond13
  %25 = load float*, float** %Adst9, align 4
  %26 = load i32, i32* %nskip.addr, align 4
  %add.ptr18 = getelementptr inbounds float, float* %25, i32 %26
  store float* %add.ptr18, float** %Asrc17, align 4
  %27 = load float*, float** %Adst9, align 4
  %28 = bitcast float* %27 to i8*
  %29 = load float*, float** %Asrc17, align 4
  %30 = bitcast float* %29 to i8*
  %31 = load i32, i32* %cpy_size, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %30, i32 %31, i1 false)
  %32 = load float*, float** %Asrc17, align 4
  store float* %32, float** %Adst9, align 4
  br label %for.inc19

for.inc19:                                        ; preds = %for.body16
  %33 = load i32, i32* %i12, align 4
  %inc20 = add nsw i32 %33, 1
  store i32 %inc20, i32* %i12, align 4
  br label %for.cond13

for.end21:                                        ; preds = %for.cond13
  br label %if.end22

if.end22:                                         ; preds = %for.end21, %if.end
  %34 = load i32, i32* %n.addr, align 4
  %35 = load i32, i32* %r.addr, align 4
  %sub24 = sub nsw i32 %34, %35
  %sub25 = sub nsw i32 %sub24, 1
  %mul26 = mul i32 %sub25, 4
  store i32 %mul26, i32* %cpy_size23, align 4
  %36 = load float*, float** %A.addr, align 4
  %37 = load i32, i32* %r.addr, align 4
  %38 = load i32, i32* %nskip.addr, align 4
  %add = add nsw i32 %38, 1
  %mul28 = mul nsw i32 %37, %add
  %add.ptr29 = getelementptr inbounds float, float* %36, i32 %mul28
  store float* %add.ptr29, float** %Adst27, align 4
  %39 = load i32, i32* %r.addr, align 4
  store i32 %39, i32* %i30, align 4
  br label %for.cond31

for.cond31:                                       ; preds = %for.inc39, %if.end22
  %40 = load i32, i32* %i30, align 4
  %41 = load i32, i32* %n.addr, align 4
  %sub32 = sub nsw i32 %41, 1
  %cmp33 = icmp slt i32 %40, %sub32
  br i1 %cmp33, label %for.body34, label %for.end41

for.body34:                                       ; preds = %for.cond31
  %42 = load float*, float** %Adst27, align 4
  %43 = load i32, i32* %nskip.addr, align 4
  %add36 = add nsw i32 %43, 1
  %add.ptr37 = getelementptr inbounds float, float* %42, i32 %add36
  store float* %add.ptr37, float** %Asrc35, align 4
  %44 = load float*, float** %Adst27, align 4
  %45 = bitcast float* %44 to i8*
  %46 = load float*, float** %Asrc35, align 4
  %47 = bitcast float* %46 to i8*
  %48 = load i32, i32* %cpy_size23, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %45, i8* align 4 %47, i32 %48, i1 false)
  %49 = load float*, float** %Asrc35, align 4
  %add.ptr38 = getelementptr inbounds float, float* %49, i32 -1
  store float* %add.ptr38, float** %Adst27, align 4
  br label %for.inc39

for.inc39:                                        ; preds = %for.body34
  %50 = load i32, i32* %i30, align 4
  %inc40 = add nsw i32 %50, 1
  store i32 %inc40, i32* %i30, align 4
  br label %for.cond31

for.end41:                                        ; preds = %if.then, %for.cond31
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memmove.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline optnone
define hidden void @_Z11btLDLTAddTLPfS_PKfiiR20btAlignedObjectArrayIfE(float* %L, float* %d, float* %a, i32 %n, i32 %nskip, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %scratch) #2 {
entry:
  %L.addr = alloca float*, align 4
  %d.addr = alloca float*, align 4
  %a.addr = alloca float*, align 4
  %n.addr = alloca i32, align 4
  %nskip.addr = alloca i32, align 4
  %scratch.addr = alloca %class.btAlignedObjectArray*, align 4
  %ref.tmp = alloca float, align 4
  %W1 = alloca float*, align 4
  %W2 = alloca float*, align 4
  %j = alloca i32, align 4
  %W11 = alloca float, align 4
  %W21 = alloca float, align 4
  %alpha1 = alloca float, align 4
  %alpha2 = alloca float, align 4
  %dee = alloca float, align 4
  %alphanew = alloca float, align 4
  %gamma1 = alloca float, align 4
  %k1 = alloca float, align 4
  %k2 = alloca float, align 4
  %ll = alloca float*, align 4
  %p = alloca i32, align 4
  %Wp = alloca float, align 4
  %ell = alloca float, align 4
  %ll44 = alloca float*, align 4
  %j47 = alloca i32, align 4
  %k151 = alloca float, align 4
  %k253 = alloca float, align 4
  %dee55 = alloca float, align 4
  %alphanew57 = alloca float, align 4
  %gamma162 = alloca float, align 4
  %gamma2 = alloca float, align 4
  %l = alloca float*, align 4
  %p73 = alloca i32, align 4
  %ell78 = alloca float, align 4
  %Wp79 = alloca float, align 4
  store float* %L, float** %L.addr, align 4
  store float* %d, float** %d.addr, align 4
  store float* %a, float** %a.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 %nskip, i32* %nskip.addr, align 4
  store %class.btAlignedObjectArray* %scratch, %class.btAlignedObjectArray** %scratch.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %cmp = icmp slt i32 %0, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %for.end100

if.end:                                           ; preds = %entry
  %1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %scratch.addr, align 4
  %2 = load i32, i32* %nskip.addr, align 4
  %mul = mul nsw i32 2, %2
  store float 0.000000e+00, float* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %1, i32 %mul, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %scratch.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %3, i32 0)
  store float* %call, float** %W1, align 4
  %4 = load float*, float** %W1, align 4
  %5 = load i32, i32* %nskip.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %4, i32 %5
  store float* %add.ptr, float** %W2, align 4
  %6 = load float*, float** %W1, align 4
  %arrayidx = getelementptr inbounds float, float* %6, i32 0
  store float 0.000000e+00, float* %arrayidx, align 4
  %7 = load float*, float** %W2, align 4
  %arrayidx1 = getelementptr inbounds float, float* %7, i32 0
  store float 0.000000e+00, float* %arrayidx1, align 4
  store i32 1, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %8 = load i32, i32* %j, align 4
  %9 = load i32, i32* %n.addr, align 4
  %cmp2 = icmp slt i32 %8, %9
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load float*, float** %a.addr, align 4
  %11 = load i32, i32* %j, align 4
  %arrayidx3 = getelementptr inbounds float, float* %10, i32 %11
  %12 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %12, 0x3FE6A09E60000000
  %13 = load float*, float** %W2, align 4
  %14 = load i32, i32* %j, align 4
  %arrayidx5 = getelementptr inbounds float, float* %13, i32 %14
  store float %mul4, float* %arrayidx5, align 4
  %15 = load float*, float** %W1, align 4
  %16 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds float, float* %15, i32 %16
  store float %mul4, float* %arrayidx6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %j, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %18 = load float*, float** %a.addr, align 4
  %arrayidx7 = getelementptr inbounds float, float* %18, i32 0
  %19 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float 5.000000e-01, %19
  %add = fadd float %mul8, 1.000000e+00
  %mul9 = fmul float %add, 0x3FE6A09E60000000
  store float %mul9, float* %W11, align 4
  %20 = load float*, float** %a.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %20, i32 0
  %21 = load float, float* %arrayidx10, align 4
  %mul11 = fmul float 5.000000e-01, %21
  %sub = fsub float %mul11, 1.000000e+00
  %mul12 = fmul float %sub, 0x3FE6A09E60000000
  store float %mul12, float* %W21, align 4
  store float 1.000000e+00, float* %alpha1, align 4
  store float 1.000000e+00, float* %alpha2, align 4
  %22 = load float*, float** %d.addr, align 4
  %arrayidx13 = getelementptr inbounds float, float* %22, i32 0
  %23 = load float, float* %arrayidx13, align 4
  store float %23, float* %dee, align 4
  %24 = load float, float* %alpha1, align 4
  %25 = load float, float* %W11, align 4
  %26 = load float, float* %W11, align 4
  %mul14 = fmul float %25, %26
  %27 = load float, float* %dee, align 4
  %mul15 = fmul float %mul14, %27
  %add16 = fadd float %24, %mul15
  store float %add16, float* %alphanew, align 4
  %28 = load float, float* %alphanew, align 4
  %29 = load float, float* %dee, align 4
  %div = fdiv float %29, %28
  store float %div, float* %dee, align 4
  %30 = load float, float* %W11, align 4
  %31 = load float, float* %dee, align 4
  %mul17 = fmul float %30, %31
  store float %mul17, float* %gamma1, align 4
  %32 = load float, float* %alpha1, align 4
  %33 = load float, float* %dee, align 4
  %mul18 = fmul float %33, %32
  store float %mul18, float* %dee, align 4
  %34 = load float, float* %alphanew, align 4
  store float %34, float* %alpha1, align 4
  %35 = load float, float* %alpha2, align 4
  %36 = load float, float* %W21, align 4
  %37 = load float, float* %W21, align 4
  %mul19 = fmul float %36, %37
  %38 = load float, float* %dee, align 4
  %mul20 = fmul float %mul19, %38
  %sub21 = fsub float %35, %mul20
  store float %sub21, float* %alphanew, align 4
  %39 = load float, float* %alphanew, align 4
  %40 = load float, float* %dee, align 4
  %div22 = fdiv float %40, %39
  store float %div22, float* %dee, align 4
  %41 = load float, float* %alphanew, align 4
  store float %41, float* %alpha2, align 4
  %42 = load float, float* %W21, align 4
  %43 = load float, float* %gamma1, align 4
  %mul23 = fmul float %42, %43
  %sub24 = fsub float 1.000000e+00, %mul23
  store float %sub24, float* %k1, align 4
  %44 = load float, float* %W21, align 4
  %45 = load float, float* %gamma1, align 4
  %mul25 = fmul float %44, %45
  %46 = load float, float* %W11, align 4
  %mul26 = fmul float %mul25, %46
  %47 = load float, float* %W21, align 4
  %sub27 = fsub float %mul26, %47
  store float %sub27, float* %k2, align 4
  %48 = load float*, float** %L.addr, align 4
  %49 = load i32, i32* %nskip.addr, align 4
  %add.ptr28 = getelementptr inbounds float, float* %48, i32 %49
  store float* %add.ptr28, float** %ll, align 4
  store i32 1, i32* %p, align 4
  br label %for.cond29

for.cond29:                                       ; preds = %for.inc40, %for.end
  %50 = load i32, i32* %p, align 4
  %51 = load i32, i32* %n.addr, align 4
  %cmp30 = icmp slt i32 %50, %51
  br i1 %cmp30, label %for.body31, label %for.end43

for.body31:                                       ; preds = %for.cond29
  %52 = load float*, float** %W1, align 4
  %53 = load i32, i32* %p, align 4
  %arrayidx32 = getelementptr inbounds float, float* %52, i32 %53
  %54 = load float, float* %arrayidx32, align 4
  store float %54, float* %Wp, align 4
  %55 = load float*, float** %ll, align 4
  %56 = load float, float* %55, align 4
  store float %56, float* %ell, align 4
  %57 = load float, float* %Wp, align 4
  %58 = load float, float* %W11, align 4
  %59 = load float, float* %ell, align 4
  %mul33 = fmul float %58, %59
  %sub34 = fsub float %57, %mul33
  %60 = load float*, float** %W1, align 4
  %61 = load i32, i32* %p, align 4
  %arrayidx35 = getelementptr inbounds float, float* %60, i32 %61
  store float %sub34, float* %arrayidx35, align 4
  %62 = load float, float* %k1, align 4
  %63 = load float, float* %Wp, align 4
  %mul36 = fmul float %62, %63
  %64 = load float, float* %k2, align 4
  %65 = load float, float* %ell, align 4
  %mul37 = fmul float %64, %65
  %add38 = fadd float %mul36, %mul37
  %66 = load float*, float** %W2, align 4
  %67 = load i32, i32* %p, align 4
  %arrayidx39 = getelementptr inbounds float, float* %66, i32 %67
  store float %add38, float* %arrayidx39, align 4
  br label %for.inc40

for.inc40:                                        ; preds = %for.body31
  %68 = load i32, i32* %nskip.addr, align 4
  %69 = load float*, float** %ll, align 4
  %add.ptr41 = getelementptr inbounds float, float* %69, i32 %68
  store float* %add.ptr41, float** %ll, align 4
  %70 = load i32, i32* %p, align 4
  %inc42 = add nsw i32 %70, 1
  store i32 %inc42, i32* %p, align 4
  br label %for.cond29

for.end43:                                        ; preds = %for.cond29
  %71 = load float*, float** %L.addr, align 4
  %72 = load i32, i32* %nskip.addr, align 4
  %add45 = add nsw i32 %72, 1
  %add.ptr46 = getelementptr inbounds float, float* %71, i32 %add45
  store float* %add.ptr46, float** %ll44, align 4
  store i32 1, i32* %j47, align 4
  br label %for.cond48

for.cond48:                                       ; preds = %for.inc96, %for.end43
  %73 = load i32, i32* %j47, align 4
  %74 = load i32, i32* %n.addr, align 4
  %cmp49 = icmp slt i32 %73, %74
  br i1 %cmp49, label %for.body50, label %for.end100

for.body50:                                       ; preds = %for.cond48
  %75 = load float*, float** %W1, align 4
  %76 = load i32, i32* %j47, align 4
  %arrayidx52 = getelementptr inbounds float, float* %75, i32 %76
  %77 = load float, float* %arrayidx52, align 4
  store float %77, float* %k151, align 4
  %78 = load float*, float** %W2, align 4
  %79 = load i32, i32* %j47, align 4
  %arrayidx54 = getelementptr inbounds float, float* %78, i32 %79
  %80 = load float, float* %arrayidx54, align 4
  store float %80, float* %k253, align 4
  %81 = load float*, float** %d.addr, align 4
  %82 = load i32, i32* %j47, align 4
  %arrayidx56 = getelementptr inbounds float, float* %81, i32 %82
  %83 = load float, float* %arrayidx56, align 4
  store float %83, float* %dee55, align 4
  %84 = load float, float* %alpha1, align 4
  %85 = load float, float* %k151, align 4
  %86 = load float, float* %k151, align 4
  %mul58 = fmul float %85, %86
  %87 = load float, float* %dee55, align 4
  %mul59 = fmul float %mul58, %87
  %add60 = fadd float %84, %mul59
  store float %add60, float* %alphanew57, align 4
  %88 = load float, float* %alphanew57, align 4
  %89 = load float, float* %dee55, align 4
  %div61 = fdiv float %89, %88
  store float %div61, float* %dee55, align 4
  %90 = load float, float* %k151, align 4
  %91 = load float, float* %dee55, align 4
  %mul63 = fmul float %90, %91
  store float %mul63, float* %gamma162, align 4
  %92 = load float, float* %alpha1, align 4
  %93 = load float, float* %dee55, align 4
  %mul64 = fmul float %93, %92
  store float %mul64, float* %dee55, align 4
  %94 = load float, float* %alphanew57, align 4
  store float %94, float* %alpha1, align 4
  %95 = load float, float* %alpha2, align 4
  %96 = load float, float* %k253, align 4
  %97 = load float, float* %k253, align 4
  %mul65 = fmul float %96, %97
  %98 = load float, float* %dee55, align 4
  %mul66 = fmul float %mul65, %98
  %sub67 = fsub float %95, %mul66
  store float %sub67, float* %alphanew57, align 4
  %99 = load float, float* %alphanew57, align 4
  %100 = load float, float* %dee55, align 4
  %div68 = fdiv float %100, %99
  store float %div68, float* %dee55, align 4
  %101 = load float, float* %k253, align 4
  %102 = load float, float* %dee55, align 4
  %mul69 = fmul float %101, %102
  store float %mul69, float* %gamma2, align 4
  %103 = load float, float* %alpha2, align 4
  %104 = load float, float* %dee55, align 4
  %mul70 = fmul float %104, %103
  store float %mul70, float* %dee55, align 4
  %105 = load float, float* %dee55, align 4
  %106 = load float*, float** %d.addr, align 4
  %107 = load i32, i32* %j47, align 4
  %arrayidx71 = getelementptr inbounds float, float* %106, i32 %107
  store float %105, float* %arrayidx71, align 4
  %108 = load float, float* %alphanew57, align 4
  store float %108, float* %alpha2, align 4
  %109 = load float*, float** %ll44, align 4
  %110 = load i32, i32* %nskip.addr, align 4
  %add.ptr72 = getelementptr inbounds float, float* %109, i32 %110
  store float* %add.ptr72, float** %l, align 4
  %111 = load i32, i32* %j47, align 4
  %add74 = add nsw i32 %111, 1
  store i32 %add74, i32* %p73, align 4
  br label %for.cond75

for.cond75:                                       ; preds = %for.inc92, %for.body50
  %112 = load i32, i32* %p73, align 4
  %113 = load i32, i32* %n.addr, align 4
  %cmp76 = icmp slt i32 %112, %113
  br i1 %cmp76, label %for.body77, label %for.end95

for.body77:                                       ; preds = %for.cond75
  %114 = load float*, float** %l, align 4
  %115 = load float, float* %114, align 4
  store float %115, float* %ell78, align 4
  %116 = load float*, float** %W1, align 4
  %117 = load i32, i32* %p73, align 4
  %arrayidx80 = getelementptr inbounds float, float* %116, i32 %117
  %118 = load float, float* %arrayidx80, align 4
  %119 = load float, float* %k151, align 4
  %120 = load float, float* %ell78, align 4
  %mul81 = fmul float %119, %120
  %sub82 = fsub float %118, %mul81
  store float %sub82, float* %Wp79, align 4
  %121 = load float, float* %gamma162, align 4
  %122 = load float, float* %Wp79, align 4
  %mul83 = fmul float %121, %122
  %123 = load float, float* %ell78, align 4
  %add84 = fadd float %123, %mul83
  store float %add84, float* %ell78, align 4
  %124 = load float, float* %Wp79, align 4
  %125 = load float*, float** %W1, align 4
  %126 = load i32, i32* %p73, align 4
  %arrayidx85 = getelementptr inbounds float, float* %125, i32 %126
  store float %124, float* %arrayidx85, align 4
  %127 = load float*, float** %W2, align 4
  %128 = load i32, i32* %p73, align 4
  %arrayidx86 = getelementptr inbounds float, float* %127, i32 %128
  %129 = load float, float* %arrayidx86, align 4
  %130 = load float, float* %k253, align 4
  %131 = load float, float* %ell78, align 4
  %mul87 = fmul float %130, %131
  %sub88 = fsub float %129, %mul87
  store float %sub88, float* %Wp79, align 4
  %132 = load float, float* %gamma2, align 4
  %133 = load float, float* %Wp79, align 4
  %mul89 = fmul float %132, %133
  %134 = load float, float* %ell78, align 4
  %sub90 = fsub float %134, %mul89
  store float %sub90, float* %ell78, align 4
  %135 = load float, float* %Wp79, align 4
  %136 = load float*, float** %W2, align 4
  %137 = load i32, i32* %p73, align 4
  %arrayidx91 = getelementptr inbounds float, float* %136, i32 %137
  store float %135, float* %arrayidx91, align 4
  %138 = load float, float* %ell78, align 4
  %139 = load float*, float** %l, align 4
  store float %138, float* %139, align 4
  br label %for.inc92

for.inc92:                                        ; preds = %for.body77
  %140 = load i32, i32* %nskip.addr, align 4
  %141 = load float*, float** %l, align 4
  %add.ptr93 = getelementptr inbounds float, float* %141, i32 %140
  store float* %add.ptr93, float** %l, align 4
  %142 = load i32, i32* %p73, align 4
  %inc94 = add nsw i32 %142, 1
  store i32 %inc94, i32* %p73, align 4
  br label %for.cond75

for.end95:                                        ; preds = %for.cond75
  br label %for.inc96

for.inc96:                                        ; preds = %for.end95
  %143 = load i32, i32* %nskip.addr, align 4
  %add97 = add nsw i32 %143, 1
  %144 = load float*, float** %ll44, align 4
  %add.ptr98 = getelementptr inbounds float, float* %144, i32 %add97
  store float* %add.ptr98, float** %ll44, align 4
  %145 = load i32, i32* %j47, align 4
  %inc99 = add nsw i32 %145, 1
  store i32 %inc99, i32* %j47, align 4
  br label %for.cond48

for.end100:                                       ; preds = %if.then, %for.cond48
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store float* %fillData, float** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load float*, float** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load float*, float** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds float, float* %14, i32 %15
  %16 = bitcast float* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to float*
  %18 = load float*, float** %fillData.addr, align 4
  %19 = load float, float* %18, align 4
  store float %19, float* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_Z12btLDLTRemovePPfPKiS_S_iiiiR20btAlignedObjectArrayIfE(float** %A, i32* %p, float* %L, float* %d, i32 %n1, i32 %n2, i32 %r, i32 %nskip, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %scratch) #2 {
entry:
  %A.addr = alloca float**, align 4
  %p.addr = alloca i32*, align 4
  %L.addr = alloca float*, align 4
  %d.addr = alloca float*, align 4
  %n1.addr = alloca i32, align 4
  %n2.addr = alloca i32, align 4
  %r.addr = alloca i32, align 4
  %nskip.addr = alloca i32, align 4
  %scratch.addr = alloca %class.btAlignedObjectArray*, align 4
  %LDLTAddTL_size = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  %tmp = alloca float*, align 4
  %a = alloca float*, align 4
  %p_0 = alloca i32, align 4
  %i = alloca i32, align 4
  %t = alloca float*, align 4
  %Lcurr = alloca float*, align 4
  %i20 = alloca i32, align 4
  %a29 = alloca float*, align 4
  %Lcurr31 = alloca float*, align 4
  %pp_r = alloca i32*, align 4
  %p_r = alloca i32, align 4
  %n2_minus_r = alloca i32, align 4
  %i36 = alloca i32, align 4
  store float** %A, float*** %A.addr, align 4
  store i32* %p, i32** %p.addr, align 4
  store float* %L, float** %L.addr, align 4
  store float* %d, float** %d.addr, align 4
  store i32 %n1, i32* %n1.addr, align 4
  store i32 %n2, i32* %n2.addr, align 4
  store i32 %r, i32* %r.addr, align 4
  store i32 %nskip, i32* %nskip.addr, align 4
  store %class.btAlignedObjectArray* %scratch, %class.btAlignedObjectArray** %scratch.addr, align 4
  %0 = load i32, i32* %r.addr, align 4
  %1 = load i32, i32* %n2.addr, align 4
  %sub = sub nsw i32 %1, 1
  %cmp = icmp eq i32 %0, %sub
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  br label %if.end76

if.else:                                          ; preds = %entry
  %2 = load i32, i32* %nskip.addr, align 4
  %call = call i32 @_Z29btEstimateLDLTAddTLTmpbufSizei(i32 %2)
  store i32 %call, i32* %LDLTAddTL_size, align 4
  %3 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %scratch.addr, align 4
  %4 = load i32, i32* %nskip.addr, align 4
  %mul = mul nsw i32 %4, 2
  %5 = load i32, i32* %n2.addr, align 4
  %add = add nsw i32 %mul, %5
  store float 0.000000e+00, float* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %3, i32 %add, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %6 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %scratch.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %6, i32 0)
  store float* %call1, float** %tmp, align 4
  %7 = load i32, i32* %r.addr, align 4
  %cmp2 = icmp eq i32 %7, 0
  br i1 %cmp2, label %if.then3, label %if.else16

if.then3:                                         ; preds = %if.else
  %8 = load float*, float** %tmp, align 4
  %9 = bitcast float* %8 to i8*
  %10 = load i32, i32* %LDLTAddTL_size, align 4
  %add.ptr = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = bitcast i8* %add.ptr to float*
  store float* %11, float** %a, align 4
  %12 = load i32*, i32** %p.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %12, i32 0
  %13 = load i32, i32* %arrayidx, align 4
  store i32 %13, i32* %p_0, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then3
  %14 = load i32, i32* %i, align 4
  %15 = load i32, i32* %n2.addr, align 4
  %cmp4 = icmp slt i32 %14, %15
  br i1 %cmp4, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %16 = load i32*, i32** %p.addr, align 4
  %17 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds i32, i32* %16, i32 %17
  %18 = load i32, i32* %arrayidx5, align 4
  %19 = load i32, i32* %p_0, align 4
  %cmp6 = icmp sgt i32 %18, %19
  br i1 %cmp6, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %20 = load float**, float*** %A.addr, align 4
  %21 = load i32*, i32** %p.addr, align 4
  %22 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds i32, i32* %21, i32 %22
  %23 = load i32, i32* %arrayidx7, align 4
  %arrayidx8 = getelementptr inbounds float*, float** %20, i32 %23
  %24 = load float*, float** %arrayidx8, align 4
  %25 = load i32, i32* %p_0, align 4
  %arrayidx9 = getelementptr inbounds float, float* %24, i32 %25
  %26 = load float, float* %arrayidx9, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.body
  %27 = load float**, float*** %A.addr, align 4
  %28 = load i32, i32* %p_0, align 4
  %arrayidx10 = getelementptr inbounds float*, float** %27, i32 %28
  %29 = load float*, float** %arrayidx10, align 4
  %30 = load i32*, i32** %p.addr, align 4
  %31 = load i32, i32* %i, align 4
  %arrayidx11 = getelementptr inbounds i32, i32* %30, i32 %31
  %32 = load i32, i32* %arrayidx11, align 4
  %arrayidx12 = getelementptr inbounds float, float* %29, i32 %32
  %33 = load float, float* %arrayidx12, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %26, %cond.true ], [ %33, %cond.false ]
  %fneg = fneg float %cond
  %34 = load float*, float** %a, align 4
  %35 = load i32, i32* %i, align 4
  %arrayidx13 = getelementptr inbounds float, float* %34, i32 %35
  store float %fneg, float* %arrayidx13, align 4
  br label %for.inc

for.inc:                                          ; preds = %cond.end
  %36 = load i32, i32* %i, align 4
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %37 = load float*, float** %a, align 4
  %arrayidx14 = getelementptr inbounds float, float* %37, i32 0
  %38 = load float, float* %arrayidx14, align 4
  %add15 = fadd float %38, 1.000000e+00
  store float %add15, float* %arrayidx14, align 4
  %39 = load float*, float** %L.addr, align 4
  %40 = load float*, float** %d.addr, align 4
  %41 = load float*, float** %a, align 4
  %42 = load i32, i32* %n2.addr, align 4
  %43 = load i32, i32* %nskip.addr, align 4
  %44 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %scratch.addr, align 4
  call void @_Z11btLDLTAddTLPfS_PKfiiR20btAlignedObjectArrayIfE(float* %39, float* %40, float* %41, i32 %42, i32 %43, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %44)
  br label %if.end

if.else16:                                        ; preds = %if.else
  %45 = load float*, float** %tmp, align 4
  %46 = bitcast float* %45 to i8*
  %47 = load i32, i32* %LDLTAddTL_size, align 4
  %add.ptr17 = getelementptr inbounds i8, i8* %46, i32 %47
  %48 = bitcast i8* %add.ptr17 to float*
  store float* %48, float** %t, align 4
  %49 = load float*, float** %L.addr, align 4
  %50 = load i32, i32* %r.addr, align 4
  %51 = load i32, i32* %nskip.addr, align 4
  %mul18 = mul nsw i32 %50, %51
  %add.ptr19 = getelementptr inbounds float, float* %49, i32 %mul18
  store float* %add.ptr19, float** %Lcurr, align 4
  store i32 0, i32* %i20, align 4
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc26, %if.else16
  %52 = load i32, i32* %i20, align 4
  %53 = load i32, i32* %r.addr, align 4
  %cmp22 = icmp slt i32 %52, %53
  br i1 %cmp22, label %for.body23, label %for.end28

for.body23:                                       ; preds = %for.cond21
  %54 = load float*, float** %Lcurr, align 4
  %55 = load float, float* %54, align 4
  %56 = load float*, float** %d.addr, align 4
  %57 = load i32, i32* %i20, align 4
  %arrayidx24 = getelementptr inbounds float, float* %56, i32 %57
  %58 = load float, float* %arrayidx24, align 4
  %div = fdiv float %55, %58
  %59 = load float*, float** %t, align 4
  %60 = load i32, i32* %i20, align 4
  %arrayidx25 = getelementptr inbounds float, float* %59, i32 %60
  store float %div, float* %arrayidx25, align 4
  br label %for.inc26

for.inc26:                                        ; preds = %for.body23
  %61 = load float*, float** %Lcurr, align 4
  %incdec.ptr = getelementptr inbounds float, float* %61, i32 1
  store float* %incdec.ptr, float** %Lcurr, align 4
  %62 = load i32, i32* %i20, align 4
  %inc27 = add nsw i32 %62, 1
  store i32 %inc27, i32* %i20, align 4
  br label %for.cond21

for.end28:                                        ; preds = %for.cond21
  %63 = load float*, float** %t, align 4
  %64 = load i32, i32* %r.addr, align 4
  %add.ptr30 = getelementptr inbounds float, float* %63, i32 %64
  store float* %add.ptr30, float** %a29, align 4
  %65 = load float*, float** %L.addr, align 4
  %66 = load i32, i32* %r.addr, align 4
  %67 = load i32, i32* %nskip.addr, align 4
  %mul32 = mul nsw i32 %66, %67
  %add.ptr33 = getelementptr inbounds float, float* %65, i32 %mul32
  store float* %add.ptr33, float** %Lcurr31, align 4
  %68 = load i32*, i32** %p.addr, align 4
  %69 = load i32, i32* %r.addr, align 4
  %add.ptr34 = getelementptr inbounds i32, i32* %68, i32 %69
  store i32* %add.ptr34, i32** %pp_r, align 4
  %70 = load i32*, i32** %pp_r, align 4
  %71 = load i32, i32* %70, align 4
  store i32 %71, i32* %p_r, align 4
  %72 = load i32, i32* %n2.addr, align 4
  %73 = load i32, i32* %r.addr, align 4
  %sub35 = sub nsw i32 %72, %73
  store i32 %sub35, i32* %n2_minus_r, align 4
  store i32 0, i32* %i36, align 4
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc55, %for.end28
  %74 = load i32, i32* %i36, align 4
  %75 = load i32, i32* %n2_minus_r, align 4
  %cmp38 = icmp slt i32 %74, %75
  br i1 %cmp38, label %for.body39, label %for.end58

for.body39:                                       ; preds = %for.cond37
  %76 = load float*, float** %Lcurr31, align 4
  %77 = load float*, float** %t, align 4
  %78 = load i32, i32* %r.addr, align 4
  %call40 = call float @_Z10btLargeDotPKfS0_i(float* %76, float* %77, i32 %78)
  %79 = load i32*, i32** %pp_r, align 4
  %80 = load i32, i32* %i36, align 4
  %arrayidx41 = getelementptr inbounds i32, i32* %79, i32 %80
  %81 = load i32, i32* %arrayidx41, align 4
  %82 = load i32, i32* %p_r, align 4
  %cmp42 = icmp sgt i32 %81, %82
  br i1 %cmp42, label %cond.true43, label %cond.false47

cond.true43:                                      ; preds = %for.body39
  %83 = load float**, float*** %A.addr, align 4
  %84 = load i32*, i32** %pp_r, align 4
  %85 = load i32, i32* %i36, align 4
  %arrayidx44 = getelementptr inbounds i32, i32* %84, i32 %85
  %86 = load i32, i32* %arrayidx44, align 4
  %arrayidx45 = getelementptr inbounds float*, float** %83, i32 %86
  %87 = load float*, float** %arrayidx45, align 4
  %88 = load i32, i32* %p_r, align 4
  %arrayidx46 = getelementptr inbounds float, float* %87, i32 %88
  %89 = load float, float* %arrayidx46, align 4
  br label %cond.end51

cond.false47:                                     ; preds = %for.body39
  %90 = load float**, float*** %A.addr, align 4
  %91 = load i32, i32* %p_r, align 4
  %arrayidx48 = getelementptr inbounds float*, float** %90, i32 %91
  %92 = load float*, float** %arrayidx48, align 4
  %93 = load i32*, i32** %pp_r, align 4
  %94 = load i32, i32* %i36, align 4
  %arrayidx49 = getelementptr inbounds i32, i32* %93, i32 %94
  %95 = load i32, i32* %arrayidx49, align 4
  %arrayidx50 = getelementptr inbounds float, float* %92, i32 %95
  %96 = load float, float* %arrayidx50, align 4
  br label %cond.end51

cond.end51:                                       ; preds = %cond.false47, %cond.true43
  %cond52 = phi float [ %89, %cond.true43 ], [ %96, %cond.false47 ]
  %sub53 = fsub float %call40, %cond52
  %97 = load float*, float** %a29, align 4
  %98 = load i32, i32* %i36, align 4
  %arrayidx54 = getelementptr inbounds float, float* %97, i32 %98
  store float %sub53, float* %arrayidx54, align 4
  br label %for.inc55

for.inc55:                                        ; preds = %cond.end51
  %99 = load i32, i32* %nskip.addr, align 4
  %100 = load float*, float** %Lcurr31, align 4
  %add.ptr56 = getelementptr inbounds float, float* %100, i32 %99
  store float* %add.ptr56, float** %Lcurr31, align 4
  %101 = load i32, i32* %i36, align 4
  %inc57 = add nsw i32 %101, 1
  store i32 %inc57, i32* %i36, align 4
  br label %for.cond37

for.end58:                                        ; preds = %for.cond37
  %102 = load float*, float** %a29, align 4
  %arrayidx59 = getelementptr inbounds float, float* %102, i32 0
  %103 = load float, float* %arrayidx59, align 4
  %add60 = fadd float %103, 1.000000e+00
  store float %add60, float* %arrayidx59, align 4
  %104 = load float*, float** %L.addr, align 4
  %105 = load i32, i32* %r.addr, align 4
  %106 = load i32, i32* %nskip.addr, align 4
  %mul61 = mul nsw i32 %105, %106
  %add.ptr62 = getelementptr inbounds float, float* %104, i32 %mul61
  %107 = load i32, i32* %r.addr, align 4
  %add.ptr63 = getelementptr inbounds float, float* %add.ptr62, i32 %107
  %108 = load float*, float** %d.addr, align 4
  %109 = load i32, i32* %r.addr, align 4
  %add.ptr64 = getelementptr inbounds float, float* %108, i32 %109
  %110 = load float*, float** %a29, align 4
  %111 = load i32, i32* %n2.addr, align 4
  %112 = load i32, i32* %r.addr, align 4
  %sub65 = sub nsw i32 %111, %112
  %113 = load i32, i32* %nskip.addr, align 4
  %114 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %scratch.addr, align 4
  call void @_Z11btLDLTAddTLPfS_PKfiiR20btAlignedObjectArrayIfE(float* %add.ptr63, float* %add.ptr64, float* %110, i32 %sub65, i32 %113, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %114)
  br label %if.end

if.end:                                           ; preds = %for.end58, %for.end
  br label %if.end66

if.end66:                                         ; preds = %if.end
  %115 = load float*, float** %L.addr, align 4
  %116 = load i32, i32* %n2.addr, align 4
  %117 = load i32, i32* %nskip.addr, align 4
  %118 = load i32, i32* %r.addr, align 4
  call void @_Z14btRemoveRowColPfiii(float* %115, i32 %116, i32 %117, i32 %118)
  %119 = load i32, i32* %r.addr, align 4
  %120 = load i32, i32* %n2.addr, align 4
  %sub67 = sub nsw i32 %120, 1
  %cmp68 = icmp slt i32 %119, %sub67
  br i1 %cmp68, label %if.then69, label %if.end76

if.then69:                                        ; preds = %if.end66
  %121 = load float*, float** %d.addr, align 4
  %122 = load i32, i32* %r.addr, align 4
  %add.ptr70 = getelementptr inbounds float, float* %121, i32 %122
  %123 = bitcast float* %add.ptr70 to i8*
  %124 = load float*, float** %d.addr, align 4
  %125 = load i32, i32* %r.addr, align 4
  %add.ptr71 = getelementptr inbounds float, float* %124, i32 %125
  %add.ptr72 = getelementptr inbounds float, float* %add.ptr71, i32 1
  %126 = bitcast float* %add.ptr72 to i8*
  %127 = load i32, i32* %n2.addr, align 4
  %128 = load i32, i32* %r.addr, align 4
  %sub73 = sub nsw i32 %127, %128
  %sub74 = sub nsw i32 %sub73, 1
  %mul75 = mul i32 %sub74, 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %123, i8* align 4 %126, i32 %mul75, i1 false)
  br label %if.end76

if.end76:                                         ; preds = %if.then, %if.then69, %if.end66
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_Z29btEstimateLDLTAddTLTmpbufSizei(i32 %nskip) #1 comdat {
entry:
  %nskip.addr = alloca i32, align 4
  store i32 %nskip, i32* %nskip.addr, align 4
  %0 = load i32, i32* %nskip.addr, align 4
  %mul = mul nsw i32 %0, 2
  %mul1 = mul i32 %mul, 4
  ret i32 %mul1
}

; Function Attrs: noinline optnone
define hidden void @_ZN5btLCP22transfer_i_from_C_to_NEiR20btAlignedObjectArrayIfE(%struct.btLCP* %this, i32 %i, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %scratch) #2 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  %scratch.addr = alloca %class.btAlignedObjectArray*, align 4
  %C = alloca i32*, align 4
  %last_idx = alloca i32, align 4
  %nC = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  store %class.btAlignedObjectArray* %scratch, %class.btAlignedObjectArray** %scratch.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_C = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %0 = load i32*, i32** %m_C, align 4
  store i32* %0, i32** %C, align 4
  store i32 -1, i32* %last_idx, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %1 = load i32, i32* %m_nC, align 4
  store i32 %1, i32* %nC, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc28, %entry
  %2 = load i32, i32* %j, align 4
  %3 = load i32, i32* %nC, align 4
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end30

for.body:                                         ; preds = %for.cond
  %4 = load i32*, i32** %C, align 4
  %5 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds i32, i32* %4, i32 %5
  %6 = load i32, i32* %arrayidx, align 4
  %7 = load i32, i32* %nC, align 4
  %sub = sub nsw i32 %7, 1
  %cmp2 = icmp eq i32 %6, %sub
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %8 = load i32, i32* %j, align 4
  store i32 %8, i32* %last_idx, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %9 = load i32*, i32** %C, align 4
  %10 = load i32, i32* %j, align 4
  %arrayidx3 = getelementptr inbounds i32, i32* %9, i32 %10
  %11 = load i32, i32* %arrayidx3, align 4
  %12 = load i32, i32* %i.addr, align 4
  %cmp4 = icmp eq i32 %11, %12
  br i1 %cmp4, label %if.then5, label %if.end27

if.then5:                                         ; preds = %if.end
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %13 = load float**, float*** %m_A, align 4
  %14 = load i32*, i32** %C, align 4
  %m_L = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %15 = load float*, float** %m_L, align 4
  %m_d = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %16 = load float*, float** %m_d, align 4
  %m_n = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %17 = load i32, i32* %m_n, align 4
  %18 = load i32, i32* %nC, align 4
  %19 = load i32, i32* %j, align 4
  %m_nskip = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %20 = load i32, i32* %m_nskip, align 4
  %21 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %scratch.addr, align 4
  call void @_Z12btLDLTRemovePPfPKiS_S_iiiiR20btAlignedObjectArrayIfE(float** %13, i32* %14, float* %15, float* %16, i32 %17, i32 %18, i32 %19, i32 %20, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %21)
  %22 = load i32, i32* %last_idx, align 4
  %cmp6 = icmp eq i32 %22, -1
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.then5
  %23 = load i32, i32* %j, align 4
  %add = add nsw i32 %23, 1
  store i32 %add, i32* %k, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %if.then7
  %24 = load i32, i32* %k, align 4
  %25 = load i32, i32* %nC, align 4
  %cmp9 = icmp slt i32 %24, %25
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %26 = load i32*, i32** %C, align 4
  %27 = load i32, i32* %k, align 4
  %arrayidx11 = getelementptr inbounds i32, i32* %26, i32 %27
  %28 = load i32, i32* %arrayidx11, align 4
  %29 = load i32, i32* %nC, align 4
  %sub12 = sub nsw i32 %29, 1
  %cmp13 = icmp eq i32 %28, %sub12
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %for.body10
  br label %for.end

if.end15:                                         ; preds = %for.body10
  br label %for.inc

for.inc:                                          ; preds = %if.end15
  %30 = load i32, i32* %k, align 4
  %inc = add nsw i32 %30, 1
  store i32 %inc, i32* %k, align 4
  br label %for.cond8

for.end:                                          ; preds = %if.then14, %for.cond8
  br label %if.end16

if.else:                                          ; preds = %if.then5
  %31 = load i32, i32* %last_idx, align 4
  store i32 %31, i32* %k, align 4
  br label %if.end16

if.end16:                                         ; preds = %if.else, %for.end
  %32 = load i32*, i32** %C, align 4
  %33 = load i32, i32* %j, align 4
  %arrayidx17 = getelementptr inbounds i32, i32* %32, i32 %33
  %34 = load i32, i32* %arrayidx17, align 4
  %35 = load i32*, i32** %C, align 4
  %36 = load i32, i32* %k, align 4
  %arrayidx18 = getelementptr inbounds i32, i32* %35, i32 %36
  store i32 %34, i32* %arrayidx18, align 4
  %37 = load i32, i32* %j, align 4
  %38 = load i32, i32* %nC, align 4
  %sub19 = sub nsw i32 %38, 1
  %cmp20 = icmp slt i32 %37, %sub19
  br i1 %cmp20, label %if.then21, label %if.end26

if.then21:                                        ; preds = %if.end16
  %39 = load i32*, i32** %C, align 4
  %40 = load i32, i32* %j, align 4
  %add.ptr = getelementptr inbounds i32, i32* %39, i32 %40
  %41 = bitcast i32* %add.ptr to i8*
  %42 = load i32*, i32** %C, align 4
  %43 = load i32, i32* %j, align 4
  %add.ptr22 = getelementptr inbounds i32, i32* %42, i32 %43
  %add.ptr23 = getelementptr inbounds i32, i32* %add.ptr22, i32 1
  %44 = bitcast i32* %add.ptr23 to i8*
  %45 = load i32, i32* %nC, align 4
  %46 = load i32, i32* %j, align 4
  %sub24 = sub nsw i32 %45, %46
  %sub25 = sub nsw i32 %sub24, 1
  %mul = mul i32 %sub25, 4
  call void @llvm.memmove.p0i8.p0i8.i32(i8* align 4 %41, i8* align 4 %44, i32 %mul, i1 false)
  br label %if.end26

if.end26:                                         ; preds = %if.then21, %if.end16
  br label %for.end30

if.end27:                                         ; preds = %if.end
  br label %for.inc28

for.inc28:                                        ; preds = %if.end27
  %47 = load i32, i32* %j, align 4
  %inc29 = add nsw i32 %47, 1
  store i32 %inc29, i32* %j, align 4
  br label %for.cond

for.end30:                                        ; preds = %if.end26, %for.cond
  %m_A31 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %48 = load float**, float*** %m_A31, align 4
  %m_x = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %49 = load float*, float** %m_x, align 4
  %m_b = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 7
  %50 = load float*, float** %m_b, align 4
  %m_w = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %51 = load float*, float** %m_w, align 4
  %m_lo = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 9
  %52 = load float*, float** %m_lo, align 4
  %m_hi = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 10
  %53 = load float*, float** %m_hi, align 4
  %m_p = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %54 = load i32*, i32** %m_p, align 4
  %m_state = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 16
  %55 = load i8*, i8** %m_state, align 4
  %m_findex = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 17
  %56 = load i32*, i32** %m_findex, align 4
  %m_n32 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %57 = load i32, i32* %m_n32, align 4
  %58 = load i32, i32* %i.addr, align 4
  %59 = load i32, i32* %nC, align 4
  %sub33 = sub nsw i32 %59, 1
  %m_nskip34 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %60 = load i32, i32* %m_nskip34, align 4
  call void @_ZL13btSwapProblemPPfS_S_S_S_S_PiPbS1_iiiii(float** %48, float* %49, float* %50, float* %51, float* %52, float* %53, i32* %54, i8* %55, i32* %56, i32 %57, i32 %58, i32 %sub33, i32 %60, i32 1)
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %61 = load i32, i32* %m_nN, align 4
  %inc35 = add nsw i32 %61, 1
  store i32 %inc35, i32* %m_nN, align 4
  %62 = load i32, i32* %nC, align 4
  %sub36 = sub nsw i32 %62, 1
  %m_nC37 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  store i32 %sub36, i32* %m_nC37, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN5btLCP22pN_equals_ANC_times_qCEPfS0_(%struct.btLCP* %this, float* %p, float* %q) #2 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %p.addr = alloca float*, align 4
  %q.addr = alloca float*, align 4
  %nC = alloca i32, align 4
  %ptgt = alloca float*, align 4
  %nN = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store float* %p, float** %p.addr, align 4
  store float* %q, float** %q.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_nC, align 4
  store i32 %0, i32* %nC, align 4
  %1 = load float*, float** %p.addr, align 4
  %2 = load i32, i32* %nC, align 4
  %add.ptr = getelementptr inbounds float, float* %1, i32 %2
  store float* %add.ptr, float** %ptgt, align 4
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %3 = load i32, i32* %m_nN, align 4
  store i32 %3, i32* %nN, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4
  %5 = load i32, i32* %nN, align 4
  %cmp = icmp slt i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %6 = load float**, float*** %m_A, align 4
  %7 = load i32, i32* %i, align 4
  %8 = load i32, i32* %nC, align 4
  %add = add nsw i32 %7, %8
  %arrayidx = getelementptr inbounds float*, float** %6, i32 %add
  %9 = load float*, float** %arrayidx, align 4
  %10 = load float*, float** %q.addr, align 4
  %11 = load i32, i32* %nC, align 4
  %call = call float @_Z10btLargeDotPKfS0_i(float* %9, float* %10, i32 %11)
  %12 = load float*, float** %ptgt, align 4
  %13 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %12, i32 %13
  store float %call, float* %arrayidx2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5btLCP17pN_plusequals_ANiEPfii(%struct.btLCP* %this, float* %p, i32 %i, i32 %sign) #1 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %p.addr = alloca float*, align 4
  %i.addr = alloca i32, align 4
  %sign.addr = alloca i32, align 4
  %nC = alloca i32, align 4
  %aptr = alloca float*, align 4
  %ptgt = alloca float*, align 4
  %nN = alloca i32, align 4
  %j = alloca i32, align 4
  %nN6 = alloca i32, align 4
  %j8 = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store float* %p, float** %p.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  store i32 %sign, i32* %sign.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_nC, align 4
  store i32 %0, i32* %nC, align 4
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %1 = load float**, float*** %m_A, align 4
  %2 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds float*, float** %1, i32 %2
  %3 = load float*, float** %arrayidx, align 4
  %4 = load i32, i32* %nC, align 4
  %add.ptr = getelementptr inbounds float, float* %3, i32 %4
  store float* %add.ptr, float** %aptr, align 4
  %5 = load float*, float** %p.addr, align 4
  %6 = load i32, i32* %nC, align 4
  %add.ptr2 = getelementptr inbounds float, float* %5, i32 %6
  store float* %add.ptr2, float** %ptgt, align 4
  %7 = load i32, i32* %sign.addr, align 4
  %cmp = icmp sgt i32 %7, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %8 = load i32, i32* %m_nN, align 4
  store i32 %8, i32* %nN, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %9 = load i32, i32* %j, align 4
  %10 = load i32, i32* %nN, align 4
  %cmp3 = icmp slt i32 %9, %10
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load float*, float** %aptr, align 4
  %12 = load i32, i32* %j, align 4
  %arrayidx4 = getelementptr inbounds float, float* %11, i32 %12
  %13 = load float, float* %arrayidx4, align 4
  %14 = load float*, float** %ptgt, align 4
  %15 = load i32, i32* %j, align 4
  %arrayidx5 = getelementptr inbounds float, float* %14, i32 %15
  %16 = load float, float* %arrayidx5, align 4
  %add = fadd float %16, %13
  store float %add, float* %arrayidx5, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %17 = load i32, i32* %j, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.else:                                          ; preds = %entry
  %m_nN7 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %18 = load i32, i32* %m_nN7, align 4
  store i32 %18, i32* %nN6, align 4
  store i32 0, i32* %j8, align 4
  br label %for.cond9

for.cond9:                                        ; preds = %for.inc14, %if.else
  %19 = load i32, i32* %j8, align 4
  %20 = load i32, i32* %nN6, align 4
  %cmp10 = icmp slt i32 %19, %20
  br i1 %cmp10, label %for.body11, label %for.end16

for.body11:                                       ; preds = %for.cond9
  %21 = load float*, float** %aptr, align 4
  %22 = load i32, i32* %j8, align 4
  %arrayidx12 = getelementptr inbounds float, float* %21, i32 %22
  %23 = load float, float* %arrayidx12, align 4
  %24 = load float*, float** %ptgt, align 4
  %25 = load i32, i32* %j8, align 4
  %arrayidx13 = getelementptr inbounds float, float* %24, i32 %25
  %26 = load float, float* %arrayidx13, align 4
  %sub = fsub float %26, %23
  store float %sub, float* %arrayidx13, align 4
  br label %for.inc14

for.inc14:                                        ; preds = %for.body11
  %27 = load i32, i32* %j8, align 4
  %inc15 = add nsw i32 %27, 1
  store i32 %inc15, i32* %j8, align 4
  br label %for.cond9

for.end16:                                        ; preds = %for.cond9
  br label %if.end

if.end:                                           ; preds = %for.end16, %for.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5btLCP24pC_plusequals_s_times_qCEPffS0_(%struct.btLCP* %this, float* %p, float %s, float* %q) #1 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %p.addr = alloca float*, align 4
  %s.addr = alloca float, align 4
  %q.addr = alloca float*, align 4
  %nC = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store float* %p, float** %p.addr, align 4
  store float %s, float* %s.addr, align 4
  store float* %q, float** %q.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_nC, align 4
  store i32 %0, i32* %nC, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %nC, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float, float* %s.addr, align 4
  %4 = load float*, float** %q.addr, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %4, i32 %5
  %6 = load float, float* %arrayidx, align 4
  %mul = fmul float %3, %6
  %7 = load float*, float** %p.addr, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx2, align 4
  %add = fadd float %9, %mul
  store float %add, float* %arrayidx2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5btLCP24pN_plusequals_s_times_qNEPffS0_(%struct.btLCP* %this, float* %p, float %s, float* %q) #1 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %p.addr = alloca float*, align 4
  %s.addr = alloca float, align 4
  %q.addr = alloca float*, align 4
  %nC = alloca i32, align 4
  %ptgt = alloca float*, align 4
  %qsrc = alloca float*, align 4
  %nN = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store float* %p, float** %p.addr, align 4
  store float %s, float* %s.addr, align 4
  store float* %q, float** %q.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_nC, align 4
  store i32 %0, i32* %nC, align 4
  %1 = load float*, float** %p.addr, align 4
  %2 = load i32, i32* %nC, align 4
  %add.ptr = getelementptr inbounds float, float* %1, i32 %2
  store float* %add.ptr, float** %ptgt, align 4
  %3 = load float*, float** %q.addr, align 4
  %4 = load i32, i32* %nC, align 4
  %add.ptr2 = getelementptr inbounds float, float* %3, i32 %4
  store float* %add.ptr2, float** %qsrc, align 4
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %5 = load i32, i32* %m_nN, align 4
  store i32 %5, i32* %nN, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4
  %7 = load i32, i32* %nN, align 4
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load float, float* %s.addr, align 4
  %9 = load float*, float** %qsrc, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %9, i32 %10
  %11 = load float, float* %arrayidx, align 4
  %mul = fmul float %8, %11
  %12 = load float*, float** %ptgt, align 4
  %13 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds float, float* %12, i32 %13
  %14 = load float, float* %arrayidx3, align 4
  %add = fadd float %14, %mul
  store float %add, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5btLCP6solve1EPfiii(%struct.btLCP* %this, float* %a, i32 %i, i32 %dir, i32 %only_transfer) #1 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %a.addr = alloca float*, align 4
  %i.addr = alloca i32, align 4
  %dir.addr = alloca i32, align 4
  %only_transfer.addr = alloca i32, align 4
  %Dell = alloca float*, align 4
  %C = alloca i32*, align 4
  %aptr = alloca float*, align 4
  %nub = alloca i32, align 4
  %j = alloca i32, align 4
  %nC = alloca i32, align 4
  %ell = alloca float*, align 4
  %Dell17 = alloca float*, align 4
  %d = alloca float*, align 4
  %nC19 = alloca i32, align 4
  %j21 = alloca i32, align 4
  %tmp = alloca float*, align 4
  %ell32 = alloca float*, align 4
  %nC34 = alloca i32, align 4
  %j36 = alloca i32, align 4
  %C50 = alloca i32*, align 4
  %tmp52 = alloca float*, align 4
  %nC54 = alloca i32, align 4
  %j56 = alloca i32, align 4
  %C66 = alloca i32*, align 4
  %tmp68 = alloca float*, align 4
  %nC70 = alloca i32, align 4
  %j72 = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store float* %a, float** %a.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  store i32 %dir, i32* %dir.addr, align 4
  store i32 %only_transfer, i32* %only_transfer.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_nC, align 4
  %cmp = icmp sgt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end83

if.then:                                          ; preds = %entry
  %m_Dell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %1 = load float*, float** %m_Dell, align 4
  store float* %1, float** %Dell, align 4
  %m_C = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %2 = load i32*, i32** %m_C, align 4
  store i32* %2, i32** %C, align 4
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %3 = load float**, float*** %m_A, align 4
  %4 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds float*, float** %3, i32 %4
  %5 = load float*, float** %arrayidx, align 4
  store float* %5, float** %aptr, align 4
  %m_nub = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %6 = load i32, i32* %m_nub, align 4
  store i32 %6, i32* %nub, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %7 = load i32, i32* %j, align 4
  %8 = load i32, i32* %nub, align 4
  %cmp2 = icmp slt i32 %7, %8
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load float*, float** %aptr, align 4
  %10 = load i32, i32* %j, align 4
  %arrayidx3 = getelementptr inbounds float, float* %9, i32 %10
  %11 = load float, float* %arrayidx3, align 4
  %12 = load float*, float** %Dell, align 4
  %13 = load i32, i32* %j, align 4
  %arrayidx4 = getelementptr inbounds float, float* %12, i32 %13
  store float %11, float* %arrayidx4, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %j, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_nC5 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %15 = load i32, i32* %m_nC5, align 4
  store i32 %15, i32* %nC, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %for.end
  %16 = load i32, i32* %j, align 4
  %17 = load i32, i32* %nC, align 4
  %cmp7 = icmp slt i32 %16, %17
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %18 = load float*, float** %aptr, align 4
  %19 = load i32*, i32** %C, align 4
  %20 = load i32, i32* %j, align 4
  %arrayidx9 = getelementptr inbounds i32, i32* %19, i32 %20
  %21 = load i32, i32* %arrayidx9, align 4
  %arrayidx10 = getelementptr inbounds float, float* %18, i32 %21
  %22 = load float, float* %arrayidx10, align 4
  %23 = load float*, float** %Dell, align 4
  %24 = load i32, i32* %j, align 4
  %arrayidx11 = getelementptr inbounds float, float* %23, i32 %24
  store float %22, float* %arrayidx11, align 4
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %25 = load i32, i32* %j, align 4
  %inc13 = add nsw i32 %25, 1
  store i32 %inc13, i32* %j, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  %m_L = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %26 = load float*, float** %m_L, align 4
  %m_Dell15 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %27 = load float*, float** %m_Dell15, align 4
  %m_nC16 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %28 = load i32, i32* %m_nC16, align 4
  %m_nskip = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %29 = load i32, i32* %m_nskip, align 4
  call void @_Z9btSolveL1PKfPfii(float* %26, float* %27, i32 %28, i32 %29)
  %m_ell = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 14
  %30 = load float*, float** %m_ell, align 4
  store float* %30, float** %ell, align 4
  %m_Dell18 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 13
  %31 = load float*, float** %m_Dell18, align 4
  store float* %31, float** %Dell17, align 4
  %m_d = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 12
  %32 = load float*, float** %m_d, align 4
  store float* %32, float** %d, align 4
  %m_nC20 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %33 = load i32, i32* %m_nC20, align 4
  store i32 %33, i32* %nC19, align 4
  store i32 0, i32* %j21, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc28, %for.end14
  %34 = load i32, i32* %j21, align 4
  %35 = load i32, i32* %nC19, align 4
  %cmp23 = icmp slt i32 %34, %35
  br i1 %cmp23, label %for.body24, label %for.end30

for.body24:                                       ; preds = %for.cond22
  %36 = load float*, float** %Dell17, align 4
  %37 = load i32, i32* %j21, align 4
  %arrayidx25 = getelementptr inbounds float, float* %36, i32 %37
  %38 = load float, float* %arrayidx25, align 4
  %39 = load float*, float** %d, align 4
  %40 = load i32, i32* %j21, align 4
  %arrayidx26 = getelementptr inbounds float, float* %39, i32 %40
  %41 = load float, float* %arrayidx26, align 4
  %mul = fmul float %38, %41
  %42 = load float*, float** %ell, align 4
  %43 = load i32, i32* %j21, align 4
  %arrayidx27 = getelementptr inbounds float, float* %42, i32 %43
  store float %mul, float* %arrayidx27, align 4
  br label %for.inc28

for.inc28:                                        ; preds = %for.body24
  %44 = load i32, i32* %j21, align 4
  %inc29 = add nsw i32 %44, 1
  store i32 %inc29, i32* %j21, align 4
  br label %for.cond22

for.end30:                                        ; preds = %for.cond22
  %45 = load i32, i32* %only_transfer.addr, align 4
  %tobool = icmp ne i32 %45, 0
  br i1 %tobool, label %if.end82, label %if.then31

if.then31:                                        ; preds = %for.end30
  %m_tmp = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %46 = load float*, float** %m_tmp, align 4
  store float* %46, float** %tmp, align 4
  %m_ell33 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 14
  %47 = load float*, float** %m_ell33, align 4
  store float* %47, float** %ell32, align 4
  %m_nC35 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %48 = load i32, i32* %m_nC35, align 4
  store i32 %48, i32* %nC34, align 4
  store i32 0, i32* %j36, align 4
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc42, %if.then31
  %49 = load i32, i32* %j36, align 4
  %50 = load i32, i32* %nC34, align 4
  %cmp38 = icmp slt i32 %49, %50
  br i1 %cmp38, label %for.body39, label %for.end44

for.body39:                                       ; preds = %for.cond37
  %51 = load float*, float** %ell32, align 4
  %52 = load i32, i32* %j36, align 4
  %arrayidx40 = getelementptr inbounds float, float* %51, i32 %52
  %53 = load float, float* %arrayidx40, align 4
  %54 = load float*, float** %tmp, align 4
  %55 = load i32, i32* %j36, align 4
  %arrayidx41 = getelementptr inbounds float, float* %54, i32 %55
  store float %53, float* %arrayidx41, align 4
  br label %for.inc42

for.inc42:                                        ; preds = %for.body39
  %56 = load i32, i32* %j36, align 4
  %inc43 = add nsw i32 %56, 1
  store i32 %inc43, i32* %j36, align 4
  br label %for.cond37

for.end44:                                        ; preds = %for.cond37
  %m_L45 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 11
  %57 = load float*, float** %m_L45, align 4
  %58 = load float*, float** %tmp, align 4
  %m_nC46 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %59 = load i32, i32* %m_nC46, align 4
  %m_nskip47 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 1
  %60 = load i32, i32* %m_nskip47, align 4
  call void @_Z10btSolveL1TPKfPfii(float* %57, float* %58, i32 %59, i32 %60)
  %61 = load i32, i32* %dir.addr, align 4
  %cmp48 = icmp sgt i32 %61, 0
  br i1 %cmp48, label %if.then49, label %if.else

if.then49:                                        ; preds = %for.end44
  %m_C51 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %62 = load i32*, i32** %m_C51, align 4
  store i32* %62, i32** %C50, align 4
  %m_tmp53 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %63 = load float*, float** %m_tmp53, align 4
  store float* %63, float** %tmp52, align 4
  %m_nC55 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %64 = load i32, i32* %m_nC55, align 4
  store i32 %64, i32* %nC54, align 4
  store i32 0, i32* %j56, align 4
  br label %for.cond57

for.cond57:                                       ; preds = %for.inc63, %if.then49
  %65 = load i32, i32* %j56, align 4
  %66 = load i32, i32* %nC54, align 4
  %cmp58 = icmp slt i32 %65, %66
  br i1 %cmp58, label %for.body59, label %for.end65

for.body59:                                       ; preds = %for.cond57
  %67 = load float*, float** %tmp52, align 4
  %68 = load i32, i32* %j56, align 4
  %arrayidx60 = getelementptr inbounds float, float* %67, i32 %68
  %69 = load float, float* %arrayidx60, align 4
  %fneg = fneg float %69
  %70 = load float*, float** %a.addr, align 4
  %71 = load i32*, i32** %C50, align 4
  %72 = load i32, i32* %j56, align 4
  %arrayidx61 = getelementptr inbounds i32, i32* %71, i32 %72
  %73 = load i32, i32* %arrayidx61, align 4
  %arrayidx62 = getelementptr inbounds float, float* %70, i32 %73
  store float %fneg, float* %arrayidx62, align 4
  br label %for.inc63

for.inc63:                                        ; preds = %for.body59
  %74 = load i32, i32* %j56, align 4
  %inc64 = add nsw i32 %74, 1
  store i32 %inc64, i32* %j56, align 4
  br label %for.cond57

for.end65:                                        ; preds = %for.cond57
  br label %if.end

if.else:                                          ; preds = %for.end44
  %m_C67 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 19
  %75 = load i32*, i32** %m_C67, align 4
  store i32* %75, i32** %C66, align 4
  %m_tmp69 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %76 = load float*, float** %m_tmp69, align 4
  store float* %76, float** %tmp68, align 4
  %m_nC71 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %77 = load i32, i32* %m_nC71, align 4
  store i32 %77, i32* %nC70, align 4
  store i32 0, i32* %j72, align 4
  br label %for.cond73

for.cond73:                                       ; preds = %for.inc79, %if.else
  %78 = load i32, i32* %j72, align 4
  %79 = load i32, i32* %nC70, align 4
  %cmp74 = icmp slt i32 %78, %79
  br i1 %cmp74, label %for.body75, label %for.end81

for.body75:                                       ; preds = %for.cond73
  %80 = load float*, float** %tmp68, align 4
  %81 = load i32, i32* %j72, align 4
  %arrayidx76 = getelementptr inbounds float, float* %80, i32 %81
  %82 = load float, float* %arrayidx76, align 4
  %83 = load float*, float** %a.addr, align 4
  %84 = load i32*, i32** %C66, align 4
  %85 = load i32, i32* %j72, align 4
  %arrayidx77 = getelementptr inbounds i32, i32* %84, i32 %85
  %86 = load i32, i32* %arrayidx77, align 4
  %arrayidx78 = getelementptr inbounds float, float* %83, i32 %86
  store float %82, float* %arrayidx78, align 4
  br label %for.inc79

for.inc79:                                        ; preds = %for.body75
  %87 = load i32, i32* %j72, align 4
  %inc80 = add nsw i32 %87, 1
  store i32 %inc80, i32* %j72, align 4
  br label %for.cond73

for.end81:                                        ; preds = %for.cond73
  br label %if.end

if.end:                                           ; preds = %for.end81, %for.end65
  br label %if.end82

if.end82:                                         ; preds = %if.end, %for.end30
  br label %if.end83

if.end83:                                         ; preds = %if.end82, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN5btLCP9unpermuteEv(%struct.btLCP* %this) #1 {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %x = alloca float*, align 4
  %tmp = alloca float*, align 4
  %p = alloca i32*, align 4
  %n = alloca i32, align 4
  %j = alloca i32, align 4
  %w = alloca float*, align 4
  %tmp11 = alloca float*, align 4
  %p13 = alloca i32*, align 4
  %n15 = alloca i32, align 4
  %j17 = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_tmp = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %0 = load float*, float** %m_tmp, align 4
  %1 = bitcast float* %0 to i8*
  %m_x = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %2 = load float*, float** %m_x, align 4
  %3 = bitcast float* %2 to i8*
  %m_n = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %4 = load i32, i32* %m_n, align 4
  %mul = mul i32 %4, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %3, i32 %mul, i1 false)
  %m_x2 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 6
  %5 = load float*, float** %m_x2, align 4
  store float* %5, float** %x, align 4
  %m_tmp3 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %6 = load float*, float** %m_tmp3, align 4
  store float* %6, float** %tmp, align 4
  %m_p = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %7 = load i32*, i32** %m_p, align 4
  store i32* %7, i32** %p, align 4
  %m_n4 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %8 = load i32, i32* %m_n4, align 4
  store i32 %8, i32* %n, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %9 = load i32, i32* %j, align 4
  %10 = load i32, i32* %n, align 4
  %cmp = icmp slt i32 %9, %10
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load float*, float** %tmp, align 4
  %12 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds float, float* %11, i32 %12
  %13 = load float, float* %arrayidx, align 4
  %14 = load float*, float** %x, align 4
  %15 = load i32*, i32** %p, align 4
  %16 = load i32, i32* %j, align 4
  %arrayidx5 = getelementptr inbounds i32, i32* %15, i32 %16
  %17 = load i32, i32* %arrayidx5, align 4
  %arrayidx6 = getelementptr inbounds float, float* %14, i32 %17
  store float %13, float* %arrayidx6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %18 = load i32, i32* %j, align 4
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_tmp7 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %19 = load float*, float** %m_tmp7, align 4
  %20 = bitcast float* %19 to i8*
  %m_w = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %21 = load float*, float** %m_w, align 4
  %22 = bitcast float* %21 to i8*
  %m_n8 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %23 = load i32, i32* %m_n8, align 4
  %mul9 = mul i32 %23, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %22, i32 %mul9, i1 false)
  %m_w10 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 8
  %24 = load float*, float** %m_w10, align 4
  store float* %24, float** %w, align 4
  %m_tmp12 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 15
  %25 = load float*, float** %m_tmp12, align 4
  store float* %25, float** %tmp11, align 4
  %m_p14 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 18
  %26 = load i32*, i32** %m_p14, align 4
  store i32* %26, i32** %p13, align 4
  %m_n16 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 0
  %27 = load i32, i32* %m_n16, align 4
  store i32 %27, i32* %n15, align 4
  store i32 0, i32* %j17, align 4
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc24, %for.end
  %28 = load i32, i32* %j17, align 4
  %29 = load i32, i32* %n15, align 4
  %cmp19 = icmp slt i32 %28, %29
  br i1 %cmp19, label %for.body20, label %for.end26

for.body20:                                       ; preds = %for.cond18
  %30 = load float*, float** %tmp11, align 4
  %31 = load i32, i32* %j17, align 4
  %arrayidx21 = getelementptr inbounds float, float* %30, i32 %31
  %32 = load float, float* %arrayidx21, align 4
  %33 = load float*, float** %w, align 4
  %34 = load i32*, i32** %p13, align 4
  %35 = load i32, i32* %j17, align 4
  %arrayidx22 = getelementptr inbounds i32, i32* %34, i32 %35
  %36 = load i32, i32* %arrayidx22, align 4
  %arrayidx23 = getelementptr inbounds float, float* %33, i32 %36
  store float %32, float* %arrayidx23, align 4
  br label %for.inc24

for.inc24:                                        ; preds = %for.body20
  %37 = load i32, i32* %j17, align 4
  %inc25 = add nsw i32 %37, 1
  store i32 %inc25, i32* %j17, align 4
  br label %for.cond18

for.end26:                                        ; preds = %for.cond18
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_Z17btSolveDantzigLCPiPfS_S_S_iS_S_PiR22btDantzigScratchMemory(i32 %n, float* %A, float* %x, float* %b, float* %outer_w, i32 %nub, float* %lo, float* %hi, i32* %findex, %struct.btDantzigScratchMemory* nonnull align 4 dereferenceable(220) %scratchMem) #2 {
entry:
  %retval = alloca i1, align 1
  %n.addr = alloca i32, align 4
  %A.addr = alloca float*, align 4
  %x.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %outer_w.addr = alloca float*, align 4
  %nub.addr = alloca i32, align 4
  %lo.addr = alloca float*, align 4
  %hi.addr = alloca float*, align 4
  %findex.addr = alloca i32*, align 4
  %scratchMem.addr = alloca %struct.btDantzigScratchMemory*, align 4
  %nskip = alloca i32, align 4
  %nskip1 = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %w = alloca float*, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float*, align 4
  %ref.tmp9 = alloca i32, align 4
  %ref.tmp10 = alloca i32, align 4
  %ref.tmp11 = alloca i8, align 1
  %lcp = alloca %struct.btLCP, align 4
  %adj_nub = alloca i32, align 4
  %hit_first_friction_index = alloca i8, align 1
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %wfk = alloca float, align 4
  %dir = alloca i32, align 4
  %dirf = alloca float, align 4
  %cmd = alloca i32, align 4
  %si = alloca i32, align 4
  %s = alloca float, align 4
  %s2 = alloca float, align 4
  %s2138 = alloca float, align 4
  %numN = alloca i32, align 4
  %k149 = alloca i32, align 4
  %indexN_k = alloca i32, align 4
  %s2171 = alloca float, align 4
  %numC = alloca i32, align 4
  %k185 = alloca i32, align 4
  %indexC_k = alloca i32, align 4
  %s2198 = alloca float, align 4
  %s2216 = alloca float, align 4
  store i32 %n, i32* %n.addr, align 4
  store float* %A, float** %A.addr, align 4
  store float* %x, float** %x.addr, align 4
  store float* %b, float** %b.addr, align 4
  store float* %outer_w, float** %outer_w.addr, align 4
  store i32 %nub, i32* %nub.addr, align 4
  store float* %lo, float** %lo.addr, align 4
  store float* %hi, float** %hi.addr, align 4
  store i32* %findex, i32** %findex.addr, align 4
  store %struct.btDantzigScratchMemory* %scratchMem, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  store i8 0, i8* @s_error, align 1
  %0 = load i32, i32* %nub.addr, align 4
  %1 = load i32, i32* %n.addr, align 4
  %cmp = icmp sge i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %n.addr, align 4
  store i32 %2, i32* %nskip, align 4
  %3 = load float*, float** %A.addr, align 4
  %4 = load float*, float** %outer_w.addr, align 4
  %5 = load i32, i32* %n.addr, align 4
  %6 = load i32, i32* %nskip, align 4
  call void @_Z12btFactorLDLTPfS_ii(float* %3, float* %4, i32 %5, i32 %6)
  %7 = load float*, float** %A.addr, align 4
  %8 = load float*, float** %outer_w.addr, align 4
  %9 = load float*, float** %b.addr, align 4
  %10 = load i32, i32* %n.addr, align 4
  %11 = load i32, i32* %nskip, align 4
  call void @_Z11btSolveLDLTPKfS0_Pfii(float* %7, float* %8, float* %9, i32 %10, i32 %11)
  %12 = load float*, float** %x.addr, align 4
  %13 = bitcast float* %12 to i8*
  %14 = load float*, float** %b.addr, align 4
  %15 = bitcast float* %14 to i8*
  %16 = load i32, i32* %n.addr, align 4
  %mul = mul i32 %16, 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %15, i32 %mul, i1 false)
  %17 = load i8, i8* @s_error, align 1
  %tobool = trunc i8 %17 to i1
  %lnot = xor i1 %tobool, true
  store i1 %lnot, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %18 = load i32, i32* %n.addr, align 4
  store i32 %18, i32* %nskip1, align 4
  %19 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %L = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %19, i32 0, i32 1
  %20 = load i32, i32* %n.addr, align 4
  %21 = load i32, i32* %nskip1, align 4
  %mul2 = mul nsw i32 %20, %21
  store float 0.000000e+00, float* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %L, i32 %mul2, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  %22 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %d = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %22, i32 0, i32 2
  %23 = load i32, i32* %n.addr, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %d, i32 %23, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %24 = load float*, float** %outer_w.addr, align 4
  store float* %24, float** %w, align 4
  %25 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_w = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %25, i32 0, i32 3
  %26 = load i32, i32* %n.addr, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %delta_w, i32 %26, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %27 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_x = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %27, i32 0, i32 4
  %28 = load i32, i32* %n.addr, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %delta_x, i32 %28, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %29 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %Dell = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %29, i32 0, i32 5
  %30 = load i32, i32* %n.addr, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %Dell, i32 %30, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %31 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %ell = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %31, i32 0, i32 6
  %32 = load i32, i32* %n.addr, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray* %ell, i32 %32, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %33 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %Arows = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %33, i32 0, i32 7
  %34 = load i32, i32* %n.addr, align 4
  store float* null, float** %ref.tmp8, align 4
  call void @_ZN20btAlignedObjectArrayIPfE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %Arows, i32 %34, float** nonnull align 4 dereferenceable(4) %ref.tmp8)
  %35 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %p = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %35, i32 0, i32 8
  %36 = load i32, i32* %n.addr, align 4
  store i32 0, i32* %ref.tmp9, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.4* %p, i32 %36, i32* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %37 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %C = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %37, i32 0, i32 9
  %38 = load i32, i32* %n.addr, align 4
  store i32 0, i32* %ref.tmp10, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.4* %C, i32 %38, i32* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %39 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %state = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %39, i32 0, i32 10
  %40 = load i32, i32* %n.addr, align 4
  store i8 0, i8* %ref.tmp11, align 1
  call void @_ZN20btAlignedObjectArrayIbE6resizeEiRKb(%class.btAlignedObjectArray.8* %state, i32 %40, i8* nonnull align 1 dereferenceable(1) %ref.tmp11)
  %41 = load i32, i32* %n.addr, align 4
  %42 = load i32, i32* %nskip1, align 4
  %43 = load i32, i32* %nub.addr, align 4
  %44 = load float*, float** %A.addr, align 4
  %45 = load float*, float** %x.addr, align 4
  %46 = load float*, float** %b.addr, align 4
  %47 = load float*, float** %w, align 4
  %48 = load float*, float** %lo.addr, align 4
  %49 = load float*, float** %hi.addr, align 4
  %50 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %L12 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %50, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %L12, i32 0)
  %51 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %d13 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %51, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %d13, i32 0)
  %52 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %Dell15 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %52, i32 0, i32 5
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %Dell15, i32 0)
  %53 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %ell17 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %53, i32 0, i32 6
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %ell17, i32 0)
  %54 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_w19 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %54, i32 0, i32 3
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w19, i32 0)
  %55 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %state21 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %55, i32 0, i32 10
  %call22 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state21, i32 0)
  %56 = load i32*, i32** %findex.addr, align 4
  %57 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %p23 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %57, i32 0, i32 8
  %call24 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.4* %p23, i32 0)
  %58 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %C25 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %58, i32 0, i32 9
  %call26 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.4* %C25, i32 0)
  %59 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %Arows27 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %59, i32 0, i32 7
  %call28 = call nonnull align 4 dereferenceable(4) float** @_ZN20btAlignedObjectArrayIPfEixEi(%class.btAlignedObjectArray.0* %Arows27, i32 0)
  %call29 = call %struct.btLCP* @_ZN5btLCPC1EiiiPfS0_S0_S0_S0_S0_S0_S0_S0_S0_S0_PbPiS2_S2_PS0_(%struct.btLCP* %lcp, i32 %41, i32 %42, i32 %43, float* %44, float* %45, float* %46, float* %47, float* %48, float* %49, float* %call, float* %call14, float* %call16, float* %call18, float* %call20, i8* %call22, i32* %56, i32* %call24, i32* %call26, float** %call28)
  %call30 = call i32 @_ZNK5btLCP6getNubEv(%struct.btLCP* %lcp)
  store i32 %call30, i32* %adj_nub, align 4
  store i8 0, i8* %hit_first_friction_index, align 1
  %60 = load i32, i32* %adj_nub, align 4
  store i32 %60, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc285, %if.end
  %61 = load i32, i32* %i, align 4
  %62 = load i32, i32* %n.addr, align 4
  %cmp31 = icmp slt i32 %61, %62
  br i1 %cmp31, label %for.body, label %for.end287

for.body:                                         ; preds = %for.cond
  store i8 0, i8* @s_error, align 1
  %63 = load i8, i8* %hit_first_friction_index, align 1
  %tobool32 = trunc i8 %63 to i1
  br i1 %tobool32, label %if.end65, label %land.lhs.true

land.lhs.true:                                    ; preds = %for.body
  %64 = load i32*, i32** %findex.addr, align 4
  %tobool33 = icmp ne i32* %64, null
  br i1 %tobool33, label %land.lhs.true34, label %if.end65

land.lhs.true34:                                  ; preds = %land.lhs.true
  %65 = load i32*, i32** %findex.addr, align 4
  %66 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %65, i32 %66
  %67 = load i32, i32* %arrayidx, align 4
  %cmp35 = icmp sge i32 %67, 0
  br i1 %cmp35, label %if.then36, label %if.end65

if.then36:                                        ; preds = %land.lhs.true34
  store i32 0, i32* %j, align 4
  br label %for.cond37

for.cond37:                                       ; preds = %for.inc, %if.then36
  %68 = load i32, i32* %j, align 4
  %69 = load i32, i32* %n.addr, align 4
  %cmp38 = icmp slt i32 %68, %69
  br i1 %cmp38, label %for.body39, label %for.end

for.body39:                                       ; preds = %for.cond37
  %70 = load float*, float** %x.addr, align 4
  %71 = load i32, i32* %j, align 4
  %arrayidx40 = getelementptr inbounds float, float* %70, i32 %71
  %72 = load float, float* %arrayidx40, align 4
  %73 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_w41 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %73, i32 0, i32 3
  %74 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %p42 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %74, i32 0, i32 8
  %75 = load i32, i32* %j, align 4
  %call43 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.4* %p42, i32 %75)
  %76 = load i32, i32* %call43, align 4
  %call44 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w41, i32 %76)
  store float %72, float* %call44, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body39
  %77 = load i32, i32* %j, align 4
  %inc = add nsw i32 %77, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond37

for.end:                                          ; preds = %for.cond37
  %78 = load i32, i32* %i, align 4
  store i32 %78, i32* %k, align 4
  br label %for.cond45

for.cond45:                                       ; preds = %for.inc62, %for.end
  %79 = load i32, i32* %k, align 4
  %80 = load i32, i32* %n.addr, align 4
  %cmp46 = icmp slt i32 %79, %80
  br i1 %cmp46, label %for.body47, label %for.end64

for.body47:                                       ; preds = %for.cond45
  %81 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_w48 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %81, i32 0, i32 3
  %82 = load i32*, i32** %findex.addr, align 4
  %83 = load i32, i32* %k, align 4
  %arrayidx49 = getelementptr inbounds i32, i32* %82, i32 %83
  %84 = load i32, i32* %arrayidx49, align 4
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w48, i32 %84)
  %85 = load float, float* %call50, align 4
  store float %85, float* %wfk, align 4
  %86 = load float, float* %wfk, align 4
  %cmp51 = fcmp oeq float %86, 0.000000e+00
  br i1 %cmp51, label %if.then52, label %if.else

if.then52:                                        ; preds = %for.body47
  %87 = load float*, float** %hi.addr, align 4
  %88 = load i32, i32* %k, align 4
  %arrayidx53 = getelementptr inbounds float, float* %87, i32 %88
  store float 0.000000e+00, float* %arrayidx53, align 4
  %89 = load float*, float** %lo.addr, align 4
  %90 = load i32, i32* %k, align 4
  %arrayidx54 = getelementptr inbounds float, float* %89, i32 %90
  store float 0.000000e+00, float* %arrayidx54, align 4
  br label %if.end61

if.else:                                          ; preds = %for.body47
  %91 = load float*, float** %hi.addr, align 4
  %92 = load i32, i32* %k, align 4
  %arrayidx55 = getelementptr inbounds float, float* %91, i32 %92
  %93 = load float, float* %arrayidx55, align 4
  %94 = load float, float* %wfk, align 4
  %mul56 = fmul float %93, %94
  %call57 = call float @_Z6btFabsf(float %mul56)
  %95 = load float*, float** %hi.addr, align 4
  %96 = load i32, i32* %k, align 4
  %arrayidx58 = getelementptr inbounds float, float* %95, i32 %96
  store float %call57, float* %arrayidx58, align 4
  %97 = load float*, float** %hi.addr, align 4
  %98 = load i32, i32* %k, align 4
  %arrayidx59 = getelementptr inbounds float, float* %97, i32 %98
  %99 = load float, float* %arrayidx59, align 4
  %fneg = fneg float %99
  %100 = load float*, float** %lo.addr, align 4
  %101 = load i32, i32* %k, align 4
  %arrayidx60 = getelementptr inbounds float, float* %100, i32 %101
  store float %fneg, float* %arrayidx60, align 4
  br label %if.end61

if.end61:                                         ; preds = %if.else, %if.then52
  br label %for.inc62

for.inc62:                                        ; preds = %if.end61
  %102 = load i32, i32* %k, align 4
  %inc63 = add nsw i32 %102, 1
  store i32 %inc63, i32* %k, align 4
  br label %for.cond45

for.end64:                                        ; preds = %for.cond45
  store i8 1, i8* %hit_first_friction_index, align 1
  br label %if.end65

if.end65:                                         ; preds = %for.end64, %land.lhs.true34, %land.lhs.true, %for.body
  %103 = load i32, i32* %i, align 4
  %104 = load float*, float** %x.addr, align 4
  %call66 = call float @_ZNK5btLCP12AiC_times_qCEiPf(%struct.btLCP* %lcp, i32 %103, float* %104)
  %105 = load i32, i32* %i, align 4
  %106 = load float*, float** %x.addr, align 4
  %call67 = call float @_ZNK5btLCP12AiN_times_qNEiPf(%struct.btLCP* %lcp, i32 %105, float* %106)
  %add = fadd float %call66, %call67
  %107 = load float*, float** %b.addr, align 4
  %108 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds float, float* %107, i32 %108
  %109 = load float, float* %arrayidx68, align 4
  %sub = fsub float %add, %109
  %110 = load float*, float** %w, align 4
  %111 = load i32, i32* %i, align 4
  %arrayidx69 = getelementptr inbounds float, float* %110, i32 %111
  store float %sub, float* %arrayidx69, align 4
  %112 = load float*, float** %lo.addr, align 4
  %113 = load i32, i32* %i, align 4
  %arrayidx70 = getelementptr inbounds float, float* %112, i32 %113
  %114 = load float, float* %arrayidx70, align 4
  %cmp71 = fcmp oeq float %114, 0.000000e+00
  br i1 %cmp71, label %land.lhs.true72, label %if.else78

land.lhs.true72:                                  ; preds = %if.end65
  %115 = load float*, float** %w, align 4
  %116 = load i32, i32* %i, align 4
  %arrayidx73 = getelementptr inbounds float, float* %115, i32 %116
  %117 = load float, float* %arrayidx73, align 4
  %cmp74 = fcmp oge float %117, 0.000000e+00
  br i1 %cmp74, label %if.then75, label %if.else78

if.then75:                                        ; preds = %land.lhs.true72
  %118 = load i32, i32* %i, align 4
  call void @_ZN5btLCP15transfer_i_to_NEi(%struct.btLCP* %lcp, i32 %118)
  %119 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %state76 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %119, i32 0, i32 10
  %120 = load i32, i32* %i, align 4
  %call77 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state76, i32 %120)
  store i8 0, i8* %call77, align 1
  br label %if.end281

if.else78:                                        ; preds = %land.lhs.true72, %if.end65
  %121 = load float*, float** %hi.addr, align 4
  %122 = load i32, i32* %i, align 4
  %arrayidx79 = getelementptr inbounds float, float* %121, i32 %122
  %123 = load float, float* %arrayidx79, align 4
  %cmp80 = fcmp oeq float %123, 0.000000e+00
  br i1 %cmp80, label %land.lhs.true81, label %if.else87

land.lhs.true81:                                  ; preds = %if.else78
  %124 = load float*, float** %w, align 4
  %125 = load i32, i32* %i, align 4
  %arrayidx82 = getelementptr inbounds float, float* %124, i32 %125
  %126 = load float, float* %arrayidx82, align 4
  %cmp83 = fcmp ole float %126, 0.000000e+00
  br i1 %cmp83, label %if.then84, label %if.else87

if.then84:                                        ; preds = %land.lhs.true81
  %127 = load i32, i32* %i, align 4
  call void @_ZN5btLCP15transfer_i_to_NEi(%struct.btLCP* %lcp, i32 %127)
  %128 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %state85 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %128, i32 0, i32 10
  %129 = load i32, i32* %i, align 4
  %call86 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state85, i32 %129)
  store i8 1, i8* %call86, align 1
  br label %if.end280

if.else87:                                        ; preds = %land.lhs.true81, %if.else78
  %130 = load float*, float** %w, align 4
  %131 = load i32, i32* %i, align 4
  %arrayidx88 = getelementptr inbounds float, float* %130, i32 %131
  %132 = load float, float* %arrayidx88, align 4
  %cmp89 = fcmp oeq float %132, 0.000000e+00
  br i1 %cmp89, label %if.then90, label %if.else93

if.then90:                                        ; preds = %if.else87
  %133 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_x91 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %133, i32 0, i32 4
  %call92 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x91, i32 0)
  %134 = load i32, i32* %i, align 4
  call void @_ZN5btLCP6solve1EPfiii(%struct.btLCP* %lcp, float* %call92, i32 %134, i32 0, i32 1)
  %135 = load i32, i32* %i, align 4
  call void @_ZN5btLCP15transfer_i_to_CEi(%struct.btLCP* %lcp, i32 %135)
  br label %if.end279

if.else93:                                        ; preds = %if.else87
  br label %for.cond94

for.cond94:                                       ; preds = %if.end277, %if.else93
  %136 = load float*, float** %w, align 4
  %137 = load i32, i32* %i, align 4
  %arrayidx95 = getelementptr inbounds float, float* %136, i32 %137
  %138 = load float, float* %arrayidx95, align 4
  %cmp96 = fcmp ole float %138, 0.000000e+00
  br i1 %cmp96, label %if.then97, label %if.else98

if.then97:                                        ; preds = %for.cond94
  store i32 1, i32* %dir, align 4
  store float 1.000000e+00, float* %dirf, align 4
  br label %if.end99

if.else98:                                        ; preds = %for.cond94
  store i32 -1, i32* %dir, align 4
  store float -1.000000e+00, float* %dirf, align 4
  br label %if.end99

if.end99:                                         ; preds = %if.else98, %if.then97
  %139 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_x100 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %139, i32 0, i32 4
  %call101 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x100, i32 0)
  %140 = load i32, i32* %i, align 4
  %141 = load i32, i32* %dir, align 4
  call void @_ZN5btLCP6solve1EPfiii(%struct.btLCP* %lcp, float* %call101, i32 %140, i32 %141, i32 0)
  %142 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_w102 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %142, i32 0, i32 3
  %call103 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w102, i32 0)
  %143 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_x104 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %143, i32 0, i32 4
  %call105 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x104, i32 0)
  call void @_ZN5btLCP22pN_equals_ANC_times_qCEPfS0_(%struct.btLCP* %lcp, float* %call103, float* %call105)
  %144 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_w106 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %144, i32 0, i32 3
  %call107 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w106, i32 0)
  %145 = load i32, i32* %i, align 4
  %146 = load i32, i32* %dir, align 4
  call void @_ZN5btLCP17pN_plusequals_ANiEPfii(%struct.btLCP* %lcp, float* %call107, i32 %145, i32 %146)
  %147 = load i32, i32* %i, align 4
  %148 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_x108 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %148, i32 0, i32 4
  %call109 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x108, i32 0)
  %call110 = call float @_ZNK5btLCP12AiC_times_qCEiPf(%struct.btLCP* %lcp, i32 %147, float* %call109)
  %149 = load i32, i32* %i, align 4
  %call111 = call float @_ZNK5btLCP3AiiEi(%struct.btLCP* %lcp, i32 %149)
  %150 = load float, float* %dirf, align 4
  %mul112 = fmul float %call111, %150
  %add113 = fadd float %call110, %mul112
  %151 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_w114 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %151, i32 0, i32 3
  %152 = load i32, i32* %i, align 4
  %call115 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w114, i32 %152)
  store float %add113, float* %call115, align 4
  store i32 1, i32* %cmd, align 4
  store i32 0, i32* %si, align 4
  %153 = load float*, float** %w, align 4
  %154 = load i32, i32* %i, align 4
  %arrayidx116 = getelementptr inbounds float, float* %153, i32 %154
  %155 = load float, float* %arrayidx116, align 4
  %fneg117 = fneg float %155
  %156 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_w118 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %156, i32 0, i32 3
  %157 = load i32, i32* %i, align 4
  %call119 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w118, i32 %157)
  %158 = load float, float* %call119, align 4
  %div = fdiv float %fneg117, %158
  store float %div, float* %s, align 4
  %159 = load i32, i32* %dir, align 4
  %cmp120 = icmp sgt i32 %159, 0
  br i1 %cmp120, label %if.then121, label %if.else133

if.then121:                                       ; preds = %if.end99
  %160 = load float*, float** %hi.addr, align 4
  %161 = load i32, i32* %i, align 4
  %arrayidx122 = getelementptr inbounds float, float* %160, i32 %161
  %162 = load float, float* %arrayidx122, align 4
  %163 = load float, float* getelementptr inbounds (%struct.btInfMaskConverter, %struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 0, i32 0, i32 0), align 4
  %cmp123 = fcmp olt float %162, %163
  br i1 %cmp123, label %if.then124, label %if.end132

if.then124:                                       ; preds = %if.then121
  %164 = load float*, float** %hi.addr, align 4
  %165 = load i32, i32* %i, align 4
  %arrayidx125 = getelementptr inbounds float, float* %164, i32 %165
  %166 = load float, float* %arrayidx125, align 4
  %167 = load float*, float** %x.addr, align 4
  %168 = load i32, i32* %i, align 4
  %arrayidx126 = getelementptr inbounds float, float* %167, i32 %168
  %169 = load float, float* %arrayidx126, align 4
  %sub127 = fsub float %166, %169
  %170 = load float, float* %dirf, align 4
  %mul128 = fmul float %sub127, %170
  store float %mul128, float* %s2, align 4
  %171 = load float, float* %s2, align 4
  %172 = load float, float* %s, align 4
  %cmp129 = fcmp olt float %171, %172
  br i1 %cmp129, label %if.then130, label %if.end131

if.then130:                                       ; preds = %if.then124
  %173 = load float, float* %s2, align 4
  store float %173, float* %s, align 4
  store i32 3, i32* %cmd, align 4
  br label %if.end131

if.end131:                                        ; preds = %if.then130, %if.then124
  br label %if.end132

if.end132:                                        ; preds = %if.end131, %if.then121
  br label %if.end147

if.else133:                                       ; preds = %if.end99
  %174 = load float*, float** %lo.addr, align 4
  %175 = load i32, i32* %i, align 4
  %arrayidx134 = getelementptr inbounds float, float* %174, i32 %175
  %176 = load float, float* %arrayidx134, align 4
  %177 = load float, float* getelementptr inbounds (%struct.btInfMaskConverter, %struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 0, i32 0, i32 0), align 4
  %fneg135 = fneg float %177
  %cmp136 = fcmp ogt float %176, %fneg135
  br i1 %cmp136, label %if.then137, label %if.end146

if.then137:                                       ; preds = %if.else133
  %178 = load float*, float** %lo.addr, align 4
  %179 = load i32, i32* %i, align 4
  %arrayidx139 = getelementptr inbounds float, float* %178, i32 %179
  %180 = load float, float* %arrayidx139, align 4
  %181 = load float*, float** %x.addr, align 4
  %182 = load i32, i32* %i, align 4
  %arrayidx140 = getelementptr inbounds float, float* %181, i32 %182
  %183 = load float, float* %arrayidx140, align 4
  %sub141 = fsub float %180, %183
  %184 = load float, float* %dirf, align 4
  %mul142 = fmul float %sub141, %184
  store float %mul142, float* %s2138, align 4
  %185 = load float, float* %s2138, align 4
  %186 = load float, float* %s, align 4
  %cmp143 = fcmp olt float %185, %186
  br i1 %cmp143, label %if.then144, label %if.end145

if.then144:                                       ; preds = %if.then137
  %187 = load float, float* %s2138, align 4
  store float %187, float* %s, align 4
  store i32 2, i32* %cmd, align 4
  br label %if.end145

if.end145:                                        ; preds = %if.then144, %if.then137
  br label %if.end146

if.end146:                                        ; preds = %if.end145, %if.else133
  br label %if.end147

if.end147:                                        ; preds = %if.end146, %if.end132
  %call148 = call i32 @_ZNK5btLCP4numNEv(%struct.btLCP* %lcp)
  store i32 %call148, i32* %numN, align 4
  store i32 0, i32* %k149, align 4
  br label %for.cond150

for.cond150:                                      ; preds = %for.inc181, %if.end147
  %188 = load i32, i32* %k149, align 4
  %189 = load i32, i32* %numN, align 4
  %cmp151 = icmp slt i32 %188, %189
  br i1 %cmp151, label %for.body152, label %for.end183

for.body152:                                      ; preds = %for.cond150
  %190 = load i32, i32* %k149, align 4
  %call153 = call i32 @_ZNK5btLCP6indexNEi(%struct.btLCP* %lcp, i32 %190)
  store i32 %call153, i32* %indexN_k, align 4
  %191 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %state154 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %191, i32 0, i32 10
  %192 = load i32, i32* %indexN_k, align 4
  %call155 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state154, i32 %192)
  %193 = load i8, i8* %call155, align 1
  %tobool156 = trunc i8 %193 to i1
  br i1 %tobool156, label %cond.false, label %cond.true

cond.true:                                        ; preds = %for.body152
  %194 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_w157 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %194, i32 0, i32 3
  %195 = load i32, i32* %indexN_k, align 4
  %call158 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w157, i32 %195)
  %196 = load float, float* %call158, align 4
  %cmp159 = fcmp olt float %196, 0.000000e+00
  br i1 %cmp159, label %if.then163, label %if.end180

cond.false:                                       ; preds = %for.body152
  %197 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_w160 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %197, i32 0, i32 3
  %198 = load i32, i32* %indexN_k, align 4
  %call161 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w160, i32 %198)
  %199 = load float, float* %call161, align 4
  %cmp162 = fcmp ogt float %199, 0.000000e+00
  br i1 %cmp162, label %if.then163, label %if.end180

if.then163:                                       ; preds = %cond.false, %cond.true
  %200 = load float*, float** %lo.addr, align 4
  %201 = load i32, i32* %indexN_k, align 4
  %arrayidx164 = getelementptr inbounds float, float* %200, i32 %201
  %202 = load float, float* %arrayidx164, align 4
  %cmp165 = fcmp oeq float %202, 0.000000e+00
  br i1 %cmp165, label %land.lhs.true166, label %if.end170

land.lhs.true166:                                 ; preds = %if.then163
  %203 = load float*, float** %hi.addr, align 4
  %204 = load i32, i32* %indexN_k, align 4
  %arrayidx167 = getelementptr inbounds float, float* %203, i32 %204
  %205 = load float, float* %arrayidx167, align 4
  %cmp168 = fcmp oeq float %205, 0.000000e+00
  br i1 %cmp168, label %if.then169, label %if.end170

if.then169:                                       ; preds = %land.lhs.true166
  br label %for.inc181

if.end170:                                        ; preds = %land.lhs.true166, %if.then163
  %206 = load float*, float** %w, align 4
  %207 = load i32, i32* %indexN_k, align 4
  %arrayidx172 = getelementptr inbounds float, float* %206, i32 %207
  %208 = load float, float* %arrayidx172, align 4
  %fneg173 = fneg float %208
  %209 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_w174 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %209, i32 0, i32 3
  %210 = load i32, i32* %indexN_k, align 4
  %call175 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w174, i32 %210)
  %211 = load float, float* %call175, align 4
  %div176 = fdiv float %fneg173, %211
  store float %div176, float* %s2171, align 4
  %212 = load float, float* %s2171, align 4
  %213 = load float, float* %s, align 4
  %cmp177 = fcmp olt float %212, %213
  br i1 %cmp177, label %if.then178, label %if.end179

if.then178:                                       ; preds = %if.end170
  %214 = load float, float* %s2171, align 4
  store float %214, float* %s, align 4
  store i32 4, i32* %cmd, align 4
  %215 = load i32, i32* %indexN_k, align 4
  store i32 %215, i32* %si, align 4
  br label %if.end179

if.end179:                                        ; preds = %if.then178, %if.end170
  br label %if.end180

if.end180:                                        ; preds = %if.end179, %cond.false, %cond.true
  br label %for.inc181

for.inc181:                                       ; preds = %if.end180, %if.then169
  %216 = load i32, i32* %k149, align 4
  %inc182 = add nsw i32 %216, 1
  store i32 %inc182, i32* %k149, align 4
  br label %for.cond150

for.end183:                                       ; preds = %for.cond150
  %call184 = call i32 @_ZNK5btLCP4numCEv(%struct.btLCP* %lcp)
  store i32 %call184, i32* %numC, align 4
  %217 = load i32, i32* %adj_nub, align 4
  store i32 %217, i32* %k185, align 4
  br label %for.cond186

for.cond186:                                      ; preds = %for.inc227, %for.end183
  %218 = load i32, i32* %k185, align 4
  %219 = load i32, i32* %numC, align 4
  %cmp187 = icmp slt i32 %218, %219
  br i1 %cmp187, label %for.body188, label %for.end229

for.body188:                                      ; preds = %for.cond186
  %220 = load i32, i32* %k185, align 4
  %call189 = call i32 @_ZNK5btLCP6indexCEi(%struct.btLCP* %lcp, i32 %220)
  store i32 %call189, i32* %indexC_k, align 4
  %221 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_x190 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %221, i32 0, i32 4
  %222 = load i32, i32* %indexC_k, align 4
  %call191 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x190, i32 %222)
  %223 = load float, float* %call191, align 4
  %cmp192 = fcmp olt float %223, 0.000000e+00
  br i1 %cmp192, label %land.lhs.true193, label %if.end208

land.lhs.true193:                                 ; preds = %for.body188
  %224 = load float*, float** %lo.addr, align 4
  %225 = load i32, i32* %indexC_k, align 4
  %arrayidx194 = getelementptr inbounds float, float* %224, i32 %225
  %226 = load float, float* %arrayidx194, align 4
  %227 = load float, float* getelementptr inbounds (%struct.btInfMaskConverter, %struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 0, i32 0, i32 0), align 4
  %fneg195 = fneg float %227
  %cmp196 = fcmp ogt float %226, %fneg195
  br i1 %cmp196, label %if.then197, label %if.end208

if.then197:                                       ; preds = %land.lhs.true193
  %228 = load float*, float** %lo.addr, align 4
  %229 = load i32, i32* %indexC_k, align 4
  %arrayidx199 = getelementptr inbounds float, float* %228, i32 %229
  %230 = load float, float* %arrayidx199, align 4
  %231 = load float*, float** %x.addr, align 4
  %232 = load i32, i32* %indexC_k, align 4
  %arrayidx200 = getelementptr inbounds float, float* %231, i32 %232
  %233 = load float, float* %arrayidx200, align 4
  %sub201 = fsub float %230, %233
  %234 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_x202 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %234, i32 0, i32 4
  %235 = load i32, i32* %indexC_k, align 4
  %call203 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x202, i32 %235)
  %236 = load float, float* %call203, align 4
  %div204 = fdiv float %sub201, %236
  store float %div204, float* %s2198, align 4
  %237 = load float, float* %s2198, align 4
  %238 = load float, float* %s, align 4
  %cmp205 = fcmp olt float %237, %238
  br i1 %cmp205, label %if.then206, label %if.end207

if.then206:                                       ; preds = %if.then197
  %239 = load float, float* %s2198, align 4
  store float %239, float* %s, align 4
  store i32 5, i32* %cmd, align 4
  %240 = load i32, i32* %indexC_k, align 4
  store i32 %240, i32* %si, align 4
  br label %if.end207

if.end207:                                        ; preds = %if.then206, %if.then197
  br label %if.end208

if.end208:                                        ; preds = %if.end207, %land.lhs.true193, %for.body188
  %241 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_x209 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %241, i32 0, i32 4
  %242 = load i32, i32* %indexC_k, align 4
  %call210 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x209, i32 %242)
  %243 = load float, float* %call210, align 4
  %cmp211 = fcmp ogt float %243, 0.000000e+00
  br i1 %cmp211, label %land.lhs.true212, label %if.end226

land.lhs.true212:                                 ; preds = %if.end208
  %244 = load float*, float** %hi.addr, align 4
  %245 = load i32, i32* %indexC_k, align 4
  %arrayidx213 = getelementptr inbounds float, float* %244, i32 %245
  %246 = load float, float* %arrayidx213, align 4
  %247 = load float, float* getelementptr inbounds (%struct.btInfMaskConverter, %struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 0, i32 0, i32 0), align 4
  %cmp214 = fcmp olt float %246, %247
  br i1 %cmp214, label %if.then215, label %if.end226

if.then215:                                       ; preds = %land.lhs.true212
  %248 = load float*, float** %hi.addr, align 4
  %249 = load i32, i32* %indexC_k, align 4
  %arrayidx217 = getelementptr inbounds float, float* %248, i32 %249
  %250 = load float, float* %arrayidx217, align 4
  %251 = load float*, float** %x.addr, align 4
  %252 = load i32, i32* %indexC_k, align 4
  %arrayidx218 = getelementptr inbounds float, float* %251, i32 %252
  %253 = load float, float* %arrayidx218, align 4
  %sub219 = fsub float %250, %253
  %254 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_x220 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %254, i32 0, i32 4
  %255 = load i32, i32* %indexC_k, align 4
  %call221 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x220, i32 %255)
  %256 = load float, float* %call221, align 4
  %div222 = fdiv float %sub219, %256
  store float %div222, float* %s2216, align 4
  %257 = load float, float* %s2216, align 4
  %258 = load float, float* %s, align 4
  %cmp223 = fcmp olt float %257, %258
  br i1 %cmp223, label %if.then224, label %if.end225

if.then224:                                       ; preds = %if.then215
  %259 = load float, float* %s2216, align 4
  store float %259, float* %s, align 4
  store i32 6, i32* %cmd, align 4
  %260 = load i32, i32* %indexC_k, align 4
  store i32 %260, i32* %si, align 4
  br label %if.end225

if.end225:                                        ; preds = %if.then224, %if.then215
  br label %if.end226

if.end226:                                        ; preds = %if.end225, %land.lhs.true212, %if.end208
  br label %for.inc227

for.inc227:                                       ; preds = %if.end226
  %261 = load i32, i32* %k185, align 4
  %inc228 = add nsw i32 %261, 1
  store i32 %inc228, i32* %k185, align 4
  br label %for.cond186

for.end229:                                       ; preds = %for.cond186
  %262 = load float, float* %s, align 4
  %cmp230 = fcmp ole float %262, 0.000000e+00
  br i1 %cmp230, label %if.then231, label %if.end238

if.then231:                                       ; preds = %for.end229
  %263 = load i32, i32* %i, align 4
  %264 = load i32, i32* %n.addr, align 4
  %cmp232 = icmp slt i32 %263, %264
  br i1 %cmp232, label %if.then233, label %if.end237

if.then233:                                       ; preds = %if.then231
  %265 = load float*, float** %x.addr, align 4
  %266 = load i32, i32* %i, align 4
  %add.ptr = getelementptr inbounds float, float* %265, i32 %266
  %267 = load i32, i32* %n.addr, align 4
  %268 = load i32, i32* %i, align 4
  %sub234 = sub nsw i32 %267, %268
  call void @_Z9btSetZeroIfEvPT_i(float* %add.ptr, i32 %sub234)
  %269 = load float*, float** %w, align 4
  %270 = load i32, i32* %i, align 4
  %add.ptr235 = getelementptr inbounds float, float* %269, i32 %270
  %271 = load i32, i32* %n.addr, align 4
  %272 = load i32, i32* %i, align 4
  %sub236 = sub nsw i32 %271, %272
  call void @_Z9btSetZeroIfEvPT_i(float* %add.ptr235, i32 %sub236)
  br label %if.end237

if.end237:                                        ; preds = %if.then233, %if.then231
  store i8 1, i8* @s_error, align 1
  br label %for.end278

if.end238:                                        ; preds = %for.end229
  %273 = load float*, float** %x.addr, align 4
  %274 = load float, float* %s, align 4
  %275 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_x239 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %275, i32 0, i32 4
  %call240 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_x239, i32 0)
  call void @_ZN5btLCP24pC_plusequals_s_times_qCEPffS0_(%struct.btLCP* %lcp, float* %273, float %274, float* %call240)
  %276 = load float, float* %s, align 4
  %277 = load float, float* %dirf, align 4
  %mul241 = fmul float %276, %277
  %278 = load float*, float** %x.addr, align 4
  %279 = load i32, i32* %i, align 4
  %arrayidx242 = getelementptr inbounds float, float* %278, i32 %279
  %280 = load float, float* %arrayidx242, align 4
  %add243 = fadd float %280, %mul241
  store float %add243, float* %arrayidx242, align 4
  %281 = load float*, float** %w, align 4
  %282 = load float, float* %s, align 4
  %283 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_w244 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %283, i32 0, i32 3
  %call245 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w244, i32 0)
  call void @_ZN5btLCP24pN_plusequals_s_times_qNEPffS0_(%struct.btLCP* %lcp, float* %281, float %282, float* %call245)
  %284 = load float, float* %s, align 4
  %285 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %delta_w246 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %285, i32 0, i32 3
  %286 = load i32, i32* %i, align 4
  %call247 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray* %delta_w246, i32 %286)
  %287 = load float, float* %call247, align 4
  %mul248 = fmul float %284, %287
  %288 = load float*, float** %w, align 4
  %289 = load i32, i32* %i, align 4
  %arrayidx249 = getelementptr inbounds float, float* %288, i32 %289
  %290 = load float, float* %arrayidx249, align 4
  %add250 = fadd float %290, %mul248
  store float %add250, float* %arrayidx249, align 4
  %291 = load i32, i32* %cmd, align 4
  switch i32 %291, label %sw.epilog [
    i32 1, label %sw.bb
    i32 2, label %sw.bb252
    i32 3, label %sw.bb257
    i32 4, label %sw.bb262
    i32 5, label %sw.bb264
    i32 6, label %sw.bb269
  ]

sw.bb:                                            ; preds = %if.end238
  %292 = load float*, float** %w, align 4
  %293 = load i32, i32* %i, align 4
  %arrayidx251 = getelementptr inbounds float, float* %292, i32 %293
  store float 0.000000e+00, float* %arrayidx251, align 4
  %294 = load i32, i32* %i, align 4
  call void @_ZN5btLCP15transfer_i_to_CEi(%struct.btLCP* %lcp, i32 %294)
  br label %sw.epilog

sw.bb252:                                         ; preds = %if.end238
  %295 = load float*, float** %lo.addr, align 4
  %296 = load i32, i32* %i, align 4
  %arrayidx253 = getelementptr inbounds float, float* %295, i32 %296
  %297 = load float, float* %arrayidx253, align 4
  %298 = load float*, float** %x.addr, align 4
  %299 = load i32, i32* %i, align 4
  %arrayidx254 = getelementptr inbounds float, float* %298, i32 %299
  store float %297, float* %arrayidx254, align 4
  %300 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %state255 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %300, i32 0, i32 10
  %301 = load i32, i32* %i, align 4
  %call256 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state255, i32 %301)
  store i8 0, i8* %call256, align 1
  %302 = load i32, i32* %i, align 4
  call void @_ZN5btLCP15transfer_i_to_NEi(%struct.btLCP* %lcp, i32 %302)
  br label %sw.epilog

sw.bb257:                                         ; preds = %if.end238
  %303 = load float*, float** %hi.addr, align 4
  %304 = load i32, i32* %i, align 4
  %arrayidx258 = getelementptr inbounds float, float* %303, i32 %304
  %305 = load float, float* %arrayidx258, align 4
  %306 = load float*, float** %x.addr, align 4
  %307 = load i32, i32* %i, align 4
  %arrayidx259 = getelementptr inbounds float, float* %306, i32 %307
  store float %305, float* %arrayidx259, align 4
  %308 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %state260 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %308, i32 0, i32 10
  %309 = load i32, i32* %i, align 4
  %call261 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state260, i32 %309)
  store i8 1, i8* %call261, align 1
  %310 = load i32, i32* %i, align 4
  call void @_ZN5btLCP15transfer_i_to_NEi(%struct.btLCP* %lcp, i32 %310)
  br label %sw.epilog

sw.bb262:                                         ; preds = %if.end238
  %311 = load float*, float** %w, align 4
  %312 = load i32, i32* %si, align 4
  %arrayidx263 = getelementptr inbounds float, float* %311, i32 %312
  store float 0.000000e+00, float* %arrayidx263, align 4
  %313 = load i32, i32* %si, align 4
  call void @_ZN5btLCP22transfer_i_from_N_to_CEi(%struct.btLCP* %lcp, i32 %313)
  br label %sw.epilog

sw.bb264:                                         ; preds = %if.end238
  %314 = load float*, float** %lo.addr, align 4
  %315 = load i32, i32* %si, align 4
  %arrayidx265 = getelementptr inbounds float, float* %314, i32 %315
  %316 = load float, float* %arrayidx265, align 4
  %317 = load float*, float** %x.addr, align 4
  %318 = load i32, i32* %si, align 4
  %arrayidx266 = getelementptr inbounds float, float* %317, i32 %318
  store float %316, float* %arrayidx266, align 4
  %319 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %state267 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %319, i32 0, i32 10
  %320 = load i32, i32* %si, align 4
  %call268 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state267, i32 %320)
  store i8 0, i8* %call268, align 1
  %321 = load i32, i32* %si, align 4
  %322 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %m_scratch = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %322, i32 0, i32 0
  call void @_ZN5btLCP22transfer_i_from_C_to_NEiR20btAlignedObjectArrayIfE(%struct.btLCP* %lcp, i32 %321, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %m_scratch)
  br label %sw.epilog

sw.bb269:                                         ; preds = %if.end238
  %323 = load float*, float** %hi.addr, align 4
  %324 = load i32, i32* %si, align 4
  %arrayidx270 = getelementptr inbounds float, float* %323, i32 %324
  %325 = load float, float* %arrayidx270, align 4
  %326 = load float*, float** %x.addr, align 4
  %327 = load i32, i32* %si, align 4
  %arrayidx271 = getelementptr inbounds float, float* %326, i32 %327
  store float %325, float* %arrayidx271, align 4
  %328 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %state272 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %328, i32 0, i32 10
  %329 = load i32, i32* %si, align 4
  %call273 = call nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %state272, i32 %329)
  store i8 1, i8* %call273, align 1
  %330 = load i32, i32* %si, align 4
  %331 = load %struct.btDantzigScratchMemory*, %struct.btDantzigScratchMemory** %scratchMem.addr, align 4
  %m_scratch274 = getelementptr inbounds %struct.btDantzigScratchMemory, %struct.btDantzigScratchMemory* %331, i32 0, i32 0
  call void @_ZN5btLCP22transfer_i_from_C_to_NEiR20btAlignedObjectArrayIfE(%struct.btLCP* %lcp, i32 %330, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %m_scratch274)
  br label %sw.epilog

sw.epilog:                                        ; preds = %if.end238, %sw.bb269, %sw.bb264, %sw.bb262, %sw.bb257, %sw.bb252, %sw.bb
  %332 = load i32, i32* %cmd, align 4
  %cmp275 = icmp sle i32 %332, 3
  br i1 %cmp275, label %if.then276, label %if.end277

if.then276:                                       ; preds = %sw.epilog
  br label %for.end278

if.end277:                                        ; preds = %sw.epilog
  br label %for.cond94

for.end278:                                       ; preds = %if.then276, %if.end237
  br label %if.end279

if.end279:                                        ; preds = %for.end278, %if.then90
  br label %if.end280

if.end280:                                        ; preds = %if.end279, %if.then84
  br label %if.end281

if.end281:                                        ; preds = %if.end280, %if.then75
  %333 = load i8, i8* @s_error, align 1
  %tobool282 = trunc i8 %333 to i1
  br i1 %tobool282, label %if.then283, label %if.end284

if.then283:                                       ; preds = %if.end281
  br label %for.end287

if.end284:                                        ; preds = %if.end281
  br label %for.inc285

for.inc285:                                       ; preds = %if.end284
  %334 = load i32, i32* %i, align 4
  %inc286 = add nsw i32 %334, 1
  store i32 %inc286, i32* %i, align 4
  br label %for.cond

for.end287:                                       ; preds = %if.then283, %for.cond
  call void @_ZN5btLCP9unpermuteEv(%struct.btLCP* %lcp)
  %335 = load i8, i8* @s_error, align 1
  %tobool288 = trunc i8 %335 to i1
  %lnot289 = xor i1 %tobool288, true
  store i1 %lnot289, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end287, %if.then
  %336 = load i1, i1* %retval, align 1
  ret i1 %336
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPfE6resizeEiRKS0_(%class.btAlignedObjectArray.0* %this, i32 %newsize, float** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store float** %fillData, float*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPfE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %5 = load float**, float*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float*, float** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPfE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %14 = load float**, float*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds float*, float** %14, i32 %15
  %16 = bitcast float** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to float**
  %18 = load float**, float*** %fillData.addr, align 4
  %19 = load float*, float** %18, align 4
  store float* %19, float** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.4* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store i32* %fillData, i32** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %5 = load i32*, i32** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %14 = load i32*, i32** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %14, i32 %15
  %16 = bitcast i32* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to i32*
  %18 = load i32*, i32** %fillData.addr, align 4
  %19 = load i32, i32* %18, align 4
  store i32 %19, i32* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE6resizeEiRKb(%class.btAlignedObjectArray.8* %this, i32 %newsize, i8* nonnull align 1 dereferenceable(1) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i8*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store i8* %fillData, i8** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %5 = load i8*, i8** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIbE7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %14 = load i8*, i8** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds i8, i8* %14, i32 %15
  %16 = load i8*, i8** %fillData.addr, align 4
  %17 = load i8, i8* %16, align 1
  %tobool = trunc i8 %17 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %arrayidx10, align 1
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %18 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %18, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %19 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 %19, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 1 dereferenceable(1) i8* @_ZN20btAlignedObjectArrayIbEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load i8*, i8** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 %1
  ret i8* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float** @_ZN20btAlignedObjectArrayIPfEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load float**, float*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float*, float** %0, i32 %1
  ret float** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5btLCP6getNubEv(%struct.btLCP* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nub = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_nub, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK5btLCP12AiC_times_qCEiPf(%struct.btLCP* %this, i32 %i, float* %q) #1 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  %q.addr = alloca float*, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  store float* %q, float** %q.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %0 = load float**, float*** %m_A, align 4
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds float*, float** %0, i32 %1
  %2 = load float*, float** %arrayidx, align 4
  %3 = load float*, float** %q.addr, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %4 = load i32, i32* %m_nC, align 4
  %call = call float @_Z10btLargeDotPKfS0_i(float* %2, float* %3, i32 %4)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK5btLCP12AiN_times_qNEiPf(%struct.btLCP* %this, i32 %i, float* %q) #1 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  %q.addr = alloca float*, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  store float* %q, float** %q.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %0 = load float**, float*** %m_A, align 4
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds float*, float** %0, i32 %1
  %2 = load float*, float** %arrayidx, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %3 = load i32, i32* %m_nC, align 4
  %add.ptr = getelementptr inbounds float, float* %2, i32 %3
  %4 = load float*, float** %q.addr, align 4
  %m_nC2 = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %5 = load i32, i32* %m_nC2, align 4
  %add.ptr3 = getelementptr inbounds float, float* %4, i32 %5
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %6 = load i32, i32* %m_nN, align 4
  %call = call float @_Z10btLargeDotPKfS0_i(float* %add.ptr, float* %add.ptr3, i32 %6)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN5btLCP15transfer_i_to_NEi(%struct.btLCP* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_nN, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_nN, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK5btLCP3AiiEi(%struct.btLCP* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_A = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 5
  %0 = load float**, float*** %m_A, align 4
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds float*, float** %0, i32 %1
  %2 = load float*, float** %arrayidx, align 4
  %3 = load i32, i32* %i.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %2, i32 %3
  %4 = load float, float* %arrayidx2, align 4
  ret float %4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5btLCP4numNEv(%struct.btLCP* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nN = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_nN, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5btLCP6indexNEi(%struct.btLCP* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %1 = load i32, i32* %m_nC, align 4
  %add = add nsw i32 %0, %1
  ret i32 %add
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5btLCP4numCEv(%struct.btLCP* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %m_nC = getelementptr inbounds %struct.btLCP, %struct.btLCP* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_nC, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK5btLCP6indexCEi(%struct.btLCP* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %struct.btLCP*, align 4
  %i.addr = alloca i32, align 4
  store %struct.btLCP* %this, %struct.btLCP** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %struct.btLCP*, %struct.btLCP** %this.addr, align 4
  %0 = load i32, i32* %i.addr, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define internal void @_ZL17btSwapRowsAndColsPPfiiiii(float** %A, i32 %n, i32 %i1, i32 %i2, i32 %nskip, i32 %do_fast_row_swaps) #1 {
entry:
  %A.addr = alloca float**, align 4
  %n.addr = alloca i32, align 4
  %i1.addr = alloca i32, align 4
  %i2.addr = alloca i32, align 4
  %nskip.addr = alloca i32, align 4
  %do_fast_row_swaps.addr = alloca i32, align 4
  %A_i1 = alloca float*, align 4
  %A_i2 = alloca float*, align 4
  %i = alloca i32, align 4
  %A_i_i1 = alloca float*, align 4
  %k = alloca i32, align 4
  %tmp = alloca float, align 4
  %j = alloca i32, align 4
  %A_j = alloca float*, align 4
  %tmp28 = alloca float, align 4
  store float** %A, float*** %A.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32 %i1, i32* %i1.addr, align 4
  store i32 %i2, i32* %i2.addr, align 4
  store i32 %nskip, i32* %nskip.addr, align 4
  store i32 %do_fast_row_swaps, i32* %do_fast_row_swaps.addr, align 4
  %0 = load float**, float*** %A.addr, align 4
  %1 = load i32, i32* %i1.addr, align 4
  %arrayidx = getelementptr inbounds float*, float** %0, i32 %1
  %2 = load float*, float** %arrayidx, align 4
  store float* %2, float** %A_i1, align 4
  %3 = load float**, float*** %A.addr, align 4
  %4 = load i32, i32* %i2.addr, align 4
  %arrayidx1 = getelementptr inbounds float*, float** %3, i32 %4
  %5 = load float*, float** %arrayidx1, align 4
  store float* %5, float** %A_i2, align 4
  %6 = load i32, i32* %i1.addr, align 4
  %add = add nsw i32 %6, 1
  store i32 %add, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4
  %8 = load i32, i32* %i2.addr, align 4
  %cmp = icmp slt i32 %7, %8
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load float**, float*** %A.addr, align 4
  %10 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float*, float** %9, i32 %10
  %11 = load float*, float** %arrayidx2, align 4
  %12 = load i32, i32* %i1.addr, align 4
  %add.ptr = getelementptr inbounds float, float* %11, i32 %12
  store float* %add.ptr, float** %A_i_i1, align 4
  %13 = load float*, float** %A_i_i1, align 4
  %14 = load float, float* %13, align 4
  %15 = load float*, float** %A_i1, align 4
  %16 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds float, float* %15, i32 %16
  store float %14, float* %arrayidx3, align 4
  %17 = load float*, float** %A_i2, align 4
  %18 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds float, float* %17, i32 %18
  %19 = load float, float* %arrayidx4, align 4
  %20 = load float*, float** %A_i_i1, align 4
  store float %19, float* %20, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %21 = load i32, i32* %i, align 4
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %22 = load float*, float** %A_i1, align 4
  %23 = load i32, i32* %i1.addr, align 4
  %arrayidx5 = getelementptr inbounds float, float* %22, i32 %23
  %24 = load float, float* %arrayidx5, align 4
  %25 = load float*, float** %A_i1, align 4
  %26 = load i32, i32* %i2.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %25, i32 %26
  store float %24, float* %arrayidx6, align 4
  %27 = load float*, float** %A_i2, align 4
  %28 = load i32, i32* %i1.addr, align 4
  %arrayidx7 = getelementptr inbounds float, float* %27, i32 %28
  %29 = load float, float* %arrayidx7, align 4
  %30 = load float*, float** %A_i1, align 4
  %31 = load i32, i32* %i1.addr, align 4
  %arrayidx8 = getelementptr inbounds float, float* %30, i32 %31
  store float %29, float* %arrayidx8, align 4
  %32 = load float*, float** %A_i2, align 4
  %33 = load i32, i32* %i2.addr, align 4
  %arrayidx9 = getelementptr inbounds float, float* %32, i32 %33
  %34 = load float, float* %arrayidx9, align 4
  %35 = load float*, float** %A_i2, align 4
  %36 = load i32, i32* %i1.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %35, i32 %36
  store float %34, float* %arrayidx10, align 4
  %37 = load i32, i32* %do_fast_row_swaps.addr, align 4
  %tobool = icmp ne i32 %37, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.end
  %38 = load float*, float** %A_i2, align 4
  %39 = load float**, float*** %A.addr, align 4
  %40 = load i32, i32* %i1.addr, align 4
  %arrayidx11 = getelementptr inbounds float*, float** %39, i32 %40
  store float* %38, float** %arrayidx11, align 4
  %41 = load float*, float** %A_i1, align 4
  %42 = load float**, float*** %A.addr, align 4
  %43 = load i32, i32* %i2.addr, align 4
  %arrayidx12 = getelementptr inbounds float*, float** %42, i32 %43
  store float* %41, float** %arrayidx12, align 4
  br label %if.end

if.else:                                          ; preds = %for.end
  store i32 0, i32* %k, align 4
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc20, %if.else
  %44 = load i32, i32* %k, align 4
  %45 = load i32, i32* %i2.addr, align 4
  %cmp14 = icmp sle i32 %44, %45
  br i1 %cmp14, label %for.body15, label %for.end22

for.body15:                                       ; preds = %for.cond13
  %46 = load float*, float** %A_i1, align 4
  %47 = load i32, i32* %k, align 4
  %arrayidx16 = getelementptr inbounds float, float* %46, i32 %47
  %48 = load float, float* %arrayidx16, align 4
  store float %48, float* %tmp, align 4
  %49 = load float*, float** %A_i2, align 4
  %50 = load i32, i32* %k, align 4
  %arrayidx17 = getelementptr inbounds float, float* %49, i32 %50
  %51 = load float, float* %arrayidx17, align 4
  %52 = load float*, float** %A_i1, align 4
  %53 = load i32, i32* %k, align 4
  %arrayidx18 = getelementptr inbounds float, float* %52, i32 %53
  store float %51, float* %arrayidx18, align 4
  %54 = load float, float* %tmp, align 4
  %55 = load float*, float** %A_i2, align 4
  %56 = load i32, i32* %k, align 4
  %arrayidx19 = getelementptr inbounds float, float* %55, i32 %56
  store float %54, float* %arrayidx19, align 4
  br label %for.inc20

for.inc20:                                        ; preds = %for.body15
  %57 = load i32, i32* %k, align 4
  %inc21 = add nsw i32 %57, 1
  store i32 %inc21, i32* %k, align 4
  br label %for.cond13

for.end22:                                        ; preds = %for.cond13
  br label %if.end

if.end:                                           ; preds = %for.end22, %if.then
  %58 = load i32, i32* %i2.addr, align 4
  %add23 = add nsw i32 %58, 1
  store i32 %add23, i32* %j, align 4
  br label %for.cond24

for.cond24:                                       ; preds = %for.inc33, %if.end
  %59 = load i32, i32* %j, align 4
  %60 = load i32, i32* %n.addr, align 4
  %cmp25 = icmp slt i32 %59, %60
  br i1 %cmp25, label %for.body26, label %for.end35

for.body26:                                       ; preds = %for.cond24
  %61 = load float**, float*** %A.addr, align 4
  %62 = load i32, i32* %j, align 4
  %arrayidx27 = getelementptr inbounds float*, float** %61, i32 %62
  %63 = load float*, float** %arrayidx27, align 4
  store float* %63, float** %A_j, align 4
  %64 = load float*, float** %A_j, align 4
  %65 = load i32, i32* %i1.addr, align 4
  %arrayidx29 = getelementptr inbounds float, float* %64, i32 %65
  %66 = load float, float* %arrayidx29, align 4
  store float %66, float* %tmp28, align 4
  %67 = load float*, float** %A_j, align 4
  %68 = load i32, i32* %i2.addr, align 4
  %arrayidx30 = getelementptr inbounds float, float* %67, i32 %68
  %69 = load float, float* %arrayidx30, align 4
  %70 = load float*, float** %A_j, align 4
  %71 = load i32, i32* %i1.addr, align 4
  %arrayidx31 = getelementptr inbounds float, float* %70, i32 %71
  store float %69, float* %arrayidx31, align 4
  %72 = load float, float* %tmp28, align 4
  %73 = load float*, float** %A_j, align 4
  %74 = load i32, i32* %i2.addr, align 4
  %arrayidx32 = getelementptr inbounds float, float* %73, i32 %74
  store float %72, float* %arrayidx32, align 4
  br label %for.inc33

for.inc33:                                        ; preds = %for.body26
  %75 = load i32, i32* %j, align 4
  %inc34 = add nsw i32 %75, 1
  store i32 %inc34, i32* %j, align 4
  br label %for.cond24

for.end35:                                        ; preds = %for.cond24
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to float*
  store float* %2, float** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load float*, float** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, float* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load float*, float** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store float* %4, float** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, float* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store float* %dest, float** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  %5 = bitcast float* %arrayidx to i8*
  %6 = bitcast i8* %5 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load float*, float** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx2, align 4
  store float %9, float* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load float*, float** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator* %this, i32 %n, float** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store float** %hint, float*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator* %this, float* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store float* %ptr, float** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPfE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPfE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float**, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPfE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIPfE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %1)
  %2 = bitcast i8* %call2 to float**
  store float** %2, float*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPfE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %3 = load float**, float*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIPfE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, float** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPfE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIPfE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIPfE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load float**, float*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store float** %4, float*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPfE8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIPfE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call float** @_ZN18btAlignedAllocatorIPfLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, float*** null)
  %2 = bitcast float** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIPfE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, float** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store float** %dest, float*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float**, float*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float*, float** %3, i32 %4
  %5 = bitcast float** %arrayidx to i8*
  %6 = bitcast i8* %5 to float**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load float**, float*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float*, float** %7, i32 %8
  %9 = load float*, float** %arrayidx2, align 4
  store float* %9, float** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPfE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load float**, float*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float*, float** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPfE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load float**, float*** %m_data, align 4
  %tobool = icmp ne float** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load float**, float*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIPfLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, float** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store float** null, float*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float** @_ZN18btAlignedAllocatorIPfLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, float*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float***, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store float*** %hint, float**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float**
  ret float** %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPfLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, float** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca float**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store float** %ptr, float*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load float**, float*** %ptr.addr, align 4
  %1 = bitcast float** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i32*
  store i32* %2, i32** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %3 = load i32*, i32** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, i32* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i32*, i32** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store i32* %4, i32** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.5* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i32* %dest, i32** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = bitcast i32* %arrayidx to i8*
  %6 = bitcast i8* %5 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx2, align 4
  store i32 %9, i32* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.5* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.5* %this, i32 %n, i32** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32** %hint, i32*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.5* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIbE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIbE8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %1)
  store i8* %call2, i8** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %2 = load i8*, i8** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIbE4copyEiiPb(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, i8* %2)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIbE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIbE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIbE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %3 = load i8*, i8** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i8* %3, i8** %m_data, align 4
  %4 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %4, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIbE8capacityEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIbE8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i8* @_ZN18btAlignedAllocatorIbLj16EE8allocateEiPPKb(%class.btAlignedAllocator.9* %m_allocator, i32 %1, i8** null)
  store i8* %call, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i8*, i8** %retval, align 4
  ret i8* %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIbE4copyEiiPb(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, i8* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i8*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i8* %dest, i8** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i8*, i8** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 %4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %5 = load i8*, i8** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i8, i8* %5, i32 %6
  %7 = load i8, i8* %arrayidx2, align 1
  %tobool = trunc i8 %7 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %arrayidx, align 1
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %3 = load i8*, i8** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i8, i8* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIbE10deallocateEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load i8*, i8** %m_data, align 4
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load i8*, i8** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIbLj16EE10deallocateEPb(%class.btAlignedAllocator.9* %m_allocator, i8* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i8* null, i8** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN18btAlignedAllocatorIbLj16EE8allocateEiPPKb(%class.btAlignedAllocator.9* %this, i32 %n, i8** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i8**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i8** %hint, i8*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 1, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  ret i8* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIbLj16EE10deallocateEPb(%class.btAlignedAllocator.9* %this, i8* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca i8*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btDantzigLCP.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind readnone speculatable willreturn }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
