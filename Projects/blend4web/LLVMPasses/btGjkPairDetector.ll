; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btGjkPairDetector.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btGjkPairDetector.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btGjkPairDetector = type { %struct.btDiscreteCollisionDetectorInterface, %class.btVector3, %class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, i32, i32, float, float, i8, float, i32, i32, i32, i32, i32 }
%struct.btDiscreteCollisionDetectorInterface = type { i32 (...)** }
%class.btVector3 = type { [4 x float] }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btVoronoiSimplexSolver = type <{ i32, [5 x %class.btVector3], [5 x %class.btVector3], [5 x %class.btVector3], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, i8, [3 x i8], %struct.btSubSimplexClosestResult, i8, [3 x i8] }>
%struct.btSubSimplexClosestResult = type <{ %class.btVector3, %struct.btUsageBitfield, [2 x i8], [4 x float], i8, [3 x i8] }>
%struct.btUsageBitfield = type { i8, i8 }
%class.btConvexPenetrationDepthSolver = type { i32 (...)** }
%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput" = type { %class.btTransform, %class.btTransform, float }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%class.btIDebugDraw = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceC2Ev = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZNK16btCollisionShape10isConvex2dEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK22btVoronoiSimplexSolver11fullSimplexEv = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN9btVector37setZeroEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZN17btGjkPairDetectorD2Ev = comdat any

$_ZN17btGjkPairDetectorD0Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD0Ev = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN17btBroadphaseProxy10isConvex2dEi = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZTS36btDiscreteCollisionDetectorInterface = comdat any

$_ZTI36btDiscreteCollisionDetectorInterface = comdat any

$_ZTV36btDiscreteCollisionDetectorInterface = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@gGjkEpaPenetrationTolerance = hidden global float 0x3F50624DE0000000, align 4
@gNumDeepPenetrationChecks = hidden global i32 0, align 4
@gNumGjkChecks = hidden global i32 0, align 4
@_ZTV17btGjkPairDetector = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI17btGjkPairDetector to i8*), i8* bitcast (%class.btGjkPairDetector* (%class.btGjkPairDetector*)* @_ZN17btGjkPairDetectorD2Ev to i8*), i8* bitcast (void (%class.btGjkPairDetector*)* @_ZN17btGjkPairDetectorD0Ev to i8*), i8* bitcast (void (%class.btGjkPairDetector*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btIDebugDraw*, i1)* @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS17btGjkPairDetector = hidden constant [20 x i8] c"17btGjkPairDetector\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS36btDiscreteCollisionDetectorInterface = linkonce_odr hidden constant [39 x i8] c"36btDiscreteCollisionDetectorInterface\00", comdat, align 1
@_ZTI36btDiscreteCollisionDetectorInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([39 x i8], [39 x i8]* @_ZTS36btDiscreteCollisionDetectorInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI17btGjkPairDetector = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btGjkPairDetector, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI36btDiscreteCollisionDetectorInterface to i8*) }, align 4
@_ZTV36btDiscreteCollisionDetectorInterface = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI36btDiscreteCollisionDetectorInterface to i8*), i8* bitcast (%struct.btDiscreteCollisionDetectorInterface* (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev to i8*), i8* bitcast (void (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btGjkPairDetector.cpp, i8* null }]

@_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver = hidden unnamed_addr alias %class.btGjkPairDetector* (%class.btGjkPairDetector*, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*), %class.btGjkPairDetector* (%class.btGjkPairDetector*, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*)* @_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
@_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver = hidden unnamed_addr alias %class.btGjkPairDetector* (%class.btGjkPairDetector*, %class.btConvexShape*, %class.btConvexShape*, i32, i32, float, float, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*), %class.btGjkPairDetector* (%class.btGjkPairDetector*, %class.btConvexShape*, %class.btConvexShape*, i32, i32, float, float, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*)* @_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* returned %this, %class.btConvexShape* %objectA, %class.btConvexShape* %objectB, %class.btVoronoiSimplexSolver* %simplexSolver, %class.btConvexPenetrationDepthSolver* %penetrationDepthSolver) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  %objectA.addr = alloca %class.btConvexShape*, align 4
  %objectB.addr = alloca %class.btConvexShape*, align 4
  %simplexSolver.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %penetrationDepthSolver.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  store %class.btConvexShape* %objectA, %class.btConvexShape** %objectA.addr, align 4
  store %class.btConvexShape* %objectB, %class.btConvexShape** %objectB.addr, align 4
  store %class.btVoronoiSimplexSolver* %simplexSolver, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4
  store %class.btConvexPenetrationDepthSolver* %penetrationDepthSolver, %class.btConvexPenetrationDepthSolver** %penetrationDepthSolver.addr, align 4
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = bitcast %class.btGjkPairDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceC2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #8
  %1 = bitcast %class.btGjkPairDetector* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV17btGjkPairDetector, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_cachedSeparatingAxis = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_cachedSeparatingAxis, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %m_penetrationDepthSolver = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 2
  %2 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %penetrationDepthSolver.addr, align 4
  store %class.btConvexPenetrationDepthSolver* %2, %class.btConvexPenetrationDepthSolver** %m_penetrationDepthSolver, align 4
  %m_simplexSolver = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %3 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4
  store %class.btVoronoiSimplexSolver* %3, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4
  %m_minkowskiA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 4
  %4 = load %class.btConvexShape*, %class.btConvexShape** %objectA.addr, align 4
  store %class.btConvexShape* %4, %class.btConvexShape** %m_minkowskiA, align 4
  %m_minkowskiB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 5
  %5 = load %class.btConvexShape*, %class.btConvexShape** %objectB.addr, align 4
  store %class.btConvexShape* %5, %class.btConvexShape** %m_minkowskiB, align 4
  %m_shapeTypeA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 6
  %6 = load %class.btConvexShape*, %class.btConvexShape** %objectA.addr, align 4
  %7 = bitcast %class.btConvexShape* %6 to %class.btCollisionShape*
  %call5 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %7)
  store i32 %call5, i32* %m_shapeTypeA, align 4
  %m_shapeTypeB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 7
  %8 = load %class.btConvexShape*, %class.btConvexShape** %objectB.addr, align 4
  %9 = bitcast %class.btConvexShape* %8 to %class.btCollisionShape*
  %call6 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %9)
  store i32 %call6, i32* %m_shapeTypeB, align 4
  %m_marginA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 8
  %10 = load %class.btConvexShape*, %class.btConvexShape** %objectA.addr, align 4
  %11 = bitcast %class.btConvexShape* %10 to float (%class.btConvexShape*)***
  %vtable = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %11, align 4
  %vfn = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable, i64 12
  %12 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn, align 4
  %call7 = call float %12(%class.btConvexShape* %10)
  store float %call7, float* %m_marginA, align 4
  %m_marginB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 9
  %13 = load %class.btConvexShape*, %class.btConvexShape** %objectB.addr, align 4
  %14 = bitcast %class.btConvexShape* %13 to float (%class.btConvexShape*)***
  %vtable8 = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %14, align 4
  %vfn9 = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable8, i64 12
  %15 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn9, align 4
  %call10 = call float %15(%class.btConvexShape* %13)
  store float %call10, float* %m_marginB, align 4
  %m_ignoreMargin = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 10
  store i8 0, i8* %m_ignoreMargin, align 4
  %m_lastUsedMethod = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 -1, i32* %m_lastUsedMethod, align 4
  %m_catchDegeneracies = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 15
  store i32 1, i32* %m_catchDegeneracies, align 4
  %m_fixContactNormalDirection = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 16
  store i32 1, i32* %m_fixContactNormalDirection, align 4
  ret %class.btGjkPairDetector* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceC2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %0 = bitcast %struct.btDiscreteCollisionDetectorInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV36btDiscreteCollisionDetectorInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC2EPK13btConvexShapeS2_iiffP22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* returned %this, %class.btConvexShape* %objectA, %class.btConvexShape* %objectB, i32 %shapeTypeA, i32 %shapeTypeB, float %marginA, float %marginB, %class.btVoronoiSimplexSolver* %simplexSolver, %class.btConvexPenetrationDepthSolver* %penetrationDepthSolver) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  %objectA.addr = alloca %class.btConvexShape*, align 4
  %objectB.addr = alloca %class.btConvexShape*, align 4
  %shapeTypeA.addr = alloca i32, align 4
  %shapeTypeB.addr = alloca i32, align 4
  %marginA.addr = alloca float, align 4
  %marginB.addr = alloca float, align 4
  %simplexSolver.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  %penetrationDepthSolver.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  store %class.btConvexShape* %objectA, %class.btConvexShape** %objectA.addr, align 4
  store %class.btConvexShape* %objectB, %class.btConvexShape** %objectB.addr, align 4
  store i32 %shapeTypeA, i32* %shapeTypeA.addr, align 4
  store i32 %shapeTypeB, i32* %shapeTypeB.addr, align 4
  store float %marginA, float* %marginA.addr, align 4
  store float %marginB, float* %marginB.addr, align 4
  store %class.btVoronoiSimplexSolver* %simplexSolver, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4
  store %class.btConvexPenetrationDepthSolver* %penetrationDepthSolver, %class.btConvexPenetrationDepthSolver** %penetrationDepthSolver.addr, align 4
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = bitcast %class.btGjkPairDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceC2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #8
  %1 = bitcast %class.btGjkPairDetector* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV17btGjkPairDetector, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_cachedSeparatingAxis = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_cachedSeparatingAxis, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %m_penetrationDepthSolver = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 2
  %2 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %penetrationDepthSolver.addr, align 4
  store %class.btConvexPenetrationDepthSolver* %2, %class.btConvexPenetrationDepthSolver** %m_penetrationDepthSolver, align 4
  %m_simplexSolver = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %3 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4
  store %class.btVoronoiSimplexSolver* %3, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4
  %m_minkowskiA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 4
  %4 = load %class.btConvexShape*, %class.btConvexShape** %objectA.addr, align 4
  store %class.btConvexShape* %4, %class.btConvexShape** %m_minkowskiA, align 4
  %m_minkowskiB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 5
  %5 = load %class.btConvexShape*, %class.btConvexShape** %objectB.addr, align 4
  store %class.btConvexShape* %5, %class.btConvexShape** %m_minkowskiB, align 4
  %m_shapeTypeA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 6
  %6 = load i32, i32* %shapeTypeA.addr, align 4
  store i32 %6, i32* %m_shapeTypeA, align 4
  %m_shapeTypeB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 7
  %7 = load i32, i32* %shapeTypeB.addr, align 4
  store i32 %7, i32* %m_shapeTypeB, align 4
  %m_marginA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 8
  %8 = load float, float* %marginA.addr, align 4
  store float %8, float* %m_marginA, align 4
  %m_marginB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 9
  %9 = load float, float* %marginB.addr, align 4
  store float %9, float* %m_marginB, align 4
  %m_ignoreMargin = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 10
  store i8 0, i8* %m_ignoreMargin, align 4
  %m_lastUsedMethod = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 -1, i32* %m_lastUsedMethod, align 4
  %m_catchDegeneracies = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 15
  store i32 1, i32* %m_catchDegeneracies, align 4
  %m_fixContactNormalDirection = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 16
  store i32 1, i32* %m_fixContactNormalDirection, align 4
  ret %class.btGjkPairDetector* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector* %this, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %output, %class.btIDebugDraw* %debugDraw, i1 zeroext %swapResults) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  %input.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, align 4
  %output.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %debugDraw.addr = alloca %class.btIDebugDraw*, align 4
  %swapResults.addr = alloca i8, align 1
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %output, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  store %class.btIDebugDraw* %debugDraw, %class.btIDebugDraw** %debugDraw.addr, align 4
  %frombool = zext i1 %swapResults to i8
  store i8 %frombool, i8* %swapResults.addr, align 1
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  %2 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDraw.addr, align 4
  call void @_ZN17btGjkPairDetector26getClosestPointsNonVirtualERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDraw(%class.btGjkPairDetector* %this1, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %0, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %1, %class.btIDebugDraw* %2)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN17btGjkPairDetector26getClosestPointsNonVirtualERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDraw(%class.btGjkPairDetector* %this, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %output, %class.btIDebugDraw* %debugDraw) #2 {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  %input.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, align 4
  %output.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %debugDraw.addr = alloca %class.btIDebugDraw*, align 4
  %distance = alloca float, align 4
  %normalInB = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %pointOnA = alloca %class.btVector3, align 4
  %pointOnB = alloca %class.btVector3, align 4
  %localTransA = alloca %class.btTransform, align 4
  %localTransB = alloca %class.btTransform, align 4
  %positionOffset = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  %check2d = alloca i8, align 1
  %marginA = alloca float, align 4
  %marginB = alloca float, align 4
  %gGjkMaxIter = alloca i32, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %isValid = alloca i8, align 1
  %checkSimplex = alloca i8, align 1
  %checkPenetration = alloca i8, align 1
  %squaredDistance = alloca float, align 4
  %delta = alloca float, align 4
  %margin = alloca float, align 4
  %seperatingAxisInA = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %seperatingAxisInB = alloca %class.btVector3, align 4
  %pInA = alloca %class.btVector3, align 4
  %qInB = alloca %class.btVector3, align 4
  %pWorld = alloca %class.btVector3, align 4
  %qWorld = alloca %class.btVector3, align 4
  %w = alloca %class.btVector3, align 4
  %f0 = alloca float, align 4
  %f1 = alloca float, align 4
  %newCachedSeparatingAxis = alloca %class.btVector3, align 4
  %previousSquaredDistance = alloca float, align 4
  %check = alloca i8, align 1
  %lenSqr = alloca float, align 4
  %rlen = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp105 = alloca %class.btVector3, align 4
  %ref.tmp107 = alloca float, align 4
  %ref.tmp110 = alloca %class.btVector3, align 4
  %ref.tmp112 = alloca float, align 4
  %catchDegeneratePenetrationCase = alloca i8, align 1
  %tmpPointOnA = alloca %class.btVector3, align 4
  %tmpPointOnB = alloca %class.btVector3, align 4
  %isValid2 = alloca i8, align 1
  %tmpNormalInB = alloca %class.btVector3, align 4
  %lenSqr154 = alloca float, align 4
  %ref.tmp164 = alloca float, align 4
  %distance2 = alloca float, align 4
  %ref.tmp167 = alloca %class.btVector3, align 4
  %distance2185 = alloca float, align 4
  %ref.tmp186 = alloca %class.btVector3, align 4
  %ref.tmp193 = alloca %class.btVector3, align 4
  %ref.tmp196 = alloca %class.btVector3, align 4
  %d1 = alloca float, align 4
  %seperatingAxisInA219 = alloca %class.btVector3, align 4
  %seperatingAxisInB222 = alloca %class.btVector3, align 4
  %ref.tmp223 = alloca %class.btVector3, align 4
  %pInA226 = alloca %class.btVector3, align 4
  %qInB228 = alloca %class.btVector3, align 4
  %pWorld230 = alloca %class.btVector3, align 4
  %qWorld231 = alloca %class.btVector3, align 4
  %w232 = alloca %class.btVector3, align 4
  %ref.tmp233 = alloca %class.btVector3, align 4
  %d0 = alloca float, align 4
  %seperatingAxisInA235 = alloca %class.btVector3, align 4
  %ref.tmp236 = alloca %class.btVector3, align 4
  %seperatingAxisInB239 = alloca %class.btVector3, align 4
  %pInA242 = alloca %class.btVector3, align 4
  %qInB244 = alloca %class.btVector3, align 4
  %pWorld246 = alloca %class.btVector3, align 4
  %qWorld247 = alloca %class.btVector3, align 4
  %w248 = alloca %class.btVector3, align 4
  %ref.tmp253 = alloca float, align 4
  %ref.tmp256 = alloca %class.btVector3, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %output, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  store %class.btIDebugDraw* %debugDraw, %class.btIDebugDraw** %debugDraw.addr, align 4
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %m_cachedSeparatingDistance = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 11
  store float 0.000000e+00, float* %m_cachedSeparatingDistance, align 4
  store float 0.000000e+00, float* %distance, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %normalInB, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pointOnA)
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pointOnB)
  %0 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %0, i32 0, i32 0
  %call6 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %localTransA, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transformA)
  %1 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %1, i32 0, i32 1
  %call7 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %localTransB, %class.btTransform* nonnull align 4 dereferenceable(64) %m_transformB)
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %localTransA)
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %localTransB)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %call9, %class.btVector3* nonnull align 4 dereferenceable(16) %call10)
  store float 5.000000e-01, float* %ref.tmp11, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %positionOffset, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %localTransA)
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %call12, %class.btVector3* nonnull align 4 dereferenceable(16) %positionOffset)
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %localTransB)
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %call14, %class.btVector3* nonnull align 4 dereferenceable(16) %positionOffset)
  %m_minkowskiA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 4
  %2 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiA, align 4
  %3 = bitcast %class.btConvexShape* %2 to %class.btCollisionShape*
  %call16 = call zeroext i1 @_ZNK16btCollisionShape10isConvex2dEv(%class.btCollisionShape* %3)
  br i1 %call16, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %m_minkowskiB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 5
  %4 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiB, align 4
  %5 = bitcast %class.btConvexShape* %4 to %class.btCollisionShape*
  %call17 = call zeroext i1 @_ZNK16btCollisionShape10isConvex2dEv(%class.btCollisionShape* %5)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %6 = phi i1 [ false, %entry ], [ %call17, %land.rhs ]
  %frombool = zext i1 %6 to i8
  store i8 %frombool, i8* %check2d, align 1
  %m_marginA = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 8
  %7 = load float, float* %m_marginA, align 4
  store float %7, float* %marginA, align 4
  %m_marginB = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 9
  %8 = load float, float* %m_marginB, align 4
  store float %8, float* %marginB, align 4
  %9 = load i32, i32* @gNumGjkChecks, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* @gNumGjkChecks, align 4
  %m_ignoreMargin = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 10
  %10 = load i8, i8* %m_ignoreMargin, align 4
  %tobool = trunc i8 %10 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %land.end
  store float 0.000000e+00, float* %marginA, align 4
  store float 0.000000e+00, float* %marginB, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %land.end
  %m_curIter = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 13
  store i32 0, i32* %m_curIter, align 4
  store i32 1000, i32* %gGjkMaxIter, align 4
  %m_cachedSeparatingAxis = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp18, align 4
  store float 1.000000e+00, float* %ref.tmp19, align 4
  store float 0.000000e+00, float* %ref.tmp20, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_cachedSeparatingAxis, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20)
  store i8 0, i8* %isValid, align 1
  store i8 0, i8* %checkSimplex, align 1
  store i8 1, i8* %checkPenetration, align 1
  %m_degenerateSimplex = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 0, i32* %m_degenerateSimplex, align 4
  %m_lastUsedMethod = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 -1, i32* %m_lastUsedMethod, align 4
  store float 0x43ABC16D60000000, float* %squaredDistance, align 4
  store float 0.000000e+00, float* %delta, align 4
  %11 = load float, float* %marginA, align 4
  %12 = load float, float* %marginB, align 4
  %add = fadd float %11, %12
  store float %add, float* %margin, align 4
  %m_simplexSolver = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %13 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4
  call void @_ZN22btVoronoiSimplexSolver5resetEv(%class.btVoronoiSimplexSolver* %13)
  br label %for.cond

for.cond:                                         ; preds = %if.end89, %if.end
  %m_cachedSeparatingAxis22 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedSeparatingAxis22)
  %14 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %m_transformA23 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %14, i32 0, i32 0
  %call24 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %m_transformA23)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %seperatingAxisInA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call24)
  %m_cachedSeparatingAxis25 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %15 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %m_transformB26 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %15, i32 0, i32 1
  %call27 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %m_transformB26)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %seperatingAxisInB, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedSeparatingAxis25, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call27)
  %m_minkowskiA28 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 4
  %16 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiA28, align 4
  call void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4 %pInA, %class.btConvexShape* %16, %class.btVector3* nonnull align 4 dereferenceable(16) %seperatingAxisInA)
  %m_minkowskiB29 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 5
  %17 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiB29, align 4
  call void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4 %qInB, %class.btConvexShape* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %seperatingAxisInB)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %pWorld, %class.btTransform* %localTransA, %class.btVector3* nonnull align 4 dereferenceable(16) %pInA)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %qWorld, %class.btTransform* %localTransB, %class.btVector3* nonnull align 4 dereferenceable(16) %qInB)
  %18 = load i8, i8* %check2d, align 1
  %tobool30 = trunc i8 %18 to i1
  br i1 %tobool30, label %if.then31, label %if.end35

if.then31:                                        ; preds = %for.cond
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pWorld)
  %arrayidx = getelementptr inbounds float, float* %call32, i32 2
  store float 0.000000e+00, float* %arrayidx, align 4
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %qWorld)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 2
  store float 0.000000e+00, float* %arrayidx34, align 4
  br label %if.end35

if.end35:                                         ; preds = %if.then31, %for.cond
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %w, %class.btVector3* nonnull align 4 dereferenceable(16) %pWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %qWorld)
  %m_cachedSeparatingAxis36 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %call37 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_cachedSeparatingAxis36, %class.btVector3* nonnull align 4 dereferenceable(16) %w)
  store float %call37, float* %delta, align 4
  %19 = load float, float* %delta, align 4
  %cmp = fcmp ogt float %19, 0.000000e+00
  br i1 %cmp, label %land.lhs.true, label %if.end42

land.lhs.true:                                    ; preds = %if.end35
  %20 = load float, float* %delta, align 4
  %21 = load float, float* %delta, align 4
  %mul = fmul float %20, %21
  %22 = load float, float* %squaredDistance, align 4
  %23 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %m_maximumDistanceSquared = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %23, i32 0, i32 2
  %24 = load float, float* %m_maximumDistanceSquared, align 4
  %mul38 = fmul float %22, %24
  %cmp39 = fcmp ogt float %mul, %mul38
  br i1 %cmp39, label %if.then40, label %if.end42

if.then40:                                        ; preds = %land.lhs.true
  %m_degenerateSimplex41 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 10, i32* %m_degenerateSimplex41, align 4
  store i8 1, i8* %checkSimplex, align 1
  br label %for.end

if.end42:                                         ; preds = %land.lhs.true, %if.end35
  %m_simplexSolver43 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %25 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver43, align 4
  %call44 = call zeroext i1 @_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3(%class.btVoronoiSimplexSolver* %25, %class.btVector3* nonnull align 4 dereferenceable(16) %w)
  br i1 %call44, label %if.then45, label %if.end47

if.then45:                                        ; preds = %if.end42
  %m_degenerateSimplex46 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 1, i32* %m_degenerateSimplex46, align 4
  store i8 1, i8* %checkSimplex, align 1
  br label %for.end

if.end47:                                         ; preds = %if.end42
  %26 = load float, float* %squaredDistance, align 4
  %27 = load float, float* %delta, align 4
  %sub = fsub float %26, %27
  store float %sub, float* %f0, align 4
  %28 = load float, float* %squaredDistance, align 4
  %mul48 = fmul float %28, 0x3EB0C6F7A0000000
  store float %mul48, float* %f1, align 4
  %29 = load float, float* %f0, align 4
  %30 = load float, float* %f1, align 4
  %cmp49 = fcmp ole float %29, %30
  br i1 %cmp49, label %if.then50, label %if.end56

if.then50:                                        ; preds = %if.end47
  %31 = load float, float* %f0, align 4
  %cmp51 = fcmp ole float %31, 0.000000e+00
  br i1 %cmp51, label %if.then52, label %if.else

if.then52:                                        ; preds = %if.then50
  %m_degenerateSimplex53 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 2, i32* %m_degenerateSimplex53, align 4
  br label %if.end55

if.else:                                          ; preds = %if.then50
  %m_degenerateSimplex54 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 11, i32* %m_degenerateSimplex54, align 4
  br label %if.end55

if.end55:                                         ; preds = %if.else, %if.then52
  store i8 1, i8* %checkSimplex, align 1
  br label %for.end

if.end56:                                         ; preds = %if.end47
  %m_simplexSolver57 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %32 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver57, align 4
  call void @_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_(%class.btVoronoiSimplexSolver* %32, %class.btVector3* nonnull align 4 dereferenceable(16) %w, %class.btVector3* nonnull align 4 dereferenceable(16) %pWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %qWorld)
  %call58 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %newCachedSeparatingAxis)
  %m_simplexSolver59 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %33 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver59, align 4
  %call60 = call zeroext i1 @_ZN22btVoronoiSimplexSolver7closestER9btVector3(%class.btVoronoiSimplexSolver* %33, %class.btVector3* nonnull align 4 dereferenceable(16) %newCachedSeparatingAxis)
  br i1 %call60, label %if.end63, label %if.then61

if.then61:                                        ; preds = %if.end56
  %m_degenerateSimplex62 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 3, i32* %m_degenerateSimplex62, align 4
  store i8 1, i8* %checkSimplex, align 1
  br label %for.end

if.end63:                                         ; preds = %if.end56
  %call64 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %newCachedSeparatingAxis)
  %cmp65 = fcmp olt float %call64, 0x3EB0C6F7A0000000
  br i1 %cmp65, label %if.then66, label %if.end69

if.then66:                                        ; preds = %if.end63
  %m_cachedSeparatingAxis67 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %34 = bitcast %class.btVector3* %m_cachedSeparatingAxis67 to i8*
  %35 = bitcast %class.btVector3* %newCachedSeparatingAxis to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %34, i8* align 4 %35, i32 16, i1 false)
  %m_degenerateSimplex68 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 6, i32* %m_degenerateSimplex68, align 4
  store i8 1, i8* %checkSimplex, align 1
  br label %for.end

if.end69:                                         ; preds = %if.end63
  %36 = load float, float* %squaredDistance, align 4
  store float %36, float* %previousSquaredDistance, align 4
  %call70 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %newCachedSeparatingAxis)
  store float %call70, float* %squaredDistance, align 4
  %37 = load float, float* %previousSquaredDistance, align 4
  %38 = load float, float* %squaredDistance, align 4
  %sub71 = fsub float %37, %38
  %39 = load float, float* %previousSquaredDistance, align 4
  %mul72 = fmul float 0x3E80000000000000, %39
  %cmp73 = fcmp ole float %sub71, %mul72
  br i1 %cmp73, label %if.then74, label %if.end76

if.then74:                                        ; preds = %if.end69
  store i8 1, i8* %checkSimplex, align 1
  %m_degenerateSimplex75 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 12, i32* %m_degenerateSimplex75, align 4
  br label %for.end

if.end76:                                         ; preds = %if.end69
  %m_cachedSeparatingAxis77 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %40 = bitcast %class.btVector3* %m_cachedSeparatingAxis77 to i8*
  %41 = bitcast %class.btVector3* %newCachedSeparatingAxis to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %40, i8* align 4 %41, i32 16, i1 false)
  %m_curIter78 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 13
  %42 = load i32, i32* %m_curIter78, align 4
  %inc79 = add nsw i32 %42, 1
  store i32 %inc79, i32* %m_curIter78, align 4
  %43 = load i32, i32* %gGjkMaxIter, align 4
  %cmp80 = icmp sgt i32 %42, %43
  br i1 %cmp80, label %if.then81, label %if.end82

if.then81:                                        ; preds = %if.end76
  br label %for.end

if.end82:                                         ; preds = %if.end76
  %m_simplexSolver83 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %44 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver83, align 4
  %call84 = call zeroext i1 @_ZNK22btVoronoiSimplexSolver11fullSimplexEv(%class.btVoronoiSimplexSolver* %44)
  %lnot = xor i1 %call84, true
  %frombool85 = zext i1 %lnot to i8
  store i8 %frombool85, i8* %check, align 1
  %45 = load i8, i8* %check, align 1
  %tobool86 = trunc i8 %45 to i1
  br i1 %tobool86, label %if.end89, label %if.then87

if.then87:                                        ; preds = %if.end82
  %m_degenerateSimplex88 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 13, i32* %m_degenerateSimplex88, align 4
  br label %for.end

if.end89:                                         ; preds = %if.end82
  br label %for.cond

for.end:                                          ; preds = %if.then87, %if.then81, %if.then74, %if.then66, %if.then61, %if.end55, %if.then45, %if.then40
  %46 = load i8, i8* %checkSimplex, align 1
  %tobool90 = trunc i8 %46 to i1
  br i1 %tobool90, label %if.then91, label %if.end121

if.then91:                                        ; preds = %for.end
  %m_simplexSolver92 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %47 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver92, align 4
  call void @_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_(%class.btVoronoiSimplexSolver* %47, %class.btVector3* nonnull align 4 dereferenceable(16) %pointOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %pointOnB)
  %m_cachedSeparatingAxis93 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %48 = bitcast %class.btVector3* %normalInB to i8*
  %49 = bitcast %class.btVector3* %m_cachedSeparatingAxis93 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %48, i8* align 4 %49, i32 16, i1 false)
  %m_cachedSeparatingAxis94 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %call95 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_cachedSeparatingAxis94)
  store float %call95, float* %lenSqr, align 4
  %50 = load float, float* %lenSqr, align 4
  %cmp96 = fcmp olt float %50, 0x3EB0C6F7A0000000
  br i1 %cmp96, label %if.then97, label %if.end99

if.then97:                                        ; preds = %if.then91
  %m_degenerateSimplex98 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  store i32 5, i32* %m_degenerateSimplex98, align 4
  br label %if.end99

if.end99:                                         ; preds = %if.then97, %if.then91
  %51 = load float, float* %lenSqr, align 4
  %cmp100 = fcmp ogt float %51, 0x3D10000000000000
  br i1 %cmp100, label %if.then101, label %if.else118

if.then101:                                       ; preds = %if.end99
  %52 = load float, float* %lenSqr, align 4
  %call102 = call float @_Z6btSqrtf(float %52)
  %div = fdiv float 1.000000e+00, %call102
  store float %div, float* %rlen, align 4
  %call103 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %normalInB, float* nonnull align 4 dereferenceable(4) %rlen)
  %53 = load float, float* %squaredDistance, align 4
  %call104 = call float @_Z6btSqrtf(float %53)
  store float %call104, float* %s, align 4
  %m_cachedSeparatingAxis106 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %54 = load float, float* %marginA, align 4
  %55 = load float, float* %s, align 4
  %div108 = fdiv float %54, %55
  store float %div108, float* %ref.tmp107, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp105, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedSeparatingAxis106, float* nonnull align 4 dereferenceable(4) %ref.tmp107)
  %call109 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %pointOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp105)
  %m_cachedSeparatingAxis111 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %56 = load float, float* %marginB, align 4
  %57 = load float, float* %s, align 4
  %div113 = fdiv float %56, %57
  store float %div113, float* %ref.tmp112, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp110, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedSeparatingAxis111, float* nonnull align 4 dereferenceable(4) %ref.tmp112)
  %call114 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %pointOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp110)
  %58 = load float, float* %rlen, align 4
  %div115 = fdiv float 1.000000e+00, %58
  %59 = load float, float* %margin, align 4
  %sub116 = fsub float %div115, %59
  store float %sub116, float* %distance, align 4
  store i8 1, i8* %isValid, align 1
  %m_lastUsedMethod117 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 1, i32* %m_lastUsedMethod117, align 4
  br label %if.end120

if.else118:                                       ; preds = %if.end99
  %m_lastUsedMethod119 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 2, i32* %m_lastUsedMethod119, align 4
  br label %if.end120

if.end120:                                        ; preds = %if.else118, %if.then101
  br label %if.end121

if.end121:                                        ; preds = %if.end120, %for.end
  %m_catchDegeneracies = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 15
  %60 = load i32, i32* %m_catchDegeneracies, align 4
  %tobool122 = icmp ne i32 %60, 0
  br i1 %tobool122, label %land.lhs.true123, label %land.end131

land.lhs.true123:                                 ; preds = %if.end121
  %m_penetrationDepthSolver = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 2
  %61 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_penetrationDepthSolver, align 4
  %tobool124 = icmp ne %class.btConvexPenetrationDepthSolver* %61, null
  br i1 %tobool124, label %land.lhs.true125, label %land.end131

land.lhs.true125:                                 ; preds = %land.lhs.true123
  %m_degenerateSimplex126 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 14
  %62 = load i32, i32* %m_degenerateSimplex126, align 4
  %tobool127 = icmp ne i32 %62, 0
  br i1 %tobool127, label %land.rhs128, label %land.end131

land.rhs128:                                      ; preds = %land.lhs.true125
  %63 = load float, float* %distance, align 4
  %64 = load float, float* %margin, align 4
  %add129 = fadd float %63, %64
  %65 = load float, float* @gGjkEpaPenetrationTolerance, align 4
  %cmp130 = fcmp olt float %add129, %65
  br label %land.end131

land.end131:                                      ; preds = %land.rhs128, %land.lhs.true125, %land.lhs.true123, %if.end121
  %66 = phi i1 [ false, %land.lhs.true125 ], [ false, %land.lhs.true123 ], [ false, %if.end121 ], [ %cmp130, %land.rhs128 ]
  %frombool132 = zext i1 %66 to i8
  store i8 %frombool132, i8* %catchDegeneratePenetrationCase, align 1
  %67 = load i8, i8* %checkPenetration, align 1
  %tobool133 = trunc i8 %67 to i1
  br i1 %tobool133, label %land.lhs.true134, label %if.end208

land.lhs.true134:                                 ; preds = %land.end131
  %68 = load i8, i8* %isValid, align 1
  %tobool135 = trunc i8 %68 to i1
  br i1 %tobool135, label %lor.lhs.false, label %if.then137

lor.lhs.false:                                    ; preds = %land.lhs.true134
  %69 = load i8, i8* %catchDegeneratePenetrationCase, align 1
  %tobool136 = trunc i8 %69 to i1
  br i1 %tobool136, label %if.then137, label %if.end208

if.then137:                                       ; preds = %lor.lhs.false, %land.lhs.true134
  %m_penetrationDepthSolver138 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 2
  %70 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_penetrationDepthSolver138, align 4
  %tobool139 = icmp ne %class.btConvexPenetrationDepthSolver* %70, null
  br i1 %tobool139, label %if.then140, label %if.end207

if.then140:                                       ; preds = %if.then137
  %call141 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpPointOnA)
  %call142 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmpPointOnB)
  %71 = load i32, i32* @gNumDeepPenetrationChecks, align 4
  %inc143 = add nsw i32 %71, 1
  store i32 %inc143, i32* @gNumDeepPenetrationChecks, align 4
  %m_cachedSeparatingAxis144 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_cachedSeparatingAxis144)
  %m_penetrationDepthSolver145 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 2
  %72 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_penetrationDepthSolver145, align 4
  %m_simplexSolver146 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 3
  %73 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver146, align 4
  %m_minkowskiA147 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 4
  %74 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiA147, align 4
  %m_minkowskiB148 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 5
  %75 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiB148, align 4
  %m_cachedSeparatingAxis149 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %76 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDraw.addr, align 4
  %77 = bitcast %class.btConvexPenetrationDepthSolver* %72 to i1 (%class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)***
  %vtable = load i1 (%class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)**, i1 (%class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)*** %77, align 4
  %vfn = getelementptr inbounds i1 (%class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)*, i1 (%class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)** %vtable, i64 2
  %78 = load i1 (%class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)*, i1 (%class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, %class.btTransform*, %class.btTransform*, %class.btVector3*, %class.btVector3*, %class.btVector3*, %class.btIDebugDraw*)** %vfn, align 4
  %call150 = call zeroext i1 %78(%class.btConvexPenetrationDepthSolver* %72, %class.btVoronoiSimplexSolver* nonnull align 4 dereferenceable(357) %73, %class.btConvexShape* %74, %class.btConvexShape* %75, %class.btTransform* nonnull align 4 dereferenceable(64) %localTransA, %class.btTransform* nonnull align 4 dereferenceable(64) %localTransB, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedSeparatingAxis149, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnB, %class.btIDebugDraw* %76)
  %frombool151 = zext i1 %call150 to i8
  store i8 %frombool151, i8* %isValid2, align 1
  %79 = load i8, i8* %isValid2, align 1
  %tobool152 = trunc i8 %79 to i1
  br i1 %tobool152, label %if.then153, label %if.else180

if.then153:                                       ; preds = %if.then140
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %tmpNormalInB, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnA)
  %call155 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %tmpNormalInB)
  store float %call155, float* %lenSqr154, align 4
  %80 = load float, float* %lenSqr154, align 4
  %cmp156 = fcmp ole float %80, 0x3D10000000000000
  br i1 %cmp156, label %if.then157, label %if.end161

if.then157:                                       ; preds = %if.then153
  %m_cachedSeparatingAxis158 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %81 = bitcast %class.btVector3* %tmpNormalInB to i8*
  %82 = bitcast %class.btVector3* %m_cachedSeparatingAxis158 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %81, i8* align 4 %82, i32 16, i1 false)
  %m_cachedSeparatingAxis159 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %call160 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_cachedSeparatingAxis159)
  store float %call160, float* %lenSqr154, align 4
  br label %if.end161

if.end161:                                        ; preds = %if.then157, %if.then153
  %83 = load float, float* %lenSqr154, align 4
  %cmp162 = fcmp ogt float %83, 0x3D10000000000000
  br i1 %cmp162, label %if.then163, label %if.else177

if.then163:                                       ; preds = %if.end161
  %84 = load float, float* %lenSqr154, align 4
  %call165 = call float @_Z6btSqrtf(float %84)
  store float %call165, float* %ref.tmp164, align 4
  %call166 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %tmpNormalInB, float* nonnull align 4 dereferenceable(4) %ref.tmp164)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp167, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnB)
  %call168 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp167)
  %fneg = fneg float %call168
  store float %fneg, float* %distance2, align 4
  %m_lastUsedMethod169 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 3, i32* %m_lastUsedMethod169, align 4
  %85 = load i8, i8* %isValid, align 1
  %tobool170 = trunc i8 %85 to i1
  br i1 %tobool170, label %lor.lhs.false171, label %if.then173

lor.lhs.false171:                                 ; preds = %if.then163
  %86 = load float, float* %distance2, align 4
  %87 = load float, float* %distance, align 4
  %cmp172 = fcmp olt float %86, %87
  br i1 %cmp172, label %if.then173, label %if.else174

if.then173:                                       ; preds = %lor.lhs.false171, %if.then163
  %88 = load float, float* %distance2, align 4
  store float %88, float* %distance, align 4
  %89 = bitcast %class.btVector3* %pointOnA to i8*
  %90 = bitcast %class.btVector3* %tmpPointOnA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %89, i8* align 4 %90, i32 16, i1 false)
  %91 = bitcast %class.btVector3* %pointOnB to i8*
  %92 = bitcast %class.btVector3* %tmpPointOnB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %91, i8* align 4 %92, i32 16, i1 false)
  %93 = bitcast %class.btVector3* %normalInB to i8*
  %94 = bitcast %class.btVector3* %tmpNormalInB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %93, i8* align 4 %94, i32 16, i1 false)
  store i8 1, i8* %isValid, align 1
  br label %if.end176

if.else174:                                       ; preds = %lor.lhs.false171
  %m_lastUsedMethod175 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 8, i32* %m_lastUsedMethod175, align 4
  br label %if.end176

if.end176:                                        ; preds = %if.else174, %if.then173
  br label %if.end179

if.else177:                                       ; preds = %if.end161
  %m_lastUsedMethod178 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 9, i32* %m_lastUsedMethod178, align 4
  br label %if.end179

if.end179:                                        ; preds = %if.else177, %if.end176
  br label %if.end206

if.else180:                                       ; preds = %if.then140
  %m_cachedSeparatingAxis181 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %call182 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %m_cachedSeparatingAxis181)
  %cmp183 = fcmp ogt float %call182, 0.000000e+00
  br i1 %cmp183, label %if.then184, label %if.end205

if.then184:                                       ; preds = %if.else180
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp186, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %tmpPointOnB)
  %call187 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp186)
  %95 = load float, float* %margin, align 4
  %sub188 = fsub float %call187, %95
  store float %sub188, float* %distance2185, align 4
  %96 = load i8, i8* %isValid, align 1
  %tobool189 = trunc i8 %96 to i1
  br i1 %tobool189, label %lor.lhs.false190, label %if.then192

lor.lhs.false190:                                 ; preds = %if.then184
  %97 = load float, float* %distance2185, align 4
  %98 = load float, float* %distance, align 4
  %cmp191 = fcmp olt float %97, %98
  br i1 %cmp191, label %if.then192, label %if.else202

if.then192:                                       ; preds = %lor.lhs.false190, %if.then184
  %99 = load float, float* %distance2185, align 4
  store float %99, float* %distance, align 4
  %100 = bitcast %class.btVector3* %pointOnA to i8*
  %101 = bitcast %class.btVector3* %tmpPointOnA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %100, i8* align 4 %101, i32 16, i1 false)
  %102 = bitcast %class.btVector3* %pointOnB to i8*
  %103 = bitcast %class.btVector3* %tmpPointOnB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %102, i8* align 4 %103, i32 16, i1 false)
  %m_cachedSeparatingAxis194 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp193, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedSeparatingAxis194, float* nonnull align 4 dereferenceable(4) %marginA)
  %call195 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %pointOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp193)
  %m_cachedSeparatingAxis197 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp196, %class.btVector3* nonnull align 4 dereferenceable(16) %m_cachedSeparatingAxis197, float* nonnull align 4 dereferenceable(4) %marginB)
  %call198 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %pointOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp196)
  %m_cachedSeparatingAxis199 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %104 = bitcast %class.btVector3* %normalInB to i8*
  %105 = bitcast %class.btVector3* %m_cachedSeparatingAxis199 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %104, i8* align 4 %105, i32 16, i1 false)
  %call200 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %normalInB)
  store i8 1, i8* %isValid, align 1
  %m_lastUsedMethod201 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 6, i32* %m_lastUsedMethod201, align 4
  br label %if.end204

if.else202:                                       ; preds = %lor.lhs.false190
  %m_lastUsedMethod203 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 5, i32* %m_lastUsedMethod203, align 4
  br label %if.end204

if.end204:                                        ; preds = %if.else202, %if.then192
  br label %if.end205

if.end205:                                        ; preds = %if.end204, %if.else180
  br label %if.end206

if.end206:                                        ; preds = %if.end205, %if.end179
  br label %if.end207

if.end207:                                        ; preds = %if.end206, %if.then137
  br label %if.end208

if.end208:                                        ; preds = %if.end207, %lor.lhs.false, %land.end131
  %106 = load i8, i8* %isValid, align 1
  %tobool209 = trunc i8 %106 to i1
  br i1 %tobool209, label %land.lhs.true210, label %if.end259

land.lhs.true210:                                 ; preds = %if.end208
  %107 = load float, float* %distance, align 4
  %cmp211 = fcmp olt float %107, 0.000000e+00
  br i1 %cmp211, label %if.then216, label %lor.lhs.false212

lor.lhs.false212:                                 ; preds = %land.lhs.true210
  %108 = load float, float* %distance, align 4
  %109 = load float, float* %distance, align 4
  %mul213 = fmul float %108, %109
  %110 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %m_maximumDistanceSquared214 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %110, i32 0, i32 2
  %111 = load float, float* %m_maximumDistanceSquared214, align 4
  %cmp215 = fcmp olt float %mul213, %111
  br i1 %cmp215, label %if.then216, label %if.end259

if.then216:                                       ; preds = %lor.lhs.false212, %land.lhs.true210
  %m_cachedSeparatingAxis217 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 1
  %112 = bitcast %class.btVector3* %m_cachedSeparatingAxis217 to i8*
  %113 = bitcast %class.btVector3* %normalInB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %112, i8* align 4 %113, i32 16, i1 false)
  %114 = load float, float* %distance, align 4
  %m_cachedSeparatingDistance218 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 11
  store float %114, float* %m_cachedSeparatingDistance218, align 4
  store float 0.000000e+00, float* %d1, align 4
  %115 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %m_transformA220 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %115, i32 0, i32 0
  %call221 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %m_transformA220)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %seperatingAxisInA219, %class.btVector3* nonnull align 4 dereferenceable(16) %normalInB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call221)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp223, %class.btVector3* nonnull align 4 dereferenceable(16) %normalInB)
  %116 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %m_transformB224 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %116, i32 0, i32 1
  %call225 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %m_transformB224)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %seperatingAxisInB222, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp223, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call225)
  %m_minkowskiA227 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 4
  %117 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiA227, align 4
  call void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4 %pInA226, %class.btConvexShape* %117, %class.btVector3* nonnull align 4 dereferenceable(16) %seperatingAxisInA219)
  %m_minkowskiB229 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 5
  %118 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiB229, align 4
  call void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4 %qInB228, %class.btConvexShape* %118, %class.btVector3* nonnull align 4 dereferenceable(16) %seperatingAxisInB222)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %pWorld230, %class.btTransform* %localTransA, %class.btVector3* nonnull align 4 dereferenceable(16) %pInA226)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %qWorld231, %class.btTransform* %localTransB, %class.btVector3* nonnull align 4 dereferenceable(16) %qInB228)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %w232, %class.btVector3* nonnull align 4 dereferenceable(16) %pWorld230, %class.btVector3* nonnull align 4 dereferenceable(16) %qWorld231)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp233, %class.btVector3* nonnull align 4 dereferenceable(16) %normalInB)
  %call234 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp233, %class.btVector3* nonnull align 4 dereferenceable(16) %w232)
  store float %call234, float* %d1, align 4
  store float 0.000000e+00, float* %d0, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp236, %class.btVector3* nonnull align 4 dereferenceable(16) %normalInB)
  %119 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %m_transformA237 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %119, i32 0, i32 0
  %call238 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %m_transformA237)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %seperatingAxisInA235, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp236, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call238)
  %120 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %m_transformB240 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %120, i32 0, i32 1
  %call241 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %m_transformB240)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %seperatingAxisInB239, %class.btVector3* nonnull align 4 dereferenceable(16) %normalInB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call241)
  %m_minkowskiA243 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 4
  %121 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiA243, align 4
  call void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4 %pInA242, %class.btConvexShape* %121, %class.btVector3* nonnull align 4 dereferenceable(16) %seperatingAxisInA235)
  %m_minkowskiB245 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 5
  %122 = load %class.btConvexShape*, %class.btConvexShape** %m_minkowskiB245, align 4
  call void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4 %qInB244, %class.btConvexShape* %122, %class.btVector3* nonnull align 4 dereferenceable(16) %seperatingAxisInB239)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %pWorld246, %class.btTransform* %localTransA, %class.btVector3* nonnull align 4 dereferenceable(16) %pInA242)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %qWorld247, %class.btTransform* %localTransB, %class.btVector3* nonnull align 4 dereferenceable(16) %qInB244)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %w248, %class.btVector3* nonnull align 4 dereferenceable(16) %pWorld246, %class.btVector3* nonnull align 4 dereferenceable(16) %qWorld247)
  %call249 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %normalInB, %class.btVector3* nonnull align 4 dereferenceable(16) %w248)
  store float %call249, float* %d0, align 4
  %123 = load float, float* %d1, align 4
  %124 = load float, float* %d0, align 4
  %cmp250 = fcmp ogt float %123, %124
  br i1 %cmp250, label %if.then251, label %if.end255

if.then251:                                       ; preds = %if.then216
  %m_lastUsedMethod252 = getelementptr inbounds %class.btGjkPairDetector, %class.btGjkPairDetector* %this1, i32 0, i32 12
  store i32 10, i32* %m_lastUsedMethod252, align 4
  store float -1.000000e+00, float* %ref.tmp253, align 4
  %call254 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %normalInB, float* nonnull align 4 dereferenceable(4) %ref.tmp253)
  br label %if.end255

if.end255:                                        ; preds = %if.then251, %if.then216
  %125 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp256, %class.btVector3* nonnull align 4 dereferenceable(16) %pointOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %positionOffset)
  %126 = load float, float* %distance, align 4
  %127 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %125 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable257 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %127, align 4
  %vfn258 = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable257, i64 4
  %128 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn258, align 4
  call void %128(%"struct.btDiscreteCollisionDetectorInterface::Result"* %125, %class.btVector3* nonnull align 4 dereferenceable(16) %normalInB, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp256, float %126)
  br label %if.end259

if.end259:                                        ; preds = %if.end255, %lor.lhs.false212, %if.end208
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionShape10isConvex2dEv(%class.btCollisionShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %call = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this1)
  %call2 = call zeroext i1 @_ZN17btBroadphaseProxy10isConvex2dEi(i32 %call)
  ret i1 %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

declare void @_ZN22btVoronoiSimplexSolver5resetEv(%class.btVoronoiSimplexSolver*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call2, float* %ref.tmp1, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp3, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

declare void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4, %class.btConvexShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

declare zeroext i1 @_ZN22btVoronoiSimplexSolver9inSimplexERK9btVector3(%class.btVoronoiSimplexSolver*, %class.btVector3* nonnull align 4 dereferenceable(16)) #3

declare void @_ZN22btVoronoiSimplexSolver9addVertexERK9btVector3S2_S2_(%class.btVoronoiSimplexSolver*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #3

declare zeroext i1 @_ZN22btVoronoiSimplexSolver7closestER9btVector3(%class.btVoronoiSimplexSolver*, %class.btVector3* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK22btVoronoiSimplexSolver11fullSimplexEv(%class.btVoronoiSimplexSolver* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  store %class.btVoronoiSimplexSolver* %this, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %this1 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %this.addr, align 4
  %m_numVertices = getelementptr inbounds %class.btVoronoiSimplexSolver, %class.btVoronoiSimplexSolver* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_numVertices, align 4
  %cmp = icmp eq i32 %0, 4
  ret i1 %cmp
}

declare void @_ZN22btVoronoiSimplexSolver14compute_pointsER9btVector3S1_(%class.btVoronoiSimplexSolver*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector37setZeroEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGjkPairDetector* @_ZN17btGjkPairDetectorD2Ev(%class.btGjkPairDetector* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = bitcast %class.btGjkPairDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #8
  ret %class.btGjkPairDetector* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btGjkPairDetectorD0Ev(%class.btGjkPairDetector* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %call = call %class.btGjkPairDetector* @_ZN17btGjkPairDetectorD2Ev(%class.btGjkPairDetector* %this1) #8
  %0 = bitcast %class.btGjkPairDetector* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN36btDiscreteCollisionDetectorInterfaceD0Ev(%struct.btDiscreteCollisionDetectorInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #5

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy10isConvex2dEi(i32 %proxyType) #1 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4
  %0 = load i32, i32* %proxyType.addr, align 4
  %cmp = icmp eq i32 %0, 17
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %entry
  %1 = load i32, i32* %proxyType.addr, align 4
  %cmp1 = icmp eq i32 %1, 18
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %entry
  %2 = phi i1 [ true, %entry ], [ %cmp1, %lor.rhs ]
  ret i1 %2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btGjkPairDetector.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { cold noreturn nounwind }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
