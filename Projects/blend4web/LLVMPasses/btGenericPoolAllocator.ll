; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/Gimpact/btGenericPoolAllocator.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/Gimpact/btGenericPoolAllocator.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.GIM_STANDARD_ALLOCATOR = type { %class.btGenericPoolAllocator }
%class.btGenericPoolAllocator = type { i32 (...)**, i32, i32, [16 x %class.btGenericMemoryPool*], i32 }
%class.btGenericMemoryPool = type { i8*, i32*, i32*, i32, i32, i32, i32 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN19btGenericMemoryPool16get_element_dataEm = comdat any

$_ZN19btGenericMemoryPool17get_pool_capacityEv = comdat any

$_ZN22btGenericPoolAllocator17get_pool_capacityEv = comdat any

$_ZN22GIM_STANDARD_ALLOCATORC2Ev = comdat any

$_ZN22GIM_STANDARD_ALLOCATORD2Ev = comdat any

$_ZN22btGenericPoolAllocatorC2Emm = comdat any

$_ZN22GIM_STANDARD_ALLOCATORD0Ev = comdat any

$_ZTV22GIM_STANDARD_ALLOCATOR = comdat any

$_ZTS22GIM_STANDARD_ALLOCATOR = comdat any

$_ZTI22GIM_STANDARD_ALLOCATOR = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV22btGenericPoolAllocator = hidden unnamed_addr constant { [4 x i8*] } { [4 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI22btGenericPoolAllocator to i8*), i8* bitcast (%class.btGenericPoolAllocator* (%class.btGenericPoolAllocator*)* @_ZN22btGenericPoolAllocatorD1Ev to i8*), i8* bitcast (void (%class.btGenericPoolAllocator*)* @_ZN22btGenericPoolAllocatorD0Ev to i8*)] }, align 4
@g_main_allocator = hidden global %class.GIM_STANDARD_ALLOCATOR zeroinitializer, align 4
@__dso_handle = external hidden global i8
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS22btGenericPoolAllocator = hidden constant [25 x i8] c"22btGenericPoolAllocator\00", align 1
@_ZTI22btGenericPoolAllocator = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btGenericPoolAllocator, i32 0, i32 0) }, align 4
@_ZTV22GIM_STANDARD_ALLOCATOR = linkonce_odr hidden unnamed_addr constant { [4 x i8*] } { [4 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22GIM_STANDARD_ALLOCATOR to i8*), i8* bitcast (%class.GIM_STANDARD_ALLOCATOR* (%class.GIM_STANDARD_ALLOCATOR*)* @_ZN22GIM_STANDARD_ALLOCATORD2Ev to i8*), i8* bitcast (void (%class.GIM_STANDARD_ALLOCATOR*)* @_ZN22GIM_STANDARD_ALLOCATORD0Ev to i8*)] }, comdat, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS22GIM_STANDARD_ALLOCATOR = linkonce_odr hidden constant [25 x i8] c"22GIM_STANDARD_ALLOCATOR\00", comdat, align 1
@_ZTI22GIM_STANDARD_ALLOCATOR = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22GIM_STANDARD_ALLOCATOR, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI22btGenericPoolAllocator to i8*) }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btGenericPoolAllocator.cpp, i8* null }]

@_ZN22btGenericPoolAllocatorD1Ev = hidden unnamed_addr alias %class.btGenericPoolAllocator* (%class.btGenericPoolAllocator*), %class.btGenericPoolAllocator* (%class.btGenericPoolAllocator*)* @_ZN22btGenericPoolAllocatorD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZN19btGenericMemoryPool24allocate_from_free_nodesEm(%class.btGenericMemoryPool* %this, i32 %num_elements) #1 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btGenericMemoryPool*, align 4
  %num_elements.addr = alloca i32, align 4
  %ptr = alloca i32, align 4
  %revindex = alloca i32, align 4
  %finalsize = alloca i32, align 4
  store %class.btGenericMemoryPool* %this, %class.btGenericMemoryPool** %this.addr, align 4
  store i32 %num_elements, i32* %num_elements.addr, align 4
  %this1 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %this.addr, align 4
  store i32 -1, i32* %ptr, align 4
  %m_free_nodes_count = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_free_nodes_count, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %m_free_nodes_count2 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 4
  %1 = load i32, i32* %m_free_nodes_count2, align 4
  store i32 %1, i32* %revindex, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end7, %if.end
  %2 = load i32, i32* %revindex, align 4
  %dec = add i32 %2, -1
  store i32 %dec, i32* %revindex, align 4
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %3 = load i32, i32* %ptr, align 4
  %cmp3 = icmp eq i32 %3, -1
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %4 = phi i1 [ false, %while.cond ], [ %cmp3, %land.rhs ]
  br i1 %4, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_allocated_sizes = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 2
  %5 = load i32*, i32** %m_allocated_sizes, align 4
  %m_free_nodes = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 1
  %6 = load i32*, i32** %m_free_nodes, align 4
  %7 = load i32, i32* %revindex, align 4
  %arrayidx = getelementptr inbounds i32, i32* %6, i32 %7
  %8 = load i32, i32* %arrayidx, align 4
  %arrayidx4 = getelementptr inbounds i32, i32* %5, i32 %8
  %9 = load i32, i32* %arrayidx4, align 4
  %10 = load i32, i32* %num_elements.addr, align 4
  %cmp5 = icmp uge i32 %9, %10
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %while.body
  %11 = load i32, i32* %revindex, align 4
  store i32 %11, i32* %ptr, align 4
  br label %if.end7

if.end7:                                          ; preds = %if.then6, %while.body
  br label %while.cond

while.end:                                        ; preds = %land.end
  %12 = load i32, i32* %ptr, align 4
  %cmp8 = icmp eq i32 %12, -1
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %while.end
  store i32 -1, i32* %retval, align 4
  br label %return

if.end10:                                         ; preds = %while.end
  %13 = load i32, i32* %ptr, align 4
  store i32 %13, i32* %revindex, align 4
  %m_free_nodes11 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 1
  %14 = load i32*, i32** %m_free_nodes11, align 4
  %15 = load i32, i32* %revindex, align 4
  %arrayidx12 = getelementptr inbounds i32, i32* %14, i32 %15
  %16 = load i32, i32* %arrayidx12, align 4
  store i32 %16, i32* %ptr, align 4
  %m_allocated_sizes13 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 2
  %17 = load i32*, i32** %m_allocated_sizes13, align 4
  %18 = load i32, i32* %ptr, align 4
  %arrayidx14 = getelementptr inbounds i32, i32* %17, i32 %18
  %19 = load i32, i32* %arrayidx14, align 4
  store i32 %19, i32* %finalsize, align 4
  %20 = load i32, i32* %num_elements.addr, align 4
  %21 = load i32, i32* %finalsize, align 4
  %sub = sub i32 %21, %20
  store i32 %sub, i32* %finalsize, align 4
  %22 = load i32, i32* %num_elements.addr, align 4
  %m_allocated_sizes15 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 2
  %23 = load i32*, i32** %m_allocated_sizes15, align 4
  %24 = load i32, i32* %ptr, align 4
  %arrayidx16 = getelementptr inbounds i32, i32* %23, i32 %24
  store i32 %22, i32* %arrayidx16, align 4
  %25 = load i32, i32* %finalsize, align 4
  %cmp17 = icmp ugt i32 %25, 0
  br i1 %cmp17, label %if.then18, label %if.else

if.then18:                                        ; preds = %if.end10
  %26 = load i32, i32* %ptr, align 4
  %27 = load i32, i32* %num_elements.addr, align 4
  %add = add i32 %26, %27
  %m_free_nodes19 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 1
  %28 = load i32*, i32** %m_free_nodes19, align 4
  %29 = load i32, i32* %revindex, align 4
  %arrayidx20 = getelementptr inbounds i32, i32* %28, i32 %29
  store i32 %add, i32* %arrayidx20, align 4
  %30 = load i32, i32* %finalsize, align 4
  %m_allocated_sizes21 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 2
  %31 = load i32*, i32** %m_allocated_sizes21, align 4
  %32 = load i32, i32* %ptr, align 4
  %33 = load i32, i32* %num_elements.addr, align 4
  %add22 = add i32 %32, %33
  %arrayidx23 = getelementptr inbounds i32, i32* %31, i32 %add22
  store i32 %30, i32* %arrayidx23, align 4
  br label %if.end32

if.else:                                          ; preds = %if.end10
  %m_free_nodes24 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 1
  %34 = load i32*, i32** %m_free_nodes24, align 4
  %m_free_nodes_count25 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 4
  %35 = load i32, i32* %m_free_nodes_count25, align 4
  %sub26 = sub i32 %35, 1
  %arrayidx27 = getelementptr inbounds i32, i32* %34, i32 %sub26
  %36 = load i32, i32* %arrayidx27, align 4
  %m_free_nodes28 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 1
  %37 = load i32*, i32** %m_free_nodes28, align 4
  %38 = load i32, i32* %revindex, align 4
  %arrayidx29 = getelementptr inbounds i32, i32* %37, i32 %38
  store i32 %36, i32* %arrayidx29, align 4
  %m_free_nodes_count30 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 4
  %39 = load i32, i32* %m_free_nodes_count30, align 4
  %dec31 = add i32 %39, -1
  store i32 %dec31, i32* %m_free_nodes_count30, align 4
  br label %if.end32

if.end32:                                         ; preds = %if.else, %if.then18
  %40 = load i32, i32* %ptr, align 4
  store i32 %40, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end32, %if.then9, %if.then
  %41 = load i32, i32* %retval, align 4
  ret i32 %41
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZN19btGenericMemoryPool18allocate_from_poolEm(%class.btGenericMemoryPool* %this, i32 %num_elements) #1 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btGenericMemoryPool*, align 4
  %num_elements.addr = alloca i32, align 4
  %ptr = alloca i32, align 4
  store %class.btGenericMemoryPool* %this, %class.btGenericMemoryPool** %this.addr, align 4
  store i32 %num_elements, i32* %num_elements.addr, align 4
  %this1 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %this.addr, align 4
  %m_allocated_count = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_allocated_count, align 4
  %1 = load i32, i32* %num_elements.addr, align 4
  %add = add i32 %0, %1
  %m_max_element_count = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 6
  %2 = load i32, i32* %m_max_element_count, align 4
  %cmp = icmp ugt i32 %add, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %m_allocated_count2 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 3
  %3 = load i32, i32* %m_allocated_count2, align 4
  store i32 %3, i32* %ptr, align 4
  %4 = load i32, i32* %num_elements.addr, align 4
  %m_allocated_sizes = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 2
  %5 = load i32*, i32** %m_allocated_sizes, align 4
  %m_allocated_count3 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 3
  %6 = load i32, i32* %m_allocated_count3, align 4
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  store i32 %4, i32* %arrayidx, align 4
  %7 = load i32, i32* %num_elements.addr, align 4
  %m_allocated_count4 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 3
  %8 = load i32, i32* %m_allocated_count4, align 4
  %add5 = add i32 %8, %7
  store i32 %add5, i32* %m_allocated_count4, align 4
  %9 = load i32, i32* %ptr, align 4
  store i32 %9, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: noinline optnone
define hidden void @_ZN19btGenericMemoryPool9init_poolEmm(%class.btGenericMemoryPool* %this, i32 %element_size, i32 %element_count) #2 {
entry:
  %this.addr = alloca %class.btGenericMemoryPool*, align 4
  %element_size.addr = alloca i32, align 4
  %element_count.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btGenericMemoryPool* %this, %class.btGenericMemoryPool** %this.addr, align 4
  store i32 %element_size, i32* %element_size.addr, align 4
  store i32 %element_count, i32* %element_count.addr, align 4
  %this1 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %this.addr, align 4
  %m_allocated_count = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 3
  store i32 0, i32* %m_allocated_count, align 4
  %m_free_nodes_count = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 4
  store i32 0, i32* %m_free_nodes_count, align 4
  %0 = load i32, i32* %element_size.addr, align 4
  %m_element_size = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 5
  store i32 %0, i32* %m_element_size, align 4
  %1 = load i32, i32* %element_count.addr, align 4
  %m_max_element_count = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 6
  store i32 %1, i32* %m_max_element_count, align 4
  %m_element_size2 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 5
  %2 = load i32, i32* %m_element_size2, align 4
  %m_max_element_count3 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 6
  %3 = load i32, i32* %m_max_element_count3, align 4
  %mul = mul i32 %2, %3
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %m_pool = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 0
  store i8* %call, i8** %m_pool, align 4
  %m_max_element_count4 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 6
  %4 = load i32, i32* %m_max_element_count4, align 4
  %mul5 = mul i32 4, %4
  %call6 = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul5, i32 16)
  %5 = bitcast i8* %call6 to i32*
  %m_free_nodes = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 1
  store i32* %5, i32** %m_free_nodes, align 4
  %m_max_element_count7 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 6
  %6 = load i32, i32* %m_max_element_count7, align 4
  %mul8 = mul i32 4, %6
  %call9 = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul8, i32 16)
  %7 = bitcast i8* %call9 to i32*
  %m_allocated_sizes = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 2
  store i32* %7, i32** %m_allocated_sizes, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %i, align 4
  %m_max_element_count10 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 6
  %9 = load i32, i32* %m_max_element_count10, align 4
  %cmp = icmp ult i32 %8, %9
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_allocated_sizes11 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 2
  %10 = load i32*, i32** %m_allocated_sizes11, align 4
  %11 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %10, i32 %11
  store i32 0, i32* %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %12 = load i32, i32* %i, align 4
  %inc = add i32 %12, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline optnone
define hidden void @_ZN19btGenericMemoryPool8end_poolEv(%class.btGenericMemoryPool* %this) #2 {
entry:
  %this.addr = alloca %class.btGenericMemoryPool*, align 4
  store %class.btGenericMemoryPool* %this, %class.btGenericMemoryPool** %this.addr, align 4
  %this1 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %this.addr, align 4
  %m_pool = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 0
  %0 = load i8*, i8** %m_pool, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  %m_free_nodes = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 1
  %1 = load i32*, i32** %m_free_nodes, align 4
  %2 = bitcast i32* %1 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %2)
  %m_allocated_sizes = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 2
  %3 = load i32*, i32** %m_allocated_sizes, align 4
  %4 = bitcast i32* %3 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %4)
  %m_allocated_count = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 3
  store i32 0, i32* %m_allocated_count, align 4
  %m_free_nodes_count = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 4
  store i32 0, i32* %m_free_nodes_count, align 4
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define hidden i8* @_ZN19btGenericMemoryPool8allocateEm(%class.btGenericMemoryPool* %this, i32 %size_bytes) #2 {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btGenericMemoryPool*, align 4
  %size_bytes.addr = alloca i32, align 4
  %module = alloca i32, align 4
  %element_count = alloca i32, align 4
  %alloc_pos = alloca i32, align 4
  store %class.btGenericMemoryPool* %this, %class.btGenericMemoryPool** %this.addr, align 4
  store i32 %size_bytes, i32* %size_bytes.addr, align 4
  %this1 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %this.addr, align 4
  %0 = load i32, i32* %size_bytes.addr, align 4
  %m_element_size = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 5
  %1 = load i32, i32* %m_element_size, align 4
  %rem = urem i32 %0, %1
  store i32 %rem, i32* %module, align 4
  %2 = load i32, i32* %size_bytes.addr, align 4
  %m_element_size2 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 5
  %3 = load i32, i32* %m_element_size2, align 4
  %div = udiv i32 %2, %3
  store i32 %div, i32* %element_count, align 4
  %4 = load i32, i32* %module, align 4
  %cmp = icmp ugt i32 %4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load i32, i32* %element_count, align 4
  %inc = add i32 %5, 1
  store i32 %inc, i32* %element_count, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = load i32, i32* %element_count, align 4
  %call = call i32 @_ZN19btGenericMemoryPool24allocate_from_free_nodesEm(%class.btGenericMemoryPool* %this1, i32 %6)
  store i32 %call, i32* %alloc_pos, align 4
  %7 = load i32, i32* %alloc_pos, align 4
  %cmp3 = icmp ne i32 %7, -1
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %8 = load i32, i32* %alloc_pos, align 4
  %call5 = call i8* @_ZN19btGenericMemoryPool16get_element_dataEm(%class.btGenericMemoryPool* %this1, i32 %8)
  store i8* %call5, i8** %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end
  %9 = load i32, i32* %element_count, align 4
  %call7 = call i32 @_ZN19btGenericMemoryPool18allocate_from_poolEm(%class.btGenericMemoryPool* %this1, i32 %9)
  store i32 %call7, i32* %alloc_pos, align 4
  %10 = load i32, i32* %alloc_pos, align 4
  %cmp8 = icmp eq i32 %10, -1
  br i1 %cmp8, label %if.then9, label %if.end10

if.then9:                                         ; preds = %if.end6
  store i8* null, i8** %retval, align 4
  br label %return

if.end10:                                         ; preds = %if.end6
  %11 = load i32, i32* %alloc_pos, align 4
  %call11 = call i8* @_ZN19btGenericMemoryPool16get_element_dataEm(%class.btGenericMemoryPool* %this1, i32 %11)
  store i8* %call11, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end10, %if.then9, %if.then4
  %12 = load i8*, i8** %retval, align 4
  ret i8* %12
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN19btGenericMemoryPool16get_element_dataEm(%class.btGenericMemoryPool* %this, i32 %element_index) #1 comdat {
entry:
  %this.addr = alloca %class.btGenericMemoryPool*, align 4
  %element_index.addr = alloca i32, align 4
  store %class.btGenericMemoryPool* %this, %class.btGenericMemoryPool** %this.addr, align 4
  store i32 %element_index, i32* %element_index.addr, align 4
  %this1 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %this.addr, align 4
  %m_pool = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 0
  %0 = load i8*, i8** %m_pool, align 4
  %1 = load i32, i32* %element_index.addr, align 4
  %m_element_size = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 5
  %2 = load i32, i32* %m_element_size, align 4
  %mul = mul i32 %1, %2
  %arrayidx = getelementptr inbounds i8, i8* %0, i32 %mul
  ret i8* %arrayidx
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN19btGenericMemoryPool10freeMemoryEPv(%class.btGenericMemoryPool* %this, i8* %pointer) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btGenericMemoryPool*, align 4
  %pointer.addr = alloca i8*, align 4
  %pointer_pos = alloca i8*, align 4
  %pool_pos = alloca i8*, align 4
  %offset = alloca i32, align 4
  store %class.btGenericMemoryPool* %this, %class.btGenericMemoryPool** %this.addr, align 4
  store i8* %pointer, i8** %pointer.addr, align 4
  %this1 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %this.addr, align 4
  %0 = load i8*, i8** %pointer.addr, align 4
  store i8* %0, i8** %pointer_pos, align 4
  %m_pool = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 0
  %1 = load i8*, i8** %m_pool, align 4
  store i8* %1, i8** %pool_pos, align 4
  %2 = load i8*, i8** %pointer_pos, align 4
  %3 = load i8*, i8** %pool_pos, align 4
  %cmp = icmp ult i8* %2, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %4 = load i8*, i8** %pointer_pos, align 4
  %5 = load i8*, i8** %pool_pos, align 4
  %sub.ptr.lhs.cast = ptrtoint i8* %4 to i32
  %sub.ptr.rhs.cast = ptrtoint i8* %5 to i32
  %sub.ptr.sub = sub i32 %sub.ptr.lhs.cast, %sub.ptr.rhs.cast
  store i32 %sub.ptr.sub, i32* %offset, align 4
  %6 = load i32, i32* %offset, align 4
  %call = call i32 @_ZN19btGenericMemoryPool17get_pool_capacityEv(%class.btGenericMemoryPool* %this1)
  %cmp2 = icmp uge i32 %6, %call
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i1 false, i1* %retval, align 1
  br label %return

if.end4:                                          ; preds = %if.end
  %7 = load i32, i32* %offset, align 4
  %m_element_size = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 5
  %8 = load i32, i32* %m_element_size, align 4
  %div = udiv i32 %7, %8
  %m_free_nodes = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 1
  %9 = load i32*, i32** %m_free_nodes, align 4
  %m_free_nodes_count = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 4
  %10 = load i32, i32* %m_free_nodes_count, align 4
  %arrayidx = getelementptr inbounds i32, i32* %9, i32 %10
  store i32 %div, i32* %arrayidx, align 4
  %m_free_nodes_count5 = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 4
  %11 = load i32, i32* %m_free_nodes_count5, align 4
  %inc = add i32 %11, 1
  store i32 %inc, i32* %m_free_nodes_count5, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end4, %if.then3, %if.then
  %12 = load i1, i1* %retval, align 1
  ret i1 %12
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN19btGenericMemoryPool17get_pool_capacityEv(%class.btGenericMemoryPool* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btGenericMemoryPool*, align 4
  store %class.btGenericMemoryPool* %this, %class.btGenericMemoryPool** %this.addr, align 4
  %this1 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %this.addr, align 4
  %m_element_size = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_element_size, align 4
  %m_max_element_count = getelementptr inbounds %class.btGenericMemoryPool, %class.btGenericMemoryPool* %this1, i32 0, i32 6
  %1 = load i32, i32* %m_max_element_count, align 4
  %mul = mul i32 %0, %1
  ret i32 %mul
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btGenericPoolAllocator* @_ZN22btGenericPoolAllocatorD2Ev(%class.btGenericPoolAllocator* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btGenericPoolAllocator*, align 4
  %this.addr = alloca %class.btGenericPoolAllocator*, align 4
  %i = alloca i32, align 4
  store %class.btGenericPoolAllocator* %this, %class.btGenericPoolAllocator** %this.addr, align 4
  %this1 = load %class.btGenericPoolAllocator*, %class.btGenericPoolAllocator** %this.addr, align 4
  store %class.btGenericPoolAllocator* %this1, %class.btGenericPoolAllocator** %retval, align 4
  %0 = bitcast %class.btGenericPoolAllocator* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [4 x i8*] }, { [4 x i8*] }* @_ZTV22btGenericPoolAllocator, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %m_pool_count = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 4
  %2 = load i32, i32* %m_pool_count, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pools = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 3
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [16 x %class.btGenericMemoryPool*], [16 x %class.btGenericMemoryPool*]* %m_pools, i32 0, i32 %3
  %4 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %arrayidx, align 4
  call void @_ZN19btGenericMemoryPool8end_poolEv(%class.btGenericMemoryPool* %4)
  %m_pools2 = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 3
  %5 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [16 x %class.btGenericMemoryPool*], [16 x %class.btGenericMemoryPool*]* %m_pools2, i32 0, i32 %5
  %6 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %arrayidx3, align 4
  %7 = bitcast %class.btGenericMemoryPool* %6 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %7)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load %class.btGenericPoolAllocator*, %class.btGenericPoolAllocator** %retval, align 4
  ret %class.btGenericPoolAllocator* %9
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN22btGenericPoolAllocatorD0Ev(%class.btGenericPoolAllocator* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btGenericPoolAllocator*, align 4
  store %class.btGenericPoolAllocator* %this, %class.btGenericPoolAllocator** %this.addr, align 4
  %this1 = load %class.btGenericPoolAllocator*, %class.btGenericPoolAllocator** %this.addr, align 4
  %call = call %class.btGenericPoolAllocator* @_ZN22btGenericPoolAllocatorD1Ev(%class.btGenericPoolAllocator* %this1) #5
  %0 = bitcast %class.btGenericPoolAllocator* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

; Function Attrs: noinline optnone
define hidden %class.btGenericMemoryPool* @_ZN22btGenericPoolAllocator13push_new_poolEv(%class.btGenericPoolAllocator* %this) #2 {
entry:
  %retval = alloca %class.btGenericMemoryPool*, align 4
  %this.addr = alloca %class.btGenericPoolAllocator*, align 4
  %newptr = alloca %class.btGenericMemoryPool*, align 4
  store %class.btGenericPoolAllocator* %this, %class.btGenericPoolAllocator** %this.addr, align 4
  %this1 = load %class.btGenericPoolAllocator*, %class.btGenericPoolAllocator** %this.addr, align 4
  %m_pool_count = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_pool_count, align 4
  %cmp = icmp uge i32 %0, 16
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %class.btGenericMemoryPool* null, %class.btGenericMemoryPool** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 28, i32 16)
  %1 = bitcast i8* %call to %class.btGenericMemoryPool*
  store %class.btGenericMemoryPool* %1, %class.btGenericMemoryPool** %newptr, align 4
  %2 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %newptr, align 4
  %m_pools = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 3
  %m_pool_count2 = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 4
  %3 = load i32, i32* %m_pool_count2, align 4
  %arrayidx = getelementptr inbounds [16 x %class.btGenericMemoryPool*], [16 x %class.btGenericMemoryPool*]* %m_pools, i32 0, i32 %3
  store %class.btGenericMemoryPool* %2, %class.btGenericMemoryPool** %arrayidx, align 4
  %m_pools3 = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 3
  %m_pool_count4 = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 4
  %4 = load i32, i32* %m_pool_count4, align 4
  %arrayidx5 = getelementptr inbounds [16 x %class.btGenericMemoryPool*], [16 x %class.btGenericMemoryPool*]* %m_pools3, i32 0, i32 %4
  %5 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %arrayidx5, align 4
  %m_pool_element_size = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 1
  %6 = load i32, i32* %m_pool_element_size, align 4
  %m_pool_element_count = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_pool_element_count, align 4
  call void @_ZN19btGenericMemoryPool9init_poolEmm(%class.btGenericMemoryPool* %5, i32 %6, i32 %7)
  %m_pool_count6 = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 4
  %8 = load i32, i32* %m_pool_count6, align 4
  %inc = add i32 %8, 1
  store i32 %inc, i32* %m_pool_count6, align 4
  %9 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %newptr, align 4
  store %class.btGenericMemoryPool* %9, %class.btGenericMemoryPool** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %10 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %retval, align 4
  ret %class.btGenericMemoryPool* %10
}

; Function Attrs: noinline optnone
define hidden i8* @_ZN22btGenericPoolAllocator14failback_allocEm(%class.btGenericPoolAllocator* %this, i32 %size_bytes) #2 {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btGenericPoolAllocator*, align 4
  %size_bytes.addr = alloca i32, align 4
  %pool = alloca %class.btGenericMemoryPool*, align 4
  store %class.btGenericPoolAllocator* %this, %class.btGenericPoolAllocator** %this.addr, align 4
  store i32 %size_bytes, i32* %size_bytes.addr, align 4
  %this1 = load %class.btGenericPoolAllocator*, %class.btGenericPoolAllocator** %this.addr, align 4
  store %class.btGenericMemoryPool* null, %class.btGenericMemoryPool** %pool, align 4
  %0 = load i32, i32* %size_bytes.addr, align 4
  %call = call i32 @_ZN22btGenericPoolAllocator17get_pool_capacityEv(%class.btGenericPoolAllocator* %this1)
  %cmp = icmp ule i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call2 = call %class.btGenericMemoryPool* @_ZN22btGenericPoolAllocator13push_new_poolEv(%class.btGenericPoolAllocator* %this1)
  store %class.btGenericMemoryPool* %call2, %class.btGenericMemoryPool** %pool, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %1 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %pool, align 4
  %cmp3 = icmp eq %class.btGenericMemoryPool* %1, null
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %2 = load i32, i32* %size_bytes.addr, align 4
  %call5 = call i8* @_Z22btAlignedAllocInternalmi(i32 %2, i32 16)
  store i8* %call5, i8** %retval, align 4
  br label %return

if.end6:                                          ; preds = %if.end
  %3 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %pool, align 4
  %4 = load i32, i32* %size_bytes.addr, align 4
  %call7 = call i8* @_ZN19btGenericMemoryPool8allocateEm(%class.btGenericMemoryPool* %3, i32 %4)
  store i8* %call7, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then4
  %5 = load i8*, i8** %retval, align 4
  ret i8* %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN22btGenericPoolAllocator17get_pool_capacityEv(%class.btGenericPoolAllocator* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btGenericPoolAllocator*, align 4
  store %class.btGenericPoolAllocator* %this, %class.btGenericPoolAllocator** %this.addr, align 4
  %this1 = load %class.btGenericPoolAllocator*, %class.btGenericPoolAllocator** %this.addr, align 4
  %m_pool_element_size = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_pool_element_size, align 4
  %m_pool_element_count = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 2
  %1 = load i32, i32* %m_pool_element_count, align 4
  %mul = mul i32 %0, %1
  ret i32 %mul
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN22btGenericPoolAllocator13failback_freeEPv(%class.btGenericPoolAllocator* %this, i8* %pointer) #2 {
entry:
  %this.addr = alloca %class.btGenericPoolAllocator*, align 4
  %pointer.addr = alloca i8*, align 4
  store %class.btGenericPoolAllocator* %this, %class.btGenericPoolAllocator** %this.addr, align 4
  store i8* %pointer, i8** %pointer.addr, align 4
  %this1 = load %class.btGenericPoolAllocator*, %class.btGenericPoolAllocator** %this.addr, align 4
  %0 = load i8*, i8** %pointer.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret i1 true
}

; Function Attrs: noinline optnone
define hidden i8* @_ZN22btGenericPoolAllocator8allocateEm(%class.btGenericPoolAllocator* %this, i32 %size_bytes) #2 {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btGenericPoolAllocator*, align 4
  %size_bytes.addr = alloca i32, align 4
  %ptr = alloca i8*, align 4
  %i = alloca i32, align 4
  store %class.btGenericPoolAllocator* %this, %class.btGenericPoolAllocator** %this.addr, align 4
  store i32 %size_bytes, i32* %size_bytes.addr, align 4
  %this1 = load %class.btGenericPoolAllocator*, %class.btGenericPoolAllocator** %this.addr, align 4
  store i8* null, i8** %ptr, align 4
  store i32 0, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %i, align 4
  %m_pool_count = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 4
  %1 = load i32, i32* %m_pool_count, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %2 = load i8*, i8** %ptr, align 4
  %cmp2 = icmp eq i8* %2, null
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %3 = phi i1 [ false, %while.cond ], [ %cmp2, %land.rhs ]
  br i1 %3, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_pools = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 3
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [16 x %class.btGenericMemoryPool*], [16 x %class.btGenericMemoryPool*]* %m_pools, i32 0, i32 %4
  %5 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %arrayidx, align 4
  %6 = load i32, i32* %size_bytes.addr, align 4
  %call = call i8* @_ZN19btGenericMemoryPool8allocateEm(%class.btGenericMemoryPool* %5, i32 %6)
  store i8* %call, i8** %ptr, align 4
  %7 = load i32, i32* %i, align 4
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %8 = load i8*, i8** %ptr, align 4
  %tobool = icmp ne i8* %8, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  %9 = load i8*, i8** %ptr, align 4
  store i8* %9, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %while.end
  %10 = load i32, i32* %size_bytes.addr, align 4
  %call3 = call i8* @_ZN22btGenericPoolAllocator14failback_allocEm(%class.btGenericPoolAllocator* %this1, i32 %10)
  store i8* %call3, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %11 = load i8*, i8** %retval, align 4
  ret i8* %11
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN22btGenericPoolAllocator10freeMemoryEPv(%class.btGenericPoolAllocator* %this, i8* %pointer) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btGenericPoolAllocator*, align 4
  %pointer.addr = alloca i8*, align 4
  %result = alloca i8, align 1
  %i = alloca i32, align 4
  store %class.btGenericPoolAllocator* %this, %class.btGenericPoolAllocator** %this.addr, align 4
  store i8* %pointer, i8** %pointer.addr, align 4
  %this1 = load %class.btGenericPoolAllocator*, %class.btGenericPoolAllocator** %this.addr, align 4
  store i8 0, i8* %result, align 1
  store i32 0, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %i, align 4
  %m_pool_count = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 4
  %1 = load i32, i32* %m_pool_count, align 4
  %cmp = icmp ult i32 %0, %1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %2 = load i8, i8* %result, align 1
  %tobool = trunc i8 %2 to i1
  %conv = zext i1 %tobool to i32
  %cmp2 = icmp eq i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %3 = phi i1 [ false, %while.cond ], [ %cmp2, %land.rhs ]
  br i1 %3, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_pools = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 3
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [16 x %class.btGenericMemoryPool*], [16 x %class.btGenericMemoryPool*]* %m_pools, i32 0, i32 %4
  %5 = load %class.btGenericMemoryPool*, %class.btGenericMemoryPool** %arrayidx, align 4
  %6 = load i8*, i8** %pointer.addr, align 4
  %call = call zeroext i1 @_ZN19btGenericMemoryPool10freeMemoryEPv(%class.btGenericMemoryPool* %5, i8* %6)
  %frombool = zext i1 %call to i8
  store i8 %frombool, i8* %result, align 1
  %7 = load i32, i32* %i, align 4
  %inc = add i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %8 = load i8, i8* %result, align 1
  %tobool3 = trunc i8 %8 to i1
  br i1 %tobool3, label %if.then, label %if.end

if.then:                                          ; preds = %while.end
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %while.end
  %9 = load i8*, i8** %pointer.addr, align 4
  %call4 = call zeroext i1 @_ZN22btGenericPoolAllocator13failback_freeEPv(%class.btGenericPoolAllocator* %this1, i8* %9)
  store i1 %call4, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline
define internal void @__cxx_global_var_init.1() #0 {
entry:
  %call = call %class.GIM_STANDARD_ALLOCATOR* @_ZN22GIM_STANDARD_ALLOCATORC2Ev(%class.GIM_STANDARD_ALLOCATOR* @g_main_allocator)
  %0 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor, i8* null, i8* @__dso_handle) #5
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.GIM_STANDARD_ALLOCATOR* @_ZN22GIM_STANDARD_ALLOCATORC2Ev(%class.GIM_STANDARD_ALLOCATOR* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.GIM_STANDARD_ALLOCATOR*, align 4
  store %class.GIM_STANDARD_ALLOCATOR* %this, %class.GIM_STANDARD_ALLOCATOR** %this.addr, align 4
  %this1 = load %class.GIM_STANDARD_ALLOCATOR*, %class.GIM_STANDARD_ALLOCATOR** %this.addr, align 4
  %0 = bitcast %class.GIM_STANDARD_ALLOCATOR* %this1 to %class.btGenericPoolAllocator*
  %call = call %class.btGenericPoolAllocator* @_ZN22btGenericPoolAllocatorC2Emm(%class.btGenericPoolAllocator* %0, i32 8, i32 32768)
  %1 = bitcast %class.GIM_STANDARD_ALLOCATOR* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [4 x i8*] }, { [4 x i8*] }* @_ZTV22GIM_STANDARD_ALLOCATOR, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %class.GIM_STANDARD_ALLOCATOR* %this1
}

; Function Attrs: noinline
define internal void @__cxx_global_array_dtor(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  %call = call %class.GIM_STANDARD_ALLOCATOR* @_ZN22GIM_STANDARD_ALLOCATORD2Ev(%class.GIM_STANDARD_ALLOCATOR* @g_main_allocator) #5
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.GIM_STANDARD_ALLOCATOR* @_ZN22GIM_STANDARD_ALLOCATORD2Ev(%class.GIM_STANDARD_ALLOCATOR* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.GIM_STANDARD_ALLOCATOR*, align 4
  store %class.GIM_STANDARD_ALLOCATOR* %this, %class.GIM_STANDARD_ALLOCATOR** %this.addr, align 4
  %this1 = load %class.GIM_STANDARD_ALLOCATOR*, %class.GIM_STANDARD_ALLOCATOR** %this.addr, align 4
  %0 = bitcast %class.GIM_STANDARD_ALLOCATOR* %this1 to %class.btGenericPoolAllocator*
  %call = call %class.btGenericPoolAllocator* @_ZN22btGenericPoolAllocatorD2Ev(%class.btGenericPoolAllocator* %0) #5
  ret %class.GIM_STANDARD_ALLOCATOR* %this1
}

; Function Attrs: nounwind
declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #5

; Function Attrs: noinline optnone
define hidden i8* @_Z11btPoolAllocm(i32 %size) #2 {
entry:
  %size.addr = alloca i32, align 4
  store i32 %size, i32* %size.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %call = call i8* @_ZN22btGenericPoolAllocator8allocateEm(%class.btGenericPoolAllocator* getelementptr inbounds (%class.GIM_STANDARD_ALLOCATOR, %class.GIM_STANDARD_ALLOCATOR* @g_main_allocator, i32 0, i32 0), i32 %0)
  ret i8* %call
}

; Function Attrs: noinline optnone
define hidden i8* @_Z13btPoolReallocPvmm(i8* %ptr, i32 %oldsize, i32 %newsize) #2 {
entry:
  %ptr.addr = alloca i8*, align 4
  %oldsize.addr = alloca i32, align 4
  %newsize.addr = alloca i32, align 4
  %newptr = alloca i8*, align 4
  %copysize = alloca i32, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  store i32 %oldsize, i32* %oldsize.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %call = call i8* @_Z11btPoolAllocm(i32 %0)
  store i8* %call, i8** %newptr, align 4
  %1 = load i32, i32* %oldsize.addr, align 4
  %2 = load i32, i32* %newsize.addr, align 4
  %cmp = icmp ult i32 %1, %2
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load i32, i32* %oldsize.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %4 = load i32, i32* %newsize.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %3, %cond.true ], [ %4, %cond.false ]
  store i32 %cond, i32* %copysize, align 4
  %5 = load i8*, i8** %newptr, align 4
  %6 = load i8*, i8** %ptr.addr, align 4
  %7 = load i32, i32* %copysize, align 4
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 1 %5, i8* align 1 %6, i32 %7, i1 false)
  %8 = load i8*, i8** %ptr.addr, align 4
  call void @_Z10btPoolFreePv(i8* %8)
  %9 = load i8*, i8** %newptr, align 4
  ret i8* %9
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: noinline optnone
define hidden void @_Z10btPoolFreePv(i8* %ptr) #2 {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  %call = call zeroext i1 @_ZN22btGenericPoolAllocator10freeMemoryEPv(%class.btGenericPoolAllocator* getelementptr inbounds (%class.GIM_STANDARD_ALLOCATOR, %class.GIM_STANDARD_ALLOCATOR* @g_main_allocator, i32 0, i32 0), i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGenericPoolAllocator* @_ZN22btGenericPoolAllocatorC2Emm(%class.btGenericPoolAllocator* returned %this, i32 %pool_element_size, i32 %pool_element_count) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGenericPoolAllocator*, align 4
  %pool_element_size.addr = alloca i32, align 4
  %pool_element_count.addr = alloca i32, align 4
  store %class.btGenericPoolAllocator* %this, %class.btGenericPoolAllocator** %this.addr, align 4
  store i32 %pool_element_size, i32* %pool_element_size.addr, align 4
  store i32 %pool_element_count, i32* %pool_element_count.addr, align 4
  %this1 = load %class.btGenericPoolAllocator*, %class.btGenericPoolAllocator** %this.addr, align 4
  %0 = bitcast %class.btGenericPoolAllocator* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [4 x i8*] }, { [4 x i8*] }* @_ZTV22btGenericPoolAllocator, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_pool_count = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 4
  store i32 0, i32* %m_pool_count, align 4
  %1 = load i32, i32* %pool_element_size.addr, align 4
  %m_pool_element_size = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 1
  store i32 %1, i32* %m_pool_element_size, align 4
  %2 = load i32, i32* %pool_element_count.addr, align 4
  %m_pool_element_count = getelementptr inbounds %class.btGenericPoolAllocator, %class.btGenericPoolAllocator* %this1, i32 0, i32 2
  store i32 %2, i32* %m_pool_element_count, align 4
  ret %class.btGenericPoolAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN22GIM_STANDARD_ALLOCATORD0Ev(%class.GIM_STANDARD_ALLOCATOR* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.GIM_STANDARD_ALLOCATOR*, align 4
  store %class.GIM_STANDARD_ALLOCATOR* %this, %class.GIM_STANDARD_ALLOCATOR** %this.addr, align 4
  %this1 = load %class.GIM_STANDARD_ALLOCATOR*, %class.GIM_STANDARD_ALLOCATOR** %this.addr, align 4
  %call = call %class.GIM_STANDARD_ALLOCATOR* @_ZN22GIM_STANDARD_ALLOCATORD2Ev(%class.GIM_STANDARD_ALLOCATOR* %this1) #5
  %0 = bitcast %class.GIM_STANDARD_ALLOCATOR* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btGenericPoolAllocator.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  call void @__cxx_global_var_init.1()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
