; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btPolyhedralContactClipping.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btPolyhedralContactClipping.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btConvexPolyhedron = type { i32 (...)**, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btFace*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btFace = type { %class.btAlignedObjectArray.3, [4 x float] }
%class.btAlignedObjectArray.3 = type <{ %class.btAlignedAllocator.4, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.4 = type { i8 }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_ = comdat any

$_ZNK9btVector34lerpERKS_RKf = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_Z10BoxSupportPKfS0_Pf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI6btFaceEixEi = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_Z12IsAlmostZeroRK9btVector3 = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_Z23btSegmentsClosestPointsR9btVector3S0_S0_RfS1_RKS_S3_fS3_f = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_Z6btSqrtf = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIiEixEi = comdat any

$_Z6btSwapIP20btAlignedObjectArrayI9btVector3EEvRT_S5_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E2atEi = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z5btDotRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@gExpectedNbTests = hidden global i32 0, align 4
@gActualNbTests = hidden global i32 0, align 4
@gUseInternalObject = hidden global i8 1, align 1
@_ZL19gActualSATPairTests = internal global i32 0, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btPolyhedralContactClipping.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN27btPolyhedralContactClipping8clipFaceERK20btAlignedObjectArrayI9btVector3ERS2_RKS1_f(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %pVtxIn, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %ppVtxOut, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormalWS, float %planeEqWS) #2 {
entry:
  %pVtxIn.addr = alloca %class.btAlignedObjectArray*, align 4
  %ppVtxOut.addr = alloca %class.btAlignedObjectArray*, align 4
  %planeNormalWS.addr = alloca %class.btVector3*, align 4
  %planeEqWS.addr = alloca float, align 4
  %ve = alloca i32, align 4
  %ds = alloca float, align 4
  %de = alloca float, align 4
  %numVerts = alloca i32, align 4
  %firstVertex = alloca %class.btVector3, align 4
  %endVertex = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca float, align 4
  store %class.btAlignedObjectArray* %pVtxIn, %class.btAlignedObjectArray** %pVtxIn.addr, align 4
  store %class.btAlignedObjectArray* %ppVtxOut, %class.btAlignedObjectArray** %ppVtxOut.addr, align 4
  store %class.btVector3* %planeNormalWS, %class.btVector3** %planeNormalWS.addr, align 4
  store float %planeEqWS, float* %planeEqWS.addr, align 4
  %0 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %0)
  store i32 %call, i32* %numVerts, align 4
  %1 = load i32, i32* %numVerts, align 4
  %cmp = icmp slt i32 %1, 2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %for.end

if.end:                                           ; preds = %entry
  %2 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn.addr, align 4
  %3 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn.addr, align 4
  %call1 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %3)
  %sub = sub nsw i32 %call1, 1
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %2, i32 %sub)
  %4 = bitcast %class.btVector3* %firstVertex to i8*
  %5 = bitcast %class.btVector3* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %6, i32 0)
  %7 = bitcast %class.btVector3* %endVertex to i8*
  %8 = bitcast %class.btVector3* %call3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btVector3*, %class.btVector3** %planeNormalWS.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %firstVertex)
  %10 = load float, float* %planeEqWS.addr, align 4
  %add = fadd float %call4, %10
  store float %add, float* %ds, align 4
  store i32 0, i32* %ve, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %11 = load i32, i32* %ve, align 4
  %12 = load i32, i32* %numVerts, align 4
  %cmp5 = icmp slt i32 %11, %12
  br i1 %cmp5, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn.addr, align 4
  %14 = load i32, i32* %ve, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %13, i32 %14)
  %15 = bitcast %class.btVector3* %endVertex to i8*
  %16 = bitcast %class.btVector3* %call6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  %17 = load %class.btVector3*, %class.btVector3** %planeNormalWS.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %endVertex)
  %18 = load float, float* %planeEqWS.addr, align 4
  %add8 = fadd float %call7, %18
  store float %add8, float* %de, align 4
  %19 = load float, float* %ds, align 4
  %cmp9 = fcmp olt float %19, 0.000000e+00
  br i1 %cmp9, label %if.then10, label %if.else16

if.then10:                                        ; preds = %for.body
  %20 = load float, float* %de, align 4
  %cmp11 = fcmp olt float %20, 0.000000e+00
  br i1 %cmp11, label %if.then12, label %if.else

if.then12:                                        ; preds = %if.then10
  %21 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %ppVtxOut.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %21, %class.btVector3* nonnull align 4 dereferenceable(16) %endVertex)
  br label %if.end15

if.else:                                          ; preds = %if.then10
  %22 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %ppVtxOut.addr, align 4
  %23 = load float, float* %ds, align 4
  %mul = fmul float %23, 1.000000e+00
  %24 = load float, float* %ds, align 4
  %25 = load float, float* %de, align 4
  %sub14 = fsub float %24, %25
  %div = fdiv float %mul, %sub14
  store float %div, float* %ref.tmp13, align 4
  call void @_ZNK9btVector34lerpERKS_RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %firstVertex, %class.btVector3* nonnull align 4 dereferenceable(16) %endVertex, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %22, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  br label %if.end15

if.end15:                                         ; preds = %if.else, %if.then12
  br label %if.end25

if.else16:                                        ; preds = %for.body
  %26 = load float, float* %de, align 4
  %cmp17 = fcmp olt float %26, 0.000000e+00
  br i1 %cmp17, label %if.then18, label %if.end24

if.then18:                                        ; preds = %if.else16
  %27 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %ppVtxOut.addr, align 4
  %28 = load float, float* %ds, align 4
  %mul21 = fmul float %28, 1.000000e+00
  %29 = load float, float* %ds, align 4
  %30 = load float, float* %de, align 4
  %sub22 = fsub float %29, %30
  %div23 = fdiv float %mul21, %sub22
  store float %div23, float* %ref.tmp20, align 4
  call void @_ZNK9btVector34lerpERKS_RKf(%class.btVector3* sret align 4 %ref.tmp19, %class.btVector3* %firstVertex, %class.btVector3* nonnull align 4 dereferenceable(16) %endVertex, float* nonnull align 4 dereferenceable(4) %ref.tmp20)
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %27, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp19)
  %31 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %ppVtxOut.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %31, %class.btVector3* nonnull align 4 dereferenceable(16) %endVertex)
  br label %if.end24

if.end24:                                         ; preds = %if.then18, %if.else16
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %if.end15
  %32 = bitcast %class.btVector3* %firstVertex to i8*
  %33 = bitcast %class.btVector3* %endVertex to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 16, i1 false)
  %34 = load float, float* %de, align 4
  store float %34, float* %ds, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end25
  %35 = load i32, i32* %ve, align 4
  %inc = add nsw i32 %35, 1
  store i32 %inc, i32* %ve, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btVector3* %_Val, %class.btVector3** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 %2
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %3)
  %4 = bitcast i8* %call5 to %class.btVector3*
  %5 = load %class.btVector3*, %class.btVector3** %_Val.addr, align 4
  %6 = bitcast %class.btVector3* %4 to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34lerpERKS_RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %t) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %t.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %t, float** %t.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 0
  %3 = load float, float* %arrayidx5, align 4
  %sub = fsub float %2, %3
  %4 = load float*, float** %t.addr, align 4
  %5 = load float, float* %4, align 4
  %mul = fmul float %sub, %5
  %add = fadd float %0, %mul
  store float %add, float* %ref.tmp, align 4
  %m_floats7 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %6 = load float, float* %arrayidx8, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 1
  %8 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 1
  %9 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %8, %9
  %10 = load float*, float** %t.addr, align 4
  %11 = load float, float* %10, align 4
  %mul14 = fmul float %sub13, %11
  %add15 = fadd float %6, %mul14
  store float %add15, float* %ref.tmp6, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %12 = load float, float* %arrayidx18, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats19 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [4 x float], [4 x float]* %m_floats19, i32 0, i32 2
  %14 = load float, float* %arrayidx20, align 4
  %m_floats21 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 2
  %15 = load float, float* %arrayidx22, align 4
  %sub23 = fsub float %14, %15
  %16 = load float*, float** %t.addr, align 4
  %17 = load float, float* %16, align 4
  %mul24 = fmul float %sub23, %17
  %add25 = fadd float %12, %mul24
  store float %add25, float* %ref.tmp16, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp16)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_Z24InverseTransformPoint3x3R9btVector3RKS_RK11btTransform(%class.btVector3* nonnull align 4 dereferenceable(16) %out, %class.btVector3* nonnull align 4 dereferenceable(16) %in, %class.btTransform* nonnull align 4 dereferenceable(64) %tr) #2 {
entry:
  %out.addr = alloca %class.btVector3*, align 4
  %in.addr = alloca %class.btVector3*, align 4
  %tr.addr = alloca %class.btTransform*, align 4
  %rot = alloca %class.btMatrix3x3*, align 4
  %r0 = alloca %class.btVector3*, align 4
  %r1 = alloca %class.btVector3*, align 4
  %r2 = alloca %class.btVector3*, align 4
  %x = alloca float, align 4
  %y = alloca float, align 4
  %z = alloca float, align 4
  store %class.btVector3* %out, %class.btVector3** %out.addr, align 4
  store %class.btVector3* %in, %class.btVector3** %in.addr, align 4
  store %class.btTransform* %tr, %class.btTransform** %tr.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %tr.addr, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %0)
  store %class.btMatrix3x3* %call, %class.btMatrix3x3** %rot, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot, align 4
  %call1 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %1, i32 0)
  store %class.btVector3* %call1, %class.btVector3** %r0, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  store %class.btVector3* %call2, %class.btVector3** %r1, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %rot, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %3, i32 2)
  store %class.btVector3* %call3, %class.btVector3** %r2, align 4
  %4 = load %class.btVector3*, %class.btVector3** %r0, align 4
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %4)
  %5 = load float, float* %call4, align 4
  %6 = load %class.btVector3*, %class.btVector3** %in.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %6)
  %7 = load float, float* %call5, align 4
  %mul = fmul float %5, %7
  %8 = load %class.btVector3*, %class.btVector3** %r1, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %8)
  %9 = load float, float* %call6, align 4
  %10 = load %class.btVector3*, %class.btVector3** %in.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %10)
  %11 = load float, float* %call7, align 4
  %mul8 = fmul float %9, %11
  %add = fadd float %mul, %mul8
  %12 = load %class.btVector3*, %class.btVector3** %r2, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %12)
  %13 = load float, float* %call9, align 4
  %14 = load %class.btVector3*, %class.btVector3** %in.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %14)
  %15 = load float, float* %call10, align 4
  %mul11 = fmul float %13, %15
  %add12 = fadd float %add, %mul11
  store float %add12, float* %x, align 4
  %16 = load %class.btVector3*, %class.btVector3** %r0, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %16)
  %17 = load float, float* %call13, align 4
  %18 = load %class.btVector3*, %class.btVector3** %in.addr, align 4
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %18)
  %19 = load float, float* %call14, align 4
  %mul15 = fmul float %17, %19
  %20 = load %class.btVector3*, %class.btVector3** %r1, align 4
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %20)
  %21 = load float, float* %call16, align 4
  %22 = load %class.btVector3*, %class.btVector3** %in.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %22)
  %23 = load float, float* %call17, align 4
  %mul18 = fmul float %21, %23
  %add19 = fadd float %mul15, %mul18
  %24 = load %class.btVector3*, %class.btVector3** %r2, align 4
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %24)
  %25 = load float, float* %call20, align 4
  %26 = load %class.btVector3*, %class.btVector3** %in.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %26)
  %27 = load float, float* %call21, align 4
  %mul22 = fmul float %25, %27
  %add23 = fadd float %add19, %mul22
  store float %add23, float* %y, align 4
  %28 = load %class.btVector3*, %class.btVector3** %r0, align 4
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %28)
  %29 = load float, float* %call24, align 4
  %30 = load %class.btVector3*, %class.btVector3** %in.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %30)
  %31 = load float, float* %call25, align 4
  %mul26 = fmul float %29, %31
  %32 = load %class.btVector3*, %class.btVector3** %r1, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %32)
  %33 = load float, float* %call27, align 4
  %34 = load %class.btVector3*, %class.btVector3** %in.addr, align 4
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %34)
  %35 = load float, float* %call28, align 4
  %mul29 = fmul float %33, %35
  %add30 = fadd float %mul26, %mul29
  %36 = load %class.btVector3*, %class.btVector3** %r2, align 4
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %36)
  %37 = load float, float* %call31, align 4
  %38 = load %class.btVector3*, %class.btVector3** %in.addr, align 4
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %38)
  %39 = load float, float* %call32, align 4
  %mul33 = fmul float %37, %39
  %add34 = fadd float %add30, %mul33
  store float %add34, float* %z, align 4
  %40 = load %class.btVector3*, %class.btVector3** %out.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %40, float* nonnull align 4 dereferenceable(4) %x, float* nonnull align 4 dereferenceable(4) %y, float* nonnull align 4 dereferenceable(4) %z)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_Z19TestInternalObjectsRK11btTransformS1_RK9btVector3S4_RK18btConvexPolyhedronS7_f(%class.btTransform* nonnull align 4 dereferenceable(64) %trans0, %class.btTransform* nonnull align 4 dereferenceable(64) %trans1, %class.btVector3* nonnull align 4 dereferenceable(16) %delta_c, %class.btVector3* nonnull align 4 dereferenceable(16) %axis, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %convex0, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %convex1, float %dmin) #2 {
entry:
  %retval = alloca i1, align 1
  %trans0.addr = alloca %class.btTransform*, align 4
  %trans1.addr = alloca %class.btTransform*, align 4
  %delta_c.addr = alloca %class.btVector3*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %convex0.addr = alloca %class.btConvexPolyhedron*, align 4
  %convex1.addr = alloca %class.btConvexPolyhedron*, align 4
  %dmin.addr = alloca float, align 4
  %dp = alloca float, align 4
  %localAxis0 = alloca %class.btVector3, align 4
  %localAxis1 = alloca %class.btVector3, align 4
  %p0 = alloca [3 x float], align 4
  %p1 = alloca [3 x float], align 4
  %Radius0 = alloca float, align 4
  %Radius1 = alloca float, align 4
  %MinRadius = alloca float, align 4
  %MaxRadius = alloca float, align 4
  %MinMaxRadius = alloca float, align 4
  %d0 = alloca float, align 4
  %d1 = alloca float, align 4
  %depth = alloca float, align 4
  store %class.btTransform* %trans0, %class.btTransform** %trans0.addr, align 4
  store %class.btTransform* %trans1, %class.btTransform** %trans1.addr, align 4
  store %class.btVector3* %delta_c, %class.btVector3** %delta_c.addr, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4
  store %class.btConvexPolyhedron* %convex0, %class.btConvexPolyhedron** %convex0.addr, align 4
  store %class.btConvexPolyhedron* %convex1, %class.btConvexPolyhedron** %convex1.addr, align 4
  store float %dmin, float* %dmin.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %delta_c.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %dp, align 4
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAxis0)
  %2 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %3 = load %class.btTransform*, %class.btTransform** %trans0.addr, align 4
  call void @_Z24InverseTransformPoint3x3R9btVector3RKS_RK11btTransform(%class.btVector3* nonnull align 4 dereferenceable(16) %localAxis0, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localAxis1)
  %4 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %5 = load %class.btTransform*, %class.btTransform** %trans1.addr, align 4
  call void @_Z24InverseTransformPoint3x3R9btVector3RKS_RK11btTransform(%class.btVector3* nonnull align 4 dereferenceable(16) %localAxis1, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btTransform* nonnull align 4 dereferenceable(64) %5)
  %6 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %convex0.addr, align 4
  %m_extents = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %6, i32 0, i32 5
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents)
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAxis0)
  %arraydecay = getelementptr inbounds [3 x float], [3 x float]* %p0, i32 0, i32 0
  call void @_Z10BoxSupportPKfS0_Pf(float* %call3, float* %call4, float* %arraydecay)
  %7 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %convex1.addr, align 4
  %m_extents5 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %7, i32 0, i32 5
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents5)
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %localAxis1)
  %arraydecay8 = getelementptr inbounds [3 x float], [3 x float]* %p1, i32 0, i32 0
  call void @_Z10BoxSupportPKfS0_Pf(float* %call6, float* %call7, float* %arraydecay8)
  %arrayidx = getelementptr inbounds [3 x float], [3 x float]* %p0, i32 0, i32 0
  %8 = load float, float* %arrayidx, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %localAxis0)
  %9 = load float, float* %call9, align 4
  %mul = fmul float %8, %9
  %arrayidx10 = getelementptr inbounds [3 x float], [3 x float]* %p0, i32 0, i32 1
  %10 = load float, float* %arrayidx10, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %localAxis0)
  %11 = load float, float* %call11, align 4
  %mul12 = fmul float %10, %11
  %add = fadd float %mul, %mul12
  %arrayidx13 = getelementptr inbounds [3 x float], [3 x float]* %p0, i32 0, i32 2
  %12 = load float, float* %arrayidx13, align 4
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %localAxis0)
  %13 = load float, float* %call14, align 4
  %mul15 = fmul float %12, %13
  %add16 = fadd float %add, %mul15
  store float %add16, float* %Radius0, align 4
  %arrayidx17 = getelementptr inbounds [3 x float], [3 x float]* %p1, i32 0, i32 0
  %14 = load float, float* %arrayidx17, align 4
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %localAxis1)
  %15 = load float, float* %call18, align 4
  %mul19 = fmul float %14, %15
  %arrayidx20 = getelementptr inbounds [3 x float], [3 x float]* %p1, i32 0, i32 1
  %16 = load float, float* %arrayidx20, align 4
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %localAxis1)
  %17 = load float, float* %call21, align 4
  %mul22 = fmul float %16, %17
  %add23 = fadd float %mul19, %mul22
  %arrayidx24 = getelementptr inbounds [3 x float], [3 x float]* %p1, i32 0, i32 2
  %18 = load float, float* %arrayidx24, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %localAxis1)
  %19 = load float, float* %call25, align 4
  %mul26 = fmul float %18, %19
  %add27 = fadd float %add23, %mul26
  store float %add27, float* %Radius1, align 4
  %20 = load float, float* %Radius0, align 4
  %21 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %convex0.addr, align 4
  %m_radius = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %21, i32 0, i32 6
  %22 = load float, float* %m_radius, align 4
  %cmp = fcmp ogt float %20, %22
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %23 = load float, float* %Radius0, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %24 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %convex0.addr, align 4
  %m_radius28 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %24, i32 0, i32 6
  %25 = load float, float* %m_radius28, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %23, %cond.true ], [ %25, %cond.false ]
  store float %cond, float* %MinRadius, align 4
  %26 = load float, float* %Radius1, align 4
  %27 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %convex1.addr, align 4
  %m_radius29 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %27, i32 0, i32 6
  %28 = load float, float* %m_radius29, align 4
  %cmp30 = fcmp ogt float %26, %28
  br i1 %cmp30, label %cond.true31, label %cond.false32

cond.true31:                                      ; preds = %cond.end
  %29 = load float, float* %Radius1, align 4
  br label %cond.end34

cond.false32:                                     ; preds = %cond.end
  %30 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %convex1.addr, align 4
  %m_radius33 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %30, i32 0, i32 6
  %31 = load float, float* %m_radius33, align 4
  br label %cond.end34

cond.end34:                                       ; preds = %cond.false32, %cond.true31
  %cond35 = phi float [ %29, %cond.true31 ], [ %31, %cond.false32 ]
  store float %cond35, float* %MaxRadius, align 4
  %32 = load float, float* %MaxRadius, align 4
  %33 = load float, float* %MinRadius, align 4
  %add36 = fadd float %32, %33
  store float %add36, float* %MinMaxRadius, align 4
  %34 = load float, float* %MinMaxRadius, align 4
  %35 = load float, float* %dp, align 4
  %add37 = fadd float %34, %35
  store float %add37, float* %d0, align 4
  %36 = load float, float* %MinMaxRadius, align 4
  %37 = load float, float* %dp, align 4
  %sub = fsub float %36, %37
  store float %sub, float* %d1, align 4
  %38 = load float, float* %d0, align 4
  %39 = load float, float* %d1, align 4
  %cmp38 = fcmp olt float %38, %39
  br i1 %cmp38, label %cond.true39, label %cond.false40

cond.true39:                                      ; preds = %cond.end34
  %40 = load float, float* %d0, align 4
  br label %cond.end41

cond.false40:                                     ; preds = %cond.end34
  %41 = load float, float* %d1, align 4
  br label %cond.end41

cond.end41:                                       ; preds = %cond.false40, %cond.true39
  %cond42 = phi float [ %40, %cond.true39 ], [ %41, %cond.false40 ]
  store float %cond42, float* %depth, align 4
  %42 = load float, float* %depth, align 4
  %43 = load float, float* %dmin.addr, align 4
  %cmp43 = fcmp ogt float %42, %43
  br i1 %cmp43, label %if.then, label %if.end

if.then:                                          ; preds = %cond.end41
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %cond.end41
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %44 = load i1, i1* %retval, align 1
  ret i1 %44
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z10BoxSupportPKfS0_Pf(float* %extents, float* %sv, float* %p) #1 comdat {
entry:
  %extents.addr = alloca float*, align 4
  %sv.addr = alloca float*, align 4
  %p.addr = alloca float*, align 4
  store float* %extents, float** %extents.addr, align 4
  store float* %sv, float** %sv.addr, align 4
  store float* %p, float** %p.addr, align 4
  %0 = load float*, float** %sv.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %cmp = fcmp olt float %1, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load float*, float** %extents.addr, align 4
  %arrayidx1 = getelementptr inbounds float, float* %2, i32 0
  %3 = load float, float* %arrayidx1, align 4
  %fneg = fneg float %3
  br label %cond.end

cond.false:                                       ; preds = %entry
  %4 = load float*, float** %extents.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %4, i32 0
  %5 = load float, float* %arrayidx2, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %fneg, %cond.true ], [ %5, %cond.false ]
  %6 = load float*, float** %p.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %6, i32 0
  store float %cond, float* %arrayidx3, align 4
  %7 = load float*, float** %sv.addr, align 4
  %arrayidx4 = getelementptr inbounds float, float* %7, i32 1
  %8 = load float, float* %arrayidx4, align 4
  %cmp5 = fcmp olt float %8, 0.000000e+00
  br i1 %cmp5, label %cond.true6, label %cond.false9

cond.true6:                                       ; preds = %cond.end
  %9 = load float*, float** %extents.addr, align 4
  %arrayidx7 = getelementptr inbounds float, float* %9, i32 1
  %10 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %10
  br label %cond.end11

cond.false9:                                      ; preds = %cond.end
  %11 = load float*, float** %extents.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %11, i32 1
  %12 = load float, float* %arrayidx10, align 4
  br label %cond.end11

cond.end11:                                       ; preds = %cond.false9, %cond.true6
  %cond12 = phi float [ %fneg8, %cond.true6 ], [ %12, %cond.false9 ]
  %13 = load float*, float** %p.addr, align 4
  %arrayidx13 = getelementptr inbounds float, float* %13, i32 1
  store float %cond12, float* %arrayidx13, align 4
  %14 = load float*, float** %sv.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %14, i32 2
  %15 = load float, float* %arrayidx14, align 4
  %cmp15 = fcmp olt float %15, 0.000000e+00
  br i1 %cmp15, label %cond.true16, label %cond.false19

cond.true16:                                      ; preds = %cond.end11
  %16 = load float*, float** %extents.addr, align 4
  %arrayidx17 = getelementptr inbounds float, float* %16, i32 2
  %17 = load float, float* %arrayidx17, align 4
  %fneg18 = fneg float %17
  br label %cond.end21

cond.false19:                                     ; preds = %cond.end11
  %18 = load float*, float** %extents.addr, align 4
  %arrayidx20 = getelementptr inbounds float, float* %18, i32 2
  %19 = load float, float* %arrayidx20, align 4
  br label %cond.end21

cond.end21:                                       ; preds = %cond.false19, %cond.true16
  %cond22 = phi float [ %fneg18, %cond.true16 ], [ %19, %cond.false19 ]
  %20 = load float*, float** %p.addr, align 4
  %arrayidx23 = getelementptr inbounds float, float* %20, i32 2
  store float %cond22, float* %arrayidx23, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN27btPolyhedralContactClipping18findSeparatingAxisERK18btConvexPolyhedronS2_RK11btTransformS5_R9btVector3RN36btDiscreteCollisionDetectorInterface6ResultE(%class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %hullA, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %hullB, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %sep, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %resultOut) #2 {
entry:
  %retval = alloca i1, align 1
  %hullA.addr = alloca %class.btConvexPolyhedron*, align 4
  %hullB.addr = alloca %class.btConvexPolyhedron*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %sep.addr = alloca %class.btVector3*, align 4
  %resultOut.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %c0 = alloca %class.btVector3, align 4
  %c1 = alloca %class.btVector3, align 4
  %DeltaC2 = alloca %class.btVector3, align 4
  %dmin = alloca float, align 4
  %curPlaneTests = alloca i32, align 4
  %numFacesA = alloca i32, align 4
  %i = alloca i32, align 4
  %Normal = alloca %class.btVector3, align 4
  %faceANormalWS = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %d = alloca float, align 4
  %wA = alloca %class.btVector3, align 4
  %wB = alloca %class.btVector3, align 4
  %numFacesB = alloca i32, align 4
  %i34 = alloca i32, align 4
  %Normal38 = alloca %class.btVector3, align 4
  %WorldNormal = alloca %class.btVector3, align 4
  %ref.tmp56 = alloca float, align 4
  %d67 = alloca float, align 4
  %wA68 = alloca %class.btVector3, align 4
  %wB70 = alloca %class.btVector3, align 4
  %edgeAstart = alloca %class.btVector3, align 4
  %edgeAend = alloca %class.btVector3, align 4
  %edgeBstart = alloca %class.btVector3, align 4
  %edgeBend = alloca %class.btVector3, align 4
  %edgeA = alloca i32, align 4
  %edgeB = alloca i32, align 4
  %worldEdgeA = alloca %class.btVector3, align 4
  %worldEdgeB = alloca %class.btVector3, align 4
  %witnessPointA = alloca %class.btVector3, align 4
  %ref.tmp87 = alloca float, align 4
  %ref.tmp88 = alloca float, align 4
  %ref.tmp89 = alloca float, align 4
  %witnessPointB = alloca %class.btVector3, align 4
  %ref.tmp91 = alloca float, align 4
  %ref.tmp92 = alloca float, align 4
  %ref.tmp93 = alloca float, align 4
  %curEdgeEdge = alloca i32, align 4
  %e0 = alloca i32, align 4
  %edge0 = alloca %class.btVector3, align 4
  %WorldEdge0 = alloca %class.btVector3, align 4
  %e1 = alloca i32, align 4
  %edge1 = alloca %class.btVector3, align 4
  %WorldEdge1 = alloca %class.btVector3, align 4
  %Cross = alloca %class.btVector3, align 4
  %ref.tmp117 = alloca float, align 4
  %dist = alloca float, align 4
  %wA127 = alloca %class.btVector3, align 4
  %wB129 = alloca %class.btVector3, align 4
  %ptsVector = alloca %class.btVector3, align 4
  %offsetA = alloca %class.btVector3, align 4
  %offsetB = alloca %class.btVector3, align 4
  %tA = alloca float, align 4
  %tB = alloca float, align 4
  %translation = alloca %class.btVector3, align 4
  %dirA = alloca %class.btVector3, align 4
  %dirB = alloca %class.btVector3, align 4
  %hlenB = alloca float, align 4
  %hlenA = alloca float, align 4
  %nlSqrt = alloca float, align 4
  %nl = alloca float, align 4
  %ref.tmp155 = alloca float, align 4
  %ref.tmp160 = alloca float, align 4
  %ptOnB = alloca %class.btVector3, align 4
  %distance = alloca float, align 4
  %ref.tmp168 = alloca %class.btVector3, align 4
  store %class.btConvexPolyhedron* %hullA, %class.btConvexPolyhedron** %hullA.addr, align 4
  store %class.btConvexPolyhedron* %hullB, %class.btConvexPolyhedron** %hullB.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store %class.btVector3* %sep, %class.btVector3** %sep.addr, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %resultOut, %"struct.btDiscreteCollisionDetectorInterface::Result"** %resultOut.addr, align 4
  %0 = load i32, i32* @_ZL19gActualSATPairTests, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @_ZL19gActualSATPairTests, align 4
  %1 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %2 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %m_localCenter = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %2, i32 0, i32 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %c0, %class.btTransform* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter)
  %3 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %4 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %m_localCenter1 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %4, i32 0, i32 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %c1, %class.btTransform* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter1)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %c0, %class.btVector3* nonnull align 4 dereferenceable(16) %c1)
  store float 0x47EFFFFFE0000000, float* %dmin, align 4
  store i32 0, i32* %curPlaneTests, align 4
  %5 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %5, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces)
  store i32 %call, i32* %numFacesA, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4
  %7 = load i32, i32* %numFacesA, align 4
  %cmp = icmp slt i32 %6, %7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %8 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %m_faces2 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %8, i32 0, i32 2
  %9 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces2, i32 %9)
  %m_plane = getelementptr inbounds %struct.btFace, %struct.btFace* %call3, i32 0, i32 1
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_plane, i32 0, i32 0
  %10 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %m_faces4 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %10, i32 0, i32 2
  %11 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces4, i32 %11)
  %m_plane6 = getelementptr inbounds %struct.btFace, %struct.btFace* %call5, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_plane6, i32 0, i32 1
  %12 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %m_faces8 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %12, i32 0, i32 2
  %13 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces8, i32 %13)
  %m_plane10 = getelementptr inbounds %struct.btFace, %struct.btFace* %call9, i32 0, i32 1
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_plane10, i32 0, i32 2
  %call12 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %Normal, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx7, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %14 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %14)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %faceANormalWS, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call13, %class.btVector3* nonnull align 4 dereferenceable(16) %Normal)
  %call14 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %faceANormalWS)
  %cmp15 = fcmp olt float %call14, 0.000000e+00
  br i1 %cmp15, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store float -1.000000e+00, float* %ref.tmp, align 4
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %faceANormalWS, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %15 = load i32, i32* %curPlaneTests, align 4
  %inc17 = add nsw i32 %15, 1
  store i32 %inc17, i32* %curPlaneTests, align 4
  %16 = load i32, i32* @gExpectedNbTests, align 4
  %inc18 = add nsw i32 %16, 1
  store i32 %inc18, i32* @gExpectedNbTests, align 4
  %17 = load i8, i8* @gUseInternalObject, align 1
  %tobool = trunc i8 %17 to i1
  br i1 %tobool, label %land.lhs.true, label %if.end21

land.lhs.true:                                    ; preds = %if.end
  %18 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %19 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %20 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %21 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %22 = load float, float* %dmin, align 4
  %call19 = call zeroext i1 @_Z19TestInternalObjectsRK11btTransformS1_RK9btVector3S4_RK18btConvexPolyhedronS7_f(%class.btTransform* nonnull align 4 dereferenceable(64) %18, %class.btTransform* nonnull align 4 dereferenceable(64) %19, %class.btVector3* nonnull align 4 dereferenceable(16) %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %faceANormalWS, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %20, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %21, float %22)
  br i1 %call19, label %if.end21, label %if.then20

if.then20:                                        ; preds = %land.lhs.true
  br label %for.inc

if.end21:                                         ; preds = %land.lhs.true, %if.end
  %23 = load i32, i32* @gActualNbTests, align 4
  %inc22 = add nsw i32 %23, 1
  store i32 %inc22, i32* @gActualNbTests, align 4
  %call23 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %wA)
  %call24 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %wB)
  %24 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %25 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %26 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %27 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call25 = call zeroext i1 @_ZL11TestSepAxisRK18btConvexPolyhedronS1_RK11btTransformS4_RK9btVector3RfRS5_S9_(%class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %24, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %25, %class.btTransform* nonnull align 4 dereferenceable(64) %26, %class.btTransform* nonnull align 4 dereferenceable(64) %27, %class.btVector3* nonnull align 4 dereferenceable(16) %faceANormalWS, float* nonnull align 4 dereferenceable(4) %d, %class.btVector3* nonnull align 4 dereferenceable(16) %wA, %class.btVector3* nonnull align 4 dereferenceable(16) %wB)
  br i1 %call25, label %if.end27, label %if.then26

if.then26:                                        ; preds = %if.end21
  store i1 false, i1* %retval, align 1
  br label %return

if.end27:                                         ; preds = %if.end21
  %28 = load float, float* %d, align 4
  %29 = load float, float* %dmin, align 4
  %cmp28 = fcmp olt float %28, %29
  br i1 %cmp28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.end27
  %30 = load float, float* %d, align 4
  store float %30, float* %dmin, align 4
  %31 = load %class.btVector3*, %class.btVector3** %sep.addr, align 4
  %32 = bitcast %class.btVector3* %31 to i8*
  %33 = bitcast %class.btVector3* %faceANormalWS to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 16, i1 false)
  br label %if.end30

if.end30:                                         ; preds = %if.then29, %if.end27
  br label %for.inc

for.inc:                                          ; preds = %if.end30, %if.then20
  %34 = load i32, i32* %i, align 4
  %inc31 = add nsw i32 %34, 1
  store i32 %inc31, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %35 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %m_faces32 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %35, i32 0, i32 2
  %call33 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces32)
  store i32 %call33, i32* %numFacesB, align 4
  store i32 0, i32* %i34, align 4
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc78, %for.end
  %36 = load i32, i32* %i34, align 4
  %37 = load i32, i32* %numFacesB, align 4
  %cmp36 = icmp slt i32 %36, %37
  br i1 %cmp36, label %for.body37, label %for.end80

for.body37:                                       ; preds = %for.cond35
  %38 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %m_faces39 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %38, i32 0, i32 2
  %39 = load i32, i32* %i34, align 4
  %call40 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces39, i32 %39)
  %m_plane41 = getelementptr inbounds %struct.btFace, %struct.btFace* %call40, i32 0, i32 1
  %arrayidx42 = getelementptr inbounds [4 x float], [4 x float]* %m_plane41, i32 0, i32 0
  %40 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %m_faces43 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %40, i32 0, i32 2
  %41 = load i32, i32* %i34, align 4
  %call44 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces43, i32 %41)
  %m_plane45 = getelementptr inbounds %struct.btFace, %struct.btFace* %call44, i32 0, i32 1
  %arrayidx46 = getelementptr inbounds [4 x float], [4 x float]* %m_plane45, i32 0, i32 1
  %42 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %m_faces47 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %42, i32 0, i32 2
  %43 = load i32, i32* %i34, align 4
  %call48 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces47, i32 %43)
  %m_plane49 = getelementptr inbounds %struct.btFace, %struct.btFace* %call48, i32 0, i32 1
  %arrayidx50 = getelementptr inbounds [4 x float], [4 x float]* %m_plane49, i32 0, i32 2
  %call51 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %Normal38, float* nonnull align 4 dereferenceable(4) %arrayidx42, float* nonnull align 4 dereferenceable(4) %arrayidx46, float* nonnull align 4 dereferenceable(4) %arrayidx50)
  %44 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call52 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %44)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %WorldNormal, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call52, %class.btVector3* nonnull align 4 dereferenceable(16) %Normal38)
  %call53 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %WorldNormal)
  %cmp54 = fcmp olt float %call53, 0.000000e+00
  br i1 %cmp54, label %if.then55, label %if.end58

if.then55:                                        ; preds = %for.body37
  store float -1.000000e+00, float* %ref.tmp56, align 4
  %call57 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %WorldNormal, float* nonnull align 4 dereferenceable(4) %ref.tmp56)
  br label %if.end58

if.end58:                                         ; preds = %if.then55, %for.body37
  %45 = load i32, i32* %curPlaneTests, align 4
  %inc59 = add nsw i32 %45, 1
  store i32 %inc59, i32* %curPlaneTests, align 4
  %46 = load i32, i32* @gExpectedNbTests, align 4
  %inc60 = add nsw i32 %46, 1
  store i32 %inc60, i32* @gExpectedNbTests, align 4
  %47 = load i8, i8* @gUseInternalObject, align 1
  %tobool61 = trunc i8 %47 to i1
  br i1 %tobool61, label %land.lhs.true62, label %if.end65

land.lhs.true62:                                  ; preds = %if.end58
  %48 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %49 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %50 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %51 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %52 = load float, float* %dmin, align 4
  %call63 = call zeroext i1 @_Z19TestInternalObjectsRK11btTransformS1_RK9btVector3S4_RK18btConvexPolyhedronS7_f(%class.btTransform* nonnull align 4 dereferenceable(64) %48, %class.btTransform* nonnull align 4 dereferenceable(64) %49, %class.btVector3* nonnull align 4 dereferenceable(16) %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %WorldNormal, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %50, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %51, float %52)
  br i1 %call63, label %if.end65, label %if.then64

if.then64:                                        ; preds = %land.lhs.true62
  br label %for.inc78

if.end65:                                         ; preds = %land.lhs.true62, %if.end58
  %53 = load i32, i32* @gActualNbTests, align 4
  %inc66 = add nsw i32 %53, 1
  store i32 %inc66, i32* @gActualNbTests, align 4
  %call69 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %wA68)
  %call71 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %wB70)
  %54 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %55 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %56 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %57 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call72 = call zeroext i1 @_ZL11TestSepAxisRK18btConvexPolyhedronS1_RK11btTransformS4_RK9btVector3RfRS5_S9_(%class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %54, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %55, %class.btTransform* nonnull align 4 dereferenceable(64) %56, %class.btTransform* nonnull align 4 dereferenceable(64) %57, %class.btVector3* nonnull align 4 dereferenceable(16) %WorldNormal, float* nonnull align 4 dereferenceable(4) %d67, %class.btVector3* nonnull align 4 dereferenceable(16) %wA68, %class.btVector3* nonnull align 4 dereferenceable(16) %wB70)
  br i1 %call72, label %if.end74, label %if.then73

if.then73:                                        ; preds = %if.end65
  store i1 false, i1* %retval, align 1
  br label %return

if.end74:                                         ; preds = %if.end65
  %58 = load float, float* %d67, align 4
  %59 = load float, float* %dmin, align 4
  %cmp75 = fcmp olt float %58, %59
  br i1 %cmp75, label %if.then76, label %if.end77

if.then76:                                        ; preds = %if.end74
  %60 = load float, float* %d67, align 4
  store float %60, float* %dmin, align 4
  %61 = load %class.btVector3*, %class.btVector3** %sep.addr, align 4
  %62 = bitcast %class.btVector3* %61 to i8*
  %63 = bitcast %class.btVector3* %WorldNormal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %62, i8* align 4 %63, i32 16, i1 false)
  br label %if.end77

if.end77:                                         ; preds = %if.then76, %if.end74
  br label %for.inc78

for.inc78:                                        ; preds = %if.end77, %if.then64
  %64 = load i32, i32* %i34, align 4
  %inc79 = add nsw i32 %64, 1
  store i32 %inc79, i32* %i34, align 4
  br label %for.cond35

for.end80:                                        ; preds = %for.cond35
  %call81 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %edgeAstart)
  %call82 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %edgeAend)
  %call83 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %edgeBstart)
  %call84 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %edgeBend)
  store i32 -1, i32* %edgeA, align 4
  store i32 -1, i32* %edgeB, align 4
  %call85 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %worldEdgeA)
  %call86 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %worldEdgeB)
  store float 0.000000e+00, float* %ref.tmp87, align 4
  store float 0.000000e+00, float* %ref.tmp88, align 4
  store float 0.000000e+00, float* %ref.tmp89, align 4
  %call90 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %witnessPointA, float* nonnull align 4 dereferenceable(4) %ref.tmp87, float* nonnull align 4 dereferenceable(4) %ref.tmp88, float* nonnull align 4 dereferenceable(4) %ref.tmp89)
  store float 0.000000e+00, float* %ref.tmp91, align 4
  store float 0.000000e+00, float* %ref.tmp92, align 4
  store float 0.000000e+00, float* %ref.tmp93, align 4
  %call94 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %witnessPointB, float* nonnull align 4 dereferenceable(4) %ref.tmp91, float* nonnull align 4 dereferenceable(4) %ref.tmp92, float* nonnull align 4 dereferenceable(4) %ref.tmp93)
  store i32 0, i32* %curEdgeEdge, align 4
  store i32 0, i32* %e0, align 4
  br label %for.cond95

for.cond95:                                       ; preds = %for.inc141, %for.end80
  %65 = load i32, i32* %e0, align 4
  %66 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %m_uniqueEdges = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %66, i32 0, i32 3
  %call96 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_uniqueEdges)
  %cmp97 = icmp slt i32 %65, %call96
  br i1 %cmp97, label %for.body98, label %for.end143

for.body98:                                       ; preds = %for.cond95
  %67 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %m_uniqueEdges99 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %67, i32 0, i32 3
  %68 = load i32, i32* %e0, align 4
  %call100 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_uniqueEdges99, i32 %68)
  %69 = bitcast %class.btVector3* %edge0 to i8*
  %70 = bitcast %class.btVector3* %call100 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %69, i8* align 4 %70, i32 16, i1 false)
  %71 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call101 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %71)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %WorldEdge0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call101, %class.btVector3* nonnull align 4 dereferenceable(16) %edge0)
  store i32 0, i32* %e1, align 4
  br label %for.cond102

for.cond102:                                      ; preds = %for.inc138, %for.body98
  %72 = load i32, i32* %e1, align 4
  %73 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %m_uniqueEdges103 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %73, i32 0, i32 3
  %call104 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_uniqueEdges103)
  %cmp105 = icmp slt i32 %72, %call104
  br i1 %cmp105, label %for.body106, label %for.end140

for.body106:                                      ; preds = %for.cond102
  %74 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %m_uniqueEdges107 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %74, i32 0, i32 3
  %75 = load i32, i32* %e1, align 4
  %call108 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_uniqueEdges107, i32 %75)
  %76 = bitcast %class.btVector3* %edge1 to i8*
  %77 = bitcast %class.btVector3* %call108 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %76, i8* align 4 %77, i32 16, i1 false)
  %78 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call109 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %78)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %WorldEdge1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call109, %class.btVector3* nonnull align 4 dereferenceable(16) %edge1)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %Cross, %class.btVector3* %WorldEdge0, %class.btVector3* nonnull align 4 dereferenceable(16) %WorldEdge1)
  %79 = load i32, i32* %curEdgeEdge, align 4
  %inc110 = add nsw i32 %79, 1
  store i32 %inc110, i32* %curEdgeEdge, align 4
  %call111 = call zeroext i1 @_Z12IsAlmostZeroRK9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %Cross)
  br i1 %call111, label %if.end137, label %if.then112

if.then112:                                       ; preds = %for.body106
  %call113 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %Cross)
  %80 = bitcast %class.btVector3* %Cross to i8*
  %81 = bitcast %class.btVector3* %call113 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %80, i8* align 4 %81, i32 16, i1 false)
  %call114 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %Cross)
  %cmp115 = fcmp olt float %call114, 0.000000e+00
  br i1 %cmp115, label %if.then116, label %if.end119

if.then116:                                       ; preds = %if.then112
  store float -1.000000e+00, float* %ref.tmp117, align 4
  %call118 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %Cross, float* nonnull align 4 dereferenceable(4) %ref.tmp117)
  br label %if.end119

if.end119:                                        ; preds = %if.then116, %if.then112
  %82 = load i32, i32* @gExpectedNbTests, align 4
  %inc120 = add nsw i32 %82, 1
  store i32 %inc120, i32* @gExpectedNbTests, align 4
  %83 = load i8, i8* @gUseInternalObject, align 1
  %tobool121 = trunc i8 %83 to i1
  br i1 %tobool121, label %land.lhs.true122, label %if.end125

land.lhs.true122:                                 ; preds = %if.end119
  %84 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %85 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %86 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %87 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %88 = load float, float* %dmin, align 4
  %call123 = call zeroext i1 @_Z19TestInternalObjectsRK11btTransformS1_RK9btVector3S4_RK18btConvexPolyhedronS7_f(%class.btTransform* nonnull align 4 dereferenceable(64) %84, %class.btTransform* nonnull align 4 dereferenceable(64) %85, %class.btVector3* nonnull align 4 dereferenceable(16) %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %Cross, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %86, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %87, float %88)
  br i1 %call123, label %if.end125, label %if.then124

if.then124:                                       ; preds = %land.lhs.true122
  br label %for.inc138

if.end125:                                        ; preds = %land.lhs.true122, %if.end119
  %89 = load i32, i32* @gActualNbTests, align 4
  %inc126 = add nsw i32 %89, 1
  store i32 %inc126, i32* @gActualNbTests, align 4
  %call128 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %wA127)
  %call130 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %wB129)
  %90 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %91 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %92 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %93 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call131 = call zeroext i1 @_ZL11TestSepAxisRK18btConvexPolyhedronS1_RK11btTransformS4_RK9btVector3RfRS5_S9_(%class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %90, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %91, %class.btTransform* nonnull align 4 dereferenceable(64) %92, %class.btTransform* nonnull align 4 dereferenceable(64) %93, %class.btVector3* nonnull align 4 dereferenceable(16) %Cross, float* nonnull align 4 dereferenceable(4) %dist, %class.btVector3* nonnull align 4 dereferenceable(16) %wA127, %class.btVector3* nonnull align 4 dereferenceable(16) %wB129)
  br i1 %call131, label %if.end133, label %if.then132

if.then132:                                       ; preds = %if.end125
  store i1 false, i1* %retval, align 1
  br label %return

if.end133:                                        ; preds = %if.end125
  %94 = load float, float* %dist, align 4
  %95 = load float, float* %dmin, align 4
  %cmp134 = fcmp olt float %94, %95
  br i1 %cmp134, label %if.then135, label %if.end136

if.then135:                                       ; preds = %if.end133
  %96 = load float, float* %dist, align 4
  store float %96, float* %dmin, align 4
  %97 = load %class.btVector3*, %class.btVector3** %sep.addr, align 4
  %98 = bitcast %class.btVector3* %97 to i8*
  %99 = bitcast %class.btVector3* %Cross to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %98, i8* align 4 %99, i32 16, i1 false)
  %100 = load i32, i32* %e0, align 4
  store i32 %100, i32* %edgeA, align 4
  %101 = load i32, i32* %e1, align 4
  store i32 %101, i32* %edgeB, align 4
  %102 = bitcast %class.btVector3* %worldEdgeA to i8*
  %103 = bitcast %class.btVector3* %WorldEdge0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %102, i8* align 4 %103, i32 16, i1 false)
  %104 = bitcast %class.btVector3* %worldEdgeB to i8*
  %105 = bitcast %class.btVector3* %WorldEdge1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %104, i8* align 4 %105, i32 16, i1 false)
  %106 = bitcast %class.btVector3* %witnessPointA to i8*
  %107 = bitcast %class.btVector3* %wA127 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %106, i8* align 4 %107, i32 16, i1 false)
  %108 = bitcast %class.btVector3* %witnessPointB to i8*
  %109 = bitcast %class.btVector3* %wB129 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %108, i8* align 4 %109, i32 16, i1 false)
  br label %if.end136

if.end136:                                        ; preds = %if.then135, %if.end133
  br label %if.end137

if.end137:                                        ; preds = %if.end136, %for.body106
  br label %for.inc138

for.inc138:                                       ; preds = %if.end137, %if.then124
  %110 = load i32, i32* %e1, align 4
  %inc139 = add nsw i32 %110, 1
  store i32 %inc139, i32* %e1, align 4
  br label %for.cond102

for.end140:                                       ; preds = %for.cond102
  br label %for.inc141

for.inc141:                                       ; preds = %for.end140
  %111 = load i32, i32* %e0, align 4
  %inc142 = add nsw i32 %111, 1
  store i32 %inc142, i32* %e0, align 4
  br label %for.cond95

for.end143:                                       ; preds = %for.cond95
  %112 = load i32, i32* %edgeA, align 4
  %cmp144 = icmp sge i32 %112, 0
  br i1 %cmp144, label %land.lhs.true145, label %if.end164

land.lhs.true145:                                 ; preds = %for.end143
  %113 = load i32, i32* %edgeB, align 4
  %cmp146 = icmp sge i32 %113, 0
  br i1 %cmp146, label %if.then147, label %if.end164

if.then147:                                       ; preds = %land.lhs.true145
  %call148 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ptsVector)
  %call149 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %offsetA)
  %call150 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %offsetB)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %translation, %class.btVector3* nonnull align 4 dereferenceable(16) %witnessPointB, %class.btVector3* nonnull align 4 dereferenceable(16) %witnessPointA)
  %114 = bitcast %class.btVector3* %dirA to i8*
  %115 = bitcast %class.btVector3* %worldEdgeA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %114, i8* align 4 %115, i32 16, i1 false)
  %116 = bitcast %class.btVector3* %dirB to i8*
  %117 = bitcast %class.btVector3* %worldEdgeB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %116, i8* align 4 %117, i32 16, i1 false)
  store float 0x46293E5940000000, float* %hlenB, align 4
  store float 0x46293E5940000000, float* %hlenA, align 4
  %118 = load float, float* %hlenA, align 4
  %119 = load float, float* %hlenB, align 4
  call void @_Z23btSegmentsClosestPointsR9btVector3S0_S0_RfS1_RKS_S3_fS3_f(%class.btVector3* nonnull align 4 dereferenceable(16) %ptsVector, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetA, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetB, float* nonnull align 4 dereferenceable(4) %tA, float* nonnull align 4 dereferenceable(4) %tB, %class.btVector3* nonnull align 4 dereferenceable(16) %translation, %class.btVector3* nonnull align 4 dereferenceable(16) %dirA, float %118, %class.btVector3* nonnull align 4 dereferenceable(16) %dirB, float %119)
  %call151 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ptsVector)
  store float %call151, float* %nlSqrt, align 4
  %120 = load float, float* %nlSqrt, align 4
  %cmp152 = fcmp ogt float %120, 0x3E80000000000000
  br i1 %cmp152, label %if.then153, label %if.end163

if.then153:                                       ; preds = %if.then147
  %121 = load float, float* %nlSqrt, align 4
  %call154 = call float @_Z6btSqrtf(float %121)
  store float %call154, float* %nl, align 4
  %122 = load float, float* %nl, align 4
  %div = fdiv float 1.000000e+00, %122
  store float %div, float* %ref.tmp155, align 4
  %call156 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %ptsVector, float* nonnull align 4 dereferenceable(4) %ref.tmp155)
  %call157 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ptsVector, %class.btVector3* nonnull align 4 dereferenceable(16) %DeltaC2)
  %cmp158 = fcmp olt float %call157, 0.000000e+00
  br i1 %cmp158, label %if.then159, label %if.end162

if.then159:                                       ; preds = %if.then153
  store float -1.000000e+00, float* %ref.tmp160, align 4
  %call161 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %ptsVector, float* nonnull align 4 dereferenceable(4) %ref.tmp160)
  br label %if.end162

if.end162:                                        ; preds = %if.then159, %if.then153
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ptOnB, %class.btVector3* nonnull align 4 dereferenceable(16) %witnessPointB, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetB)
  %123 = load float, float* %nl, align 4
  store float %123, float* %distance, align 4
  %124 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %resultOut.addr, align 4
  %125 = load float, float* %distance, align 4
  %fneg = fneg float %125
  %126 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %124 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %126, align 4
  %vfn = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable, i64 4
  %127 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn, align 4
  call void %127(%"struct.btDiscreteCollisionDetectorInterface::Result"* %124, %class.btVector3* nonnull align 4 dereferenceable(16) %ptsVector, %class.btVector3* nonnull align 4 dereferenceable(16) %ptOnB, float %fneg)
  br label %if.end163

if.end163:                                        ; preds = %if.end162, %if.then147
  br label %if.end164

if.end164:                                        ; preds = %if.end163, %land.lhs.true145, %for.end143
  %128 = load %class.btVector3*, %class.btVector3** %sep.addr, align 4
  %call165 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %DeltaC2, %class.btVector3* nonnull align 4 dereferenceable(16) %128)
  %cmp166 = fcmp olt float %call165, 0.000000e+00
  br i1 %cmp166, label %if.then167, label %if.end169

if.then167:                                       ; preds = %if.end164
  %129 = load %class.btVector3*, %class.btVector3** %sep.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp168, %class.btVector3* nonnull align 4 dereferenceable(16) %129)
  %130 = load %class.btVector3*, %class.btVector3** %sep.addr, align 4
  %131 = bitcast %class.btVector3* %130 to i8*
  %132 = bitcast %class.btVector3* %ref.tmp168 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %131, i8* align 4 %132, i32 16, i1 false)
  br label %if.end169

if.end169:                                        ; preds = %if.then167, %if.end164
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end169, %if.then132, %if.then73, %if.then26
  %133 = load i1, i1* %retval, align 1
  ret i1 %133
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btFace*, %struct.btFace** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %0, i32 %1
  ret %struct.btFace* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define internal zeroext i1 @_ZL11TestSepAxisRK18btConvexPolyhedronS1_RK11btTransformS4_RK9btVector3RfRS5_S9_(%class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %hullA, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %hullB, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, %class.btVector3* nonnull align 4 dereferenceable(16) %sep_axis, float* nonnull align 4 dereferenceable(4) %depth, %class.btVector3* nonnull align 4 dereferenceable(16) %witnessPointA, %class.btVector3* nonnull align 4 dereferenceable(16) %witnessPointB) #2 {
entry:
  %retval = alloca i1, align 1
  %hullA.addr = alloca %class.btConvexPolyhedron*, align 4
  %hullB.addr = alloca %class.btConvexPolyhedron*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %sep_axis.addr = alloca %class.btVector3*, align 4
  %depth.addr = alloca float*, align 4
  %witnessPointA.addr = alloca %class.btVector3*, align 4
  %witnessPointB.addr = alloca %class.btVector3*, align 4
  %Min0 = alloca float, align 4
  %Max0 = alloca float, align 4
  %Min1 = alloca float, align 4
  %Max1 = alloca float, align 4
  %witnesPtMinA = alloca %class.btVector3, align 4
  %witnesPtMaxA = alloca %class.btVector3, align 4
  %witnesPtMinB = alloca %class.btVector3, align 4
  %witnesPtMaxB = alloca %class.btVector3, align 4
  %d0 = alloca float, align 4
  %d1 = alloca float, align 4
  store %class.btConvexPolyhedron* %hullA, %class.btConvexPolyhedron** %hullA.addr, align 4
  store %class.btConvexPolyhedron* %hullB, %class.btConvexPolyhedron** %hullB.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store %class.btVector3* %sep_axis, %class.btVector3** %sep_axis.addr, align 4
  store float* %depth, float** %depth.addr, align 4
  store %class.btVector3* %witnessPointA, %class.btVector3** %witnessPointA.addr, align 4
  store %class.btVector3* %witnessPointB, %class.btVector3** %witnessPointB.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %witnesPtMinA)
  %call1 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %witnesPtMaxA)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %witnesPtMinB)
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %witnesPtMaxB)
  %0 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %sep_axis.addr, align 4
  call void @_ZNK18btConvexPolyhedron7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexPolyhedron* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, float* nonnull align 4 dereferenceable(4) %Min0, float* nonnull align 4 dereferenceable(4) %Max0, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMinA, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMaxA)
  %3 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %4 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %sep_axis.addr, align 4
  call void @_ZNK18btConvexPolyhedron7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexPolyhedron* %3, %class.btTransform* nonnull align 4 dereferenceable(64) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5, float* nonnull align 4 dereferenceable(4) %Min1, float* nonnull align 4 dereferenceable(4) %Max1, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMinB, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMaxB)
  %6 = load float, float* %Max0, align 4
  %7 = load float, float* %Min1, align 4
  %cmp = fcmp olt float %6, %7
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %8 = load float, float* %Max1, align 4
  %9 = load float, float* %Min0, align 4
  %cmp4 = fcmp olt float %8, %9
  br i1 %cmp4, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %10 = load float, float* %Max0, align 4
  %11 = load float, float* %Min1, align 4
  %sub = fsub float %10, %11
  store float %sub, float* %d0, align 4
  %12 = load float, float* %Max1, align 4
  %13 = load float, float* %Min0, align 4
  %sub5 = fsub float %12, %13
  store float %sub5, float* %d1, align 4
  %14 = load float, float* %d0, align 4
  %15 = load float, float* %d1, align 4
  %cmp6 = fcmp olt float %14, %15
  br i1 %cmp6, label %if.then7, label %if.else

if.then7:                                         ; preds = %if.end
  %16 = load float, float* %d0, align 4
  %17 = load float*, float** %depth.addr, align 4
  store float %16, float* %17, align 4
  %18 = load %class.btVector3*, %class.btVector3** %witnessPointA.addr, align 4
  %19 = bitcast %class.btVector3* %18 to i8*
  %20 = bitcast %class.btVector3* %witnesPtMaxA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  %21 = load %class.btVector3*, %class.btVector3** %witnessPointB.addr, align 4
  %22 = bitcast %class.btVector3* %21 to i8*
  %23 = bitcast %class.btVector3* %witnesPtMinB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false)
  br label %if.end8

if.else:                                          ; preds = %if.end
  %24 = load float, float* %d1, align 4
  %25 = load float*, float** %depth.addr, align 4
  store float %24, float* %25, align 4
  %26 = load %class.btVector3*, %class.btVector3** %witnessPointA.addr, align 4
  %27 = bitcast %class.btVector3* %26 to i8*
  %28 = bitcast %class.btVector3* %witnesPtMinA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false)
  %29 = load %class.btVector3*, %class.btVector3** %witnessPointB.addr, align 4
  %30 = bitcast %class.btVector3* %29 to i8*
  %31 = bitcast %class.btVector3* %witnesPtMaxB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 16, i1 false)
  br label %if.end8

if.end8:                                          ; preds = %if.else, %if.then7
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end8, %if.then
  %32 = load i1, i1* %retval, align 1
  ret i1 %32
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_Z12IsAlmostZeroRK9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4
  %call1 = call float @_Z6btFabsf(float %1)
  %conv = fpext float %call1 to double
  %cmp = fcmp ogt double %conv, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %2)
  %3 = load float, float* %call2, align 4
  %call3 = call float @_Z6btFabsf(float %3)
  %conv4 = fpext float %call3 to double
  %cmp5 = fcmp ogt double %conv4, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp5, label %if.then, label %lor.lhs.false6

lor.lhs.false6:                                   ; preds = %lor.lhs.false
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %4)
  %5 = load float, float* %call7, align 4
  %call8 = call float @_Z6btFabsf(float %5)
  %conv9 = fpext float %call8 to double
  %cmp10 = fcmp ogt double %conv9, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp10, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false6, %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false6
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z23btSegmentsClosestPointsR9btVector3S0_S0_RfS1_RKS_S3_fS3_f(%class.btVector3* nonnull align 4 dereferenceable(16) %ptsVector, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetA, %class.btVector3* nonnull align 4 dereferenceable(16) %offsetB, float* nonnull align 4 dereferenceable(4) %tA, float* nonnull align 4 dereferenceable(4) %tB, %class.btVector3* nonnull align 4 dereferenceable(16) %translation, %class.btVector3* nonnull align 4 dereferenceable(16) %dirA, float %hlenA, %class.btVector3* nonnull align 4 dereferenceable(16) %dirB, float %hlenB) #2 comdat {
entry:
  %ptsVector.addr = alloca %class.btVector3*, align 4
  %offsetA.addr = alloca %class.btVector3*, align 4
  %offsetB.addr = alloca %class.btVector3*, align 4
  %tA.addr = alloca float*, align 4
  %tB.addr = alloca float*, align 4
  %translation.addr = alloca %class.btVector3*, align 4
  %dirA.addr = alloca %class.btVector3*, align 4
  %hlenA.addr = alloca float, align 4
  %dirB.addr = alloca %class.btVector3*, align 4
  %hlenB.addr = alloca float, align 4
  %dirA_dot_dirB = alloca float, align 4
  %dirA_dot_trans = alloca float, align 4
  %dirB_dot_trans = alloca float, align 4
  %denom = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp45 = alloca %class.btVector3, align 4
  %ref.tmp46 = alloca %class.btVector3, align 4
  %ref.tmp47 = alloca %class.btVector3, align 4
  store %class.btVector3* %ptsVector, %class.btVector3** %ptsVector.addr, align 4
  store %class.btVector3* %offsetA, %class.btVector3** %offsetA.addr, align 4
  store %class.btVector3* %offsetB, %class.btVector3** %offsetB.addr, align 4
  store float* %tA, float** %tA.addr, align 4
  store float* %tB, float** %tB.addr, align 4
  store %class.btVector3* %translation, %class.btVector3** %translation.addr, align 4
  store %class.btVector3* %dirA, %class.btVector3** %dirA.addr, align 4
  store float %hlenA, float* %hlenA.addr, align 4
  store %class.btVector3* %dirB, %class.btVector3** %dirB.addr, align 4
  store float %hlenB, float* %hlenB.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %dirA.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %dirB.addr, align 4
  %call = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %dirA_dot_dirB, align 4
  %2 = load %class.btVector3*, %class.btVector3** %dirA.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %translation.addr, align 4
  %call1 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call1, float* %dirA_dot_trans, align 4
  %4 = load %class.btVector3*, %class.btVector3** %dirB.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %translation.addr, align 4
  %call2 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call2, float* %dirB_dot_trans, align 4
  %6 = load float, float* %dirA_dot_dirB, align 4
  %7 = load float, float* %dirA_dot_dirB, align 4
  %mul = fmul float %6, %7
  %sub = fsub float 1.000000e+00, %mul
  store float %sub, float* %denom, align 4
  %8 = load float, float* %denom, align 4
  %cmp = fcmp oeq float %8, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %9 = load float*, float** %tA.addr, align 4
  store float 0.000000e+00, float* %9, align 4
  br label %if.end12

if.else:                                          ; preds = %entry
  %10 = load float, float* %dirA_dot_trans, align 4
  %11 = load float, float* %dirB_dot_trans, align 4
  %12 = load float, float* %dirA_dot_dirB, align 4
  %mul3 = fmul float %11, %12
  %sub4 = fsub float %10, %mul3
  %13 = load float, float* %denom, align 4
  %div = fdiv float %sub4, %13
  %14 = load float*, float** %tA.addr, align 4
  store float %div, float* %14, align 4
  %15 = load float*, float** %tA.addr, align 4
  %16 = load float, float* %15, align 4
  %17 = load float, float* %hlenA.addr, align 4
  %fneg = fneg float %17
  %cmp5 = fcmp olt float %16, %fneg
  br i1 %cmp5, label %if.then6, label %if.else8

if.then6:                                         ; preds = %if.else
  %18 = load float, float* %hlenA.addr, align 4
  %fneg7 = fneg float %18
  %19 = load float*, float** %tA.addr, align 4
  store float %fneg7, float* %19, align 4
  br label %if.end11

if.else8:                                         ; preds = %if.else
  %20 = load float*, float** %tA.addr, align 4
  %21 = load float, float* %20, align 4
  %22 = load float, float* %hlenA.addr, align 4
  %cmp9 = fcmp ogt float %21, %22
  br i1 %cmp9, label %if.then10, label %if.end

if.then10:                                        ; preds = %if.else8
  %23 = load float, float* %hlenA.addr, align 4
  %24 = load float*, float** %tA.addr, align 4
  store float %23, float* %24, align 4
  br label %if.end

if.end:                                           ; preds = %if.then10, %if.else8
  br label %if.end11

if.end11:                                         ; preds = %if.end, %if.then6
  br label %if.end12

if.end12:                                         ; preds = %if.end11, %if.then
  %25 = load float*, float** %tA.addr, align 4
  %26 = load float, float* %25, align 4
  %27 = load float, float* %dirA_dot_dirB, align 4
  %mul13 = fmul float %26, %27
  %28 = load float, float* %dirB_dot_trans, align 4
  %sub14 = fsub float %mul13, %28
  %29 = load float*, float** %tB.addr, align 4
  store float %sub14, float* %29, align 4
  %30 = load float*, float** %tB.addr, align 4
  %31 = load float, float* %30, align 4
  %32 = load float, float* %hlenB.addr, align 4
  %fneg15 = fneg float %32
  %cmp16 = fcmp olt float %31, %fneg15
  br i1 %cmp16, label %if.then17, label %if.else29

if.then17:                                        ; preds = %if.end12
  %33 = load float, float* %hlenB.addr, align 4
  %fneg18 = fneg float %33
  %34 = load float*, float** %tB.addr, align 4
  store float %fneg18, float* %34, align 4
  %35 = load float*, float** %tB.addr, align 4
  %36 = load float, float* %35, align 4
  %37 = load float, float* %dirA_dot_dirB, align 4
  %mul19 = fmul float %36, %37
  %38 = load float, float* %dirA_dot_trans, align 4
  %add = fadd float %mul19, %38
  %39 = load float*, float** %tA.addr, align 4
  store float %add, float* %39, align 4
  %40 = load float*, float** %tA.addr, align 4
  %41 = load float, float* %40, align 4
  %42 = load float, float* %hlenA.addr, align 4
  %fneg20 = fneg float %42
  %cmp21 = fcmp olt float %41, %fneg20
  br i1 %cmp21, label %if.then22, label %if.else24

if.then22:                                        ; preds = %if.then17
  %43 = load float, float* %hlenA.addr, align 4
  %fneg23 = fneg float %43
  %44 = load float*, float** %tA.addr, align 4
  store float %fneg23, float* %44, align 4
  br label %if.end28

if.else24:                                        ; preds = %if.then17
  %45 = load float*, float** %tA.addr, align 4
  %46 = load float, float* %45, align 4
  %47 = load float, float* %hlenA.addr, align 4
  %cmp25 = fcmp ogt float %46, %47
  br i1 %cmp25, label %if.then26, label %if.end27

if.then26:                                        ; preds = %if.else24
  %48 = load float, float* %hlenA.addr, align 4
  %49 = load float*, float** %tA.addr, align 4
  store float %48, float* %49, align 4
  br label %if.end27

if.end27:                                         ; preds = %if.then26, %if.else24
  br label %if.end28

if.end28:                                         ; preds = %if.end27, %if.then22
  br label %if.end44

if.else29:                                        ; preds = %if.end12
  %50 = load float*, float** %tB.addr, align 4
  %51 = load float, float* %50, align 4
  %52 = load float, float* %hlenB.addr, align 4
  %cmp30 = fcmp ogt float %51, %52
  br i1 %cmp30, label %if.then31, label %if.end43

if.then31:                                        ; preds = %if.else29
  %53 = load float, float* %hlenB.addr, align 4
  %54 = load float*, float** %tB.addr, align 4
  store float %53, float* %54, align 4
  %55 = load float*, float** %tB.addr, align 4
  %56 = load float, float* %55, align 4
  %57 = load float, float* %dirA_dot_dirB, align 4
  %mul32 = fmul float %56, %57
  %58 = load float, float* %dirA_dot_trans, align 4
  %add33 = fadd float %mul32, %58
  %59 = load float*, float** %tA.addr, align 4
  store float %add33, float* %59, align 4
  %60 = load float*, float** %tA.addr, align 4
  %61 = load float, float* %60, align 4
  %62 = load float, float* %hlenA.addr, align 4
  %fneg34 = fneg float %62
  %cmp35 = fcmp olt float %61, %fneg34
  br i1 %cmp35, label %if.then36, label %if.else38

if.then36:                                        ; preds = %if.then31
  %63 = load float, float* %hlenA.addr, align 4
  %fneg37 = fneg float %63
  %64 = load float*, float** %tA.addr, align 4
  store float %fneg37, float* %64, align 4
  br label %if.end42

if.else38:                                        ; preds = %if.then31
  %65 = load float*, float** %tA.addr, align 4
  %66 = load float, float* %65, align 4
  %67 = load float, float* %hlenA.addr, align 4
  %cmp39 = fcmp ogt float %66, %67
  br i1 %cmp39, label %if.then40, label %if.end41

if.then40:                                        ; preds = %if.else38
  %68 = load float, float* %hlenA.addr, align 4
  %69 = load float*, float** %tA.addr, align 4
  store float %68, float* %69, align 4
  br label %if.end41

if.end41:                                         ; preds = %if.then40, %if.else38
  br label %if.end42

if.end42:                                         ; preds = %if.end41, %if.then36
  br label %if.end43

if.end43:                                         ; preds = %if.end42, %if.else29
  br label %if.end44

if.end44:                                         ; preds = %if.end43, %if.end28
  %70 = load %class.btVector3*, %class.btVector3** %dirA.addr, align 4
  %71 = load float*, float** %tA.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %70, float* nonnull align 4 dereferenceable(4) %71)
  %72 = load %class.btVector3*, %class.btVector3** %offsetA.addr, align 4
  %73 = bitcast %class.btVector3* %72 to i8*
  %74 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %73, i8* align 4 %74, i32 16, i1 false)
  %75 = load %class.btVector3*, %class.btVector3** %dirB.addr, align 4
  %76 = load float*, float** %tB.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp45, %class.btVector3* nonnull align 4 dereferenceable(16) %75, float* nonnull align 4 dereferenceable(4) %76)
  %77 = load %class.btVector3*, %class.btVector3** %offsetB.addr, align 4
  %78 = bitcast %class.btVector3* %77 to i8*
  %79 = bitcast %class.btVector3* %ref.tmp45 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %78, i8* align 4 %79, i32 16, i1 false)
  %80 = load %class.btVector3*, %class.btVector3** %translation.addr, align 4
  %81 = load %class.btVector3*, %class.btVector3** %offsetA.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp47, %class.btVector3* nonnull align 4 dereferenceable(16) %80, %class.btVector3* nonnull align 4 dereferenceable(16) %81)
  %82 = load %class.btVector3*, %class.btVector3** %offsetB.addr, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp46, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp47, %class.btVector3* nonnull align 4 dereferenceable(16) %82)
  %83 = load %class.btVector3*, %class.btVector3** %ptsVector.addr, align 4
  %84 = bitcast %class.btVector3* %83 to i8*
  %85 = bitcast %class.btVector3* %ref.tmp46 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %84, i8* align 4 %85, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN27btPolyhedralContactClipping19clipFaceAgainstHullERK9btVector3RK18btConvexPolyhedronRK11btTransformR20btAlignedObjectArrayIS0_ESB_ffRN36btDiscreteCollisionDetectorInterface6ResultE(%class.btVector3* nonnull align 4 dereferenceable(16) %separatingNormal, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %hullA, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %worldVertsB1, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %worldVertsB2, float %minDist, float %maxDist, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %resultOut) #2 {
entry:
  %separatingNormal.addr = alloca %class.btVector3*, align 4
  %hullA.addr = alloca %class.btConvexPolyhedron*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %worldVertsB1.addr = alloca %class.btAlignedObjectArray*, align 4
  %worldVertsB2.addr = alloca %class.btAlignedObjectArray*, align 4
  %minDist.addr = alloca float, align 4
  %maxDist.addr = alloca float, align 4
  %resultOut.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %pVtxIn = alloca %class.btAlignedObjectArray*, align 4
  %pVtxOut = alloca %class.btAlignedObjectArray*, align 4
  %closestFaceA = alloca i32, align 4
  %dmin = alloca float, align 4
  %face = alloca i32, align 4
  %Normal = alloca %class.btVector3, align 4
  %faceANormalWS = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %polyA = alloca %struct.btFace*, align 4
  %numVerticesA = alloca i32, align 4
  %e0 = alloca i32, align 4
  %a = alloca %class.btVector3*, align 4
  %b = alloca %class.btVector3*, align 4
  %edge0 = alloca %class.btVector3, align 4
  %WorldEdge0 = alloca %class.btVector3, align 4
  %worldPlaneAnormal1 = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca %class.btVector3, align 4
  %planeNormalWS1 = alloca %class.btVector3, align 4
  %ref.tmp43 = alloca %class.btVector3, align 4
  %worldA1 = alloca %class.btVector3, align 4
  %planeEqWS1 = alloca float, align 4
  %planeNormalWS = alloca %class.btVector3, align 4
  %planeEqWS = alloca float, align 4
  %ref.tmp45 = alloca %class.btVector3, align 4
  %point = alloca %class.btVector3, align 4
  %localPlaneNormal = alloca %class.btVector3, align 4
  %localPlaneEq = alloca float, align 4
  %planeNormalWS60 = alloca %class.btVector3, align 4
  %planeEqWS62 = alloca float, align 4
  %i = alloca i32, align 4
  %vtx = alloca %class.btVector3, align 4
  %depth = alloca float, align 4
  %point77 = alloca %class.btVector3, align 4
  store %class.btVector3* %separatingNormal, %class.btVector3** %separatingNormal.addr, align 4
  store %class.btConvexPolyhedron* %hullA, %class.btConvexPolyhedron** %hullA.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btAlignedObjectArray* %worldVertsB1, %class.btAlignedObjectArray** %worldVertsB1.addr, align 4
  store %class.btAlignedObjectArray* %worldVertsB2, %class.btAlignedObjectArray** %worldVertsB2.addr, align 4
  store float %minDist, float* %minDist.addr, align 4
  store float %maxDist, float* %maxDist.addr, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %resultOut, %"struct.btDiscreteCollisionDetectorInterface::Result"** %resultOut.addr, align 4
  %0 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %worldVertsB2.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %0, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %worldVertsB1.addr, align 4
  store %class.btAlignedObjectArray* %1, %class.btAlignedObjectArray** %pVtxIn, align 4
  %2 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %worldVertsB2.addr, align 4
  store %class.btAlignedObjectArray* %2, %class.btAlignedObjectArray** %pVtxOut, align 4
  %3 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxOut, align 4
  %4 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn, align 4
  %call1 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %3, i32 %call1)
  store i32 -1, i32* %closestFaceA, align 4
  store float 0x47EFFFFFE0000000, float* %dmin, align 4
  store i32 0, i32* %face, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %5 = load i32, i32* %face, align 4
  %6 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %6, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces)
  %cmp = icmp slt i32 %5, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %7 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %m_faces3 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %7, i32 0, i32 2
  %8 = load i32, i32* %face, align 4
  %call4 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces3, i32 %8)
  %m_plane = getelementptr inbounds %struct.btFace, %struct.btFace* %call4, i32 0, i32 1
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_plane, i32 0, i32 0
  %9 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %m_faces5 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %9, i32 0, i32 2
  %10 = load i32, i32* %face, align 4
  %call6 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces5, i32 %10)
  %m_plane7 = getelementptr inbounds %struct.btFace, %struct.btFace* %call6, i32 0, i32 1
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_plane7, i32 0, i32 1
  %11 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %m_faces9 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %11, i32 0, i32 2
  %12 = load i32, i32* %face, align 4
  %call10 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces9, i32 %12)
  %m_plane11 = getelementptr inbounds %struct.btFace, %struct.btFace* %call10, i32 0, i32 1
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_plane11, i32 0, i32 2
  %call13 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %Normal, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx8, float* nonnull align 4 dereferenceable(4) %arrayidx12)
  %13 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call14 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %13)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %faceANormalWS, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call14, %class.btVector3* nonnull align 4 dereferenceable(16) %Normal)
  %14 = load %class.btVector3*, %class.btVector3** %separatingNormal.addr, align 4
  %call15 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %faceANormalWS, %class.btVector3* nonnull align 4 dereferenceable(16) %14)
  store float %call15, float* %d, align 4
  %15 = load float, float* %d, align 4
  %16 = load float, float* %dmin, align 4
  %cmp16 = fcmp olt float %15, %16
  br i1 %cmp16, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %17 = load float, float* %d, align 4
  store float %17, float* %dmin, align 4
  %18 = load i32, i32* %face, align 4
  store i32 %18, i32* %closestFaceA, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %19 = load i32, i32* %face, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %face, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %20 = load i32, i32* %closestFaceA, align 4
  %cmp17 = icmp slt i32 %20, 0
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %for.end
  br label %for.end82

if.end19:                                         ; preds = %for.end
  %21 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %m_faces20 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %21, i32 0, i32 2
  %22 = load i32, i32* %closestFaceA, align 4
  %call21 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces20, i32 %22)
  store %struct.btFace* %call21, %struct.btFace** %polyA, align 4
  %23 = load %struct.btFace*, %struct.btFace** %polyA, align 4
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %23, i32 0, i32 0
  %call22 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_indices)
  store i32 %call22, i32* %numVerticesA, align 4
  store i32 0, i32* %e0, align 4
  br label %for.cond23

for.cond23:                                       ; preds = %for.inc47, %if.end19
  %24 = load i32, i32* %e0, align 4
  %25 = load i32, i32* %numVerticesA, align 4
  %cmp24 = icmp slt i32 %24, %25
  br i1 %cmp24, label %for.body25, label %for.end49

for.body25:                                       ; preds = %for.cond23
  %26 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %m_vertices = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %26, i32 0, i32 1
  %27 = load %struct.btFace*, %struct.btFace** %polyA, align 4
  %m_indices26 = getelementptr inbounds %struct.btFace, %struct.btFace* %27, i32 0, i32 0
  %28 = load i32, i32* %e0, align 4
  %call27 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices26, i32 %28)
  %29 = load i32, i32* %call27, align 4
  %call28 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices, i32 %29)
  store %class.btVector3* %call28, %class.btVector3** %a, align 4
  %30 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %m_vertices29 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %30, i32 0, i32 1
  %31 = load %struct.btFace*, %struct.btFace** %polyA, align 4
  %m_indices30 = getelementptr inbounds %struct.btFace, %struct.btFace* %31, i32 0, i32 0
  %32 = load i32, i32* %e0, align 4
  %add = add nsw i32 %32, 1
  %33 = load i32, i32* %numVerticesA, align 4
  %rem = srem i32 %add, %33
  %call31 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices30, i32 %rem)
  %34 = load i32, i32* %call31, align 4
  %call32 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices29, i32 %34)
  store %class.btVector3* %call32, %class.btVector3** %b, align 4
  %35 = load %class.btVector3*, %class.btVector3** %a, align 4
  %36 = load %class.btVector3*, %class.btVector3** %b, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge0, %class.btVector3* nonnull align 4 dereferenceable(16) %35, %class.btVector3* nonnull align 4 dereferenceable(16) %36)
  %37 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call33 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %37)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %WorldEdge0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call33, %class.btVector3* nonnull align 4 dereferenceable(16) %edge0)
  %38 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call34 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %38)
  %39 = load %struct.btFace*, %struct.btFace** %polyA, align 4
  %m_plane36 = getelementptr inbounds %struct.btFace, %struct.btFace* %39, i32 0, i32 1
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %m_plane36, i32 0, i32 0
  %40 = load %struct.btFace*, %struct.btFace** %polyA, align 4
  %m_plane38 = getelementptr inbounds %struct.btFace, %struct.btFace* %40, i32 0, i32 1
  %arrayidx39 = getelementptr inbounds [4 x float], [4 x float]* %m_plane38, i32 0, i32 1
  %41 = load %struct.btFace*, %struct.btFace** %polyA, align 4
  %m_plane40 = getelementptr inbounds %struct.btFace, %struct.btFace* %41, i32 0, i32 1
  %arrayidx41 = getelementptr inbounds [4 x float], [4 x float]* %m_plane40, i32 0, i32 2
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp35, float* nonnull align 4 dereferenceable(4) %arrayidx37, float* nonnull align 4 dereferenceable(4) %arrayidx39, float* nonnull align 4 dereferenceable(4) %arrayidx41)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %worldPlaneAnormal1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call34, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp35)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp43, %class.btVector3* %WorldEdge0, %class.btVector3* nonnull align 4 dereferenceable(16) %worldPlaneAnormal1)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %planeNormalWS1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp43)
  %42 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %43 = load %class.btVector3*, %class.btVector3** %a, align 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %worldA1, %class.btTransform* %42, %class.btVector3* nonnull align 4 dereferenceable(16) %43)
  %call44 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %worldA1, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormalWS1)
  %fneg = fneg float %call44
  store float %fneg, float* %planeEqWS1, align 4
  %44 = bitcast %class.btVector3* %planeNormalWS to i8*
  %45 = bitcast %class.btVector3* %planeNormalWS1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %44, i8* align 4 %45, i32 16, i1 false)
  %46 = load float, float* %planeEqWS1, align 4
  store float %46, float* %planeEqWS, align 4
  %47 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn, align 4
  %48 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxOut, align 4
  %49 = load float, float* %planeEqWS, align 4
  call void @_ZN27btPolyhedralContactClipping8clipFaceERK20btAlignedObjectArrayI9btVector3ERS2_RKS1_f(%class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %47, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %48, %class.btVector3* nonnull align 4 dereferenceable(16) %planeNormalWS, float %49)
  call void @_Z6btSwapIP20btAlignedObjectArrayI9btVector3EEvRT_S5_(%class.btAlignedObjectArray** nonnull align 4 dereferenceable(4) %pVtxIn, %class.btAlignedObjectArray** nonnull align 4 dereferenceable(4) %pVtxOut)
  %50 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxOut, align 4
  %call46 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp45)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %50, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp45)
  br label %for.inc47

for.inc47:                                        ; preds = %for.body25
  %51 = load i32, i32* %e0, align 4
  %inc48 = add nsw i32 %51, 1
  store i32 %inc48, i32* %e0, align 4
  br label %for.cond23

for.end49:                                        ; preds = %for.cond23
  %call50 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %point)
  %52 = load %struct.btFace*, %struct.btFace** %polyA, align 4
  %m_plane51 = getelementptr inbounds %struct.btFace, %struct.btFace* %52, i32 0, i32 1
  %arrayidx52 = getelementptr inbounds [4 x float], [4 x float]* %m_plane51, i32 0, i32 0
  %53 = load %struct.btFace*, %struct.btFace** %polyA, align 4
  %m_plane53 = getelementptr inbounds %struct.btFace, %struct.btFace* %53, i32 0, i32 1
  %arrayidx54 = getelementptr inbounds [4 x float], [4 x float]* %m_plane53, i32 0, i32 1
  %54 = load %struct.btFace*, %struct.btFace** %polyA, align 4
  %m_plane55 = getelementptr inbounds %struct.btFace, %struct.btFace* %54, i32 0, i32 1
  %arrayidx56 = getelementptr inbounds [4 x float], [4 x float]* %m_plane55, i32 0, i32 2
  %call57 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %localPlaneNormal, float* nonnull align 4 dereferenceable(4) %arrayidx52, float* nonnull align 4 dereferenceable(4) %arrayidx54, float* nonnull align 4 dereferenceable(4) %arrayidx56)
  %55 = load %struct.btFace*, %struct.btFace** %polyA, align 4
  %m_plane58 = getelementptr inbounds %struct.btFace, %struct.btFace* %55, i32 0, i32 1
  %arrayidx59 = getelementptr inbounds [4 x float], [4 x float]* %m_plane58, i32 0, i32 3
  %56 = load float, float* %arrayidx59, align 4
  store float %56, float* %localPlaneEq, align 4
  %57 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call61 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %57)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %planeNormalWS60, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call61, %class.btVector3* nonnull align 4 dereferenceable(16) %localPlaneNormal)
  %58 = load float, float* %localPlaneEq, align 4
  %59 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %call63 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %59)
  %call64 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %planeNormalWS60, %class.btVector3* nonnull align 4 dereferenceable(16) %call63)
  %sub = fsub float %58, %call64
  store float %sub, float* %planeEqWS62, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond65

for.cond65:                                       ; preds = %for.inc80, %for.end49
  %60 = load i32, i32* %i, align 4
  %61 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn, align 4
  %call66 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %61)
  %cmp67 = icmp slt i32 %60, %call66
  br i1 %cmp67, label %for.body68, label %for.end82

for.body68:                                       ; preds = %for.cond65
  %62 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn, align 4
  %63 = load i32, i32* %i, align 4
  %call69 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3E2atEi(%class.btAlignedObjectArray* %62, i32 %63)
  %64 = bitcast %class.btVector3* %vtx to i8*
  %65 = bitcast %class.btVector3* %call69 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %64, i8* align 4 %65, i32 16, i1 false)
  %call70 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %planeNormalWS60, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx)
  %66 = load float, float* %planeEqWS62, align 4
  %add71 = fadd float %call70, %66
  store float %add71, float* %depth, align 4
  %67 = load float, float* %depth, align 4
  %68 = load float, float* %minDist.addr, align 4
  %cmp72 = fcmp ole float %67, %68
  br i1 %cmp72, label %if.then73, label %if.end74

if.then73:                                        ; preds = %for.body68
  %69 = load float, float* %minDist.addr, align 4
  store float %69, float* %depth, align 4
  br label %if.end74

if.end74:                                         ; preds = %if.then73, %for.body68
  %70 = load float, float* %depth, align 4
  %71 = load float, float* %maxDist.addr, align 4
  %cmp75 = fcmp ole float %70, %71
  br i1 %cmp75, label %if.then76, label %if.end79

if.then76:                                        ; preds = %if.end74
  %72 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %pVtxIn, align 4
  %73 = load i32, i32* %i, align 4
  %call78 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3E2atEi(%class.btAlignedObjectArray* %72, i32 %73)
  %74 = bitcast %class.btVector3* %point77 to i8*
  %75 = bitcast %class.btVector3* %call78 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %74, i8* align 4 %75, i32 16, i1 false)
  %76 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %resultOut.addr, align 4
  %77 = load %class.btVector3*, %class.btVector3** %separatingNormal.addr, align 4
  %78 = load float, float* %depth, align 4
  %79 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %76 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %79, align 4
  %vfn = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable, i64 4
  %80 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn, align 4
  call void %80(%"struct.btDiscreteCollisionDetectorInterface::Result"* %76, %class.btVector3* nonnull align 4 dereferenceable(16) %77, %class.btVector3* nonnull align 4 dereferenceable(16) %point77, float %78)
  br label %if.end79

if.end79:                                         ; preds = %if.then76, %if.end74
  br label %for.inc80

for.inc80:                                        ; preds = %if.end79
  %81 = load i32, i32* %i, align 4
  %inc81 = add nsw i32 %81, 1
  store i32 %inc81, i32* %i, align 4
  br label %for.cond65

for.end82:                                        ; preds = %if.then18, %for.cond65
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %class.btVector3*, %class.btVector3** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 %15
  %16 = bitcast %class.btVector3* %arrayidx10 to i8*
  %call11 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %16)
  %17 = bitcast i8* %call11 to %class.btVector3*
  %18 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4
  %19 = bitcast %class.btVector3* %17 to i8*
  %20 = bitcast %class.btVector3* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %2, %class.btVector3** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %class.btVector3*, %class.btVector3** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btVector3* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* %4, %class.btVector3** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z6btSwapIP20btAlignedObjectArrayI9btVector3EEvRT_S5_(%class.btAlignedObjectArray** nonnull align 4 dereferenceable(4) %a, %class.btAlignedObjectArray** nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca %class.btAlignedObjectArray**, align 4
  %b.addr = alloca %class.btAlignedObjectArray**, align 4
  %tmp = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray** %a, %class.btAlignedObjectArray*** %a.addr, align 4
  store %class.btAlignedObjectArray** %b, %class.btAlignedObjectArray*** %b.addr, align 4
  %0 = load %class.btAlignedObjectArray**, %class.btAlignedObjectArray*** %a.addr, align 4
  %1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %0, align 4
  store %class.btAlignedObjectArray* %1, %class.btAlignedObjectArray** %tmp, align 4
  %2 = load %class.btAlignedObjectArray**, %class.btAlignedObjectArray*** %b.addr, align 4
  %3 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %2, align 4
  %4 = load %class.btAlignedObjectArray**, %class.btAlignedObjectArray*** %a.addr, align 4
  store %class.btAlignedObjectArray* %3, %class.btAlignedObjectArray** %4, align 4
  %5 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %tmp, align 4
  %6 = load %class.btAlignedObjectArray**, %class.btAlignedObjectArray*** %b.addr, align 4
  store %class.btAlignedObjectArray* %5, %class.btAlignedObjectArray** %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3E2atEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN27btPolyhedralContactClipping19clipHullAgainstHullERK9btVector3RK18btConvexPolyhedronS5_RK11btTransformS8_ffR20btAlignedObjectArrayIS0_ESB_RN36btDiscreteCollisionDetectorInterface6ResultE(%class.btVector3* nonnull align 4 dereferenceable(16) %separatingNormal1, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %hullA, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %hullB, %class.btTransform* nonnull align 4 dereferenceable(64) %transA, %class.btTransform* nonnull align 4 dereferenceable(64) %transB, float %minDist, float %maxDist, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %worldVertsB1, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %worldVertsB2, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %resultOut) #2 {
entry:
  %separatingNormal1.addr = alloca %class.btVector3*, align 4
  %hullA.addr = alloca %class.btConvexPolyhedron*, align 4
  %hullB.addr = alloca %class.btConvexPolyhedron*, align 4
  %transA.addr = alloca %class.btTransform*, align 4
  %transB.addr = alloca %class.btTransform*, align 4
  %minDist.addr = alloca float, align 4
  %maxDist.addr = alloca float, align 4
  %worldVertsB1.addr = alloca %class.btAlignedObjectArray*, align 4
  %worldVertsB2.addr = alloca %class.btAlignedObjectArray*, align 4
  %resultOut.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %separatingNormal = alloca %class.btVector3, align 4
  %closestFaceB = alloca i32, align 4
  %dmax = alloca float, align 4
  %face = alloca i32, align 4
  %Normal = alloca %class.btVector3, align 4
  %WorldNormal = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %polyB = alloca %struct.btFace*, align 4
  %numVertices = alloca i32, align 4
  %e0 = alloca i32, align 4
  %b = alloca %class.btVector3*, align 4
  %ref.tmp25 = alloca %class.btVector3, align 4
  store %class.btVector3* %separatingNormal1, %class.btVector3** %separatingNormal1.addr, align 4
  store %class.btConvexPolyhedron* %hullA, %class.btConvexPolyhedron** %hullA.addr, align 4
  store %class.btConvexPolyhedron* %hullB, %class.btConvexPolyhedron** %hullB.addr, align 4
  store %class.btTransform* %transA, %class.btTransform** %transA.addr, align 4
  store %class.btTransform* %transB, %class.btTransform** %transB.addr, align 4
  store float %minDist, float* %minDist.addr, align 4
  store float %maxDist, float* %maxDist.addr, align 4
  store %class.btAlignedObjectArray* %worldVertsB1, %class.btAlignedObjectArray** %worldVertsB1.addr, align 4
  store %class.btAlignedObjectArray* %worldVertsB2, %class.btAlignedObjectArray** %worldVertsB2.addr, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %resultOut, %"struct.btDiscreteCollisionDetectorInterface::Result"** %resultOut.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %separatingNormal1.addr, align 4
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %separatingNormal, %class.btVector3* %0)
  store i32 -1, i32* %closestFaceB, align 4
  store float 0xC7EFFFFFE0000000, float* %dmax, align 4
  store i32 0, i32* %face, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %face, align 4
  %2 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %2, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %m_faces1 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %3, i32 0, i32 2
  %4 = load i32, i32* %face, align 4
  %call2 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces1, i32 %4)
  %m_plane = getelementptr inbounds %struct.btFace, %struct.btFace* %call2, i32 0, i32 1
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_plane, i32 0, i32 0
  %5 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %m_faces3 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %5, i32 0, i32 2
  %6 = load i32, i32* %face, align 4
  %call4 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces3, i32 %6)
  %m_plane5 = getelementptr inbounds %struct.btFace, %struct.btFace* %call4, i32 0, i32 1
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_plane5, i32 0, i32 1
  %7 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %m_faces7 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %7, i32 0, i32 2
  %8 = load i32, i32* %face, align 4
  %call8 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces7, i32 %8)
  %m_plane9 = getelementptr inbounds %struct.btFace, %struct.btFace* %call8, i32 0, i32 1
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_plane9, i32 0, i32 2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %Normal, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  %9 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %9)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %WorldNormal, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call12, %class.btVector3* nonnull align 4 dereferenceable(16) %Normal)
  %call13 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %WorldNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %separatingNormal)
  store float %call13, float* %d, align 4
  %10 = load float, float* %d, align 4
  %11 = load float, float* %dmax, align 4
  %cmp14 = fcmp ogt float %10, %11
  br i1 %cmp14, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %12 = load float, float* %d, align 4
  store float %12, float* %dmax, align 4
  %13 = load i32, i32* %face, align 4
  store i32 %13, i32* %closestFaceB, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %14 = load i32, i32* %face, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %face, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %15 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %worldVertsB1.addr, align 4
  %call15 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %15, i32 0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %16 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %m_faces16 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %16, i32 0, i32 2
  %17 = load i32, i32* %closestFaceB, align 4
  %call17 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces16, i32 %17)
  store %struct.btFace* %call17, %struct.btFace** %polyB, align 4
  %18 = load %struct.btFace*, %struct.btFace** %polyB, align 4
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %18, i32 0, i32 0
  %call18 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_indices)
  store i32 %call18, i32* %numVertices, align 4
  store i32 0, i32* %e0, align 4
  br label %for.cond19

for.cond19:                                       ; preds = %for.inc26, %for.end
  %19 = load i32, i32* %e0, align 4
  %20 = load i32, i32* %numVertices, align 4
  %cmp20 = icmp slt i32 %19, %20
  br i1 %cmp20, label %for.body21, label %for.end28

for.body21:                                       ; preds = %for.cond19
  %21 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullB.addr, align 4
  %m_vertices = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %21, i32 0, i32 1
  %22 = load %struct.btFace*, %struct.btFace** %polyB, align 4
  %m_indices22 = getelementptr inbounds %struct.btFace, %struct.btFace* %22, i32 0, i32 0
  %23 = load i32, i32* %e0, align 4
  %call23 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices22, i32 %23)
  %24 = load i32, i32* %call23, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices, i32 %24)
  store %class.btVector3* %call24, %class.btVector3** %b, align 4
  %25 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %worldVertsB1.addr, align 4
  %26 = load %class.btTransform*, %class.btTransform** %transB.addr, align 4
  %27 = load %class.btVector3*, %class.btVector3** %b, align 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp25, %class.btTransform* %26, %class.btVector3* nonnull align 4 dereferenceable(16) %27)
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %25, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp25)
  br label %for.inc26

for.inc26:                                        ; preds = %for.body21
  %28 = load i32, i32* %e0, align 4
  %inc27 = add nsw i32 %28, 1
  store i32 %inc27, i32* %e0, align 4
  br label %for.cond19

for.end28:                                        ; preds = %for.cond19
  %29 = load i32, i32* %closestFaceB, align 4
  %cmp29 = icmp sge i32 %29, 0
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %for.end28
  %30 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %hullA.addr, align 4
  %31 = load %class.btTransform*, %class.btTransform** %transA.addr, align 4
  %32 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %worldVertsB1.addr, align 4
  %33 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %worldVertsB2.addr, align 4
  %34 = load float, float* %minDist.addr, align 4
  %35 = load float, float* %maxDist.addr, align 4
  %36 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %resultOut.addr, align 4
  call void @_ZN27btPolyhedralContactClipping19clipFaceAgainstHullERK9btVector3RK18btConvexPolyhedronRK11btTransformR20btAlignedObjectArrayIS0_ESB_ffRN36btDiscreteCollisionDetectorInterface6ResultE(%class.btVector3* nonnull align 4 dereferenceable(16) %separatingNormal, %class.btConvexPolyhedron* nonnull align 4 dereferenceable(132) %30, %class.btTransform* nonnull align 4 dereferenceable(64) %31, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %32, %class.btAlignedObjectArray* nonnull align 4 dereferenceable(17) %33, float %34, float %35, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %36)
  br label %if.end31

if.end31:                                         ; preds = %if.then30, %for.end28
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %nrm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %nrm to i8*
  %1 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %nrm)
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

declare void @_ZNK18btConvexPolyhedron7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexPolyhedron*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #1 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btVector3* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %9 = bitcast %class.btVector3* %6 to i8*
  %10 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.btVector3** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.btVector3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btPolyhedralContactClipping.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind readnone speculatable willreturn }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
