; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/LinearMath/btConvexHull.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/LinearMath/btConvexHull.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.ConvexH = type { %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %"class.ConvexH::HalfEdge"*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%"class.ConvexH::HalfEdge" = type { i16, i8, i8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btPlane*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btPlane = type { %class.btVector3, float }
%class.int3 = type { i32, i32, i32 }
%class.btHullTriangle = type { %class.int3, %class.int3, i32, i32, float }
%class.HullLibrary = type { %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btHullTriangle**, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.int4 = type { i32, i32, i32, i32 }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.PHullResult = type { i32, i32, i32, %class.btVector3*, %class.btAlignedObjectArray.16 }
%class.HullDesc = type { i32, i32, %class.btVector3*, i32, float, i32, i32 }
%class.HullResult = type { i8, i32, %class.btAlignedObjectArray, i32, i32, %class.btAlignedObjectArray.16 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_Z5btDotRK9btVector3S1_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_Z7btCrossRK9btVector3S1_ = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_Z4fabsf = comdat any

$_ZN7btPlaneC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI7btPlaneEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE6resizeEiRKS1_ = comdat any

$_ZN7ConvexH8HalfEdgeC2Ev = comdat any

$_ZN20btAlignedObjectArrayI7btPlaneE6resizeEiRKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZNK4int3ixEi = comdat any

$_ZN4int3ixEi = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi = comdat any

$_ZN14btHullTriangleC2Eiii = comdat any

$_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE9push_backERKS1_ = comdat any

$_ZN14btHullTriangleD2Ev = comdat any

$_ZN4int3C2Eiii = comdat any

$_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK9btVector3eqERKS_ = comdat any

$_ZN4int4C2Eiiii = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_Z6btSwapIiEvRT_S1_ = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIiE9push_backERKi = comdat any

$_ZN9btVector36setMinERKS_ = comdat any

$_ZN9btVector36setMaxERKS_ = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZN4int4ixEi = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIjE6resizeEiRKj = comdat any

$_ZN20btAlignedObjectArrayIjEixEi = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE6resizeEiRKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIjE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIjE5clearEv = comdat any

$_ZN11PHullResultC2Ev = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZNK8HullDesc11HasHullFlagE8HullFlag = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZN11PHullResultD2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN20btAlignedObjectArrayIjEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIjED2Ev = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z8btSetMinIfEvRT_RKS0_ = comdat any

$_ZNK9btVector31wEv = comdat any

$_Z8btSetMaxIfEvRT_RKS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorIjLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIjE4initEv = comdat any

$_ZN18btAlignedAllocatorIN7ConvexH8HalfEdgeELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE4initEv = comdat any

$_ZN18btAlignedAllocatorI7btPlaneLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI7btPlaneE4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIN7ConvexH8HalfEdgeELj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIN7ConvexH8HalfEdgeELj16EE10deallocateEPS1_ = comdat any

$_ZNK20btAlignedObjectArrayI7btPlaneE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI7btPlaneE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayI7btPlaneE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI7btPlaneE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI7btPlaneE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI7btPlaneE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI7btPlaneE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI7btPlaneLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI7btPlaneLj16EE10deallocateEPS0_ = comdat any

$_ZNK20btAlignedObjectArrayIP14btHullTriangleE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP14btHullTriangleE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_ = comdat any

$_Z14maxdirfilteredI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE = comdat any

$_Z5btSinf = comdat any

$_Z5btCosf = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

$_ZN20btAlignedObjectArrayIiE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIjE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIjE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIjE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIjE4copyEiiPj = comdat any

$_ZN20btAlignedObjectArrayIjE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIjE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIjLj16EE8allocateEiPPKj = comdat any

$_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@planetestepsilon = hidden global float 0x3F50624DE0000000, align 4
@_ZZN14btHullTriangle4neibEiiE2er = internal global i32 -1, align 4
@__const._ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_.bmin = private unnamed_addr constant [3 x float] [float 0x47EFFFFFE0000000, float 0x47EFFFFFE0000000, float 0x47EFFFFFE0000000], align 4
@__const._ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_.bmax = private unnamed_addr constant [3 x float] [float 0xC7EFFFFFE0000000, float 0xC7EFFFFFE0000000, float 0xC7EFFFFFE0000000], align 4
@__const._ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_.bmin.1 = private unnamed_addr constant [3 x float] [float 0x47EFFFFFE0000000, float 0x47EFFFFFE0000000, float 0x47EFFFFFE0000000], align 4
@__const._ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_.bmax.2 = private unnamed_addr constant [3 x float] [float 0xC7EFFFFFE0000000, float 0xC7EFFFFFE0000000, float 0xC7EFFFFFE0000000], align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btConvexHull.cpp, i8* null }]

@_ZN7ConvexHC1Eiii = hidden unnamed_addr alias %class.ConvexH* (%class.ConvexH*, i32, i32, i32), %class.ConvexH* (%class.ConvexH*, i32, i32, i32)* @_ZN7ConvexHC2Eiii

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden void @_Z22ThreePlaneIntersectionRK7btPlaneS1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btPlane* nonnull align 4 dereferenceable(20) %p0, %class.btPlane* nonnull align 4 dereferenceable(20) %p1, %class.btPlane* nonnull align 4 dereferenceable(20) %p2) #2 {
entry:
  %p0.addr = alloca %class.btPlane*, align 4
  %p1.addr = alloca %class.btPlane*, align 4
  %p2.addr = alloca %class.btPlane*, align 4
  %N1 = alloca %class.btVector3, align 4
  %N2 = alloca %class.btVector3, align 4
  %N3 = alloca %class.btVector3, align 4
  %n2n3 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %n3n1 = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %n1n2 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %quotient = alloca float, align 4
  %potentialVertex = alloca %class.btVector3, align 4
  store %class.btPlane* %p0, %class.btPlane** %p0.addr, align 4
  store %class.btPlane* %p1, %class.btPlane** %p1.addr, align 4
  store %class.btPlane* %p2, %class.btPlane** %p2.addr, align 4
  %0 = load %class.btPlane*, %class.btPlane** %p0.addr, align 4
  %normal = getelementptr inbounds %class.btPlane, %class.btPlane* %0, i32 0, i32 0
  %1 = bitcast %class.btVector3* %N1 to i8*
  %2 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btPlane*, %class.btPlane** %p1.addr, align 4
  %normal1 = getelementptr inbounds %class.btPlane, %class.btPlane* %3, i32 0, i32 0
  %4 = bitcast %class.btVector3* %N2 to i8*
  %5 = bitcast %class.btVector3* %normal1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btPlane*, %class.btPlane** %p2.addr, align 4
  %normal2 = getelementptr inbounds %class.btPlane, %class.btPlane* %6, i32 0, i32 0
  %7 = bitcast %class.btVector3* %N3 to i8*
  %8 = bitcast %class.btVector3* %normal2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n2n3)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %N2, %class.btVector3* nonnull align 4 dereferenceable(16) %N3)
  %9 = bitcast %class.btVector3* %n2n3 to i8*
  %10 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n3n1)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* %N3, %class.btVector3* nonnull align 4 dereferenceable(16) %N1)
  %11 = bitcast %class.btVector3* %n3n1 to i8*
  %12 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false)
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n1n2)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* %N1, %class.btVector3* nonnull align 4 dereferenceable(16) %N2)
  %13 = bitcast %class.btVector3* %n1n2 to i8*
  %14 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false)
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %N1, %class.btVector3* nonnull align 4 dereferenceable(16) %n2n3)
  store float %call7, float* %quotient, align 4
  %15 = load float, float* %quotient, align 4
  %div = fdiv float -1.000000e+00, %15
  store float %div, float* %quotient, align 4
  %16 = load %class.btPlane*, %class.btPlane** %p0.addr, align 4
  %dist = getelementptr inbounds %class.btPlane, %class.btPlane* %16, i32 0, i32 1
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %n2n3, float* nonnull align 4 dereferenceable(4) %dist)
  %17 = load %class.btPlane*, %class.btPlane** %p1.addr, align 4
  %dist9 = getelementptr inbounds %class.btPlane, %class.btPlane* %17, i32 0, i32 1
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %n3n1, float* nonnull align 4 dereferenceable(4) %dist9)
  %18 = load %class.btPlane*, %class.btPlane** %p2.addr, align 4
  %dist11 = getelementptr inbounds %class.btPlane, %class.btPlane* %18, i32 0, i32 1
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %n1n2, float* nonnull align 4 dereferenceable(4) %dist11)
  %19 = bitcast %class.btVector3* %potentialVertex to i8*
  %20 = bitcast %class.btVector3* %n2n3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %potentialVertex, %class.btVector3* nonnull align 4 dereferenceable(16) %n3n1)
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %potentialVertex, %class.btVector3* nonnull align 4 dereferenceable(16) %n1n2)
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %potentialVertex, float* nonnull align 4 dereferenceable(4) %quotient)
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %potentialVertex)
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %potentialVertex)
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %potentialVertex)
  %call19 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call17, float* nonnull align 4 dereferenceable(4) %call18)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define hidden void @_Z21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_(%class.btVector3* noalias sret align 4 %agg.result, %class.btPlane* nonnull align 4 dereferenceable(20) %plane, %class.btVector3* nonnull align 4 dereferenceable(16) %p0, %class.btVector3* nonnull align 4 dereferenceable(16) %p1) #2 {
entry:
  %plane.addr = alloca %class.btPlane*, align 4
  %p0.addr = alloca %class.btVector3*, align 4
  %p1.addr = alloca %class.btVector3*, align 4
  %dif = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %dn = alloca float, align 4
  %t = alloca float, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  store %class.btPlane* %plane, %class.btPlane** %plane.addr, align 4
  store %class.btVector3* %p0, %class.btVector3** %p0.addr, align 4
  store %class.btVector3* %p1, %class.btVector3** %p1.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %dif)
  %0 = load %class.btVector3*, %class.btVector3** %p1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %p0.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = bitcast %class.btVector3* %dif to i8*
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %4 = load %class.btPlane*, %class.btPlane** %plane.addr, align 4
  %normal = getelementptr inbounds %class.btPlane, %class.btPlane* %4, i32 0, i32 0
  %call1 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %normal, %class.btVector3* nonnull align 4 dereferenceable(16) %dif)
  store float %call1, float* %dn, align 4
  %5 = load %class.btPlane*, %class.btPlane** %plane.addr, align 4
  %dist = getelementptr inbounds %class.btPlane, %class.btPlane* %5, i32 0, i32 1
  %6 = load float, float* %dist, align 4
  %7 = load %class.btPlane*, %class.btPlane** %plane.addr, align 4
  %normal2 = getelementptr inbounds %class.btPlane, %class.btPlane* %7, i32 0, i32 0
  %8 = load %class.btVector3*, %class.btVector3** %p0.addr, align 4
  %call3 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %normal2, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %add = fadd float %6, %call3
  %fneg = fneg float %add
  %9 = load float, float* %dn, align 4
  %div = fdiv float %fneg, %9
  store float %div, float* %t, align 4
  %10 = load %class.btVector3*, %class.btVector3** %p0.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %dif, float* nonnull align 4 dereferenceable(4) %t)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #1 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_Z12PlaneProjectRK7btPlaneRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btPlane* nonnull align 4 dereferenceable(20) %plane, %class.btVector3* nonnull align 4 dereferenceable(16) %point) #2 {
entry:
  %plane.addr = alloca %class.btPlane*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  store %class.btPlane* %plane, %class.btPlane** %plane.addr, align 4
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %1 = load %class.btPlane*, %class.btPlane** %plane.addr, align 4
  %normal = getelementptr inbounds %class.btPlane, %class.btPlane* %1, i32 0, i32 0
  %2 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %3 = load %class.btPlane*, %class.btPlane** %plane.addr, align 4
  %normal2 = getelementptr inbounds %class.btPlane, %class.btPlane* %3, i32 0, i32 0
  %call = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %normal2)
  %4 = load %class.btPlane*, %class.btPlane** %plane.addr, align 4
  %dist = getelementptr inbounds %class.btPlane, %class.btPlane* %4, i32 0, i32 1
  %5 = load float, float* %dist, align 4
  %add = fadd float %call, %5
  store float %add, float* %ref.tmp1, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float* nonnull align 4 dereferenceable(4) %ref.tmp1)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_Z9TriNormalRK9btVector3S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 {
entry:
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %cp = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %m = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %cp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %cp)
  store float %call, float* %m, align 4
  %4 = load float, float* %m, align 4
  %cmp = fcmp oeq float %4, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  br label %return

if.end:                                           ; preds = %entry
  %5 = load float, float* %m, align 4
  %div = fdiv float 1.000000e+00, %5
  store float %div, float* %ref.tmp6, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %cp, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z7btCrossRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define hidden float @_Z20DistanceBetweenLinesRK9btVector3S1_S1_S1_PS_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %ustart, %class.btVector3* nonnull align 4 dereferenceable(16) %udir, %class.btVector3* nonnull align 4 dereferenceable(16) %vstart, %class.btVector3* nonnull align 4 dereferenceable(16) %vdir, %class.btVector3* %upoint, %class.btVector3* %vpoint) #2 {
entry:
  %ustart.addr = alloca %class.btVector3*, align 4
  %udir.addr = alloca %class.btVector3*, align 4
  %vstart.addr = alloca %class.btVector3*, align 4
  %vdir.addr = alloca %class.btVector3*, align 4
  %upoint.addr = alloca %class.btVector3*, align 4
  %vpoint.addr = alloca %class.btVector3*, align 4
  %cp = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %distu = alloca float, align 4
  %distv = alloca float, align 4
  %dist = alloca float, align 4
  %plane = alloca %class.btPlane, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %plane17 = alloca %class.btPlane, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca %class.btVector3, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  store %class.btVector3* %ustart, %class.btVector3** %ustart.addr, align 4
  store %class.btVector3* %udir, %class.btVector3** %udir.addr, align 4
  store %class.btVector3* %vstart, %class.btVector3** %vstart.addr, align 4
  store %class.btVector3* %vdir, %class.btVector3** %vdir.addr, align 4
  store %class.btVector3* %upoint, %class.btVector3** %upoint.addr, align 4
  store %class.btVector3* %vpoint, %class.btVector3** %vpoint.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %cp)
  %0 = load %class.btVector3*, %class.btVector3** %udir.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %ref.tmp1)
  %2 = bitcast %class.btVector3* %cp to i8*
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %4 = load %class.btVector3*, %class.btVector3** %ustart.addr, align 4
  %call2 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %cp, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %fneg = fneg float %call2
  store float %fneg, float* %distu, align 4
  %5 = load %class.btVector3*, %class.btVector3** %vstart.addr, align 4
  %call3 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %cp, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  %fneg4 = fneg float %call3
  store float %fneg4, float* %distv, align 4
  %6 = load float, float* %distu, align 4
  %7 = load float, float* %distv, align 4
  %sub = fsub float %6, %7
  %call5 = call float @_Z4fabsf(float %sub) #7
  store float %call5, float* %dist, align 4
  %8 = load %class.btVector3*, %class.btVector3** %upoint.addr, align 4
  %tobool = icmp ne %class.btVector3* %8, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call6 = call %class.btPlane* @_ZN7btPlaneC2Ev(%class.btPlane* %plane)
  %9 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %cp)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* %ref.tmp8)
  %normal = getelementptr inbounds %class.btPlane, %class.btPlane* %plane, i32 0, i32 0
  %10 = bitcast %class.btVector3* %normal to i8*
  %11 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  %normal9 = getelementptr inbounds %class.btPlane, %class.btPlane* %plane, i32 0, i32 0
  %12 = load %class.btVector3*, %class.btVector3** %vstart.addr, align 4
  %call10 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %normal9, %class.btVector3* nonnull align 4 dereferenceable(16) %12)
  %fneg11 = fneg float %call10
  %dist12 = getelementptr inbounds %class.btPlane, %class.btPlane* %plane, i32 0, i32 1
  store float %fneg11, float* %dist12, align 4
  %13 = load %class.btVector3*, %class.btVector3** %ustart.addr, align 4
  %14 = load %class.btVector3*, %class.btVector3** %ustart.addr, align 4
  %15 = load %class.btVector3*, %class.btVector3** %udir.addr, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %15)
  call void @_Z21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_(%class.btVector3* sret align 4 %ref.tmp13, %class.btPlane* nonnull align 4 dereferenceable(20) %plane, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14)
  %16 = load %class.btVector3*, %class.btVector3** %upoint.addr, align 4
  %17 = bitcast %class.btVector3* %16 to i8*
  %18 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %19 = load %class.btVector3*, %class.btVector3** %vpoint.addr, align 4
  %tobool15 = icmp ne %class.btVector3* %19, null
  br i1 %tobool15, label %if.then16, label %if.end28

if.then16:                                        ; preds = %if.end
  %call18 = call %class.btPlane* @_ZN7btPlaneC2Ev(%class.btPlane* %plane17)
  %20 = load %class.btVector3*, %class.btVector3** %udir.addr, align 4
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %20, %class.btVector3* nonnull align 4 dereferenceable(16) %cp)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp19, %class.btVector3* %ref.tmp20)
  %normal21 = getelementptr inbounds %class.btPlane, %class.btPlane* %plane17, i32 0, i32 0
  %21 = bitcast %class.btVector3* %normal21 to i8*
  %22 = bitcast %class.btVector3* %ref.tmp19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  %normal22 = getelementptr inbounds %class.btPlane, %class.btPlane* %plane17, i32 0, i32 0
  %23 = load %class.btVector3*, %class.btVector3** %ustart.addr, align 4
  %call23 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %normal22, %class.btVector3* nonnull align 4 dereferenceable(16) %23)
  %fneg24 = fneg float %call23
  %dist25 = getelementptr inbounds %class.btPlane, %class.btPlane* %plane17, i32 0, i32 1
  store float %fneg24, float* %dist25, align 4
  %24 = load %class.btVector3*, %class.btVector3** %vstart.addr, align 4
  %25 = load %class.btVector3*, %class.btVector3** %vstart.addr, align 4
  %26 = load %class.btVector3*, %class.btVector3** %vdir.addr, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %25, %class.btVector3* nonnull align 4 dereferenceable(16) %26)
  call void @_Z21PlaneLineIntersectionRK7btPlaneRK9btVector3S4_(%class.btVector3* sret align 4 %ref.tmp26, %class.btPlane* nonnull align 4 dereferenceable(20) %plane17, %class.btVector3* nonnull align 4 dereferenceable(16) %24, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27)
  %27 = load %class.btVector3*, %class.btVector3** %vpoint.addr, align 4
  %28 = bitcast %class.btVector3* %27 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp26 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false)
  br label %if.end28

if.end28:                                         ; preds = %if.then16, %if.end
  %30 = load float, float* %dist, align 4
  ret float %30
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %nrm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %nrm to i8*
  %1 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %nrm)
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z4fabsf(float %__lcpp_x) #1 comdat {
entry:
  %__lcpp_x.addr = alloca float, align 4
  store float %__lcpp_x, float* %__lcpp_x.addr, align 4
  %0 = load float, float* %__lcpp_x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPlane* @_ZN7btPlaneC2Ev(%class.btPlane* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btPlane*, align 4
  store %class.btPlane* %this, %class.btPlane** %this.addr, align 4
  %this1 = load %class.btPlane*, %class.btPlane** %this.addr, align 4
  %normal = getelementptr inbounds %class.btPlane, %class.btPlane* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normal)
  %dist = getelementptr inbounds %class.btPlane, %class.btPlane* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %dist, align 4
  ret %class.btPlane* %this1
}

; Function Attrs: noinline optnone
define hidden %class.ConvexH* @_ZN7ConvexHC2Eiii(%class.ConvexH* returned %this, i32 %vertices_size, i32 %edges_size, i32 %facets_size) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.ConvexH*, align 4
  %vertices_size.addr = alloca i32, align 4
  %edges_size.addr = alloca i32, align 4
  %facets_size.addr = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %"class.ConvexH::HalfEdge", align 2
  %ref.tmp10 = alloca %class.btPlane, align 4
  store %class.ConvexH* %this, %class.ConvexH** %this.addr, align 4
  store i32 %vertices_size, i32* %vertices_size.addr, align 4
  store i32 %edges_size, i32* %edges_size.addr, align 4
  store i32 %facets_size, i32* %facets_size.addr, align 4
  %this1 = load %class.ConvexH*, %class.ConvexH** %this.addr, align 4
  %vertices = getelementptr inbounds %class.ConvexH, %class.ConvexH* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %vertices)
  %edges = getelementptr inbounds %class.ConvexH, %class.ConvexH* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEEC2Ev(%class.btAlignedObjectArray.0* %edges)
  %facets = getelementptr inbounds %class.ConvexH, %class.ConvexH* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayI7btPlaneEC2Ev(%class.btAlignedObjectArray.4* %facets)
  %vertices4 = getelementptr inbounds %class.ConvexH, %class.ConvexH* %this1, i32 0, i32 0
  %0 = load i32, i32* %vertices_size.addr, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %vertices4, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %edges6 = getelementptr inbounds %class.ConvexH, %class.ConvexH* %this1, i32 0, i32 1
  %1 = load i32, i32* %edges_size.addr, align 4
  %call8 = call %"class.ConvexH::HalfEdge"* @_ZN7ConvexH8HalfEdgeC2Ev(%"class.ConvexH::HalfEdge"* %ref.tmp7)
  call void @_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE6resizeEiRKS1_(%class.btAlignedObjectArray.0* %edges6, i32 %1, %"class.ConvexH::HalfEdge"* nonnull align 2 dereferenceable(4) %ref.tmp7)
  %facets9 = getelementptr inbounds %class.ConvexH, %class.ConvexH* %this1, i32 0, i32 2
  %2 = load i32, i32* %facets_size.addr, align 4
  %call11 = call %class.btPlane* @_ZN7btPlaneC2Ev(%class.btPlane* %ref.tmp10)
  call void @_ZN20btAlignedObjectArrayI7btPlaneE6resizeEiRKS0_(%class.btAlignedObjectArray.4* %facets9, i32 %2, %class.btPlane* nonnull align 4 dereferenceable(20) %ref.tmp10)
  ret %class.ConvexH* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIN7ConvexH8HalfEdgeELj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayI7btPlaneEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorI7btPlaneLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI7btPlaneE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %class.btVector3*, %class.btVector3** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 %15
  %16 = bitcast %class.btVector3* %arrayidx10 to i8*
  %call11 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %16)
  %17 = bitcast i8* %call11 to %class.btVector3*
  %18 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4
  %19 = bitcast %class.btVector3* %17 to i8*
  %20 = bitcast %class.btVector3* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE6resizeEiRKS1_(%class.btAlignedObjectArray.0* %this, i32 %newsize, %"class.ConvexH::HalfEdge"* nonnull align 2 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %"class.ConvexH::HalfEdge"*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %"class.ConvexH::HalfEdge"* %fillData, %"class.ConvexH::HalfEdge"** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %5 = load %"class.ConvexH::HalfEdge"*, %"class.ConvexH::HalfEdge"** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.ConvexH::HalfEdge", %"class.ConvexH::HalfEdge"* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %14 = load %"class.ConvexH::HalfEdge"*, %"class.ConvexH::HalfEdge"** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %"class.ConvexH::HalfEdge", %"class.ConvexH::HalfEdge"* %14, i32 %15
  %16 = bitcast %"class.ConvexH::HalfEdge"* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %"class.ConvexH::HalfEdge"*
  %18 = load %"class.ConvexH::HalfEdge"*, %"class.ConvexH::HalfEdge"** %fillData.addr, align 4
  %19 = bitcast %"class.ConvexH::HalfEdge"* %17 to i8*
  %20 = bitcast %"class.ConvexH::HalfEdge"* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %19, i8* align 2 %20, i32 4, i1 false)
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %21, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.ConvexH::HalfEdge"* @_ZN7ConvexH8HalfEdgeC2Ev(%"class.ConvexH::HalfEdge"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.ConvexH::HalfEdge"*, align 4
  store %"class.ConvexH::HalfEdge"* %this, %"class.ConvexH::HalfEdge"** %this.addr, align 4
  %this1 = load %"class.ConvexH::HalfEdge"*, %"class.ConvexH::HalfEdge"** %this.addr, align 4
  ret %"class.ConvexH::HalfEdge"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI7btPlaneE6resizeEiRKS0_(%class.btAlignedObjectArray.4* %this, i32 %newsize, %class.btPlane* nonnull align 4 dereferenceable(20) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btPlane*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btPlane* %fillData, %class.btPlane** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI7btPlaneE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %5 = load %class.btPlane*, %class.btPlane** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPlane, %class.btPlane* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI7btPlaneE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %14 = load %class.btPlane*, %class.btPlane** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btPlane, %class.btPlane* %14, i32 %15
  %16 = bitcast %class.btPlane* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btPlane*
  %18 = load %class.btPlane*, %class.btPlane** %fillData.addr, align 4
  %19 = bitcast %class.btPlane* %17 to i8*
  %20 = bitcast %class.btPlane* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 20, i1 false)
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %21, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_Z9PlaneTestRK7btPlaneRK9btVector3(%class.btPlane* nonnull align 4 dereferenceable(20) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 {
entry:
  %p.addr = alloca %class.btPlane*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %a = alloca float, align 4
  %flag = alloca i32, align 4
  store %class.btPlane* %p, %class.btPlane** %p.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load %class.btPlane*, %class.btPlane** %p.addr, align 4
  %normal = getelementptr inbounds %class.btPlane, %class.btPlane* %1, i32 0, i32 0
  %call = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %2 = load %class.btPlane*, %class.btPlane** %p.addr, align 4
  %dist = getelementptr inbounds %class.btPlane, %class.btPlane* %2, i32 0, i32 1
  %3 = load float, float* %dist, align 4
  %add = fadd float %call, %3
  store float %add, float* %a, align 4
  %4 = load float, float* %a, align 4
  %5 = load float, float* @planetestepsilon, align 4
  %cmp = fcmp ogt float %4, %5
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load float, float* %a, align 4
  %7 = load float, float* @planetestepsilon, align 4
  %fneg = fneg float %7
  %cmp1 = fcmp olt float %6, %fneg
  %8 = zext i1 %cmp1 to i64
  %cond = select i1 %cmp1, i32 1, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond2 = phi i32 [ 2, %cond.true ], [ %cond, %cond.false ]
  store i32 %cond2, i32* %flag, align 4
  %9 = load i32, i32* %flag, align 4
  ret i32 %9
}

; Function Attrs: noinline optnone
define hidden i32 @_Z9SplitTestR7ConvexHRK7btPlane(%class.ConvexH* nonnull align 4 dereferenceable(60) %convex, %class.btPlane* nonnull align 4 dereferenceable(20) %plane) #2 {
entry:
  %convex.addr = alloca %class.ConvexH*, align 4
  %plane.addr = alloca %class.btPlane*, align 4
  %flag = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.ConvexH* %convex, %class.ConvexH** %convex.addr, align 4
  store %class.btPlane* %plane, %class.btPlane** %plane.addr, align 4
  store i32 0, i32* %flag, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %class.ConvexH*, %class.ConvexH** %convex.addr, align 4
  %vertices = getelementptr inbounds %class.ConvexH, %class.ConvexH* %1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %vertices)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btPlane*, %class.btPlane** %plane.addr, align 4
  %3 = load %class.ConvexH*, %class.ConvexH** %convex.addr, align 4
  %vertices1 = getelementptr inbounds %class.ConvexH, %class.ConvexH* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %vertices1, i32 %4)
  %call3 = call i32 @_Z9PlaneTestRK7btPlaneRK9btVector3(%class.btPlane* nonnull align 4 dereferenceable(20) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  %5 = load i32, i32* %flag, align 4
  %or = or i32 %5, %call3
  store i32 %or, i32* %flag, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %7 = load i32, i32* %flag, align 4
  ret i32 %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_Z4orthRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %a = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %b = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %a, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  store float 1.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %b, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %call9 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %a)
  %call10 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %b)
  %cmp = fcmp ogt float %call9, %call10
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %a)
  br label %return

if.else:                                          ; preds = %entry
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %agg.result, %class.btVector3* %b)
  br label %return

return:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_ZeqRK4int3S1_(%class.int3* nonnull align 4 dereferenceable(12) %a, %class.int3* nonnull align 4 dereferenceable(12) %b) #2 {
entry:
  %retval = alloca i32, align 4
  %a.addr = alloca %class.int3*, align 4
  %b.addr = alloca %class.int3*, align 4
  %i = alloca i32, align 4
  store %class.int3* %a, %class.int3** %a.addr, align 4
  store %class.int3* %b, %class.int3** %b.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %class.int3*, %class.int3** %a.addr, align 4
  %2 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNK4int3ixEi(%class.int3* %1, i32 %2)
  %3 = load i32, i32* %call, align 4
  %4 = load %class.int3*, %class.int3** %b.addr, align 4
  %5 = load i32, i32* %i, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32* @_ZNK4int3ixEi(%class.int3* %4, i32 %5)
  %6 = load i32, i32* %call1, align 4
  %cmp2 = icmp ne i32 %3, %6
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %8 = load i32, i32* %retval, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNK4int3ixEi(%class.int3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.int3*, align 4
  %i.addr = alloca i32, align 4
  store %class.int3* %this, %class.int3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.int3*, %class.int3** %this.addr, align 4
  %x = getelementptr inbounds %class.int3, %class.int3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %x, i32 %0
  ret i32* %arrayidx
}

; Function Attrs: noinline optnone
define hidden i32 @_Z5aboveP9btVector3RK4int3RKS_f(%class.btVector3* %vertices, %class.int3* nonnull align 4 dereferenceable(12) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %p, float %epsilon) #2 {
entry:
  %vertices.addr = alloca %class.btVector3*, align 4
  %t.addr = alloca %class.int3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %epsilon.addr = alloca float, align 4
  %n = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btVector3* %vertices, %class.btVector3** %vertices.addr, align 4
  store %class.int3* %t, %class.int3** %t.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store float %epsilon, float* %epsilon.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %1 = load %class.int3*, %class.int3** %t.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNK4int3ixEi(%class.int3* %1, i32 0)
  %2 = load i32, i32* %call, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %2
  %3 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %4 = load %class.int3*, %class.int3** %t.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32* @_ZNK4int3ixEi(%class.int3* %4, i32 1)
  %5 = load i32, i32* %call1, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %5
  %6 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %7 = load %class.int3*, %class.int3** %t.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) i32* @_ZNK4int3ixEi(%class.int3* %7, i32 2)
  %8 = load i32, i32* %call3, align 4
  %arrayidx4 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 %8
  call void @_Z9TriNormalRK9btVector3S1_S1_(%class.btVector3* sret align 4 %n, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx2, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx4)
  %9 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %10 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %11 = load %class.int3*, %class.int3** %t.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNK4int3ixEi(%class.int3* %11, i32 0)
  %12 = load i32, i32* %call5, align 4
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 %12
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6)
  %call7 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %n, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %13 = load float, float* %epsilon.addr, align 4
  %cmp = fcmp ogt float %call7, %13
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: noinline optnone
define hidden i32 @_Z7hasedgeRK4int3ii(%class.int3* nonnull align 4 dereferenceable(12) %t, i32 %a, i32 %b) #2 {
entry:
  %retval = alloca i32, align 4
  %t.addr = alloca %class.int3*, align 4
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %i1 = alloca i32, align 4
  store %class.int3* %t, %class.int3** %t.addr, align 4
  store i32 %a, i32* %a.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %add = add nsw i32 %1, 1
  %rem = srem i32 %add, 3
  store i32 %rem, i32* %i1, align 4
  %2 = load %class.int3*, %class.int3** %t.addr, align 4
  %3 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNK4int3ixEi(%class.int3* %2, i32 %3)
  %4 = load i32, i32* %call, align 4
  %5 = load i32, i32* %a.addr, align 4
  %cmp1 = icmp eq i32 %4, %5
  br i1 %cmp1, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %6 = load %class.int3*, %class.int3** %t.addr, align 4
  %7 = load i32, i32* %i1, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZNK4int3ixEi(%class.int3* %6, i32 %7)
  %8 = load i32, i32* %call2, align 4
  %9 = load i32, i32* %b.addr, align 4
  %cmp3 = icmp eq i32 %8, %9
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: noinline optnone
define hidden i32 @_Z7hasvertRK4int3i(%class.int3* nonnull align 4 dereferenceable(12) %t, i32 %v) #2 {
entry:
  %t.addr = alloca %class.int3*, align 4
  %v.addr = alloca i32, align 4
  store %class.int3* %t, %class.int3** %t.addr, align 4
  store i32 %v, i32* %v.addr, align 4
  %0 = load %class.int3*, %class.int3** %t.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNK4int3ixEi(%class.int3* %0, i32 0)
  %1 = load i32, i32* %call, align 4
  %2 = load i32, i32* %v.addr, align 4
  %cmp = icmp eq i32 %1, %2
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %3 = load %class.int3*, %class.int3** %t.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32* @_ZNK4int3ixEi(%class.int3* %3, i32 1)
  %4 = load i32, i32* %call1, align 4
  %5 = load i32, i32* %v.addr, align 4
  %cmp2 = icmp eq i32 %4, %5
  br i1 %cmp2, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %lor.lhs.false
  %6 = load %class.int3*, %class.int3** %t.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) i32* @_ZNK4int3ixEi(%class.int3* %6, i32 2)
  %7 = load i32, i32* %call3, align 4
  %8 = load i32, i32* %v.addr, align 4
  %cmp4 = icmp eq i32 %7, %8
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %lor.lhs.false, %entry
  %9 = phi i1 [ true, %lor.lhs.false ], [ true, %entry ], [ %cmp4, %lor.rhs ]
  %conv = zext i1 %9 to i32
  ret i32 %conv
}

; Function Attrs: noinline optnone
define hidden i32 @_Z9shareedgeRK4int3S1_(%class.int3* nonnull align 4 dereferenceable(12) %a, %class.int3* nonnull align 4 dereferenceable(12) %b) #2 {
entry:
  %retval = alloca i32, align 4
  %a.addr = alloca %class.int3*, align 4
  %b.addr = alloca %class.int3*, align 4
  %i = alloca i32, align 4
  %i1 = alloca i32, align 4
  store %class.int3* %a, %class.int3** %a.addr, align 4
  store %class.int3* %b, %class.int3** %b.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %add = add nsw i32 %1, 1
  %rem = srem i32 %add, 3
  store i32 %rem, i32* %i1, align 4
  %2 = load %class.int3*, %class.int3** %a.addr, align 4
  %3 = load %class.int3*, %class.int3** %b.addr, align 4
  %4 = load i32, i32* %i1, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZNK4int3ixEi(%class.int3* %3, i32 %4)
  %5 = load i32, i32* %call, align 4
  %6 = load %class.int3*, %class.int3** %b.addr, align 4
  %7 = load i32, i32* %i, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32* @_ZNK4int3ixEi(%class.int3* %6, i32 %7)
  %8 = load i32, i32* %call1, align 4
  %call2 = call i32 @_Z7hasedgeRK4int3ii(%class.int3* nonnull align 4 dereferenceable(12) %2, i32 %5, i32 %8)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i32 1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  %10 = load i32, i32* %retval, align 4
  ret i32 %10
}

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(4) i32* @_ZN14btHullTriangle4neibEii(%class.btHullTriangle* %this, i32 %a, i32 %b) #2 {
entry:
  %retval = alloca i32*, align 4
  %this.addr = alloca %class.btHullTriangle*, align 4
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %i1 = alloca i32, align 4
  %i2 = alloca i32, align 4
  store %class.btHullTriangle* %this, %class.btHullTriangle** %this.addr, align 4
  store i32 %a, i32* %a.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  %this1 = load %class.btHullTriangle*, %class.btHullTriangle** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %add = add nsw i32 %1, 1
  %rem = srem i32 %add, 3
  store i32 %rem, i32* %i1, align 4
  %2 = load i32, i32* %i, align 4
  %add2 = add nsw i32 %2, 2
  %rem3 = srem i32 %add2, 3
  store i32 %rem3, i32* %i2, align 4
  %3 = bitcast %class.btHullTriangle* %this1 to %class.int3*
  %4 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %3, i32 %4)
  %5 = load i32, i32* %call, align 4
  %6 = load i32, i32* %a.addr, align 4
  %cmp4 = icmp eq i32 %5, %6
  br i1 %cmp4, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %7 = bitcast %class.btHullTriangle* %this1 to %class.int3*
  %8 = load i32, i32* %i1, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %7, i32 %8)
  %9 = load i32, i32* %call5, align 4
  %10 = load i32, i32* %b.addr, align 4
  %cmp6 = icmp eq i32 %9, %10
  br i1 %cmp6, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %n = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %this1, i32 0, i32 1
  %11 = load i32, i32* %i2, align 4
  %call7 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n, i32 %11)
  store i32* %call7, i32** %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %for.body
  %12 = bitcast %class.btHullTriangle* %this1 to %class.int3*
  %13 = load i32, i32* %i, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %12, i32 %13)
  %14 = load i32, i32* %call8, align 4
  %15 = load i32, i32* %b.addr, align 4
  %cmp9 = icmp eq i32 %14, %15
  br i1 %cmp9, label %land.lhs.true10, label %if.end16

land.lhs.true10:                                  ; preds = %if.end
  %16 = bitcast %class.btHullTriangle* %this1 to %class.int3*
  %17 = load i32, i32* %i1, align 4
  %call11 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %16, i32 %17)
  %18 = load i32, i32* %call11, align 4
  %19 = load i32, i32* %a.addr, align 4
  %cmp12 = icmp eq i32 %18, %19
  br i1 %cmp12, label %if.then13, label %if.end16

if.then13:                                        ; preds = %land.lhs.true10
  %n14 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %this1, i32 0, i32 1
  %20 = load i32, i32* %i2, align 4
  %call15 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n14, i32 %20)
  store i32* %call15, i32** %retval, align 4
  br label %return

if.end16:                                         ; preds = %land.lhs.true10, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end16
  %21 = load i32, i32* %i, align 4
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32* @_ZZN14btHullTriangle4neibEiiE2er, i32** %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then13, %if.then
  %22 = load i32*, i32** %retval, align 4
  ret i32* %22
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.int3*, align 4
  %i.addr = alloca i32, align 4
  store %class.int3* %this, %class.int3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.int3*, %class.int3** %this.addr, align 4
  %x = getelementptr inbounds %class.int3, %class.int3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %x, i32 %0
  ret i32* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN11HullLibrary6b2bfixEP14btHullTriangleS1_(%class.HullLibrary* %this, %class.btHullTriangle* %s, %class.btHullTriangle* %t) #2 {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  %s.addr = alloca %class.btHullTriangle*, align 4
  %t.addr = alloca %class.btHullTriangle*, align 4
  %i = alloca i32, align 4
  %i1 = alloca i32, align 4
  %i2 = alloca i32, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store %class.btHullTriangle* %s, %class.btHullTriangle** %s.addr, align 4
  store %class.btHullTriangle* %t, %class.btHullTriangle** %t.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %add = add nsw i32 %1, 1
  %rem = srem i32 %add, 3
  store i32 %rem, i32* %i1, align 4
  %2 = load i32, i32* %i, align 4
  %add2 = add nsw i32 %2, 2
  %rem3 = srem i32 %add2, 3
  store i32 %rem3, i32* %i2, align 4
  %3 = load %class.btHullTriangle*, %class.btHullTriangle** %s.addr, align 4
  %4 = bitcast %class.btHullTriangle* %3 to %class.int3*
  %5 = load i32, i32* %i1, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %4, i32 %5)
  %6 = load i32, i32* %call, align 4
  store i32 %6, i32* %a, align 4
  %7 = load %class.btHullTriangle*, %class.btHullTriangle** %s.addr, align 4
  %8 = bitcast %class.btHullTriangle* %7 to %class.int3*
  %9 = load i32, i32* %i2, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %8, i32 %9)
  %10 = load i32, i32* %call4, align 4
  store i32 %10, i32* %b, align 4
  %11 = load %class.btHullTriangle*, %class.btHullTriangle** %t.addr, align 4
  %12 = load i32, i32* %b, align 4
  %13 = load i32, i32* %a, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZN14btHullTriangle4neibEii(%class.btHullTriangle* %11, i32 %12, i32 %13)
  %14 = load i32, i32* %call5, align 4
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %15 = load %class.btHullTriangle*, %class.btHullTriangle** %s.addr, align 4
  %16 = load i32, i32* %a, align 4
  %17 = load i32, i32* %b, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZN14btHullTriangle4neibEii(%class.btHullTriangle* %15, i32 %16, i32 %17)
  %18 = load i32, i32* %call6, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris, i32 %18)
  %19 = load %class.btHullTriangle*, %class.btHullTriangle** %call7, align 4
  %20 = load i32, i32* %b, align 4
  %21 = load i32, i32* %a, align 4
  %call8 = call nonnull align 4 dereferenceable(4) i32* @_ZN14btHullTriangle4neibEii(%class.btHullTriangle* %19, i32 %20, i32 %21)
  store i32 %14, i32* %call8, align 4
  %22 = load %class.btHullTriangle*, %class.btHullTriangle** %s.addr, align 4
  %23 = load i32, i32* %a, align 4
  %24 = load i32, i32* %b, align 4
  %call9 = call nonnull align 4 dereferenceable(4) i32* @_ZN14btHullTriangle4neibEii(%class.btHullTriangle* %22, i32 %23, i32 %24)
  %25 = load i32, i32* %call9, align 4
  %m_tris10 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %26 = load %class.btHullTriangle*, %class.btHullTriangle** %t.addr, align 4
  %27 = load i32, i32* %b, align 4
  %28 = load i32, i32* %a, align 4
  %call11 = call nonnull align 4 dereferenceable(4) i32* @_ZN14btHullTriangle4neibEii(%class.btHullTriangle* %26, i32 %27, i32 %28)
  %29 = load i32, i32* %call11, align 4
  %call12 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris10, i32 %29)
  %30 = load %class.btHullTriangle*, %class.btHullTriangle** %call12, align 4
  %31 = load i32, i32* %a, align 4
  %32 = load i32, i32* %b, align 4
  %call13 = call nonnull align 4 dereferenceable(4) i32* @_ZN14btHullTriangle4neibEii(%class.btHullTriangle* %30, i32 %31, i32 %32)
  store i32 %25, i32* %call13, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %33 = load i32, i32* %i, align 4
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btHullTriangle*, %class.btHullTriangle** %0, i32 %1
  ret %class.btHullTriangle** %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN11HullLibrary9removeb2bEP14btHullTriangleS1_(%class.HullLibrary* %this, %class.btHullTriangle* %s, %class.btHullTriangle* %t) #2 {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  %s.addr = alloca %class.btHullTriangle*, align 4
  %t.addr = alloca %class.btHullTriangle*, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store %class.btHullTriangle* %s, %class.btHullTriangle** %s.addr, align 4
  store %class.btHullTriangle* %t, %class.btHullTriangle** %t.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %0 = load %class.btHullTriangle*, %class.btHullTriangle** %s.addr, align 4
  %1 = load %class.btHullTriangle*, %class.btHullTriangle** %t.addr, align 4
  call void @_ZN11HullLibrary6b2bfixEP14btHullTriangleS1_(%class.HullLibrary* %this1, %class.btHullTriangle* %0, %class.btHullTriangle* %1)
  %2 = load %class.btHullTriangle*, %class.btHullTriangle** %s.addr, align 4
  call void @_ZN11HullLibrary18deAllocateTriangleEP14btHullTriangle(%class.HullLibrary* %this1, %class.btHullTriangle* %2)
  %3 = load %class.btHullTriangle*, %class.btHullTriangle** %t.addr, align 4
  call void @_ZN11HullLibrary18deAllocateTriangleEP14btHullTriangle(%class.HullLibrary* %this1, %class.btHullTriangle* %3)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN11HullLibrary18deAllocateTriangleEP14btHullTriangle(%class.HullLibrary* %this, %class.btHullTriangle* %tri) #2 {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  %tri.addr = alloca %class.btHullTriangle*, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store %class.btHullTriangle* %tri, %class.btHullTriangle** %tri.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %0 = load %class.btHullTriangle*, %class.btHullTriangle** %tri.addr, align 4
  %id = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %0, i32 0, i32 2
  %1 = load i32, i32* %id, align 4
  %call = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris, i32 %1)
  store %class.btHullTriangle* null, %class.btHullTriangle** %call, align 4
  %2 = load %class.btHullTriangle*, %class.btHullTriangle** %tri.addr, align 4
  %call2 = call %class.btHullTriangle* @_ZN14btHullTriangleD2Ev(%class.btHullTriangle* %2) #7
  %3 = load %class.btHullTriangle*, %class.btHullTriangle** %tri.addr, align 4
  %4 = bitcast %class.btHullTriangle* %3 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %4)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN11HullLibrary7checkitEP14btHullTriangle(%class.HullLibrary* %this, %class.btHullTriangle* %t) #2 {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  %t.addr = alloca %class.btHullTriangle*, align 4
  %i = alloca i32, align 4
  %i1 = alloca i32, align 4
  %i2 = alloca i32, align 4
  %a = alloca i32, align 4
  %b = alloca i32, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store %class.btHullTriangle* %t, %class.btHullTriangle** %t.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load i32, i32* %i, align 4
  %add = add nsw i32 %1, 1
  %rem = srem i32 %add, 3
  store i32 %rem, i32* %i1, align 4
  %2 = load i32, i32* %i, align 4
  %add2 = add nsw i32 %2, 2
  %rem3 = srem i32 %add2, 3
  store i32 %rem3, i32* %i2, align 4
  %3 = load %class.btHullTriangle*, %class.btHullTriangle** %t.addr, align 4
  %4 = bitcast %class.btHullTriangle* %3 to %class.int3*
  %5 = load i32, i32* %i1, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %4, i32 %5)
  %6 = load i32, i32* %call, align 4
  store i32 %6, i32* %a, align 4
  %7 = load %class.btHullTriangle*, %class.btHullTriangle** %t.addr, align 4
  %8 = bitcast %class.btHullTriangle* %7 to %class.int3*
  %9 = load i32, i32* %i2, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %8, i32 %9)
  %10 = load i32, i32* %call4, align 4
  store i32 %10, i32* %b, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btHullTriangle* @_ZN11HullLibrary16allocateTriangleEiii(%class.HullLibrary* %this, i32 %a, i32 %b, i32 %c) #2 {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  %c.addr = alloca i32, align 4
  %mem = alloca i8*, align 4
  %tr = alloca %class.btHullTriangle*, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store i32 %a, i32* %a.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  store i32 %c, i32* %c.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 36, i32 16)
  store i8* %call, i8** %mem, align 4
  %0 = load i8*, i8** %mem, align 4
  %1 = bitcast i8* %0 to %class.btHullTriangle*
  %2 = load i32, i32* %a.addr, align 4
  %3 = load i32, i32* %b.addr, align 4
  %4 = load i32, i32* %c.addr, align 4
  %call2 = call %class.btHullTriangle* @_ZN14btHullTriangleC2Eiii(%class.btHullTriangle* %1, i32 %2, i32 %3, i32 %4)
  store %class.btHullTriangle* %1, %class.btHullTriangle** %tr, align 4
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %m_tris)
  %5 = load %class.btHullTriangle*, %class.btHullTriangle** %tr, align 4
  %id = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %5, i32 0, i32 2
  store i32 %call3, i32* %id, align 4
  %m_tris4 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE9push_backERKS1_(%class.btAlignedObjectArray.8* %m_tris4, %class.btHullTriangle** nonnull align 4 dereferenceable(4) %tr)
  %6 = load %class.btHullTriangle*, %class.btHullTriangle** %tr, align 4
  ret %class.btHullTriangle* %6
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btHullTriangle* @_ZN14btHullTriangleC2Eiii(%class.btHullTriangle* returned %this, i32 %a, i32 %b, i32 %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btHullTriangle*, align 4
  %a.addr = alloca i32, align 4
  %b.addr = alloca i32, align 4
  %c.addr = alloca i32, align 4
  store %class.btHullTriangle* %this, %class.btHullTriangle** %this.addr, align 4
  store i32 %a, i32* %a.addr, align 4
  store i32 %b, i32* %b.addr, align 4
  store i32 %c, i32* %c.addr, align 4
  %this1 = load %class.btHullTriangle*, %class.btHullTriangle** %this.addr, align 4
  %0 = bitcast %class.btHullTriangle* %this1 to %class.int3*
  %1 = load i32, i32* %a.addr, align 4
  %2 = load i32, i32* %b.addr, align 4
  %3 = load i32, i32* %c.addr, align 4
  %call = call %class.int3* @_ZN4int3C2Eiii(%class.int3* %0, i32 %1, i32 %2, i32 %3)
  %n = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %this1, i32 0, i32 1
  %call2 = call %class.int3* @_ZN4int3C2Eiii(%class.int3* %n, i32 -1, i32 -1, i32 -1)
  %vmax = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %this1, i32 0, i32 3
  store i32 -1, i32* %vmax, align 4
  %rise = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %this1, i32 0, i32 4
  store float 0.000000e+00, float* %rise, align 4
  ret %class.btHullTriangle* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE9push_backERKS1_(%class.btAlignedObjectArray.8* %this, %class.btHullTriangle** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Val.addr = alloca %class.btHullTriangle**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store %class.btHullTriangle** %_Val, %class.btHullTriangle*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP14btHullTriangleE9allocSizeEi(%class.btAlignedObjectArray.8* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %1 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btHullTriangle*, %class.btHullTriangle** %1, i32 %2
  %3 = bitcast %class.btHullTriangle** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btHullTriangle**
  %5 = load %class.btHullTriangle**, %class.btHullTriangle*** %_Val.addr, align 4
  %6 = load %class.btHullTriangle*, %class.btHullTriangle** %5, align 4
  store %class.btHullTriangle* %6, %class.btHullTriangle** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btHullTriangle* @_ZN14btHullTriangleD2Ev(%class.btHullTriangle* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHullTriangle*, align 4
  store %class.btHullTriangle* %this, %class.btHullTriangle** %this.addr, align 4
  %this1 = load %class.btHullTriangle*, %class.btHullTriangle** %this.addr, align 4
  ret %class.btHullTriangle* %this1
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: noinline optnone
define hidden void @_ZN11HullLibrary7extrudeEP14btHullTrianglei(%class.HullLibrary* %this, %class.btHullTriangle* %t0, i32 %v) #2 {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  %t0.addr = alloca %class.btHullTriangle*, align 4
  %v.addr = alloca i32, align 4
  %t = alloca %class.int3, align 4
  %n = alloca i32, align 4
  %ta = alloca %class.btHullTriangle*, align 4
  %ref.tmp = alloca %class.int3, align 4
  %tb = alloca %class.btHullTriangle*, align 4
  %ref.tmp21 = alloca %class.int3, align 4
  %tc = alloca %class.btHullTriangle*, align 4
  %ref.tmp39 = alloca %class.int3, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store %class.btHullTriangle* %t0, %class.btHullTriangle** %t0.addr, align 4
  store i32 %v, i32* %v.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %0 = load %class.btHullTriangle*, %class.btHullTriangle** %t0.addr, align 4
  %1 = bitcast %class.btHullTriangle* %0 to %class.int3*
  %2 = bitcast %class.int3* %t to i8*
  %3 = bitcast %class.int3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 12, i1 false)
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %m_tris)
  store i32 %call, i32* %n, align 4
  %4 = load i32, i32* %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %t, i32 1)
  %5 = load i32, i32* %call2, align 4
  %call3 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %t, i32 2)
  %6 = load i32, i32* %call3, align 4
  %call4 = call %class.btHullTriangle* @_ZN11HullLibrary16allocateTriangleEiii(%class.HullLibrary* %this1, i32 %4, i32 %5, i32 %6)
  store %class.btHullTriangle* %call4, %class.btHullTriangle** %ta, align 4
  %7 = load %class.btHullTriangle*, %class.btHullTriangle** %t0.addr, align 4
  %n5 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %7, i32 0, i32 1
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n5, i32 0)
  %8 = load i32, i32* %call6, align 4
  %9 = load i32, i32* %n, align 4
  %add = add nsw i32 %9, 1
  %10 = load i32, i32* %n, align 4
  %add7 = add nsw i32 %10, 2
  %call8 = call %class.int3* @_ZN4int3C2Eiii(%class.int3* %ref.tmp, i32 %8, i32 %add, i32 %add7)
  %11 = load %class.btHullTriangle*, %class.btHullTriangle** %ta, align 4
  %n9 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %11, i32 0, i32 1
  %12 = bitcast %class.int3* %n9 to i8*
  %13 = bitcast %class.int3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 12, i1 false)
  %14 = load i32, i32* %n, align 4
  %add10 = add nsw i32 %14, 0
  %m_tris11 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %15 = load %class.btHullTriangle*, %class.btHullTriangle** %t0.addr, align 4
  %n12 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %15, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n12, i32 0)
  %16 = load i32, i32* %call13, align 4
  %call14 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris11, i32 %16)
  %17 = load %class.btHullTriangle*, %class.btHullTriangle** %call14, align 4
  %call15 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %t, i32 1)
  %18 = load i32, i32* %call15, align 4
  %call16 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %t, i32 2)
  %19 = load i32, i32* %call16, align 4
  %call17 = call nonnull align 4 dereferenceable(4) i32* @_ZN14btHullTriangle4neibEii(%class.btHullTriangle* %17, i32 %18, i32 %19)
  store i32 %add10, i32* %call17, align 4
  %20 = load i32, i32* %v.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %t, i32 2)
  %21 = load i32, i32* %call18, align 4
  %call19 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %t, i32 0)
  %22 = load i32, i32* %call19, align 4
  %call20 = call %class.btHullTriangle* @_ZN11HullLibrary16allocateTriangleEiii(%class.HullLibrary* %this1, i32 %20, i32 %21, i32 %22)
  store %class.btHullTriangle* %call20, %class.btHullTriangle** %tb, align 4
  %23 = load %class.btHullTriangle*, %class.btHullTriangle** %t0.addr, align 4
  %n22 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %23, i32 0, i32 1
  %call23 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n22, i32 1)
  %24 = load i32, i32* %call23, align 4
  %25 = load i32, i32* %n, align 4
  %add24 = add nsw i32 %25, 2
  %26 = load i32, i32* %n, align 4
  %add25 = add nsw i32 %26, 0
  %call26 = call %class.int3* @_ZN4int3C2Eiii(%class.int3* %ref.tmp21, i32 %24, i32 %add24, i32 %add25)
  %27 = load %class.btHullTriangle*, %class.btHullTriangle** %tb, align 4
  %n27 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %27, i32 0, i32 1
  %28 = bitcast %class.int3* %n27 to i8*
  %29 = bitcast %class.int3* %ref.tmp21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 12, i1 false)
  %30 = load i32, i32* %n, align 4
  %add28 = add nsw i32 %30, 1
  %m_tris29 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %31 = load %class.btHullTriangle*, %class.btHullTriangle** %t0.addr, align 4
  %n30 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %31, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n30, i32 1)
  %32 = load i32, i32* %call31, align 4
  %call32 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris29, i32 %32)
  %33 = load %class.btHullTriangle*, %class.btHullTriangle** %call32, align 4
  %call33 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %t, i32 2)
  %34 = load i32, i32* %call33, align 4
  %call34 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %t, i32 0)
  %35 = load i32, i32* %call34, align 4
  %call35 = call nonnull align 4 dereferenceable(4) i32* @_ZN14btHullTriangle4neibEii(%class.btHullTriangle* %33, i32 %34, i32 %35)
  store i32 %add28, i32* %call35, align 4
  %36 = load i32, i32* %v.addr, align 4
  %call36 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %t, i32 0)
  %37 = load i32, i32* %call36, align 4
  %call37 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %t, i32 1)
  %38 = load i32, i32* %call37, align 4
  %call38 = call %class.btHullTriangle* @_ZN11HullLibrary16allocateTriangleEiii(%class.HullLibrary* %this1, i32 %36, i32 %37, i32 %38)
  store %class.btHullTriangle* %call38, %class.btHullTriangle** %tc, align 4
  %39 = load %class.btHullTriangle*, %class.btHullTriangle** %t0.addr, align 4
  %n40 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %39, i32 0, i32 1
  %call41 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n40, i32 2)
  %40 = load i32, i32* %call41, align 4
  %41 = load i32, i32* %n, align 4
  %add42 = add nsw i32 %41, 0
  %42 = load i32, i32* %n, align 4
  %add43 = add nsw i32 %42, 1
  %call44 = call %class.int3* @_ZN4int3C2Eiii(%class.int3* %ref.tmp39, i32 %40, i32 %add42, i32 %add43)
  %43 = load %class.btHullTriangle*, %class.btHullTriangle** %tc, align 4
  %n45 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %43, i32 0, i32 1
  %44 = bitcast %class.int3* %n45 to i8*
  %45 = bitcast %class.int3* %ref.tmp39 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %44, i8* align 4 %45, i32 12, i1 false)
  %46 = load i32, i32* %n, align 4
  %add46 = add nsw i32 %46, 2
  %m_tris47 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %47 = load %class.btHullTriangle*, %class.btHullTriangle** %t0.addr, align 4
  %n48 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %47, i32 0, i32 1
  %call49 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n48, i32 2)
  %48 = load i32, i32* %call49, align 4
  %call50 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris47, i32 %48)
  %49 = load %class.btHullTriangle*, %class.btHullTriangle** %call50, align 4
  %call51 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %t, i32 0)
  %50 = load i32, i32* %call51, align 4
  %call52 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %t, i32 1)
  %51 = load i32, i32* %call52, align 4
  %call53 = call nonnull align 4 dereferenceable(4) i32* @_ZN14btHullTriangle4neibEii(%class.btHullTriangle* %49, i32 %50, i32 %51)
  store i32 %add46, i32* %call53, align 4
  %52 = load %class.btHullTriangle*, %class.btHullTriangle** %ta, align 4
  call void @_ZN11HullLibrary7checkitEP14btHullTriangle(%class.HullLibrary* %this1, %class.btHullTriangle* %52)
  %53 = load %class.btHullTriangle*, %class.btHullTriangle** %tb, align 4
  call void @_ZN11HullLibrary7checkitEP14btHullTriangle(%class.HullLibrary* %this1, %class.btHullTriangle* %53)
  %54 = load %class.btHullTriangle*, %class.btHullTriangle** %tc, align 4
  call void @_ZN11HullLibrary7checkitEP14btHullTriangle(%class.HullLibrary* %this1, %class.btHullTriangle* %54)
  %m_tris54 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %55 = load %class.btHullTriangle*, %class.btHullTriangle** %ta, align 4
  %n55 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %55, i32 0, i32 1
  %call56 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n55, i32 0)
  %56 = load i32, i32* %call56, align 4
  %call57 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris54, i32 %56)
  %57 = load %class.btHullTriangle*, %class.btHullTriangle** %call57, align 4
  %58 = bitcast %class.btHullTriangle* %57 to %class.int3*
  %59 = load i32, i32* %v.addr, align 4
  %call58 = call i32 @_Z7hasvertRK4int3i(%class.int3* nonnull align 4 dereferenceable(12) %58, i32 %59)
  %tobool = icmp ne i32 %call58, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %60 = load %class.btHullTriangle*, %class.btHullTriangle** %ta, align 4
  %m_tris59 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %61 = load %class.btHullTriangle*, %class.btHullTriangle** %ta, align 4
  %n60 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %61, i32 0, i32 1
  %call61 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n60, i32 0)
  %62 = load i32, i32* %call61, align 4
  %call62 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris59, i32 %62)
  %63 = load %class.btHullTriangle*, %class.btHullTriangle** %call62, align 4
  call void @_ZN11HullLibrary9removeb2bEP14btHullTriangleS1_(%class.HullLibrary* %this1, %class.btHullTriangle* %60, %class.btHullTriangle* %63)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_tris63 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %64 = load %class.btHullTriangle*, %class.btHullTriangle** %tb, align 4
  %n64 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %64, i32 0, i32 1
  %call65 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n64, i32 0)
  %65 = load i32, i32* %call65, align 4
  %call66 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris63, i32 %65)
  %66 = load %class.btHullTriangle*, %class.btHullTriangle** %call66, align 4
  %67 = bitcast %class.btHullTriangle* %66 to %class.int3*
  %68 = load i32, i32* %v.addr, align 4
  %call67 = call i32 @_Z7hasvertRK4int3i(%class.int3* nonnull align 4 dereferenceable(12) %67, i32 %68)
  %tobool68 = icmp ne i32 %call67, 0
  br i1 %tobool68, label %if.then69, label %if.end74

if.then69:                                        ; preds = %if.end
  %69 = load %class.btHullTriangle*, %class.btHullTriangle** %tb, align 4
  %m_tris70 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %70 = load %class.btHullTriangle*, %class.btHullTriangle** %tb, align 4
  %n71 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %70, i32 0, i32 1
  %call72 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n71, i32 0)
  %71 = load i32, i32* %call72, align 4
  %call73 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris70, i32 %71)
  %72 = load %class.btHullTriangle*, %class.btHullTriangle** %call73, align 4
  call void @_ZN11HullLibrary9removeb2bEP14btHullTriangleS1_(%class.HullLibrary* %this1, %class.btHullTriangle* %69, %class.btHullTriangle* %72)
  br label %if.end74

if.end74:                                         ; preds = %if.then69, %if.end
  %m_tris75 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %73 = load %class.btHullTriangle*, %class.btHullTriangle** %tc, align 4
  %n76 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %73, i32 0, i32 1
  %call77 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n76, i32 0)
  %74 = load i32, i32* %call77, align 4
  %call78 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris75, i32 %74)
  %75 = load %class.btHullTriangle*, %class.btHullTriangle** %call78, align 4
  %76 = bitcast %class.btHullTriangle* %75 to %class.int3*
  %77 = load i32, i32* %v.addr, align 4
  %call79 = call i32 @_Z7hasvertRK4int3i(%class.int3* nonnull align 4 dereferenceable(12) %76, i32 %77)
  %tobool80 = icmp ne i32 %call79, 0
  br i1 %tobool80, label %if.then81, label %if.end86

if.then81:                                        ; preds = %if.end74
  %78 = load %class.btHullTriangle*, %class.btHullTriangle** %tc, align 4
  %m_tris82 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %79 = load %class.btHullTriangle*, %class.btHullTriangle** %tc, align 4
  %n83 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %79, i32 0, i32 1
  %call84 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n83, i32 0)
  %80 = load i32, i32* %call84, align 4
  %call85 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris82, i32 %80)
  %81 = load %class.btHullTriangle*, %class.btHullTriangle** %call85, align 4
  call void @_ZN11HullLibrary9removeb2bEP14btHullTriangleS1_(%class.HullLibrary* %this1, %class.btHullTriangle* %78, %class.btHullTriangle* %81)
  br label %if.end86

if.end86:                                         ; preds = %if.then81, %if.end74
  %82 = load %class.btHullTriangle*, %class.btHullTriangle** %t0.addr, align 4
  call void @_ZN11HullLibrary18deAllocateTriangleEP14btHullTriangle(%class.HullLibrary* %this1, %class.btHullTriangle* %82)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.int3* @_ZN4int3C2Eiii(%class.int3* returned %this, i32 %_x, i32 %_y, i32 %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.int3*, align 4
  %_x.addr = alloca i32, align 4
  %_y.addr = alloca i32, align 4
  %_z.addr = alloca i32, align 4
  store %class.int3* %this, %class.int3** %this.addr, align 4
  store i32 %_x, i32* %_x.addr, align 4
  store i32 %_y, i32* %_y.addr, align 4
  store i32 %_z, i32* %_z.addr, align 4
  %this1 = load %class.int3*, %class.int3** %this.addr, align 4
  %0 = load i32, i32* %_x.addr, align 4
  %x = getelementptr inbounds %class.int3, %class.int3* %this1, i32 0, i32 0
  store i32 %0, i32* %x, align 4
  %1 = load i32, i32* %_y.addr, align 4
  %y = getelementptr inbounds %class.int3, %class.int3* %this1, i32 0, i32 1
  store i32 %1, i32* %y, align 4
  %2 = load i32, i32* %_z.addr, align 4
  %z = getelementptr inbounds %class.int3, %class.int3* %this1, i32 0, i32 2
  store i32 %2, i32* %z, align 4
  ret %class.int3* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btHullTriangle* @_ZN11HullLibrary10extrudableEf(%class.HullLibrary* %this, float %epsilon) #2 {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  %epsilon.addr = alloca float, align 4
  %i = alloca i32, align 4
  %t = alloca %class.btHullTriangle*, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store float %epsilon, float* %epsilon.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  store %class.btHullTriangle* null, %class.btHullTriangle** %t, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %m_tris)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %class.btHullTriangle*, %class.btHullTriangle** %t, align 4
  %tobool = icmp ne %class.btHullTriangle* %1, null
  br i1 %tobool, label %lor.lhs.false, label %if.then

lor.lhs.false:                                    ; preds = %for.body
  %m_tris2 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %2 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris2, i32 %2)
  %3 = load %class.btHullTriangle*, %class.btHullTriangle** %call3, align 4
  %tobool4 = icmp ne %class.btHullTriangle* %3, null
  br i1 %tobool4, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %lor.lhs.false
  %4 = load %class.btHullTriangle*, %class.btHullTriangle** %t, align 4
  %rise = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %4, i32 0, i32 4
  %5 = load float, float* %rise, align 4
  %m_tris5 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %6 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris5, i32 %6)
  %7 = load %class.btHullTriangle*, %class.btHullTriangle** %call6, align 4
  %rise7 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %7, i32 0, i32 4
  %8 = load float, float* %rise7, align 4
  %cmp8 = fcmp olt float %5, %8
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true, %for.body
  %m_tris9 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %9 = load i32, i32* %i, align 4
  %call10 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris9, i32 %9)
  %10 = load %class.btHullTriangle*, %class.btHullTriangle** %call10, align 4
  store %class.btHullTriangle* %10, %class.btHullTriangle** %t, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %lor.lhs.false
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %12 = load %class.btHullTriangle*, %class.btHullTriangle** %t, align 4
  %rise11 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %12, i32 0, i32 4
  %13 = load float, float* %rise11, align 4
  %14 = load float, float* %epsilon.addr, align 4
  %cmp12 = fcmp ogt float %13, %14
  br i1 %cmp12, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end
  %15 = load %class.btHullTriangle*, %class.btHullTriangle** %t, align 4
  br label %cond.end

cond.false:                                       ; preds = %for.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btHullTriangle* [ %15, %cond.true ], [ null, %cond.false ]
  ret %class.btHullTriangle* %cond
}

; Function Attrs: noinline optnone
define hidden void @_ZN11HullLibrary11FindSimplexEP9btVector3iR20btAlignedObjectArrayIiE(%class.int4* noalias sret align 4 %agg.result, %class.HullLibrary* %this, %class.btVector3* %verts, i32 %verts_count, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %allow) #2 {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  %verts.addr = alloca %class.btVector3*, align 4
  %verts_count.addr = alloca i32, align 4
  %allow.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %basis = alloca [3 x %class.btVector3], align 16
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %p0 = alloca i32, align 4
  %p1 = alloca i32, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp23 = alloca %class.btVector3, align 4
  %ref.tmp24 = alloca %class.btVector3, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp35 = alloca float, align 4
  %p2 = alloca i32, align 4
  %ref.tmp57 = alloca %class.btVector3, align 4
  %ref.tmp67 = alloca %class.btVector3, align 4
  %ref.tmp71 = alloca %class.btVector3, align 4
  %ref.tmp72 = alloca %class.btVector3, align 4
  %p3 = alloca i32, align 4
  %ref.tmp84 = alloca %class.btVector3, align 4
  %ref.tmp96 = alloca %class.btVector3, align 4
  %ref.tmp99 = alloca %class.btVector3, align 4
  %ref.tmp100 = alloca %class.btVector3, align 4
  %ref.tmp103 = alloca %class.btVector3, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store %class.btVector3* %verts, %class.btVector3** %verts.addr, align 4
  store i32 %verts_count, i32* %verts_count.addr, align 4
  store %class.btAlignedObjectArray.12* %allow, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  store float 0x3F847AE140000000, float* %ref.tmp2, align 4
  store float 0x3F947AE140000000, float* %ref.tmp3, align 4
  store float 1.000000e+00, float* %ref.tmp4, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 0
  %0 = bitcast %class.btVector3* %arrayidx to i8*
  %1 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %0, i8* align 4 %1, i32 16, i1 false)
  %2 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %3 = load i32, i32* %verts_count.addr, align 4
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 0
  %4 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %call7 = call i32 @_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE(%class.btVector3* %2, i32 %3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %4)
  store i32 %call7, i32* %p0, align 4
  %5 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %6 = load i32, i32* %verts_count.addr, align 4
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 0
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx9)
  %7 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %call10 = call i32 @_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE(%class.btVector3* %5, i32 %6, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %7)
  store i32 %call10, i32* %p1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %9 = load i32, i32* %p0, align 4
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 %9
  %10 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %11 = load i32, i32* %p1, align 4
  %arrayidx13 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 %11
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx12, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx13)
  %arrayidx14 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 0
  %12 = bitcast %class.btVector3* %arrayidx14 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %12, i8* align 4 %13, i32 16, i1 false)
  %14 = load i32, i32* %p0, align 4
  %15 = load i32, i32* %p1, align 4
  %cmp = icmp eq i32 %14, %15
  br i1 %cmp, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %arrayctor.cont
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 0
  store float 0.000000e+00, float* %ref.tmp17, align 4
  store float 0.000000e+00, float* %ref.tmp18, align 4
  store float 0.000000e+00, float* %ref.tmp19, align 4
  %call20 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19)
  %call21 = call zeroext i1 @_ZNK9btVector3eqERKS_(%class.btVector3* %arrayidx15, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp16)
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %arrayctor.cont
  %16 = phi i1 [ true, %arrayctor.cont ], [ %call21, %lor.rhs ]
  br i1 %16, label %if.then, label %if.end

if.then:                                          ; preds = %lor.end
  %call22 = call %class.int4* @_ZN4int4C2Eiiii(%class.int4* %agg.result, i32 -1, i32 -1, i32 -1, i32 -1)
  br label %return

if.end:                                           ; preds = %lor.end
  store float 1.000000e+00, float* %ref.tmp25, align 4
  store float 0x3F947AE140000000, float* %ref.tmp26, align 4
  store float 0.000000e+00, float* %ref.tmp27, align 4
  %call28 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp27)
  %arrayidx29 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 0
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp23, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp24, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx29)
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 1
  %17 = bitcast %class.btVector3* %arrayidx30 to i8*
  %18 = bitcast %class.btVector3* %ref.tmp23 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %17, i8* align 4 %18, i32 16, i1 false)
  store float 0xBF947AE140000000, float* %ref.tmp33, align 4
  store float 1.000000e+00, float* %ref.tmp34, align 4
  store float 0.000000e+00, float* %ref.tmp35, align 4
  %call36 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp35)
  %arrayidx37 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 0
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx37)
  %arrayidx38 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 2
  %19 = bitcast %class.btVector3* %arrayidx38 to i8*
  %20 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %19, i8* align 4 %20, i32 16, i1 false)
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 1
  %call40 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %arrayidx39)
  %arrayidx41 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 2
  %call42 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %arrayidx41)
  %cmp43 = fcmp ogt float %call40, %call42
  br i1 %cmp43, label %if.then44, label %if.else

if.then44:                                        ; preds = %if.end
  %arrayidx45 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 1
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %arrayidx45)
  br label %if.end51

if.else:                                          ; preds = %if.end
  %arrayidx47 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 2
  %arrayidx48 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 1
  %21 = bitcast %class.btVector3* %arrayidx48 to i8*
  %22 = bitcast %class.btVector3* %arrayidx47 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %21, i8* align 16 %22, i32 16, i1 false)
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 1
  %call50 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %arrayidx49)
  br label %if.end51

if.end51:                                         ; preds = %if.else, %if.then44
  %23 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %24 = load i32, i32* %verts_count.addr, align 4
  %arrayidx52 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 1
  %25 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %call53 = call i32 @_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE(%class.btVector3* %23, i32 %24, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx52, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %25)
  store i32 %call53, i32* %p2, align 4
  %26 = load i32, i32* %p2, align 4
  %27 = load i32, i32* %p0, align 4
  %cmp54 = icmp eq i32 %26, %27
  br i1 %cmp54, label %if.then56, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end51
  %28 = load i32, i32* %p2, align 4
  %29 = load i32, i32* %p1, align 4
  %cmp55 = icmp eq i32 %28, %29
  br i1 %cmp55, label %if.then56, label %if.end60

if.then56:                                        ; preds = %lor.lhs.false, %if.end51
  %30 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %31 = load i32, i32* %verts_count.addr, align 4
  %arrayidx58 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp57, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx58)
  %32 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %call59 = call i32 @_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE(%class.btVector3* %30, i32 %31, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp57, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %32)
  store i32 %call59, i32* %p2, align 4
  br label %if.end60

if.end60:                                         ; preds = %if.then56, %lor.lhs.false
  %33 = load i32, i32* %p2, align 4
  %34 = load i32, i32* %p0, align 4
  %cmp61 = icmp eq i32 %33, %34
  br i1 %cmp61, label %if.then64, label %lor.lhs.false62

lor.lhs.false62:                                  ; preds = %if.end60
  %35 = load i32, i32* %p2, align 4
  %36 = load i32, i32* %p1, align 4
  %cmp63 = icmp eq i32 %35, %36
  br i1 %cmp63, label %if.then64, label %if.end66

if.then64:                                        ; preds = %lor.lhs.false62, %if.end60
  %call65 = call %class.int4* @_ZN4int4C2Eiiii(%class.int4* %agg.result, i32 -1, i32 -1, i32 -1, i32 -1)
  br label %return

if.end66:                                         ; preds = %lor.lhs.false62
  %37 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %38 = load i32, i32* %p2, align 4
  %arrayidx68 = getelementptr inbounds %class.btVector3, %class.btVector3* %37, i32 %38
  %39 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %40 = load i32, i32* %p0, align 4
  %arrayidx69 = getelementptr inbounds %class.btVector3, %class.btVector3* %39, i32 %40
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp67, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx68, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx69)
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 1
  %41 = bitcast %class.btVector3* %arrayidx70 to i8*
  %42 = bitcast %class.btVector3* %ref.tmp67 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %41, i8* align 4 %42, i32 16, i1 false)
  %arrayidx73 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 1
  %arrayidx74 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 0
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp72, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx73, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx74)
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %ref.tmp71, %class.btVector3* %ref.tmp72)
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 2
  %43 = bitcast %class.btVector3* %arrayidx75 to i8*
  %44 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %43, i8* align 4 %44, i32 16, i1 false)
  %45 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %46 = load i32, i32* %verts_count.addr, align 4
  %arrayidx76 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 2
  %47 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %call77 = call i32 @_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE(%class.btVector3* %45, i32 %46, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx76, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %47)
  store i32 %call77, i32* %p3, align 4
  %48 = load i32, i32* %p3, align 4
  %49 = load i32, i32* %p0, align 4
  %cmp78 = icmp eq i32 %48, %49
  br i1 %cmp78, label %if.then83, label %lor.lhs.false79

lor.lhs.false79:                                  ; preds = %if.end66
  %50 = load i32, i32* %p3, align 4
  %51 = load i32, i32* %p1, align 4
  %cmp80 = icmp eq i32 %50, %51
  br i1 %cmp80, label %if.then83, label %lor.lhs.false81

lor.lhs.false81:                                  ; preds = %lor.lhs.false79
  %52 = load i32, i32* %p3, align 4
  %53 = load i32, i32* %p2, align 4
  %cmp82 = icmp eq i32 %52, %53
  br i1 %cmp82, label %if.then83, label %if.end87

if.then83:                                        ; preds = %lor.lhs.false81, %lor.lhs.false79, %if.end66
  %54 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %55 = load i32, i32* %verts_count.addr, align 4
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %basis, i32 0, i32 2
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp84, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx85)
  %56 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %call86 = call i32 @_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE(%class.btVector3* %54, i32 %55, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp84, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %56)
  store i32 %call86, i32* %p3, align 4
  br label %if.end87

if.end87:                                         ; preds = %if.then83, %lor.lhs.false81
  %57 = load i32, i32* %p3, align 4
  %58 = load i32, i32* %p0, align 4
  %cmp88 = icmp eq i32 %57, %58
  br i1 %cmp88, label %if.then93, label %lor.lhs.false89

lor.lhs.false89:                                  ; preds = %if.end87
  %59 = load i32, i32* %p3, align 4
  %60 = load i32, i32* %p1, align 4
  %cmp90 = icmp eq i32 %59, %60
  br i1 %cmp90, label %if.then93, label %lor.lhs.false91

lor.lhs.false91:                                  ; preds = %lor.lhs.false89
  %61 = load i32, i32* %p3, align 4
  %62 = load i32, i32* %p2, align 4
  %cmp92 = icmp eq i32 %61, %62
  br i1 %cmp92, label %if.then93, label %if.end95

if.then93:                                        ; preds = %lor.lhs.false91, %lor.lhs.false89, %if.end87
  %call94 = call %class.int4* @_ZN4int4C2Eiiii(%class.int4* %agg.result, i32 -1, i32 -1, i32 -1, i32 -1)
  br label %return

if.end95:                                         ; preds = %lor.lhs.false91
  %63 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %64 = load i32, i32* %p3, align 4
  %arrayidx97 = getelementptr inbounds %class.btVector3, %class.btVector3* %63, i32 %64
  %65 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %66 = load i32, i32* %p0, align 4
  %arrayidx98 = getelementptr inbounds %class.btVector3, %class.btVector3* %65, i32 %66
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp96, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx97, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx98)
  %67 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %68 = load i32, i32* %p1, align 4
  %arrayidx101 = getelementptr inbounds %class.btVector3, %class.btVector3* %67, i32 %68
  %69 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %70 = load i32, i32* %p0, align 4
  %arrayidx102 = getelementptr inbounds %class.btVector3, %class.btVector3* %69, i32 %70
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp100, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx101, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx102)
  %71 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %72 = load i32, i32* %p2, align 4
  %arrayidx104 = getelementptr inbounds %class.btVector3, %class.btVector3* %71, i32 %72
  %73 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %74 = load i32, i32* %p0, align 4
  %arrayidx105 = getelementptr inbounds %class.btVector3, %class.btVector3* %73, i32 %74
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp103, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx104, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx105)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp99, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp100, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp103)
  %call106 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp96, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp99)
  %cmp107 = fcmp olt float %call106, 0.000000e+00
  br i1 %cmp107, label %if.then108, label %if.end109

if.then108:                                       ; preds = %if.end95
  call void @_Z6btSwapIiEvRT_S1_(i32* nonnull align 4 dereferenceable(4) %p2, i32* nonnull align 4 dereferenceable(4) %p3)
  br label %if.end109

if.end109:                                        ; preds = %if.then108, %if.end95
  %75 = load i32, i32* %p0, align 4
  %76 = load i32, i32* %p1, align 4
  %77 = load i32, i32* %p2, align 4
  %78 = load i32, i32* %p3, align 4
  %call110 = call %class.int4* @_ZN4int4C2Eiiii(%class.int4* %agg.result, i32 %75, i32 %76, i32 %77, i32 %78)
  br label %return

return:                                           ; preds = %if.end109, %if.then93, %if.then64, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE(%class.btVector3* %p, i32 %count, %class.btVector3* nonnull align 4 dereferenceable(16) %dir, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %allow) #2 comdat {
entry:
  %retval = alloca i32, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %count.addr = alloca i32, align 4
  %dir.addr = alloca %class.btVector3*, align 4
  %allow.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %m = alloca i32, align 4
  %u = alloca %class.btVector3, align 4
  %v = alloca %class.btVector3, align 4
  %ma = alloca i32, align 4
  %x = alloca float, align 4
  %s = alloca float, align 4
  %c = alloca float, align 4
  %mb = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  %mc = alloca i32, align 4
  %xx = alloca float, align 4
  %s25 = alloca float, align 4
  %c28 = alloca float, align 4
  %md = alloca i32, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %ref.tmp34 = alloca %class.btVector3, align 4
  %ref.tmp35 = alloca %class.btVector3, align 4
  %ref.tmp36 = alloca float, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store i32 %count, i32* %count.addr, align 4
  store %class.btVector3* %dir, %class.btVector3** %dir.addr, align 4
  store %class.btAlignedObjectArray.12* %allow, %class.btAlignedObjectArray.12** %allow.addr, align 4
  store i32 -1, i32* %m, align 4
  br label %while.cond

while.cond:                                       ; preds = %for.end47, %entry
  %0 = load i32, i32* %m, align 4
  %cmp = icmp eq i32 %0, -1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %1 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %2 = load i32, i32* %count.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4
  %4 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %call = call i32 @_Z14maxdirfilteredI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE(%class.btVector3* %1, i32 %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %4)
  store i32 %call, i32* %m, align 4
  %5 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %6 = load i32, i32* %m, align 4
  %call1 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %5, i32 %6)
  %7 = load i32, i32* %call1, align 4
  %cmp2 = icmp eq i32 %7, 3
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %while.body
  %8 = load i32, i32* %m, align 4
  store i32 %8, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %while.body
  %9 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4
  call void @_Z4orthRK9btVector3(%class.btVector3* sret align 4 %u, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %10 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %u, %class.btVector3* nonnull align 4 dereferenceable(16) %10)
  store i32 -1, i32* %ma, align 4
  store float 0.000000e+00, float* %x, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc45, %if.end
  %11 = load float, float* %x, align 4
  %cmp3 = fcmp ole float %11, 3.600000e+02
  br i1 %cmp3, label %for.body, label %for.end47

for.body:                                         ; preds = %for.cond
  %12 = load float, float* %x, align 4
  %mul = fmul float 0x3F91DF46A0000000, %12
  %call4 = call float @_Z5btSinf(float %mul)
  store float %call4, float* %s, align 4
  %13 = load float, float* %x, align 4
  %mul5 = fmul float 0x3F91DF46A0000000, %13
  %call6 = call float @_Z5btCosf(float %mul5)
  store float %call6, float* %c, align 4
  %14 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %15 = load i32, i32* %count.addr, align 4
  %16 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %u, float* nonnull align 4 dereferenceable(4) %s)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %c)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  store float 0x3F999999A0000000, float* %ref.tmp11, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %17 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %call12 = call i32 @_Z14maxdirfilteredI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE(%class.btVector3* %14, i32 %15, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %17)
  store i32 %call12, i32* %mb, align 4
  %18 = load i32, i32* %ma, align 4
  %19 = load i32, i32* %m, align 4
  %cmp13 = icmp eq i32 %18, %19
  br i1 %cmp13, label %land.lhs.true, label %if.end17

land.lhs.true:                                    ; preds = %for.body
  %20 = load i32, i32* %mb, align 4
  %21 = load i32, i32* %m, align 4
  %cmp14 = icmp eq i32 %20, %21
  br i1 %cmp14, label %if.then15, label %if.end17

if.then15:                                        ; preds = %land.lhs.true
  %22 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %23 = load i32, i32* %m, align 4
  %call16 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %22, i32 %23)
  store i32 3, i32* %call16, align 4
  %24 = load i32, i32* %m, align 4
  store i32 %24, i32* %retval, align 4
  br label %return

if.end17:                                         ; preds = %land.lhs.true, %for.body
  %25 = load i32, i32* %ma, align 4
  %cmp18 = icmp ne i32 %25, -1
  br i1 %cmp18, label %land.lhs.true19, label %if.end44

land.lhs.true19:                                  ; preds = %if.end17
  %26 = load i32, i32* %ma, align 4
  %27 = load i32, i32* %mb, align 4
  %cmp20 = icmp ne i32 %26, %27
  br i1 %cmp20, label %if.then21, label %if.end44

if.then21:                                        ; preds = %land.lhs.true19
  %28 = load i32, i32* %ma, align 4
  store i32 %28, i32* %mc, align 4
  %29 = load float, float* %x, align 4
  %sub = fsub float %29, 4.000000e+01
  store float %sub, float* %xx, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc, %if.then21
  %30 = load float, float* %xx, align 4
  %31 = load float, float* %x, align 4
  %cmp23 = fcmp ole float %30, %31
  br i1 %cmp23, label %for.body24, label %for.end

for.body24:                                       ; preds = %for.cond22
  %32 = load float, float* %xx, align 4
  %mul26 = fmul float 0x3F91DF46A0000000, %32
  %call27 = call float @_Z5btSinf(float %mul26)
  store float %call27, float* %s25, align 4
  %33 = load float, float* %xx, align 4
  %mul29 = fmul float 0x3F91DF46A0000000, %33
  %call30 = call float @_Z5btCosf(float %mul29)
  store float %call30, float* %c28, align 4
  %34 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %35 = load i32, i32* %count.addr, align 4
  %36 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp34, %class.btVector3* nonnull align 4 dereferenceable(16) %u, float* nonnull align 4 dereferenceable(4) %s25)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp35, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %c28)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp33, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp34, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp35)
  store float 0x3F999999A0000000, float* %ref.tmp36, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp36)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %36, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32)
  %37 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %call37 = call i32 @_Z14maxdirfilteredI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE(%class.btVector3* %34, i32 %35, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp31, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %37)
  store i32 %call37, i32* %md, align 4
  %38 = load i32, i32* %mc, align 4
  %39 = load i32, i32* %m, align 4
  %cmp38 = icmp eq i32 %38, %39
  br i1 %cmp38, label %land.lhs.true39, label %if.end43

land.lhs.true39:                                  ; preds = %for.body24
  %40 = load i32, i32* %md, align 4
  %41 = load i32, i32* %m, align 4
  %cmp40 = icmp eq i32 %40, %41
  br i1 %cmp40, label %if.then41, label %if.end43

if.then41:                                        ; preds = %land.lhs.true39
  %42 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %43 = load i32, i32* %m, align 4
  %call42 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %42, i32 %43)
  store i32 3, i32* %call42, align 4
  %44 = load i32, i32* %m, align 4
  store i32 %44, i32* %retval, align 4
  br label %return

if.end43:                                         ; preds = %land.lhs.true39, %for.body24
  %45 = load i32, i32* %md, align 4
  store i32 %45, i32* %mc, align 4
  br label %for.inc

for.inc:                                          ; preds = %if.end43
  %46 = load float, float* %xx, align 4
  %add = fadd float %46, 5.000000e+00
  store float %add, float* %xx, align 4
  br label %for.cond22

for.end:                                          ; preds = %for.cond22
  br label %if.end44

if.end44:                                         ; preds = %for.end, %land.lhs.true19, %if.end17
  %47 = load i32, i32* %mb, align 4
  store i32 %47, i32* %ma, align 4
  br label %for.inc45

for.inc45:                                        ; preds = %if.end44
  %48 = load float, float* %x, align 4
  %add46 = fadd float %48, 4.500000e+01
  store float %add46, float* %x, align 4
  br label %for.cond

for.end47:                                        ; preds = %for.cond
  %49 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %50 = load i32, i32* %m, align 4
  %call48 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %49, i32 %50)
  store i32 0, i32* %call48, align 4
  store i32 -1, i32* %m, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %51 = load i32, i32* %m, align 4
  store i32 %51, i32* %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.then41, %if.then15, %if.then
  %52 = load i32, i32* %retval, align 4
  ret i32 %52
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK9btVector3eqERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 3
  %2 = load float, float* %arrayidx3, align 4
  %cmp = fcmp oeq float %0, %2
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp oeq float %3, %5
  br i1 %cmp8, label %land.lhs.true9, label %land.end

land.lhs.true9:                                   ; preds = %land.lhs.true
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 1
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 1
  %8 = load float, float* %arrayidx13, align 4
  %cmp14 = fcmp oeq float %6, %8
  br i1 %cmp14, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true9
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 0
  %11 = load float, float* %arrayidx18, align 4
  %cmp19 = fcmp oeq float %9, %11
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true9, %land.lhs.true, %entry
  %12 = phi i1 [ false, %land.lhs.true9 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp19, %land.rhs ]
  ret i1 %12
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.int4* @_ZN4int4C2Eiiii(%class.int4* returned %this, i32 %_x, i32 %_y, i32 %_z, i32 %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.int4*, align 4
  %_x.addr = alloca i32, align 4
  %_y.addr = alloca i32, align 4
  %_z.addr = alloca i32, align 4
  %_w.addr = alloca i32, align 4
  store %class.int4* %this, %class.int4** %this.addr, align 4
  store i32 %_x, i32* %_x.addr, align 4
  store i32 %_y, i32* %_y.addr, align 4
  store i32 %_z, i32* %_z.addr, align 4
  store i32 %_w, i32* %_w.addr, align 4
  %this1 = load %class.int4*, %class.int4** %this.addr, align 4
  %0 = load i32, i32* %_x.addr, align 4
  %x = getelementptr inbounds %class.int4, %class.int4* %this1, i32 0, i32 0
  store i32 %0, i32* %x, align 4
  %1 = load i32, i32* %_y.addr, align 4
  %y = getelementptr inbounds %class.int4, %class.int4* %this1, i32 0, i32 1
  store i32 %1, i32* %y, align 4
  %2 = load i32, i32* %_z.addr, align 4
  %z = getelementptr inbounds %class.int4, %class.int4* %this1, i32 0, i32 2
  store i32 %2, i32* %z, align 4
  %3 = load i32, i32* %_w.addr, align 4
  %w = getelementptr inbounds %class.int4, %class.int4* %this1, i32 0, i32 3
  store i32 %3, i32* %w, align 4
  ret %class.int4* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z6btSwapIiEvRT_S1_(i32* nonnull align 4 dereferenceable(4) %a, i32* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca i32*, align 4
  %b.addr = alloca i32*, align 4
  %tmp = alloca i32, align 4
  store i32* %a, i32** %a.addr, align 4
  store i32* %b, i32** %b.addr, align 4
  %0 = load i32*, i32** %a.addr, align 4
  %1 = load i32, i32* %0, align 4
  store i32 %1, i32* %tmp, align 4
  %2 = load i32*, i32** %b.addr, align 4
  %3 = load i32, i32* %2, align 4
  %4 = load i32*, i32** %a.addr, align 4
  store i32 %3, i32* %4, align 4
  %5 = load i32, i32* %tmp, align 4
  %6 = load i32*, i32** %b.addr, align 4
  store i32 %5, i32* %6, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN11HullLibrary11calchullgenEP9btVector3ii(%class.HullLibrary* %this, %class.btVector3* %verts, i32 %verts_count, i32 %vlimit) #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.HullLibrary*, align 4
  %verts.addr = alloca %class.btVector3*, align 4
  %verts_count.addr = alloca i32, align 4
  %vlimit.addr = alloca i32, align 4
  %j = alloca i32, align 4
  %bmin = alloca %class.btVector3, align 4
  %bmax = alloca %class.btVector3, align 4
  %isextreme = alloca %class.btAlignedObjectArray.12, align 4
  %allow = alloca %class.btAlignedObjectArray.12, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp7 = alloca i32, align 4
  %epsilon = alloca float, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %p = alloca %class.int4, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %center = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp25 = alloca float, align 4
  %t0 = alloca %class.btHullTriangle*, align 4
  %ref.tmp30 = alloca %class.int3, align 4
  %t1 = alloca %class.btHullTriangle*, align 4
  %ref.tmp36 = alloca %class.int3, align 4
  %t2 = alloca %class.btHullTriangle*, align 4
  %ref.tmp43 = alloca %class.int3, align 4
  %t3 = alloca %class.btHullTriangle*, align 4
  %ref.tmp50 = alloca %class.int3, align 4
  %t = alloca %class.btHullTriangle*, align 4
  %n67 = alloca %class.btVector3, align 4
  %ref.tmp75 = alloca %class.btVector3, align 4
  %te = alloca %class.btHullTriangle*, align 4
  %v = alloca i32, align 4
  %t98 = alloca %class.int3, align 4
  %nt = alloca %class.int3, align 4
  %ref.tmp131 = alloca %class.btVector3, align 4
  %ref.tmp132 = alloca %class.btVector3, align 4
  %ref.tmp137 = alloca %class.btVector3, align 4
  %nb = alloca %class.btHullTriangle*, align 4
  %t163 = alloca %class.btHullTriangle*, align 4
  %n173 = alloca %class.btVector3, align 4
  %ref.tmp187 = alloca %class.btVector3, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store %class.btVector3* %verts, %class.btVector3** %verts.addr, align 4
  store i32 %verts_count, i32* %verts_count.addr, align 4
  store i32 %vlimit, i32* %vlimit.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %0 = load i32, i32* %verts_count.addr, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %1 = load i32, i32* %vlimit.addr, align 4
  %cmp2 = icmp eq i32 %1, 0
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  store i32 1000000000, i32* %vlimit.addr, align 4
  br label %if.end4

if.end4:                                          ; preds = %if.then3, %if.end
  %2 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %3 = bitcast %class.btVector3* %bmin to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %5 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %6 = bitcast %class.btVector3* %bmax to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %call = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.12* %isextreme)
  %8 = load i32, i32* %verts_count.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.12* %isextreme, i32 %8)
  %call5 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.12* %allow)
  %9 = load i32, i32* %verts_count.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.12* %allow, i32 %9)
  store i32 0, i32* %j, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end4
  %10 = load i32, i32* %j, align 4
  %11 = load i32, i32* %verts_count.addr, align 4
  %cmp6 = icmp slt i32 %10, %11
  br i1 %cmp6, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store i32 1, i32* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.12* %allow, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  store i32 0, i32* %ref.tmp7, align 4
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.12* %isextreme, i32* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %12 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %13 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 %13
  call void @_ZN9btVector36setMinERKS_(%class.btVector3* %bmin, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %14 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %15 = load i32, i32* %j, align 4
  %arrayidx8 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 %15
  call void @_ZN9btVector36setMaxERKS_(%class.btVector3* %bmax, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %16 = load i32, i32* %j, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %bmax, %class.btVector3* nonnull align 4 dereferenceable(16) %bmin)
  %call10 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp9)
  %mul = fmul float %call10, 0x3F50624DE0000000
  store float %mul, float* %epsilon, align 4
  %17 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %18 = load i32, i32* %verts_count.addr, align 4
  call void @_ZN11HullLibrary11FindSimplexEP9btVector3iR20btAlignedObjectArrayIiE(%class.int4* sret align 4 %p, %class.HullLibrary* %this1, %class.btVector3* %17, i32 %18, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %allow)
  %x = getelementptr inbounds %class.int4, %class.int4* %p, i32 0, i32 0
  %19 = load i32, i32* %x, align 4
  %cmp11 = icmp eq i32 %19, -1
  br i1 %cmp11, label %if.then12, label %if.end13

if.then12:                                        ; preds = %for.end
  store i32 0, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end13:                                         ; preds = %for.end
  %20 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 0)
  %21 = load i32, i32* %call17, align 4
  %arrayidx18 = getelementptr inbounds %class.btVector3, %class.btVector3* %20, i32 %21
  %22 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %call19 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 1)
  %23 = load i32, i32* %call19, align 4
  %arrayidx20 = getelementptr inbounds %class.btVector3, %class.btVector3* %22, i32 %23
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx18, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx20)
  %24 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 2)
  %25 = load i32, i32* %call21, align 4
  %arrayidx22 = getelementptr inbounds %class.btVector3, %class.btVector3* %24, i32 %25
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp16, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx22)
  %26 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 3)
  %27 = load i32, i32* %call23, align 4
  %arrayidx24 = getelementptr inbounds %class.btVector3, %class.btVector3* %26, i32 %27
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx24)
  store float 4.000000e+00, float* %ref.tmp25, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %center, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  %call26 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 2)
  %28 = load i32, i32* %call26, align 4
  %call27 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 3)
  %29 = load i32, i32* %call27, align 4
  %call28 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 1)
  %30 = load i32, i32* %call28, align 4
  %call29 = call %class.btHullTriangle* @_ZN11HullLibrary16allocateTriangleEiii(%class.HullLibrary* %this1, i32 %28, i32 %29, i32 %30)
  store %class.btHullTriangle* %call29, %class.btHullTriangle** %t0, align 4
  %call31 = call %class.int3* @_ZN4int3C2Eiii(%class.int3* %ref.tmp30, i32 2, i32 3, i32 1)
  %31 = load %class.btHullTriangle*, %class.btHullTriangle** %t0, align 4
  %n = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %31, i32 0, i32 1
  %32 = bitcast %class.int3* %n to i8*
  %33 = bitcast %class.int3* %ref.tmp30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 12, i1 false)
  %call32 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 3)
  %34 = load i32, i32* %call32, align 4
  %call33 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 2)
  %35 = load i32, i32* %call33, align 4
  %call34 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 0)
  %36 = load i32, i32* %call34, align 4
  %call35 = call %class.btHullTriangle* @_ZN11HullLibrary16allocateTriangleEiii(%class.HullLibrary* %this1, i32 %34, i32 %35, i32 %36)
  store %class.btHullTriangle* %call35, %class.btHullTriangle** %t1, align 4
  %call37 = call %class.int3* @_ZN4int3C2Eiii(%class.int3* %ref.tmp36, i32 3, i32 2, i32 0)
  %37 = load %class.btHullTriangle*, %class.btHullTriangle** %t1, align 4
  %n38 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %37, i32 0, i32 1
  %38 = bitcast %class.int3* %n38 to i8*
  %39 = bitcast %class.int3* %ref.tmp36 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %38, i8* align 4 %39, i32 12, i1 false)
  %call39 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 0)
  %40 = load i32, i32* %call39, align 4
  %call40 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 1)
  %41 = load i32, i32* %call40, align 4
  %call41 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 3)
  %42 = load i32, i32* %call41, align 4
  %call42 = call %class.btHullTriangle* @_ZN11HullLibrary16allocateTriangleEiii(%class.HullLibrary* %this1, i32 %40, i32 %41, i32 %42)
  store %class.btHullTriangle* %call42, %class.btHullTriangle** %t2, align 4
  %call44 = call %class.int3* @_ZN4int3C2Eiii(%class.int3* %ref.tmp43, i32 0, i32 1, i32 3)
  %43 = load %class.btHullTriangle*, %class.btHullTriangle** %t2, align 4
  %n45 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %43, i32 0, i32 1
  %44 = bitcast %class.int3* %n45 to i8*
  %45 = bitcast %class.int3* %ref.tmp43 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %44, i8* align 4 %45, i32 12, i1 false)
  %call46 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 1)
  %46 = load i32, i32* %call46, align 4
  %call47 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 0)
  %47 = load i32, i32* %call47, align 4
  %call48 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 2)
  %48 = load i32, i32* %call48, align 4
  %call49 = call %class.btHullTriangle* @_ZN11HullLibrary16allocateTriangleEiii(%class.HullLibrary* %this1, i32 %46, i32 %47, i32 %48)
  store %class.btHullTriangle* %call49, %class.btHullTriangle** %t3, align 4
  %call51 = call %class.int3* @_ZN4int3C2Eiii(%class.int3* %ref.tmp50, i32 1, i32 0, i32 2)
  %49 = load %class.btHullTriangle*, %class.btHullTriangle** %t3, align 4
  %n52 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %49, i32 0, i32 1
  %50 = bitcast %class.int3* %n52 to i8*
  %51 = bitcast %class.int3* %ref.tmp50 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %50, i8* align 4 %51, i32 12, i1 false)
  %call53 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 3)
  %52 = load i32, i32* %call53, align 4
  %call54 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %isextreme, i32 %52)
  store i32 1, i32* %call54, align 4
  %call55 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 2)
  %53 = load i32, i32* %call55, align 4
  %call56 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %isextreme, i32 %53)
  store i32 1, i32* %call56, align 4
  %call57 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 1)
  %54 = load i32, i32* %call57, align 4
  %call58 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %isextreme, i32 %54)
  store i32 1, i32* %call58, align 4
  %call59 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %p, i32 0)
  %55 = load i32, i32* %call59, align 4
  %call60 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %isextreme, i32 %55)
  store i32 1, i32* %call60, align 4
  %56 = load %class.btHullTriangle*, %class.btHullTriangle** %t0, align 4
  call void @_ZN11HullLibrary7checkitEP14btHullTriangle(%class.HullLibrary* %this1, %class.btHullTriangle* %56)
  %57 = load %class.btHullTriangle*, %class.btHullTriangle** %t1, align 4
  call void @_ZN11HullLibrary7checkitEP14btHullTriangle(%class.HullLibrary* %this1, %class.btHullTriangle* %57)
  %58 = load %class.btHullTriangle*, %class.btHullTriangle** %t2, align 4
  call void @_ZN11HullLibrary7checkitEP14btHullTriangle(%class.HullLibrary* %this1, %class.btHullTriangle* %58)
  %59 = load %class.btHullTriangle*, %class.btHullTriangle** %t3, align 4
  call void @_ZN11HullLibrary7checkitEP14btHullTriangle(%class.HullLibrary* %this1, %class.btHullTriangle* %59)
  store i32 0, i32* %j, align 4
  br label %for.cond61

for.cond61:                                       ; preds = %for.inc81, %if.end13
  %60 = load i32, i32* %j, align 4
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call62 = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %m_tris)
  %cmp63 = icmp slt i32 %60, %call62
  br i1 %cmp63, label %for.body64, label %for.end83

for.body64:                                       ; preds = %for.cond61
  %m_tris65 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %61 = load i32, i32* %j, align 4
  %call66 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris65, i32 %61)
  %62 = load %class.btHullTriangle*, %class.btHullTriangle** %call66, align 4
  store %class.btHullTriangle* %62, %class.btHullTriangle** %t, align 4
  %63 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %64 = load %class.btHullTriangle*, %class.btHullTriangle** %t, align 4
  %65 = bitcast %class.btHullTriangle* %64 to %class.int3*
  %call68 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %65, i32 0)
  %66 = load i32, i32* %call68, align 4
  %arrayidx69 = getelementptr inbounds %class.btVector3, %class.btVector3* %63, i32 %66
  %67 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %68 = load %class.btHullTriangle*, %class.btHullTriangle** %t, align 4
  %69 = bitcast %class.btHullTriangle* %68 to %class.int3*
  %call70 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %69, i32 1)
  %70 = load i32, i32* %call70, align 4
  %arrayidx71 = getelementptr inbounds %class.btVector3, %class.btVector3* %67, i32 %70
  %71 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %72 = load %class.btHullTriangle*, %class.btHullTriangle** %t, align 4
  %73 = bitcast %class.btHullTriangle* %72 to %class.int3*
  %call72 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %73, i32 2)
  %74 = load i32, i32* %call72, align 4
  %arrayidx73 = getelementptr inbounds %class.btVector3, %class.btVector3* %71, i32 %74
  call void @_Z9TriNormalRK9btVector3S1_S1_(%class.btVector3* sret align 4 %n67, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx69, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx71, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx73)
  %75 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %76 = load i32, i32* %verts_count.addr, align 4
  %call74 = call i32 @_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE(%class.btVector3* %75, i32 %76, %class.btVector3* nonnull align 4 dereferenceable(16) %n67, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %allow)
  %77 = load %class.btHullTriangle*, %class.btHullTriangle** %t, align 4
  %vmax = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %77, i32 0, i32 3
  store i32 %call74, i32* %vmax, align 4
  %78 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %79 = load %class.btHullTriangle*, %class.btHullTriangle** %t, align 4
  %vmax76 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %79, i32 0, i32 3
  %80 = load i32, i32* %vmax76, align 4
  %arrayidx77 = getelementptr inbounds %class.btVector3, %class.btVector3* %78, i32 %80
  %81 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %82 = load %class.btHullTriangle*, %class.btHullTriangle** %t, align 4
  %83 = bitcast %class.btHullTriangle* %82 to %class.int3*
  %call78 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %83, i32 0)
  %84 = load i32, i32* %call78, align 4
  %arrayidx79 = getelementptr inbounds %class.btVector3, %class.btVector3* %81, i32 %84
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp75, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx77, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx79)
  %call80 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %n67, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp75)
  %85 = load %class.btHullTriangle*, %class.btHullTriangle** %t, align 4
  %rise = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %85, i32 0, i32 4
  store float %call80, float* %rise, align 4
  br label %for.inc81

for.inc81:                                        ; preds = %for.body64
  %86 = load i32, i32* %j, align 4
  %inc82 = add nsw i32 %86, 1
  store i32 %inc82, i32* %j, align 4
  br label %for.cond61

for.end83:                                        ; preds = %for.cond61
  %87 = load i32, i32* %vlimit.addr, align 4
  %sub = sub nsw i32 %87, 4
  store i32 %sub, i32* %vlimit.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.end195, %for.end83
  %88 = load i32, i32* %vlimit.addr, align 4
  %cmp84 = icmp sgt i32 %88, 0
  br i1 %cmp84, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %89 = load float, float* %epsilon, align 4
  %call85 = call %class.btHullTriangle* @_ZN11HullLibrary10extrudableEf(%class.HullLibrary* %this1, float %89)
  store %class.btHullTriangle* %call85, %class.btHullTriangle** %te, align 4
  %cmp86 = icmp ne %class.btHullTriangle* %call85, null
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %90 = phi i1 [ false, %while.cond ], [ %cmp86, %land.rhs ]
  br i1 %90, label %while.body, label %while.end197

while.body:                                       ; preds = %land.end
  %91 = load %class.btHullTriangle*, %class.btHullTriangle** %te, align 4
  %vmax87 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %91, i32 0, i32 3
  %92 = load i32, i32* %vmax87, align 4
  store i32 %92, i32* %v, align 4
  %93 = load i32, i32* %v, align 4
  %call88 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %isextreme, i32 %93)
  store i32 1, i32* %call88, align 4
  %m_tris89 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call90 = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %m_tris89)
  store i32 %call90, i32* %j, align 4
  br label %while.cond91

while.cond91:                                     ; preds = %if.end108, %if.then96, %while.body
  %94 = load i32, i32* %j, align 4
  %dec = add nsw i32 %94, -1
  store i32 %dec, i32* %j, align 4
  %tobool = icmp ne i32 %94, 0
  br i1 %tobool, label %while.body92, label %while.end

while.body92:                                     ; preds = %while.cond91
  %m_tris93 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %95 = load i32, i32* %j, align 4
  %call94 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris93, i32 %95)
  %96 = load %class.btHullTriangle*, %class.btHullTriangle** %call94, align 4
  %tobool95 = icmp ne %class.btHullTriangle* %96, null
  br i1 %tobool95, label %if.end97, label %if.then96

if.then96:                                        ; preds = %while.body92
  br label %while.cond91

if.end97:                                         ; preds = %while.body92
  %m_tris99 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %97 = load i32, i32* %j, align 4
  %call100 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris99, i32 %97)
  %98 = load %class.btHullTriangle*, %class.btHullTriangle** %call100, align 4
  %99 = bitcast %class.btHullTriangle* %98 to %class.int3*
  %100 = bitcast %class.int3* %t98 to i8*
  %101 = bitcast %class.int3* %99 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %100, i8* align 4 %101, i32 12, i1 false)
  %102 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %103 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %104 = load i32, i32* %v, align 4
  %arrayidx101 = getelementptr inbounds %class.btVector3, %class.btVector3* %103, i32 %104
  %105 = load float, float* %epsilon, align 4
  %mul102 = fmul float 0x3F847AE140000000, %105
  %call103 = call i32 @_Z5aboveP9btVector3RK4int3RKS_f(%class.btVector3* %102, %class.int3* nonnull align 4 dereferenceable(12) %t98, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx101, float %mul102)
  %tobool104 = icmp ne i32 %call103, 0
  br i1 %tobool104, label %if.then105, label %if.end108

if.then105:                                       ; preds = %if.end97
  %m_tris106 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %106 = load i32, i32* %j, align 4
  %call107 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris106, i32 %106)
  %107 = load %class.btHullTriangle*, %class.btHullTriangle** %call107, align 4
  %108 = load i32, i32* %v, align 4
  call void @_ZN11HullLibrary7extrudeEP14btHullTrianglei(%class.HullLibrary* %this1, %class.btHullTriangle* %107, i32 %108)
  br label %if.end108

if.end108:                                        ; preds = %if.then105, %if.end97
  br label %while.cond91

while.end:                                        ; preds = %while.cond91
  %m_tris109 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call110 = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %m_tris109)
  store i32 %call110, i32* %j, align 4
  br label %while.cond111

while.cond111:                                    ; preds = %if.end155, %if.then118, %while.end
  %109 = load i32, i32* %j, align 4
  %dec112 = add nsw i32 %109, -1
  store i32 %dec112, i32* %j, align 4
  %tobool113 = icmp ne i32 %109, 0
  br i1 %tobool113, label %while.body114, label %while.end156

while.body114:                                    ; preds = %while.cond111
  %m_tris115 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %110 = load i32, i32* %j, align 4
  %call116 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris115, i32 %110)
  %111 = load %class.btHullTriangle*, %class.btHullTriangle** %call116, align 4
  %tobool117 = icmp ne %class.btHullTriangle* %111, null
  br i1 %tobool117, label %if.end119, label %if.then118

if.then118:                                       ; preds = %while.body114
  br label %while.cond111

if.end119:                                        ; preds = %while.body114
  %m_tris120 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %112 = load i32, i32* %j, align 4
  %call121 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris120, i32 %112)
  %113 = load %class.btHullTriangle*, %class.btHullTriangle** %call121, align 4
  %114 = bitcast %class.btHullTriangle* %113 to %class.int3*
  %115 = load i32, i32* %v, align 4
  %call122 = call i32 @_Z7hasvertRK4int3i(%class.int3* nonnull align 4 dereferenceable(12) %114, i32 %115)
  %tobool123 = icmp ne i32 %call122, 0
  br i1 %tobool123, label %if.end125, label %if.then124

if.then124:                                       ; preds = %if.end119
  br label %while.end156

if.end125:                                        ; preds = %if.end119
  %m_tris126 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %116 = load i32, i32* %j, align 4
  %call127 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris126, i32 %116)
  %117 = load %class.btHullTriangle*, %class.btHullTriangle** %call127, align 4
  %118 = bitcast %class.btHullTriangle* %117 to %class.int3*
  %119 = bitcast %class.int3* %nt to i8*
  %120 = bitcast %class.int3* %118 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %119, i8* align 4 %120, i32 12, i1 false)
  %121 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %122 = load float, float* %epsilon, align 4
  %mul128 = fmul float 0x3F847AE140000000, %122
  %call129 = call i32 @_Z5aboveP9btVector3RK4int3RKS_f(%class.btVector3* %121, %class.int3* nonnull align 4 dereferenceable(12) %nt, %class.btVector3* nonnull align 4 dereferenceable(16) %center, float %mul128)
  %tobool130 = icmp ne i32 %call129, 0
  br i1 %tobool130, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %if.end125
  %123 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %call133 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %nt, i32 1)
  %124 = load i32, i32* %call133, align 4
  %arrayidx134 = getelementptr inbounds %class.btVector3, %class.btVector3* %123, i32 %124
  %125 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %call135 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %nt, i32 0)
  %126 = load i32, i32* %call135, align 4
  %arrayidx136 = getelementptr inbounds %class.btVector3, %class.btVector3* %125, i32 %126
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp132, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx134, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx136)
  %127 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %call138 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %nt, i32 2)
  %128 = load i32, i32* %call138, align 4
  %arrayidx139 = getelementptr inbounds %class.btVector3, %class.btVector3* %127, i32 %128
  %129 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %call140 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %nt, i32 1)
  %130 = load i32, i32* %call140, align 4
  %arrayidx141 = getelementptr inbounds %class.btVector3, %class.btVector3* %129, i32 %130
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp137, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx139, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx141)
  call void @_Z7btCrossRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp131, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp132, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp137)
  %call142 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp131)
  %131 = load float, float* %epsilon, align 4
  %132 = load float, float* %epsilon, align 4
  %mul143 = fmul float %131, %132
  %mul144 = fmul float %mul143, 0x3FB99999A0000000
  %cmp145 = fcmp olt float %call142, %mul144
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %if.end125
  %133 = phi i1 [ true, %if.end125 ], [ %cmp145, %lor.rhs ]
  br i1 %133, label %if.then146, label %if.end155

if.then146:                                       ; preds = %lor.end
  %m_tris147 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %m_tris148 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %134 = load i32, i32* %j, align 4
  %call149 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris148, i32 %134)
  %135 = load %class.btHullTriangle*, %class.btHullTriangle** %call149, align 4
  %n150 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %135, i32 0, i32 1
  %call151 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %n150, i32 0)
  %136 = load i32, i32* %call151, align 4
  %call152 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris147, i32 %136)
  %137 = load %class.btHullTriangle*, %class.btHullTriangle** %call152, align 4
  store %class.btHullTriangle* %137, %class.btHullTriangle** %nb, align 4
  %138 = load %class.btHullTriangle*, %class.btHullTriangle** %nb, align 4
  %139 = load i32, i32* %v, align 4
  call void @_ZN11HullLibrary7extrudeEP14btHullTrianglei(%class.HullLibrary* %this1, %class.btHullTriangle* %138, i32 %139)
  %m_tris153 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call154 = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %m_tris153)
  store i32 %call154, i32* %j, align 4
  br label %if.end155

if.end155:                                        ; preds = %if.then146, %lor.end
  br label %while.cond111

while.end156:                                     ; preds = %if.then124, %while.cond111
  %m_tris157 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call158 = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %m_tris157)
  store i32 %call158, i32* %j, align 4
  br label %while.cond159

while.cond159:                                    ; preds = %if.end194, %if.then167, %while.end156
  %140 = load i32, i32* %j, align 4
  %dec160 = add nsw i32 %140, -1
  store i32 %dec160, i32* %j, align 4
  %tobool161 = icmp ne i32 %140, 0
  br i1 %tobool161, label %while.body162, label %while.end195

while.body162:                                    ; preds = %while.cond159
  %m_tris164 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %141 = load i32, i32* %j, align 4
  %call165 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris164, i32 %141)
  %142 = load %class.btHullTriangle*, %class.btHullTriangle** %call165, align 4
  store %class.btHullTriangle* %142, %class.btHullTriangle** %t163, align 4
  %143 = load %class.btHullTriangle*, %class.btHullTriangle** %t163, align 4
  %tobool166 = icmp ne %class.btHullTriangle* %143, null
  br i1 %tobool166, label %if.end168, label %if.then167

if.then167:                                       ; preds = %while.body162
  br label %while.cond159

if.end168:                                        ; preds = %while.body162
  %144 = load %class.btHullTriangle*, %class.btHullTriangle** %t163, align 4
  %vmax169 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %144, i32 0, i32 3
  %145 = load i32, i32* %vmax169, align 4
  %cmp170 = icmp sge i32 %145, 0
  br i1 %cmp170, label %if.then171, label %if.end172

if.then171:                                       ; preds = %if.end168
  br label %while.end195

if.end172:                                        ; preds = %if.end168
  %146 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %147 = load %class.btHullTriangle*, %class.btHullTriangle** %t163, align 4
  %148 = bitcast %class.btHullTriangle* %147 to %class.int3*
  %call174 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %148, i32 0)
  %149 = load i32, i32* %call174, align 4
  %arrayidx175 = getelementptr inbounds %class.btVector3, %class.btVector3* %146, i32 %149
  %150 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %151 = load %class.btHullTriangle*, %class.btHullTriangle** %t163, align 4
  %152 = bitcast %class.btHullTriangle* %151 to %class.int3*
  %call176 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %152, i32 1)
  %153 = load i32, i32* %call176, align 4
  %arrayidx177 = getelementptr inbounds %class.btVector3, %class.btVector3* %150, i32 %153
  %154 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %155 = load %class.btHullTriangle*, %class.btHullTriangle** %t163, align 4
  %156 = bitcast %class.btHullTriangle* %155 to %class.int3*
  %call178 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %156, i32 2)
  %157 = load i32, i32* %call178, align 4
  %arrayidx179 = getelementptr inbounds %class.btVector3, %class.btVector3* %154, i32 %157
  call void @_Z9TriNormalRK9btVector3S1_S1_(%class.btVector3* sret align 4 %n173, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx175, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx177, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx179)
  %158 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %159 = load i32, i32* %verts_count.addr, align 4
  %call180 = call i32 @_Z12maxdirsteridI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE(%class.btVector3* %158, i32 %159, %class.btVector3* nonnull align 4 dereferenceable(16) %n173, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %allow)
  %160 = load %class.btHullTriangle*, %class.btHullTriangle** %t163, align 4
  %vmax181 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %160, i32 0, i32 3
  store i32 %call180, i32* %vmax181, align 4
  %161 = load %class.btHullTriangle*, %class.btHullTriangle** %t163, align 4
  %vmax182 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %161, i32 0, i32 3
  %162 = load i32, i32* %vmax182, align 4
  %call183 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %isextreme, i32 %162)
  %163 = load i32, i32* %call183, align 4
  %tobool184 = icmp ne i32 %163, 0
  br i1 %tobool184, label %if.then185, label %if.else

if.then185:                                       ; preds = %if.end172
  %164 = load %class.btHullTriangle*, %class.btHullTriangle** %t163, align 4
  %vmax186 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %164, i32 0, i32 3
  store i32 -1, i32* %vmax186, align 4
  br label %if.end194

if.else:                                          ; preds = %if.end172
  %165 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %166 = load %class.btHullTriangle*, %class.btHullTriangle** %t163, align 4
  %vmax188 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %166, i32 0, i32 3
  %167 = load i32, i32* %vmax188, align 4
  %arrayidx189 = getelementptr inbounds %class.btVector3, %class.btVector3* %165, i32 %167
  %168 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %169 = load %class.btHullTriangle*, %class.btHullTriangle** %t163, align 4
  %170 = bitcast %class.btHullTriangle* %169 to %class.int3*
  %call190 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %170, i32 0)
  %171 = load i32, i32* %call190, align 4
  %arrayidx191 = getelementptr inbounds %class.btVector3, %class.btVector3* %168, i32 %171
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp187, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx189, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx191)
  %call192 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %n173, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp187)
  %172 = load %class.btHullTriangle*, %class.btHullTriangle** %t163, align 4
  %rise193 = getelementptr inbounds %class.btHullTriangle, %class.btHullTriangle* %172, i32 0, i32 4
  store float %call192, float* %rise193, align 4
  br label %if.end194

if.end194:                                        ; preds = %if.else, %if.then185
  br label %while.cond159

while.end195:                                     ; preds = %if.then171, %while.cond159
  %173 = load i32, i32* %vlimit.addr, align 4
  %dec196 = add nsw i32 %173, -1
  store i32 %dec196, i32* %vlimit.addr, align 4
  br label %while.cond

while.end197:                                     ; preds = %land.end
  store i32 1, i32* %retval, align 4
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end197, %if.then12
  %call198 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.12* %allow) #7
  %call200 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.12* %isextreme) #7
  br label %return

return:                                           ; preds = %cleanup, %if.then
  %174 = load i32, i32* %retval, align 4
  ret i32 %174
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.13* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.12* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.12* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i32*
  store i32* %2, i32** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %3 = load i32*, i32** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call3, i32* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i32*, i32** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store i32* %4, i32** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.12* %this, i32* nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Val.addr = alloca i32*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32* %_Val, i32** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.12* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.12* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %1 = load i32*, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  %3 = bitcast i32* %arrayidx to i8*
  %4 = bitcast i8* %3 to i32*
  %5 = load i32*, i32** %_Val.addr, align 4
  %6 = load i32, i32* %5, align 4
  store i32 %6, i32* %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector36setMinERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector36setMaxERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %other.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %other, %class.btVector3** %other.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx3)
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_floats8 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [4 x float], [4 x float]* %m_floats8, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 3
  %3 = load %class.btVector3*, %class.btVector3** %other.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %3)
  call void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  %2 = load float, float* %1, align 4
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN4int4ixEi(%class.int4* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.int4*, align 4
  %i.addr = alloca i32, align 4
  store %class.int4* %this, %class.int4** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.int4*, %class.int4** %this.addr, align 4
  %x = getelementptr inbounds %class.int4, %class.int4* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %x, i32 %0
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN11HullLibrary8calchullEP9btVector3iR20btAlignedObjectArrayIjERii(%class.HullLibrary* %this, %class.btVector3* %verts, i32 %verts_count, %class.btAlignedObjectArray.16* nonnull align 4 dereferenceable(17) %tris_out, i32* nonnull align 4 dereferenceable(4) %tris_count, i32 %vlimit) #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.HullLibrary*, align 4
  %verts.addr = alloca %class.btVector3*, align 4
  %verts_count.addr = alloca i32, align 4
  %tris_out.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %tris_count.addr = alloca i32*, align 4
  %vlimit.addr = alloca i32, align 4
  %rc = alloca i32, align 4
  %ts = alloca %class.btAlignedObjectArray.12, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp32 = alloca %class.btHullTriangle*, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store %class.btVector3* %verts, %class.btVector3** %verts.addr, align 4
  store i32 %verts_count, i32* %verts_count.addr, align 4
  store %class.btAlignedObjectArray.16* %tris_out, %class.btAlignedObjectArray.16** %tris_out.addr, align 4
  store i32* %tris_count, i32** %tris_count.addr, align 4
  store i32 %vlimit, i32* %vlimit.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %1 = load i32, i32* %verts_count.addr, align 4
  %2 = load i32, i32* %vlimit.addr, align 4
  %call = call i32 @_ZN11HullLibrary11calchullgenEP9btVector3ii(%class.HullLibrary* %this1, %class.btVector3* %0, i32 %1, i32 %2)
  store i32 %call, i32* %rc, align 4
  %3 = load i32, i32* %rc, align 4
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i32 0, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %call2 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.12* %ts)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc17, %if.end
  %4 = load i32, i32* %i, align 4
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %m_tris)
  %cmp = icmp slt i32 %4, %call3
  br i1 %cmp, label %for.body, label %for.end19

for.body:                                         ; preds = %for.cond
  %m_tris4 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %5 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris4, i32 %5)
  %6 = load %class.btHullTriangle*, %class.btHullTriangle** %call5, align 4
  %tobool6 = icmp ne %class.btHullTriangle* %6, null
  br i1 %tobool6, label %if.then7, label %if.end16

if.then7:                                         ; preds = %for.body
  store i32 0, i32* %j, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %if.then7
  %7 = load i32, i32* %j, align 4
  %cmp9 = icmp slt i32 %7, 3
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %m_tris11 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %8 = load i32, i32* %i, align 4
  %call12 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris11, i32 %8)
  %9 = load %class.btHullTriangle*, %class.btHullTriangle** %call12, align 4
  %10 = bitcast %class.btHullTriangle* %9 to %class.int3*
  %11 = load i32, i32* %j, align 4
  %call13 = call nonnull align 4 dereferenceable(4) i32* @_ZN4int3ixEi(%class.int3* %10, i32 %11)
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.12* %ts, i32* nonnull align 4 dereferenceable(4) %call13)
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %12 = load i32, i32* %j, align 4
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  %m_tris14 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %13 = load i32, i32* %i, align 4
  %call15 = call nonnull align 4 dereferenceable(4) %class.btHullTriangle** @_ZN20btAlignedObjectArrayIP14btHullTriangleEixEi(%class.btAlignedObjectArray.8* %m_tris14, i32 %13)
  %14 = load %class.btHullTriangle*, %class.btHullTriangle** %call15, align 4
  call void @_ZN11HullLibrary18deAllocateTriangleEP14btHullTriangle(%class.HullLibrary* %this1, %class.btHullTriangle* %14)
  br label %if.end16

if.end16:                                         ; preds = %for.end, %for.body
  br label %for.inc17

for.inc17:                                        ; preds = %if.end16
  %15 = load i32, i32* %i, align 4
  %inc18 = add nsw i32 %15, 1
  store i32 %inc18, i32* %i, align 4
  br label %for.cond

for.end19:                                        ; preds = %for.cond
  %call20 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.12* %ts)
  %div = sdiv i32 %call20, 3
  %16 = load i32*, i32** %tris_count.addr, align 4
  store i32 %div, i32* %16, align 4
  %17 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %tris_out.addr, align 4
  %call21 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.12* %ts)
  store i32 0, i32* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIjE6resizeEiRKj(%class.btAlignedObjectArray.16* %17, i32 %call21, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  store i32 0, i32* %i, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc28, %for.end19
  %18 = load i32, i32* %i, align 4
  %call23 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.12* %ts)
  %cmp24 = icmp slt i32 %18, %call23
  br i1 %cmp24, label %for.body25, label %for.end30

for.body25:                                       ; preds = %for.cond22
  %19 = load i32, i32* %i, align 4
  %call26 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %ts, i32 %19)
  %20 = load i32, i32* %call26, align 4
  %21 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %tris_out.addr, align 4
  %22 = load i32, i32* %i, align 4
  %call27 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.16* %21, i32 %22)
  store i32 %20, i32* %call27, align 4
  br label %for.inc28

for.inc28:                                        ; preds = %for.body25
  %23 = load i32, i32* %i, align 4
  %inc29 = add nsw i32 %23, 1
  store i32 %inc29, i32* %i, align 4
  br label %for.cond22

for.end30:                                        ; preds = %for.cond22
  %m_tris31 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  store %class.btHullTriangle* null, %class.btHullTriangle** %ref.tmp32, align 4
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE6resizeEiRKS1_(%class.btAlignedObjectArray.8* %m_tris31, i32 0, %class.btHullTriangle** nonnull align 4 dereferenceable(4) %ref.tmp32)
  store i32 1, i32* %retval, align 4
  %call33 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.12* %ts) #7
  br label %return

return:                                           ; preds = %for.end30, %if.then
  %24 = load i32, i32* %retval, align 4
  ret i32 %24
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE6resizeEiRKj(%class.btAlignedObjectArray.16* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store i32* %fillData, i32** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %5 = load i32*, i32** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIjE7reserveEi(%class.btAlignedObjectArray.16* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %14 = load i32*, i32** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %14, i32 %15
  %16 = bitcast i32* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to i32*
  %18 = load i32*, i32** %fillData.addr, align 4
  %19 = load i32, i32* %18, align 4
  store i32 %19, i32* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.16* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE6resizeEiRKS1_(%class.btAlignedObjectArray.8* %this, i32 %newsize, %class.btHullTriangle** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btHullTriangle**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btHullTriangle** %fillData, %class.btHullTriangle*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %5 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btHullTriangle*, %class.btHullTriangle** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %14 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btHullTriangle*, %class.btHullTriangle** %14, i32 %15
  %16 = bitcast %class.btHullTriangle** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btHullTriangle**
  %18 = load %class.btHullTriangle**, %class.btHullTriangle*** %fillData.addr, align 4
  %19 = load %class.btHullTriangle*, %class.btHullTriangle** %18, align 4
  store %class.btHullTriangle* %19, %class.btHullTriangle** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN11HullLibrary11ComputeHullEjPK9btVector3R11PHullResultj(%class.HullLibrary* %this, i32 %vcount, %class.btVector3* %vertices, %class.PHullResult* nonnull align 4 dereferenceable(36) %result, i32 %vlimit) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.HullLibrary*, align 4
  %vcount.addr = alloca i32, align 4
  %vertices.addr = alloca %class.btVector3*, align 4
  %result.addr = alloca %class.PHullResult*, align 4
  %vlimit.addr = alloca i32, align 4
  %tris_count = alloca i32, align 4
  %ret = alloca i32, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store i32 %vcount, i32* %vcount.addr, align 4
  store %class.btVector3* %vertices, %class.btVector3** %vertices.addr, align 4
  store %class.PHullResult* %result, %class.PHullResult** %result.addr, align 4
  store i32 %vlimit, i32* %vlimit.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %1 = load i32, i32* %vcount.addr, align 4
  %2 = load %class.PHullResult*, %class.PHullResult** %result.addr, align 4
  %m_Indices = getelementptr inbounds %class.PHullResult, %class.PHullResult* %2, i32 0, i32 4
  %3 = load i32, i32* %vlimit.addr, align 4
  %call = call i32 @_ZN11HullLibrary8calchullEP9btVector3iR20btAlignedObjectArrayIjERii(%class.HullLibrary* %this1, %class.btVector3* %0, i32 %1, %class.btAlignedObjectArray.16* nonnull align 4 dereferenceable(17) %m_Indices, i32* nonnull align 4 dereferenceable(4) %tris_count, i32 %3)
  store i32 %call, i32* %ret, align 4
  %4 = load i32, i32* %ret, align 4
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %5 = load i32, i32* %tris_count, align 4
  %mul = mul nsw i32 %5, 3
  %6 = load %class.PHullResult*, %class.PHullResult** %result.addr, align 4
  %mIndexCount = getelementptr inbounds %class.PHullResult, %class.PHullResult* %6, i32 0, i32 1
  store i32 %mul, i32* %mIndexCount, align 4
  %7 = load i32, i32* %tris_count, align 4
  %8 = load %class.PHullResult*, %class.PHullResult** %result.addr, align 4
  %mFaceCount = getelementptr inbounds %class.PHullResult, %class.PHullResult* %8, i32 0, i32 2
  store i32 %7, i32* %mFaceCount, align 4
  %9 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %10 = load %class.PHullResult*, %class.PHullResult** %result.addr, align 4
  %mVertices = getelementptr inbounds %class.PHullResult, %class.PHullResult* %10, i32 0, i32 3
  store %class.btVector3* %9, %class.btVector3** %mVertices, align 4
  %11 = load i32, i32* %vcount.addr, align 4
  %12 = load %class.PHullResult*, %class.PHullResult** %result.addr, align 4
  %mVcount = getelementptr inbounds %class.PHullResult, %class.PHullResult* %12, i32 0, i32 0
  store i32 %11, i32* %mVcount, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %13 = load i1, i1* %retval, align 1
  ret i1 %13
}

; Function Attrs: noinline optnone
define hidden void @_Z11ReleaseHullR11PHullResult(%class.PHullResult* nonnull align 4 dereferenceable(36) %result) #2 {
entry:
  %result.addr = alloca %class.PHullResult*, align 4
  store %class.PHullResult* %result, %class.PHullResult** %result.addr, align 4
  %0 = load %class.PHullResult*, %class.PHullResult** %result.addr, align 4
  %m_Indices = getelementptr inbounds %class.PHullResult, %class.PHullResult* %0, i32 0, i32 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.16* %m_Indices)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.PHullResult*, %class.PHullResult** %result.addr, align 4
  %m_Indices1 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %1, i32 0, i32 4
  call void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.16* %m_Indices1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load %class.PHullResult*, %class.PHullResult** %result.addr, align 4
  %mVcount = getelementptr inbounds %class.PHullResult, %class.PHullResult* %2, i32 0, i32 0
  store i32 0, i32* %mVcount, align 4
  %3 = load %class.PHullResult*, %class.PHullResult** %result.addr, align 4
  %mIndexCount = getelementptr inbounds %class.PHullResult, %class.PHullResult* %3, i32 0, i32 1
  store i32 0, i32* %mIndexCount, align 4
  %4 = load %class.PHullResult*, %class.PHullResult** %result.addr, align 4
  %mVertices = getelementptr inbounds %class.PHullResult, %class.PHullResult* %4, i32 0, i32 3
  store %class.btVector3* null, %class.btVector3** %mVertices, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.16* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.16* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.16* %this1)
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult(%class.HullLibrary* %this, %class.HullDesc* nonnull align 4 dereferenceable(28) %desc, %class.HullResult* nonnull align 4 dereferenceable(56) %result) #2 {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  %desc.addr = alloca %class.HullDesc*, align 4
  %result.addr = alloca %class.HullResult*, align 4
  %ret = alloca i32, align 4
  %hr = alloca %class.PHullResult, align 4
  %vcount = alloca i32, align 4
  %vertexSource = alloca %class.btAlignedObjectArray, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %scale = alloca %class.btVector3, align 4
  %ovcount = alloca i32, align 4
  %ok = alloca i8, align 1
  %i = alloca i32, align 4
  %v = alloca %class.btVector3*, align 4
  %vertexScratch = alloca %class.btAlignedObjectArray, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %ref.tmp39 = alloca %class.btVector3, align 4
  %ref.tmp44 = alloca i32, align 4
  %source = alloca i32*, align 4
  %dest = alloca i32*, align 4
  %i55 = alloca i32, align 4
  %ref.tmp81 = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca i32, align 4
  %source95 = alloca i32*, align 4
  %dest98 = alloca i32*, align 4
  %i101 = alloca i32, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store %class.HullDesc* %desc, %class.HullDesc** %desc.addr, align 4
  store %class.HullResult* %result, %class.HullResult** %result.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  store i32 1, i32* %ret, align 4
  %call = call %class.PHullResult* @_ZN11PHullResultC2Ev(%class.PHullResult* %hr)
  %0 = load %class.HullDesc*, %class.HullDesc** %desc.addr, align 4
  %mVcount = getelementptr inbounds %class.HullDesc, %class.HullDesc* %0, i32 0, i32 1
  %1 = load i32, i32* %mVcount, align 4
  store i32 %1, i32* %vcount, align 4
  %2 = load i32, i32* %vcount, align 4
  %cmp = icmp ult i32 %2, 8
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 8, i32* %vcount, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %vertexSource)
  %3 = load i32, i32* %vcount, align 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %vertexSource, i32 %3, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %scale)
  %4 = load %class.HullDesc*, %class.HullDesc** %desc.addr, align 4
  %mVcount5 = getelementptr inbounds %class.HullDesc, %class.HullDesc* %4, i32 0, i32 1
  %5 = load i32, i32* %mVcount5, align 4
  %6 = load %class.HullDesc*, %class.HullDesc** %desc.addr, align 4
  %mVertices = getelementptr inbounds %class.HullDesc, %class.HullDesc* %6, i32 0, i32 2
  %7 = load %class.btVector3*, %class.btVector3** %mVertices, align 4
  %8 = load %class.HullDesc*, %class.HullDesc** %desc.addr, align 4
  %mVertexStride = getelementptr inbounds %class.HullDesc, %class.HullDesc* %8, i32 0, i32 3
  %9 = load i32, i32* %mVertexStride, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %vertexSource, i32 0)
  %10 = load %class.HullDesc*, %class.HullDesc** %desc.addr, align 4
  %mNormalEpsilon = getelementptr inbounds %class.HullDesc, %class.HullDesc* %10, i32 0, i32 4
  %11 = load float, float* %mNormalEpsilon, align 4
  %call7 = call zeroext i1 @_ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_(%class.HullLibrary* %this1, i32 %5, %class.btVector3* %7, i32 %9, i32* nonnull align 4 dereferenceable(4) %ovcount, %class.btVector3* %call6, float %11, %class.btVector3* nonnull align 4 dereferenceable(16) %scale)
  %frombool = zext i1 %call7 to i8
  store i8 %frombool, i8* %ok, align 1
  %12 = load i8, i8* %ok, align 1
  %tobool = trunc i8 %12 to i1
  br i1 %tobool, label %if.then8, label %if.end131

if.then8:                                         ; preds = %if.end
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then8
  %13 = load i32, i32* %i, align 4
  %14 = load i32, i32* %ovcount, align 4
  %cmp9 = icmp ult i32 %13, %14
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %15 = load i32, i32* %i, align 4
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %vertexSource, i32 %15)
  store %class.btVector3* %call10, %class.btVector3** %v, align 4
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scale)
  %arrayidx = getelementptr inbounds float, float* %call11, i32 0
  %16 = load float, float* %arrayidx, align 4
  %17 = load %class.btVector3*, %class.btVector3** %v, align 4
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %17)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  %18 = load float, float* %arrayidx13, align 4
  %mul = fmul float %18, %16
  store float %mul, float* %arrayidx13, align 4
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scale)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 1
  %19 = load float, float* %arrayidx15, align 4
  %20 = load %class.btVector3*, %class.btVector3** %v, align 4
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %20)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 1
  %21 = load float, float* %arrayidx17, align 4
  %mul18 = fmul float %21, %19
  store float %mul18, float* %arrayidx17, align 4
  %call19 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %scale)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 2
  %22 = load float, float* %arrayidx20, align 4
  %23 = load %class.btVector3*, %class.btVector3** %v, align 4
  %call21 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %23)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 2
  %24 = load float, float* %arrayidx22, align 4
  %mul23 = fmul float %24, %22
  store float %mul23, float* %arrayidx22, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %25 = load i32, i32* %i, align 4
  %inc = add i32 %25, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %26 = load i32, i32* %ovcount, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %vertexSource, i32 0)
  %27 = load %class.HullDesc*, %class.HullDesc** %desc.addr, align 4
  %mMaxVertices = getelementptr inbounds %class.HullDesc, %class.HullDesc* %27, i32 0, i32 5
  %28 = load i32, i32* %mMaxVertices, align 4
  %call25 = call zeroext i1 @_ZN11HullLibrary11ComputeHullEjPK9btVector3R11PHullResultj(%class.HullLibrary* %this1, i32 %26, %class.btVector3* %call24, %class.PHullResult* nonnull align 4 dereferenceable(36) %hr, i32 %28)
  %frombool26 = zext i1 %call25 to i8
  store i8 %frombool26, i8* %ok, align 1
  %29 = load i8, i8* %ok, align 1
  %tobool27 = trunc i8 %29 to i1
  br i1 %tobool27, label %if.then28, label %if.end130

if.then28:                                        ; preds = %for.end
  %call29 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %vertexScratch)
  %mVcount30 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 0
  %30 = load i32, i32* %mVcount30, align 4
  %call32 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp31)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %vertexScratch, i32 %30, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp31)
  %mVertices33 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 3
  %31 = load %class.btVector3*, %class.btVector3** %mVertices33, align 4
  %mVcount34 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 0
  %32 = load i32, i32* %mVcount34, align 4
  %call35 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %vertexScratch, i32 0)
  %m_Indices = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 4
  %call36 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.16* %m_Indices, i32 0)
  %mIndexCount = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 1
  %33 = load i32, i32* %mIndexCount, align 4
  call void @_ZN11HullLibrary16BringOutYourDeadEPK9btVector3jPS0_RjPjj(%class.HullLibrary* %this1, %class.btVector3* %31, i32 %32, %class.btVector3* %call35, i32* nonnull align 4 dereferenceable(4) %ovcount, i32* %call36, i32 %33)
  store i32 0, i32* %ret, align 4
  %34 = load %class.HullDesc*, %class.HullDesc** %desc.addr, align 4
  %call37 = call zeroext i1 @_ZNK8HullDesc11HasHullFlagE8HullFlag(%class.HullDesc* %34, i32 1)
  br i1 %call37, label %if.then38, label %if.else77

if.then38:                                        ; preds = %if.then28
  %35 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %mPolygons = getelementptr inbounds %class.HullResult, %class.HullResult* %35, i32 0, i32 0
  store i8 0, i8* %mPolygons, align 4
  %36 = load i32, i32* %ovcount, align 4
  %37 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %mNumOutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %37, i32 0, i32 1
  store i32 %36, i32* %mNumOutputVertices, align 4
  %38 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %m_OutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %38, i32 0, i32 2
  %39 = load i32, i32* %ovcount, align 4
  %call40 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp39)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %m_OutputVertices, i32 %39, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp39)
  %mFaceCount = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 2
  %40 = load i32, i32* %mFaceCount, align 4
  %41 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %mNumFaces = getelementptr inbounds %class.HullResult, %class.HullResult* %41, i32 0, i32 3
  store i32 %40, i32* %mNumFaces, align 4
  %mIndexCount41 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 1
  %42 = load i32, i32* %mIndexCount41, align 4
  %43 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %mNumIndices = getelementptr inbounds %class.HullResult, %class.HullResult* %43, i32 0, i32 4
  store i32 %42, i32* %mNumIndices, align 4
  %44 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %m_Indices42 = getelementptr inbounds %class.HullResult, %class.HullResult* %44, i32 0, i32 5
  %mIndexCount43 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 1
  %45 = load i32, i32* %mIndexCount43, align 4
  store i32 0, i32* %ref.tmp44, align 4
  call void @_ZN20btAlignedObjectArrayIjE6resizeEiRKj(%class.btAlignedObjectArray.16* %m_Indices42, i32 %45, i32* nonnull align 4 dereferenceable(4) %ref.tmp44)
  %46 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %m_OutputVertices45 = getelementptr inbounds %class.HullResult, %class.HullResult* %46, i32 0, i32 2
  %call46 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_OutputVertices45, i32 0)
  %47 = bitcast %class.btVector3* %call46 to i8*
  %call47 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %vertexScratch, i32 0)
  %48 = bitcast %class.btVector3* %call47 to i8*
  %49 = load i32, i32* %ovcount, align 4
  %mul48 = mul i32 16, %49
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %47, i8* align 4 %48, i32 %mul48, i1 false)
  %50 = load %class.HullDesc*, %class.HullDesc** %desc.addr, align 4
  %call49 = call zeroext i1 @_ZNK8HullDesc11HasHullFlagE8HullFlag(%class.HullDesc* %50, i32 2)
  br i1 %call49, label %if.then50, label %if.else

if.then50:                                        ; preds = %if.then38
  %m_Indices51 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 4
  %call52 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.16* %m_Indices51, i32 0)
  store i32* %call52, i32** %source, align 4
  %51 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %m_Indices53 = getelementptr inbounds %class.HullResult, %class.HullResult* %51, i32 0, i32 5
  %call54 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.16* %m_Indices53, i32 0)
  store i32* %call54, i32** %dest, align 4
  store i32 0, i32* %i55, align 4
  br label %for.cond56

for.cond56:                                       ; preds = %for.inc67, %if.then50
  %52 = load i32, i32* %i55, align 4
  %mFaceCount57 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 2
  %53 = load i32, i32* %mFaceCount57, align 4
  %cmp58 = icmp ult i32 %52, %53
  br i1 %cmp58, label %for.body59, label %for.end69

for.body59:                                       ; preds = %for.cond56
  %54 = load i32*, i32** %source, align 4
  %arrayidx60 = getelementptr inbounds i32, i32* %54, i32 2
  %55 = load i32, i32* %arrayidx60, align 4
  %56 = load i32*, i32** %dest, align 4
  %arrayidx61 = getelementptr inbounds i32, i32* %56, i32 0
  store i32 %55, i32* %arrayidx61, align 4
  %57 = load i32*, i32** %source, align 4
  %arrayidx62 = getelementptr inbounds i32, i32* %57, i32 1
  %58 = load i32, i32* %arrayidx62, align 4
  %59 = load i32*, i32** %dest, align 4
  %arrayidx63 = getelementptr inbounds i32, i32* %59, i32 1
  store i32 %58, i32* %arrayidx63, align 4
  %60 = load i32*, i32** %source, align 4
  %arrayidx64 = getelementptr inbounds i32, i32* %60, i32 0
  %61 = load i32, i32* %arrayidx64, align 4
  %62 = load i32*, i32** %dest, align 4
  %arrayidx65 = getelementptr inbounds i32, i32* %62, i32 2
  store i32 %61, i32* %arrayidx65, align 4
  %63 = load i32*, i32** %dest, align 4
  %add.ptr = getelementptr inbounds i32, i32* %63, i32 3
  store i32* %add.ptr, i32** %dest, align 4
  %64 = load i32*, i32** %source, align 4
  %add.ptr66 = getelementptr inbounds i32, i32* %64, i32 3
  store i32* %add.ptr66, i32** %source, align 4
  br label %for.inc67

for.inc67:                                        ; preds = %for.body59
  %65 = load i32, i32* %i55, align 4
  %inc68 = add i32 %65, 1
  store i32 %inc68, i32* %i55, align 4
  br label %for.cond56

for.end69:                                        ; preds = %for.cond56
  br label %if.end76

if.else:                                          ; preds = %if.then38
  %66 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %m_Indices70 = getelementptr inbounds %class.HullResult, %class.HullResult* %66, i32 0, i32 5
  %call71 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.16* %m_Indices70, i32 0)
  %67 = bitcast i32* %call71 to i8*
  %m_Indices72 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 4
  %call73 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.16* %m_Indices72, i32 0)
  %68 = bitcast i32* %call73 to i8*
  %mIndexCount74 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 1
  %69 = load i32, i32* %mIndexCount74, align 4
  %mul75 = mul i32 4, %69
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %67, i8* align 4 %68, i32 %mul75, i1 false)
  br label %if.end76

if.end76:                                         ; preds = %if.else, %for.end69
  br label %if.end128

if.else77:                                        ; preds = %if.then28
  %70 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %mPolygons78 = getelementptr inbounds %class.HullResult, %class.HullResult* %70, i32 0, i32 0
  store i8 1, i8* %mPolygons78, align 4
  %71 = load i32, i32* %ovcount, align 4
  %72 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %mNumOutputVertices79 = getelementptr inbounds %class.HullResult, %class.HullResult* %72, i32 0, i32 1
  store i32 %71, i32* %mNumOutputVertices79, align 4
  %73 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %m_OutputVertices80 = getelementptr inbounds %class.HullResult, %class.HullResult* %73, i32 0, i32 2
  %74 = load i32, i32* %ovcount, align 4
  %call82 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp81)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %m_OutputVertices80, i32 %74, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp81)
  %mFaceCount83 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 2
  %75 = load i32, i32* %mFaceCount83, align 4
  %76 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %mNumFaces84 = getelementptr inbounds %class.HullResult, %class.HullResult* %76, i32 0, i32 3
  store i32 %75, i32* %mNumFaces84, align 4
  %mIndexCount85 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 1
  %77 = load i32, i32* %mIndexCount85, align 4
  %mFaceCount86 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 2
  %78 = load i32, i32* %mFaceCount86, align 4
  %add = add i32 %77, %78
  %79 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %mNumIndices87 = getelementptr inbounds %class.HullResult, %class.HullResult* %79, i32 0, i32 4
  store i32 %add, i32* %mNumIndices87, align 4
  %80 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %m_Indices88 = getelementptr inbounds %class.HullResult, %class.HullResult* %80, i32 0, i32 5
  %81 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %mNumIndices89 = getelementptr inbounds %class.HullResult, %class.HullResult* %81, i32 0, i32 4
  %82 = load i32, i32* %mNumIndices89, align 4
  store i32 0, i32* %ref.tmp90, align 4
  call void @_ZN20btAlignedObjectArrayIjE6resizeEiRKj(%class.btAlignedObjectArray.16* %m_Indices88, i32 %82, i32* nonnull align 4 dereferenceable(4) %ref.tmp90)
  %83 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %m_OutputVertices91 = getelementptr inbounds %class.HullResult, %class.HullResult* %83, i32 0, i32 2
  %call92 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_OutputVertices91, i32 0)
  %84 = bitcast %class.btVector3* %call92 to i8*
  %call93 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %vertexScratch, i32 0)
  %85 = bitcast %class.btVector3* %call93 to i8*
  %86 = load i32, i32* %ovcount, align 4
  %mul94 = mul i32 16, %86
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %84, i8* align 4 %85, i32 %mul94, i1 false)
  %m_Indices96 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 4
  %call97 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.16* %m_Indices96, i32 0)
  store i32* %call97, i32** %source95, align 4
  %87 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %m_Indices99 = getelementptr inbounds %class.HullResult, %class.HullResult* %87, i32 0, i32 5
  %call100 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.16* %m_Indices99, i32 0)
  store i32* %call100, i32** %dest98, align 4
  store i32 0, i32* %i101, align 4
  br label %for.cond102

for.cond102:                                      ; preds = %for.inc125, %if.else77
  %88 = load i32, i32* %i101, align 4
  %mFaceCount103 = getelementptr inbounds %class.PHullResult, %class.PHullResult* %hr, i32 0, i32 2
  %89 = load i32, i32* %mFaceCount103, align 4
  %cmp104 = icmp ult i32 %88, %89
  br i1 %cmp104, label %for.body105, label %for.end127

for.body105:                                      ; preds = %for.cond102
  %90 = load i32*, i32** %dest98, align 4
  %arrayidx106 = getelementptr inbounds i32, i32* %90, i32 0
  store i32 3, i32* %arrayidx106, align 4
  %91 = load %class.HullDesc*, %class.HullDesc** %desc.addr, align 4
  %call107 = call zeroext i1 @_ZNK8HullDesc11HasHullFlagE8HullFlag(%class.HullDesc* %91, i32 2)
  br i1 %call107, label %if.then108, label %if.else115

if.then108:                                       ; preds = %for.body105
  %92 = load i32*, i32** %source95, align 4
  %arrayidx109 = getelementptr inbounds i32, i32* %92, i32 2
  %93 = load i32, i32* %arrayidx109, align 4
  %94 = load i32*, i32** %dest98, align 4
  %arrayidx110 = getelementptr inbounds i32, i32* %94, i32 1
  store i32 %93, i32* %arrayidx110, align 4
  %95 = load i32*, i32** %source95, align 4
  %arrayidx111 = getelementptr inbounds i32, i32* %95, i32 1
  %96 = load i32, i32* %arrayidx111, align 4
  %97 = load i32*, i32** %dest98, align 4
  %arrayidx112 = getelementptr inbounds i32, i32* %97, i32 2
  store i32 %96, i32* %arrayidx112, align 4
  %98 = load i32*, i32** %source95, align 4
  %arrayidx113 = getelementptr inbounds i32, i32* %98, i32 0
  %99 = load i32, i32* %arrayidx113, align 4
  %100 = load i32*, i32** %dest98, align 4
  %arrayidx114 = getelementptr inbounds i32, i32* %100, i32 3
  store i32 %99, i32* %arrayidx114, align 4
  br label %if.end122

if.else115:                                       ; preds = %for.body105
  %101 = load i32*, i32** %source95, align 4
  %arrayidx116 = getelementptr inbounds i32, i32* %101, i32 0
  %102 = load i32, i32* %arrayidx116, align 4
  %103 = load i32*, i32** %dest98, align 4
  %arrayidx117 = getelementptr inbounds i32, i32* %103, i32 1
  store i32 %102, i32* %arrayidx117, align 4
  %104 = load i32*, i32** %source95, align 4
  %arrayidx118 = getelementptr inbounds i32, i32* %104, i32 1
  %105 = load i32, i32* %arrayidx118, align 4
  %106 = load i32*, i32** %dest98, align 4
  %arrayidx119 = getelementptr inbounds i32, i32* %106, i32 2
  store i32 %105, i32* %arrayidx119, align 4
  %107 = load i32*, i32** %source95, align 4
  %arrayidx120 = getelementptr inbounds i32, i32* %107, i32 2
  %108 = load i32, i32* %arrayidx120, align 4
  %109 = load i32*, i32** %dest98, align 4
  %arrayidx121 = getelementptr inbounds i32, i32* %109, i32 3
  store i32 %108, i32* %arrayidx121, align 4
  br label %if.end122

if.end122:                                        ; preds = %if.else115, %if.then108
  %110 = load i32*, i32** %dest98, align 4
  %add.ptr123 = getelementptr inbounds i32, i32* %110, i32 4
  store i32* %add.ptr123, i32** %dest98, align 4
  %111 = load i32*, i32** %source95, align 4
  %add.ptr124 = getelementptr inbounds i32, i32* %111, i32 3
  store i32* %add.ptr124, i32** %source95, align 4
  br label %for.inc125

for.inc125:                                       ; preds = %if.end122
  %112 = load i32, i32* %i101, align 4
  %inc126 = add i32 %112, 1
  store i32 %inc126, i32* %i101, align 4
  br label %for.cond102

for.end127:                                       ; preds = %for.cond102
  br label %if.end128

if.end128:                                        ; preds = %for.end127, %if.end76
  call void @_Z11ReleaseHullR11PHullResult(%class.PHullResult* nonnull align 4 dereferenceable(36) %hr)
  %call129 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %vertexScratch) #7
  br label %if.end130

if.end130:                                        ; preds = %if.end128, %for.end
  br label %if.end131

if.end131:                                        ; preds = %if.end130, %if.end
  %113 = load i32, i32* %ret, align 4
  %call132 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %vertexSource) #7
  %call133 = call %class.PHullResult* @_ZN11PHullResultD2Ev(%class.PHullResult* %hr) #7
  ret i32 %113
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.PHullResult* @_ZN11PHullResultC2Ev(%class.PHullResult* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.PHullResult*, align 4
  store %class.PHullResult* %this, %class.PHullResult** %this.addr, align 4
  %this1 = load %class.PHullResult*, %class.PHullResult** %this.addr, align 4
  %m_Indices = getelementptr inbounds %class.PHullResult, %class.PHullResult* %this1, i32 0, i32 4
  %call = call %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.16* %m_Indices)
  %mVcount = getelementptr inbounds %class.PHullResult, %class.PHullResult* %this1, i32 0, i32 0
  store i32 0, i32* %mVcount, align 4
  %mIndexCount = getelementptr inbounds %class.PHullResult, %class.PHullResult* %this1, i32 0, i32 1
  store i32 0, i32* %mIndexCount, align 4
  %mFaceCount = getelementptr inbounds %class.PHullResult, %class.PHullResult* %this1, i32 0, i32 2
  store i32 0, i32* %mFaceCount, align 4
  %mVertices = getelementptr inbounds %class.PHullResult, %class.PHullResult* %this1, i32 0, i32 3
  store %class.btVector3* null, %class.btVector3** %mVertices, align 4
  ret %class.PHullResult* %this1
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_(%class.HullLibrary* %this, i32 %svcount, %class.btVector3* %svertices, i32 %stride, i32* nonnull align 4 dereferenceable(4) %vcount, %class.btVector3* %vertices, float %normalepsilon, %class.btVector3* nonnull align 4 dereferenceable(16) %scale) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.HullLibrary*, align 4
  %svcount.addr = alloca i32, align 4
  %svertices.addr = alloca %class.btVector3*, align 4
  %stride.addr = alloca i32, align 4
  %vcount.addr = alloca i32*, align 4
  %vertices.addr = alloca %class.btVector3*, align 4
  %normalepsilon.addr = alloca float, align 4
  %scale.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca i32, align 4
  %recip = alloca [3 x float], align 4
  %bmin = alloca [3 x float], align 4
  %bmax = alloca [3 x float], align 4
  %vtx = alloca i8*, align 4
  %i = alloca i32, align 4
  %p = alloca float*, align 4
  %j = alloca i32, align 4
  %dx = alloca float, align 4
  %dy = alloca float, align 4
  %dz = alloca float, align 4
  %center = alloca %class.btVector3, align 4
  %len = alloca float, align 4
  %x1 = alloca float, align 4
  %x2 = alloca float, align 4
  %y1 = alloca float, align 4
  %y2 = alloca float, align 4
  %z1 = alloca float, align 4
  %z2 = alloca float, align 4
  %i135 = alloca i32, align 4
  %p139 = alloca %class.btVector3*, align 4
  %px = alloca float, align 4
  %py = alloca float, align 4
  %pz = alloca float, align 4
  %j154 = alloca i32, align 4
  %v = alloca %class.btVector3*, align 4
  %x = alloca float, align 4
  %y = alloca float, align 4
  %z = alloca float, align 4
  %dx165 = alloca float, align 4
  %dy168 = alloca float, align 4
  %dz171 = alloca float, align 4
  %dist1 = alloca float, align 4
  %dist2 = alloca float, align 4
  %dest = alloca %class.btVector3*, align 4
  %ref.tmp215 = alloca i32, align 4
  %bmin219 = alloca [3 x float], align 4
  %bmax220 = alloca [3 x float], align 4
  %i221 = alloca i32, align 4
  %p225 = alloca %class.btVector3*, align 4
  %j227 = alloca i32, align 4
  %dx255 = alloca float, align 4
  %dy259 = alloca float, align 4
  %dz263 = alloca float, align 4
  %cx = alloca float, align 4
  %cy = alloca float, align 4
  %cz = alloca float, align 4
  %len284 = alloca float, align 4
  %x1316 = alloca float, align 4
  %x2318 = alloca float, align 4
  %y1320 = alloca float, align 4
  %y2322 = alloca float, align 4
  %z1324 = alloca float, align 4
  %z2326 = alloca float, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store i32 %svcount, i32* %svcount.addr, align 4
  store %class.btVector3* %svertices, %class.btVector3** %svertices.addr, align 4
  store i32 %stride, i32* %stride.addr, align 4
  store i32* %vcount, i32** %vcount.addr, align 4
  store %class.btVector3* %vertices, %class.btVector3** %vertices.addr, align 4
  store float %normalepsilon, float* %normalepsilon.addr, align 4
  store %class.btVector3* %scale, %class.btVector3** %scale.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %0 = load i32, i32* %svcount.addr, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %m_vertexIndexMapping = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  store i32 0, i32* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.12* %m_vertexIndexMapping, i32 0, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = load i32*, i32** %vcount.addr, align 4
  store i32 0, i32* %1, align 4
  %2 = bitcast [3 x float]* %recip to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %2, i8 0, i32 12, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %scale.addr, align 4
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %3)
  %tobool = icmp ne float* %call, null
  br i1 %tobool, label %if.then2, label %if.end8

if.then2:                                         ; preds = %if.end
  %4 = load %class.btVector3*, %class.btVector3** %scale.addr, align 4
  %call3 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %4)
  %arrayidx = getelementptr inbounds float, float* %call3, i32 0
  store float 1.000000e+00, float* %arrayidx, align 4
  %5 = load %class.btVector3*, %class.btVector3** %scale.addr, align 4
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %5)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  store float 1.000000e+00, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %scale.addr, align 4
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %6)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  store float 1.000000e+00, float* %arrayidx7, align 4
  br label %if.end8

if.end8:                                          ; preds = %if.then2, %if.end
  %7 = bitcast [3 x float]* %bmin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 bitcast ([3 x float]* @__const._ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_.bmin to i8*), i32 12, i1 false)
  %8 = bitcast [3 x float]* %bmax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 bitcast ([3 x float]* @__const._ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_.bmax to i8*), i32 12, i1 false)
  %9 = load %class.btVector3*, %class.btVector3** %svertices.addr, align 4
  %10 = bitcast %class.btVector3* %9 to i8*
  store i8* %10, i8** %vtx, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc27, %if.end8
  %11 = load i32, i32* %i, align 4
  %12 = load i32, i32* %svcount.addr, align 4
  %cmp9 = icmp ult i32 %11, %12
  br i1 %cmp9, label %for.body, label %for.end29

for.body:                                         ; preds = %for.cond
  %13 = load i8*, i8** %vtx, align 4
  %14 = bitcast i8* %13 to float*
  store float* %14, float** %p, align 4
  %15 = load i32, i32* %stride.addr, align 4
  %16 = load i8*, i8** %vtx, align 4
  %add.ptr = getelementptr inbounds i8, i8* %16, i32 %15
  store i8* %add.ptr, i8** %vtx, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc, %for.body
  %17 = load i32, i32* %j, align 4
  %cmp11 = icmp slt i32 %17, 3
  br i1 %cmp11, label %for.body12, label %for.end

for.body12:                                       ; preds = %for.cond10
  %18 = load float*, float** %p, align 4
  %19 = load i32, i32* %j, align 4
  %arrayidx13 = getelementptr inbounds float, float* %18, i32 %19
  %20 = load float, float* %arrayidx13, align 4
  %21 = load i32, i32* %j, align 4
  %arrayidx14 = getelementptr inbounds [3 x float], [3 x float]* %bmin, i32 0, i32 %21
  %22 = load float, float* %arrayidx14, align 4
  %cmp15 = fcmp olt float %20, %22
  br i1 %cmp15, label %if.then16, label %if.end19

if.then16:                                        ; preds = %for.body12
  %23 = load float*, float** %p, align 4
  %24 = load i32, i32* %j, align 4
  %arrayidx17 = getelementptr inbounds float, float* %23, i32 %24
  %25 = load float, float* %arrayidx17, align 4
  %26 = load i32, i32* %j, align 4
  %arrayidx18 = getelementptr inbounds [3 x float], [3 x float]* %bmin, i32 0, i32 %26
  store float %25, float* %arrayidx18, align 4
  br label %if.end19

if.end19:                                         ; preds = %if.then16, %for.body12
  %27 = load float*, float** %p, align 4
  %28 = load i32, i32* %j, align 4
  %arrayidx20 = getelementptr inbounds float, float* %27, i32 %28
  %29 = load float, float* %arrayidx20, align 4
  %30 = load i32, i32* %j, align 4
  %arrayidx21 = getelementptr inbounds [3 x float], [3 x float]* %bmax, i32 0, i32 %30
  %31 = load float, float* %arrayidx21, align 4
  %cmp22 = fcmp ogt float %29, %31
  br i1 %cmp22, label %if.then23, label %if.end26

if.then23:                                        ; preds = %if.end19
  %32 = load float*, float** %p, align 4
  %33 = load i32, i32* %j, align 4
  %arrayidx24 = getelementptr inbounds float, float* %32, i32 %33
  %34 = load float, float* %arrayidx24, align 4
  %35 = load i32, i32* %j, align 4
  %arrayidx25 = getelementptr inbounds [3 x float], [3 x float]* %bmax, i32 0, i32 %35
  store float %34, float* %arrayidx25, align 4
  br label %if.end26

if.end26:                                         ; preds = %if.then23, %if.end19
  br label %for.inc

for.inc:                                          ; preds = %if.end26
  %36 = load i32, i32* %j, align 4
  %inc = add nsw i32 %36, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond10

for.end:                                          ; preds = %for.cond10
  br label %for.inc27

for.inc27:                                        ; preds = %for.end
  %37 = load i32, i32* %i, align 4
  %inc28 = add i32 %37, 1
  store i32 %inc28, i32* %i, align 4
  br label %for.cond

for.end29:                                        ; preds = %for.cond
  %arrayidx30 = getelementptr inbounds [3 x float], [3 x float]* %bmax, i32 0, i32 0
  %38 = load float, float* %arrayidx30, align 4
  %arrayidx31 = getelementptr inbounds [3 x float], [3 x float]* %bmin, i32 0, i32 0
  %39 = load float, float* %arrayidx31, align 4
  %sub = fsub float %38, %39
  store float %sub, float* %dx, align 4
  %arrayidx32 = getelementptr inbounds [3 x float], [3 x float]* %bmax, i32 0, i32 1
  %40 = load float, float* %arrayidx32, align 4
  %arrayidx33 = getelementptr inbounds [3 x float], [3 x float]* %bmin, i32 0, i32 1
  %41 = load float, float* %arrayidx33, align 4
  %sub34 = fsub float %40, %41
  store float %sub34, float* %dy, align 4
  %arrayidx35 = getelementptr inbounds [3 x float], [3 x float]* %bmax, i32 0, i32 2
  %42 = load float, float* %arrayidx35, align 4
  %arrayidx36 = getelementptr inbounds [3 x float], [3 x float]* %bmin, i32 0, i32 2
  %43 = load float, float* %arrayidx36, align 4
  %sub37 = fsub float %42, %43
  store float %sub37, float* %dz, align 4
  %call38 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %center)
  %44 = load float, float* %dx, align 4
  %mul = fmul float %44, 5.000000e-01
  %arrayidx39 = getelementptr inbounds [3 x float], [3 x float]* %bmin, i32 0, i32 0
  %45 = load float, float* %arrayidx39, align 4
  %add = fadd float %mul, %45
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 0
  store float %add, float* %arrayidx41, align 4
  %46 = load float, float* %dy, align 4
  %mul42 = fmul float %46, 5.000000e-01
  %arrayidx43 = getelementptr inbounds [3 x float], [3 x float]* %bmin, i32 0, i32 1
  %47 = load float, float* %arrayidx43, align 4
  %add44 = fadd float %mul42, %47
  %call45 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 1
  store float %add44, float* %arrayidx46, align 4
  %48 = load float, float* %dz, align 4
  %mul47 = fmul float %48, 5.000000e-01
  %arrayidx48 = getelementptr inbounds [3 x float], [3 x float]* %bmin, i32 0, i32 2
  %49 = load float, float* %arrayidx48, align 4
  %add49 = fadd float %mul47, %49
  %call50 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 2
  store float %add49, float* %arrayidx51, align 4
  %50 = load float, float* %dx, align 4
  %cmp52 = fcmp olt float %50, 0x3EB0C6F7A0000000
  br i1 %cmp52, label %if.then58, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.end29
  %51 = load float, float* %dy, align 4
  %cmp53 = fcmp olt float %51, 0x3EB0C6F7A0000000
  br i1 %cmp53, label %if.then58, label %lor.lhs.false54

lor.lhs.false54:                                  ; preds = %lor.lhs.false
  %52 = load float, float* %dz, align 4
  %cmp55 = fcmp olt float %52, 0x3EB0C6F7A0000000
  br i1 %cmp55, label %if.then58, label %lor.lhs.false56

lor.lhs.false56:                                  ; preds = %lor.lhs.false54
  %53 = load i32, i32* %svcount.addr, align 4
  %cmp57 = icmp ult i32 %53, 3
  br i1 %cmp57, label %if.then58, label %if.else106

if.then58:                                        ; preds = %lor.lhs.false56, %lor.lhs.false54, %lor.lhs.false, %for.end29
  store float 0x47EFFFFFE0000000, float* %len, align 4
  %54 = load float, float* %dx, align 4
  %cmp59 = fcmp ogt float %54, 0x3EB0C6F7A0000000
  br i1 %cmp59, label %land.lhs.true, label %if.end62

land.lhs.true:                                    ; preds = %if.then58
  %55 = load float, float* %dx, align 4
  %56 = load float, float* %len, align 4
  %cmp60 = fcmp olt float %55, %56
  br i1 %cmp60, label %if.then61, label %if.end62

if.then61:                                        ; preds = %land.lhs.true
  %57 = load float, float* %dx, align 4
  store float %57, float* %len, align 4
  br label %if.end62

if.end62:                                         ; preds = %if.then61, %land.lhs.true, %if.then58
  %58 = load float, float* %dy, align 4
  %cmp63 = fcmp ogt float %58, 0x3EB0C6F7A0000000
  br i1 %cmp63, label %land.lhs.true64, label %if.end67

land.lhs.true64:                                  ; preds = %if.end62
  %59 = load float, float* %dy, align 4
  %60 = load float, float* %len, align 4
  %cmp65 = fcmp olt float %59, %60
  br i1 %cmp65, label %if.then66, label %if.end67

if.then66:                                        ; preds = %land.lhs.true64
  %61 = load float, float* %dy, align 4
  store float %61, float* %len, align 4
  br label %if.end67

if.end67:                                         ; preds = %if.then66, %land.lhs.true64, %if.end62
  %62 = load float, float* %dz, align 4
  %cmp68 = fcmp ogt float %62, 0x3EB0C6F7A0000000
  br i1 %cmp68, label %land.lhs.true69, label %if.end72

land.lhs.true69:                                  ; preds = %if.end67
  %63 = load float, float* %dz, align 4
  %64 = load float, float* %len, align 4
  %cmp70 = fcmp olt float %63, %64
  br i1 %cmp70, label %if.then71, label %if.end72

if.then71:                                        ; preds = %land.lhs.true69
  %65 = load float, float* %dz, align 4
  store float %65, float* %len, align 4
  br label %if.end72

if.end72:                                         ; preds = %if.then71, %land.lhs.true69, %if.end67
  %66 = load float, float* %len, align 4
  %cmp73 = fcmp oeq float %66, 0x47EFFFFFE0000000
  br i1 %cmp73, label %if.then74, label %if.else

if.then74:                                        ; preds = %if.end72
  store float 0x3F847AE140000000, float* %dz, align 4
  store float 0x3F847AE140000000, float* %dy, align 4
  store float 0x3F847AE140000000, float* %dx, align 4
  br label %if.end87

if.else:                                          ; preds = %if.end72
  %67 = load float, float* %dx, align 4
  %cmp75 = fcmp olt float %67, 0x3EB0C6F7A0000000
  br i1 %cmp75, label %if.then76, label %if.end78

if.then76:                                        ; preds = %if.else
  %68 = load float, float* %len, align 4
  %mul77 = fmul float %68, 0x3FA99999A0000000
  store float %mul77, float* %dx, align 4
  br label %if.end78

if.end78:                                         ; preds = %if.then76, %if.else
  %69 = load float, float* %dy, align 4
  %cmp79 = fcmp olt float %69, 0x3EB0C6F7A0000000
  br i1 %cmp79, label %if.then80, label %if.end82

if.then80:                                        ; preds = %if.end78
  %70 = load float, float* %len, align 4
  %mul81 = fmul float %70, 0x3FA99999A0000000
  store float %mul81, float* %dy, align 4
  br label %if.end82

if.end82:                                         ; preds = %if.then80, %if.end78
  %71 = load float, float* %dz, align 4
  %cmp83 = fcmp olt float %71, 0x3EB0C6F7A0000000
  br i1 %cmp83, label %if.then84, label %if.end86

if.then84:                                        ; preds = %if.end82
  %72 = load float, float* %len, align 4
  %mul85 = fmul float %72, 0x3FA99999A0000000
  store float %mul85, float* %dz, align 4
  br label %if.end86

if.end86:                                         ; preds = %if.then84, %if.end82
  br label %if.end87

if.end87:                                         ; preds = %if.end86, %if.then74
  %call88 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx89 = getelementptr inbounds float, float* %call88, i32 0
  %73 = load float, float* %arrayidx89, align 4
  %74 = load float, float* %dx, align 4
  %sub90 = fsub float %73, %74
  store float %sub90, float* %x1, align 4
  %call91 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx92 = getelementptr inbounds float, float* %call91, i32 0
  %75 = load float, float* %arrayidx92, align 4
  %76 = load float, float* %dx, align 4
  %add93 = fadd float %75, %76
  store float %add93, float* %x2, align 4
  %call94 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx95 = getelementptr inbounds float, float* %call94, i32 1
  %77 = load float, float* %arrayidx95, align 4
  %78 = load float, float* %dy, align 4
  %sub96 = fsub float %77, %78
  store float %sub96, float* %y1, align 4
  %call97 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 1
  %79 = load float, float* %arrayidx98, align 4
  %80 = load float, float* %dy, align 4
  %add99 = fadd float %79, %80
  store float %add99, float* %y2, align 4
  %call100 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx101 = getelementptr inbounds float, float* %call100, i32 2
  %81 = load float, float* %arrayidx101, align 4
  %82 = load float, float* %dz, align 4
  %sub102 = fsub float %81, %82
  store float %sub102, float* %z1, align 4
  %call103 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx104 = getelementptr inbounds float, float* %call103, i32 2
  %83 = load float, float* %arrayidx104, align 4
  %84 = load float, float* %dz, align 4
  %add105 = fadd float %83, %84
  store float %add105, float* %z2, align 4
  %85 = load i32*, i32** %vcount.addr, align 4
  %86 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %87 = load float, float* %x1, align 4
  %88 = load float, float* %y1, align 4
  %89 = load float, float* %z1, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %85, %class.btVector3* %86, float %87, float %88, float %89)
  %90 = load i32*, i32** %vcount.addr, align 4
  %91 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %92 = load float, float* %x2, align 4
  %93 = load float, float* %y1, align 4
  %94 = load float, float* %z1, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %90, %class.btVector3* %91, float %92, float %93, float %94)
  %95 = load i32*, i32** %vcount.addr, align 4
  %96 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %97 = load float, float* %x2, align 4
  %98 = load float, float* %y2, align 4
  %99 = load float, float* %z1, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %95, %class.btVector3* %96, float %97, float %98, float %99)
  %100 = load i32*, i32** %vcount.addr, align 4
  %101 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %102 = load float, float* %x1, align 4
  %103 = load float, float* %y2, align 4
  %104 = load float, float* %z1, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %100, %class.btVector3* %101, float %102, float %103, float %104)
  %105 = load i32*, i32** %vcount.addr, align 4
  %106 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %107 = load float, float* %x1, align 4
  %108 = load float, float* %y1, align 4
  %109 = load float, float* %z2, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %105, %class.btVector3* %106, float %107, float %108, float %109)
  %110 = load i32*, i32** %vcount.addr, align 4
  %111 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %112 = load float, float* %x2, align 4
  %113 = load float, float* %y1, align 4
  %114 = load float, float* %z2, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %110, %class.btVector3* %111, float %112, float %113, float %114)
  %115 = load i32*, i32** %vcount.addr, align 4
  %116 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %117 = load float, float* %x2, align 4
  %118 = load float, float* %y2, align 4
  %119 = load float, float* %z2, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %115, %class.btVector3* %116, float %117, float %118, float %119)
  %120 = load i32*, i32** %vcount.addr, align 4
  %121 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %122 = load float, float* %x1, align 4
  %123 = load float, float* %y2, align 4
  %124 = load float, float* %z2, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %120, %class.btVector3* %121, float %122, float %123, float %124)
  store i1 true, i1* %retval, align 1
  br label %return

if.else106:                                       ; preds = %lor.lhs.false56
  %125 = load %class.btVector3*, %class.btVector3** %scale.addr, align 4
  %call107 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %125)
  %tobool108 = icmp ne float* %call107, null
  br i1 %tobool108, label %if.then109, label %if.end133

if.then109:                                       ; preds = %if.else106
  %126 = load float, float* %dx, align 4
  %127 = load %class.btVector3*, %class.btVector3** %scale.addr, align 4
  %call110 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %127)
  %arrayidx111 = getelementptr inbounds float, float* %call110, i32 0
  store float %126, float* %arrayidx111, align 4
  %128 = load float, float* %dy, align 4
  %129 = load %class.btVector3*, %class.btVector3** %scale.addr, align 4
  %call112 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %129)
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 1
  store float %128, float* %arrayidx113, align 4
  %130 = load float, float* %dz, align 4
  %131 = load %class.btVector3*, %class.btVector3** %scale.addr, align 4
  %call114 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %131)
  %arrayidx115 = getelementptr inbounds float, float* %call114, i32 2
  store float %130, float* %arrayidx115, align 4
  %132 = load float, float* %dx, align 4
  %div = fdiv float 1.000000e+00, %132
  %arrayidx116 = getelementptr inbounds [3 x float], [3 x float]* %recip, i32 0, i32 0
  store float %div, float* %arrayidx116, align 4
  %133 = load float, float* %dy, align 4
  %div117 = fdiv float 1.000000e+00, %133
  %arrayidx118 = getelementptr inbounds [3 x float], [3 x float]* %recip, i32 0, i32 1
  store float %div117, float* %arrayidx118, align 4
  %134 = load float, float* %dz, align 4
  %div119 = fdiv float 1.000000e+00, %134
  %arrayidx120 = getelementptr inbounds [3 x float], [3 x float]* %recip, i32 0, i32 2
  store float %div119, float* %arrayidx120, align 4
  %arrayidx121 = getelementptr inbounds [3 x float], [3 x float]* %recip, i32 0, i32 0
  %135 = load float, float* %arrayidx121, align 4
  %call122 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx123 = getelementptr inbounds float, float* %call122, i32 0
  %136 = load float, float* %arrayidx123, align 4
  %mul124 = fmul float %136, %135
  store float %mul124, float* %arrayidx123, align 4
  %arrayidx125 = getelementptr inbounds [3 x float], [3 x float]* %recip, i32 0, i32 1
  %137 = load float, float* %arrayidx125, align 4
  %call126 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx127 = getelementptr inbounds float, float* %call126, i32 1
  %138 = load float, float* %arrayidx127, align 4
  %mul128 = fmul float %138, %137
  store float %mul128, float* %arrayidx127, align 4
  %arrayidx129 = getelementptr inbounds [3 x float], [3 x float]* %recip, i32 0, i32 2
  %139 = load float, float* %arrayidx129, align 4
  %call130 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %arrayidx131 = getelementptr inbounds float, float* %call130, i32 2
  %140 = load float, float* %arrayidx131, align 4
  %mul132 = fmul float %140, %139
  store float %mul132, float* %arrayidx131, align 4
  br label %if.end133

if.end133:                                        ; preds = %if.then109, %if.else106
  br label %if.end134

if.end134:                                        ; preds = %if.end133
  %141 = load %class.btVector3*, %class.btVector3** %svertices.addr, align 4
  %142 = bitcast %class.btVector3* %141 to i8*
  store i8* %142, i8** %vtx, align 4
  store i32 0, i32* %i135, align 4
  br label %for.cond136

for.cond136:                                      ; preds = %for.inc216, %if.end134
  %143 = load i32, i32* %i135, align 4
  %144 = load i32, i32* %svcount.addr, align 4
  %cmp137 = icmp ult i32 %143, %144
  br i1 %cmp137, label %for.body138, label %for.end218

for.body138:                                      ; preds = %for.cond136
  %145 = load i8*, i8** %vtx, align 4
  %146 = bitcast i8* %145 to %class.btVector3*
  store %class.btVector3* %146, %class.btVector3** %p139, align 4
  %147 = load i32, i32* %stride.addr, align 4
  %148 = load i8*, i8** %vtx, align 4
  %add.ptr140 = getelementptr inbounds i8, i8* %148, i32 %147
  store i8* %add.ptr140, i8** %vtx, align 4
  %149 = load %class.btVector3*, %class.btVector3** %p139, align 4
  %call141 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %149)
  %150 = load float, float* %call141, align 4
  store float %150, float* %px, align 4
  %151 = load %class.btVector3*, %class.btVector3** %p139, align 4
  %call142 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %151)
  %152 = load float, float* %call142, align 4
  store float %152, float* %py, align 4
  %153 = load %class.btVector3*, %class.btVector3** %p139, align 4
  %call143 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %153)
  %154 = load float, float* %call143, align 4
  store float %154, float* %pz, align 4
  %155 = load %class.btVector3*, %class.btVector3** %scale.addr, align 4
  %call144 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %155)
  %tobool145 = icmp ne float* %call144, null
  br i1 %tobool145, label %if.then146, label %if.end153

if.then146:                                       ; preds = %for.body138
  %156 = load float, float* %px, align 4
  %arrayidx147 = getelementptr inbounds [3 x float], [3 x float]* %recip, i32 0, i32 0
  %157 = load float, float* %arrayidx147, align 4
  %mul148 = fmul float %156, %157
  store float %mul148, float* %px, align 4
  %158 = load float, float* %py, align 4
  %arrayidx149 = getelementptr inbounds [3 x float], [3 x float]* %recip, i32 0, i32 1
  %159 = load float, float* %arrayidx149, align 4
  %mul150 = fmul float %158, %159
  store float %mul150, float* %py, align 4
  %160 = load float, float* %pz, align 4
  %arrayidx151 = getelementptr inbounds [3 x float], [3 x float]* %recip, i32 0, i32 2
  %161 = load float, float* %arrayidx151, align 4
  %mul152 = fmul float %160, %161
  store float %mul152, float* %pz, align 4
  br label %if.end153

if.end153:                                        ; preds = %if.then146, %for.body138
  store i32 0, i32* %j154, align 4
  br label %for.cond155

for.cond155:                                      ; preds = %for.inc200, %if.end153
  %162 = load i32, i32* %j154, align 4
  %163 = load i32*, i32** %vcount.addr, align 4
  %164 = load i32, i32* %163, align 4
  %cmp156 = icmp ult i32 %162, %164
  br i1 %cmp156, label %for.body157, label %for.end202

for.body157:                                      ; preds = %for.cond155
  %165 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %166 = load i32, i32* %j154, align 4
  %arrayidx158 = getelementptr inbounds %class.btVector3, %class.btVector3* %165, i32 %166
  store %class.btVector3* %arrayidx158, %class.btVector3** %v, align 4
  %167 = load %class.btVector3*, %class.btVector3** %v, align 4
  %call159 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %167)
  %arrayidx160 = getelementptr inbounds float, float* %call159, i32 0
  %168 = load float, float* %arrayidx160, align 4
  store float %168, float* %x, align 4
  %169 = load %class.btVector3*, %class.btVector3** %v, align 4
  %call161 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %169)
  %arrayidx162 = getelementptr inbounds float, float* %call161, i32 1
  %170 = load float, float* %arrayidx162, align 4
  store float %170, float* %y, align 4
  %171 = load %class.btVector3*, %class.btVector3** %v, align 4
  %call163 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %171)
  %arrayidx164 = getelementptr inbounds float, float* %call163, i32 2
  %172 = load float, float* %arrayidx164, align 4
  store float %172, float* %z, align 4
  %173 = load float, float* %x, align 4
  %174 = load float, float* %px, align 4
  %sub166 = fsub float %173, %174
  %call167 = call float @_Z6btFabsf(float %sub166)
  store float %call167, float* %dx165, align 4
  %175 = load float, float* %y, align 4
  %176 = load float, float* %py, align 4
  %sub169 = fsub float %175, %176
  %call170 = call float @_Z6btFabsf(float %sub169)
  store float %call170, float* %dy168, align 4
  %177 = load float, float* %z, align 4
  %178 = load float, float* %pz, align 4
  %sub172 = fsub float %177, %178
  %call173 = call float @_Z6btFabsf(float %sub172)
  store float %call173, float* %dz171, align 4
  %179 = load float, float* %dx165, align 4
  %180 = load float, float* %normalepsilon.addr, align 4
  %cmp174 = fcmp olt float %179, %180
  br i1 %cmp174, label %land.lhs.true175, label %if.end199

land.lhs.true175:                                 ; preds = %for.body157
  %181 = load float, float* %dy168, align 4
  %182 = load float, float* %normalepsilon.addr, align 4
  %cmp176 = fcmp olt float %181, %182
  br i1 %cmp176, label %land.lhs.true177, label %if.end199

land.lhs.true177:                                 ; preds = %land.lhs.true175
  %183 = load float, float* %dz171, align 4
  %184 = load float, float* %normalepsilon.addr, align 4
  %cmp178 = fcmp olt float %183, %184
  br i1 %cmp178, label %if.then179, label %if.end199

if.then179:                                       ; preds = %land.lhs.true177
  %185 = load float, float* %px, align 4
  %186 = load float, float* %py, align 4
  %187 = load float, float* %pz, align 4
  %call180 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %call181 = call float @_Z7GetDistfffPKf(float %185, float %186, float %187, float* %call180)
  store float %call181, float* %dist1, align 4
  %188 = load %class.btVector3*, %class.btVector3** %v, align 4
  %call182 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %188)
  %arrayidx183 = getelementptr inbounds float, float* %call182, i32 0
  %189 = load float, float* %arrayidx183, align 4
  %190 = load %class.btVector3*, %class.btVector3** %v, align 4
  %call184 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %190)
  %arrayidx185 = getelementptr inbounds float, float* %call184, i32 1
  %191 = load float, float* %arrayidx185, align 4
  %192 = load %class.btVector3*, %class.btVector3** %v, align 4
  %call186 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %192)
  %arrayidx187 = getelementptr inbounds float, float* %call186, i32 2
  %193 = load float, float* %arrayidx187, align 4
  %call188 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %center)
  %call189 = call float @_Z7GetDistfffPKf(float %189, float %191, float %193, float* %call188)
  store float %call189, float* %dist2, align 4
  %194 = load float, float* %dist1, align 4
  %195 = load float, float* %dist2, align 4
  %cmp190 = fcmp ogt float %194, %195
  br i1 %cmp190, label %if.then191, label %if.end198

if.then191:                                       ; preds = %if.then179
  %196 = load float, float* %px, align 4
  %197 = load %class.btVector3*, %class.btVector3** %v, align 4
  %call192 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %197)
  %arrayidx193 = getelementptr inbounds float, float* %call192, i32 0
  store float %196, float* %arrayidx193, align 4
  %198 = load float, float* %py, align 4
  %199 = load %class.btVector3*, %class.btVector3** %v, align 4
  %call194 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %199)
  %arrayidx195 = getelementptr inbounds float, float* %call194, i32 1
  store float %198, float* %arrayidx195, align 4
  %200 = load float, float* %pz, align 4
  %201 = load %class.btVector3*, %class.btVector3** %v, align 4
  %call196 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %201)
  %arrayidx197 = getelementptr inbounds float, float* %call196, i32 2
  store float %200, float* %arrayidx197, align 4
  br label %if.end198

if.end198:                                        ; preds = %if.then191, %if.then179
  br label %for.end202

if.end199:                                        ; preds = %land.lhs.true177, %land.lhs.true175, %for.body157
  br label %for.inc200

for.inc200:                                       ; preds = %if.end199
  %202 = load i32, i32* %j154, align 4
  %inc201 = add i32 %202, 1
  store i32 %inc201, i32* %j154, align 4
  br label %for.cond155

for.end202:                                       ; preds = %if.end198, %for.cond155
  %203 = load i32, i32* %j154, align 4
  %204 = load i32*, i32** %vcount.addr, align 4
  %205 = load i32, i32* %204, align 4
  %cmp203 = icmp eq i32 %203, %205
  br i1 %cmp203, label %if.then204, label %if.end213

if.then204:                                       ; preds = %for.end202
  %206 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %207 = load i32*, i32** %vcount.addr, align 4
  %208 = load i32, i32* %207, align 4
  %arrayidx205 = getelementptr inbounds %class.btVector3, %class.btVector3* %206, i32 %208
  store %class.btVector3* %arrayidx205, %class.btVector3** %dest, align 4
  %209 = load float, float* %px, align 4
  %210 = load %class.btVector3*, %class.btVector3** %dest, align 4
  %call206 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %210)
  %arrayidx207 = getelementptr inbounds float, float* %call206, i32 0
  store float %209, float* %arrayidx207, align 4
  %211 = load float, float* %py, align 4
  %212 = load %class.btVector3*, %class.btVector3** %dest, align 4
  %call208 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %212)
  %arrayidx209 = getelementptr inbounds float, float* %call208, i32 1
  store float %211, float* %arrayidx209, align 4
  %213 = load float, float* %pz, align 4
  %214 = load %class.btVector3*, %class.btVector3** %dest, align 4
  %call210 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %214)
  %arrayidx211 = getelementptr inbounds float, float* %call210, i32 2
  store float %213, float* %arrayidx211, align 4
  %215 = load i32*, i32** %vcount.addr, align 4
  %216 = load i32, i32* %215, align 4
  %inc212 = add i32 %216, 1
  store i32 %inc212, i32* %215, align 4
  br label %if.end213

if.end213:                                        ; preds = %if.then204, %for.end202
  %m_vertexIndexMapping214 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  %217 = load i32, i32* %j154, align 4
  store i32 %217, i32* %ref.tmp215, align 4
  call void @_ZN20btAlignedObjectArrayIiE9push_backERKi(%class.btAlignedObjectArray.12* %m_vertexIndexMapping214, i32* nonnull align 4 dereferenceable(4) %ref.tmp215)
  br label %for.inc216

for.inc216:                                       ; preds = %if.end213
  %218 = load i32, i32* %i135, align 4
  %inc217 = add i32 %218, 1
  store i32 %inc217, i32* %i135, align 4
  br label %for.cond136

for.end218:                                       ; preds = %for.cond136
  %219 = bitcast [3 x float]* %bmin219 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %219, i8* align 4 bitcast ([3 x float]* @__const._ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_.bmin.1 to i8*), i32 12, i1 false)
  %220 = bitcast [3 x float]* %bmax220 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %220, i8* align 4 bitcast ([3 x float]* @__const._ZN11HullLibrary15CleanupVerticesEjPK9btVector3jRjPS0_fRS0_.bmax.2 to i8*), i32 12, i1 false)
  store i32 0, i32* %i221, align 4
  br label %for.cond222

for.cond222:                                      ; preds = %for.inc252, %for.end218
  %221 = load i32, i32* %i221, align 4
  %222 = load i32*, i32** %vcount.addr, align 4
  %223 = load i32, i32* %222, align 4
  %cmp223 = icmp ult i32 %221, %223
  br i1 %cmp223, label %for.body224, label %for.end254

for.body224:                                      ; preds = %for.cond222
  %224 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %225 = load i32, i32* %i221, align 4
  %arrayidx226 = getelementptr inbounds %class.btVector3, %class.btVector3* %224, i32 %225
  store %class.btVector3* %arrayidx226, %class.btVector3** %p225, align 4
  store i32 0, i32* %j227, align 4
  br label %for.cond228

for.cond228:                                      ; preds = %for.inc249, %for.body224
  %226 = load i32, i32* %j227, align 4
  %cmp229 = icmp slt i32 %226, 3
  br i1 %cmp229, label %for.body230, label %for.end251

for.body230:                                      ; preds = %for.cond228
  %227 = load %class.btVector3*, %class.btVector3** %p225, align 4
  %call231 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %227)
  %228 = load i32, i32* %j227, align 4
  %arrayidx232 = getelementptr inbounds float, float* %call231, i32 %228
  %229 = load float, float* %arrayidx232, align 4
  %230 = load i32, i32* %j227, align 4
  %arrayidx233 = getelementptr inbounds [3 x float], [3 x float]* %bmin219, i32 0, i32 %230
  %231 = load float, float* %arrayidx233, align 4
  %cmp234 = fcmp olt float %229, %231
  br i1 %cmp234, label %if.then235, label %if.end239

if.then235:                                       ; preds = %for.body230
  %232 = load %class.btVector3*, %class.btVector3** %p225, align 4
  %call236 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %232)
  %233 = load i32, i32* %j227, align 4
  %arrayidx237 = getelementptr inbounds float, float* %call236, i32 %233
  %234 = load float, float* %arrayidx237, align 4
  %235 = load i32, i32* %j227, align 4
  %arrayidx238 = getelementptr inbounds [3 x float], [3 x float]* %bmin219, i32 0, i32 %235
  store float %234, float* %arrayidx238, align 4
  br label %if.end239

if.end239:                                        ; preds = %if.then235, %for.body230
  %236 = load %class.btVector3*, %class.btVector3** %p225, align 4
  %call240 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %236)
  %237 = load i32, i32* %j227, align 4
  %arrayidx241 = getelementptr inbounds float, float* %call240, i32 %237
  %238 = load float, float* %arrayidx241, align 4
  %239 = load i32, i32* %j227, align 4
  %arrayidx242 = getelementptr inbounds [3 x float], [3 x float]* %bmax220, i32 0, i32 %239
  %240 = load float, float* %arrayidx242, align 4
  %cmp243 = fcmp ogt float %238, %240
  br i1 %cmp243, label %if.then244, label %if.end248

if.then244:                                       ; preds = %if.end239
  %241 = load %class.btVector3*, %class.btVector3** %p225, align 4
  %call245 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %241)
  %242 = load i32, i32* %j227, align 4
  %arrayidx246 = getelementptr inbounds float, float* %call245, i32 %242
  %243 = load float, float* %arrayidx246, align 4
  %244 = load i32, i32* %j227, align 4
  %arrayidx247 = getelementptr inbounds [3 x float], [3 x float]* %bmax220, i32 0, i32 %244
  store float %243, float* %arrayidx247, align 4
  br label %if.end248

if.end248:                                        ; preds = %if.then244, %if.end239
  br label %for.inc249

for.inc249:                                       ; preds = %if.end248
  %245 = load i32, i32* %j227, align 4
  %inc250 = add nsw i32 %245, 1
  store i32 %inc250, i32* %j227, align 4
  br label %for.cond228

for.end251:                                       ; preds = %for.cond228
  br label %for.inc252

for.inc252:                                       ; preds = %for.end251
  %246 = load i32, i32* %i221, align 4
  %inc253 = add i32 %246, 1
  store i32 %inc253, i32* %i221, align 4
  br label %for.cond222

for.end254:                                       ; preds = %for.cond222
  %arrayidx256 = getelementptr inbounds [3 x float], [3 x float]* %bmax220, i32 0, i32 0
  %247 = load float, float* %arrayidx256, align 4
  %arrayidx257 = getelementptr inbounds [3 x float], [3 x float]* %bmin219, i32 0, i32 0
  %248 = load float, float* %arrayidx257, align 4
  %sub258 = fsub float %247, %248
  store float %sub258, float* %dx255, align 4
  %arrayidx260 = getelementptr inbounds [3 x float], [3 x float]* %bmax220, i32 0, i32 1
  %249 = load float, float* %arrayidx260, align 4
  %arrayidx261 = getelementptr inbounds [3 x float], [3 x float]* %bmin219, i32 0, i32 1
  %250 = load float, float* %arrayidx261, align 4
  %sub262 = fsub float %249, %250
  store float %sub262, float* %dy259, align 4
  %arrayidx264 = getelementptr inbounds [3 x float], [3 x float]* %bmax220, i32 0, i32 2
  %251 = load float, float* %arrayidx264, align 4
  %arrayidx265 = getelementptr inbounds [3 x float], [3 x float]* %bmin219, i32 0, i32 2
  %252 = load float, float* %arrayidx265, align 4
  %sub266 = fsub float %251, %252
  store float %sub266, float* %dz263, align 4
  %253 = load float, float* %dx255, align 4
  %cmp267 = fcmp olt float %253, 0x3EB0C6F7A0000000
  br i1 %cmp267, label %if.then274, label %lor.lhs.false268

lor.lhs.false268:                                 ; preds = %for.end254
  %254 = load float, float* %dy259, align 4
  %cmp269 = fcmp olt float %254, 0x3EB0C6F7A0000000
  br i1 %cmp269, label %if.then274, label %lor.lhs.false270

lor.lhs.false270:                                 ; preds = %lor.lhs.false268
  %255 = load float, float* %dz263, align 4
  %cmp271 = fcmp olt float %255, 0x3EB0C6F7A0000000
  br i1 %cmp271, label %if.then274, label %lor.lhs.false272

lor.lhs.false272:                                 ; preds = %lor.lhs.false270
  %256 = load i32*, i32** %vcount.addr, align 4
  %257 = load i32, i32* %256, align 4
  %cmp273 = icmp ult i32 %257, 3
  br i1 %cmp273, label %if.then274, label %if.end328

if.then274:                                       ; preds = %lor.lhs.false272, %lor.lhs.false270, %lor.lhs.false268, %for.end254
  %258 = load float, float* %dx255, align 4
  %mul275 = fmul float %258, 5.000000e-01
  %arrayidx276 = getelementptr inbounds [3 x float], [3 x float]* %bmin219, i32 0, i32 0
  %259 = load float, float* %arrayidx276, align 4
  %add277 = fadd float %mul275, %259
  store float %add277, float* %cx, align 4
  %260 = load float, float* %dy259, align 4
  %mul278 = fmul float %260, 5.000000e-01
  %arrayidx279 = getelementptr inbounds [3 x float], [3 x float]* %bmin219, i32 0, i32 1
  %261 = load float, float* %arrayidx279, align 4
  %add280 = fadd float %mul278, %261
  store float %add280, float* %cy, align 4
  %262 = load float, float* %dz263, align 4
  %mul281 = fmul float %262, 5.000000e-01
  %arrayidx282 = getelementptr inbounds [3 x float], [3 x float]* %bmin219, i32 0, i32 2
  %263 = load float, float* %arrayidx282, align 4
  %add283 = fadd float %mul281, %263
  store float %add283, float* %cz, align 4
  store float 0x47EFFFFFE0000000, float* %len284, align 4
  %264 = load float, float* %dx255, align 4
  %cmp285 = fcmp oge float %264, 0x3EB0C6F7A0000000
  br i1 %cmp285, label %land.lhs.true286, label %if.end289

land.lhs.true286:                                 ; preds = %if.then274
  %265 = load float, float* %dx255, align 4
  %266 = load float, float* %len284, align 4
  %cmp287 = fcmp olt float %265, %266
  br i1 %cmp287, label %if.then288, label %if.end289

if.then288:                                       ; preds = %land.lhs.true286
  %267 = load float, float* %dx255, align 4
  store float %267, float* %len284, align 4
  br label %if.end289

if.end289:                                        ; preds = %if.then288, %land.lhs.true286, %if.then274
  %268 = load float, float* %dy259, align 4
  %cmp290 = fcmp oge float %268, 0x3EB0C6F7A0000000
  br i1 %cmp290, label %land.lhs.true291, label %if.end294

land.lhs.true291:                                 ; preds = %if.end289
  %269 = load float, float* %dy259, align 4
  %270 = load float, float* %len284, align 4
  %cmp292 = fcmp olt float %269, %270
  br i1 %cmp292, label %if.then293, label %if.end294

if.then293:                                       ; preds = %land.lhs.true291
  %271 = load float, float* %dy259, align 4
  store float %271, float* %len284, align 4
  br label %if.end294

if.end294:                                        ; preds = %if.then293, %land.lhs.true291, %if.end289
  %272 = load float, float* %dz263, align 4
  %cmp295 = fcmp oge float %272, 0x3EB0C6F7A0000000
  br i1 %cmp295, label %land.lhs.true296, label %if.end299

land.lhs.true296:                                 ; preds = %if.end294
  %273 = load float, float* %dz263, align 4
  %274 = load float, float* %len284, align 4
  %cmp297 = fcmp olt float %273, %274
  br i1 %cmp297, label %if.then298, label %if.end299

if.then298:                                       ; preds = %land.lhs.true296
  %275 = load float, float* %dz263, align 4
  store float %275, float* %len284, align 4
  br label %if.end299

if.end299:                                        ; preds = %if.then298, %land.lhs.true296, %if.end294
  %276 = load float, float* %len284, align 4
  %cmp300 = fcmp oeq float %276, 0x47EFFFFFE0000000
  br i1 %cmp300, label %if.then301, label %if.else302

if.then301:                                       ; preds = %if.end299
  store float 0x3F847AE140000000, float* %dz263, align 4
  store float 0x3F847AE140000000, float* %dy259, align 4
  store float 0x3F847AE140000000, float* %dx255, align 4
  br label %if.end315

if.else302:                                       ; preds = %if.end299
  %277 = load float, float* %dx255, align 4
  %cmp303 = fcmp olt float %277, 0x3EB0C6F7A0000000
  br i1 %cmp303, label %if.then304, label %if.end306

if.then304:                                       ; preds = %if.else302
  %278 = load float, float* %len284, align 4
  %mul305 = fmul float %278, 0x3FA99999A0000000
  store float %mul305, float* %dx255, align 4
  br label %if.end306

if.end306:                                        ; preds = %if.then304, %if.else302
  %279 = load float, float* %dy259, align 4
  %cmp307 = fcmp olt float %279, 0x3EB0C6F7A0000000
  br i1 %cmp307, label %if.then308, label %if.end310

if.then308:                                       ; preds = %if.end306
  %280 = load float, float* %len284, align 4
  %mul309 = fmul float %280, 0x3FA99999A0000000
  store float %mul309, float* %dy259, align 4
  br label %if.end310

if.end310:                                        ; preds = %if.then308, %if.end306
  %281 = load float, float* %dz263, align 4
  %cmp311 = fcmp olt float %281, 0x3EB0C6F7A0000000
  br i1 %cmp311, label %if.then312, label %if.end314

if.then312:                                       ; preds = %if.end310
  %282 = load float, float* %len284, align 4
  %mul313 = fmul float %282, 0x3FA99999A0000000
  store float %mul313, float* %dz263, align 4
  br label %if.end314

if.end314:                                        ; preds = %if.then312, %if.end310
  br label %if.end315

if.end315:                                        ; preds = %if.end314, %if.then301
  %283 = load float, float* %cx, align 4
  %284 = load float, float* %dx255, align 4
  %sub317 = fsub float %283, %284
  store float %sub317, float* %x1316, align 4
  %285 = load float, float* %cx, align 4
  %286 = load float, float* %dx255, align 4
  %add319 = fadd float %285, %286
  store float %add319, float* %x2318, align 4
  %287 = load float, float* %cy, align 4
  %288 = load float, float* %dy259, align 4
  %sub321 = fsub float %287, %288
  store float %sub321, float* %y1320, align 4
  %289 = load float, float* %cy, align 4
  %290 = load float, float* %dy259, align 4
  %add323 = fadd float %289, %290
  store float %add323, float* %y2322, align 4
  %291 = load float, float* %cz, align 4
  %292 = load float, float* %dz263, align 4
  %sub325 = fsub float %291, %292
  store float %sub325, float* %z1324, align 4
  %293 = load float, float* %cz, align 4
  %294 = load float, float* %dz263, align 4
  %add327 = fadd float %293, %294
  store float %add327, float* %z2326, align 4
  %295 = load i32*, i32** %vcount.addr, align 4
  store i32 0, i32* %295, align 4
  %296 = load i32*, i32** %vcount.addr, align 4
  %297 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %298 = load float, float* %x1316, align 4
  %299 = load float, float* %y1320, align 4
  %300 = load float, float* %z1324, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %296, %class.btVector3* %297, float %298, float %299, float %300)
  %301 = load i32*, i32** %vcount.addr, align 4
  %302 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %303 = load float, float* %x2318, align 4
  %304 = load float, float* %y1320, align 4
  %305 = load float, float* %z1324, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %301, %class.btVector3* %302, float %303, float %304, float %305)
  %306 = load i32*, i32** %vcount.addr, align 4
  %307 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %308 = load float, float* %x2318, align 4
  %309 = load float, float* %y2322, align 4
  %310 = load float, float* %z1324, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %306, %class.btVector3* %307, float %308, float %309, float %310)
  %311 = load i32*, i32** %vcount.addr, align 4
  %312 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %313 = load float, float* %x1316, align 4
  %314 = load float, float* %y2322, align 4
  %315 = load float, float* %z1324, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %311, %class.btVector3* %312, float %313, float %314, float %315)
  %316 = load i32*, i32** %vcount.addr, align 4
  %317 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %318 = load float, float* %x1316, align 4
  %319 = load float, float* %y1320, align 4
  %320 = load float, float* %z2326, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %316, %class.btVector3* %317, float %318, float %319, float %320)
  %321 = load i32*, i32** %vcount.addr, align 4
  %322 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %323 = load float, float* %x2318, align 4
  %324 = load float, float* %y1320, align 4
  %325 = load float, float* %z2326, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %321, %class.btVector3* %322, float %323, float %324, float %325)
  %326 = load i32*, i32** %vcount.addr, align 4
  %327 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %328 = load float, float* %x2318, align 4
  %329 = load float, float* %y2322, align 4
  %330 = load float, float* %z2326, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %326, %class.btVector3* %327, float %328, float %329, float %330)
  %331 = load i32*, i32** %vcount.addr, align 4
  %332 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %333 = load float, float* %x1316, align 4
  %334 = load float, float* %y2322, align 4
  %335 = load float, float* %z2326, align 4
  call void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %331, %class.btVector3* %332, float %333, float %334, float %335)
  store i1 true, i1* %retval, align 1
  br label %return

if.end328:                                        ; preds = %lor.lhs.false272
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end328, %if.end315, %if.end87, %if.then
  %336 = load i1, i1* %retval, align 1
  ret i1 %336
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN11HullLibrary16BringOutYourDeadEPK9btVector3jPS0_RjPjj(%class.HullLibrary* %this, %class.btVector3* %verts, i32 %vcount, %class.btVector3* %overts, i32* nonnull align 4 dereferenceable(4) %ocount, i32* %indices, i32 %indexcount) #2 {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  %verts.addr = alloca %class.btVector3*, align 4
  %vcount.addr = alloca i32, align 4
  %overts.addr = alloca %class.btVector3*, align 4
  %ocount.addr = alloca i32*, align 4
  %indices.addr = alloca i32*, align 4
  %indexcount.addr = alloca i32, align 4
  %tmpIndices = alloca %class.btAlignedObjectArray.12, align 4
  %ref.tmp = alloca i32, align 4
  %i = alloca i32, align 4
  %usedIndices = alloca %class.btAlignedObjectArray.16, align 4
  %ref.tmp9 = alloca i32, align 4
  %v = alloca i32, align 4
  %k = alloca i32, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store %class.btVector3* %verts, %class.btVector3** %verts.addr, align 4
  store i32 %vcount, i32* %vcount.addr, align 4
  store %class.btVector3* %overts, %class.btVector3** %overts.addr, align 4
  store i32* %ocount, i32** %ocount.addr, align 4
  store i32* %indices, i32** %indices.addr, align 4
  store i32 %indexcount, i32* %indexcount.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %call = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.12* %tmpIndices)
  %m_vertexIndexMapping = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.12* %m_vertexIndexMapping)
  store i32 0, i32* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.12* %tmpIndices, i32 %call2, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_vertexIndexMapping3 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.12* %m_vertexIndexMapping3)
  %cmp = icmp slt i32 %0, %call4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_vertexIndexMapping5 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  %1 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %m_vertexIndexMapping5, i32 %1)
  %2 = load i32, i32* %call6, align 4
  %3 = load i32, i32* %i, align 4
  %call7 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %tmpIndices, i32 %3)
  store i32 %2, i32* %call7, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call8 = call %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.16* %usedIndices)
  %5 = load i32, i32* %vcount.addr, align 4
  store i32 0, i32* %ref.tmp9, align 4
  call void @_ZN20btAlignedObjectArrayIjE6resizeEiRKj(%class.btAlignedObjectArray.16* %usedIndices, i32 %5, i32* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %call10 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.16* %usedIndices, i32 0)
  %6 = bitcast i32* %call10 to i8*
  %7 = load i32, i32* %vcount.addr, align 4
  %mul = mul i32 4, %7
  call void @llvm.memset.p0i8.i32(i8* align 4 %6, i8 0, i32 %mul, i1 false)
  %8 = load i32*, i32** %ocount.addr, align 4
  store i32 0, i32* %8, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond11

for.cond11:                                       ; preds = %for.inc52, %for.end
  %9 = load i32, i32* %i, align 4
  %10 = load i32, i32* %indexcount.addr, align 4
  %cmp12 = icmp slt i32 %9, %10
  br i1 %cmp12, label %for.body13, label %for.end54

for.body13:                                       ; preds = %for.cond11
  %11 = load i32*, i32** %indices.addr, align 4
  %12 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %11, i32 %12
  %13 = load i32, i32* %arrayidx, align 4
  store i32 %13, i32* %v, align 4
  %14 = load i32, i32* %v, align 4
  %call14 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.16* %usedIndices, i32 %14)
  %15 = load i32, i32* %call14, align 4
  %tobool = icmp ne i32 %15, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.body13
  %16 = load i32, i32* %v, align 4
  %call15 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.16* %usedIndices, i32 %16)
  %17 = load i32, i32* %call15, align 4
  %sub = sub i32 %17, 1
  %18 = load i32*, i32** %indices.addr, align 4
  %19 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds i32, i32* %18, i32 %19
  store i32 %sub, i32* %arrayidx16, align 4
  br label %if.end51

if.else:                                          ; preds = %for.body13
  %20 = load i32*, i32** %ocount.addr, align 4
  %21 = load i32, i32* %20, align 4
  %22 = load i32*, i32** %indices.addr, align 4
  %23 = load i32, i32* %i, align 4
  %arrayidx17 = getelementptr inbounds i32, i32* %22, i32 %23
  store i32 %21, i32* %arrayidx17, align 4
  %24 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %25 = load i32, i32* %v, align 4
  %arrayidx18 = getelementptr inbounds %class.btVector3, %class.btVector3* %24, i32 %25
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx18)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 0
  %26 = load float, float* %arrayidx20, align 4
  %27 = load %class.btVector3*, %class.btVector3** %overts.addr, align 4
  %28 = load i32*, i32** %ocount.addr, align 4
  %29 = load i32, i32* %28, align 4
  %arrayidx21 = getelementptr inbounds %class.btVector3, %class.btVector3* %27, i32 %29
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx21)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 0
  store float %26, float* %arrayidx23, align 4
  %30 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %31 = load i32, i32* %v, align 4
  %arrayidx24 = getelementptr inbounds %class.btVector3, %class.btVector3* %30, i32 %31
  %call25 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 1
  %32 = load float, float* %arrayidx26, align 4
  %33 = load %class.btVector3*, %class.btVector3** %overts.addr, align 4
  %34 = load i32*, i32** %ocount.addr, align 4
  %35 = load i32, i32* %34, align 4
  %arrayidx27 = getelementptr inbounds %class.btVector3, %class.btVector3* %33, i32 %35
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx27)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 1
  store float %32, float* %arrayidx29, align 4
  %36 = load %class.btVector3*, %class.btVector3** %verts.addr, align 4
  %37 = load i32, i32* %v, align 4
  %arrayidx30 = getelementptr inbounds %class.btVector3, %class.btVector3* %36, i32 %37
  %call31 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx30)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 2
  %38 = load float, float* %arrayidx32, align 4
  %39 = load %class.btVector3*, %class.btVector3** %overts.addr, align 4
  %40 = load i32*, i32** %ocount.addr, align 4
  %41 = load i32, i32* %40, align 4
  %arrayidx33 = getelementptr inbounds %class.btVector3, %class.btVector3* %39, i32 %41
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %arrayidx33)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 2
  store float %38, float* %arrayidx35, align 4
  store i32 0, i32* %k, align 4
  br label %for.cond36

for.cond36:                                       ; preds = %for.inc46, %if.else
  %42 = load i32, i32* %k, align 4
  %m_vertexIndexMapping37 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  %call38 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.12* %m_vertexIndexMapping37)
  %cmp39 = icmp slt i32 %42, %call38
  br i1 %cmp39, label %for.body40, label %for.end48

for.body40:                                       ; preds = %for.cond36
  %43 = load i32, i32* %k, align 4
  %call41 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %tmpIndices, i32 %43)
  %44 = load i32, i32* %call41, align 4
  %45 = load i32, i32* %v, align 4
  %cmp42 = icmp eq i32 %44, %45
  br i1 %cmp42, label %if.then43, label %if.end

if.then43:                                        ; preds = %for.body40
  %46 = load i32*, i32** %ocount.addr, align 4
  %47 = load i32, i32* %46, align 4
  %m_vertexIndexMapping44 = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  %48 = load i32, i32* %k, align 4
  %call45 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %m_vertexIndexMapping44, i32 %48)
  store i32 %47, i32* %call45, align 4
  br label %if.end

if.end:                                           ; preds = %if.then43, %for.body40
  br label %for.inc46

for.inc46:                                        ; preds = %if.end
  %49 = load i32, i32* %k, align 4
  %inc47 = add nsw i32 %49, 1
  store i32 %inc47, i32* %k, align 4
  br label %for.cond36

for.end48:                                        ; preds = %for.cond36
  %50 = load i32*, i32** %ocount.addr, align 4
  %51 = load i32, i32* %50, align 4
  %inc49 = add i32 %51, 1
  store i32 %inc49, i32* %50, align 4
  %52 = load i32*, i32** %ocount.addr, align 4
  %53 = load i32, i32* %52, align 4
  %54 = load i32, i32* %v, align 4
  %call50 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.16* %usedIndices, i32 %54)
  store i32 %53, i32* %call50, align 4
  br label %if.end51

if.end51:                                         ; preds = %for.end48, %if.then
  br label %for.inc52

for.inc52:                                        ; preds = %if.end51
  %55 = load i32, i32* %i, align 4
  %inc53 = add nsw i32 %55, 1
  store i32 %inc53, i32* %i, align 4
  br label %for.cond11

for.end54:                                        ; preds = %for.cond11
  %call55 = call %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.16* %usedIndices) #7
  %call56 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.12* %tmpIndices) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK8HullDesc11HasHullFlagE8HullFlag(%class.HullDesc* %this, i32 %flag) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.HullDesc*, align 4
  %flag.addr = alloca i32, align 4
  store %class.HullDesc* %this, %class.HullDesc** %this.addr, align 4
  store i32 %flag, i32* %flag.addr, align 4
  %this1 = load %class.HullDesc*, %class.HullDesc** %this.addr, align 4
  %mFlags = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 0
  %0 = load i32, i32* %mFlags, align 4
  %1 = load i32, i32* %flag.addr, align 4
  %and = and i32 %0, %1
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %2 = load i1, i1* %retval, align 1
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.PHullResult* @_ZN11PHullResultD2Ev(%class.PHullResult* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.PHullResult*, align 4
  store %class.PHullResult* %this, %class.PHullResult** %this.addr, align 4
  %this1 = load %class.PHullResult*, %class.PHullResult** %this.addr, align 4
  %m_Indices = getelementptr inbounds %class.PHullResult, %class.PHullResult* %this1, i32 0, i32 4
  %call = call %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.16* %m_Indices) #7
  ret %class.PHullResult* %this1
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN11HullLibrary13ReleaseResultER10HullResult(%class.HullLibrary* %this, %class.HullResult* nonnull align 4 dereferenceable(56) %result) #2 {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  %result.addr = alloca %class.HullResult*, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  store %class.HullResult* %result, %class.HullResult** %result.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %0 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %m_OutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %0, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_OutputVertices)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %mNumOutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %1, i32 0, i32 1
  store i32 0, i32* %mNumOutputVertices, align 4
  %2 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %m_OutputVertices2 = getelementptr inbounds %class.HullResult, %class.HullResult* %2, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %m_OutputVertices2)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %3 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %m_Indices = getelementptr inbounds %class.HullResult, %class.HullResult* %3, i32 0, i32 5
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.16* %m_Indices)
  %tobool4 = icmp ne i32 %call3, 0
  br i1 %tobool4, label %if.then5, label %if.end7

if.then5:                                         ; preds = %if.end
  %4 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %mNumIndices = getelementptr inbounds %class.HullResult, %class.HullResult* %4, i32 0, i32 4
  store i32 0, i32* %mNumIndices, align 4
  %5 = load %class.HullResult*, %class.HullResult** %result.addr, align 4
  %m_Indices6 = getelementptr inbounds %class.HullResult, %class.HullResult* %5, i32 0, i32 5
  call void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.16* %m_Indices6)
  br label %if.end7

if.end7:                                          ; preds = %if.then5, %if.end
  ret i32 0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden float @_Z7GetDistfffPKf(float %px, float %py, float %pz, float* %p2) #1 {
entry:
  %px.addr = alloca float, align 4
  %py.addr = alloca float, align 4
  %pz.addr = alloca float, align 4
  %p2.addr = alloca float*, align 4
  %dx = alloca float, align 4
  %dy = alloca float, align 4
  %dz = alloca float, align 4
  store float %px, float* %px.addr, align 4
  store float %py, float* %py.addr, align 4
  store float %pz, float* %pz.addr, align 4
  store float* %p2, float** %p2.addr, align 4
  %0 = load float, float* %px.addr, align 4
  %1 = load float*, float** %p2.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %1, i32 0
  %2 = load float, float* %arrayidx, align 4
  %sub = fsub float %0, %2
  store float %sub, float* %dx, align 4
  %3 = load float, float* %py.addr, align 4
  %4 = load float*, float** %p2.addr, align 4
  %arrayidx1 = getelementptr inbounds float, float* %4, i32 1
  %5 = load float, float* %arrayidx1, align 4
  %sub2 = fsub float %3, %5
  store float %sub2, float* %dy, align 4
  %6 = load float, float* %pz.addr, align 4
  %7 = load float*, float** %p2.addr, align 4
  %arrayidx3 = getelementptr inbounds float, float* %7, i32 2
  %8 = load float, float* %arrayidx3, align 4
  %sub4 = fsub float %6, %8
  store float %sub4, float* %dz, align 4
  %9 = load float, float* %dx, align 4
  %10 = load float, float* %dx, align 4
  %mul = fmul float %9, %10
  %11 = load float, float* %dy, align 4
  %12 = load float, float* %dy, align 4
  %mul5 = fmul float %11, %12
  %add = fadd float %mul, %mul5
  %13 = load float, float* %dz, align 4
  %14 = load float, float* %dz, align 4
  %mul6 = fmul float %13, %14
  %add7 = fadd float %add, %mul6
  ret float %add7
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.12* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store i32* %fillData, i32** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %5 = load i32*, i32** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.12* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %14 = load i32*, i32** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %14, i32 %15
  %16 = bitcast i32* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to i32*
  %18 = load i32*, i32** %fillData.addr, align 4
  %19 = load i32, i32* %18, align 4
  store i32 %19, i32* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #5

; Function Attrs: noinline nounwind optnone
define internal void @_ZL8addPointRjP9btVector3fff(i32* nonnull align 4 dereferenceable(4) %vcount, %class.btVector3* %p, float %x, float %y, float %z) #1 {
entry:
  %vcount.addr = alloca i32*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  %z.addr = alloca float, align 4
  %dest = alloca %class.btVector3*, align 4
  store i32* %vcount, i32** %vcount.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  store float %z, float* %z.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %1 = load i32*, i32** %vcount.addr, align 4
  %2 = load i32, i32* %1, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %2
  store %class.btVector3* %arrayidx, %class.btVector3** %dest, align 4
  %3 = load float, float* %x.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %dest, align 4
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %4)
  %arrayidx1 = getelementptr inbounds float, float* %call, i32 0
  store float %3, float* %arrayidx1, align 4
  %5 = load float, float* %y.addr, align 4
  %6 = load %class.btVector3*, %class.btVector3** %dest, align 4
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %6)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 1
  store float %5, float* %arrayidx3, align 4
  %7 = load float, float* %z.addr, align 4
  %8 = load %class.btVector3*, %class.btVector3** %dest, align 4
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %8)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 2
  store float %7, float* %arrayidx5, align 4
  %9 = load i32*, i32** %vcount.addr, align 4
  %10 = load i32, i32* %9, align 4
  %inc = add i32 %10, 1
  store i32 %inc, i32* %9, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.16* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.17* @_ZN18btAlignedAllocatorIjLj16EEC2Ev(%class.btAlignedAllocator.17* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.16* %this1)
  ret %class.btAlignedObjectArray.16* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.16* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.16* %this1)
  ret %class.btAlignedObjectArray.16* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMinIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %b.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %a.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31wEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z8btSetMaxIfEvRT_RKS0_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = load float*, float** %b.addr, align 4
  %5 = load float, float* %4, align 4
  %6 = load float*, float** %a.addr, align 4
  store float %5, float* %6, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.17* @_ZN18btAlignedAllocatorIjLj16EEC2Ev(%class.btAlignedAllocator.17* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  ret %class.btAlignedAllocator.17* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.16* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIN7ConvexH8HalfEdgeELj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %"class.ConvexH::HalfEdge"* null, %"class.ConvexH::HalfEdge"** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorI7btPlaneLj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI7btPlaneE4initEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btPlane* null, %class.btPlane** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %2, %class.btVector3** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %class.btVector3*, %class.btVector3** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btVector3* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* %4, %class.btVector3** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btVector3* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %9 = bitcast %class.btVector3* %6 to i8*
  %10 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.btVector3** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.btVector3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"class.ConvexH::HalfEdge"*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %"class.ConvexH::HalfEdge"*
  store %"class.ConvexH::HalfEdge"* %2, %"class.ConvexH::HalfEdge"** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %3 = load %"class.ConvexH::HalfEdge"*, %"class.ConvexH::HalfEdge"** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE4copyEiiPS1_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %"class.ConvexH::HalfEdge"* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %"class.ConvexH::HalfEdge"*, %"class.ConvexH::HalfEdge"** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %"class.ConvexH::HalfEdge"* %4, %"class.ConvexH::HalfEdge"** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %"class.ConvexH::HalfEdge"* @_ZN18btAlignedAllocatorIN7ConvexH8HalfEdgeELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %"class.ConvexH::HalfEdge"** null)
  %2 = bitcast %"class.ConvexH::HalfEdge"* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE4copyEiiPS1_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %"class.ConvexH::HalfEdge"* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"class.ConvexH::HalfEdge"*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %"class.ConvexH::HalfEdge"* %dest, %"class.ConvexH::HalfEdge"** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %"class.ConvexH::HalfEdge"*, %"class.ConvexH::HalfEdge"** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.ConvexH::HalfEdge", %"class.ConvexH::HalfEdge"* %3, i32 %4
  %5 = bitcast %"class.ConvexH::HalfEdge"* %arrayidx to i8*
  %6 = bitcast i8* %5 to %"class.ConvexH::HalfEdge"*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load %"class.ConvexH::HalfEdge"*, %"class.ConvexH::HalfEdge"** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %"class.ConvexH::HalfEdge", %"class.ConvexH::HalfEdge"* %7, i32 %8
  %9 = bitcast %"class.ConvexH::HalfEdge"* %6 to i8*
  %10 = bitcast %"class.ConvexH::HalfEdge"* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %9, i8* align 2 %10, i32 4, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %"class.ConvexH::HalfEdge"*, %"class.ConvexH::HalfEdge"** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"class.ConvexH::HalfEdge", %"class.ConvexH::HalfEdge"* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN7ConvexH8HalfEdgeEE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %"class.ConvexH::HalfEdge"*, %"class.ConvexH::HalfEdge"** %m_data, align 4
  %tobool = icmp ne %"class.ConvexH::HalfEdge"* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %"class.ConvexH::HalfEdge"*, %"class.ConvexH::HalfEdge"** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIN7ConvexH8HalfEdgeELj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %m_allocator, %"class.ConvexH::HalfEdge"* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %"class.ConvexH::HalfEdge"* null, %"class.ConvexH::HalfEdge"** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.ConvexH::HalfEdge"* @_ZN18btAlignedAllocatorIN7ConvexH8HalfEdgeELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.1* %this, i32 %n, %"class.ConvexH::HalfEdge"** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"class.ConvexH::HalfEdge"**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %"class.ConvexH::HalfEdge"** %hint, %"class.ConvexH::HalfEdge"*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"class.ConvexH::HalfEdge"*
  ret %"class.ConvexH::HalfEdge"* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIN7ConvexH8HalfEdgeELj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %this, %"class.ConvexH::HalfEdge"* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %"class.ConvexH::HalfEdge"*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %"class.ConvexH::HalfEdge"* %ptr, %"class.ConvexH::HalfEdge"** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %"class.ConvexH::HalfEdge"*, %"class.ConvexH::HalfEdge"** %ptr.addr, align 4
  %1 = bitcast %"class.ConvexH::HalfEdge"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI7btPlaneE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI7btPlaneE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPlane*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI7btPlaneE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI7btPlaneE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btPlane*
  store %class.btPlane* %2, %class.btPlane** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI7btPlaneE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %3 = load %class.btPlane*, %class.btPlane** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI7btPlaneE4copyEiiPS0_(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, %class.btPlane* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI7btPlaneE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayI7btPlaneE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI7btPlaneE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btPlane*, %class.btPlane** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btPlane* %4, %class.btPlane** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI7btPlaneE8capacityEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI7btPlaneE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btPlane* @_ZN18btAlignedAllocatorI7btPlaneLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.5* %m_allocator, i32 %1, %class.btPlane** null)
  %2 = bitcast %class.btPlane* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI7btPlaneE4copyEiiPS0_(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, %class.btPlane* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPlane*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btPlane* %dest, %class.btPlane** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btPlane*, %class.btPlane** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPlane, %class.btPlane* %3, i32 %4
  %5 = bitcast %class.btPlane* %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btPlane*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %7 = load %class.btPlane*, %class.btPlane** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btPlane, %class.btPlane* %7, i32 %8
  %9 = bitcast %class.btPlane* %6 to i8*
  %10 = bitcast %class.btPlane* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 20, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI7btPlaneE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load %class.btPlane*, %class.btPlane** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPlane, %class.btPlane* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI7btPlaneE10deallocateEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btPlane*, %class.btPlane** %m_data, align 4
  %tobool = icmp ne %class.btPlane* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btPlane*, %class.btPlane** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI7btPlaneLj16EE10deallocateEPS0_(%class.btAlignedAllocator.5* %m_allocator, %class.btPlane* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btPlane* null, %class.btPlane** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPlane* @_ZN18btAlignedAllocatorI7btPlaneLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.5* %this, i32 %n, %class.btPlane** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPlane**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btPlane** %hint, %class.btPlane*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 20, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPlane*
  ret %class.btPlane* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI7btPlaneLj16EE10deallocateEPS0_(%class.btAlignedAllocator.5* %this, %class.btPlane* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca %class.btPlane*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store %class.btPlane* %ptr, %class.btPlane** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load %class.btPlane*, %class.btPlane** %ptr.addr, align 4
  %1 = bitcast %class.btPlane* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE8capacityEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btHullTriangle**, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP14btHullTriangleE8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btHullTriangle**
  store %class.btHullTriangle** %2, %class.btHullTriangle*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %3 = load %class.btHullTriangle**, %class.btHullTriangle*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4copyEiiPS1_(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, %class.btHullTriangle** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btHullTriangle**, %class.btHullTriangle*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %class.btHullTriangle** %4, %class.btHullTriangle*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP14btHullTriangleE9allocSizeEi(%class.btAlignedObjectArray.8* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP14btHullTriangleE8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btHullTriangle** @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.9* %m_allocator, i32 %1, %class.btHullTriangle*** null)
  %2 = bitcast %class.btHullTriangle** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4copyEiiPS1_(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, %class.btHullTriangle** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btHullTriangle**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btHullTriangle** %dest, %class.btHullTriangle*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btHullTriangle**, %class.btHullTriangle*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btHullTriangle*, %class.btHullTriangle** %3, i32 %4
  %5 = bitcast %class.btHullTriangle** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btHullTriangle**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %7 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btHullTriangle*, %class.btHullTriangle** %7, i32 %8
  %9 = load %class.btHullTriangle*, %class.btHullTriangle** %arrayidx2, align 4
  store %class.btHullTriangle* %9, %class.btHullTriangle** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %3 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btHullTriangle*, %class.btHullTriangle** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data, align 4
  %tobool = icmp ne %class.btHullTriangle** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_(%class.btAlignedAllocator.9* %m_allocator, %class.btHullTriangle** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %class.btHullTriangle** null, %class.btHullTriangle*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btHullTriangle** @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.9* %this, i32 %n, %class.btHullTriangle*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btHullTriangle***, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btHullTriangle*** %hint, %class.btHullTriangle**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btHullTriangle**
  ret %class.btHullTriangle** %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_(%class.btAlignedAllocator.9* %this, %class.btHullTriangle** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca %class.btHullTriangle**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store %class.btHullTriangle** %ptr, %class.btHullTriangle*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load %class.btHullTriangle**, %class.btHullTriangle*** %ptr.addr, align 4
  %1 = bitcast %class.btHullTriangle** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z14maxdirfilteredI9btVector3EiPKT_iRS2_R20btAlignedObjectArrayIiE(%class.btVector3* %p, i32 %count, %class.btVector3* nonnull align 4 dereferenceable(16) %dir, %class.btAlignedObjectArray.12* nonnull align 4 dereferenceable(17) %allow) #2 comdat {
entry:
  %p.addr = alloca %class.btVector3*, align 4
  %count.addr = alloca i32, align 4
  %dir.addr = alloca %class.btVector3*, align 4
  %allow.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %m = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store i32 %count, i32* %count.addr, align 4
  store %class.btVector3* %dir, %class.btVector3** %dir.addr, align 4
  store %class.btAlignedObjectArray.12* %allow, %class.btAlignedObjectArray.12** %allow.addr, align 4
  store i32 -1, i32* %m, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %count.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %allow.addr, align 4
  %3 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.12* %2, i32 %3)
  %4 = load i32, i32* %call, align 4
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.then, label %if.end7

if.then:                                          ; preds = %for.body
  %5 = load i32, i32* %m, align 4
  %cmp1 = icmp eq i32 %5, -1
  br i1 %cmp1, label %if.then6, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.then
  %6 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 %7
  %8 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4
  %call2 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %9 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %10 = load i32, i32* %m, align 4
  %arrayidx3 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 %10
  %11 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4
  %call4 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3, %class.btVector3* nonnull align 4 dereferenceable(16) %11)
  %cmp5 = fcmp ogt float %call2, %call4
  br i1 %cmp5, label %if.then6, label %if.end

if.then6:                                         ; preds = %lor.lhs.false, %if.then
  %12 = load i32, i32* %i, align 4
  store i32 %12, i32* %m, align 4
  br label %if.end

if.end:                                           ; preds = %if.then6, %lor.lhs.false
  br label %if.end7

if.end7:                                          ; preds = %if.end, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end7
  %13 = load i32, i32* %i, align 4
  %inc = add nsw i32 %13, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %14 = load i32, i32* %m, align 4
  ret i32 %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btSinf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.sin.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z5btCosf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.cos.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sin.f32(float) #6

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.cos.f32(float) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.13* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  ret %class.btAlignedAllocator.13* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.12* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.12* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.12* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.12* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.13* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.13* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.12* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.13* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.12* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i32* %dest, i32** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = bitcast i32* %arrayidx to i8*
  %6 = bitcast i8* %5 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx2, align 4
  store i32 %9, i32* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.13* %this, i32 %n, i32** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32** %hint, i32*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIiE9allocSizeEi(%class.btAlignedObjectArray.12* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE7reserveEi(%class.btAlignedObjectArray.16* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE8capacityEv(%class.btAlignedObjectArray.16* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIjE8allocateEi(%class.btAlignedObjectArray.16* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i32*
  store i32* %2, i32** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  %3 = load i32*, i32** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIjE4copyEiiPj(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call3, i32* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.16* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i32*, i32** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store i32* %4, i32** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIjE8capacityEv(%class.btAlignedObjectArray.16* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIjE8allocateEi(%class.btAlignedObjectArray.16* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32* @_ZN18btAlignedAllocatorIjLj16EE8allocateEiPPKj(%class.btAlignedAllocator.17* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIjE4copyEiiPj(%class.btAlignedObjectArray.16* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i32* %dest, i32** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = bitcast i32* %arrayidx to i8*
  %6 = bitcast i8* %5 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx2, align 4
  store i32 %9, i32* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.16* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.16* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj(%class.btAlignedAllocator.17* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIjLj16EE8allocateEiPPKj(%class.btAlignedAllocator.17* %this, i32 %n, i32** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32** %hint, i32*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj(%class.btAlignedAllocator.17* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btConvexHull.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn writeonly }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
