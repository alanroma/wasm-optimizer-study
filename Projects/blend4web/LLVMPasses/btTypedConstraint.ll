; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btTypedConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btTypedConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.1, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.0, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.0 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.btSerializer = type { i32 (...)** }
%struct.btTypedConstraintFloatData = type { %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData*, i8*, i32, i32, i32, i32, float, float, i32, i32, float, i32 }
%struct.btRigidBodyFloatData = type { %struct.btCollisionObjectFloatData, %struct.btMatrix3x3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, i32 }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type opaque
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btVector3FloatData = type { [4 x float] }
%class.btAngularLimit = type <{ float, float, float, float, float, float, float, i8, [3 x i8] }>
%class.btAlignedObjectArray.5 = type opaque
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN13btTypedObjectC2Ei = comdat any

$_ZNK11btRigidBody20getNumConstraintRefsEv = comdat any

$_ZN11btRigidBody16getConstraintRefEi = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN11btRigidBodyD2Ev = comdat any

$_Z16btNormalizeAnglef = comdat any

$_Z7btEqualff = comdat any

$_ZN17btTypedConstraintD2Ev = comdat any

$_ZN17btTypedConstraintD0Ev = comdat any

$_ZN17btTypedConstraint13buildJacobianEv = comdat any

$_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif = comdat any

$_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f = comdat any

$_ZNK17btTypedConstraint28calculateSerializeBufferSizeEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_ = comdat any

$_Z6btFmodff = comdat any

$_ZTS13btTypedObject = comdat any

$_ZTI13btTypedObject = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV17btTypedConstraint = hidden unnamed_addr constant { [13 x i8*] } { [13 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i32, i32, i8*, i32 }* @_ZTI17btTypedConstraint to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btTypedConstraint*)* @_ZN17btTypedConstraintD2Ev to i8*), i8* bitcast (void (%class.btTypedConstraint*)* @_ZN17btTypedConstraintD0Ev to i8*), i8* bitcast (void (%class.btTypedConstraint*)* @_ZN17btTypedConstraint13buildJacobianEv to i8*), i8* bitcast (void (%class.btTypedConstraint*, %class.btAlignedObjectArray.5*, i32, i32, float)* @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)* @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (i32 (%class.btTypedConstraint*)* @_ZNK17btTypedConstraint28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btTypedConstraint*, i8*, %class.btSerializer*)* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer to i8*)] }, align 4
@.str = private unnamed_addr constant [27 x i8] c"btTypedConstraintFloatData\00", align 1
@_ZZN17btTypedConstraint12getFixedBodyEvE7s_fixed = internal global %class.btRigidBody zeroinitializer, align 4
@_ZGVZN17btTypedConstraint12getFixedBodyEvE7s_fixed = internal global i32 0, align 4
@__dso_handle = external hidden global i8
@_ZTVN10__cxxabiv121__vmi_class_type_infoE = external global i8*
@_ZTS17btTypedConstraint = hidden constant [20 x i8] c"17btTypedConstraint\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS13btTypedObject = linkonce_odr hidden constant [16 x i8] c"13btTypedObject\00", comdat, align 1
@_ZTI13btTypedObject = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([16 x i8], [16 x i8]* @_ZTS13btTypedObject, i32 0, i32 0) }, comdat, align 4
@_ZTI17btTypedConstraint = hidden constant { i8*, i8*, i32, i32, i8*, i32 } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv121__vmi_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btTypedConstraint, i32 0, i32 0), i32 0, i32 1, i8* bitcast ({ i8*, i8* }* @_ZTI13btTypedObject to i8*), i32 1026 }, align 4
@_ZTV11btRigidBody = external unnamed_addr constant { [9 x i8*] }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btTypedConstraint.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody(%class.btTypedConstraint* returned %this, i32 %type, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbA) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %type.addr = alloca i32, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i8*
  %1 = getelementptr inbounds i8, i8* %0, i32 4
  %2 = bitcast i8* %1 to %struct.btTypedObject*
  %3 = load i32, i32* %type.addr, align 4
  %call = call %struct.btTypedObject* @_ZN13btTypedObjectC2Ei(%struct.btTypedObject* %2, i32 %3)
  %4 = bitcast %class.btTypedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btTypedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4
  %m_userConstraintType = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 2
  store i32 -1, i32* %m_userConstraintType, align 4
  %5 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 3
  %m_userConstraintPtr = bitcast %union.anon.0* %5 to i8**
  store i8* inttoptr (i32 -1 to i8*), i8** %m_userConstraintPtr, align 4
  %m_breakingImpulseThreshold = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 4
  store float 0x47EFFFFFE0000000, float* %m_breakingImpulseThreshold, align 4
  %m_isEnabled = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 5
  store i8 1, i8* %m_isEnabled, align 4
  %m_needsFeedback = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 6
  store i8 0, i8* %m_needsFeedback, align 1
  %m_overrideNumSolverIterations = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 7
  store i32 -1, i32* %m_overrideNumSolverIterations, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %6 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  store %class.btRigidBody* %6, %class.btRigidBody** %m_rbA, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %call2 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint12getFixedBodyEv()
  store %class.btRigidBody* %call2, %class.btRigidBody** %m_rbB, align 4
  %m_appliedImpulse = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %m_dbgDrawSize = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 11
  store float 0x3FD3333340000000, float* %m_dbgDrawSize, align 4
  %m_jointFeedback = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 12
  store %struct.btJointFeedback* null, %struct.btJointFeedback** %m_jointFeedback, align 4
  ret %class.btTypedConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btTypedObject* @_ZN13btTypedObjectC2Ei(%struct.btTypedObject* returned %this, i32 %objectType) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btTypedObject*, align 4
  %objectType.addr = alloca i32, align 4
  store %struct.btTypedObject* %this, %struct.btTypedObject** %this.addr, align 4
  store i32 %objectType, i32* %objectType.addr, align 4
  %this1 = load %struct.btTypedObject*, %struct.btTypedObject** %this.addr, align 4
  %m_objectType = getelementptr inbounds %struct.btTypedObject, %struct.btTypedObject* %this1, i32 0, i32 0
  %0 = load i32, i32* %objectType.addr, align 4
  store i32 %0, i32* %m_objectType, align 4
  ret %struct.btTypedObject* %this1
}

; Function Attrs: noinline optnone
define hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZN17btTypedConstraint12getFixedBodyEv() #2 {
entry:
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN17btTypedConstraint12getFixedBodyEvE7s_fixed to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN17btTypedConstraint12getFixedBodyEvE7s_fixed) #3
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %call4 = call %class.btRigidBody* @_ZN11btRigidBodyC1EfP13btMotionStateP16btCollisionShapeRK9btVector3(%class.btRigidBody* @_ZZN17btTypedConstraint12getFixedBodyEvE7s_fixed, float 0.000000e+00, %class.btMotionState* null, %class.btCollisionShape* null, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = call i32 @__cxa_atexit(void (i8*)* @__cxx_global_array_dtor, i8* null, i8* @__dso_handle) #3
  call void @__cxa_guard_release(i32* @_ZGVZN17btTypedConstraint12getFixedBodyEvE7s_fixed) #3
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  %call9 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  call void @_ZN11btRigidBody12setMassPropsEfRK9btVector3(%class.btRigidBody* @_ZZN17btTypedConstraint12getFixedBodyEvE7s_fixed, float 0.000000e+00, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  ret %class.btRigidBody* @_ZZN17btTypedConstraint12getFixedBodyEvE7s_fixed
}

; Function Attrs: noinline optnone
define hidden %class.btTypedConstraint* @_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_(%class.btTypedConstraint* returned %this, i32 %type, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbA, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbB) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %type.addr = alloca i32, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store i32 %type, i32* %type.addr, align 4
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i8*
  %1 = getelementptr inbounds i8, i8* %0, i32 4
  %2 = bitcast i8* %1 to %struct.btTypedObject*
  %3 = load i32, i32* %type.addr, align 4
  %call = call %struct.btTypedObject* @_ZN13btTypedObjectC2Ei(%struct.btTypedObject* %2, i32 %3)
  %4 = bitcast %class.btTypedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btTypedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4
  %m_userConstraintType = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 2
  store i32 -1, i32* %m_userConstraintType, align 4
  %5 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 3
  %m_userConstraintPtr = bitcast %union.anon.0* %5 to i8**
  store i8* inttoptr (i32 -1 to i8*), i8** %m_userConstraintPtr, align 4
  %m_breakingImpulseThreshold = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 4
  store float 0x47EFFFFFE0000000, float* %m_breakingImpulseThreshold, align 4
  %m_isEnabled = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 5
  store i8 1, i8* %m_isEnabled, align 4
  %m_needsFeedback = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 6
  store i8 0, i8* %m_needsFeedback, align 1
  %m_overrideNumSolverIterations = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 7
  store i32 -1, i32* %m_overrideNumSolverIterations, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %6 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  store %class.btRigidBody* %6, %class.btRigidBody** %m_rbA, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %7 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4
  store %class.btRigidBody* %7, %class.btRigidBody** %m_rbB, align 4
  %m_appliedImpulse = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 10
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %m_dbgDrawSize = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 11
  store float 0x3FD3333340000000, float* %m_dbgDrawSize, align 4
  %m_jointFeedback = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 12
  store %struct.btJointFeedback* null, %struct.btJointFeedback** %m_jointFeedback, align 4
  ret %class.btTypedConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZN17btTypedConstraint14getMotorFactorEfffff(%class.btTypedConstraint* %this, float %pos, float %lowLim, float %uppLim, float %vel, float %timeFact) #1 {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %pos.addr = alloca float, align 4
  %lowLim.addr = alloca float, align 4
  %uppLim.addr = alloca float, align 4
  %vel.addr = alloca float, align 4
  %timeFact.addr = alloca float, align 4
  %lim_fact = alloca float, align 4
  %delta_max = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store float %pos, float* %pos.addr, align 4
  store float %lowLim, float* %lowLim.addr, align 4
  store float %uppLim, float* %uppLim.addr, align 4
  store float %vel, float* %vel.addr, align 4
  store float %timeFact, float* %timeFact.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load float, float* %lowLim.addr, align 4
  %1 = load float, float* %uppLim.addr, align 4
  %cmp = fcmp ogt float %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  store float 1.000000e+00, float* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %2 = load float, float* %lowLim.addr, align 4
  %3 = load float, float* %uppLim.addr, align 4
  %cmp2 = fcmp oeq float %2, %3
  br i1 %cmp2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.else
  store float 0.000000e+00, float* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.else
  br label %if.end4

if.end4:                                          ; preds = %if.end
  store float 1.000000e+00, float* %lim_fact, align 4
  %4 = load float, float* %vel.addr, align 4
  %5 = load float, float* %timeFact.addr, align 4
  %div = fdiv float %4, %5
  store float %div, float* %delta_max, align 4
  %6 = load float, float* %delta_max, align 4
  %cmp5 = fcmp olt float %6, 0.000000e+00
  br i1 %cmp5, label %if.then6, label %if.else18

if.then6:                                         ; preds = %if.end4
  %7 = load float, float* %pos.addr, align 4
  %8 = load float, float* %lowLim.addr, align 4
  %cmp7 = fcmp oge float %7, %8
  br i1 %cmp7, label %land.lhs.true, label %if.else12

land.lhs.true:                                    ; preds = %if.then6
  %9 = load float, float* %pos.addr, align 4
  %10 = load float, float* %lowLim.addr, align 4
  %11 = load float, float* %delta_max, align 4
  %sub = fsub float %10, %11
  %cmp8 = fcmp olt float %9, %sub
  br i1 %cmp8, label %if.then9, label %if.else12

if.then9:                                         ; preds = %land.lhs.true
  %12 = load float, float* %lowLim.addr, align 4
  %13 = load float, float* %pos.addr, align 4
  %sub10 = fsub float %12, %13
  %14 = load float, float* %delta_max, align 4
  %div11 = fdiv float %sub10, %14
  store float %div11, float* %lim_fact, align 4
  br label %if.end17

if.else12:                                        ; preds = %land.lhs.true, %if.then6
  %15 = load float, float* %pos.addr, align 4
  %16 = load float, float* %lowLim.addr, align 4
  %cmp13 = fcmp olt float %15, %16
  br i1 %cmp13, label %if.then14, label %if.else15

if.then14:                                        ; preds = %if.else12
  store float 0.000000e+00, float* %lim_fact, align 4
  br label %if.end16

if.else15:                                        ; preds = %if.else12
  store float 1.000000e+00, float* %lim_fact, align 4
  br label %if.end16

if.end16:                                         ; preds = %if.else15, %if.then14
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.then9
  br label %if.end36

if.else18:                                        ; preds = %if.end4
  %17 = load float, float* %delta_max, align 4
  %cmp19 = fcmp ogt float %17, 0.000000e+00
  br i1 %cmp19, label %if.then20, label %if.else34

if.then20:                                        ; preds = %if.else18
  %18 = load float, float* %pos.addr, align 4
  %19 = load float, float* %uppLim.addr, align 4
  %cmp21 = fcmp ole float %18, %19
  br i1 %cmp21, label %land.lhs.true22, label %if.else28

land.lhs.true22:                                  ; preds = %if.then20
  %20 = load float, float* %pos.addr, align 4
  %21 = load float, float* %uppLim.addr, align 4
  %22 = load float, float* %delta_max, align 4
  %sub23 = fsub float %21, %22
  %cmp24 = fcmp ogt float %20, %sub23
  br i1 %cmp24, label %if.then25, label %if.else28

if.then25:                                        ; preds = %land.lhs.true22
  %23 = load float, float* %uppLim.addr, align 4
  %24 = load float, float* %pos.addr, align 4
  %sub26 = fsub float %23, %24
  %25 = load float, float* %delta_max, align 4
  %div27 = fdiv float %sub26, %25
  store float %div27, float* %lim_fact, align 4
  br label %if.end33

if.else28:                                        ; preds = %land.lhs.true22, %if.then20
  %26 = load float, float* %pos.addr, align 4
  %27 = load float, float* %uppLim.addr, align 4
  %cmp29 = fcmp ogt float %26, %27
  br i1 %cmp29, label %if.then30, label %if.else31

if.then30:                                        ; preds = %if.else28
  store float 0.000000e+00, float* %lim_fact, align 4
  br label %if.end32

if.else31:                                        ; preds = %if.else28
  store float 1.000000e+00, float* %lim_fact, align 4
  br label %if.end32

if.end32:                                         ; preds = %if.else31, %if.then30
  br label %if.end33

if.end33:                                         ; preds = %if.end32, %if.then25
  br label %if.end35

if.else34:                                        ; preds = %if.else18
  store float 0.000000e+00, float* %lim_fact, align 4
  br label %if.end35

if.end35:                                         ; preds = %if.else34, %if.end33
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %if.end17
  %28 = load float, float* %lim_fact, align 4
  store float %28, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.end36, %if.then3, %if.then
  %29 = load float, float* %retval, align 4
  ret float %29
}

; Function Attrs: noinline optnone
define hidden i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %tcd = alloca %struct.btTypedConstraintFloatData*, align 4
  %name = alloca i8*, align 4
  %i = alloca i32, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btTypedConstraintFloatData*
  store %struct.btTypedConstraintFloatData* %1, %struct.btTypedConstraintFloatData** %tcd, align 4
  %2 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %4 = bitcast %class.btRigidBody* %3 to i8*
  %5 = bitcast %class.btSerializer* %2 to i8* (%class.btSerializer*, i8*)***
  %vtable = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %5, align 4
  %vfn = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable, i64 7
  %6 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn, align 4
  %call = call i8* %6(%class.btSerializer* %2, i8* %4)
  %7 = bitcast i8* %call to %struct.btRigidBodyFloatData*
  %8 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_rbA2 = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %8, i32 0, i32 0
  store %struct.btRigidBodyFloatData* %7, %struct.btRigidBodyFloatData** %m_rbA2, align 4
  %9 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %10 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %11 = bitcast %class.btRigidBody* %10 to i8*
  %12 = bitcast %class.btSerializer* %9 to i8* (%class.btSerializer*, i8*)***
  %vtable3 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %12, align 4
  %vfn4 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable3, i64 7
  %13 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn4, align 4
  %call5 = call i8* %13(%class.btSerializer* %9, i8* %11)
  %14 = bitcast i8* %call5 to %struct.btRigidBodyFloatData*
  %15 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_rbB6 = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %15, i32 0, i32 1
  store %struct.btRigidBodyFloatData* %14, %struct.btRigidBodyFloatData** %m_rbB6, align 4
  %16 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %17 = bitcast %class.btTypedConstraint* %this1 to i8*
  %18 = bitcast %class.btSerializer* %16 to i8* (%class.btSerializer*, i8*)***
  %vtable7 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %18, align 4
  %vfn8 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable7, i64 10
  %19 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn8, align 4
  %call9 = call i8* %19(%class.btSerializer* %16, i8* %17)
  store i8* %call9, i8** %name, align 4
  %20 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %21 = load i8*, i8** %name, align 4
  %22 = bitcast %class.btSerializer* %20 to i8* (%class.btSerializer*, i8*)***
  %vtable10 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %22, align 4
  %vfn11 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable10, i64 7
  %23 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn11, align 4
  %call12 = call i8* %23(%class.btSerializer* %20, i8* %21)
  %24 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_name = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %24, i32 0, i32 2
  store i8* %call12, i8** %m_name, align 4
  %25 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_name13 = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %25, i32 0, i32 2
  %26 = load i8*, i8** %m_name13, align 4
  %tobool = icmp ne i8* %26, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %27 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %28 = load i8*, i8** %name, align 4
  %29 = bitcast %class.btSerializer* %27 to void (%class.btSerializer*, i8*)***
  %vtable14 = load void (%class.btSerializer*, i8*)**, void (%class.btSerializer*, i8*)*** %29, align 4
  %vfn15 = getelementptr inbounds void (%class.btSerializer*, i8*)*, void (%class.btSerializer*, i8*)** %vtable14, i64 12
  %30 = load void (%class.btSerializer*, i8*)*, void (%class.btSerializer*, i8*)** %vfn15, align 4
  call void %30(%class.btSerializer* %27, i8* %28)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %31 = bitcast %class.btTypedConstraint* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %31, i32 4
  %32 = bitcast i8* %add.ptr to %struct.btTypedObject*
  %m_objectType = getelementptr inbounds %struct.btTypedObject, %struct.btTypedObject* %32, i32 0, i32 0
  %33 = load i32, i32* %m_objectType, align 4
  %34 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_objectType16 = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %34, i32 0, i32 3
  store i32 %33, i32* %m_objectType16, align 4
  %m_needsFeedback = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 6
  %35 = load i8, i8* %m_needsFeedback, align 1
  %tobool17 = trunc i8 %35 to i1
  %conv = zext i1 %tobool17 to i32
  %36 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_needsFeedback18 = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %36, i32 0, i32 6
  store i32 %conv, i32* %m_needsFeedback18, align 4
  %m_overrideNumSolverIterations = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 7
  %37 = load i32, i32* %m_overrideNumSolverIterations, align 4
  %38 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_overrideNumSolverIterations19 = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %38, i32 0, i32 10
  store i32 %37, i32* %m_overrideNumSolverIterations19, align 4
  %m_breakingImpulseThreshold = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 4
  %39 = load float, float* %m_breakingImpulseThreshold, align 4
  %40 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_breakingImpulseThreshold20 = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %40, i32 0, i32 11
  store float %39, float* %m_breakingImpulseThreshold20, align 4
  %m_isEnabled = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 5
  %41 = load i8, i8* %m_isEnabled, align 4
  %tobool21 = trunc i8 %41 to i1
  %42 = zext i1 %tobool21 to i64
  %cond = select i1 %tobool21, i32 1, i32 0
  %43 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_isEnabled22 = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %43, i32 0, i32 12
  store i32 %cond, i32* %m_isEnabled22, align 4
  %44 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 3
  %m_userConstraintId = bitcast %union.anon.0* %44 to i32*
  %45 = load i32, i32* %m_userConstraintId, align 4
  %46 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_userConstraintId23 = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %46, i32 0, i32 5
  store i32 %45, i32* %m_userConstraintId23, align 4
  %m_userConstraintType = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 2
  %47 = load i32, i32* %m_userConstraintType, align 4
  %48 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_userConstraintType24 = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %48, i32 0, i32 4
  store i32 %47, i32* %m_userConstraintType24, align 4
  %m_appliedImpulse = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 10
  %49 = load float, float* %m_appliedImpulse, align 4
  %50 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_appliedImpulse25 = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %50, i32 0, i32 7
  store float %49, float* %m_appliedImpulse25, align 4
  %m_dbgDrawSize = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 11
  %51 = load float, float* %m_dbgDrawSize, align 4
  %52 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_dbgDrawSize26 = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %52, i32 0, i32 8
  store float %51, float* %m_dbgDrawSize26, align 4
  %53 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_disableCollisionsBetweenLinkedBodies = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %53, i32 0, i32 9
  store i32 0, i32* %m_disableCollisionsBetweenLinkedBodies, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %54 = load i32, i32* %i, align 4
  %m_rbA27 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %55 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA27, align 4
  %call28 = call i32 @_ZNK11btRigidBody20getNumConstraintRefsEv(%class.btRigidBody* %55)
  %cmp = icmp slt i32 %54, %call28
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_rbA29 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %56 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA29, align 4
  %57 = load i32, i32* %i, align 4
  %call30 = call %class.btTypedConstraint* @_ZN11btRigidBody16getConstraintRefEi(%class.btRigidBody* %56, i32 %57)
  %cmp31 = icmp eq %class.btTypedConstraint* %call30, %this1
  br i1 %cmp31, label %if.then32, label %if.end34

if.then32:                                        ; preds = %for.body
  %58 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_disableCollisionsBetweenLinkedBodies33 = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %58, i32 0, i32 9
  store i32 1, i32* %m_disableCollisionsBetweenLinkedBodies33, align 4
  br label %if.end34

if.end34:                                         ; preds = %if.then32, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end34
  %59 = load i32, i32* %i, align 4
  %inc = add nsw i32 %59, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond35

for.cond35:                                       ; preds = %for.inc46, %for.end
  %60 = load i32, i32* %i, align 4
  %m_rbB36 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %61 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB36, align 4
  %call37 = call i32 @_ZNK11btRigidBody20getNumConstraintRefsEv(%class.btRigidBody* %61)
  %cmp38 = icmp slt i32 %60, %call37
  br i1 %cmp38, label %for.body39, label %for.end48

for.body39:                                       ; preds = %for.cond35
  %m_rbB40 = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %62 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB40, align 4
  %63 = load i32, i32* %i, align 4
  %call41 = call %class.btTypedConstraint* @_ZN11btRigidBody16getConstraintRefEi(%class.btRigidBody* %62, i32 %63)
  %cmp42 = icmp eq %class.btTypedConstraint* %call41, %this1
  br i1 %cmp42, label %if.then43, label %if.end45

if.then43:                                        ; preds = %for.body39
  %64 = load %struct.btTypedConstraintFloatData*, %struct.btTypedConstraintFloatData** %tcd, align 4
  %m_disableCollisionsBetweenLinkedBodies44 = getelementptr inbounds %struct.btTypedConstraintFloatData, %struct.btTypedConstraintFloatData* %64, i32 0, i32 9
  store i32 1, i32* %m_disableCollisionsBetweenLinkedBodies44, align 4
  br label %if.end45

if.end45:                                         ; preds = %if.then43, %for.body39
  br label %for.inc46

for.inc46:                                        ; preds = %if.end45
  %65 = load i32, i32* %i, align 4
  %inc47 = add nsw i32 %65, 1
  store i32 %inc47, i32* %i, align 4
  br label %for.cond35

for.end48:                                        ; preds = %for.cond35
  ret i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK11btRigidBody20getNumConstraintRefsEv(%class.btRigidBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.1* %m_constraintRefs)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTypedConstraint* @_ZN11btRigidBody16getConstraintRefEi(%class.btRigidBody* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  %index.addr = alloca i32, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.1* %m_constraintRefs, i32 %0)
  %1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %call, align 4
  ret %class.btTypedConstraint* %1
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

declare %class.btRigidBody* @_ZN11btRigidBodyC1EfP13btMotionStateP16btCollisionShapeRK9btVector3(%class.btRigidBody* returned, float, %class.btMotionState*, %class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #4

; Function Attrs: noinline
define internal void @__cxx_global_array_dtor(i8* %0) #0 {
entry:
  %.addr = alloca i8*, align 4
  store i8* %0, i8** %.addr, align 4
  %call = call %class.btRigidBody* @_ZN11btRigidBodyD2Ev(%class.btRigidBody* @_ZZN17btTypedConstraint12getFixedBodyEvE7s_fixed) #3
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btRigidBody* @_ZN11btRigidBodyD2Ev(%class.btRigidBody* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV11btRigidBody, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_constraintRefs = getelementptr inbounds %class.btRigidBody, %class.btRigidBody* %this1, i32 0, i32 21
  %call = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.1* %m_constraintRefs) #3
  %1 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %call2 = call %class.btCollisionObject* @_ZN17btCollisionObjectD2Ev(%class.btCollisionObject* %1) #3
  ret %class.btRigidBody* %this1
}

; Function Attrs: nounwind
declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) #3

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #3

declare void @_ZN11btRigidBody12setMassPropsEfRK9btVector3(%class.btRigidBody*, float, %class.btVector3* nonnull align 4 dereferenceable(16)) #4

; Function Attrs: noinline optnone
define hidden void @_ZN14btAngularLimit3setEfffff(%class.btAngularLimit* %this, float %low, float %high, float %_softness, float %_biasFactor, float %_relaxationFactor) #2 {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  %low.addr = alloca float, align 4
  %high.addr = alloca float, align 4
  %_softness.addr = alloca float, align 4
  %_biasFactor.addr = alloca float, align 4
  %_relaxationFactor.addr = alloca float, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4
  store float %low, float* %low.addr, align 4
  store float %high, float* %high.addr, align 4
  store float %_softness, float* %_softness.addr, align 4
  store float %_biasFactor, float* %_biasFactor.addr, align 4
  store float %_relaxationFactor, float* %_relaxationFactor.addr, align 4
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %0 = load float, float* %high.addr, align 4
  %1 = load float, float* %low.addr, align 4
  %sub = fsub float %0, %1
  %div = fdiv float %sub, 2.000000e+00
  %m_halfRange = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 1
  store float %div, float* %m_halfRange, align 4
  %2 = load float, float* %low.addr, align 4
  %m_halfRange2 = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 1
  %3 = load float, float* %m_halfRange2, align 4
  %add = fadd float %2, %3
  %call = call float @_Z16btNormalizeAnglef(float %add)
  %m_center = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 0
  store float %call, float* %m_center, align 4
  %4 = load float, float* %_softness.addr, align 4
  %m_softness = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 2
  store float %4, float* %m_softness, align 4
  %5 = load float, float* %_biasFactor.addr, align 4
  %m_biasFactor = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 3
  store float %5, float* %m_biasFactor, align 4
  %6 = load float, float* %_relaxationFactor.addr, align 4
  %m_relaxationFactor = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 4
  store float %6, float* %m_relaxationFactor, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_Z16btNormalizeAnglef(float %angleInRadians) #2 comdat {
entry:
  %retval = alloca float, align 4
  %angleInRadians.addr = alloca float, align 4
  store float %angleInRadians, float* %angleInRadians.addr, align 4
  %0 = load float, float* %angleInRadians.addr, align 4
  %call = call float @_Z6btFmodff(float %0, float 0x401921FB60000000)
  store float %call, float* %angleInRadians.addr, align 4
  %1 = load float, float* %angleInRadians.addr, align 4
  %cmp = fcmp olt float %1, 0xC00921FB60000000
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load float, float* %angleInRadians.addr, align 4
  %add = fadd float %2, 0x401921FB60000000
  store float %add, float* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %3 = load float, float* %angleInRadians.addr, align 4
  %cmp1 = fcmp ogt float %3, 0x400921FB60000000
  br i1 %cmp1, label %if.then2, label %if.else3

if.then2:                                         ; preds = %if.else
  %4 = load float, float* %angleInRadians.addr, align 4
  %sub = fsub float %4, 0x401921FB60000000
  store float %sub, float* %retval, align 4
  br label %return

if.else3:                                         ; preds = %if.else
  %5 = load float, float* %angleInRadians.addr, align 4
  store float %5, float* %retval, align 4
  br label %return

return:                                           ; preds = %if.else3, %if.then2, %if.then
  %6 = load float, float* %retval, align 4
  ret float %6
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btAngularLimit4testEf(%class.btAngularLimit* %this, float %angle) #2 {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  %angle.addr = alloca float, align 4
  %deviation = alloca float, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4
  store float %angle, float* %angle.addr, align 4
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_correction = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 5
  store float 0.000000e+00, float* %m_correction, align 4
  %m_sign = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 6
  store float 0.000000e+00, float* %m_sign, align 4
  %m_solveLimit = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 7
  store i8 0, i8* %m_solveLimit, align 4
  %m_halfRange = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 1
  %0 = load float, float* %m_halfRange, align 4
  %cmp = fcmp oge float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end19

if.then:                                          ; preds = %entry
  %1 = load float, float* %angle.addr, align 4
  %m_center = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 0
  %2 = load float, float* %m_center, align 4
  %sub = fsub float %1, %2
  %call = call float @_Z16btNormalizeAnglef(float %sub)
  store float %call, float* %deviation, align 4
  %3 = load float, float* %deviation, align 4
  %m_halfRange2 = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 1
  %4 = load float, float* %m_halfRange2, align 4
  %fneg = fneg float %4
  %cmp3 = fcmp olt float %3, %fneg
  br i1 %cmp3, label %if.then4, label %if.else

if.then4:                                         ; preds = %if.then
  %m_solveLimit5 = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 7
  store i8 1, i8* %m_solveLimit5, align 4
  %5 = load float, float* %deviation, align 4
  %m_halfRange6 = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 1
  %6 = load float, float* %m_halfRange6, align 4
  %add = fadd float %5, %6
  %fneg7 = fneg float %add
  %m_correction8 = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 5
  store float %fneg7, float* %m_correction8, align 4
  %m_sign9 = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 6
  store float 1.000000e+00, float* %m_sign9, align 4
  br label %if.end18

if.else:                                          ; preds = %if.then
  %7 = load float, float* %deviation, align 4
  %m_halfRange10 = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 1
  %8 = load float, float* %m_halfRange10, align 4
  %cmp11 = fcmp ogt float %7, %8
  br i1 %cmp11, label %if.then12, label %if.end

if.then12:                                        ; preds = %if.else
  %m_solveLimit13 = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 7
  store i8 1, i8* %m_solveLimit13, align 4
  %m_halfRange14 = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 1
  %9 = load float, float* %m_halfRange14, align 4
  %10 = load float, float* %deviation, align 4
  %sub15 = fsub float %9, %10
  %m_correction16 = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 5
  store float %sub15, float* %m_correction16, align 4
  %m_sign17 = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 6
  store float -1.000000e+00, float* %m_sign17, align 4
  br label %if.end

if.end:                                           ; preds = %if.then12, %if.else
  br label %if.end18

if.end18:                                         ; preds = %if.end, %if.then4
  br label %if.end19

if.end19:                                         ; preds = %if.end18, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZNK14btAngularLimit8getErrorEv(%class.btAngularLimit* %this) #1 {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_correction = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 5
  %0 = load float, float* %m_correction, align 4
  %m_sign = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 6
  %1 = load float, float* %m_sign, align 4
  %mul = fmul float %0, %1
  ret float %mul
}

; Function Attrs: noinline optnone
define hidden void @_ZNK14btAngularLimit3fitERf(%class.btAngularLimit* %this, float* nonnull align 4 dereferenceable(4) %angle) #2 {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  %angle.addr = alloca float*, align 4
  %relativeAngle = alloca float, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4
  store float* %angle, float** %angle.addr, align 4
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_halfRange = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 1
  %0 = load float, float* %m_halfRange, align 4
  %cmp = fcmp ogt float %0, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end10

if.then:                                          ; preds = %entry
  %1 = load float*, float** %angle.addr, align 4
  %2 = load float, float* %1, align 4
  %m_center = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 0
  %3 = load float, float* %m_center, align 4
  %sub = fsub float %2, %3
  %call = call float @_Z16btNormalizeAnglef(float %sub)
  store float %call, float* %relativeAngle, align 4
  %4 = load float, float* %relativeAngle, align 4
  %m_halfRange2 = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 1
  %5 = load float, float* %m_halfRange2, align 4
  %call3 = call zeroext i1 @_Z7btEqualff(float %4, float %5)
  br i1 %call3, label %if.end9, label %if.then4

if.then4:                                         ; preds = %if.then
  %6 = load float, float* %relativeAngle, align 4
  %cmp5 = fcmp ogt float %6, 0.000000e+00
  br i1 %cmp5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.then4
  %call7 = call float @_ZNK14btAngularLimit7getHighEv(%class.btAngularLimit* %this1)
  %7 = load float*, float** %angle.addr, align 4
  store float %call7, float* %7, align 4
  br label %if.end

if.else:                                          ; preds = %if.then4
  %call8 = call float @_ZNK14btAngularLimit6getLowEv(%class.btAngularLimit* %this1)
  %8 = load float*, float** %angle.addr, align 4
  store float %call8, float* %8, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then6
  br label %if.end9

if.end9:                                          ; preds = %if.end, %if.then
  br label %if.end10

if.end10:                                         ; preds = %if.end9, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_Z7btEqualff(float %a, float %eps) #1 comdat {
entry:
  %a.addr = alloca float, align 4
  %eps.addr = alloca float, align 4
  store float %a, float* %a.addr, align 4
  store float %eps, float* %eps.addr, align 4
  %0 = load float, float* %a.addr, align 4
  %1 = load float, float* %eps.addr, align 4
  %cmp = fcmp ole float %0, %1
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %2 = load float, float* %a.addr, align 4
  %3 = load float, float* %eps.addr, align 4
  %fneg = fneg float %3
  %cmp1 = fcmp olt float %2, %fneg
  %lnot = xor i1 %cmp1, true
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %4 = phi i1 [ false, %entry ], [ %lnot, %land.rhs ]
  ret i1 %4
}

; Function Attrs: noinline optnone
define hidden float @_ZNK14btAngularLimit7getHighEv(%class.btAngularLimit* %this) #2 {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_center = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 0
  %0 = load float, float* %m_center, align 4
  %m_halfRange = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 1
  %1 = load float, float* %m_halfRange, align 4
  %add = fadd float %0, %1
  %call = call float @_Z16btNormalizeAnglef(float %add)
  ret float %call
}

; Function Attrs: noinline optnone
define hidden float @_ZNK14btAngularLimit6getLowEv(%class.btAngularLimit* %this) #2 {
entry:
  %this.addr = alloca %class.btAngularLimit*, align 4
  store %class.btAngularLimit* %this, %class.btAngularLimit** %this.addr, align 4
  %this1 = load %class.btAngularLimit*, %class.btAngularLimit** %this.addr, align 4
  %m_center = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 0
  %0 = load float, float* %m_center, align 4
  %m_halfRange = getelementptr inbounds %class.btAngularLimit, %class.btAngularLimit* %this1, i32 0, i32 1
  %1 = load float, float* %m_halfRange, align 4
  %sub = fsub float %0, %1
  %call = call float @_Z16btNormalizeAnglef(float %sub)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btTypedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btTypedConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraintD0Ev(%class.btTypedConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  call void @llvm.trap() #7
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint13buildJacobianEv(%class.btTypedConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif(%class.btTypedConstraint* %this, %class.btAlignedObjectArray.5* nonnull align 1 %ca, i32 %solverBodyA, i32 %solverBodyB, float %timeStep) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %ca.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %solverBodyA.addr = alloca i32, align 4
  %solverBodyB.addr = alloca i32, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store %class.btAlignedObjectArray.5* %ca, %class.btAlignedObjectArray.5** %ca.addr, align 4
  store i32 %solverBodyA, i32* %solverBodyA.addr, align 4
  store i32 %solverBodyB, i32* %solverBodyB.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %ca.addr, align 4
  ret void
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f(%class.btTypedConstraint* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, float %2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %.addr = alloca %struct.btSolverBody*, align 4
  %.addr1 = alloca %struct.btSolverBody*, align 4
  %.addr2 = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store %struct.btSolverBody* %0, %struct.btSolverBody** %.addr, align 4
  store %struct.btSolverBody* %1, %struct.btSolverBody** %.addr1, align 4
  store float %2, float* %.addr2, align 4
  %this3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btTypedConstraint28calculateSerializeBufferSizeEv(%class.btTypedConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  ret i32 52
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.1* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %0, i32 %1
  ret %class.btTypedConstraint** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray.1* %this1)
  ret %class.btAlignedObjectArray.1* %this1
}

; Function Attrs: nounwind
declare %class.btCollisionObject* @_ZN17btCollisionObjectD2Ev(%class.btCollisionObject* returned) unnamed_addr #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray.1* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.1* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.1* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.1* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %tobool = icmp ne %class.btTypedConstraint** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %2 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.2* %m_allocator, %class.btTypedConstraint** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.2* %this, %class.btTypedConstraint** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  %ptr.addr = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  store %class.btTypedConstraint** %ptr, %class.btTypedConstraint*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %ptr.addr, align 4
  %1 = bitcast %class.btTypedConstraint** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFmodff(float %x, float %y) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  %y.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = load float, float* %y.addr, align 4
  %fmod = frem float %0, %1
  ret float %fmod
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #6

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btTypedConstraint.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { cold noreturn nounwind }
attributes #7 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1048575}
