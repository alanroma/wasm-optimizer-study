; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btGjkConvexCast.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/NarrowPhaseCollision/btGjkConvexCast.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btGjkConvexCast = type { %class.btConvexCast, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape* }
%class.btConvexCast = type { i32 (...)** }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btVoronoiSimplexSolver = type <{ i32, [5 x %class.btVector3], [5 x %class.btVector3], [5 x %class.btVector3], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, i8, [3 x i8], %struct.btSubSimplexClosestResult, i8, [3 x i8] }>
%class.btVector3 = type { [4 x float] }
%struct.btSubSimplexClosestResult = type <{ %class.btVector3, %struct.btUsageBitfield, [2 x i8], [4 x float], i8, [3 x i8] }>
%struct.btUsageBitfield = type { i8, i8 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%"struct.btConvexCast::CastResult" = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, float, %class.btIDebugDraw*, float }
%class.btIDebugDraw = type { i32 (...)** }
%struct.btPointCollector = type <{ %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btVector3, %class.btVector3, float, i8, [3 x i8] }>
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%class.btGjkPairDetector = type { %struct.btDiscreteCollisionDetectorInterface, %class.btVector3, %class.btConvexPenetrationDepthSolver*, %class.btVoronoiSimplexSolver*, %class.btConvexShape*, %class.btConvexShape*, i32, i32, float, float, i8, float, i32, i32, i32, i32, i32 }
%struct.btDiscreteCollisionDetectorInterface = type { i32 (...)** }
%class.btConvexPenetrationDepthSolver = type opaque
%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput" = type { %class.btTransform, %class.btTransform, float }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN12btConvexCastC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN16btPointCollectorC2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZN9btVector315setInterpolate3ERKS_S1_f = comdat any

$_ZN17btGjkPairDetectorD2Ev = comdat any

$_ZN16btPointCollectorD2Ev = comdat any

$_ZN15btGjkConvexCastD2Ev = comdat any

$_ZN15btGjkConvexCastD0Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev = comdat any

$_ZN16btPointCollectorD0Ev = comdat any

$_ZN16btPointCollector20setShapeIdentifiersAEii = comdat any

$_ZN16btPointCollector20setShapeIdentifiersBEii = comdat any

$_ZN16btPointCollector15addContactPointERK9btVector3S2_f = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD2Ev = comdat any

$_ZTV16btPointCollector = comdat any

$_ZTS16btPointCollector = comdat any

$_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTI16btPointCollector = comdat any

$_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV15btGjkConvexCast = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btGjkConvexCast to i8*), i8* bitcast (%class.btGjkConvexCast* (%class.btGjkConvexCast*)* @_ZN15btGjkConvexCastD2Ev to i8*), i8* bitcast (void (%class.btGjkConvexCast*)* @_ZN15btGjkConvexCastD0Ev to i8*), i8* bitcast (i1 (%class.btGjkConvexCast*, %class.btTransform*, %class.btTransform*, %class.btTransform*, %class.btTransform*, %"struct.btConvexCast::CastResult"*)* @_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS15btGjkConvexCast = hidden constant [18 x i8] c"15btGjkConvexCast\00", align 1
@_ZTI12btConvexCast = external constant i8*
@_ZTI15btGjkConvexCast = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btGjkConvexCast, i32 0, i32 0), i8* bitcast (i8** @_ZTI12btConvexCast to i8*) }, align 4
@_ZTV12btConvexCast = external unnamed_addr constant { [5 x i8*] }, align 4
@_ZTV16btPointCollector = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btPointCollector to i8*), i8* bitcast (%struct.btPointCollector* (%struct.btPointCollector*)* @_ZN16btPointCollectorD2Ev to i8*), i8* bitcast (void (%struct.btPointCollector*)* @_ZN16btPointCollectorD0Ev to i8*), i8* bitcast (void (%struct.btPointCollector*, i32, i32)* @_ZN16btPointCollector20setShapeIdentifiersAEii to i8*), i8* bitcast (void (%struct.btPointCollector*, i32, i32)* @_ZN16btPointCollector20setShapeIdentifiersBEii to i8*), i8* bitcast (void (%struct.btPointCollector*, %class.btVector3*, %class.btVector3*, float)* @_ZN16btPointCollector15addContactPointERK9btVector3S2_f to i8*)] }, comdat, align 4
@_ZTS16btPointCollector = linkonce_odr hidden constant [19 x i8] c"16btPointCollector\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant [48 x i8] c"N36btDiscreteCollisionDetectorInterface6ResultE\00", comdat, align 1
@_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([48 x i8], [48 x i8]* @_ZTSN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, i32 0) }, comdat, align 4
@_ZTI16btPointCollector = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btPointCollector, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*) }, comdat, align 4
@_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*), i8* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to i8*), i8* bitcast (void (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btGjkConvexCast.cpp, i8* null }]

@_ZN15btGjkConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver = hidden unnamed_addr alias %class.btGjkConvexCast* (%class.btGjkConvexCast*, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*), %class.btGjkConvexCast* (%class.btGjkConvexCast*, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*)* @_ZN15btGjkConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btGjkConvexCast* @_ZN15btGjkConvexCastC2EPK13btConvexShapeS2_P22btVoronoiSimplexSolver(%class.btGjkConvexCast* returned %this, %class.btConvexShape* %convexA, %class.btConvexShape* %convexB, %class.btVoronoiSimplexSolver* %simplexSolver) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btGjkConvexCast*, align 4
  %convexA.addr = alloca %class.btConvexShape*, align 4
  %convexB.addr = alloca %class.btConvexShape*, align 4
  %simplexSolver.addr = alloca %class.btVoronoiSimplexSolver*, align 4
  store %class.btGjkConvexCast* %this, %class.btGjkConvexCast** %this.addr, align 4
  store %class.btConvexShape* %convexA, %class.btConvexShape** %convexA.addr, align 4
  store %class.btConvexShape* %convexB, %class.btConvexShape** %convexB.addr, align 4
  store %class.btVoronoiSimplexSolver* %simplexSolver, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4
  %this1 = load %class.btGjkConvexCast*, %class.btGjkConvexCast** %this.addr, align 4
  %0 = bitcast %class.btGjkConvexCast* %this1 to %class.btConvexCast*
  %call = call %class.btConvexCast* @_ZN12btConvexCastC2Ev(%class.btConvexCast* %0) #8
  %1 = bitcast %class.btGjkConvexCast* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV15btGjkConvexCast, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_simplexSolver = getelementptr inbounds %class.btGjkConvexCast, %class.btGjkConvexCast* %this1, i32 0, i32 1
  %2 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %simplexSolver.addr, align 4
  store %class.btVoronoiSimplexSolver* %2, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4
  %m_convexA = getelementptr inbounds %class.btGjkConvexCast, %class.btGjkConvexCast* %this1, i32 0, i32 2
  %3 = load %class.btConvexShape*, %class.btConvexShape** %convexA.addr, align 4
  store %class.btConvexShape* %3, %class.btConvexShape** %m_convexA, align 4
  %m_convexB = getelementptr inbounds %class.btGjkConvexCast, %class.btGjkConvexCast* %this1, i32 0, i32 3
  %4 = load %class.btConvexShape*, %class.btConvexShape** %convexB.addr, align 4
  store %class.btConvexShape* %4, %class.btConvexShape** %m_convexB, align 4
  ret %class.btGjkConvexCast* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexCast* @_ZN12btConvexCastC2Ev(%class.btConvexCast* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexCast*, align 4
  store %class.btConvexCast* %this, %class.btConvexCast** %this.addr, align 4
  %this1 = load %class.btConvexCast*, %class.btConvexCast** %this.addr, align 4
  %0 = bitcast %class.btConvexCast* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV12btConvexCast, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btConvexCast* %this1
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN15btGjkConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE(%class.btGjkConvexCast* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %fromA, %class.btTransform* nonnull align 4 dereferenceable(64) %toA, %class.btTransform* nonnull align 4 dereferenceable(64) %fromB, %class.btTransform* nonnull align 4 dereferenceable(64) %toB, %"struct.btConvexCast::CastResult"* nonnull align 4 dereferenceable(176) %result) unnamed_addr #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btGjkConvexCast*, align 4
  %fromA.addr = alloca %class.btTransform*, align 4
  %toA.addr = alloca %class.btTransform*, align 4
  %fromB.addr = alloca %class.btTransform*, align 4
  %toB.addr = alloca %class.btTransform*, align 4
  %result.addr = alloca %"struct.btConvexCast::CastResult"*, align 4
  %linVelA = alloca %class.btVector3, align 4
  %linVelB = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %radius = alloca float, align 4
  %lambda = alloca float, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %maxIter = alloca i32, align 4
  %n = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %hasResult = alloca i8, align 1
  %c = alloca %class.btVector3, align 4
  %r = alloca %class.btVector3, align 4
  %lastLambda = alloca float, align 4
  %numIter = alloca i32, align 4
  %identityTrans = alloca %class.btTransform, align 4
  %pointCollector = alloca %struct.btPointCollector, align 4
  %gjk = alloca %class.btGjkPairDetector, align 4
  %input = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", align 4
  %dist = alloca float, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %dLambda = alloca float, align 4
  %projectedLinearVelocity = alloca float, align 4
  store %class.btGjkConvexCast* %this, %class.btGjkConvexCast** %this.addr, align 4
  store %class.btTransform* %fromA, %class.btTransform** %fromA.addr, align 4
  store %class.btTransform* %toA, %class.btTransform** %toA.addr, align 4
  store %class.btTransform* %fromB, %class.btTransform** %fromB.addr, align 4
  store %class.btTransform* %toB, %class.btTransform** %toB.addr, align 4
  store %"struct.btConvexCast::CastResult"* %result, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %this1 = load %class.btGjkConvexCast*, %class.btGjkConvexCast** %this.addr, align 4
  %m_simplexSolver = getelementptr inbounds %class.btGjkConvexCast, %class.btGjkConvexCast* %this1, i32 0, i32 1
  %0 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver, align 4
  call void @_ZN22btVoronoiSimplexSolver5resetEv(%class.btVoronoiSimplexSolver* %0)
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %linVelA)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %linVelB)
  %1 = load %class.btTransform*, %class.btTransform** %toA.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %1)
  %2 = load %class.btTransform*, %class.btTransform** %fromA.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %2)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call4)
  %3 = bitcast %class.btVector3* %linVelA to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %5 = load %class.btTransform*, %class.btTransform** %toB.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %5)
  %6 = load %class.btTransform*, %class.btTransform** %fromB.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %6)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %call7)
  %7 = bitcast %class.btVector3* %linVelB to i8*
  %8 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  store float 0x3F50624DE0000000, float* %radius, align 4
  store float 0.000000e+00, float* %lambda, align 4
  store float 1.000000e+00, float* %ref.tmp8, align 4
  store float 0.000000e+00, float* %ref.tmp9, align 4
  store float 0.000000e+00, float* %ref.tmp10, align 4
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %v, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  store i32 32, i32* %maxIter, align 4
  %call12 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %n)
  store float 0.000000e+00, float* %ref.tmp13, align 4
  store float 0.000000e+00, float* %ref.tmp14, align 4
  store float 0.000000e+00, float* %ref.tmp15, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %n, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  store i8 0, i8* %hasResult, align 1
  %call16 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %c)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %r, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelA, %class.btVector3* nonnull align 4 dereferenceable(16) %linVelB)
  %9 = load float, float* %lambda, align 4
  store float %9, float* %lastLambda, align 4
  store i32 0, i32* %numIter, align 4
  %call17 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %identityTrans)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %identityTrans)
  %call18 = call %struct.btPointCollector* @_ZN16btPointCollectorC2Ev(%struct.btPointCollector* %pointCollector)
  %m_convexA = getelementptr inbounds %class.btGjkConvexCast, %class.btGjkConvexCast* %this1, i32 0, i32 2
  %10 = load %class.btConvexShape*, %class.btConvexShape** %m_convexA, align 4
  %m_convexB = getelementptr inbounds %class.btGjkConvexCast, %class.btGjkConvexCast* %this1, i32 0, i32 3
  %11 = load %class.btConvexShape*, %class.btConvexShape** %m_convexB, align 4
  %m_simplexSolver19 = getelementptr inbounds %class.btGjkConvexCast, %class.btGjkConvexCast* %this1, i32 0, i32 1
  %12 = load %class.btVoronoiSimplexSolver*, %class.btVoronoiSimplexSolver** %m_simplexSolver19, align 4
  %call20 = call %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* %gjk, %class.btConvexShape* %10, %class.btConvexShape* %11, %class.btVoronoiSimplexSolver* %12, %class.btConvexPenetrationDepthSolver* null)
  %call21 = call %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* @_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev(%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input)
  %13 = load %class.btTransform*, %class.btTransform** %fromA.addr, align 4
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformA, %class.btTransform* nonnull align 4 dereferenceable(64) %13)
  %14 = load %class.btTransform*, %class.btTransform** %fromB.addr, align 4
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 1
  %call23 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformB, %class.btTransform* nonnull align 4 dereferenceable(64) %14)
  %15 = bitcast %struct.btPointCollector* %pointCollector to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  call void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector* %gjk, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %15, %class.btIDebugDraw* null, i1 zeroext false)
  %m_hasResult = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 4
  %16 = load i8, i8* %m_hasResult, align 4
  %tobool = trunc i8 %16 to i1
  %frombool = zext i1 %tobool to i8
  store i8 %frombool, i8* %hasResult, align 1
  %m_pointInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 2
  %17 = bitcast %class.btVector3* %c to i8*
  %18 = bitcast %class.btVector3* %m_pointInWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 16, i1 false)
  %19 = load i8, i8* %hasResult, align 1
  %tobool24 = trunc i8 %19 to i1
  br i1 %tobool24, label %if.then, label %if.end65

if.then:                                          ; preds = %entry
  %m_distance = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 3
  %20 = load float, float* %m_distance, align 4
  store float %20, float* %dist, align 4
  %m_normalOnBInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 1
  %21 = bitcast %class.btVector3* %n to i8*
  %22 = bitcast %class.btVector3* %m_normalOnBInWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  br label %while.cond

while.cond:                                       ; preds = %if.end57, %if.then
  %23 = load float, float* %dist, align 4
  %24 = load float, float* %radius, align 4
  %cmp = fcmp ogt float %23, %24
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %25 = load i32, i32* %numIter, align 4
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %numIter, align 4
  %26 = load i32, i32* %numIter, align 4
  %27 = load i32, i32* %maxIter, align 4
  %cmp25 = icmp sgt i32 %26, %27
  br i1 %cmp25, label %if.then26, label %if.end

if.then26:                                        ; preds = %while.body
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %while.body
  store float 0.000000e+00, float* %dLambda, align 4
  %call27 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %r, %class.btVector3* nonnull align 4 dereferenceable(16) %n)
  store float %call27, float* %projectedLinearVelocity, align 4
  %28 = load float, float* %dist, align 4
  %29 = load float, float* %projectedLinearVelocity, align 4
  %div = fdiv float %28, %29
  store float %div, float* %dLambda, align 4
  %30 = load float, float* %lambda, align 4
  %31 = load float, float* %dLambda, align 4
  %sub = fsub float %30, %31
  store float %sub, float* %lambda, align 4
  %32 = load float, float* %lambda, align 4
  %cmp28 = fcmp ogt float %32, 1.000000e+00
  br i1 %cmp28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.end
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end30:                                         ; preds = %if.end
  %33 = load float, float* %lambda, align 4
  %cmp31 = fcmp olt float %33, 0.000000e+00
  br i1 %cmp31, label %if.then32, label %if.end33

if.then32:                                        ; preds = %if.end30
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end33:                                         ; preds = %if.end30
  %34 = load float, float* %lambda, align 4
  %35 = load float, float* %lastLambda, align 4
  %cmp34 = fcmp ole float %34, %35
  br i1 %cmp34, label %if.then35, label %if.end36

if.then35:                                        ; preds = %if.end33
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end36:                                         ; preds = %if.end33
  %36 = load float, float* %lambda, align 4
  store float %36, float* %lastLambda, align 4
  %37 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %38 = load float, float* %lambda, align 4
  %39 = bitcast %"struct.btConvexCast::CastResult"* %37 to void (%"struct.btConvexCast::CastResult"*, float)***
  %vtable = load void (%"struct.btConvexCast::CastResult"*, float)**, void (%"struct.btConvexCast::CastResult"*, float)*** %39, align 4
  %vfn = getelementptr inbounds void (%"struct.btConvexCast::CastResult"*, float)*, void (%"struct.btConvexCast::CastResult"*, float)** %vtable, i64 0
  %40 = load void (%"struct.btConvexCast::CastResult"*, float)*, void (%"struct.btConvexCast::CastResult"*, float)** %vfn, align 4
  call void %40(%"struct.btConvexCast::CastResult"* %37, float %38)
  %m_transformA37 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 0
  %call38 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_transformA37)
  %41 = load %class.btTransform*, %class.btTransform** %fromA.addr, align 4
  %call39 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %41)
  %42 = load %class.btTransform*, %class.btTransform** %toA.addr, align 4
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %42)
  %43 = load float, float* %lambda, align 4
  call void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %call38, %class.btVector3* nonnull align 4 dereferenceable(16) %call39, %class.btVector3* nonnull align 4 dereferenceable(16) %call40, float %43)
  %m_transformB41 = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 1
  %call42 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %m_transformB41)
  %44 = load %class.btTransform*, %class.btTransform** %fromB.addr, align 4
  %call43 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %44)
  %45 = load %class.btTransform*, %class.btTransform** %toB.addr, align 4
  %call44 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %45)
  %46 = load float, float* %lambda, align 4
  call void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %call42, %class.btVector3* nonnull align 4 dereferenceable(16) %call43, %class.btVector3* nonnull align 4 dereferenceable(16) %call44, float %46)
  %47 = bitcast %struct.btPointCollector* %pointCollector to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  call void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector* %gjk, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %47, %class.btIDebugDraw* null, i1 zeroext false)
  %m_hasResult45 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 4
  %48 = load i8, i8* %m_hasResult45, align 4
  %tobool46 = trunc i8 %48 to i1
  br i1 %tobool46, label %if.then47, label %if.else

if.then47:                                        ; preds = %if.end36
  %m_distance48 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 3
  %49 = load float, float* %m_distance48, align 4
  %cmp49 = fcmp olt float %49, 0.000000e+00
  br i1 %cmp49, label %if.then50, label %if.end53

if.then50:                                        ; preds = %if.then47
  %50 = load float, float* %lastLambda, align 4
  %51 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_fraction = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %51, i32 0, i32 5
  store float %50, float* %m_fraction, align 4
  %m_normalOnBInWorld51 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 1
  %52 = bitcast %class.btVector3* %n to i8*
  %53 = bitcast %class.btVector3* %m_normalOnBInWorld51 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %52, i8* align 4 %53, i32 16, i1 false)
  %54 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_normal = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %54, i32 0, i32 3
  %55 = bitcast %class.btVector3* %m_normal to i8*
  %56 = bitcast %class.btVector3* %n to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %55, i8* align 4 %56, i32 16, i1 false)
  %m_pointInWorld52 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 2
  %57 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_hitPoint = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %57, i32 0, i32 4
  %58 = bitcast %class.btVector3* %m_hitPoint to i8*
  %59 = bitcast %class.btVector3* %m_pointInWorld52 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %58, i8* align 4 %59, i32 16, i1 false)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end53:                                         ; preds = %if.then47
  %m_pointInWorld54 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 2
  %60 = bitcast %class.btVector3* %c to i8*
  %61 = bitcast %class.btVector3* %m_pointInWorld54 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %60, i8* align 4 %61, i32 16, i1 false)
  %m_normalOnBInWorld55 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 1
  %62 = bitcast %class.btVector3* %n to i8*
  %63 = bitcast %class.btVector3* %m_normalOnBInWorld55 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %62, i8* align 4 %63, i32 16, i1 false)
  %m_distance56 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %pointCollector, i32 0, i32 3
  %64 = load float, float* %m_distance56, align 4
  store float %64, float* %dist, align 4
  br label %if.end57

if.else:                                          ; preds = %if.end36
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end57:                                         ; preds = %if.end53
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %call58 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %n, %class.btVector3* nonnull align 4 dereferenceable(16) %r)
  %65 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_allowedPenetration = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %65, i32 0, i32 7
  %66 = load float, float* %m_allowedPenetration, align 4
  %fneg = fneg float %66
  %cmp59 = fcmp oge float %call58, %fneg
  br i1 %cmp59, label %if.then60, label %if.end61

if.then60:                                        ; preds = %while.end
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end61:                                         ; preds = %while.end
  %67 = load float, float* %lambda, align 4
  %68 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_fraction62 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %68, i32 0, i32 5
  store float %67, float* %m_fraction62, align 4
  %69 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_normal63 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %69, i32 0, i32 3
  %70 = bitcast %class.btVector3* %m_normal63 to i8*
  %71 = bitcast %class.btVector3* %n to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %70, i8* align 4 %71, i32 16, i1 false)
  %72 = load %"struct.btConvexCast::CastResult"*, %"struct.btConvexCast::CastResult"** %result.addr, align 4
  %m_hitPoint64 = getelementptr inbounds %"struct.btConvexCast::CastResult", %"struct.btConvexCast::CastResult"* %72, i32 0, i32 4
  %73 = bitcast %class.btVector3* %m_hitPoint64 to i8*
  %74 = bitcast %class.btVector3* %c to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %73, i8* align 4 %74, i32 16, i1 false)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end65:                                         ; preds = %entry
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %if.end65, %if.end61, %if.then60, %if.else, %if.then50, %if.then35, %if.then32, %if.then29, %if.then26
  %call66 = call %class.btGjkPairDetector* @_ZN17btGjkPairDetectorD2Ev(%class.btGjkPairDetector* %gjk) #8
  %call68 = call %struct.btPointCollector* @_ZN16btPointCollectorD2Ev(%struct.btPointCollector* %pointCollector) #8
  %75 = load i1, i1* %retval, align 1
  ret i1 %75
}

declare void @_ZN22btVoronoiSimplexSolver5resetEv(%class.btVoronoiSimplexSolver*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btPointCollector* @_ZN16btPointCollectorC2Ev(%struct.btPointCollector* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  %0 = bitcast %struct.btPointCollector* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #8
  %1 = bitcast %struct.btPointCollector* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV16btPointCollector, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_normalOnBInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normalOnBInWorld)
  %m_pointInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_pointInWorld)
  %m_distance = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 3
  store float 0x43ABC16D60000000, float* %m_distance, align 4
  %m_hasResult = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 4
  store i8 0, i8* %m_hasResult, align 4
  ret %struct.btPointCollector* %this1
}

declare %class.btGjkPairDetector* @_ZN17btGjkPairDetectorC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver(%class.btGjkPairDetector* returned, %class.btConvexShape*, %class.btConvexShape*, %class.btVoronoiSimplexSolver*, %class.btConvexPenetrationDepthSolver*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* @_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev(%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %this.addr, align 4
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 0
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transformA)
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 1
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transformB)
  %m_maximumDistanceSquared = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 2
  store float 0x43ABC16D60000000, float* %m_maximumDistanceSquared, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

declare void @_ZN17btGjkPairDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%class.btGjkPairDetector*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132), %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4), %class.btIDebugDraw*, i1 zeroext) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector315setInterpolate3ERKS_S1_f(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, float %rt) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %rt.addr = alloca float, align 4
  %s = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store float %rt, float* %rt.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %rt.addr, align 4
  %sub = fsub float 1.000000e+00, %0
  store float %sub, float* %s, align 4
  %1 = load float, float* %s, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %3 = load float, float* %arrayidx, align 4
  %mul = fmul float %1, %3
  %4 = load float, float* %rt.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %6 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %4, %6
  %add = fadd float %mul, %mul4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 0
  store float %add, float* %arrayidx6, align 4
  %7 = load float, float* %s, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %m_floats7 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %9 = load float, float* %arrayidx8, align 4
  %mul9 = fmul float %7, %9
  %10 = load float, float* %rt.addr, align 4
  %11 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 1
  %12 = load float, float* %arrayidx11, align 4
  %mul12 = fmul float %10, %12
  %add13 = fadd float %mul9, %mul12
  %m_floats14 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [4 x float], [4 x float]* %m_floats14, i32 0, i32 1
  store float %add13, float* %arrayidx15, align 4
  %13 = load float, float* %s, align 4
  %14 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %m_floats16 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [4 x float], [4 x float]* %m_floats16, i32 0, i32 2
  %15 = load float, float* %arrayidx17, align 4
  %mul18 = fmul float %13, %15
  %16 = load float, float* %rt.addr, align 4
  %17 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats19 = getelementptr inbounds %class.btVector3, %class.btVector3* %17, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [4 x float], [4 x float]* %m_floats19, i32 0, i32 2
  %18 = load float, float* %arrayidx20, align 4
  %mul21 = fmul float %16, %18
  %add22 = fadd float %mul18, %mul21
  %m_floats23 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 2
  store float %add22, float* %arrayidx24, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGjkPairDetector* @_ZN17btGjkPairDetectorD2Ev(%class.btGjkPairDetector* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGjkPairDetector*, align 4
  store %class.btGjkPairDetector* %this, %class.btGjkPairDetector** %this.addr, align 4
  %this1 = load %class.btGjkPairDetector*, %class.btGjkPairDetector** %this.addr, align 4
  %0 = bitcast %class.btGjkPairDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #8
  ret %class.btGjkPairDetector* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btPointCollector* @_ZN16btPointCollectorD2Ev(%struct.btPointCollector* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  %0 = bitcast %struct.btPointCollector* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #8
  ret %struct.btPointCollector* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGjkConvexCast* @_ZN15btGjkConvexCastD2Ev(%class.btGjkConvexCast* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGjkConvexCast*, align 4
  store %class.btGjkConvexCast* %this, %class.btGjkConvexCast** %this.addr, align 4
  %this1 = load %class.btGjkConvexCast*, %class.btGjkConvexCast** %this.addr, align 4
  %0 = bitcast %class.btGjkConvexCast* %this1 to %class.btConvexCast*
  %call = call %class.btConvexCast* @_ZN12btConvexCastD2Ev(%class.btConvexCast* %0) #8
  ret %class.btGjkConvexCast* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btGjkConvexCastD0Ev(%class.btGjkConvexCast* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGjkConvexCast*, align 4
  store %class.btGjkConvexCast* %this, %class.btGjkConvexCast** %this.addr, align 4
  %this1 = load %class.btGjkConvexCast*, %class.btGjkConvexCast** %this.addr, align 4
  %call = call %class.btGjkConvexCast* @_ZN15btGjkConvexCastD2Ev(%class.btGjkConvexCast* %this1) #8
  %0 = bitcast %class.btGjkConvexCast* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %0 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btPointCollectorD0Ev(%struct.btPointCollector* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  %call = call %struct.btPointCollector* @_ZN16btPointCollectorD2Ev(%struct.btPointCollector* %this1) #8
  %0 = bitcast %struct.btPointCollector* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btPointCollector20setShapeIdentifiersAEii(%struct.btPointCollector* %this, i32 %partId0, i32 %index0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  %partId0.addr = alloca i32, align 4
  %index0.addr = alloca i32, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4
  store i32 %partId0, i32* %partId0.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btPointCollector20setShapeIdentifiersBEii(%struct.btPointCollector* %this, i32 %partId1, i32 %index1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  %partId1.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4
  store i32 %partId1, i32* %partId1.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btPointCollector15addContactPointERK9btVector3S2_f(%struct.btPointCollector* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnBInWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %pointInWorld, float %depth) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btPointCollector*, align 4
  %normalOnBInWorld.addr = alloca %class.btVector3*, align 4
  %pointInWorld.addr = alloca %class.btVector3*, align 4
  %depth.addr = alloca float, align 4
  store %struct.btPointCollector* %this, %struct.btPointCollector** %this.addr, align 4
  store %class.btVector3* %normalOnBInWorld, %class.btVector3** %normalOnBInWorld.addr, align 4
  store %class.btVector3* %pointInWorld, %class.btVector3** %pointInWorld.addr, align 4
  store float %depth, float* %depth.addr, align 4
  %this1 = load %struct.btPointCollector*, %struct.btPointCollector** %this.addr, align 4
  %0 = load float, float* %depth.addr, align 4
  %m_distance = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 3
  %1 = load float, float* %m_distance, align 4
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_hasResult = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 4
  store i8 1, i8* %m_hasResult, align 4
  %2 = load %class.btVector3*, %class.btVector3** %normalOnBInWorld.addr, align 4
  %m_normalOnBInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 1
  %3 = bitcast %class.btVector3* %m_normalOnBInWorld to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %5 = load %class.btVector3*, %class.btVector3** %pointInWorld.addr, align 4
  %m_pointInWorld = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 2
  %6 = bitcast %class.btVector3* %m_pointInWorld to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %8 = load float, float* %depth.addr, align 4
  %m_distance2 = getelementptr inbounds %struct.btPointCollector, %struct.btPointCollector* %this1, i32 0, i32 3
  store float %8, float* %m_distance2, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #5

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: nounwind
declare %class.btConvexCast* @_ZN12btConvexCastD2Ev(%class.btConvexCast* returned) unnamed_addr #7

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btGjkConvexCast.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { cold noreturn nounwind }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
