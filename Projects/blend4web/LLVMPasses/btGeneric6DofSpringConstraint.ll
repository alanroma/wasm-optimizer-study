; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btGeneric6DofSpringConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/ConstraintSolver/btGeneric6DofSpringConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btGeneric6DofSpringConstraint = type { %class.btGeneric6DofConstraint.base, [6 x i8], [6 x float], [6 x float], [6 x float] }
%class.btGeneric6DofConstraint.base = type <{ %class.btTypedConstraint, %class.btTransform, %class.btTransform, [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry], %class.btTranslationalLimitMotor, [3 x %class.btRotationalLimitMotor], float, %class.btTransform, %class.btTransform, %class.btVector3, [3 x %class.btVector3], %class.btVector3, float, float, i8, [3 x i8], %class.btVector3, i8, i8, [2 x i8], i32, i8 }>
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.0, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.0 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btJacobianEntry = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float }
%class.btTranslationalLimitMotor = type { %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, %class.btVector3, %class.btVector3, %class.btVector3, [3 x i8], %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [3 x i32] }
%class.btRotationalLimitMotor = type { float, float, float, float, float, float, float, float, float, float, float, i8, float, float, i32, float }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.1, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%class.btGeneric6DofConstraint = type <{ %class.btTypedConstraint, %class.btTransform, %class.btTransform, [3 x %class.btJacobianEntry], [3 x %class.btJacobianEntry], %class.btTranslationalLimitMotor, [3 x %class.btRotationalLimitMotor], float, %class.btTransform, %class.btTransform, %class.btVector3, [3 x %class.btVector3], %class.btVector3, float, float, i8, [3 x i8], %class.btVector3, i8, i8, [2 x i8], i32, i8, [3 x i8] }>
%"struct.btTypedConstraint::btConstraintInfo2" = type { float, float, float*, float*, float*, float*, i32, float*, float*, float*, float*, i32, float }
%class.btAlignedObjectArray.5 = type opaque
%"struct.btTypedConstraint::btConstraintInfo1" = type { i32, i32 }
%struct.btSolverBody = type { %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btRigidBody* }
%class.btSerializer = type opaque
%struct.btGeneric6DofSpringConstraintData = type { %struct.btGeneric6DofConstraintData, [6 x i32], [6 x float], [6 x float], [6 x float] }
%struct.btGeneric6DofConstraintData = type { %struct.btTypedConstraintData, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, i32, i32 }
%struct.btTypedConstraintData = type { %struct.btRigidBodyFloatData*, %struct.btRigidBodyFloatData*, i8*, i32, i32, i32, i32, float, float, i32, i32, float, i32 }
%struct.btRigidBodyFloatData = type { %struct.btCollisionObjectFloatData, %struct.btMatrix3x3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, i32 }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type opaque
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector310normalizedEv = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btRigidBody24getCenterOfMassTransformEv = comdat any

$_ZNK11btTransform7inverseEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN29btGeneric6DofSpringConstraintD2Ev = comdat any

$_ZN29btGeneric6DofSpringConstraintD0Ev = comdat any

$_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif = comdat any

$_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f = comdat any

$_ZNK29btGeneric6DofSpringConstraint28calculateSerializeBufferSizeEv = comdat any

$_ZNK29btGeneric6DofSpringConstraint9serializeEPvP12btSerializer = comdat any

$_ZNK23btGeneric6DofConstraint8getFlagsEv = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZN23btGeneric6DofConstraintD2Ev = comdat any

$_ZN17btTypedConstraintD2Ev = comdat any

$_ZN29btGeneric6DofSpringConstraintdlEPv = comdat any

$_ZNK23btGeneric6DofConstraint9serializeEPvP12btSerializer = comdat any

$_ZNK11btTransform9serializeER20btTransformFloatData = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV29btGeneric6DofSpringConstraint = hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI29btGeneric6DofSpringConstraint to i8*), i8* bitcast (%class.btGeneric6DofSpringConstraint* (%class.btGeneric6DofSpringConstraint*)* @_ZN29btGeneric6DofSpringConstraintD2Ev to i8*), i8* bitcast (void (%class.btGeneric6DofSpringConstraint*)* @_ZN29btGeneric6DofSpringConstraintD0Ev to i8*), i8* bitcast (void (%class.btGeneric6DofConstraint*)* @_ZN23btGeneric6DofConstraint13buildJacobianEv to i8*), i8* bitcast (void (%class.btTypedConstraint*, %class.btAlignedObjectArray.5*, i32, i32, float)* @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif to i8*), i8* bitcast (void (%class.btGeneric6DofConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*)* @_ZN23btGeneric6DofConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E to i8*), i8* bitcast (void (%class.btGeneric6DofSpringConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*)* @_ZN29btGeneric6DofSpringConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E to i8*), i8* bitcast (void (%class.btTypedConstraint*, %struct.btSolverBody*, %struct.btSolverBody*, float)* @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f to i8*), i8* bitcast (void (%class.btGeneric6DofConstraint*, i32, float, i32)* @_ZN23btGeneric6DofConstraint8setParamEifi to i8*), i8* bitcast (float (%class.btGeneric6DofConstraint*, i32, i32)* @_ZNK23btGeneric6DofConstraint8getParamEii to i8*), i8* bitcast (i32 (%class.btGeneric6DofSpringConstraint*)* @_ZNK29btGeneric6DofSpringConstraint28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btGeneric6DofSpringConstraint*, i8*, %class.btSerializer*)* @_ZNK29btGeneric6DofSpringConstraint9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btGeneric6DofConstraint*)* @_ZN23btGeneric6DofConstraint13calcAnchorPosEv to i8*), i8* bitcast (i32 (%class.btGeneric6DofConstraint*)* @_ZNK23btGeneric6DofConstraint8getFlagsEv to i8*), i8* bitcast (void (%class.btGeneric6DofSpringConstraint*, %class.btVector3*, %class.btVector3*)* @_ZN29btGeneric6DofSpringConstraint7setAxisERK9btVector3S2_ to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS29btGeneric6DofSpringConstraint = hidden constant [32 x i8] c"29btGeneric6DofSpringConstraint\00", align 1
@_ZTI23btGeneric6DofConstraint = external constant i8*
@_ZTI29btGeneric6DofSpringConstraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([32 x i8], [32 x i8]* @_ZTS29btGeneric6DofSpringConstraint, i32 0, i32 0), i8* bitcast (i8** @_ZTI23btGeneric6DofConstraint to i8*) }, align 4
@_ZTV17btTypedConstraint = external unnamed_addr constant { [13 x i8*] }, align 4
@.str = private unnamed_addr constant [34 x i8] c"btGeneric6DofSpringConstraintData\00", align 1
@.str.1 = private unnamed_addr constant [28 x i8] c"btGeneric6DofConstraintData\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btGeneric6DofSpringConstraint.cpp, i8* null }]

@_ZN29btGeneric6DofSpringConstraintC1ER11btRigidBodyS1_RK11btTransformS4_b = hidden unnamed_addr alias %class.btGeneric6DofSpringConstraint* (%class.btGeneric6DofSpringConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*, i1), %class.btGeneric6DofSpringConstraint* (%class.btGeneric6DofSpringConstraint*, %class.btRigidBody*, %class.btRigidBody*, %class.btTransform*, %class.btTransform*, i1)* @_ZN29btGeneric6DofSpringConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b
@_ZN29btGeneric6DofSpringConstraintC1ER11btRigidBodyRK11btTransformb = hidden unnamed_addr alias %class.btGeneric6DofSpringConstraint* (%class.btGeneric6DofSpringConstraint*, %class.btRigidBody*, %class.btTransform*, i1), %class.btGeneric6DofSpringConstraint* (%class.btGeneric6DofSpringConstraint*, %class.btRigidBody*, %class.btTransform*, i1)* @_ZN29btGeneric6DofSpringConstraintC2ER11btRigidBodyRK11btTransformb

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btGeneric6DofSpringConstraint* @_ZN29btGeneric6DofSpringConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b(%class.btGeneric6DofSpringConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbA, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbB, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInA, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInB, i1 zeroext %useLinearReferenceFrameA) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  %rbA.addr = alloca %class.btRigidBody*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %frameInA.addr = alloca %class.btTransform*, align 4
  %frameInB.addr = alloca %class.btTransform*, align 4
  %useLinearReferenceFrameA.addr = alloca i8, align 1
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  store %class.btRigidBody* %rbA, %class.btRigidBody** %rbA.addr, align 4
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4
  store %class.btTransform* %frameInA, %class.btTransform** %frameInA.addr, align 4
  store %class.btTransform* %frameInB, %class.btTransform** %frameInB.addr, align 4
  %frombool = zext i1 %useLinearReferenceFrameA to i8
  store i8 %frombool, i8* %useLinearReferenceFrameA.addr, align 1
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %0 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbA.addr, align 4
  %2 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4
  %3 = load %class.btTransform*, %class.btTransform** %frameInA.addr, align 4
  %4 = load %class.btTransform*, %class.btTransform** %frameInB.addr, align 4
  %5 = load i8, i8* %useLinearReferenceFrameA.addr, align 1
  %tobool = trunc i8 %5 to i1
  %call = call %class.btGeneric6DofConstraint* @_ZN23btGeneric6DofConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b(%class.btGeneric6DofConstraint* %0, %class.btRigidBody* nonnull align 4 dereferenceable(676) %1, %class.btRigidBody* nonnull align 4 dereferenceable(676) %2, %class.btTransform* nonnull align 4 dereferenceable(64) %3, %class.btTransform* nonnull align 4 dereferenceable(64) %4, i1 zeroext %tobool)
  %6 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV29btGeneric6DofSpringConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %6, align 4
  call void @_ZN29btGeneric6DofSpringConstraint4initEv(%class.btGeneric6DofSpringConstraint* %this1)
  ret %class.btGeneric6DofSpringConstraint* %this1
}

declare %class.btGeneric6DofConstraint* @_ZN23btGeneric6DofConstraintC2ER11btRigidBodyS1_RK11btTransformS4_b(%class.btGeneric6DofConstraint* returned, %class.btRigidBody* nonnull align 4 dereferenceable(676), %class.btRigidBody* nonnull align 4 dereferenceable(676), %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), i1 zeroext) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN29btGeneric6DofSpringConstraint4initEv(%class.btGeneric6DofSpringConstraint* %this) #1 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %0 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to i8*
  %add.ptr = getelementptr inbounds i8, i8* %0, i32 4
  %1 = bitcast i8* %add.ptr to %struct.btTypedObject*
  %m_objectType = getelementptr inbounds %struct.btTypedObject, %struct.btTypedObject* %1, i32 0, i32 0
  store i32 9, i32* %m_objectType, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %2, 6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_springEnabled = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 1
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [6 x i8], [6 x i8]* %m_springEnabled, i32 0, i32 %3
  store i8 0, i8* %arrayidx, align 1
  %m_equilibriumPoint = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 2
  %4 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds [6 x float], [6 x float]* %m_equilibriumPoint, i32 0, i32 %4
  store float 0.000000e+00, float* %arrayidx2, align 4
  %m_springStiffness = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 3
  %5 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [6 x float], [6 x float]* %m_springStiffness, i32 0, i32 %5
  store float 0.000000e+00, float* %arrayidx3, align 4
  %m_springDamping = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 4
  %6 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [6 x float], [6 x float]* %m_springDamping, i32 0, i32 %6
  store float 1.000000e+00, float* %arrayidx4, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btGeneric6DofSpringConstraint* @_ZN29btGeneric6DofSpringConstraintC2ER11btRigidBodyRK11btTransformb(%class.btGeneric6DofSpringConstraint* returned %this, %class.btRigidBody* nonnull align 4 dereferenceable(676) %rbB, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInB, i1 zeroext %useLinearReferenceFrameB) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  %rbB.addr = alloca %class.btRigidBody*, align 4
  %frameInB.addr = alloca %class.btTransform*, align 4
  %useLinearReferenceFrameB.addr = alloca i8, align 1
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  store %class.btRigidBody* %rbB, %class.btRigidBody** %rbB.addr, align 4
  store %class.btTransform* %frameInB, %class.btTransform** %frameInB.addr, align 4
  %frombool = zext i1 %useLinearReferenceFrameB to i8
  store i8 %frombool, i8* %useLinearReferenceFrameB.addr, align 1
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %0 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %1 = load %class.btRigidBody*, %class.btRigidBody** %rbB.addr, align 4
  %2 = load %class.btTransform*, %class.btTransform** %frameInB.addr, align 4
  %3 = load i8, i8* %useLinearReferenceFrameB.addr, align 1
  %tobool = trunc i8 %3 to i1
  %call = call %class.btGeneric6DofConstraint* @_ZN23btGeneric6DofConstraintC2ER11btRigidBodyRK11btTransformb(%class.btGeneric6DofConstraint* %0, %class.btRigidBody* nonnull align 4 dereferenceable(676) %1, %class.btTransform* nonnull align 4 dereferenceable(64) %2, i1 zeroext %tobool)
  %4 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV29btGeneric6DofSpringConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4
  call void @_ZN29btGeneric6DofSpringConstraint4initEv(%class.btGeneric6DofSpringConstraint* %this1)
  ret %class.btGeneric6DofSpringConstraint* %this1
}

declare %class.btGeneric6DofConstraint* @_ZN23btGeneric6DofConstraintC2ER11btRigidBodyRK11btTransformb(%class.btGeneric6DofConstraint* returned, %class.btRigidBody* nonnull align 4 dereferenceable(676), %class.btTransform* nonnull align 4 dereferenceable(64), i1 zeroext) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN29btGeneric6DofSpringConstraint12enableSpringEib(%class.btGeneric6DofSpringConstraint* %this, i32 %index, i1 zeroext %onOff) #1 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  %index.addr = alloca i32, align 4
  %onOff.addr = alloca i8, align 1
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %frombool = zext i1 %onOff to i8
  store i8 %frombool, i8* %onOff.addr, align 1
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %0 = load i8, i8* %onOff.addr, align 1
  %tobool = trunc i8 %0 to i1
  %m_springEnabled = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 1
  %1 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [6 x i8], [6 x i8]* %m_springEnabled, i32 0, i32 %1
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %arrayidx, align 1
  %2 = load i32, i32* %index.addr, align 4
  %cmp = icmp slt i32 %2, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load i8, i8* %onOff.addr, align 1
  %tobool3 = trunc i8 %3 to i1
  %4 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %4, i32 0, i32 5
  %m_enableMotor = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits, i32 0, i32 9
  %5 = load i32, i32* %index.addr, align 4
  %arrayidx4 = getelementptr inbounds [3 x i8], [3 x i8]* %m_enableMotor, i32 0, i32 %5
  %frombool5 = zext i1 %tobool3 to i8
  store i8 %frombool5, i8* %arrayidx4, align 1
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = load i8, i8* %onOff.addr, align 1
  %tobool6 = trunc i8 %6 to i1
  %7 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %7, i32 0, i32 6
  %8 = load i32, i32* %index.addr, align 4
  %sub = sub nsw i32 %8, 3
  %arrayidx7 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits, i32 0, i32 %sub
  %m_enableMotor8 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx7, i32 0, i32 11
  %frombool9 = zext i1 %tobool6 to i8
  store i8 %frombool9, i8* %m_enableMotor8, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN29btGeneric6DofSpringConstraint12setStiffnessEif(%class.btGeneric6DofSpringConstraint* %this, i32 %index, float %stiffness) #1 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  %index.addr = alloca i32, align 4
  %stiffness.addr = alloca float, align 4
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store float %stiffness, float* %stiffness.addr, align 4
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %0 = load float, float* %stiffness.addr, align 4
  %m_springStiffness = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 3
  %1 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [6 x float], [6 x float]* %m_springStiffness, i32 0, i32 %1
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN29btGeneric6DofSpringConstraint10setDampingEif(%class.btGeneric6DofSpringConstraint* %this, i32 %index, float %damping) #1 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  %index.addr = alloca i32, align 4
  %damping.addr = alloca float, align 4
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store float %damping, float* %damping.addr, align 4
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %0 = load float, float* %damping.addr, align 4
  %m_springDamping = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 4
  %1 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [6 x float], [6 x float]* %m_springDamping, i32 0, i32 %1
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEv(%class.btGeneric6DofSpringConstraint* %this) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %0 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  call void @_ZN23btGeneric6DofConstraint19calculateTransformsEv(%class.btGeneric6DofConstraint* %0)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %m_calculatedLinearDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %2, i32 0, i32 12
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedLinearDiff)
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %3
  %4 = load float, float* %arrayidx, align 4
  %m_equilibriumPoint = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 2
  %5 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds [6 x float], [6 x float]* %m_equilibriumPoint, i32 0, i32 %5
  store float %4, float* %arrayidx2, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc10, %for.end
  %7 = load i32, i32* %i, align 4
  %cmp4 = icmp slt i32 %7, 3
  br i1 %cmp4, label %for.body5, label %for.end12

for.body5:                                        ; preds = %for.cond3
  %8 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %8, i32 0, i32 10
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedAxisAngleDiff)
  %9 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 %9
  %10 = load float, float* %arrayidx7, align 4
  %m_equilibriumPoint8 = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 2
  %11 = load i32, i32* %i, align 4
  %add = add nsw i32 %11, 3
  %arrayidx9 = getelementptr inbounds [6 x float], [6 x float]* %m_equilibriumPoint8, i32 0, i32 %add
  store float %10, float* %arrayidx9, align 4
  br label %for.inc10

for.inc10:                                        ; preds = %for.body5
  %12 = load i32, i32* %i, align 4
  %inc11 = add nsw i32 %12, 1
  store i32 %inc11, i32* %i, align 4
  br label %for.cond3

for.end12:                                        ; preds = %for.cond3
  ret void
}

declare void @_ZN23btGeneric6DofConstraint19calculateTransformsEv(%class.btGeneric6DofConstraint*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEi(%class.btGeneric6DofSpringConstraint* %this, i32 %index) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  %index.addr = alloca i32, align 4
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %0 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  call void @_ZN23btGeneric6DofConstraint19calculateTransformsEv(%class.btGeneric6DofConstraint* %0)
  %1 = load i32, i32* %index.addr, align 4
  %cmp = icmp slt i32 %1, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %m_calculatedLinearDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %2, i32 0, i32 12
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedLinearDiff)
  %3 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %3
  %4 = load float, float* %arrayidx, align 4
  %m_equilibriumPoint = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 2
  %5 = load i32, i32* %index.addr, align 4
  %arrayidx2 = getelementptr inbounds [6 x float], [6 x float]* %m_equilibriumPoint, i32 0, i32 %5
  store float %4, float* %arrayidx2, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %6, i32 0, i32 10
  %call3 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedAxisAngleDiff)
  %7 = load i32, i32* %index.addr, align 4
  %sub = sub nsw i32 %7, 3
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 %sub
  %8 = load float, float* %arrayidx4, align 4
  %m_equilibriumPoint5 = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 2
  %9 = load i32, i32* %index.addr, align 4
  %arrayidx6 = getelementptr inbounds [6 x float], [6 x float]* %m_equilibriumPoint5, i32 0, i32 %9
  store float %8, float* %arrayidx6, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN29btGeneric6DofSpringConstraint19setEquilibriumPointEif(%class.btGeneric6DofSpringConstraint* %this, i32 %index, float %val) #1 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  %index.addr = alloca i32, align 4
  %val.addr = alloca float, align 4
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store float %val, float* %val.addr, align 4
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %0 = load float, float* %val.addr, align 4
  %m_equilibriumPoint = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 2
  %1 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [6 x float], [6 x float]* %m_equilibriumPoint, i32 0, i32 %1
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN29btGeneric6DofSpringConstraint21internalUpdateSpringsEPN17btTypedConstraint17btConstraintInfo2E(%class.btGeneric6DofSpringConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info) #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  %i = alloca i32, align 4
  %currPos = alloca float, align 4
  %delta = alloca float, align 4
  %force = alloca float, align 4
  %velFactor = alloca float, align 4
  %currPos23 = alloca float, align 4
  %delta26 = alloca float, align 4
  %force31 = alloca float, align 4
  %velFactor36 = alloca float, align 4
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_springEnabled = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 1
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [6 x i8], [6 x i8]* %m_springEnabled, i32 0, i32 %1
  %2 = load i8, i8* %arrayidx, align 1
  %tobool = trunc i8 %2 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %3 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %m_calculatedLinearDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %3, i32 0, i32 12
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedLinearDiff)
  %4 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %4
  %5 = load float, float* %arrayidx2, align 4
  store float %5, float* %currPos, align 4
  %6 = load float, float* %currPos, align 4
  %m_equilibriumPoint = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 2
  %7 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [6 x float], [6 x float]* %m_equilibriumPoint, i32 0, i32 %7
  %8 = load float, float* %arrayidx3, align 4
  %sub = fsub float %6, %8
  store float %sub, float* %delta, align 4
  %9 = load float, float* %delta, align 4
  %m_springStiffness = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 3
  %10 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [6 x float], [6 x float]* %m_springStiffness, i32 0, i32 %10
  %11 = load float, float* %arrayidx4, align 4
  %mul = fmul float %9, %11
  store float %mul, float* %force, align 4
  %12 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %12, i32 0, i32 0
  %13 = load float, float* %fps, align 4
  %m_springDamping = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 4
  %14 = load i32, i32* %i, align 4
  %arrayidx5 = getelementptr inbounds [6 x float], [6 x float]* %m_springDamping, i32 0, i32 %14
  %15 = load float, float* %arrayidx5, align 4
  %mul6 = fmul float %13, %15
  %16 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_numIterations = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %16, i32 0, i32 11
  %17 = load i32, i32* %m_numIterations, align 4
  %conv = sitofp i32 %17 to float
  %div = fdiv float %mul6, %conv
  store float %div, float* %velFactor, align 4
  %18 = load float, float* %velFactor, align 4
  %19 = load float, float* %force, align 4
  %mul7 = fmul float %18, %19
  %20 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %20, i32 0, i32 5
  %m_targetVelocity = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits, i32 0, i32 10
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_targetVelocity)
  %21 = load i32, i32* %i, align 4
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 %21
  store float %mul7, float* %arrayidx9, align 4
  %22 = load float, float* %force, align 4
  %call10 = call float @_Z6btFabsf(float %22)
  %23 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps11 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %23, i32 0, i32 0
  %24 = load float, float* %fps11, align 4
  %div12 = fdiv float %call10, %24
  %25 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %m_linearLimits13 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %25, i32 0, i32 5
  %m_maxMotorForce = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits13, i32 0, i32 11
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_maxMotorForce)
  %26 = load i32, i32* %i, align 4
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 %26
  store float %div12, float* %arrayidx15, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %27 = load i32, i32* %i, align 4
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc55, %for.end
  %28 = load i32, i32* %i, align 4
  %cmp17 = icmp slt i32 %28, 3
  br i1 %cmp17, label %for.body18, label %for.end57

for.body18:                                       ; preds = %for.cond16
  %m_springEnabled19 = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 1
  %29 = load i32, i32* %i, align 4
  %add = add nsw i32 %29, 3
  %arrayidx20 = getelementptr inbounds [6 x i8], [6 x i8]* %m_springEnabled19, i32 0, i32 %add
  %30 = load i8, i8* %arrayidx20, align 1
  %tobool21 = trunc i8 %30 to i1
  br i1 %tobool21, label %if.then22, label %if.end54

if.then22:                                        ; preds = %for.body18
  %31 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %m_calculatedAxisAngleDiff = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %31, i32 0, i32 10
  %call24 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_calculatedAxisAngleDiff)
  %32 = load i32, i32* %i, align 4
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 %32
  %33 = load float, float* %arrayidx25, align 4
  store float %33, float* %currPos23, align 4
  %34 = load float, float* %currPos23, align 4
  %m_equilibriumPoint27 = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 2
  %35 = load i32, i32* %i, align 4
  %add28 = add nsw i32 %35, 3
  %arrayidx29 = getelementptr inbounds [6 x float], [6 x float]* %m_equilibriumPoint27, i32 0, i32 %add28
  %36 = load float, float* %arrayidx29, align 4
  %sub30 = fsub float %34, %36
  store float %sub30, float* %delta26, align 4
  %37 = load float, float* %delta26, align 4
  %fneg = fneg float %37
  %m_springStiffness32 = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 3
  %38 = load i32, i32* %i, align 4
  %add33 = add nsw i32 %38, 3
  %arrayidx34 = getelementptr inbounds [6 x float], [6 x float]* %m_springStiffness32, i32 0, i32 %add33
  %39 = load float, float* %arrayidx34, align 4
  %mul35 = fmul float %fneg, %39
  store float %mul35, float* %force31, align 4
  %40 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps37 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %40, i32 0, i32 0
  %41 = load float, float* %fps37, align 4
  %m_springDamping38 = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 4
  %42 = load i32, i32* %i, align 4
  %add39 = add nsw i32 %42, 3
  %arrayidx40 = getelementptr inbounds [6 x float], [6 x float]* %m_springDamping38, i32 0, i32 %add39
  %43 = load float, float* %arrayidx40, align 4
  %mul41 = fmul float %41, %43
  %44 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %m_numIterations42 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %44, i32 0, i32 11
  %45 = load i32, i32* %m_numIterations42, align 4
  %conv43 = sitofp i32 %45 to float
  %div44 = fdiv float %mul41, %conv43
  store float %div44, float* %velFactor36, align 4
  %46 = load float, float* %velFactor36, align 4
  %47 = load float, float* %force31, align 4
  %mul45 = fmul float %46, %47
  %48 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %48, i32 0, i32 6
  %49 = load i32, i32* %i, align 4
  %arrayidx46 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits, i32 0, i32 %49
  %m_targetVelocity47 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx46, i32 0, i32 2
  store float %mul45, float* %m_targetVelocity47, align 4
  %50 = load float, float* %force31, align 4
  %call48 = call float @_Z6btFabsf(float %50)
  %51 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %fps49 = getelementptr inbounds %"struct.btTypedConstraint::btConstraintInfo2", %"struct.btTypedConstraint::btConstraintInfo2"* %51, i32 0, i32 0
  %52 = load float, float* %fps49, align 4
  %div50 = fdiv float %call48, %52
  %53 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %m_angularLimits51 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %53, i32 0, i32 6
  %54 = load i32, i32* %i, align 4
  %arrayidx52 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits51, i32 0, i32 %54
  %m_maxMotorForce53 = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx52, i32 0, i32 3
  store float %div50, float* %m_maxMotorForce53, align 4
  br label %if.end54

if.end54:                                         ; preds = %if.then22, %for.body18
  br label %for.inc55

for.inc55:                                        ; preds = %if.end54
  %55 = load i32, i32* %i, align 4
  %inc56 = add nsw i32 %55, 1
  store i32 %inc56, i32* %i, align 4
  br label %for.cond16

for.end57:                                        ; preds = %for.cond16
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define hidden void @_ZN29btGeneric6DofSpringConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E(%class.btGeneric6DofSpringConstraint* %this, %"struct.btTypedConstraint::btConstraintInfo2"* %info) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  %info.addr = alloca %"struct.btTypedConstraint::btConstraintInfo2"*, align 4
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  store %"struct.btTypedConstraint::btConstraintInfo2"* %info, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %0 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  call void @_ZN29btGeneric6DofSpringConstraint21internalUpdateSpringsEPN17btTypedConstraint17btConstraintInfo2E(%class.btGeneric6DofSpringConstraint* %this1, %"struct.btTypedConstraint::btConstraintInfo2"* %0)
  %1 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %2 = load %"struct.btTypedConstraint::btConstraintInfo2"*, %"struct.btTypedConstraint::btConstraintInfo2"** %info.addr, align 4
  call void @_ZN23btGeneric6DofConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E(%class.btGeneric6DofConstraint* %1, %"struct.btTypedConstraint::btConstraintInfo2"* %2)
  ret void
}

declare void @_ZN23btGeneric6DofConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E(%class.btGeneric6DofConstraint*, %"struct.btTypedConstraint::btConstraintInfo2"*) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN29btGeneric6DofSpringConstraint7setAxisERK9btVector3S2_(%class.btGeneric6DofSpringConstraint* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %axis1, %class.btVector3* nonnull align 4 dereferenceable(16) %axis2) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  %axis1.addr = alloca %class.btVector3*, align 4
  %axis2.addr = alloca %class.btVector3*, align 4
  %zAxis = alloca %class.btVector3, align 4
  %yAxis = alloca %class.btVector3, align 4
  %xAxis = alloca %class.btVector3, align 4
  %frameInW = alloca %class.btTransform, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  %ref.tmp20 = alloca %class.btTransform, align 4
  %ref.tmp23 = alloca %class.btTransform, align 4
  %ref.tmp24 = alloca %class.btTransform, align 4
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  store %class.btVector3* %axis1, %class.btVector3** %axis1.addr, align 4
  store %class.btVector3* %axis2, %class.btVector3** %axis2.addr, align 4
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %axis1.addr, align 4
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %zAxis, %class.btVector3* %0)
  %1 = load %class.btVector3*, %class.btVector3** %axis2.addr, align 4
  call void @_ZNK9btVector310normalizedEv(%class.btVector3* sret align 4 %yAxis, %class.btVector3* %1)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %xAxis, %class.btVector3* %yAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %zAxis)
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %frameInW)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %frameInW)
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %frameInW)
  %call3 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %xAxis)
  %arrayidx = getelementptr inbounds float, float* %call3, i32 0
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %yAxis)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %zAxis)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 0
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %xAxis)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 1
  %call10 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %yAxis)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %zAxis)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 1
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %xAxis)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  %call16 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %yAxis)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 2
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %zAxis)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 2
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %call2, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx5, float* nonnull align 4 dereferenceable(4) %arrayidx7, float* nonnull align 4 dereferenceable(4) %arrayidx9, float* nonnull align 4 dereferenceable(4) %arrayidx11, float* nonnull align 4 dereferenceable(4) %arrayidx13, float* nonnull align 4 dereferenceable(4) %arrayidx15, float* nonnull align 4 dereferenceable(4) %arrayidx17, float* nonnull align 4 dereferenceable(4) %arrayidx19)
  %2 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btTypedConstraint*
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %2, i32 0, i32 8
  %3 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  %call21 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %3)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp20, %class.btTransform* %call21)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %ref.tmp20, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInW)
  %4 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %4, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_frameInA, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp)
  %5 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btTypedConstraint*
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %5, i32 0, i32 9
  %6 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  %call25 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %6)
  call void @_ZNK11btTransform7inverseEv(%class.btTransform* sret align 4 %ref.tmp24, %class.btTransform* %call25)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp23, %class.btTransform* %ref.tmp24, %class.btTransform* nonnull align 4 dereferenceable(64) %frameInW)
  %7 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %7, i32 0, i32 2
  %call26 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_frameInB, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp23)
  %8 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  call void @_ZN23btGeneric6DofConstraint19calculateTransformsEv(%class.btGeneric6DofConstraint* %8)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector310normalizedEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %nrm = alloca %class.btVector3, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = bitcast %class.btVector3* %nrm to i8*
  %1 = bitcast %class.btVector3* %this1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %nrm)
  %2 = bitcast %class.btVector3* %agg.result to i8*
  %3 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform7inverseEv(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %inv = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %inv, %class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %inv, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGeneric6DofSpringConstraint* @_ZN29btGeneric6DofSpringConstraintD2Ev(%class.btGeneric6DofSpringConstraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %0 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %call = call %class.btGeneric6DofConstraint* @_ZN23btGeneric6DofConstraintD2Ev(%class.btGeneric6DofConstraint* %0) #6
  ret %class.btGeneric6DofSpringConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN29btGeneric6DofSpringConstraintD0Ev(%class.btGeneric6DofSpringConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %call = call %class.btGeneric6DofSpringConstraint* @_ZN29btGeneric6DofSpringConstraintD2Ev(%class.btGeneric6DofSpringConstraint* %this1) #6
  %0 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to i8*
  call void @_ZN29btGeneric6DofSpringConstraintdlEPv(i8* %0) #6
  ret void
}

declare void @_ZN23btGeneric6DofConstraint13buildJacobianEv(%class.btGeneric6DofConstraint*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif(%class.btTypedConstraint* %this, %class.btAlignedObjectArray.5* nonnull align 1 %ca, i32 %solverBodyA, i32 %solverBodyB, float %timeStep) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %ca.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %solverBodyA.addr = alloca i32, align 4
  %solverBodyB.addr = alloca i32, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store %class.btAlignedObjectArray.5* %ca, %class.btAlignedObjectArray.5** %ca.addr, align 4
  store i32 %solverBodyA, i32* %solverBodyA.addr, align 4
  store i32 %solverBodyB, i32* %solverBodyB.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %ca.addr, align 4
  ret void
}

declare void @_ZN23btGeneric6DofConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E(%class.btGeneric6DofConstraint*, %"struct.btTypedConstraint::btConstraintInfo1"*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btTypedConstraint23solveConstraintObsoleteER12btSolverBodyS1_f(%class.btTypedConstraint* %this, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %0, %struct.btSolverBody* nonnull align 4 dereferenceable(244) %1, float %2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  %.addr = alloca %struct.btSolverBody*, align 4
  %.addr1 = alloca %struct.btSolverBody*, align 4
  %.addr2 = alloca float, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  store %struct.btSolverBody* %0, %struct.btSolverBody** %.addr, align 4
  store %struct.btSolverBody* %1, %struct.btSolverBody** %.addr1, align 4
  store float %2, float* %.addr2, align 4
  %this3 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  ret void
}

declare void @_ZN23btGeneric6DofConstraint8setParamEifi(%class.btGeneric6DofConstraint*, i32, float, i32) unnamed_addr #3

declare float @_ZNK23btGeneric6DofConstraint8getParamEii(%class.btGeneric6DofConstraint*, i32, i32) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK29btGeneric6DofSpringConstraint28calculateSerializeBufferSizeEv(%class.btGeneric6DofSpringConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  ret i32 348
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNK29btGeneric6DofSpringConstraint9serializeEPvP12btSerializer(%class.btGeneric6DofSpringConstraint* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofSpringConstraint*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %dof = alloca %struct.btGeneric6DofSpringConstraintData*, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofSpringConstraint* %this, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btGeneric6DofSpringConstraint*, %class.btGeneric6DofSpringConstraint** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btGeneric6DofSpringConstraintData*
  store %struct.btGeneric6DofSpringConstraintData* %1, %struct.btGeneric6DofSpringConstraintData** %dof, align 4
  %2 = bitcast %class.btGeneric6DofSpringConstraint* %this1 to %class.btGeneric6DofConstraint*
  %3 = load %struct.btGeneric6DofSpringConstraintData*, %struct.btGeneric6DofSpringConstraintData** %dof, align 4
  %m_6dofData = getelementptr inbounds %struct.btGeneric6DofSpringConstraintData, %struct.btGeneric6DofSpringConstraintData* %3, i32 0, i32 0
  %4 = bitcast %struct.btGeneric6DofConstraintData* %m_6dofData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK23btGeneric6DofConstraint9serializeEPvP12btSerializer(%class.btGeneric6DofConstraint* %2, i8* %4, %class.btSerializer* %5)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %6 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %6, 6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_equilibriumPoint = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 2
  %7 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [6 x float], [6 x float]* %m_equilibriumPoint, i32 0, i32 %7
  %8 = load float, float* %arrayidx, align 4
  %9 = load %struct.btGeneric6DofSpringConstraintData*, %struct.btGeneric6DofSpringConstraintData** %dof, align 4
  %m_equilibriumPoint2 = getelementptr inbounds %struct.btGeneric6DofSpringConstraintData, %struct.btGeneric6DofSpringConstraintData* %9, i32 0, i32 2
  %10 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [6 x float], [6 x float]* %m_equilibriumPoint2, i32 0, i32 %10
  store float %8, float* %arrayidx3, align 4
  %m_springDamping = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 4
  %11 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [6 x float], [6 x float]* %m_springDamping, i32 0, i32 %11
  %12 = load float, float* %arrayidx4, align 4
  %13 = load %struct.btGeneric6DofSpringConstraintData*, %struct.btGeneric6DofSpringConstraintData** %dof, align 4
  %m_springDamping5 = getelementptr inbounds %struct.btGeneric6DofSpringConstraintData, %struct.btGeneric6DofSpringConstraintData* %13, i32 0, i32 4
  %14 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds [6 x float], [6 x float]* %m_springDamping5, i32 0, i32 %14
  store float %12, float* %arrayidx6, align 4
  %m_springEnabled = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 1
  %15 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds [6 x i8], [6 x i8]* %m_springEnabled, i32 0, i32 %15
  %16 = load i8, i8* %arrayidx7, align 1
  %tobool = trunc i8 %16 to i1
  %17 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 0
  %18 = load %struct.btGeneric6DofSpringConstraintData*, %struct.btGeneric6DofSpringConstraintData** %dof, align 4
  %m_springEnabled8 = getelementptr inbounds %struct.btGeneric6DofSpringConstraintData, %struct.btGeneric6DofSpringConstraintData* %18, i32 0, i32 1
  %19 = load i32, i32* %i, align 4
  %arrayidx9 = getelementptr inbounds [6 x i32], [6 x i32]* %m_springEnabled8, i32 0, i32 %19
  store i32 %cond, i32* %arrayidx9, align 4
  %m_springStiffness = getelementptr inbounds %class.btGeneric6DofSpringConstraint, %class.btGeneric6DofSpringConstraint* %this1, i32 0, i32 3
  %20 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds [6 x float], [6 x float]* %m_springStiffness, i32 0, i32 %20
  %21 = load float, float* %arrayidx10, align 4
  %22 = load %struct.btGeneric6DofSpringConstraintData*, %struct.btGeneric6DofSpringConstraintData** %dof, align 4
  %m_springStiffness11 = getelementptr inbounds %struct.btGeneric6DofSpringConstraintData, %struct.btGeneric6DofSpringConstraintData* %22, i32 0, i32 3
  %23 = load i32, i32* %i, align 4
  %arrayidx12 = getelementptr inbounds [6 x float], [6 x float]* %m_springStiffness11, i32 0, i32 %23
  store float %21, float* %arrayidx12, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %24 = load i32, i32* %i, align 4
  %inc = add nsw i32 %24, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str, i32 0, i32 0)
}

declare void @_ZN23btGeneric6DofConstraint13calcAnchorPosEv(%class.btGeneric6DofConstraint*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK23btGeneric6DofConstraint8getFlagsEv(%class.btGeneric6DofConstraint* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %m_flags = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 21
  %0 = load i32, i32* %m_flags, align 4
  ret i32 %0
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #4

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGeneric6DofConstraint* @_ZN23btGeneric6DofConstraintD2Ev(%class.btGeneric6DofConstraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %call = call %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* %0) #6
  ret %class.btGeneric6DofConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTypedConstraint* @_ZN17btTypedConstraintD2Ev(%class.btTypedConstraint* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %0 = bitcast %class.btTypedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [13 x i8*] }, { [13 x i8*] }* @_ZTV17btTypedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btTypedConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN29btGeneric6DofSpringConstraintdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNK23btGeneric6DofConstraint9serializeEPvP12btSerializer(%class.btGeneric6DofConstraint* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGeneric6DofConstraint*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %dof = alloca %struct.btGeneric6DofConstraintData*, align 4
  %i = alloca i32, align 4
  store %class.btGeneric6DofConstraint* %this, %class.btGeneric6DofConstraint** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btGeneric6DofConstraint*, %class.btGeneric6DofConstraint** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btGeneric6DofConstraintData*
  store %struct.btGeneric6DofConstraintData* %1, %struct.btGeneric6DofConstraintData** %dof, align 4
  %2 = bitcast %class.btGeneric6DofConstraint* %this1 to %class.btTypedConstraint*
  %3 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4
  %m_typeConstraintData = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %3, i32 0, i32 0
  %4 = bitcast %struct.btTypedConstraintData* %m_typeConstraintData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint* %2, i8* %4, %class.btSerializer* %5)
  %m_frameInA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 1
  %6 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4
  %m_rbAFrame = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %6, i32 0, i32 1
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_frameInA, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbAFrame)
  %m_frameInB = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 2
  %7 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4
  %m_rbBFrame = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %7, i32 0, i32 2
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_frameInB, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_rbBFrame)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %8, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_angularLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %9 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits, i32 0, i32 %9
  %m_loLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx, i32 0, i32 0
  %10 = load float, float* %m_loLimit, align 4
  %11 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4
  %m_angularLowerLimit = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %11, i32 0, i32 6
  %m_floats = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularLowerLimit, i32 0, i32 0
  %12 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %12
  store float %10, float* %arrayidx2, align 4
  %m_angularLimits3 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 6
  %13 = load i32, i32* %i, align 4
  %arrayidx4 = getelementptr inbounds [3 x %class.btRotationalLimitMotor], [3 x %class.btRotationalLimitMotor]* %m_angularLimits3, i32 0, i32 %13
  %m_hiLimit = getelementptr inbounds %class.btRotationalLimitMotor, %class.btRotationalLimitMotor* %arrayidx4, i32 0, i32 1
  %14 = load float, float* %m_hiLimit, align 4
  %15 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4
  %m_angularUpperLimit = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %15, i32 0, i32 5
  %m_floats5 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_angularUpperLimit, i32 0, i32 0
  %16 = load i32, i32* %i, align 4
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 %16
  store float %14, float* %arrayidx6, align 4
  %m_linearLimits = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_lowerLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits, i32 0, i32 0
  %call7 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_lowerLimit)
  %17 = load i32, i32* %i, align 4
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 %17
  %18 = load float, float* %arrayidx8, align 4
  %19 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4
  %m_linearLowerLimit = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %19, i32 0, i32 4
  %m_floats9 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_linearLowerLimit, i32 0, i32 0
  %20 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 %20
  store float %18, float* %arrayidx10, align 4
  %m_linearLimits11 = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 5
  %m_upperLimit = getelementptr inbounds %class.btTranslationalLimitMotor, %class.btTranslationalLimitMotor* %m_linearLimits11, i32 0, i32 1
  %call12 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_upperLimit)
  %21 = load i32, i32* %i, align 4
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 %21
  %22 = load float, float* %arrayidx13, align 4
  %23 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4
  %m_linearUpperLimit = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %23, i32 0, i32 3
  %m_floats14 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %m_linearUpperLimit, i32 0, i32 0
  %24 = load i32, i32* %i, align 4
  %arrayidx15 = getelementptr inbounds [4 x float], [4 x float]* %m_floats14, i32 0, i32 %24
  store float %22, float* %arrayidx15, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %25 = load i32, i32* %i, align 4
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_useLinearReferenceFrameA = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 18
  %26 = load i8, i8* %m_useLinearReferenceFrameA, align 4
  %tobool = trunc i8 %26 to i1
  %27 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 1, i32 0
  %28 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4
  %m_useLinearReferenceFrameA16 = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %28, i32 0, i32 7
  store i32 %cond, i32* %m_useLinearReferenceFrameA16, align 4
  %m_useOffsetForConstraintFrame = getelementptr inbounds %class.btGeneric6DofConstraint, %class.btGeneric6DofConstraint* %this1, i32 0, i32 19
  %29 = load i8, i8* %m_useOffsetForConstraintFrame, align 1
  %tobool17 = trunc i8 %29 to i1
  %30 = zext i1 %tobool17 to i64
  %cond18 = select i1 %tobool17, i32 1, i32 0
  %31 = load %struct.btGeneric6DofConstraintData*, %struct.btGeneric6DofConstraintData** %dof, align 4
  %m_useOffsetForConstraintFrame19 = getelementptr inbounds %struct.btGeneric6DofConstraintData, %struct.btGeneric6DofConstraintData* %31, i32 0, i32 8
  store i32 %cond18, i32* %m_useOffsetForConstraintFrame19, align 4
  ret i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str.1, i32 0, i32 0)
}

declare i8* @_ZNK17btTypedConstraint9serializeEPvP12btSerializer(%class.btTypedConstraint*, i8*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %this, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %dataOut.addr = alloca %struct.btTransformFloatData*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %struct.btTransformFloatData* %dataOut, %struct.btTransformFloatData** %dataOut.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_basis2 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %0, i32 0, i32 0
  call void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_basis, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_origin3 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %1, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_origin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_origin3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %1
  %2 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %2, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %3
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btGeneric6DofSpringConstraint.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind readnone speculatable willreturn }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
