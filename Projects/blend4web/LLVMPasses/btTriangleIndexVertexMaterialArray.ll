; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btTriangleIndexVertexMaterialArray.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btTriangleIndexVertexMaterialArray.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btTriangleIndexVertexMaterialArray = type { %class.btTriangleIndexVertexArray, %class.btAlignedObjectArray.0 }
%class.btTriangleIndexVertexArray = type { %class.btStridingMeshInterface, %class.btAlignedObjectArray, [2 x i32], i32, %class.btVector3, %class.btVector3 }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btIndexedMesh*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btIndexedMesh = type { i32, i8*, i32, i32, i8*, i32, i32, i32 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btMaterialProperties*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btMaterialProperties = type { i32, i8*, i32, i32, i32, i8*, i32, i32 }
%class.btInternalTriangleIndexCallback = type { i32 (...)** }
%class.btSerializer = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesEC2Ev = comdat any

$_ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesEixEi = comdat any

$_ZN34btTriangleIndexVertexMaterialArrayD2Ev = comdat any

$_ZN34btTriangleIndexVertexMaterialArrayD0Ev = comdat any

$_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi = comdat any

$_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi = comdat any

$_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv = comdat any

$_ZN26btTriangleIndexVertexArray19preallocateVerticesEi = comdat any

$_ZN26btTriangleIndexVertexArray18preallocateIndicesEi = comdat any

$_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE9push_backERKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesED2Ev = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI20btMaterialPropertiesE4initEv = comdat any

$_ZN34btTriangleIndexVertexMaterialArraydlEPv = comdat any

$_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv = comdat any

$_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EEC2Ev = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV34btTriangleIndexVertexMaterialArray = hidden unnamed_addr constant { [19 x i8*] } { [19 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI34btTriangleIndexVertexMaterialArray to i8*), i8* bitcast (%class.btTriangleIndexVertexMaterialArray* (%class.btTriangleIndexVertexMaterialArray*)* @_ZN34btTriangleIndexVertexMaterialArrayD2Ev to i8*), i8* bitcast (void (%class.btTriangleIndexVertexMaterialArray*)* @_ZN34btTriangleIndexVertexMaterialArrayD0Ev to i8*), i8* bitcast (void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_ to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)* @_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)* @_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i32)* @_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i32)* @_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi to i8*), i8* bitcast (i32 (%class.btTriangleIndexVertexArray*)* @_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i32)* @_ZN26btTriangleIndexVertexArray19preallocateVerticesEi to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i32)* @_ZN26btTriangleIndexVertexArray18preallocateIndicesEi to i8*), i8* bitcast (i1 (%class.btTriangleIndexVertexArray*)* @_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, %class.btVector3*, %class.btVector3*)* @_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_ to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, %class.btVector3*, %class.btVector3*)* @_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_ to i8*), i8* bitcast (i32 (%class.btStridingMeshInterface*)* @_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)* @_ZNK23btStridingMeshInterface9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)* @_ZN34btTriangleIndexVertexMaterialArray21getLockedMaterialBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i to i8*), i8* bitcast (void (%class.btTriangleIndexVertexMaterialArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)* @_ZN34btTriangleIndexVertexMaterialArray29getLockedReadOnlyMaterialBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS34btTriangleIndexVertexMaterialArray = hidden constant [37 x i8] c"34btTriangleIndexVertexMaterialArray\00", align 1
@_ZTI26btTriangleIndexVertexArray = external constant i8*
@_ZTI34btTriangleIndexVertexMaterialArray = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([37 x i8], [37 x i8]* @_ZTS34btTriangleIndexVertexMaterialArray, i32 0, i32 0), i8* bitcast (i8** @_ZTI26btTriangleIndexVertexArray to i8*) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btTriangleIndexVertexMaterialArray.cpp, i8* null }]

@_ZN34btTriangleIndexVertexMaterialArrayC1EiPiiiPfiiPhiS0_i = hidden unnamed_addr alias %class.btTriangleIndexVertexMaterialArray* (%class.btTriangleIndexVertexMaterialArray*, i32, i32*, i32, i32, float*, i32, i32, i8*, i32, i32*, i32), %class.btTriangleIndexVertexMaterialArray* (%class.btTriangleIndexVertexMaterialArray*, i32, i32*, i32, i32, float*, i32, i32, i8*, i32, i32*, i32)* @_ZN34btTriangleIndexVertexMaterialArrayC2EiPiiiPfiiPhiS0_i

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btTriangleIndexVertexMaterialArray* @_ZN34btTriangleIndexVertexMaterialArrayC2EiPiiiPfiiPhiS0_i(%class.btTriangleIndexVertexMaterialArray* returned %this, i32 %numTriangles, i32* %triangleIndexBase, i32 %triangleIndexStride, i32 %numVertices, float* %vertexBase, i32 %vertexStride, i32 %numMaterials, i8* %materialBase, i32 %materialStride, i32* %triangleMaterialsBase, i32 %materialIndexStride) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexMaterialArray*, align 4
  %numTriangles.addr = alloca i32, align 4
  %triangleIndexBase.addr = alloca i32*, align 4
  %triangleIndexStride.addr = alloca i32, align 4
  %numVertices.addr = alloca i32, align 4
  %vertexBase.addr = alloca float*, align 4
  %vertexStride.addr = alloca i32, align 4
  %numMaterials.addr = alloca i32, align 4
  %materialBase.addr = alloca i8*, align 4
  %materialStride.addr = alloca i32, align 4
  %triangleMaterialsBase.addr = alloca i32*, align 4
  %materialIndexStride.addr = alloca i32, align 4
  %mat = alloca %struct.btMaterialProperties, align 4
  store %class.btTriangleIndexVertexMaterialArray* %this, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  store i32 %numTriangles, i32* %numTriangles.addr, align 4
  store i32* %triangleIndexBase, i32** %triangleIndexBase.addr, align 4
  store i32 %triangleIndexStride, i32* %triangleIndexStride.addr, align 4
  store i32 %numVertices, i32* %numVertices.addr, align 4
  store float* %vertexBase, float** %vertexBase.addr, align 4
  store i32 %vertexStride, i32* %vertexStride.addr, align 4
  store i32 %numMaterials, i32* %numMaterials.addr, align 4
  store i8* %materialBase, i8** %materialBase.addr, align 4
  store i32 %materialStride, i32* %materialStride.addr, align 4
  store i32* %triangleMaterialsBase, i32** %triangleMaterialsBase.addr, align 4
  store i32 %materialIndexStride, i32* %materialIndexStride.addr, align 4
  %this1 = load %class.btTriangleIndexVertexMaterialArray*, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  %0 = bitcast %class.btTriangleIndexVertexMaterialArray* %this1 to %class.btTriangleIndexVertexArray*
  %1 = load i32, i32* %numTriangles.addr, align 4
  %2 = load i32*, i32** %triangleIndexBase.addr, align 4
  %3 = load i32, i32* %triangleIndexStride.addr, align 4
  %4 = load i32, i32* %numVertices.addr, align 4
  %5 = load float*, float** %vertexBase.addr, align 4
  %6 = load i32, i32* %vertexStride.addr, align 4
  %call = call %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayC2EiPiiiPfi(%class.btTriangleIndexVertexArray* %0, i32 %1, i32* %2, i32 %3, i32 %4, float* %5, i32 %6)
  %7 = bitcast %class.btTriangleIndexVertexMaterialArray* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV34btTriangleIndexVertexMaterialArray, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %7, align 4
  %m_materials = getelementptr inbounds %class.btTriangleIndexVertexMaterialArray, %class.btTriangleIndexVertexMaterialArray* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesEC2Ev(%class.btAlignedObjectArray.0* %m_materials)
  %8 = load i32, i32* %numMaterials.addr, align 4
  %m_numMaterials = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 0
  store i32 %8, i32* %m_numMaterials, align 4
  %9 = load i8*, i8** %materialBase.addr, align 4
  %m_materialBase = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 1
  store i8* %9, i8** %m_materialBase, align 4
  %10 = load i32, i32* %materialStride.addr, align 4
  %m_materialStride = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 2
  store i32 %10, i32* %m_materialStride, align 4
  %m_materialType = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 3
  store i32 0, i32* %m_materialType, align 4
  %11 = load i32, i32* %numTriangles.addr, align 4
  %m_numTriangles = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 4
  store i32 %11, i32* %m_numTriangles, align 4
  %12 = load i32*, i32** %triangleMaterialsBase.addr, align 4
  %13 = bitcast i32* %12 to i8*
  %m_triangleMaterialsBase = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 5
  store i8* %13, i8** %m_triangleMaterialsBase, align 4
  %14 = load i32, i32* %materialIndexStride.addr, align 4
  %m_triangleMaterialStride = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 6
  store i32 %14, i32* %m_triangleMaterialStride, align 4
  %m_triangleType = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %mat, i32 0, i32 7
  store i32 2, i32* %m_triangleType, align 4
  call void @_ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType(%class.btTriangleIndexVertexMaterialArray* %this1, %struct.btMaterialProperties* nonnull align 4 dereferenceable(32) %mat, i32 2)
  ret %class.btTriangleIndexVertexMaterialArray* %this1
}

declare %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayC2EiPiiiPfi(%class.btTriangleIndexVertexArray* returned, i32, i32*, i32, i32, float*, i32) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType(%class.btTriangleIndexVertexMaterialArray* %this, %struct.btMaterialProperties* nonnull align 4 dereferenceable(32) %mat, i32 %triangleType) #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexMaterialArray*, align 4
  %mat.addr = alloca %struct.btMaterialProperties*, align 4
  %triangleType.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexMaterialArray* %this, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  store %struct.btMaterialProperties* %mat, %struct.btMaterialProperties** %mat.addr, align 4
  store i32 %triangleType, i32* %triangleType.addr, align 4
  %this1 = load %class.btTriangleIndexVertexMaterialArray*, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  %m_materials = getelementptr inbounds %class.btTriangleIndexVertexMaterialArray, %class.btTriangleIndexVertexMaterialArray* %this1, i32 0, i32 1
  %0 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mat.addr, align 4
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE9push_backERKS0_(%class.btAlignedObjectArray.0* %m_materials, %struct.btMaterialProperties* nonnull align 4 dereferenceable(32) %0)
  %1 = load i32, i32* %triangleType.addr, align 4
  %m_materials2 = getelementptr inbounds %class.btTriangleIndexVertexMaterialArray, %class.btTriangleIndexVertexMaterialArray* %this1, i32 0, i32 1
  %m_materials3 = getelementptr inbounds %class.btTriangleIndexVertexMaterialArray, %class.btTriangleIndexVertexMaterialArray* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv(%class.btAlignedObjectArray.0* %m_materials3)
  %sub = sub nsw i32 %call, 1
  %call4 = call nonnull align 4 dereferenceable(32) %struct.btMaterialProperties* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesEixEi(%class.btAlignedObjectArray.0* %m_materials2, i32 %sub)
  %m_triangleType = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %call4, i32 0, i32 7
  store i32 %1, i32* %m_triangleType, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN34btTriangleIndexVertexMaterialArray21getLockedMaterialBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i(%class.btTriangleIndexVertexMaterialArray* %this, i8** %materialBase, i32* nonnull align 4 dereferenceable(4) %numMaterials, i32* nonnull align 4 dereferenceable(4) %materialType, i32* nonnull align 4 dereferenceable(4) %materialStride, i8** %triangleMaterialBase, i32* nonnull align 4 dereferenceable(4) %numTriangles, i32* nonnull align 4 dereferenceable(4) %triangleMaterialStride, i32* nonnull align 4 dereferenceable(4) %triangleType, i32 %subpart) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexMaterialArray*, align 4
  %materialBase.addr = alloca i8**, align 4
  %numMaterials.addr = alloca i32*, align 4
  %materialType.addr = alloca i32*, align 4
  %materialStride.addr = alloca i32*, align 4
  %triangleMaterialBase.addr = alloca i8**, align 4
  %numTriangles.addr = alloca i32*, align 4
  %triangleMaterialStride.addr = alloca i32*, align 4
  %triangleType.addr = alloca i32*, align 4
  %subpart.addr = alloca i32, align 4
  %mats = alloca %struct.btMaterialProperties*, align 4
  store %class.btTriangleIndexVertexMaterialArray* %this, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  store i8** %materialBase, i8*** %materialBase.addr, align 4
  store i32* %numMaterials, i32** %numMaterials.addr, align 4
  store i32* %materialType, i32** %materialType.addr, align 4
  store i32* %materialStride, i32** %materialStride.addr, align 4
  store i8** %triangleMaterialBase, i8*** %triangleMaterialBase.addr, align 4
  store i32* %numTriangles, i32** %numTriangles.addr, align 4
  store i32* %triangleMaterialStride, i32** %triangleMaterialStride.addr, align 4
  store i32* %triangleType, i32** %triangleType.addr, align 4
  store i32 %subpart, i32* %subpart.addr, align 4
  %this1 = load %class.btTriangleIndexVertexMaterialArray*, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  %m_materials = getelementptr inbounds %class.btTriangleIndexVertexMaterialArray, %class.btTriangleIndexVertexMaterialArray* %this1, i32 0, i32 1
  %0 = load i32, i32* %subpart.addr, align 4
  %call = call nonnull align 4 dereferenceable(32) %struct.btMaterialProperties* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesEixEi(%class.btAlignedObjectArray.0* %m_materials, i32 %0)
  store %struct.btMaterialProperties* %call, %struct.btMaterialProperties** %mats, align 4
  %1 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4
  %m_numMaterials = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %1, i32 0, i32 0
  %2 = load i32, i32* %m_numMaterials, align 4
  %3 = load i32*, i32** %numMaterials.addr, align 4
  store i32 %2, i32* %3, align 4
  %4 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4
  %m_materialBase = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %4, i32 0, i32 1
  %5 = load i8*, i8** %m_materialBase, align 4
  %6 = load i8**, i8*** %materialBase.addr, align 4
  store i8* %5, i8** %6, align 4
  %7 = load i32*, i32** %materialType.addr, align 4
  store i32 0, i32* %7, align 4
  %8 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4
  %m_materialStride = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %8, i32 0, i32 2
  %9 = load i32, i32* %m_materialStride, align 4
  %10 = load i32*, i32** %materialStride.addr, align 4
  store i32 %9, i32* %10, align 4
  %11 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4
  %m_numTriangles = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %11, i32 0, i32 4
  %12 = load i32, i32* %m_numTriangles, align 4
  %13 = load i32*, i32** %numTriangles.addr, align 4
  store i32 %12, i32* %13, align 4
  %14 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4
  %m_triangleMaterialsBase = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %14, i32 0, i32 5
  %15 = load i8*, i8** %m_triangleMaterialsBase, align 4
  %16 = load i8**, i8*** %triangleMaterialBase.addr, align 4
  store i8* %15, i8** %16, align 4
  %17 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4
  %m_triangleMaterialStride = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %17, i32 0, i32 6
  %18 = load i32, i32* %m_triangleMaterialStride, align 4
  %19 = load i32*, i32** %triangleMaterialStride.addr, align 4
  store i32 %18, i32* %19, align 4
  %20 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4
  %m_triangleType = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %20, i32 0, i32 7
  %21 = load i32, i32* %m_triangleType, align 4
  %22 = load i32*, i32** %triangleType.addr, align 4
  store i32 %21, i32* %22, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %struct.btMaterialProperties* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %0, i32 %1
  ret %struct.btMaterialProperties* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN34btTriangleIndexVertexMaterialArray29getLockedReadOnlyMaterialBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i(%class.btTriangleIndexVertexMaterialArray* %this, i8** %materialBase, i32* nonnull align 4 dereferenceable(4) %numMaterials, i32* nonnull align 4 dereferenceable(4) %materialType, i32* nonnull align 4 dereferenceable(4) %materialStride, i8** %triangleMaterialBase, i32* nonnull align 4 dereferenceable(4) %numTriangles, i32* nonnull align 4 dereferenceable(4) %triangleMaterialStride, i32* nonnull align 4 dereferenceable(4) %triangleType, i32 %subpart) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexMaterialArray*, align 4
  %materialBase.addr = alloca i8**, align 4
  %numMaterials.addr = alloca i32*, align 4
  %materialType.addr = alloca i32*, align 4
  %materialStride.addr = alloca i32*, align 4
  %triangleMaterialBase.addr = alloca i8**, align 4
  %numTriangles.addr = alloca i32*, align 4
  %triangleMaterialStride.addr = alloca i32*, align 4
  %triangleType.addr = alloca i32*, align 4
  %subpart.addr = alloca i32, align 4
  %mats = alloca %struct.btMaterialProperties*, align 4
  store %class.btTriangleIndexVertexMaterialArray* %this, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  store i8** %materialBase, i8*** %materialBase.addr, align 4
  store i32* %numMaterials, i32** %numMaterials.addr, align 4
  store i32* %materialType, i32** %materialType.addr, align 4
  store i32* %materialStride, i32** %materialStride.addr, align 4
  store i8** %triangleMaterialBase, i8*** %triangleMaterialBase.addr, align 4
  store i32* %numTriangles, i32** %numTriangles.addr, align 4
  store i32* %triangleMaterialStride, i32** %triangleMaterialStride.addr, align 4
  store i32* %triangleType, i32** %triangleType.addr, align 4
  store i32 %subpart, i32* %subpart.addr, align 4
  %this1 = load %class.btTriangleIndexVertexMaterialArray*, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  %m_materials = getelementptr inbounds %class.btTriangleIndexVertexMaterialArray, %class.btTriangleIndexVertexMaterialArray* %this1, i32 0, i32 1
  %0 = load i32, i32* %subpart.addr, align 4
  %call = call nonnull align 4 dereferenceable(32) %struct.btMaterialProperties* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesEixEi(%class.btAlignedObjectArray.0* %m_materials, i32 %0)
  store %struct.btMaterialProperties* %call, %struct.btMaterialProperties** %mats, align 4
  %1 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4
  %m_numMaterials = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %1, i32 0, i32 0
  %2 = load i32, i32* %m_numMaterials, align 4
  %3 = load i32*, i32** %numMaterials.addr, align 4
  store i32 %2, i32* %3, align 4
  %4 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4
  %m_materialBase = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %4, i32 0, i32 1
  %5 = load i8*, i8** %m_materialBase, align 4
  %6 = load i8**, i8*** %materialBase.addr, align 4
  store i8* %5, i8** %6, align 4
  %7 = load i32*, i32** %materialType.addr, align 4
  store i32 0, i32* %7, align 4
  %8 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4
  %m_materialStride = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %8, i32 0, i32 2
  %9 = load i32, i32* %m_materialStride, align 4
  %10 = load i32*, i32** %materialStride.addr, align 4
  store i32 %9, i32* %10, align 4
  %11 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4
  %m_numTriangles = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %11, i32 0, i32 4
  %12 = load i32, i32* %m_numTriangles, align 4
  %13 = load i32*, i32** %numTriangles.addr, align 4
  store i32 %12, i32* %13, align 4
  %14 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4
  %m_triangleMaterialsBase = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %14, i32 0, i32 5
  %15 = load i8*, i8** %m_triangleMaterialsBase, align 4
  %16 = load i8**, i8*** %triangleMaterialBase.addr, align 4
  store i8* %15, i8** %16, align 4
  %17 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4
  %m_triangleMaterialStride = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %17, i32 0, i32 6
  %18 = load i32, i32* %m_triangleMaterialStride, align 4
  %19 = load i32*, i32** %triangleMaterialStride.addr, align 4
  store i32 %18, i32* %19, align 4
  %20 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %mats, align 4
  %m_triangleType = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %20, i32 0, i32 7
  %21 = load i32, i32* %m_triangleType, align 4
  %22 = load i32*, i32** %triangleType.addr, align 4
  store i32 %21, i32* %22, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTriangleIndexVertexMaterialArray* @_ZN34btTriangleIndexVertexMaterialArrayD2Ev(%class.btTriangleIndexVertexMaterialArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexMaterialArray*, align 4
  store %class.btTriangleIndexVertexMaterialArray* %this, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  %this1 = load %class.btTriangleIndexVertexMaterialArray*, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  %0 = bitcast %class.btTriangleIndexVertexMaterialArray* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [19 x i8*] }, { [19 x i8*] }* @_ZTV34btTriangleIndexVertexMaterialArray, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_materials = getelementptr inbounds %class.btTriangleIndexVertexMaterialArray, %class.btTriangleIndexVertexMaterialArray* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesED2Ev(%class.btAlignedObjectArray.0* %m_materials) #6
  %1 = bitcast %class.btTriangleIndexVertexMaterialArray* %this1 to %class.btTriangleIndexVertexArray*
  %call2 = call %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayD2Ev(%class.btTriangleIndexVertexArray* %1) #6
  ret %class.btTriangleIndexVertexMaterialArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN34btTriangleIndexVertexMaterialArrayD0Ev(%class.btTriangleIndexVertexMaterialArray* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexMaterialArray*, align 4
  store %class.btTriangleIndexVertexMaterialArray* %this, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  %this1 = load %class.btTriangleIndexVertexMaterialArray*, %class.btTriangleIndexVertexMaterialArray** %this.addr, align 4
  %call = call %class.btTriangleIndexVertexMaterialArray* @_ZN34btTriangleIndexVertexMaterialArrayD2Ev(%class.btTriangleIndexVertexMaterialArray* %this1) #6
  %0 = bitcast %class.btTriangleIndexVertexMaterialArray* %this1 to i8*
  call void @_ZN34btTriangleIndexVertexMaterialArraydlEPv(i8* %0) #6
  ret void
}

declare void @_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_(%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i(%class.btTriangleIndexVertexArray*, i8**, i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i8**, i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32) unnamed_addr #3

declare void @_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i(%class.btTriangleIndexVertexArray*, i8**, i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i8**, i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi(%class.btTriangleIndexVertexArray* %this, i32 %subpart) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %subpart.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store i32 %subpart, i32* %subpart.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi(%class.btTriangleIndexVertexArray* %this, i32 %subpart) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %subpart.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store i32 %subpart, i32* %subpart.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv(%class.btTriangleIndexVertexArray* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %m_indexedMeshes)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26btTriangleIndexVertexArray19preallocateVerticesEi(%class.btTriangleIndexVertexArray* %this, i32 %numverts) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %numverts.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store i32 %numverts, i32* %numverts.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26btTriangleIndexVertexArray18preallocateIndicesEi(%class.btTriangleIndexVertexArray* %this, i32 %numindices) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %numindices.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store i32 %numindices, i32* %numindices.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  ret void
}

declare zeroext i1 @_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv(%class.btTriangleIndexVertexArray*) unnamed_addr #3

declare void @_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_(%class.btTriangleIndexVertexArray*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_(%class.btTriangleIndexVertexArray*, %class.btVector3*, %class.btVector3*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv(%class.btStridingMeshInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  ret i32 28
}

declare i8* @_ZNK23btStridingMeshInterface9serializeEPvP12btSerializer(%class.btStridingMeshInterface*, i8*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE9push_backERKS0_(%class.btAlignedObjectArray.0* %this, %struct.btMaterialProperties* nonnull align 4 dereferenceable(32) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %struct.btMaterialProperties*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store %struct.btMaterialProperties* %_Val, %struct.btMaterialProperties** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %1 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %1, i32 %2
  %3 = bitcast %struct.btMaterialProperties* %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.btMaterialProperties*
  %5 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %_Val.addr, align 4
  %6 = bitcast %struct.btMaterialProperties* %4 to i8*
  %7 = bitcast %struct.btMaterialProperties* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 32, i1 false)
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btMaterialProperties*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btMaterialProperties*
  store %struct.btMaterialProperties* %2, %struct.btMaterialProperties** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %3 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %struct.btMaterialProperties* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btMaterialProperties* %4, %struct.btMaterialProperties** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btMaterialProperties* @_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %struct.btMaterialProperties** null)
  %2 = bitcast %struct.btMaterialProperties* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %struct.btMaterialProperties* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btMaterialProperties*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btMaterialProperties* %dest, %struct.btMaterialProperties** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %3, i32 %4
  %5 = bitcast %struct.btMaterialProperties* %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btMaterialProperties*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %7, i32 %8
  %9 = bitcast %struct.btMaterialProperties* %6 to i8*
  %10 = bitcast %struct.btMaterialProperties* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 32, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btMaterialProperties, %struct.btMaterialProperties* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %m_data, align 4
  %tobool = icmp ne %struct.btMaterialProperties* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.btMaterialProperties* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btMaterialProperties* null, %struct.btMaterialProperties** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btMaterialProperties* @_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, %struct.btMaterialProperties** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btMaterialProperties**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btMaterialProperties** %hint, %struct.btMaterialProperties*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 32, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btMaterialProperties*
  ret %struct.btMaterialProperties* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.btMaterialProperties* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.btMaterialProperties*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %struct.btMaterialProperties* %ptr, %struct.btMaterialProperties** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.btMaterialProperties*, %struct.btMaterialProperties** %ptr.addr, align 4
  %1 = bitcast %struct.btMaterialProperties* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI20btMaterialPropertiesED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
declare %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayD2Ev(%class.btTriangleIndexVertexArray* returned) unnamed_addr #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btMaterialPropertiesE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btMaterialProperties* null, %struct.btMaterialProperties** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN34btTriangleIndexVertexMaterialArraydlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI20btMaterialPropertiesLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btTriangleIndexVertexMaterialArray.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
