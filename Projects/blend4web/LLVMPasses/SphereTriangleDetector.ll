; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/SphereTriangleDetector.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/SphereTriangleDetector.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%struct.SphereTriangleDetector = type { %struct.btDiscreteCollisionDetectorInterface, %class.btSphereShape*, %class.btTriangleShape*, float }
%struct.btDiscreteCollisionDetectorInterface = type { i32 (...)** }
%class.btSphereShape = type { %class.btConvexInternalShape }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btVector3 = type { [4 x float] }
%class.btTriangleShape = type { %class.btPolyhedralConvexShape, [3 x %class.btVector3] }
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexPolyhedron = type opaque
%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput" = type { %class.btTransform, %class.btTransform, float }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%class.btIDebugDraw = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK11btTransform12inverseTimesERKS_ = comdat any

$_ZN11btTransform9getOriginEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZngRK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZN15btTriangleShape12getVertexPtrEi = comdat any

$_ZNK13btSphereShape9getRadiusEv = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_Z6btSqrtf = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZN22SphereTriangleDetectorD2Ev = comdat any

$_ZN22SphereTriangleDetectorD0Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD0Ev = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK11btMatrix3x314transposeTimesERKS_ = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZTS36btDiscreteCollisionDetectorInterface = comdat any

$_ZTI36btDiscreteCollisionDetectorInterface = comdat any

$_ZTV36btDiscreteCollisionDetectorInterface = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV22SphereTriangleDetector = hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22SphereTriangleDetector to i8*), i8* bitcast (%struct.SphereTriangleDetector* (%struct.SphereTriangleDetector*)* @_ZN22SphereTriangleDetectorD2Ev to i8*), i8* bitcast (void (%struct.SphereTriangleDetector*)* @_ZN22SphereTriangleDetectorD0Ev to i8*), i8* bitcast (void (%struct.SphereTriangleDetector*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btIDebugDraw*, i1)* @_ZN22SphereTriangleDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS22SphereTriangleDetector = hidden constant [25 x i8] c"22SphereTriangleDetector\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS36btDiscreteCollisionDetectorInterface = linkonce_odr hidden constant [39 x i8] c"36btDiscreteCollisionDetectorInterface\00", comdat, align 1
@_ZTI36btDiscreteCollisionDetectorInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([39 x i8], [39 x i8]* @_ZTS36btDiscreteCollisionDetectorInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI22SphereTriangleDetector = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22SphereTriangleDetector, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI36btDiscreteCollisionDetectorInterface to i8*) }, align 4
@_ZTV36btDiscreteCollisionDetectorInterface = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI36btDiscreteCollisionDetectorInterface to i8*), i8* bitcast (%struct.btDiscreteCollisionDetectorInterface* (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev to i8*), i8* bitcast (void (%struct.btDiscreteCollisionDetectorInterface*)* @_ZN36btDiscreteCollisionDetectorInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_SphereTriangleDetector.cpp, i8* null }]

@_ZN22SphereTriangleDetectorC1EP13btSphereShapeP15btTriangleShapef = hidden unnamed_addr alias %struct.SphereTriangleDetector* (%struct.SphereTriangleDetector*, %class.btSphereShape*, %class.btTriangleShape*, float), %struct.SphereTriangleDetector* (%struct.SphereTriangleDetector*, %class.btSphereShape*, %class.btTriangleShape*, float)* @_ZN22SphereTriangleDetectorC2EP13btSphereShapeP15btTriangleShapef

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %struct.SphereTriangleDetector* @_ZN22SphereTriangleDetectorC2EP13btSphereShapeP15btTriangleShapef(%struct.SphereTriangleDetector* returned %this, %class.btSphereShape* %sphere, %class.btTriangleShape* %triangle, float %contactBreakingThreshold) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.SphereTriangleDetector*, align 4
  %sphere.addr = alloca %class.btSphereShape*, align 4
  %triangle.addr = alloca %class.btTriangleShape*, align 4
  %contactBreakingThreshold.addr = alloca float, align 4
  store %struct.SphereTriangleDetector* %this, %struct.SphereTriangleDetector** %this.addr, align 4
  store %class.btSphereShape* %sphere, %class.btSphereShape** %sphere.addr, align 4
  store %class.btTriangleShape* %triangle, %class.btTriangleShape** %triangle.addr, align 4
  store float %contactBreakingThreshold, float* %contactBreakingThreshold.addr, align 4
  %this1 = load %struct.SphereTriangleDetector*, %struct.SphereTriangleDetector** %this.addr, align 4
  %0 = bitcast %struct.SphereTriangleDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceC2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #7
  %1 = bitcast %struct.SphereTriangleDetector* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV22SphereTriangleDetector, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_sphere = getelementptr inbounds %struct.SphereTriangleDetector, %struct.SphereTriangleDetector* %this1, i32 0, i32 1
  %2 = load %class.btSphereShape*, %class.btSphereShape** %sphere.addr, align 4
  store %class.btSphereShape* %2, %class.btSphereShape** %m_sphere, align 4
  %m_triangle = getelementptr inbounds %struct.SphereTriangleDetector, %struct.SphereTriangleDetector* %this1, i32 0, i32 2
  %3 = load %class.btTriangleShape*, %class.btTriangleShape** %triangle.addr, align 4
  store %class.btTriangleShape* %3, %class.btTriangleShape** %m_triangle, align 4
  %m_contactBreakingThreshold = getelementptr inbounds %struct.SphereTriangleDetector, %struct.SphereTriangleDetector* %this1, i32 0, i32 3
  %4 = load float, float* %contactBreakingThreshold.addr, align 4
  store float %4, float* %m_contactBreakingThreshold, align 4
  ret %struct.SphereTriangleDetector* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceC2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %0 = bitcast %struct.btDiscreteCollisionDetectorInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV36btDiscreteCollisionDetectorInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN22SphereTriangleDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%struct.SphereTriangleDetector* %this, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %output, %class.btIDebugDraw* %debugDraw, i1 zeroext %swapResults) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.SphereTriangleDetector*, align 4
  %input.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, align 4
  %output.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  %debugDraw.addr = alloca %class.btIDebugDraw*, align 4
  %swapResults.addr = alloca i8, align 1
  %transformA = alloca %class.btTransform*, align 4
  %transformB = alloca %class.btTransform*, align 4
  %point = alloca %class.btVector3, align 4
  %normal = alloca %class.btVector3, align 4
  %timeOfImpact = alloca float, align 4
  %depth = alloca float, align 4
  %sphereInTr = alloca %class.btTransform, align 4
  %normalOnB = alloca %class.btVector3, align 4
  %normalOnA = alloca %class.btVector3, align 4
  %pointOnA = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  store %struct.SphereTriangleDetector* %this, %struct.SphereTriangleDetector** %this.addr, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %output, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  store %class.btIDebugDraw* %debugDraw, %class.btIDebugDraw** %debugDraw.addr, align 4
  %frombool = zext i1 %swapResults to i8
  store i8 %frombool, i8* %swapResults.addr, align 1
  %this1 = load %struct.SphereTriangleDetector*, %struct.SphereTriangleDetector** %this.addr, align 4
  %0 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %0, i32 0, i32 0
  store %class.btTransform* %m_transformA, %class.btTransform** %transformA, align 4
  %1 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %input.addr, align 4
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %1, i32 0, i32 1
  store %class.btTransform* %m_transformB, %class.btTransform** %transformB, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %point)
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %normal)
  store float 1.000000e+00, float* %timeOfImpact, align 4
  store float 0.000000e+00, float* %depth, align 4
  %2 = load %class.btTransform*, %class.btTransform** %transformB, align 4
  %3 = load %class.btTransform*, %class.btTransform** %transformA, align 4
  call void @_ZNK11btTransform12inverseTimesERKS_(%class.btTransform* sret align 4 %sphereInTr, %class.btTransform* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %sphereInTr)
  %m_contactBreakingThreshold = getelementptr inbounds %struct.SphereTriangleDetector, %struct.SphereTriangleDetector* %this1, i32 0, i32 3
  %4 = load float, float* %m_contactBreakingThreshold, align 4
  %call4 = call zeroext i1 @_ZN22SphereTriangleDetector7collideERK9btVector3RS0_S3_RfS4_f(%struct.SphereTriangleDetector* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %point, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float* nonnull align 4 dereferenceable(4) %depth, float* nonnull align 4 dereferenceable(4) %timeOfImpact, float %4)
  br i1 %call4, label %if.then, label %if.end13

if.then:                                          ; preds = %entry
  %5 = load i8, i8* %swapResults.addr, align 1
  %tobool = trunc i8 %5 to i1
  br i1 %tobool, label %if.then5, label %if.else

if.then5:                                         ; preds = %if.then
  %6 = load %class.btTransform*, %class.btTransform** %transformB, align 4
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %6)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %normalOnB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %normalOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnB)
  %7 = load %class.btTransform*, %class.btTransform** %transformB, align 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btTransform* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %point)
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnB, float* nonnull align 4 dereferenceable(4) %depth)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %pointOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %8 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  %9 = load float, float* %depth, align 4
  %10 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %8 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %10, align 4
  %vfn = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable, i64 4
  %11 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn, align 4
  call void %11(%"struct.btDiscreteCollisionDetectorInterface::Result"* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %normalOnA, %class.btVector3* nonnull align 4 dereferenceable(16) %pointOnA, float %9)
  br label %if.end

if.else:                                          ; preds = %if.then
  %12 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %output.addr, align 4
  %13 = load %class.btTransform*, %class.btTransform** %transformB, align 4
  %call9 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %13)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp8, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call9, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %14 = load %class.btTransform*, %class.btTransform** %transformB, align 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp10, %class.btTransform* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %point)
  %15 = load float, float* %depth, align 4
  %16 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %12 to void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)***
  %vtable11 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)**, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*** %16, align 4
  %vfn12 = getelementptr inbounds void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vtable11, i64 4
  %17 = load void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)*, void (%"struct.btDiscreteCollisionDetectorInterface::Result"*, %class.btVector3*, %class.btVector3*, float)** %vfn12, align 4
  call void %17(%"struct.btDiscreteCollisionDetectorInterface::Result"* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10, float %15)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then5
  br label %if.end13

if.end13:                                         ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform12inverseTimesERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %v = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %1)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 0
  call void @_ZNK11btMatrix3x314transposeTimesERKS_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis4)
  %call5 = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN22SphereTriangleDetector7collideERK9btVector3RS0_S3_RfS4_f(%struct.SphereTriangleDetector* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %sphereCenter, %class.btVector3* nonnull align 4 dereferenceable(16) %point, %class.btVector3* nonnull align 4 dereferenceable(16) %resultNormal, float* nonnull align 4 dereferenceable(4) %depth, float* nonnull align 4 dereferenceable(4) %timeOfImpact, float %contactBreakingThreshold) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %struct.SphereTriangleDetector*, align 4
  %sphereCenter.addr = alloca %class.btVector3*, align 4
  %point.addr = alloca %class.btVector3*, align 4
  %resultNormal.addr = alloca %class.btVector3*, align 4
  %depth.addr = alloca float*, align 4
  %timeOfImpact.addr = alloca float*, align 4
  %contactBreakingThreshold.addr = alloca float, align 4
  %vertices = alloca %class.btVector3*, align 4
  %radius = alloca float, align 4
  %radiusWithThreshold = alloca float, align 4
  %normal = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %l2 = alloca float, align 4
  %hasContact = alloca i8, align 1
  %contactPoint = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca float, align 4
  %p1ToCentre = alloca %class.btVector3, align 4
  %distanceFromPlane = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %isInsideContactPlane = alloca i8, align 1
  %ref.tmp22 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca %class.btVector3, align 4
  %contactCapsuleRadiusSqr = alloca float, align 4
  %nearestOnEdge = alloca %class.btVector3, align 4
  %i = alloca i32, align 4
  %pa = alloca %class.btVector3, align 4
  %pb = alloca %class.btVector3, align 4
  %distanceSqr = alloca float, align 4
  %contactToCentre = alloca %class.btVector3, align 4
  %distanceSqr43 = alloca float, align 4
  %distance = alloca float, align 4
  store %struct.SphereTriangleDetector* %this, %struct.SphereTriangleDetector** %this.addr, align 4
  store %class.btVector3* %sphereCenter, %class.btVector3** %sphereCenter.addr, align 4
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4
  store %class.btVector3* %resultNormal, %class.btVector3** %resultNormal.addr, align 4
  store float* %depth, float** %depth.addr, align 4
  store float* %timeOfImpact, float** %timeOfImpact.addr, align 4
  store float %contactBreakingThreshold, float* %contactBreakingThreshold.addr, align 4
  %this1 = load %struct.SphereTriangleDetector*, %struct.SphereTriangleDetector** %this.addr, align 4
  %m_triangle = getelementptr inbounds %struct.SphereTriangleDetector, %struct.SphereTriangleDetector* %this1, i32 0, i32 2
  %0 = load %class.btTriangleShape*, %class.btTriangleShape** %m_triangle, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN15btTriangleShape12getVertexPtrEi(%class.btTriangleShape* %0, i32 0)
  store %class.btVector3* %call, %class.btVector3** %vertices, align 4
  %m_sphere = getelementptr inbounds %struct.SphereTriangleDetector, %struct.SphereTriangleDetector* %this1, i32 0, i32 1
  %1 = load %class.btSphereShape*, %class.btSphereShape** %m_sphere, align 4
  %call2 = call float @_ZNK13btSphereShape9getRadiusEv(%class.btSphereShape* %1)
  store float %call2, float* %radius, align 4
  %2 = load float, float* %radius, align 4
  %3 = load float, float* %contactBreakingThreshold.addr, align 4
  %add = fadd float %2, %3
  store float %add, float* %radiusWithThreshold, align 4
  %4 = load %class.btVector3*, %class.btVector3** %vertices, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 1
  %5 = load %class.btVector3*, %class.btVector3** %vertices, align 4
  %arrayidx3 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3)
  %6 = load %class.btVector3*, %class.btVector3** %vertices, align 4
  %arrayidx5 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 2
  %7 = load %class.btVector3*, %class.btVector3** %vertices, align 4
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx6)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %normal, %class.btVector3* %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp4)
  %call7 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %normal)
  store float %call7, float* %l2, align 4
  store i8 0, i8* %hasContact, align 1
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %contactPoint)
  %8 = load float, float* %l2, align 4
  %cmp = fcmp oge float %8, 0x3D10000000000000
  br i1 %cmp, label %if.then, label %if.end40

if.then:                                          ; preds = %entry
  %9 = load float, float* %l2, align 4
  %call10 = call float @_Z6btSqrtf(float %9)
  store float %call10, float* %ref.tmp9, align 4
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %normal, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %10 = load %class.btVector3*, %class.btVector3** %sphereCenter.addr, align 4
  %11 = load %class.btVector3*, %class.btVector3** %vertices, align 4
  %arrayidx12 = getelementptr inbounds %class.btVector3, %class.btVector3* %11, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %p1ToCentre, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx12)
  %call13 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %p1ToCentre, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  store float %call13, float* %distanceFromPlane, align 4
  %12 = load float, float* %distanceFromPlane, align 4
  %cmp14 = fcmp olt float %12, 0.000000e+00
  br i1 %cmp14, label %if.then15, label %if.end

if.then15:                                        ; preds = %if.then
  %13 = load float, float* %distanceFromPlane, align 4
  %mul = fmul float %13, -1.000000e+00
  store float %mul, float* %distanceFromPlane, align 4
  store float -1.000000e+00, float* %ref.tmp16, align 4
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %normal, float* nonnull align 4 dereferenceable(4) %ref.tmp16)
  br label %if.end

if.end:                                           ; preds = %if.then15, %if.then
  %14 = load float, float* %distanceFromPlane, align 4
  %15 = load float, float* %radiusWithThreshold, align 4
  %cmp18 = fcmp olt float %14, %15
  %frombool = zext i1 %cmp18 to i8
  store i8 %frombool, i8* %isInsideContactPlane, align 1
  %16 = load i8, i8* %isInsideContactPlane, align 1
  %tobool = trunc i8 %16 to i1
  br i1 %tobool, label %if.then19, label %if.end39

if.then19:                                        ; preds = %if.end
  %17 = load %class.btVector3*, %class.btVector3** %sphereCenter.addr, align 4
  %18 = load %class.btVector3*, %class.btVector3** %vertices, align 4
  %call20 = call zeroext i1 @_ZN22SphereTriangleDetector12facecontainsERK9btVector3PS1_RS0_(%struct.SphereTriangleDetector* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %17, %class.btVector3* %18, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  br i1 %call20, label %if.then21, label %if.else

if.then21:                                        ; preds = %if.then19
  store i8 1, i8* %hasContact, align 1
  %19 = load %class.btVector3*, %class.btVector3** %sphereCenter.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp23, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float* nonnull align 4 dereferenceable(4) %distanceFromPlane)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp22, %class.btVector3* nonnull align 4 dereferenceable(16) %19, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp23)
  %20 = bitcast %class.btVector3* %contactPoint to i8*
  %21 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %20, i8* align 4 %21, i32 16, i1 false)
  br label %if.end38

if.else:                                          ; preds = %if.then19
  %22 = load float, float* %radiusWithThreshold, align 4
  %23 = load float, float* %radiusWithThreshold, align 4
  %mul24 = fmul float %22, %23
  store float %mul24, float* %contactCapsuleRadiusSqr, align 4
  %call25 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %nearestOnEdge)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.else
  %24 = load i32, i32* %i, align 4
  %m_triangle26 = getelementptr inbounds %struct.SphereTriangleDetector, %struct.SphereTriangleDetector* %this1, i32 0, i32 2
  %25 = load %class.btTriangleShape*, %class.btTriangleShape** %m_triangle26, align 4
  %26 = bitcast %class.btTriangleShape* %25 to i32 (%class.btTriangleShape*)***
  %vtable = load i32 (%class.btTriangleShape*)**, i32 (%class.btTriangleShape*)*** %26, align 4
  %vfn = getelementptr inbounds i32 (%class.btTriangleShape*)*, i32 (%class.btTriangleShape*)** %vtable, i64 25
  %27 = load i32 (%class.btTriangleShape*)*, i32 (%class.btTriangleShape*)** %vfn, align 4
  %call27 = call i32 %27(%class.btTriangleShape* %25)
  %cmp28 = icmp slt i32 %24, %call27
  br i1 %cmp28, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call29 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pa)
  %call30 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pb)
  %m_triangle31 = getelementptr inbounds %struct.SphereTriangleDetector, %struct.SphereTriangleDetector* %this1, i32 0, i32 2
  %28 = load %class.btTriangleShape*, %class.btTriangleShape** %m_triangle31, align 4
  %29 = load i32, i32* %i, align 4
  %30 = bitcast %class.btTriangleShape* %28 to void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)***
  %vtable32 = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)**, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*** %30, align 4
  %vfn33 = getelementptr inbounds void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vtable32, i64 26
  %31 = load void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)*, void (%class.btTriangleShape*, i32, %class.btVector3*, %class.btVector3*)** %vfn33, align 4
  call void %31(%class.btTriangleShape* %28, i32 %29, %class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb)
  %32 = load %class.btVector3*, %class.btVector3** %sphereCenter.addr, align 4
  %call34 = call float @_Z18SegmentSqrDistanceRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %pa, %class.btVector3* nonnull align 4 dereferenceable(16) %pb, %class.btVector3* nonnull align 4 dereferenceable(16) %32, %class.btVector3* nonnull align 4 dereferenceable(16) %nearestOnEdge)
  store float %call34, float* %distanceSqr, align 4
  %33 = load float, float* %distanceSqr, align 4
  %34 = load float, float* %contactCapsuleRadiusSqr, align 4
  %cmp35 = fcmp olt float %33, %34
  br i1 %cmp35, label %if.then36, label %if.end37

if.then36:                                        ; preds = %for.body
  store i8 1, i8* %hasContact, align 1
  %35 = bitcast %class.btVector3* %contactPoint to i8*
  %36 = bitcast %class.btVector3* %nearestOnEdge to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %36, i32 16, i1 false)
  br label %if.end37

if.end37:                                         ; preds = %if.then36, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end37
  %37 = load i32, i32* %i, align 4
  %inc = add nsw i32 %37, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end38

if.end38:                                         ; preds = %for.end, %if.then21
  br label %if.end39

if.end39:                                         ; preds = %if.end38, %if.end
  br label %if.end40

if.end40:                                         ; preds = %if.end39, %entry
  %38 = load i8, i8* %hasContact, align 1
  %tobool41 = trunc i8 %38 to i1
  br i1 %tobool41, label %if.then42, label %if.end56

if.then42:                                        ; preds = %if.end40
  %39 = load %class.btVector3*, %class.btVector3** %sphereCenter.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %contactToCentre, %class.btVector3* nonnull align 4 dereferenceable(16) %39, %class.btVector3* nonnull align 4 dereferenceable(16) %contactPoint)
  %call44 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %contactToCentre)
  store float %call44, float* %distanceSqr43, align 4
  %40 = load float, float* %distanceSqr43, align 4
  %41 = load float, float* %radiusWithThreshold, align 4
  %42 = load float, float* %radiusWithThreshold, align 4
  %mul45 = fmul float %41, %42
  %cmp46 = fcmp olt float %40, %mul45
  br i1 %cmp46, label %if.then47, label %if.end55

if.then47:                                        ; preds = %if.then42
  %43 = load float, float* %distanceSqr43, align 4
  %cmp48 = fcmp ogt float %43, 0x3E80000000000000
  br i1 %cmp48, label %if.then49, label %if.else52

if.then49:                                        ; preds = %if.then47
  %44 = load float, float* %distanceSqr43, align 4
  %call50 = call float @_Z6btSqrtf(float %44)
  store float %call50, float* %distance, align 4
  %45 = load %class.btVector3*, %class.btVector3** %resultNormal.addr, align 4
  %46 = bitcast %class.btVector3* %45 to i8*
  %47 = bitcast %class.btVector3* %contactToCentre to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %46, i8* align 4 %47, i32 16, i1 false)
  %48 = load %class.btVector3*, %class.btVector3** %resultNormal.addr, align 4
  %call51 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %48)
  %49 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %50 = bitcast %class.btVector3* %49 to i8*
  %51 = bitcast %class.btVector3* %contactPoint to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %50, i8* align 4 %51, i32 16, i1 false)
  %52 = load float, float* %radius, align 4
  %53 = load float, float* %distance, align 4
  %sub = fsub float %52, %53
  %fneg = fneg float %sub
  %54 = load float*, float** %depth.addr, align 4
  store float %fneg, float* %54, align 4
  br label %if.end54

if.else52:                                        ; preds = %if.then47
  %55 = load %class.btVector3*, %class.btVector3** %resultNormal.addr, align 4
  %56 = bitcast %class.btVector3* %55 to i8*
  %57 = bitcast %class.btVector3* %normal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %56, i8* align 4 %57, i32 16, i1 false)
  %58 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %59 = bitcast %class.btVector3* %58 to i8*
  %60 = bitcast %class.btVector3* %contactPoint to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %59, i8* align 4 %60, i32 16, i1 false)
  %61 = load float, float* %radius, align 4
  %fneg53 = fneg float %61
  %62 = load float*, float** %depth.addr, align 4
  store float %fneg53, float* %62, align 4
  br label %if.end54

if.end54:                                         ; preds = %if.else52, %if.then49
  store i1 true, i1* %retval, align 1
  br label %return

if.end55:                                         ; preds = %if.then42
  br label %if.end56

if.end56:                                         ; preds = %if.end55, %if.end40
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end56, %if.end54
  %63 = load i1, i1* %retval, align 1
  ret i1 %63
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define hidden float @_Z18SegmentSqrDistanceRK9btVector3S1_S1_RS_(%class.btVector3* nonnull align 4 dereferenceable(16) %from, %class.btVector3* nonnull align 4 dereferenceable(16) %to, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* nonnull align 4 dereferenceable(16) %nearest) #2 {
entry:
  %from.addr = alloca %class.btVector3*, align 4
  %to.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %nearest.addr = alloca %class.btVector3*, align 4
  %diff = alloca %class.btVector3, align 4
  %v = alloca %class.btVector3, align 4
  %t = alloca float, align 4
  %dotVV = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  store %class.btVector3* %from, %class.btVector3** %from.addr, align 4
  store %class.btVector3* %to, %class.btVector3** %to.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store %class.btVector3* %nearest, %class.btVector3** %nearest.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %from.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %diff, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btVector3*, %class.btVector3** %to.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %from.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %v, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %v, %class.btVector3* nonnull align 4 dereferenceable(16) %diff)
  store float %call, float* %t, align 4
  %4 = load float, float* %t, align 4
  %cmp = fcmp ogt float %4, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else6

if.then:                                          ; preds = %entry
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %v, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  store float %call1, float* %dotVV, align 4
  %5 = load float, float* %t, align 4
  %6 = load float, float* %dotVV, align 4
  %cmp2 = fcmp olt float %5, %6
  br i1 %cmp2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.then
  %7 = load float, float* %dotVV, align 4
  %8 = load float, float* %t, align 4
  %div = fdiv float %8, %7
  store float %div, float* %t, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, float* nonnull align 4 dereferenceable(4) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %diff, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  br label %if.end

if.else:                                          ; preds = %if.then
  store float 1.000000e+00, float* %t, align 4
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %diff, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then3
  br label %if.end7

if.else6:                                         ; preds = %entry
  store float 0.000000e+00, float* %t, align 4
  br label %if.end7

if.end7:                                          ; preds = %if.else6, %if.end
  %9 = load %class.btVector3*, %class.btVector3** %from.addr, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp9, float* nonnull align 4 dereferenceable(4) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %10 = load %class.btVector3*, %class.btVector3** %nearest.addr, align 4
  %11 = bitcast %class.btVector3* %10 to i8*
  %12 = bitcast %class.btVector3* %ref.tmp8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false)
  %call10 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %diff, %class.btVector3* nonnull align 4 dereferenceable(16) %diff)
  ret float %call10
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN22SphereTriangleDetector12facecontainsERK9btVector3PS1_RS0_(%struct.SphereTriangleDetector* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %p, %class.btVector3* %vertices, %class.btVector3* nonnull align 4 dereferenceable(16) %normal) #2 {
entry:
  %this.addr = alloca %struct.SphereTriangleDetector*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %vertices.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %lp = alloca %class.btVector3, align 4
  %lnormal = alloca %class.btVector3, align 4
  store %struct.SphereTriangleDetector* %this, %struct.SphereTriangleDetector** %this.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  store %class.btVector3* %vertices, %class.btVector3** %vertices.addr, align 4
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4
  %this1 = load %struct.SphereTriangleDetector*, %struct.SphereTriangleDetector** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %1 = bitcast %class.btVector3* %lp to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %4 = bitcast %class.btVector3* %lnormal to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %call = call zeroext i1 @_ZN22SphereTriangleDetector15pointInTriangleEPK9btVector3RS1_PS0_(%struct.SphereTriangleDetector* %this1, %class.btVector3* %6, %class.btVector3* nonnull align 4 dereferenceable(16) %lnormal, %class.btVector3* %lp)
  ret i1 %call
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN22SphereTriangleDetector15pointInTriangleEPK9btVector3RS1_PS0_(%struct.SphereTriangleDetector* %this, %class.btVector3* %vertices, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, %class.btVector3* %p) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %struct.SphereTriangleDetector*, align 4
  %vertices.addr = alloca %class.btVector3*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %p.addr = alloca %class.btVector3*, align 4
  %p1 = alloca %class.btVector3*, align 4
  %p2 = alloca %class.btVector3*, align 4
  %p3 = alloca %class.btVector3*, align 4
  %edge1 = alloca %class.btVector3, align 4
  %edge2 = alloca %class.btVector3, align 4
  %edge3 = alloca %class.btVector3, align 4
  %p1_to_p = alloca %class.btVector3, align 4
  %p2_to_p = alloca %class.btVector3, align 4
  %p3_to_p = alloca %class.btVector3, align 4
  %edge1_normal = alloca %class.btVector3, align 4
  %edge2_normal = alloca %class.btVector3, align 4
  %edge3_normal = alloca %class.btVector3, align 4
  %r1 = alloca float, align 4
  %r2 = alloca float, align 4
  %r3 = alloca float, align 4
  store %struct.SphereTriangleDetector* %this, %struct.SphereTriangleDetector** %this.addr, align 4
  store %class.btVector3* %vertices, %class.btVector3** %vertices.addr, align 4
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4
  store %class.btVector3* %p, %class.btVector3** %p.addr, align 4
  %this1 = load %struct.SphereTriangleDetector*, %struct.SphereTriangleDetector** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0
  store %class.btVector3* %arrayidx, %class.btVector3** %p1, align 4
  %1 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 1
  store %class.btVector3* %arrayidx2, %class.btVector3** %p2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %vertices.addr, align 4
  %arrayidx3 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 2
  store %class.btVector3* %arrayidx3, %class.btVector3** %p3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %p2, align 4
  %4 = load %class.btVector3*, %class.btVector3** %p1, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge1, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %5 = load %class.btVector3*, %class.btVector3** %p3, align 4
  %6 = load %class.btVector3*, %class.btVector3** %p2, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge2, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  %7 = load %class.btVector3*, %class.btVector3** %p1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %p3, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge3, %class.btVector3* nonnull align 4 dereferenceable(16) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %9 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %10 = load %class.btVector3*, %class.btVector3** %p1, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %p1_to_p, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btVector3* nonnull align 4 dereferenceable(16) %10)
  %11 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %12 = load %class.btVector3*, %class.btVector3** %p2, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %p2_to_p, %class.btVector3* nonnull align 4 dereferenceable(16) %11, %class.btVector3* nonnull align 4 dereferenceable(16) %12)
  %13 = load %class.btVector3*, %class.btVector3** %p.addr, align 4
  %14 = load %class.btVector3*, %class.btVector3** %p3, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %p3_to_p, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %14)
  %15 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %edge1_normal, %class.btVector3* %edge1, %class.btVector3* nonnull align 4 dereferenceable(16) %15)
  %16 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %edge2_normal, %class.btVector3* %edge2, %class.btVector3* nonnull align 4 dereferenceable(16) %16)
  %17 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %edge3_normal, %class.btVector3* %edge3, %class.btVector3* nonnull align 4 dereferenceable(16) %17)
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %edge1_normal, %class.btVector3* nonnull align 4 dereferenceable(16) %p1_to_p)
  store float %call, float* %r1, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %edge2_normal, %class.btVector3* nonnull align 4 dereferenceable(16) %p2_to_p)
  store float %call4, float* %r2, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %edge3_normal, %class.btVector3* nonnull align 4 dereferenceable(16) %p3_to_p)
  store float %call5, float* %r3, align 4
  %18 = load float, float* %r1, align 4
  %cmp = fcmp ogt float %18, 0.000000e+00
  br i1 %cmp, label %land.lhs.true, label %lor.lhs.false

land.lhs.true:                                    ; preds = %entry
  %19 = load float, float* %r2, align 4
  %cmp6 = fcmp ogt float %19, 0.000000e+00
  br i1 %cmp6, label %land.lhs.true7, label %lor.lhs.false

land.lhs.true7:                                   ; preds = %land.lhs.true
  %20 = load float, float* %r3, align 4
  %cmp8 = fcmp ogt float %20, 0.000000e+00
  br i1 %cmp8, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true7, %land.lhs.true, %entry
  %21 = load float, float* %r1, align 4
  %cmp9 = fcmp ole float %21, 0.000000e+00
  br i1 %cmp9, label %land.lhs.true10, label %if.end

land.lhs.true10:                                  ; preds = %lor.lhs.false
  %22 = load float, float* %r2, align 4
  %cmp11 = fcmp ole float %22, 0.000000e+00
  br i1 %cmp11, label %land.lhs.true12, label %if.end

land.lhs.true12:                                  ; preds = %land.lhs.true10
  %23 = load float, float* %r3, align 4
  %cmp13 = fcmp ole float %23, 0.000000e+00
  br i1 %cmp13, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true12, %land.lhs.true7
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %land.lhs.true12, %land.lhs.true10, %lor.lhs.false
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %24 = load i1, i1* %retval, align 1
  ret i1 %24
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN15btTriangleShape12getVertexPtrEi(%class.btTriangleShape* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btTriangleShape* %this, %class.btTriangleShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btTriangleShape*, %class.btTriangleShape** %this.addr, align 4
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %index.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK13btSphereShape9getRadiusEv(%class.btSphereShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btSphereShape*, align 4
  store %class.btSphereShape* %this, %class.btSphereShape** %this.addr, align 4
  %this1 = load %class.btSphereShape*, %class.btSphereShape** %this.addr, align 4
  %0 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %0, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_implicitShapeDimensions)
  %1 = load float, float* %call, align 4
  %2 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %2, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling)
  %3 = load float, float* %call2, align 4
  %mul = fmul float %1, %3
  ret float %mul
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.SphereTriangleDetector* @_ZN22SphereTriangleDetectorD2Ev(%struct.SphereTriangleDetector* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.SphereTriangleDetector*, align 4
  store %struct.SphereTriangleDetector* %this, %struct.SphereTriangleDetector** %this.addr, align 4
  %this1 = load %struct.SphereTriangleDetector*, %struct.SphereTriangleDetector** %this.addr, align 4
  %0 = bitcast %struct.SphereTriangleDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #7
  ret %struct.SphereTriangleDetector* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN22SphereTriangleDetectorD0Ev(%struct.SphereTriangleDetector* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.SphereTriangleDetector*, align 4
  store %struct.SphereTriangleDetector* %this, %struct.SphereTriangleDetector** %this.addr, align 4
  %this1 = load %struct.SphereTriangleDetector*, %struct.SphereTriangleDetector** %this.addr, align 4
  %call = call %struct.SphereTriangleDetector* @_ZN22SphereTriangleDetectorD2Ev(%struct.SphereTriangleDetector* %this1) #7
  %0 = bitcast %struct.SphereTriangleDetector* %this1 to i8*
  call void @_ZdlPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN36btDiscreteCollisionDetectorInterfaceD0Ev(%struct.btDiscreteCollisionDetectorInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  call void @llvm.trap() #9
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x314transposeTimesERKS_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp59 = alloca float, align 4
  %ref.tmp80 = alloca float, align 4
  %ref.tmp101 = alloca float, align 4
  %ref.tmp122 = alloca float, align 4
  %ref.tmp143 = alloca float, align 4
  %ref.tmp164 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call2)
  %3 = load float, float* %call3, align 4
  %mul = fmul float %1, %3
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx5)
  %4 = load float, float* %call6, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %5, i32 1)
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call7)
  %6 = load float, float* %call8, align 4
  %mul9 = fmul float %4, %6
  %add = fadd float %mul, %mul9
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx11)
  %7 = load float, float* %call12, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 2)
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call13)
  %9 = load float, float* %call14, align 4
  %mul15 = fmul float %7, %9
  %add16 = fadd float %add, %mul15
  store float %add16, float* %ref.tmp, align 4
  %m_el18 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx19 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el18, i32 0, i32 0
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx19)
  %10 = load float, float* %call20, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %11, i32 0)
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call21)
  %12 = load float, float* %call22, align 4
  %mul23 = fmul float %10, %12
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx25)
  %13 = load float, float* %call26, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 1)
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call27)
  %15 = load float, float* %call28, align 4
  %mul29 = fmul float %13, %15
  %add30 = fadd float %mul23, %mul29
  %m_el31 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx32 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el31, i32 0, i32 2
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx32)
  %16 = load float, float* %call33, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %17, i32 2)
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call34)
  %18 = load float, float* %call35, align 4
  %mul36 = fmul float %16, %18
  %add37 = fadd float %add30, %mul36
  store float %add37, float* %ref.tmp17, align 4
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 0
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx40)
  %19 = load float, float* %call41, align 4
  %20 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call42 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %20, i32 0)
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call42)
  %21 = load float, float* %call43, align 4
  %mul44 = fmul float %19, %21
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx46)
  %22 = load float, float* %call47, align 4
  %23 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call48 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %23, i32 1)
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call48)
  %24 = load float, float* %call49, align 4
  %mul50 = fmul float %22, %24
  %add51 = fadd float %mul44, %mul50
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 2
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %25 = load float, float* %call54, align 4
  %26 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call55 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %26, i32 2)
  %call56 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call55)
  %27 = load float, float* %call56, align 4
  %mul57 = fmul float %25, %27
  %add58 = fadd float %add51, %mul57
  store float %add58, float* %ref.tmp38, align 4
  %m_el60 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx61 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el60, i32 0, i32 0
  %call62 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx61)
  %28 = load float, float* %call62, align 4
  %29 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call63 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %29, i32 0)
  %call64 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call63)
  %30 = load float, float* %call64, align 4
  %mul65 = fmul float %28, %30
  %m_el66 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx67 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el66, i32 0, i32 1
  %call68 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx67)
  %31 = load float, float* %call68, align 4
  %32 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call69 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %32, i32 1)
  %call70 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call69)
  %33 = load float, float* %call70, align 4
  %mul71 = fmul float %31, %33
  %add72 = fadd float %mul65, %mul71
  %m_el73 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx74 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el73, i32 0, i32 2
  %call75 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx74)
  %34 = load float, float* %call75, align 4
  %35 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call76 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %35, i32 2)
  %call77 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call76)
  %36 = load float, float* %call77, align 4
  %mul78 = fmul float %34, %36
  %add79 = fadd float %add72, %mul78
  store float %add79, float* %ref.tmp59, align 4
  %m_el81 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx82 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el81, i32 0, i32 0
  %call83 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx82)
  %37 = load float, float* %call83, align 4
  %38 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call84 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %38, i32 0)
  %call85 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call84)
  %39 = load float, float* %call85, align 4
  %mul86 = fmul float %37, %39
  %m_el87 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx88 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el87, i32 0, i32 1
  %call89 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx88)
  %40 = load float, float* %call89, align 4
  %41 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call90 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %41, i32 1)
  %call91 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call90)
  %42 = load float, float* %call91, align 4
  %mul92 = fmul float %40, %42
  %add93 = fadd float %mul86, %mul92
  %m_el94 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx95 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el94, i32 0, i32 2
  %call96 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx95)
  %43 = load float, float* %call96, align 4
  %44 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call97 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %44, i32 2)
  %call98 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call97)
  %45 = load float, float* %call98, align 4
  %mul99 = fmul float %43, %45
  %add100 = fadd float %add93, %mul99
  store float %add100, float* %ref.tmp80, align 4
  %m_el102 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx103 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el102, i32 0, i32 0
  %call104 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx103)
  %46 = load float, float* %call104, align 4
  %47 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call105 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %47, i32 0)
  %call106 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call105)
  %48 = load float, float* %call106, align 4
  %mul107 = fmul float %46, %48
  %m_el108 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx109 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el108, i32 0, i32 1
  %call110 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx109)
  %49 = load float, float* %call110, align 4
  %50 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call111 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %50, i32 1)
  %call112 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call111)
  %51 = load float, float* %call112, align 4
  %mul113 = fmul float %49, %51
  %add114 = fadd float %mul107, %mul113
  %m_el115 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx116 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el115, i32 0, i32 2
  %call117 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx116)
  %52 = load float, float* %call117, align 4
  %53 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call118 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %53, i32 2)
  %call119 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call118)
  %54 = load float, float* %call119, align 4
  %mul120 = fmul float %52, %54
  %add121 = fadd float %add114, %mul120
  store float %add121, float* %ref.tmp101, align 4
  %m_el123 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx124 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el123, i32 0, i32 0
  %call125 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx124)
  %55 = load float, float* %call125, align 4
  %56 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call126 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %56, i32 0)
  %call127 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call126)
  %57 = load float, float* %call127, align 4
  %mul128 = fmul float %55, %57
  %m_el129 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx130 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el129, i32 0, i32 1
  %call131 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx130)
  %58 = load float, float* %call131, align 4
  %59 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call132 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %59, i32 1)
  %call133 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call132)
  %60 = load float, float* %call133, align 4
  %mul134 = fmul float %58, %60
  %add135 = fadd float %mul128, %mul134
  %m_el136 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx137 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el136, i32 0, i32 2
  %call138 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx137)
  %61 = load float, float* %call138, align 4
  %62 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call139 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %62, i32 2)
  %call140 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %call139)
  %63 = load float, float* %call140, align 4
  %mul141 = fmul float %61, %63
  %add142 = fadd float %add135, %mul141
  store float %add142, float* %ref.tmp122, align 4
  %m_el144 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx145 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el144, i32 0, i32 0
  %call146 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx145)
  %64 = load float, float* %call146, align 4
  %65 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call147 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %65, i32 0)
  %call148 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call147)
  %66 = load float, float* %call148, align 4
  %mul149 = fmul float %64, %66
  %m_el150 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx151 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el150, i32 0, i32 1
  %call152 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx151)
  %67 = load float, float* %call152, align 4
  %68 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call153 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %68, i32 1)
  %call154 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call153)
  %69 = load float, float* %call154, align 4
  %mul155 = fmul float %67, %69
  %add156 = fadd float %mul149, %mul155
  %m_el157 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx158 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el157, i32 0, i32 2
  %call159 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx158)
  %70 = load float, float* %call159, align 4
  %71 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call160 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %71, i32 2)
  %call161 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %call160)
  %72 = load float, float* %call161, align 4
  %mul162 = fmul float %70, %72
  %add163 = fadd float %add156, %mul162
  store float %add163, float* %ref.tmp143, align 4
  %m_el165 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx166 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el165, i32 0, i32 0
  %call167 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx166)
  %73 = load float, float* %call167, align 4
  %74 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call168 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %74, i32 0)
  %call169 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call168)
  %75 = load float, float* %call169, align 4
  %mul170 = fmul float %73, %75
  %m_el171 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx172 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el171, i32 0, i32 1
  %call173 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx172)
  %76 = load float, float* %call173, align 4
  %77 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call174 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %77, i32 1)
  %call175 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call174)
  %78 = load float, float* %call175, align 4
  %mul176 = fmul float %76, %78
  %add177 = fadd float %mul170, %mul176
  %m_el178 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx179 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el178, i32 0, i32 2
  %call180 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx179)
  %79 = load float, float* %call180, align 4
  %80 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call181 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %80, i32 2)
  %call182 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %call181)
  %81 = load float, float* %call182, align 4
  %mul183 = fmul float %79, %81
  %add184 = fadd float %add177, %mul183
  store float %add184, float* %ref.tmp164, align 4
  %call185 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp59, float* nonnull align 4 dereferenceable(4) %ref.tmp80, float* nonnull align 4 dereferenceable(4) %ref.tmp101, float* nonnull align 4 dereferenceable(4) %ref.tmp122, float* nonnull align 4 dereferenceable(4) %ref.tmp143, float* nonnull align 4 dereferenceable(4) %ref.tmp164)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call2, float* %ref.tmp1, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp3, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_SphereTriangleDetector.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { cold noreturn nounwind }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }
attributes #8 = { builtin nounwind }
attributes #9 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
