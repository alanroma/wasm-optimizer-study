; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btSimulationIslandManager.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btSimulationIslandManager.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btSimulationIslandManager = type <{ i32 (...)**, %class.btUnionFind, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.10, i8, [3 x i8] }>
%class.btUnionFind = type { %class.btAlignedObjectArray }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btElement*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btElement = type { i32, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.3, %union.anon.4, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.3 = type { float }
%union.anon.4 = type { float }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.5, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray.5 = type <{ %class.btAlignedAllocator.6, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.6 = type { i8 }
%class.btAlignedObjectArray.10 = type <{ %class.btAlignedAllocator.11, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.11 = type { i8 }
%class.btDispatcher = type { i32 (...)** }
%class.btCollisionWorld = type <{ i32 (...)**, %class.btAlignedObjectArray.10, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8, [3 x i8] }>
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btIDebugDraw = type opaque
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btOverlappingPairCallback = type { i32 (...)** }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.14 }
%class.btCollisionAlgorithm = type opaque
%union.anon.14 = type { i8* }
%class.CProfileSample = type { i8 }
%"struct.btSimulationIslandManager::IslandCallback" = type { i32 (...)** }
%class.btPersistentManifoldSortPredicate = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev = comdat any

$_ZN16btCollisionWorld12getPairCacheEv = comdat any

$_ZNK17btCollisionObject23mergesSimulationIslandsEv = comdat any

$_ZN11btUnionFind5uniteEii = comdat any

$_ZNK17btCollisionObject12getIslandTagEv = comdat any

$_ZN16btCollisionWorld23getCollisionObjectArrayEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi = comdat any

$_ZNK17btCollisionObject25isStaticOrKinematicObjectEv = comdat any

$_ZN17btCollisionObject12setIslandTagEi = comdat any

$_ZN17btCollisionObject14setCompanionIdEi = comdat any

$_ZN17btCollisionObject14setHitFractionEf = comdat any

$_ZN11btUnionFind4findEi = comdat any

$_ZN11btUnionFind10getElementEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_ = comdat any

$_ZN25btSimulationIslandManager12getUnionFindEv = comdat any

$_ZNK11btUnionFind14getNumElementsEv = comdat any

$_ZNK17btCollisionObject18getActivationStateEv = comdat any

$_ZN17btCollisionObject19setDeactivationTimeEf = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK20btPersistentManifold8getBody1Ev = comdat any

$_ZNK17btCollisionObject17isKinematicObjectEv = comdat any

$_ZNK17btCollisionObject18hasContactResponseEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9quickSortI33btPersistentManifoldSortPredicateEEvRKT_ = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_ = comdat any

$_ZNK17btCollisionObject8isActiveEv = comdat any

$_Z11getIslandIdPK20btPersistentManifold = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayI9btElementEixEi = comdat any

$_ZNK20btAlignedObjectArrayI9btElementE4sizeEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvRKT_ii = comdat any

$_ZNK33btPersistentManifoldSortPredicateclEPK20btPersistentManifoldS2_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4swapEii = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV25btSimulationIslandManager = hidden unnamed_addr constant { [6 x i8*] } { [6 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI25btSimulationIslandManager to i8*), i8* bitcast (%class.btSimulationIslandManager* (%class.btSimulationIslandManager*)* @_ZN25btSimulationIslandManagerD1Ev to i8*), i8* bitcast (void (%class.btSimulationIslandManager*)* @_ZN25btSimulationIslandManagerD0Ev to i8*), i8* bitcast (void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)* @_ZN25btSimulationIslandManager21updateActivationStateEP16btCollisionWorldP12btDispatcher to i8*), i8* bitcast (void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)* @_ZN25btSimulationIslandManager26storeIslandActivationStateEP16btCollisionWorld to i8*)] }, align 4
@.str = private unnamed_addr constant [28 x i8] c"islandUnionFindAndQuickSort\00", align 1
@.str.1 = private unnamed_addr constant [15 x i8] c"processIslands\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS25btSimulationIslandManager = hidden constant [28 x i8] c"25btSimulationIslandManager\00", align 1
@_ZTI25btSimulationIslandManager = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([28 x i8], [28 x i8]* @_ZTS25btSimulationIslandManager, i32 0, i32 0) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btSimulationIslandManager.cpp, i8* null }]

@_ZN25btSimulationIslandManagerC1Ev = hidden unnamed_addr alias %class.btSimulationIslandManager* (%class.btSimulationIslandManager*), %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)* @_ZN25btSimulationIslandManagerC2Ev
@_ZN25btSimulationIslandManagerD1Ev = hidden unnamed_addr alias %class.btSimulationIslandManager* (%class.btSimulationIslandManager*), %class.btSimulationIslandManager* (%class.btSimulationIslandManager*)* @_ZN25btSimulationIslandManagerD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btSimulationIslandManager* @_ZN25btSimulationIslandManagerC2Ev(%class.btSimulationIslandManager* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %0 = bitcast %class.btSimulationIslandManager* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTV25btSimulationIslandManager, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  %call = call %class.btUnionFind* @_ZN11btUnionFindC1Ev(%class.btUnionFind* %m_unionFind)
  %m_islandmanifold = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.0* %m_islandmanifold)
  %m_islandBodies = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 3
  %call3 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev(%class.btAlignedObjectArray.10* %m_islandBodies)
  %m_splitIslands = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 4
  store i8 1, i8* %m_splitIslands, align 4
  ret %class.btSimulationIslandManager* %this1
}

declare %class.btUnionFind* @_ZN11btUnionFindC1Ev(%class.btUnionFind* returned) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev(%class.btAlignedObjectArray.10* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.11* @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator.11* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray.10* %this1)
  ret %class.btAlignedObjectArray.10* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btSimulationIslandManager* @_ZN25btSimulationIslandManagerD2Ev(%class.btSimulationIslandManager* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %0 = bitcast %class.btSimulationIslandManager* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [6 x i8*] }, { [6 x i8*] }* @_ZTV25btSimulationIslandManager, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_islandBodies = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 3
  %call = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev(%class.btAlignedObjectArray.10* %m_islandBodies) #6
  %m_islandmanifold = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.0* %m_islandmanifold) #6
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  %call3 = call %class.btUnionFind* @_ZN11btUnionFindD1Ev(%class.btUnionFind* %m_unionFind) #6
  ret %class.btSimulationIslandManager* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev(%class.btAlignedObjectArray.10* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv(%class.btAlignedObjectArray.10* %this1)
  ret %class.btAlignedObjectArray.10* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
declare %class.btUnionFind* @_ZN11btUnionFindD1Ev(%class.btUnionFind* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN25btSimulationIslandManagerD0Ev(%class.btSimulationIslandManager* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %call = call %class.btSimulationIslandManager* @_ZN25btSimulationIslandManagerD1Ev(%class.btSimulationIslandManager* %this1) #6
  %0 = bitcast %class.btSimulationIslandManager* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline optnone
define hidden void @_ZN25btSimulationIslandManager13initUnionFindEi(%class.btSimulationIslandManager* %this, i32 %n) #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  %n.addr = alloca i32, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  %0 = load i32, i32* %n.addr, align 4
  call void @_ZN11btUnionFind5resetEi(%class.btUnionFind* %m_unionFind, i32 %0)
  ret void
}

declare void @_ZN11btUnionFind5resetEi(%class.btUnionFind*, i32) #3

; Function Attrs: noinline optnone
define hidden void @_ZN25btSimulationIslandManager10findUnionsEP12btDispatcherP16btCollisionWorld(%class.btSimulationIslandManager* %this, %class.btDispatcher* %0, %class.btCollisionWorld* %colWorld) #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %colWorld.addr = alloca %class.btCollisionWorld*, align 4
  %pairCachePtr = alloca %class.btOverlappingPairCache*, align 4
  %numOverlappingPairs = alloca i32, align 4
  %pairPtr = alloca %struct.btBroadphasePair*, align 4
  %i = alloca i32, align 4
  %collisionPair = alloca %struct.btBroadphasePair*, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %colObj1 = alloca %class.btCollisionObject*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4
  store %class.btCollisionWorld* %colWorld, %class.btCollisionWorld** %colWorld.addr, align 4
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %colWorld.addr, align 4
  %call = call %class.btOverlappingPairCache* @_ZN16btCollisionWorld12getPairCacheEv(%class.btCollisionWorld* %1)
  store %class.btOverlappingPairCache* %call, %class.btOverlappingPairCache** %pairCachePtr, align 4
  %2 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCachePtr, align 4
  %3 = bitcast %class.btOverlappingPairCache* %2 to i32 (%class.btOverlappingPairCache*)***
  %vtable = load i32 (%class.btOverlappingPairCache*)**, i32 (%class.btOverlappingPairCache*)*** %3, align 4
  %vfn = getelementptr inbounds i32 (%class.btOverlappingPairCache*)*, i32 (%class.btOverlappingPairCache*)** %vtable, i64 9
  %4 = load i32 (%class.btOverlappingPairCache*)*, i32 (%class.btOverlappingPairCache*)** %vfn, align 4
  %call2 = call i32 %4(%class.btOverlappingPairCache* %2)
  store i32 %call2, i32* %numOverlappingPairs, align 4
  %5 = load i32, i32* %numOverlappingPairs, align 4
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %if.then, label %if.end16

if.then:                                          ; preds = %entry
  %6 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCachePtr, align 4
  %7 = bitcast %class.btOverlappingPairCache* %6 to %struct.btBroadphasePair* (%class.btOverlappingPairCache*)***
  %vtable3 = load %struct.btBroadphasePair* (%class.btOverlappingPairCache*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCache*)*** %7, align 4
  %vfn4 = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCache*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCache*)** %vtable3, i64 5
  %8 = load %struct.btBroadphasePair* (%class.btOverlappingPairCache*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCache*)** %vfn4, align 4
  %call5 = call %struct.btBroadphasePair* %8(%class.btOverlappingPairCache* %6)
  store %struct.btBroadphasePair* %call5, %struct.btBroadphasePair** %pairPtr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %9 = load i32, i32* %i, align 4
  %10 = load i32, i32* %numOverlappingPairs, align 4
  %cmp = icmp slt i32 %9, %10
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %11 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pairPtr, align 4
  %12 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %11, i32 %12
  store %struct.btBroadphasePair* %arrayidx, %struct.btBroadphasePair** %collisionPair, align 4
  %13 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %13, i32 0, i32 0
  %14 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %14, i32 0, i32 0
  %15 = load i8*, i8** %m_clientObject, align 4
  %16 = bitcast i8* %15 to %class.btCollisionObject*
  store %class.btCollisionObject* %16, %class.btCollisionObject** %colObj0, align 4
  %17 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %17, i32 0, i32 1
  %18 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_clientObject6 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %18, i32 0, i32 0
  %19 = load i8*, i8** %m_clientObject6, align 4
  %20 = bitcast i8* %19 to %class.btCollisionObject*
  store %class.btCollisionObject* %20, %class.btCollisionObject** %colObj1, align 4
  %21 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %tobool7 = icmp ne %class.btCollisionObject* %21, null
  br i1 %tobool7, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %22 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call8 = call zeroext i1 @_ZNK17btCollisionObject23mergesSimulationIslandsEv(%class.btCollisionObject* %22)
  br i1 %call8, label %land.lhs.true9, label %if.end

land.lhs.true9:                                   ; preds = %land.lhs.true
  %23 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %tobool10 = icmp ne %class.btCollisionObject* %23, null
  br i1 %tobool10, label %land.lhs.true11, label %if.end

land.lhs.true11:                                  ; preds = %land.lhs.true9
  %24 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call12 = call zeroext i1 @_ZNK17btCollisionObject23mergesSimulationIslandsEv(%class.btCollisionObject* %24)
  br i1 %call12, label %if.then13, label %if.end

if.then13:                                        ; preds = %land.lhs.true11
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  %25 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call14 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %25)
  %26 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call15 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %26)
  call void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %m_unionFind, i32 %call14, i32 %call15)
  br label %if.end

if.end:                                           ; preds = %if.then13, %land.lhs.true11, %land.lhs.true9, %land.lhs.true, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %27 = load i32, i32* %i, align 4
  %inc = add nsw i32 %27, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end16

if.end16:                                         ; preds = %for.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btOverlappingPairCache* @_ZN16btCollisionWorld12getPairCacheEv(%class.btCollisionWorld* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_broadphasePairCache = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 4
  %0 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %m_broadphasePairCache, align 4
  %1 = bitcast %class.btBroadphaseInterface* %0 to %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)***
  %vtable = load %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)**, %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)*** %1, align 4
  %vfn = getelementptr inbounds %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)*, %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)** %vtable, i64 9
  %2 = load %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)*, %class.btOverlappingPairCache* (%class.btBroadphaseInterface*)** %vfn, align 4
  %call = call %class.btOverlappingPairCache* %2(%class.btBroadphaseInterface* %0)
  ret %class.btOverlappingPairCache* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject23mergesSimulationIslandsEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 7
  %cmp = icmp eq i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btUnionFind5uniteEii(%class.btUnionFind* %this, i32 %p, i32 %q) #2 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %p.addr = alloca i32, align 4
  %q.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  store i32 %p, i32* %p.addr, align 4
  store i32 %q, i32* %q.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %0 = load i32, i32* %p.addr, align 4
  %call = call i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this1, i32 %0)
  store i32 %call, i32* %i, align 4
  %1 = load i32, i32* %q.addr, align 4
  %call2 = call i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this1, i32 %1)
  store i32 %call2, i32* %j, align 4
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %j, align 4
  %cmp = icmp eq i32 %2, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %4 = load i32, i32* %j, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %5 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements, i32 %5)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call3, i32 0, i32 0
  store i32 %4, i32* %m_id, align 4
  %m_elements4 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %6 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements4, i32 %6)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call5, i32 0, i32 1
  %7 = load i32, i32* %m_sz, align 4
  %m_elements6 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %8 = load i32, i32* %j, align 4
  %call7 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements6, i32 %8)
  %m_sz8 = getelementptr inbounds %struct.btElement, %struct.btElement* %call7, i32 0, i32 1
  %9 = load i32, i32* %m_sz8, align 4
  %add = add nsw i32 %9, %7
  store i32 %add, i32* %m_sz8, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  %0 = load i32, i32* %m_islandTag1, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN25btSimulationIslandManager21updateActivationStateEP16btCollisionWorldP12btDispatcher(%class.btSimulationIslandManager* %this, %class.btCollisionWorld* %colWorld, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  %colWorld.addr = alloca %class.btCollisionWorld*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  %collisionObject = alloca %class.btCollisionObject*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4
  store %class.btCollisionWorld* %colWorld, %class.btCollisionWorld** %colWorld.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  store i32 0, i32* %index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %colWorld.addr, align 4
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %1)
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %call)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btCollisionWorld*, %class.btCollisionWorld** %colWorld.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %2)
  %3 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %call3, i32 %3)
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %call4, align 4
  store %class.btCollisionObject* %4, %class.btCollisionObject** %collisionObject, align 4
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4
  %call5 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %5)
  br i1 %call5, label %if.end, label %if.then

if.then:                                          ; preds = %for.body
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4
  %7 = load i32, i32* %index, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %index, align 4
  call void @_ZN17btCollisionObject12setIslandTagEi(%class.btCollisionObject* %6, i32 %7)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %8 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4
  call void @_ZN17btCollisionObject14setCompanionIdEi(%class.btCollisionObject* %8, i32 -1)
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4
  call void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %9, float 1.000000e+00)
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %i, align 4
  %inc6 = add nsw i32 %10, 1
  store i32 %inc6, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %11 = load i32, i32* %index, align 4
  call void @_ZN25btSimulationIslandManager13initUnionFindEi(%class.btSimulationIslandManager* %this1, i32 %11)
  %12 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %13 = load %class.btCollisionWorld*, %class.btCollisionWorld** %colWorld.addr, align 4
  call void @_ZN25btSimulationIslandManager10findUnionsEP12btDispatcherP16btCollisionWorld(%class.btSimulationIslandManager* %this1, %class.btDispatcher* %12, %class.btCollisionWorld* %13)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray.10* %m_collisionObjects
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %0, i32 %1
  ret %class.btCollisionObject** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 3
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btCollisionObject12setIslandTagEi(%class.btCollisionObject* %this, i32 %tag) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %tag.addr = alloca i32, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store i32 %tag, i32* %tag.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load i32, i32* %tag.addr, align 4
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  store i32 %0, i32* %m_islandTag1, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btCollisionObject14setCompanionIdEi(%class.btCollisionObject* %this, i32 %id) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %id.addr = alloca i32, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store i32 %id, i32* %id.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load i32, i32* %id.addr, align 4
  %m_companionId = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 14
  store i32 %0, i32* %m_companionId, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btCollisionObject14setHitFractionEf(%class.btCollisionObject* %this, float %hitFraction) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %hitFraction.addr = alloca float, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store float %hitFraction, float* %hitFraction.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load float, float* %hitFraction.addr, align 4
  %m_hitFraction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 28
  store float %0, float* %m_hitFraction, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN25btSimulationIslandManager26storeIslandActivationStateEP16btCollisionWorld(%class.btSimulationIslandManager* %this, %class.btCollisionWorld* %colWorld) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  %colWorld.addr = alloca %class.btCollisionWorld*, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  %collisionObject = alloca %class.btCollisionObject*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4
  store %class.btCollisionWorld* %colWorld, %class.btCollisionWorld** %colWorld.addr, align 4
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  store i32 0, i32* %index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %colWorld.addr, align 4
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %1)
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %call)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btCollisionWorld*, %class.btCollisionWorld** %colWorld.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %2)
  %3 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %call3, i32 %3)
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %call4, align 4
  store %class.btCollisionObject* %4, %class.btCollisionObject** %collisionObject, align 4
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4
  %call5 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %5)
  br i1 %call5, label %if.else, label %if.then

if.then:                                          ; preds = %for.body
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  %7 = load i32, i32* %index, align 4
  %call6 = call i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %m_unionFind, i32 %7)
  call void @_ZN17btCollisionObject12setIslandTagEi(%class.btCollisionObject* %6, i32 %call6)
  %8 = load i32, i32* %i, align 4
  %m_unionFind7 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  %9 = load i32, i32* %index, align 4
  %call8 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %m_unionFind7, i32 %9)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call8, i32 0, i32 1
  store i32 %8, i32* %m_sz, align 4
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4
  call void @_ZN17btCollisionObject14setCompanionIdEi(%class.btCollisionObject* %10, i32 -1)
  %11 = load i32, i32* %index, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %index, align 4
  br label %if.end

if.else:                                          ; preds = %for.body
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4
  call void @_ZN17btCollisionObject12setIslandTagEi(%class.btCollisionObject* %12, i32 -1)
  %13 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4
  call void @_ZN17btCollisionObject14setCompanionIdEi(%class.btCollisionObject* %13, i32 -2)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %14 = load i32, i32* %i, align 4
  %inc9 = add nsw i32 %14, 1
  store i32 %inc9, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN11btUnionFind4findEi(%class.btUnionFind* %this, i32 %x) #1 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %x.addr = alloca i32, align 4
  %elementPtr = alloca %struct.btElement*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  store i32 %x, i32* %x.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %x.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %1 = load i32, i32* %x.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements, i32 %1)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call, i32 0, i32 0
  %2 = load i32, i32* %m_id, align 4
  %cmp = icmp ne i32 %0, %2
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_elements2 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %m_elements3 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %3 = load i32, i32* %x.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements3, i32 %3)
  %m_id5 = getelementptr inbounds %struct.btElement, %struct.btElement* %call4, i32 0, i32 0
  %4 = load i32, i32* %m_id5, align 4
  %call6 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements2, i32 %4)
  store %struct.btElement* %call6, %struct.btElement** %elementPtr, align 4
  %5 = load %struct.btElement*, %struct.btElement** %elementPtr, align 4
  %m_id7 = getelementptr inbounds %struct.btElement, %struct.btElement* %5, i32 0, i32 0
  %6 = load i32, i32* %m_id7, align 4
  %m_elements8 = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %7 = load i32, i32* %x.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements8, i32 %7)
  %m_id10 = getelementptr inbounds %struct.btElement, %struct.btElement* %call9, i32 0, i32 0
  store i32 %6, i32* %m_id10, align 4
  %8 = load %struct.btElement*, %struct.btElement** %elementPtr, align 4
  %m_id11 = getelementptr inbounds %struct.btElement, %struct.btElement* %8, i32 0, i32 0
  %9 = load i32, i32* %m_id11, align 4
  store i32 %9, i32* %x.addr, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %10 = load i32, i32* %x.addr, align 4
  ret i32 %10
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %this, i32 %index) #1 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %index.addr = alloca i32, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements, i32 %0)
  ret %struct.btElement* %call
}

; Function Attrs: noinline optnone
define hidden void @_ZN25btSimulationIslandManager12buildIslandsEP12btDispatcherP16btCollisionWorld(%class.btSimulationIslandManager* %this, %class.btDispatcher* %dispatcher, %class.btCollisionWorld* %collisionWorld) #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %collisionObjects = alloca %class.btAlignedObjectArray.10*, align 4
  %ref.tmp = alloca %class.btPersistentManifold*, align 4
  %numElem = alloca i32, align 4
  %endIslandIndex = alloca i32, align 4
  %startIslandIndex = alloca i32, align 4
  %islandId = alloca i32, align 4
  %allSleeping = alloca i8, align 1
  %idx = alloca i32, align 4
  %i = alloca i32, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %idx41 = alloca i32, align 4
  %i45 = alloca i32, align 4
  %colObj049 = alloca %class.btCollisionObject*, align 4
  %idx65 = alloca i32, align 4
  %i69 = alloca i32, align 4
  %colObj073 = alloca %class.btCollisionObject*, align 4
  %i96 = alloca i32, align 4
  %maxNumManifolds = alloca i32, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  %colObj0104 = alloca %class.btCollisionObject*, align 4
  %colObj1 = alloca %class.btCollisionObject*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str, i32 0, i32 0))
  %0 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %0)
  store %class.btAlignedObjectArray.10* %call2, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %m_islandmanifold = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.0* %m_islandmanifold, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp)
  %call3 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  call void @_ZN11btUnionFind11sortIslandsEv(%class.btUnionFind* %call3)
  %call4 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %call5 = call i32 @_ZNK11btUnionFind14getNumElementsEv(%class.btUnionFind* %call4)
  store i32 %call5, i32* %numElem, align 4
  store i32 1, i32* %endIslandIndex, align 4
  store i32 0, i32* %startIslandIndex, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc94, %entry
  %1 = load i32, i32* %startIslandIndex, align 4
  %2 = load i32, i32* %numElem, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end95

for.body:                                         ; preds = %for.cond
  %call6 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %3 = load i32, i32* %startIslandIndex, align 4
  %call7 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call6, i32 %3)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call7, i32 0, i32 0
  %4 = load i32, i32* %m_id, align 4
  store i32 %4, i32* %islandId, align 4
  %5 = load i32, i32* %startIslandIndex, align 4
  %add = add nsw i32 %5, 1
  store i32 %add, i32* %endIslandIndex, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %6 = load i32, i32* %endIslandIndex, align 4
  %7 = load i32, i32* %numElem, align 4
  %cmp9 = icmp slt i32 %6, %7
  br i1 %cmp9, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond8
  %call10 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %8 = load i32, i32* %endIslandIndex, align 4
  %call11 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call10, i32 %8)
  %m_id12 = getelementptr inbounds %struct.btElement, %struct.btElement* %call11, i32 0, i32 0
  %9 = load i32, i32* %m_id12, align 4
  %10 = load i32, i32* %islandId, align 4
  %cmp13 = icmp eq i32 %9, %10
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond8
  %11 = phi i1 [ false, %for.cond8 ], [ %cmp13, %land.rhs ]
  br i1 %11, label %for.body14, label %for.end

for.body14:                                       ; preds = %land.end
  br label %for.inc

for.inc:                                          ; preds = %for.body14
  %12 = load i32, i32* %endIslandIndex, align 4
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %endIslandIndex, align 4
  br label %for.cond8

for.end:                                          ; preds = %land.end
  store i8 1, i8* %allSleeping, align 1
  %13 = load i32, i32* %startIslandIndex, align 4
  store i32 %13, i32* %idx, align 4
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc37, %for.end
  %14 = load i32, i32* %idx, align 4
  %15 = load i32, i32* %endIslandIndex, align 4
  %cmp16 = icmp slt i32 %14, %15
  br i1 %cmp16, label %for.body17, label %for.end39

for.body17:                                       ; preds = %for.cond15
  %call18 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %16 = load i32, i32* %idx, align 4
  %call19 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call18, i32 %16)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call19, i32 0, i32 1
  %17 = load i32, i32* %m_sz, align 4
  store i32 %17, i32* %i, align 4
  %18 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %19 = load i32, i32* %i, align 4
  %call20 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %18, i32 %19)
  %20 = load %class.btCollisionObject*, %class.btCollisionObject** %call20, align 4
  store %class.btCollisionObject* %20, %class.btCollisionObject** %colObj0, align 4
  %21 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call21 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %21)
  %22 = load i32, i32* %islandId, align 4
  %cmp22 = icmp ne i32 %call21, %22
  br i1 %cmp22, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body17
  %23 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call23 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %23)
  %cmp24 = icmp ne i32 %call23, -1
  br i1 %cmp24, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body17
  %24 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call25 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %24)
  %25 = load i32, i32* %islandId, align 4
  %cmp26 = icmp eq i32 %call25, %25
  br i1 %cmp26, label %if.then27, label %if.end36

if.then27:                                        ; preds = %if.end
  %26 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call28 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %26)
  %cmp29 = icmp eq i32 %call28, 1
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then27
  store i8 0, i8* %allSleeping, align 1
  br label %if.end31

if.end31:                                         ; preds = %if.then30, %if.then27
  %27 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call32 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %27)
  %cmp33 = icmp eq i32 %call32, 4
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.end31
  store i8 0, i8* %allSleeping, align 1
  br label %if.end35

if.end35:                                         ; preds = %if.then34, %if.end31
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %if.end
  br label %for.inc37

for.inc37:                                        ; preds = %if.end36
  %28 = load i32, i32* %idx, align 4
  %inc38 = add nsw i32 %28, 1
  store i32 %inc38, i32* %idx, align 4
  br label %for.cond15

for.end39:                                        ; preds = %for.cond15
  %29 = load i8, i8* %allSleeping, align 1
  %tobool = trunc i8 %29 to i1
  br i1 %tobool, label %if.then40, label %if.else

if.then40:                                        ; preds = %for.end39
  %30 = load i32, i32* %startIslandIndex, align 4
  store i32 %30, i32* %idx41, align 4
  br label %for.cond42

for.cond42:                                       ; preds = %for.inc62, %if.then40
  %31 = load i32, i32* %idx41, align 4
  %32 = load i32, i32* %endIslandIndex, align 4
  %cmp43 = icmp slt i32 %31, %32
  br i1 %cmp43, label %for.body44, label %for.end64

for.body44:                                       ; preds = %for.cond42
  %call46 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %33 = load i32, i32* %idx41, align 4
  %call47 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call46, i32 %33)
  %m_sz48 = getelementptr inbounds %struct.btElement, %struct.btElement* %call47, i32 0, i32 1
  %34 = load i32, i32* %m_sz48, align 4
  store i32 %34, i32* %i45, align 4
  %35 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %36 = load i32, i32* %i45, align 4
  %call50 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %35, i32 %36)
  %37 = load %class.btCollisionObject*, %class.btCollisionObject** %call50, align 4
  store %class.btCollisionObject* %37, %class.btCollisionObject** %colObj049, align 4
  %38 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj049, align 4
  %call51 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %38)
  %39 = load i32, i32* %islandId, align 4
  %cmp52 = icmp ne i32 %call51, %39
  br i1 %cmp52, label %land.lhs.true53, label %if.end57

land.lhs.true53:                                  ; preds = %for.body44
  %40 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj049, align 4
  %call54 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %40)
  %cmp55 = icmp ne i32 %call54, -1
  br i1 %cmp55, label %if.then56, label %if.end57

if.then56:                                        ; preds = %land.lhs.true53
  br label %if.end57

if.end57:                                         ; preds = %if.then56, %land.lhs.true53, %for.body44
  %41 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj049, align 4
  %call58 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %41)
  %42 = load i32, i32* %islandId, align 4
  %cmp59 = icmp eq i32 %call58, %42
  br i1 %cmp59, label %if.then60, label %if.end61

if.then60:                                        ; preds = %if.end57
  %43 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj049, align 4
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %43, i32 2)
  br label %if.end61

if.end61:                                         ; preds = %if.then60, %if.end57
  br label %for.inc62

for.inc62:                                        ; preds = %if.end61
  %44 = load i32, i32* %idx41, align 4
  %inc63 = add nsw i32 %44, 1
  store i32 %inc63, i32* %idx41, align 4
  br label %for.cond42

for.end64:                                        ; preds = %for.cond42
  br label %if.end93

if.else:                                          ; preds = %for.end39
  %45 = load i32, i32* %startIslandIndex, align 4
  store i32 %45, i32* %idx65, align 4
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc90, %if.else
  %46 = load i32, i32* %idx65, align 4
  %47 = load i32, i32* %endIslandIndex, align 4
  %cmp67 = icmp slt i32 %46, %47
  br i1 %cmp67, label %for.body68, label %for.end92

for.body68:                                       ; preds = %for.cond66
  %call70 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %48 = load i32, i32* %idx65, align 4
  %call71 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call70, i32 %48)
  %m_sz72 = getelementptr inbounds %struct.btElement, %struct.btElement* %call71, i32 0, i32 1
  %49 = load i32, i32* %m_sz72, align 4
  store i32 %49, i32* %i69, align 4
  %50 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %51 = load i32, i32* %i69, align 4
  %call74 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %50, i32 %51)
  %52 = load %class.btCollisionObject*, %class.btCollisionObject** %call74, align 4
  store %class.btCollisionObject* %52, %class.btCollisionObject** %colObj073, align 4
  %53 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4
  %call75 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %53)
  %54 = load i32, i32* %islandId, align 4
  %cmp76 = icmp ne i32 %call75, %54
  br i1 %cmp76, label %land.lhs.true77, label %if.end81

land.lhs.true77:                                  ; preds = %for.body68
  %55 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4
  %call78 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %55)
  %cmp79 = icmp ne i32 %call78, -1
  br i1 %cmp79, label %if.then80, label %if.end81

if.then80:                                        ; preds = %land.lhs.true77
  br label %if.end81

if.end81:                                         ; preds = %if.then80, %land.lhs.true77, %for.body68
  %56 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4
  %call82 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %56)
  %57 = load i32, i32* %islandId, align 4
  %cmp83 = icmp eq i32 %call82, %57
  br i1 %cmp83, label %if.then84, label %if.end89

if.then84:                                        ; preds = %if.end81
  %58 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4
  %call85 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %58)
  %cmp86 = icmp eq i32 %call85, 2
  br i1 %cmp86, label %if.then87, label %if.end88

if.then87:                                        ; preds = %if.then84
  %59 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %59, i32 3)
  %60 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4
  call void @_ZN17btCollisionObject19setDeactivationTimeEf(%class.btCollisionObject* %60, float 0.000000e+00)
  br label %if.end88

if.end88:                                         ; preds = %if.then87, %if.then84
  br label %if.end89

if.end89:                                         ; preds = %if.end88, %if.end81
  br label %for.inc90

for.inc90:                                        ; preds = %if.end89
  %61 = load i32, i32* %idx65, align 4
  %inc91 = add nsw i32 %61, 1
  store i32 %inc91, i32* %idx65, align 4
  br label %for.cond66

for.end92:                                        ; preds = %for.cond66
  br label %if.end93

if.end93:                                         ; preds = %for.end92, %for.end64
  br label %for.inc94

for.inc94:                                        ; preds = %if.end93
  %62 = load i32, i32* %endIslandIndex, align 4
  store i32 %62, i32* %startIslandIndex, align 4
  br label %for.cond

for.end95:                                        ; preds = %for.cond
  %63 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %64 = bitcast %class.btDispatcher* %63 to i32 (%class.btDispatcher*)***
  %vtable = load i32 (%class.btDispatcher*)**, i32 (%class.btDispatcher*)*** %64, align 4
  %vfn = getelementptr inbounds i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vtable, i64 9
  %65 = load i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vfn, align 4
  %call97 = call i32 %65(%class.btDispatcher* %63)
  store i32 %call97, i32* %maxNumManifolds, align 4
  store i32 0, i32* %i96, align 4
  br label %for.cond98

for.cond98:                                       ; preds = %for.inc144, %for.end95
  %66 = load i32, i32* %i96, align 4
  %67 = load i32, i32* %maxNumManifolds, align 4
  %cmp99 = icmp slt i32 %66, %67
  br i1 %cmp99, label %for.body100, label %for.end146

for.body100:                                      ; preds = %for.cond98
  %68 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %69 = load i32, i32* %i96, align 4
  %70 = bitcast %class.btDispatcher* %68 to %class.btPersistentManifold* (%class.btDispatcher*, i32)***
  %vtable101 = load %class.btPersistentManifold* (%class.btDispatcher*, i32)**, %class.btPersistentManifold* (%class.btDispatcher*, i32)*** %70, align 4
  %vfn102 = getelementptr inbounds %class.btPersistentManifold* (%class.btDispatcher*, i32)*, %class.btPersistentManifold* (%class.btDispatcher*, i32)** %vtable101, i64 10
  %71 = load %class.btPersistentManifold* (%class.btDispatcher*, i32)*, %class.btPersistentManifold* (%class.btDispatcher*, i32)** %vfn102, align 4
  %call103 = call %class.btPersistentManifold* %71(%class.btDispatcher* %68, i32 %69)
  store %class.btPersistentManifold* %call103, %class.btPersistentManifold** %manifold, align 4
  %72 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call105 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %72)
  store %class.btCollisionObject* %call105, %class.btCollisionObject** %colObj0104, align 4
  %73 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call106 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %73)
  store %class.btCollisionObject* %call106, %class.btCollisionObject** %colObj1, align 4
  %74 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0104, align 4
  %tobool107 = icmp ne %class.btCollisionObject* %74, null
  br i1 %tobool107, label %land.lhs.true108, label %lor.lhs.false

land.lhs.true108:                                 ; preds = %for.body100
  %75 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0104, align 4
  %call109 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %75)
  %cmp110 = icmp ne i32 %call109, 2
  br i1 %cmp110, label %if.then115, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true108, %for.body100
  %76 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %tobool111 = icmp ne %class.btCollisionObject* %76, null
  br i1 %tobool111, label %land.lhs.true112, label %if.end143

land.lhs.true112:                                 ; preds = %lor.lhs.false
  %77 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call113 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %77)
  %cmp114 = icmp ne i32 %call113, 2
  br i1 %cmp114, label %if.then115, label %if.end143

if.then115:                                       ; preds = %land.lhs.true112, %land.lhs.true108
  %78 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0104, align 4
  %call116 = call zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %78)
  br i1 %call116, label %land.lhs.true117, label %if.end124

land.lhs.true117:                                 ; preds = %if.then115
  %79 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0104, align 4
  %call118 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %79)
  %cmp119 = icmp ne i32 %call118, 2
  br i1 %cmp119, label %if.then120, label %if.end124

if.then120:                                       ; preds = %land.lhs.true117
  %80 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0104, align 4
  %call121 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %80)
  br i1 %call121, label %if.then122, label %if.end123

if.then122:                                       ; preds = %if.then120
  %81 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  call void @_ZNK17btCollisionObject8activateEb(%class.btCollisionObject* %81, i1 zeroext false)
  br label %if.end123

if.end123:                                        ; preds = %if.then122, %if.then120
  br label %if.end124

if.end124:                                        ; preds = %if.end123, %land.lhs.true117, %if.then115
  %82 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call125 = call zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %82)
  br i1 %call125, label %land.lhs.true126, label %if.end133

land.lhs.true126:                                 ; preds = %if.end124
  %83 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call127 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %83)
  %cmp128 = icmp ne i32 %call127, 2
  br i1 %cmp128, label %if.then129, label %if.end133

if.then129:                                       ; preds = %land.lhs.true126
  %84 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call130 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %84)
  br i1 %call130, label %if.then131, label %if.end132

if.then131:                                       ; preds = %if.then129
  %85 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0104, align 4
  call void @_ZNK17btCollisionObject8activateEb(%class.btCollisionObject* %85, i1 zeroext false)
  br label %if.end132

if.end132:                                        ; preds = %if.then131, %if.then129
  br label %if.end133

if.end133:                                        ; preds = %if.end132, %land.lhs.true126, %if.end124
  %m_splitIslands = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 4
  %86 = load i8, i8* %m_splitIslands, align 4
  %tobool134 = trunc i8 %86 to i1
  br i1 %tobool134, label %if.then135, label %if.end142

if.then135:                                       ; preds = %if.end133
  %87 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %88 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0104, align 4
  %89 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %90 = bitcast %class.btDispatcher* %87 to i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable136 = load i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*** %90, align 4
  %vfn137 = getelementptr inbounds i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable136, i64 7
  %91 = load i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn137, align 4
  %call138 = call zeroext i1 %91(%class.btDispatcher* %87, %class.btCollisionObject* %88, %class.btCollisionObject* %89)
  br i1 %call138, label %if.then139, label %if.end141

if.then139:                                       ; preds = %if.then135
  %m_islandmanifold140 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.0* %m_islandmanifold140, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %manifold)
  br label %if.end141

if.end141:                                        ; preds = %if.then139, %if.then135
  br label %if.end142

if.end142:                                        ; preds = %if.end141, %if.end133
  br label %if.end143

if.end143:                                        ; preds = %if.end142, %land.lhs.true112, %lor.lhs.false
  br label %for.inc144

for.inc144:                                       ; preds = %if.end143
  %92 = load i32, i32* %i96, align 4
  %inc145 = add nsw i32 %92, 1
  store i32 %inc145, i32* %i96, align 4
  br label %for.cond98

for.end146:                                       ; preds = %for.cond98
  %call147 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #6
  ret void
}

declare %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* returned, i8*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.0* %this, i32 %newsize, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btPersistentManifold**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btPersistentManifold** %fillData, %class.btPersistentManifold*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %14 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %14, i32 %15
  %16 = bitcast %class.btPersistentManifold** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btPersistentManifold**
  %18 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %fillData.addr, align 4
  %19 = load %class.btPersistentManifold*, %class.btPersistentManifold** %18, align 4
  store %class.btPersistentManifold* %19, %class.btPersistentManifold** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  ret %class.btUnionFind* %m_unionFind
}

declare void @_ZN11btUnionFind11sortIslandsEv(%class.btUnionFind*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK11btUnionFind14getNumElementsEv(%class.btUnionFind* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %m_elements)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  %0 = load i32, i32* %m_activationState1, align 4
  ret i32 %0
}

declare void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject*, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btCollisionObject19setDeactivationTimeEf(%class.btCollisionObject* %this, float %time) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %time.addr = alloca float, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store float %time, float* %time.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load float, float* %time.addr, align 4
  %m_deactivationTime = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 17
  store float %0, float* %m_deactivationTime, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body1 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 3
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body1, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 2
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 4
  %cmp = icmp eq i32 %and, 0
  ret i1 %cmp
}

declare void @_ZNK17btCollisionObject8activateEb(%class.btCollisionObject*, i1 zeroext) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.0* %this, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %class.btPersistentManifold**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store %class.btPersistentManifold** %_Val, %class.btPersistentManifold*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %1 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %1, i32 %2
  %3 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btPersistentManifold**
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %_Val.addr, align 4
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %5, align 4
  store %class.btPersistentManifold* %6, %class.btPersistentManifold** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: nounwind
declare %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* returned) unnamed_addr #4

; Function Attrs: noinline optnone
define hidden void @_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE(%class.btSimulationIslandManager* %this, %class.btDispatcher* %dispatcher, %class.btCollisionWorld* %collisionWorld, %"struct.btSimulationIslandManager::IslandCallback"* %callback) #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %callback.addr = alloca %"struct.btSimulationIslandManager::IslandCallback"*, align 4
  %collisionObjects = alloca %class.btAlignedObjectArray.10*, align 4
  %endIslandIndex = alloca i32, align 4
  %startIslandIndex = alloca i32, align 4
  %numElem = alloca i32, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %manifold = alloca %class.btPersistentManifold**, align 4
  %maxNumManifolds = alloca i32, align 4
  %numManifolds = alloca i32, align 4
  %ref.tmp = alloca %class.btPersistentManifoldSortPredicate, align 1
  %startManifoldIndex = alloca i32, align 4
  %endManifoldIndex = alloca i32, align 4
  %islandId = alloca i32, align 4
  %islandSleeping = alloca i8, align 1
  %i = alloca i32, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %numIslandManifolds = alloca i32, align 4
  %startManifold = alloca %class.btPersistentManifold**, align 4
  %curIslandId = alloca i32, align 4
  %ref.tmp65 = alloca %class.btCollisionObject*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4
  store %"struct.btSimulationIslandManager::IslandCallback"* %callback, %"struct.btSimulationIslandManager::IslandCallback"** %callback.addr, align 4
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %0 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %0)
  store %class.btAlignedObjectArray.10* %call, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %1 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %2 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  call void @_ZN25btSimulationIslandManager12buildIslandsEP12btDispatcherP16btCollisionWorld(%class.btSimulationIslandManager* %this1, %class.btDispatcher* %1, %class.btCollisionWorld* %2)
  store i32 1, i32* %endIslandIndex, align 4
  %call2 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %call3 = call i32 @_ZNK11btUnionFind14getNumElementsEv(%class.btUnionFind* %call2)
  store i32 %call3, i32* %numElem, align 4
  %call4 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.1, i32 0, i32 0))
  %m_splitIslands = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 4
  %3 = load i8, i8* %m_splitIslands, align 4
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %4 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %5 = bitcast %class.btDispatcher* %4 to %class.btPersistentManifold** (%class.btDispatcher*)***
  %vtable = load %class.btPersistentManifold** (%class.btDispatcher*)**, %class.btPersistentManifold** (%class.btDispatcher*)*** %5, align 4
  %vfn = getelementptr inbounds %class.btPersistentManifold** (%class.btDispatcher*)*, %class.btPersistentManifold** (%class.btDispatcher*)** %vtable, i64 11
  %6 = load %class.btPersistentManifold** (%class.btDispatcher*)*, %class.btPersistentManifold** (%class.btDispatcher*)** %vfn, align 4
  %call5 = call %class.btPersistentManifold** %6(%class.btDispatcher* %4)
  store %class.btPersistentManifold** %call5, %class.btPersistentManifold*** %manifold, align 4
  %7 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %8 = bitcast %class.btDispatcher* %7 to i32 (%class.btDispatcher*)***
  %vtable6 = load i32 (%class.btDispatcher*)**, i32 (%class.btDispatcher*)*** %8, align 4
  %vfn7 = getelementptr inbounds i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vtable6, i64 9
  %9 = load i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vfn7, align 4
  %call8 = call i32 %9(%class.btDispatcher* %7)
  store i32 %call8, i32* %maxNumManifolds, align 4
  %10 = load %"struct.btSimulationIslandManager::IslandCallback"*, %"struct.btSimulationIslandManager::IslandCallback"** %callback.addr, align 4
  %11 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %call9 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %11, i32 0)
  %12 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %call10 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %12)
  %13 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifold, align 4
  %14 = load i32, i32* %maxNumManifolds, align 4
  %15 = bitcast %"struct.btSimulationIslandManager::IslandCallback"* %10 to void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)***
  %vtable11 = load void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)**, void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)*** %15, align 4
  %vfn12 = getelementptr inbounds void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)*, void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)** %vtable11, i64 2
  %16 = load void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)*, void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)** %vfn12, align 4
  call void %16(%"struct.btSimulationIslandManager::IslandCallback"* %10, %class.btCollisionObject** %call9, i32 %call10, %class.btPersistentManifold** %13, i32 %14, i32 -1)
  br label %if.end68

if.else:                                          ; preds = %entry
  %m_islandmanifold = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  %call13 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %m_islandmanifold)
  store i32 %call13, i32* %numManifolds, align 4
  %m_islandmanifold14 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9quickSortI33btPersistentManifoldSortPredicateEEvRKT_(%class.btAlignedObjectArray.0* %m_islandmanifold14, %class.btPersistentManifoldSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  store i32 0, i32* %startManifoldIndex, align 4
  store i32 1, i32* %endManifoldIndex, align 4
  store i32 0, i32* %startIslandIndex, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc66, %if.else
  %17 = load i32, i32* %startIslandIndex, align 4
  %18 = load i32, i32* %numElem, align 4
  %cmp = icmp slt i32 %17, %18
  br i1 %cmp, label %for.body, label %for.end67

for.body:                                         ; preds = %for.cond
  %call15 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %19 = load i32, i32* %startIslandIndex, align 4
  %call16 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call15, i32 %19)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call16, i32 0, i32 0
  %20 = load i32, i32* %m_id, align 4
  store i32 %20, i32* %islandId, align 4
  store i8 1, i8* %islandSleeping, align 1
  %21 = load i32, i32* %startIslandIndex, align 4
  store i32 %21, i32* %endIslandIndex, align 4
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc, %for.body
  %22 = load i32, i32* %endIslandIndex, align 4
  %23 = load i32, i32* %numElem, align 4
  %cmp18 = icmp slt i32 %22, %23
  br i1 %cmp18, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond17
  %call19 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %24 = load i32, i32* %endIslandIndex, align 4
  %call20 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call19, i32 %24)
  %m_id21 = getelementptr inbounds %struct.btElement, %struct.btElement* %call20, i32 0, i32 0
  %25 = load i32, i32* %m_id21, align 4
  %26 = load i32, i32* %islandId, align 4
  %cmp22 = icmp eq i32 %25, %26
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond17
  %27 = phi i1 [ false, %for.cond17 ], [ %cmp22, %land.rhs ]
  br i1 %27, label %for.body23, label %for.end

for.body23:                                       ; preds = %land.end
  %call24 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this1)
  %28 = load i32, i32* %endIslandIndex, align 4
  %call25 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call24, i32 %28)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call25, i32 0, i32 1
  %29 = load i32, i32* %m_sz, align 4
  store i32 %29, i32* %i, align 4
  %30 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %31 = load i32, i32* %i, align 4
  %call26 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %30, i32 %31)
  %32 = load %class.btCollisionObject*, %class.btCollisionObject** %call26, align 4
  store %class.btCollisionObject* %32, %class.btCollisionObject** %colObj0, align 4
  %m_islandBodies = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray.10* %m_islandBodies, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %colObj0)
  %33 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call27 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %33)
  br i1 %call27, label %if.then28, label %if.end

if.then28:                                        ; preds = %for.body23
  store i8 0, i8* %islandSleeping, align 1
  br label %if.end

if.end:                                           ; preds = %if.then28, %for.body23
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %34 = load i32, i32* %endIslandIndex, align 4
  %inc = add nsw i32 %34, 1
  store i32 %inc, i32* %endIslandIndex, align 4
  br label %for.cond17

for.end:                                          ; preds = %land.end
  store i32 0, i32* %numIslandManifolds, align 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %startManifold, align 4
  %35 = load i32, i32* %startManifoldIndex, align 4
  %36 = load i32, i32* %numManifolds, align 4
  %cmp29 = icmp slt i32 %35, %36
  br i1 %cmp29, label %if.then30, label %if.end51

if.then30:                                        ; preds = %for.end
  %m_islandmanifold31 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  %37 = load i32, i32* %startManifoldIndex, align 4
  %call32 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.0* %m_islandmanifold31, i32 %37)
  %38 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call32, align 4
  %call33 = call i32 @_Z11getIslandIdPK20btPersistentManifold(%class.btPersistentManifold* %38)
  store i32 %call33, i32* %curIslandId, align 4
  %39 = load i32, i32* %curIslandId, align 4
  %40 = load i32, i32* %islandId, align 4
  %cmp34 = icmp eq i32 %39, %40
  br i1 %cmp34, label %if.then35, label %if.end50

if.then35:                                        ; preds = %if.then30
  %m_islandmanifold36 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  %41 = load i32, i32* %startManifoldIndex, align 4
  %call37 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.0* %m_islandmanifold36, i32 %41)
  store %class.btPersistentManifold** %call37, %class.btPersistentManifold*** %startManifold, align 4
  %42 = load i32, i32* %startManifoldIndex, align 4
  %add = add nsw i32 %42, 1
  store i32 %add, i32* %endManifoldIndex, align 4
  br label %for.cond38

for.cond38:                                       ; preds = %for.inc47, %if.then35
  %43 = load i32, i32* %endManifoldIndex, align 4
  %44 = load i32, i32* %numManifolds, align 4
  %cmp39 = icmp slt i32 %43, %44
  br i1 %cmp39, label %land.rhs40, label %land.end45

land.rhs40:                                       ; preds = %for.cond38
  %45 = load i32, i32* %islandId, align 4
  %m_islandmanifold41 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 2
  %46 = load i32, i32* %endManifoldIndex, align 4
  %call42 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.0* %m_islandmanifold41, i32 %46)
  %47 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call42, align 4
  %call43 = call i32 @_Z11getIslandIdPK20btPersistentManifold(%class.btPersistentManifold* %47)
  %cmp44 = icmp eq i32 %45, %call43
  br label %land.end45

land.end45:                                       ; preds = %land.rhs40, %for.cond38
  %48 = phi i1 [ false, %for.cond38 ], [ %cmp44, %land.rhs40 ]
  br i1 %48, label %for.body46, label %for.end49

for.body46:                                       ; preds = %land.end45
  br label %for.inc47

for.inc47:                                        ; preds = %for.body46
  %49 = load i32, i32* %endManifoldIndex, align 4
  %inc48 = add nsw i32 %49, 1
  store i32 %inc48, i32* %endManifoldIndex, align 4
  br label %for.cond38

for.end49:                                        ; preds = %land.end45
  %50 = load i32, i32* %endManifoldIndex, align 4
  %51 = load i32, i32* %startManifoldIndex, align 4
  %sub = sub nsw i32 %50, %51
  store i32 %sub, i32* %numIslandManifolds, align 4
  br label %if.end50

if.end50:                                         ; preds = %for.end49, %if.then30
  br label %if.end51

if.end51:                                         ; preds = %if.end50, %for.end
  %52 = load i8, i8* %islandSleeping, align 1
  %tobool52 = trunc i8 %52 to i1
  br i1 %tobool52, label %if.end60, label %if.then53

if.then53:                                        ; preds = %if.end51
  %53 = load %"struct.btSimulationIslandManager::IslandCallback"*, %"struct.btSimulationIslandManager::IslandCallback"** %callback.addr, align 4
  %m_islandBodies54 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 3
  %call55 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %m_islandBodies54, i32 0)
  %m_islandBodies56 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 3
  %call57 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %m_islandBodies56)
  %54 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %startManifold, align 4
  %55 = load i32, i32* %numIslandManifolds, align 4
  %56 = load i32, i32* %islandId, align 4
  %57 = bitcast %"struct.btSimulationIslandManager::IslandCallback"* %53 to void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)***
  %vtable58 = load void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)**, void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)*** %57, align 4
  %vfn59 = getelementptr inbounds void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)*, void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)** %vtable58, i64 2
  %58 = load void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)*, void (%"struct.btSimulationIslandManager::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, i32)** %vfn59, align 4
  call void %58(%"struct.btSimulationIslandManager::IslandCallback"* %53, %class.btCollisionObject** %call55, i32 %call57, %class.btPersistentManifold** %54, i32 %55, i32 %56)
  br label %if.end60

if.end60:                                         ; preds = %if.then53, %if.end51
  %59 = load i32, i32* %numIslandManifolds, align 4
  %tobool61 = icmp ne i32 %59, 0
  br i1 %tobool61, label %if.then62, label %if.end63

if.then62:                                        ; preds = %if.end60
  %60 = load i32, i32* %endManifoldIndex, align 4
  store i32 %60, i32* %startManifoldIndex, align 4
  br label %if.end63

if.end63:                                         ; preds = %if.then62, %if.end60
  %m_islandBodies64 = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 3
  store %class.btCollisionObject* null, %class.btCollisionObject** %ref.tmp65, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray.10* %m_islandBodies64, i32 0, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %ref.tmp65)
  br label %for.inc66

for.inc66:                                        ; preds = %if.end63
  %61 = load i32, i32* %endIslandIndex, align 4
  store i32 %61, i32* %startIslandIndex, align 4
  br label %for.cond

for.end67:                                        ; preds = %for.cond
  br label %if.end68

if.end68:                                         ; preds = %for.end67, %if.then
  %call69 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9quickSortI33btPersistentManifoldSortPredicateEEvRKT_(%class.btAlignedObjectArray.0* %this, %class.btPersistentManifoldSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %CompareFunc.addr = alloca %class.btPersistentManifoldSortPredicate*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store %class.btPersistentManifoldSortPredicate* %CompareFunc, %class.btPersistentManifoldSortPredicate** %CompareFunc.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btPersistentManifoldSortPredicate*, %class.btPersistentManifoldSortPredicate** %CompareFunc.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.btPersistentManifoldSortPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray.10* %this, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %_Val.addr = alloca %class.btCollisionObject**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store %class.btCollisionObject** %_Val, %class.btCollisionObject*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray.10* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray.10* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray.10* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %1, i32 %2
  %3 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btCollisionObject**
  %5 = load %class.btCollisionObject**, %class.btCollisionObject*** %_Val.addr, align 4
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %5, align 4
  store %class.btCollisionObject* %6, %class.btCollisionObject** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp = icmp ne i32 %call, 2
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %call2 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp3 = icmp ne i32 %call2, 5
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %0 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_Z11getIslandIdPK20btPersistentManifold(%class.btPersistentManifold* %lhs) #1 comdat {
entry:
  %lhs.addr = alloca %class.btPersistentManifold*, align 4
  %islandId = alloca i32, align 4
  %rcolObj0 = alloca %class.btCollisionObject*, align 4
  %rcolObj1 = alloca %class.btCollisionObject*, align 4
  store %class.btPersistentManifold* %lhs, %class.btPersistentManifold** %lhs.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %lhs.addr, align 4
  %call = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %0)
  store %class.btCollisionObject* %call, %class.btCollisionObject** %rcolObj0, align 4
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %lhs.addr, align 4
  %call1 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %1)
  store %class.btCollisionObject* %call1, %class.btCollisionObject** %rcolObj1, align 4
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj0, align 4
  %call2 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %2)
  %cmp = icmp sge i32 %call2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj0, align 4
  %call3 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj1, align 4
  %call4 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %4)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call3, %cond.true ], [ %call4, %cond.false ]
  store i32 %cond, i32* %islandId, align 4
  %5 = load i32, i32* %islandId, align 4
  ret i32 %5
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray.10* %this, i32 %newsize, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btCollisionObject**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btCollisionObject** %fillData, %class.btCollisionObject*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %5 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray.10* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %14 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %14, i32 %15
  %16 = bitcast %class.btCollisionObject** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btCollisionObject**
  %18 = load %class.btCollisionObject**, %class.btCollisionObject*** %fillData.addr, align 4
  %19 = load %class.btCollisionObject*, %class.btCollisionObject** %18, align 4
  store %class.btCollisionObject* %19, %class.btCollisionObject** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btElement*, %struct.btElement** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %0, i32 %1
  ret %struct.btElement* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.11* @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator.11* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  ret %class.btAlignedAllocator.11* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray.10* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %this, %class.btPersistentManifold** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv(%class.btAlignedObjectArray.10* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray.10* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray.10* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray.10* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %tobool = icmp ne %class.btCollisionObject** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_(%class.btAlignedAllocator.11* %m_allocator, %class.btCollisionObject** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_(%class.btAlignedAllocator.11* %this, %class.btCollisionObject** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  %ptr.addr = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4
  store %class.btCollisionObject** %ptr, %class.btCollisionObject*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %ptr.addr, align 4
  %1 = bitcast %class.btCollisionObject** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %2, %class.btPersistentManifold*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %4, %class.btPersistentManifold*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  %5 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %7, i32 %8
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4
  store %class.btPersistentManifold* %9, %class.btPersistentManifold** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.1* %this, i32 %n, %class.btPersistentManifold*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.0* %this, %class.btPersistentManifoldSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %CompareFunc.addr = alloca %class.btPersistentManifoldSortPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %class.btPersistentManifold*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store %class.btPersistentManifoldSortPredicate* %CompareFunc, %class.btPersistentManifoldSortPredicate** %CompareFunc.addr, align 4
  store i32 %lo, i32* %lo.addr, align 4
  store i32 %hi, i32* %hi.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %lo.addr, align 4
  store i32 %0, i32* %i, align 4
  %1 = load i32, i32* %hi.addr, align 4
  store i32 %1, i32* %j, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %3 = load i32, i32* %lo.addr, align 4
  %4 = load i32, i32* %hi.addr, align 4
  %add = add nsw i32 %3, %4
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %2, i32 %div
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx, align 4
  store %class.btPersistentManifold* %5, %class.btPersistentManifold** %x, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %6 = load %class.btPersistentManifoldSortPredicate*, %class.btPersistentManifoldSortPredicate** %CompareFunc.addr, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data2, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %7, i32 %8
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx3, align 4
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %x, align 4
  %call = call zeroext i1 @_ZNK33btPersistentManifoldSortPredicateclEPK20btPersistentManifoldS2_(%class.btPersistentManifoldSortPredicate* %6, %class.btPersistentManifold* %9, %class.btPersistentManifold* %10)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond4

while.cond4:                                      ; preds = %while.body8, %while.end
  %12 = load %class.btPersistentManifoldSortPredicate*, %class.btPersistentManifoldSortPredicate** %CompareFunc.addr, align 4
  %13 = load %class.btPersistentManifold*, %class.btPersistentManifold** %x, align 4
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %14 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data5, align 4
  %15 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %14, i32 %15
  %16 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx6, align 4
  %call7 = call zeroext i1 @_ZNK33btPersistentManifoldSortPredicateclEPK20btPersistentManifoldS2_(%class.btPersistentManifoldSortPredicate* %12, %class.btPersistentManifold* %13, %class.btPersistentManifold* %16)
  br i1 %call7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond4
  %17 = load i32, i32* %j, align 4
  %dec = add nsw i32 %17, -1
  store i32 %dec, i32* %j, align 4
  br label %while.cond4

while.end9:                                       ; preds = %while.cond4
  %18 = load i32, i32* %i, align 4
  %19 = load i32, i32* %j, align 4
  %cmp = icmp sle i32 %18, %19
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %20 = load i32, i32* %i, align 4
  %21 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4swapEii(%class.btAlignedObjectArray.0* %this1, i32 %20, i32 %21)
  %22 = load i32, i32* %i, align 4
  %inc10 = add nsw i32 %22, 1
  store i32 %inc10, i32* %i, align 4
  %23 = load i32, i32* %j, align 4
  %dec11 = add nsw i32 %23, -1
  store i32 %dec11, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %24 = load i32, i32* %i, align 4
  %25 = load i32, i32* %j, align 4
  %cmp12 = icmp sle i32 %24, %25
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %26 = load i32, i32* %lo.addr, align 4
  %27 = load i32, i32* %j, align 4
  %cmp13 = icmp slt i32 %26, %27
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %28 = load %class.btPersistentManifoldSortPredicate*, %class.btPersistentManifoldSortPredicate** %CompareFunc.addr, align 4
  %29 = load i32, i32* %lo.addr, align 4
  %30 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.btPersistentManifoldSortPredicate* nonnull align 1 dereferenceable(1) %28, i32 %29, i32 %30)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %31 = load i32, i32* %i, align 4
  %32 = load i32, i32* %hi.addr, align 4
  %cmp16 = icmp slt i32 %31, %32
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %33 = load %class.btPersistentManifoldSortPredicate*, %class.btPersistentManifoldSortPredicate** %CompareFunc.addr, align 4
  %34 = load i32, i32* %i, align 4
  %35 = load i32, i32* %hi.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE17quickSortInternalI33btPersistentManifoldSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.0* %this1, %class.btPersistentManifoldSortPredicate* nonnull align 1 dereferenceable(1) %33, i32 %34, i32 %35)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK33btPersistentManifoldSortPredicateclEPK20btPersistentManifoldS2_(%class.btPersistentManifoldSortPredicate* %this, %class.btPersistentManifold* %lhs, %class.btPersistentManifold* %rhs) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifoldSortPredicate*, align 4
  %lhs.addr = alloca %class.btPersistentManifold*, align 4
  %rhs.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifoldSortPredicate* %this, %class.btPersistentManifoldSortPredicate** %this.addr, align 4
  store %class.btPersistentManifold* %lhs, %class.btPersistentManifold** %lhs.addr, align 4
  store %class.btPersistentManifold* %rhs, %class.btPersistentManifold** %rhs.addr, align 4
  %this1 = load %class.btPersistentManifoldSortPredicate*, %class.btPersistentManifoldSortPredicate** %this.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %lhs.addr, align 4
  %call = call i32 @_Z11getIslandIdPK20btPersistentManifold(%class.btPersistentManifold* %0)
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %rhs.addr, align 4
  %call2 = call i32 @_Z11getIslandIdPK20btPersistentManifold(%class.btPersistentManifold* %1)
  %cmp = icmp slt i32 %call, %call2
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4swapEii(%class.btAlignedObjectArray.0* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btPersistentManifold*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  %2 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx, align 4
  store %class.btPersistentManifold* %2, %class.btPersistentManifold** %temp, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data2, align 4
  %4 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx3, align 4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %6 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %6, i32 %7
  store %class.btPersistentManifold* %5, %class.btPersistentManifold** %arrayidx5, align 4
  %8 = load %class.btPersistentManifold*, %class.btPersistentManifold** %temp, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %9 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data6, align 4
  %10 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %9, i32 %10
  store %class.btPersistentManifold* %8, %class.btPersistentManifold** %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray.10* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray.10* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray.10* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray.10* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btCollisionObject**
  store %class.btCollisionObject** %2, %class.btCollisionObject*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call3, %class.btCollisionObject** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray.10* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store %class.btCollisionObject** %4, %class.btCollisionObject*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray.10* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray.10* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btCollisionObject** @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.11* %m_allocator, i32 %1, %class.btCollisionObject*** null)
  %2 = bitcast %class.btCollisionObject** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_(%class.btAlignedObjectArray.10* %this, i32 %start, i32 %end, %class.btCollisionObject** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btCollisionObject**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btCollisionObject** %dest, %class.btCollisionObject*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %3, i32 %4
  %5 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btCollisionObject**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %7 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %7, i32 %8
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx2, align 4
  store %class.btCollisionObject* %9, %class.btCollisionObject** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionObject** @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.11* %this, i32 %n, %class.btCollisionObject*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btCollisionObject***, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btCollisionObject*** %hint, %class.btCollisionObject**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btCollisionObject**
  ret %class.btCollisionObject** %1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btSimulationIslandManager.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }
attributes #7 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
