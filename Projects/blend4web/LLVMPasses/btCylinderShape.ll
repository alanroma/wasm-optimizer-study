; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btCylinderShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btCylinderShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btCylinderShape = type { %class.btConvexInternalShape, i32 }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btVector3 = type { [4 x float] }
%class.btCylinderShapeX = type { %class.btCylinderShape }
%class.btCylinderShapeZ = type { %class.btCylinderShape }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btSerializer = type opaque
%struct.btCylinderShapeData = type { %struct.btConvexInternalShapeData, i32, [4 x i8] }
%struct.btConvexInternalShapeData = type { %struct.btCollisionShapeData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btVector3FloatData = type { [4 x float] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN21btConvexInternalShape13setSafeMarginERK9btVector3f = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_Z15btTransformAabbRK9btVector3fRK11btTransformRS_S5_ = comdat any

$_ZNK15btCylinderShape27getHalfExtentsWithoutMarginEv = comdat any

$_ZNK15btCylinderShape24getHalfExtentsWithMarginEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_Z21CylinderLocalSupportXRK9btVector3S1_ = comdat any

$_Z21CylinderLocalSupportZRK9btVector3S1_ = comdat any

$_Z21CylinderLocalSupportYRK9btVector3S1_ = comdat any

$_ZN15btCylinderShapeD2Ev = comdat any

$_ZN15btCylinderShapeD0Ev = comdat any

$_ZN15btCylinderShape15setLocalScalingERK9btVector3 = comdat any

$_ZNK21btConvexInternalShape15getLocalScalingEv = comdat any

$_ZNK15btCylinderShape7getNameEv = comdat any

$_ZNK15btCylinderShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN15btCylinderShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9getMarginEv = comdat any

$_ZNK15btCylinderShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK15btCylinderShape9serializeEPvP12btSerializer = comdat any

$_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3 = comdat any

$_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv = comdat any

$_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 = comdat any

$_ZNK15btCylinderShape9getRadiusEv = comdat any

$_ZN16btCylinderShapeXD2Ev = comdat any

$_ZN16btCylinderShapeXD0Ev = comdat any

$_ZNK16btCylinderShapeX7getNameEv = comdat any

$_ZNK16btCylinderShapeX9getRadiusEv = comdat any

$_ZN16btCylinderShapeZD2Ev = comdat any

$_ZN16btCylinderShapeZD0Ev = comdat any

$_ZNK16btCylinderShapeZ7getNameEv = comdat any

$_ZNK16btCylinderShapeZ9getRadiusEv = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK9btVector37minAxisEv = comdat any

$_ZN21btConvexInternalShape13setSafeMarginEff = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x38absoluteEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_Z6btSqrtf = comdat any

$_ZN21btConvexInternalShapeD2Ev = comdat any

$_ZN15btCylinderShapedlEPv = comdat any

$_ZdvRK9btVector3S1_ = comdat any

$_ZNK15btCylinderShape9getUpAxisEv = comdat any

$_ZN21btConvexInternalShape9setMarginEf = comdat any

$_ZNK21btConvexInternalShape9serializeEPvP12btSerializer = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZN16btCylinderShapeXdlEPv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZN16btCylinderShapeZdlEPv = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV15btCylinderShape = hidden unnamed_addr constant { [26 x i8*] } { [26 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btCylinderShape to i8*), i8* bitcast (%class.btCylinderShape* (%class.btCylinderShape*)* @_ZN15btCylinderShapeD2Ev to i8*), i8* bitcast (void (%class.btCylinderShape*)* @_ZN15btCylinderShapeD0Ev to i8*), i8* bitcast (void (%class.btCylinderShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK15btCylinderShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btCylinderShape*, %class.btVector3*)* @_ZN15btCylinderShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btCylinderShape*, float, %class.btVector3*)* @_ZNK15btCylinderShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btCylinderShape*)* @_ZNK15btCylinderShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCylinderShape*)* @_ZNK15btCylinderShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btCylinderShape*, float)* @_ZN15btCylinderShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCylinderShape*)* @_ZNK15btCylinderShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCylinderShape*, i8*, %class.btSerializer*)* @_ZNK15btCylinderShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btCylinderShape*, %class.btVector3*)* @_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btCylinderShape*, %class.btVector3*)* @_ZNK15btCylinderShape37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*, %class.btVector3*, %class.btVector3*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_ to i8*), i8* bitcast (void (%class.btCylinderShape*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK15btCylinderShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (float (%class.btCylinderShape*)* @_ZNK15btCylinderShape9getRadiusEv to i8*)] }, align 4
@_ZTV16btCylinderShapeX = hidden unnamed_addr constant { [26 x i8*] } { [26 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btCylinderShapeX to i8*), i8* bitcast (%class.btCylinderShapeX* (%class.btCylinderShapeX*)* @_ZN16btCylinderShapeXD2Ev to i8*), i8* bitcast (void (%class.btCylinderShapeX*)* @_ZN16btCylinderShapeXD0Ev to i8*), i8* bitcast (void (%class.btCylinderShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK15btCylinderShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btCylinderShape*, %class.btVector3*)* @_ZN15btCylinderShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btCylinderShape*, float, %class.btVector3*)* @_ZNK15btCylinderShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btCylinderShapeX*)* @_ZNK16btCylinderShapeX7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCylinderShape*)* @_ZNK15btCylinderShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btCylinderShape*, float)* @_ZN15btCylinderShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCylinderShape*)* @_ZNK15btCylinderShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCylinderShape*, i8*, %class.btSerializer*)* @_ZNK15btCylinderShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btCylinderShape*, %class.btVector3*)* @_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btCylinderShapeX*, %class.btVector3*)* @_ZNK16btCylinderShapeX37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*, %class.btVector3*, %class.btVector3*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_ to i8*), i8* bitcast (void (%class.btCylinderShapeX*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK16btCylinderShapeX49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (float (%class.btCylinderShapeX*)* @_ZNK16btCylinderShapeX9getRadiusEv to i8*)] }, align 4
@_ZTV16btCylinderShapeZ = hidden unnamed_addr constant { [26 x i8*] } { [26 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btCylinderShapeZ to i8*), i8* bitcast (%class.btCylinderShapeZ* (%class.btCylinderShapeZ*)* @_ZN16btCylinderShapeZD2Ev to i8*), i8* bitcast (void (%class.btCylinderShapeZ*)* @_ZN16btCylinderShapeZD0Ev to i8*), i8* bitcast (void (%class.btCylinderShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK15btCylinderShape7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btCylinderShape*, %class.btVector3*)* @_ZN15btCylinderShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape15getLocalScalingEv to i8*), i8* bitcast (void (%class.btCylinderShape*, float, %class.btVector3*)* @_ZNK15btCylinderShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btCylinderShapeZ*)* @_ZNK16btCylinderShapeZ7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCylinderShape*)* @_ZNK15btCylinderShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btCylinderShape*, float)* @_ZN15btCylinderShape9setMarginEf to i8*), i8* bitcast (float (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCylinderShape*)* @_ZNK15btCylinderShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCylinderShape*, i8*, %class.btSerializer*)* @_ZNK15btCylinderShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btVector3*, %class.btCylinderShape*, %class.btVector3*)* @_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btCylinderShapeZ*, %class.btVector3*)* @_ZNK16btCylinderShapeZ37localGetSupportingVertexWithoutMarginERK9btVector3 to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*, %class.btVector3*, %class.btVector3*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_ to i8*), i8* bitcast (void (%class.btCylinderShapeZ*, %class.btVector3*, %class.btVector3*, i32)* @_ZNK16btCylinderShapeZ49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i to i8*), i8* bitcast (void (%class.btConvexInternalShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (i32 (%class.btConvexInternalShape*)* @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv to i8*), i8* bitcast (void (%class.btConvexInternalShape*, i32, %class.btVector3*)* @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3 to i8*), i8* bitcast (float (%class.btCylinderShapeZ*)* @_ZNK16btCylinderShapeZ9getRadiusEv to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS15btCylinderShape = hidden constant [18 x i8] c"15btCylinderShape\00", align 1
@_ZTI21btConvexInternalShape = external constant i8*
@_ZTI15btCylinderShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([18 x i8], [18 x i8]* @_ZTS15btCylinderShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI21btConvexInternalShape to i8*) }, align 4
@_ZTS16btCylinderShapeX = hidden constant [19 x i8] c"16btCylinderShapeX\00", align 1
@_ZTI16btCylinderShapeX = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btCylinderShapeX, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btCylinderShape to i8*) }, align 4
@_ZTS16btCylinderShapeZ = hidden constant [19 x i8] c"16btCylinderShapeZ\00", align 1
@_ZTI16btCylinderShapeZ = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btCylinderShapeZ, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI15btCylinderShape to i8*) }, align 4
@.str = private unnamed_addr constant [10 x i8] c"CylinderY\00", align 1
@.str.1 = private unnamed_addr constant [20 x i8] c"btCylinderShapeData\00", align 1
@.str.2 = private unnamed_addr constant [26 x i8] c"btConvexInternalShapeData\00", align 1
@.str.3 = private unnamed_addr constant [10 x i8] c"CylinderX\00", align 1
@.str.4 = private unnamed_addr constant [10 x i8] c"CylinderZ\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btCylinderShape.cpp, i8* null }]

@_ZN15btCylinderShapeC1ERK9btVector3 = hidden unnamed_addr alias %class.btCylinderShape* (%class.btCylinderShape*, %class.btVector3*), %class.btCylinderShape* (%class.btCylinderShape*, %class.btVector3*)* @_ZN15btCylinderShapeC2ERK9btVector3
@_ZN16btCylinderShapeXC1ERK9btVector3 = hidden unnamed_addr alias %class.btCylinderShapeX* (%class.btCylinderShapeX*, %class.btVector3*), %class.btCylinderShapeX* (%class.btCylinderShapeX*, %class.btVector3*)* @_ZN16btCylinderShapeXC2ERK9btVector3
@_ZN16btCylinderShapeZC1ERK9btVector3 = hidden unnamed_addr alias %class.btCylinderShapeZ* (%class.btCylinderShapeZ*, %class.btVector3*), %class.btCylinderShapeZ* (%class.btCylinderShapeZ*, %class.btVector3*)* @_ZN16btCylinderShapeZC2ERK9btVector3

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btCylinderShape* @_ZN15btCylinderShapeC2ERK9btVector3(%class.btCylinderShape* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %halfExtents) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  %halfExtents.addr = alloca %class.btVector3*, align 4
  %margin = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  store %class.btVector3* %halfExtents, %class.btVector3** %halfExtents.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  %0 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* %0)
  %1 = bitcast %class.btCylinderShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [26 x i8*] }, { [26 x i8*] }* @_ZTV15btCylinderShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_upAxis = getelementptr inbounds %class.btCylinderShape, %class.btCylinderShape* %this1, i32 0, i32 1
  store i32 1, i32* %m_upAxis, align 4
  %2 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %3 = load %class.btVector3*, %class.btVector3** %halfExtents.addr, align 4
  call void @_ZN21btConvexInternalShape13setSafeMarginERK9btVector3f(%class.btConvexInternalShape* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, float 0x3FB99999A0000000)
  %4 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %5 = bitcast %class.btConvexInternalShape* %4 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %5, align 4
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %6 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call2 = call float %6(%class.btConvexInternalShape* %4)
  store float %call2, float* %ref.tmp, align 4
  %7 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %8 = bitcast %class.btConvexInternalShape* %7 to float (%class.btConvexInternalShape*)***
  %vtable4 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %8, align 4
  %vfn5 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable4, i64 12
  %9 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn5, align 4
  %call6 = call float %9(%class.btConvexInternalShape* %7)
  store float %call6, float* %ref.tmp3, align 4
  %10 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %11 = bitcast %class.btConvexInternalShape* %10 to float (%class.btConvexInternalShape*)***
  %vtable8 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %11, align 4
  %vfn9 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable8, i64 12
  %12 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn9, align 4
  %call10 = call float %12(%class.btConvexInternalShape* %10)
  store float %call10, float* %ref.tmp7, align 4
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %margin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %13 = load %class.btVector3*, %class.btVector3** %halfExtents.addr, align 4
  %14 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %14, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %margin)
  %15 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %15, i32 0, i32 2
  %16 = bitcast %class.btVector3* %m_implicitShapeDimensions to i8*
  %17 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  %18 = bitcast %class.btCylinderShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %18, i32 0, i32 1
  store i32 13, i32* %m_shapeType, align 4
  ret %class.btCylinderShape* %this1
}

declare %class.btConvexInternalShape* @_ZN21btConvexInternalShapeC2Ev(%class.btConvexInternalShape* returned) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN21btConvexInternalShape13setSafeMarginERK9btVector3f(%class.btConvexInternalShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %halfExtents, float %defaultMarginMultiplier) #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %halfExtents.addr = alloca %class.btVector3*, align 4
  %defaultMarginMultiplier.addr = alloca float, align 4
  %minDimension = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  store %class.btVector3* %halfExtents, %class.btVector3** %halfExtents.addr, align 4
  store float %defaultMarginMultiplier, float* %defaultMarginMultiplier.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %halfExtents.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %1 = load %class.btVector3*, %class.btVector3** %halfExtents.addr, align 4
  %call2 = call i32 @_ZNK9btVector37minAxisEv(%class.btVector3* %1)
  %arrayidx = getelementptr inbounds float, float* %call, i32 %call2
  %2 = load float, float* %arrayidx, align 4
  store float %2, float* %minDimension, align 4
  %3 = load float, float* %minDimension, align 4
  %4 = load float, float* %defaultMarginMultiplier.addr, align 4
  call void @_ZN21btConvexInternalShape13setSafeMarginEff(%class.btConvexInternalShape* %this1, float %3, float %4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define hidden %class.btCylinderShapeX* @_ZN16btCylinderShapeXC2ERK9btVector3(%class.btCylinderShapeX* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %halfExtents) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCylinderShapeX*, align 4
  %halfExtents.addr = alloca %class.btVector3*, align 4
  store %class.btCylinderShapeX* %this, %class.btCylinderShapeX** %this.addr, align 4
  store %class.btVector3* %halfExtents, %class.btVector3** %halfExtents.addr, align 4
  %this1 = load %class.btCylinderShapeX*, %class.btCylinderShapeX** %this.addr, align 4
  %0 = bitcast %class.btCylinderShapeX* %this1 to %class.btCylinderShape*
  %1 = load %class.btVector3*, %class.btVector3** %halfExtents.addr, align 4
  %call = call %class.btCylinderShape* @_ZN15btCylinderShapeC2ERK9btVector3(%class.btCylinderShape* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = bitcast %class.btCylinderShapeX* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [26 x i8*] }, { [26 x i8*] }* @_ZTV16btCylinderShapeX, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4
  %3 = bitcast %class.btCylinderShapeX* %this1 to %class.btCylinderShape*
  %m_upAxis = getelementptr inbounds %class.btCylinderShape, %class.btCylinderShape* %3, i32 0, i32 1
  store i32 0, i32* %m_upAxis, align 4
  ret %class.btCylinderShapeX* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btCylinderShapeZ* @_ZN16btCylinderShapeZC2ERK9btVector3(%class.btCylinderShapeZ* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %halfExtents) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCylinderShapeZ*, align 4
  %halfExtents.addr = alloca %class.btVector3*, align 4
  store %class.btCylinderShapeZ* %this, %class.btCylinderShapeZ** %this.addr, align 4
  store %class.btVector3* %halfExtents, %class.btVector3** %halfExtents.addr, align 4
  %this1 = load %class.btCylinderShapeZ*, %class.btCylinderShapeZ** %this.addr, align 4
  %0 = bitcast %class.btCylinderShapeZ* %this1 to %class.btCylinderShape*
  %1 = load %class.btVector3*, %class.btVector3** %halfExtents.addr, align 4
  %call = call %class.btCylinderShape* @_ZN15btCylinderShapeC2ERK9btVector3(%class.btCylinderShape* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = bitcast %class.btCylinderShapeZ* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [26 x i8*] }, { [26 x i8*] }* @_ZTV16btCylinderShapeZ, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4
  %3 = bitcast %class.btCylinderShapeZ* %this1 to %class.btCylinderShape*
  %m_upAxis = getelementptr inbounds %class.btCylinderShape, %class.btCylinderShape* %3, i32 0, i32 1
  store i32 2, i32* %m_upAxis, align 4
  ret %class.btCylinderShapeZ* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK15btCylinderShape7getAabbERK11btTransformR9btVector3S4_(%class.btCylinderShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btCylinderShape27getHalfExtentsWithoutMarginEv(%class.btCylinderShape* %this1)
  %0 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %1 = bitcast %class.btConvexInternalShape* %0 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %1, align 4
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %2 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call2 = call float %2(%class.btConvexInternalShape* %0)
  %3 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_Z15btTransformAabbRK9btVector3fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %call, float %call2, %class.btTransform* nonnull align 4 dereferenceable(64) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z15btTransformAabbRK9btVector3fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %halfExtents, float %margin, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMinOut, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMaxOut) #2 comdat {
entry:
  %halfExtents.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMinOut.addr = alloca %class.btVector3*, align 4
  %aabbMaxOut.addr = alloca %class.btVector3*, align 4
  %halfExtentsWithMargin = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center = alloca %class.btVector3, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  store %class.btVector3* %halfExtents, %class.btVector3** %halfExtents.addr, align 4
  store float %margin, float* %margin.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  store %class.btVector3* %aabbMinOut, %class.btVector3** %aabbMinOut.addr, align 4
  store %class.btVector3* %aabbMaxOut, %class.btVector3** %aabbMaxOut.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %halfExtents.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %halfExtentsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %1)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call1)
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %2)
  %3 = bitcast %class.btVector3* %center to i8*
  %4 = bitcast %class.btVector3* %call2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent, %class.btVector3* %halfExtentsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %5 = load %class.btVector3*, %class.btVector3** %aabbMinOut.addr, align 4
  %6 = bitcast %class.btVector3* %5 to i8*
  %7 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %8 = load %class.btVector3*, %class.btVector3** %aabbMaxOut.addr, align 4
  %9 = bitcast %class.btVector3* %8 to i8*
  %10 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btCylinderShape27getHalfExtentsWithoutMarginEv(%class.btCylinderShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  %0 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %0, i32 0, i32 2
  ret %class.btVector3* %m_implicitShapeDimensions
}

; Function Attrs: noinline optnone
define hidden void @_ZNK15btCylinderShape21calculateLocalInertiaEfR9btVector3(%class.btCylinderShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %radius2 = alloca float, align 4
  %height2 = alloca float, align 4
  %halfExtents = alloca %class.btVector3, align 4
  %div12 = alloca float, align 4
  %div4 = alloca float, align 4
  %div23 = alloca float, align 4
  %idxRadius = alloca i32, align 4
  %idxHeight = alloca i32, align 4
  %t1 = alloca float, align 4
  %t2 = alloca float, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  store float %mass, float* %mass.addr, align 4
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  call void @_ZNK15btCylinderShape24getHalfExtentsWithMarginEv(%class.btVector3* sret align 4 %halfExtents, %class.btCylinderShape* %this1)
  %0 = load float, float* %mass.addr, align 4
  %div = fdiv float %0, 1.200000e+01
  store float %div, float* %div12, align 4
  %1 = load float, float* %mass.addr, align 4
  %div2 = fdiv float %1, 4.000000e+00
  store float %div2, float* %div4, align 4
  %2 = load float, float* %mass.addr, align 4
  %div5 = fdiv float %2, 2.000000e+00
  store float %div5, float* %div23, align 4
  %m_upAxis = getelementptr inbounds %class.btCylinderShape, %class.btCylinderShape* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_upAxis, align 4
  switch i32 %3, label %sw.default [
    i32 0, label %sw.bb
    i32 2, label %sw.bb6
  ]

sw.bb:                                            ; preds = %entry
  store i32 1, i32* %idxRadius, align 4
  store i32 0, i32* %idxHeight, align 4
  br label %sw.epilog

sw.bb6:                                           ; preds = %entry
  store i32 0, i32* %idxRadius, align 4
  store i32 2, i32* %idxHeight, align 4
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  store i32 0, i32* %idxRadius, align 4
  store i32 1, i32* %idxHeight, align 4
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb6, %sw.bb
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents)
  %4 = load i32, i32* %idxRadius, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %4
  %5 = load float, float* %arrayidx, align 4
  %call7 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents)
  %6 = load i32, i32* %idxRadius, align 4
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 %6
  %7 = load float, float* %arrayidx8, align 4
  %mul = fmul float %5, %7
  store float %mul, float* %radius2, align 4
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents)
  %8 = load i32, i32* %idxHeight, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %8
  %9 = load float, float* %arrayidx10, align 4
  %mul11 = fmul float 4.000000e+00, %9
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents)
  %10 = load i32, i32* %idxHeight, align 4
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 %10
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %mul11, %11
  store float %mul14, float* %height2, align 4
  %12 = load float, float* %div12, align 4
  %13 = load float, float* %height2, align 4
  %mul15 = fmul float %12, %13
  %14 = load float, float* %div4, align 4
  %15 = load float, float* %radius2, align 4
  %mul16 = fmul float %14, %15
  %add = fadd float %mul15, %mul16
  store float %add, float* %t1, align 4
  %16 = load float, float* %div23, align 4
  %17 = load float, float* %radius2, align 4
  %mul17 = fmul float %16, %17
  store float %mul17, float* %t2, align 4
  %m_upAxis18 = getelementptr inbounds %class.btCylinderShape, %class.btCylinderShape* %this1, i32 0, i32 1
  %18 = load i32, i32* %m_upAxis18, align 4
  switch i32 %18, label %sw.default21 [
    i32 0, label %sw.bb19
    i32 2, label %sw.bb20
  ]

sw.bb19:                                          ; preds = %sw.epilog
  %19 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %19, float* nonnull align 4 dereferenceable(4) %t2, float* nonnull align 4 dereferenceable(4) %t1, float* nonnull align 4 dereferenceable(4) %t1)
  br label %sw.epilog22

sw.bb20:                                          ; preds = %sw.epilog
  %20 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %20, float* nonnull align 4 dereferenceable(4) %t1, float* nonnull align 4 dereferenceable(4) %t1, float* nonnull align 4 dereferenceable(4) %t2)
  br label %sw.epilog22

sw.default21:                                     ; preds = %sw.epilog
  %21 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %21, float* nonnull align 4 dereferenceable(4) %t1, float* nonnull align 4 dereferenceable(4) %t2, float* nonnull align 4 dereferenceable(4) %t1)
  br label %sw.epilog22

sw.epilog22:                                      ; preds = %sw.default21, %sw.bb20, %sw.bb19
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK15btCylinderShape24getHalfExtentsWithMarginEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCylinderShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  %margin = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btCylinderShape27getHalfExtentsWithoutMarginEv(%class.btCylinderShape* %this1)
  %0 = bitcast %class.btVector3* %agg.result to i8*
  %1 = bitcast %class.btVector3* %call to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %0, i8* align 4 %1, i32 16, i1 false)
  %2 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %3 = bitcast %class.btConvexInternalShape* %2 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %3, align 4
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %4 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call2 = call float %4(%class.btConvexInternalShape* %2)
  store float %call2, float* %ref.tmp, align 4
  %5 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %6 = bitcast %class.btConvexInternalShape* %5 to float (%class.btConvexInternalShape*)***
  %vtable4 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %6, align 4
  %vfn5 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable4, i64 12
  %7 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn5, align 4
  %call6 = call float %7(%class.btConvexInternalShape* %5)
  store float %call6, float* %ref.tmp3, align 4
  %8 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %9 = bitcast %class.btConvexInternalShape* %8 to float (%class.btConvexInternalShape*)***
  %vtable8 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %9, align 4
  %vfn9 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable8, i64 12
  %10 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn9, align 4
  %call10 = call float %10(%class.btConvexInternalShape* %8)
  store float %call10, float* %ref.tmp7, align 4
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %margin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %margin)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK16btCylinderShapeX37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btCylinderShapeX* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCylinderShapeX*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  store %class.btCylinderShapeX* %this, %class.btCylinderShapeX** %this.addr, align 4
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4
  %this1 = load %class.btCylinderShapeX*, %class.btCylinderShapeX** %this.addr, align 4
  %0 = bitcast %class.btCylinderShapeX* %this1 to %class.btCylinderShape*
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btCylinderShape27getHalfExtentsWithoutMarginEv(%class.btCylinderShape* %0)
  %1 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4
  call void @_Z21CylinderLocalSupportXRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z21CylinderLocalSupportXRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %halfExtents.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %cylinderUpAxis = alloca i32, align 4
  %XX = alloca i32, align 4
  %YY = alloca i32, align 4
  %ZZ = alloca i32, align 4
  %radius = alloca float, align 4
  %halfHeight = alloca float, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  store %class.btVector3* %halfExtents, %class.btVector3** %halfExtents.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store i32 0, i32* %cylinderUpAxis, align 4
  store i32 1, i32* %XX, align 4
  store i32 0, i32* %YY, align 4
  store i32 2, i32* %ZZ, align 4
  %0 = load %class.btVector3*, %class.btVector3** %halfExtents.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 1
  %1 = load float, float* %arrayidx, align 4
  store float %1, float* %radius, align 4
  %2 = load %class.btVector3*, %class.btVector3** %halfExtents.addr, align 4
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx2 = getelementptr inbounds float, float* %call1, i32 0
  %3 = load float, float* %arrayidx2, align 4
  store float %3, float* %halfHeight, align 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %agg.result)
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul = fmul float %5, %7
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %9 = load float, float* %arrayidx9, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %10)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 2
  %11 = load float, float* %arrayidx11, align 4
  %mul12 = fmul float %9, %11
  %add = fadd float %mul, %mul12
  %call13 = call float @_Z6btSqrtf(float %add)
  store float %call13, float* %s, align 4
  %12 = load float, float* %s, align 4
  %cmp = fcmp une float %12, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %13 = load float, float* %radius, align 4
  %14 = load float, float* %s, align 4
  %div = fdiv float %13, %14
  store float %div, float* %d, align 4
  %15 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %15)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 1
  %16 = load float, float* %arrayidx15, align 4
  %17 = load float, float* %d, align 4
  %mul16 = fmul float %16, %17
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  store float %mul16, float* %arrayidx18, align 4
  %18 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 0
  %19 = load float, float* %arrayidx20, align 4
  %conv = fpext float %19 to double
  %cmp21 = fcmp olt double %conv, 0.000000e+00
  br i1 %cmp21, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %20 = load float, float* %halfHeight, align 4
  %fneg = fneg float %20
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %21 = load float, float* %halfHeight, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %fneg, %cond.true ], [ %21, %cond.false ]
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 0
  store float %cond, float* %arrayidx23, align 4
  %22 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call24 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %22)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 2
  %23 = load float, float* %arrayidx25, align 4
  %24 = load float, float* %d, align 4
  %mul26 = fmul float %23, %24
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 2
  store float %mul26, float* %arrayidx28, align 4
  br label %return

if.else:                                          ; preds = %entry
  %25 = load float, float* %radius, align 4
  %call29 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 1
  store float %25, float* %arrayidx30, align 4
  %26 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call31 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %26)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 0
  %27 = load float, float* %arrayidx32, align 4
  %conv33 = fpext float %27 to double
  %cmp34 = fcmp olt double %conv33, 0.000000e+00
  br i1 %cmp34, label %cond.true35, label %cond.false37

cond.true35:                                      ; preds = %if.else
  %28 = load float, float* %halfHeight, align 4
  %fneg36 = fneg float %28
  br label %cond.end38

cond.false37:                                     ; preds = %if.else
  %29 = load float, float* %halfHeight, align 4
  br label %cond.end38

cond.end38:                                       ; preds = %cond.false37, %cond.true35
  %cond39 = phi float [ %fneg36, %cond.true35 ], [ %29, %cond.false37 ]
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 0
  store float %cond39, float* %arrayidx41, align 4
  %call42 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx43 = getelementptr inbounds float, float* %call42, i32 2
  store float 0.000000e+00, float* %arrayidx43, align 4
  br label %return

return:                                           ; preds = %cond.end38, %cond.end
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK16btCylinderShapeZ37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btCylinderShapeZ* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCylinderShapeZ*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  store %class.btCylinderShapeZ* %this, %class.btCylinderShapeZ** %this.addr, align 4
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4
  %this1 = load %class.btCylinderShapeZ*, %class.btCylinderShapeZ** %this.addr, align 4
  %0 = bitcast %class.btCylinderShapeZ* %this1 to %class.btCylinderShape*
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btCylinderShape27getHalfExtentsWithoutMarginEv(%class.btCylinderShape* %0)
  %1 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4
  call void @_Z21CylinderLocalSupportZRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z21CylinderLocalSupportZRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %halfExtents.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %cylinderUpAxis = alloca i32, align 4
  %XX = alloca i32, align 4
  %YY = alloca i32, align 4
  %ZZ = alloca i32, align 4
  %radius = alloca float, align 4
  %halfHeight = alloca float, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  store %class.btVector3* %halfExtents, %class.btVector3** %halfExtents.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store i32 2, i32* %cylinderUpAxis, align 4
  store i32 0, i32* %XX, align 4
  store i32 2, i32* %YY, align 4
  store i32 1, i32* %ZZ, align 4
  %0 = load %class.btVector3*, %class.btVector3** %halfExtents.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %1 = load float, float* %arrayidx, align 4
  store float %1, float* %radius, align 4
  %2 = load %class.btVector3*, %class.btVector3** %halfExtents.addr, align 4
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx2 = getelementptr inbounds float, float* %call1, i32 2
  %3 = load float, float* %arrayidx2, align 4
  store float %3, float* %halfHeight, align 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %agg.result)
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 0
  %7 = load float, float* %arrayidx7, align 4
  %mul = fmul float %5, %7
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 1
  %9 = load float, float* %arrayidx9, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %10)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 1
  %11 = load float, float* %arrayidx11, align 4
  %mul12 = fmul float %9, %11
  %add = fadd float %mul, %mul12
  %call13 = call float @_Z6btSqrtf(float %add)
  store float %call13, float* %s, align 4
  %12 = load float, float* %s, align 4
  %cmp = fcmp une float %12, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %13 = load float, float* %radius, align 4
  %14 = load float, float* %s, align 4
  %div = fdiv float %13, %14
  store float %div, float* %d, align 4
  %15 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %15)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 0
  %16 = load float, float* %arrayidx15, align 4
  %17 = load float, float* %d, align 4
  %mul16 = fmul float %16, %17
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 0
  store float %mul16, float* %arrayidx18, align 4
  %18 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 2
  %19 = load float, float* %arrayidx20, align 4
  %conv = fpext float %19 to double
  %cmp21 = fcmp olt double %conv, 0.000000e+00
  br i1 %cmp21, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %20 = load float, float* %halfHeight, align 4
  %fneg = fneg float %20
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %21 = load float, float* %halfHeight, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %fneg, %cond.true ], [ %21, %cond.false ]
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 2
  store float %cond, float* %arrayidx23, align 4
  %22 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call24 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %22)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 1
  %23 = load float, float* %arrayidx25, align 4
  %24 = load float, float* %d, align 4
  %mul26 = fmul float %23, %24
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 1
  store float %mul26, float* %arrayidx28, align 4
  br label %return

if.else:                                          ; preds = %entry
  %25 = load float, float* %radius, align 4
  %call29 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 0
  store float %25, float* %arrayidx30, align 4
  %26 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call31 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %26)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 2
  %27 = load float, float* %arrayidx32, align 4
  %conv33 = fpext float %27 to double
  %cmp34 = fcmp olt double %conv33, 0.000000e+00
  br i1 %cmp34, label %cond.true35, label %cond.false37

cond.true35:                                      ; preds = %if.else
  %28 = load float, float* %halfHeight, align 4
  %fneg36 = fneg float %28
  br label %cond.end38

cond.false37:                                     ; preds = %if.else
  %29 = load float, float* %halfHeight, align 4
  br label %cond.end38

cond.end38:                                       ; preds = %cond.false37, %cond.true35
  %cond39 = phi float [ %fneg36, %cond.true35 ], [ %29, %cond.false37 ]
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  store float %cond39, float* %arrayidx41, align 4
  %call42 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx43 = getelementptr inbounds float, float* %call42, i32 1
  store float 0.000000e+00, float* %arrayidx43, align 4
  br label %return

return:                                           ; preds = %cond.end38, %cond.end
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK15btCylinderShape37localGetSupportingVertexWithoutMarginERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btCylinderShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btCylinderShape27getHalfExtentsWithoutMarginEv(%class.btCylinderShape* %this1)
  %0 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4
  call void @_Z21CylinderLocalSupportYRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z21CylinderLocalSupportYRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %halfExtents.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %cylinderUpAxis = alloca i32, align 4
  %XX = alloca i32, align 4
  %YY = alloca i32, align 4
  %ZZ = alloca i32, align 4
  %radius = alloca float, align 4
  %halfHeight = alloca float, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  store %class.btVector3* %halfExtents, %class.btVector3** %halfExtents.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store i32 1, i32* %cylinderUpAxis, align 4
  store i32 0, i32* %XX, align 4
  store i32 1, i32* %YY, align 4
  store i32 2, i32* %ZZ, align 4
  %0 = load %class.btVector3*, %class.btVector3** %halfExtents.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %1 = load float, float* %arrayidx, align 4
  store float %1, float* %radius, align 4
  %2 = load %class.btVector3*, %class.btVector3** %halfExtents.addr, align 4
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx2 = getelementptr inbounds float, float* %call1, i32 1
  %3 = load float, float* %arrayidx2, align 4
  store float %3, float* %halfHeight, align 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %agg.result)
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 0
  %7 = load float, float* %arrayidx7, align 4
  %mul = fmul float %5, %7
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %9 = load float, float* %arrayidx9, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %10)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 2
  %11 = load float, float* %arrayidx11, align 4
  %mul12 = fmul float %9, %11
  %add = fadd float %mul, %mul12
  %call13 = call float @_Z6btSqrtf(float %add)
  store float %call13, float* %s, align 4
  %12 = load float, float* %s, align 4
  %cmp = fcmp une float %12, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %13 = load float, float* %radius, align 4
  %14 = load float, float* %s, align 4
  %div = fdiv float %13, %14
  store float %div, float* %d, align 4
  %15 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %15)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 0
  %16 = load float, float* %arrayidx15, align 4
  %17 = load float, float* %d, align 4
  %mul16 = fmul float %16, %17
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 0
  store float %mul16, float* %arrayidx18, align 4
  %18 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %19 = load float, float* %arrayidx20, align 4
  %conv = fpext float %19 to double
  %cmp21 = fcmp olt double %conv, 0.000000e+00
  br i1 %cmp21, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %20 = load float, float* %halfHeight, align 4
  %fneg = fneg float %20
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %21 = load float, float* %halfHeight, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %fneg, %cond.true ], [ %21, %cond.false ]
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 1
  store float %cond, float* %arrayidx23, align 4
  %22 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call24 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %22)
  %arrayidx25 = getelementptr inbounds float, float* %call24, i32 2
  %23 = load float, float* %arrayidx25, align 4
  %24 = load float, float* %d, align 4
  %mul26 = fmul float %23, %24
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 2
  store float %mul26, float* %arrayidx28, align 4
  br label %return

if.else:                                          ; preds = %entry
  %25 = load float, float* %radius, align 4
  %call29 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 0
  store float %25, float* %arrayidx30, align 4
  %26 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call31 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %26)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 1
  %27 = load float, float* %arrayidx32, align 4
  %conv33 = fpext float %27 to double
  %cmp34 = fcmp olt double %conv33, 0.000000e+00
  br i1 %cmp34, label %cond.true35, label %cond.false37

cond.true35:                                      ; preds = %if.else
  %28 = load float, float* %halfHeight, align 4
  %fneg36 = fneg float %28
  br label %cond.end38

cond.false37:                                     ; preds = %if.else
  %29 = load float, float* %halfHeight, align 4
  br label %cond.end38

cond.end38:                                       ; preds = %cond.false37, %cond.true35
  %cond39 = phi float [ %fneg36, %cond.true35 ], [ %29, %cond.false37 ]
  %call40 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 1
  store float %cond39, float* %arrayidx41, align 4
  %call42 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %arrayidx43 = getelementptr inbounds float, float* %call42, i32 2
  store float 0.000000e+00, float* %arrayidx43, align 4
  br label %return

return:                                           ; preds = %cond.end38, %cond.end
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK15btCylinderShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btCylinderShape* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4
  store i32 %numVectors, i32* %numVectors.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numVectors.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btCylinderShape27getHalfExtentsWithoutMarginEv(%class.btCylinderShape* %this1)
  %2 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 %3
  call void @_Z21CylinderLocalSupportYRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %4 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4
  %5 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  %6 = bitcast %class.btVector3* %arrayidx2 to i8*
  %7 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK16btCylinderShapeZ49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btCylinderShapeZ* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCylinderShapeZ*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btCylinderShapeZ* %this, %class.btCylinderShapeZ** %this.addr, align 4
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4
  store i32 %numVectors, i32* %numVectors.addr, align 4
  %this1 = load %class.btCylinderShapeZ*, %class.btCylinderShapeZ** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numVectors.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast %class.btCylinderShapeZ* %this1 to %class.btCylinderShape*
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btCylinderShape27getHalfExtentsWithoutMarginEv(%class.btCylinderShape* %2)
  %3 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  call void @_Z21CylinderLocalSupportZRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %5 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  %7 = bitcast %class.btVector3* %arrayidx2 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK16btCylinderShapeX49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i(%class.btCylinderShapeX* %this, %class.btVector3* %vectors, %class.btVector3* %supportVerticesOut, i32 %numVectors) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCylinderShapeX*, align 4
  %vectors.addr = alloca %class.btVector3*, align 4
  %supportVerticesOut.addr = alloca %class.btVector3*, align 4
  %numVectors.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btCylinderShapeX* %this, %class.btCylinderShapeX** %this.addr, align 4
  store %class.btVector3* %vectors, %class.btVector3** %vectors.addr, align 4
  store %class.btVector3* %supportVerticesOut, %class.btVector3** %supportVerticesOut.addr, align 4
  store i32 %numVectors, i32* %numVectors.addr, align 4
  %this1 = load %class.btCylinderShapeX*, %class.btCylinderShapeX** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numVectors.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast %class.btCylinderShapeX* %this1 to %class.btCylinderShape*
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK15btCylinderShape27getHalfExtentsWithoutMarginEv(%class.btCylinderShape* %2)
  %3 = load %class.btVector3*, %class.btVector3** %vectors.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  call void @_Z21CylinderLocalSupportXRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %5 = load %class.btVector3*, %class.btVector3** %supportVerticesOut.addr, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  %7 = bitcast %class.btVector3* %arrayidx2 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCylinderShape* @_ZN15btCylinderShapeD2Ev(%class.btCylinderShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  %0 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %call = call %class.btConvexInternalShape* @_ZN21btConvexInternalShapeD2Ev(%class.btConvexInternalShape* %0) #7
  ret %class.btCylinderShape* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btCylinderShapeD0Ev(%class.btCylinderShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  %call = call %class.btCylinderShape* @_ZN15btCylinderShapeD2Ev(%class.btCylinderShape* %this1) #7
  %0 = bitcast %class.btCylinderShape* %this1 to i8*
  call void @_ZN15btCylinderShapedlEPv(i8* %0) #7
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #3

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #3

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN15btCylinderShape15setLocalScalingERK9btVector3(%class.btCylinderShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  %oldMargin = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %implicitShapeDimensionsWithMargin = alloca %class.btVector3, align 4
  %unScaledImplicitShapeDimensionsWithMargin = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  %0 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %1 = bitcast %class.btConvexInternalShape* %0 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %1, align 4
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %2 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call = call float %2(%class.btConvexInternalShape* %0)
  store float %call, float* %ref.tmp, align 4
  %3 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %4 = bitcast %class.btConvexInternalShape* %3 to float (%class.btConvexInternalShape*)***
  %vtable3 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %4, align 4
  %vfn4 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable3, i64 12
  %5 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn4, align 4
  %call5 = call float %5(%class.btConvexInternalShape* %3)
  store float %call5, float* %ref.tmp2, align 4
  %6 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %7 = bitcast %class.btConvexInternalShape* %6 to float (%class.btConvexInternalShape*)***
  %vtable7 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %7, align 4
  %vfn8 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable7, i64 12
  %8 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn8, align 4
  %call9 = call float %8(%class.btConvexInternalShape* %6)
  store float %call9, float* %ref.tmp6, align 4
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %oldMargin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %9 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %9, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %implicitShapeDimensionsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions, %class.btVector3* nonnull align 4 dereferenceable(16) %oldMargin)
  %10 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %10, i32 0, i32 1
  call void @_ZdvRK9btVector3S1_(%class.btVector3* sret align 4 %unScaledImplicitShapeDimensionsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %implicitShapeDimensionsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling)
  %11 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %12 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  call void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %12)
  %13 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling13 = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %13, i32 0, i32 1
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %unScaledImplicitShapeDimensionsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localScaling13)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12, %class.btVector3* nonnull align 4 dereferenceable(16) %oldMargin)
  %14 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions14 = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %14, i32 0, i32 2
  %15 = bitcast %class.btVector3* %m_implicitShapeDimensions14 to i8*
  %16 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape15getLocalScalingEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK15btCylinderShape7getNameEv(%class.btCylinderShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK15btCylinderShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCylinderShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %call4 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %agg.result)
  %call5 = call i32 @_ZNK15btCylinderShape9getUpAxisEv(%class.btCylinderShape* %this1)
  %arrayidx = getelementptr inbounds float, float* %call4, i32 %call5
  store float 1.000000e+00, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN15btCylinderShape9setMarginEf(%class.btCylinderShape* %this, float %collisionMargin) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  %collisionMargin.addr = alloca float, align 4
  %oldMargin = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %implicitShapeDimensionsWithMargin = alloca %class.btVector3, align 4
  %newMargin = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp24 = alloca %class.btVector3, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  store float %collisionMargin, float* %collisionMargin.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  %0 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %1 = bitcast %class.btConvexInternalShape* %0 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %1, align 4
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %2 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call = call float %2(%class.btConvexInternalShape* %0)
  store float %call, float* %ref.tmp, align 4
  %3 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %4 = bitcast %class.btConvexInternalShape* %3 to float (%class.btConvexInternalShape*)***
  %vtable3 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %4, align 4
  %vfn4 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable3, i64 12
  %5 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn4, align 4
  %call5 = call float %5(%class.btConvexInternalShape* %3)
  store float %call5, float* %ref.tmp2, align 4
  %6 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %7 = bitcast %class.btConvexInternalShape* %6 to float (%class.btConvexInternalShape*)***
  %vtable7 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %7, align 4
  %vfn8 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable7, i64 12
  %8 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn8, align 4
  %call9 = call float %8(%class.btConvexInternalShape* %6)
  store float %call9, float* %ref.tmp6, align 4
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %oldMargin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %9 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %9, i32 0, i32 2
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %implicitShapeDimensionsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions, %class.btVector3* nonnull align 4 dereferenceable(16) %oldMargin)
  %10 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %11 = load float, float* %collisionMargin.addr, align 4
  call void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %10, float %11)
  %12 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %13 = bitcast %class.btConvexInternalShape* %12 to float (%class.btConvexInternalShape*)***
  %vtable12 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %13, align 4
  %vfn13 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable12, i64 12
  %14 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn13, align 4
  %call14 = call float %14(%class.btConvexInternalShape* %12)
  store float %call14, float* %ref.tmp11, align 4
  %15 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %16 = bitcast %class.btConvexInternalShape* %15 to float (%class.btConvexInternalShape*)***
  %vtable16 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %16, align 4
  %vfn17 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable16, i64 12
  %17 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn17, align 4
  %call18 = call float %17(%class.btConvexInternalShape* %15)
  store float %call18, float* %ref.tmp15, align 4
  %18 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %19 = bitcast %class.btConvexInternalShape* %18 to float (%class.btConvexInternalShape*)***
  %vtable20 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %19, align 4
  %vfn21 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable20, i64 12
  %20 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn21, align 4
  %call22 = call float %20(%class.btConvexInternalShape* %18)
  store float %call22, float* %ref.tmp19, align 4
  %call23 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %newMargin, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp19)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp24, %class.btVector3* nonnull align 4 dereferenceable(16) %implicitShapeDimensionsWithMargin, %class.btVector3* nonnull align 4 dereferenceable(16) %newMargin)
  %21 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions25 = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %21, i32 0, i32 2
  %22 = bitcast %class.btVector3* %m_implicitShapeDimensions25 to i8*
  %23 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK21btConvexInternalShape9getMarginEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK15btCylinderShape28calculateSerializeBufferSizeEv(%class.btCylinderShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  ret i32 60
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNK15btCylinderShape9serializeEPvP12btSerializer(%class.btCylinderShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btCylinderShapeData*, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btCylinderShapeData*
  store %struct.btCylinderShapeData* %1, %struct.btCylinderShapeData** %shapeData, align 4
  %2 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %3 = load %struct.btCylinderShapeData*, %struct.btCylinderShapeData** %shapeData, align 4
  %m_convexInternalShapeData = getelementptr inbounds %struct.btCylinderShapeData, %struct.btCylinderShapeData* %3, i32 0, i32 0
  %4 = bitcast %struct.btConvexInternalShapeData* %m_convexInternalShapeData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %2, i8* %4, %class.btSerializer* %5)
  %m_upAxis = getelementptr inbounds %class.btCylinderShape, %class.btCylinderShape* %this1, i32 0, i32 1
  %6 = load i32, i32* %m_upAxis, align 4
  %7 = load %struct.btCylinderShapeData*, %struct.btCylinderShapeData** %shapeData, align 4
  %m_upAxis2 = getelementptr inbounds %struct.btCylinderShapeData, %struct.btCylinderShapeData* %7, i32 0, i32 1
  store i32 %6, i32* %m_upAxis2, align 4
  ret i8* getelementptr inbounds ([20 x i8], [20 x i8]* @.str.1, i32 0, i32 0)
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK15btCylinderShape24localGetSupportingVertexERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btCylinderShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vec) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  %vec.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %vecnorm = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca float, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  store %class.btVector3* %vec, %class.btVector3** %vec.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %agg.result)
  %0 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4
  %1 = bitcast %class.btCylinderShape* %this1 to void (%class.btVector3*, %class.btCylinderShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btCylinderShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btCylinderShape*, %class.btVector3*)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btCylinderShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btCylinderShape*, %class.btVector3*)** %vtable, i64 17
  %2 = load void (%class.btVector3*, %class.btCylinderShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btCylinderShape*, %class.btVector3*)** %vfn, align 4
  call void %2(%class.btVector3* sret align 4 %ref.tmp, %class.btCylinderShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %3 = bitcast %class.btVector3* %agg.result to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %5 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %6 = bitcast %class.btConvexInternalShape* %5 to float (%class.btConvexInternalShape*)***
  %vtable2 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %6, align 4
  %vfn3 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable2, i64 12
  %7 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn3, align 4
  %call4 = call float %7(%class.btConvexInternalShape* %5)
  %cmp = fcmp une float %call4, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end18

if.then:                                          ; preds = %entry
  %8 = load %class.btVector3*, %class.btVector3** %vec.addr, align 4
  %9 = bitcast %class.btVector3* %vecnorm to i8*
  %10 = bitcast %class.btVector3* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %call5 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vecnorm)
  %cmp6 = fcmp olt float %call5, 0x3D10000000000000
  br i1 %cmp6, label %if.then7, label %if.end

if.then7:                                         ; preds = %if.then
  store float -1.000000e+00, float* %ref.tmp8, align 4
  store float -1.000000e+00, float* %ref.tmp9, align 4
  store float -1.000000e+00, float* %ref.tmp10, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %vecnorm, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  br label %if.end

if.end:                                           ; preds = %if.then7, %if.then
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %vecnorm)
  %11 = bitcast %class.btCylinderShape* %this1 to %class.btConvexInternalShape*
  %12 = bitcast %class.btConvexInternalShape* %11 to float (%class.btConvexInternalShape*)***
  %vtable14 = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %12, align 4
  %vfn15 = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable14, i64 12
  %13 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn15, align 4
  %call16 = call float %13(%class.btConvexInternalShape* %11)
  store float %call16, float* %ref.tmp13, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %vecnorm)
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp12)
  br label %if.end18

if.end18:                                         ; preds = %if.end, %entry
  ret void
}

declare void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4), float* nonnull align 4 dereferenceable(4), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_(%class.btConvexInternalShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv(%class.btConvexInternalShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3(%class.btConvexInternalShape* %this, i32 %index, %class.btVector3* nonnull align 4 dereferenceable(16) %penetrationVector) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %index.addr = alloca i32, align 4
  %penetrationVector.addr = alloca %class.btVector3*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store %class.btVector3* %penetrationVector, %class.btVector3** %penetrationVector.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %penetrationVector.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK15btCylinderShape9getRadiusEv(%class.btCylinderShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  call void @_ZNK15btCylinderShape24getHalfExtentsWithMarginEv(%class.btVector3* sret align 4 %ref.tmp, %class.btCylinderShape* %this1)
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %ref.tmp)
  %0 = load float, float* %call, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCylinderShapeX* @_ZN16btCylinderShapeXD2Ev(%class.btCylinderShapeX* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCylinderShapeX*, align 4
  store %class.btCylinderShapeX* %this, %class.btCylinderShapeX** %this.addr, align 4
  %this1 = load %class.btCylinderShapeX*, %class.btCylinderShapeX** %this.addr, align 4
  %0 = bitcast %class.btCylinderShapeX* %this1 to %class.btCylinderShape*
  %call = call %class.btCylinderShape* @_ZN15btCylinderShapeD2Ev(%class.btCylinderShape* %0) #7
  ret %class.btCylinderShapeX* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btCylinderShapeXD0Ev(%class.btCylinderShapeX* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCylinderShapeX*, align 4
  store %class.btCylinderShapeX* %this, %class.btCylinderShapeX** %this.addr, align 4
  %this1 = load %class.btCylinderShapeX*, %class.btCylinderShapeX** %this.addr, align 4
  %call = call %class.btCylinderShapeX* @_ZN16btCylinderShapeXD2Ev(%class.btCylinderShapeX* %this1) #7
  %0 = bitcast %class.btCylinderShapeX* %this1 to i8*
  call void @_ZN16btCylinderShapeXdlEPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK16btCylinderShapeX7getNameEv(%class.btCylinderShapeX* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCylinderShapeX*, align 4
  store %class.btCylinderShapeX* %this, %class.btCylinderShapeX** %this.addr, align 4
  %this1 = load %class.btCylinderShapeX*, %class.btCylinderShapeX** %this.addr, align 4
  ret i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.3, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK16btCylinderShapeX9getRadiusEv(%class.btCylinderShapeX* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCylinderShapeX*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btCylinderShapeX* %this, %class.btCylinderShapeX** %this.addr, align 4
  %this1 = load %class.btCylinderShapeX*, %class.btCylinderShapeX** %this.addr, align 4
  %0 = bitcast %class.btCylinderShapeX* %this1 to %class.btCylinderShape*
  call void @_ZNK15btCylinderShape24getHalfExtentsWithMarginEv(%class.btVector3* sret align 4 %ref.tmp, %class.btCylinderShape* %0)
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %ref.tmp)
  %1 = load float, float* %call, align 4
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCylinderShapeZ* @_ZN16btCylinderShapeZD2Ev(%class.btCylinderShapeZ* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCylinderShapeZ*, align 4
  store %class.btCylinderShapeZ* %this, %class.btCylinderShapeZ** %this.addr, align 4
  %this1 = load %class.btCylinderShapeZ*, %class.btCylinderShapeZ** %this.addr, align 4
  %0 = bitcast %class.btCylinderShapeZ* %this1 to %class.btCylinderShape*
  %call = call %class.btCylinderShape* @_ZN15btCylinderShapeD2Ev(%class.btCylinderShape* %0) #7
  ret %class.btCylinderShapeZ* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btCylinderShapeZD0Ev(%class.btCylinderShapeZ* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCylinderShapeZ*, align 4
  store %class.btCylinderShapeZ* %this, %class.btCylinderShapeZ** %this.addr, align 4
  %this1 = load %class.btCylinderShapeZ*, %class.btCylinderShapeZ** %this.addr, align 4
  %call = call %class.btCylinderShapeZ* @_ZN16btCylinderShapeZD2Ev(%class.btCylinderShapeZ* %this1) #7
  %0 = bitcast %class.btCylinderShapeZ* %this1 to i8*
  call void @_ZN16btCylinderShapeZdlEPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK16btCylinderShapeZ7getNameEv(%class.btCylinderShapeZ* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCylinderShapeZ*, align 4
  store %class.btCylinderShapeZ* %this, %class.btCylinderShapeZ** %this.addr, align 4
  %this1 = load %class.btCylinderShapeZ*, %class.btCylinderShapeZ** %this.addr, align 4
  ret i8* getelementptr inbounds ([10 x i8], [10 x i8]* @.str.4, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK16btCylinderShapeZ9getRadiusEv(%class.btCylinderShapeZ* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCylinderShapeZ*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btCylinderShapeZ* %this, %class.btCylinderShapeZ** %this.addr, align 4
  %this1 = load %class.btCylinderShapeZ*, %class.btCylinderShapeZ** %this.addr, align 4
  %0 = bitcast %class.btCylinderShapeZ* %this1 to %class.btCylinderShape*
  call void @_ZNK15btCylinderShape24getHalfExtentsWithMarginEv(%class.btVector3* sret align 4 %ref.tmp, %class.btCylinderShape* %0)
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %ref.tmp)
  %1 = load float, float* %call, align 4
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVector37minAxisEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 0
  %2 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 0, i32 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 1
  %5 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 1, i32 2
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN21btConvexInternalShape13setSafeMarginEff(%class.btConvexInternalShape* %this, float %minDimension, float %defaultMarginMultiplier) #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %minDimension.addr = alloca float, align 4
  %defaultMarginMultiplier.addr = alloca float, align 4
  %safeMargin = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  store float %minDimension, float* %minDimension.addr, align 4
  store float %defaultMarginMultiplier, float* %defaultMarginMultiplier.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %defaultMarginMultiplier.addr, align 4
  %1 = load float, float* %minDimension.addr, align 4
  %mul = fmul float %0, %1
  store float %mul, float* %safeMargin, align 4
  %2 = load float, float* %safeMargin, align 4
  %3 = bitcast %class.btConvexInternalShape* %this1 to float (%class.btConvexInternalShape*)***
  %vtable = load float (%class.btConvexInternalShape*)**, float (%class.btConvexInternalShape*)*** %3, align 4
  %vfn = getelementptr inbounds float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vtable, i64 12
  %4 = load float (%class.btConvexInternalShape*)*, float (%class.btConvexInternalShape*)** %vfn, align 4
  %call = call float %4(%class.btConvexInternalShape* %this1)
  %cmp = fcmp olt float %2, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load float, float* %safeMargin, align 4
  %6 = bitcast %class.btConvexInternalShape* %this1 to void (%class.btConvexInternalShape*, float)***
  %vtable2 = load void (%class.btConvexInternalShape*, float)**, void (%class.btConvexInternalShape*, float)*** %6, align 4
  %vfn3 = getelementptr inbounds void (%class.btConvexInternalShape*, float)*, void (%class.btConvexInternalShape*, float)** %vtable2, i64 11
  %7 = load void (%class.btConvexInternalShape*, float)*, void (%class.btConvexInternalShape*, float)** %vfn3, align 4
  call void %7(%class.btConvexInternalShape* %this1, float %5)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4
  %call2 = call float @_Z6btFabsf(float %1)
  store float %call2, float* %ref.tmp, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %2 = load float, float* %call6, align 4
  %call7 = call float @_Z6btFabsf(float %2)
  store float %call7, float* %ref.tmp3, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx10)
  %3 = load float, float* %call11, align 4
  %call12 = call float @_Z6btFabsf(float %3)
  store float %call12, float* %ref.tmp8, align 4
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx15)
  %4 = load float, float* %call16, align 4
  %call17 = call float @_Z6btFabsf(float %4)
  store float %call17, float* %ref.tmp13, align 4
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx20)
  %5 = load float, float* %call21, align 4
  %call22 = call float @_Z6btFabsf(float %5)
  store float %call22, float* %ref.tmp18, align 4
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx25)
  %6 = load float, float* %call26, align 4
  %call27 = call float @_Z6btFabsf(float %6)
  store float %call27, float* %ref.tmp23, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %7 = load float, float* %call31, align 4
  %call32 = call float @_Z6btFabsf(float %7)
  store float %call32, float* %ref.tmp28, align 4
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx35)
  %8 = load float, float* %call36, align 4
  %call37 = call float @_Z6btFabsf(float %8)
  store float %call37, float* %ref.tmp33, align 4
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx40)
  %9 = load float, float* %call41, align 4
  %call42 = call float @_Z6btFabsf(float %9)
  store float %call42, float* %ref.tmp38, align 4
  %call43 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexInternalShape* @_ZN21btConvexInternalShapeD2Ev(%class.btConvexInternalShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = bitcast %class.btConvexInternalShape* %this1 to %class.btConvexShape*
  %call = call %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* %0) #7
  ret %class.btConvexInternalShape* %this1
}

; Function Attrs: nounwind
declare %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned) unnamed_addr #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btCylinderShapedlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %div = fdiv float %1, %3
  store float %div, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %div8 = fdiv float %5, %7
  store float %div8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %div14 = fdiv float %9, %11
  store float %div14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

declare void @_ZN21btConvexInternalShape15setLocalScalingERK9btVector3(%class.btConvexInternalShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK15btCylinderShape9getUpAxisEv(%class.btCylinderShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  %m_upAxis = getelementptr inbounds %class.btCylinderShape, %class.btCylinderShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_upAxis, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btConvexInternalShape9setMarginEf(%class.btConvexInternalShape* %this, float %margin) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %margin.addr = alloca float, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  store float %0, float* %m_collisionMargin, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZNK21btConvexInternalShape9serializeEPvP12btSerializer(%class.btConvexInternalShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %shapeData = alloca %struct.btConvexInternalShapeData*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btConvexInternalShapeData*
  store %struct.btConvexInternalShapeData* %1, %struct.btConvexInternalShapeData** %shapeData, align 4
  %2 = bitcast %class.btConvexInternalShape* %this1 to %class.btCollisionShape*
  %3 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_collisionShapeData = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %3, i32 0, i32 0
  %4 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %2, i8* %4, %class.btSerializer* %5)
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  %6 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_implicitShapeDimensions2 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %6, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_implicitShapeDimensions, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_implicitShapeDimensions2)
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  %7 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_localScaling3 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %7, i32 0, i32 1
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %m_localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling3)
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %8 = load float, float* %m_collisionMargin, align 4
  %9 = load %struct.btConvexInternalShapeData*, %struct.btConvexInternalShapeData** %shapeData, align 4
  %m_collisionMargin4 = getelementptr inbounds %struct.btConvexInternalShapeData, %struct.btConvexInternalShapeData* %9, i32 0, i32 3
  store float %8, float* %m_collisionMargin4, align 4
  ret i8* getelementptr inbounds ([26 x i8], [26 x i8]* @.str.2, i32 0, i32 0)
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btCylinderShapeXdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btCylinderShapeZdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btCylinderShape.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
