; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/BroadphaseCollision/btDbvt.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/BroadphaseCollision/btDbvt.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btVector3 = type { [4 x float] }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.0 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.0 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%"struct.btDbvt::IWriter" = type { i32 (...)** }
%struct.btDbvtNodeEnumerator = type { %"struct.btDbvt::ICollide", %class.btAlignedObjectArray.5 }
%"struct.btDbvt::ICollide" = type { i32 (...)** }
%class.btAlignedObjectArray.5 = type <{ %class.btAlignedAllocator.6, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.6 = type { i8 }
%"struct.btDbvt::IClone" = type { i32 (...)** }
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, %"struct.btDbvt::sStkCLN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%"struct.btDbvt::sStkCLN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEED2Ev = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP10btDbvtNodeEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP10btDbvtNodeEixEi = comdat any

$_ZN20btAlignedObjectArrayIP10btDbvtNodeED2Ev = comdat any

$_ZNK10btDbvtNode10isinternalEv = comdat any

$_ZNK12btDbvtAabbMm7ContainERKS_ = comdat any

$_ZN12btDbvtAabbMm6ExpandERK9btVector3 = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN12btDbvtAabbMm12SignedExpandERK9btVector3 = comdat any

$_ZN20btDbvtNodeEnumeratorC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi = comdat any

$_ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE = comdat any

$_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi = comdat any

$_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE16findLinearSearchERKS2_ = comdat any

$_ZN20btDbvtNodeEnumeratorD2Ev = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE9push_backERKS1_ = comdat any

$_ZN6btDbvt7sStkCLNC2EPK10btDbvtNodePS1_ = comdat any

$_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEEixEi = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE8pop_backEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEED2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_ = comdat any

$_ZNK10btDbvtNode6isleafEv = comdat any

$_ZN20btAlignedObjectArrayIP10btDbvtNodeE9push_backERKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIP10btDbvtNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP10btDbvtNodeE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP10btDbvtNodeE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIP10btDbvtNodeE8pop_backEv = comdat any

$_ZNK12btDbvtAabbMm7LengthsEv = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_Z5MergeRK12btDbvtAabbMmS1_RS_ = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN10btDbvtNodeC2Ev = comdat any

$_ZN12btDbvtAabbMmC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK12btDbvtAabbMm6CenterEv = comdat any

$_Z5btDotRK9btVector3S1_ = comdat any

$_Z6btFabsf = comdat any

$_ZNK20btAlignedObjectArrayIP10btDbvtNodeEixEi = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN20btAlignedObjectArrayIP10btDbvtNodeE6resizeEiRKS1_ = comdat any

$_Z6btSwapI12btDbvtAabbMmEvRT_S2_ = comdat any

$_Z6SelectRK12btDbvtAabbMmS1_S1_ = comdat any

$_Z9ProximityRK12btDbvtAabbMmS1_ = comdat any

$_Z8NotEqualRK12btDbvtAabbMmS1_ = comdat any

$_ZN9btVector3mIERKS_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN9btVector34setXEf = comdat any

$_ZN9btVector34setYEf = comdat any

$_ZN9btVector34setZEf = comdat any

$_ZN6btDbvt8ICollideC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2Ev = comdat any

$_ZN20btDbvtNodeEnumeratorD0Ev = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ = comdat any

$_ZN20btDbvtNodeEnumerator7ProcessEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef = comdat any

$_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollideD2Ev = comdat any

$_ZN6btDbvt8ICollideD0Ev = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode = comdat any

$_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE10deallocateEPS2_ = comdat any

$_Z5btMaxIiERKT_S2_S2_ = comdat any

$_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE4initEv = comdat any

$_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_ = comdat any

$_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE8allocateEiPPKS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9allocSizeEi = comdat any

$_ZN18btAlignedAllocatorIP10btDbvtNodeLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP10btDbvtNodeE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP10btDbvtNodeE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP10btDbvtNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP10btDbvtNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP10btDbvtNodeLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP10btDbvtNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP10btDbvtNodeLj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE10deallocateEPS1_ = comdat any

$_ZN18btAlignedAllocatorIN6btDbvt7sStkCLNELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4initEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIN6btDbvt7sStkCLNELj16EE10deallocateEPS1_ = comdat any

$_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIN6btDbvt7sStkCLNELj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE9allocSizeEi = comdat any

$_ZTV20btDbvtNodeEnumerator = comdat any

$_ZTS20btDbvtNodeEnumerator = comdat any

$_ZTSN6btDbvt8ICollideE = comdat any

$_ZTIN6btDbvt8ICollideE = comdat any

$_ZTI20btDbvtNodeEnumerator = comdat any

$_ZTVN6btDbvt8ICollideE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@__const._ZL8bottomupP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeE.minidx = private unnamed_addr constant [2 x i32] [i32 -1, i32 -1], align 4
@_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis = internal global [3 x %class.btVector3] zeroinitializer, align 16
@_ZGVZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis = internal global i32 0, align 4
@_ZTV20btDbvtNodeEnumerator = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI20btDbvtNodeEnumerator to i8*), i8* bitcast (%struct.btDbvtNodeEnumerator* (%struct.btDbvtNodeEnumerator*)* @_ZN20btDbvtNodeEnumeratorD2Ev to i8*), i8* bitcast (void (%struct.btDbvtNodeEnumerator*)* @_ZN20btDbvtNodeEnumeratorD0Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ to i8*), i8* bitcast (void (%struct.btDbvtNodeEnumerator*, %struct.btDbvtNode*)* @_ZN20btDbvtNodeEnumerator7ProcessEPK10btDbvtNode to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, float)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode to i8*)] }, comdat, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS20btDbvtNodeEnumerator = linkonce_odr hidden constant [23 x i8] c"20btDbvtNodeEnumerator\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN6btDbvt8ICollideE = linkonce_odr hidden constant [19 x i8] c"N6btDbvt8ICollideE\00", comdat, align 1
@_ZTIN6btDbvt8ICollideE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTSN6btDbvt8ICollideE, i32 0, i32 0) }, comdat, align 4
@_ZTI20btDbvtNodeEnumerator = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([23 x i8], [23 x i8]* @_ZTS20btDbvtNodeEnumerator, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN6btDbvt8ICollideE to i8*) }, comdat, align 4
@_ZTVN6btDbvt8ICollideE = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN6btDbvt8ICollideE to i8*), i8* bitcast (%"struct.btDbvt::ICollide"* (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD2Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD0Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, float)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btDbvt.cpp, i8* null }]

@_ZN6btDbvtC1Ev = hidden unnamed_addr alias %struct.btDbvt* (%struct.btDbvt*), %struct.btDbvt* (%struct.btDbvt*)* @_ZN6btDbvtC2Ev
@_ZN6btDbvtD1Ev = hidden unnamed_addr alias %struct.btDbvt* (%struct.btDbvt*), %struct.btDbvt* (%struct.btDbvt*)* @_ZN6btDbvtD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %struct.btDbvt* @_ZN6btDbvtC2Ev(%struct.btDbvt* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %m_stkStack = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEC2Ev(%class.btAlignedObjectArray* %m_stkStack)
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %m_root, align 4
  %m_free = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 1
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %m_free, align 4
  %m_lkhd = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 2
  store i32 -1, i32* %m_lkhd, align 4
  %m_leaves = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 3
  store i32 0, i32* %m_leaves, align 4
  %m_opath = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 4
  store i32 0, i32* %m_opath, align 4
  ret %struct.btDbvt* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %struct.btDbvt* @_ZN6btDbvtD2Ev(%struct.btDbvt* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  call void @_ZN6btDbvt5clearEv(%struct.btDbvt* %this1)
  %m_stkStack = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEED2Ev(%class.btAlignedObjectArray* %m_stkStack) #6
  ret %struct.btDbvt* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN6btDbvt5clearEv(%struct.btDbvt* %this) #2 {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %tobool = icmp ne %struct.btDbvtNode* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_root2 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root2, align 4
  call void @_ZL17recursedeletenodeP6btDbvtP10btDbvtNode(%struct.btDbvt* %this1, %struct.btDbvtNode* %1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_free = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 1
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_free, align 4
  %3 = bitcast %struct.btDbvtNode* %2 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %3)
  %m_free3 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 1
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %m_free3, align 4
  %m_lkhd = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 2
  store i32 -1, i32* %m_lkhd, align 4
  %m_stkStack = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE5clearEv(%class.btAlignedObjectArray* %m_stkStack)
  %m_opath = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 4
  store i32 0, i32* %m_opath, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define internal void @_ZL17recursedeletenodeP6btDbvtP10btDbvtNode(%struct.btDbvt* %pdbvt, %struct.btDbvtNode* %node) #2 {
entry:
  %pdbvt.addr = alloca %struct.btDbvt*, align 4
  %node.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvt* %pdbvt, %struct.btDbvt** %pdbvt.addr, align 4
  store %struct.btDbvtNode* %node, %struct.btDbvtNode** %node.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %0)
  br i1 %call, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %1 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %3 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %2, i32 0, i32 2
  %childs = bitcast %union.anon.0* %3 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  call void @_ZL17recursedeletenodeP6btDbvtP10btDbvtNode(%struct.btDbvt* %1, %struct.btDbvtNode* %4)
  %5 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %7 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %6, i32 0, i32 2
  %childs1 = bitcast %union.anon.0* %7 to [2 x %struct.btDbvtNode*]*
  %arrayidx2 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs1, i32 0, i32 1
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx2, align 4
  call void @_ZL17recursedeletenodeP6btDbvtP10btDbvtNode(%struct.btDbvt* %5, %struct.btDbvtNode* %8)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %10 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %10, i32 0, i32 0
  %11 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %cmp = icmp eq %struct.btDbvtNode* %9, %11
  br i1 %cmp, label %if.then3, label %if.end5

if.then3:                                         ; preds = %if.end
  %12 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_root4 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %12, i32 0, i32 0
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %m_root4, align 4
  br label %if.end5

if.end5:                                          ; preds = %if.then3, %if.end
  %13 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %14 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  call void @_ZL10deletenodeP6btDbvtP10btDbvtNode(%struct.btDbvt* %13, %struct.btDbvtNode* %14)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN6btDbvt16optimizeBottomUpEv(%struct.btDbvt* %this) #2 {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  %leaves = alloca %class.btAlignedObjectArray.1, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %tobool = icmp ne %struct.btDbvtNode* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP10btDbvtNodeEC2Ev(%class.btAlignedObjectArray.1* %leaves)
  %m_leaves = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 3
  %1 = load i32, i32* %m_leaves, align 4
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.1* %leaves, i32 %1)
  %m_root2 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root2, align 4
  call void @_ZL11fetchleavesP6btDbvtP10btDbvtNodeR20btAlignedObjectArrayIS2_Ei(%struct.btDbvt* %this1, %struct.btDbvtNode* %2, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %leaves, i32 -1)
  call void @_ZL8bottomupP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeE(%struct.btDbvt* %this1, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %leaves)
  %call3 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %leaves, i32 0)
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call3, align 4
  %m_root4 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  store %struct.btDbvtNode* %3, %struct.btDbvtNode** %m_root4, align 4
  %call5 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP10btDbvtNodeED2Ev(%class.btAlignedObjectArray.1* %leaves) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP10btDbvtNodeEC2Ev(%class.btAlignedObjectArray.1* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.2* @_ZN18btAlignedAllocatorIP10btDbvtNodeLj16EEC2Ev(%class.btAlignedAllocator.2* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE4initEv(%class.btAlignedObjectArray.1* %this1)
  ret %class.btAlignedObjectArray.1* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.1* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btDbvtNode**, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray.1* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP10btDbvtNodeE8allocateEi(%class.btAlignedObjectArray.1* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btDbvtNode**
  store %struct.btDbvtNode** %2, %struct.btDbvtNode*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  %3 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call3, %struct.btDbvtNode** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray.1* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %struct.btDbvtNode** %4, %struct.btDbvtNode*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZL11fetchleavesP6btDbvtP10btDbvtNodeR20btAlignedObjectArrayIS2_Ei(%struct.btDbvt* %pdbvt, %struct.btDbvtNode* %root, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %leaves, i32 %depth) #2 {
entry:
  %pdbvt.addr = alloca %struct.btDbvt*, align 4
  %root.addr = alloca %struct.btDbvtNode*, align 4
  %leaves.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %depth.addr = alloca i32, align 4
  store %struct.btDbvt* %pdbvt, %struct.btDbvt** %pdbvt.addr, align 4
  store %struct.btDbvtNode* %root, %struct.btDbvtNode** %root.addr, align 4
  store %class.btAlignedObjectArray.1* %leaves, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  store i32 %depth, i32* %depth.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %0)
  br i1 %call, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %depth.addr, align 4
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %land.lhs.true
  %2 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %4 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %3, i32 0, i32 2
  %childs = bitcast %union.anon.0* %4 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %6 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %7 = load i32, i32* %depth.addr, align 4
  %sub = sub nsw i32 %7, 1
  call void @_ZL11fetchleavesP6btDbvtP10btDbvtNodeR20btAlignedObjectArrayIS2_Ei(%struct.btDbvt* %2, %struct.btDbvtNode* %5, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %6, i32 %sub)
  %8 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %10 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %9, i32 0, i32 2
  %childs1 = bitcast %union.anon.0* %10 to [2 x %struct.btDbvtNode*]*
  %arrayidx2 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs1, i32 0, i32 1
  %11 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx2, align 4
  %12 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %13 = load i32, i32* %depth.addr, align 4
  %sub3 = sub nsw i32 %13, 1
  call void @_ZL11fetchleavesP6btDbvtP10btDbvtNodeR20btAlignedObjectArrayIS2_Ei(%struct.btDbvt* %8, %struct.btDbvtNode* %11, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %12, i32 %sub3)
  %14 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %15 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  call void @_ZL10deletenodeP6btDbvtP10btDbvtNode(%struct.btDbvt* %14, %struct.btDbvtNode* %15)
  br label %if.end

if.else:                                          ; preds = %land.lhs.true, %entry
  %16 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE9push_backERKS1_(%class.btAlignedObjectArray.1* %16, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %root.addr)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZL8bottomupP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeE(%struct.btDbvt* %pdbvt, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %leaves) #2 {
entry:
  %pdbvt.addr = alloca %struct.btDbvt*, align 4
  %leaves.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %minsize = alloca float, align 4
  %minidx = alloca [2 x i32], align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %sz = alloca float, align 4
  %ref.tmp = alloca %struct.btDbvtAabbMm, align 4
  %n = alloca [2 x %struct.btDbvtNode*], align 4
  %p = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvt* %pdbvt, %struct.btDbvt** %pdbvt.addr, align 4
  store %class.btAlignedObjectArray.1* %leaves, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %for.end15, %entry
  %0 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %0)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  store float 0x47EFFFFFE0000000, float* %minsize, align 4
  %1 = bitcast [2 x i32]* %minidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 bitcast ([2 x i32]* @__const._ZL8bottomupP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeE.minidx to i8*), i32 8, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc13, %while.body
  %2 = load i32, i32* %i, align 4
  %3 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call1 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %3)
  %cmp2 = icmp slt i32 %2, %call1
  br i1 %cmp2, label %for.body, label %for.end15

for.body:                                         ; preds = %for.cond
  %4 = load i32, i32* %i, align 4
  %add = add nsw i32 %4, 1
  store i32 %add, i32* %j, align 4
  br label %for.cond3

for.cond3:                                        ; preds = %for.inc, %for.body
  %5 = load i32, i32* %j, align 4
  %6 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %6)
  %cmp5 = icmp slt i32 %5, %call4
  br i1 %cmp5, label %for.body6, label %for.end

for.body6:                                        ; preds = %for.cond3
  %7 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %8 = load i32, i32* %i, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %7, i32 %8)
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call7, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %9, i32 0, i32 0
  %10 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %11 = load i32, i32* %j, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %10, i32 %11)
  %12 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call8, align 4
  %volume9 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %12, i32 0, i32 0
  call void @_ZL5mergeRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* sret align 4 %ref.tmp, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume9)
  %call10 = call float @_ZL4sizeRK12btDbvtAabbMm(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %ref.tmp)
  store float %call10, float* %sz, align 4
  %13 = load float, float* %sz, align 4
  %14 = load float, float* %minsize, align 4
  %cmp11 = fcmp olt float %13, %14
  br i1 %cmp11, label %if.then, label %if.end

if.then:                                          ; preds = %for.body6
  %15 = load float, float* %sz, align 4
  store float %15, float* %minsize, align 4
  %16 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [2 x i32], [2 x i32]* %minidx, i32 0, i32 0
  store i32 %16, i32* %arrayidx, align 4
  %17 = load i32, i32* %j, align 4
  %arrayidx12 = getelementptr inbounds [2 x i32], [2 x i32]* %minidx, i32 0, i32 1
  store i32 %17, i32* %arrayidx12, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body6
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %18 = load i32, i32* %j, align 4
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond3

for.end:                                          ; preds = %for.cond3
  br label %for.inc13

for.inc13:                                        ; preds = %for.end
  %19 = load i32, i32* %i, align 4
  %inc14 = add nsw i32 %19, 1
  store i32 %inc14, i32* %i, align 4
  br label %for.cond

for.end15:                                        ; preds = %for.cond
  %arrayinit.begin = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %n, i32 0, i32 0
  %20 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %arrayidx16 = getelementptr inbounds [2 x i32], [2 x i32]* %minidx, i32 0, i32 0
  %21 = load i32, i32* %arrayidx16, align 4
  %call17 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %20, i32 %21)
  %22 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call17, align 4
  store %struct.btDbvtNode* %22, %struct.btDbvtNode** %arrayinit.begin, align 4
  %arrayinit.element = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %arrayinit.begin, i32 1
  %23 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %arrayidx18 = getelementptr inbounds [2 x i32], [2 x i32]* %minidx, i32 0, i32 1
  %24 = load i32, i32* %arrayidx18, align 4
  %call19 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %23, i32 %24)
  %25 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call19, align 4
  store %struct.btDbvtNode* %25, %struct.btDbvtNode** %arrayinit.element, align 4
  %26 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %arrayidx20 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %n, i32 0, i32 0
  %27 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx20, align 4
  %volume21 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %27, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %n, i32 0, i32 1
  %28 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx22, align 4
  %volume23 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %28, i32 0, i32 0
  %call24 = call %struct.btDbvtNode* @_ZL10createnodeP6btDbvtP10btDbvtNodeRK12btDbvtAabbMmS5_Pv(%struct.btDbvt* %26, %struct.btDbvtNode* null, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume21, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume23, i8* null)
  store %struct.btDbvtNode* %call24, %struct.btDbvtNode** %p, align 4
  %arrayidx25 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %n, i32 0, i32 0
  %29 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx25, align 4
  %30 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %31 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %30, i32 0, i32 2
  %childs = bitcast %union.anon.0* %31 to [2 x %struct.btDbvtNode*]*
  %arrayidx26 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  store %struct.btDbvtNode* %29, %struct.btDbvtNode** %arrayidx26, align 4
  %arrayidx27 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %n, i32 0, i32 1
  %32 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx27, align 4
  %33 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %34 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %33, i32 0, i32 2
  %childs28 = bitcast %union.anon.0* %34 to [2 x %struct.btDbvtNode*]*
  %arrayidx29 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs28, i32 0, i32 1
  store %struct.btDbvtNode* %32, %struct.btDbvtNode** %arrayidx29, align 4
  %35 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %arrayidx30 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %n, i32 0, i32 0
  %36 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx30, align 4
  %parent = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %36, i32 0, i32 1
  store %struct.btDbvtNode* %35, %struct.btDbvtNode** %parent, align 4
  %37 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %arrayidx31 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %n, i32 0, i32 1
  %38 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx31, align 4
  %parent32 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %38, i32 0, i32 1
  store %struct.btDbvtNode* %37, %struct.btDbvtNode** %parent32, align 4
  %39 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %40 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %arrayidx33 = getelementptr inbounds [2 x i32], [2 x i32]* %minidx, i32 0, i32 0
  %41 = load i32, i32* %arrayidx33, align 4
  %call34 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %40, i32 %41)
  store %struct.btDbvtNode* %39, %struct.btDbvtNode** %call34, align 4
  %42 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %arrayidx35 = getelementptr inbounds [2 x i32], [2 x i32]* %minidx, i32 0, i32 1
  %43 = load i32, i32* %arrayidx35, align 4
  %44 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call36 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %44)
  %sub = sub nsw i32 %call36, 1
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE4swapEii(%class.btAlignedObjectArray.1* %42, i32 %43, i32 %sub)
  %45 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE8pop_backEv(%class.btAlignedObjectArray.1* %45)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %0, i32 %1
  ret %struct.btDbvtNode** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP10btDbvtNodeED2Ev(%class.btAlignedObjectArray.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE5clearEv(%class.btAlignedObjectArray.1* %this1)
  ret %class.btAlignedObjectArray.1* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN6btDbvt15optimizeTopDownEi(%struct.btDbvt* %this, i32 %bu_treshold) #2 {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  %bu_treshold.addr = alloca i32, align 4
  %leaves = alloca %class.btAlignedObjectArray.1, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store i32 %bu_treshold, i32* %bu_treshold.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %tobool = icmp ne %struct.btDbvtNode* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP10btDbvtNodeEC2Ev(%class.btAlignedObjectArray.1* %leaves)
  %m_leaves = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 3
  %1 = load i32, i32* %m_leaves, align 4
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.1* %leaves, i32 %1)
  %m_root2 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root2, align 4
  call void @_ZL11fetchleavesP6btDbvtP10btDbvtNodeR20btAlignedObjectArrayIS2_Ei(%struct.btDbvt* %this1, %struct.btDbvtNode* %2, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %leaves, i32 -1)
  %3 = load i32, i32* %bu_treshold.addr, align 4
  %call3 = call %struct.btDbvtNode* @_ZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEi(%struct.btDbvt* %this1, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %leaves, i32 %3)
  %m_root4 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  store %struct.btDbvtNode* %call3, %struct.btDbvtNode** %m_root4, align 4
  %call5 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP10btDbvtNodeED2Ev(%class.btAlignedObjectArray.1* %leaves) #6
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define internal %struct.btDbvtNode* @_ZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEi(%struct.btDbvt* %pdbvt, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %leaves, i32 %bu_treshold) #2 {
entry:
  %retval = alloca %struct.btDbvtNode*, align 4
  %pdbvt.addr = alloca %struct.btDbvt*, align 4
  %leaves.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %bu_treshold.addr = alloca i32, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %vol = alloca %struct.btDbvtAabbMm, align 4
  %org = alloca %class.btVector3, align 4
  %sets = alloca [2 x %class.btAlignedObjectArray.1], align 16
  %bestaxis = alloca i32, align 4
  %bestmidp = alloca i32, align 4
  %splitcount = alloca [3 x [2 x i32]], align 16
  %i = alloca i32, align 4
  %x = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %j = alloca i32, align 4
  %midp = alloca i32, align 4
  %i70 = alloca i32, align 4
  %ni = alloca i32, align 4
  %node = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvt* %pdbvt, %struct.btDbvt** %pdbvt.addr, align 4
  store %class.btAlignedObjectArray.1* %leaves, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  store i32 %bu_treshold, i32* %bu_treshold.addr, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis) #6
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis, i32 0, i32 0), float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 1.000000e+00, float* %ref.tmp4, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis, i32 0, i32 1), float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([3 x %class.btVector3], [3 x %class.btVector3]* @_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis, i32 0, i32 2), float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  call void @__cxa_guard_release(i32* @_ZGVZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis) #6
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  %3 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call11 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %3)
  %cmp = icmp sgt i32 %call11, 1
  br i1 %cmp, label %if.then, label %if.end99

if.then:                                          ; preds = %init.end
  %4 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call12 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %4)
  %5 = load i32, i32* %bu_treshold.addr, align 4
  %cmp13 = icmp sgt i32 %call12, %5
  br i1 %cmp13, label %if.then14, label %if.else97

if.then14:                                        ; preds = %if.then
  %6 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  call void @_ZL6boundsRK20btAlignedObjectArrayIP10btDbvtNodeE(%struct.btDbvtAabbMm* sret align 4 %vol, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %6)
  call void @_ZNK12btDbvtAabbMm6CenterEv(%class.btVector3* sret align 4 %org, %struct.btDbvtAabbMm* %vol)
  %array.begin = getelementptr inbounds [2 x %class.btAlignedObjectArray.1], [2 x %class.btAlignedObjectArray.1]* %sets, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %array.begin, i32 2
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %if.then14
  %arrayctor.cur = phi %class.btAlignedObjectArray.1* [ %array.begin, %if.then14 ], [ %arrayctor.next, %arrayctor.loop ]
  %call15 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP10btDbvtNodeEC2Ev(%class.btAlignedObjectArray.1* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btAlignedObjectArray.1* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  store i32 -1, i32* %bestaxis, align 4
  %7 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call16 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %7)
  store i32 %call16, i32* %bestmidp, align 4
  %8 = bitcast [3 x [2 x i32]]* %splitcount to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %8, i8 0, i32 24, i1 false)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc29, %arrayctor.cont
  %9 = load i32, i32* %i, align 4
  %10 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call17 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %10)
  %cmp18 = icmp slt i32 %9, %call17
  br i1 %cmp18, label %for.body, label %for.end31

for.body:                                         ; preds = %for.cond
  %11 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %12 = load i32, i32* %i, align 4
  %call20 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %11, i32 %12)
  %13 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call20, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %13, i32 0, i32 0
  call void @_ZNK12btDbvtAabbMm6CenterEv(%class.btVector3* sret align 4 %ref.tmp19, %struct.btDbvtAabbMm* %volume)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %x, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp19, %class.btVector3* nonnull align 4 dereferenceable(16) %org)
  store i32 0, i32* %j, align 4
  br label %for.cond21

for.cond21:                                       ; preds = %for.inc, %for.body
  %14 = load i32, i32* %j, align 4
  %cmp22 = icmp slt i32 %14, 3
  br i1 %cmp22, label %for.body23, label %for.end

for.body23:                                       ; preds = %for.cond21
  %15 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds [3 x [2 x i32]], [3 x [2 x i32]]* %splitcount, i32 0, i32 %15
  %16 = load i32, i32* %j, align 4
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* @_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis, i32 0, i32 %16
  %call25 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %x, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx24)
  %cmp26 = fcmp ogt float %call25, 0.000000e+00
  %17 = zext i1 %cmp26 to i64
  %cond = select i1 %cmp26, i32 1, i32 0
  %arrayidx27 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx, i32 0, i32 %cond
  %18 = load i32, i32* %arrayidx27, align 4
  %inc = add nsw i32 %18, 1
  store i32 %inc, i32* %arrayidx27, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body23
  %19 = load i32, i32* %j, align 4
  %inc28 = add nsw i32 %19, 1
  store i32 %inc28, i32* %j, align 4
  br label %for.cond21

for.end:                                          ; preds = %for.cond21
  br label %for.inc29

for.inc29:                                        ; preds = %for.end
  %20 = load i32, i32* %i, align 4
  %inc30 = add nsw i32 %20, 1
  store i32 %inc30, i32* %i, align 4
  br label %for.cond

for.end31:                                        ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc51, %for.end31
  %21 = load i32, i32* %i, align 4
  %cmp33 = icmp slt i32 %21, 3
  br i1 %cmp33, label %for.body34, label %for.end53

for.body34:                                       ; preds = %for.cond32
  %22 = load i32, i32* %i, align 4
  %arrayidx35 = getelementptr inbounds [3 x [2 x i32]], [3 x [2 x i32]]* %splitcount, i32 0, i32 %22
  %arrayidx36 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx35, i32 0, i32 0
  %23 = load i32, i32* %arrayidx36, align 8
  %cmp37 = icmp sgt i32 %23, 0
  br i1 %cmp37, label %land.lhs.true, label %if.end50

land.lhs.true:                                    ; preds = %for.body34
  %24 = load i32, i32* %i, align 4
  %arrayidx38 = getelementptr inbounds [3 x [2 x i32]], [3 x [2 x i32]]* %splitcount, i32 0, i32 %24
  %arrayidx39 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx38, i32 0, i32 1
  %25 = load i32, i32* %arrayidx39, align 4
  %cmp40 = icmp sgt i32 %25, 0
  br i1 %cmp40, label %if.then41, label %if.end50

if.then41:                                        ; preds = %land.lhs.true
  %26 = load i32, i32* %i, align 4
  %arrayidx42 = getelementptr inbounds [3 x [2 x i32]], [3 x [2 x i32]]* %splitcount, i32 0, i32 %26
  %arrayidx43 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx42, i32 0, i32 0
  %27 = load i32, i32* %arrayidx43, align 8
  %28 = load i32, i32* %i, align 4
  %arrayidx44 = getelementptr inbounds [3 x [2 x i32]], [3 x [2 x i32]]* %splitcount, i32 0, i32 %28
  %arrayidx45 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx44, i32 0, i32 1
  %29 = load i32, i32* %arrayidx45, align 4
  %sub = sub nsw i32 %27, %29
  %conv = sitofp i32 %sub to float
  %call46 = call float @_Z6btFabsf(float %conv)
  %conv47 = fptosi float %call46 to i32
  store i32 %conv47, i32* %midp, align 4
  %30 = load i32, i32* %midp, align 4
  %31 = load i32, i32* %bestmidp, align 4
  %cmp48 = icmp slt i32 %30, %31
  br i1 %cmp48, label %if.then49, label %if.end

if.then49:                                        ; preds = %if.then41
  %32 = load i32, i32* %i, align 4
  store i32 %32, i32* %bestaxis, align 4
  %33 = load i32, i32* %midp, align 4
  store i32 %33, i32* %bestmidp, align 4
  br label %if.end

if.end:                                           ; preds = %if.then49, %if.then41
  br label %if.end50

if.end50:                                         ; preds = %if.end, %land.lhs.true, %for.body34
  br label %for.inc51

for.inc51:                                        ; preds = %if.end50
  %34 = load i32, i32* %i, align 4
  %inc52 = add nsw i32 %34, 1
  store i32 %inc52, i32* %i, align 4
  br label %for.cond32

for.end53:                                        ; preds = %for.cond32
  %35 = load i32, i32* %bestaxis, align 4
  %cmp54 = icmp sge i32 %35, 0
  br i1 %cmp54, label %if.then55, label %if.else

if.then55:                                        ; preds = %for.end53
  %arrayidx56 = getelementptr inbounds [2 x %class.btAlignedObjectArray.1], [2 x %class.btAlignedObjectArray.1]* %sets, i32 0, i32 0
  %36 = load i32, i32* %bestaxis, align 4
  %arrayidx57 = getelementptr inbounds [3 x [2 x i32]], [3 x [2 x i32]]* %splitcount, i32 0, i32 %36
  %arrayidx58 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx57, i32 0, i32 0
  %37 = load i32, i32* %arrayidx58, align 8
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.1* %arrayidx56, i32 %37)
  %arrayidx59 = getelementptr inbounds [2 x %class.btAlignedObjectArray.1], [2 x %class.btAlignedObjectArray.1]* %sets, i32 0, i32 1
  %38 = load i32, i32* %bestaxis, align 4
  %arrayidx60 = getelementptr inbounds [3 x [2 x i32]], [3 x [2 x i32]]* %splitcount, i32 0, i32 %38
  %arrayidx61 = getelementptr inbounds [2 x i32], [2 x i32]* %arrayidx60, i32 0, i32 1
  %39 = load i32, i32* %arrayidx61, align 4
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.1* %arrayidx59, i32 %39)
  %40 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %arrayidx62 = getelementptr inbounds [2 x %class.btAlignedObjectArray.1], [2 x %class.btAlignedObjectArray.1]* %sets, i32 0, i32 0
  %arrayidx63 = getelementptr inbounds [2 x %class.btAlignedObjectArray.1], [2 x %class.btAlignedObjectArray.1]* %sets, i32 0, i32 1
  %41 = load i32, i32* %bestaxis, align 4
  %arrayidx64 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* @_ZZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEiE4axis, i32 0, i32 %41
  call void @_ZL5splitRK20btAlignedObjectArrayIP10btDbvtNodeERS2_S5_RK9btVector3S8_(%class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %40, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %arrayidx62, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %arrayidx63, %class.btVector3* nonnull align 4 dereferenceable(16) %org, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx64)
  br label %if.end80

if.else:                                          ; preds = %for.end53
  %arrayidx65 = getelementptr inbounds [2 x %class.btAlignedObjectArray.1], [2 x %class.btAlignedObjectArray.1]* %sets, i32 0, i32 0
  %42 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call66 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %42)
  %div = sdiv i32 %call66, 2
  %add = add nsw i32 %div, 1
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.1* %arrayidx65, i32 %add)
  %arrayidx67 = getelementptr inbounds [2 x %class.btAlignedObjectArray.1], [2 x %class.btAlignedObjectArray.1]* %sets, i32 0, i32 1
  %43 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call68 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %43)
  %div69 = sdiv i32 %call68, 2
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.1* %arrayidx67, i32 %div69)
  store i32 0, i32* %i70, align 4
  %44 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call71 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %44)
  store i32 %call71, i32* %ni, align 4
  br label %for.cond72

for.cond72:                                       ; preds = %for.inc77, %if.else
  %45 = load i32, i32* %i70, align 4
  %46 = load i32, i32* %ni, align 4
  %cmp73 = icmp slt i32 %45, %46
  br i1 %cmp73, label %for.body74, label %for.end79

for.body74:                                       ; preds = %for.cond72
  %47 = load i32, i32* %i70, align 4
  %and = and i32 %47, 1
  %arrayidx75 = getelementptr inbounds [2 x %class.btAlignedObjectArray.1], [2 x %class.btAlignedObjectArray.1]* %sets, i32 0, i32 %and
  %48 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %49 = load i32, i32* %i70, align 4
  %call76 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %48, i32 %49)
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE9push_backERKS1_(%class.btAlignedObjectArray.1* %arrayidx75, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %call76)
  br label %for.inc77

for.inc77:                                        ; preds = %for.body74
  %50 = load i32, i32* %i70, align 4
  %inc78 = add nsw i32 %50, 1
  store i32 %inc78, i32* %i70, align 4
  br label %for.cond72

for.end79:                                        ; preds = %for.cond72
  br label %if.end80

if.end80:                                         ; preds = %for.end79, %if.then55
  %51 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %call81 = call %struct.btDbvtNode* @_ZL10createnodeP6btDbvtP10btDbvtNodeRK12btDbvtAabbMmPv(%struct.btDbvt* %51, %struct.btDbvtNode* null, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %vol, i8* null)
  store %struct.btDbvtNode* %call81, %struct.btDbvtNode** %node, align 4
  %52 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %arrayidx82 = getelementptr inbounds [2 x %class.btAlignedObjectArray.1], [2 x %class.btAlignedObjectArray.1]* %sets, i32 0, i32 0
  %53 = load i32, i32* %bu_treshold.addr, align 4
  %call83 = call %struct.btDbvtNode* @_ZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEi(%struct.btDbvt* %52, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %arrayidx82, i32 %53)
  %54 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %55 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %54, i32 0, i32 2
  %childs = bitcast %union.anon.0* %55 to [2 x %struct.btDbvtNode*]*
  %arrayidx84 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  store %struct.btDbvtNode* %call83, %struct.btDbvtNode** %arrayidx84, align 4
  %56 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %arrayidx85 = getelementptr inbounds [2 x %class.btAlignedObjectArray.1], [2 x %class.btAlignedObjectArray.1]* %sets, i32 0, i32 1
  %57 = load i32, i32* %bu_treshold.addr, align 4
  %call86 = call %struct.btDbvtNode* @_ZL7topdownP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeEi(%struct.btDbvt* %56, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %arrayidx85, i32 %57)
  %58 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %59 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %58, i32 0, i32 2
  %childs87 = bitcast %union.anon.0* %59 to [2 x %struct.btDbvtNode*]*
  %arrayidx88 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs87, i32 0, i32 1
  store %struct.btDbvtNode* %call86, %struct.btDbvtNode** %arrayidx88, align 4
  %60 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %61 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %62 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %61, i32 0, i32 2
  %childs89 = bitcast %union.anon.0* %62 to [2 x %struct.btDbvtNode*]*
  %arrayidx90 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs89, i32 0, i32 0
  %63 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx90, align 4
  %parent = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %63, i32 0, i32 1
  store %struct.btDbvtNode* %60, %struct.btDbvtNode** %parent, align 4
  %64 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %65 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %66 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %65, i32 0, i32 2
  %childs91 = bitcast %union.anon.0* %66 to [2 x %struct.btDbvtNode*]*
  %arrayidx92 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs91, i32 0, i32 1
  %67 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx92, align 4
  %parent93 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %67, i32 0, i32 1
  store %struct.btDbvtNode* %64, %struct.btDbvtNode** %parent93, align 4
  %68 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  store %struct.btDbvtNode* %68, %struct.btDbvtNode** %retval, align 4
  %array.begin94 = getelementptr inbounds [2 x %class.btAlignedObjectArray.1], [2 x %class.btAlignedObjectArray.1]* %sets, i32 0, i32 0
  %69 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %array.begin94, i32 2
  br label %arraydestroy.body

arraydestroy.body:                                ; preds = %arraydestroy.body, %if.end80
  %arraydestroy.elementPast = phi %class.btAlignedObjectArray.1* [ %69, %if.end80 ], [ %arraydestroy.element, %arraydestroy.body ]
  %arraydestroy.element = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %arraydestroy.elementPast, i32 -1
  %call95 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIP10btDbvtNodeED2Ev(%class.btAlignedObjectArray.1* %arraydestroy.element) #6
  %arraydestroy.done = icmp eq %class.btAlignedObjectArray.1* %arraydestroy.element, %array.begin94
  br i1 %arraydestroy.done, label %arraydestroy.done96, label %arraydestroy.body

arraydestroy.done96:                              ; preds = %arraydestroy.body
  br label %return

if.else97:                                        ; preds = %if.then
  %70 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %71 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  call void @_ZL8bottomupP6btDbvtR20btAlignedObjectArrayIP10btDbvtNodeE(%struct.btDbvt* %70, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %71)
  %72 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call98 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %72, i32 0)
  %73 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call98, align 4
  store %struct.btDbvtNode* %73, %struct.btDbvtNode** %retval, align 4
  br label %return

if.end99:                                         ; preds = %init.end
  %74 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call100 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %74, i32 0)
  %75 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call100, align 4
  store %struct.btDbvtNode* %75, %struct.btDbvtNode** %retval, align 4
  br label %return

return:                                           ; preds = %if.end99, %if.else97, %arraydestroy.done96
  %76 = load %struct.btDbvtNode*, %struct.btDbvtNode** %retval, align 4
  ret %struct.btDbvtNode* %76
}

; Function Attrs: noinline optnone
define hidden void @_ZN6btDbvt19optimizeIncrementalEi(%struct.btDbvt* %this, i32 %passes) #2 {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  %passes.addr = alloca i32, align 4
  %node = alloca %struct.btDbvtNode*, align 4
  %bit = alloca i32, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store i32 %passes, i32* %passes.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %0 = load i32, i32* %passes.addr, align 4
  %cmp = icmp slt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_leaves = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 3
  %1 = load i32, i32* %m_leaves, align 4
  store i32 %1, i32* %passes.addr, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %tobool = icmp ne %struct.btDbvtNode* %2, null
  br i1 %tobool, label %land.lhs.true, label %if.end10

land.lhs.true:                                    ; preds = %if.end
  %3 = load i32, i32* %passes.addr, align 4
  %cmp2 = icmp sgt i32 %3, 0
  br i1 %cmp2, label %if.then3, label %if.end10

if.then3:                                         ; preds = %land.lhs.true
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then3
  %m_root4 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root4, align 4
  store %struct.btDbvtNode* %4, %struct.btDbvtNode** %node, align 4
  store i32 0, i32* %bit, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %5)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %m_root5 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %call6 = call %struct.btDbvtNode* @_ZL4sortP10btDbvtNodeRS0_(%struct.btDbvtNode* %6, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %m_root5)
  %7 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %call6, i32 0, i32 2
  %childs = bitcast %union.anon.0* %7 to [2 x %struct.btDbvtNode*]*
  %m_opath = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 4
  %8 = load i32, i32* %m_opath, align 4
  %9 = load i32, i32* %bit, align 4
  %shr = lshr i32 %8, %9
  %and = and i32 %shr, 1
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 %and
  %10 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  store %struct.btDbvtNode* %10, %struct.btDbvtNode** %node, align 4
  %11 = load i32, i32* %bit, align 4
  %add = add i32 %11, 1
  %and7 = and i32 %add, 31
  store i32 %and7, i32* %bit, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %12 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  call void @_ZN6btDbvt6updateEP10btDbvtNodei(%struct.btDbvt* %this1, %struct.btDbvtNode* %12, i32 -1)
  %m_opath8 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 4
  %13 = load i32, i32* %m_opath8, align 4
  %inc = add i32 %13, 1
  store i32 %inc, i32* %m_opath8, align 4
  br label %do.cond

do.cond:                                          ; preds = %while.end
  %14 = load i32, i32* %passes.addr, align 4
  %dec = add nsw i32 %14, -1
  store i32 %dec, i32* %passes.addr, align 4
  %tobool9 = icmp ne i32 %dec, 0
  br i1 %tobool9, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  br label %if.end10

if.end10:                                         ; preds = %do.end, %land.lhs.true, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this1)
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: noinline optnone
define internal %struct.btDbvtNode* @_ZL4sortP10btDbvtNodeRS0_(%struct.btDbvtNode* %n, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %r) #2 {
entry:
  %retval = alloca %struct.btDbvtNode*, align 4
  %n.addr = alloca %struct.btDbvtNode*, align 4
  %r.addr = alloca %struct.btDbvtNode**, align 4
  %p = alloca %struct.btDbvtNode*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %s = alloca %struct.btDbvtNode*, align 4
  %q = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %n, %struct.btDbvtNode** %n.addr, align 4
  store %struct.btDbvtNode** %r, %struct.btDbvtNode*** %r.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %parent = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %0, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent, align 4
  store %struct.btDbvtNode* %1, %struct.btDbvtNode** %p, align 4
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %cmp = icmp ugt %struct.btDbvtNode* %2, %3
  br i1 %cmp, label %if.then, label %if.end28

if.then:                                          ; preds = %entry
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %call = call i32 @_ZL7indexofPK10btDbvtNode(%struct.btDbvtNode* %4)
  store i32 %call, i32* %i, align 4
  %5 = load i32, i32* %i, align 4
  %sub = sub nsw i32 1, %5
  store i32 %sub, i32* %j, align 4
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %7 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %6, i32 0, i32 2
  %childs = bitcast %union.anon.0* %7 to [2 x %struct.btDbvtNode*]*
  %8 = load i32, i32* %j, align 4
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 %8
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  store %struct.btDbvtNode* %9, %struct.btDbvtNode** %s, align 4
  %10 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %parent1 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %10, i32 0, i32 1
  %11 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent1, align 4
  store %struct.btDbvtNode* %11, %struct.btDbvtNode** %q, align 4
  %12 = load %struct.btDbvtNode*, %struct.btDbvtNode** %q, align 4
  %tobool = icmp ne %struct.btDbvtNode* %12, null
  br i1 %tobool, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %13 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %14 = load %struct.btDbvtNode*, %struct.btDbvtNode** %q, align 4
  %15 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %14, i32 0, i32 2
  %childs3 = bitcast %union.anon.0* %15 to [2 x %struct.btDbvtNode*]*
  %16 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %call4 = call i32 @_ZL7indexofPK10btDbvtNode(%struct.btDbvtNode* %16)
  %arrayidx5 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs3, i32 0, i32 %call4
  store %struct.btDbvtNode* %13, %struct.btDbvtNode** %arrayidx5, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  %17 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %18 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %r.addr, align 4
  store %struct.btDbvtNode* %17, %struct.btDbvtNode** %18, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then2
  %19 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %20 = load %struct.btDbvtNode*, %struct.btDbvtNode** %s, align 4
  %parent6 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %20, i32 0, i32 1
  store %struct.btDbvtNode* %19, %struct.btDbvtNode** %parent6, align 4
  %21 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %22 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %parent7 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %22, i32 0, i32 1
  store %struct.btDbvtNode* %21, %struct.btDbvtNode** %parent7, align 4
  %23 = load %struct.btDbvtNode*, %struct.btDbvtNode** %q, align 4
  %24 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %parent8 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %24, i32 0, i32 1
  store %struct.btDbvtNode* %23, %struct.btDbvtNode** %parent8, align 4
  %25 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %26 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %25, i32 0, i32 2
  %childs9 = bitcast %union.anon.0* %26 to [2 x %struct.btDbvtNode*]*
  %arrayidx10 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs9, i32 0, i32 0
  %27 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx10, align 4
  %28 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %29 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %28, i32 0, i32 2
  %childs11 = bitcast %union.anon.0* %29 to [2 x %struct.btDbvtNode*]*
  %arrayidx12 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs11, i32 0, i32 0
  store %struct.btDbvtNode* %27, %struct.btDbvtNode** %arrayidx12, align 4
  %30 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %31 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %30, i32 0, i32 2
  %childs13 = bitcast %union.anon.0* %31 to [2 x %struct.btDbvtNode*]*
  %arrayidx14 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs13, i32 0, i32 1
  %32 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx14, align 4
  %33 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %34 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %33, i32 0, i32 2
  %childs15 = bitcast %union.anon.0* %34 to [2 x %struct.btDbvtNode*]*
  %arrayidx16 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs15, i32 0, i32 1
  store %struct.btDbvtNode* %32, %struct.btDbvtNode** %arrayidx16, align 4
  %35 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %36 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %37 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %36, i32 0, i32 2
  %childs17 = bitcast %union.anon.0* %37 to [2 x %struct.btDbvtNode*]*
  %arrayidx18 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs17, i32 0, i32 0
  %38 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx18, align 4
  %parent19 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %38, i32 0, i32 1
  store %struct.btDbvtNode* %35, %struct.btDbvtNode** %parent19, align 4
  %39 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %40 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %41 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %40, i32 0, i32 2
  %childs20 = bitcast %union.anon.0* %41 to [2 x %struct.btDbvtNode*]*
  %arrayidx21 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs20, i32 0, i32 1
  %42 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx21, align 4
  %parent22 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %42, i32 0, i32 1
  store %struct.btDbvtNode* %39, %struct.btDbvtNode** %parent22, align 4
  %43 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %44 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %45 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %44, i32 0, i32 2
  %childs23 = bitcast %union.anon.0* %45 to [2 x %struct.btDbvtNode*]*
  %46 = load i32, i32* %i, align 4
  %arrayidx24 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs23, i32 0, i32 %46
  store %struct.btDbvtNode* %43, %struct.btDbvtNode** %arrayidx24, align 4
  %47 = load %struct.btDbvtNode*, %struct.btDbvtNode** %s, align 4
  %48 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %49 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %48, i32 0, i32 2
  %childs25 = bitcast %union.anon.0* %49 to [2 x %struct.btDbvtNode*]*
  %50 = load i32, i32* %j, align 4
  %arrayidx26 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs25, i32 0, i32 %50
  store %struct.btDbvtNode* %47, %struct.btDbvtNode** %arrayidx26, align 4
  %51 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %51, i32 0, i32 0
  %52 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %volume27 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %52, i32 0, i32 0
  call void @_Z6btSwapI12btDbvtAabbMmEvRT_S2_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume27)
  %53 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p, align 4
  store %struct.btDbvtNode* %53, %struct.btDbvtNode** %retval, align 4
  br label %return

if.end28:                                         ; preds = %entry
  %54 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  store %struct.btDbvtNode* %54, %struct.btDbvtNode** %retval, align 4
  br label %return

return:                                           ; preds = %if.end28, %if.end
  %55 = load %struct.btDbvtNode*, %struct.btDbvtNode** %retval, align 4
  ret %struct.btDbvtNode* %55
}

; Function Attrs: noinline optnone
define hidden void @_ZN6btDbvt6updateEP10btDbvtNodei(%struct.btDbvt* %this, %struct.btDbvtNode* %leaf, i32 %lookahead) #2 {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  %leaf.addr = alloca %struct.btDbvtNode*, align 4
  %lookahead.addr = alloca i32, align 4
  %root = alloca %struct.btDbvtNode*, align 4
  %i = alloca i32, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store %struct.btDbvtNode* %leaf, %struct.btDbvtNode** %leaf.addr, align 4
  store i32 %lookahead, i32* %lookahead.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %call = call %struct.btDbvtNode* @_ZL10removeleafP6btDbvtP10btDbvtNode(%struct.btDbvt* %this1, %struct.btDbvtNode* %0)
  store %struct.btDbvtNode* %call, %struct.btDbvtNode** %root, align 4
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root, align 4
  %tobool = icmp ne %struct.btDbvtNode* %1, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %lookahead.addr, align 4
  %cmp = icmp sge i32 %2, 0
  br i1 %cmp, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then2
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %lookahead.addr, align 4
  %cmp3 = icmp slt i32 %3, %4
  br i1 %cmp3, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root, align 4
  %parent = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %5, i32 0, i32 1
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent, align 4
  %tobool4 = icmp ne %struct.btDbvtNode* %6, null
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %7 = phi i1 [ false, %for.cond ], [ %tobool4, %land.rhs ]
  br i1 %7, label %for.body, label %for.end

for.body:                                         ; preds = %land.end
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root, align 4
  %parent5 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %8, i32 0, i32 1
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent5, align 4
  store %struct.btDbvtNode* %9, %struct.btDbvtNode** %root, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %land.end
  br label %if.end

if.else:                                          ; preds = %if.then
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %11 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  store %struct.btDbvtNode* %11, %struct.btDbvtNode** %root, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %for.end
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  %12 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root, align 4
  %13 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  call void @_ZL10insertleafP6btDbvtP10btDbvtNodeS2_(%struct.btDbvt* %this1, %struct.btDbvtNode* %12, %struct.btDbvtNode* %13)
  ret void
}

; Function Attrs: noinline optnone
define hidden %struct.btDbvtNode* @_ZN6btDbvt6insertERK12btDbvtAabbMmPv(%struct.btDbvt* %this, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, i8* %data) #2 {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  %volume.addr = alloca %struct.btDbvtAabbMm*, align 4
  %data.addr = alloca i8*, align 4
  %leaf = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store %struct.btDbvtAabbMm* %volume, %struct.btDbvtAabbMm** %volume.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %0 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume.addr, align 4
  %1 = load i8*, i8** %data.addr, align 4
  %call = call %struct.btDbvtNode* @_ZL10createnodeP6btDbvtP10btDbvtNodeRK12btDbvtAabbMmPv(%struct.btDbvt* %this1, %struct.btDbvtNode* null, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %0, i8* %1)
  store %struct.btDbvtNode* %call, %struct.btDbvtNode** %leaf, align 4
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf, align 4
  call void @_ZL10insertleafP6btDbvtP10btDbvtNodeS2_(%struct.btDbvt* %this1, %struct.btDbvtNode* %2, %struct.btDbvtNode* %3)
  %m_leaves = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 3
  %4 = load i32, i32* %m_leaves, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %m_leaves, align 4
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf, align 4
  ret %struct.btDbvtNode* %5
}

; Function Attrs: noinline optnone
define internal %struct.btDbvtNode* @_ZL10createnodeP6btDbvtP10btDbvtNodeRK12btDbvtAabbMmPv(%struct.btDbvt* %pdbvt, %struct.btDbvtNode* %parent, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, i8* %data) #2 {
entry:
  %pdbvt.addr = alloca %struct.btDbvt*, align 4
  %parent.addr = alloca %struct.btDbvtNode*, align 4
  %volume.addr = alloca %struct.btDbvtAabbMm*, align 4
  %data.addr = alloca i8*, align 4
  %node = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvt* %pdbvt, %struct.btDbvt** %pdbvt.addr, align 4
  store %struct.btDbvtNode* %parent, %struct.btDbvtNode** %parent.addr, align 4
  store %struct.btDbvtAabbMm* %volume, %struct.btDbvtAabbMm** %volume.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %0 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent.addr, align 4
  %2 = load i8*, i8** %data.addr, align 4
  %call = call %struct.btDbvtNode* @_ZL10createnodeP6btDbvtP10btDbvtNodePv(%struct.btDbvt* %0, %struct.btDbvtNode* %1, i8* %2)
  store %struct.btDbvtNode* %call, %struct.btDbvtNode** %node, align 4
  %3 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume.addr, align 4
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %volume1 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %4, i32 0, i32 0
  %5 = bitcast %struct.btDbvtAabbMm* %volume1 to i8*
  %6 = bitcast %struct.btDbvtAabbMm* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 32, i1 false)
  %7 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  ret %struct.btDbvtNode* %7
}

; Function Attrs: noinline optnone
define internal void @_ZL10insertleafP6btDbvtP10btDbvtNodeS2_(%struct.btDbvt* %pdbvt, %struct.btDbvtNode* %root, %struct.btDbvtNode* %leaf) #2 {
entry:
  %pdbvt.addr = alloca %struct.btDbvt*, align 4
  %root.addr = alloca %struct.btDbvtNode*, align 4
  %leaf.addr = alloca %struct.btDbvtNode*, align 4
  %prev = alloca %struct.btDbvtNode*, align 4
  %node = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvt* %pdbvt, %struct.btDbvt** %pdbvt.addr, align 4
  store %struct.btDbvtNode* %root, %struct.btDbvtNode** %root.addr, align 4
  store %struct.btDbvtNode* %leaf, %struct.btDbvtNode** %leaf.addr, align 4
  %0 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %0, i32 0, i32 0
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %tobool = icmp ne %struct.btDbvtNode* %1, null
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %3 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_root1 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %3, i32 0, i32 0
  store %struct.btDbvtNode* %2, %struct.btDbvtNode** %m_root1, align 4
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %parent = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %4, i32 0, i32 1
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %parent, align 4
  br label %if.end52

if.else:                                          ; preds = %entry
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %5)
  br i1 %call, label %if.end, label %if.then2

if.then2:                                         ; preds = %if.else
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then2
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %7 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %6, i32 0, i32 2
  %childs = bitcast %union.anon.0* %7 to [2 x %struct.btDbvtNode*]*
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %8, i32 0, i32 0
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %10 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %9, i32 0, i32 2
  %childs3 = bitcast %union.anon.0* %10 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs3, i32 0, i32 0
  %11 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %volume4 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %11, i32 0, i32 0
  %12 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %13 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %12, i32 0, i32 2
  %childs5 = bitcast %union.anon.0* %13 to [2 x %struct.btDbvtNode*]*
  %arrayidx6 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs5, i32 0, i32 1
  %14 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx6, align 4
  %volume7 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %14, i32 0, i32 0
  %call8 = call i32 @_Z6SelectRK12btDbvtAabbMmS1_S1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume4, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume7)
  %arrayidx9 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 %call8
  %15 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx9, align 4
  store %struct.btDbvtNode* %15, %struct.btDbvtNode** %root.addr, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %16 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %call10 = call zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %16)
  %lnot = xor i1 %call10, true
  br i1 %lnot, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  br label %if.end

if.end:                                           ; preds = %do.end, %if.else
  %17 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %parent11 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %17, i32 0, i32 1
  %18 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent11, align 4
  store %struct.btDbvtNode* %18, %struct.btDbvtNode** %prev, align 4
  %19 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %20 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %21 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %volume12 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %21, i32 0, i32 0
  %22 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %volume13 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %22, i32 0, i32 0
  %call14 = call %struct.btDbvtNode* @_ZL10createnodeP6btDbvtP10btDbvtNodeRK12btDbvtAabbMmS5_Pv(%struct.btDbvt* %19, %struct.btDbvtNode* %20, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume12, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume13, i8* null)
  store %struct.btDbvtNode* %call14, %struct.btDbvtNode** %node, align 4
  %23 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %tobool15 = icmp ne %struct.btDbvtNode* %23, null
  br i1 %tobool15, label %if.then16, label %if.else43

if.then16:                                        ; preds = %if.end
  %24 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %25 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %26 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %25, i32 0, i32 2
  %childs17 = bitcast %union.anon.0* %26 to [2 x %struct.btDbvtNode*]*
  %27 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %call18 = call i32 @_ZL7indexofPK10btDbvtNode(%struct.btDbvtNode* %27)
  %arrayidx19 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs17, i32 0, i32 %call18
  store %struct.btDbvtNode* %24, %struct.btDbvtNode** %arrayidx19, align 4
  %28 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %29 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %30 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %29, i32 0, i32 2
  %childs20 = bitcast %union.anon.0* %30 to [2 x %struct.btDbvtNode*]*
  %arrayidx21 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs20, i32 0, i32 0
  store %struct.btDbvtNode* %28, %struct.btDbvtNode** %arrayidx21, align 4
  %31 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %32 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %parent22 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %32, i32 0, i32 1
  store %struct.btDbvtNode* %31, %struct.btDbvtNode** %parent22, align 4
  %33 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %34 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %35 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %34, i32 0, i32 2
  %childs23 = bitcast %union.anon.0* %35 to [2 x %struct.btDbvtNode*]*
  %arrayidx24 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs23, i32 0, i32 1
  store %struct.btDbvtNode* %33, %struct.btDbvtNode** %arrayidx24, align 4
  %36 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %37 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %parent25 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %37, i32 0, i32 1
  store %struct.btDbvtNode* %36, %struct.btDbvtNode** %parent25, align 4
  br label %do.body26

do.body26:                                        ; preds = %do.cond40, %if.then16
  %38 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %volume27 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %38, i32 0, i32 0
  %39 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %volume28 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %39, i32 0, i32 0
  %call29 = call zeroext i1 @_ZNK12btDbvtAabbMm7ContainERKS_(%struct.btDbvtAabbMm* %volume27, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume28)
  br i1 %call29, label %if.else38, label %if.then30

if.then30:                                        ; preds = %do.body26
  %40 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %41 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %40, i32 0, i32 2
  %childs31 = bitcast %union.anon.0* %41 to [2 x %struct.btDbvtNode*]*
  %arrayidx32 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs31, i32 0, i32 0
  %42 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx32, align 4
  %volume33 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %42, i32 0, i32 0
  %43 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %44 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %43, i32 0, i32 2
  %childs34 = bitcast %union.anon.0* %44 to [2 x %struct.btDbvtNode*]*
  %arrayidx35 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs34, i32 0, i32 1
  %45 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx35, align 4
  %volume36 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %45, i32 0, i32 0
  %46 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %volume37 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %46, i32 0, i32 0
  call void @_Z5MergeRK12btDbvtAabbMmS1_RS_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume33, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume36, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume37)
  br label %if.end39

if.else38:                                        ; preds = %do.body26
  br label %do.end42

if.end39:                                         ; preds = %if.then30
  %47 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  store %struct.btDbvtNode* %47, %struct.btDbvtNode** %node, align 4
  br label %do.cond40

do.cond40:                                        ; preds = %if.end39
  %48 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %parent41 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %48, i32 0, i32 1
  %49 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent41, align 4
  store %struct.btDbvtNode* %49, %struct.btDbvtNode** %prev, align 4
  %cmp = icmp ne %struct.btDbvtNode* null, %49
  br i1 %cmp, label %do.body26, label %do.end42

do.end42:                                         ; preds = %do.cond40, %if.else38
  br label %if.end51

if.else43:                                        ; preds = %if.end
  %50 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %51 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %52 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %51, i32 0, i32 2
  %childs44 = bitcast %union.anon.0* %52 to [2 x %struct.btDbvtNode*]*
  %arrayidx45 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs44, i32 0, i32 0
  store %struct.btDbvtNode* %50, %struct.btDbvtNode** %arrayidx45, align 4
  %53 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %54 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %parent46 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %54, i32 0, i32 1
  store %struct.btDbvtNode* %53, %struct.btDbvtNode** %parent46, align 4
  %55 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %56 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %57 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %56, i32 0, i32 2
  %childs47 = bitcast %union.anon.0* %57 to [2 x %struct.btDbvtNode*]*
  %arrayidx48 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs47, i32 0, i32 1
  store %struct.btDbvtNode* %55, %struct.btDbvtNode** %arrayidx48, align 4
  %58 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %59 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %parent49 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %59, i32 0, i32 1
  store %struct.btDbvtNode* %58, %struct.btDbvtNode** %parent49, align 4
  %60 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %61 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_root50 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %61, i32 0, i32 0
  store %struct.btDbvtNode* %60, %struct.btDbvtNode** %m_root50, align 4
  br label %if.end51

if.end51:                                         ; preds = %if.else43, %do.end42
  br label %if.end52

if.end52:                                         ; preds = %if.end51, %if.then
  ret void
}

; Function Attrs: noinline optnone
define internal %struct.btDbvtNode* @_ZL10removeleafP6btDbvtP10btDbvtNode(%struct.btDbvt* %pdbvt, %struct.btDbvtNode* %leaf) #2 {
entry:
  %retval = alloca %struct.btDbvtNode*, align 4
  %pdbvt.addr = alloca %struct.btDbvt*, align 4
  %leaf.addr = alloca %struct.btDbvtNode*, align 4
  %parent = alloca %struct.btDbvtNode*, align 4
  %prev = alloca %struct.btDbvtNode*, align 4
  %sibling = alloca %struct.btDbvtNode*, align 4
  %pb = alloca %struct.btDbvtAabbMm, align 4
  store %struct.btDbvt* %pdbvt, %struct.btDbvt** %pdbvt.addr, align 4
  store %struct.btDbvtNode* %leaf, %struct.btDbvtNode** %leaf.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %1 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %1, i32 0, i32 0
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %cmp = icmp eq %struct.btDbvtNode* %0, %2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %3 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_root1 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %3, i32 0, i32 0
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %m_root1, align 4
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %parent2 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %4, i32 0, i32 1
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent2, align 4
  store %struct.btDbvtNode* %5, %struct.btDbvtNode** %parent, align 4
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent, align 4
  %parent3 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %6, i32 0, i32 1
  %7 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent3, align 4
  store %struct.btDbvtNode* %7, %struct.btDbvtNode** %prev, align 4
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent, align 4
  %9 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %8, i32 0, i32 2
  %childs = bitcast %union.anon.0* %9 to [2 x %struct.btDbvtNode*]*
  %10 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %call = call i32 @_ZL7indexofPK10btDbvtNode(%struct.btDbvtNode* %10)
  %sub = sub nsw i32 1, %call
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 %sub
  %11 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  store %struct.btDbvtNode* %11, %struct.btDbvtNode** %sibling, align 4
  %12 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %tobool = icmp ne %struct.btDbvtNode* %12, null
  br i1 %tobool, label %if.then4, label %if.else24

if.then4:                                         ; preds = %if.else
  %13 = load %struct.btDbvtNode*, %struct.btDbvtNode** %sibling, align 4
  %14 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %15 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %14, i32 0, i32 2
  %childs5 = bitcast %union.anon.0* %15 to [2 x %struct.btDbvtNode*]*
  %16 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent, align 4
  %call6 = call i32 @_ZL7indexofPK10btDbvtNode(%struct.btDbvtNode* %16)
  %arrayidx7 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs5, i32 0, i32 %call6
  store %struct.btDbvtNode* %13, %struct.btDbvtNode** %arrayidx7, align 4
  %17 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %18 = load %struct.btDbvtNode*, %struct.btDbvtNode** %sibling, align 4
  %parent8 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %18, i32 0, i32 1
  store %struct.btDbvtNode* %17, %struct.btDbvtNode** %parent8, align 4
  %19 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %20 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent, align 4
  call void @_ZL10deletenodeP6btDbvtP10btDbvtNode(%struct.btDbvt* %19, %struct.btDbvtNode* %20)
  br label %while.cond

while.cond:                                       ; preds = %if.end, %if.then4
  %21 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %tobool9 = icmp ne %struct.btDbvtNode* %21, null
  br i1 %tobool9, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %22 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %22, i32 0, i32 0
  %23 = bitcast %struct.btDbvtAabbMm* %pb to i8*
  %24 = bitcast %struct.btDbvtAabbMm* %volume to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 32, i1 false)
  %25 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %26 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %25, i32 0, i32 2
  %childs10 = bitcast %union.anon.0* %26 to [2 x %struct.btDbvtNode*]*
  %arrayidx11 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs10, i32 0, i32 0
  %27 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx11, align 4
  %volume12 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %27, i32 0, i32 0
  %28 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %29 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %28, i32 0, i32 2
  %childs13 = bitcast %union.anon.0* %29 to [2 x %struct.btDbvtNode*]*
  %arrayidx14 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs13, i32 0, i32 1
  %30 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx14, align 4
  %volume15 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %30, i32 0, i32 0
  %31 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %volume16 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %31, i32 0, i32 0
  call void @_Z5MergeRK12btDbvtAabbMmS1_RS_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume12, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume15, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume16)
  %32 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %volume17 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %32, i32 0, i32 0
  %call18 = call zeroext i1 @_Z8NotEqualRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %pb, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume17)
  br i1 %call18, label %if.then19, label %if.else21

if.then19:                                        ; preds = %while.body
  %33 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %parent20 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %33, i32 0, i32 1
  %34 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent20, align 4
  store %struct.btDbvtNode* %34, %struct.btDbvtNode** %prev, align 4
  br label %if.end

if.else21:                                        ; preds = %while.body
  br label %while.end

if.end:                                           ; preds = %if.then19
  br label %while.cond

while.end:                                        ; preds = %if.else21, %while.cond
  %35 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  %tobool22 = icmp ne %struct.btDbvtNode* %35, null
  br i1 %tobool22, label %cond.true, label %cond.false

cond.true:                                        ; preds = %while.end
  %36 = load %struct.btDbvtNode*, %struct.btDbvtNode** %prev, align 4
  br label %cond.end

cond.false:                                       ; preds = %while.end
  %37 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_root23 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %37, i32 0, i32 0
  %38 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root23, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btDbvtNode* [ %36, %cond.true ], [ %38, %cond.false ]
  store %struct.btDbvtNode* %cond, %struct.btDbvtNode** %retval, align 4
  br label %return

if.else24:                                        ; preds = %if.else
  %39 = load %struct.btDbvtNode*, %struct.btDbvtNode** %sibling, align 4
  %40 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_root25 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %40, i32 0, i32 0
  store %struct.btDbvtNode* %39, %struct.btDbvtNode** %m_root25, align 4
  %41 = load %struct.btDbvtNode*, %struct.btDbvtNode** %sibling, align 4
  %parent26 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %41, i32 0, i32 1
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %parent26, align 4
  %42 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %43 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent, align 4
  call void @_ZL10deletenodeP6btDbvtP10btDbvtNode(%struct.btDbvt* %42, %struct.btDbvtNode* %43)
  %44 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_root27 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %44, i32 0, i32 0
  %45 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root27, align 4
  store %struct.btDbvtNode* %45, %struct.btDbvtNode** %retval, align 4
  br label %return

return:                                           ; preds = %if.else24, %cond.end, %if.then
  %46 = load %struct.btDbvtNode*, %struct.btDbvtNode** %retval, align 4
  ret %struct.btDbvtNode* %46
}

; Function Attrs: noinline optnone
define hidden void @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm(%struct.btDbvt* %this, %struct.btDbvtNode* %leaf, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume) #2 {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  %leaf.addr = alloca %struct.btDbvtNode*, align 4
  %volume.addr = alloca %struct.btDbvtAabbMm*, align 4
  %root = alloca %struct.btDbvtNode*, align 4
  %i = alloca i32, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store %struct.btDbvtNode* %leaf, %struct.btDbvtNode** %leaf.addr, align 4
  store %struct.btDbvtAabbMm* %volume, %struct.btDbvtAabbMm** %volume.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %call = call %struct.btDbvtNode* @_ZL10removeleafP6btDbvtP10btDbvtNode(%struct.btDbvt* %this1, %struct.btDbvtNode* %0)
  store %struct.btDbvtNode* %call, %struct.btDbvtNode** %root, align 4
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root, align 4
  %tobool = icmp ne %struct.btDbvtNode* %1, null
  br i1 %tobool, label %if.then, label %if.end7

if.then:                                          ; preds = %entry
  %m_lkhd = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_lkhd, align 4
  %cmp = icmp sge i32 %2, 0
  br i1 %cmp, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then2
  %3 = load i32, i32* %i, align 4
  %m_lkhd3 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 2
  %4 = load i32, i32* %m_lkhd3, align 4
  %cmp4 = icmp slt i32 %3, %4
  br i1 %cmp4, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root, align 4
  %parent = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %5, i32 0, i32 1
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent, align 4
  %tobool5 = icmp ne %struct.btDbvtNode* %6, null
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond
  %7 = phi i1 [ false, %for.cond ], [ %tobool5, %land.rhs ]
  br i1 %7, label %for.body, label %for.end

for.body:                                         ; preds = %land.end
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root, align 4
  %parent6 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %8, i32 0, i32 1
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent6, align 4
  store %struct.btDbvtNode* %9, %struct.btDbvtNode** %root, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %land.end
  br label %if.end

if.else:                                          ; preds = %if.then
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %11 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  store %struct.btDbvtNode* %11, %struct.btDbvtNode** %root, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %for.end
  br label %if.end7

if.end7:                                          ; preds = %if.end, %entry
  %12 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume.addr, align 4
  %13 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %volume8 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %13, i32 0, i32 0
  %14 = bitcast %struct.btDbvtAabbMm* %volume8 to i8*
  %15 = bitcast %struct.btDbvtAabbMm* %12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 32, i1 false)
  %16 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root, align 4
  %17 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  call void @_ZL10insertleafP6btDbvtP10btDbvtNodeS2_(%struct.btDbvt* %this1, %struct.btDbvtNode* %16, %struct.btDbvtNode* %17)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3f(%struct.btDbvt* %this, %struct.btDbvtNode* %leaf, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, %class.btVector3* nonnull align 4 dereferenceable(16) %velocity, float %margin) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %struct.btDbvt*, align 4
  %leaf.addr = alloca %struct.btDbvtNode*, align 4
  %volume.addr = alloca %struct.btDbvtAabbMm*, align 4
  %velocity.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store %struct.btDbvtNode* %leaf, %struct.btDbvtNode** %leaf.addr, align 4
  store %struct.btDbvtAabbMm* %volume, %struct.btDbvtAabbMm** %volume.addr, align 4
  store %class.btVector3* %velocity, %class.btVector3** %velocity.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %volume2 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %0, i32 0, i32 0
  %1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume.addr, align 4
  %call = call zeroext i1 @_ZNK12btDbvtAabbMm7ContainERKS_(%struct.btDbvtAabbMm* %volume2, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %1)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume.addr, align 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr)
  call void @_ZN12btDbvtAabbMm6ExpandERK9btVector3(%struct.btDbvtAabbMm* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %velocity.addr, align 4
  call void @_ZN12btDbvtAabbMm12SignedExpandERK9btVector3(%struct.btDbvtAabbMm* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %6 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume.addr, align 4
  call void @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm(%struct.btDbvt* %this1, %struct.btDbvtNode* %5, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %6)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %7 = load i1, i1* %retval, align 1
  ret i1 %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK12btDbvtAabbMm7ContainERKS_(%struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a) #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi)
  %0 = load float, float* %call, align 4
  %1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi2)
  %2 = load float, float* %call3, align 4
  %cmp = fcmp ole float %0, %2
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %mi4 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi4)
  %3 = load float, float* %call5, align 4
  %4 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi6 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %4, i32 0, i32 0
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi6)
  %5 = load float, float* %call7, align 4
  %cmp8 = fcmp ole float %3, %5
  br i1 %cmp8, label %land.lhs.true9, label %land.end

land.lhs.true9:                                   ; preds = %land.lhs.true
  %mi10 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi10)
  %6 = load float, float* %call11, align 4
  %7 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi12 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %7, i32 0, i32 0
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi12)
  %8 = load float, float* %call13, align 4
  %cmp14 = fcmp ole float %6, %8
  br i1 %cmp14, label %land.lhs.true15, label %land.end

land.lhs.true15:                                  ; preds = %land.lhs.true9
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx)
  %9 = load float, float* %call16, align 4
  %10 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx17 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %10, i32 0, i32 1
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx17)
  %11 = load float, float* %call18, align 4
  %cmp19 = fcmp oge float %9, %11
  br i1 %cmp19, label %land.lhs.true20, label %land.end

land.lhs.true20:                                  ; preds = %land.lhs.true15
  %mx21 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx21)
  %12 = load float, float* %call22, align 4
  %13 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx23 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %13, i32 0, i32 1
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx23)
  %14 = load float, float* %call24, align 4
  %cmp25 = fcmp oge float %12, %14
  br i1 %cmp25, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true20
  %mx26 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx26)
  %15 = load float, float* %call27, align 4
  %16 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx28 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %16, i32 0, i32 1
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx28)
  %17 = load float, float* %call29, align 4
  %cmp30 = fcmp oge float %15, %17
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true20, %land.lhs.true15, %land.lhs.true9, %land.lhs.true, %entry
  %18 = phi i1 [ false, %land.lhs.true20 ], [ false, %land.lhs.true15 ], [ false, %land.lhs.true9 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp30, %land.rhs ]
  ret i1 %18
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btDbvtAabbMm6ExpandERK9btVector3(%struct.btDbvtAabbMm* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %e) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  %e.addr = alloca %class.btVector3*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  store %class.btVector3* %e, %class.btVector3** %e.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %e.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  %1 = load %class.btVector3*, %class.btVector3** %e.addr, align 4
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %mx, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btDbvtAabbMm12SignedExpandERK9btVector3(%struct.btDbvtAabbMm* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %e) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  %e.addr = alloca %class.btVector3*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  store %class.btVector3* %e, %class.btVector3** %e.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %e.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4
  %cmp = fcmp ogt float %1, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %mx2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx2)
  %2 = load float, float* %call3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %e.addr, align 4
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %3)
  %arrayidx = getelementptr inbounds float, float* %call4, i32 0
  %4 = load float, float* %arrayidx, align 4
  %add = fadd float %2, %4
  call void @_ZN9btVector34setXEf(%class.btVector3* %mx, float %add)
  br label %if.end

if.else:                                          ; preds = %entry
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %mi5 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi5)
  %5 = load float, float* %call6, align 4
  %6 = load %class.btVector3*, %class.btVector3** %e.addr, align 4
  %call7 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx8 = getelementptr inbounds float, float* %call7, i32 0
  %7 = load float, float* %arrayidx8, align 4
  %add9 = fadd float %5, %7
  call void @_ZN9btVector34setXEf(%class.btVector3* %mi, float %add9)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %8 = load %class.btVector3*, %class.btVector3** %e.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %8)
  %9 = load float, float* %call10, align 4
  %cmp11 = fcmp ogt float %9, 0.000000e+00
  br i1 %cmp11, label %if.then12, label %if.else19

if.then12:                                        ; preds = %if.end
  %mx13 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %mx14 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx14)
  %10 = load float, float* %call15, align 4
  %11 = load %class.btVector3*, %class.btVector3** %e.addr, align 4
  %call16 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %11)
  %arrayidx17 = getelementptr inbounds float, float* %call16, i32 1
  %12 = load float, float* %arrayidx17, align 4
  %add18 = fadd float %10, %12
  call void @_ZN9btVector34setYEf(%class.btVector3* %mx13, float %add18)
  br label %if.end26

if.else19:                                        ; preds = %if.end
  %mi20 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %mi21 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi21)
  %13 = load float, float* %call22, align 4
  %14 = load %class.btVector3*, %class.btVector3** %e.addr, align 4
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 1
  %15 = load float, float* %arrayidx24, align 4
  %add25 = fadd float %13, %15
  call void @_ZN9btVector34setYEf(%class.btVector3* %mi20, float %add25)
  br label %if.end26

if.end26:                                         ; preds = %if.else19, %if.then12
  %16 = load %class.btVector3*, %class.btVector3** %e.addr, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %16)
  %17 = load float, float* %call27, align 4
  %cmp28 = fcmp ogt float %17, 0.000000e+00
  br i1 %cmp28, label %if.then29, label %if.else36

if.then29:                                        ; preds = %if.end26
  %mx30 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %mx31 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx31)
  %18 = load float, float* %call32, align 4
  %19 = load %class.btVector3*, %class.btVector3** %e.addr, align 4
  %call33 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %19)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 2
  %20 = load float, float* %arrayidx34, align 4
  %add35 = fadd float %18, %20
  call void @_ZN9btVector34setZEf(%class.btVector3* %mx30, float %add35)
  br label %if.end43

if.else36:                                        ; preds = %if.end26
  %mi37 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %mi38 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call39 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi38)
  %21 = load float, float* %call39, align 4
  %22 = load %class.btVector3*, %class.btVector3** %e.addr, align 4
  %call40 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %22)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 2
  %23 = load float, float* %arrayidx41, align 4
  %add42 = fadd float %21, %23
  call void @_ZN9btVector34setZEf(%class.btVector3* %mi37, float %add42)
  br label %if.end43

if.end43:                                         ; preds = %if.else36, %if.then29
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3(%struct.btDbvt* %this, %struct.btDbvtNode* %leaf, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, %class.btVector3* nonnull align 4 dereferenceable(16) %velocity) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %struct.btDbvt*, align 4
  %leaf.addr = alloca %struct.btDbvtNode*, align 4
  %volume.addr = alloca %struct.btDbvtAabbMm*, align 4
  %velocity.addr = alloca %class.btVector3*, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store %struct.btDbvtNode* %leaf, %struct.btDbvtNode** %leaf.addr, align 4
  store %struct.btDbvtAabbMm* %volume, %struct.btDbvtAabbMm** %volume.addr, align 4
  store %class.btVector3* %velocity, %class.btVector3** %velocity.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %volume2 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %0, i32 0, i32 0
  %1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume.addr, align 4
  %call = call zeroext i1 @_ZNK12btDbvtAabbMm7ContainERKS_(%struct.btDbvtAabbMm* %volume2, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %1)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %velocity.addr, align 4
  call void @_ZN12btDbvtAabbMm12SignedExpandERK9btVector3(%struct.btDbvtAabbMm* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %5 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume.addr, align 4
  call void @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm(%struct.btDbvt* %this1, %struct.btDbvtNode* %4, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %5)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmf(%struct.btDbvt* %this, %struct.btDbvtNode* %leaf, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, float %margin) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %struct.btDbvt*, align 4
  %leaf.addr = alloca %struct.btDbvtNode*, align 4
  %volume.addr = alloca %struct.btDbvtAabbMm*, align 4
  %margin.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store %struct.btDbvtNode* %leaf, %struct.btDbvtNode** %leaf.addr, align 4
  store %struct.btDbvtAabbMm* %volume, %struct.btDbvtAabbMm** %volume.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %volume2 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %0, i32 0, i32 0
  %1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume.addr, align 4
  %call = call zeroext i1 @_ZNK12btDbvtAabbMm7ContainERKS_(%struct.btDbvtAabbMm* %volume2, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %1)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume.addr, align 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr)
  call void @_ZN12btDbvtAabbMm6ExpandERK9btVector3(%struct.btDbvtAabbMm* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %4 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume.addr, align 4
  call void @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm(%struct.btDbvt* %this1, %struct.btDbvtNode* %3, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %4)
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i1, i1* %retval, align 1
  ret i1 %5
}

; Function Attrs: noinline optnone
define hidden void @_ZN6btDbvt6removeEP10btDbvtNode(%struct.btDbvt* %this, %struct.btDbvtNode* %leaf) #2 {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  %leaf.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store %struct.btDbvtNode* %leaf, %struct.btDbvtNode** %leaf.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %call = call %struct.btDbvtNode* @_ZL10removeleafP6btDbvtP10btDbvtNode(%struct.btDbvt* %this1, %struct.btDbvtNode* %0)
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  call void @_ZL10deletenodeP6btDbvtP10btDbvtNode(%struct.btDbvt* %this1, %struct.btDbvtNode* %1)
  %m_leaves = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 3
  %2 = load i32, i32* %m_leaves, align 4
  %dec = add nsw i32 %2, -1
  store i32 %dec, i32* %m_leaves, align 4
  ret void
}

; Function Attrs: noinline optnone
define internal void @_ZL10deletenodeP6btDbvtP10btDbvtNode(%struct.btDbvt* %pdbvt, %struct.btDbvtNode* %node) #2 {
entry:
  %pdbvt.addr = alloca %struct.btDbvt*, align 4
  %node.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvt* %pdbvt, %struct.btDbvt** %pdbvt.addr, align 4
  store %struct.btDbvtNode* %node, %struct.btDbvtNode** %node.addr, align 4
  %0 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_free = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %0, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_free, align 4
  %2 = bitcast %struct.btDbvtNode* %1 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %2)
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %4 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_free1 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %4, i32 0, i32 1
  store %struct.btDbvtNode* %3, %struct.btDbvtNode** %m_free1, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK6btDbvt5writeEPNS_7IWriterE(%struct.btDbvt* %this, %"struct.btDbvt::IWriter"* %iwriter) #2 {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  %iwriter.addr = alloca %"struct.btDbvt::IWriter"*, align 4
  %nodes = alloca %struct.btDbvtNodeEnumerator, align 4
  %i = alloca i32, align 4
  %n = alloca %struct.btDbvtNode*, align 4
  %p = alloca i32, align 4
  %c0 = alloca i32, align 4
  %c1 = alloca i32, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store %"struct.btDbvt::IWriter"* %iwriter, %"struct.btDbvt::IWriter"** %iwriter.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %call = call %struct.btDbvtNodeEnumerator* @_ZN20btDbvtNodeEnumeratorC2Ev(%struct.btDbvtNodeEnumerator* %nodes)
  %nodes2 = getelementptr inbounds %struct.btDbvtNodeEnumerator, %struct.btDbvtNodeEnumerator* %nodes, i32 0, i32 1
  %m_leaves = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_leaves, align 4
  %mul = mul nsw i32 %0, 2
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.5* %nodes2, i32 %mul)
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %2 = bitcast %struct.btDbvtNodeEnumerator* %nodes to %"struct.btDbvt::ICollide"*
  call void @_ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE(%struct.btDbvtNode* %1, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %2)
  %3 = load %"struct.btDbvt::IWriter"*, %"struct.btDbvt::IWriter"** %iwriter.addr, align 4
  %m_root3 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root3, align 4
  %nodes4 = getelementptr inbounds %struct.btDbvtNodeEnumerator, %struct.btDbvtNodeEnumerator* %nodes, i32 0, i32 1
  %call5 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.5* %nodes4)
  %5 = bitcast %"struct.btDbvt::IWriter"* %3 to void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32)***
  %vtable = load void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32)**, void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32)*** %5, align 4
  %vfn = getelementptr inbounds void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32)*, void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32)** %vtable, i64 2
  %6 = load void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32)*, void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32)** %vfn, align 4
  call void %6(%"struct.btDbvt::IWriter"* %3, %struct.btDbvtNode* %4, i32 %call5)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %7 = load i32, i32* %i, align 4
  %nodes6 = getelementptr inbounds %struct.btDbvtNodeEnumerator, %struct.btDbvtNodeEnumerator* %nodes, i32 0, i32 1
  %call7 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.5* %nodes6)
  %cmp = icmp slt i32 %7, %call7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %nodes8 = getelementptr inbounds %struct.btDbvtNodeEnumerator, %struct.btDbvtNodeEnumerator* %nodes, i32 0, i32 1
  %8 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi(%class.btAlignedObjectArray.5* %nodes8, i32 %8)
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call9, align 4
  store %struct.btDbvtNode* %9, %struct.btDbvtNode** %n, align 4
  store i32 -1, i32* %p, align 4
  %10 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %parent = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %10, i32 0, i32 1
  %11 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent, align 4
  %tobool = icmp ne %struct.btDbvtNode* %11, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %nodes10 = getelementptr inbounds %struct.btDbvtNodeEnumerator, %struct.btDbvtNodeEnumerator* %nodes, i32 0, i32 1
  %12 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %parent11 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %12, i32 0, i32 1
  %call12 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE16findLinearSearchERKS2_(%class.btAlignedObjectArray.5* %nodes10, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %parent11)
  store i32 %call12, i32* %p, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %13 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %call13 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %13)
  br i1 %call13, label %if.then14, label %if.else

if.then14:                                        ; preds = %if.end
  %nodes15 = getelementptr inbounds %struct.btDbvtNodeEnumerator, %struct.btDbvtNodeEnumerator* %nodes, i32 0, i32 1
  %14 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %15 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %14, i32 0, i32 2
  %childs = bitcast %union.anon.0* %15 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  %call16 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE16findLinearSearchERKS2_(%class.btAlignedObjectArray.5* %nodes15, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %arrayidx)
  store i32 %call16, i32* %c0, align 4
  %nodes17 = getelementptr inbounds %struct.btDbvtNodeEnumerator, %struct.btDbvtNodeEnumerator* %nodes, i32 0, i32 1
  %16 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %17 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %16, i32 0, i32 2
  %childs18 = bitcast %union.anon.0* %17 to [2 x %struct.btDbvtNode*]*
  %arrayidx19 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs18, i32 0, i32 1
  %call20 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE16findLinearSearchERKS2_(%class.btAlignedObjectArray.5* %nodes17, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %arrayidx19)
  store i32 %call20, i32* %c1, align 4
  %18 = load %"struct.btDbvt::IWriter"*, %"struct.btDbvt::IWriter"** %iwriter.addr, align 4
  %19 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %20 = load i32, i32* %i, align 4
  %21 = load i32, i32* %p, align 4
  %22 = load i32, i32* %c0, align 4
  %23 = load i32, i32* %c1, align 4
  %24 = bitcast %"struct.btDbvt::IWriter"* %18 to void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32, i32, i32, i32)***
  %vtable21 = load void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32, i32, i32, i32)**, void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32, i32, i32, i32)*** %24, align 4
  %vfn22 = getelementptr inbounds void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32, i32, i32, i32)*, void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32, i32, i32, i32)** %vtable21, i64 3
  %25 = load void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32, i32, i32, i32)*, void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32, i32, i32, i32)** %vfn22, align 4
  call void %25(%"struct.btDbvt::IWriter"* %18, %struct.btDbvtNode* %19, i32 %20, i32 %21, i32 %22, i32 %23)
  br label %if.end25

if.else:                                          ; preds = %if.end
  %26 = load %"struct.btDbvt::IWriter"*, %"struct.btDbvt::IWriter"** %iwriter.addr, align 4
  %27 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %28 = load i32, i32* %i, align 4
  %29 = load i32, i32* %p, align 4
  %30 = bitcast %"struct.btDbvt::IWriter"* %26 to void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32, i32)***
  %vtable23 = load void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32, i32)**, void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32, i32)*** %30, align 4
  %vfn24 = getelementptr inbounds void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32, i32)*, void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32, i32)** %vtable23, i64 4
  %31 = load void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32, i32)*, void (%"struct.btDbvt::IWriter"*, %struct.btDbvtNode*, i32, i32)** %vfn24, align 4
  call void %31(%"struct.btDbvt::IWriter"* %26, %struct.btDbvtNode* %27, i32 %28, i32 %29)
  br label %if.end25

if.end25:                                         ; preds = %if.else, %if.then14
  br label %for.inc

for.inc:                                          ; preds = %if.end25
  %32 = load i32, i32* %i, align 4
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %call26 = call %struct.btDbvtNodeEnumerator* @_ZN20btDbvtNodeEnumeratorD2Ev(%struct.btDbvtNodeEnumerator* %nodes) #6
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btDbvtNodeEnumerator* @_ZN20btDbvtNodeEnumeratorC2Ev(%struct.btDbvtNodeEnumerator* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNodeEnumerator*, align 4
  store %struct.btDbvtNodeEnumerator* %this, %struct.btDbvtNodeEnumerator** %this.addr, align 4
  %this1 = load %struct.btDbvtNodeEnumerator*, %struct.btDbvtNodeEnumerator** %this.addr, align 4
  %0 = bitcast %struct.btDbvtNodeEnumerator* %this1 to %"struct.btDbvt::ICollide"*
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideC2Ev(%"struct.btDbvt::ICollide"* %0) #6
  %1 = bitcast %struct.btDbvtNodeEnumerator* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV20btDbvtNodeEnumerator, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %nodes = getelementptr inbounds %struct.btDbvtNodeEnumerator, %struct.btDbvtNodeEnumerator* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2Ev(%class.btAlignedObjectArray.5* %nodes)
  ret %struct.btDbvtNodeEnumerator* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.5* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btDbvtNode**, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray.5* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi(%class.btAlignedObjectArray.5* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btDbvtNode**
  store %struct.btDbvtNode** %2, %struct.btDbvtNode*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  %3 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call3, %struct.btDbvtNode** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray.5* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store %struct.btDbvtNode** %4, %struct.btDbvtNode*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE(%struct.btDbvtNode* %root, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %policy) #2 comdat {
entry:
  %root.addr = alloca %struct.btDbvtNode*, align 4
  %policy.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %struct.btDbvtNode* %root, %struct.btDbvtNode** %root.addr, align 4
  store %"struct.btDbvt::ICollide"* %policy, %"struct.btDbvt::ICollide"** %policy.addr, align 4
  %0 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %policy.addr, align 4
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %2 = bitcast %"struct.btDbvt::ICollide"* %0 to void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)***
  %vtable = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)**, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*** %2, align 4
  %vfn = getelementptr inbounds void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vtable, i64 3
  %3 = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vfn, align 4
  call void %3(%"struct.btDbvt::ICollide"* %0, %struct.btDbvtNode* %1)
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %4)
  br i1 %call, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %6 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %5, i32 0, i32 2
  %childs = bitcast %union.anon.0* %6 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  %7 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %8 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %policy.addr, align 4
  call void @_ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE(%struct.btDbvtNode* %7, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %8)
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %10 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %9, i32 0, i32 2
  %childs1 = bitcast %union.anon.0* %10 to [2 x %struct.btDbvtNode*]*
  %arrayidx2 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs1, i32 0, i32 1
  %11 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx2, align 4
  %12 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %policy.addr, align 4
  call void @_ZN6btDbvt9enumNodesEPK10btDbvtNodeRNS_8ICollideE(%struct.btDbvtNode* %11, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %12)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.5* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi(%class.btAlignedObjectArray.5* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %0, i32 %1
  ret %struct.btDbvtNode** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE16findLinearSearchERKS2_(%class.btAlignedObjectArray.5* %this, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %key) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %key.addr = alloca %struct.btDbvtNode**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  store %struct.btDbvtNode** %key, %struct.btDbvtNode*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  store i32 %call, i32* %index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %1 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %1, i32 %2
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %4 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %key.addr, align 4
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %4, align 4
  %cmp3 = icmp eq %struct.btDbvtNode* %3, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  store i32 %6, i32* %index, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %8 = load i32, i32* %index, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDbvtNodeEnumerator* @_ZN20btDbvtNodeEnumeratorD2Ev(%struct.btDbvtNodeEnumerator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNodeEnumerator*, align 4
  store %struct.btDbvtNodeEnumerator* %this, %struct.btDbvtNodeEnumerator** %this.addr, align 4
  %this1 = load %struct.btDbvtNodeEnumerator*, %struct.btDbvtNodeEnumerator** %this.addr, align 4
  %0 = bitcast %struct.btDbvtNodeEnumerator* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV20btDbvtNodeEnumerator, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %nodes = getelementptr inbounds %struct.btDbvtNodeEnumerator, %struct.btDbvtNodeEnumerator* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev(%class.btAlignedObjectArray.5* %nodes) #6
  %1 = bitcast %struct.btDbvtNodeEnumerator* %this1 to %"struct.btDbvt::ICollide"*
  %call2 = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* %1) #6
  ret %struct.btDbvtNodeEnumerator* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK6btDbvt5cloneERS_PNS_6ICloneE(%struct.btDbvt* %this, %struct.btDbvt* nonnull align 4 dereferenceable(40) %dest, %"struct.btDbvt::IClone"* %iclone) #2 {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  %dest.addr = alloca %struct.btDbvt*, align 4
  %iclone.addr = alloca %"struct.btDbvt::IClone"*, align 4
  %stack = alloca %class.btAlignedObjectArray.9, align 4
  %ref.tmp = alloca %"struct.btDbvt::sStkCLN", align 4
  %i = alloca i32, align 4
  %e = alloca %"struct.btDbvt::sStkCLN", align 4
  %n = alloca %struct.btDbvtNode*, align 4
  %ref.tmp16 = alloca %"struct.btDbvt::sStkCLN", align 4
  %ref.tmp21 = alloca %"struct.btDbvt::sStkCLN", align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store %struct.btDbvt* %dest, %struct.btDbvt** %dest.addr, align 4
  store %"struct.btDbvt::IClone"* %iclone, %"struct.btDbvt::IClone"** %iclone.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %0 = load %struct.btDbvt*, %struct.btDbvt** %dest.addr, align 4
  call void @_ZN6btDbvt5clearEv(%struct.btDbvt* %0)
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %cmp = icmp ne %struct.btDbvtNode* %1, null
  br i1 %cmp, label %if.then, label %if.end31

if.then:                                          ; preds = %entry
  %call = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEEC2Ev(%class.btAlignedObjectArray.9* %stack)
  %m_leaves = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 3
  %2 = load i32, i32* %m_leaves, align 4
  call void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE7reserveEi(%class.btAlignedObjectArray.9* %stack, i32 %2)
  %m_root2 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root2, align 4
  %call3 = call %"struct.btDbvt::sStkCLN"* @_ZN6btDbvt7sStkCLNC2EPK10btDbvtNodePS1_(%"struct.btDbvt::sStkCLN"* %ref.tmp, %struct.btDbvtNode* %3, %struct.btDbvtNode* null)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE9push_backERKS1_(%class.btAlignedObjectArray.9* %stack, %"struct.btDbvt::sStkCLN"* nonnull align 4 dereferenceable(8) %ref.tmp)
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4sizeEv(%class.btAlignedObjectArray.9* %stack)
  %sub = sub nsw i32 %call4, 1
  store i32 %sub, i32* %i, align 4
  %4 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkCLN"* @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEEixEi(%class.btAlignedObjectArray.9* %stack, i32 %4)
  %5 = bitcast %"struct.btDbvt::sStkCLN"* %e to i8*
  %6 = bitcast %"struct.btDbvt::sStkCLN"* %call5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 8, i1 false)
  %7 = load %struct.btDbvt*, %struct.btDbvt** %dest.addr, align 4
  %parent = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %e, i32 0, i32 1
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent, align 4
  %node = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %e, i32 0, i32 0
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %9, i32 0, i32 0
  %node6 = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %e, i32 0, i32 0
  %10 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node6, align 4
  %11 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %10, i32 0, i32 2
  %data = bitcast %union.anon.0* %11 to i8**
  %12 = load i8*, i8** %data, align 4
  %call7 = call %struct.btDbvtNode* @_ZL10createnodeP6btDbvtP10btDbvtNodeRK12btDbvtAabbMmPv(%struct.btDbvt* %7, %struct.btDbvtNode* %8, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, i8* %12)
  store %struct.btDbvtNode* %call7, %struct.btDbvtNode** %n, align 4
  call void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE8pop_backEv(%class.btAlignedObjectArray.9* %stack)
  %parent8 = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %e, i32 0, i32 1
  %13 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent8, align 4
  %cmp9 = icmp ne %struct.btDbvtNode* %13, null
  br i1 %cmp9, label %if.then10, label %if.else

if.then10:                                        ; preds = %do.body
  %14 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %parent11 = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %e, i32 0, i32 1
  %15 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent11, align 4
  %16 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %15, i32 0, i32 2
  %childs = bitcast %union.anon.0* %16 to [2 x %struct.btDbvtNode*]*
  %17 = load i32, i32* %i, align 4
  %and = and i32 %17, 1
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 %and
  store %struct.btDbvtNode* %14, %struct.btDbvtNode** %arrayidx, align 4
  br label %if.end

if.else:                                          ; preds = %do.body
  %18 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %19 = load %struct.btDbvt*, %struct.btDbvt** %dest.addr, align 4
  %m_root12 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %19, i32 0, i32 0
  store %struct.btDbvtNode* %18, %struct.btDbvtNode** %m_root12, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then10
  %node13 = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %e, i32 0, i32 0
  %20 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node13, align 4
  %call14 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %20)
  br i1 %call14, label %if.then15, label %if.else26

if.then15:                                        ; preds = %if.end
  %node17 = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %e, i32 0, i32 0
  %21 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node17, align 4
  %22 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %21, i32 0, i32 2
  %childs18 = bitcast %union.anon.0* %22 to [2 x %struct.btDbvtNode*]*
  %arrayidx19 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs18, i32 0, i32 0
  %23 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx19, align 4
  %24 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %call20 = call %"struct.btDbvt::sStkCLN"* @_ZN6btDbvt7sStkCLNC2EPK10btDbvtNodePS1_(%"struct.btDbvt::sStkCLN"* %ref.tmp16, %struct.btDbvtNode* %23, %struct.btDbvtNode* %24)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE9push_backERKS1_(%class.btAlignedObjectArray.9* %stack, %"struct.btDbvt::sStkCLN"* nonnull align 4 dereferenceable(8) %ref.tmp16)
  %node22 = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %e, i32 0, i32 0
  %25 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node22, align 4
  %26 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %25, i32 0, i32 2
  %childs23 = bitcast %union.anon.0* %26 to [2 x %struct.btDbvtNode*]*
  %arrayidx24 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs23, i32 0, i32 1
  %27 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx24, align 4
  %28 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %call25 = call %"struct.btDbvt::sStkCLN"* @_ZN6btDbvt7sStkCLNC2EPK10btDbvtNodePS1_(%"struct.btDbvt::sStkCLN"* %ref.tmp21, %struct.btDbvtNode* %27, %struct.btDbvtNode* %28)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE9push_backERKS1_(%class.btAlignedObjectArray.9* %stack, %"struct.btDbvt::sStkCLN"* nonnull align 4 dereferenceable(8) %ref.tmp21)
  br label %if.end27

if.else26:                                        ; preds = %if.end
  %29 = load %"struct.btDbvt::IClone"*, %"struct.btDbvt::IClone"** %iclone.addr, align 4
  %30 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %31 = bitcast %"struct.btDbvt::IClone"* %29 to void (%"struct.btDbvt::IClone"*, %struct.btDbvtNode*)***
  %vtable = load void (%"struct.btDbvt::IClone"*, %struct.btDbvtNode*)**, void (%"struct.btDbvt::IClone"*, %struct.btDbvtNode*)*** %31, align 4
  %vfn = getelementptr inbounds void (%"struct.btDbvt::IClone"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::IClone"*, %struct.btDbvtNode*)** %vtable, i64 2
  %32 = load void (%"struct.btDbvt::IClone"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::IClone"*, %struct.btDbvtNode*)** %vfn, align 4
  call void %32(%"struct.btDbvt::IClone"* %29, %struct.btDbvtNode* %30)
  br label %if.end27

if.end27:                                         ; preds = %if.else26, %if.then15
  br label %do.cond

do.cond:                                          ; preds = %if.end27
  %call28 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4sizeEv(%class.btAlignedObjectArray.9* %stack)
  %cmp29 = icmp sgt i32 %call28, 0
  br i1 %cmp29, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %call30 = call %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEED2Ev(%class.btAlignedObjectArray.9* %stack) #6
  br label %if.end31

if.end31:                                         ; preds = %do.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEEC2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorIN6btDbvt7sStkCLNELj16EEC2Ev(%class.btAlignedAllocator.10* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE7reserveEi(%class.btAlignedObjectArray.9* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"struct.btDbvt::sStkCLN"*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE8allocateEi(%class.btAlignedObjectArray.9* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %"struct.btDbvt::sStkCLN"*
  store %"struct.btDbvt::sStkCLN"* %2, %"struct.btDbvt::sStkCLN"** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %3 = load %"struct.btDbvt::sStkCLN"*, %"struct.btDbvt::sStkCLN"** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4copyEiiPS1_(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call3, %"struct.btDbvt::sStkCLN"* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %"struct.btDbvt::sStkCLN"*, %"struct.btDbvt::sStkCLN"** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %"struct.btDbvt::sStkCLN"* %4, %"struct.btDbvt::sStkCLN"** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE9push_backERKS1_(%class.btAlignedObjectArray.9* %this, %"struct.btDbvt::sStkCLN"* nonnull align 4 dereferenceable(8) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %_Val.addr = alloca %"struct.btDbvt::sStkCLN"*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store %"struct.btDbvt::sStkCLN"* %_Val, %"struct.btDbvt::sStkCLN"** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE9allocSizeEi(%class.btAlignedObjectArray.9* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE7reserveEi(%class.btAlignedObjectArray.9* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %1 = load %"struct.btDbvt::sStkCLN"*, %"struct.btDbvt::sStkCLN"** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %1, i32 %2
  %3 = bitcast %"struct.btDbvt::sStkCLN"* %arrayidx to i8*
  %4 = bitcast i8* %3 to %"struct.btDbvt::sStkCLN"*
  %5 = load %"struct.btDbvt::sStkCLN"*, %"struct.btDbvt::sStkCLN"** %_Val.addr, align 4
  %6 = bitcast %"struct.btDbvt::sStkCLN"* %4 to i8*
  %7 = bitcast %"struct.btDbvt::sStkCLN"* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 8, i1 false)
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDbvt::sStkCLN"* @_ZN6btDbvt7sStkCLNC2EPK10btDbvtNodePS1_(%"struct.btDbvt::sStkCLN"* returned %this, %struct.btDbvtNode* %n, %struct.btDbvtNode* %p) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::sStkCLN"*, align 4
  %n.addr = alloca %struct.btDbvtNode*, align 4
  %p.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::sStkCLN"* %this, %"struct.btDbvt::sStkCLN"** %this.addr, align 4
  store %struct.btDbvtNode* %n, %struct.btDbvtNode** %n.addr, align 4
  store %struct.btDbvtNode* %p, %struct.btDbvtNode** %p.addr, align 4
  %this1 = load %"struct.btDbvt::sStkCLN"*, %"struct.btDbvt::sStkCLN"** %this.addr, align 4
  %node = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %this1, i32 0, i32 0
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %node, align 4
  %parent = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %this1, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %p.addr, align 4
  store %struct.btDbvtNode* %1, %struct.btDbvtNode** %parent, align 4
  ret %"struct.btDbvt::sStkCLN"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4sizeEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkCLN"* @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEEixEi(%class.btAlignedObjectArray.9* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %"struct.btDbvt::sStkCLN"*, %"struct.btDbvt::sStkCLN"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %0, i32 %1
  ret %"struct.btDbvt::sStkCLN"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE8pop_backEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %1 = load %"struct.btDbvt::sStkCLN"*, %"struct.btDbvt::sStkCLN"** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %1, i32 %2
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.9* @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEED2Ev(%class.btAlignedObjectArray.9* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE5clearEv(%class.btAlignedObjectArray.9* %this1)
  ret %class.btAlignedObjectArray.9* %this1
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN6btDbvt8maxdepthEPK10btDbvtNode(%struct.btDbvtNode* %node) #2 {
entry:
  %node.addr = alloca %struct.btDbvtNode*, align 4
  %depth = alloca i32, align 4
  store %struct.btDbvtNode* %node, %struct.btDbvtNode** %node.addr, align 4
  store i32 0, i32* %depth, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %tobool = icmp ne %struct.btDbvtNode* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  call void @_ZL11getmaxdepthPK10btDbvtNodeiRi(%struct.btDbvtNode* %1, i32 1, i32* nonnull align 4 dereferenceable(4) %depth)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %2 = load i32, i32* %depth, align 4
  ret i32 %2
}

; Function Attrs: noinline optnone
define internal void @_ZL11getmaxdepthPK10btDbvtNodeiRi(%struct.btDbvtNode* %node, i32 %depth, i32* nonnull align 4 dereferenceable(4) %maxdepth) #2 {
entry:
  %node.addr = alloca %struct.btDbvtNode*, align 4
  %depth.addr = alloca i32, align 4
  %maxdepth.addr = alloca i32*, align 4
  store %struct.btDbvtNode* %node, %struct.btDbvtNode** %node.addr, align 4
  store i32 %depth, i32* %depth.addr, align 4
  store i32* %maxdepth, i32** %maxdepth.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %0)
  br i1 %call, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %2 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %1, i32 0, i32 2
  %childs = bitcast %union.anon.0* %2 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %4 = load i32, i32* %depth.addr, align 4
  %add = add nsw i32 %4, 1
  %5 = load i32*, i32** %maxdepth.addr, align 4
  call void @_ZL11getmaxdepthPK10btDbvtNodeiRi(%struct.btDbvtNode* %3, i32 %add, i32* nonnull align 4 dereferenceable(4) %5)
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %7 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %6, i32 0, i32 2
  %childs1 = bitcast %union.anon.0* %7 to [2 x %struct.btDbvtNode*]*
  %arrayidx2 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs1, i32 0, i32 1
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx2, align 4
  %9 = load i32, i32* %depth.addr, align 4
  %add3 = add nsw i32 %9, 1
  %10 = load i32*, i32** %maxdepth.addr, align 4
  call void @_ZL11getmaxdepthPK10btDbvtNodeiRi(%struct.btDbvtNode* %8, i32 %add3, i32* nonnull align 4 dereferenceable(4) %10)
  br label %if.end

if.else:                                          ; preds = %entry
  %11 = load i32*, i32** %maxdepth.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %11, i32* nonnull align 4 dereferenceable(4) %depth.addr)
  %12 = load i32, i32* %call4, align 4
  %13 = load i32*, i32** %maxdepth.addr, align 4
  store i32 %12, i32* %13, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN6btDbvt11countLeavesEPK10btDbvtNode(%struct.btDbvtNode* %node) #2 {
entry:
  %retval = alloca i32, align 4
  %node.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %node, %struct.btDbvtNode** %node.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %0)
  br i1 %call, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %2 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %1, i32 0, i32 2
  %childs = bitcast %union.anon.0* %2 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %call1 = call i32 @_ZN6btDbvt11countLeavesEPK10btDbvtNode(%struct.btDbvtNode* %3)
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %5 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %4, i32 0, i32 2
  %childs2 = bitcast %union.anon.0* %5 to [2 x %struct.btDbvtNode*]*
  %arrayidx3 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs2, i32 0, i32 1
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx3, align 4
  %call4 = call i32 @_ZN6btDbvt11countLeavesEPK10btDbvtNode(%struct.btDbvtNode* %6)
  %add = add nsw i32 %call1, %call4
  store i32 %add, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  store i32 1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %7 = load i32, i32* %retval, align 4
  ret i32 %7
}

; Function Attrs: noinline optnone
define hidden void @_ZN6btDbvt13extractLeavesEPK10btDbvtNodeR20btAlignedObjectArrayIS2_E(%struct.btDbvtNode* %node, %class.btAlignedObjectArray.5* nonnull align 4 dereferenceable(17) %leaves) #2 {
entry:
  %node.addr = alloca %struct.btDbvtNode*, align 4
  %leaves.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %struct.btDbvtNode* %node, %struct.btDbvtNode** %node.addr, align 4
  store %class.btAlignedObjectArray.5* %leaves, %class.btAlignedObjectArray.5** %leaves.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %0)
  br i1 %call, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %2 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %1, i32 0, i32 2
  %childs = bitcast %union.anon.0* %2 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %4 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %leaves.addr, align 4
  call void @_ZN6btDbvt13extractLeavesEPK10btDbvtNodeR20btAlignedObjectArrayIS2_E(%struct.btDbvtNode* %3, %class.btAlignedObjectArray.5* nonnull align 4 dereferenceable(17) %4)
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %6 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %5, i32 0, i32 2
  %childs1 = bitcast %union.anon.0* %6 to [2 x %struct.btDbvtNode*]*
  %arrayidx2 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs1, i32 0, i32 1
  %7 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx2, align 4
  %8 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %leaves.addr, align 4
  call void @_ZN6btDbvt13extractLeavesEPK10btDbvtNodeR20btAlignedObjectArrayIS2_E(%struct.btDbvtNode* %7, %class.btAlignedObjectArray.5* nonnull align 4 dereferenceable(17) %8)
  br label %if.end

if.else:                                          ; preds = %entry
  %9 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %leaves.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray.5* %9, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %node.addr)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray.5* %this, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %_Val.addr = alloca %struct.btDbvtNode**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  store %struct.btDbvtNode** %_Val, %struct.btDbvtNode*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray.5* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9allocSizeEi(%class.btAlignedObjectArray.5* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.5* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %1 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %1, i32 %2
  %3 = bitcast %struct.btDbvtNode** %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.btDbvtNode**
  %5 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %_Val.addr, align 4
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %5, align 4
  store %struct.btDbvtNode* %6, %struct.btDbvtNode** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %this1, i32 0, i32 2
  %childs = bitcast %union.anon.0* %0 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %cmp = icmp eq %struct.btDbvtNode* %1, null
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE9push_backERKS1_(%class.btAlignedObjectArray.1* %this, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %_Val.addr = alloca %struct.btDbvtNode**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store %struct.btDbvtNode** %_Val, %struct.btDbvtNode*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray.1* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP10btDbvtNodeE9allocSizeEi(%class.btAlignedObjectArray.1* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.1* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %1 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %1, i32 %2
  %3 = bitcast %struct.btDbvtNode** %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.btDbvtNode**
  %5 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %_Val.addr, align 4
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %5, align 4
  store %struct.btDbvtNode* %6, %struct.btDbvtNode** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP10btDbvtNodeE9allocSizeEi(%class.btAlignedObjectArray.1* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define internal float @_ZL4sizeRK12btDbvtAabbMm(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a) #2 {
entry:
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %edges = alloca %class.btVector3, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4
  %0 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  call void @_ZNK12btDbvtAabbMm7LengthsEv(%class.btVector3* sret align 4 %edges, %struct.btDbvtAabbMm* %0)
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %edges)
  %1 = load float, float* %call, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %edges)
  %2 = load float, float* %call1, align 4
  %mul = fmul float %1, %2
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %edges)
  %3 = load float, float* %call2, align 4
  %mul3 = fmul float %mul, %3
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %edges)
  %4 = load float, float* %call4, align 4
  %add = fadd float %mul3, %4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %edges)
  %5 = load float, float* %call5, align 4
  %add6 = fadd float %add, %5
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %edges)
  %6 = load float, float* %call7, align 4
  %add8 = fadd float %add6, %6
  ret float %add8
}

; Function Attrs: noinline optnone
define internal void @_ZL5mergeRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* noalias sret align 4 %agg.result, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %b) #2 {
entry:
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %b.addr = alloca %struct.btDbvtAabbMm*, align 4
  %locals = alloca [32 x i8], align 16
  %ptr = alloca %struct.btDbvtAabbMm*, align 4
  %res = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4
  store %struct.btDbvtAabbMm* %b, %struct.btDbvtAabbMm** %b.addr, align 4
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %locals, i32 0, i32 0
  %0 = bitcast i8* %arraydecay to %struct.btDbvtAabbMm*
  store %struct.btDbvtAabbMm* %0, %struct.btDbvtAabbMm** %ptr, align 4
  %1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %ptr, align 4
  store %struct.btDbvtAabbMm* %1, %struct.btDbvtAabbMm** %res, align 4
  %2 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %3 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %4 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %res, align 4
  call void @_Z5MergeRK12btDbvtAabbMmS1_RS_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %2, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %3, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %4)
  %5 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %res, align 4
  %6 = bitcast %struct.btDbvtAabbMm* %agg.result to i8*
  %7 = bitcast %struct.btDbvtAabbMm* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 32, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define internal %struct.btDbvtNode* @_ZL10createnodeP6btDbvtP10btDbvtNodeRK12btDbvtAabbMmS5_Pv(%struct.btDbvt* %pdbvt, %struct.btDbvtNode* %parent, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume0, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume1, i8* %data) #2 {
entry:
  %pdbvt.addr = alloca %struct.btDbvt*, align 4
  %parent.addr = alloca %struct.btDbvtNode*, align 4
  %volume0.addr = alloca %struct.btDbvtAabbMm*, align 4
  %volume1.addr = alloca %struct.btDbvtAabbMm*, align 4
  %data.addr = alloca i8*, align 4
  %node = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvt* %pdbvt, %struct.btDbvt** %pdbvt.addr, align 4
  store %struct.btDbvtNode* %parent, %struct.btDbvtNode** %parent.addr, align 4
  store %struct.btDbvtAabbMm* %volume0, %struct.btDbvtAabbMm** %volume0.addr, align 4
  store %struct.btDbvtAabbMm* %volume1, %struct.btDbvtAabbMm** %volume1.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %0 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent.addr, align 4
  %2 = load i8*, i8** %data.addr, align 4
  %call = call %struct.btDbvtNode* @_ZL10createnodeP6btDbvtP10btDbvtNodePv(%struct.btDbvt* %0, %struct.btDbvtNode* %1, i8* %2)
  store %struct.btDbvtNode* %call, %struct.btDbvtNode** %node, align 4
  %3 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume0.addr, align 4
  %4 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume1.addr, align 4
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %5, i32 0, i32 0
  call void @_Z5MergeRK12btDbvtAabbMmS1_RS_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %3, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %4, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume)
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  ret %struct.btDbvtNode* %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE4swapEii(%class.btAlignedObjectArray.1* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.btDbvtNode*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %0, i32 %1
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  store %struct.btDbvtNode* %2, %struct.btDbvtNode** %temp, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %3 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data2, align 4
  %4 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %3, i32 %4
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx3, align 4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %6 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %6, i32 %7
  store %struct.btDbvtNode* %5, %struct.btDbvtNode** %arrayidx5, align 4
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %temp, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %9 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data6, align 4
  %10 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %9, i32 %10
  store %struct.btDbvtNode* %8, %struct.btDbvtNode** %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE8pop_backEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %1 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %1, i32 %2
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btDbvtAabbMm7LengthsEv(%class.btVector3* noalias sret align 4 %agg.result, %struct.btDbvtAabbMm* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %mx, %class.btVector3* nonnull align 4 dereferenceable(16) %mi)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z5MergeRK12btDbvtAabbMmS1_RS_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %b, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %r) #2 comdat {
entry:
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %b.addr = alloca %struct.btDbvtAabbMm*, align 4
  %r.addr = alloca %struct.btDbvtAabbMm*, align 4
  %i = alloca i32, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4
  store %struct.btDbvtAabbMm* %b, %struct.btDbvtAabbMm** %b.addr, align 4
  store %struct.btDbvtAabbMm* %r, %struct.btDbvtAabbMm** %r.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %1, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mi)
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %2
  %3 = load float, float* %arrayidx, align 4
  %4 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi1 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %4, i32 0, i32 0
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mi1)
  %5 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 %5
  %6 = load float, float* %arrayidx3, align 4
  %cmp4 = fcmp olt float %3, %6
  br i1 %cmp4, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %7 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi5 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %7, i32 0, i32 0
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mi5)
  %8 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 %8
  %9 = load float, float* %arrayidx7, align 4
  %10 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %r.addr, align 4
  %mi8 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %10, i32 0, i32 0
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %mi8)
  %11 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %11
  store float %9, float* %arrayidx10, align 4
  br label %if.end

if.else:                                          ; preds = %for.body
  %12 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi11 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %12, i32 0, i32 0
  %call12 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mi11)
  %13 = load i32, i32* %i, align 4
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 %13
  %14 = load float, float* %arrayidx13, align 4
  %15 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %r.addr, align 4
  %mi14 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %15, i32 0, i32 0
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %mi14)
  %16 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 %16
  store float %14, float* %arrayidx16, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %17, i32 0, i32 1
  %call17 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mx)
  %18 = load i32, i32* %i, align 4
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 %18
  %19 = load float, float* %arrayidx18, align 4
  %20 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx19 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %20, i32 0, i32 1
  %call20 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mx19)
  %21 = load i32, i32* %i, align 4
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 %21
  %22 = load float, float* %arrayidx21, align 4
  %cmp22 = fcmp ogt float %19, %22
  br i1 %cmp22, label %if.then23, label %if.else30

if.then23:                                        ; preds = %if.end
  %23 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx24 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %23, i32 0, i32 1
  %call25 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mx24)
  %24 = load i32, i32* %i, align 4
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 %24
  %25 = load float, float* %arrayidx26, align 4
  %26 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %r.addr, align 4
  %mx27 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %26, i32 0, i32 1
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %mx27)
  %27 = load i32, i32* %i, align 4
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 %27
  store float %25, float* %arrayidx29, align 4
  br label %if.end37

if.else30:                                        ; preds = %if.end
  %28 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx31 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %28, i32 0, i32 1
  %call32 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mx31)
  %29 = load i32, i32* %i, align 4
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 %29
  %30 = load float, float* %arrayidx33, align 4
  %31 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %r.addr, align 4
  %mx34 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %31, i32 0, i32 1
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %mx34)
  %32 = load i32, i32* %i, align 4
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 %32
  store float %30, float* %arrayidx36, align 4
  br label %if.end37

if.end37:                                         ; preds = %if.else30, %if.then23
  br label %for.inc

for.inc:                                          ; preds = %if.end37
  %33 = load i32, i32* %i, align 4
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define internal %struct.btDbvtNode* @_ZL10createnodeP6btDbvtP10btDbvtNodePv(%struct.btDbvt* %pdbvt, %struct.btDbvtNode* %parent, i8* %data) #2 {
entry:
  %pdbvt.addr = alloca %struct.btDbvt*, align 4
  %parent.addr = alloca %struct.btDbvtNode*, align 4
  %data.addr = alloca i8*, align 4
  %node = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvt* %pdbvt, %struct.btDbvt** %pdbvt.addr, align 4
  store %struct.btDbvtNode* %parent, %struct.btDbvtNode** %parent.addr, align 4
  store i8* %data, i8** %data.addr, align 4
  %0 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_free = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %0, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_free, align 4
  %tobool = icmp ne %struct.btDbvtNode* %1, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_free1 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %2, i32 0, i32 1
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_free1, align 4
  store %struct.btDbvtNode* %3, %struct.btDbvtNode** %node, align 4
  %4 = load %struct.btDbvt*, %struct.btDbvt** %pdbvt.addr, align 4
  %m_free2 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %4, i32 0, i32 1
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %m_free2, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 44, i32 16)
  %5 = bitcast i8* %call to %struct.btDbvtNode*
  %6 = bitcast %struct.btDbvtNode* %5 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 4 %6, i8 0, i32 44, i1 false)
  %call3 = call %struct.btDbvtNode* @_ZN10btDbvtNodeC2Ev(%struct.btDbvtNode* %5)
  store %struct.btDbvtNode* %5, %struct.btDbvtNode** %node, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %7 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent.addr, align 4
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %parent4 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %8, i32 0, i32 1
  store %struct.btDbvtNode* %7, %struct.btDbvtNode** %parent4, align 4
  %9 = load i8*, i8** %data.addr, align 4
  %10 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %11 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %10, i32 0, i32 2
  %data5 = bitcast %union.anon.0* %11 to i8**
  store i8* %9, i8** %data5, align 4
  %12 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %13 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %12, i32 0, i32 2
  %childs = bitcast %union.anon.0* %13 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 1
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %arrayidx, align 4
  %14 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  ret %struct.btDbvtNode* %14
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btDbvtNode* @_ZN10btDbvtNodeC2Ev(%struct.btDbvtNode* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %this1, i32 0, i32 0
  %call = call %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* %volume)
  ret %struct.btDbvtNode* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mi)
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mx)
  ret %struct.btDbvtAabbMm* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #6

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #6

; Function Attrs: noinline optnone
define internal void @_ZL6boundsRK20btAlignedObjectArrayIP10btDbvtNodeE(%struct.btDbvtAabbMm* noalias sret align 4 %agg.result, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %leaves) #2 {
entry:
  %leaves.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %locals = alloca [32 x i8], align 16
  %ptr = alloca %struct.btDbvtAabbMm*, align 4
  %volume = alloca %struct.btDbvtAabbMm*, align 4
  %i = alloca i32, align 4
  %ni = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %leaves, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %arraydecay = getelementptr inbounds [32 x i8], [32 x i8]* %locals, i32 0, i32 0
  %0 = bitcast i8* %arraydecay to %struct.btDbvtAabbMm*
  store %struct.btDbvtAabbMm* %0, %struct.btDbvtAabbMm** %ptr, align 4
  %1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %ptr, align 4
  store %struct.btDbvtAabbMm* %1, %struct.btDbvtAabbMm** %volume, align 4
  %2 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZNK20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %2, i32 0)
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call, align 4
  %volume1 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %3, i32 0, i32 0
  %4 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume, align 4
  %5 = bitcast %struct.btDbvtAabbMm* %4 to i8*
  %6 = bitcast %struct.btDbvtAabbMm* %volume1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 32, i1 false)
  store i32 1, i32* %i, align 4
  %7 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %7)
  store i32 %call2, i32* %ni, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %i, align 4
  %9 = load i32, i32* %ni, align 4
  %cmp = icmp slt i32 %8, %9
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume, align 4
  %11 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %12 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZNK20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %11, i32 %12)
  %13 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call3, align 4
  %volume4 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %13, i32 0, i32 0
  %14 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume, align 4
  call void @_Z5MergeRK12btDbvtAabbMmS1_RS_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %10, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume4, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %14)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %16 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %volume, align 4
  %17 = bitcast %struct.btDbvtAabbMm* %agg.result to i8*
  %18 = bitcast %struct.btDbvtAabbMm* %16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 32, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btDbvtAabbMm6CenterEv(%class.btVector3* noalias sret align 4 %agg.result, %struct.btDbvtAabbMm* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %mx)
  store float 2.000000e+00, float* %ref.tmp2, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define internal void @_ZL5splitRK20btAlignedObjectArrayIP10btDbvtNodeERS2_S5_RK9btVector3S8_(%class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %leaves, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %left, %class.btAlignedObjectArray.1* nonnull align 4 dereferenceable(17) %right, %class.btVector3* nonnull align 4 dereferenceable(16) %org, %class.btVector3* nonnull align 4 dereferenceable(16) %axis) #2 {
entry:
  %leaves.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %left.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %right.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %org.addr = alloca %class.btVector3*, align 4
  %axis.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %struct.btDbvtNode*, align 4
  %ref.tmp1 = alloca %struct.btDbvtNode*, align 4
  %i = alloca i32, align 4
  %ni = alloca i32, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  store %class.btAlignedObjectArray.1* %leaves, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  store %class.btAlignedObjectArray.1* %left, %class.btAlignedObjectArray.1** %left.addr, align 4
  store %class.btAlignedObjectArray.1* %right, %class.btAlignedObjectArray.1** %right.addr, align 4
  store %class.btVector3* %org, %class.btVector3** %org.addr, align 4
  store %class.btVector3* %axis, %class.btVector3** %axis.addr, align 4
  %0 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %left.addr, align 4
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE6resizeEiRKS1_(%class.btAlignedObjectArray.1* %0, i32 0, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %ref.tmp)
  %1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %right.addr, align 4
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %ref.tmp1, align 4
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE6resizeEiRKS1_(%class.btAlignedObjectArray.1* %1, i32 0, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %ref.tmp1)
  store i32 0, i32* %i, align 4
  %2 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %2)
  store i32 %call, i32* %ni, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %ni, align 4
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load %class.btVector3*, %class.btVector3** %axis.addr, align 4
  %6 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %7 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZNK20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %6, i32 %7)
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call4, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %8, i32 0, i32 0
  call void @_ZNK12btDbvtAabbMm6CenterEv(%class.btVector3* sret align 4 %ref.tmp3, %struct.btDbvtAabbMm* %volume)
  %9 = load %class.btVector3*, %class.btVector3** %org.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %call5 = call float @_Z5btDotRK9btVector3S1_(%class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %cmp6 = fcmp olt float %call5, 0.000000e+00
  br i1 %cmp6, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %10 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %left.addr, align 4
  %11 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %12 = load i32, i32* %i, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZNK20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %11, i32 %12)
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE9push_backERKS1_(%class.btAlignedObjectArray.1* %10, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %call7)
  br label %if.end

if.else:                                          ; preds = %for.body
  %13 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %right.addr, align 4
  %14 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %leaves.addr, align 4
  %15 = load i32, i32* %i, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZNK20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %14, i32 %15)
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE9push_backERKS1_(%class.btAlignedObjectArray.1* %13, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %call8)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %16 = load i32, i32* %i, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZNK20btAlignedObjectArrayIP10btDbvtNodeEixEi(%class.btAlignedObjectArray.1* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %0, i32 %1
  ret %struct.btDbvtNode** %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  %2 = load float, float* %1, align 4
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #7

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE6resizeEiRKS1_(%class.btAlignedObjectArray.1* %this, i32 %newsize, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btDbvtNode**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btDbvtNode** %fillData, %struct.btDbvtNode*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %5 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.1* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %14 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %14, i32 %15
  %16 = bitcast %struct.btDbvtNode** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %struct.btDbvtNode**
  %18 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %fillData.addr, align 4
  %19 = load %struct.btDbvtNode*, %struct.btDbvtNode** %18, align 4
  store %struct.btDbvtNode* %19, %struct.btDbvtNode** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define internal i32 @_ZL7indexofPK10btDbvtNode(%struct.btDbvtNode* %node) #1 {
entry:
  %node.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %node, %struct.btDbvtNode** %node.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %parent = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %0, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %parent, align 4
  %2 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %1, i32 0, i32 2
  %childs = bitcast %union.anon.0* %2 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 1
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node.addr, align 4
  %cmp = icmp eq %struct.btDbvtNode* %3, %4
  %conv = zext i1 %cmp to i32
  ret i32 %conv
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z6btSwapI12btDbvtAabbMmEvRT_S2_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %b) #1 comdat {
entry:
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %b.addr = alloca %struct.btDbvtAabbMm*, align 4
  %tmp = alloca %struct.btDbvtAabbMm, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4
  store %struct.btDbvtAabbMm* %b, %struct.btDbvtAabbMm** %b.addr, align 4
  %0 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %1 = bitcast %struct.btDbvtAabbMm* %tmp to i8*
  %2 = bitcast %struct.btDbvtAabbMm* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 32, i1 false)
  %3 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %4 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %5 = bitcast %struct.btDbvtAabbMm* %4 to i8*
  %6 = bitcast %struct.btDbvtAabbMm* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 32, i1 false)
  %7 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %8 = bitcast %struct.btDbvtAabbMm* %7 to i8*
  %9 = bitcast %struct.btDbvtAabbMm* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 32, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z6SelectRK12btDbvtAabbMmS1_S1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %o, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %b) #2 comdat {
entry:
  %o.addr = alloca %struct.btDbvtAabbMm*, align 4
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %b.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %o, %struct.btDbvtAabbMm** %o.addr, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4
  store %struct.btDbvtAabbMm* %b, %struct.btDbvtAabbMm** %b.addr, align 4
  %0 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %o.addr, align 4
  %1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %call = call float @_Z9ProximityRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %0, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %1)
  %2 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %o.addr, align 4
  %3 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %call1 = call float @_Z9ProximityRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %2, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %3)
  %cmp = fcmp olt float %call, %call1
  %4 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i32 0, i32 1
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_Z9ProximityRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %b) #2 comdat {
entry:
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %b.addr = alloca %struct.btDbvtAabbMm*, align 4
  %d = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4
  store %struct.btDbvtAabbMm* %b, %struct.btDbvtAabbMm** %b.addr, align 4
  %0 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %0, i32 0, i32 0
  %1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %mx)
  %2 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %2, i32 0, i32 0
  %3 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx3 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %3, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %mi2, %class.btVector3* nonnull align 4 dereferenceable(16) %mx3)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %d, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %d)
  %4 = load float, float* %call, align 4
  %call4 = call float @_Z6btFabsf(float %4)
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %d)
  %5 = load float, float* %call5, align 4
  %call6 = call float @_Z6btFabsf(float %5)
  %add = fadd float %call4, %call6
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %d)
  %6 = load float, float* %call7, align 4
  %call8 = call float @_Z6btFabsf(float %6)
  %add9 = fadd float %add, %call8
  ret float %add9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_Z8NotEqualRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %b) #1 comdat {
entry:
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %b.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4
  store %struct.btDbvtAabbMm* %b, %struct.btDbvtAabbMm** %b.addr, align 4
  %0 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %0, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi)
  %1 = load float, float* %call, align 4
  %2 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi1 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %2, i32 0, i32 0
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi1)
  %3 = load float, float* %call2, align 4
  %cmp = fcmp une float %1, %3
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %4 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi3 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %4, i32 0, i32 0
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi3)
  %5 = load float, float* %call4, align 4
  %6 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi5 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %6, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi5)
  %7 = load float, float* %call6, align 4
  %cmp7 = fcmp une float %5, %7
  br i1 %cmp7, label %lor.end, label %lor.lhs.false8

lor.lhs.false8:                                   ; preds = %lor.lhs.false
  %8 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi9 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi9)
  %9 = load float, float* %call10, align 4
  %10 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi11 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %10, i32 0, i32 0
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi11)
  %11 = load float, float* %call12, align 4
  %cmp13 = fcmp une float %9, %11
  br i1 %cmp13, label %lor.end, label %lor.lhs.false14

lor.lhs.false14:                                  ; preds = %lor.lhs.false8
  %12 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %12, i32 0, i32 1
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx)
  %13 = load float, float* %call15, align 4
  %14 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx16 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %14, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx16)
  %15 = load float, float* %call17, align 4
  %cmp18 = fcmp une float %13, %15
  br i1 %cmp18, label %lor.end, label %lor.lhs.false19

lor.lhs.false19:                                  ; preds = %lor.lhs.false14
  %16 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx20 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %16, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx20)
  %17 = load float, float* %call21, align 4
  %18 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx22 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %18, i32 0, i32 1
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx22)
  %19 = load float, float* %call23, align 4
  %cmp24 = fcmp une float %17, %19
  br i1 %cmp24, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %lor.lhs.false19
  %20 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx25 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %20, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx25)
  %21 = load float, float* %call26, align 4
  %22 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx27 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %22, i32 0, i32 1
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx27)
  %23 = load float, float* %call28, align 4
  %cmp29 = fcmp une float %21, %23
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %lor.lhs.false19, %lor.lhs.false14, %lor.lhs.false8, %lor.lhs.false, %entry
  %24 = phi i1 [ true, %lor.lhs.false19 ], [ true, %lor.lhs.false14 ], [ true, %lor.lhs.false8 ], [ true, %lor.lhs.false ], [ true, %entry ], [ %cmp29, %lor.rhs ]
  ret i1 %24
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mIERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %sub = fsub float %2, %1
  store float %sub, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %4
  store float %sub8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %sub13 = fsub float %8, %7
  store float %sub13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector34setXEf(%class.btVector3* %this, float %_x) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float %_x, float* %_x.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_x.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector34setYEf(%class.btVector3* %this, float %_y) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_y.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float %_y, float* %_y.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_y.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector34setZEf(%class.btVector3* %this, float %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_z.addr = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float %_z, float* %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float, float* %_z.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  store float %0, float* %arrayidx, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideC2Ev(%"struct.btDbvt::ICollide"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %0 = bitcast %"struct.btDbvt::ICollide"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTVN6btDbvt8ICollideE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %"struct.btDbvt::ICollide"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2Ev(%class.btAlignedObjectArray.5* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.6* @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EEC2Ev(%class.btAlignedAllocator.6* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv(%class.btAlignedObjectArray.5* %this1)
  ret %class.btAlignedObjectArray.5* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btDbvtNodeEnumeratorD0Ev(%struct.btDbvtNodeEnumerator* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNodeEnumerator*, align 4
  store %struct.btDbvtNodeEnumerator* %this, %struct.btDbvtNodeEnumerator** %this.addr, align 4
  %this1 = load %struct.btDbvtNodeEnumerator*, %struct.btDbvtNodeEnumerator** %this.addr, align 4
  %call = call %struct.btDbvtNodeEnumerator* @_ZN20btDbvtNodeEnumeratorD2Ev(%struct.btDbvtNodeEnumerator* %this1) #6
  %0 = bitcast %struct.btDbvtNodeEnumerator* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0, %struct.btDbvtNode* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  %.addr1 = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  store %struct.btDbvtNode* %1, %struct.btDbvtNode** %.addr1, align 4
  %this2 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btDbvtNodeEnumerator7ProcessEPK10btDbvtNode(%struct.btDbvtNodeEnumerator* %this, %struct.btDbvtNode* %n) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNodeEnumerator*, align 4
  %n.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNodeEnumerator* %this, %struct.btDbvtNodeEnumerator** %this.addr, align 4
  store %struct.btDbvtNode* %n, %struct.btDbvtNode** %n.addr, align 4
  %this1 = load %struct.btDbvtNodeEnumerator*, %struct.btDbvtNodeEnumerator** %this.addr, align 4
  %nodes = getelementptr inbounds %struct.btDbvtNodeEnumerator, %struct.btDbvtNodeEnumerator* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray.5* %nodes, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %n.addr)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %n, float %0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %n.addr = alloca %struct.btDbvtNode*, align 4
  %.addr = alloca float, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %n, %struct.btDbvtNode** %n.addr, align 4
  store float %0, float* %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %2 = bitcast %"struct.btDbvt::ICollide"* %this1 to void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)***
  %vtable = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)**, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*** %2, align 4
  %vfn = getelementptr inbounds void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vtable, i64 3
  %3 = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vfn, align 4
  call void %3(%"struct.btDbvt::ICollide"* %this1, %struct.btDbvtNode* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret %"struct.btDbvt::ICollide"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollideD0Ev(%"struct.btDbvt::ICollide"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* %this1) #6
  %0 = bitcast %"struct.btDbvt::ICollide"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #8

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.6* @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EEC2Ev(%class.btAlignedAllocator.6* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  ret %class.btAlignedAllocator.6* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv(%class.btAlignedObjectArray.5* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store %struct.btDbvtNode** null, %struct.btDbvtNode*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.5* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev(%class.btAlignedObjectArray.5* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE5clearEv(%class.btAlignedObjectArray.5* %this1)
  ret %class.btAlignedObjectArray.5* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE5clearEv(%class.btAlignedObjectArray.5* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray.5* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray.5* %this1)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv(%class.btAlignedObjectArray.5* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray.5* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %3 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray.5* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %tobool = icmp ne %struct.btDbvtNode** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %2 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE10deallocateEPS2_(%class.btAlignedAllocator.6* %m_allocator, %struct.btDbvtNode** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  store %struct.btDbvtNode** null, %struct.btDbvtNode*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE10deallocateEPS2_(%class.btAlignedAllocator.6* %this, %struct.btDbvtNode** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  %ptr.addr = alloca %struct.btDbvtNode**, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4
  store %struct.btDbvtNode** %ptr, %struct.btDbvtNode*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %ptr.addr, align 4
  %1 = bitcast %struct.btDbvtNode** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %a, i32* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca i32*, align 4
  %b.addr = alloca i32*, align 4
  store i32* %a, i32** %a.addr, align 4
  store i32* %b, i32** %b.addr, align 4
  %0 = load i32*, i32** %a.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %b.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp sgt i32 %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32*, i32** %a.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load i32*, i32** %b.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %4, %cond.true ], [ %5, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %"struct.btDbvt::sStkNN"* null, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray.5* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi(%class.btAlignedObjectArray.5* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btDbvtNode** @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.6* %m_allocator, i32 %1, %struct.btDbvtNode*** null)
  %2 = bitcast %struct.btDbvtNode** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_(%class.btAlignedObjectArray.5* %this, i32 %start, i32 %end, %struct.btDbvtNode** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btDbvtNode**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btDbvtNode** %dest, %struct.btDbvtNode*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %3, i32 %4
  %5 = bitcast %struct.btDbvtNode** %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btDbvtNode**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.5, %class.btAlignedObjectArray.5* %this1, i32 0, i32 4
  %7 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %7, i32 %8
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx2, align 4
  store %struct.btDbvtNode* %9, %struct.btDbvtNode** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btDbvtNode** @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.6* %this, i32 %n, %struct.btDbvtNode*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.6*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btDbvtNode***, align 4
  store %class.btAlignedAllocator.6* %this, %class.btAlignedAllocator.6** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btDbvtNode*** %hint, %struct.btDbvtNode**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.6*, %class.btAlignedAllocator.6** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btDbvtNode**
  ret %struct.btDbvtNode** %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9allocSizeEi(%class.btAlignedObjectArray.5* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.5*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.5* %this, %class.btAlignedObjectArray.5** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.5*, %class.btAlignedObjectArray.5** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.2* @_ZN18btAlignedAllocatorIP10btDbvtNodeLj16EEC2Ev(%class.btAlignedAllocator.2* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  ret %class.btAlignedAllocator.2* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE4initEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %struct.btDbvtNode** null, %struct.btDbvtNode*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE5clearEv(%class.btAlignedObjectArray.1* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE4initEv(%class.btAlignedObjectArray.1* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray.1* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %3 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray.1* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %tobool = icmp ne %struct.btDbvtNode** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %2 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP10btDbvtNodeLj16EE10deallocateEPS1_(%class.btAlignedAllocator.2* %m_allocator, %struct.btDbvtNode** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %struct.btDbvtNode** null, %struct.btDbvtNode*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP10btDbvtNodeLj16EE10deallocateEPS1_(%class.btAlignedAllocator.2* %this, %struct.btDbvtNode** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  %ptr.addr = alloca %struct.btDbvtNode**, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  store %struct.btDbvtNode** %ptr, %struct.btDbvtNode*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %ptr.addr, align 4
  %1 = bitcast %struct.btDbvtNode** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP10btDbvtNodeE8allocateEi(%class.btAlignedObjectArray.1* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btDbvtNode** @_ZN18btAlignedAllocatorIP10btDbvtNodeLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.2* %m_allocator, i32 %1, %struct.btDbvtNode*** null)
  %2 = bitcast %struct.btDbvtNode** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP10btDbvtNodeE4copyEiiPS1_(%class.btAlignedObjectArray.1* %this, i32 %start, i32 %end, %struct.btDbvtNode** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btDbvtNode**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btDbvtNode** %dest, %struct.btDbvtNode*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %3, i32 %4
  %5 = bitcast %struct.btDbvtNode** %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btDbvtNode**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %7 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %7, i32 %8
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx2, align 4
  store %struct.btDbvtNode* %9, %struct.btDbvtNode** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btDbvtNode** @_ZN18btAlignedAllocatorIP10btDbvtNodeLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.2* %this, i32 %n, %struct.btDbvtNode*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btDbvtNode***, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btDbvtNode*** %hint, %struct.btDbvtNode**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btDbvtNode**
  ret %struct.btDbvtNode** %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %tobool = icmp ne %"struct.btDbvt::sStkNN"* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %"struct.btDbvt::sStkNN"* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %"struct.btDbvt::sStkNN"* null, %"struct.btDbvt::sStkNN"** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %"struct.btDbvt::sStkNN"* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %"struct.btDbvt::sStkNN"* %ptr, %"struct.btDbvt::sStkNN"** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %ptr.addr, align 4
  %1 = bitcast %"struct.btDbvt::sStkNN"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.10* @_ZN18btAlignedAllocatorIN6btDbvt7sStkCLNELj16EEC2Ev(%class.btAlignedAllocator.10* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  ret %class.btAlignedAllocator.10* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4initEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %"struct.btDbvt::sStkCLN"* null, %"struct.btDbvt::sStkCLN"** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE5clearEv(%class.btAlignedObjectArray.9* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4initEv(%class.btAlignedObjectArray.9* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE7destroyEii(%class.btAlignedObjectArray.9* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %3 = load %"struct.btDbvt::sStkCLN"*, %"struct.btDbvt::sStkCLN"** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE10deallocateEv(%class.btAlignedObjectArray.9* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %"struct.btDbvt::sStkCLN"*, %"struct.btDbvt::sStkCLN"** %m_data, align 4
  %tobool = icmp ne %"struct.btDbvt::sStkCLN"* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load %"struct.btDbvt::sStkCLN"*, %"struct.btDbvt::sStkCLN"** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIN6btDbvt7sStkCLNELj16EE10deallocateEPS1_(%class.btAlignedAllocator.10* %m_allocator, %"struct.btDbvt::sStkCLN"* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %"struct.btDbvt::sStkCLN"* null, %"struct.btDbvt::sStkCLN"** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIN6btDbvt7sStkCLNELj16EE10deallocateEPS1_(%class.btAlignedAllocator.10* %this, %"struct.btDbvt::sStkCLN"* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %ptr.addr = alloca %"struct.btDbvt::sStkCLN"*, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4
  store %"struct.btDbvt::sStkCLN"* %ptr, %"struct.btDbvt::sStkCLN"** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load %"struct.btDbvt::sStkCLN"*, %"struct.btDbvt::sStkCLN"** %ptr.addr, align 4
  %1 = bitcast %"struct.btDbvt::sStkCLN"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE8capacityEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE8allocateEi(%class.btAlignedObjectArray.9* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %"struct.btDbvt::sStkCLN"* @_ZN18btAlignedAllocatorIN6btDbvt7sStkCLNELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.10* %m_allocator, i32 %1, %"struct.btDbvt::sStkCLN"** null)
  %2 = bitcast %"struct.btDbvt::sStkCLN"* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIN6btDbvt7sStkCLNEE4copyEiiPS1_(%class.btAlignedObjectArray.9* %this, i32 %start, i32 %end, %"struct.btDbvt::sStkCLN"* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"struct.btDbvt::sStkCLN"*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %"struct.btDbvt::sStkCLN"* %dest, %"struct.btDbvt::sStkCLN"** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %"struct.btDbvt::sStkCLN"*, %"struct.btDbvt::sStkCLN"** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %3, i32 %4
  %5 = bitcast %"struct.btDbvt::sStkCLN"* %arrayidx to i8*
  %6 = bitcast i8* %5 to %"struct.btDbvt::sStkCLN"*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %7 = load %"struct.btDbvt::sStkCLN"*, %"struct.btDbvt::sStkCLN"** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %"struct.btDbvt::sStkCLN", %"struct.btDbvt::sStkCLN"* %7, i32 %8
  %9 = bitcast %"struct.btDbvt::sStkCLN"* %6 to i8*
  %10 = bitcast %"struct.btDbvt::sStkCLN"* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 8, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btDbvt::sStkCLN"* @_ZN18btAlignedAllocatorIN6btDbvt7sStkCLNELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.10* %this, i32 %n, %"struct.btDbvt::sStkCLN"** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"struct.btDbvt::sStkCLN"**, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %"struct.btDbvt::sStkCLN"** %hint, %"struct.btDbvt::sStkCLN"*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 8, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"struct.btDbvt::sStkCLN"*
  ret %"struct.btDbvt::sStkCLN"* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIN6btDbvt7sStkCLNEE9allocSizeEi(%class.btAlignedObjectArray.9* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btDbvt.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { argmemonly nounwind willreturn writeonly }
attributes #6 = { nounwind }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1048575}
