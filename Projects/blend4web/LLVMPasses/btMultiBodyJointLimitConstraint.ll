; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Featherstone/btMultiBodyJointLimitConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Featherstone/btMultiBodyJointLimitConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btMultiBodyJointLimitConstraint = type { %class.btMultiBodyConstraint, float, float }
%class.btMultiBodyConstraint = type { i32 (...)**, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i32, i32, i32, i8, i32, float, %class.btAlignedObjectArray.8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btMultiBody = type <{ i32 (...)**, %class.btMultiBodyLinkCollider*, i8*, %class.btVector3, %class.btQuaternion, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, i8, i8, i8, i8, float, i8*, i32, i32, i32, float, float, i8, [3 x i8], float, float, i8, i8, [2 x i8], i32, i32, i8, i8, i8, i8 }>
%class.btMultiBodyLinkCollider = type { %class.btCollisionObject, %class.btMultiBody*, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type opaque
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btMultibodyLink*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btMultibodyLink = type { float, %class.btVector3, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %struct.btSpatialMotionVector, %struct.btSpatialMotionVector, [6 x %struct.btSpatialMotionVector], i32, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [7 x float], [6 x float], %class.btMultiBodyLinkCollider*, i32, i32, i32, i32, %struct.btMultiBodyJointFeedback*, %class.btTransform, i8*, i8*, i8*, float, float }
%struct.btSpatialMotionVector = type { %class.btVector3, %class.btVector3 }
%struct.btMultiBodyJointFeedback = type opaque
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btMultiBodyLinkCollider**, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btMatrix3x3*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, %struct.btMultiBodySolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }
%struct.btMultiBodySolverConstraint = type { i32, i32, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.23, i32, i32, i32, %class.btMultiBody*, i32, i32, %class.btMultiBody*, i32, %class.btMultiBodyConstraint*, i32 }
%union.anon.23 = type { i8* }
%struct.btMultiBodyJacobianData = type { %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.25*, i32 }
%class.btAlignedObjectArray.25 = type opaque
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float, float }
%class.btIDebugDraw = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN11btMultiBody7getLinkEi = comdat any

$_ZN21btMultiBodyConstraint9jacobianAEi = comdat any

$_ZN21btMultiBodyConstraint9jacobianBEi = comdat any

$_ZN21btMultiBodyConstraintdlEPv = comdat any

$_ZN11btMultiBody15getBaseColliderEv = comdat any

$_ZNK17btCollisionObject12getIslandTagEv = comdat any

$_ZNK11btMultiBody11getNumLinksEv = comdat any

$_ZN21btMultiBodyConstraint11setPositionEif = comdat any

$_ZNK21btMultiBodyConstraint10getNumRowsEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector37setZeroEv = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_Z10quatRotateRK12btQuaternionRK9btVector3 = comdat any

$_ZNK11btTransform11getRotationEv = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK21btMultiBodyConstraint11getPositionEi = comdat any

$_ZN21btMultiBodyConstraint11setFrameInBERK11btMatrix3x3 = comdat any

$_ZN21btMultiBodyConstraint11setPivotInBERK9btVector3 = comdat any

$_ZN31btMultiBodyJointLimitConstraint9debugDrawEP12btIDebugDraw = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZmlRK12btQuaternionRK9btVector3 = comdat any

$_ZNK12btQuaternion7inverseEv = comdat any

$_ZN12btQuaternionmLERKS_ = comdat any

$_ZNK10btQuadWord4getXEv = comdat any

$_ZNK10btQuadWord4getYEv = comdat any

$_ZNK10btQuadWord4getZEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZN12btQuaternionC2ERKfS1_S1_S1_ = comdat any

$_ZN10btQuadWordC2ERKfS1_S1_S1_ = comdat any

$_ZN10btQuadWord8setValueERKfS1_S1_S1_ = comdat any

$_ZN12btQuaternionC2Ev = comdat any

$_ZNK11btMatrix3x311getRotationER12btQuaternion = comdat any

$_ZN10btQuadWordC2Ev = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK20btAlignedObjectArrayIfEixEi = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_ = comdat any

$_ZN27btMultiBodySolverConstraintnwEmPv = comdat any

$_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV31btMultiBodyJointLimitConstraint = hidden unnamed_addr constant { [11 x i8*] } { [11 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI31btMultiBodyJointLimitConstraint to i8*), i8* bitcast (%class.btMultiBodyJointLimitConstraint* (%class.btMultiBodyJointLimitConstraint*)* @_ZN31btMultiBodyJointLimitConstraintD1Ev to i8*), i8* bitcast (void (%class.btMultiBodyJointLimitConstraint*)* @_ZN31btMultiBodyJointLimitConstraintD0Ev to i8*), i8* bitcast (void (%class.btMultiBodyConstraint*, %class.btMatrix3x3*)* @_ZN21btMultiBodyConstraint11setFrameInBERK11btMatrix3x3 to i8*), i8* bitcast (void (%class.btMultiBodyConstraint*, %class.btVector3*)* @_ZN21btMultiBodyConstraint11setPivotInBERK9btVector3 to i8*), i8* bitcast (void (%class.btMultiBodyJointLimitConstraint*)* @_ZN31btMultiBodyJointLimitConstraint16finalizeMultiDofEv to i8*), i8* bitcast (i32 (%class.btMultiBodyJointLimitConstraint*)* @_ZNK31btMultiBodyJointLimitConstraint12getIslandIdAEv to i8*), i8* bitcast (i32 (%class.btMultiBodyJointLimitConstraint*)* @_ZNK31btMultiBodyJointLimitConstraint12getIslandIdBEv to i8*), i8* bitcast (void (%class.btMultiBodyJointLimitConstraint*, %class.btAlignedObjectArray.20*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)* @_ZN31btMultiBodyJointLimitConstraint20createConstraintRowsER20btAlignedObjectArrayI27btMultiBodySolverConstraintER23btMultiBodyJacobianDataRK19btContactSolverInfo to i8*), i8* bitcast (void (%class.btMultiBodyJointLimitConstraint*, %class.btIDebugDraw*)* @_ZN31btMultiBodyJointLimitConstraint9debugDrawEP12btIDebugDraw to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS31btMultiBodyJointLimitConstraint = hidden constant [34 x i8] c"31btMultiBodyJointLimitConstraint\00", align 1
@_ZTI21btMultiBodyConstraint = external constant i8*
@_ZTI31btMultiBodyJointLimitConstraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([34 x i8], [34 x i8]* @_ZTS31btMultiBodyJointLimitConstraint, i32 0, i32 0), i8* bitcast (i8** @_ZTI21btMultiBodyConstraint to i8*) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btMultiBodyJointLimitConstraint.cpp, i8* null }]

@_ZN31btMultiBodyJointLimitConstraintC1EP11btMultiBodyiff = hidden unnamed_addr alias %class.btMultiBodyJointLimitConstraint* (%class.btMultiBodyJointLimitConstraint*, %class.btMultiBody*, i32, float, float), %class.btMultiBodyJointLimitConstraint* (%class.btMultiBodyJointLimitConstraint*, %class.btMultiBody*, i32, float, float)* @_ZN31btMultiBodyJointLimitConstraintC2EP11btMultiBodyiff
@_ZN31btMultiBodyJointLimitConstraintD1Ev = hidden unnamed_addr alias %class.btMultiBodyJointLimitConstraint* (%class.btMultiBodyJointLimitConstraint*), %class.btMultiBodyJointLimitConstraint* (%class.btMultiBodyJointLimitConstraint*)* @_ZN31btMultiBodyJointLimitConstraintD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btMultiBodyJointLimitConstraint* @_ZN31btMultiBodyJointLimitConstraintC2EP11btMultiBodyiff(%class.btMultiBodyJointLimitConstraint* returned %this, %class.btMultiBody* %body, i32 %link, float %lower, float %upper) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyJointLimitConstraint*, align 4
  %body.addr = alloca %class.btMultiBody*, align 4
  %link.addr = alloca i32, align 4
  %lower.addr = alloca float, align 4
  %upper.addr = alloca float, align 4
  store %class.btMultiBodyJointLimitConstraint* %this, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  store %class.btMultiBody* %body, %class.btMultiBody** %body.addr, align 4
  store i32 %link, i32* %link.addr, align 4
  store float %lower, float* %lower.addr, align 4
  store float %upper, float* %upper.addr, align 4
  %this1 = load %class.btMultiBodyJointLimitConstraint*, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %1 = load %class.btMultiBody*, %class.btMultiBody** %body.addr, align 4
  %2 = load %class.btMultiBody*, %class.btMultiBody** %body.addr, align 4
  %3 = load i32, i32* %link.addr, align 4
  %4 = load %class.btMultiBody*, %class.btMultiBody** %body.addr, align 4
  %5 = load i32, i32* %link.addr, align 4
  %call = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %4, i32 %5)
  %m_parent = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 2
  %6 = load i32, i32* %m_parent, align 4
  %call2 = call %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintC2EP11btMultiBodyS1_iiib(%class.btMultiBodyConstraint* %0, %class.btMultiBody* %1, %class.btMultiBody* %2, i32 %3, i32 %6, i32 2, i1 zeroext true)
  %7 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTV31btMultiBodyJointLimitConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %7, align 4
  %m_lowerBound = getelementptr inbounds %class.btMultiBodyJointLimitConstraint, %class.btMultiBodyJointLimitConstraint* %this1, i32 0, i32 1
  %8 = load float, float* %lower.addr, align 4
  store float %8, float* %m_lowerBound, align 4
  %m_upperBound = getelementptr inbounds %class.btMultiBodyJointLimitConstraint, %class.btMultiBodyJointLimitConstraint* %this1, i32 0, i32 2
  %9 = load float, float* %upper.addr, align 4
  store float %9, float* %m_upperBound, align 4
  ret %class.btMultiBodyJointLimitConstraint* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %index.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 11
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray.0* %m_links, i32 %0)
  ret %struct.btMultibodyLink* %call
}

declare %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintC2EP11btMultiBodyS1_iiib(%class.btMultiBodyConstraint* returned, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i1 zeroext) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN31btMultiBodyJointLimitConstraint16finalizeMultiDofEv(%class.btMultiBodyJointLimitConstraint* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyJointLimitConstraint*, align 4
  %offset = alloca i32, align 4
  store %class.btMultiBodyJointLimitConstraint* %this, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyJointLimitConstraint*, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  call void @_ZN21btMultiBodyConstraint25allocateJacobiansMultiDofEv(%class.btMultiBodyConstraint* %0)
  %1 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %1, i32 0, i32 1
  %2 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4
  %3 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %3, i32 0, i32 3
  %4 = load i32, i32* %m_linkA, align 4
  %call = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %2, i32 %4)
  %m_dofOffset = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call, i32 0, i32 9
  %5 = load i32, i32* %m_dofOffset, align 4
  %add = add nsw i32 6, %5
  store i32 %add, i32* %offset, align 4
  %6 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %call2 = call float* @_ZN21btMultiBodyConstraint9jacobianAEi(%class.btMultiBodyConstraint* %6, i32 0)
  %7 = load i32, i32* %offset, align 4
  %arrayidx = getelementptr inbounds float, float* %call2, i32 %7
  store float 1.000000e+00, float* %arrayidx, align 4
  %8 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %call3 = call float* @_ZN21btMultiBodyConstraint9jacobianBEi(%class.btMultiBodyConstraint* %8, i32 1)
  %9 = load i32, i32* %offset, align 4
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 %9
  store float -1.000000e+00, float* %arrayidx4, align 4
  %10 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_jacSizeBoth = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %10, i32 0, i32 7
  %11 = load i32, i32* %m_jacSizeBoth, align 4
  %12 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_numDofsFinalized = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %12, i32 0, i32 10
  store i32 %11, i32* %m_numDofsFinalized, align 4
  ret void
}

declare void @_ZN21btMultiBodyConstraint25allocateJacobiansMultiDofEv(%class.btMultiBodyConstraint*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden float* @_ZN21btMultiBodyConstraint9jacobianAEi(%class.btMultiBodyConstraint* %this, i32 %row) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %row.addr = alloca i32, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 12
  %m_numRows = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_numRows, align 4
  %1 = load i32, i32* %row.addr, align 4
  %m_jacSizeBoth = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 7
  %2 = load i32, i32* %m_jacSizeBoth, align 4
  %mul = mul nsw i32 %1, %2
  %add = add nsw i32 %0, %mul
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.8* %m_data, i32 %add)
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN21btMultiBodyConstraint9jacobianBEi(%class.btMultiBodyConstraint* %this, i32 %row) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %row.addr = alloca i32, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 12
  %m_numRows = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_numRows, align 4
  %1 = load i32, i32* %row.addr, align 4
  %m_jacSizeBoth = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 7
  %2 = load i32, i32* %m_jacSizeBoth, align 4
  %mul = mul nsw i32 %1, %2
  %add = add nsw i32 %0, %mul
  %m_jacSizeA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 6
  %3 = load i32, i32* %m_jacSizeA, align 4
  %add2 = add nsw i32 %add, %3
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.8* %m_data, i32 %add2)
  ret float* %call
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btMultiBodyJointLimitConstraint* @_ZN31btMultiBodyJointLimitConstraintD2Ev(%class.btMultiBodyJointLimitConstraint* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btMultiBodyJointLimitConstraint*, align 4
  store %class.btMultiBodyJointLimitConstraint* %this, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyJointLimitConstraint*, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %call = call %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintD2Ev(%class.btMultiBodyConstraint* %0) #7
  ret %class.btMultiBodyJointLimitConstraint* %this1
}

; Function Attrs: nounwind
declare %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintD2Ev(%class.btMultiBodyConstraint* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN31btMultiBodyJointLimitConstraintD0Ev(%class.btMultiBodyJointLimitConstraint* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btMultiBodyJointLimitConstraint*, align 4
  store %class.btMultiBodyJointLimitConstraint* %this, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyJointLimitConstraint*, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %call = call %class.btMultiBodyJointLimitConstraint* @_ZN31btMultiBodyJointLimitConstraintD1Ev(%class.btMultiBodyJointLimitConstraint* %this1) #7
  %0 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to i8*
  call void @_ZN21btMultiBodyConstraintdlEPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btMultiBodyConstraintdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK31btMultiBodyJointLimitConstraint12getIslandIdAEv(%class.btMultiBodyJointLimitConstraint* %this) unnamed_addr #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btMultiBodyJointLimitConstraint*, align 4
  %col = alloca %class.btMultiBodyLinkCollider*, align 4
  %i = alloca i32, align 4
  store %class.btMultiBodyJointLimitConstraint* %this, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyJointLimitConstraint*, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %0, i32 0, i32 1
  %1 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4
  %tobool = icmp ne %class.btMultiBody* %1, null
  br i1 %tobool, label %if.then, label %if.end17

if.then:                                          ; preds = %entry
  %2 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA2 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %2, i32 0, i32 1
  %3 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA2, align 4
  %call = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %3)
  store %class.btMultiBodyLinkCollider* %call, %class.btMultiBodyLinkCollider** %col, align 4
  %4 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %tobool3 = icmp ne %class.btMultiBodyLinkCollider* %4, null
  br i1 %tobool3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then
  %5 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %6 = bitcast %class.btMultiBodyLinkCollider* %5 to %class.btCollisionObject*
  %call5 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %6)
  store i32 %call5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %7 = load i32, i32* %i, align 4
  %8 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA6 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %8, i32 0, i32 1
  %9 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA6, align 4
  %call7 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %9)
  %cmp = icmp slt i32 %7, %call7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA8 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %10, i32 0, i32 1
  %11 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA8, align 4
  %12 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %11, i32 %12)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call9, i32 0, i32 19
  %13 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4
  %tobool10 = icmp ne %class.btMultiBodyLinkCollider* %13, null
  br i1 %tobool10, label %if.then11, label %if.end16

if.then11:                                        ; preds = %for.body
  %14 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA12 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %14, i32 0, i32 1
  %15 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA12, align 4
  %16 = load i32, i32* %i, align 4
  %call13 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %15, i32 %16)
  %m_collider14 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call13, i32 0, i32 19
  %17 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider14, align 4
  %18 = bitcast %class.btMultiBodyLinkCollider* %17 to %class.btCollisionObject*
  %call15 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %18)
  store i32 %call15, i32* %retval, align 4
  br label %return

if.end16:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end16
  %19 = load i32, i32* %i, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end17

if.end17:                                         ; preds = %for.end, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end17, %if.then11, %if.then4
  %20 = load i32, i32* %retval, align 4
  ret i32 %20
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_baseCollider = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 1
  %0 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_baseCollider, align 4
  ret %class.btMultiBodyLinkCollider* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  %0 = load i32, i32* %m_islandTag1, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 11
  %call = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray.0* %m_links)
  ret i32 %call
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK31btMultiBodyJointLimitConstraint12getIslandIdBEv(%class.btMultiBodyJointLimitConstraint* %this) unnamed_addr #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btMultiBodyJointLimitConstraint*, align 4
  %col = alloca %class.btMultiBodyLinkCollider*, align 4
  %i = alloca i32, align 4
  store %class.btMultiBodyJointLimitConstraint* %this, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyJointLimitConstraint*, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %0, i32 0, i32 2
  %1 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB, align 4
  %tobool = icmp ne %class.btMultiBody* %1, null
  br i1 %tobool, label %if.then, label %if.end14

if.then:                                          ; preds = %entry
  %2 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB2 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %2, i32 0, i32 2
  %3 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB2, align 4
  %call = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %3)
  store %class.btMultiBodyLinkCollider* %call, %class.btMultiBodyLinkCollider** %col, align 4
  %4 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %tobool3 = icmp ne %class.btMultiBodyLinkCollider* %4, null
  br i1 %tobool3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.then
  %5 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %6 = bitcast %class.btMultiBodyLinkCollider* %5 to %class.btCollisionObject*
  %call5 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %6)
  store i32 %call5, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %7 = load i32, i32* %i, align 4
  %8 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB6 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %8, i32 0, i32 2
  %9 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB6, align 4
  %call7 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %9)
  %cmp = icmp slt i32 %7, %call7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB8 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %10, i32 0, i32 2
  %11 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB8, align 4
  %12 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %11, i32 %12)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call9, i32 0, i32 19
  %13 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4
  store %class.btMultiBodyLinkCollider* %13, %class.btMultiBodyLinkCollider** %col, align 4
  %14 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %tobool10 = icmp ne %class.btMultiBodyLinkCollider* %14, null
  br i1 %tobool10, label %if.then11, label %if.end13

if.then11:                                        ; preds = %for.body
  %15 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %16 = bitcast %class.btMultiBodyLinkCollider* %15 to %class.btCollisionObject*
  %call12 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %16)
  store i32 %call12, i32* %retval, align 4
  br label %return

if.end13:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end13
  %17 = load i32, i32* %i, align 4
  %inc = add nsw i32 %17, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.end14:                                         ; preds = %for.end, %entry
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end14, %if.then11, %if.then4
  %18 = load i32, i32* %retval, align 4
  ret i32 %18
}

; Function Attrs: noinline optnone
define hidden void @_ZN31btMultiBodyJointLimitConstraint20createConstraintRowsER20btAlignedObjectArrayI27btMultiBodySolverConstraintER23btMultiBodyJacobianDataRK19btContactSolverInfo(%class.btMultiBodyJointLimitConstraint* %this, %class.btAlignedObjectArray.20* nonnull align 4 dereferenceable(17) %constraintRows, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %data, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyJointLimitConstraint*, align 4
  %constraintRows.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %data.addr = alloca %struct.btMultiBodyJacobianData*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %row = alloca i32, align 4
  %direction = alloca float, align 4
  %constraintRow = alloca %struct.btMultiBodySolverConstraint*, align 4
  %posError = alloca float, align 4
  %dummy = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %rel_vel = alloca float, align 4
  %revoluteAxisInWorld = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca %class.btQuaternion, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %prismaticAxisInWorld = alloca %class.btVector3, align 4
  %ref.tmp29 = alloca %class.btVector3, align 4
  %ref.tmp30 = alloca %class.btQuaternion, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %penetration = alloca float, align 4
  %positionalError = alloca float, align 4
  %velocityError = alloca float, align 4
  %erp = alloca float, align 4
  %penetrationImpulse = alloca float, align 4
  %velocityImpulse = alloca float, align 4
  store %class.btMultiBodyJointLimitConstraint* %this, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  store %class.btAlignedObjectArray.20* %constraintRows, %class.btAlignedObjectArray.20** %constraintRows.addr, align 4
  store %struct.btMultiBodyJacobianData* %data, %struct.btMultiBodyJacobianData** %data.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %this1 = load %class.btMultiBodyJointLimitConstraint*, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_numDofsFinalized = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %0, i32 0, i32 10
  %1 = load i32, i32* %m_numDofsFinalized, align 4
  %2 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_jacSizeBoth = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %2, i32 0, i32 7
  %3 = load i32, i32* %m_jacSizeBoth, align 4
  %cmp = icmp ne i32 %1, %3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %4 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to void (%class.btMultiBodyJointLimitConstraint*)***
  %vtable = load void (%class.btMultiBodyJointLimitConstraint*)**, void (%class.btMultiBodyJointLimitConstraint*)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btMultiBodyJointLimitConstraint*)*, void (%class.btMultiBodyJointLimitConstraint*)** %vtable, i64 4
  %5 = load void (%class.btMultiBodyJointLimitConstraint*)*, void (%class.btMultiBodyJointLimitConstraint*)** %vfn, align 4
  call void %5(%class.btMultiBodyJointLimitConstraint* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %7 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %7, i32 0, i32 1
  %8 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4
  %9 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %9, i32 0, i32 3
  %10 = load i32, i32* %m_linkA, align 4
  %call = call float @_ZNK11btMultiBody11getJointPosEi(%class.btMultiBody* %8, i32 %10)
  %m_lowerBound = getelementptr inbounds %class.btMultiBodyJointLimitConstraint, %class.btMultiBodyJointLimitConstraint* %this1, i32 0, i32 1
  %11 = load float, float* %m_lowerBound, align 4
  %sub = fsub float %call, %11
  call void @_ZN21btMultiBodyConstraint11setPositionEif(%class.btMultiBodyConstraint* %6, i32 0, float %sub)
  %12 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_upperBound = getelementptr inbounds %class.btMultiBodyJointLimitConstraint, %class.btMultiBodyJointLimitConstraint* %this1, i32 0, i32 2
  %13 = load float, float* %m_upperBound, align 4
  %14 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA2 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %14, i32 0, i32 1
  %15 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA2, align 4
  %16 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkA3 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %16, i32 0, i32 3
  %17 = load i32, i32* %m_linkA3, align 4
  %call4 = call float @_ZNK11btMultiBody11getJointPosEi(%class.btMultiBody* %15, i32 %17)
  %sub5 = fsub float %13, %call4
  call void @_ZN21btMultiBodyConstraint11setPositionEif(%class.btMultiBodyConstraint* %12, i32 1, float %sub5)
  store i32 0, i32* %row, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %18 = load i32, i32* %row, align 4
  %19 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %call6 = call i32 @_ZNK21btMultiBodyConstraint10getNumRowsEv(%class.btMultiBodyConstraint* %19)
  %cmp7 = icmp slt i32 %18, %call6
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %20 = load i32, i32* %row, align 4
  %tobool = icmp ne i32 %20, 0
  %21 = zext i1 %tobool to i64
  %cond = select i1 %tobool, i32 -1, i32 1
  %conv = sitofp i32 %cond to float
  store float %conv, float* %direction, align 4
  %22 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %constraintRows.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.20* %22)
  store %struct.btMultiBodySolverConstraint* %call8, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %23 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %24 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_orgConstraint = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %24, i32 0, i32 28
  store %class.btMultiBodyConstraint* %23, %class.btMultiBodyConstraint** %m_orgConstraint, align 4
  %25 = load i32, i32* %row, align 4
  %26 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_orgDofIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %26, i32 0, i32 29
  store i32 %25, i32* %m_orgDofIndex, align 4
  %27 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA9 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %27, i32 0, i32 1
  %28 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA9, align 4
  %29 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_multiBodyA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %29, i32 0, i32 23
  store %class.btMultiBody* %28, %class.btMultiBody** %m_multiBodyA, align 4
  %30 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %30, i32 0, i32 2
  %31 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB, align 4
  %32 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_multiBodyB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %32, i32 0, i32 26
  store %class.btMultiBody* %31, %class.btMultiBody** %m_multiBodyB, align 4
  store float 0.000000e+00, float* %posError, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp10, align 4
  store float 0.000000e+00, float* %ref.tmp11, align 4
  %call12 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %dummy, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %33 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %34 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %35 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %36 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %37 = load i32, i32* %row, align 4
  %call13 = call float* @_ZN21btMultiBodyConstraint9jacobianAEi(%class.btMultiBodyConstraint* %36, i32 %37)
  %38 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %39 = load i32, i32* %row, align 4
  %call14 = call float* @_ZN21btMultiBodyConstraint9jacobianBEi(%class.btMultiBodyConstraint* %38, i32 %39)
  %40 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %41 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_maxAppliedImpulse = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %41, i32 0, i32 11
  %42 = load float, float* %m_maxAppliedImpulse, align 4
  %call15 = call float @_ZN21btMultiBodyConstraint23fillMultiBodyConstraintER27btMultiBodySolverConstraintR23btMultiBodyJacobianDataPfS4_RK9btVector3S7_S7_S7_fRK19btContactSolverInfoffbfbff(%class.btMultiBodyConstraint* %33, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %34, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %35, float* %call13, float* %call14, %class.btVector3* nonnull align 4 dereferenceable(16) %dummy, %class.btVector3* nonnull align 4 dereferenceable(16) %dummy, %class.btVector3* nonnull align 4 dereferenceable(16) %dummy, %class.btVector3* nonnull align 4 dereferenceable(16) %dummy, float 0.000000e+00, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %40, float 0.000000e+00, float %42, i1 zeroext false, float 1.000000e+00, i1 zeroext false, float 0.000000e+00, float 0.000000e+00)
  store float %call15, float* %rel_vel, align 4
  %43 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA16 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %43, i32 0, i32 1
  %44 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA16, align 4
  %45 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkA17 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %45, i32 0, i32 3
  %46 = load i32, i32* %m_linkA17, align 4
  %call18 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %44, i32 %46)
  %m_jointType = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call18, i32 0, i32 23
  %47 = load i32, i32* %m_jointType, align 4
  switch i32 %47, label %sw.default [
    i32 0, label %sw.bb
    i32 1, label %sw.bb28
  ]

sw.bb:                                            ; preds = %for.body
  %48 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %48, i32 0, i32 5
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_contactNormal1)
  %49 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %49, i32 0, i32 7
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_contactNormal2)
  %50 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA21 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %50, i32 0, i32 1
  %51 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA21, align 4
  %52 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkA22 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %52, i32 0, i32 3
  %53 = load i32, i32* %m_linkA22, align 4
  %call23 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %51, i32 %53)
  %m_cachedWorldTransform = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call23, i32 0, i32 25
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp20, %class.btTransform* %m_cachedWorldTransform)
  %54 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA24 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %54, i32 0, i32 1
  %55 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA24, align 4
  %56 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkA25 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %56, i32 0, i32 3
  %57 = load i32, i32* %m_linkA25, align 4
  %call26 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %55, i32 %57)
  %m_axes = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call26, i32 0, i32 8
  %arrayidx = getelementptr inbounds [6 x %struct.btSpatialMotionVector], [6 x %struct.btSpatialMotionVector]* %m_axes, i32 0, i32 0
  %m_topVec = getelementptr inbounds %struct.btSpatialMotionVector, %struct.btSpatialMotionVector* %arrayidx, i32 0, i32 0
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp19, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %m_topVec)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %revoluteAxisInWorld, float* nonnull align 4 dereferenceable(4) %direction, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp19)
  %58 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %58, i32 0, i32 4
  %59 = bitcast %class.btVector3* %m_relpos1CrossNormal to i8*
  %60 = bitcast %class.btVector3* %revoluteAxisInWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %59, i8* align 4 %60, i32 16, i1 false)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp27, %class.btVector3* nonnull align 4 dereferenceable(16) %revoluteAxisInWorld)
  %61 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %61, i32 0, i32 6
  %62 = bitcast %class.btVector3* %m_relpos2CrossNormal to i8*
  %63 = bitcast %class.btVector3* %ref.tmp27 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %62, i8* align 4 %63, i32 16, i1 false)
  br label %sw.epilog

sw.bb28:                                          ; preds = %for.body
  %64 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA31 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %64, i32 0, i32 1
  %65 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA31, align 4
  %66 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkA32 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %66, i32 0, i32 3
  %67 = load i32, i32* %m_linkA32, align 4
  %call33 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %65, i32 %67)
  %m_cachedWorldTransform34 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call33, i32 0, i32 25
  call void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* sret align 4 %ref.tmp30, %class.btTransform* %m_cachedWorldTransform34)
  %68 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA35 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %68, i32 0, i32 1
  %69 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA35, align 4
  %70 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkA36 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %70, i32 0, i32 3
  %71 = load i32, i32* %m_linkA36, align 4
  %call37 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %69, i32 %71)
  %m_axes38 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call37, i32 0, i32 8
  %arrayidx39 = getelementptr inbounds [6 x %struct.btSpatialMotionVector], [6 x %struct.btSpatialMotionVector]* %m_axes38, i32 0, i32 0
  %m_bottomVec = getelementptr inbounds %struct.btSpatialMotionVector, %struct.btSpatialMotionVector* %arrayidx39, i32 0, i32 1
  call void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* sret align 4 %ref.tmp29, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp30, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bottomVec)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %prismaticAxisInWorld, float* nonnull align 4 dereferenceable(4) %direction, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp29)
  %72 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_contactNormal140 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %72, i32 0, i32 5
  %73 = bitcast %class.btVector3* %m_contactNormal140 to i8*
  %74 = bitcast %class.btVector3* %prismaticAxisInWorld to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %73, i8* align 4 %74, i32 16, i1 false)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp41, %class.btVector3* nonnull align 4 dereferenceable(16) %prismaticAxisInWorld)
  %75 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_contactNormal242 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %75, i32 0, i32 7
  %76 = bitcast %class.btVector3* %m_contactNormal242 to i8*
  %77 = bitcast %class.btVector3* %ref.tmp41 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %76, i8* align 4 %77, i32 16, i1 false)
  %78 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_relpos1CrossNormal43 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %78, i32 0, i32 4
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_relpos1CrossNormal43)
  %79 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_relpos2CrossNormal44 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %79, i32 0, i32 6
  call void @_ZN9btVector37setZeroEv(%class.btVector3* %m_relpos2CrossNormal44)
  br label %sw.epilog

sw.default:                                       ; preds = %for.body
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb28, %sw.bb
  %80 = bitcast %class.btMultiBodyJointLimitConstraint* %this1 to %class.btMultiBodyConstraint*
  %81 = load i32, i32* %row, align 4
  %call45 = call float @_ZNK21btMultiBodyConstraint11getPositionEi(%class.btMultiBodyConstraint* %80, i32 %81)
  store float %call45, float* %penetration, align 4
  store float 0.000000e+00, float* %positionalError, align 4
  %82 = load float, float* %rel_vel, align 4
  %fneg = fneg float %82
  store float %fneg, float* %velocityError, align 4
  %83 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %84 = bitcast %struct.btContactSolverInfo* %83 to %struct.btContactSolverInfoData*
  %m_erp2 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %84, i32 0, i32 9
  %85 = load float, float* %m_erp2, align 4
  store float %85, float* %erp, align 4
  %86 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %87 = bitcast %struct.btContactSolverInfo* %86 to %struct.btContactSolverInfoData*
  %m_splitImpulse = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %87, i32 0, i32 11
  %88 = load i32, i32* %m_splitImpulse, align 4
  %tobool46 = icmp ne i32 %88, 0
  br i1 %tobool46, label %lor.lhs.false, label %if.then48

lor.lhs.false:                                    ; preds = %sw.epilog
  %89 = load float, float* %penetration, align 4
  %90 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %91 = bitcast %struct.btContactSolverInfo* %90 to %struct.btContactSolverInfoData*
  %m_splitImpulsePenetrationThreshold = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %91, i32 0, i32 12
  %92 = load float, float* %m_splitImpulsePenetrationThreshold, align 4
  %cmp47 = fcmp ogt float %89, %92
  br i1 %cmp47, label %if.then48, label %if.end49

if.then48:                                        ; preds = %lor.lhs.false, %sw.epilog
  %93 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %94 = bitcast %struct.btContactSolverInfo* %93 to %struct.btContactSolverInfoData*
  %m_erp = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %94, i32 0, i32 8
  %95 = load float, float* %m_erp, align 4
  store float %95, float* %erp, align 4
  br label %if.end49

if.end49:                                         ; preds = %if.then48, %lor.lhs.false
  %96 = load float, float* %penetration, align 4
  %cmp50 = fcmp ogt float %96, 0.000000e+00
  br i1 %cmp50, label %if.then51, label %if.else

if.then51:                                        ; preds = %if.end49
  store float 0.000000e+00, float* %positionalError, align 4
  %97 = load float, float* %penetration, align 4
  %fneg52 = fneg float %97
  %98 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %99 = bitcast %struct.btContactSolverInfo* %98 to %struct.btContactSolverInfoData*
  %m_timeStep = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %99, i32 0, i32 3
  %100 = load float, float* %m_timeStep, align 4
  %div = fdiv float %fneg52, %100
  store float %div, float* %velocityError, align 4
  br label %if.end56

if.else:                                          ; preds = %if.end49
  %101 = load float, float* %penetration, align 4
  %fneg53 = fneg float %101
  %102 = load float, float* %erp, align 4
  %mul = fmul float %fneg53, %102
  %103 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %104 = bitcast %struct.btContactSolverInfo* %103 to %struct.btContactSolverInfoData*
  %m_timeStep54 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %104, i32 0, i32 3
  %105 = load float, float* %m_timeStep54, align 4
  %div55 = fdiv float %mul, %105
  store float %div55, float* %positionalError, align 4
  br label %if.end56

if.end56:                                         ; preds = %if.else, %if.then51
  %106 = load float, float* %positionalError, align 4
  %107 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_jacDiagABInv = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %107, i32 0, i32 13
  %108 = load float, float* %m_jacDiagABInv, align 4
  %mul57 = fmul float %106, %108
  store float %mul57, float* %penetrationImpulse, align 4
  %109 = load float, float* %velocityError, align 4
  %110 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_jacDiagABInv58 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %110, i32 0, i32 13
  %111 = load float, float* %m_jacDiagABInv58, align 4
  %mul59 = fmul float %109, %111
  store float %mul59, float* %velocityImpulse, align 4
  %112 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %113 = bitcast %struct.btContactSolverInfo* %112 to %struct.btContactSolverInfoData*
  %m_splitImpulse60 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %113, i32 0, i32 11
  %114 = load i32, i32* %m_splitImpulse60, align 4
  %tobool61 = icmp ne i32 %114, 0
  br i1 %tobool61, label %lor.lhs.false62, label %if.then65

lor.lhs.false62:                                  ; preds = %if.end56
  %115 = load float, float* %penetration, align 4
  %116 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %117 = bitcast %struct.btContactSolverInfo* %116 to %struct.btContactSolverInfoData*
  %m_splitImpulsePenetrationThreshold63 = getelementptr inbounds %struct.btContactSolverInfoData, %struct.btContactSolverInfoData* %117, i32 0, i32 12
  %118 = load float, float* %m_splitImpulsePenetrationThreshold63, align 4
  %cmp64 = fcmp ogt float %115, %118
  br i1 %cmp64, label %if.then65, label %if.else66

if.then65:                                        ; preds = %lor.lhs.false62, %if.end56
  %119 = load float, float* %penetrationImpulse, align 4
  %120 = load float, float* %velocityImpulse, align 4
  %add = fadd float %119, %120
  %121 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_rhs = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %121, i32 0, i32 14
  store float %add, float* %m_rhs, align 4
  %122 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_rhsPenetration = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %122, i32 0, i32 18
  store float 0.000000e+00, float* %m_rhsPenetration, align 4
  br label %if.end69

if.else66:                                        ; preds = %lor.lhs.false62
  %123 = load float, float* %velocityImpulse, align 4
  %124 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_rhs67 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %124, i32 0, i32 14
  store float %123, float* %m_rhs67, align 4
  %125 = load float, float* %penetrationImpulse, align 4
  %126 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_rhsPenetration68 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %126, i32 0, i32 18
  store float %125, float* %m_rhsPenetration68, align 4
  br label %if.end69

if.end69:                                         ; preds = %if.else66, %if.then65
  br label %for.inc

for.inc:                                          ; preds = %if.end69
  %127 = load i32, i32* %row, align 4
  %inc = add nsw i32 %127, 1
  store i32 %inc, i32* %row, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btMultiBodyConstraint11setPositionEif(%class.btMultiBodyConstraint* %this, i32 %row, float %pos) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %row.addr = alloca i32, align 4
  %pos.addr = alloca float, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  store float %pos, float* %pos.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %0 = load float, float* %pos.addr, align 4
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 12
  %m_posOffset = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 8
  %1 = load i32, i32* %m_posOffset, align 4
  %2 = load i32, i32* %row.addr, align 4
  %add = add nsw i32 %1, %2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.8* %m_data, i32 %add)
  store float %0, float* %call, align 4
  ret void
}

declare float @_ZNK11btMultiBody11getJointPosEi(%class.btMultiBody*, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK21btMultiBodyConstraint10getNumRowsEv(%class.btMultiBodyConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %m_numRows = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_numRows, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.20* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.20* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.20* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.20* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 2
  %1 = load i32, i32* %m_size, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %2 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %3 = load i32, i32* %sz, align 4
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %2, i32 %3
  ret %struct.btMultiBodySolverConstraint* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

declare float @_ZN21btMultiBodyConstraint23fillMultiBodyConstraintER27btMultiBodySolverConstraintR23btMultiBodyJacobianDataPfS4_RK9btVector3S7_S7_S7_fRK19btContactSolverInfoffbfbff(%class.btMultiBodyConstraint*, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192), %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128), float*, float*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), float, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), float, float, i1 zeroext, float, i1 zeroext, float, float) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btVector37setZeroEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z10quatRotateRK12btQuaternionRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %rotation, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %rotation.addr = alloca %class.btQuaternion*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %q = alloca %class.btQuaternion, align 4
  %ref.tmp = alloca %class.btQuaternion, align 4
  store %class.btQuaternion* %rotation, %class.btQuaternion** %rotation.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  call void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* sret align 4 %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btQuaternion*, %class.btQuaternion** %rotation.addr, align 4
  call void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* sret align 4 %ref.tmp, %class.btQuaternion* %2)
  %call = call nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %q, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp)
  %3 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %3)
  %4 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %4)
  %5 = bitcast %class.btQuaternion* %q to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %5)
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call1, float* nonnull align 4 dereferenceable(4) %call2, float* nonnull align 4 dereferenceable(4) %call3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform11getRotationEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* %agg.result)
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %m_basis, %class.btQuaternion* nonnull align 4 dereferenceable(16) %agg.result)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK21btMultiBodyConstraint11getPositionEi(%class.btMultiBodyConstraint* %this, i32 %row) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %row.addr = alloca i32, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  store i32 %row, i32* %row.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 12
  %m_posOffset = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %this1, i32 0, i32 8
  %0 = load i32, i32* %m_posOffset, align 4
  %1 = load i32, i32* %row.addr, align 4
  %add = add nsw i32 %0, %1
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.8* %m_data, i32 %add)
  %2 = load float, float* %call, align 4
  ret float %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btMultiBodyConstraint11setFrameInBERK11btMatrix3x3(%class.btMultiBodyConstraint* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %frameInB) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %frameInB.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  store %class.btMatrix3x3* %frameInB, %class.btMatrix3x3** %frameInB.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btMultiBodyConstraint11setPivotInBERK9btVector3(%class.btMultiBodyConstraint* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInB) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyConstraint*, align 4
  %pivotInB.addr = alloca %class.btVector3*, align 4
  store %class.btMultiBodyConstraint* %this, %class.btMultiBodyConstraint** %this.addr, align 4
  store %class.btVector3* %pivotInB, %class.btVector3** %pivotInB.addr, align 4
  %this1 = load %class.btMultiBodyConstraint*, %class.btMultiBodyConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN31btMultiBodyJointLimitConstraint9debugDrawEP12btIDebugDraw(%class.btMultiBodyJointLimitConstraint* %this, %class.btIDebugDraw* %drawer) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyJointLimitConstraint*, align 4
  %drawer.addr = alloca %class.btIDebugDraw*, align 4
  store %class.btMultiBodyJointLimitConstraint* %this, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  store %class.btIDebugDraw* %drawer, %class.btIDebugDraw** %drawer.addr, align 4
  %this1 = load %class.btMultiBodyJointLimitConstraint*, %class.btMultiBodyJointLimitConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %0, i32 %1
  ret %struct.btMultibodyLink* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK12btQuaternionRK9btVector3(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q, %class.btVector3* nonnull align 4 dereferenceable(16) %w) #2 comdat {
entry:
  %q.addr = alloca %class.btQuaternion*, align 4
  %w.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  store %class.btVector3* %w, %class.btVector3** %w.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %1 = bitcast %class.btQuaternion* %0 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %1)
  %2 = load float, float* %call, align 4
  %3 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %3)
  %4 = load float, float* %call1, align 4
  %mul = fmul float %2, %4
  %5 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %6 = bitcast %class.btQuaternion* %5 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %6)
  %7 = load float, float* %call2, align 4
  %8 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %8)
  %9 = load float, float* %call3, align 4
  %mul4 = fmul float %7, %9
  %add = fadd float %mul, %mul4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %13)
  %14 = load float, float* %call6, align 4
  %mul7 = fmul float %12, %14
  %sub = fsub float %add, %mul7
  store float %sub, float* %ref.tmp, align 4
  %15 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %16 = bitcast %class.btQuaternion* %15 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %16)
  %17 = load float, float* %call9, align 4
  %18 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %18)
  %19 = load float, float* %call10, align 4
  %mul11 = fmul float %17, %19
  %20 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %21 = bitcast %class.btQuaternion* %20 to %class.btQuadWord*
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %21)
  %22 = load float, float* %call12, align 4
  %23 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %23)
  %24 = load float, float* %call13, align 4
  %mul14 = fmul float %22, %24
  %add15 = fadd float %mul11, %mul14
  %25 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %26 = bitcast %class.btQuaternion* %25 to %class.btQuadWord*
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %26)
  %27 = load float, float* %call16, align 4
  %28 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %28)
  %29 = load float, float* %call17, align 4
  %mul18 = fmul float %27, %29
  %sub19 = fsub float %add15, %mul18
  store float %sub19, float* %ref.tmp8, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %31)
  %32 = load float, float* %call21, align 4
  %33 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %33)
  %34 = load float, float* %call22, align 4
  %mul23 = fmul float %32, %34
  %35 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %36 = bitcast %class.btQuaternion* %35 to %class.btQuadWord*
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %36)
  %37 = load float, float* %call24, align 4
  %38 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %38)
  %39 = load float, float* %call25, align 4
  %mul26 = fmul float %37, %39
  %add27 = fadd float %mul23, %mul26
  %40 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %41 = bitcast %class.btQuaternion* %40 to %class.btQuadWord*
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %41)
  %42 = load float, float* %call28, align 4
  %43 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %43)
  %44 = load float, float* %call29, align 4
  %mul30 = fmul float %42, %44
  %sub31 = fsub float %add27, %mul30
  store float %sub31, float* %ref.tmp20, align 4
  %45 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %46 = bitcast %class.btQuaternion* %45 to %class.btQuadWord*
  %call33 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %46)
  %47 = load float, float* %call33, align 4
  %fneg = fneg float %47
  %48 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %48)
  %49 = load float, float* %call34, align 4
  %mul35 = fmul float %fneg, %49
  %50 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %51 = bitcast %class.btQuaternion* %50 to %class.btQuadWord*
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %51)
  %52 = load float, float* %call36, align 4
  %53 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %53)
  %54 = load float, float* %call37, align 4
  %mul38 = fmul float %52, %54
  %sub39 = fsub float %mul35, %mul38
  %55 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %56 = bitcast %class.btQuaternion* %55 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %56)
  %57 = load float, float* %call40, align 4
  %58 = load %class.btVector3*, %class.btVector3** %w.addr, align 4
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %58)
  %59 = load float, float* %call41, align 4
  %mul42 = fmul float %57, %59
  %sub43 = fsub float %sub39, %mul42
  store float %sub43, float* %ref.tmp32, align 4
  %call44 = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK12btQuaternion7inverseEv(%class.btQuaternion* noalias sret align 4 %agg.result, %class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats3 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %2, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %3 = load float, float* %arrayidx4, align 4
  %fneg5 = fneg float %3
  store float %fneg5, float* %ref.tmp2, align 4
  %4 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %4, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %5 = load float, float* %arrayidx8, align 4
  %fneg9 = fneg float %5
  store float %fneg9, float* %ref.tmp6, align 4
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats10 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 3
  %call = call %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %arrayidx11)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btQuaternion* @_ZN12btQuaternionmLERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp58 = alloca float, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  %2 = load float, float* %arrayidx, align 4
  %3 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %4 = bitcast %class.btQuaternion* %3 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %4)
  %5 = load float, float* %call, align 4
  %mul = fmul float %2, %5
  %6 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %6, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %7 = load float, float* %arrayidx3, align 4
  %8 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %9 = bitcast %class.btQuaternion* %8 to %class.btQuadWord*
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %9, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 3
  %10 = load float, float* %arrayidx5, align 4
  %mul6 = fmul float %7, %10
  %add = fadd float %mul, %mul6
  %11 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats7 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %11, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 1
  %12 = load float, float* %arrayidx8, align 4
  %13 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %14 = bitcast %class.btQuaternion* %13 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %14)
  %15 = load float, float* %call9, align 4
  %mul10 = fmul float %12, %15
  %add11 = fadd float %add, %mul10
  %16 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats12 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %16, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %17 = load float, float* %arrayidx13, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %19)
  %20 = load float, float* %call14, align 4
  %mul15 = fmul float %17, %20
  %sub = fsub float %add11, %mul15
  store float %sub, float* %ref.tmp, align 4
  %21 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats17 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %21, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 3
  %22 = load float, float* %arrayidx18, align 4
  %23 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %24 = bitcast %class.btQuaternion* %23 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %24)
  %25 = load float, float* %call19, align 4
  %mul20 = fmul float %22, %25
  %26 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats21 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %26, i32 0, i32 0
  %arrayidx22 = getelementptr inbounds [4 x float], [4 x float]* %m_floats21, i32 0, i32 1
  %27 = load float, float* %arrayidx22, align 4
  %28 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %29 = bitcast %class.btQuaternion* %28 to %class.btQuadWord*
  %m_floats23 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %29, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [4 x float], [4 x float]* %m_floats23, i32 0, i32 3
  %30 = load float, float* %arrayidx24, align 4
  %mul25 = fmul float %27, %30
  %add26 = fadd float %mul20, %mul25
  %31 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats27 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %31, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 2
  %32 = load float, float* %arrayidx28, align 4
  %33 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %34 = bitcast %class.btQuaternion* %33 to %class.btQuadWord*
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %34)
  %35 = load float, float* %call29, align 4
  %mul30 = fmul float %32, %35
  %add31 = fadd float %add26, %mul30
  %36 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats32 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %36, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [4 x float], [4 x float]* %m_floats32, i32 0, i32 0
  %37 = load float, float* %arrayidx33, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %39)
  %40 = load float, float* %call34, align 4
  %mul35 = fmul float %37, %40
  %sub36 = fsub float %add31, %mul35
  store float %sub36, float* %ref.tmp16, align 4
  %41 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats38 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %41, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [4 x float], [4 x float]* %m_floats38, i32 0, i32 3
  %42 = load float, float* %arrayidx39, align 4
  %43 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %44 = bitcast %class.btQuaternion* %43 to %class.btQuadWord*
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %44)
  %45 = load float, float* %call40, align 4
  %mul41 = fmul float %42, %45
  %46 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats42 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %46, i32 0, i32 0
  %arrayidx43 = getelementptr inbounds [4 x float], [4 x float]* %m_floats42, i32 0, i32 2
  %47 = load float, float* %arrayidx43, align 4
  %48 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %49 = bitcast %class.btQuaternion* %48 to %class.btQuadWord*
  %m_floats44 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %49, i32 0, i32 0
  %arrayidx45 = getelementptr inbounds [4 x float], [4 x float]* %m_floats44, i32 0, i32 3
  %50 = load float, float* %arrayidx45, align 4
  %mul46 = fmul float %47, %50
  %add47 = fadd float %mul41, %mul46
  %51 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats48 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %51, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [4 x float], [4 x float]* %m_floats48, i32 0, i32 0
  %52 = load float, float* %arrayidx49, align 4
  %53 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %54 = bitcast %class.btQuaternion* %53 to %class.btQuadWord*
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %54)
  %55 = load float, float* %call50, align 4
  %mul51 = fmul float %52, %55
  %add52 = fadd float %add47, %mul51
  %56 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats53 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %56, i32 0, i32 0
  %arrayidx54 = getelementptr inbounds [4 x float], [4 x float]* %m_floats53, i32 0, i32 1
  %57 = load float, float* %arrayidx54, align 4
  %58 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %59 = bitcast %class.btQuaternion* %58 to %class.btQuadWord*
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %59)
  %60 = load float, float* %call55, align 4
  %mul56 = fmul float %57, %60
  %sub57 = fsub float %add52, %mul56
  store float %sub57, float* %ref.tmp37, align 4
  %61 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats59 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %61, i32 0, i32 0
  %arrayidx60 = getelementptr inbounds [4 x float], [4 x float]* %m_floats59, i32 0, i32 3
  %62 = load float, float* %arrayidx60, align 4
  %63 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %64 = bitcast %class.btQuaternion* %63 to %class.btQuadWord*
  %m_floats61 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %64, i32 0, i32 0
  %arrayidx62 = getelementptr inbounds [4 x float], [4 x float]* %m_floats61, i32 0, i32 3
  %65 = load float, float* %arrayidx62, align 4
  %mul63 = fmul float %62, %65
  %66 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats64 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %66, i32 0, i32 0
  %arrayidx65 = getelementptr inbounds [4 x float], [4 x float]* %m_floats64, i32 0, i32 0
  %67 = load float, float* %arrayidx65, align 4
  %68 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %69 = bitcast %class.btQuaternion* %68 to %class.btQuadWord*
  %call66 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %69)
  %70 = load float, float* %call66, align 4
  %mul67 = fmul float %67, %70
  %sub68 = fsub float %mul63, %mul67
  %71 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats69 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %71, i32 0, i32 0
  %arrayidx70 = getelementptr inbounds [4 x float], [4 x float]* %m_floats69, i32 0, i32 1
  %72 = load float, float* %arrayidx70, align 4
  %73 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %74 = bitcast %class.btQuaternion* %73 to %class.btQuadWord*
  %call71 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %74)
  %75 = load float, float* %call71, align 4
  %mul72 = fmul float %72, %75
  %sub73 = fsub float %sub68, %mul72
  %76 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats74 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %76, i32 0, i32 0
  %arrayidx75 = getelementptr inbounds [4 x float], [4 x float]* %m_floats74, i32 0, i32 2
  %77 = load float, float* %arrayidx75, align 4
  %78 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %79 = bitcast %class.btQuaternion* %78 to %class.btQuadWord*
  %call76 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %79)
  %80 = load float, float* %call76, align 4
  %mul77 = fmul float %77, %80
  %sub78 = fsub float %sub73, %mul77
  store float %sub78, float* %ref.tmp58, align 4
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %ref.tmp58)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getXEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getYEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord4getZEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2ERKfS1_S1_S1_(%class.btQuaternion* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %1 = load float*, float** %_x.addr, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float*, float** %_z.addr, align 4
  %4 = load float*, float** %_w.addr, align 4
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2ERKfS1_S1_S1_(%class.btQuadWord* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z, float* nonnull align 4 dereferenceable(4) %_w) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  %_w.addr = alloca float*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  store float* %_w, float** %_w.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %6 = load float*, float** %_w.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float %7, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuaternion* @_ZN12btQuaternionC2Ev(%class.btQuaternion* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %call = call %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* %0)
  ret %class.btQuaternion* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x311getRotationER12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %trace = alloca float, align 4
  %temp = alloca [4 x float], align 16
  %s = alloca float, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %s64 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx3)
  %1 = load float, float* %call4, align 4
  %add = fadd float %0, %1
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx6)
  %2 = load float, float* %call7, align 4
  %add8 = fadd float %add, %2
  store float %add8, float* %trace, align 4
  %3 = load float, float* %trace, align 4
  %cmp = fcmp ogt float %3, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %4 = load float, float* %trace, align 4
  %add9 = fadd float %4, 1.000000e+00
  %call10 = call float @_Z6btSqrtf(float %add9)
  store float %call10, float* %s, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %5, 5.000000e-01
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul, float* %arrayidx11, align 4
  %6 = load float, float* %s, align 4
  %div = fdiv float 5.000000e-01, %6
  store float %div, float* %s, align 4
  %m_el12 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el12, i32 0, i32 2
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx13)
  %7 = load float, float* %call14, align 4
  %m_el15 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el15, i32 0, i32 1
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx16)
  %8 = load float, float* %call17, align 4
  %sub = fsub float %7, %8
  %9 = load float, float* %s, align 4
  %mul18 = fmul float %sub, %9
  %arrayidx19 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  store float %mul18, float* %arrayidx19, align 16
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 0
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %10 = load float, float* %call22, align 4
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx24)
  %11 = load float, float* %call25, align 4
  %sub26 = fsub float %10, %11
  %12 = load float, float* %s, align 4
  %mul27 = fmul float %sub26, %12
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  store float %mul27, float* %arrayidx28, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %13 = load float, float* %call31, align 4
  %m_el32 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx33 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el32, i32 0, i32 0
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx33)
  %14 = load float, float* %call34, align 4
  %sub35 = fsub float %13, %14
  %15 = load float, float* %s, align 4
  %mul36 = fmul float %sub35, %15
  %arrayidx37 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  store float %mul36, float* %arrayidx37, align 8
  br label %if.end

if.else:                                          ; preds = %entry
  %m_el38 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx39 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el38, i32 0, i32 0
  %call40 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx39)
  %16 = load float, float* %call40, align 4
  %m_el41 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx42 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el41, i32 0, i32 1
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx42)
  %17 = load float, float* %call43, align 4
  %cmp44 = fcmp olt float %16, %17
  br i1 %cmp44, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.else
  %m_el45 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx46 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el45, i32 0, i32 1
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx46)
  %18 = load float, float* %call47, align 4
  %m_el48 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx49 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el48, i32 0, i32 2
  %call50 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx49)
  %19 = load float, float* %call50, align 4
  %cmp51 = fcmp olt float %18, %19
  %20 = zext i1 %cmp51 to i64
  %cond = select i1 %cmp51, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %if.else
  %m_el52 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx53 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el52, i32 0, i32 0
  %call54 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx53)
  %21 = load float, float* %call54, align 4
  %m_el55 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx56 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el55, i32 0, i32 2
  %call57 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx56)
  %22 = load float, float* %call57, align 4
  %cmp58 = fcmp olt float %21, %22
  %23 = zext i1 %cmp58 to i64
  %cond59 = select i1 %cmp58, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond60 = phi i32 [ %cond, %cond.true ], [ %cond59, %cond.false ]
  store i32 %cond60, i32* %i, align 4
  %24 = load i32, i32* %i, align 4
  %add61 = add nsw i32 %24, 1
  %rem = srem i32 %add61, 3
  store i32 %rem, i32* %j, align 4
  %25 = load i32, i32* %i, align 4
  %add62 = add nsw i32 %25, 2
  %rem63 = srem i32 %add62, 3
  store i32 %rem63, i32* %k, align 4
  %m_el65 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %26 = load i32, i32* %i, align 4
  %arrayidx66 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el65, i32 0, i32 %26
  %call67 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx66)
  %27 = load i32, i32* %i, align 4
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %27
  %28 = load float, float* %arrayidx68, align 4
  %m_el69 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %29 = load i32, i32* %j, align 4
  %arrayidx70 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el69, i32 0, i32 %29
  %call71 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx70)
  %30 = load i32, i32* %j, align 4
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 %30
  %31 = load float, float* %arrayidx72, align 4
  %sub73 = fsub float %28, %31
  %m_el74 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %32 = load i32, i32* %k, align 4
  %arrayidx75 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el74, i32 0, i32 %32
  %call76 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx75)
  %33 = load i32, i32* %k, align 4
  %arrayidx77 = getelementptr inbounds float, float* %call76, i32 %33
  %34 = load float, float* %arrayidx77, align 4
  %sub78 = fsub float %sub73, %34
  %add79 = fadd float %sub78, 1.000000e+00
  %call80 = call float @_Z6btSqrtf(float %add79)
  store float %call80, float* %s64, align 4
  %35 = load float, float* %s64, align 4
  %mul81 = fmul float %35, 5.000000e-01
  %36 = load i32, i32* %i, align 4
  %arrayidx82 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %36
  store float %mul81, float* %arrayidx82, align 4
  %37 = load float, float* %s64, align 4
  %div83 = fdiv float 5.000000e-01, %37
  store float %div83, float* %s64, align 4
  %m_el84 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %38 = load i32, i32* %k, align 4
  %arrayidx85 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el84, i32 0, i32 %38
  %call86 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx85)
  %39 = load i32, i32* %j, align 4
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %39
  %40 = load float, float* %arrayidx87, align 4
  %m_el88 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %41 = load i32, i32* %j, align 4
  %arrayidx89 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el88, i32 0, i32 %41
  %call90 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx89)
  %42 = load i32, i32* %k, align 4
  %arrayidx91 = getelementptr inbounds float, float* %call90, i32 %42
  %43 = load float, float* %arrayidx91, align 4
  %sub92 = fsub float %40, %43
  %44 = load float, float* %s64, align 4
  %mul93 = fmul float %sub92, %44
  %arrayidx94 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  store float %mul93, float* %arrayidx94, align 4
  %m_el95 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %45 = load i32, i32* %j, align 4
  %arrayidx96 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el95, i32 0, i32 %45
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx96)
  %46 = load i32, i32* %i, align 4
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %46
  %47 = load float, float* %arrayidx98, align 4
  %m_el99 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %48 = load i32, i32* %i, align 4
  %arrayidx100 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el99, i32 0, i32 %48
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx100)
  %49 = load i32, i32* %j, align 4
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 %49
  %50 = load float, float* %arrayidx102, align 4
  %add103 = fadd float %47, %50
  %51 = load float, float* %s64, align 4
  %mul104 = fmul float %add103, %51
  %52 = load i32, i32* %j, align 4
  %arrayidx105 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %52
  store float %mul104, float* %arrayidx105, align 4
  %m_el106 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %53 = load i32, i32* %k, align 4
  %arrayidx107 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el106, i32 0, i32 %53
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx107)
  %54 = load i32, i32* %i, align 4
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 %54
  %55 = load float, float* %arrayidx109, align 4
  %m_el110 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %56 = load i32, i32* %i, align 4
  %arrayidx111 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el110, i32 0, i32 %56
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx111)
  %57 = load i32, i32* %k, align 4
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 %57
  %58 = load float, float* %arrayidx113, align 4
  %add114 = fadd float %55, %58
  %59 = load float, float* %s64, align 4
  %mul115 = fmul float %add114, %59
  %60 = load i32, i32* %k, align 4
  %arrayidx116 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 %60
  store float %mul115, float* %arrayidx116, align 4
  br label %if.end

if.end:                                           ; preds = %cond.end, %if.then
  %61 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %62 = bitcast %class.btQuaternion* %61 to %class.btQuadWord*
  %arrayidx117 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 0
  %arrayidx118 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 1
  %arrayidx119 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 2
  %arrayidx120 = getelementptr inbounds [4 x float], [4 x float]* %temp, i32 0, i32 3
  call void @_ZN10btQuadWord8setValueERKfS1_S1_S1_(%class.btQuadWord* %62, float* nonnull align 4 dereferenceable(4) %arrayidx117, float* nonnull align 4 dereferenceable(4) %arrayidx118, float* nonnull align 4 dereferenceable(4) %arrayidx119, float* nonnull align 4 dereferenceable(4) %arrayidx120)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuadWord* @_ZN10btQuadWordC2Ev(%class.btQuadWord* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  ret %class.btQuadWord* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.20* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.20* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.20* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.20* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi(%class.btAlignedObjectArray.20* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btMultiBodySolverConstraint*
  store %struct.btMultiBodySolverConstraint* %2, %struct.btMultiBodySolverConstraint** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.20* %this1, i32 0, i32 %call3, %struct.btMultiBodySolverConstraint* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.20* %this1)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.20* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.20* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* %4, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.20* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi(%class.btAlignedObjectArray.20* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btMultiBodySolverConstraint* @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.21* %m_allocator, i32 %1, %struct.btMultiBodySolverConstraint** null)
  %2 = bitcast %struct.btMultiBodySolverConstraint* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.20* %this, i32 %start, i32 %end, %struct.btMultiBodySolverConstraint* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btMultiBodySolverConstraint* %dest, %struct.btMultiBodySolverConstraint** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 %4
  %5 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx to i8*
  %call = call i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 192, i8* %5)
  %6 = bitcast i8* %call to %struct.btMultiBodySolverConstraint*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %7 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %7, i32 %8
  %9 = bitcast %struct.btMultiBodySolverConstraint* %6 to i8*
  %10 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 192, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.20* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.20* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.20*, align 4
  store %class.btAlignedObjectArray.20* %this, %class.btAlignedObjectArray.20** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.20*, %class.btAlignedObjectArray.20** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %tobool = icmp ne %struct.btMultiBodySolverConstraint* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  %2 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.21* %m_allocator, %struct.btMultiBodySolverConstraint* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* null, %struct.btMultiBodySolverConstraint** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btMultiBodySolverConstraint* @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.21* %this, i32 %n, %struct.btMultiBodySolverConstraint** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.21*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btMultiBodySolverConstraint**, align 4
  store %class.btAlignedAllocator.21* %this, %class.btAlignedAllocator.21** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btMultiBodySolverConstraint** %hint, %struct.btMultiBodySolverConstraint*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.21*, %class.btAlignedAllocator.21** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 192, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btMultiBodySolverConstraint*
  ret %struct.btMultiBodySolverConstraint* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.21* %this, %struct.btMultiBodySolverConstraint* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.21*, align 4
  %ptr.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %class.btAlignedAllocator.21* %this, %class.btAlignedAllocator.21** %this.addr, align 4
  store %struct.btMultiBodySolverConstraint* %ptr, %struct.btMultiBodySolverConstraint** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.21*, %class.btAlignedAllocator.21** %this.addr, align 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %ptr.addr, align 4
  %1 = bitcast %struct.btMultiBodySolverConstraint* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btMultiBodyJointLimitConstraint.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
