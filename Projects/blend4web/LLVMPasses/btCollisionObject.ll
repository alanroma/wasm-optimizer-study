; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btCollisionObject.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btCollisionObject.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type opaque
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btSerializer = type { i32 (...)** }
%struct.btCollisionObjectFloatData = type { i8*, i8*, %struct.btCollisionShapeData*, i8*, %struct.btTransformFloatData, %struct.btTransformFloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, %struct.btVector3FloatData, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, i32, i32, [4 x i8] }
%struct.btCollisionShapeData = type opaque
%struct.btTransformFloatData = type { %struct.btMatrix3x3FloatData, %struct.btVector3FloatData }
%struct.btMatrix3x3FloatData = type { [3 x %struct.btVector3FloatData] }
%struct.btVector3FloatData = type { [4 x float] }
%class.btChunk = type { i32, i32, i8*, i32, i32 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectEC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectED2Ev = comdat any

$_ZN17btCollisionObjectdlEPv = comdat any

$_ZNK11btTransform9serializeER20btTransformFloatData = comdat any

$_ZNK9btVector39serializeER18btVector3FloatData = comdat any

$_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape = comdat any

$_ZNK17btCollisionObject24checkCollideWithOverrideEPKS_ = comdat any

$_ZNK17btCollisionObject28calculateSerializeBufferSizeEv = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData = comdat any

$_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE16findLinearSearchERKS2_ = comdat any

$_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv = comdat any

$_ZN18btAlignedAllocatorIPK17btCollisionObjectLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectE4initEv = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIPK17btCollisionObjectE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPK17btCollisionObjectLj16EE10deallocateEPS2_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV17btCollisionObject = hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI17btCollisionObject to i8*), i8* bitcast (%class.btCollisionObject* (%class.btCollisionObject*)* @_ZN17btCollisionObjectD1Ev to i8*), i8* bitcast (void (%class.btCollisionObject*)* @_ZN17btCollisionObjectD0Ev to i8*), i8* bitcast (void (%class.btCollisionObject*, %class.btCollisionShape*)* @_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape to i8*), i8* bitcast (i1 (%class.btCollisionObject*, %class.btCollisionObject*)* @_ZNK17btCollisionObject24checkCollideWithOverrideEPKS_ to i8*), i8* bitcast (i32 (%class.btCollisionObject*)* @_ZNK17btCollisionObject28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)* @_ZNK17btCollisionObject9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionObject*, %class.btSerializer*)* @_ZNK17btCollisionObject21serializeSingleObjectEP12btSerializer to i8*)] }, align 4
@.str = private unnamed_addr constant [27 x i8] c"btCollisionObjectFloatData\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS17btCollisionObject = hidden constant [20 x i8] c"17btCollisionObject\00", align 1
@_ZTI17btCollisionObject = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btCollisionObject, i32 0, i32 0) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btCollisionObject.cpp, i8* null }]

@_ZN17btCollisionObjectC1Ev = hidden unnamed_addr alias %class.btCollisionObject* (%class.btCollisionObject*), %class.btCollisionObject* (%class.btCollisionObject*)* @_ZN17btCollisionObjectC2Ev
@_ZN17btCollisionObjectD1Ev = hidden unnamed_addr alias %class.btCollisionObject* (%class.btCollisionObject*), %class.btCollisionObject* (%class.btCollisionObject*)* @_ZN17btCollisionObjectD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btCollisionObject* @_ZN17btCollisionObjectC2Ev(%class.btCollisionObject* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = bitcast %class.btCollisionObject* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV17btCollisionObject, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_worldTransform)
  %m_interpolationWorldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 2
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_interpolationWorldTransform)
  %m_interpolationLinearVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_interpolationLinearVelocity)
  %m_interpolationAngularVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_interpolationAngularVelocity)
  %m_anisotropicFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 5
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 1.000000e+00, float* %ref.tmp6, align 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_anisotropicFriction, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %m_hasAnisotropicFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 6
  store i32 0, i32* %m_hasAnisotropicFriction, align 4
  %m_contactProcessingThreshold = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 7
  store float 0x43ABC16D60000000, float* %m_contactProcessingThreshold, align 4
  %m_broadphaseHandle = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 8
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_broadphaseHandle, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  store %class.btCollisionShape* null, %class.btCollisionShape** %m_collisionShape, align 4
  %m_extensionPointer = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 10
  store i8* null, i8** %m_extensionPointer, align 4
  %m_rootCollisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 11
  store %class.btCollisionShape* null, %class.btCollisionShape** %m_rootCollisionShape, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  store i32 1, i32* %m_collisionFlags, align 4
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  store i32 -1, i32* %m_islandTag1, align 4
  %m_companionId = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 14
  store i32 -1, i32* %m_companionId, align 4
  %m_worldArrayIndex = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 15
  store i32 -1, i32* %m_worldArrayIndex, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  store i32 1, i32* %m_activationState1, align 4
  %m_deactivationTime = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_deactivationTime, align 4
  %m_friction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 18
  store float 5.000000e-01, float* %m_friction, align 4
  %m_restitution = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %m_restitution, align 4
  %m_rollingFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %m_rollingFriction, align 4
  %m_spinningFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 21
  store float 0.000000e+00, float* %m_spinningFriction, align 4
  %m_contactDamping = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 22
  store float 0x3FB99999A0000000, float* %m_contactDamping, align 4
  %m_contactStiffness = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 23
  store float 1.000000e+04, float* %m_contactStiffness, align 4
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 24
  store i32 1, i32* %m_internalType, align 4
  %m_userObjectPointer = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 25
  store i8* null, i8** %m_userObjectPointer, align 4
  %m_userIndex2 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 26
  store i32 -1, i32* %m_userIndex2, align 4
  %m_userIndex = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 27
  store i32 -1, i32* %m_userIndex, align 4
  %m_hitFraction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 28
  store float 1.000000e+00, float* %m_hitFraction, align 4
  %m_ccdSweptSphereRadius = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 29
  store float 0.000000e+00, float* %m_ccdSweptSphereRadius, align 4
  %m_ccdMotionThreshold = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 30
  store float 0.000000e+00, float* %m_ccdMotionThreshold, align 4
  %m_checkCollideWith = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 31
  store i32 0, i32* %m_checkCollideWith, align 4
  %m_objectsWithoutCollisionCheck = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 32
  %call8 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPK17btCollisionObjectEC2Ev(%class.btAlignedObjectArray* %m_objectsWithoutCollisionCheck)
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 33
  store i32 0, i32* %m_updateRevision, align 4
  %m_customDebugColorRGB = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 34
  %call9 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_customDebugColorRGB)
  %m_worldTransform10 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %m_worldTransform10)
  ret %class.btCollisionObject* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPK17btCollisionObjectEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIPK17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btCollisionObject* @_ZN17btCollisionObjectD2Ev(%class.btCollisionObject* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = bitcast %class.btCollisionObject* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV17btCollisionObject, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_objectsWithoutCollisionCheck = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 32
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPK17btCollisionObjectED2Ev(%class.btAlignedObjectArray* %m_objectsWithoutCollisionCheck) #4
  ret %class.btCollisionObject* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIPK17btCollisionObjectED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN17btCollisionObjectD0Ev(%class.btCollisionObject* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %call = call %class.btCollisionObject* @_ZN17btCollisionObjectD1Ev(%class.btCollisionObject* %this1) #4
  %0 = bitcast %class.btCollisionObject* %this1 to i8*
  call void @_ZN17btCollisionObjectdlEPv(i8* %0) #4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btCollisionObjectdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %this, i32 %newState) #1 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %newState.addr = alloca i32, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store i32 %newState, i32* %newState.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  %0 = load i32, i32* %m_activationState1, align 4
  %cmp = icmp ne i32 %0, 4
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %m_activationState12 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  %1 = load i32, i32* %m_activationState12, align 4
  %cmp3 = icmp ne i32 %1, 5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %2 = load i32, i32* %newState.addr, align 4
  %m_activationState14 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  store i32 %2, i32* %m_activationState14, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK17btCollisionObject20forceActivationStateEi(%class.btCollisionObject* %this, i32 %newState) #1 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %newState.addr = alloca i32, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store i32 %newState, i32* %newState.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load i32, i32* %newState.addr, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  store i32 %0, i32* %m_activationState1, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK17btCollisionObject8activateEb(%class.btCollisionObject* %this, i1 zeroext %forceActivation) #1 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %forceActivation.addr = alloca i8, align 1
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %frombool = zext i1 %forceActivation to i8
  store i8 %frombool, i8* %forceActivation.addr, align 1
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load i8, i8* %forceActivation.addr, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %1 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %1, 3
  %tobool2 = icmp ne i32 %and, 0
  br i1 %tobool2, label %if.end, label %if.then

if.then:                                          ; preds = %lor.lhs.false, %entry
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %this1, i32 1)
  %m_deactivationTime = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_deactivationTime, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %lor.lhs.false
  ret void
}

; Function Attrs: noinline optnone
define hidden i8* @_ZNK17btCollisionObject9serializeEPvP12btSerializer(%class.btCollisionObject* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %dataOut = alloca %struct.btCollisionObjectFloatData*, align 4
  %name = alloca i8*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btCollisionObjectFloatData*
  store %struct.btCollisionObjectFloatData* %1, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  %2 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_worldTransform2 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %2, i32 0, i32 4
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_worldTransform, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_worldTransform2)
  %m_interpolationWorldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 2
  %3 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_interpolationWorldTransform3 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %3, i32 0, i32 5
  call void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %m_interpolationWorldTransform, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %m_interpolationWorldTransform3)
  %m_interpolationLinearVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 3
  %4 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_interpolationLinearVelocity4 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %4, i32 0, i32 6
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_interpolationLinearVelocity, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_interpolationLinearVelocity4)
  %m_interpolationAngularVelocity = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 4
  %5 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_interpolationAngularVelocity5 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %5, i32 0, i32 7
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_interpolationAngularVelocity, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_interpolationAngularVelocity5)
  %m_anisotropicFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 5
  %6 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_anisotropicFriction6 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %6, i32 0, i32 8
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_anisotropicFriction, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_anisotropicFriction6)
  %m_hasAnisotropicFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 6
  %7 = load i32, i32* %m_hasAnisotropicFriction, align 4
  %8 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_hasAnisotropicFriction7 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %8, i32 0, i32 19
  store i32 %7, i32* %m_hasAnisotropicFriction7, align 4
  %m_contactProcessingThreshold = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 7
  %9 = load float, float* %m_contactProcessingThreshold, align 4
  %10 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_contactProcessingThreshold8 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %10, i32 0, i32 9
  store float %9, float* %m_contactProcessingThreshold8, align 4
  %11 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_broadphaseHandle = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %11, i32 0, i32 0
  store i8* null, i8** %m_broadphaseHandle, align 4
  %12 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %13 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4
  %14 = bitcast %class.btCollisionShape* %13 to i8*
  %15 = bitcast %class.btSerializer* %12 to i8* (%class.btSerializer*, i8*)***
  %vtable = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %15, align 4
  %vfn = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable, i64 7
  %16 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn, align 4
  %call = call i8* %16(%class.btSerializer* %12, i8* %14)
  %17 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_collisionShape9 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %17, i32 0, i32 1
  store i8* %call, i8** %m_collisionShape9, align 4
  %18 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_rootCollisionShape = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %18, i32 0, i32 2
  store %struct.btCollisionShapeData* null, %struct.btCollisionShapeData** %m_rootCollisionShape, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %19 = load i32, i32* %m_collisionFlags, align 4
  %20 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_collisionFlags10 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %20, i32 0, i32 20
  store i32 %19, i32* %m_collisionFlags10, align 4
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  %21 = load i32, i32* %m_islandTag1, align 4
  %22 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_islandTag111 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %22, i32 0, i32 21
  store i32 %21, i32* %m_islandTag111, align 4
  %m_companionId = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 14
  %23 = load i32, i32* %m_companionId, align 4
  %24 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_companionId12 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %24, i32 0, i32 22
  store i32 %23, i32* %m_companionId12, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  %25 = load i32, i32* %m_activationState1, align 4
  %26 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_activationState113 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %26, i32 0, i32 23
  store i32 %25, i32* %m_activationState113, align 4
  %m_deactivationTime = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 17
  %27 = load float, float* %m_deactivationTime, align 4
  %28 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_deactivationTime14 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %28, i32 0, i32 10
  store float %27, float* %m_deactivationTime14, align 4
  %m_friction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 18
  %29 = load float, float* %m_friction, align 4
  %30 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_friction15 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %30, i32 0, i32 11
  store float %29, float* %m_friction15, align 4
  %m_rollingFriction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 20
  %31 = load float, float* %m_rollingFriction, align 4
  %32 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_rollingFriction16 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %32, i32 0, i32 12
  store float %31, float* %m_rollingFriction16, align 4
  %m_contactDamping = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 22
  %33 = load float, float* %m_contactDamping, align 4
  %34 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_contactDamping17 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %34, i32 0, i32 13
  store float %33, float* %m_contactDamping17, align 4
  %m_contactStiffness = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 23
  %35 = load float, float* %m_contactStiffness, align 4
  %36 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_contactStiffness18 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %36, i32 0, i32 14
  store float %35, float* %m_contactStiffness18, align 4
  %m_restitution = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 19
  %37 = load float, float* %m_restitution, align 4
  %38 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_restitution19 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %38, i32 0, i32 15
  store float %37, float* %m_restitution19, align 4
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 24
  %39 = load i32, i32* %m_internalType, align 4
  %40 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_internalType20 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %40, i32 0, i32 24
  store i32 %39, i32* %m_internalType20, align 4
  %41 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %42 = bitcast %class.btCollisionObject* %this1 to i8*
  %43 = bitcast %class.btSerializer* %41 to i8* (%class.btSerializer*, i8*)***
  %vtable21 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %43, align 4
  %vfn22 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable21, i64 10
  %44 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn22, align 4
  %call23 = call i8* %44(%class.btSerializer* %41, i8* %42)
  store i8* %call23, i8** %name, align 4
  %45 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %46 = load i8*, i8** %name, align 4
  %47 = bitcast %class.btSerializer* %45 to i8* (%class.btSerializer*, i8*)***
  %vtable24 = load i8* (%class.btSerializer*, i8*)**, i8* (%class.btSerializer*, i8*)*** %47, align 4
  %vfn25 = getelementptr inbounds i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vtable24, i64 7
  %48 = load i8* (%class.btSerializer*, i8*)*, i8* (%class.btSerializer*, i8*)** %vfn25, align 4
  %call26 = call i8* %48(%class.btSerializer* %45, i8* %46)
  %49 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_name = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %49, i32 0, i32 3
  store i8* %call26, i8** %m_name, align 4
  %50 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_name27 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %50, i32 0, i32 3
  %51 = load i8*, i8** %m_name27, align 4
  %tobool = icmp ne i8* %51, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %52 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %53 = load i8*, i8** %name, align 4
  %54 = bitcast %class.btSerializer* %52 to void (%class.btSerializer*, i8*)***
  %vtable28 = load void (%class.btSerializer*, i8*)**, void (%class.btSerializer*, i8*)*** %54, align 4
  %vfn29 = getelementptr inbounds void (%class.btSerializer*, i8*)*, void (%class.btSerializer*, i8*)** %vtable28, i64 12
  %55 = load void (%class.btSerializer*, i8*)*, void (%class.btSerializer*, i8*)** %vfn29, align 4
  call void %55(%class.btSerializer* %52, i8* %53)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_hitFraction = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 28
  %56 = load float, float* %m_hitFraction, align 4
  %57 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_hitFraction30 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %57, i32 0, i32 16
  store float %56, float* %m_hitFraction30, align 4
  %m_ccdSweptSphereRadius = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 29
  %58 = load float, float* %m_ccdSweptSphereRadius, align 4
  %59 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_ccdSweptSphereRadius31 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %59, i32 0, i32 17
  store float %58, float* %m_ccdSweptSphereRadius31, align 4
  %m_ccdMotionThreshold = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 30
  %60 = load float, float* %m_ccdMotionThreshold, align 4
  %61 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_ccdMotionThreshold32 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %61, i32 0, i32 18
  store float %60, float* %m_ccdMotionThreshold32, align 4
  %m_checkCollideWith = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 31
  %62 = load i32, i32* %m_checkCollideWith, align 4
  %63 = load %struct.btCollisionObjectFloatData*, %struct.btCollisionObjectFloatData** %dataOut, align 4
  %m_checkCollideWith33 = getelementptr inbounds %struct.btCollisionObjectFloatData, %struct.btCollisionObjectFloatData* %63, i32 0, i32 25
  store i32 %62, i32* %m_checkCollideWith33, align 4
  ret i8* getelementptr inbounds ([27 x i8], [27 x i8]* @.str, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransform9serializeER20btTransformFloatData(%class.btTransform* %this, %struct.btTransformFloatData* nonnull align 4 dereferenceable(64) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %dataOut.addr = alloca %struct.btTransformFloatData*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %struct.btTransformFloatData* %dataOut, %struct.btTransformFloatData** %dataOut.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_basis2 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %0, i32 0, i32 0
  call void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %m_basis, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %struct.btTransformFloatData*, %struct.btTransformFloatData** %dataOut.addr, align 4
  %m_origin3 = getelementptr inbounds %struct.btTransformFloatData, %struct.btTransformFloatData* %1, i32 0, i32 1
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %m_origin, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_origin3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK17btCollisionObject21serializeSingleObjectEP12btSerializer(%class.btCollisionObject* %this, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %len = alloca i32, align 4
  %chunk = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = bitcast %class.btCollisionObject* %this1 to i32 (%class.btCollisionObject*)***
  %vtable = load i32 (%class.btCollisionObject*)**, i32 (%class.btCollisionObject*)*** %0, align 4
  %vfn = getelementptr inbounds i32 (%class.btCollisionObject*)*, i32 (%class.btCollisionObject*)** %vtable, i64 4
  %1 = load i32 (%class.btCollisionObject*)*, i32 (%class.btCollisionObject*)** %vfn, align 4
  %call = call i32 %1(%class.btCollisionObject* %this1)
  store i32 %call, i32* %len, align 4
  %2 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %3 = load i32, i32* %len, align 4
  %4 = bitcast %class.btSerializer* %2 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable2 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %4, align 4
  %vfn3 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable2, i64 4
  %5 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn3, align 4
  %call4 = call %class.btChunk* %5(%class.btSerializer* %2, i32 %3, i32 1)
  store %class.btChunk* %call4, %class.btChunk** %chunk, align 4
  %6 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %6, i32 0, i32 2
  %7 = load i8*, i8** %m_oldPtr, align 4
  %8 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %9 = bitcast %class.btCollisionObject* %this1 to i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)***
  %vtable5 = load i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)**, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*** %9, align 4
  %vfn6 = getelementptr inbounds i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)** %vtable5, i64 5
  %10 = load i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)** %vfn6, align 4
  %call7 = call i8* %10(%class.btCollisionObject* %this1, i8* %7, %class.btSerializer* %8)
  store i8* %call7, i8** %structType, align 4
  %11 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %12 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %13 = load i8*, i8** %structType, align 4
  %14 = bitcast %class.btCollisionObject* %this1 to i8*
  %15 = bitcast %class.btSerializer* %11 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable8 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %15, align 4
  %vfn9 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable8, i64 5
  %16 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn9, align 4
  call void %16(%class.btSerializer* %11, %class.btChunk* %12, i8* %13, i32 1245859651, i8* %14)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape(%class.btCollisionObject* %this, %class.btCollisionShape* %collisionShape) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %collisionShape.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store %class.btCollisionShape* %collisionShape, %class.btCollisionShape** %collisionShape.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_updateRevision = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 33
  %0 = load i32, i32* %m_updateRevision, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* %m_updateRevision, align 4
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  store %class.btCollisionShape* %1, %class.btCollisionShape** %m_collisionShape, align 4
  %2 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4
  %m_rootCollisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 11
  store %class.btCollisionShape* %2, %class.btCollisionShape** %m_rootCollisionShape, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject24checkCollideWithOverrideEPKS_(%class.btCollisionObject* %this, %class.btCollisionObject* %co) unnamed_addr #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btCollisionObject*, align 4
  %co.addr = alloca %class.btCollisionObject*, align 4
  %index = alloca i32, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store %class.btCollisionObject* %co, %class.btCollisionObject** %co.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_objectsWithoutCollisionCheck = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 32
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE16findLinearSearchERKS2_(%class.btAlignedObjectArray* %m_objectsWithoutCollisionCheck, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %co.addr)
  store i32 %call, i32* %index, align 4
  %0 = load i32, i32* %index, align 4
  %m_objectsWithoutCollisionCheck2 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 32
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_objectsWithoutCollisionCheck2)
  %cmp = icmp slt i32 %0, %call3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %1 = load i1, i1* %retval, align 1
  ret i1 %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject28calculateSerializeBufferSizeEv(%class.btCollisionObject* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  ret i32 264
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39serializeER20btMatrix3x3FloatData(%class.btMatrix3x3* %this, %struct.btMatrix3x3FloatData* nonnull align 4 dereferenceable(48) %dataOut) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %dataOut.addr = alloca %struct.btMatrix3x3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %struct.btMatrix3x3FloatData* %dataOut, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %1
  %2 = load %struct.btMatrix3x3FloatData*, %struct.btMatrix3x3FloatData** %dataOut.addr, align 4
  %m_el2 = getelementptr inbounds %struct.btMatrix3x3FloatData, %struct.btMatrix3x3FloatData* %2, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [3 x %struct.btVector3FloatData], [3 x %struct.btVector3FloatData]* %m_el2, i32 0, i32 %3
  call void @_ZNK9btVector39serializeER18btVector3FloatData(%class.btVector3* %arrayidx, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %arrayidx3)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE16findLinearSearchERKS2_(%class.btAlignedObjectArray* %this, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %key) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %key.addr = alloca %class.btCollisionObject**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btCollisionObject** %key, %class.btCollisionObject*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %1, i32 %2
  %3 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx, align 4
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %key.addr, align 4
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %4, align 4
  %cmp3 = icmp eq %class.btCollisionObject* %3, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  store i32 %6, i32* %index, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %8 = load i32, i32* %index, align 4
  ret i32 %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIPK17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %tobool = icmp ne %class.btCollisionObject** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIPK17btCollisionObjectLj16EE10deallocateEPS2_(%class.btAlignedAllocator* %m_allocator, %class.btCollisionObject** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPK17btCollisionObjectLj16EE10deallocateEPS2_(%class.btAlignedAllocator* %this, %class.btCollisionObject** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.btCollisionObject** %ptr, %class.btCollisionObject*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %ptr.addr, align 4
  %1 = bitcast %class.btCollisionObject** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btCollisionObject.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
