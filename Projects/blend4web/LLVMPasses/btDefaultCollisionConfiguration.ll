; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btDefaultCollisionConfiguration.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btDefaultCollisionConfiguration.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btDefaultCollisionConfiguration = type { %class.btCollisionConfiguration, i32, %class.btPoolAllocator*, i8, %class.btPoolAllocator*, i8, %class.btConvexPenetrationDepthSolver*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc* }
%class.btCollisionConfiguration = type { i32 (...)** }
%class.btPoolAllocator = type { i32, i32, i32, i8*, i8*, %class.btSpinMutex }
%class.btSpinMutex = type { i32 }
%class.btConvexPenetrationDepthSolver = type { i32 (...)** }
%struct.btCollisionAlgorithmCreateFunc = type <{ i32 (...)**, i8, [3 x i8] }>
%struct.btDefaultCollisionConstructionInfo = type { %class.btPoolAllocator*, %class.btPoolAllocator*, i32, i32, i32, i32 }
%class.btGjkEpaPenetrationDepthSolver = type { %class.btConvexPenetrationDepthSolver }
%class.btMinkowskiPenetrationDepthSolver = type { %class.btConvexPenetrationDepthSolver }
%"struct.btConvexConvexAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, %class.btConvexPenetrationDepthSolver*, i32, i32 }
%struct.btCollisionAlgorithmCreateFunc.base = type <{ i32 (...)**, i8 }>
%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btCompoundCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btEmptyAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btSphereSphereCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btBoxBoxCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, [3 x i8] }
%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc" = type { %struct.btCollisionAlgorithmCreateFunc.base, i32, i32 }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.0, %union.anon.1, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.0 = type { float }
%union.anon.1 = type { float }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%class.btConvexConcaveCollisionAlgorithm = type <{ %class.btActivatingCollisionAlgorithm, %class.btConvexTriangleCallback, i8, [3 x i8] }>
%class.btActivatingCollisionAlgorithm = type { %class.btCollisionAlgorithm }
%class.btConvexTriangleCallback = type { %class.btTriangleCallback, %class.btVector3, %class.btVector3, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btManifoldResult*, %class.btDispatcher*, %struct.btDispatcherInfo*, float, i32, %class.btPersistentManifold* }
%class.btTriangleCallback = type { i32 (...)** }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32, float }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type opaque
%class.btCompoundCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.7, %class.btAlignedObjectArray.11, i8, %class.btPersistentManifold*, i8, i32 }
%class.btAlignedObjectArray.2 = type <{ %class.btAlignedAllocator.3, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.3 = type { i8 }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.5 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.5 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray.7 = type <{ %class.btAlignedAllocator.8, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.8 = type { i8 }
%class.btAlignedObjectArray.11 = type <{ %class.btAlignedAllocator.12, [3 x i8], i32, i32, %class.btCollisionAlgorithm**, i8, [3 x i8] }>
%class.btAlignedAllocator.12 = type { i8 }
%class.btCompoundCompoundCollisionAlgorithm = type { %class.btCompoundCollisionAlgorithm, %class.btHashedSimplePairCache*, %class.btAlignedObjectArray.15, i32, i32 }
%class.btHashedSimplePairCache = type { i32 (...)**, %class.btAlignedObjectArray.15, %class.btAlignedObjectArray.20, %class.btAlignedObjectArray.20 }
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }
%class.btAlignedObjectArray.15 = type <{ %class.btAlignedAllocator.16, [3 x i8], i32, i32, %struct.btSimplePair*, i8, [3 x i8] }>
%class.btAlignedAllocator.16 = type { i8 }
%struct.btSimplePair = type { i32, i32, %union.anon.18 }
%union.anon.18 = type { i8* }
%class.btEmptyAlgorithm = type { %class.btCollisionAlgorithm }
%class.btSphereSphereCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, i8, %class.btPersistentManifold* }
%class.btSphereTriangleCollisionAlgorithm = type <{ %class.btActivatingCollisionAlgorithm, i8, [3 x i8], %class.btPersistentManifold*, i8, [3 x i8] }>
%class.btBoxBoxCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, i8, %class.btPersistentManifold* }
%class.btConvexPlaneCollisionAlgorithm = type { %class.btCollisionAlgorithm, i8, %class.btPersistentManifold*, i8, i32, i32 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN24btCollisionConfigurationC2Ev = comdat any

$_ZN30btGjkEpaPenetrationDepthSolverC2Ev = comdat any

$_ZN33btMinkowskiPenetrationDepthSolverC2Ev = comdat any

$_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncC2Ev = comdat any

$_ZN28btCompoundCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncC2Ev = comdat any

$_ZN16btEmptyAlgorithm10CreateFuncC2Ev = comdat any

$_ZN32btSphereSphereCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN26btBoxBoxCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncC2Ev = comdat any

$_Z5btMaxIiERKT_S2_S2_ = comdat any

$_ZN15btPoolAllocatorC2Eii = comdat any

$_ZN15btPoolAllocatorD2Ev = comdat any

$_ZN17btBroadphaseProxy8isConvexEi = comdat any

$_ZN17btBroadphaseProxy9isConcaveEi = comdat any

$_ZN17btBroadphaseProxy10isCompoundEi = comdat any

$_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv = comdat any

$_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv = comdat any

$_ZN24btCollisionConfigurationD2Ev = comdat any

$_ZN24btCollisionConfigurationD0Ev = comdat any

$_ZN30btConvexPenetrationDepthSolverC2Ev = comdat any

$_ZN30btConvexPenetrationDepthSolverD2Ev = comdat any

$_ZN30btConvexPenetrationDepthSolverD0Ev = comdat any

$_ZN30btCollisionAlgorithmCreateFuncC2Ev = comdat any

$_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD2Ev = comdat any

$_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN30btCollisionAlgorithmCreateFuncD2Ev = comdat any

$_ZN30btCollisionAlgorithmCreateFuncD0Ev = comdat any

$_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_ = comdat any

$_ZN33btConvexConcaveCollisionAlgorithmnwEmPv = comdat any

$_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD2Ev = comdat any

$_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev = comdat any

$_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN28btCompoundCollisionAlgorithm10CreateFuncD2Ev = comdat any

$_ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncD2Ev = comdat any

$_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN36btCompoundCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD2Ev = comdat any

$_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev = comdat any

$_ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN16btEmptyAlgorithm10CreateFuncD2Ev = comdat any

$_ZN16btEmptyAlgorithm10CreateFuncD0Ev = comdat any

$_ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD2Ev = comdat any

$_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD2Ev = comdat any

$_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD2Ev = comdat any

$_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD2Ev = comdat any

$_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev = comdat any

$_ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ = comdat any

$_ZN11btSpinMutexC2Ev = comdat any

$_ZTS24btCollisionConfiguration = comdat any

$_ZTI24btCollisionConfiguration = comdat any

$_ZTV24btCollisionConfiguration = comdat any

$_ZTV30btConvexPenetrationDepthSolver = comdat any

$_ZTS30btConvexPenetrationDepthSolver = comdat any

$_ZTI30btConvexPenetrationDepthSolver = comdat any

$_ZTVN33btConvexConcaveCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN33btConvexConcaveCollisionAlgorithm10CreateFuncE = comdat any

$_ZTS30btCollisionAlgorithmCreateFunc = comdat any

$_ZTI30btCollisionAlgorithmCreateFunc = comdat any

$_ZTIN33btConvexConcaveCollisionAlgorithm10CreateFuncE = comdat any

$_ZTV30btCollisionAlgorithmCreateFunc = comdat any

$_ZTVN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTSN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTIN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTVN28btCompoundCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN28btCompoundCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN28btCompoundCollisionAlgorithm10CreateFuncE = comdat any

$_ZTVN36btCompoundCompoundCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN36btCompoundCompoundCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN36btCompoundCompoundCollisionAlgorithm10CreateFuncE = comdat any

$_ZTVN28btCompoundCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTSN28btCompoundCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTIN28btCompoundCollisionAlgorithm17SwappedCreateFuncE = comdat any

$_ZTVN16btEmptyAlgorithm10CreateFuncE = comdat any

$_ZTSN16btEmptyAlgorithm10CreateFuncE = comdat any

$_ZTIN16btEmptyAlgorithm10CreateFuncE = comdat any

$_ZTVN32btSphereSphereCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN32btSphereSphereCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN32btSphereSphereCollisionAlgorithm10CreateFuncE = comdat any

$_ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN34btSphereTriangleCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN34btSphereTriangleCollisionAlgorithm10CreateFuncE = comdat any

$_ZTVN26btBoxBoxCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN26btBoxBoxCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN26btBoxBoxCollisionAlgorithm10CreateFuncE = comdat any

$_ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE = comdat any

$_ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE = comdat any

$_ZTIN31btConvexPlaneCollisionAlgorithm10CreateFuncE = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV31btDefaultCollisionConfiguration = hidden unnamed_addr constant { [8 x i8*] } { [8 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI31btDefaultCollisionConfiguration to i8*), i8* bitcast (%class.btDefaultCollisionConfiguration* (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfigurationD1Ev to i8*), i8* bitcast (void (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfigurationD0Ev to i8*), i8* bitcast (%class.btPoolAllocator* (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv to i8*), i8* bitcast (%class.btPoolAllocator* (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%class.btDefaultCollisionConfiguration*, i32, i32)* @_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%class.btDefaultCollisionConfiguration*, i32, i32)* @_ZN31btDefaultCollisionConfiguration35getClosestPointsAlgorithmCreateFuncEii to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS31btDefaultCollisionConfiguration = hidden constant [34 x i8] c"31btDefaultCollisionConfiguration\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS24btCollisionConfiguration = linkonce_odr hidden constant [27 x i8] c"24btCollisionConfiguration\00", comdat, align 1
@_ZTI24btCollisionConfiguration = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @_ZTS24btCollisionConfiguration, i32 0, i32 0) }, comdat, align 4
@_ZTI31btDefaultCollisionConfiguration = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([34 x i8], [34 x i8]* @_ZTS31btDefaultCollisionConfiguration, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI24btCollisionConfiguration to i8*) }, align 4
@_ZTV24btCollisionConfiguration = linkonce_odr hidden unnamed_addr constant { [8 x i8*] } { [8 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI24btCollisionConfiguration to i8*), i8* bitcast (%class.btCollisionConfiguration* (%class.btCollisionConfiguration*)* @_ZN24btCollisionConfigurationD2Ev to i8*), i8* bitcast (void (%class.btCollisionConfiguration*)* @_ZN24btCollisionConfigurationD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV30btGjkEpaPenetrationDepthSolver = external unnamed_addr constant { [5 x i8*] }, align 4
@_ZTV30btConvexPenetrationDepthSolver = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI30btConvexPenetrationDepthSolver to i8*), i8* bitcast (%class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)* @_ZN30btConvexPenetrationDepthSolverD2Ev to i8*), i8* bitcast (void (%class.btConvexPenetrationDepthSolver*)* @_ZN30btConvexPenetrationDepthSolverD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTS30btConvexPenetrationDepthSolver = linkonce_odr hidden constant [33 x i8] c"30btConvexPenetrationDepthSolver\00", comdat, align 1
@_ZTI30btConvexPenetrationDepthSolver = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btConvexPenetrationDepthSolver, i32 0, i32 0) }, comdat, align 4
@_ZTV33btMinkowskiPenetrationDepthSolver = external unnamed_addr constant { [5 x i8*] }, align 4
@_ZTVN33btConvexConcaveCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN33btConvexConcaveCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* (%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*)* @_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*)* @_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN33btConvexConcaveCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [50 x i8] c"N33btConvexConcaveCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTS30btCollisionAlgorithmCreateFunc = linkonce_odr hidden constant [33 x i8] c"30btCollisionAlgorithmCreateFunc\00", comdat, align 1
@_ZTI30btCollisionAlgorithmCreateFunc = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btCollisionAlgorithmCreateFunc, i32 0, i32 0) }, comdat, align 4
@_ZTIN33btConvexConcaveCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([50 x i8], [50 x i8]* @_ZTSN33btConvexConcaveCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTV30btCollisionAlgorithmCreateFunc = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*), i8* bitcast (%struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD2Ev to i8*), i8* bitcast (void (%struct.btCollisionAlgorithmCreateFunc*)* @_ZN30btCollisionAlgorithmCreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_ to i8*)] }, comdat, align 4
@_ZTVN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE to i8*), i8* bitcast (%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* (%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*)* @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*)* @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden constant [57 x i8] c"N33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE\00", comdat, align 1
@_ZTIN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([57 x i8], [57 x i8]* @_ZTSN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN28btCompoundCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN28btCompoundCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%"struct.btCompoundCollisionAlgorithm::CreateFunc"* (%"struct.btCompoundCollisionAlgorithm::CreateFunc"*)* @_ZN28btCompoundCollisionAlgorithm10CreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btCompoundCollisionAlgorithm::CreateFunc"*)* @_ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btCompoundCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN28btCompoundCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [45 x i8] c"N28btCompoundCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN28btCompoundCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([45 x i8], [45 x i8]* @_ZTSN28btCompoundCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN36btCompoundCompoundCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN36btCompoundCompoundCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* (%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*)* @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*)* @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN36btCompoundCompoundCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [53 x i8] c"N36btCompoundCompoundCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN36btCompoundCompoundCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([53 x i8], [53 x i8]* @_ZTSN36btCompoundCompoundCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN28btCompoundCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN28btCompoundCollisionAlgorithm17SwappedCreateFuncE to i8*), i8* bitcast (%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* (%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*)* @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*)* @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN28btCompoundCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden constant [52 x i8] c"N28btCompoundCollisionAlgorithm17SwappedCreateFuncE\00", comdat, align 1
@_ZTIN28btCompoundCollisionAlgorithm17SwappedCreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([52 x i8], [52 x i8]* @_ZTSN28btCompoundCollisionAlgorithm17SwappedCreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN16btEmptyAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN16btEmptyAlgorithm10CreateFuncE to i8*), i8* bitcast (%"struct.btEmptyAlgorithm::CreateFunc"* (%"struct.btEmptyAlgorithm::CreateFunc"*)* @_ZN16btEmptyAlgorithm10CreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btEmptyAlgorithm::CreateFunc"*)* @_ZN16btEmptyAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btEmptyAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN16btEmptyAlgorithm10CreateFuncE = linkonce_odr hidden constant [33 x i8] c"N16btEmptyAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN16btEmptyAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTSN16btEmptyAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN32btSphereSphereCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN32btSphereSphereCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* (%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*)* @_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*)* @_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN32btSphereSphereCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [49 x i8] c"N32btSphereSphereCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN32btSphereSphereCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([49 x i8], [49 x i8]* @_ZTSN32btSphereSphereCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN34btSphereTriangleCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* (%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*)* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*)* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN34btSphereTriangleCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [51 x i8] c"N34btSphereTriangleCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN34btSphereTriangleCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([51 x i8], [51 x i8]* @_ZTSN34btSphereTriangleCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN26btBoxBoxCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN26btBoxBoxCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* (%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*)* @_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*)* @_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN26btBoxBoxCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [43 x i8] c"N26btBoxBoxCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN26btBoxBoxCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([43 x i8], [43 x i8]* @_ZTSN26btBoxBoxCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@_ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN31btConvexPlaneCollisionAlgorithm10CreateFuncE to i8*), i8* bitcast (%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* (%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*)* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD2Ev to i8*), i8* bitcast (void (%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*)* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_ to i8*)] }, comdat, align 4
@_ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant [48 x i8] c"N31btConvexPlaneCollisionAlgorithm10CreateFuncE\00", comdat, align 1
@_ZTIN31btConvexPlaneCollisionAlgorithm10CreateFuncE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([48 x i8], [48 x i8]* @_ZTSN31btConvexPlaneCollisionAlgorithm10CreateFuncE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI30btCollisionAlgorithmCreateFunc to i8*) }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btDefaultCollisionConfiguration.cpp, i8* null }]

@_ZN31btDefaultCollisionConfigurationC1ERK34btDefaultCollisionConstructionInfo = hidden unnamed_addr alias %class.btDefaultCollisionConfiguration* (%class.btDefaultCollisionConfiguration*, %struct.btDefaultCollisionConstructionInfo*), %class.btDefaultCollisionConfiguration* (%class.btDefaultCollisionConfiguration*, %struct.btDefaultCollisionConstructionInfo*)* @_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo
@_ZN31btDefaultCollisionConfigurationD1Ev = hidden unnamed_addr alias %class.btDefaultCollisionConfiguration* (%class.btDefaultCollisionConfiguration*), %class.btDefaultCollisionConfiguration* (%class.btDefaultCollisionConfiguration*)* @_ZN31btDefaultCollisionConfigurationD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo(%class.btDefaultCollisionConfiguration* returned %this, %struct.btDefaultCollisionConstructionInfo* nonnull align 4 dereferenceable(24) %constructionInfo) unnamed_addr #2 {
entry:
  %retval = alloca %class.btDefaultCollisionConfiguration*, align 4
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  %constructionInfo.addr = alloca %struct.btDefaultCollisionConstructionInfo*, align 4
  %mem = alloca i8*, align 4
  %maxSize = alloca i32, align 4
  %maxSize2 = alloca i32, align 4
  %maxSize3 = alloca i32, align 4
  %maxSize4 = alloca i32, align 4
  %collisionAlgorithmMaxElementSize = alloca i32, align 4
  %mem47 = alloca i8*, align 4
  %mem59 = alloca i8*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  store %struct.btDefaultCollisionConstructionInfo* %constructionInfo, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  store %class.btDefaultCollisionConfiguration* %this1, %class.btDefaultCollisionConfiguration** %retval, align 4
  %0 = bitcast %class.btDefaultCollisionConfiguration* %this1 to %class.btCollisionConfiguration*
  %call = call %class.btCollisionConfiguration* @_ZN24btCollisionConfigurationC2Ev(%class.btCollisionConfiguration* %0) #6
  %1 = bitcast %class.btDefaultCollisionConfiguration* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [8 x i8*] }, { [8 x i8*] }* @_ZTV31btDefaultCollisionConfiguration, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  store i8* null, i8** %mem, align 4
  %2 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4
  %m_useEpaPenetrationAlgorithm = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %2, i32 0, i32 5
  %3 = load i32, i32* %m_useEpaPenetrationAlgorithm, align 4
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %call2 = call i8* @_Z22btAlignedAllocInternalmi(i32 4, i32 16)
  store i8* %call2, i8** %mem, align 4
  %4 = load i8*, i8** %mem, align 4
  %5 = bitcast i8* %4 to %class.btGjkEpaPenetrationDepthSolver*
  %call3 = call %class.btGjkEpaPenetrationDepthSolver* @_ZN30btGjkEpaPenetrationDepthSolverC2Ev(%class.btGjkEpaPenetrationDepthSolver* %5)
  %6 = bitcast %class.btGjkEpaPenetrationDepthSolver* %5 to %class.btConvexPenetrationDepthSolver*
  %m_pdSolver = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 6
  store %class.btConvexPenetrationDepthSolver* %6, %class.btConvexPenetrationDepthSolver** %m_pdSolver, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %call4 = call i8* @_Z22btAlignedAllocInternalmi(i32 4, i32 16)
  store i8* %call4, i8** %mem, align 4
  %7 = load i8*, i8** %mem, align 4
  %8 = bitcast i8* %7 to %class.btMinkowskiPenetrationDepthSolver*
  %call5 = call %class.btMinkowskiPenetrationDepthSolver* @_ZN33btMinkowskiPenetrationDepthSolverC2Ev(%class.btMinkowskiPenetrationDepthSolver* %8) #6
  %9 = bitcast %class.btMinkowskiPenetrationDepthSolver* %8 to %class.btConvexPenetrationDepthSolver*
  %m_pdSolver6 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 6
  store %class.btConvexPenetrationDepthSolver* %9, %class.btConvexPenetrationDepthSolver** %m_pdSolver6, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %call7 = call i8* @_Z22btAlignedAllocInternalmi(i32 20, i32 16)
  store i8* %call7, i8** %mem, align 4
  %10 = load i8*, i8** %mem, align 4
  %11 = bitcast i8* %10 to %"struct.btConvexConvexAlgorithm::CreateFunc"*
  %m_pdSolver8 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 6
  %12 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_pdSolver8, align 4
  %call9 = call %"struct.btConvexConvexAlgorithm::CreateFunc"* @_ZN23btConvexConvexAlgorithm10CreateFuncC1EP30btConvexPenetrationDepthSolver(%"struct.btConvexConvexAlgorithm::CreateFunc"* %11, %class.btConvexPenetrationDepthSolver* %12)
  %13 = bitcast %"struct.btConvexConvexAlgorithm::CreateFunc"* %11 to %struct.btCollisionAlgorithmCreateFunc*
  %m_convexConvexCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 7
  store %struct.btCollisionAlgorithmCreateFunc* %13, %struct.btCollisionAlgorithmCreateFunc** %m_convexConvexCreateFunc, align 4
  %call10 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call10, i8** %mem, align 4
  %14 = load i8*, i8** %mem, align 4
  %15 = bitcast i8* %14 to %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*
  %call11 = call %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* @_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncC2Ev(%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %15)
  %16 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %15 to %struct.btCollisionAlgorithmCreateFunc*
  %m_convexConcaveCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 8
  store %struct.btCollisionAlgorithmCreateFunc* %16, %struct.btCollisionAlgorithmCreateFunc** %m_convexConcaveCreateFunc, align 4
  %call12 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call12, i8** %mem, align 4
  %17 = load i8*, i8** %mem, align 4
  %18 = bitcast i8* %17 to %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*
  %call13 = call %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncC2Ev(%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %18)
  %19 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %18 to %struct.btCollisionAlgorithmCreateFunc*
  %m_swappedConvexConcaveCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 9
  store %struct.btCollisionAlgorithmCreateFunc* %19, %struct.btCollisionAlgorithmCreateFunc** %m_swappedConvexConcaveCreateFunc, align 4
  %call14 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call14, i8** %mem, align 4
  %20 = load i8*, i8** %mem, align 4
  %21 = bitcast i8* %20 to %"struct.btCompoundCollisionAlgorithm::CreateFunc"*
  %call15 = call %"struct.btCompoundCollisionAlgorithm::CreateFunc"* @_ZN28btCompoundCollisionAlgorithm10CreateFuncC2Ev(%"struct.btCompoundCollisionAlgorithm::CreateFunc"* %21)
  %22 = bitcast %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %21 to %struct.btCollisionAlgorithmCreateFunc*
  %m_compoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 10
  store %struct.btCollisionAlgorithmCreateFunc* %22, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCreateFunc, align 4
  %call16 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call16, i8** %mem, align 4
  %23 = load i8*, i8** %mem, align 4
  %24 = bitcast i8* %23 to %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*
  %call17 = call %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncC2Ev(%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %24)
  %25 = bitcast %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %24 to %struct.btCollisionAlgorithmCreateFunc*
  %m_compoundCompoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 11
  store %struct.btCollisionAlgorithmCreateFunc* %25, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCompoundCreateFunc, align 4
  %call18 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call18, i8** %mem, align 4
  %26 = load i8*, i8** %mem, align 4
  %27 = bitcast i8* %26 to %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*
  %call19 = call %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncC2Ev(%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %27)
  %28 = bitcast %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %27 to %struct.btCollisionAlgorithmCreateFunc*
  %m_swappedCompoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 12
  store %struct.btCollisionAlgorithmCreateFunc* %28, %struct.btCollisionAlgorithmCreateFunc** %m_swappedCompoundCreateFunc, align 4
  %call20 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call20, i8** %mem, align 4
  %29 = load i8*, i8** %mem, align 4
  %30 = bitcast i8* %29 to %"struct.btEmptyAlgorithm::CreateFunc"*
  %call21 = call %"struct.btEmptyAlgorithm::CreateFunc"* @_ZN16btEmptyAlgorithm10CreateFuncC2Ev(%"struct.btEmptyAlgorithm::CreateFunc"* %30)
  %31 = bitcast %"struct.btEmptyAlgorithm::CreateFunc"* %30 to %struct.btCollisionAlgorithmCreateFunc*
  %m_emptyCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 13
  store %struct.btCollisionAlgorithmCreateFunc* %31, %struct.btCollisionAlgorithmCreateFunc** %m_emptyCreateFunc, align 4
  %call22 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call22, i8** %mem, align 4
  %32 = load i8*, i8** %mem, align 4
  %33 = bitcast i8* %32 to %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*
  %call23 = call %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* @_ZN32btSphereSphereCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %33)
  %34 = bitcast %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %33 to %struct.btCollisionAlgorithmCreateFunc*
  %m_sphereSphereCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 14
  store %struct.btCollisionAlgorithmCreateFunc* %34, %struct.btCollisionAlgorithmCreateFunc** %m_sphereSphereCF, align 4
  %call24 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call24, i8** %mem, align 4
  %35 = load i8*, i8** %mem, align 4
  %36 = bitcast i8* %35 to %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*
  %call25 = call %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %36)
  %37 = bitcast %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %36 to %struct.btCollisionAlgorithmCreateFunc*
  %m_sphereTriangleCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 18
  store %struct.btCollisionAlgorithmCreateFunc* %37, %struct.btCollisionAlgorithmCreateFunc** %m_sphereTriangleCF, align 4
  %call26 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call26, i8** %mem, align 4
  %38 = load i8*, i8** %mem, align 4
  %39 = bitcast i8* %38 to %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*
  %call27 = call %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %39)
  %40 = bitcast %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %39 to %struct.btCollisionAlgorithmCreateFunc*
  %m_triangleSphereCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 19
  store %struct.btCollisionAlgorithmCreateFunc* %40, %struct.btCollisionAlgorithmCreateFunc** %m_triangleSphereCF, align 4
  %m_triangleSphereCF28 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 19
  %41 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_triangleSphereCF28, align 4
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %41, i32 0, i32 1
  store i8 1, i8* %m_swapped, align 4
  %call29 = call i8* @_Z22btAlignedAllocInternalmi(i32 8, i32 16)
  store i8* %call29, i8** %mem, align 4
  %42 = load i8*, i8** %mem, align 4
  %43 = bitcast i8* %42 to %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*
  %call30 = call %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* @_ZN26btBoxBoxCollisionAlgorithm10CreateFuncC2Ev(%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %43)
  %44 = bitcast %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %43 to %struct.btCollisionAlgorithmCreateFunc*
  %m_boxBoxCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 17
  store %struct.btCollisionAlgorithmCreateFunc* %44, %struct.btCollisionAlgorithmCreateFunc** %m_boxBoxCF, align 4
  %call31 = call i8* @_Z22btAlignedAllocInternalmi(i32 16, i32 16)
  store i8* %call31, i8** %mem, align 4
  %45 = load i8*, i8** %mem, align 4
  %46 = bitcast i8* %45 to %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*
  %call32 = call %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncC2Ev(%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %46)
  %47 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %46 to %struct.btCollisionAlgorithmCreateFunc*
  %m_convexPlaneCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 21
  store %struct.btCollisionAlgorithmCreateFunc* %47, %struct.btCollisionAlgorithmCreateFunc** %m_convexPlaneCF, align 4
  %call33 = call i8* @_Z22btAlignedAllocInternalmi(i32 16, i32 16)
  store i8* %call33, i8** %mem, align 4
  %48 = load i8*, i8** %mem, align 4
  %49 = bitcast i8* %48 to %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*
  %call34 = call %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncC2Ev(%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %49)
  %50 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %49 to %struct.btCollisionAlgorithmCreateFunc*
  %m_planeConvexCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 20
  store %struct.btCollisionAlgorithmCreateFunc* %50, %struct.btCollisionAlgorithmCreateFunc** %m_planeConvexCF, align 4
  %m_planeConvexCF35 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 20
  %51 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_planeConvexCF35, align 4
  %m_swapped36 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %51, i32 0, i32 1
  store i8 1, i8* %m_swapped36, align 4
  store i32 72, i32* %maxSize, align 4
  store i32 80, i32* %maxSize2, align 4
  store i32 84, i32* %maxSize3, align 4
  store i32 116, i32* %maxSize4, align 4
  %52 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4
  %m_customCollisionAlgorithmMaxElementSize = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %52, i32 0, i32 4
  %call37 = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %maxSize, i32* nonnull align 4 dereferenceable(4) %m_customCollisionAlgorithmMaxElementSize)
  %53 = load i32, i32* %call37, align 4
  store i32 %53, i32* %collisionAlgorithmMaxElementSize, align 4
  %call38 = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %collisionAlgorithmMaxElementSize, i32* nonnull align 4 dereferenceable(4) %maxSize2)
  %54 = load i32, i32* %call38, align 4
  store i32 %54, i32* %collisionAlgorithmMaxElementSize, align 4
  %call39 = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %collisionAlgorithmMaxElementSize, i32* nonnull align 4 dereferenceable(4) %maxSize3)
  %55 = load i32, i32* %call39, align 4
  store i32 %55, i32* %collisionAlgorithmMaxElementSize, align 4
  %call40 = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %collisionAlgorithmMaxElementSize, i32* nonnull align 4 dereferenceable(4) %maxSize4)
  %56 = load i32, i32* %call40, align 4
  store i32 %56, i32* %collisionAlgorithmMaxElementSize, align 4
  %57 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4
  %m_persistentManifoldPool = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %57, i32 0, i32 0
  %58 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPool, align 4
  %tobool41 = icmp ne %class.btPoolAllocator* %58, null
  br i1 %tobool41, label %if.then42, label %if.else45

if.then42:                                        ; preds = %if.end
  %m_ownsPersistentManifoldPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 3
  store i8 0, i8* %m_ownsPersistentManifoldPool, align 4
  %59 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4
  %m_persistentManifoldPool43 = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %59, i32 0, i32 0
  %60 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPool43, align 4
  %m_persistentManifoldPool44 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 2
  store %class.btPoolAllocator* %60, %class.btPoolAllocator** %m_persistentManifoldPool44, align 4
  br label %if.end51

if.else45:                                        ; preds = %if.end
  %m_ownsPersistentManifoldPool46 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 3
  store i8 1, i8* %m_ownsPersistentManifoldPool46, align 4
  %call48 = call i8* @_Z22btAlignedAllocInternalmi(i32 24, i32 16)
  store i8* %call48, i8** %mem47, align 4
  %61 = load i8*, i8** %mem47, align 4
  %62 = bitcast i8* %61 to %class.btPoolAllocator*
  %63 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4
  %m_defaultMaxPersistentManifoldPoolSize = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %63, i32 0, i32 2
  %64 = load i32, i32* %m_defaultMaxPersistentManifoldPoolSize, align 4
  %call49 = call %class.btPoolAllocator* @_ZN15btPoolAllocatorC2Eii(%class.btPoolAllocator* %62, i32 804, i32 %64)
  %m_persistentManifoldPool50 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 2
  store %class.btPoolAllocator* %62, %class.btPoolAllocator** %m_persistentManifoldPool50, align 4
  br label %if.end51

if.end51:                                         ; preds = %if.else45, %if.then42
  %65 = load i32, i32* %collisionAlgorithmMaxElementSize, align 4
  %add = add nsw i32 %65, 16
  %conv = sext i32 %add to i64
  %and = and i64 %conv, 4503599627370480
  %conv52 = trunc i64 %and to i32
  store i32 %conv52, i32* %collisionAlgorithmMaxElementSize, align 4
  %66 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4
  %m_collisionAlgorithmPool = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %66, i32 0, i32 1
  %67 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool, align 4
  %tobool53 = icmp ne %class.btPoolAllocator* %67, null
  br i1 %tobool53, label %if.then54, label %if.else57

if.then54:                                        ; preds = %if.end51
  %m_ownsCollisionAlgorithmPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 5
  store i8 0, i8* %m_ownsCollisionAlgorithmPool, align 4
  %68 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4
  %m_collisionAlgorithmPool55 = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %68, i32 0, i32 1
  %69 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool55, align 4
  %m_collisionAlgorithmPool56 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 4
  store %class.btPoolAllocator* %69, %class.btPoolAllocator** %m_collisionAlgorithmPool56, align 4
  br label %if.end63

if.else57:                                        ; preds = %if.end51
  %m_ownsCollisionAlgorithmPool58 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsCollisionAlgorithmPool58, align 4
  %call60 = call i8* @_Z22btAlignedAllocInternalmi(i32 24, i32 16)
  store i8* %call60, i8** %mem59, align 4
  %70 = load i8*, i8** %mem59, align 4
  %71 = bitcast i8* %70 to %class.btPoolAllocator*
  %72 = load i32, i32* %collisionAlgorithmMaxElementSize, align 4
  %73 = load %struct.btDefaultCollisionConstructionInfo*, %struct.btDefaultCollisionConstructionInfo** %constructionInfo.addr, align 4
  %m_defaultMaxCollisionAlgorithmPoolSize = getelementptr inbounds %struct.btDefaultCollisionConstructionInfo, %struct.btDefaultCollisionConstructionInfo* %73, i32 0, i32 3
  %74 = load i32, i32* %m_defaultMaxCollisionAlgorithmPoolSize, align 4
  %call61 = call %class.btPoolAllocator* @_ZN15btPoolAllocatorC2Eii(%class.btPoolAllocator* %71, i32 %72, i32 %74)
  %m_collisionAlgorithmPool62 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 4
  store %class.btPoolAllocator* %71, %class.btPoolAllocator** %m_collisionAlgorithmPool62, align 4
  br label %if.end63

if.end63:                                         ; preds = %if.else57, %if.then54
  %75 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %retval, align 4
  ret %class.btDefaultCollisionConfiguration* %75
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionConfiguration* @_ZN24btCollisionConfigurationC2Ev(%class.btCollisionConfiguration* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionConfiguration*, align 4
  store %class.btCollisionConfiguration* %this, %class.btCollisionConfiguration** %this.addr, align 4
  %this1 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %this.addr, align 4
  %0 = bitcast %class.btCollisionConfiguration* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [8 x i8*] }, { [8 x i8*] }* @_ZTV24btCollisionConfiguration, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btCollisionConfiguration* %this1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGjkEpaPenetrationDepthSolver* @_ZN30btGjkEpaPenetrationDepthSolverC2Ev(%class.btGjkEpaPenetrationDepthSolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGjkEpaPenetrationDepthSolver*, align 4
  store %class.btGjkEpaPenetrationDepthSolver* %this, %class.btGjkEpaPenetrationDepthSolver** %this.addr, align 4
  %this1 = load %class.btGjkEpaPenetrationDepthSolver*, %class.btGjkEpaPenetrationDepthSolver** %this.addr, align 4
  %0 = bitcast %class.btGjkEpaPenetrationDepthSolver* %this1 to %class.btConvexPenetrationDepthSolver*
  %call = call %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverC2Ev(%class.btConvexPenetrationDepthSolver* %0) #6
  %1 = bitcast %class.btGjkEpaPenetrationDepthSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV30btGjkEpaPenetrationDepthSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %class.btGjkEpaPenetrationDepthSolver* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btMinkowskiPenetrationDepthSolver* @_ZN33btMinkowskiPenetrationDepthSolverC2Ev(%class.btMinkowskiPenetrationDepthSolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btMinkowskiPenetrationDepthSolver*, align 4
  store %class.btMinkowskiPenetrationDepthSolver* %this, %class.btMinkowskiPenetrationDepthSolver** %this.addr, align 4
  %this1 = load %class.btMinkowskiPenetrationDepthSolver*, %class.btMinkowskiPenetrationDepthSolver** %this.addr, align 4
  %0 = bitcast %class.btMinkowskiPenetrationDepthSolver* %this1 to %class.btConvexPenetrationDepthSolver*
  %call = call %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverC2Ev(%class.btConvexPenetrationDepthSolver* %0) #6
  %1 = bitcast %class.btMinkowskiPenetrationDepthSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV33btMinkowskiPenetrationDepthSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %class.btMinkowskiPenetrationDepthSolver* %this1
}

declare %"struct.btConvexConvexAlgorithm::CreateFunc"* @_ZN23btConvexConvexAlgorithm10CreateFuncC1EP30btConvexPenetrationDepthSolver(%"struct.btConvexConvexAlgorithm::CreateFunc"* returned, %class.btConvexPenetrationDepthSolver*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* @_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncC2Ev(%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this, %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN33btConvexConcaveCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncC2Ev(%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCompoundCollisionAlgorithm::CreateFunc"* @_ZN28btCompoundCollisionAlgorithm10CreateFuncC2Ev(%"struct.btCompoundCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this, %"struct.btCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btCompoundCollisionAlgorithm::CreateFunc"*, %"struct.btCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN28btCompoundCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncC2Ev(%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this, %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN36btCompoundCompoundCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncC2Ev(%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN28btCompoundCollisionAlgorithm17SwappedCreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btEmptyAlgorithm::CreateFunc"* @_ZN16btEmptyAlgorithm10CreateFuncC2Ev(%"struct.btEmptyAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btEmptyAlgorithm::CreateFunc"*, align 4
  store %"struct.btEmptyAlgorithm::CreateFunc"* %this, %"struct.btEmptyAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btEmptyAlgorithm::CreateFunc"*, %"struct.btEmptyAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btEmptyAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btEmptyAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN16btEmptyAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"struct.btEmptyAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* @_ZN32btSphereSphereCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this, %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN32btSphereSphereCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncC2Ev(%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this, %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN34btSphereTriangleCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* @_ZN26btBoxBoxCollisionAlgorithm10CreateFuncC2Ev(%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this, %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN26btBoxBoxCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncC2Ev(%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* %0)
  %1 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTVN31btConvexPlaneCollisionAlgorithm10CreateFuncE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_numPerturbationIterations = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1, i32 0, i32 1
  store i32 1, i32* %m_numPerturbationIterations, align 4
  %m_minimumPointsPerturbationThreshold = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1, i32 0, i32 2
  store i32 0, i32* %m_minimumPointsPerturbationThreshold, align 4
  ret %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %a, i32* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca i32*, align 4
  %b.addr = alloca i32*, align 4
  store i32* %a, i32** %a.addr, align 4
  store i32* %b, i32** %b.addr, align 4
  %0 = load i32*, i32** %a.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %b.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp sgt i32 %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32*, i32** %a.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load i32*, i32** %b.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %4, %cond.true ], [ %5, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPoolAllocator* @_ZN15btPoolAllocatorC2Eii(%class.btPoolAllocator* returned %this, i32 %elemSize, i32 %maxElements) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btPoolAllocator*, align 4
  %this.addr = alloca %class.btPoolAllocator*, align 4
  %elemSize.addr = alloca i32, align 4
  %maxElements.addr = alloca i32, align 4
  %p = alloca i8*, align 4
  %count = alloca i32, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4
  store i32 %elemSize, i32* %elemSize.addr, align 4
  store i32 %maxElements, i32* %maxElements.addr, align 4
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  store %class.btPoolAllocator* %this1, %class.btPoolAllocator** %retval, align 4
  %m_elemSize = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %0 = load i32, i32* %elemSize.addr, align 4
  store i32 %0, i32* %m_elemSize, align 4
  %m_maxElements = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %1 = load i32, i32* %maxElements.addr, align 4
  store i32 %1, i32* %m_maxElements, align 4
  %m_mutex = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 5
  %call = call %class.btSpinMutex* @_ZN11btSpinMutexC2Ev(%class.btSpinMutex* %m_mutex)
  %m_elemSize2 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %2 = load i32, i32* %m_elemSize2, align 4
  %m_maxElements3 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_maxElements3, align 4
  %mul = mul nsw i32 %2, %3
  %call4 = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %m_pool = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  store i8* %call4, i8** %m_pool, align 4
  %m_pool5 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  %4 = load i8*, i8** %m_pool5, align 4
  store i8* %4, i8** %p, align 4
  %5 = load i8*, i8** %p, align 4
  %m_firstFree = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  store i8* %5, i8** %m_firstFree, align 4
  %m_maxElements6 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %6 = load i32, i32* %m_maxElements6, align 4
  %m_freeCount = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 2
  store i32 %6, i32* %m_freeCount, align 4
  %m_maxElements7 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %7 = load i32, i32* %m_maxElements7, align 4
  store i32 %7, i32* %count, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %8 = load i32, i32* %count, align 4
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %count, align 4
  %tobool = icmp ne i32 %dec, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = load i8*, i8** %p, align 4
  %m_elemSize8 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %10 = load i32, i32* %m_elemSize8, align 4
  %add.ptr = getelementptr inbounds i8, i8* %9, i32 %10
  %11 = load i8*, i8** %p, align 4
  %12 = bitcast i8* %11 to i8**
  store i8* %add.ptr, i8** %12, align 4
  %m_elemSize9 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %13 = load i32, i32* %m_elemSize9, align 4
  %14 = load i8*, i8** %p, align 4
  %add.ptr10 = getelementptr inbounds i8, i8* %14, i32 %13
  store i8* %add.ptr10, i8** %p, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %15 = load i8*, i8** %p, align 4
  %16 = bitcast i8* %15 to i8**
  store i8* null, i8** %16, align 4
  %17 = load %class.btPoolAllocator*, %class.btPoolAllocator** %retval, align 4
  ret %class.btPoolAllocator* %17
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationD2Ev(%class.btDefaultCollisionConfiguration* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btDefaultCollisionConfiguration*, align 4
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  store %class.btDefaultCollisionConfiguration* %this1, %class.btDefaultCollisionConfiguration** %retval, align 4
  %0 = bitcast %class.btDefaultCollisionConfiguration* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [8 x i8*] }, { [8 x i8*] }* @_ZTV31btDefaultCollisionConfiguration, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_ownsCollisionAlgorithmPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsCollisionAlgorithmPool, align 4
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_collisionAlgorithmPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 4
  %2 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool, align 4
  %call = call %class.btPoolAllocator* @_ZN15btPoolAllocatorD2Ev(%class.btPoolAllocator* %2) #6
  %m_collisionAlgorithmPool2 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 4
  %3 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool2, align 4
  %4 = bitcast %class.btPoolAllocator* %3 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_ownsPersistentManifoldPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 3
  %5 = load i8, i8* %m_ownsPersistentManifoldPool, align 4
  %tobool3 = trunc i8 %5 to i1
  br i1 %tobool3, label %if.then4, label %if.end7

if.then4:                                         ; preds = %if.end
  %m_persistentManifoldPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 2
  %6 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPool, align 4
  %call5 = call %class.btPoolAllocator* @_ZN15btPoolAllocatorD2Ev(%class.btPoolAllocator* %6) #6
  %m_persistentManifoldPool6 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 2
  %7 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPool6, align 4
  %8 = bitcast %class.btPoolAllocator* %7 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %8)
  br label %if.end7

if.end7:                                          ; preds = %if.then4, %if.end
  %m_convexConvexCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 7
  %9 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConvexCreateFunc, align 4
  %10 = bitcast %struct.btCollisionAlgorithmCreateFunc* %9 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %10, align 4
  %vfn = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable, i64 0
  %11 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn, align 4
  %call8 = call %struct.btCollisionAlgorithmCreateFunc* %11(%struct.btCollisionAlgorithmCreateFunc* %9) #6
  %m_convexConvexCreateFunc9 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 7
  %12 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConvexCreateFunc9, align 4
  %13 = bitcast %struct.btCollisionAlgorithmCreateFunc* %12 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %13)
  %m_convexConcaveCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 8
  %14 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConcaveCreateFunc, align 4
  %15 = bitcast %struct.btCollisionAlgorithmCreateFunc* %14 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable10 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %15, align 4
  %vfn11 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable10, i64 0
  %16 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn11, align 4
  %call12 = call %struct.btCollisionAlgorithmCreateFunc* %16(%struct.btCollisionAlgorithmCreateFunc* %14) #6
  %m_convexConcaveCreateFunc13 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 8
  %17 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConcaveCreateFunc13, align 4
  %18 = bitcast %struct.btCollisionAlgorithmCreateFunc* %17 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %18)
  %m_swappedConvexConcaveCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 9
  %19 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedConvexConcaveCreateFunc, align 4
  %20 = bitcast %struct.btCollisionAlgorithmCreateFunc* %19 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable14 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %20, align 4
  %vfn15 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable14, i64 0
  %21 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn15, align 4
  %call16 = call %struct.btCollisionAlgorithmCreateFunc* %21(%struct.btCollisionAlgorithmCreateFunc* %19) #6
  %m_swappedConvexConcaveCreateFunc17 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 9
  %22 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedConvexConcaveCreateFunc17, align 4
  %23 = bitcast %struct.btCollisionAlgorithmCreateFunc* %22 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %23)
  %m_compoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 10
  %24 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCreateFunc, align 4
  %25 = bitcast %struct.btCollisionAlgorithmCreateFunc* %24 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable18 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %25, align 4
  %vfn19 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable18, i64 0
  %26 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn19, align 4
  %call20 = call %struct.btCollisionAlgorithmCreateFunc* %26(%struct.btCollisionAlgorithmCreateFunc* %24) #6
  %m_compoundCreateFunc21 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 10
  %27 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCreateFunc21, align 4
  %28 = bitcast %struct.btCollisionAlgorithmCreateFunc* %27 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %28)
  %m_compoundCompoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 11
  %29 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCompoundCreateFunc, align 4
  %30 = bitcast %struct.btCollisionAlgorithmCreateFunc* %29 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable22 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %30, align 4
  %vfn23 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable22, i64 0
  %31 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn23, align 4
  %call24 = call %struct.btCollisionAlgorithmCreateFunc* %31(%struct.btCollisionAlgorithmCreateFunc* %29) #6
  %m_compoundCompoundCreateFunc25 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 11
  %32 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCompoundCreateFunc25, align 4
  %33 = bitcast %struct.btCollisionAlgorithmCreateFunc* %32 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %33)
  %m_swappedCompoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 12
  %34 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedCompoundCreateFunc, align 4
  %35 = bitcast %struct.btCollisionAlgorithmCreateFunc* %34 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable26 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %35, align 4
  %vfn27 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable26, i64 0
  %36 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn27, align 4
  %call28 = call %struct.btCollisionAlgorithmCreateFunc* %36(%struct.btCollisionAlgorithmCreateFunc* %34) #6
  %m_swappedCompoundCreateFunc29 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 12
  %37 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedCompoundCreateFunc29, align 4
  %38 = bitcast %struct.btCollisionAlgorithmCreateFunc* %37 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %38)
  %m_emptyCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 13
  %39 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_emptyCreateFunc, align 4
  %40 = bitcast %struct.btCollisionAlgorithmCreateFunc* %39 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable30 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %40, align 4
  %vfn31 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable30, i64 0
  %41 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn31, align 4
  %call32 = call %struct.btCollisionAlgorithmCreateFunc* %41(%struct.btCollisionAlgorithmCreateFunc* %39) #6
  %m_emptyCreateFunc33 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 13
  %42 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_emptyCreateFunc33, align 4
  %43 = bitcast %struct.btCollisionAlgorithmCreateFunc* %42 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %43)
  %m_sphereSphereCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 14
  %44 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_sphereSphereCF, align 4
  %45 = bitcast %struct.btCollisionAlgorithmCreateFunc* %44 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable34 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %45, align 4
  %vfn35 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable34, i64 0
  %46 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn35, align 4
  %call36 = call %struct.btCollisionAlgorithmCreateFunc* %46(%struct.btCollisionAlgorithmCreateFunc* %44) #6
  %m_sphereSphereCF37 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 14
  %47 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_sphereSphereCF37, align 4
  %48 = bitcast %struct.btCollisionAlgorithmCreateFunc* %47 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %48)
  %m_sphereTriangleCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 18
  %49 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_sphereTriangleCF, align 4
  %50 = bitcast %struct.btCollisionAlgorithmCreateFunc* %49 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable38 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %50, align 4
  %vfn39 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable38, i64 0
  %51 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn39, align 4
  %call40 = call %struct.btCollisionAlgorithmCreateFunc* %51(%struct.btCollisionAlgorithmCreateFunc* %49) #6
  %m_sphereTriangleCF41 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 18
  %52 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_sphereTriangleCF41, align 4
  %53 = bitcast %struct.btCollisionAlgorithmCreateFunc* %52 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %53)
  %m_triangleSphereCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 19
  %54 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_triangleSphereCF, align 4
  %55 = bitcast %struct.btCollisionAlgorithmCreateFunc* %54 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable42 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %55, align 4
  %vfn43 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable42, i64 0
  %56 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn43, align 4
  %call44 = call %struct.btCollisionAlgorithmCreateFunc* %56(%struct.btCollisionAlgorithmCreateFunc* %54) #6
  %m_triangleSphereCF45 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 19
  %57 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_triangleSphereCF45, align 4
  %58 = bitcast %struct.btCollisionAlgorithmCreateFunc* %57 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %58)
  %m_boxBoxCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 17
  %59 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_boxBoxCF, align 4
  %60 = bitcast %struct.btCollisionAlgorithmCreateFunc* %59 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable46 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %60, align 4
  %vfn47 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable46, i64 0
  %61 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn47, align 4
  %call48 = call %struct.btCollisionAlgorithmCreateFunc* %61(%struct.btCollisionAlgorithmCreateFunc* %59) #6
  %m_boxBoxCF49 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 17
  %62 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_boxBoxCF49, align 4
  %63 = bitcast %struct.btCollisionAlgorithmCreateFunc* %62 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %63)
  %m_convexPlaneCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 21
  %64 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexPlaneCF, align 4
  %65 = bitcast %struct.btCollisionAlgorithmCreateFunc* %64 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable50 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %65, align 4
  %vfn51 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable50, i64 0
  %66 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn51, align 4
  %call52 = call %struct.btCollisionAlgorithmCreateFunc* %66(%struct.btCollisionAlgorithmCreateFunc* %64) #6
  %m_convexPlaneCF53 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 21
  %67 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexPlaneCF53, align 4
  %68 = bitcast %struct.btCollisionAlgorithmCreateFunc* %67 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %68)
  %m_planeConvexCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 20
  %69 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_planeConvexCF, align 4
  %70 = bitcast %struct.btCollisionAlgorithmCreateFunc* %69 to %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)***
  %vtable54 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)**, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*** %70, align 4
  %vfn55 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vtable54, i64 0
  %71 = load %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)*, %struct.btCollisionAlgorithmCreateFunc* (%struct.btCollisionAlgorithmCreateFunc*)** %vfn55, align 4
  %call56 = call %struct.btCollisionAlgorithmCreateFunc* %71(%struct.btCollisionAlgorithmCreateFunc* %69) #6
  %m_planeConvexCF57 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 20
  %72 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_planeConvexCF57, align 4
  %73 = bitcast %struct.btCollisionAlgorithmCreateFunc* %72 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %73)
  %m_pdSolver = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 6
  %74 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_pdSolver, align 4
  %75 = bitcast %class.btConvexPenetrationDepthSolver* %74 to %class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)***
  %vtable58 = load %class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)**, %class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)*** %75, align 4
  %vfn59 = getelementptr inbounds %class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)*, %class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)** %vtable58, i64 0
  %76 = load %class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)*, %class.btConvexPenetrationDepthSolver* (%class.btConvexPenetrationDepthSolver*)** %vfn59, align 4
  %call60 = call %class.btConvexPenetrationDepthSolver* %76(%class.btConvexPenetrationDepthSolver* %74) #6
  %m_pdSolver61 = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 6
  %77 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %m_pdSolver61, align 4
  %78 = bitcast %class.btConvexPenetrationDepthSolver* %77 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %78)
  %79 = bitcast %class.btDefaultCollisionConfiguration* %this1 to %class.btCollisionConfiguration*
  %call62 = call %class.btCollisionConfiguration* @_ZN24btCollisionConfigurationD2Ev(%class.btCollisionConfiguration* %79) #6
  %80 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %retval, align 4
  ret %class.btDefaultCollisionConfiguration* %80
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPoolAllocator* @_ZN15btPoolAllocatorD2Ev(%class.btPoolAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btPoolAllocator*, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  %m_pool = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  %0 = load i8*, i8** %m_pool, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret %class.btPoolAllocator* %this1
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN31btDefaultCollisionConfigurationD0Ev(%class.btDefaultCollisionConfiguration* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %call = call %class.btDefaultCollisionConfiguration* @_ZN31btDefaultCollisionConfigurationD1Ev(%class.btDefaultCollisionConfiguration* %this1) #6
  %0 = bitcast %class.btDefaultCollisionConfiguration* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

; Function Attrs: noinline optnone
define hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN31btDefaultCollisionConfiguration35getClosestPointsAlgorithmCreateFuncEii(%class.btDefaultCollisionConfiguration* %this, i32 %proxyType0, i32 %proxyType1) unnamed_addr #2 {
entry:
  %retval = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  %proxyType0.addr = alloca i32, align 4
  %proxyType1.addr = alloca i32, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  store i32 %proxyType0, i32* %proxyType0.addr, align 4
  store i32 %proxyType1, i32* %proxyType1.addr, align 4
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %0 = load i32, i32* %proxyType0.addr, align 4
  %cmp = icmp eq i32 %0, 8
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %proxyType1.addr, align 4
  %cmp2 = icmp eq i32 %1, 8
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %m_sphereSphereCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 14
  %2 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_sphereSphereCF, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %2, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %3 = load i32, i32* %proxyType0.addr, align 4
  %cmp3 = icmp eq i32 %3, 8
  br i1 %cmp3, label %land.lhs.true4, label %if.end7

land.lhs.true4:                                   ; preds = %if.end
  %4 = load i32, i32* %proxyType1.addr, align 4
  %cmp5 = icmp eq i32 %4, 1
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %land.lhs.true4
  %m_sphereTriangleCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 18
  %5 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_sphereTriangleCF, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %5, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end7:                                          ; preds = %land.lhs.true4, %if.end
  %6 = load i32, i32* %proxyType0.addr, align 4
  %cmp8 = icmp eq i32 %6, 1
  br i1 %cmp8, label %land.lhs.true9, label %if.end12

land.lhs.true9:                                   ; preds = %if.end7
  %7 = load i32, i32* %proxyType1.addr, align 4
  %cmp10 = icmp eq i32 %7, 8
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %land.lhs.true9
  %m_triangleSphereCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 19
  %8 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_triangleSphereCF, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %8, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end12:                                         ; preds = %land.lhs.true9, %if.end7
  %9 = load i32, i32* %proxyType0.addr, align 4
  %call = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %9)
  br i1 %call, label %land.lhs.true13, label %if.end16

land.lhs.true13:                                  ; preds = %if.end12
  %10 = load i32, i32* %proxyType1.addr, align 4
  %cmp14 = icmp eq i32 %10, 28
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %land.lhs.true13
  %m_convexPlaneCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 21
  %11 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexPlaneCF, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %11, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end16:                                         ; preds = %land.lhs.true13, %if.end12
  %12 = load i32, i32* %proxyType1.addr, align 4
  %call17 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %12)
  br i1 %call17, label %land.lhs.true18, label %if.end21

land.lhs.true18:                                  ; preds = %if.end16
  %13 = load i32, i32* %proxyType0.addr, align 4
  %cmp19 = icmp eq i32 %13, 28
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %land.lhs.true18
  %m_planeConvexCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 20
  %14 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_planeConvexCF, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %14, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end21:                                         ; preds = %land.lhs.true18, %if.end16
  %15 = load i32, i32* %proxyType0.addr, align 4
  %call22 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %15)
  br i1 %call22, label %land.lhs.true23, label %if.end26

land.lhs.true23:                                  ; preds = %if.end21
  %16 = load i32, i32* %proxyType1.addr, align 4
  %call24 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %16)
  br i1 %call24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %land.lhs.true23
  %m_convexConvexCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 7
  %17 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConvexCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %17, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end26:                                         ; preds = %land.lhs.true23, %if.end21
  %18 = load i32, i32* %proxyType0.addr, align 4
  %call27 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %18)
  br i1 %call27, label %land.lhs.true28, label %if.end31

land.lhs.true28:                                  ; preds = %if.end26
  %19 = load i32, i32* %proxyType1.addr, align 4
  %call29 = call zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %19)
  br i1 %call29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %land.lhs.true28
  %m_convexConcaveCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 8
  %20 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConcaveCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %20, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end31:                                         ; preds = %land.lhs.true28, %if.end26
  %21 = load i32, i32* %proxyType1.addr, align 4
  %call32 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %21)
  br i1 %call32, label %land.lhs.true33, label %if.end36

land.lhs.true33:                                  ; preds = %if.end31
  %22 = load i32, i32* %proxyType0.addr, align 4
  %call34 = call zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %22)
  br i1 %call34, label %if.then35, label %if.end36

if.then35:                                        ; preds = %land.lhs.true33
  %m_swappedConvexConcaveCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 9
  %23 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedConvexConcaveCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %23, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end36:                                         ; preds = %land.lhs.true33, %if.end31
  %24 = load i32, i32* %proxyType0.addr, align 4
  %call37 = call zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %24)
  br i1 %call37, label %land.lhs.true38, label %if.end41

land.lhs.true38:                                  ; preds = %if.end36
  %25 = load i32, i32* %proxyType1.addr, align 4
  %call39 = call zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %25)
  br i1 %call39, label %if.then40, label %if.end41

if.then40:                                        ; preds = %land.lhs.true38
  %m_compoundCompoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 11
  %26 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCompoundCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %26, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end41:                                         ; preds = %land.lhs.true38, %if.end36
  %27 = load i32, i32* %proxyType0.addr, align 4
  %call42 = call zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %27)
  br i1 %call42, label %if.then43, label %if.else

if.then43:                                        ; preds = %if.end41
  %m_compoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 10
  %28 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %28, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.else:                                          ; preds = %if.end41
  %29 = load i32, i32* %proxyType1.addr, align 4
  %call44 = call zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %29)
  br i1 %call44, label %if.then45, label %if.end46

if.then45:                                        ; preds = %if.else
  %m_swappedCompoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 12
  %30 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedCompoundCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %30, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end46:                                         ; preds = %if.else
  br label %if.end47

if.end47:                                         ; preds = %if.end46
  %m_emptyCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 13
  %31 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_emptyCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %31, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

return:                                           ; preds = %if.end47, %if.then45, %if.then43, %if.then40, %if.then35, %if.then30, %if.then25, %if.then20, %if.then15, %if.then11, %if.then6, %if.then
  %32 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  ret %struct.btCollisionAlgorithmCreateFunc* %32
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %proxyType) #1 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4
  %0 = load i32, i32* %proxyType.addr, align 4
  %cmp = icmp slt i32 %0, 20
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %proxyType) #1 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4
  %0 = load i32, i32* %proxyType.addr, align 4
  %cmp = icmp sgt i32 %0, 20
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load i32, i32* %proxyType.addr, align 4
  %cmp1 = icmp slt i32 %1, 30
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %cmp1, %land.rhs ]
  ret i1 %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %proxyType) #1 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4
  %0 = load i32, i32* %proxyType.addr, align 4
  %cmp = icmp eq i32 %0, 31
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii(%class.btDefaultCollisionConfiguration* %this, i32 %proxyType0, i32 %proxyType1) unnamed_addr #2 {
entry:
  %retval = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  %proxyType0.addr = alloca i32, align 4
  %proxyType1.addr = alloca i32, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  store i32 %proxyType0, i32* %proxyType0.addr, align 4
  store i32 %proxyType1, i32* %proxyType1.addr, align 4
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %0 = load i32, i32* %proxyType0.addr, align 4
  %cmp = icmp eq i32 %0, 8
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %1 = load i32, i32* %proxyType1.addr, align 4
  %cmp2 = icmp eq i32 %1, 8
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %m_sphereSphereCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 14
  %2 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_sphereSphereCF, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %2, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end:                                           ; preds = %land.lhs.true, %entry
  %3 = load i32, i32* %proxyType0.addr, align 4
  %cmp3 = icmp eq i32 %3, 8
  br i1 %cmp3, label %land.lhs.true4, label %if.end7

land.lhs.true4:                                   ; preds = %if.end
  %4 = load i32, i32* %proxyType1.addr, align 4
  %cmp5 = icmp eq i32 %4, 1
  br i1 %cmp5, label %if.then6, label %if.end7

if.then6:                                         ; preds = %land.lhs.true4
  %m_sphereTriangleCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 18
  %5 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_sphereTriangleCF, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %5, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end7:                                          ; preds = %land.lhs.true4, %if.end
  %6 = load i32, i32* %proxyType0.addr, align 4
  %cmp8 = icmp eq i32 %6, 1
  br i1 %cmp8, label %land.lhs.true9, label %if.end12

land.lhs.true9:                                   ; preds = %if.end7
  %7 = load i32, i32* %proxyType1.addr, align 4
  %cmp10 = icmp eq i32 %7, 8
  br i1 %cmp10, label %if.then11, label %if.end12

if.then11:                                        ; preds = %land.lhs.true9
  %m_triangleSphereCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 19
  %8 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_triangleSphereCF, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %8, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end12:                                         ; preds = %land.lhs.true9, %if.end7
  %9 = load i32, i32* %proxyType0.addr, align 4
  %cmp13 = icmp eq i32 %9, 0
  br i1 %cmp13, label %land.lhs.true14, label %if.end17

land.lhs.true14:                                  ; preds = %if.end12
  %10 = load i32, i32* %proxyType1.addr, align 4
  %cmp15 = icmp eq i32 %10, 0
  br i1 %cmp15, label %if.then16, label %if.end17

if.then16:                                        ; preds = %land.lhs.true14
  %m_boxBoxCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 17
  %11 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_boxBoxCF, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %11, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end17:                                         ; preds = %land.lhs.true14, %if.end12
  %12 = load i32, i32* %proxyType0.addr, align 4
  %call = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %12)
  br i1 %call, label %land.lhs.true18, label %if.end21

land.lhs.true18:                                  ; preds = %if.end17
  %13 = load i32, i32* %proxyType1.addr, align 4
  %cmp19 = icmp eq i32 %13, 28
  br i1 %cmp19, label %if.then20, label %if.end21

if.then20:                                        ; preds = %land.lhs.true18
  %m_convexPlaneCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 21
  %14 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexPlaneCF, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %14, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end21:                                         ; preds = %land.lhs.true18, %if.end17
  %15 = load i32, i32* %proxyType1.addr, align 4
  %call22 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %15)
  br i1 %call22, label %land.lhs.true23, label %if.end26

land.lhs.true23:                                  ; preds = %if.end21
  %16 = load i32, i32* %proxyType0.addr, align 4
  %cmp24 = icmp eq i32 %16, 28
  br i1 %cmp24, label %if.then25, label %if.end26

if.then25:                                        ; preds = %land.lhs.true23
  %m_planeConvexCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 20
  %17 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_planeConvexCF, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %17, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end26:                                         ; preds = %land.lhs.true23, %if.end21
  %18 = load i32, i32* %proxyType0.addr, align 4
  %call27 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %18)
  br i1 %call27, label %land.lhs.true28, label %if.end31

land.lhs.true28:                                  ; preds = %if.end26
  %19 = load i32, i32* %proxyType1.addr, align 4
  %call29 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %19)
  br i1 %call29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %land.lhs.true28
  %m_convexConvexCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 7
  %20 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConvexCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %20, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end31:                                         ; preds = %land.lhs.true28, %if.end26
  %21 = load i32, i32* %proxyType0.addr, align 4
  %call32 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %21)
  br i1 %call32, label %land.lhs.true33, label %if.end36

land.lhs.true33:                                  ; preds = %if.end31
  %22 = load i32, i32* %proxyType1.addr, align 4
  %call34 = call zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %22)
  br i1 %call34, label %if.then35, label %if.end36

if.then35:                                        ; preds = %land.lhs.true33
  %m_convexConcaveCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 8
  %23 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConcaveCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %23, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end36:                                         ; preds = %land.lhs.true33, %if.end31
  %24 = load i32, i32* %proxyType1.addr, align 4
  %call37 = call zeroext i1 @_ZN17btBroadphaseProxy8isConvexEi(i32 %24)
  br i1 %call37, label %land.lhs.true38, label %if.end41

land.lhs.true38:                                  ; preds = %if.end36
  %25 = load i32, i32* %proxyType0.addr, align 4
  %call39 = call zeroext i1 @_ZN17btBroadphaseProxy9isConcaveEi(i32 %25)
  br i1 %call39, label %if.then40, label %if.end41

if.then40:                                        ; preds = %land.lhs.true38
  %m_swappedConvexConcaveCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 9
  %26 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedConvexConcaveCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %26, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end41:                                         ; preds = %land.lhs.true38, %if.end36
  %27 = load i32, i32* %proxyType0.addr, align 4
  %call42 = call zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %27)
  br i1 %call42, label %land.lhs.true43, label %if.end46

land.lhs.true43:                                  ; preds = %if.end41
  %28 = load i32, i32* %proxyType1.addr, align 4
  %call44 = call zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %28)
  br i1 %call44, label %if.then45, label %if.end46

if.then45:                                        ; preds = %land.lhs.true43
  %m_compoundCompoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 11
  %29 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCompoundCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %29, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end46:                                         ; preds = %land.lhs.true43, %if.end41
  %30 = load i32, i32* %proxyType0.addr, align 4
  %call47 = call zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %30)
  br i1 %call47, label %if.then48, label %if.else

if.then48:                                        ; preds = %if.end46
  %m_compoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 10
  %31 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_compoundCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %31, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.else:                                          ; preds = %if.end46
  %32 = load i32, i32* %proxyType1.addr, align 4
  %call49 = call zeroext i1 @_ZN17btBroadphaseProxy10isCompoundEi(i32 %32)
  br i1 %call49, label %if.then50, label %if.end51

if.then50:                                        ; preds = %if.else
  %m_swappedCompoundCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 12
  %33 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_swappedCompoundCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %33, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

if.end51:                                         ; preds = %if.else
  br label %if.end52

if.end52:                                         ; preds = %if.end51
  %m_emptyCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 13
  %34 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_emptyCreateFunc, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %34, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  br label %return

return:                                           ; preds = %if.end52, %if.then50, %if.then48, %if.then45, %if.then40, %if.then35, %if.then30, %if.then25, %if.then20, %if.then16, %if.then11, %if.then6, %if.then
  %35 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %retval, align 4
  ret %struct.btCollisionAlgorithmCreateFunc* %35
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN31btDefaultCollisionConfiguration35setConvexConvexMultipointIterationsEii(%class.btDefaultCollisionConfiguration* %this, i32 %numPerturbationIterations, i32 %minimumPointsPerturbationThreshold) #1 {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  %numPerturbationIterations.addr = alloca i32, align 4
  %minimumPointsPerturbationThreshold.addr = alloca i32, align 4
  %convexConvex = alloca %"struct.btConvexConvexAlgorithm::CreateFunc"*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  store i32 %numPerturbationIterations, i32* %numPerturbationIterations.addr, align 4
  store i32 %minimumPointsPerturbationThreshold, i32* %minimumPointsPerturbationThreshold.addr, align 4
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %m_convexConvexCreateFunc = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 7
  %0 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexConvexCreateFunc, align 4
  %1 = bitcast %struct.btCollisionAlgorithmCreateFunc* %0 to %"struct.btConvexConvexAlgorithm::CreateFunc"*
  store %"struct.btConvexConvexAlgorithm::CreateFunc"* %1, %"struct.btConvexConvexAlgorithm::CreateFunc"** %convexConvex, align 4
  %2 = load i32, i32* %numPerturbationIterations.addr, align 4
  %3 = load %"struct.btConvexConvexAlgorithm::CreateFunc"*, %"struct.btConvexConvexAlgorithm::CreateFunc"** %convexConvex, align 4
  %m_numPerturbationIterations = getelementptr inbounds %"struct.btConvexConvexAlgorithm::CreateFunc", %"struct.btConvexConvexAlgorithm::CreateFunc"* %3, i32 0, i32 2
  store i32 %2, i32* %m_numPerturbationIterations, align 4
  %4 = load i32, i32* %minimumPointsPerturbationThreshold.addr, align 4
  %5 = load %"struct.btConvexConvexAlgorithm::CreateFunc"*, %"struct.btConvexConvexAlgorithm::CreateFunc"** %convexConvex, align 4
  %m_minimumPointsPerturbationThreshold = getelementptr inbounds %"struct.btConvexConvexAlgorithm::CreateFunc", %"struct.btConvexConvexAlgorithm::CreateFunc"* %5, i32 0, i32 3
  store i32 %4, i32* %m_minimumPointsPerturbationThreshold, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN31btDefaultCollisionConfiguration34setPlaneConvexMultipointIterationsEii(%class.btDefaultCollisionConfiguration* %this, i32 %numPerturbationIterations, i32 %minimumPointsPerturbationThreshold) #1 {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  %numPerturbationIterations.addr = alloca i32, align 4
  %minimumPointsPerturbationThreshold.addr = alloca i32, align 4
  %cpCF = alloca %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, align 4
  %pcCF = alloca %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  store i32 %numPerturbationIterations, i32* %numPerturbationIterations.addr, align 4
  store i32 %minimumPointsPerturbationThreshold, i32* %minimumPointsPerturbationThreshold.addr, align 4
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %m_convexPlaneCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 21
  %0 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_convexPlaneCF, align 4
  %1 = bitcast %struct.btCollisionAlgorithmCreateFunc* %0 to %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*
  store %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %1, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %cpCF, align 4
  %2 = load i32, i32* %numPerturbationIterations.addr, align 4
  %3 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %cpCF, align 4
  %m_numPerturbationIterations = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %3, i32 0, i32 1
  store i32 %2, i32* %m_numPerturbationIterations, align 4
  %4 = load i32, i32* %minimumPointsPerturbationThreshold.addr, align 4
  %5 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %cpCF, align 4
  %m_minimumPointsPerturbationThreshold = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %5, i32 0, i32 2
  store i32 %4, i32* %m_minimumPointsPerturbationThreshold, align 4
  %m_planeConvexCF = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 20
  %6 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %m_planeConvexCF, align 4
  %7 = bitcast %struct.btCollisionAlgorithmCreateFunc* %6 to %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*
  store %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %7, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %pcCF, align 4
  %8 = load i32, i32* %numPerturbationIterations.addr, align 4
  %9 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %pcCF, align 4
  %m_numPerturbationIterations2 = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %9, i32 0, i32 1
  store i32 %8, i32* %m_numPerturbationIterations2, align 4
  %10 = load i32, i32* %minimumPointsPerturbationThreshold.addr, align 4
  %11 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %pcCF, align 4
  %m_minimumPointsPerturbationThreshold3 = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %11, i32 0, i32 2
  store i32 %10, i32* %m_minimumPointsPerturbationThreshold3, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPoolAllocator* @_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv(%class.btDefaultCollisionConfiguration* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %m_persistentManifoldPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 2
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPool, align 4
  ret %class.btPoolAllocator* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPoolAllocator* @_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv(%class.btDefaultCollisionConfiguration* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDefaultCollisionConfiguration*, align 4
  store %class.btDefaultCollisionConfiguration* %this, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %this1 = load %class.btDefaultCollisionConfiguration*, %class.btDefaultCollisionConfiguration** %this.addr, align 4
  %m_collisionAlgorithmPool = getelementptr inbounds %class.btDefaultCollisionConfiguration, %class.btDefaultCollisionConfiguration* %this1, i32 0, i32 4
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPool, align 4
  ret %class.btPoolAllocator* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionConfiguration* @_ZN24btCollisionConfigurationD2Ev(%class.btCollisionConfiguration* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionConfiguration*, align 4
  store %class.btCollisionConfiguration* %this, %class.btCollisionConfiguration** %this.addr, align 4
  %this1 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %this.addr, align 4
  ret %class.btCollisionConfiguration* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN24btCollisionConfigurationD0Ev(%class.btCollisionConfiguration* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionConfiguration*, align 4
  store %class.btCollisionConfiguration* %this, %class.btCollisionConfiguration** %this.addr, align 4
  %this1 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %this.addr, align 4
  call void @llvm.trap() #8
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverC2Ev(%class.btConvexPenetrationDepthSolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  store %class.btConvexPenetrationDepthSolver* %this, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  %this1 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  %0 = bitcast %class.btConvexPenetrationDepthSolver* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV30btConvexPenetrationDepthSolver, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btConvexPenetrationDepthSolver* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btConvexPenetrationDepthSolver* @_ZN30btConvexPenetrationDepthSolverD2Ev(%class.btConvexPenetrationDepthSolver* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  store %class.btConvexPenetrationDepthSolver* %this, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  %this1 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  ret %class.btConvexPenetrationDepthSolver* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btConvexPenetrationDepthSolverD0Ev(%class.btConvexPenetrationDepthSolver* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConvexPenetrationDepthSolver*, align 4
  store %class.btConvexPenetrationDepthSolver* %this, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  %this1 = load %class.btConvexPenetrationDepthSolver*, %class.btConvexPenetrationDepthSolver** %this.addr, align 4
  call void @llvm.trap() #8
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncC2Ev(%struct.btCollisionAlgorithmCreateFunc* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %0 = bitcast %struct.btCollisionAlgorithmCreateFunc* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV30btCollisionAlgorithmCreateFunc, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %this1, i32 0, i32 1
  store i8 0, i8* %m_swapped, align 4
  ret %struct.btCollisionAlgorithmCreateFunc* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* @_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD2Ev(%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this, %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #6
  ret %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD0Ev(%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this, %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* @_ZN33btConvexConcaveCollisionAlgorithm10CreateFuncD2Ev(%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this1) #6
  %0 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN33btConvexConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"* %this, %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"*, %"struct.btConvexConcaveCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %3 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %3(%class.btDispatcher* %1, i32 80)
  store i8* %call, i8** %mem, align 4
  %4 = load i8*, i8** %mem, align 4
  %call2 = call i8* @_ZN33btConvexConcaveCollisionAlgorithmnwEmPv(i32 80, i8* %4)
  %5 = bitcast i8* %call2 to %class.btConvexConcaveCollisionAlgorithm*
  %6 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call3 = call %class.btConvexConcaveCollisionAlgorithm* @_ZN33btConvexConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btConvexConcaveCollisionAlgorithm* %5, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %6, %struct.btCollisionObjectWrapper* %7, %struct.btCollisionObjectWrapper* %8, i1 zeroext false)
  %9 = bitcast %class.btConvexConcaveCollisionAlgorithm* %5 to %class.btCollisionAlgorithm*
  ret %class.btCollisionAlgorithm* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  ret %struct.btCollisionAlgorithmCreateFunc* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN30btCollisionAlgorithmCreateFuncD0Ev(%struct.btCollisionAlgorithmCreateFunc* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %this1) #6
  %0 = bitcast %struct.btCollisionAlgorithmCreateFunc* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN30btCollisionAlgorithmCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS4_(%struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %0, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  %.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %this, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %0, %struct.btCollisionAlgorithmConstructionInfo** %.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %this.addr, align 4
  ret %class.btCollisionAlgorithm* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN33btConvexConcaveCollisionAlgorithmnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

declare %class.btConvexConcaveCollisionAlgorithm* @_ZN33btConvexConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btConvexConcaveCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD2Ev(%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #6
  ret %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev(%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %call = call %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFuncD2Ev(%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1) #6
  %0 = bitcast %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN33btConvexConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btConvexConcaveCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %3 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %3(%class.btDispatcher* %1, i32 80)
  store i8* %call, i8** %mem, align 4
  %4 = load i8*, i8** %mem, align 4
  %call2 = call i8* @_ZN33btConvexConcaveCollisionAlgorithmnwEmPv(i32 80, i8* %4)
  %5 = bitcast i8* %call2 to %class.btConvexConcaveCollisionAlgorithm*
  %6 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call3 = call %class.btConvexConcaveCollisionAlgorithm* @_ZN33btConvexConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btConvexConcaveCollisionAlgorithm* %5, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %6, %struct.btCollisionObjectWrapper* %7, %struct.btCollisionObjectWrapper* %8, i1 zeroext true)
  %9 = bitcast %class.btConvexConcaveCollisionAlgorithm* %5 to %class.btCollisionAlgorithm*
  ret %class.btCollisionAlgorithm* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCompoundCollisionAlgorithm::CreateFunc"* @_ZN28btCompoundCollisionAlgorithm10CreateFuncD2Ev(%"struct.btCompoundCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this, %"struct.btCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btCompoundCollisionAlgorithm::CreateFunc"*, %"struct.btCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #6
  ret %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN28btCompoundCollisionAlgorithm10CreateFuncD0Ev(%"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this, %"struct.btCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btCompoundCollisionAlgorithm::CreateFunc"*, %"struct.btCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btCompoundCollisionAlgorithm::CreateFunc"* @_ZN28btCompoundCollisionAlgorithm10CreateFuncD2Ev(%"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this1) #6
  %0 = bitcast %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btCompoundCollisionAlgorithm::CreateFunc"* %this, %"struct.btCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %"struct.btCompoundCollisionAlgorithm::CreateFunc"*, %"struct.btCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %3 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %3(%class.btDispatcher* %1, i32 84)
  store i8* %call, i8** %mem, align 4
  %4 = load i8*, i8** %mem, align 4
  %5 = bitcast i8* %4 to %class.btCompoundCollisionAlgorithm*
  %6 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call2 = call %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCollisionAlgorithm* %5, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %6, %struct.btCollisionObjectWrapper* %7, %struct.btCollisionObjectWrapper* %8, i1 zeroext false)
  %9 = bitcast %class.btCompoundCollisionAlgorithm* %5 to %class.btCollisionAlgorithm*
  ret %class.btCollisionAlgorithm* %9
}

declare %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncD2Ev(%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this, %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #6
  ret %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncD0Ev(%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this, %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFuncD2Ev(%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this1) #6
  %0 = bitcast %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN36btCompoundCompoundCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"* %this, %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"*, %"struct.btCompoundCompoundCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %3 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %3(%class.btDispatcher* %1, i32 116)
  store i8* %call, i8** %mem, align 4
  %4 = load i8*, i8** %mem, align 4
  %5 = bitcast i8* %4 to %class.btCompoundCompoundCollisionAlgorithm*
  %6 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call2 = call %class.btCompoundCompoundCollisionAlgorithm* @_ZN36btCompoundCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCompoundCollisionAlgorithm* %5, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %6, %struct.btCollisionObjectWrapper* %7, %struct.btCollisionObjectWrapper* %8, i1 zeroext false)
  %9 = bitcast %class.btCompoundCompoundCollisionAlgorithm* %5 to %class.btCollisionAlgorithm*
  ret %class.btCollisionAlgorithm* %9
}

declare %class.btCompoundCompoundCollisionAlgorithm* @_ZN36btCompoundCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCompoundCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD2Ev(%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #6
  ret %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD0Ev(%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, align 4
  store %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %call = call %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFuncD2Ev(%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this1) #6
  %0 = bitcast %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"* %this, %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"*, %"struct.btCompoundCollisionAlgorithm::SwappedCreateFunc"** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %3 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %3(%class.btDispatcher* %1, i32 84)
  store i8* %call, i8** %mem, align 4
  %4 = load i8*, i8** %mem, align 4
  %5 = bitcast i8* %4 to %class.btCompoundCollisionAlgorithm*
  %6 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call2 = call %class.btCompoundCollisionAlgorithm* @_ZN28btCompoundCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_b(%class.btCompoundCollisionAlgorithm* %5, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %6, %struct.btCollisionObjectWrapper* %7, %struct.btCollisionObjectWrapper* %8, i1 zeroext true)
  %9 = bitcast %class.btCompoundCollisionAlgorithm* %5 to %class.btCollisionAlgorithm*
  ret %class.btCollisionAlgorithm* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btEmptyAlgorithm::CreateFunc"* @_ZN16btEmptyAlgorithm10CreateFuncD2Ev(%"struct.btEmptyAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btEmptyAlgorithm::CreateFunc"*, align 4
  store %"struct.btEmptyAlgorithm::CreateFunc"* %this, %"struct.btEmptyAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btEmptyAlgorithm::CreateFunc"*, %"struct.btEmptyAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btEmptyAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #6
  ret %"struct.btEmptyAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btEmptyAlgorithm10CreateFuncD0Ev(%"struct.btEmptyAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btEmptyAlgorithm::CreateFunc"*, align 4
  store %"struct.btEmptyAlgorithm::CreateFunc"* %this, %"struct.btEmptyAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btEmptyAlgorithm::CreateFunc"*, %"struct.btEmptyAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btEmptyAlgorithm::CreateFunc"* @_ZN16btEmptyAlgorithm10CreateFuncD2Ev(%"struct.btEmptyAlgorithm::CreateFunc"* %this1) #6
  %0 = bitcast %"struct.btEmptyAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN16btEmptyAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btEmptyAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btEmptyAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btEmptyAlgorithm::CreateFunc"* %this, %"struct.btEmptyAlgorithm::CreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %"struct.btEmptyAlgorithm::CreateFunc"*, %"struct.btEmptyAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %3 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %3(%class.btDispatcher* %1, i32 8)
  store i8* %call, i8** %mem, align 4
  %4 = load i8*, i8** %mem, align 4
  %5 = bitcast i8* %4 to %class.btEmptyAlgorithm*
  %6 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %call2 = call %class.btEmptyAlgorithm* @_ZN16btEmptyAlgorithmC1ERK36btCollisionAlgorithmConstructionInfo(%class.btEmptyAlgorithm* %5, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %6)
  %7 = bitcast %class.btEmptyAlgorithm* %5 to %class.btCollisionAlgorithm*
  ret %class.btCollisionAlgorithm* %7
}

declare %class.btEmptyAlgorithm* @_ZN16btEmptyAlgorithmC1ERK36btCollisionAlgorithmConstructionInfo(%class.btEmptyAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8)) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* @_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD2Ev(%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this, %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #6
  ret %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD0Ev(%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this, %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* @_ZN32btSphereSphereCollisionAlgorithm10CreateFuncD2Ev(%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this1) #6
  %0 = bitcast %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN32btSphereSphereCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %col0Wrap, %struct.btCollisionObjectWrapper* %col1Wrap) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %col0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %col1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"* %this, %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %col0Wrap, %struct.btCollisionObjectWrapper** %col0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %col1Wrap, %struct.btCollisionObjectWrapper** %col1Wrap.addr, align 4
  %this1 = load %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"*, %"struct.btSphereSphereCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %3 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %3(%class.btDispatcher* %1, i32 16)
  store i8* %call, i8** %mem, align 4
  %4 = load i8*, i8** %mem, align 4
  %5 = bitcast i8* %4 to %class.btSphereSphereCollisionAlgorithm*
  %6 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0Wrap.addr, align 4
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1Wrap.addr, align 4
  %call2 = call %class.btSphereSphereCollisionAlgorithm* @_ZN32btSphereSphereCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btSphereSphereCollisionAlgorithm* %5, %class.btPersistentManifold* null, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %6, %struct.btCollisionObjectWrapper* %7, %struct.btCollisionObjectWrapper* %8)
  %9 = bitcast %class.btSphereSphereCollisionAlgorithm* %5 to %class.btCollisionAlgorithm*
  ret %class.btCollisionAlgorithm* %9
}

declare %class.btSphereSphereCollisionAlgorithm* @_ZN32btSphereSphereCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btSphereSphereCollisionAlgorithm* returned, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD2Ev(%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this, %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #6
  ret %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD0Ev(%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this, %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFuncD2Ev(%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this1) #6
  %0 = bitcast %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN34btSphereTriangleCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this, %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"*, %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %3 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %3(%class.btDispatcher* %1, i32 20)
  store i8* %call, i8** %mem, align 4
  %4 = load i8*, i8** %mem, align 4
  %5 = bitcast i8* %4 to %class.btSphereTriangleCollisionAlgorithm*
  %6 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_manifold = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %6, i32 0, i32 1
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifold, align 4
  %8 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %11 = bitcast %"struct.btSphereTriangleCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %11, i32 0, i32 1
  %12 = load i8, i8* %m_swapped, align 4
  %tobool = trunc i8 %12 to i1
  %call2 = call %class.btSphereTriangleCollisionAlgorithm* @_ZN34btSphereTriangleCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b(%class.btSphereTriangleCollisionAlgorithm* %5, %class.btPersistentManifold* %7, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %8, %struct.btCollisionObjectWrapper* %9, %struct.btCollisionObjectWrapper* %10, i1 zeroext %tobool)
  %13 = bitcast %class.btSphereTriangleCollisionAlgorithm* %5 to %class.btCollisionAlgorithm*
  ret %class.btCollisionAlgorithm* %13
}

declare %class.btSphereTriangleCollisionAlgorithm* @_ZN34btSphereTriangleCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b(%class.btSphereTriangleCollisionAlgorithm* returned, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* @_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD2Ev(%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this, %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #6
  ret %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD0Ev(%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this, %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* @_ZN26btBoxBoxCollisionAlgorithm10CreateFuncD2Ev(%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this1) #6
  %0 = bitcast %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN26btBoxBoxCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %bbsize = alloca i32, align 4
  %ptr = alloca i8*, align 4
  store %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"* %this, %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"*, %"struct.btBoxBoxCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  store i32 16, i32* %bbsize, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = load i32, i32* %bbsize, align 4
  %3 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %3, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %4 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %4(%class.btDispatcher* %1, i32 %2)
  store i8* %call, i8** %ptr, align 4
  %5 = load i8*, i8** %ptr, align 4
  %6 = bitcast i8* %5 to %class.btBoxBoxCollisionAlgorithm*
  %7 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call2 = call %class.btBoxBoxCollisionAlgorithm* @_ZN26btBoxBoxCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btBoxBoxCollisionAlgorithm* %6, %class.btPersistentManifold* null, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %7, %struct.btCollisionObjectWrapper* %8, %struct.btCollisionObjectWrapper* %9)
  %10 = bitcast %class.btBoxBoxCollisionAlgorithm* %6 to %class.btCollisionAlgorithm*
  ret %class.btCollisionAlgorithm* %10
}

declare %class.btBoxBoxCollisionAlgorithm* @_ZN26btBoxBoxCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btBoxBoxCollisionAlgorithm* returned, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD2Ev(%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %call = call %struct.btCollisionAlgorithmCreateFunc* @_ZN30btCollisionAlgorithmCreateFuncD2Ev(%struct.btCollisionAlgorithmCreateFunc* %0) #6
  ret %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD0Ev(%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, align 4
  store %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %this1 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %call = call %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFuncD2Ev(%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1) #6
  %0 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #7
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN31btConvexPlaneCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btCollisionAlgorithm*, align 4
  %this.addr = alloca %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %mem = alloca i8*, align 4
  store %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %this1 = load %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"*, %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %0, i32 0, i32 0
  %1 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher1, align 4
  %2 = bitcast %class.btDispatcher* %1 to i8* (%class.btDispatcher*, i32)***
  %vtable = load i8* (%class.btDispatcher*, i32)**, i8* (%class.btDispatcher*, i32)*** %2, align 4
  %vfn = getelementptr inbounds i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vtable, i64 14
  %3 = load i8* (%class.btDispatcher*, i32)*, i8* (%class.btDispatcher*, i32)** %vfn, align 4
  %call = call i8* %3(%class.btDispatcher* %1, i32 28)
  store i8* %call, i8** %mem, align 4
  %4 = bitcast %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1 to %struct.btCollisionAlgorithmCreateFunc*
  %m_swapped = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc, %struct.btCollisionAlgorithmCreateFunc* %4, i32 0, i32 1
  %5 = load i8, i8* %m_swapped, align 4
  %tobool = trunc i8 %5 to i1
  br i1 %tobool, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %6 = load i8*, i8** %mem, align 4
  %7 = bitcast i8* %6 to %class.btConvexPlaneCollisionAlgorithm*
  %8 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %m_numPerturbationIterations = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1, i32 0, i32 1
  %11 = load i32, i32* %m_numPerturbationIterations, align 4
  %m_minimumPointsPerturbationThreshold = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1, i32 0, i32 2
  %12 = load i32, i32* %m_minimumPointsPerturbationThreshold, align 4
  %call2 = call %class.btConvexPlaneCollisionAlgorithm* @_ZN31btConvexPlaneCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_bii(%class.btConvexPlaneCollisionAlgorithm* %7, %class.btPersistentManifold* null, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %8, %struct.btCollisionObjectWrapper* %9, %struct.btCollisionObjectWrapper* %10, i1 zeroext false, i32 %11, i32 %12)
  %13 = bitcast %class.btConvexPlaneCollisionAlgorithm* %7 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %13, %class.btCollisionAlgorithm** %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %14 = load i8*, i8** %mem, align 4
  %15 = bitcast i8* %14 to %class.btConvexPlaneCollisionAlgorithm*
  %16 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %17 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %18 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %m_numPerturbationIterations3 = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1, i32 0, i32 1
  %19 = load i32, i32* %m_numPerturbationIterations3, align 4
  %m_minimumPointsPerturbationThreshold4 = getelementptr inbounds %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc", %"struct.btConvexPlaneCollisionAlgorithm::CreateFunc"* %this1, i32 0, i32 2
  %20 = load i32, i32* %m_minimumPointsPerturbationThreshold4, align 4
  %call5 = call %class.btConvexPlaneCollisionAlgorithm* @_ZN31btConvexPlaneCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_bii(%class.btConvexPlaneCollisionAlgorithm* %15, %class.btPersistentManifold* null, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %16, %struct.btCollisionObjectWrapper* %17, %struct.btCollisionObjectWrapper* %18, i1 zeroext true, i32 %19, i32 %20)
  %21 = bitcast %class.btConvexPlaneCollisionAlgorithm* %15 to %class.btCollisionAlgorithm*
  store %class.btCollisionAlgorithm* %21, %class.btCollisionAlgorithm** %retval, align 4
  br label %return

return:                                           ; preds = %if.else, %if.then
  %22 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %retval, align 4
  ret %class.btCollisionAlgorithm* %22
}

declare %class.btConvexPlaneCollisionAlgorithm* @_ZN31btConvexPlaneCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_bii(%class.btConvexPlaneCollisionAlgorithm* returned, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1 zeroext, i32, i32) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btSpinMutex* @_ZN11btSpinMutexC2Ev(%class.btSpinMutex* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSpinMutex*, align 4
  store %class.btSpinMutex* %this, %class.btSpinMutex** %this.addr, align 4
  %this1 = load %class.btSpinMutex*, %class.btSpinMutex** %this.addr, align 4
  %mLock = getelementptr inbounds %class.btSpinMutex, %class.btSpinMutex* %this1, i32 0, i32 0
  store i32 0, i32* %mLock, align 4
  ret %class.btSpinMutex* %this1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btDefaultCollisionConfiguration.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { cold noreturn nounwind }
attributes #6 = { nounwind }
attributes #7 = { builtin nounwind }
attributes #8 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
