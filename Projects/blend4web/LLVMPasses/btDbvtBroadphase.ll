; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/BroadphaseCollision/btDbvtBroadphase.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/BroadphaseCollision/btDbvtBroadphase.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%struct.btDbvtBroadphase = type { %class.btBroadphaseInterface, [2 x %struct.btDbvt], [3 x %struct.btDbvtProxy*], %class.btOverlappingPairCache*, float, i32, i32, i32, i32, i32, i32, i32, i32, float, i32, i32, i32, i8, i8, i8, i8, %class.btAlignedObjectArray.1 }
%class.btBroadphaseInterface = type { i32 (...)** }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.0 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%union.anon.0 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%struct.btDbvtProxy = type { %struct.btBroadphaseProxy, %struct.btDbvtNode*, [2 x %struct.btDbvtProxy*], i32 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.1 = type <{ %class.btAlignedAllocator.2, [3 x i8], i32, i32, %class.btAlignedObjectArray.4*, i8, [3 x i8] }>
%class.btAlignedAllocator.2 = type { i8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %struct.btDbvtNode**, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btOverlappingPairCallback = type { i32 (...)** }
%class.btHashedOverlappingPairCache = type { %class.btOverlappingPairCache, %class.btAlignedObjectArray.9, %struct.btOverlapFilterCallback*, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14, %class.btOverlappingPairCallback* }
%class.btAlignedObjectArray.9 = type <{ %class.btAlignedAllocator.10, [3 x i8], i32, i32, %struct.btBroadphasePair*, i8, [3 x i8] }>
%class.btAlignedAllocator.10 = type { i8 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.12 }
%class.btCollisionAlgorithm = type opaque
%union.anon.12 = type { i8* }
%struct.btOverlapFilterCallback = type { i32 (...)** }
%class.btAlignedObjectArray.14 = type <{ %class.btAlignedAllocator.15, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.15 = type { i8 }
%class.btDispatcher = type opaque
%struct.btDbvtTreeCollider = type { %"struct.btDbvt::ICollide", %struct.btDbvtBroadphase*, %struct.btDbvtProxy* }
%"struct.btDbvt::ICollide" = type { i32 (...)** }
%struct.btBroadphaseRayCallback = type { %struct.btBroadphaseAabbCallback, %class.btVector3, [3 x i32], float }
%struct.btBroadphaseAabbCallback = type { i32 (...)** }
%struct.BroadphaseRayTester = type { %"struct.btDbvt::ICollide", %struct.btBroadphaseRayCallback* }
%struct.BroadphaseAabbTester = type { %"struct.btDbvt::ICollide", %struct.btBroadphaseAabbCallback* }
%class.btBroadphasePairSortPredicate = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN21btBroadphaseInterfaceC2Ev = comdat any

$_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEEC2Ev = comdat any

$_ZN28btHashedOverlappingPairCachenwEmPv = comdat any

$_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE6resizeEiRKS3_ = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev = comdat any

$_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEED2Ev = comdat any

$_ZN17btBroadphaseProxynwEmPv = comdat any

$_ZN11btDbvtProxyC2ERK9btVector3S2_Pvii = comdat any

$_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_ = comdat any

$_ZN18btDbvtTreeColliderC2EP16btDbvtBroadphase = comdat any

$_ZNK6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE = comdat any

$_ZN18btDbvtTreeColliderD2Ev = comdat any

$_ZN19BroadphaseRayTesterC2ER23btBroadphaseRayCallback = comdat any

$_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEEixEi = comdat any

$_ZNK6btDbvt15rayTestInternalEPK10btDbvtNodeRK9btVector3S5_S5_PjfS5_S5_R20btAlignedObjectArrayIS2_ERNS_8ICollideE = comdat any

$_ZN19BroadphaseRayTesterD2Ev = comdat any

$_ZN20BroadphaseAabbTesterC2ER24btBroadphaseAabbCallback = comdat any

$_ZN20BroadphaseAabbTesterD2Ev = comdat any

$_Z9IntersectRK12btDbvtAabbMmS1_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_ = comdat any

$_ZN16btBroadphasePairC2Ev = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi = comdat any

$_ZeqRK16btBroadphasePairS1_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_ = comdat any

$_Z5btMaxIiERKT_S2_S2_ = comdat any

$_Z5btMinIiERKT_S2_S2_ = comdat any

$_ZN12btDbvtAabbMmC2Ev = comdat any

$_ZNK6btDbvt5emptyEv = comdat any

$_Z5MergeRK12btDbvtAabbMmS1_RS_ = comdat any

$_ZN12btDbvtAabbMm6FromCRERK9btVector3f = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK12btDbvtAabbMm4MinsEv = comdat any

$_ZNK12btDbvtAabbMm4MaxsEv = comdat any

$_ZN21btBroadphaseInterfaceD2Ev = comdat any

$_ZN21btBroadphaseInterfaceD0Ev = comdat any

$_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher = comdat any

$_ZN17btBroadphaseProxyC2ERK9btVector3S2_Pvii = comdat any

$_ZN6btDbvt8ICollideC2Ev = comdat any

$_ZN18btDbvtTreeColliderD0Ev = comdat any

$_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNodeS2_ = comdat any

$_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef = comdat any

$_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode = comdat any

$_ZN6btDbvt8ICollideD2Ev = comdat any

$_ZN6btDbvt8ICollideD0Ev = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ = comdat any

$_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE6resizeEiRKS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi = comdat any

$_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8pop_backEv = comdat any

$_ZNK10btDbvtNode10isinternalEv = comdat any

$_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE8allocateEiPPKS2_ = comdat any

$_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE10deallocateEPS2_ = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9allocSizeEi = comdat any

$_ZNK10btDbvtNode6isleafEv = comdat any

$_ZN19BroadphaseRayTesterD0Ev = comdat any

$_ZN19BroadphaseRayTester7ProcessEPK10btDbvtNode = comdat any

$_ZN9btVector3C2Ev = comdat any

$_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN20BroadphaseAabbTesterD0Ev = comdat any

$_ZN20BroadphaseAabbTester7ProcessEPK10btDbvtNode = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE6resizeEiRKS1_ = comdat any

$_ZN6btDbvt6sStkNNC2Ev = comdat any

$_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_ = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi = comdat any

$_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE10deallocateEPS1_ = comdat any

$_ZN12btDbvtAabbMm6FromCEERK9btVector3S2_ = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeE5clearEv = comdat any

$_ZN18btAlignedAllocatorI20btAlignedObjectArrayIPK10btDbvtNodeELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE4initEv = comdat any

$_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIS_IPK10btDbvtNodeEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI20btAlignedObjectArrayIPK10btDbvtNodeELj16EE10deallocateEPS4_ = comdat any

$_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2ERKS3_ = comdat any

$_ZNK20btAlignedObjectArrayIS_IPK10btDbvtNodeEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIS_IPK10btDbvtNodeEE4copyEiiPS3_ = comdat any

$_ZN18btAlignedAllocatorI20btAlignedObjectArrayIPK10btDbvtNodeELj16EE8allocateEiPPKS4_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii = comdat any

$_ZN16btBroadphasePairC2ERKS_ = comdat any

$_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi = comdat any

$_ZN16btBroadphasePairnwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_ = comdat any

$_ZTS21btBroadphaseInterface = comdat any

$_ZTI21btBroadphaseInterface = comdat any

$_ZTV21btBroadphaseInterface = comdat any

$_ZTV18btDbvtTreeCollider = comdat any

$_ZTS18btDbvtTreeCollider = comdat any

$_ZTSN6btDbvt8ICollideE = comdat any

$_ZTIN6btDbvt8ICollideE = comdat any

$_ZTI18btDbvtTreeCollider = comdat any

$_ZTVN6btDbvt8ICollideE = comdat any

$_ZTV19BroadphaseRayTester = comdat any

$_ZTS19BroadphaseRayTester = comdat any

$_ZTI19BroadphaseRayTester = comdat any

$_ZTV20BroadphaseAabbTester = comdat any

$_ZTS20BroadphaseAabbTester = comdat any

$_ZTI20BroadphaseAabbTester = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV16btDbvtBroadphase = hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btDbvtBroadphase to i8*), i8* bitcast (%struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)* @_ZN16btDbvtBroadphaseD1Ev to i8*), i8* bitcast (void (%struct.btDbvtBroadphase*)* @_ZN16btDbvtBroadphaseD0Ev to i8*), i8* bitcast (%struct.btBroadphaseProxy* (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, i32, i8*, i32, i32, %class.btDispatcher*)* @_ZN16btDbvtBroadphase11createProxyERK9btVector3S2_iPviiP12btDispatcher to i8*), i8* bitcast (void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btDispatcher*)* @_ZN16btDbvtBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher to i8*), i8* bitcast (void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*, %class.btDispatcher*)* @_ZN16btDbvtBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher to i8*), i8* bitcast (void (%struct.btDbvtBroadphase*, %struct.btBroadphaseProxy*, %class.btVector3*, %class.btVector3*)* @_ZNK16btDbvtBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_ to i8*), i8* bitcast (void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)* @_ZN16btDbvtBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_ to i8*), i8* bitcast (void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseAabbCallback*)* @_ZN16btDbvtBroadphase8aabbTestERK9btVector3S2_R24btBroadphaseAabbCallback to i8*), i8* bitcast (void (%struct.btDbvtBroadphase*, %class.btDispatcher*)* @_ZN16btDbvtBroadphase25calculateOverlappingPairsEP12btDispatcher to i8*), i8* bitcast (%class.btOverlappingPairCache* (%struct.btDbvtBroadphase*)* @_ZN16btDbvtBroadphase23getOverlappingPairCacheEv to i8*), i8* bitcast (%class.btOverlappingPairCache* (%struct.btDbvtBroadphase*)* @_ZNK16btDbvtBroadphase23getOverlappingPairCacheEv to i8*), i8* bitcast (void (%struct.btDbvtBroadphase*, %class.btVector3*, %class.btVector3*)* @_ZNK16btDbvtBroadphase17getBroadphaseAabbER9btVector3S1_ to i8*), i8* bitcast (void (%struct.btDbvtBroadphase*, %class.btDispatcher*)* @_ZN16btDbvtBroadphase9resetPoolEP12btDispatcher to i8*), i8* bitcast (void (%struct.btDbvtBroadphase*)* @_ZN16btDbvtBroadphase10printStatsEv to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS16btDbvtBroadphase = hidden constant [19 x i8] c"16btDbvtBroadphase\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS21btBroadphaseInterface = linkonce_odr hidden constant [24 x i8] c"21btBroadphaseInterface\00", comdat, align 1
@_ZTI21btBroadphaseInterface = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btBroadphaseInterface, i32 0, i32 0) }, comdat, align 4
@_ZTI16btDbvtBroadphase = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btDbvtBroadphase, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI21btBroadphaseInterface to i8*) }, align 4
@_ZTV21btBroadphaseInterface = linkonce_odr hidden unnamed_addr constant { [16 x i8*] } { [16 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI21btBroadphaseInterface to i8*), i8* bitcast (%class.btBroadphaseInterface* (%class.btBroadphaseInterface*)* @_ZN21btBroadphaseInterfaceD2Ev to i8*), i8* bitcast (void (%class.btBroadphaseInterface*)* @_ZN21btBroadphaseInterfaceD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btBroadphaseInterface*, %class.btDispatcher*)* @_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV18btDbvtTreeCollider = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI18btDbvtTreeCollider to i8*), i8* bitcast (%struct.btDbvtTreeCollider* (%struct.btDbvtTreeCollider*)* @_ZN18btDbvtTreeColliderD2Ev to i8*), i8* bitcast (void (%struct.btDbvtTreeCollider*)* @_ZN18btDbvtTreeColliderD0Ev to i8*), i8* bitcast (void (%struct.btDbvtTreeCollider*, %struct.btDbvtNode*, %struct.btDbvtNode*)* @_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNodeS2_ to i8*), i8* bitcast (void (%struct.btDbvtTreeCollider*, %struct.btDbvtNode*)* @_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNode to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, float)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode to i8*)] }, comdat, align 4
@_ZTS18btDbvtTreeCollider = linkonce_odr hidden constant [21 x i8] c"18btDbvtTreeCollider\00", comdat, align 1
@_ZTSN6btDbvt8ICollideE = linkonce_odr hidden constant [19 x i8] c"N6btDbvt8ICollideE\00", comdat, align 1
@_ZTIN6btDbvt8ICollideE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTSN6btDbvt8ICollideE, i32 0, i32 0) }, comdat, align 4
@_ZTI18btDbvtTreeCollider = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @_ZTS18btDbvtTreeCollider, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN6btDbvt8ICollideE to i8*) }, comdat, align 4
@_ZTVN6btDbvt8ICollideE = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN6btDbvt8ICollideE to i8*), i8* bitcast (%"struct.btDbvt::ICollide"* (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD2Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*)* @_ZN6btDbvt8ICollideD0Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, float)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode to i8*)] }, comdat, align 4
@_ZTV19BroadphaseRayTester = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI19BroadphaseRayTester to i8*), i8* bitcast (%struct.BroadphaseRayTester* (%struct.BroadphaseRayTester*)* @_ZN19BroadphaseRayTesterD2Ev to i8*), i8* bitcast (void (%struct.BroadphaseRayTester*)* @_ZN19BroadphaseRayTesterD0Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ to i8*), i8* bitcast (void (%struct.BroadphaseRayTester*, %struct.btDbvtNode*)* @_ZN19BroadphaseRayTester7ProcessEPK10btDbvtNode to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, float)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode to i8*)] }, comdat, align 4
@_ZTS19BroadphaseRayTester = linkonce_odr hidden constant [22 x i8] c"19BroadphaseRayTester\00", comdat, align 1
@_ZTI19BroadphaseRayTester = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([22 x i8], [22 x i8]* @_ZTS19BroadphaseRayTester, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN6btDbvt8ICollideE to i8*) }, comdat, align 4
@_ZTV20BroadphaseAabbTester = linkonce_odr hidden unnamed_addr constant { [9 x i8*] } { [9 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI20BroadphaseAabbTester to i8*), i8* bitcast (%struct.BroadphaseAabbTester* (%struct.BroadphaseAabbTester*)* @_ZN20BroadphaseAabbTesterD2Ev to i8*), i8* bitcast (void (%struct.BroadphaseAabbTester*)* @_ZN20BroadphaseAabbTesterD0Ev to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_ to i8*), i8* bitcast (void (%struct.BroadphaseAabbTester*, %struct.btDbvtNode*)* @_ZN20BroadphaseAabbTester7ProcessEPK10btDbvtNode to i8*), i8* bitcast (void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, float)* @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode to i8*), i8* bitcast (i1 (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)* @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode to i8*)] }, comdat, align 4
@_ZTS20BroadphaseAabbTester = linkonce_odr hidden constant [23 x i8] c"20BroadphaseAabbTester\00", comdat, align 1
@_ZTI20BroadphaseAabbTester = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([23 x i8], [23 x i8]* @_ZTS20BroadphaseAabbTester, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTIN6btDbvt8ICollideE to i8*) }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btDbvtBroadphase.cpp, i8* null }]

@_ZN16btDbvtBroadphaseC1EP22btOverlappingPairCache = hidden unnamed_addr alias %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*, %class.btOverlappingPairCache*), %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*, %class.btOverlappingPairCache*)* @_ZN16btDbvtBroadphaseC2EP22btOverlappingPairCache
@_ZN16btDbvtBroadphaseD1Ev = hidden unnamed_addr alias %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*), %struct.btDbvtBroadphase* (%struct.btDbvtBroadphase*)* @_ZN16btDbvtBroadphaseD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %struct.btDbvtBroadphase* @_ZN16btDbvtBroadphaseC2EP22btOverlappingPairCache(%struct.btDbvtBroadphase* returned %this, %class.btOverlappingPairCache* %paircache) unnamed_addr #2 {
entry:
  %retval = alloca %struct.btDbvtBroadphase*, align 4
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  %paircache.addr = alloca %class.btOverlappingPairCache*, align 4
  %saved-rvalue = alloca i8*, align 4
  %saved-rvalue6 = alloca i8*, align 4
  %cleanup.cond = alloca i1, align 1
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btAlignedObjectArray.4, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  store %class.btOverlappingPairCache* %paircache, %class.btOverlappingPairCache** %paircache.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  store %struct.btDbvtBroadphase* %this1, %struct.btDbvtBroadphase** %retval, align 4
  %0 = bitcast %struct.btDbvtBroadphase* %this1 to %class.btBroadphaseInterface*
  %call = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceC2Ev(%class.btBroadphaseInterface* %0) #8
  %1 = bitcast %struct.btDbvtBroadphase* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV16btDbvtBroadphase, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_sets = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %array.begin, i32 2
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %struct.btDbvt* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %struct.btDbvt* @_ZN6btDbvtC1Ev(%struct.btDbvt* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %struct.btDbvt* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_rayTestStacks = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 21
  %call3 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEEC2Ev(%class.btAlignedObjectArray.1* %m_rayTestStacks)
  %m_deferedcollide = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 18
  store i8 0, i8* %m_deferedcollide, align 1
  %m_needcleanup = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 19
  store i8 1, i8* %m_needcleanup, align 2
  %2 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %paircache.addr, align 4
  %cmp = icmp ne %class.btOverlappingPairCache* %2, null
  %3 = zext i1 %cmp to i64
  %cond = select i1 %cmp, i1 false, i1 true
  %m_releasepaircache = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 17
  %frombool = zext i1 %cond to i8
  store i8 %frombool, i8* %m_releasepaircache, align 4
  %m_prediction = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 4
  store float 0.000000e+00, float* %m_prediction, align 4
  %m_stageCurrent = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 5
  store i32 0, i32* %m_stageCurrent, align 4
  %m_fixedleft = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 10
  store i32 0, i32* %m_fixedleft, align 4
  %m_fupdates = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 6
  store i32 1, i32* %m_fupdates, align 4
  %m_dupdates = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 7
  store i32 0, i32* %m_dupdates, align 4
  %m_cupdates = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 8
  store i32 10, i32* %m_cupdates, align 4
  %m_newpairs = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 9
  store i32 1, i32* %m_newpairs, align 4
  %m_updates_call = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 11
  store i32 0, i32* %m_updates_call, align 4
  %m_updates_done = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 12
  store i32 0, i32* %m_updates_done, align 4
  %m_updates_ratio = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 13
  store float 0.000000e+00, float* %m_updates_ratio, align 4
  %4 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %paircache.addr, align 4
  %tobool = icmp ne %class.btOverlappingPairCache* %4, null
  store i1 false, i1* %cleanup.cond, align 1
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %arrayctor.cont
  %5 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %paircache.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %arrayctor.cont
  %call4 = call i8* @_Z22btAlignedAllocInternalmi(i32 72, i32 16)
  %call5 = call i8* @_ZN28btHashedOverlappingPairCachenwEmPv(i32 72, i8* %call4)
  store i8* %call5, i8** %saved-rvalue, align 4
  store i8* %call4, i8** %saved-rvalue6, align 4
  store i1 true, i1* %cleanup.cond, align 1
  %6 = bitcast i8* %call5 to %class.btHashedOverlappingPairCache*
  %call7 = call %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC1Ev(%class.btHashedOverlappingPairCache* %6)
  %7 = bitcast %class.btHashedOverlappingPairCache* %6 to %class.btOverlappingPairCache*
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond8 = phi %class.btOverlappingPairCache* [ %5, %cond.true ], [ %7, %cond.false ]
  %m_paircache = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 3
  store %class.btOverlappingPairCache* %cond8, %class.btOverlappingPairCache** %m_paircache, align 4
  %m_gid = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 16
  store i32 0, i32* %m_gid, align 4
  %m_pid = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 14
  store i32 0, i32* %m_pid, align 4
  %m_cid = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 15
  store i32 0, i32* %m_cid, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end
  %8 = load i32, i32* %i, align 4
  %cmp9 = icmp sle i32 %8, 2
  br i1 %cmp9, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_stageRoots = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 2
  %9 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [3 x %struct.btDbvtProxy*], [3 x %struct.btDbvtProxy*]* %m_stageRoots, i32 0, i32 %9
  store %struct.btDbvtProxy* null, %struct.btDbvtProxy** %arrayidx, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_rayTestStacks10 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 21
  %call11 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2Ev(%class.btAlignedObjectArray.4* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE6resizeEiRKS3_(%class.btAlignedObjectArray.1* %m_rayTestStacks10, i32 1, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %ref.tmp)
  %call12 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev(%class.btAlignedObjectArray.4* %ref.tmp) #8
  %11 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %retval, align 4
  ret %struct.btDbvtBroadphase* %11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceC2Ev(%class.btBroadphaseInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  %0 = bitcast %class.btBroadphaseInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV21btBroadphaseInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btBroadphaseInterface* %this1
}

declare %struct.btDbvt* @_ZN6btDbvtC1Ev(%struct.btDbvt* returned) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEEC2Ev(%class.btAlignedObjectArray.1* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.2* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIPK10btDbvtNodeELj16EEC2Ev(%class.btAlignedAllocator.2* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE4initEv(%class.btAlignedObjectArray.1* %this1)
  ret %class.btAlignedObjectArray.1* %this1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN28btHashedOverlappingPairCachenwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

declare %class.btHashedOverlappingPairCache* @_ZN28btHashedOverlappingPairCacheC1Ev(%class.btHashedOverlappingPairCache* returned) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE6resizeEiRKS3_(%class.btAlignedObjectArray.1* %this, i32 %newsize, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i6 = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btAlignedObjectArray.4* %fillData, %class.btAlignedObjectArray.4** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIS_IPK10btDbvtNodeEE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %5 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %5, i32 %6
  %call3 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev(%class.btAlignedObjectArray.4* %arrayidx) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end16

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp4 = icmp sgt i32 %8, %9
  br i1 %cmp4, label %if.then5, label %if.end

if.then5:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE7reserveEi(%class.btAlignedObjectArray.1* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then5, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i6, align 4
  br label %for.cond7

for.cond7:                                        ; preds = %for.inc13, %if.end
  %12 = load i32, i32* %i6, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp8 = icmp slt i32 %12, %13
  br i1 %cmp8, label %for.body9, label %for.end15

for.body9:                                        ; preds = %for.cond7
  %m_data10 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %14 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %m_data10, align 4
  %15 = load i32, i32* %i6, align 4
  %arrayidx11 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %14, i32 %15
  %16 = bitcast %class.btAlignedObjectArray.4* %arrayidx11 to i8*
  %17 = bitcast i8* %16 to %class.btAlignedObjectArray.4*
  %18 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %fillData.addr, align 4
  %call12 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2ERKS3_(%class.btAlignedObjectArray.4* %17, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %18)
  br label %for.inc13

for.inc13:                                        ; preds = %for.body9
  %19 = load i32, i32* %i6, align 4
  %inc14 = add nsw i32 %19, 1
  store i32 %inc14, i32* %i6, align 4
  br label %for.cond7

for.end15:                                        ; preds = %for.cond7
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %20 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  store i32 %20, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %struct.btDbvtBroadphase* @_ZN16btDbvtBroadphaseD2Ev(%struct.btDbvtBroadphase* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %struct.btDbvtBroadphase*, align 4
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  store %struct.btDbvtBroadphase* %this1, %struct.btDbvtBroadphase** %retval, align 4
  %0 = bitcast %struct.btDbvtBroadphase* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [16 x i8*] }, { [16 x i8*] }* @_ZTV16btDbvtBroadphase, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_releasepaircache = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 17
  %1 = load i8, i8* %m_releasepaircache, align 4
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_paircache = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 3
  %2 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_paircache, align 4
  %3 = bitcast %class.btOverlappingPairCache* %2 to %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)***
  %vtable = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)**, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*** %3, align 4
  %vfn = getelementptr inbounds %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vtable, i64 0
  %4 = load %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)*, %class.btOverlappingPairCache* (%class.btOverlappingPairCache*)** %vfn, align 4
  %call = call %class.btOverlappingPairCache* %4(%class.btOverlappingPairCache* %2) #8
  %m_paircache2 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 3
  %5 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_paircache2, align 4
  %6 = bitcast %class.btOverlappingPairCache* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_rayTestStacks = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 21
  %call3 = call %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEED2Ev(%class.btAlignedObjectArray.1* %m_rayTestStacks) #8
  %m_sets = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets, i32 0, i32 0
  %7 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %array.begin, i32 2
  br label %arraydestroy.body

arraydestroy.body:                                ; preds = %arraydestroy.body, %if.end
  %arraydestroy.elementPast = phi %struct.btDbvt* [ %7, %if.end ], [ %arraydestroy.element, %arraydestroy.body ]
  %arraydestroy.element = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arraydestroy.elementPast, i32 -1
  %call4 = call %struct.btDbvt* @_ZN6btDbvtD1Ev(%struct.btDbvt* %arraydestroy.element) #8
  %arraydestroy.done = icmp eq %struct.btDbvt* %arraydestroy.element, %array.begin
  br i1 %arraydestroy.done, label %arraydestroy.done5, label %arraydestroy.body

arraydestroy.done5:                               ; preds = %arraydestroy.body
  %8 = bitcast %struct.btDbvtBroadphase* %this1 to %class.btBroadphaseInterface*
  %call6 = call %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceD2Ev(%class.btBroadphaseInterface* %8) #8
  %9 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %retval, align 4
  ret %struct.btDbvtBroadphase* %9
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.1* @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEED2Ev(%class.btAlignedObjectArray.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE5clearEv(%class.btAlignedObjectArray.1* %this1)
  ret %class.btAlignedObjectArray.1* %this1
}

; Function Attrs: nounwind
declare %struct.btDbvt* @_ZN6btDbvtD1Ev(%struct.btDbvt* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN16btDbvtBroadphaseD0Ev(%struct.btDbvtBroadphase* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %call = call %struct.btDbvtBroadphase* @_ZN16btDbvtBroadphaseD1Ev(%struct.btDbvtBroadphase* %this1) #8
  %0 = bitcast %struct.btDbvtBroadphase* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline optnone
define hidden %struct.btBroadphaseProxy* @_ZN16btDbvtBroadphase11createProxyERK9btVector3S2_iPviiP12btDispatcher(%struct.btDbvtBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i32 %0, i8* %userPtr, i32 %collisionFilterGroup, i32 %collisionFilterMask, %class.btDispatcher* %1) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %.addr = alloca i32, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i32, align 4
  %collisionFilterMask.addr = alloca i32, align 4
  %.addr1 = alloca %class.btDispatcher*, align 4
  %proxy = alloca %struct.btDbvtProxy*, align 4
  %aabb = alloca %struct.btDbvtAabbMm, align 4
  %collider = alloca %struct.btDbvtTreeCollider, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %userPtr, i8** %userPtr.addr, align 4
  store i32 %collisionFilterGroup, i32* %collisionFilterGroup.addr, align 4
  store i32 %collisionFilterMask, i32* %collisionFilterMask.addr, align 4
  store %class.btDispatcher* %1, %class.btDispatcher** %.addr1, align 4
  %this2 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 64, i32 16)
  %call3 = call i8* @_ZN17btBroadphaseProxynwEmPv(i32 64, i8* %call)
  %2 = bitcast i8* %call3 to %struct.btDbvtProxy*
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %5 = load i8*, i8** %userPtr.addr, align 4
  %6 = load i32, i32* %collisionFilterGroup.addr, align 4
  %7 = load i32, i32* %collisionFilterMask.addr, align 4
  %call4 = call %struct.btDbvtProxy* @_ZN11btDbvtProxyC2ERK9btVector3S2_Pvii(%struct.btDbvtProxy* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, i8* %5, i32 %6, i32 %7)
  store %struct.btDbvtProxy* %2, %struct.btDbvtProxy** %proxy, align 4
  %8 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %9 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %aabb, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %m_stageCurrent = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this2, i32 0, i32 5
  %10 = load i32, i32* %m_stageCurrent, align 4
  %11 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %stage = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %11, i32 0, i32 3
  store i32 %10, i32* %stage, align 4
  %m_gid = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this2, i32 0, i32 16
  %12 = load i32, i32* %m_gid, align 4
  %inc = add nsw i32 %12, 1
  store i32 %inc, i32* %m_gid, align 4
  %13 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %14 = bitcast %struct.btDbvtProxy* %13 to %struct.btBroadphaseProxy*
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %14, i32 0, i32 3
  store i32 %inc, i32* %m_uniqueId, align 4
  %m_sets = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this2, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets, i32 0, i32 0
  %15 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %16 = bitcast %struct.btDbvtProxy* %15 to i8*
  %call5 = call %struct.btDbvtNode* @_ZN6btDbvt6insertERK12btDbvtAabbMmPv(%struct.btDbvt* %arrayidx, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %aabb, i8* %16)
  %17 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %17, i32 0, i32 1
  store %struct.btDbvtNode* %call5, %struct.btDbvtNode** %leaf, align 4
  %18 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %m_stageRoots = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this2, i32 0, i32 2
  %m_stageCurrent6 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this2, i32 0, i32 5
  %19 = load i32, i32* %m_stageCurrent6, align 4
  %arrayidx7 = getelementptr inbounds [3 x %struct.btDbvtProxy*], [3 x %struct.btDbvtProxy*]* %m_stageRoots, i32 0, i32 %19
  call void @_ZL10listappendI11btDbvtProxyEvPT_RS2_(%struct.btDbvtProxy* %18, %struct.btDbvtProxy** nonnull align 4 dereferenceable(4) %arrayidx7)
  %m_deferedcollide = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this2, i32 0, i32 18
  %20 = load i8, i8* %m_deferedcollide, align 1
  %tobool = trunc i8 %20 to i1
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %call8 = call %struct.btDbvtTreeCollider* @_ZN18btDbvtTreeColliderC2EP16btDbvtBroadphase(%struct.btDbvtTreeCollider* %collider, %struct.btDbvtBroadphase* %this2)
  %21 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %proxy9 = getelementptr inbounds %struct.btDbvtTreeCollider, %struct.btDbvtTreeCollider* %collider, i32 0, i32 2
  store %struct.btDbvtProxy* %21, %struct.btDbvtProxy** %proxy9, align 4
  %m_sets10 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this2, i32 0, i32 1
  %arrayidx11 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets10, i32 0, i32 0
  %m_sets12 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this2, i32 0, i32 1
  %arrayidx13 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets12, i32 0, i32 0
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx13, i32 0, i32 0
  %22 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %23 = bitcast %struct.btDbvtTreeCollider* %collider to %"struct.btDbvt::ICollide"*
  call void @_ZNK6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE(%struct.btDbvt* %arrayidx11, %struct.btDbvtNode* %22, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %aabb, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %23)
  %m_sets14 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this2, i32 0, i32 1
  %arrayidx15 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets14, i32 0, i32 1
  %m_sets16 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this2, i32 0, i32 1
  %arrayidx17 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets16, i32 0, i32 1
  %m_root18 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx17, i32 0, i32 0
  %24 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root18, align 4
  %25 = bitcast %struct.btDbvtTreeCollider* %collider to %"struct.btDbvt::ICollide"*
  call void @_ZNK6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE(%struct.btDbvt* %arrayidx15, %struct.btDbvtNode* %24, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %aabb, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %25)
  %call19 = call %struct.btDbvtTreeCollider* @_ZN18btDbvtTreeColliderD2Ev(%struct.btDbvtTreeCollider* %collider) #8
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %26 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %27 = bitcast %struct.btDbvtProxy* %26 to %struct.btBroadphaseProxy*
  ret %struct.btBroadphaseProxy* %27
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN17btBroadphaseProxynwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btDbvtProxy* @_ZN11btDbvtProxyC2ERK9btVector3S2_Pvii(%struct.btDbvtProxy* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i8* %userPtr, i32 %collisionFilterGroup, i32 %collisionFilterMask) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i32, align 4
  %collisionFilterMask.addr = alloca i32, align 4
  store %struct.btDbvtProxy* %this, %struct.btDbvtProxy** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store i8* %userPtr, i8** %userPtr.addr, align 4
  store i32 %collisionFilterGroup, i32* %collisionFilterGroup.addr, align 4
  store i32 %collisionFilterMask, i32* %collisionFilterMask.addr, align 4
  %this1 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %this.addr, align 4
  %0 = bitcast %struct.btDbvtProxy* %this1 to %struct.btBroadphaseProxy*
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %3 = load i8*, i8** %userPtr.addr, align 4
  %4 = load i32, i32* %collisionFilterGroup.addr, align 4
  %5 = load i32, i32* %collisionFilterMask.addr, align 4
  %call = call %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2ERK9btVector3S2_Pvii(%struct.btBroadphaseProxy* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i8* %3, i32 %4, i32 %5)
  %links = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %this1, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links, i32 0, i32 1
  store %struct.btDbvtProxy* null, %struct.btDbvtProxy** %arrayidx, align 4
  %links2 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %this1, i32 0, i32 2
  %arrayidx3 = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links2, i32 0, i32 0
  store %struct.btDbvtProxy* null, %struct.btDbvtProxy** %arrayidx3, align 4
  ret %struct.btDbvtProxy* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %mi, %class.btVector3* nonnull align 4 dereferenceable(16) %mx) #2 comdat {
entry:
  %mi.addr = alloca %class.btVector3*, align 4
  %mx.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %mi, %class.btVector3** %mi.addr, align 4
  store %class.btVector3* %mx, %class.btVector3** %mx.addr, align 4
  %call = call %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* %agg.result)
  %0 = load %class.btVector3*, %class.btVector3** %mi.addr, align 4
  %mi1 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 0
  %1 = bitcast %class.btVector3* %mi1 to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %mx.addr, align 4
  %mx2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 1
  %4 = bitcast %class.btVector3* %mx2 to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  ret void
}

declare %struct.btDbvtNode* @_ZN6btDbvt6insertERK12btDbvtAabbMmPv(%struct.btDbvt*, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32), i8*) #3

; Function Attrs: noinline nounwind optnone
define internal void @_ZL10listappendI11btDbvtProxyEvPT_RS2_(%struct.btDbvtProxy* %item, %struct.btDbvtProxy** nonnull align 4 dereferenceable(4) %list) #1 {
entry:
  %item.addr = alloca %struct.btDbvtProxy*, align 4
  %list.addr = alloca %struct.btDbvtProxy**, align 4
  store %struct.btDbvtProxy* %item, %struct.btDbvtProxy** %item.addr, align 4
  store %struct.btDbvtProxy** %list, %struct.btDbvtProxy*** %list.addr, align 4
  %0 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %item.addr, align 4
  %links = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %0, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links, i32 0, i32 0
  store %struct.btDbvtProxy* null, %struct.btDbvtProxy** %arrayidx, align 4
  %1 = load %struct.btDbvtProxy**, %struct.btDbvtProxy*** %list.addr, align 4
  %2 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %1, align 4
  %3 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %item.addr, align 4
  %links1 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %3, i32 0, i32 2
  %arrayidx2 = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links1, i32 0, i32 1
  store %struct.btDbvtProxy* %2, %struct.btDbvtProxy** %arrayidx2, align 4
  %4 = load %struct.btDbvtProxy**, %struct.btDbvtProxy*** %list.addr, align 4
  %5 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %4, align 4
  %tobool = icmp ne %struct.btDbvtProxy* %5, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %item.addr, align 4
  %7 = load %struct.btDbvtProxy**, %struct.btDbvtProxy*** %list.addr, align 4
  %8 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %7, align 4
  %links3 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %8, i32 0, i32 2
  %arrayidx4 = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links3, i32 0, i32 0
  store %struct.btDbvtProxy* %6, %struct.btDbvtProxy** %arrayidx4, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %9 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %item.addr, align 4
  %10 = load %struct.btDbvtProxy**, %struct.btDbvtProxy*** %list.addr, align 4
  store %struct.btDbvtProxy* %9, %struct.btDbvtProxy** %10, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDbvtTreeCollider* @_ZN18btDbvtTreeColliderC2EP16btDbvtBroadphase(%struct.btDbvtTreeCollider* returned %this, %struct.btDbvtBroadphase* %p) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtTreeCollider*, align 4
  %p.addr = alloca %struct.btDbvtBroadphase*, align 4
  store %struct.btDbvtTreeCollider* %this, %struct.btDbvtTreeCollider** %this.addr, align 4
  store %struct.btDbvtBroadphase* %p, %struct.btDbvtBroadphase** %p.addr, align 4
  %this1 = load %struct.btDbvtTreeCollider*, %struct.btDbvtTreeCollider** %this.addr, align 4
  %0 = bitcast %struct.btDbvtTreeCollider* %this1 to %"struct.btDbvt::ICollide"*
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideC2Ev(%"struct.btDbvt::ICollide"* %0) #8
  %1 = bitcast %struct.btDbvtTreeCollider* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV18btDbvtTreeCollider, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %pbp = getelementptr inbounds %struct.btDbvtTreeCollider, %struct.btDbvtTreeCollider* %this1, i32 0, i32 1
  %2 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %p.addr, align 4
  store %struct.btDbvtBroadphase* %2, %struct.btDbvtBroadphase** %pbp, align 4
  ret %struct.btDbvtTreeCollider* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE(%struct.btDbvt* %this, %struct.btDbvtNode* %root, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %vol, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %policy) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  %root.addr = alloca %struct.btDbvtNode*, align 4
  %vol.addr = alloca %struct.btDbvtAabbMm*, align 4
  %policy.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %volume = alloca %struct.btDbvtAabbMm, align 4
  %stack = alloca %class.btAlignedObjectArray.4, align 4
  %ref.tmp = alloca %struct.btDbvtNode*, align 4
  %n = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store %struct.btDbvtNode* %root, %struct.btDbvtNode** %root.addr, align 4
  store %struct.btDbvtAabbMm* %vol, %struct.btDbvtAabbMm** %vol.addr, align 4
  store %"struct.btDbvt::ICollide"* %policy, %"struct.btDbvt::ICollide"** %policy.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %tobool = icmp ne %struct.btDbvtNode* %0, null
  br i1 %tobool, label %if.then, label %if.end14

if.then:                                          ; preds = %entry
  %1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %vol.addr, align 4
  %2 = bitcast %struct.btDbvtAabbMm* %volume to i8*
  %3 = bitcast %struct.btDbvtAabbMm* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 32, i1 false)
  %call = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2Ev(%class.btAlignedObjectArray.4* %stack)
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE6resizeEiRKS2_(%class.btAlignedObjectArray.4* %stack, i32 0, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %ref.tmp)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.4* %stack, i32 64)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray.4* %stack, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %root.addr)
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.4* %stack)
  %sub = sub nsw i32 %call2, 1
  %call3 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi(%class.btAlignedObjectArray.4* %stack, i32 %sub)
  %4 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call3, align 4
  store %struct.btDbvtNode* %4, %struct.btDbvtNode** %n, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8pop_backEv(%class.btAlignedObjectArray.4* %stack)
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %volume4 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %5, i32 0, i32 0
  %call5 = call zeroext i1 @_Z9IntersectRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume4, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume)
  br i1 %call5, label %if.then6, label %if.end11

if.then6:                                         ; preds = %do.body
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %call7 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %6)
  br i1 %call7, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.then6
  %7 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %8 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %7, i32 0, i32 2
  %childs = bitcast %union.anon.0* %8 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray.4* %stack, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %arrayidx)
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %10 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %9, i32 0, i32 2
  %childs9 = bitcast %union.anon.0* %10 to [2 x %struct.btDbvtNode*]*
  %arrayidx10 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs9, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray.4* %stack, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %arrayidx10)
  br label %if.end

if.else:                                          ; preds = %if.then6
  %11 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %policy.addr, align 4
  %12 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n, align 4
  %13 = bitcast %"struct.btDbvt::ICollide"* %11 to void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)***
  %vtable = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)**, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*** %13, align 4
  %vfn = getelementptr inbounds void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vtable, i64 3
  %14 = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vfn, align 4
  call void %14(%"struct.btDbvt::ICollide"* %11, %struct.btDbvtNode* %12)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then8
  br label %if.end11

if.end11:                                         ; preds = %if.end, %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end11
  %call12 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.4* %stack)
  %cmp = icmp sgt i32 %call12, 0
  br i1 %cmp, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %call13 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev(%class.btAlignedObjectArray.4* %stack) #8
  br label %if.end14

if.end14:                                         ; preds = %do.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDbvtTreeCollider* @_ZN18btDbvtTreeColliderD2Ev(%struct.btDbvtTreeCollider* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtTreeCollider*, align 4
  store %struct.btDbvtTreeCollider* %this, %struct.btDbvtTreeCollider** %this.addr, align 4
  %this1 = load %struct.btDbvtTreeCollider*, %struct.btDbvtTreeCollider** %this.addr, align 4
  %0 = bitcast %struct.btDbvtTreeCollider* %this1 to %"struct.btDbvt::ICollide"*
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* %0) #8
  ret %struct.btDbvtTreeCollider* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btDbvtBroadphase12destroyProxyEP17btBroadphaseProxyP12btDispatcher(%struct.btDbvtBroadphase* %this, %struct.btBroadphaseProxy* %absproxy, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  %absproxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %proxy = alloca %struct.btDbvtProxy*, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  store %struct.btBroadphaseProxy* %absproxy, %struct.btBroadphaseProxy** %absproxy.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %absproxy.addr, align 4
  %1 = bitcast %struct.btBroadphaseProxy* %0 to %struct.btDbvtProxy*
  store %struct.btDbvtProxy* %1, %struct.btDbvtProxy** %proxy, align 4
  %2 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %stage = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %2, i32 0, i32 3
  %3 = load i32, i32* %stage, align 4
  %cmp = icmp eq i32 %3, 2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_sets = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets, i32 0, i32 1
  %4 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %4, i32 0, i32 1
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf, align 4
  call void @_ZN6btDbvt6removeEP10btDbvtNode(%struct.btDbvt* %arrayidx, %struct.btDbvtNode* %5)
  br label %if.end

if.else:                                          ; preds = %entry
  %m_sets2 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets2, i32 0, i32 0
  %6 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf4 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %6, i32 0, i32 1
  %7 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf4, align 4
  call void @_ZN6btDbvt6removeEP10btDbvtNode(%struct.btDbvt* %arrayidx3, %struct.btDbvtNode* %7)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %8 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %m_stageRoots = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 2
  %9 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %stage5 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %9, i32 0, i32 3
  %10 = load i32, i32* %stage5, align 4
  %arrayidx6 = getelementptr inbounds [3 x %struct.btDbvtProxy*], [3 x %struct.btDbvtProxy*]* %m_stageRoots, i32 0, i32 %10
  call void @_ZL10listremoveI11btDbvtProxyEvPT_RS2_(%struct.btDbvtProxy* %8, %struct.btDbvtProxy** nonnull align 4 dereferenceable(4) %arrayidx6)
  %m_paircache = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 3
  %11 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_paircache, align 4
  %12 = bitcast %class.btOverlappingPairCache* %11 to %class.btOverlappingPairCallback*
  %13 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %14 = bitcast %struct.btDbvtProxy* %13 to %struct.btBroadphaseProxy*
  %15 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %16 = bitcast %class.btOverlappingPairCallback* %12 to void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable = load void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %16, align 4
  %vfn = getelementptr inbounds void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable, i64 4
  %17 = load void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, void (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn, align 4
  call void %17(%class.btOverlappingPairCallback* %12, %struct.btBroadphaseProxy* %14, %class.btDispatcher* %15)
  %18 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %19 = bitcast %struct.btDbvtProxy* %18 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %19)
  %m_needcleanup = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 19
  store i8 1, i8* %m_needcleanup, align 2
  ret void
}

declare void @_ZN6btDbvt6removeEP10btDbvtNode(%struct.btDbvt*, %struct.btDbvtNode*) #3

; Function Attrs: noinline nounwind optnone
define internal void @_ZL10listremoveI11btDbvtProxyEvPT_RS2_(%struct.btDbvtProxy* %item, %struct.btDbvtProxy** nonnull align 4 dereferenceable(4) %list) #1 {
entry:
  %item.addr = alloca %struct.btDbvtProxy*, align 4
  %list.addr = alloca %struct.btDbvtProxy**, align 4
  store %struct.btDbvtProxy* %item, %struct.btDbvtProxy** %item.addr, align 4
  store %struct.btDbvtProxy** %list, %struct.btDbvtProxy*** %list.addr, align 4
  %0 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %item.addr, align 4
  %links = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %0, i32 0, i32 2
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links, i32 0, i32 0
  %1 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %arrayidx, align 4
  %tobool = icmp ne %struct.btDbvtProxy* %1, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %item.addr, align 4
  %links1 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %2, i32 0, i32 2
  %arrayidx2 = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links1, i32 0, i32 1
  %3 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %arrayidx2, align 4
  %4 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %item.addr, align 4
  %links3 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %4, i32 0, i32 2
  %arrayidx4 = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links3, i32 0, i32 0
  %5 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %arrayidx4, align 4
  %links5 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %5, i32 0, i32 2
  %arrayidx6 = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links5, i32 0, i32 1
  store %struct.btDbvtProxy* %3, %struct.btDbvtProxy** %arrayidx6, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %item.addr, align 4
  %links7 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %6, i32 0, i32 2
  %arrayidx8 = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links7, i32 0, i32 1
  %7 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %arrayidx8, align 4
  %8 = load %struct.btDbvtProxy**, %struct.btDbvtProxy*** %list.addr, align 4
  store %struct.btDbvtProxy* %7, %struct.btDbvtProxy** %8, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %9 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %item.addr, align 4
  %links9 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %9, i32 0, i32 2
  %arrayidx10 = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links9, i32 0, i32 1
  %10 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %arrayidx10, align 4
  %tobool11 = icmp ne %struct.btDbvtProxy* %10, null
  br i1 %tobool11, label %if.then12, label %if.end19

if.then12:                                        ; preds = %if.end
  %11 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %item.addr, align 4
  %links13 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %11, i32 0, i32 2
  %arrayidx14 = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links13, i32 0, i32 0
  %12 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %arrayidx14, align 4
  %13 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %item.addr, align 4
  %links15 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %13, i32 0, i32 2
  %arrayidx16 = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links15, i32 0, i32 1
  %14 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %arrayidx16, align 4
  %links17 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %14, i32 0, i32 2
  %arrayidx18 = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links17, i32 0, i32 0
  store %struct.btDbvtProxy* %12, %struct.btDbvtProxy** %arrayidx18, align 4
  br label %if.end19

if.end19:                                         ; preds = %if.then12, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK16btDbvtBroadphase7getAabbEP17btBroadphaseProxyR9btVector3S3_(%struct.btDbvtBroadphase* %this, %struct.btBroadphaseProxy* %absproxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  %absproxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %proxy = alloca %struct.btDbvtProxy*, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  store %struct.btBroadphaseProxy* %absproxy, %struct.btBroadphaseProxy** %absproxy.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %absproxy.addr, align 4
  %1 = bitcast %struct.btBroadphaseProxy* %0 to %struct.btDbvtProxy*
  store %struct.btDbvtProxy* %1, %struct.btDbvtProxy** %proxy, align 4
  %2 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %3 = bitcast %struct.btDbvtProxy* %2 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %3, i32 0, i32 4
  %4 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %m_aabbMin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %7 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %8 = bitcast %struct.btDbvtProxy* %7 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 5
  %9 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %10 = bitcast %class.btVector3* %9 to i8*
  %11 = bitcast %class.btVector3* %m_aabbMax to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: noinline optnone
define hidden void @_ZN16btDbvtBroadphase7rayTestERK9btVector3S2_R23btBroadphaseRayCallbackS2_S2_(%struct.btDbvtBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTo, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %rayCallback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayTo.addr = alloca %class.btVector3*, align 4
  %rayCallback.addr = alloca %struct.btBroadphaseRayCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %callback = alloca %struct.BroadphaseRayTester, align 4
  %stack = alloca %class.btAlignedObjectArray.4*, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4
  store %class.btVector3* %rayTo, %class.btVector3** %rayTo.addr, align 4
  store %struct.btBroadphaseRayCallback* %rayCallback, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %0 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  %call = call %struct.BroadphaseRayTester* @_ZN19BroadphaseRayTesterC2ER23btBroadphaseRayCallback(%struct.BroadphaseRayTester* %callback, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %0)
  %m_rayTestStacks = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 21
  %call2 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEEixEi(%class.btAlignedObjectArray.1* %m_rayTestStacks, i32 0)
  store %class.btAlignedObjectArray.4* %call2, %class.btAlignedObjectArray.4** %stack, align 4
  %m_sets = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets, i32 0, i32 0
  %m_sets3 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx4 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets3, i32 0, i32 0
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx4, i32 0, i32 0
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %2 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %rayTo.addr, align 4
  %4 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  %m_rayDirectionInverse = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %4, i32 0, i32 1
  %5 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  %m_signs = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %5, i32 0, i32 2
  %arraydecay = getelementptr inbounds [3 x i32], [3 x i32]* %m_signs, i32 0, i32 0
  %6 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  %m_lambda_max = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %6, i32 0, i32 3
  %7 = load float, float* %m_lambda_max, align 4
  %8 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %9 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %10 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %stack, align 4
  %11 = bitcast %struct.BroadphaseRayTester* %callback to %"struct.btDbvt::ICollide"*
  call void @_ZNK6btDbvt15rayTestInternalEPK10btDbvtNodeRK9btVector3S5_S5_PjfS5_S5_R20btAlignedObjectArrayIS2_ERNS_8ICollideE(%struct.btDbvt* %arrayidx, %struct.btDbvtNode* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_rayDirectionInverse, i32* %arraydecay, float %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8, %class.btVector3* nonnull align 4 dereferenceable(16) %9, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %10, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %11)
  %m_sets5 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx6 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets5, i32 0, i32 1
  %m_sets7 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx8 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets7, i32 0, i32 1
  %m_root9 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx8, i32 0, i32 0
  %12 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root9, align 4
  %13 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %14 = load %class.btVector3*, %class.btVector3** %rayTo.addr, align 4
  %15 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  %m_rayDirectionInverse10 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %15, i32 0, i32 1
  %16 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  %m_signs11 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %16, i32 0, i32 2
  %arraydecay12 = getelementptr inbounds [3 x i32], [3 x i32]* %m_signs11, i32 0, i32 0
  %17 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %rayCallback.addr, align 4
  %m_lambda_max13 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %17, i32 0, i32 3
  %18 = load float, float* %m_lambda_max13, align 4
  %19 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %20 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %21 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %stack, align 4
  %22 = bitcast %struct.BroadphaseRayTester* %callback to %"struct.btDbvt::ICollide"*
  call void @_ZNK6btDbvt15rayTestInternalEPK10btDbvtNodeRK9btVector3S5_S5_PjfS5_S5_R20btAlignedObjectArrayIS2_ERNS_8ICollideE(%struct.btDbvt* %arrayidx6, %struct.btDbvtNode* %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %m_rayDirectionInverse10, i32* %arraydecay12, float %18, %class.btVector3* nonnull align 4 dereferenceable(16) %19, %class.btVector3* nonnull align 4 dereferenceable(16) %20, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %21, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %22)
  %call14 = call %struct.BroadphaseRayTester* @_ZN19BroadphaseRayTesterD2Ev(%struct.BroadphaseRayTester* %callback) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.BroadphaseRayTester* @_ZN19BroadphaseRayTesterC2ER23btBroadphaseRayCallback(%struct.BroadphaseRayTester* returned %this, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %orgCallback) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.BroadphaseRayTester*, align 4
  %orgCallback.addr = alloca %struct.btBroadphaseRayCallback*, align 4
  store %struct.BroadphaseRayTester* %this, %struct.BroadphaseRayTester** %this.addr, align 4
  store %struct.btBroadphaseRayCallback* %orgCallback, %struct.btBroadphaseRayCallback** %orgCallback.addr, align 4
  %this1 = load %struct.BroadphaseRayTester*, %struct.BroadphaseRayTester** %this.addr, align 4
  %0 = bitcast %struct.BroadphaseRayTester* %this1 to %"struct.btDbvt::ICollide"*
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideC2Ev(%"struct.btDbvt::ICollide"* %0) #8
  %1 = bitcast %struct.BroadphaseRayTester* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV19BroadphaseRayTester, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_rayCallback = getelementptr inbounds %struct.BroadphaseRayTester, %struct.BroadphaseRayTester* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %orgCallback.addr, align 4
  store %struct.btBroadphaseRayCallback* %2, %struct.btBroadphaseRayCallback** %m_rayCallback, align 4
  ret %struct.BroadphaseRayTester* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEEixEi(%class.btAlignedObjectArray.1* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %0, i32 %1
  ret %class.btAlignedObjectArray.4* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK6btDbvt15rayTestInternalEPK10btDbvtNodeRK9btVector3S5_S5_PjfS5_S5_R20btAlignedObjectArrayIS2_ERNS_8ICollideE(%struct.btDbvt* %this, %struct.btDbvtNode* %root, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTo, %class.btVector3* nonnull align 4 dereferenceable(16) %rayDirectionInverse, i32* %signs, float %lambda_max, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %stack, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %policy) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  %root.addr = alloca %struct.btDbvtNode*, align 4
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayTo.addr = alloca %class.btVector3*, align 4
  %rayDirectionInverse.addr = alloca %class.btVector3*, align 4
  %signs.addr = alloca i32*, align 4
  %lambda_max.addr = alloca float, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %stack.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %policy.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %resultNormal = alloca %class.btVector3, align 4
  %depth = alloca i32, align 4
  %treshold = alloca i32, align 4
  %ref.tmp = alloca %struct.btDbvtNode*, align 4
  %bounds = alloca [2 x %class.btVector3], align 16
  %node = alloca %struct.btDbvtNode*, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %tmin = alloca float, align 4
  %lambda_min = alloca float, align 4
  %result1 = alloca i32, align 4
  %ref.tmp18 = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store %struct.btDbvtNode* %root, %struct.btDbvtNode** %root.addr, align 4
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4
  store %class.btVector3* %rayTo, %class.btVector3** %rayTo.addr, align 4
  store %class.btVector3* %rayDirectionInverse, %class.btVector3** %rayDirectionInverse.addr, align 4
  store i32* %signs, i32** %signs.addr, align 4
  store float %lambda_max, float* %lambda_max.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store %class.btAlignedObjectArray.4* %stack, %class.btAlignedObjectArray.4** %stack.addr, align 4
  store %"struct.btDbvt::ICollide"* %policy, %"struct.btDbvt::ICollide"** %policy.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %rayTo.addr, align 4
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %tobool = icmp ne %struct.btDbvtNode* %1, null
  br i1 %tobool, label %if.then, label %if.end29

if.then:                                          ; preds = %entry
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %resultNormal)
  store i32 1, i32* %depth, align 4
  store i32 126, i32* %treshold, align 4
  %2 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %stack.addr, align 4
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE6resizeEiRKS2_(%class.btAlignedObjectArray.4* %2, i32 128, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %ref.tmp)
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root.addr, align 4
  %4 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %stack.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi(%class.btAlignedObjectArray.4* %4, i32 0)
  store %struct.btDbvtNode* %3, %struct.btDbvtNode** %call2, align 4
  %array.begin = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 2
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %if.then
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %if.then ], [ %arrayctor.next, %arrayctor.loop ]
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  br label %do.body

do.body:                                          ; preds = %do.cond, %arrayctor.cont
  %5 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %stack.addr, align 4
  %6 = load i32, i32* %depth, align 4
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %depth, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi(%class.btAlignedObjectArray.4* %5, i32 %dec)
  %7 = load %struct.btDbvtNode*, %struct.btDbvtNode** %call4, align 4
  store %struct.btDbvtNode* %7, %struct.btDbvtNode** %node, align 4
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %8, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btDbvtAabbMm4MinsEv(%struct.btDbvtAabbMm* %volume)
  %9 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %9)
  %arrayidx = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %10 = bitcast %class.btVector3* %arrayidx to i8*
  %11 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %10, i8* align 4 %11, i32 16, i1 false)
  %12 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %volume8 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %12, i32 0, i32 0
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btDbvtAabbMm4MaxsEv(%struct.btDbvtAabbMm* %volume8)
  %13 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9, %class.btVector3* nonnull align 4 dereferenceable(16) %13)
  %arrayidx10 = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 1
  %14 = bitcast %class.btVector3* %arrayidx10 to i8*
  %15 = bitcast %class.btVector3* %ref.tmp7 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %14, i8* align 4 %15, i32 16, i1 false)
  store float 1.000000e+00, float* %tmin, align 4
  store float 0.000000e+00, float* %lambda_min, align 4
  store i32 0, i32* %result1, align 4
  %16 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %17 = load %class.btVector3*, %class.btVector3** %rayDirectionInverse.addr, align 4
  %18 = load i32*, i32** %signs.addr, align 4
  %arraydecay = getelementptr inbounds [2 x %class.btVector3], [2 x %class.btVector3]* %bounds, i32 0, i32 0
  %19 = load float, float* %lambda_min, align 4
  %20 = load float, float* %lambda_max.addr, align 4
  %call11 = call zeroext i1 @_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff(%class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %17, i32* %18, %class.btVector3* %arraydecay, float* nonnull align 4 dereferenceable(4) %tmin, float %19, float %20)
  %conv = zext i1 %call11 to i32
  store i32 %conv, i32* %result1, align 4
  %21 = load i32, i32* %result1, align 4
  %tobool12 = icmp ne i32 %21, 0
  br i1 %tobool12, label %if.then13, label %if.end27

if.then13:                                        ; preds = %do.body
  %22 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %call14 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %22)
  br i1 %call14, label %if.then15, label %if.else

if.then15:                                        ; preds = %if.then13
  %23 = load i32, i32* %depth, align 4
  %24 = load i32, i32* %treshold, align 4
  %cmp = icmp sgt i32 %23, %24
  br i1 %cmp, label %if.then16, label %if.end

if.then16:                                        ; preds = %if.then15
  %25 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %stack.addr, align 4
  %26 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %stack.addr, align 4
  %call17 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.4* %26)
  %mul = mul nsw i32 %call17, 2
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %ref.tmp18, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE6resizeEiRKS2_(%class.btAlignedObjectArray.4* %25, i32 %mul, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %ref.tmp18)
  %27 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %stack.addr, align 4
  %call19 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.4* %27)
  %sub = sub nsw i32 %call19, 2
  store i32 %sub, i32* %treshold, align 4
  br label %if.end

if.end:                                           ; preds = %if.then16, %if.then15
  %28 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %29 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %28, i32 0, i32 2
  %childs = bitcast %union.anon.0* %29 to [2 x %struct.btDbvtNode*]*
  %arrayidx20 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  %30 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx20, align 4
  %31 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %stack.addr, align 4
  %32 = load i32, i32* %depth, align 4
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %depth, align 4
  %call21 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi(%class.btAlignedObjectArray.4* %31, i32 %32)
  store %struct.btDbvtNode* %30, %struct.btDbvtNode** %call21, align 4
  %33 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %34 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %33, i32 0, i32 2
  %childs22 = bitcast %union.anon.0* %34 to [2 x %struct.btDbvtNode*]*
  %arrayidx23 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs22, i32 0, i32 1
  %35 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx23, align 4
  %36 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %stack.addr, align 4
  %37 = load i32, i32* %depth, align 4
  %inc24 = add nsw i32 %37, 1
  store i32 %inc24, i32* %depth, align 4
  %call25 = call nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi(%class.btAlignedObjectArray.4* %36, i32 %37)
  store %struct.btDbvtNode* %35, %struct.btDbvtNode** %call25, align 4
  br label %if.end26

if.else:                                          ; preds = %if.then13
  %38 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %policy.addr, align 4
  %39 = load %struct.btDbvtNode*, %struct.btDbvtNode** %node, align 4
  %40 = bitcast %"struct.btDbvt::ICollide"* %38 to void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)***
  %vtable = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)**, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*** %40, align 4
  %vfn = getelementptr inbounds void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vtable, i64 3
  %41 = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vfn, align 4
  call void %41(%"struct.btDbvt::ICollide"* %38, %struct.btDbvtNode* %39)
  br label %if.end26

if.end26:                                         ; preds = %if.else, %if.end
  br label %if.end27

if.end27:                                         ; preds = %if.end26, %do.body
  br label %do.cond

do.cond:                                          ; preds = %if.end27
  %42 = load i32, i32* %depth, align 4
  %tobool28 = icmp ne i32 %42, 0
  br i1 %tobool28, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  br label %if.end29

if.end29:                                         ; preds = %do.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.BroadphaseRayTester* @_ZN19BroadphaseRayTesterD2Ev(%struct.BroadphaseRayTester* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.BroadphaseRayTester*, align 4
  store %struct.BroadphaseRayTester* %this, %struct.BroadphaseRayTester** %this.addr, align 4
  %this1 = load %struct.BroadphaseRayTester*, %struct.BroadphaseRayTester** %this.addr, align 4
  %0 = bitcast %struct.BroadphaseRayTester* %this1 to %"struct.btDbvt::ICollide"*
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* %0) #8
  ret %struct.BroadphaseRayTester* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btDbvtBroadphase8aabbTestERK9btVector3S2_R24btBroadphaseAabbCallback(%struct.btDbvtBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %struct.btBroadphaseAabbCallback* nonnull align 4 dereferenceable(4) %aabbCallback) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %aabbCallback.addr = alloca %struct.btBroadphaseAabbCallback*, align 4
  %callback = alloca %struct.BroadphaseAabbTester, align 4
  %bounds = alloca %struct.btDbvtAabbMm, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store %struct.btBroadphaseAabbCallback* %aabbCallback, %struct.btBroadphaseAabbCallback** %aabbCallback.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %0 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %aabbCallback.addr, align 4
  %call = call %struct.BroadphaseAabbTester* @_ZN20BroadphaseAabbTesterC2ER24btBroadphaseAabbCallback(%struct.BroadphaseAabbTester* %callback, %struct.btBroadphaseAabbCallback* nonnull align 4 dereferenceable(4) %0)
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %bounds, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  %m_sets = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets, i32 0, i32 0
  %m_sets2 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets2, i32 0, i32 0
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx3, i32 0, i32 0
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %4 = bitcast %struct.BroadphaseAabbTester* %callback to %"struct.btDbvt::ICollide"*
  call void @_ZNK6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE(%struct.btDbvt* %arrayidx, %struct.btDbvtNode* %3, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %bounds, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %4)
  %m_sets4 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets4, i32 0, i32 1
  %m_sets6 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx7 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets6, i32 0, i32 1
  %m_root8 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx7, i32 0, i32 0
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root8, align 4
  %6 = bitcast %struct.BroadphaseAabbTester* %callback to %"struct.btDbvt::ICollide"*
  call void @_ZNK6btDbvt9collideTVEPK10btDbvtNodeRK12btDbvtAabbMmRNS_8ICollideE(%struct.btDbvt* %arrayidx5, %struct.btDbvtNode* %5, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %bounds, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %6)
  %call9 = call %struct.BroadphaseAabbTester* @_ZN20BroadphaseAabbTesterD2Ev(%struct.BroadphaseAabbTester* %callback) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.BroadphaseAabbTester* @_ZN20BroadphaseAabbTesterC2ER24btBroadphaseAabbCallback(%struct.BroadphaseAabbTester* returned %this, %struct.btBroadphaseAabbCallback* nonnull align 4 dereferenceable(4) %orgCallback) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.BroadphaseAabbTester*, align 4
  %orgCallback.addr = alloca %struct.btBroadphaseAabbCallback*, align 4
  store %struct.BroadphaseAabbTester* %this, %struct.BroadphaseAabbTester** %this.addr, align 4
  store %struct.btBroadphaseAabbCallback* %orgCallback, %struct.btBroadphaseAabbCallback** %orgCallback.addr, align 4
  %this1 = load %struct.BroadphaseAabbTester*, %struct.BroadphaseAabbTester** %this.addr, align 4
  %0 = bitcast %struct.BroadphaseAabbTester* %this1 to %"struct.btDbvt::ICollide"*
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideC2Ev(%"struct.btDbvt::ICollide"* %0) #8
  %1 = bitcast %struct.BroadphaseAabbTester* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTV20BroadphaseAabbTester, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_aabbCallback = getelementptr inbounds %struct.BroadphaseAabbTester, %struct.BroadphaseAabbTester* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %orgCallback.addr, align 4
  store %struct.btBroadphaseAabbCallback* %2, %struct.btBroadphaseAabbCallback** %m_aabbCallback, align 4
  ret %struct.BroadphaseAabbTester* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.BroadphaseAabbTester* @_ZN20BroadphaseAabbTesterD2Ev(%struct.BroadphaseAabbTester* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.BroadphaseAabbTester*, align 4
  store %struct.BroadphaseAabbTester* %this, %struct.BroadphaseAabbTester** %this.addr, align 4
  %this1 = load %struct.BroadphaseAabbTester*, %struct.BroadphaseAabbTester** %this.addr, align 4
  %0 = bitcast %struct.BroadphaseAabbTester* %this1 to %"struct.btDbvt::ICollide"*
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* %0) #8
  ret %struct.BroadphaseAabbTester* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btDbvtBroadphase7setAabbEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher(%struct.btDbvtBroadphase* %this, %struct.btBroadphaseProxy* %absproxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btDispatcher* %0) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  %absproxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %proxy = alloca %struct.btDbvtProxy*, align 4
  %aabb = alloca %struct.btDbvtAabbMm, align 4
  %docollide = alloca i8, align 1
  %delta = alloca %class.btVector3, align 4
  %velocity = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca float, align 4
  %collider = alloca %struct.btDbvtTreeCollider, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  store %struct.btBroadphaseProxy* %absproxy, %struct.btBroadphaseProxy** %absproxy.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %absproxy.addr, align 4
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %struct.btDbvtProxy*
  store %struct.btDbvtProxy* %2, %struct.btDbvtProxy** %proxy, align 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %aabb, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  store i8 0, i8* %docollide, align 1
  %5 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %stage = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %5, i32 0, i32 3
  %6 = load i32, i32* %stage, align 4
  %cmp = icmp eq i32 %6, 2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_sets = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets, i32 0, i32 1
  %7 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %7, i32 0, i32 1
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf, align 4
  call void @_ZN6btDbvt6removeEP10btDbvtNode(%struct.btDbvt* %arrayidx, %struct.btDbvtNode* %8)
  %m_sets2 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets2, i32 0, i32 0
  %9 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %10 = bitcast %struct.btDbvtProxy* %9 to i8*
  %call = call %struct.btDbvtNode* @_ZN6btDbvt6insertERK12btDbvtAabbMmPv(%struct.btDbvt* %arrayidx3, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %aabb, i8* %10)
  %11 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf4 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %11, i32 0, i32 1
  store %struct.btDbvtNode* %call, %struct.btDbvtNode** %leaf4, align 4
  store i8 1, i8* %docollide, align 1
  br label %if.end53

if.else:                                          ; preds = %entry
  %m_updates_call = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 11
  %12 = load i32, i32* %m_updates_call, align 4
  %inc = add i32 %12, 1
  store i32 %inc, i32* %m_updates_call, align 4
  %13 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf5 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %13, i32 0, i32 1
  %14 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf5, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %14, i32 0, i32 0
  %call6 = call zeroext i1 @_Z9IntersectRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %aabb)
  br i1 %call6, label %if.then7, label %if.else46

if.then7:                                         ; preds = %if.else
  %15 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %16 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %17 = bitcast %struct.btDbvtProxy* %16 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %17, i32 0, i32 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %delta, %class.btVector3* nonnull align 4 dereferenceable(16) %15, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMin)
  %18 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %19 = bitcast %struct.btDbvtProxy* %18 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %19, i32 0, i32 5
  %20 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %21 = bitcast %struct.btDbvtProxy* %20 to %struct.btBroadphaseProxy*
  %m_aabbMin9 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %21, i32 0, i32 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMax, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMin9)
  store float 2.000000e+00, float* %ref.tmp10, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %m_prediction = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %velocity, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %m_prediction)
  %call11 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %delta)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 0
  %22 = load float, float* %arrayidx12, align 4
  %cmp13 = fcmp olt float %22, 0.000000e+00
  br i1 %cmp13, label %if.then14, label %if.end

if.then14:                                        ; preds = %if.then7
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %velocity)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 0
  %23 = load float, float* %arrayidx16, align 4
  %fneg = fneg float %23
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %velocity)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 0
  store float %fneg, float* %arrayidx18, align 4
  br label %if.end

if.end:                                           ; preds = %if.then14, %if.then7
  %call19 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %delta)
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 1
  %24 = load float, float* %arrayidx20, align 4
  %cmp21 = fcmp olt float %24, 0.000000e+00
  br i1 %cmp21, label %if.then22, label %if.end28

if.then22:                                        ; preds = %if.end
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %velocity)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 1
  %25 = load float, float* %arrayidx24, align 4
  %fneg25 = fneg float %25
  %call26 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %velocity)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 1
  store float %fneg25, float* %arrayidx27, align 4
  br label %if.end28

if.end28:                                         ; preds = %if.then22, %if.end
  %call29 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %delta)
  %arrayidx30 = getelementptr inbounds float, float* %call29, i32 2
  %26 = load float, float* %arrayidx30, align 4
  %cmp31 = fcmp olt float %26, 0.000000e+00
  br i1 %cmp31, label %if.then32, label %if.end38

if.then32:                                        ; preds = %if.end28
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %velocity)
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 2
  %27 = load float, float* %arrayidx34, align 4
  %fneg35 = fneg float %27
  %call36 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %velocity)
  %arrayidx37 = getelementptr inbounds float, float* %call36, i32 2
  store float %fneg35, float* %arrayidx37, align 4
  br label %if.end38

if.end38:                                         ; preds = %if.then32, %if.end28
  %m_sets39 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx40 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets39, i32 0, i32 0
  %28 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf41 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %28, i32 0, i32 1
  %29 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf41, align 4
  %call42 = call zeroext i1 @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3f(%struct.btDbvt* %arrayidx40, %struct.btDbvtNode* %29, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %aabb, %class.btVector3* nonnull align 4 dereferenceable(16) %velocity, float 0x3FA99999A0000000)
  br i1 %call42, label %if.then43, label %if.end45

if.then43:                                        ; preds = %if.end38
  %m_updates_done = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 12
  %30 = load i32, i32* %m_updates_done, align 4
  %inc44 = add i32 %30, 1
  store i32 %inc44, i32* %m_updates_done, align 4
  store i8 1, i8* %docollide, align 1
  br label %if.end45

if.end45:                                         ; preds = %if.then43, %if.end38
  br label %if.end52

if.else46:                                        ; preds = %if.else
  %m_sets47 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx48 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets47, i32 0, i32 0
  %31 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf49 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %31, i32 0, i32 1
  %32 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf49, align 4
  call void @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm(%struct.btDbvt* %arrayidx48, %struct.btDbvtNode* %32, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %aabb)
  %m_updates_done50 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 12
  %33 = load i32, i32* %m_updates_done50, align 4
  %inc51 = add i32 %33, 1
  store i32 %inc51, i32* %m_updates_done50, align 4
  store i8 1, i8* %docollide, align 1
  br label %if.end52

if.end52:                                         ; preds = %if.else46, %if.end45
  br label %if.end53

if.end53:                                         ; preds = %if.end52, %if.then
  %34 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %m_stageRoots = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 2
  %35 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %stage54 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %35, i32 0, i32 3
  %36 = load i32, i32* %stage54, align 4
  %arrayidx55 = getelementptr inbounds [3 x %struct.btDbvtProxy*], [3 x %struct.btDbvtProxy*]* %m_stageRoots, i32 0, i32 %36
  call void @_ZL10listremoveI11btDbvtProxyEvPT_RS2_(%struct.btDbvtProxy* %34, %struct.btDbvtProxy** nonnull align 4 dereferenceable(4) %arrayidx55)
  %37 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %38 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %39 = bitcast %struct.btDbvtProxy* %38 to %struct.btBroadphaseProxy*
  %m_aabbMin56 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %39, i32 0, i32 4
  %40 = bitcast %class.btVector3* %m_aabbMin56 to i8*
  %41 = bitcast %class.btVector3* %37 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %40, i8* align 4 %41, i32 16, i1 false)
  %42 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %43 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %44 = bitcast %struct.btDbvtProxy* %43 to %struct.btBroadphaseProxy*
  %m_aabbMax57 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %44, i32 0, i32 5
  %45 = bitcast %class.btVector3* %m_aabbMax57 to i8*
  %46 = bitcast %class.btVector3* %42 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %45, i8* align 4 %46, i32 16, i1 false)
  %m_stageCurrent = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 5
  %47 = load i32, i32* %m_stageCurrent, align 4
  %48 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %stage58 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %48, i32 0, i32 3
  store i32 %47, i32* %stage58, align 4
  %49 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %m_stageRoots59 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 2
  %m_stageCurrent60 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 5
  %50 = load i32, i32* %m_stageCurrent60, align 4
  %arrayidx61 = getelementptr inbounds [3 x %struct.btDbvtProxy*], [3 x %struct.btDbvtProxy*]* %m_stageRoots59, i32 0, i32 %50
  call void @_ZL10listappendI11btDbvtProxyEvPT_RS2_(%struct.btDbvtProxy* %49, %struct.btDbvtProxy** nonnull align 4 dereferenceable(4) %arrayidx61)
  %51 = load i8, i8* %docollide, align 1
  %tobool = trunc i8 %51 to i1
  br i1 %tobool, label %if.then62, label %if.end79

if.then62:                                        ; preds = %if.end53
  %m_needcleanup = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 19
  store i8 1, i8* %m_needcleanup, align 2
  %m_deferedcollide = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 18
  %52 = load i8, i8* %m_deferedcollide, align 1
  %tobool63 = trunc i8 %52 to i1
  br i1 %tobool63, label %if.end78, label %if.then64

if.then64:                                        ; preds = %if.then62
  %call65 = call %struct.btDbvtTreeCollider* @_ZN18btDbvtTreeColliderC2EP16btDbvtBroadphase(%struct.btDbvtTreeCollider* %collider, %struct.btDbvtBroadphase* %this1)
  %m_sets66 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx67 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets66, i32 0, i32 1
  %m_sets68 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx69 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets68, i32 0, i32 1
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx69, i32 0, i32 0
  %53 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %54 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf70 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %54, i32 0, i32 1
  %55 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf70, align 4
  %56 = bitcast %struct.btDbvtTreeCollider* %collider to %"struct.btDbvt::ICollide"*
  call void @_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE(%struct.btDbvt* %arrayidx67, %struct.btDbvtNode* %53, %struct.btDbvtNode* %55, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %56)
  %m_sets71 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx72 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets71, i32 0, i32 0
  %m_sets73 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx74 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets73, i32 0, i32 0
  %m_root75 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx74, i32 0, i32 0
  %57 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root75, align 4
  %58 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf76 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %58, i32 0, i32 1
  %59 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf76, align 4
  %60 = bitcast %struct.btDbvtTreeCollider* %collider to %"struct.btDbvt::ICollide"*
  call void @_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE(%struct.btDbvt* %arrayidx72, %struct.btDbvtNode* %57, %struct.btDbvtNode* %59, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %60)
  %call77 = call %struct.btDbvtTreeCollider* @_ZN18btDbvtTreeColliderD2Ev(%struct.btDbvtTreeCollider* %collider) #8
  br label %if.end78

if.end78:                                         ; preds = %if.then64, %if.then62
  br label %if.end79

if.end79:                                         ; preds = %if.end78, %if.end53
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_Z9IntersectRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %b) #2 comdat {
entry:
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %b.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4
  store %struct.btDbvtAabbMm* %b, %struct.btDbvtAabbMm** %b.addr, align 4
  %0 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %0, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi)
  %1 = load float, float* %call, align 4
  %2 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %2, i32 0, i32 1
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx)
  %3 = load float, float* %call1, align 4
  %cmp = fcmp ole float %1, %3
  br i1 %cmp, label %land.lhs.true, label %land.end

land.lhs.true:                                    ; preds = %entry
  %4 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx2 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %4, i32 0, i32 1
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mx2)
  %5 = load float, float* %call3, align 4
  %6 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi4 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %6, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %mi4)
  %7 = load float, float* %call5, align 4
  %cmp6 = fcmp oge float %5, %7
  br i1 %cmp6, label %land.lhs.true7, label %land.end

land.lhs.true7:                                   ; preds = %land.lhs.true
  %8 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi8 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %8, i32 0, i32 0
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi8)
  %9 = load float, float* %call9, align 4
  %10 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx10 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %10, i32 0, i32 1
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx10)
  %11 = load float, float* %call11, align 4
  %cmp12 = fcmp ole float %9, %11
  br i1 %cmp12, label %land.lhs.true13, label %land.end

land.lhs.true13:                                  ; preds = %land.lhs.true7
  %12 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx14 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %12, i32 0, i32 1
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mx14)
  %13 = load float, float* %call15, align 4
  %14 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi16 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %14, i32 0, i32 0
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %mi16)
  %15 = load float, float* %call17, align 4
  %cmp18 = fcmp oge float %13, %15
  br i1 %cmp18, label %land.lhs.true19, label %land.end

land.lhs.true19:                                  ; preds = %land.lhs.true13
  %16 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi20 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %16, i32 0, i32 0
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi20)
  %17 = load float, float* %call21, align 4
  %18 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx22 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %18, i32 0, i32 1
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx22)
  %19 = load float, float* %call23, align 4
  %cmp24 = fcmp ole float %17, %19
  br i1 %cmp24, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true19
  %20 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx25 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %20, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mx25)
  %21 = load float, float* %call26, align 4
  %22 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi27 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %22, i32 0, i32 0
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %mi27)
  %23 = load float, float* %call28, align 4
  %cmp29 = fcmp oge float %21, %23
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true19, %land.lhs.true13, %land.lhs.true7, %land.lhs.true, %entry
  %24 = phi i1 [ false, %land.lhs.true19 ], [ false, %land.lhs.true13 ], [ false, %land.lhs.true7 ], [ false, %land.lhs.true ], [ false, %entry ], [ %cmp29, %land.rhs ]
  ret i1 %24
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  %2 = load float, float* %1, align 4
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

declare zeroext i1 @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMmRK9btVector3f(%struct.btDbvt*, %struct.btDbvtNode*, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32), %class.btVector3* nonnull align 4 dereferenceable(16), float) #3

declare void @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm(%struct.btDbvt*, %struct.btDbvtNode*, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32)) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE(%struct.btDbvt* %this, %struct.btDbvtNode* %root0, %struct.btDbvtNode* %root1, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %policy) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  %root0.addr = alloca %struct.btDbvtNode*, align 4
  %root1.addr = alloca %struct.btDbvtNode*, align 4
  %policy.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %depth = alloca i32, align 4
  %treshold = alloca i32, align 4
  %ref.tmp = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp3 = alloca %"struct.btDbvt::sStkNN", align 4
  %p = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp13 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp22 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp30 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp41 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp64 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp75 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp86 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp97 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp109 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp118 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp132 = alloca %"struct.btDbvt::sStkNN", align 4
  %ref.tmp141 = alloca %"struct.btDbvt::sStkNN", align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  store %struct.btDbvtNode* %root0, %struct.btDbvtNode** %root0.addr, align 4
  store %struct.btDbvtNode* %root1, %struct.btDbvtNode** %root1.addr, align 4
  store %"struct.btDbvt::ICollide"* %policy, %"struct.btDbvt::ICollide"** %policy.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root0.addr, align 4
  %tobool = icmp ne %struct.btDbvtNode* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end158

land.lhs.true:                                    ; preds = %entry
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root1.addr, align 4
  %tobool2 = icmp ne %struct.btDbvtNode* %1, null
  br i1 %tobool2, label %if.then, label %if.end158

if.then:                                          ; preds = %land.lhs.true
  store i32 1, i32* %depth, align 4
  store i32 124, i32* %treshold, align 4
  %m_stkStack = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %call = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2Ev(%"struct.btDbvt::sStkNN"* %ref.tmp)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE6resizeEiRKS1_(%class.btAlignedObjectArray* %m_stkStack, i32 128, %"struct.btDbvt::sStkNN"* nonnull align 4 dereferenceable(8) %ref.tmp)
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root0.addr, align 4
  %3 = load %struct.btDbvtNode*, %struct.btDbvtNode** %root1.addr, align 4
  %call4 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp3, %struct.btDbvtNode* %2, %struct.btDbvtNode* %3)
  %m_stkStack5 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %call6 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray* %m_stkStack5, i32 0)
  %4 = bitcast %"struct.btDbvt::sStkNN"* %call6 to i8*
  %5 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 8, i1 false)
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then
  %m_stkStack7 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %6 = load i32, i32* %depth, align 4
  %dec = add nsw i32 %6, -1
  store i32 %dec, i32* %depth, align 4
  %call8 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray* %m_stkStack7, i32 %dec)
  %7 = bitcast %"struct.btDbvt::sStkNN"* %p to i8*
  %8 = bitcast %"struct.btDbvt::sStkNN"* %call8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 8, i1 false)
  %9 = load i32, i32* %depth, align 4
  %10 = load i32, i32* %treshold, align 4
  %cmp = icmp sgt i32 %9, %10
  br i1 %cmp, label %if.then9, label %if.end

if.then9:                                         ; preds = %do.body
  %m_stkStack10 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %m_stkStack11 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %call12 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray* %m_stkStack11)
  %mul = mul nsw i32 %call12, 2
  %call14 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2Ev(%"struct.btDbvt::sStkNN"* %ref.tmp13)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE6resizeEiRKS1_(%class.btAlignedObjectArray* %m_stkStack10, i32 %mul, %"struct.btDbvt::sStkNN"* nonnull align 4 dereferenceable(8) %ref.tmp13)
  %m_stkStack15 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %call16 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray* %m_stkStack15)
  %sub = sub nsw i32 %call16, 4
  store i32 %sub, i32* %treshold, align 4
  br label %if.end

if.end:                                           ; preds = %if.then9, %do.body
  %a = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %11 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a, align 4
  %b = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %12 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b, align 4
  %cmp17 = icmp eq %struct.btDbvtNode* %11, %12
  br i1 %cmp17, label %if.then18, label %if.else

if.then18:                                        ; preds = %if.end
  %a19 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %13 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a19, align 4
  %call20 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %13)
  br i1 %call20, label %if.then21, label %if.end52

if.then21:                                        ; preds = %if.then18
  %a23 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %14 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a23, align 4
  %15 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %14, i32 0, i32 2
  %childs = bitcast %union.anon.0* %15 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 0
  %16 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %a24 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %17 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a24, align 4
  %18 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %17, i32 0, i32 2
  %childs25 = bitcast %union.anon.0* %18 to [2 x %struct.btDbvtNode*]*
  %arrayidx26 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs25, i32 0, i32 0
  %19 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx26, align 4
  %call27 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp22, %struct.btDbvtNode* %16, %struct.btDbvtNode* %19)
  %m_stkStack28 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %20 = load i32, i32* %depth, align 4
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %depth, align 4
  %call29 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray* %m_stkStack28, i32 %20)
  %21 = bitcast %"struct.btDbvt::sStkNN"* %call29 to i8*
  %22 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 8, i1 false)
  %a31 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %23 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a31, align 4
  %24 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %23, i32 0, i32 2
  %childs32 = bitcast %union.anon.0* %24 to [2 x %struct.btDbvtNode*]*
  %arrayidx33 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs32, i32 0, i32 1
  %25 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx33, align 4
  %a34 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %26 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a34, align 4
  %27 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %26, i32 0, i32 2
  %childs35 = bitcast %union.anon.0* %27 to [2 x %struct.btDbvtNode*]*
  %arrayidx36 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs35, i32 0, i32 1
  %28 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx36, align 4
  %call37 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp30, %struct.btDbvtNode* %25, %struct.btDbvtNode* %28)
  %m_stkStack38 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %29 = load i32, i32* %depth, align 4
  %inc39 = add nsw i32 %29, 1
  store i32 %inc39, i32* %depth, align 4
  %call40 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray* %m_stkStack38, i32 %29)
  %30 = bitcast %"struct.btDbvt::sStkNN"* %call40 to i8*
  %31 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp30 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 8, i1 false)
  %a42 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %32 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a42, align 4
  %33 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %32, i32 0, i32 2
  %childs43 = bitcast %union.anon.0* %33 to [2 x %struct.btDbvtNode*]*
  %arrayidx44 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs43, i32 0, i32 0
  %34 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx44, align 4
  %a45 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %35 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a45, align 4
  %36 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %35, i32 0, i32 2
  %childs46 = bitcast %union.anon.0* %36 to [2 x %struct.btDbvtNode*]*
  %arrayidx47 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs46, i32 0, i32 1
  %37 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx47, align 4
  %call48 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp41, %struct.btDbvtNode* %34, %struct.btDbvtNode* %37)
  %m_stkStack49 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %38 = load i32, i32* %depth, align 4
  %inc50 = add nsw i32 %38, 1
  store i32 %inc50, i32* %depth, align 4
  %call51 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray* %m_stkStack49, i32 %38)
  %39 = bitcast %"struct.btDbvt::sStkNN"* %call51 to i8*
  %40 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp41 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %39, i8* align 4 %40, i32 8, i1 false)
  br label %if.end52

if.end52:                                         ; preds = %if.then21, %if.then18
  br label %if.end156

if.else:                                          ; preds = %if.end
  %a53 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %41 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a53, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %41, i32 0, i32 0
  %b54 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %42 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b54, align 4
  %volume55 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %42, i32 0, i32 0
  %call56 = call zeroext i1 @_Z9IntersectRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume55)
  br i1 %call56, label %if.then57, label %if.end155

if.then57:                                        ; preds = %if.else
  %a58 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %43 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a58, align 4
  %call59 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %43)
  br i1 %call59, label %if.then60, label %if.else128

if.then60:                                        ; preds = %if.then57
  %b61 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %44 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b61, align 4
  %call62 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %44)
  br i1 %call62, label %if.then63, label %if.else108

if.then63:                                        ; preds = %if.then60
  %a65 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %45 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a65, align 4
  %46 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %45, i32 0, i32 2
  %childs66 = bitcast %union.anon.0* %46 to [2 x %struct.btDbvtNode*]*
  %arrayidx67 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs66, i32 0, i32 0
  %47 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx67, align 4
  %b68 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %48 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b68, align 4
  %49 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %48, i32 0, i32 2
  %childs69 = bitcast %union.anon.0* %49 to [2 x %struct.btDbvtNode*]*
  %arrayidx70 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs69, i32 0, i32 0
  %50 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx70, align 4
  %call71 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp64, %struct.btDbvtNode* %47, %struct.btDbvtNode* %50)
  %m_stkStack72 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %51 = load i32, i32* %depth, align 4
  %inc73 = add nsw i32 %51, 1
  store i32 %inc73, i32* %depth, align 4
  %call74 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray* %m_stkStack72, i32 %51)
  %52 = bitcast %"struct.btDbvt::sStkNN"* %call74 to i8*
  %53 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp64 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %52, i8* align 4 %53, i32 8, i1 false)
  %a76 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %54 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a76, align 4
  %55 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %54, i32 0, i32 2
  %childs77 = bitcast %union.anon.0* %55 to [2 x %struct.btDbvtNode*]*
  %arrayidx78 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs77, i32 0, i32 1
  %56 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx78, align 4
  %b79 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %57 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b79, align 4
  %58 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %57, i32 0, i32 2
  %childs80 = bitcast %union.anon.0* %58 to [2 x %struct.btDbvtNode*]*
  %arrayidx81 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs80, i32 0, i32 0
  %59 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx81, align 4
  %call82 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp75, %struct.btDbvtNode* %56, %struct.btDbvtNode* %59)
  %m_stkStack83 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %60 = load i32, i32* %depth, align 4
  %inc84 = add nsw i32 %60, 1
  store i32 %inc84, i32* %depth, align 4
  %call85 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray* %m_stkStack83, i32 %60)
  %61 = bitcast %"struct.btDbvt::sStkNN"* %call85 to i8*
  %62 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp75 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %61, i8* align 4 %62, i32 8, i1 false)
  %a87 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %63 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a87, align 4
  %64 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %63, i32 0, i32 2
  %childs88 = bitcast %union.anon.0* %64 to [2 x %struct.btDbvtNode*]*
  %arrayidx89 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs88, i32 0, i32 0
  %65 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx89, align 4
  %b90 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %66 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b90, align 4
  %67 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %66, i32 0, i32 2
  %childs91 = bitcast %union.anon.0* %67 to [2 x %struct.btDbvtNode*]*
  %arrayidx92 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs91, i32 0, i32 1
  %68 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx92, align 4
  %call93 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp86, %struct.btDbvtNode* %65, %struct.btDbvtNode* %68)
  %m_stkStack94 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %69 = load i32, i32* %depth, align 4
  %inc95 = add nsw i32 %69, 1
  store i32 %inc95, i32* %depth, align 4
  %call96 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray* %m_stkStack94, i32 %69)
  %70 = bitcast %"struct.btDbvt::sStkNN"* %call96 to i8*
  %71 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp86 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %70, i8* align 4 %71, i32 8, i1 false)
  %a98 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %72 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a98, align 4
  %73 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %72, i32 0, i32 2
  %childs99 = bitcast %union.anon.0* %73 to [2 x %struct.btDbvtNode*]*
  %arrayidx100 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs99, i32 0, i32 1
  %74 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx100, align 4
  %b101 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %75 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b101, align 4
  %76 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %75, i32 0, i32 2
  %childs102 = bitcast %union.anon.0* %76 to [2 x %struct.btDbvtNode*]*
  %arrayidx103 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs102, i32 0, i32 1
  %77 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx103, align 4
  %call104 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp97, %struct.btDbvtNode* %74, %struct.btDbvtNode* %77)
  %m_stkStack105 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %78 = load i32, i32* %depth, align 4
  %inc106 = add nsw i32 %78, 1
  store i32 %inc106, i32* %depth, align 4
  %call107 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray* %m_stkStack105, i32 %78)
  %79 = bitcast %"struct.btDbvt::sStkNN"* %call107 to i8*
  %80 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp97 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %79, i8* align 4 %80, i32 8, i1 false)
  br label %if.end127

if.else108:                                       ; preds = %if.then60
  %a110 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %81 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a110, align 4
  %82 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %81, i32 0, i32 2
  %childs111 = bitcast %union.anon.0* %82 to [2 x %struct.btDbvtNode*]*
  %arrayidx112 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs111, i32 0, i32 0
  %83 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx112, align 4
  %b113 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %84 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b113, align 4
  %call114 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp109, %struct.btDbvtNode* %83, %struct.btDbvtNode* %84)
  %m_stkStack115 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %85 = load i32, i32* %depth, align 4
  %inc116 = add nsw i32 %85, 1
  store i32 %inc116, i32* %depth, align 4
  %call117 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray* %m_stkStack115, i32 %85)
  %86 = bitcast %"struct.btDbvt::sStkNN"* %call117 to i8*
  %87 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp109 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %86, i8* align 4 %87, i32 8, i1 false)
  %a119 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %88 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a119, align 4
  %89 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %88, i32 0, i32 2
  %childs120 = bitcast %union.anon.0* %89 to [2 x %struct.btDbvtNode*]*
  %arrayidx121 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs120, i32 0, i32 1
  %90 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx121, align 4
  %b122 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %91 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b122, align 4
  %call123 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp118, %struct.btDbvtNode* %90, %struct.btDbvtNode* %91)
  %m_stkStack124 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %92 = load i32, i32* %depth, align 4
  %inc125 = add nsw i32 %92, 1
  store i32 %inc125, i32* %depth, align 4
  %call126 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray* %m_stkStack124, i32 %92)
  %93 = bitcast %"struct.btDbvt::sStkNN"* %call126 to i8*
  %94 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp118 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %93, i8* align 4 %94, i32 8, i1 false)
  br label %if.end127

if.end127:                                        ; preds = %if.else108, %if.then63
  br label %if.end154

if.else128:                                       ; preds = %if.then57
  %b129 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %95 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b129, align 4
  %call130 = call zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %95)
  br i1 %call130, label %if.then131, label %if.else150

if.then131:                                       ; preds = %if.else128
  %a133 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %96 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a133, align 4
  %b134 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %97 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b134, align 4
  %98 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %97, i32 0, i32 2
  %childs135 = bitcast %union.anon.0* %98 to [2 x %struct.btDbvtNode*]*
  %arrayidx136 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs135, i32 0, i32 0
  %99 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx136, align 4
  %call137 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp132, %struct.btDbvtNode* %96, %struct.btDbvtNode* %99)
  %m_stkStack138 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %100 = load i32, i32* %depth, align 4
  %inc139 = add nsw i32 %100, 1
  store i32 %inc139, i32* %depth, align 4
  %call140 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray* %m_stkStack138, i32 %100)
  %101 = bitcast %"struct.btDbvt::sStkNN"* %call140 to i8*
  %102 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp132 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %101, i8* align 4 %102, i32 8, i1 false)
  %a142 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %103 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a142, align 4
  %b143 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %104 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b143, align 4
  %105 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %104, i32 0, i32 2
  %childs144 = bitcast %union.anon.0* %105 to [2 x %struct.btDbvtNode*]*
  %arrayidx145 = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs144, i32 0, i32 1
  %106 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx145, align 4
  %call146 = call %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* %ref.tmp141, %struct.btDbvtNode* %103, %struct.btDbvtNode* %106)
  %m_stkStack147 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 5
  %107 = load i32, i32* %depth, align 4
  %inc148 = add nsw i32 %107, 1
  store i32 %inc148, i32* %depth, align 4
  %call149 = call nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray* %m_stkStack147, i32 %107)
  %108 = bitcast %"struct.btDbvt::sStkNN"* %call149 to i8*
  %109 = bitcast %"struct.btDbvt::sStkNN"* %ref.tmp141 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %108, i8* align 4 %109, i32 8, i1 false)
  br label %if.end153

if.else150:                                       ; preds = %if.else128
  %110 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %policy.addr, align 4
  %a151 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 0
  %111 = load %struct.btDbvtNode*, %struct.btDbvtNode** %a151, align 4
  %b152 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %p, i32 0, i32 1
  %112 = load %struct.btDbvtNode*, %struct.btDbvtNode** %b152, align 4
  %113 = bitcast %"struct.btDbvt::ICollide"* %110 to void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)***
  %vtable = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)**, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)*** %113, align 4
  %vfn = getelementptr inbounds void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)** %vtable, i64 2
  %114 = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*, %struct.btDbvtNode*)** %vfn, align 4
  call void %114(%"struct.btDbvt::ICollide"* %110, %struct.btDbvtNode* %111, %struct.btDbvtNode* %112)
  br label %if.end153

if.end153:                                        ; preds = %if.else150, %if.then131
  br label %if.end154

if.end154:                                        ; preds = %if.end153, %if.end127
  br label %if.end155

if.end155:                                        ; preds = %if.end154, %if.else
  br label %if.end156

if.end156:                                        ; preds = %if.end155, %if.end52
  br label %do.cond

do.cond:                                          ; preds = %if.end156
  %115 = load i32, i32* %depth, align 4
  %tobool157 = icmp ne i32 %115, 0
  br i1 %tobool157, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  br label %if.end158

if.end158:                                        ; preds = %do.end, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btDbvtBroadphase18setAabbForceUpdateEP17btBroadphaseProxyRK9btVector3S4_P12btDispatcher(%struct.btDbvtBroadphase* %this, %struct.btBroadphaseProxy* %absproxy, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, %class.btDispatcher* %0) #2 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  %absproxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %.addr = alloca %class.btDispatcher*, align 4
  %proxy = alloca %struct.btDbvtProxy*, align 4
  %aabb = alloca %struct.btDbvtAabbMm, align 4
  %docollide = alloca i8, align 1
  %collider = alloca %struct.btDbvtTreeCollider, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  store %struct.btBroadphaseProxy* %absproxy, %struct.btBroadphaseProxy** %absproxy.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store %class.btDispatcher* %0, %class.btDispatcher** %.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %absproxy.addr, align 4
  %2 = bitcast %struct.btBroadphaseProxy* %1 to %struct.btDbvtProxy*
  store %struct.btDbvtProxy* %2, %struct.btDbvtProxy** %proxy, align 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %aabb, %class.btVector3* nonnull align 4 dereferenceable(16) %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  store i8 0, i8* %docollide, align 1
  %5 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %stage = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %5, i32 0, i32 3
  %6 = load i32, i32* %stage, align 4
  %cmp = icmp eq i32 %6, 2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_sets = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets, i32 0, i32 1
  %7 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %7, i32 0, i32 1
  %8 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf, align 4
  call void @_ZN6btDbvt6removeEP10btDbvtNode(%struct.btDbvt* %arrayidx, %struct.btDbvtNode* %8)
  %m_sets2 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets2, i32 0, i32 0
  %9 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %10 = bitcast %struct.btDbvtProxy* %9 to i8*
  %call = call %struct.btDbvtNode* @_ZN6btDbvt6insertERK12btDbvtAabbMmPv(%struct.btDbvt* %arrayidx3, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %aabb, i8* %10)
  %11 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf4 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %11, i32 0, i32 1
  store %struct.btDbvtNode* %call, %struct.btDbvtNode** %leaf4, align 4
  store i8 1, i8* %docollide, align 1
  br label %if.end

if.else:                                          ; preds = %entry
  %m_updates_call = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 11
  %12 = load i32, i32* %m_updates_call, align 4
  %inc = add i32 %12, 1
  store i32 %inc, i32* %m_updates_call, align 4
  %m_sets5 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx6 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets5, i32 0, i32 0
  %13 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf7 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %13, i32 0, i32 1
  %14 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf7, align 4
  call void @_ZN6btDbvt6updateEP10btDbvtNodeR12btDbvtAabbMm(%struct.btDbvt* %arrayidx6, %struct.btDbvtNode* %14, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %aabb)
  %m_updates_done = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 12
  %15 = load i32, i32* %m_updates_done, align 4
  %inc8 = add i32 %15, 1
  store i32 %inc8, i32* %m_updates_done, align 4
  store i8 1, i8* %docollide, align 1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %16 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %m_stageRoots = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 2
  %17 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %stage9 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %17, i32 0, i32 3
  %18 = load i32, i32* %stage9, align 4
  %arrayidx10 = getelementptr inbounds [3 x %struct.btDbvtProxy*], [3 x %struct.btDbvtProxy*]* %m_stageRoots, i32 0, i32 %18
  call void @_ZL10listremoveI11btDbvtProxyEvPT_RS2_(%struct.btDbvtProxy* %16, %struct.btDbvtProxy** nonnull align 4 dereferenceable(4) %arrayidx10)
  %19 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %20 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %21 = bitcast %struct.btDbvtProxy* %20 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %21, i32 0, i32 4
  %22 = bitcast %class.btVector3* %m_aabbMin to i8*
  %23 = bitcast %class.btVector3* %19 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %22, i8* align 4 %23, i32 16, i1 false)
  %24 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %25 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %26 = bitcast %struct.btDbvtProxy* %25 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %26, i32 0, i32 5
  %27 = bitcast %class.btVector3* %m_aabbMax to i8*
  %28 = bitcast %class.btVector3* %24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false)
  %m_stageCurrent = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 5
  %29 = load i32, i32* %m_stageCurrent, align 4
  %30 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %stage11 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %30, i32 0, i32 3
  store i32 %29, i32* %stage11, align 4
  %31 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %m_stageRoots12 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 2
  %m_stageCurrent13 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 5
  %32 = load i32, i32* %m_stageCurrent13, align 4
  %arrayidx14 = getelementptr inbounds [3 x %struct.btDbvtProxy*], [3 x %struct.btDbvtProxy*]* %m_stageRoots12, i32 0, i32 %32
  call void @_ZL10listappendI11btDbvtProxyEvPT_RS2_(%struct.btDbvtProxy* %31, %struct.btDbvtProxy** nonnull align 4 dereferenceable(4) %arrayidx14)
  %33 = load i8, i8* %docollide, align 1
  %tobool = trunc i8 %33 to i1
  br i1 %tobool, label %if.then15, label %if.end32

if.then15:                                        ; preds = %if.end
  %m_needcleanup = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 19
  store i8 1, i8* %m_needcleanup, align 2
  %m_deferedcollide = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 18
  %34 = load i8, i8* %m_deferedcollide, align 1
  %tobool16 = trunc i8 %34 to i1
  br i1 %tobool16, label %if.end31, label %if.then17

if.then17:                                        ; preds = %if.then15
  %call18 = call %struct.btDbvtTreeCollider* @_ZN18btDbvtTreeColliderC2EP16btDbvtBroadphase(%struct.btDbvtTreeCollider* %collider, %struct.btDbvtBroadphase* %this1)
  %m_sets19 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx20 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets19, i32 0, i32 1
  %m_sets21 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx22 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets21, i32 0, i32 1
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx22, i32 0, i32 0
  %35 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %36 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf23 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %36, i32 0, i32 1
  %37 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf23, align 4
  %38 = bitcast %struct.btDbvtTreeCollider* %collider to %"struct.btDbvt::ICollide"*
  call void @_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE(%struct.btDbvt* %arrayidx20, %struct.btDbvtNode* %35, %struct.btDbvtNode* %37, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %38)
  %m_sets24 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx25 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets24, i32 0, i32 0
  %m_sets26 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx27 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets26, i32 0, i32 0
  %m_root28 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx27, i32 0, i32 0
  %39 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root28, align 4
  %40 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf29 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %40, i32 0, i32 1
  %41 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf29, align 4
  %42 = bitcast %struct.btDbvtTreeCollider* %collider to %"struct.btDbvt::ICollide"*
  call void @_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE(%struct.btDbvt* %arrayidx25, %struct.btDbvtNode* %39, %struct.btDbvtNode* %41, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %42)
  %call30 = call %struct.btDbvtTreeCollider* @_ZN18btDbvtTreeColliderD2Ev(%struct.btDbvtTreeCollider* %collider) #8
  br label %if.end31

if.end31:                                         ; preds = %if.then17, %if.then15
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %if.end
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btDbvtBroadphase25calculateOverlappingPairsEP12btDispatcher(%struct.btDbvtBroadphase* %this, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %0 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN16btDbvtBroadphase7collideEP12btDispatcher(%struct.btDbvtBroadphase* %this1, %class.btDispatcher* %0)
  %1 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  call void @_ZN16btDbvtBroadphase22performDeferredRemovalEP12btDispatcher(%struct.btDbvtBroadphase* %this1, %class.btDispatcher* %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btDbvtBroadphase7collideEP12btDispatcher(%struct.btDbvtBroadphase* %this, %class.btDispatcher* %dispatcher) #2 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %count = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp19 = alloca i32, align 4
  %current = alloca %struct.btDbvtProxy*, align 4
  %collider = alloca %struct.btDbvtTreeCollider, align 4
  %next = alloca %struct.btDbvtProxy*, align 4
  %curAabb = alloca %struct.btDbvtAabbMm, align 4
  %collider48 = alloca %struct.btDbvtTreeCollider, align 4
  %pairs = alloca %class.btAlignedObjectArray.9*, align 4
  %ni = alloca i32, align 4
  %ref.tmp79 = alloca i32, align 4
  %ref.tmp81 = alloca i32, align 4
  %i = alloca i32, align 4
  %p = alloca %struct.btBroadphasePair*, align 4
  %pa = alloca %struct.btDbvtProxy*, align 4
  %pb = alloca %struct.btDbvtProxy*, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %m_sets = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets, i32 0, i32 0
  %m_sets2 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets2, i32 0, i32 0
  %m_leaves = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx3, i32 0, i32 3
  %0 = load i32, i32* %m_leaves, align 4
  %m_dupdates = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 7
  %1 = load i32, i32* %m_dupdates, align 4
  %mul = mul nsw i32 %0, %1
  %div = sdiv i32 %mul, 100
  %add = add nsw i32 1, %div
  call void @_ZN6btDbvt19optimizeIncrementalEi(%struct.btDbvt* %arrayidx, i32 %add)
  %m_fixedleft = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 10
  %2 = load i32, i32* %m_fixedleft, align 4
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_sets4 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets4, i32 0, i32 1
  %m_leaves6 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx5, i32 0, i32 3
  %3 = load i32, i32* %m_leaves6, align 4
  %m_fupdates = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 6
  %4 = load i32, i32* %m_fupdates, align 4
  %mul7 = mul nsw i32 %3, %4
  %div8 = sdiv i32 %mul7, 100
  %add9 = add nsw i32 1, %div8
  store i32 %add9, i32* %count, align 4
  %m_sets10 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx11 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets10, i32 0, i32 1
  %m_sets12 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx13 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets12, i32 0, i32 1
  %m_leaves14 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx13, i32 0, i32 3
  %5 = load i32, i32* %m_leaves14, align 4
  %m_fupdates15 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 6
  %6 = load i32, i32* %m_fupdates15, align 4
  %mul16 = mul nsw i32 %5, %6
  %div17 = sdiv i32 %mul16, 100
  %add18 = add nsw i32 1, %div17
  call void @_ZN6btDbvt19optimizeIncrementalEi(%struct.btDbvt* %arrayidx11, i32 %add18)
  store i32 0, i32* %ref.tmp, align 4
  %m_fixedleft20 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 10
  %7 = load i32, i32* %m_fixedleft20, align 4
  %8 = load i32, i32* %count, align 4
  %sub = sub nsw i32 %7, %8
  store i32 %sub, i32* %ref.tmp19, align 4
  %call = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %ref.tmp, i32* nonnull align 4 dereferenceable(4) %ref.tmp19)
  %9 = load i32, i32* %call, align 4
  %m_fixedleft21 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 10
  store i32 %9, i32* %m_fixedleft21, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_stageCurrent = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 5
  %10 = load i32, i32* %m_stageCurrent, align 4
  %add22 = add nsw i32 %10, 1
  %rem = srem i32 %add22, 2
  %m_stageCurrent23 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 5
  store i32 %rem, i32* %m_stageCurrent23, align 4
  %m_stageRoots = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 2
  %m_stageCurrent24 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 5
  %11 = load i32, i32* %m_stageCurrent24, align 4
  %arrayidx25 = getelementptr inbounds [3 x %struct.btDbvtProxy*], [3 x %struct.btDbvtProxy*]* %m_stageRoots, i32 0, i32 %11
  %12 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %arrayidx25, align 4
  store %struct.btDbvtProxy* %12, %struct.btDbvtProxy** %current, align 4
  %13 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %current, align 4
  %tobool26 = icmp ne %struct.btDbvtProxy* %13, null
  br i1 %tobool26, label %if.then27, label %if.end47

if.then27:                                        ; preds = %if.end
  %call28 = call %struct.btDbvtTreeCollider* @_ZN18btDbvtTreeColliderC2EP16btDbvtBroadphase(%struct.btDbvtTreeCollider* %collider, %struct.btDbvtBroadphase* %this1)
  br label %do.body

do.body:                                          ; preds = %do.cond, %if.then27
  %14 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %current, align 4
  %links = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %14, i32 0, i32 2
  %arrayidx29 = getelementptr inbounds [2 x %struct.btDbvtProxy*], [2 x %struct.btDbvtProxy*]* %links, i32 0, i32 1
  %15 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %arrayidx29, align 4
  store %struct.btDbvtProxy* %15, %struct.btDbvtProxy** %next, align 4
  %16 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %current, align 4
  %m_stageRoots30 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 2
  %17 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %current, align 4
  %stage = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %17, i32 0, i32 3
  %18 = load i32, i32* %stage, align 4
  %arrayidx31 = getelementptr inbounds [3 x %struct.btDbvtProxy*], [3 x %struct.btDbvtProxy*]* %m_stageRoots30, i32 0, i32 %18
  call void @_ZL10listremoveI11btDbvtProxyEvPT_RS2_(%struct.btDbvtProxy* %16, %struct.btDbvtProxy** nonnull align 4 dereferenceable(4) %arrayidx31)
  %19 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %current, align 4
  %m_stageRoots32 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 2
  %arrayidx33 = getelementptr inbounds [3 x %struct.btDbvtProxy*], [3 x %struct.btDbvtProxy*]* %m_stageRoots32, i32 0, i32 2
  call void @_ZL10listappendI11btDbvtProxyEvPT_RS2_(%struct.btDbvtProxy* %19, %struct.btDbvtProxy** nonnull align 4 dereferenceable(4) %arrayidx33)
  %m_sets34 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx35 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets34, i32 0, i32 0
  %20 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %current, align 4
  %leaf = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %20, i32 0, i32 1
  %21 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf, align 4
  call void @_ZN6btDbvt6removeEP10btDbvtNode(%struct.btDbvt* %arrayidx35, %struct.btDbvtNode* %21)
  %22 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %current, align 4
  %23 = bitcast %struct.btDbvtProxy* %22 to %struct.btBroadphaseProxy*
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %23, i32 0, i32 4
  %24 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %current, align 4
  %25 = bitcast %struct.btDbvtProxy* %24 to %struct.btBroadphaseProxy*
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %25, i32 0, i32 5
  call void @_ZN12btDbvtAabbMm6FromMMERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %curAabb, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_aabbMax)
  %m_sets36 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx37 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets36, i32 0, i32 1
  %26 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %current, align 4
  %27 = bitcast %struct.btDbvtProxy* %26 to i8*
  %call38 = call %struct.btDbvtNode* @_ZN6btDbvt6insertERK12btDbvtAabbMmPv(%struct.btDbvt* %arrayidx37, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %curAabb, i8* %27)
  %28 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %current, align 4
  %leaf39 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %28, i32 0, i32 1
  store %struct.btDbvtNode* %call38, %struct.btDbvtNode** %leaf39, align 4
  %29 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %current, align 4
  %stage40 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %29, i32 0, i32 3
  store i32 2, i32* %stage40, align 4
  %30 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %next, align 4
  store %struct.btDbvtProxy* %30, %struct.btDbvtProxy** %current, align 4
  br label %do.cond

do.cond:                                          ; preds = %do.body
  %31 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %current, align 4
  %tobool41 = icmp ne %struct.btDbvtProxy* %31, null
  br i1 %tobool41, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %m_sets42 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx43 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets42, i32 0, i32 1
  %m_leaves44 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx43, i32 0, i32 3
  %32 = load i32, i32* %m_leaves44, align 4
  %m_fixedleft45 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 10
  store i32 %32, i32* %m_fixedleft45, align 4
  %m_needcleanup = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 19
  store i8 1, i8* %m_needcleanup, align 2
  %call46 = call %struct.btDbvtTreeCollider* @_ZN18btDbvtTreeColliderD2Ev(%struct.btDbvtTreeCollider* %collider) #8
  br label %if.end47

if.end47:                                         ; preds = %do.end, %if.end
  %call49 = call %struct.btDbvtTreeCollider* @_ZN18btDbvtTreeColliderC2EP16btDbvtBroadphase(%struct.btDbvtTreeCollider* %collider48, %struct.btDbvtBroadphase* %this1)
  %m_deferedcollide = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 18
  %33 = load i8, i8* %m_deferedcollide, align 1
  %tobool50 = trunc i8 %33 to i1
  br i1 %tobool50, label %if.then51, label %if.end59

if.then51:                                        ; preds = %if.end47
  %m_sets52 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx53 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets52, i32 0, i32 0
  %m_sets54 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx55 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets54, i32 0, i32 0
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx55, i32 0, i32 0
  %34 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %m_sets56 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx57 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets56, i32 0, i32 1
  %m_root58 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx57, i32 0, i32 0
  %35 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root58, align 4
  %36 = bitcast %struct.btDbvtTreeCollider* %collider48 to %"struct.btDbvt::ICollide"*
  call void @_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE(%struct.btDbvt* %arrayidx53, %struct.btDbvtNode* %34, %struct.btDbvtNode* %35, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %36)
  br label %if.end59

if.end59:                                         ; preds = %if.then51, %if.end47
  %m_deferedcollide60 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 18
  %37 = load i8, i8* %m_deferedcollide60, align 1
  %tobool61 = trunc i8 %37 to i1
  br i1 %tobool61, label %if.then62, label %if.end71

if.then62:                                        ; preds = %if.end59
  %m_sets63 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx64 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets63, i32 0, i32 0
  %m_sets65 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx66 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets65, i32 0, i32 0
  %m_root67 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx66, i32 0, i32 0
  %38 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root67, align 4
  %m_sets68 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx69 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets68, i32 0, i32 0
  %m_root70 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx69, i32 0, i32 0
  %39 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root70, align 4
  %40 = bitcast %struct.btDbvtTreeCollider* %collider48 to %"struct.btDbvt::ICollide"*
  call void @_ZN6btDbvt24collideTTpersistentStackEPK10btDbvtNodeS2_RNS_8ICollideE(%struct.btDbvt* %arrayidx64, %struct.btDbvtNode* %38, %struct.btDbvtNode* %39, %"struct.btDbvt::ICollide"* nonnull align 4 dereferenceable(4) %40)
  br label %if.end71

if.end71:                                         ; preds = %if.then62, %if.end59
  %call72 = call %struct.btDbvtTreeCollider* @_ZN18btDbvtTreeColliderD2Ev(%struct.btDbvtTreeCollider* %collider48) #8
  %m_needcleanup73 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 19
  %41 = load i8, i8* %m_needcleanup73, align 2
  %tobool74 = trunc i8 %41 to i1
  br i1 %tobool74, label %if.then75, label %if.end114

if.then75:                                        ; preds = %if.end71
  %m_paircache = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 3
  %42 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_paircache, align 4
  %43 = bitcast %class.btOverlappingPairCache* %42 to %class.btAlignedObjectArray.9* (%class.btOverlappingPairCache*)***
  %vtable = load %class.btAlignedObjectArray.9* (%class.btOverlappingPairCache*)**, %class.btAlignedObjectArray.9* (%class.btOverlappingPairCache*)*** %43, align 4
  %vfn = getelementptr inbounds %class.btAlignedObjectArray.9* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray.9* (%class.btOverlappingPairCache*)** %vtable, i64 7
  %44 = load %class.btAlignedObjectArray.9* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray.9* (%class.btOverlappingPairCache*)** %vfn, align 4
  %call76 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.9* %44(%class.btOverlappingPairCache* %42)
  store %class.btAlignedObjectArray.9* %call76, %class.btAlignedObjectArray.9** %pairs, align 4
  %45 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %pairs, align 4
  %call77 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.9* %45)
  %cmp = icmp sgt i32 %call77, 0
  br i1 %cmp, label %if.then78, label %if.end113

if.then78:                                        ; preds = %if.then75
  %46 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %pairs, align 4
  %call80 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.9* %46)
  store i32 %call80, i32* %ref.tmp79, align 4
  %m_newpairs = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 9
  %47 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %pairs, align 4
  %call82 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.9* %47)
  %m_cupdates = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 8
  %48 = load i32, i32* %m_cupdates, align 4
  %mul83 = mul nsw i32 %call82, %48
  %div84 = sdiv i32 %mul83, 100
  store i32 %div84, i32* %ref.tmp81, align 4
  %call85 = call nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %m_newpairs, i32* nonnull align 4 dereferenceable(4) %ref.tmp81)
  %call86 = call nonnull align 4 dereferenceable(4) i32* @_Z5btMinIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %ref.tmp79, i32* nonnull align 4 dereferenceable(4) %call85)
  %49 = load i32, i32* %call86, align 4
  store i32 %49, i32* %ni, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then78
  %50 = load i32, i32* %i, align 4
  %51 = load i32, i32* %ni, align 4
  %cmp87 = icmp slt i32 %50, %51
  br i1 %cmp87, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %52 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %pairs, align 4
  %m_cid = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 15
  %53 = load i32, i32* %m_cid, align 4
  %54 = load i32, i32* %i, align 4
  %add88 = add nsw i32 %53, %54
  %55 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %pairs, align 4
  %call89 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.9* %55)
  %rem90 = srem i32 %add88, %call89
  %call91 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.9* %52, i32 %rem90)
  store %struct.btBroadphasePair* %call91, %struct.btBroadphasePair** %p, align 4
  %56 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %p, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %56, i32 0, i32 0
  %57 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %58 = bitcast %struct.btBroadphaseProxy* %57 to %struct.btDbvtProxy*
  store %struct.btDbvtProxy* %58, %struct.btDbvtProxy** %pa, align 4
  %59 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %p, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %59, i32 0, i32 1
  %60 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %61 = bitcast %struct.btBroadphaseProxy* %60 to %struct.btDbvtProxy*
  store %struct.btDbvtProxy* %61, %struct.btDbvtProxy** %pb, align 4
  %62 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %pa, align 4
  %leaf92 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %62, i32 0, i32 1
  %63 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf92, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %63, i32 0, i32 0
  %64 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %pb, align 4
  %leaf93 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %64, i32 0, i32 1
  %65 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf93, align 4
  %volume94 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %65, i32 0, i32 0
  %call95 = call zeroext i1 @_Z9IntersectRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume94)
  br i1 %call95, label %if.end102, label %if.then96

if.then96:                                        ; preds = %for.body
  %m_paircache97 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 3
  %66 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_paircache97, align 4
  %67 = bitcast %class.btOverlappingPairCache* %66 to %class.btOverlappingPairCallback*
  %68 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %pa, align 4
  %69 = bitcast %struct.btDbvtProxy* %68 to %struct.btBroadphaseProxy*
  %70 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %pb, align 4
  %71 = bitcast %struct.btDbvtProxy* %70 to %struct.btBroadphaseProxy*
  %72 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %73 = bitcast %class.btOverlappingPairCallback* %67 to i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)***
  %vtable98 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)**, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*** %73, align 4
  %vfn99 = getelementptr inbounds i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vtable98, i64 3
  %74 = load i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)*, i8* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btDispatcher*)** %vfn99, align 4
  %call100 = call i8* %74(%class.btOverlappingPairCallback* %67, %struct.btBroadphaseProxy* %69, %struct.btBroadphaseProxy* %71, %class.btDispatcher* %72)
  %75 = load i32, i32* %ni, align 4
  %dec = add nsw i32 %75, -1
  store i32 %dec, i32* %ni, align 4
  %76 = load i32, i32* %i, align 4
  %dec101 = add nsw i32 %76, -1
  store i32 %dec101, i32* %i, align 4
  br label %if.end102

if.end102:                                        ; preds = %if.then96, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end102
  %77 = load i32, i32* %i, align 4
  %inc = add nsw i32 %77, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %78 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %pairs, align 4
  %call103 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.9* %78)
  %cmp104 = icmp sgt i32 %call103, 0
  br i1 %cmp104, label %if.then105, label %if.else

if.then105:                                       ; preds = %for.end
  %m_cid106 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 15
  %79 = load i32, i32* %m_cid106, align 4
  %80 = load i32, i32* %ni, align 4
  %add107 = add nsw i32 %79, %80
  %81 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %pairs, align 4
  %call108 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.9* %81)
  %rem109 = srem i32 %add107, %call108
  %m_cid110 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 15
  store i32 %rem109, i32* %m_cid110, align 4
  br label %if.end112

if.else:                                          ; preds = %for.end
  %m_cid111 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 15
  store i32 0, i32* %m_cid111, align 4
  br label %if.end112

if.end112:                                        ; preds = %if.else, %if.then105
  br label %if.end113

if.end113:                                        ; preds = %if.end112, %if.then75
  br label %if.end114

if.end114:                                        ; preds = %if.end113, %if.end71
  %m_pid = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 14
  %82 = load i32, i32* %m_pid, align 4
  %inc115 = add nsw i32 %82, 1
  store i32 %inc115, i32* %m_pid, align 4
  %m_newpairs116 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 9
  store i32 1, i32* %m_newpairs116, align 4
  %m_needcleanup117 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 19
  store i8 0, i8* %m_needcleanup117, align 2
  %m_updates_call = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 11
  %83 = load i32, i32* %m_updates_call, align 4
  %cmp118 = icmp ugt i32 %83, 0
  br i1 %cmp118, label %if.then119, label %if.else123

if.then119:                                       ; preds = %if.end114
  %m_updates_done = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 12
  %84 = load i32, i32* %m_updates_done, align 4
  %conv = uitofp i32 %84 to float
  %m_updates_call120 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 11
  %85 = load i32, i32* %m_updates_call120, align 4
  %conv121 = uitofp i32 %85 to float
  %div122 = fdiv float %conv, %conv121
  %m_updates_ratio = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 13
  store float %div122, float* %m_updates_ratio, align 4
  br label %if.end125

if.else123:                                       ; preds = %if.end114
  %m_updates_ratio124 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 13
  store float 0.000000e+00, float* %m_updates_ratio124, align 4
  br label %if.end125

if.end125:                                        ; preds = %if.else123, %if.then119
  %m_updates_done126 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 12
  %86 = load i32, i32* %m_updates_done126, align 4
  %div127 = udiv i32 %86, 2
  store i32 %div127, i32* %m_updates_done126, align 4
  %m_updates_call128 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 11
  %87 = load i32, i32* %m_updates_call128, align 4
  %div129 = udiv i32 %87, 2
  store i32 %div129, i32* %m_updates_call128, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btDbvtBroadphase22performDeferredRemovalEP12btDispatcher(%struct.btDbvtBroadphase* %this, %class.btDispatcher* %dispatcher) #2 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %overlappingPairArray = alloca %class.btAlignedObjectArray.9*, align 4
  %ref.tmp = alloca %class.btBroadphasePairSortPredicate, align 1
  %invalidPair = alloca i32, align 4
  %i = alloca i32, align 4
  %previousPair = alloca %struct.btBroadphasePair, align 4
  %pair = alloca %struct.btBroadphasePair*, align 4
  %isDuplicate = alloca i8, align 1
  %needsRemoval = alloca i8, align 1
  %pa = alloca %struct.btDbvtProxy*, align 4
  %pb = alloca %struct.btDbvtProxy*, align 4
  %hasOverlap = alloca i8, align 1
  %ref.tmp30 = alloca %class.btBroadphasePairSortPredicate, align 1
  %ref.tmp32 = alloca %struct.btBroadphasePair, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %m_paircache = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 3
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_paircache, align 4
  %1 = bitcast %class.btOverlappingPairCache* %0 to i1 (%class.btOverlappingPairCache*)***
  %vtable = load i1 (%class.btOverlappingPairCache*)**, i1 (%class.btOverlappingPairCache*)*** %1, align 4
  %vfn = getelementptr inbounds i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vtable, i64 14
  %2 = load i1 (%class.btOverlappingPairCache*)*, i1 (%class.btOverlappingPairCache*)** %vfn, align 4
  %call = call zeroext i1 %2(%class.btOverlappingPairCache* %0)
  br i1 %call, label %if.then, label %if.end34

if.then:                                          ; preds = %entry
  %m_paircache2 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 3
  %3 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_paircache2, align 4
  %4 = bitcast %class.btOverlappingPairCache* %3 to %class.btAlignedObjectArray.9* (%class.btOverlappingPairCache*)***
  %vtable3 = load %class.btAlignedObjectArray.9* (%class.btOverlappingPairCache*)**, %class.btAlignedObjectArray.9* (%class.btOverlappingPairCache*)*** %4, align 4
  %vfn4 = getelementptr inbounds %class.btAlignedObjectArray.9* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray.9* (%class.btOverlappingPairCache*)** %vtable3, i64 7
  %5 = load %class.btAlignedObjectArray.9* (%class.btOverlappingPairCache*)*, %class.btAlignedObjectArray.9* (%class.btOverlappingPairCache*)** %vfn4, align 4
  %call5 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.9* %5(%class.btOverlappingPairCache* %3)
  store %class.btAlignedObjectArray.9* %call5, %class.btAlignedObjectArray.9** %overlappingPairArray, align 4
  %6 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %overlappingPairArray, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.9* %6, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  store i32 0, i32* %invalidPair, align 4
  %call6 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %previousPair)
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %previousPair, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %7 = load i32, i32* %i, align 4
  %8 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %overlappingPairArray, align 4
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.9* %8)
  %cmp = icmp slt i32 %7, %call7
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %9 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %overlappingPairArray, align 4
  %10 = load i32, i32* %i, align 4
  %call8 = call nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.9* %9, i32 %10)
  store %struct.btBroadphasePair* %call8, %struct.btBroadphasePair** %pair, align 4
  %11 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %call9 = call zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %11, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %previousPair)
  %frombool = zext i1 %call9 to i8
  store i8 %frombool, i8* %isDuplicate, align 1
  %12 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %13 = bitcast %struct.btBroadphasePair* %previousPair to i8*
  %14 = bitcast %struct.btBroadphasePair* %12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false)
  store i8 0, i8* %needsRemoval, align 1
  %15 = load i8, i8* %isDuplicate, align 1
  %tobool = trunc i8 %15 to i1
  br i1 %tobool, label %if.else19, label %if.then10

if.then10:                                        ; preds = %for.body
  %16 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy011 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %16, i32 0, i32 0
  %17 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy011, align 4
  %18 = bitcast %struct.btBroadphaseProxy* %17 to %struct.btDbvtProxy*
  store %struct.btDbvtProxy* %18, %struct.btDbvtProxy** %pa, align 4
  %19 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy112 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %19, i32 0, i32 1
  %20 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy112, align 4
  %21 = bitcast %struct.btBroadphaseProxy* %20 to %struct.btDbvtProxy*
  store %struct.btDbvtProxy* %21, %struct.btDbvtProxy** %pb, align 4
  %22 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %pa, align 4
  %leaf = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %22, i32 0, i32 1
  %23 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %23, i32 0, i32 0
  %24 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %pb, align 4
  %leaf13 = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %24, i32 0, i32 1
  %25 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf13, align 4
  %volume14 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %25, i32 0, i32 0
  %call15 = call zeroext i1 @_Z9IntersectRK12btDbvtAabbMmS1_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume14)
  %frombool16 = zext i1 %call15 to i8
  store i8 %frombool16, i8* %hasOverlap, align 1
  %26 = load i8, i8* %hasOverlap, align 1
  %tobool17 = trunc i8 %26 to i1
  br i1 %tobool17, label %if.then18, label %if.else

if.then18:                                        ; preds = %if.then10
  store i8 0, i8* %needsRemoval, align 1
  br label %if.end

if.else:                                          ; preds = %if.then10
  store i8 1, i8* %needsRemoval, align 1
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then18
  br label %if.end20

if.else19:                                        ; preds = %for.body
  store i8 1, i8* %needsRemoval, align 1
  br label %if.end20

if.end20:                                         ; preds = %if.else19, %if.end
  %27 = load i8, i8* %needsRemoval, align 1
  %tobool21 = trunc i8 %27 to i1
  br i1 %tobool21, label %if.then22, label %if.end28

if.then22:                                        ; preds = %if.end20
  %m_paircache23 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 3
  %28 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_paircache23, align 4
  %29 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %30 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %31 = bitcast %class.btOverlappingPairCache* %28 to void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)***
  %vtable24 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)**, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*** %31, align 4
  %vfn25 = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vtable24, i64 8
  %32 = load void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btBroadphasePair*, %class.btDispatcher*)** %vfn25, align 4
  call void %32(%class.btOverlappingPairCache* %28, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %29, %class.btDispatcher* %30)
  %33 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy026 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %33, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy026, align 4
  %34 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair, align 4
  %m_pProxy127 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %34, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy127, align 4
  %35 = load i32, i32* %invalidPair, align 4
  %inc = add nsw i32 %35, 1
  store i32 %inc, i32* %invalidPair, align 4
  br label %if.end28

if.end28:                                         ; preds = %if.then22, %if.end20
  br label %for.inc

for.inc:                                          ; preds = %if.end28
  %36 = load i32, i32* %i, align 4
  %inc29 = add nsw i32 %36, 1
  store i32 %inc29, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %37 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %overlappingPairArray, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.9* %37, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp30)
  %38 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %overlappingPairArray, align 4
  %39 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %overlappingPairArray, align 4
  %call31 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.9* %39)
  %40 = load i32, i32* %invalidPair, align 4
  %sub = sub nsw i32 %call31, %40
  %call33 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* %ref.tmp32)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.9* %38, i32 %sub, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %ref.tmp32)
  br label %if.end34

if.end34:                                         ; preds = %for.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvRKT_(%class.btAlignedObjectArray.9* %this, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %CompareFunc.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store %class.btBroadphasePairSortPredicate* %CompareFunc, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.9* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2Ev(%struct.btBroadphasePair* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  store %struct.btBroadphaseProxy* null, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon.12* %0 to i8**
  store i8* null, i8** %m_internalInfo1, align 4
  ret %struct.btBroadphasePair* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.btBroadphasePair* @_ZN20btAlignedObjectArrayI16btBroadphasePairEixEi(%class.btAlignedObjectArray.9* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  ret %struct.btBroadphasePair* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZeqRK16btBroadphasePairS1_(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %a.addr = alloca %struct.btBroadphasePair*, align 4
  %b.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %a, %struct.btBroadphasePair** %a.addr, align 4
  store %struct.btBroadphasePair* %b, %struct.btBroadphasePair** %b.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy01 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 0
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy01, align 4
  %cmp = icmp eq %struct.btBroadphaseProxy* %1, %3
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy12 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %6, i32 0, i32 1
  %7 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy12, align 4
  %cmp3 = icmp eq %struct.btBroadphaseProxy* %5, %7
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %8 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %8
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_(%class.btAlignedObjectArray.9* %this, i32 %newsize, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btBroadphasePair*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btBroadphasePair* %fillData, %struct.btBroadphasePair** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end16

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray.9* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc13, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end15

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %14 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %14, i32 %15
  %16 = bitcast %struct.btBroadphasePair* %arrayidx10 to i8*
  %call11 = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %16)
  %17 = bitcast i8* %call11 to %struct.btBroadphasePair*
  %18 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %fillData.addr, align 4
  %call12 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %17, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %18)
  br label %for.inc13

for.inc13:                                        ; preds = %for.body8
  %19 = load i32, i32* %i5, align 4
  %inc14 = add nsw i32 %19, 1
  store i32 %inc14, i32* %i5, align 4
  br label %for.cond6

for.end15:                                        ; preds = %for.cond6
  br label %if.end16

if.end16:                                         ; preds = %for.end15, %for.end
  %20 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 2
  store i32 %20, i32* %m_size, align 4
  ret void
}

declare void @_ZN6btDbvt19optimizeIncrementalEi(%struct.btDbvt*, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_Z5btMaxIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %a, i32* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca i32*, align 4
  %b.addr = alloca i32*, align 4
  store i32* %a, i32** %a.addr, align 4
  store i32* %b, i32** %b.addr, align 4
  %0 = load i32*, i32** %a.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %b.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp sgt i32 %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32*, i32** %a.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load i32*, i32** %b.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %4, %cond.true ], [ %5, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_Z5btMinIiERKT_S2_S2_(i32* nonnull align 4 dereferenceable(4) %a, i32* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca i32*, align 4
  %b.addr = alloca i32*, align 4
  store i32* %a, i32** %a.addr, align 4
  store i32* %b, i32** %b.addr, align 4
  %0 = load i32*, i32** %a.addr, align 4
  %1 = load i32, i32* %0, align 4
  %2 = load i32*, i32** %b.addr, align 4
  %3 = load i32, i32* %2, align 4
  %cmp = icmp slt i32 %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load i32*, i32** %a.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load i32*, i32** %b.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi i32* [ %4, %cond.true ], [ %5, %cond.false ]
  ret i32* %cond-lvalue
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btDbvtBroadphase8optimizeEv(%struct.btDbvtBroadphase* %this) #2 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %m_sets = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets, i32 0, i32 0
  call void @_ZN6btDbvt15optimizeTopDownEi(%struct.btDbvt* %arrayidx, i32 128)
  %m_sets2 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets2, i32 0, i32 1
  call void @_ZN6btDbvt15optimizeTopDownEi(%struct.btDbvt* %arrayidx3, i32 128)
  ret void
}

declare void @_ZN6btDbvt15optimizeTopDownEi(%struct.btDbvt*, i32) #3

; Function Attrs: noinline nounwind optnone
define hidden %class.btOverlappingPairCache* @_ZN16btDbvtBroadphase23getOverlappingPairCacheEv(%struct.btDbvtBroadphase* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %m_paircache = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 3
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_paircache, align 4
  ret %class.btOverlappingPairCache* %0
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btOverlappingPairCache* @_ZNK16btDbvtBroadphase23getOverlappingPairCacheEv(%struct.btDbvtBroadphase* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %m_paircache = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 3
  %0 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_paircache, align 4
  ret %class.btOverlappingPairCache* %0
}

; Function Attrs: noinline optnone
define hidden void @_ZNK16btDbvtBroadphase17getBroadphaseAabbER9btVector3S1_(%struct.btDbvtBroadphase* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %bounds = alloca %struct.btDbvtAabbMm, align 4
  %ref.tmp = alloca %struct.btDbvtAabbMm, align 4
  %ref.tmp27 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %call = call %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* %bounds)
  %m_sets = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets, i32 0, i32 0
  %call2 = call zeroext i1 @_ZNK6btDbvt5emptyEv(%struct.btDbvt* %arrayidx)
  br i1 %call2, label %if.else17, label %if.then

if.then:                                          ; preds = %entry
  %m_sets3 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx4 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets3, i32 0, i32 1
  %call5 = call zeroext i1 @_ZNK6btDbvt5emptyEv(%struct.btDbvt* %arrayidx4)
  br i1 %call5, label %if.else, label %if.then6

if.then6:                                         ; preds = %if.then
  %m_sets7 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx8 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets7, i32 0, i32 0
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx8, i32 0, i32 0
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %volume = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %0, i32 0, i32 0
  %m_sets9 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx10 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets9, i32 0, i32 1
  %m_root11 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx10, i32 0, i32 0
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root11, align 4
  %volume12 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %1, i32 0, i32 0
  call void @_Z5MergeRK12btDbvtAabbMmS1_RS_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %volume12, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %bounds)
  br label %if.end

if.else:                                          ; preds = %if.then
  %m_sets13 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx14 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets13, i32 0, i32 0
  %m_root15 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx14, i32 0, i32 0
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root15, align 4
  %volume16 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %2, i32 0, i32 0
  %3 = bitcast %struct.btDbvtAabbMm* %bounds to i8*
  %4 = bitcast %struct.btDbvtAabbMm* %volume16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 32, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then6
  br label %if.end33

if.else17:                                        ; preds = %entry
  %m_sets18 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx19 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets18, i32 0, i32 1
  %call20 = call zeroext i1 @_ZNK6btDbvt5emptyEv(%struct.btDbvt* %arrayidx19)
  br i1 %call20, label %if.else26, label %if.then21

if.then21:                                        ; preds = %if.else17
  %m_sets22 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx23 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets22, i32 0, i32 1
  %m_root24 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx23, i32 0, i32 0
  %5 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root24, align 4
  %volume25 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %5, i32 0, i32 0
  %6 = bitcast %struct.btDbvtAabbMm* %bounds to i8*
  %7 = bitcast %struct.btDbvtAabbMm* %volume25 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 32, i1 false)
  br label %if.end32

if.else26:                                        ; preds = %if.else17
  store float 0.000000e+00, float* %ref.tmp28, align 4
  store float 0.000000e+00, float* %ref.tmp29, align 4
  store float 0.000000e+00, float* %ref.tmp30, align 4
  %call31 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp30)
  call void @_ZN12btDbvtAabbMm6FromCRERK9btVector3f(%struct.btDbvtAabbMm* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp27, float 0.000000e+00)
  %8 = bitcast %struct.btDbvtAabbMm* %bounds to i8*
  %9 = bitcast %struct.btDbvtAabbMm* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 32, i1 false)
  br label %if.end32

if.end32:                                         ; preds = %if.else26, %if.then21
  br label %if.end33

if.end33:                                         ; preds = %if.end32, %if.end
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btDbvtAabbMm4MinsEv(%struct.btDbvtAabbMm* %bounds)
  %10 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %11 = bitcast %class.btVector3* %10 to i8*
  %12 = bitcast %class.btVector3* %call34 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %11, i8* align 4 %12, i32 16, i1 false)
  %call35 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btDbvtAabbMm4MaxsEv(%struct.btDbvtAabbMm* %bounds)
  %13 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %14 = bitcast %class.btVector3* %13 to i8*
  %15 = bitcast %class.btVector3* %call35 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %14, i8* align 4 %15, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mi)
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mx)
  ret %struct.btDbvtAabbMm* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK6btDbvt5emptyEv(%struct.btDbvt* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvt*, align 4
  store %struct.btDbvt* %this, %struct.btDbvt** %this.addr, align 4
  %this1 = load %struct.btDbvt*, %struct.btDbvt** %this.addr, align 4
  %m_root = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %this1, i32 0, i32 0
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %m_root, align 4
  %cmp = icmp eq %struct.btDbvtNode* null, %0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z5MergeRK12btDbvtAabbMmS1_RS_(%struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %a, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %b, %struct.btDbvtAabbMm* nonnull align 4 dereferenceable(32) %r) #1 comdat {
entry:
  %a.addr = alloca %struct.btDbvtAabbMm*, align 4
  %b.addr = alloca %struct.btDbvtAabbMm*, align 4
  %r.addr = alloca %struct.btDbvtAabbMm*, align 4
  %i = alloca i32, align 4
  store %struct.btDbvtAabbMm* %a, %struct.btDbvtAabbMm** %a.addr, align 4
  store %struct.btDbvtAabbMm* %b, %struct.btDbvtAabbMm** %b.addr, align 4
  store %struct.btDbvtAabbMm* %r, %struct.btDbvtAabbMm** %r.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %1, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mi)
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %2
  %3 = load float, float* %arrayidx, align 4
  %4 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi1 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %4, i32 0, i32 0
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mi1)
  %5 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 %5
  %6 = load float, float* %arrayidx3, align 4
  %cmp4 = fcmp olt float %3, %6
  br i1 %cmp4, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %7 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mi5 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %7, i32 0, i32 0
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mi5)
  %8 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 %8
  %9 = load float, float* %arrayidx7, align 4
  %10 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %r.addr, align 4
  %mi8 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %10, i32 0, i32 0
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %mi8)
  %11 = load i32, i32* %i, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %11
  store float %9, float* %arrayidx10, align 4
  br label %if.end

if.else:                                          ; preds = %for.body
  %12 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mi11 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %12, i32 0, i32 0
  %call12 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mi11)
  %13 = load i32, i32* %i, align 4
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 %13
  %14 = load float, float* %arrayidx13, align 4
  %15 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %r.addr, align 4
  %mi14 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %15, i32 0, i32 0
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %mi14)
  %16 = load i32, i32* %i, align 4
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 %16
  store float %14, float* %arrayidx16, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %17, i32 0, i32 1
  %call17 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mx)
  %18 = load i32, i32* %i, align 4
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 %18
  %19 = load float, float* %arrayidx18, align 4
  %20 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx19 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %20, i32 0, i32 1
  %call20 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mx19)
  %21 = load i32, i32* %i, align 4
  %arrayidx21 = getelementptr inbounds float, float* %call20, i32 %21
  %22 = load float, float* %arrayidx21, align 4
  %cmp22 = fcmp ogt float %19, %22
  br i1 %cmp22, label %if.then23, label %if.else30

if.then23:                                        ; preds = %if.end
  %23 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %a.addr, align 4
  %mx24 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %23, i32 0, i32 1
  %call25 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mx24)
  %24 = load i32, i32* %i, align 4
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 %24
  %25 = load float, float* %arrayidx26, align 4
  %26 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %r.addr, align 4
  %mx27 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %26, i32 0, i32 1
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %mx27)
  %27 = load i32, i32* %i, align 4
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 %27
  store float %25, float* %arrayidx29, align 4
  br label %if.end37

if.else30:                                        ; preds = %if.end
  %28 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %b.addr, align 4
  %mx31 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %28, i32 0, i32 1
  %call32 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %mx31)
  %29 = load i32, i32* %i, align 4
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 %29
  %30 = load float, float* %arrayidx33, align 4
  %31 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %r.addr, align 4
  %mx34 = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %31, i32 0, i32 1
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %mx34)
  %32 = load i32, i32* %i, align 4
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 %32
  store float %30, float* %arrayidx36, align 4
  br label %if.end37

if.end37:                                         ; preds = %if.else30, %if.then23
  br label %for.inc

for.inc:                                          ; preds = %if.end37
  %33 = load i32, i32* %i, align 4
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btDbvtAabbMm6FromCRERK9btVector3f(%struct.btDbvtAabbMm* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %c, float %r) #2 comdat {
entry:
  %c.addr = alloca %class.btVector3*, align 4
  %r.addr = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  store float %r, float* %r.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %r.addr, float* nonnull align 4 dereferenceable(4) %r.addr, float* nonnull align 4 dereferenceable(4) %r.addr)
  call void @_ZN12btDbvtAabbMm6FromCEERK9btVector3S2_(%struct.btDbvtAabbMm* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btDbvtAabbMm4MinsEv(%struct.btDbvtAabbMm* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 0
  ret %class.btVector3* %mi
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btDbvtAabbMm4MaxsEv(%struct.btDbvtAabbMm* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtAabbMm*, align 4
  store %struct.btDbvtAabbMm* %this, %struct.btDbvtAabbMm** %this.addr, align 4
  %this1 = load %struct.btDbvtAabbMm*, %struct.btDbvtAabbMm** %this.addr, align 4
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %this1, i32 0, i32 1
  ret %class.btVector3* %mx
}

; Function Attrs: noinline optnone
define hidden void @_ZN16btDbvtBroadphase9resetPoolEP12btDispatcher(%struct.btDbvtBroadphase* %this, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %totalObjects = alloca i32, align 4
  %i = alloca i32, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  %m_sets = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets, i32 0, i32 0
  %m_leaves = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx, i32 0, i32 3
  %0 = load i32, i32* %m_leaves, align 4
  %m_sets2 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets2, i32 0, i32 1
  %m_leaves4 = getelementptr inbounds %struct.btDbvt, %struct.btDbvt* %arrayidx3, i32 0, i32 3
  %1 = load i32, i32* %m_leaves4, align 4
  %add = add nsw i32 %0, %1
  store i32 %add, i32* %totalObjects, align 4
  %2 = load i32, i32* %totalObjects, align 4
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %m_sets5 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx6 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets5, i32 0, i32 0
  call void @_ZN6btDbvt5clearEv(%struct.btDbvt* %arrayidx6)
  %m_sets7 = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 1
  %arrayidx8 = getelementptr inbounds [2 x %struct.btDbvt], [2 x %struct.btDbvt]* %m_sets7, i32 0, i32 1
  call void @_ZN6btDbvt5clearEv(%struct.btDbvt* %arrayidx8)
  %m_deferedcollide = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 18
  store i8 0, i8* %m_deferedcollide, align 1
  %m_needcleanup = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 19
  store i8 1, i8* %m_needcleanup, align 2
  %m_stageCurrent = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 5
  store i32 0, i32* %m_stageCurrent, align 4
  %m_fixedleft = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 10
  store i32 0, i32* %m_fixedleft, align 4
  %m_fupdates = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 6
  store i32 1, i32* %m_fupdates, align 4
  %m_dupdates = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 7
  store i32 0, i32* %m_dupdates, align 4
  %m_cupdates = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 8
  store i32 10, i32* %m_cupdates, align 4
  %m_newpairs = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 9
  store i32 1, i32* %m_newpairs, align 4
  %m_updates_call = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 11
  store i32 0, i32* %m_updates_call, align 4
  %m_updates_done = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 12
  store i32 0, i32* %m_updates_done, align 4
  %m_updates_ratio = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 13
  store float 0.000000e+00, float* %m_updates_ratio, align 4
  %m_gid = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 16
  store i32 0, i32* %m_gid, align 4
  %m_pid = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 14
  store i32 0, i32* %m_pid, align 4
  %m_cid = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 15
  store i32 0, i32* %m_cid, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %cmp = icmp sle i32 %3, 2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_stageRoots = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %this1, i32 0, i32 2
  %4 = load i32, i32* %i, align 4
  %arrayidx9 = getelementptr inbounds [3 x %struct.btDbvtProxy*], [3 x %struct.btDbvtProxy*]* %m_stageRoots, i32 0, i32 %4
  store %struct.btDbvtProxy* null, %struct.btDbvtProxy** %arrayidx9, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  ret void
}

declare void @_ZN6btDbvt5clearEv(%struct.btDbvt*) #3

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN16btDbvtBroadphase10printStatsEv(%struct.btDbvtBroadphase* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %struct.btDbvtBroadphase*, align 4
  store %struct.btDbvtBroadphase* %this, %struct.btDbvtBroadphase** %this.addr, align 4
  %this1 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN16btDbvtBroadphase9benchmarkEP21btBroadphaseInterface(%class.btBroadphaseInterface* %0) #1 {
entry:
  %.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %0, %class.btBroadphaseInterface** %.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btBroadphaseInterface* @_ZN21btBroadphaseInterfaceD2Ev(%class.btBroadphaseInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  ret %class.btBroadphaseInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btBroadphaseInterfaceD0Ev(%class.btBroadphaseInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btBroadphaseInterface9resetPoolEP12btDispatcher(%class.btBroadphaseInterface* %this, %class.btDispatcher* %dispatcher) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphaseInterface*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  store %class.btBroadphaseInterface* %this, %class.btBroadphaseInterface** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %this.addr, align 4
  ret void
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN17btBroadphaseProxyC2ERK9btVector3S2_Pvii(%struct.btBroadphaseProxy* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, i8* %userPtr, i32 %collisionFilterGroup, i32 %collisionFilterMask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseProxy*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %userPtr.addr = alloca i8*, align 4
  %collisionFilterGroup.addr = alloca i32, align 4
  %collisionFilterMask.addr = alloca i32, align 4
  store %struct.btBroadphaseProxy* %this, %struct.btBroadphaseProxy** %this.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store i8* %userPtr, i8** %userPtr.addr, align 4
  store i32 %collisionFilterGroup, i32* %collisionFilterGroup.addr, align 4
  store i32 %collisionFilterMask, i32* %collisionFilterMask.addr, align 4
  %this1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %this.addr, align 4
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 0
  %0 = load i8*, i8** %userPtr.addr, align 4
  store i8* %0, i8** %m_clientObject, align 4
  %m_collisionFilterGroup = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 1
  %1 = load i32, i32* %collisionFilterGroup.addr, align 4
  store i32 %1, i32* %m_collisionFilterGroup, align 4
  %m_collisionFilterMask = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 2
  %2 = load i32, i32* %collisionFilterMask.addr, align 4
  store i32 %2, i32* %m_collisionFilterMask, align 4
  %m_aabbMin = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %4 = bitcast %class.btVector3* %m_aabbMin to i8*
  %5 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %m_aabbMax = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %this1, i32 0, i32 5
  %6 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %7 = bitcast %class.btVector3* %m_aabbMax to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %struct.btBroadphaseProxy* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideC2Ev(%"struct.btDbvt::ICollide"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %0 = bitcast %"struct.btDbvt::ICollide"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [9 x i8*] }, { [9 x i8*] }* @_ZTVN6btDbvt8ICollideE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %"struct.btDbvt::ICollide"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btDbvtTreeColliderD0Ev(%struct.btDbvtTreeCollider* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtTreeCollider*, align 4
  store %struct.btDbvtTreeCollider* %this, %struct.btDbvtTreeCollider** %this.addr, align 4
  %this1 = load %struct.btDbvtTreeCollider*, %struct.btDbvtTreeCollider** %this.addr, align 4
  %call = call %struct.btDbvtTreeCollider* @_ZN18btDbvtTreeColliderD2Ev(%struct.btDbvtTreeCollider* %this1) #8
  %0 = bitcast %struct.btDbvtTreeCollider* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNodeS2_(%struct.btDbvtTreeCollider* %this, %struct.btDbvtNode* %na, %struct.btDbvtNode* %nb) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtTreeCollider*, align 4
  %na.addr = alloca %struct.btDbvtNode*, align 4
  %nb.addr = alloca %struct.btDbvtNode*, align 4
  %pa = alloca %struct.btDbvtProxy*, align 4
  %pb = alloca %struct.btDbvtProxy*, align 4
  store %struct.btDbvtTreeCollider* %this, %struct.btDbvtTreeCollider** %this.addr, align 4
  store %struct.btDbvtNode* %na, %struct.btDbvtNode** %na.addr, align 4
  store %struct.btDbvtNode* %nb, %struct.btDbvtNode** %nb.addr, align 4
  %this1 = load %struct.btDbvtTreeCollider*, %struct.btDbvtTreeCollider** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %na.addr, align 4
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %nb.addr, align 4
  %cmp = icmp ne %struct.btDbvtNode* %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %na.addr, align 4
  %3 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %2, i32 0, i32 2
  %data = bitcast %union.anon.0* %3 to i8**
  %4 = load i8*, i8** %data, align 4
  %5 = bitcast i8* %4 to %struct.btDbvtProxy*
  store %struct.btDbvtProxy* %5, %struct.btDbvtProxy** %pa, align 4
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %nb.addr, align 4
  %7 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %6, i32 0, i32 2
  %data2 = bitcast %union.anon.0* %7 to i8**
  %8 = load i8*, i8** %data2, align 4
  %9 = bitcast i8* %8 to %struct.btDbvtProxy*
  store %struct.btDbvtProxy* %9, %struct.btDbvtProxy** %pb, align 4
  %pbp = getelementptr inbounds %struct.btDbvtTreeCollider, %struct.btDbvtTreeCollider* %this1, i32 0, i32 1
  %10 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %pbp, align 4
  %m_paircache = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %10, i32 0, i32 3
  %11 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %m_paircache, align 4
  %12 = bitcast %class.btOverlappingPairCache* %11 to %class.btOverlappingPairCallback*
  %13 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %pa, align 4
  %14 = bitcast %struct.btDbvtProxy* %13 to %struct.btBroadphaseProxy*
  %15 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %pb, align 4
  %16 = bitcast %struct.btDbvtProxy* %15 to %struct.btBroadphaseProxy*
  %17 = bitcast %class.btOverlappingPairCallback* %12 to %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)***
  %vtable = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)**, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*** %17, align 4
  %vfn = getelementptr inbounds %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %18 = load %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)*, %struct.btBroadphasePair* (%class.btOverlappingPairCallback*, %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call = call %struct.btBroadphasePair* %18(%class.btOverlappingPairCallback* %12, %struct.btBroadphaseProxy* %14, %struct.btBroadphaseProxy* %16)
  %pbp3 = getelementptr inbounds %struct.btDbvtTreeCollider, %struct.btDbvtTreeCollider* %this1, i32 0, i32 1
  %19 = load %struct.btDbvtBroadphase*, %struct.btDbvtBroadphase** %pbp3, align 4
  %m_newpairs = getelementptr inbounds %struct.btDbvtBroadphase, %struct.btDbvtBroadphase* %19, i32 0, i32 9
  %20 = load i32, i32* %m_newpairs, align 4
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %m_newpairs, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btDbvtTreeCollider7ProcessEPK10btDbvtNode(%struct.btDbvtTreeCollider* %this, %struct.btDbvtNode* %n) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtTreeCollider*, align 4
  %n.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtTreeCollider* %this, %struct.btDbvtTreeCollider** %this.addr, align 4
  store %struct.btDbvtNode* %n, %struct.btDbvtNode** %n.addr, align 4
  %this1 = load %struct.btDbvtTreeCollider*, %struct.btDbvtTreeCollider** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %proxy = getelementptr inbounds %struct.btDbvtTreeCollider, %struct.btDbvtTreeCollider* %this1, i32 0, i32 2
  %1 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %leaf = getelementptr inbounds %struct.btDbvtProxy, %struct.btDbvtProxy* %1, i32 0, i32 1
  %2 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf, align 4
  %3 = bitcast %struct.btDbvtTreeCollider* %this1 to void (%struct.btDbvtTreeCollider*, %struct.btDbvtNode*, %struct.btDbvtNode*)***
  %vtable = load void (%struct.btDbvtTreeCollider*, %struct.btDbvtNode*, %struct.btDbvtNode*)**, void (%struct.btDbvtTreeCollider*, %struct.btDbvtNode*, %struct.btDbvtNode*)*** %3, align 4
  %vfn = getelementptr inbounds void (%struct.btDbvtTreeCollider*, %struct.btDbvtNode*, %struct.btDbvtNode*)*, void (%struct.btDbvtTreeCollider*, %struct.btDbvtNode*, %struct.btDbvtNode*)** %vtable, i64 2
  %4 = load void (%struct.btDbvtTreeCollider*, %struct.btDbvtNode*, %struct.btDbvtNode*)*, void (%struct.btDbvtTreeCollider*, %struct.btDbvtNode*, %struct.btDbvtNode*)** %vfn, align 4
  call void %4(%struct.btDbvtTreeCollider* %this1, %struct.btDbvtNode* %0, %struct.btDbvtNode* %2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodef(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %n, float %0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %n.addr = alloca %struct.btDbvtNode*, align 4
  %.addr = alloca float, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %n, %struct.btDbvtNode** %n.addr, align 4
  store float %0, float* %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %n.addr, align 4
  %2 = bitcast %"struct.btDbvt::ICollide"* %this1 to void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)***
  %vtable = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)**, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*** %2, align 4
  %vfn = getelementptr inbounds void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vtable, i64 3
  %3 = load void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)*, void (%"struct.btDbvt::ICollide"*, %struct.btDbvtNode*)** %vfn, align 4
  call void %3(%"struct.btDbvt::ICollide"* %this1, %struct.btDbvtNode* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN6btDbvt8ICollide7DescentEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN6btDbvt8ICollide9AllLeavesEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret %"struct.btDbvt::ICollide"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollideD0Ev(%"struct.btDbvt::ICollide"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  %call = call %"struct.btDbvt::ICollide"* @_ZN6btDbvt8ICollideD2Ev(%"struct.btDbvt::ICollide"* %this1) #8
  %0 = bitcast %"struct.btDbvt::ICollide"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNodeS3_(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0, %struct.btDbvtNode* %1) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  %.addr1 = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  store %struct.btDbvtNode* %1, %struct.btDbvtNode** %.addr1, align 4
  %this2 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN6btDbvt8ICollide7ProcessEPK10btDbvtNode(%"struct.btDbvt::ICollide"* %this, %struct.btDbvtNode* %0) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::ICollide"*, align 4
  %.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::ICollide"* %this, %"struct.btDbvt::ICollide"** %this.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %.addr, align 4
  %this1 = load %"struct.btDbvt::ICollide"*, %"struct.btDbvt::ICollide"** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE6resizeEiRKS2_(%class.btAlignedObjectArray.4* %this, i32 %newsize, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %struct.btDbvtNode**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %struct.btDbvtNode** %fillData, %struct.btDbvtNode*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %5 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %14 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %14, i32 %15
  %16 = bitcast %struct.btDbvtNode** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %struct.btDbvtNode**
  %18 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %fillData.addr, align 4
  %19 = load %struct.btDbvtNode*, %struct.btDbvtNode** %18, align 4
  store %struct.btDbvtNode* %19, %struct.btDbvtNode** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btDbvtNode**, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btDbvtNode**
  store %struct.btDbvtNode** %2, %struct.btDbvtNode*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %3 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, %struct.btDbvtNode** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %struct.btDbvtNode** %4, %struct.btDbvtNode*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9push_backERKS2_(%class.btAlignedObjectArray.4* %this, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Val.addr = alloca %struct.btDbvtNode**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store %struct.btDbvtNode** %_Val, %struct.btDbvtNode*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9allocSizeEi(%class.btAlignedObjectArray.4* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %1 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %1, i32 %2
  %3 = bitcast %struct.btDbvtNode** %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.btDbvtNode**
  %5 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %_Val.addr, align 4
  %6 = load %struct.btDbvtNode*, %struct.btDbvtNode** %5, align 4
  store %struct.btDbvtNode* %6, %struct.btDbvtNode** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %struct.btDbvtNode** @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %0, i32 %1
  ret %struct.btDbvtNode** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8pop_backEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %1 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %1, i32 %2
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode10isinternalEv(%struct.btDbvtNode* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %call = call zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this1)
  %lnot = xor i1 %call, true
  ret i1 %lnot
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE8capacityEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btDbvtNode** @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.5* %m_allocator, i32 %1, %struct.btDbvtNode*** null)
  %2 = bitcast %struct.btDbvtNode** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, %struct.btDbvtNode** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btDbvtNode**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btDbvtNode** %dest, %struct.btDbvtNode*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %3, i32 %4
  %5 = bitcast %struct.btDbvtNode** %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btDbvtNode**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %7 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %7, i32 %8
  %9 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx2, align 4
  store %struct.btDbvtNode* %9, %struct.btDbvtNode** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btDbvtNode*, %struct.btDbvtNode** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  %tobool = icmp ne %struct.btDbvtNode** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE10deallocateEPS2_(%class.btAlignedAllocator.5* %m_allocator, %struct.btDbvtNode** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %struct.btDbvtNode** null, %struct.btDbvtNode*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btDbvtNode** @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.5* %this, i32 %n, %struct.btDbvtNode*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btDbvtNode***, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btDbvtNode*** %hint, %struct.btDbvtNode**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btDbvtNode**
  ret %struct.btDbvtNode** %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EE10deallocateEPS2_(%class.btAlignedAllocator.5* %this, %struct.btDbvtNode** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca %struct.btDbvtNode**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store %struct.btDbvtNode** %ptr, %struct.btDbvtNode*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %ptr.addr, align 4
  %1 = bitcast %struct.btDbvtNode** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE9allocSizeEi(%class.btAlignedObjectArray.4* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK10btDbvtNode6isleafEv(%struct.btDbvtNode* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btDbvtNode*, align 4
  store %struct.btDbvtNode* %this, %struct.btDbvtNode** %this.addr, align 4
  %this1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %this1, i32 0, i32 2
  %childs = bitcast %union.anon.0* %0 to [2 x %struct.btDbvtNode*]*
  %arrayidx = getelementptr inbounds [2 x %struct.btDbvtNode*], [2 x %struct.btDbvtNode*]* %childs, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %arrayidx, align 4
  %cmp = icmp eq %struct.btDbvtNode* %1, null
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN19BroadphaseRayTesterD0Ev(%struct.BroadphaseRayTester* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.BroadphaseRayTester*, align 4
  store %struct.BroadphaseRayTester* %this, %struct.BroadphaseRayTester** %this.addr, align 4
  %this1 = load %struct.BroadphaseRayTester*, %struct.BroadphaseRayTester** %this.addr, align 4
  %call = call %struct.BroadphaseRayTester* @_ZN19BroadphaseRayTesterD2Ev(%struct.BroadphaseRayTester* %this1) #8
  %0 = bitcast %struct.BroadphaseRayTester* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN19BroadphaseRayTester7ProcessEPK10btDbvtNode(%struct.BroadphaseRayTester* %this, %struct.btDbvtNode* %leaf) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.BroadphaseRayTester*, align 4
  %leaf.addr = alloca %struct.btDbvtNode*, align 4
  %proxy = alloca %struct.btDbvtProxy*, align 4
  store %struct.BroadphaseRayTester* %this, %struct.BroadphaseRayTester** %this.addr, align 4
  store %struct.btDbvtNode* %leaf, %struct.btDbvtNode** %leaf.addr, align 4
  %this1 = load %struct.BroadphaseRayTester*, %struct.BroadphaseRayTester** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %1 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %0, i32 0, i32 2
  %data = bitcast %union.anon.0* %1 to i8**
  %2 = load i8*, i8** %data, align 4
  %3 = bitcast i8* %2 to %struct.btDbvtProxy*
  store %struct.btDbvtProxy* %3, %struct.btDbvtProxy** %proxy, align 4
  %m_rayCallback = getelementptr inbounds %struct.BroadphaseRayTester, %struct.BroadphaseRayTester* %this1, i32 0, i32 1
  %4 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %m_rayCallback, align 4
  %5 = bitcast %struct.btBroadphaseRayCallback* %4 to %struct.btBroadphaseAabbCallback*
  %6 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %7 = bitcast %struct.btDbvtProxy* %6 to %struct.btBroadphaseProxy*
  %8 = bitcast %struct.btBroadphaseAabbCallback* %5 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %8, align 4
  %vfn = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %9 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call = call zeroext i1 %9(%struct.btBroadphaseAabbCallback* %5, %struct.btBroadphaseProxy* %7)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_Z10btRayAabb2RK9btVector3S1_PKjPS0_Rfff(%class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayInvDirection, i32* %raySign, %class.btVector3* %bounds, float* nonnull align 4 dereferenceable(4) %tmin, float %lambda_min, float %lambda_max) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayInvDirection.addr = alloca %class.btVector3*, align 4
  %raySign.addr = alloca i32*, align 4
  %bounds.addr = alloca %class.btVector3*, align 4
  %tmin.addr = alloca float*, align 4
  %lambda_min.addr = alloca float, align 4
  %lambda_max.addr = alloca float, align 4
  %tmax = alloca float, align 4
  %tymin = alloca float, align 4
  %tymax = alloca float, align 4
  %tzmin = alloca float, align 4
  %tzmax = alloca float, align 4
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4
  store %class.btVector3* %rayInvDirection, %class.btVector3** %rayInvDirection.addr, align 4
  store i32* %raySign, i32** %raySign.addr, align 4
  store %class.btVector3* %bounds, %class.btVector3** %bounds.addr, align 4
  store float* %tmin, float** %tmin.addr, align 4
  store float %lambda_min, float* %lambda_min.addr, align 4
  store float %lambda_max, float* %lambda_max.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4
  %1 = load i32*, i32** %raySign.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 0
  %2 = load i32, i32* %arrayidx, align 4
  %arrayidx1 = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %arrayidx1)
  %3 = load float, float* %call, align 4
  %4 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %4)
  %5 = load float, float* %call2, align 4
  %sub = fsub float %3, %5
  %6 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %6)
  %7 = load float, float* %call3, align 4
  %mul = fmul float %sub, %7
  %8 = load float*, float** %tmin.addr, align 4
  store float %mul, float* %8, align 4
  %9 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4
  %10 = load i32*, i32** %raySign.addr, align 4
  %arrayidx4 = getelementptr inbounds i32, i32* %10, i32 0
  %11 = load i32, i32* %arrayidx4, align 4
  %sub5 = sub i32 1, %11
  %arrayidx6 = getelementptr inbounds %class.btVector3, %class.btVector3* %9, i32 %sub5
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %arrayidx6)
  %12 = load float, float* %call7, align 4
  %13 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %13)
  %14 = load float, float* %call8, align 4
  %sub9 = fsub float %12, %14
  %15 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %15)
  %16 = load float, float* %call10, align 4
  %mul11 = fmul float %sub9, %16
  store float %mul11, float* %tmax, align 4
  %17 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4
  %18 = load i32*, i32** %raySign.addr, align 4
  %arrayidx12 = getelementptr inbounds i32, i32* %18, i32 1
  %19 = load i32, i32* %arrayidx12, align 4
  %arrayidx13 = getelementptr inbounds %class.btVector3, %class.btVector3* %17, i32 %19
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %arrayidx13)
  %20 = load float, float* %call14, align 4
  %21 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %21)
  %22 = load float, float* %call15, align 4
  %sub16 = fsub float %20, %22
  %23 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %23)
  %24 = load float, float* %call17, align 4
  %mul18 = fmul float %sub16, %24
  store float %mul18, float* %tymin, align 4
  %25 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4
  %26 = load i32*, i32** %raySign.addr, align 4
  %arrayidx19 = getelementptr inbounds i32, i32* %26, i32 1
  %27 = load i32, i32* %arrayidx19, align 4
  %sub20 = sub i32 1, %27
  %arrayidx21 = getelementptr inbounds %class.btVector3, %class.btVector3* %25, i32 %sub20
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %arrayidx21)
  %28 = load float, float* %call22, align 4
  %29 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %29)
  %30 = load float, float* %call23, align 4
  %sub24 = fsub float %28, %30
  %31 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %31)
  %32 = load float, float* %call25, align 4
  %mul26 = fmul float %sub24, %32
  store float %mul26, float* %tymax, align 4
  %33 = load float*, float** %tmin.addr, align 4
  %34 = load float, float* %33, align 4
  %35 = load float, float* %tymax, align 4
  %cmp = fcmp ogt float %34, %35
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %36 = load float, float* %tymin, align 4
  %37 = load float, float* %tmax, align 4
  %cmp27 = fcmp ogt float %36, %37
  br i1 %cmp27, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false
  %38 = load float, float* %tymin, align 4
  %39 = load float*, float** %tmin.addr, align 4
  %40 = load float, float* %39, align 4
  %cmp28 = fcmp ogt float %38, %40
  br i1 %cmp28, label %if.then29, label %if.end30

if.then29:                                        ; preds = %if.end
  %41 = load float, float* %tymin, align 4
  %42 = load float*, float** %tmin.addr, align 4
  store float %41, float* %42, align 4
  br label %if.end30

if.end30:                                         ; preds = %if.then29, %if.end
  %43 = load float, float* %tymax, align 4
  %44 = load float, float* %tmax, align 4
  %cmp31 = fcmp olt float %43, %44
  br i1 %cmp31, label %if.then32, label %if.end33

if.then32:                                        ; preds = %if.end30
  %45 = load float, float* %tymax, align 4
  store float %45, float* %tmax, align 4
  br label %if.end33

if.end33:                                         ; preds = %if.then32, %if.end30
  %46 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4
  %47 = load i32*, i32** %raySign.addr, align 4
  %arrayidx34 = getelementptr inbounds i32, i32* %47, i32 2
  %48 = load i32, i32* %arrayidx34, align 4
  %arrayidx35 = getelementptr inbounds %class.btVector3, %class.btVector3* %46, i32 %48
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %arrayidx35)
  %49 = load float, float* %call36, align 4
  %50 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %50)
  %51 = load float, float* %call37, align 4
  %sub38 = fsub float %49, %51
  %52 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4
  %call39 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %52)
  %53 = load float, float* %call39, align 4
  %mul40 = fmul float %sub38, %53
  store float %mul40, float* %tzmin, align 4
  %54 = load %class.btVector3*, %class.btVector3** %bounds.addr, align 4
  %55 = load i32*, i32** %raySign.addr, align 4
  %arrayidx41 = getelementptr inbounds i32, i32* %55, i32 2
  %56 = load i32, i32* %arrayidx41, align 4
  %sub42 = sub i32 1, %56
  %arrayidx43 = getelementptr inbounds %class.btVector3, %class.btVector3* %54, i32 %sub42
  %call44 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %arrayidx43)
  %57 = load float, float* %call44, align 4
  %58 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %58)
  %59 = load float, float* %call45, align 4
  %sub46 = fsub float %57, %59
  %60 = load %class.btVector3*, %class.btVector3** %rayInvDirection.addr, align 4
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %60)
  %61 = load float, float* %call47, align 4
  %mul48 = fmul float %sub46, %61
  store float %mul48, float* %tzmax, align 4
  %62 = load float*, float** %tmin.addr, align 4
  %63 = load float, float* %62, align 4
  %64 = load float, float* %tzmax, align 4
  %cmp49 = fcmp ogt float %63, %64
  br i1 %cmp49, label %if.then52, label %lor.lhs.false50

lor.lhs.false50:                                  ; preds = %if.end33
  %65 = load float, float* %tzmin, align 4
  %66 = load float, float* %tmax, align 4
  %cmp51 = fcmp ogt float %65, %66
  br i1 %cmp51, label %if.then52, label %if.end53

if.then52:                                        ; preds = %lor.lhs.false50, %if.end33
  store i1 false, i1* %retval, align 1
  br label %return

if.end53:                                         ; preds = %lor.lhs.false50
  %67 = load float, float* %tzmin, align 4
  %68 = load float*, float** %tmin.addr, align 4
  %69 = load float, float* %68, align 4
  %cmp54 = fcmp ogt float %67, %69
  br i1 %cmp54, label %if.then55, label %if.end56

if.then55:                                        ; preds = %if.end53
  %70 = load float, float* %tzmin, align 4
  %71 = load float*, float** %tmin.addr, align 4
  store float %70, float* %71, align 4
  br label %if.end56

if.end56:                                         ; preds = %if.then55, %if.end53
  %72 = load float, float* %tzmax, align 4
  %73 = load float, float* %tmax, align 4
  %cmp57 = fcmp olt float %72, %73
  br i1 %cmp57, label %if.then58, label %if.end59

if.then58:                                        ; preds = %if.end56
  %74 = load float, float* %tzmax, align 4
  store float %74, float* %tmax, align 4
  br label %if.end59

if.end59:                                         ; preds = %if.then58, %if.end56
  %75 = load float*, float** %tmin.addr, align 4
  %76 = load float, float* %75, align 4
  %77 = load float, float* %lambda_max.addr, align 4
  %cmp60 = fcmp olt float %76, %77
  br i1 %cmp60, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %if.end59
  %78 = load float, float* %tmax, align 4
  %79 = load float, float* %lambda_min.addr, align 4
  %cmp61 = fcmp ogt float %78, %79
  br label %land.end

land.end:                                         ; preds = %land.rhs, %if.end59
  %80 = phi i1 [ false, %if.end59 ], [ %cmp61, %land.rhs ]
  store i1 %80, i1* %retval, align 1
  br label %return

return:                                           ; preds = %land.end, %if.then52, %if.then
  %81 = load i1, i1* %retval, align 1
  ret i1 %81
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20BroadphaseAabbTesterD0Ev(%struct.BroadphaseAabbTester* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.BroadphaseAabbTester*, align 4
  store %struct.BroadphaseAabbTester* %this, %struct.BroadphaseAabbTester** %this.addr, align 4
  %this1 = load %struct.BroadphaseAabbTester*, %struct.BroadphaseAabbTester** %this.addr, align 4
  %call = call %struct.BroadphaseAabbTester* @_ZN20BroadphaseAabbTesterD2Ev(%struct.BroadphaseAabbTester* %this1) #8
  %0 = bitcast %struct.BroadphaseAabbTester* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20BroadphaseAabbTester7ProcessEPK10btDbvtNode(%struct.BroadphaseAabbTester* %this, %struct.btDbvtNode* %leaf) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.BroadphaseAabbTester*, align 4
  %leaf.addr = alloca %struct.btDbvtNode*, align 4
  %proxy = alloca %struct.btDbvtProxy*, align 4
  store %struct.BroadphaseAabbTester* %this, %struct.BroadphaseAabbTester** %this.addr, align 4
  store %struct.btDbvtNode* %leaf, %struct.btDbvtNode** %leaf.addr, align 4
  %this1 = load %struct.BroadphaseAabbTester*, %struct.BroadphaseAabbTester** %this.addr, align 4
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %leaf.addr, align 4
  %1 = getelementptr inbounds %struct.btDbvtNode, %struct.btDbvtNode* %0, i32 0, i32 2
  %data = bitcast %union.anon.0* %1 to i8**
  %2 = load i8*, i8** %data, align 4
  %3 = bitcast i8* %2 to %struct.btDbvtProxy*
  store %struct.btDbvtProxy* %3, %struct.btDbvtProxy** %proxy, align 4
  %m_aabbCallback = getelementptr inbounds %struct.BroadphaseAabbTester, %struct.BroadphaseAabbTester* %this1, i32 0, i32 1
  %4 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %m_aabbCallback, align 4
  %5 = load %struct.btDbvtProxy*, %struct.btDbvtProxy** %proxy, align 4
  %6 = bitcast %struct.btDbvtProxy* %5 to %struct.btBroadphaseProxy*
  %7 = bitcast %struct.btBroadphaseAabbCallback* %4 to i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)**, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*** %7, align 4
  %vfn = getelementptr inbounds i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %8 = load i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)*, i1 (%struct.btBroadphaseAabbCallback*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call = call zeroext i1 %8(%struct.btBroadphaseAabbCallback* %4, %struct.btBroadphaseProxy* %6)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE6resizeEiRKS1_(%class.btAlignedObjectArray* %this, i32 %newsize, %"struct.btDbvt::sStkNN"* nonnull align 4 dereferenceable(8) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %"struct.btDbvt::sStkNN"* %fillData, %"struct.btDbvt::sStkNN"** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %14, i32 %15
  %16 = bitcast %"struct.btDbvt::sStkNN"* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %"struct.btDbvt::sStkNN"*
  %18 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %fillData.addr, align 4
  %19 = bitcast %"struct.btDbvt::sStkNN"* %17 to i8*
  %20 = bitcast %"struct.btDbvt::sStkNN"* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 8, i1 false)
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %21, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2Ev(%"struct.btDbvt::sStkNN"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  store %"struct.btDbvt::sStkNN"* %this, %"struct.btDbvt::sStkNN"** %this.addr, align 4
  %this1 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %this.addr, align 4
  ret %"struct.btDbvt::sStkNN"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDbvt::sStkNN"* @_ZN6btDbvt6sStkNNC2EPK10btDbvtNodeS3_(%"struct.btDbvt::sStkNN"* returned %this, %struct.btDbvtNode* %na, %struct.btDbvtNode* %nb) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  %na.addr = alloca %struct.btDbvtNode*, align 4
  %nb.addr = alloca %struct.btDbvtNode*, align 4
  store %"struct.btDbvt::sStkNN"* %this, %"struct.btDbvt::sStkNN"** %this.addr, align 4
  store %struct.btDbvtNode* %na, %struct.btDbvtNode** %na.addr, align 4
  store %struct.btDbvtNode* %nb, %struct.btDbvtNode** %nb.addr, align 4
  %this1 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %this.addr, align 4
  %a = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %this1, i32 0, i32 0
  %0 = load %struct.btDbvtNode*, %struct.btDbvtNode** %na.addr, align 4
  store %struct.btDbvtNode* %0, %struct.btDbvtNode** %a, align 4
  %b = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %this1, i32 0, i32 1
  %1 = load %struct.btDbvtNode*, %struct.btDbvtNode** %nb.addr, align 4
  store %struct.btDbvtNode* %1, %struct.btDbvtNode** %b, align 4
  ret %"struct.btDbvt::sStkNN"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %"struct.btDbvt::sStkNN"* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %0, i32 %1
  ret %"struct.btDbvt::sStkNN"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"struct.btDbvt::sStkNN"*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %"struct.btDbvt::sStkNN"*
  store %"struct.btDbvt::sStkNN"* %2, %"struct.btDbvt::sStkNN"** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4copyEiiPS1_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %"struct.btDbvt::sStkNN"* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %"struct.btDbvt::sStkNN"* %4, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %"struct.btDbvt::sStkNN"* @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %m_allocator, i32 %1, %"struct.btDbvt::sStkNN"** null)
  %2 = bitcast %"struct.btDbvt::sStkNN"* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIN6btDbvt6sStkNNEE4copyEiiPS1_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %"struct.btDbvt::sStkNN"* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %"struct.btDbvt::sStkNN"* %dest, %"struct.btDbvt::sStkNN"** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %3, i32 %4
  %5 = bitcast %"struct.btDbvt::sStkNN"* %arrayidx to i8*
  %6 = bitcast i8* %5 to %"struct.btDbvt::sStkNN"*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %7, i32 %8
  %9 = bitcast %"struct.btDbvt::sStkNN"* %6 to i8*
  %10 = bitcast %"struct.btDbvt::sStkNN"* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 8, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btDbvt::sStkNN", %"struct.btDbvt::sStkNN"* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIN6btDbvt6sStkNNEE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data, align 4
  %tobool = icmp ne %"struct.btDbvt::sStkNN"* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %"struct.btDbvt::sStkNN"* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %"struct.btDbvt::sStkNN"* null, %"struct.btDbvt::sStkNN"** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btDbvt::sStkNN"* @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %this, i32 %n, %"struct.btDbvt::sStkNN"** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"struct.btDbvt::sStkNN"**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %"struct.btDbvt::sStkNN"** %hint, %"struct.btDbvt::sStkNN"*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 8, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"struct.btDbvt::sStkNN"*
  ret %"struct.btDbvt::sStkNN"* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIN6btDbvt6sStkNNELj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %"struct.btDbvt::sStkNN"* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %"struct.btDbvt::sStkNN"*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %"struct.btDbvt::sStkNN"* %ptr, %"struct.btDbvt::sStkNN"** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %"struct.btDbvt::sStkNN"*, %"struct.btDbvt::sStkNN"** %ptr.addr, align 4
  %1 = bitcast %"struct.btDbvt::sStkNN"* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN12btDbvtAabbMm6FromCEERK9btVector3S2_(%struct.btDbvtAabbMm* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %c, %class.btVector3* nonnull align 4 dereferenceable(16) %e) #2 comdat {
entry:
  %c.addr = alloca %class.btVector3*, align 4
  %e.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  store %class.btVector3* %e, %class.btVector3** %e.addr, align 4
  %call = call %struct.btDbvtAabbMm* @_ZN12btDbvtAabbMmC2Ev(%struct.btDbvtAabbMm* %agg.result)
  %0 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %e.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %mi = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 0
  %2 = bitcast %class.btVector3* %mi to i8*
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %4 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %e.addr, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  %mx = getelementptr inbounds %struct.btDbvtAabbMm, %struct.btDbvtAabbMm* %agg.result, i32 0, i32 1
  %6 = bitcast %class.btVector3* %mx to i8*
  %7 = bitcast %class.btVector3* %ref.tmp1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %struct.btDbvtNode** null, %struct.btDbvtNode*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE5clearEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.2* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIPK10btDbvtNodeELj16EEC2Ev(%class.btAlignedAllocator.2* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  ret %class.btAlignedAllocator.2* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE4initEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btAlignedObjectArray.4* null, %class.btAlignedObjectArray.4** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE5clearEv(%class.btAlignedObjectArray.1* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIS_IPK10btDbvtNodeEE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE7destroyEii(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE10deallocateEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE4initEv(%class.btAlignedObjectArray.1* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE7destroyEii(%class.btAlignedObjectArray.1* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %3 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %3, i32 %4
  %call = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeED2Ev(%class.btAlignedObjectArray.4* %arrayidx) #8
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIS_IPK10btDbvtNodeEE4sizeEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE10deallocateEv(%class.btAlignedObjectArray.1* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %0 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %m_data, align 4
  %tobool = icmp ne %class.btAlignedObjectArray.4* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %2 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIPK10btDbvtNodeELj16EE10deallocateEPS4_(%class.btAlignedAllocator.2* %m_allocator, %class.btAlignedObjectArray.4* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btAlignedObjectArray.4* null, %class.btAlignedObjectArray.4** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIPK10btDbvtNodeELj16EE10deallocateEPS4_(%class.btAlignedAllocator.2* %this, %class.btAlignedObjectArray.4* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  %ptr.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  store %class.btAlignedObjectArray.4* %ptr, %class.btAlignedObjectArray.4** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  %0 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %ptr.addr, align 4
  %1 = bitcast %class.btAlignedObjectArray.4* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE7reserveEi(%class.btAlignedObjectArray.1* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIS_IPK10btDbvtNodeEE8capacityEv(%class.btAlignedObjectArray.1* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE8allocateEi(%class.btAlignedObjectArray.1* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btAlignedObjectArray.4*
  store %class.btAlignedObjectArray.4* %2, %class.btAlignedObjectArray.4** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIS_IPK10btDbvtNodeEE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  %3 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIS_IPK10btDbvtNodeEE4copyEiiPS3_(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call3, %class.btAlignedObjectArray.4* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIS_IPK10btDbvtNodeEE4sizeEv(%class.btAlignedObjectArray.1* %this1)
  call void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE7destroyEii(%class.btAlignedObjectArray.1* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE10deallocateEv(%class.btAlignedObjectArray.1* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  store %class.btAlignedObjectArray.4* %4, %class.btAlignedObjectArray.4** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2ERKS3_(%class.btAlignedObjectArray.4* returned %this, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %otherArray) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %otherArray.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %otherSize = alloca i32, align 4
  %ref.tmp = alloca %struct.btDbvtNode*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store %class.btAlignedObjectArray.4* %otherArray, %class.btAlignedObjectArray.4** %otherArray.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIPK10btDbvtNodeLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE4initEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %otherArray.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4sizeEv(%class.btAlignedObjectArray.4* %0)
  store i32 %call2, i32* %otherSize, align 4
  %1 = load i32, i32* %otherSize, align 4
  store %struct.btDbvtNode* null, %struct.btDbvtNode** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIPK10btDbvtNodeE6resizeEiRKS2_(%class.btAlignedObjectArray.4* %this1, i32 %1, %struct.btDbvtNode** nonnull align 4 dereferenceable(4) %ref.tmp)
  %2 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %otherArray.addr, align 4
  %3 = load i32, i32* %otherSize, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %4 = load %struct.btDbvtNode**, %struct.btDbvtNode*** %m_data, align 4
  call void @_ZNK20btAlignedObjectArrayIPK10btDbvtNodeE4copyEiiPS2_(%class.btAlignedObjectArray.4* %2, i32 0, i32 %3, %struct.btDbvtNode** %4)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIS_IPK10btDbvtNodeEE8capacityEv(%class.btAlignedObjectArray.1* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIS_IPK10btDbvtNodeEE8allocateEi(%class.btAlignedObjectArray.1* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btAlignedObjectArray.4* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIPK10btDbvtNodeELj16EE8allocateEiPPKS4_(%class.btAlignedAllocator.2* %m_allocator, i32 %1, %class.btAlignedObjectArray.4** null)
  %2 = bitcast %class.btAlignedObjectArray.4* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIS_IPK10btDbvtNodeEE4copyEiiPS3_(%class.btAlignedObjectArray.1* %this, i32 %start, i32 %end, %class.btAlignedObjectArray.4* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.1*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.1* %this, %class.btAlignedObjectArray.1** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btAlignedObjectArray.4* %dest, %class.btAlignedObjectArray.4** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.1*, %class.btAlignedObjectArray.1** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %3, i32 %4
  %5 = bitcast %class.btAlignedObjectArray.4* %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btAlignedObjectArray.4*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.1, %class.btAlignedObjectArray.1* %this1, i32 0, i32 4
  %7 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %7, i32 %8
  %call = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIPK10btDbvtNodeEC2ERKS3_(%class.btAlignedObjectArray.4* %6, %class.btAlignedObjectArray.4* nonnull align 4 dereferenceable(17) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN18btAlignedAllocatorI20btAlignedObjectArrayIPK10btDbvtNodeELj16EE8allocateEiPPKS4_(%class.btAlignedAllocator.2* %this, i32 %n, %class.btAlignedObjectArray.4** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.2*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btAlignedObjectArray.4**, align 4
  store %class.btAlignedAllocator.2* %this, %class.btAlignedAllocator.2** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btAlignedObjectArray.4** %hint, %class.btAlignedObjectArray.4*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.2*, %class.btAlignedAllocator.2** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 20, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btAlignedObjectArray.4*
  ret %class.btAlignedObjectArray.4* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.9* %this, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %CompareFunc.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store %class.btBroadphasePairSortPredicate* %CompareFunc, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  store i32 %lo, i32* %lo.addr, align 4
  store i32 %hi, i32* %hi.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %lo.addr, align 4
  store i32 %0, i32* %i, align 4
  %1 = load i32, i32* %hi.addr, align 4
  store i32 %1, i32* %j, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %3 = load i32, i32* %lo.addr, align 4
  %4 = load i32, i32* %hi.addr, align 4
  %add = add nsw i32 %3, %4
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 %div
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %5 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %6 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4
  %7 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %6, i32 %7
  %call4 = call zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %5, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx3, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x)
  br i1 %call4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond5

while.cond5:                                      ; preds = %while.body9, %while.end
  %9 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %10 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4
  %11 = load i32, i32* %j, align 4
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %10, i32 %11
  %call8 = call zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %9, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %x, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx7)
  br i1 %call8, label %while.body9, label %while.end10

while.body9:                                      ; preds = %while.cond5
  %12 = load i32, i32* %j, align 4
  %dec = add nsw i32 %12, -1
  store i32 %dec, i32* %j, align 4
  br label %while.cond5

while.end10:                                      ; preds = %while.cond5
  %13 = load i32, i32* %i, align 4
  %14 = load i32, i32* %j, align 4
  %cmp = icmp sle i32 %13, %14
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end10
  %15 = load i32, i32* %i, align 4
  %16 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray.9* %this1, i32 %15, i32 %16)
  %17 = load i32, i32* %i, align 4
  %inc11 = add nsw i32 %17, 1
  store i32 %inc11, i32* %i, align 4
  %18 = load i32, i32* %j, align 4
  %dec12 = add nsw i32 %18, -1
  store i32 %dec12, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end10
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %19 = load i32, i32* %i, align 4
  %20 = load i32, i32* %j, align 4
  %cmp13 = icmp sle i32 %19, %20
  br i1 %cmp13, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %21 = load i32, i32* %lo.addr, align 4
  %22 = load i32, i32* %j, align 4
  %cmp14 = icmp slt i32 %21, %22
  br i1 %cmp14, label %if.then15, label %if.end16

if.then15:                                        ; preds = %do.end
  %23 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %24 = load i32, i32* %lo.addr, align 4
  %25 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.9* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %23, i32 %24, i32 %25)
  br label %if.end16

if.end16:                                         ; preds = %if.then15, %do.end
  %26 = load i32, i32* %i, align 4
  %27 = load i32, i32* %hi.addr, align 4
  %cmp17 = icmp slt i32 %26, %27
  br i1 %cmp17, label %if.then18, label %if.end19

if.then18:                                        ; preds = %if.end16
  %28 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %CompareFunc.addr, align 4
  %29 = load i32, i32* %i, align 4
  %30 = load i32, i32* %hi.addr, align 4
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.9* %this1, %class.btBroadphasePairSortPredicate* nonnull align 1 dereferenceable(1) %28, i32 %29, i32 %30)
  br label %if.end19

if.end19:                                         ; preds = %if.then18, %if.end16
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* returned %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %other) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphasePair*, align 4
  %other.addr = alloca %struct.btBroadphasePair*, align 4
  store %struct.btBroadphasePair* %this, %struct.btBroadphasePair** %this.addr, align 4
  store %struct.btBroadphasePair* %other, %struct.btBroadphasePair** %other.addr, align 4
  %this1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %this.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 0
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4
  store %struct.btBroadphaseProxy* %1, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 1
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %m_pProxy13 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 1
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy13, align 4
  store %struct.btBroadphaseProxy* %3, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 2
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %m_algorithm4 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 2
  %5 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm4, align 4
  store %class.btCollisionAlgorithm* %5, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %6 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %this1, i32 0, i32 3
  %m_internalInfo1 = bitcast %union.anon.12* %6 to i8**
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %other.addr, align 4
  %8 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 3
  %m_internalInfo15 = bitcast %union.anon.12* %8 to i8**
  %9 = load i8*, i8** %m_internalInfo15, align 4
  store i8* %9, i8** %m_internalInfo1, align 4
  ret %struct.btBroadphasePair* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_(%class.btBroadphasePairSortPredicate* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %a, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %this.addr = alloca %class.btBroadphasePairSortPredicate*, align 4
  %a.addr = alloca %struct.btBroadphasePair*, align 4
  %b.addr = alloca %struct.btBroadphasePair*, align 4
  %uidA0 = alloca i32, align 4
  %uidB0 = alloca i32, align 4
  %uidA1 = alloca i32, align 4
  %uidB1 = alloca i32, align 4
  store %class.btBroadphasePairSortPredicate* %this, %class.btBroadphasePairSortPredicate** %this.addr, align 4
  store %struct.btBroadphasePair* %a, %struct.btBroadphasePair** %a.addr, align 4
  store %struct.btBroadphasePair* %b, %struct.btBroadphasePair** %b.addr, align 4
  %this1 = load %class.btBroadphasePairSortPredicate*, %class.btBroadphasePairSortPredicate** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %tobool = icmp ne %struct.btBroadphaseProxy* %1, null
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy02 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 0, i32 0
  %3 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy02, align 4
  %m_uniqueId = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %3, i32 0, i32 3
  %4 = load i32, i32* %m_uniqueId, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %4, %cond.true ], [ -1, %cond.false ]
  store i32 %cond, i32* %uidA0, align 4
  %5 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy03 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %5, i32 0, i32 0
  %6 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy03, align 4
  %tobool4 = icmp ne %struct.btBroadphaseProxy* %6, null
  br i1 %tobool4, label %cond.true5, label %cond.false8

cond.true5:                                       ; preds = %cond.end
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy06 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 0, i32 0
  %8 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy06, align 4
  %m_uniqueId7 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %8, i32 0, i32 3
  %9 = load i32, i32* %m_uniqueId7, align 4
  br label %cond.end9

cond.false8:                                      ; preds = %cond.end
  br label %cond.end9

cond.end9:                                        ; preds = %cond.false8, %cond.true5
  %cond10 = phi i32 [ %9, %cond.true5 ], [ -1, %cond.false8 ]
  store i32 %cond10, i32* %uidB0, align 4
  %10 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %10, i32 0, i32 1
  %11 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %tobool11 = icmp ne %struct.btBroadphaseProxy* %11, null
  br i1 %tobool11, label %cond.true12, label %cond.false15

cond.true12:                                      ; preds = %cond.end9
  %12 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy113 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %12, i32 0, i32 1
  %13 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy113, align 4
  %m_uniqueId14 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %13, i32 0, i32 3
  %14 = load i32, i32* %m_uniqueId14, align 4
  br label %cond.end16

cond.false15:                                     ; preds = %cond.end9
  br label %cond.end16

cond.end16:                                       ; preds = %cond.false15, %cond.true12
  %cond17 = phi i32 [ %14, %cond.true12 ], [ -1, %cond.false15 ]
  store i32 %cond17, i32* %uidA1, align 4
  %15 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy118 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %15, i32 0, i32 1
  %16 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy118, align 4
  %tobool19 = icmp ne %struct.btBroadphaseProxy* %16, null
  br i1 %tobool19, label %cond.true20, label %cond.false23

cond.true20:                                      ; preds = %cond.end16
  %17 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy121 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %17, i32 0, i32 1
  %18 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy121, align 4
  %m_uniqueId22 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %18, i32 0, i32 3
  %19 = load i32, i32* %m_uniqueId22, align 4
  br label %cond.end24

cond.false23:                                     ; preds = %cond.end16
  br label %cond.end24

cond.end24:                                       ; preds = %cond.false23, %cond.true20
  %cond25 = phi i32 [ %19, %cond.true20 ], [ -1, %cond.false23 ]
  store i32 %cond25, i32* %uidB1, align 4
  %20 = load i32, i32* %uidA0, align 4
  %21 = load i32, i32* %uidB0, align 4
  %cmp = icmp sgt i32 %20, %21
  br i1 %cmp, label %lor.end, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %cond.end24
  %22 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy026 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %22, i32 0, i32 0
  %23 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy026, align 4
  %24 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy027 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %24, i32 0, i32 0
  %25 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy027, align 4
  %cmp28 = icmp eq %struct.btBroadphaseProxy* %23, %25
  br i1 %cmp28, label %land.lhs.true, label %lor.rhs

land.lhs.true:                                    ; preds = %lor.lhs.false
  %26 = load i32, i32* %uidA1, align 4
  %27 = load i32, i32* %uidB1, align 4
  %cmp29 = icmp sgt i32 %26, %27
  br i1 %cmp29, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %land.lhs.true, %lor.lhs.false
  %28 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy030 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %28, i32 0, i32 0
  %29 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy030, align 4
  %30 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy031 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %30, i32 0, i32 0
  %31 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy031, align 4
  %cmp32 = icmp eq %struct.btBroadphaseProxy* %29, %31
  br i1 %cmp32, label %land.lhs.true33, label %land.end

land.lhs.true33:                                  ; preds = %lor.rhs
  %32 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_pProxy134 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %32, i32 0, i32 1
  %33 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy134, align 4
  %34 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_pProxy135 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %34, i32 0, i32 1
  %35 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy135, align 4
  %cmp36 = icmp eq %struct.btBroadphaseProxy* %33, %35
  br i1 %cmp36, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %land.lhs.true33
  %36 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %a.addr, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %36, i32 0, i32 2
  %37 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %38 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %b.addr, align 4
  %m_algorithm37 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %38, i32 0, i32 2
  %39 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm37, align 4
  %cmp38 = icmp ugt %class.btCollisionAlgorithm* %37, %39
  br label %land.end

land.end:                                         ; preds = %land.rhs, %land.lhs.true33, %lor.rhs
  %40 = phi i1 [ false, %land.lhs.true33 ], [ false, %lor.rhs ], [ %cmp38, %land.rhs ]
  br label %lor.end

lor.end:                                          ; preds = %land.end, %land.lhs.true, %cond.end24
  %41 = phi i1 [ true, %land.lhs.true ], [ true, %cond.end24 ], [ %40, %land.end ]
  ret i1 %41
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE4swapEii(%class.btAlignedObjectArray.9* %this, i32 %index0, i32 %index1) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %struct.btBroadphasePair, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 %1
  %call = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %temp, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx)
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data2, align 4
  %3 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %2, i32 %3
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4
  %5 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 %5
  %6 = bitcast %struct.btBroadphasePair* %arrayidx5 to i8*
  %7 = bitcast %struct.btBroadphasePair* %arrayidx3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %8 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data6, align 4
  %9 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %8, i32 %9
  %10 = bitcast %struct.btBroadphasePair* %arrayidx7 to i8*
  %11 = bitcast %struct.btBroadphasePair* %temp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi(%class.btAlignedObjectArray.9* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray.9* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray.9* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btBroadphasePair*
  store %struct.btBroadphasePair* %2, %struct.btBroadphasePair** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call3, %struct.btBroadphasePair* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4sizeEv(%class.btAlignedObjectArray.9* %this1)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray.9* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray.9* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* %4, %struct.btBroadphasePair** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN16btBroadphasePairnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI16btBroadphasePairE8capacityEv(%class.btAlignedObjectArray.9* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI16btBroadphasePairE8allocateEi(%class.btAlignedObjectArray.9* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.10* %m_allocator, i32 %1, %struct.btBroadphasePair** null)
  %2 = bitcast %struct.btBroadphasePair* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_(%class.btAlignedObjectArray.9* %this, i32 %start, i32 %end, %struct.btBroadphasePair* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btBroadphasePair*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btBroadphasePair* %dest, %struct.btBroadphasePair** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 %4
  %5 = bitcast %struct.btBroadphasePair* %arrayidx to i8*
  %call = call i8* @_ZN16btBroadphasePairnwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %struct.btBroadphasePair*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %7 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %7, i32 %8
  %call3 = call %struct.btBroadphasePair* @_ZN16btBroadphasePairC2ERKS_(%struct.btBroadphasePair* %6, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %arrayidx2)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE7destroyEii(%class.btAlignedObjectArray.9* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %3 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI16btBroadphasePairE10deallocateEv(%class.btAlignedObjectArray.9* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.9*, align 4
  store %class.btAlignedObjectArray.9* %this, %class.btAlignedObjectArray.9** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.9*, %class.btAlignedObjectArray.9** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data, align 4
  %tobool = icmp ne %struct.btBroadphasePair* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  %2 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator.10* %m_allocator, %struct.btBroadphasePair* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.9, %class.btAlignedObjectArray.9* %this1, i32 0, i32 4
  store %struct.btBroadphasePair* null, %struct.btBroadphasePair** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphasePair* @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.10* %this, i32 %n, %struct.btBroadphasePair** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btBroadphasePair**, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btBroadphasePair** %hint, %struct.btBroadphasePair*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btBroadphasePair*
  ret %struct.btBroadphasePair* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI16btBroadphasePairLj16EE10deallocateEPS0_(%class.btAlignedAllocator.10* %this, %struct.btBroadphasePair* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.10*, align 4
  %ptr.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.btAlignedAllocator.10* %this, %class.btAlignedAllocator.10** %this.addr, align 4
  store %struct.btBroadphasePair* %ptr, %struct.btBroadphasePair** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.10*, %class.btAlignedAllocator.10** %this.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %ptr.addr, align 4
  %1 = bitcast %struct.btBroadphasePair* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btDbvtBroadphase.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { cold noreturn nounwind }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }
attributes #10 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
