; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/Gimpact/gim_box_set.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/Gimpact/gim_box_set.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.GIM_BOX_TREE = type { i32, %class.gim_array }
%class.gim_array = type { %struct.GIM_BOX_TREE_NODE*, i32, i32 }
%struct.GIM_BOX_TREE_NODE = type { %class.GIM_AABB, i32, i32, i32, i32 }
%class.GIM_AABB = type { %class.btVector3, %class.btVector3 }
%class.btVector3 = type { [4 x float] }
%class.gim_array.0 = type { %struct.GIM_AABB_DATA*, i32, i32 }
%struct.GIM_AABB_DATA = type { %class.GIM_AABB, i32 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZN9gim_arrayI13GIM_AABB_DATAEixEm = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN9gim_arrayI13GIM_AABB_DATAE4swapEjj = comdat any

$_ZN9gim_arrayI17GIM_BOX_TREE_NODEEixEm = comdat any

$_ZN8GIM_AABB10invalidateEv = comdat any

$_ZN8GIM_AABB5mergeERKS_ = comdat any

$_ZN9gim_arrayI17GIM_BOX_TREE_NODEE6resizeEjbRKS0_ = comdat any

$_ZNK9gim_arrayI13GIM_AABB_DATAE4sizeEv = comdat any

$_ZN17GIM_BOX_TREE_NODEC2Ev = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN8GIM_AABBC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_Z17gim_swap_elementsI13GIM_AABB_DATAEvPT_mm = comdat any

$_ZN13GIM_AABB_DATAC2ERKS_ = comdat any

$_ZN8GIM_AABBC2ERKS_ = comdat any

$_ZN9gim_arrayI17GIM_BOX_TREE_NODEE7reserveEj = comdat any

$_ZN9gim_arrayI17GIM_BOX_TREE_NODEE11clear_rangeEj = comdat any

$_ZN9gim_arrayI17GIM_BOX_TREE_NODEE10resizeDataEj = comdat any

$_ZN9gim_arrayI17GIM_BOX_TREE_NODEE11destroyDataEv = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_gim_box_set.cpp, i8* null }]

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN12GIM_BOX_TREE20_calc_splitting_axisER9gim_arrayI13GIM_AABB_DATAEjj(%class.GIM_BOX_TREE* %this, %class.gim_array.0* nonnull align 4 dereferenceable(12) %primitive_boxes, i32 %startIndex, i32 %endIndex) #2 {
entry:
  %this.addr = alloca %class.GIM_BOX_TREE*, align 4
  %primitive_boxes.addr = alloca %class.gim_array.0*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %means = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %variance = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %numIndices = alloca i32, align 4
  %center = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca float, align 4
  %center19 = alloca %class.btVector3, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %diff2 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca float, align 4
  store %class.GIM_BOX_TREE* %this, %class.GIM_BOX_TREE** %this.addr, align 4
  store %class.gim_array.0* %primitive_boxes, %class.gim_array.0** %primitive_boxes.addr, align 4
  store i32 %startIndex, i32* %startIndex.addr, align 4
  store i32 %endIndex, i32* %endIndex.addr, align 4
  %this1 = load %class.GIM_BOX_TREE*, %class.GIM_BOX_TREE** %this.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %variance, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %0 = load i32, i32* %endIndex.addr, align 4
  %1 = load i32, i32* %startIndex.addr, align 4
  %sub = sub i32 %0, %1
  store i32 %sub, i32* %numIndices, align 4
  %2 = load i32, i32* %startIndex.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %endIndex.addr, align 4
  %cmp = icmp ult i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store float 5.000000e-01, float* %ref.tmp8, align 4
  %5 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %6 = load i32, i32* %i, align 4
  %call10 = call nonnull align 4 dereferenceable(36) %struct.GIM_AABB_DATA* @_ZN9gim_arrayI13GIM_AABB_DATAEixEm(%class.gim_array.0* %5, i32 %6)
  %m_bound = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %call10, i32 0, i32 0
  %m_max = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %m_bound, i32 0, i32 1
  %7 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %8 = load i32, i32* %i, align 4
  %call11 = call nonnull align 4 dereferenceable(36) %struct.GIM_AABB_DATA* @_ZN9gim_arrayI13GIM_AABB_DATAEixEm(%class.gim_array.0* %7, i32 %8)
  %m_bound12 = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %call11, i32 0, i32 0
  %m_min = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %m_bound12, i32 0, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center, float* nonnull align 4 dereferenceable(4) %ref.tmp8, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp9)
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %means, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %9 = load i32, i32* %i, align 4
  %inc = add i32 %9, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %10 = load i32, i32* %numIndices, align 4
  %conv = uitofp i32 %10 to float
  %div = fdiv float 1.000000e+00, %conv
  store float %div, float* %ref.tmp14, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %means, float* nonnull align 4 dereferenceable(4) %ref.tmp14)
  %11 = load i32, i32* %startIndex.addr, align 4
  store i32 %11, i32* %i, align 4
  br label %for.cond16

for.cond16:                                       ; preds = %for.inc30, %for.end
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %endIndex.addr, align 4
  %cmp17 = icmp ult i32 %12, %13
  br i1 %cmp17, label %for.body18, label %for.end32

for.body18:                                       ; preds = %for.cond16
  store float 5.000000e-01, float* %ref.tmp20, align 4
  %14 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %15 = load i32, i32* %i, align 4
  %call22 = call nonnull align 4 dereferenceable(36) %struct.GIM_AABB_DATA* @_ZN9gim_arrayI13GIM_AABB_DATAEixEm(%class.gim_array.0* %14, i32 %15)
  %m_bound23 = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %call22, i32 0, i32 0
  %m_max24 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %m_bound23, i32 0, i32 1
  %16 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %17 = load i32, i32* %i, align 4
  %call25 = call nonnull align 4 dereferenceable(36) %struct.GIM_AABB_DATA* @_ZN9gim_arrayI13GIM_AABB_DATAEixEm(%class.gim_array.0* %16, i32 %17)
  %m_bound26 = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %call25, i32 0, i32 0
  %m_min27 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %m_bound26, i32 0, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max24, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min27)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %center19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp21)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %diff2, %class.btVector3* nonnull align 4 dereferenceable(16) %center19, %class.btVector3* nonnull align 4 dereferenceable(16) %means)
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp28, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2)
  %18 = bitcast %class.btVector3* %diff2 to i8*
  %19 = bitcast %class.btVector3* %ref.tmp28 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %18, i8* align 4 %19, i32 16, i1 false)
  %call29 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %variance, %class.btVector3* nonnull align 4 dereferenceable(16) %diff2)
  br label %for.inc30

for.inc30:                                        ; preds = %for.body18
  %20 = load i32, i32* %i, align 4
  %inc31 = add i32 %20, 1
  store i32 %inc31, i32* %i, align 4
  br label %for.cond16

for.end32:                                        ; preds = %for.cond16
  %21 = load i32, i32* %numIndices, align 4
  %conv34 = uitofp i32 %21 to float
  %sub35 = fsub float %conv34, 1.000000e+00
  %div36 = fdiv float 1.000000e+00, %sub35
  store float %div36, float* %ref.tmp33, align 4
  %call37 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %variance, float* nonnull align 4 dereferenceable(4) %ref.tmp33)
  %call38 = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %variance)
  ret i32 %call38
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %struct.GIM_AABB_DATA* @_ZN9gim_arrayI13GIM_AABB_DATAEixEm(%class.gim_array.0* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.gim_array.0*, align 4
  %i.addr = alloca i32, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 0
  %0 = load %struct.GIM_AABB_DATA*, %struct.GIM_AABB_DATA** %m_data, align 4
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %0, i32 %1
  ret %struct.GIM_AABB_DATA* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN12GIM_BOX_TREE30_sort_and_calc_splitting_indexER9gim_arrayI13GIM_AABB_DATAEjjj(%class.GIM_BOX_TREE* %this, %class.gim_array.0* nonnull align 4 dereferenceable(12) %primitive_boxes, i32 %startIndex, i32 %endIndex, i32 %splitAxis) #2 {
entry:
  %this.addr = alloca %class.GIM_BOX_TREE*, align 4
  %primitive_boxes.addr = alloca %class.gim_array.0*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %splitAxis.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %splitIndex = alloca i32, align 4
  %numIndices = alloca i32, align 4
  %splitValue = alloca float, align 4
  %center = alloca float, align 4
  %rangeBalancedIndices = alloca i32, align 4
  %unbalanced = alloca i8, align 1
  store %class.GIM_BOX_TREE* %this, %class.GIM_BOX_TREE** %this.addr, align 4
  store %class.gim_array.0* %primitive_boxes, %class.gim_array.0** %primitive_boxes.addr, align 4
  store i32 %startIndex, i32* %startIndex.addr, align 4
  store i32 %endIndex, i32* %endIndex.addr, align 4
  store i32 %splitAxis, i32* %splitAxis.addr, align 4
  %this1 = load %class.GIM_BOX_TREE*, %class.GIM_BOX_TREE** %this.addr, align 4
  %0 = load i32, i32* %startIndex.addr, align 4
  store i32 %0, i32* %splitIndex, align 4
  %1 = load i32, i32* %endIndex.addr, align 4
  %2 = load i32, i32* %startIndex.addr, align 4
  %sub = sub i32 %1, %2
  store i32 %sub, i32* %numIndices, align 4
  store float 0.000000e+00, float* %splitValue, align 4
  %3 = load i32, i32* %startIndex.addr, align 4
  store i32 %3, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %4 = load i32, i32* %i, align 4
  %5 = load i32, i32* %endIndex.addr, align 4
  %cmp = icmp ult i32 %4, %5
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %6 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %7 = load i32, i32* %i, align 4
  %call = call nonnull align 4 dereferenceable(36) %struct.GIM_AABB_DATA* @_ZN9gim_arrayI13GIM_AABB_DATAEixEm(%class.gim_array.0* %6, i32 %7)
  %m_bound = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %call, i32 0, i32 0
  %m_max = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %m_bound, i32 0, i32 1
  %call2 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max)
  %8 = load i32, i32* %splitAxis.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %call2, i32 %8
  %9 = load float, float* %arrayidx, align 4
  %10 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %11 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(36) %struct.GIM_AABB_DATA* @_ZN9gim_arrayI13GIM_AABB_DATAEixEm(%class.gim_array.0* %10, i32 %11)
  %m_bound4 = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %call3, i32 0, i32 0
  %m_min = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %m_bound4, i32 0, i32 0
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min)
  %12 = load i32, i32* %splitAxis.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %12
  %13 = load float, float* %arrayidx6, align 4
  %add = fadd float %9, %13
  %mul = fmul float 5.000000e-01, %add
  %14 = load float, float* %splitValue, align 4
  %add7 = fadd float %14, %mul
  store float %add7, float* %splitValue, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4
  %inc = add i32 %15, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %16 = load i32, i32* %numIndices, align 4
  %conv = uitofp i32 %16 to float
  %17 = load float, float* %splitValue, align 4
  %div = fdiv float %17, %conv
  store float %div, float* %splitValue, align 4
  %18 = load i32, i32* %startIndex.addr, align 4
  store i32 %18, i32* %i, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc25, %for.end
  %19 = load i32, i32* %i, align 4
  %20 = load i32, i32* %endIndex.addr, align 4
  %cmp9 = icmp ult i32 %19, %20
  br i1 %cmp9, label %for.body10, label %for.end27

for.body10:                                       ; preds = %for.cond8
  %21 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %22 = load i32, i32* %i, align 4
  %call11 = call nonnull align 4 dereferenceable(36) %struct.GIM_AABB_DATA* @_ZN9gim_arrayI13GIM_AABB_DATAEixEm(%class.gim_array.0* %21, i32 %22)
  %m_bound12 = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %call11, i32 0, i32 0
  %m_max13 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %m_bound12, i32 0, i32 1
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max13)
  %23 = load i32, i32* %splitAxis.addr, align 4
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 %23
  %24 = load float, float* %arrayidx15, align 4
  %25 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %26 = load i32, i32* %i, align 4
  %call16 = call nonnull align 4 dereferenceable(36) %struct.GIM_AABB_DATA* @_ZN9gim_arrayI13GIM_AABB_DATAEixEm(%class.gim_array.0* %25, i32 %26)
  %m_bound17 = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %call16, i32 0, i32 0
  %m_min18 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %m_bound17, i32 0, i32 0
  %call19 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min18)
  %27 = load i32, i32* %splitAxis.addr, align 4
  %arrayidx20 = getelementptr inbounds float, float* %call19, i32 %27
  %28 = load float, float* %arrayidx20, align 4
  %add21 = fadd float %24, %28
  %mul22 = fmul float 5.000000e-01, %add21
  store float %mul22, float* %center, align 4
  %29 = load float, float* %center, align 4
  %30 = load float, float* %splitValue, align 4
  %cmp23 = fcmp ogt float %29, %30
  br i1 %cmp23, label %if.then, label %if.end

if.then:                                          ; preds = %for.body10
  %31 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %32 = load i32, i32* %i, align 4
  %33 = load i32, i32* %splitIndex, align 4
  call void @_ZN9gim_arrayI13GIM_AABB_DATAE4swapEjj(%class.gim_array.0* %31, i32 %32, i32 %33)
  %34 = load i32, i32* %splitIndex, align 4
  %inc24 = add i32 %34, 1
  store i32 %inc24, i32* %splitIndex, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body10
  br label %for.inc25

for.inc25:                                        ; preds = %if.end
  %35 = load i32, i32* %i, align 4
  %inc26 = add i32 %35, 1
  store i32 %inc26, i32* %i, align 4
  br label %for.cond8

for.end27:                                        ; preds = %for.cond8
  %36 = load i32, i32* %numIndices, align 4
  %div28 = udiv i32 %36, 3
  store i32 %div28, i32* %rangeBalancedIndices, align 4
  %37 = load i32, i32* %splitIndex, align 4
  %38 = load i32, i32* %startIndex.addr, align 4
  %39 = load i32, i32* %rangeBalancedIndices, align 4
  %add29 = add i32 %38, %39
  %cmp30 = icmp ule i32 %37, %add29
  br i1 %cmp30, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %for.end27
  %40 = load i32, i32* %splitIndex, align 4
  %41 = load i32, i32* %endIndex.addr, align 4
  %sub31 = sub i32 %41, 1
  %42 = load i32, i32* %rangeBalancedIndices, align 4
  %sub32 = sub i32 %sub31, %42
  %cmp33 = icmp uge i32 %40, %sub32
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %for.end27
  %43 = phi i1 [ true, %for.end27 ], [ %cmp33, %lor.rhs ]
  %frombool = zext i1 %43 to i8
  store i8 %frombool, i8* %unbalanced, align 1
  %44 = load i8, i8* %unbalanced, align 1
  %tobool = trunc i8 %44 to i1
  br i1 %tobool, label %if.then34, label %if.end36

if.then34:                                        ; preds = %lor.end
  %45 = load i32, i32* %startIndex.addr, align 4
  %46 = load i32, i32* %numIndices, align 4
  %shr = lshr i32 %46, 1
  %add35 = add i32 %45, %shr
  store i32 %add35, i32* %splitIndex, align 4
  br label %if.end36

if.end36:                                         ; preds = %if.then34, %lor.end
  %47 = load i32, i32* %splitIndex, align 4
  ret i32 %47
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9gim_arrayI13GIM_AABB_DATAE4swapEjj(%class.gim_array.0* %this, i32 %i, i32 %j) #2 comdat {
entry:
  %this.addr = alloca %class.gim_array.0*, align 4
  %i.addr = alloca i32, align 4
  %j.addr = alloca i32, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  store i32 %j, i32* %j.addr, align 4
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 0
  %0 = load %struct.GIM_AABB_DATA*, %struct.GIM_AABB_DATA** %m_data, align 4
  %1 = load i32, i32* %i.addr, align 4
  %2 = load i32, i32* %j.addr, align 4
  call void @_Z17gim_swap_elementsI13GIM_AABB_DATAEvPT_mm(%struct.GIM_AABB_DATA* %0, i32 %1, i32 %2)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN12GIM_BOX_TREE15_build_sub_treeER9gim_arrayI13GIM_AABB_DATAEjj(%class.GIM_BOX_TREE* %this, %class.gim_array.0* nonnull align 4 dereferenceable(12) %primitive_boxes, i32 %startIndex, i32 %endIndex) #2 {
entry:
  %this.addr = alloca %class.GIM_BOX_TREE*, align 4
  %primitive_boxes.addr = alloca %class.gim_array.0*, align 4
  %startIndex.addr = alloca i32, align 4
  %endIndex.addr = alloca i32, align 4
  %current_index = alloca i32, align 4
  %splitIndex = alloca i32, align 4
  store %class.GIM_BOX_TREE* %this, %class.GIM_BOX_TREE** %this.addr, align 4
  store %class.gim_array.0* %primitive_boxes, %class.gim_array.0** %primitive_boxes.addr, align 4
  store i32 %startIndex, i32* %startIndex.addr, align 4
  store i32 %endIndex, i32* %endIndex.addr, align 4
  %this1 = load %class.GIM_BOX_TREE*, %class.GIM_BOX_TREE** %this.addr, align 4
  %m_num_nodes = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_num_nodes, align 4
  %inc = add i32 %0, 1
  store i32 %inc, i32* %m_num_nodes, align 4
  store i32 %0, i32* %current_index, align 4
  %1 = load i32, i32* %endIndex.addr, align 4
  %2 = load i32, i32* %startIndex.addr, align 4
  %sub = sub i32 %1, %2
  %cmp = icmp eq i32 %sub, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_node_array = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 1
  %3 = load i32, i32* %current_index, align 4
  %call = call nonnull align 4 dereferenceable(48) %struct.GIM_BOX_TREE_NODE* @_ZN9gim_arrayI17GIM_BOX_TREE_NODEEixEm(%class.gim_array* %m_node_array, i32 %3)
  %m_left = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %call, i32 0, i32 1
  store i32 0, i32* %m_left, align 4
  %m_node_array2 = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 1
  %4 = load i32, i32* %current_index, align 4
  %call3 = call nonnull align 4 dereferenceable(48) %struct.GIM_BOX_TREE_NODE* @_ZN9gim_arrayI17GIM_BOX_TREE_NODEEixEm(%class.gim_array* %m_node_array2, i32 %4)
  %m_right = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %call3, i32 0, i32 2
  store i32 0, i32* %m_right, align 4
  %m_node_array4 = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 1
  %5 = load i32, i32* %current_index, align 4
  %call5 = call nonnull align 4 dereferenceable(48) %struct.GIM_BOX_TREE_NODE* @_ZN9gim_arrayI17GIM_BOX_TREE_NODEEixEm(%class.gim_array* %m_node_array4, i32 %5)
  %m_escapeIndex = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %call5, i32 0, i32 3
  store i32 0, i32* %m_escapeIndex, align 4
  %6 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %7 = load i32, i32* %startIndex.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(36) %struct.GIM_AABB_DATA* @_ZN9gim_arrayI13GIM_AABB_DATAEixEm(%class.gim_array.0* %6, i32 %7)
  %m_bound = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %call6, i32 0, i32 0
  %m_node_array7 = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 1
  %8 = load i32, i32* %current_index, align 4
  %call8 = call nonnull align 4 dereferenceable(48) %struct.GIM_BOX_TREE_NODE* @_ZN9gim_arrayI17GIM_BOX_TREE_NODEEixEm(%class.gim_array* %m_node_array7, i32 %8)
  %m_bound9 = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %call8, i32 0, i32 0
  %9 = bitcast %class.GIM_AABB* %m_bound9 to i8*
  %10 = bitcast %class.GIM_AABB* %m_bound to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 32, i1 false)
  %11 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %12 = load i32, i32* %startIndex.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(36) %struct.GIM_AABB_DATA* @_ZN9gim_arrayI13GIM_AABB_DATAEixEm(%class.gim_array.0* %11, i32 %12)
  %m_data = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %call10, i32 0, i32 1
  %13 = load i32, i32* %m_data, align 4
  %m_node_array11 = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 1
  %14 = load i32, i32* %current_index, align 4
  %call12 = call nonnull align 4 dereferenceable(48) %struct.GIM_BOX_TREE_NODE* @_ZN9gim_arrayI17GIM_BOX_TREE_NODEEixEm(%class.gim_array* %m_node_array11, i32 %14)
  %m_data13 = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %call12, i32 0, i32 4
  store i32 %13, i32* %m_data13, align 4
  br label %return

if.end:                                           ; preds = %entry
  %m_node_array14 = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 1
  %15 = load i32, i32* %current_index, align 4
  %call15 = call nonnull align 4 dereferenceable(48) %struct.GIM_BOX_TREE_NODE* @_ZN9gim_arrayI17GIM_BOX_TREE_NODEEixEm(%class.gim_array* %m_node_array14, i32 %15)
  %m_bound16 = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %call15, i32 0, i32 0
  call void @_ZN8GIM_AABB10invalidateEv(%class.GIM_AABB* %m_bound16)
  %16 = load i32, i32* %startIndex.addr, align 4
  store i32 %16, i32* %splitIndex, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %17 = load i32, i32* %splitIndex, align 4
  %18 = load i32, i32* %endIndex.addr, align 4
  %cmp17 = icmp ult i32 %17, %18
  br i1 %cmp17, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_node_array18 = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 1
  %19 = load i32, i32* %current_index, align 4
  %call19 = call nonnull align 4 dereferenceable(48) %struct.GIM_BOX_TREE_NODE* @_ZN9gim_arrayI17GIM_BOX_TREE_NODEEixEm(%class.gim_array* %m_node_array18, i32 %19)
  %m_bound20 = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %call19, i32 0, i32 0
  %20 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %21 = load i32, i32* %splitIndex, align 4
  %call21 = call nonnull align 4 dereferenceable(36) %struct.GIM_AABB_DATA* @_ZN9gim_arrayI13GIM_AABB_DATAEixEm(%class.gim_array.0* %20, i32 %21)
  %m_bound22 = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %call21, i32 0, i32 0
  call void @_ZN8GIM_AABB5mergeERKS_(%class.GIM_AABB* %m_bound20, %class.GIM_AABB* nonnull align 4 dereferenceable(32) %m_bound22)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %22 = load i32, i32* %splitIndex, align 4
  %inc23 = add i32 %22, 1
  store i32 %inc23, i32* %splitIndex, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %23 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %24 = load i32, i32* %startIndex.addr, align 4
  %25 = load i32, i32* %endIndex.addr, align 4
  %call24 = call i32 @_ZN12GIM_BOX_TREE20_calc_splitting_axisER9gim_arrayI13GIM_AABB_DATAEjj(%class.GIM_BOX_TREE* %this1, %class.gim_array.0* nonnull align 4 dereferenceable(12) %23, i32 %24, i32 %25)
  store i32 %call24, i32* %splitIndex, align 4
  %26 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %27 = load i32, i32* %startIndex.addr, align 4
  %28 = load i32, i32* %endIndex.addr, align 4
  %29 = load i32, i32* %splitIndex, align 4
  %call25 = call i32 @_ZN12GIM_BOX_TREE30_sort_and_calc_splitting_indexER9gim_arrayI13GIM_AABB_DATAEjjj(%class.GIM_BOX_TREE* %this1, %class.gim_array.0* nonnull align 4 dereferenceable(12) %26, i32 %27, i32 %28, i32 %29)
  store i32 %call25, i32* %splitIndex, align 4
  %m_num_nodes26 = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 0
  %30 = load i32, i32* %m_num_nodes26, align 4
  %m_node_array27 = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 1
  %31 = load i32, i32* %current_index, align 4
  %call28 = call nonnull align 4 dereferenceable(48) %struct.GIM_BOX_TREE_NODE* @_ZN9gim_arrayI17GIM_BOX_TREE_NODEEixEm(%class.gim_array* %m_node_array27, i32 %31)
  %m_left29 = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %call28, i32 0, i32 1
  store i32 %30, i32* %m_left29, align 4
  %32 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %33 = load i32, i32* %startIndex.addr, align 4
  %34 = load i32, i32* %splitIndex, align 4
  call void @_ZN12GIM_BOX_TREE15_build_sub_treeER9gim_arrayI13GIM_AABB_DATAEjj(%class.GIM_BOX_TREE* %this1, %class.gim_array.0* nonnull align 4 dereferenceable(12) %32, i32 %33, i32 %34)
  %m_num_nodes30 = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 0
  %35 = load i32, i32* %m_num_nodes30, align 4
  %m_node_array31 = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 1
  %36 = load i32, i32* %current_index, align 4
  %call32 = call nonnull align 4 dereferenceable(48) %struct.GIM_BOX_TREE_NODE* @_ZN9gim_arrayI17GIM_BOX_TREE_NODEEixEm(%class.gim_array* %m_node_array31, i32 %36)
  %m_right33 = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %call32, i32 0, i32 2
  store i32 %35, i32* %m_right33, align 4
  %37 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %38 = load i32, i32* %splitIndex, align 4
  %39 = load i32, i32* %endIndex.addr, align 4
  call void @_ZN12GIM_BOX_TREE15_build_sub_treeER9gim_arrayI13GIM_AABB_DATAEjj(%class.GIM_BOX_TREE* %this1, %class.gim_array.0* nonnull align 4 dereferenceable(12) %37, i32 %38, i32 %39)
  %m_num_nodes34 = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 0
  %40 = load i32, i32* %m_num_nodes34, align 4
  %41 = load i32, i32* %current_index, align 4
  %sub35 = sub i32 %40, %41
  %m_node_array36 = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 1
  %42 = load i32, i32* %current_index, align 4
  %call37 = call nonnull align 4 dereferenceable(48) %struct.GIM_BOX_TREE_NODE* @_ZN9gim_arrayI17GIM_BOX_TREE_NODEEixEm(%class.gim_array* %m_node_array36, i32 %42)
  %m_escapeIndex38 = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %call37, i32 0, i32 3
  store i32 %sub35, i32* %m_escapeIndex38, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %struct.GIM_BOX_TREE_NODE* @_ZN9gim_arrayI17GIM_BOX_TREE_NODEEixEm(%class.gim_array* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.gim_array*, align 4
  %i.addr = alloca i32, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  %m_data = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  %0 = load %struct.GIM_BOX_TREE_NODE*, %struct.GIM_BOX_TREE_NODE** %m_data, align 4
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %0, i32 %1
  ret %struct.GIM_BOX_TREE_NODE* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN8GIM_AABB10invalidateEv(%class.GIM_AABB* %this) #1 comdat {
entry:
  %this.addr = alloca %class.GIM_AABB*, align 4
  store %class.GIM_AABB* %this, %class.GIM_AABB** %this.addr, align 4
  %this1 = load %class.GIM_AABB*, %class.GIM_AABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  store float 0x47EFFFFFE0000000, float* %arrayidx, align 4
  %m_min2 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 0
  %call3 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 1
  store float 0x47EFFFFFE0000000, float* %arrayidx4, align 4
  %m_min5 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 0
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  store float 0x47EFFFFFE0000000, float* %arrayidx7, align 4
  %m_max = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 1
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  store float 0xC7EFFFFFE0000000, float* %arrayidx9, align 4
  %m_max10 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 1
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max10)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 1
  store float 0xC7EFFFFFE0000000, float* %arrayidx12, align 4
  %m_max13 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 1
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max13)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  store float 0xC7EFFFFFE0000000, float* %arrayidx15, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN8GIM_AABB5mergeERKS_(%class.GIM_AABB* %this, %class.GIM_AABB* nonnull align 4 dereferenceable(32) %box) #2 comdat {
entry:
  %this.addr = alloca %class.GIM_AABB*, align 4
  %box.addr = alloca %class.GIM_AABB*, align 4
  store %class.GIM_AABB* %this, %class.GIM_AABB** %this.addr, align 4
  store %class.GIM_AABB* %box, %class.GIM_AABB** %box.addr, align 4
  %this1 = load %class.GIM_AABB*, %class.GIM_AABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.GIM_AABB*, %class.GIM_AABB** %box.addr, align 4
  %m_min2 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %1, i32 0, i32 0
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 0
  %2 = load float, float* %arrayidx4, align 4
  %cmp = fcmp ogt float %0, %2
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %class.GIM_AABB*, %class.GIM_AABB** %box.addr, align 4
  %m_min5 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %3, i32 0, i32 0
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 0
  %4 = load float, float* %arrayidx7, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_min8 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 0
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 0
  %5 = load float, float* %arrayidx10, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %4, %cond.true ], [ %5, %cond.false ]
  %m_min11 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 0
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float %cond, float* %arrayidx13, align 4
  %m_min14 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 0
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min14)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 1
  %6 = load float, float* %arrayidx16, align 4
  %7 = load %class.GIM_AABB*, %class.GIM_AABB** %box.addr, align 4
  %m_min17 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %7, i32 0, i32 0
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 1
  %8 = load float, float* %arrayidx19, align 4
  %cmp20 = fcmp ogt float %6, %8
  br i1 %cmp20, label %cond.true21, label %cond.false25

cond.true21:                                      ; preds = %cond.end
  %9 = load %class.GIM_AABB*, %class.GIM_AABB** %box.addr, align 4
  %m_min22 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %9, i32 0, i32 0
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min22)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 1
  %10 = load float, float* %arrayidx24, align 4
  br label %cond.end29

cond.false25:                                     ; preds = %cond.end
  %m_min26 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 0
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min26)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 1
  %11 = load float, float* %arrayidx28, align 4
  br label %cond.end29

cond.end29:                                       ; preds = %cond.false25, %cond.true21
  %cond30 = phi float [ %10, %cond.true21 ], [ %11, %cond.false25 ]
  %m_min31 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 0
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min31)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 1
  store float %cond30, float* %arrayidx33, align 4
  %m_min34 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 0
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min34)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 2
  %12 = load float, float* %arrayidx36, align 4
  %13 = load %class.GIM_AABB*, %class.GIM_AABB** %box.addr, align 4
  %m_min37 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %13, i32 0, i32 0
  %call38 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min37)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 2
  %14 = load float, float* %arrayidx39, align 4
  %cmp40 = fcmp ogt float %12, %14
  br i1 %cmp40, label %cond.true41, label %cond.false45

cond.true41:                                      ; preds = %cond.end29
  %15 = load %class.GIM_AABB*, %class.GIM_AABB** %box.addr, align 4
  %m_min42 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %15, i32 0, i32 0
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min42)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 2
  %16 = load float, float* %arrayidx44, align 4
  br label %cond.end49

cond.false45:                                     ; preds = %cond.end29
  %m_min46 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 0
  %call47 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min46)
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 2
  %17 = load float, float* %arrayidx48, align 4
  br label %cond.end49

cond.end49:                                       ; preds = %cond.false45, %cond.true41
  %cond50 = phi float [ %16, %cond.true41 ], [ %17, %cond.false45 ]
  %m_min51 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 0
  %call52 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min51)
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 2
  store float %cond50, float* %arrayidx53, align 4
  %m_max = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 1
  %call54 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 0
  %18 = load float, float* %arrayidx55, align 4
  %19 = load %class.GIM_AABB*, %class.GIM_AABB** %box.addr, align 4
  %m_max56 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %19, i32 0, i32 1
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max56)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 0
  %20 = load float, float* %arrayidx58, align 4
  %cmp59 = fcmp olt float %18, %20
  br i1 %cmp59, label %cond.true60, label %cond.false64

cond.true60:                                      ; preds = %cond.end49
  %21 = load %class.GIM_AABB*, %class.GIM_AABB** %box.addr, align 4
  %m_max61 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %21, i32 0, i32 1
  %call62 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max61)
  %arrayidx63 = getelementptr inbounds float, float* %call62, i32 0
  %22 = load float, float* %arrayidx63, align 4
  br label %cond.end68

cond.false64:                                     ; preds = %cond.end49
  %m_max65 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 1
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max65)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 0
  %23 = load float, float* %arrayidx67, align 4
  br label %cond.end68

cond.end68:                                       ; preds = %cond.false64, %cond.true60
  %cond69 = phi float [ %22, %cond.true60 ], [ %23, %cond.false64 ]
  %m_max70 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 1
  %call71 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max70)
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 0
  store float %cond69, float* %arrayidx72, align 4
  %m_max73 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 1
  %call74 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max73)
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 1
  %24 = load float, float* %arrayidx75, align 4
  %25 = load %class.GIM_AABB*, %class.GIM_AABB** %box.addr, align 4
  %m_max76 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %25, i32 0, i32 1
  %call77 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max76)
  %arrayidx78 = getelementptr inbounds float, float* %call77, i32 1
  %26 = load float, float* %arrayidx78, align 4
  %cmp79 = fcmp olt float %24, %26
  br i1 %cmp79, label %cond.true80, label %cond.false84

cond.true80:                                      ; preds = %cond.end68
  %27 = load %class.GIM_AABB*, %class.GIM_AABB** %box.addr, align 4
  %m_max81 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %27, i32 0, i32 1
  %call82 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max81)
  %arrayidx83 = getelementptr inbounds float, float* %call82, i32 1
  %28 = load float, float* %arrayidx83, align 4
  br label %cond.end88

cond.false84:                                     ; preds = %cond.end68
  %m_max85 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 1
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max85)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 1
  %29 = load float, float* %arrayidx87, align 4
  br label %cond.end88

cond.end88:                                       ; preds = %cond.false84, %cond.true80
  %cond89 = phi float [ %28, %cond.true80 ], [ %29, %cond.false84 ]
  %m_max90 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 1
  %call91 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max90)
  %arrayidx92 = getelementptr inbounds float, float* %call91, i32 1
  store float %cond89, float* %arrayidx92, align 4
  %m_max93 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 1
  %call94 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max93)
  %arrayidx95 = getelementptr inbounds float, float* %call94, i32 2
  %30 = load float, float* %arrayidx95, align 4
  %31 = load %class.GIM_AABB*, %class.GIM_AABB** %box.addr, align 4
  %m_max96 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %31, i32 0, i32 1
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max96)
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 2
  %32 = load float, float* %arrayidx98, align 4
  %cmp99 = fcmp olt float %30, %32
  br i1 %cmp99, label %cond.true100, label %cond.false104

cond.true100:                                     ; preds = %cond.end88
  %33 = load %class.GIM_AABB*, %class.GIM_AABB** %box.addr, align 4
  %m_max101 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %33, i32 0, i32 1
  %call102 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max101)
  %arrayidx103 = getelementptr inbounds float, float* %call102, i32 2
  %34 = load float, float* %arrayidx103, align 4
  br label %cond.end108

cond.false104:                                    ; preds = %cond.end88
  %m_max105 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 1
  %call106 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max105)
  %arrayidx107 = getelementptr inbounds float, float* %call106, i32 2
  %35 = load float, float* %arrayidx107, align 4
  br label %cond.end108

cond.end108:                                      ; preds = %cond.false104, %cond.true100
  %cond109 = phi float [ %34, %cond.true100 ], [ %35, %cond.false104 ]
  %m_max110 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 1
  %call111 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max110)
  %arrayidx112 = getelementptr inbounds float, float* %call111, i32 2
  store float %cond109, float* %arrayidx112, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN12GIM_BOX_TREE10build_treeER9gim_arrayI13GIM_AABB_DATAE(%class.GIM_BOX_TREE* %this, %class.gim_array.0* nonnull align 4 dereferenceable(12) %primitive_boxes) #2 {
entry:
  %this.addr = alloca %class.GIM_BOX_TREE*, align 4
  %primitive_boxes.addr = alloca %class.gim_array.0*, align 4
  %ref.tmp = alloca %struct.GIM_BOX_TREE_NODE, align 4
  store %class.GIM_BOX_TREE* %this, %class.GIM_BOX_TREE** %this.addr, align 4
  store %class.gim_array.0* %primitive_boxes, %class.gim_array.0** %primitive_boxes.addr, align 4
  %this1 = load %class.GIM_BOX_TREE*, %class.GIM_BOX_TREE** %this.addr, align 4
  %m_num_nodes = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 0
  store i32 0, i32* %m_num_nodes, align 4
  %m_node_array = getelementptr inbounds %class.GIM_BOX_TREE, %class.GIM_BOX_TREE* %this1, i32 0, i32 1
  %0 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %call = call i32 @_ZNK9gim_arrayI13GIM_AABB_DATAE4sizeEv(%class.gim_array.0* %0)
  %mul = mul i32 %call, 2
  %call2 = call %struct.GIM_BOX_TREE_NODE* @_ZN17GIM_BOX_TREE_NODEC2Ev(%struct.GIM_BOX_TREE_NODE* %ref.tmp)
  call void @_ZN9gim_arrayI17GIM_BOX_TREE_NODEE6resizeEjbRKS0_(%class.gim_array* %m_node_array, i32 %mul, i1 zeroext true, %struct.GIM_BOX_TREE_NODE* nonnull align 4 dereferenceable(48) %ref.tmp)
  %1 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %2 = load %class.gim_array.0*, %class.gim_array.0** %primitive_boxes.addr, align 4
  %call3 = call i32 @_ZNK9gim_arrayI13GIM_AABB_DATAE4sizeEv(%class.gim_array.0* %2)
  call void @_ZN12GIM_BOX_TREE15_build_sub_treeER9gim_arrayI13GIM_AABB_DATAEjj(%class.GIM_BOX_TREE* %this1, %class.gim_array.0* nonnull align 4 dereferenceable(12) %1, i32 0, i32 %call3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9gim_arrayI17GIM_BOX_TREE_NODEE6resizeEjbRKS0_(%class.gim_array* %this, i32 %size, i1 zeroext %call_constructor, %struct.GIM_BOX_TREE_NODE* nonnull align 4 dereferenceable(48) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.gim_array*, align 4
  %size.addr = alloca i32, align 4
  %call_constructor.addr = alloca i8, align 1
  %fillData.addr = alloca %struct.GIM_BOX_TREE_NODE*, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %frombool = zext i1 %call_constructor to i8
  store i8 %frombool, i8* %call_constructor.addr, align 1
  store %struct.GIM_BOX_TREE_NODE* %fillData, %struct.GIM_BOX_TREE_NODE** %fillData.addr, align 4
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %m_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_size, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else8

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %size.addr, align 4
  %call = call zeroext i1 @_ZN9gim_arrayI17GIM_BOX_TREE_NODEE7reserveEj(%class.gim_array* %this1, i32 %2)
  %3 = load i8, i8* %call_constructor.addr, align 1
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then2
  %m_size3 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %4 = load i32, i32* %m_size3, align 4
  %5 = load i32, i32* %size.addr, align 4
  %cmp4 = icmp ult i32 %4, %5
  br i1 %cmp4, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = load %struct.GIM_BOX_TREE_NODE*, %struct.GIM_BOX_TREE_NODE** %fillData.addr, align 4
  %m_data = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  %7 = load %struct.GIM_BOX_TREE_NODE*, %struct.GIM_BOX_TREE_NODE** %m_data, align 4
  %m_size5 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %8 = load i32, i32* %m_size5, align 4
  %arrayidx = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %7, i32 %8
  %9 = bitcast %struct.GIM_BOX_TREE_NODE* %arrayidx to i8*
  %10 = bitcast %struct.GIM_BOX_TREE_NODE* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 48, i1 false)
  %m_size6 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %11 = load i32, i32* %m_size6, align 4
  %inc = add i32 %11, 1
  store i32 %inc, i32* %m_size6, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %if.end

if.else:                                          ; preds = %if.then
  %12 = load i32, i32* %size.addr, align 4
  %m_size7 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  store i32 %12, i32* %m_size7, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %while.end
  br label %if.end17

if.else8:                                         ; preds = %entry
  %13 = load i32, i32* %size.addr, align 4
  %m_size9 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %14 = load i32, i32* %m_size9, align 4
  %cmp10 = icmp ult i32 %13, %14
  br i1 %cmp10, label %if.then11, label %if.end16

if.then11:                                        ; preds = %if.else8
  %15 = load i8, i8* %call_constructor.addr, align 1
  %tobool12 = trunc i8 %15 to i1
  br i1 %tobool12, label %if.then13, label %if.end14

if.then13:                                        ; preds = %if.then11
  %16 = load i32, i32* %size.addr, align 4
  call void @_ZN9gim_arrayI17GIM_BOX_TREE_NODEE11clear_rangeEj(%class.gim_array* %this1, i32 %16)
  br label %if.end14

if.end14:                                         ; preds = %if.then13, %if.then11
  %17 = load i32, i32* %size.addr, align 4
  %m_size15 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  store i32 %17, i32* %m_size15, align 4
  br label %if.end16

if.end16:                                         ; preds = %if.end14, %if.else8
  br label %if.end17

if.end17:                                         ; preds = %if.end16, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9gim_arrayI13GIM_AABB_DATAE4sizeEv(%class.gim_array.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.gim_array.0*, align 4
  store %class.gim_array.0* %this, %class.gim_array.0** %this.addr, align 4
  %this1 = load %class.gim_array.0*, %class.gim_array.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.gim_array.0, %class.gim_array.0* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.GIM_BOX_TREE_NODE* @_ZN17GIM_BOX_TREE_NODEC2Ev(%struct.GIM_BOX_TREE_NODE* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.GIM_BOX_TREE_NODE*, align 4
  store %struct.GIM_BOX_TREE_NODE* %this, %struct.GIM_BOX_TREE_NODE** %this.addr, align 4
  %this1 = load %struct.GIM_BOX_TREE_NODE*, %struct.GIM_BOX_TREE_NODE** %this.addr, align 4
  %m_bound = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %this1, i32 0, i32 0
  %call = call %class.GIM_AABB* @_ZN8GIM_AABBC2Ev(%class.GIM_AABB* %m_bound)
  %m_left = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %this1, i32 0, i32 1
  store i32 0, i32* %m_left, align 4
  %m_right = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %this1, i32 0, i32 2
  store i32 0, i32* %m_right, align 4
  %m_escapeIndex = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %this1, i32 0, i32 3
  store i32 0, i32* %m_escapeIndex, align 4
  %m_data = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %this1, i32 0, i32 4
  store i32 0, i32* %m_data, align 4
  ret %struct.GIM_BOX_TREE_NODE* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.GIM_AABB* @_ZN8GIM_AABBC2Ev(%class.GIM_AABB* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.GIM_AABB*, align 4
  store %class.GIM_AABB* %this, %class.GIM_AABB** %this.addr, align 4
  %this1 = load %class.GIM_AABB*, %class.GIM_AABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_min)
  %m_max = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_max)
  ret %class.GIM_AABB* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z17gim_swap_elementsI13GIM_AABB_DATAEvPT_mm(%struct.GIM_AABB_DATA* %_array, i32 %_i, i32 %_j) #2 comdat {
entry:
  %_array.addr = alloca %struct.GIM_AABB_DATA*, align 4
  %_i.addr = alloca i32, align 4
  %_j.addr = alloca i32, align 4
  %_e_tmp_ = alloca %struct.GIM_AABB_DATA, align 4
  store %struct.GIM_AABB_DATA* %_array, %struct.GIM_AABB_DATA** %_array.addr, align 4
  store i32 %_i, i32* %_i.addr, align 4
  store i32 %_j, i32* %_j.addr, align 4
  %0 = load %struct.GIM_AABB_DATA*, %struct.GIM_AABB_DATA** %_array.addr, align 4
  %1 = load i32, i32* %_i.addr, align 4
  %arrayidx = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %0, i32 %1
  %call = call %struct.GIM_AABB_DATA* @_ZN13GIM_AABB_DATAC2ERKS_(%struct.GIM_AABB_DATA* %_e_tmp_, %struct.GIM_AABB_DATA* nonnull align 4 dereferenceable(36) %arrayidx)
  %2 = load %struct.GIM_AABB_DATA*, %struct.GIM_AABB_DATA** %_array.addr, align 4
  %3 = load i32, i32* %_j.addr, align 4
  %arrayidx1 = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %2, i32 %3
  %4 = load %struct.GIM_AABB_DATA*, %struct.GIM_AABB_DATA** %_array.addr, align 4
  %5 = load i32, i32* %_i.addr, align 4
  %arrayidx2 = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %4, i32 %5
  %6 = bitcast %struct.GIM_AABB_DATA* %arrayidx2 to i8*
  %7 = bitcast %struct.GIM_AABB_DATA* %arrayidx1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 36, i1 false)
  %8 = load %struct.GIM_AABB_DATA*, %struct.GIM_AABB_DATA** %_array.addr, align 4
  %9 = load i32, i32* %_j.addr, align 4
  %arrayidx3 = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %8, i32 %9
  %10 = bitcast %struct.GIM_AABB_DATA* %arrayidx3 to i8*
  %11 = bitcast %struct.GIM_AABB_DATA* %_e_tmp_ to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 36, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.GIM_AABB_DATA* @_ZN13GIM_AABB_DATAC2ERKS_(%struct.GIM_AABB_DATA* returned %this, %struct.GIM_AABB_DATA* nonnull align 4 dereferenceable(36) %0) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.GIM_AABB_DATA*, align 4
  %.addr = alloca %struct.GIM_AABB_DATA*, align 4
  store %struct.GIM_AABB_DATA* %this, %struct.GIM_AABB_DATA** %this.addr, align 4
  store %struct.GIM_AABB_DATA* %0, %struct.GIM_AABB_DATA** %.addr, align 4
  %this1 = load %struct.GIM_AABB_DATA*, %struct.GIM_AABB_DATA** %this.addr, align 4
  %m_bound = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %this1, i32 0, i32 0
  %1 = load %struct.GIM_AABB_DATA*, %struct.GIM_AABB_DATA** %.addr, align 4
  %m_bound2 = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %1, i32 0, i32 0
  %call = call %class.GIM_AABB* @_ZN8GIM_AABBC2ERKS_(%class.GIM_AABB* %m_bound, %class.GIM_AABB* nonnull align 4 dereferenceable(32) %m_bound2)
  %m_data = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %this1, i32 0, i32 1
  %2 = load %struct.GIM_AABB_DATA*, %struct.GIM_AABB_DATA** %.addr, align 4
  %m_data3 = getelementptr inbounds %struct.GIM_AABB_DATA, %struct.GIM_AABB_DATA* %2, i32 0, i32 1
  %3 = load i32, i32* %m_data3, align 4
  store i32 %3, i32* %m_data, align 4
  ret %struct.GIM_AABB_DATA* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.GIM_AABB* @_ZN8GIM_AABBC2ERKS_(%class.GIM_AABB* returned %this, %class.GIM_AABB* nonnull align 4 dereferenceable(32) %other) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.GIM_AABB*, align 4
  %other.addr = alloca %class.GIM_AABB*, align 4
  store %class.GIM_AABB* %this, %class.GIM_AABB** %this.addr, align 4
  store %class.GIM_AABB* %other, %class.GIM_AABB** %other.addr, align 4
  %this1 = load %class.GIM_AABB*, %class.GIM_AABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 0
  %0 = load %class.GIM_AABB*, %class.GIM_AABB** %other.addr, align 4
  %m_min2 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %0, i32 0, i32 0
  %1 = bitcast %class.btVector3* %m_min to i8*
  %2 = bitcast %class.btVector3* %m_min2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_max = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %this1, i32 0, i32 1
  %3 = load %class.GIM_AABB*, %class.GIM_AABB** %other.addr, align 4
  %m_max3 = getelementptr inbounds %class.GIM_AABB, %class.GIM_AABB* %3, i32 0, i32 1
  %4 = bitcast %class.btVector3* %m_max to i8*
  %5 = bitcast %class.btVector3* %m_max3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  ret %class.GIM_AABB* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZN9gim_arrayI17GIM_BOX_TREE_NODEE7reserveEj(%class.gim_array* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.gim_array*, align 4
  %size.addr = alloca i32, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  %m_allocated_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_allocated_size, align 4
  %1 = load i32, i32* %size.addr, align 4
  %cmp = icmp uge i32 %0, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load i32, i32* %size.addr, align 4
  %call = call zeroext i1 @_ZN9gim_arrayI17GIM_BOX_TREE_NODEE10resizeDataEj(%class.gim_array* %this1, i32 %2)
  store i1 %call, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i1, i1* %retval, align 1
  ret i1 %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9gim_arrayI17GIM_BOX_TREE_NODEE11clear_rangeEj(%class.gim_array* %this, i32 %start_range) #1 comdat {
entry:
  %this.addr = alloca %class.gim_array*, align 4
  %start_range.addr = alloca i32, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4
  store i32 %start_range, i32* %start_range.addr, align 4
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %m_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_size, align 4
  %1 = load i32, i32* %start_range.addr, align 4
  %cmp = icmp ugt i32 %0, %1
  br i1 %cmp, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_data = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  %2 = load %struct.GIM_BOX_TREE_NODE*, %struct.GIM_BOX_TREE_NODE** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %3 = load i32, i32* %m_size2, align 4
  %dec = add i32 %3, -1
  store i32 %dec, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %struct.GIM_BOX_TREE_NODE, %struct.GIM_BOX_TREE_NODE* %2, i32 %dec
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZN9gim_arrayI17GIM_BOX_TREE_NODEE10resizeDataEj(%class.gim_array* %this, i32 %newsize) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.gim_array*, align 4
  %newsize.addr = alloca i32, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  call void @_ZN9gim_arrayI17GIM_BOX_TREE_NODEE11destroyDataEv(%class.gim_array* %this1)
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %m_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_size, align 4
  %cmp2 = icmp ugt i32 %1, 0
  br i1 %cmp2, label %if.then3, label %if.else

if.then3:                                         ; preds = %if.end
  %m_data = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  %2 = load %struct.GIM_BOX_TREE_NODE*, %struct.GIM_BOX_TREE_NODE** %m_data, align 4
  %3 = bitcast %struct.GIM_BOX_TREE_NODE* %2 to i8*
  %m_size4 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 1
  %4 = load i32, i32* %m_size4, align 4
  %mul = mul i32 %4, 48
  %5 = load i32, i32* %newsize.addr, align 4
  %mul5 = mul i32 %5, 48
  %call = call i8* @_Z11gim_reallocPvmm(i8* %3, i32 %mul, i32 %mul5)
  %6 = bitcast i8* %call to %struct.GIM_BOX_TREE_NODE*
  %m_data6 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  store %struct.GIM_BOX_TREE_NODE* %6, %struct.GIM_BOX_TREE_NODE** %m_data6, align 4
  br label %if.end10

if.else:                                          ; preds = %if.end
  %7 = load i32, i32* %newsize.addr, align 4
  %mul7 = mul i32 %7, 48
  %call8 = call i8* @_Z9gim_allocm(i32 %mul7)
  %8 = bitcast i8* %call8 to %struct.GIM_BOX_TREE_NODE*
  %m_data9 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  store %struct.GIM_BOX_TREE_NODE* %8, %struct.GIM_BOX_TREE_NODE** %m_data9, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.else, %if.then3
  %9 = load i32, i32* %newsize.addr, align 4
  %m_allocated_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 2
  store i32 %9, i32* %m_allocated_size, align 4
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end10, %if.then
  %10 = load i1, i1* %retval, align 1
  ret i1 %10
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9gim_arrayI17GIM_BOX_TREE_NODEE11destroyDataEv(%class.gim_array* %this) #2 comdat {
entry:
  %this.addr = alloca %class.gim_array*, align 4
  store %class.gim_array* %this, %class.gim_array** %this.addr, align 4
  %this1 = load %class.gim_array*, %class.gim_array** %this.addr, align 4
  %m_allocated_size = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 2
  store i32 0, i32* %m_allocated_size, align 4
  %m_data = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  %0 = load %struct.GIM_BOX_TREE_NODE*, %struct.GIM_BOX_TREE_NODE** %m_data, align 4
  %cmp = icmp eq %struct.GIM_BOX_TREE_NODE* %0, null
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %m_data2 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  %1 = load %struct.GIM_BOX_TREE_NODE*, %struct.GIM_BOX_TREE_NODE** %m_data2, align 4
  %2 = bitcast %struct.GIM_BOX_TREE_NODE* %1 to i8*
  call void @_Z8gim_freePv(i8* %2)
  %m_data3 = getelementptr inbounds %class.gim_array, %class.gim_array* %this1, i32 0, i32 0
  store %struct.GIM_BOX_TREE_NODE* null, %struct.GIM_BOX_TREE_NODE** %m_data3, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

declare i8* @_Z11gim_reallocPvmm(i8*, i32, i32) #4

declare i8* @_Z9gim_allocm(i32) #4

declare void @_Z8gim_freePv(i8*) #4

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_gim_box_set.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
