; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Featherstone/btMultiBodyFixedConstraint.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Featherstone/btMultiBodyFixedConstraint.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btMultiBodyFixedConstraint = type { %class.btMultiBodyConstraint, %class.btRigidBody*, %class.btRigidBody*, %class.btVector3, %class.btVector3, %class.btMatrix3x3, %class.btMatrix3x3 }
%class.btMultiBodyConstraint = type { i32 (...)**, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i32, i32, i32, i8, i32, float, %class.btAlignedObjectArray.8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btMultiBody = type <{ i32 (...)**, %class.btMultiBodyLinkCollider*, i8*, %class.btVector3, %class.btQuaternion, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3, i8, i8, i8, i8, float, i8*, i32, i32, i32, float, float, i8, [3 x i8], float, float, i8, i8, [2 x i8], i32, i32, i8, i8, i8, i8 }>
%class.btMultiBodyLinkCollider = type { %class.btCollisionObject, %class.btMultiBody*, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btQuaternion = type { %class.btQuadWord }
%class.btQuadWord = type { [4 x float] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btMultibodyLink*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btMultibodyLink = type { float, %class.btVector3, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %struct.btSpatialMotionVector, %struct.btSpatialMotionVector, [6 x %struct.btSpatialMotionVector], i32, i32, %class.btQuaternion, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, [7 x float], [6 x float], %class.btMultiBodyLinkCollider*, i32, i32, i32, i32, %struct.btMultiBodyJointFeedback*, %class.btTransform, i8*, i8*, i8*, float, float }
%struct.btSpatialMotionVector = type { %class.btVector3, %class.btVector3 }
%struct.btMultiBodyJointFeedback = type opaque
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btMultiBodyLinkCollider**, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btMatrix3x3*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.20, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%class.btAlignedObjectArray.20 = type <{ %class.btAlignedAllocator.21, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.21 = type { i8 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.23, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%struct.btTypedObject = type { i32 }
%union.anon.23 = type { i32 }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.25 = type <{ %class.btAlignedAllocator.26, [3 x i8], i32, i32, %struct.btMultiBodySolverConstraint*, i8, [3 x i8] }>
%class.btAlignedAllocator.26 = type { i8 }
%struct.btMultiBodySolverConstraint = type { i32, i32, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, float, float, float, float, %union.anon.28, i32, i32, i32, %class.btMultiBody*, i32, i32, %class.btMultiBody*, i32, %class.btMultiBodyConstraint*, i32 }
%union.anon.28 = type { i8* }
%struct.btMultiBodyJacobianData = type { %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.30*, i32 }
%class.btAlignedObjectArray.30 = type opaque
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float, float }
%class.btIDebugDraw = type { i32 (...)** }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayIfE6resizeEiRKf = comdat any

$_ZN21btMultiBodyConstraintdlEPv = comdat any

$_ZNK17btCollisionObject12getIslandTagEv = comdat any

$_ZN11btMultiBody15getBaseColliderEv = comdat any

$_ZNK11btMultiBody11getNumLinksEv = comdat any

$_ZN11btMultiBody7getLinkEi = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZNK17btCollisionObject14getCompanionIdEv = comdat any

$_ZNK11btRigidBody24getCenterOfMassTransformEv = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZN11btMatrix3x3C2ERK12btQuaternion = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK11btMatrix3x37inverseEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZNK11btMatrix3x39getColumnEi = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN26btMultiBodyFixedConstraint11setFrameInBERK11btMatrix3x3 = comdat any

$_ZN26btMultiBodyFixedConstraint11setPivotInBERK9btVector3 = comdat any

$_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x311setRotationERK12btQuaternion = comdat any

$_ZNK12btQuaternion7length2Ev = comdat any

$_ZNK10btQuadWord1xEv = comdat any

$_ZNK10btQuadWord1yEv = comdat any

$_ZNK10btQuadWord1zEv = comdat any

$_ZNK10btQuadWord1wEv = comdat any

$_ZNK12btQuaternion3dotERKS_ = comdat any

$_ZNK11btMatrix3x35cofacEiiii = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_ = comdat any

$_ZN27btMultiBodySolverConstraintnwEmPv = comdat any

$_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV26btMultiBodyFixedConstraint = hidden unnamed_addr constant { [11 x i8*] } { [11 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI26btMultiBodyFixedConstraint to i8*), i8* bitcast (%class.btMultiBodyFixedConstraint* (%class.btMultiBodyFixedConstraint*)* @_ZN26btMultiBodyFixedConstraintD1Ev to i8*), i8* bitcast (void (%class.btMultiBodyFixedConstraint*)* @_ZN26btMultiBodyFixedConstraintD0Ev to i8*), i8* bitcast (void (%class.btMultiBodyFixedConstraint*, %class.btMatrix3x3*)* @_ZN26btMultiBodyFixedConstraint11setFrameInBERK11btMatrix3x3 to i8*), i8* bitcast (void (%class.btMultiBodyFixedConstraint*, %class.btVector3*)* @_ZN26btMultiBodyFixedConstraint11setPivotInBERK9btVector3 to i8*), i8* bitcast (void (%class.btMultiBodyFixedConstraint*)* @_ZN26btMultiBodyFixedConstraint16finalizeMultiDofEv to i8*), i8* bitcast (i32 (%class.btMultiBodyFixedConstraint*)* @_ZNK26btMultiBodyFixedConstraint12getIslandIdAEv to i8*), i8* bitcast (i32 (%class.btMultiBodyFixedConstraint*)* @_ZNK26btMultiBodyFixedConstraint12getIslandIdBEv to i8*), i8* bitcast (void (%class.btMultiBodyFixedConstraint*, %class.btAlignedObjectArray.25*, %struct.btMultiBodyJacobianData*, %struct.btContactSolverInfo*)* @_ZN26btMultiBodyFixedConstraint20createConstraintRowsER20btAlignedObjectArrayI27btMultiBodySolverConstraintER23btMultiBodyJacobianDataRK19btContactSolverInfo to i8*), i8* bitcast (void (%class.btMultiBodyFixedConstraint*, %class.btIDebugDraw*)* @_ZN26btMultiBodyFixedConstraint9debugDrawEP12btIDebugDraw to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS26btMultiBodyFixedConstraint = hidden constant [29 x i8] c"26btMultiBodyFixedConstraint\00", align 1
@_ZTI21btMultiBodyConstraint = external constant i8*
@_ZTI26btMultiBodyFixedConstraint = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([29 x i8], [29 x i8]* @_ZTS26btMultiBodyFixedConstraint, i32 0, i32 0), i8* bitcast (i8** @_ZTI21btMultiBodyConstraint to i8*) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btMultiBodyFixedConstraint.cpp, i8* null }]

@_ZN26btMultiBodyFixedConstraintC1EP11btMultiBodyiP11btRigidBodyRK9btVector3S6_RK11btMatrix3x3S9_ = hidden unnamed_addr alias %class.btMultiBodyFixedConstraint* (%class.btMultiBodyFixedConstraint*, %class.btMultiBody*, i32, %class.btRigidBody*, %class.btVector3*, %class.btVector3*, %class.btMatrix3x3*, %class.btMatrix3x3*), %class.btMultiBodyFixedConstraint* (%class.btMultiBodyFixedConstraint*, %class.btMultiBody*, i32, %class.btRigidBody*, %class.btVector3*, %class.btVector3*, %class.btMatrix3x3*, %class.btMatrix3x3*)* @_ZN26btMultiBodyFixedConstraintC2EP11btMultiBodyiP11btRigidBodyRK9btVector3S6_RK11btMatrix3x3S9_
@_ZN26btMultiBodyFixedConstraintC1EP11btMultiBodyiS1_iRK9btVector3S4_RK11btMatrix3x3S7_ = hidden unnamed_addr alias %class.btMultiBodyFixedConstraint* (%class.btMultiBodyFixedConstraint*, %class.btMultiBody*, i32, %class.btMultiBody*, i32, %class.btVector3*, %class.btVector3*, %class.btMatrix3x3*, %class.btMatrix3x3*), %class.btMultiBodyFixedConstraint* (%class.btMultiBodyFixedConstraint*, %class.btMultiBody*, i32, %class.btMultiBody*, i32, %class.btVector3*, %class.btVector3*, %class.btMatrix3x3*, %class.btMatrix3x3*)* @_ZN26btMultiBodyFixedConstraintC2EP11btMultiBodyiS1_iRK9btVector3S4_RK11btMatrix3x3S7_
@_ZN26btMultiBodyFixedConstraintD1Ev = hidden unnamed_addr alias %class.btMultiBodyFixedConstraint* (%class.btMultiBodyFixedConstraint*), %class.btMultiBodyFixedConstraint* (%class.btMultiBodyFixedConstraint*)* @_ZN26btMultiBodyFixedConstraintD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btMultiBodyFixedConstraint* @_ZN26btMultiBodyFixedConstraintC2EP11btMultiBodyiP11btRigidBodyRK9btVector3S6_RK11btMatrix3x3S9_(%class.btMultiBodyFixedConstraint* returned %this, %class.btMultiBody* %body, i32 %link, %class.btRigidBody* %bodyB, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInA, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %frameInA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %frameInB) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyFixedConstraint*, align 4
  %body.addr = alloca %class.btMultiBody*, align 4
  %link.addr = alloca i32, align 4
  %bodyB.addr = alloca %class.btRigidBody*, align 4
  %pivotInA.addr = alloca %class.btVector3*, align 4
  %pivotInB.addr = alloca %class.btVector3*, align 4
  %frameInA.addr = alloca %class.btMatrix3x3*, align 4
  %frameInB.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btMultiBodyFixedConstraint* %this, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  store %class.btMultiBody* %body, %class.btMultiBody** %body.addr, align 4
  store i32 %link, i32* %link.addr, align 4
  store %class.btRigidBody* %bodyB, %class.btRigidBody** %bodyB.addr, align 4
  store %class.btVector3* %pivotInA, %class.btVector3** %pivotInA.addr, align 4
  store %class.btVector3* %pivotInB, %class.btVector3** %pivotInB.addr, align 4
  store %class.btMatrix3x3* %frameInA, %class.btMatrix3x3** %frameInA.addr, align 4
  store %class.btMatrix3x3* %frameInB, %class.btMatrix3x3** %frameInB.addr, align 4
  %this1 = load %class.btMultiBodyFixedConstraint*, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %1 = load %class.btMultiBody*, %class.btMultiBody** %body.addr, align 4
  %2 = load i32, i32* %link.addr, align 4
  %call = call %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintC2EP11btMultiBodyS1_iiib(%class.btMultiBodyConstraint* %0, %class.btMultiBody* %1, %class.btMultiBody* null, i32 %2, i32 -1, i32 6, i1 zeroext false)
  %3 = bitcast %class.btMultiBodyFixedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTV26btMultiBodyFixedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %3, align 4
  %m_rigidBodyA = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 1
  store %class.btRigidBody* null, %class.btRigidBody** %m_rigidBodyA, align 4
  %m_rigidBodyB = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 2
  %4 = load %class.btRigidBody*, %class.btRigidBody** %bodyB.addr, align 4
  store %class.btRigidBody* %4, %class.btRigidBody** %m_rigidBodyB, align 4
  %m_pivotInA = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 3
  %5 = load %class.btVector3*, %class.btVector3** %pivotInA.addr, align 4
  %6 = bitcast %class.btVector3* %m_pivotInA to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_pivotInB = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 4
  %8 = load %class.btVector3*, %class.btVector3** %pivotInB.addr, align 4
  %9 = bitcast %class.btVector3* %m_pivotInB to i8*
  %10 = bitcast %class.btVector3* %8 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  %m_frameInA = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 5
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %frameInA.addr, align 4
  %call2 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_frameInA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %11)
  %m_frameInB = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 6
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %frameInB.addr, align 4
  %call3 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_frameInB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %12)
  %13 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %13, i32 0, i32 12
  store float 0.000000e+00, float* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.8* %m_data, i32 6, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btMultiBodyFixedConstraint* %this1
}

declare %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintC2EP11btMultiBodyS1_iiib(%class.btMultiBodyConstraint* returned, %class.btMultiBody*, %class.btMultiBody*, i32, i32, i32, i1 zeroext) unnamed_addr #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.8* %this, i32 %newsize, float* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca float*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store float* %fillData, float** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %5 = load float*, float** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %14 = load float*, float** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds float, float* %14, i32 %15
  %16 = bitcast float* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to float*
  %18 = load float*, float** %fillData.addr, align 4
  %19 = load float, float* %18, align 4
  store float %19, float* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden %class.btMultiBodyFixedConstraint* @_ZN26btMultiBodyFixedConstraintC2EP11btMultiBodyiS1_iRK9btVector3S4_RK11btMatrix3x3S7_(%class.btMultiBodyFixedConstraint* returned %this, %class.btMultiBody* %bodyA, i32 %linkA, %class.btMultiBody* %bodyB, i32 %linkB, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInA, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %frameInA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %frameInB) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyFixedConstraint*, align 4
  %bodyA.addr = alloca %class.btMultiBody*, align 4
  %linkA.addr = alloca i32, align 4
  %bodyB.addr = alloca %class.btMultiBody*, align 4
  %linkB.addr = alloca i32, align 4
  %pivotInA.addr = alloca %class.btVector3*, align 4
  %pivotInB.addr = alloca %class.btVector3*, align 4
  %frameInA.addr = alloca %class.btMatrix3x3*, align 4
  %frameInB.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btMultiBodyFixedConstraint* %this, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  store %class.btMultiBody* %bodyA, %class.btMultiBody** %bodyA.addr, align 4
  store i32 %linkA, i32* %linkA.addr, align 4
  store %class.btMultiBody* %bodyB, %class.btMultiBody** %bodyB.addr, align 4
  store i32 %linkB, i32* %linkB.addr, align 4
  store %class.btVector3* %pivotInA, %class.btVector3** %pivotInA.addr, align 4
  store %class.btVector3* %pivotInB, %class.btVector3** %pivotInB.addr, align 4
  store %class.btMatrix3x3* %frameInA, %class.btMatrix3x3** %frameInA.addr, align 4
  store %class.btMatrix3x3* %frameInB, %class.btMatrix3x3** %frameInB.addr, align 4
  %this1 = load %class.btMultiBodyFixedConstraint*, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %1 = load %class.btMultiBody*, %class.btMultiBody** %bodyA.addr, align 4
  %2 = load %class.btMultiBody*, %class.btMultiBody** %bodyB.addr, align 4
  %3 = load i32, i32* %linkA.addr, align 4
  %4 = load i32, i32* %linkB.addr, align 4
  %call = call %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintC2EP11btMultiBodyS1_iiib(%class.btMultiBodyConstraint* %0, %class.btMultiBody* %1, %class.btMultiBody* %2, i32 %3, i32 %4, i32 6, i1 zeroext false)
  %5 = bitcast %class.btMultiBodyFixedConstraint* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [11 x i8*] }, { [11 x i8*] }* @_ZTV26btMultiBodyFixedConstraint, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %5, align 4
  %m_rigidBodyA = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 1
  store %class.btRigidBody* null, %class.btRigidBody** %m_rigidBodyA, align 4
  %m_rigidBodyB = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 2
  store %class.btRigidBody* null, %class.btRigidBody** %m_rigidBodyB, align 4
  %m_pivotInA = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 3
  %6 = load %class.btVector3*, %class.btVector3** %pivotInA.addr, align 4
  %7 = bitcast %class.btVector3* %m_pivotInA to i8*
  %8 = bitcast %class.btVector3* %6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %m_pivotInB = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 4
  %9 = load %class.btVector3*, %class.btVector3** %pivotInB.addr, align 4
  %10 = bitcast %class.btVector3* %m_pivotInB to i8*
  %11 = bitcast %class.btVector3* %9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  %m_frameInA = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 5
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %frameInA.addr, align 4
  %call2 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_frameInA, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %12)
  %m_frameInB = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 6
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %frameInB.addr, align 4
  %call3 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_frameInB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %13)
  %14 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_data = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %14, i32 0, i32 12
  store float 0.000000e+00, float* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIfE6resizeEiRKf(%class.btAlignedObjectArray.8* %m_data, i32 6, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btMultiBodyFixedConstraint* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN26btMultiBodyFixedConstraint16finalizeMultiDofEv(%class.btMultiBodyFixedConstraint* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btMultiBodyFixedConstraint*, align 4
  store %class.btMultiBodyFixedConstraint* %this, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyFixedConstraint*, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btMultiBodyFixedConstraint* @_ZN26btMultiBodyFixedConstraintD2Ev(%class.btMultiBodyFixedConstraint* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btMultiBodyFixedConstraint*, align 4
  store %class.btMultiBodyFixedConstraint* %this, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyFixedConstraint*, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  %0 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %call = call %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintD2Ev(%class.btMultiBodyConstraint* %0) #6
  ret %class.btMultiBodyFixedConstraint* %this1
}

; Function Attrs: nounwind
declare %class.btMultiBodyConstraint* @_ZN21btMultiBodyConstraintD2Ev(%class.btMultiBodyConstraint* returned) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN26btMultiBodyFixedConstraintD0Ev(%class.btMultiBodyFixedConstraint* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btMultiBodyFixedConstraint*, align 4
  store %class.btMultiBodyFixedConstraint* %this, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyFixedConstraint*, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  %call = call %class.btMultiBodyFixedConstraint* @_ZN26btMultiBodyFixedConstraintD1Ev(%class.btMultiBodyFixedConstraint* %this1) #6
  %0 = bitcast %class.btMultiBodyFixedConstraint* %this1 to i8*
  call void @_ZN21btMultiBodyConstraintdlEPv(i8* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btMultiBodyConstraintdlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK26btMultiBodyFixedConstraint12getIslandIdAEv(%class.btMultiBodyFixedConstraint* %this) unnamed_addr #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btMultiBodyFixedConstraint*, align 4
  %col = alloca %class.btMultiBodyLinkCollider*, align 4
  %i = alloca i32, align 4
  store %class.btMultiBodyFixedConstraint* %this, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyFixedConstraint*, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  %m_rigidBodyA = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 1
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyA, align 4
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_rigidBodyA2 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 1
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyA2, align 4
  %2 = bitcast %class.btRigidBody* %1 to %class.btCollisionObject*
  %call = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %2)
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %3, i32 0, i32 1
  %4 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4
  %tobool3 = icmp ne %class.btMultiBody* %4, null
  br i1 %tobool3, label %if.then4, label %if.end22

if.then4:                                         ; preds = %if.end
  %5 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA5 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %5, i32 0, i32 1
  %6 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA5, align 4
  %call6 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %6)
  store %class.btMultiBodyLinkCollider* %call6, %class.btMultiBodyLinkCollider** %col, align 4
  %7 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %tobool7 = icmp ne %class.btMultiBodyLinkCollider* %7, null
  br i1 %tobool7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.then4
  %8 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %9 = bitcast %class.btMultiBodyLinkCollider* %8 to %class.btCollisionObject*
  %call9 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %9)
  store i32 %call9, i32* %retval, align 4
  br label %return

if.end10:                                         ; preds = %if.then4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end10
  %10 = load i32, i32* %i, align 4
  %11 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA11 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %11, i32 0, i32 1
  %12 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA11, align 4
  %call12 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %12)
  %cmp = icmp slt i32 %10, %call12
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA13 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %13, i32 0, i32 1
  %14 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA13, align 4
  %15 = load i32, i32* %i, align 4
  %call14 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %14, i32 %15)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call14, i32 0, i32 19
  %16 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4
  %tobool15 = icmp ne %class.btMultiBodyLinkCollider* %16, null
  br i1 %tobool15, label %if.then16, label %if.end21

if.then16:                                        ; preds = %for.body
  %17 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA17 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %17, i32 0, i32 1
  %18 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA17, align 4
  %19 = load i32, i32* %i, align 4
  %call18 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %18, i32 %19)
  %m_collider19 = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call18, i32 0, i32 19
  %20 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider19, align 4
  %21 = bitcast %class.btMultiBodyLinkCollider* %20 to %class.btCollisionObject*
  %call20 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %21)
  store i32 %call20, i32* %retval, align 4
  br label %return

if.end21:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end21
  %22 = load i32, i32* %i, align 4
  %inc = add nsw i32 %22, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end22

if.end22:                                         ; preds = %for.end, %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end22, %if.then16, %if.then8, %if.then
  %23 = load i32, i32* %retval, align 4
  ret i32 %23
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  %0 = load i32, i32* %m_islandTag1, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_baseCollider = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 1
  %0 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_baseCollider, align 4
  ret %class.btMultiBodyLinkCollider* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 11
  %call = call i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray.0* %m_links)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btMultiBody*, align 4
  %index.addr = alloca i32, align 4
  store %class.btMultiBody* %this, %class.btMultiBody** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btMultiBody*, %class.btMultiBody** %this.addr, align 4
  %m_links = getelementptr inbounds %class.btMultiBody, %class.btMultiBody* %this1, i32 0, i32 11
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray.0* %m_links, i32 %0)
  ret %struct.btMultibodyLink* %call
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK26btMultiBodyFixedConstraint12getIslandIdBEv(%class.btMultiBodyFixedConstraint* %this) unnamed_addr #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btMultiBodyFixedConstraint*, align 4
  %col = alloca %class.btMultiBodyLinkCollider*, align 4
  %i = alloca i32, align 4
  store %class.btMultiBodyFixedConstraint* %this, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  %this1 = load %class.btMultiBodyFixedConstraint*, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  %m_rigidBodyB = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 2
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyB, align 4
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_rigidBodyB2 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 2
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyB2, align 4
  %2 = bitcast %class.btRigidBody* %1 to %class.btCollisionObject*
  %call = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %2)
  store i32 %call, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %3 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %3, i32 0, i32 2
  %4 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB, align 4
  %tobool3 = icmp ne %class.btMultiBody* %4, null
  br i1 %tobool3, label %if.then4, label %if.end19

if.then4:                                         ; preds = %if.end
  %5 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB5 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %5, i32 0, i32 2
  %6 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB5, align 4
  %call6 = call %class.btMultiBodyLinkCollider* @_ZN11btMultiBody15getBaseColliderEv(%class.btMultiBody* %6)
  store %class.btMultiBodyLinkCollider* %call6, %class.btMultiBodyLinkCollider** %col, align 4
  %7 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %tobool7 = icmp ne %class.btMultiBodyLinkCollider* %7, null
  br i1 %tobool7, label %if.then8, label %if.end10

if.then8:                                         ; preds = %if.then4
  %8 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %9 = bitcast %class.btMultiBodyLinkCollider* %8 to %class.btCollisionObject*
  %call9 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %9)
  store i32 %call9, i32* %retval, align 4
  br label %return

if.end10:                                         ; preds = %if.then4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end10
  %10 = load i32, i32* %i, align 4
  %11 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB11 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %11, i32 0, i32 2
  %12 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB11, align 4
  %call12 = call i32 @_ZNK11btMultiBody11getNumLinksEv(%class.btMultiBody* %12)
  %cmp = icmp slt i32 %10, %call12
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %13 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB13 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %13, i32 0, i32 2
  %14 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB13, align 4
  %15 = load i32, i32* %i, align 4
  %call14 = call nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN11btMultiBody7getLinkEi(%class.btMultiBody* %14, i32 %15)
  %m_collider = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %call14, i32 0, i32 19
  %16 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %m_collider, align 4
  store %class.btMultiBodyLinkCollider* %16, %class.btMultiBodyLinkCollider** %col, align 4
  %17 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %tobool15 = icmp ne %class.btMultiBodyLinkCollider* %17, null
  br i1 %tobool15, label %if.then16, label %if.end18

if.then16:                                        ; preds = %for.body
  %18 = load %class.btMultiBodyLinkCollider*, %class.btMultiBodyLinkCollider** %col, align 4
  %19 = bitcast %class.btMultiBodyLinkCollider* %18 to %class.btCollisionObject*
  %call17 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %19)
  store i32 %call17, i32* %retval, align 4
  br label %return

if.end18:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end18
  %20 = load i32, i32* %i, align 4
  %inc = add nsw i32 %20, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end19

if.end19:                                         ; preds = %for.end, %if.end
  store i32 -1, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end19, %if.then16, %if.then8, %if.then
  %21 = load i32, i32* %retval, align 4
  ret i32 %21
}

; Function Attrs: noinline optnone
define hidden void @_ZN26btMultiBodyFixedConstraint20createConstraintRowsER20btAlignedObjectArrayI27btMultiBodySolverConstraintER23btMultiBodyJacobianDataRK19btContactSolverInfo(%class.btMultiBodyFixedConstraint* %this, %class.btAlignedObjectArray.25* nonnull align 4 dereferenceable(17) %constraintRows, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %data, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %infoGlobal) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyFixedConstraint*, align 4
  %constraintRows.addr = alloca %class.btAlignedObjectArray.25*, align 4
  %data.addr = alloca %struct.btMultiBodyJacobianData*, align 4
  %infoGlobal.addr = alloca %struct.btContactSolverInfo*, align 4
  %numDim = alloca i32, align 4
  %i = alloca i32, align 4
  %constraintRow = alloca %struct.btMultiBodySolverConstraint*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %pivotAworld = alloca %class.btVector3, align 4
  %frameAworld = alloca %class.btMatrix3x3, align 4
  %ref.tmp24 = alloca %class.btVector3, align 4
  %ref.tmp28 = alloca %class.btMatrix3x3, align 4
  %ref.tmp29 = alloca %class.btMatrix3x3, align 4
  %ref.tmp30 = alloca %class.btMatrix3x3, align 4
  %ref.tmp31 = alloca %class.btQuaternion, align 4
  %ref.tmp37 = alloca %class.btVector3, align 4
  %ref.tmp40 = alloca %class.btMatrix3x3, align 4
  %pivotBworld = alloca %class.btVector3, align 4
  %frameBworld = alloca %class.btMatrix3x3, align 4
  %ref.tmp51 = alloca %class.btVector3, align 4
  %ref.tmp55 = alloca %class.btMatrix3x3, align 4
  %ref.tmp56 = alloca %class.btMatrix3x3, align 4
  %ref.tmp57 = alloca %class.btMatrix3x3, align 4
  %ref.tmp58 = alloca %class.btQuaternion, align 4
  %ref.tmp65 = alloca %class.btVector3, align 4
  %ref.tmp68 = alloca %class.btMatrix3x3, align 4
  %relRot = alloca %class.btMatrix3x3, align 4
  %ref.tmp74 = alloca %class.btMatrix3x3, align 4
  %angleDiff = alloca %class.btVector3, align 4
  %constraintNormalLin = alloca %class.btVector3, align 4
  %ref.tmp77 = alloca float, align 4
  %ref.tmp78 = alloca float, align 4
  %ref.tmp79 = alloca float, align 4
  %constraintNormalAng = alloca %class.btVector3, align 4
  %ref.tmp81 = alloca float, align 4
  %ref.tmp82 = alloca float, align 4
  %ref.tmp83 = alloca float, align 4
  %posError = alloca float, align 4
  %ref.tmp88 = alloca %class.btVector3, align 4
  %ref.tmp93 = alloca %class.btVector3, align 4
  store %class.btMultiBodyFixedConstraint* %this, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  store %class.btAlignedObjectArray.25* %constraintRows, %class.btAlignedObjectArray.25** %constraintRows.addr, align 4
  store %struct.btMultiBodyJacobianData* %data, %struct.btMultiBodyJacobianData** %data.addr, align 4
  store %struct.btContactSolverInfo* %infoGlobal, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %this1 = load %class.btMultiBodyFixedConstraint*, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  store i32 6, i32* %numDim, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %numDim, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btAlignedObjectArray.25*, %class.btAlignedObjectArray.25** %constraintRows.addr, align 4
  %call = call nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.25* %2)
  store %struct.btMultiBodySolverConstraint* %call, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %3 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_orgConstraint = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %4, i32 0, i32 28
  store %class.btMultiBodyConstraint* %3, %class.btMultiBodyConstraint** %m_orgConstraint, align 4
  %5 = load i32, i32* %i, align 4
  %6 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_orgDofIndex = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %6, i32 0, i32 29
  store i32 %5, i32* %m_orgDofIndex, align 4
  %7 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_relpos1CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %7, i32 0, i32 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_relpos1CrossNormal, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %8 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_contactNormal1 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %8, i32 0, i32 5
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_contactNormal1, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %9 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_relpos2CrossNormal = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %9, i32 0, i32 6
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 0.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_relpos2CrossNormal, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  %10 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_contactNormal2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %10, i32 0, i32 7
  store float 0.000000e+00, float* %ref.tmp10, align 4
  store float 0.000000e+00, float* %ref.tmp11, align 4
  store float 0.000000e+00, float* %ref.tmp12, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_contactNormal2, float* nonnull align 4 dereferenceable(4) %ref.tmp10, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12)
  %11 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_angularComponentA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %11, i32 0, i32 8
  store float 0.000000e+00, float* %ref.tmp13, align 4
  store float 0.000000e+00, float* %ref.tmp14, align 4
  store float 0.000000e+00, float* %ref.tmp15, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_angularComponentA, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp15)
  %12 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_angularComponentB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %12, i32 0, i32 9
  store float 0.000000e+00, float* %ref.tmp16, align 4
  store float 0.000000e+00, float* %ref.tmp17, align 4
  store float 0.000000e+00, float* %ref.tmp18, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_angularComponentB, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18)
  %13 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_fixedBodyId = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %13, i32 0, i32 7
  %14 = load i32, i32* %m_fixedBodyId, align 4
  %15 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_solverBodyIdA = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %15, i32 0, i32 22
  store i32 %14, i32* %m_solverBodyIdA, align 4
  %16 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %m_fixedBodyId19 = getelementptr inbounds %struct.btMultiBodyJacobianData, %struct.btMultiBodyJacobianData* %16, i32 0, i32 7
  %17 = load i32, i32* %m_fixedBodyId19, align 4
  %18 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_solverBodyIdB = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %18, i32 0, i32 25
  store i32 %17, i32* %m_solverBodyIdB, align 4
  %m_pivotInA = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 3
  %19 = bitcast %class.btVector3* %pivotAworld to i8*
  %20 = bitcast %class.btVector3* %m_pivotInA to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  %m_frameInA = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 5
  %call20 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %frameAworld, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_frameInA)
  %m_rigidBodyA = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 1
  %21 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyA, align 4
  %tobool = icmp ne %class.btRigidBody* %21, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %m_rigidBodyA21 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 1
  %22 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyA21, align 4
  %23 = bitcast %class.btRigidBody* %22 to %class.btCollisionObject*
  %call22 = call i32 @_ZNK17btCollisionObject14getCompanionIdEv(%class.btCollisionObject* %23)
  %24 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_solverBodyIdA23 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %24, i32 0, i32 22
  store i32 %call22, i32* %m_solverBodyIdA23, align 4
  %m_rigidBodyA25 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 1
  %25 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyA25, align 4
  %call26 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %25)
  %m_pivotInA27 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp24, %class.btTransform* %call26, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pivotInA27)
  %26 = bitcast %class.btVector3* %pivotAworld to i8*
  %27 = bitcast %class.btVector3* %ref.tmp24 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %26, i8* align 4 %27, i32 16, i1 false)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp29, %class.btMatrix3x3* %frameAworld)
  %m_rigidBodyA32 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 1
  %28 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyA32, align 4
  call void @_ZNK11btRigidBody14getOrientationEv(%class.btQuaternion* sret align 4 %ref.tmp31, %class.btRigidBody* %28)
  %call33 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %ref.tmp30, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp31)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp28, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp29, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp30)
  %call34 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %frameAworld, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp28)
  br label %if.end44

if.else:                                          ; preds = %for.body
  %29 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %29, i32 0, i32 1
  %30 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4
  %tobool35 = icmp ne %class.btMultiBody* %30, null
  br i1 %tobool35, label %if.then36, label %if.end

if.then36:                                        ; preds = %if.else
  %31 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA38 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %31, i32 0, i32 1
  %32 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA38, align 4
  %33 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %33, i32 0, i32 3
  %34 = load i32, i32* %m_linkA, align 4
  %m_pivotInA39 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btMultiBody15localPosToWorldEiRK9btVector3(%class.btVector3* sret align 4 %ref.tmp37, %class.btMultiBody* %32, i32 %34, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pivotInA39)
  %35 = bitcast %class.btVector3* %pivotAworld to i8*
  %36 = bitcast %class.btVector3* %ref.tmp37 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %36, i32 16, i1 false)
  %37 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA41 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %37, i32 0, i32 1
  %38 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA41, align 4
  %39 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkA42 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %39, i32 0, i32 3
  %40 = load i32, i32* %m_linkA42, align 4
  call void @_ZNK11btMultiBody17localFrameToWorldEiRK11btMatrix3x3(%class.btMatrix3x3* sret align 4 %ref.tmp40, %class.btMultiBody* %38, i32 %40, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %frameAworld)
  %call43 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %frameAworld, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp40)
  br label %if.end

if.end:                                           ; preds = %if.then36, %if.else
  br label %if.end44

if.end44:                                         ; preds = %if.end, %if.then
  %m_pivotInB = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 4
  %41 = bitcast %class.btVector3* %pivotBworld to i8*
  %42 = bitcast %class.btVector3* %m_pivotInB to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %41, i8* align 4 %42, i32 16, i1 false)
  %m_frameInB = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 6
  %call45 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %frameBworld, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_frameInB)
  %m_rigidBodyB = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 2
  %43 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyB, align 4
  %tobool46 = icmp ne %class.btRigidBody* %43, null
  br i1 %tobool46, label %if.then47, label %if.else62

if.then47:                                        ; preds = %if.end44
  %m_rigidBodyB48 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 2
  %44 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyB48, align 4
  %45 = bitcast %class.btRigidBody* %44 to %class.btCollisionObject*
  %call49 = call i32 @_ZNK17btCollisionObject14getCompanionIdEv(%class.btCollisionObject* %45)
  %46 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %m_solverBodyIdB50 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %46, i32 0, i32 25
  store i32 %call49, i32* %m_solverBodyIdB50, align 4
  %m_rigidBodyB52 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 2
  %47 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyB52, align 4
  %call53 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %47)
  %m_pivotInB54 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp51, %class.btTransform* %call53, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pivotInB54)
  %48 = bitcast %class.btVector3* %pivotBworld to i8*
  %49 = bitcast %class.btVector3* %ref.tmp51 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %48, i8* align 4 %49, i32 16, i1 false)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp56, %class.btMatrix3x3* %frameBworld)
  %m_rigidBodyB59 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 2
  %50 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyB59, align 4
  call void @_ZNK11btRigidBody14getOrientationEv(%class.btQuaternion* sret align 4 %ref.tmp58, %class.btRigidBody* %50)
  %call60 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* %ref.tmp57, %class.btQuaternion* nonnull align 4 dereferenceable(16) %ref.tmp58)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp55, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp56, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp57)
  %call61 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %frameBworld, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp55)
  br label %if.end73

if.else62:                                        ; preds = %if.end44
  %51 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %51, i32 0, i32 2
  %52 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB, align 4
  %tobool63 = icmp ne %class.btMultiBody* %52, null
  br i1 %tobool63, label %if.then64, label %if.end72

if.then64:                                        ; preds = %if.else62
  %53 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB66 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %53, i32 0, i32 2
  %54 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB66, align 4
  %55 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %55, i32 0, i32 4
  %56 = load i32, i32* %m_linkB, align 4
  %m_pivotInB67 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 4
  call void @_ZNK11btMultiBody15localPosToWorldEiRK9btVector3(%class.btVector3* sret align 4 %ref.tmp65, %class.btMultiBody* %54, i32 %56, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pivotInB67)
  %57 = bitcast %class.btVector3* %pivotBworld to i8*
  %58 = bitcast %class.btVector3* %ref.tmp65 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %57, i8* align 4 %58, i32 16, i1 false)
  %59 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB69 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %59, i32 0, i32 2
  %60 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB69, align 4
  %61 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkB70 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %61, i32 0, i32 4
  %62 = load i32, i32* %m_linkB70, align 4
  call void @_ZNK11btMultiBody17localFrameToWorldEiRK11btMatrix3x3(%class.btMatrix3x3* sret align 4 %ref.tmp68, %class.btMultiBody* %60, i32 %62, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %frameBworld)
  %call71 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %frameBworld, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp68)
  br label %if.end72

if.end72:                                         ; preds = %if.then64, %if.else62
  br label %if.end73

if.end73:                                         ; preds = %if.end72, %if.then47
  call void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* sret align 4 %ref.tmp74, %class.btMatrix3x3* %frameAworld)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %relRot, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp74, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %frameBworld)
  %call75 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %angleDiff)
  %call76 = call zeroext i1 @_ZN30btGeneric6DofSpring2Constraint16matrixToEulerXYZERK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48) %relRot, %class.btVector3* nonnull align 4 dereferenceable(16) %angleDiff)
  store float 0.000000e+00, float* %ref.tmp77, align 4
  store float 0.000000e+00, float* %ref.tmp78, align 4
  store float 0.000000e+00, float* %ref.tmp79, align 4
  %call80 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %constraintNormalLin, float* nonnull align 4 dereferenceable(4) %ref.tmp77, float* nonnull align 4 dereferenceable(4) %ref.tmp78, float* nonnull align 4 dereferenceable(4) %ref.tmp79)
  store float 0.000000e+00, float* %ref.tmp81, align 4
  store float 0.000000e+00, float* %ref.tmp82, align 4
  store float 0.000000e+00, float* %ref.tmp83, align 4
  %call84 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %constraintNormalAng, float* nonnull align 4 dereferenceable(4) %ref.tmp81, float* nonnull align 4 dereferenceable(4) %ref.tmp82, float* nonnull align 4 dereferenceable(4) %ref.tmp83)
  store float 0.000000e+00, float* %posError, align 4
  %63 = load i32, i32* %i, align 4
  %cmp85 = icmp slt i32 %63, 3
  br i1 %cmp85, label %if.then86, label %if.else92

if.then86:                                        ; preds = %if.end73
  %call87 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %constraintNormalLin)
  %64 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call87, i32 %64
  store float -1.000000e+00, float* %arrayidx, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp88, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAworld, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBworld)
  %call89 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp88, %class.btVector3* nonnull align 4 dereferenceable(16) %constraintNormalLin)
  store float %call89, float* %posError, align 4
  %65 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %66 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %67 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %68 = load float, float* %posError, align 4
  %69 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %70 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_maxAppliedImpulse = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %70, i32 0, i32 11
  %71 = load float, float* %m_maxAppliedImpulse, align 4
  %fneg = fneg float %71
  %72 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_maxAppliedImpulse90 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %72, i32 0, i32 11
  %73 = load float, float* %m_maxAppliedImpulse90, align 4
  %call91 = call float @_ZN21btMultiBodyConstraint23fillMultiBodyConstraintER27btMultiBodySolverConstraintR23btMultiBodyJacobianDataPfS4_RK9btVector3S7_S7_S7_fRK19btContactSolverInfoffbfbff(%class.btMultiBodyConstraint* %65, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %66, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %67, float* null, float* null, %class.btVector3* nonnull align 4 dereferenceable(16) %constraintNormalAng, %class.btVector3* nonnull align 4 dereferenceable(16) %constraintNormalLin, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAworld, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBworld, float %68, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %69, float %fneg, float %73, i1 zeroext false, float 1.000000e+00, i1 zeroext false, float 0.000000e+00, float 0.000000e+00)
  br label %if.end101

if.else92:                                        ; preds = %if.end73
  %74 = load i32, i32* %i, align 4
  %rem = srem i32 %74, 3
  call void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* sret align 4 %ref.tmp93, %class.btMatrix3x3* %frameAworld, i32 %rem)
  %75 = bitcast %class.btVector3* %constraintNormalAng to i8*
  %76 = bitcast %class.btVector3* %ref.tmp93 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %75, i8* align 4 %76, i32 16, i1 false)
  %call94 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %angleDiff)
  %77 = load i32, i32* %i, align 4
  %rem95 = srem i32 %77, 3
  %arrayidx96 = getelementptr inbounds float, float* %call94, i32 %rem95
  %78 = load float, float* %arrayidx96, align 4
  store float %78, float* %posError, align 4
  %79 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %80 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %constraintRow, align 4
  %81 = load %struct.btMultiBodyJacobianData*, %struct.btMultiBodyJacobianData** %data.addr, align 4
  %82 = load float, float* %posError, align 4
  %83 = load %struct.btContactSolverInfo*, %struct.btContactSolverInfo** %infoGlobal.addr, align 4
  %84 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_maxAppliedImpulse97 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %84, i32 0, i32 11
  %85 = load float, float* %m_maxAppliedImpulse97, align 4
  %fneg98 = fneg float %85
  %86 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_maxAppliedImpulse99 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %86, i32 0, i32 11
  %87 = load float, float* %m_maxAppliedImpulse99, align 4
  %call100 = call float @_ZN21btMultiBodyConstraint23fillMultiBodyConstraintER27btMultiBodySolverConstraintR23btMultiBodyJacobianDataPfS4_RK9btVector3S7_S7_S7_fRK19btContactSolverInfoffbfbff(%class.btMultiBodyConstraint* %79, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192) %80, %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128) %81, float* null, float* null, %class.btVector3* nonnull align 4 dereferenceable(16) %constraintNormalAng, %class.btVector3* nonnull align 4 dereferenceable(16) %constraintNormalLin, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAworld, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBworld, float %82, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88) %83, float %fneg98, float %87, i1 zeroext true, float 1.000000e+00, i1 zeroext false, float 0.000000e+00, float 0.000000e+00)
  br label %if.end101

if.end101:                                        ; preds = %if.else92, %if.then86
  br label %for.inc

for.inc:                                          ; preds = %if.end101
  %88 = load i32, i32* %i, align 4
  %inc = add nsw i32 %88, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(192) %struct.btMultiBodySolverConstraint* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE21expandNonInitializingEv(%class.btAlignedObjectArray.25* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.25*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.25* %this, %class.btAlignedObjectArray.25** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.25*, %class.btAlignedObjectArray.25** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.25* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.25* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.25* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.25* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.25* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 2
  %1 = load i32, i32* %m_size, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 4
  %2 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %3 = load i32, i32* %sz, align 4
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %2, i32 %3
  ret %struct.btMultiBodySolverConstraint* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject14getCompanionIdEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_companionId = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 14
  %0 = load i32, i32* %m_companionId, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btRigidBody*, align 4
  store %class.btRigidBody* %this, %class.btRigidBody** %this.addr, align 4
  %this1 = load %class.btRigidBody*, %class.btRigidBody** %this.addr, align 4
  %0 = bitcast %class.btRigidBody* %this1 to %class.btCollisionObject*
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %0, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

declare void @_ZNK11btRigidBody14getOrientationEv(%class.btQuaternion* sret align 4, %class.btRigidBody*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERK12btQuaternion(%class.btMatrix3x3* returned %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  call void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %0)
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

declare void @_ZNK11btMultiBody15localPosToWorldEiRK9btVector3(%class.btVector3* sret align 4, %class.btMultiBody*, i32, %class.btVector3* nonnull align 4 dereferenceable(16)) #3

declare void @_ZNK11btMultiBody17localFrameToWorldEiRK11btMatrix3x3(%class.btMatrix3x3* sret align 4, %class.btMultiBody*, i32, %class.btMatrix3x3* nonnull align 4 dereferenceable(48)) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x37inverseEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %co = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %det = alloca float, align 4
  %s = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %call = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 1, i32 2, i32 2)
  store float %call, float* %ref.tmp, align 4
  %call3 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 2, i32 2, i32 0)
  store float %call3, float* %ref.tmp2, align 4
  %call5 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 1, i32 0, i32 2, i32 1)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %co, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this1, i32 0)
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %co)
  store float %call8, float* %det, align 4
  %1 = load float, float* %det, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %s, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %co)
  %2 = load float, float* %call10, align 4
  %3 = load float, float* %s, align 4
  %mul = fmul float %2, %3
  store float %mul, float* %ref.tmp9, align 4
  %call12 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 2, i32 1)
  %4 = load float, float* %s, align 4
  %mul13 = fmul float %call12, %4
  store float %mul13, float* %ref.tmp11, align 4
  %call15 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 1, i32 2)
  %5 = load float, float* %s, align 4
  %mul16 = fmul float %call15, %5
  store float %mul16, float* %ref.tmp14, align 4
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %co)
  %6 = load float, float* %call18, align 4
  %7 = load float, float* %s, align 4
  %mul19 = fmul float %6, %7
  store float %mul19, float* %ref.tmp17, align 4
  %call21 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 2, i32 2)
  %8 = load float, float* %s, align 4
  %mul22 = fmul float %call21, %8
  store float %mul22, float* %ref.tmp20, align 4
  %call24 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 2, i32 1, i32 0)
  %9 = load float, float* %s, align 4
  %mul25 = fmul float %call24, %9
  store float %mul25, float* %ref.tmp23, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %co)
  %10 = load float, float* %call27, align 4
  %11 = load float, float* %s, align 4
  %mul28 = fmul float %10, %11
  store float %mul28, float* %ref.tmp26, align 4
  %call30 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 1, i32 2, i32 0)
  %12 = load float, float* %s, align 4
  %mul31 = fmul float %call30, %12
  store float %mul31, float* %ref.tmp29, align 4
  %call33 = call float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this1, i32 0, i32 0, i32 1, i32 1)
  %13 = load float, float* %s, align 4
  %mul34 = fmul float %call33, %13
  store float %mul34, float* %ref.tmp32, align 4
  %call35 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp32)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

declare zeroext i1 @_ZN30btGeneric6DofSpring2Constraint16matrixToEulerXYZERK11btMatrix3x3R9btVector3(%class.btMatrix3x3* nonnull align 4 dereferenceable(48), %class.btVector3* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

declare float @_ZN21btMultiBodyConstraint23fillMultiBodyConstraintER27btMultiBodySolverConstraintR23btMultiBodyJacobianDataPfS4_RK9btVector3S7_S7_S7_fRK19btContactSolverInfoffbfbff(%class.btMultiBodyConstraint*, %struct.btMultiBodySolverConstraint* nonnull align 4 dereferenceable(192), %struct.btMultiBodyJacobianData* nonnull align 4 dereferenceable(128), float*, float*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), float, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88), float, float, i1 zeroext, float, i1 zeroext, float, float) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39getColumnEi(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, i32 %i) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %1 = load i32, i32* %i.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 2
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %2 = load i32, i32* %i.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %arrayidx2, float* nonnull align 4 dereferenceable(4) %arrayidx6, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN26btMultiBodyFixedConstraint9debugDrawEP12btIDebugDraw(%class.btMultiBodyFixedConstraint* %this, %class.btIDebugDraw* %drawer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btMultiBodyFixedConstraint*, align 4
  %drawer.addr = alloca %class.btIDebugDraw*, align 4
  %tr = alloca %class.btTransform, align 4
  %pivot = alloca %class.btVector3, align 4
  %pivotAworld = alloca %class.btVector3, align 4
  %pivot13 = alloca %class.btVector3, align 4
  %pivotBworld = alloca %class.btVector3, align 4
  store %class.btMultiBodyFixedConstraint* %this, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  store %class.btIDebugDraw* %drawer, %class.btIDebugDraw** %drawer.addr, align 4
  %this1 = load %class.btMultiBodyFixedConstraint*, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %tr)
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %tr)
  %m_rigidBodyA = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 1
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyA, align 4
  %tobool = icmp ne %class.btRigidBody* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_rigidBodyA2 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 1
  %1 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyA2, align 4
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %1)
  %m_pivotInA = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pivot, %class.btTransform* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pivotInA)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %tr, %class.btVector3* nonnull align 4 dereferenceable(16) %pivot)
  %2 = load %class.btIDebugDraw*, %class.btIDebugDraw** %drawer.addr, align 4
  %3 = bitcast %class.btIDebugDraw* %2 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable, i64 16
  %4 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn, align 4
  call void %4(%class.btIDebugDraw* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %tr, float 0x3FB99999A0000000)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %5 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %5, i32 0, i32 1
  %6 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA, align 4
  %tobool4 = icmp ne %class.btMultiBody* %6, null
  br i1 %tobool4, label %if.then5, label %if.end10

if.then5:                                         ; preds = %if.end
  %7 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyA6 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %7, i32 0, i32 1
  %8 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyA6, align 4
  %9 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkA = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %9, i32 0, i32 3
  %10 = load i32, i32* %m_linkA, align 4
  %m_pivotInA7 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 3
  call void @_ZNK11btMultiBody15localPosToWorldEiRK9btVector3(%class.btVector3* sret align 4 %pivotAworld, %class.btMultiBody* %8, i32 %10, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pivotInA7)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %tr, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotAworld)
  %11 = load %class.btIDebugDraw*, %class.btIDebugDraw** %drawer.addr, align 4
  %12 = bitcast %class.btIDebugDraw* %11 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable8 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %12, align 4
  %vfn9 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable8, i64 16
  %13 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn9, align 4
  call void %13(%class.btIDebugDraw* %11, %class.btTransform* nonnull align 4 dereferenceable(64) %tr, float 0x3FB99999A0000000)
  br label %if.end10

if.end10:                                         ; preds = %if.then5, %if.end
  %m_rigidBodyB = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 2
  %14 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyB, align 4
  %tobool11 = icmp ne %class.btRigidBody* %14, null
  br i1 %tobool11, label %if.then12, label %if.end18

if.then12:                                        ; preds = %if.end10
  %m_rigidBodyB14 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 2
  %15 = load %class.btRigidBody*, %class.btRigidBody** %m_rigidBodyB14, align 4
  %call15 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK11btRigidBody24getCenterOfMassTransformEv(%class.btRigidBody* %15)
  %m_pivotInB = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 4
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pivot13, %class.btTransform* %call15, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pivotInB)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %tr, %class.btVector3* nonnull align 4 dereferenceable(16) %pivot13)
  %16 = load %class.btIDebugDraw*, %class.btIDebugDraw** %drawer.addr, align 4
  %17 = bitcast %class.btIDebugDraw* %16 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable16 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %17, align 4
  %vfn17 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable16, i64 16
  %18 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn17, align 4
  call void %18(%class.btIDebugDraw* %16, %class.btTransform* nonnull align 4 dereferenceable(64) %tr, float 0x3FB99999A0000000)
  br label %if.end18

if.end18:                                         ; preds = %if.then12, %if.end10
  %19 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %19, i32 0, i32 2
  %20 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB, align 4
  %tobool19 = icmp ne %class.btMultiBody* %20, null
  br i1 %tobool19, label %if.then20, label %if.end25

if.then20:                                        ; preds = %if.end18
  %21 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_bodyB21 = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %21, i32 0, i32 2
  %22 = load %class.btMultiBody*, %class.btMultiBody** %m_bodyB21, align 4
  %23 = bitcast %class.btMultiBodyFixedConstraint* %this1 to %class.btMultiBodyConstraint*
  %m_linkB = getelementptr inbounds %class.btMultiBodyConstraint, %class.btMultiBodyConstraint* %23, i32 0, i32 4
  %24 = load i32, i32* %m_linkB, align 4
  %m_pivotInB22 = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 4
  call void @_ZNK11btMultiBody15localPosToWorldEiRK9btVector3(%class.btVector3* sret align 4 %pivotBworld, %class.btMultiBody* %22, i32 %24, %class.btVector3* nonnull align 4 dereferenceable(16) %m_pivotInB22)
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %tr, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotBworld)
  %25 = load %class.btIDebugDraw*, %class.btIDebugDraw** %drawer.addr, align 4
  %26 = bitcast %class.btIDebugDraw* %25 to void (%class.btIDebugDraw*, %class.btTransform*, float)***
  %vtable23 = load void (%class.btIDebugDraw*, %class.btTransform*, float)**, void (%class.btIDebugDraw*, %class.btTransform*, float)*** %26, align 4
  %vfn24 = getelementptr inbounds void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vtable23, i64 16
  %27 = load void (%class.btIDebugDraw*, %class.btTransform*, float)*, void (%class.btIDebugDraw*, %class.btTransform*, float)** %vfn24, align 4
  call void %27(%class.btIDebugDraw* %25, %class.btTransform* nonnull align 4 dereferenceable(64) %tr, float 0x3FB99999A0000000)
  br label %if.end25

if.end25:                                         ; preds = %if.then20, %if.end18
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26btMultiBodyFixedConstraint11setFrameInBERK11btMatrix3x3(%class.btMultiBodyFixedConstraint* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %frameInB) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyFixedConstraint*, align 4
  %frameInB.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMultiBodyFixedConstraint* %this, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  store %class.btMatrix3x3* %frameInB, %class.btMatrix3x3** %frameInB.addr, align 4
  %this1 = load %class.btMultiBodyFixedConstraint*, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %frameInB.addr, align 4
  %m_frameInB = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 6
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_frameInB, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26btMultiBodyFixedConstraint11setPivotInBERK9btVector3(%class.btMultiBodyFixedConstraint* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %pivotInB) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btMultiBodyFixedConstraint*, align 4
  %pivotInB.addr = alloca %class.btVector3*, align 4
  store %class.btMultiBodyFixedConstraint* %this, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  store %class.btVector3* %pivotInB, %class.btVector3** %pivotInB.addr, align 4
  %this1 = load %class.btMultiBodyFixedConstraint*, %class.btMultiBodyFixedConstraint** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %pivotInB.addr, align 4
  %m_pivotInB = getelementptr inbounds %class.btMultiBodyFixedConstraint, %class.btMultiBodyFixedConstraint* %this1, i32 0, i32 4
  %1 = bitcast %class.btVector3* %m_pivotInB to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI15btMultibodyLinkE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(592) %struct.btMultibodyLink* @_ZN20btAlignedObjectArrayI15btMultibodyLinkEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btMultibodyLink*, %struct.btMultibodyLink** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btMultibodyLink, %struct.btMultibodyLink* %0, i32 %1
  ret %struct.btMultibodyLink* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setRotationERK12btQuaternion(%class.btMatrix3x3* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %xs = alloca float, align 4
  %ys = alloca float, align 4
  %zs = alloca float, align 4
  %wx = alloca float, align 4
  %wy = alloca float, align 4
  %wz = alloca float, align 4
  %xx = alloca float, align 4
  %xy = alloca float, align 4
  %xz = alloca float, align 4
  %yy = alloca float, align 4
  %yz = alloca float, align 4
  %zz = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp34 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %call = call float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %0)
  store float %call, float* %d, align 4
  %1 = load float, float* %d, align 4
  %div = fdiv float 2.000000e+00, %1
  store float %div, float* %s, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call2, align 4
  %5 = load float, float* %s, align 4
  %mul = fmul float %4, %5
  store float %mul, float* %xs, align 4
  %6 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %7 = bitcast %class.btQuaternion* %6 to %class.btQuadWord*
  %call3 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %7)
  %8 = load float, float* %call3, align 4
  %9 = load float, float* %s, align 4
  %mul4 = fmul float %8, %9
  store float %mul4, float* %ys, align 4
  %10 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %11 = bitcast %class.btQuaternion* %10 to %class.btQuadWord*
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %11)
  %12 = load float, float* %call5, align 4
  %13 = load float, float* %s, align 4
  %mul6 = fmul float %12, %13
  store float %mul6, float* %zs, align 4
  %14 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %15 = bitcast %class.btQuaternion* %14 to %class.btQuadWord*
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %15)
  %16 = load float, float* %call7, align 4
  %17 = load float, float* %xs, align 4
  %mul8 = fmul float %16, %17
  store float %mul8, float* %wx, align 4
  %18 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %19 = bitcast %class.btQuaternion* %18 to %class.btQuadWord*
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %19)
  %20 = load float, float* %call9, align 4
  %21 = load float, float* %ys, align 4
  %mul10 = fmul float %20, %21
  store float %mul10, float* %wy, align 4
  %22 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %23 = bitcast %class.btQuaternion* %22 to %class.btQuadWord*
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %23)
  %24 = load float, float* %call11, align 4
  %25 = load float, float* %zs, align 4
  %mul12 = fmul float %24, %25
  store float %mul12, float* %wz, align 4
  %26 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %27 = bitcast %class.btQuaternion* %26 to %class.btQuadWord*
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %27)
  %28 = load float, float* %call13, align 4
  %29 = load float, float* %xs, align 4
  %mul14 = fmul float %28, %29
  store float %mul14, float* %xx, align 4
  %30 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %31 = bitcast %class.btQuaternion* %30 to %class.btQuadWord*
  %call15 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %31)
  %32 = load float, float* %call15, align 4
  %33 = load float, float* %ys, align 4
  %mul16 = fmul float %32, %33
  store float %mul16, float* %xy, align 4
  %34 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %35 = bitcast %class.btQuaternion* %34 to %class.btQuadWord*
  %call17 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %35)
  %36 = load float, float* %call17, align 4
  %37 = load float, float* %zs, align 4
  %mul18 = fmul float %36, %37
  store float %mul18, float* %xz, align 4
  %38 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %39 = bitcast %class.btQuaternion* %38 to %class.btQuadWord*
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %39)
  %40 = load float, float* %call19, align 4
  %41 = load float, float* %ys, align 4
  %mul20 = fmul float %40, %41
  store float %mul20, float* %yy, align 4
  %42 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %43 = bitcast %class.btQuaternion* %42 to %class.btQuadWord*
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %43)
  %44 = load float, float* %call21, align 4
  %45 = load float, float* %zs, align 4
  %mul22 = fmul float %44, %45
  store float %mul22, float* %yz, align 4
  %46 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %47 = bitcast %class.btQuaternion* %46 to %class.btQuadWord*
  %call23 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %47)
  %48 = load float, float* %call23, align 4
  %49 = load float, float* %zs, align 4
  %mul24 = fmul float %48, %49
  store float %mul24, float* %zz, align 4
  %50 = load float, float* %yy, align 4
  %51 = load float, float* %zz, align 4
  %add = fadd float %50, %51
  %sub = fsub float 1.000000e+00, %add
  store float %sub, float* %ref.tmp, align 4
  %52 = load float, float* %xy, align 4
  %53 = load float, float* %wz, align 4
  %sub26 = fsub float %52, %53
  store float %sub26, float* %ref.tmp25, align 4
  %54 = load float, float* %xz, align 4
  %55 = load float, float* %wy, align 4
  %add28 = fadd float %54, %55
  store float %add28, float* %ref.tmp27, align 4
  %56 = load float, float* %xy, align 4
  %57 = load float, float* %wz, align 4
  %add30 = fadd float %56, %57
  store float %add30, float* %ref.tmp29, align 4
  %58 = load float, float* %xx, align 4
  %59 = load float, float* %zz, align 4
  %add32 = fadd float %58, %59
  %sub33 = fsub float 1.000000e+00, %add32
  store float %sub33, float* %ref.tmp31, align 4
  %60 = load float, float* %yz, align 4
  %61 = load float, float* %wx, align 4
  %sub35 = fsub float %60, %61
  store float %sub35, float* %ref.tmp34, align 4
  %62 = load float, float* %xz, align 4
  %63 = load float, float* %wy, align 4
  %sub37 = fsub float %62, %63
  store float %sub37, float* %ref.tmp36, align 4
  %64 = load float, float* %yz, align 4
  %65 = load float, float* %wx, align 4
  %add39 = fadd float %64, %65
  store float %add39, float* %ref.tmp38, align 4
  %66 = load float, float* %xx, align 4
  %67 = load float, float* %yy, align 4
  %add41 = fadd float %66, %67
  %sub42 = fsub float 1.000000e+00, %add41
  store float %sub42, float* %ref.tmp40, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp25, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp29, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp34, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp38, float* nonnull align 4 dereferenceable(4) %ref.tmp40)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion7length2Ev(%class.btQuaternion* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %call = call float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this1, %class.btQuaternion* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1wEv(%class.btQuadWord* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuadWord*, align 4
  store %class.btQuadWord* %this, %class.btQuadWord** %this.addr, align 4
  %this1 = load %class.btQuadWord*, %class.btQuadWord** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 3
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK12btQuaternion3dotERKS_(%class.btQuaternion* %this, %class.btQuaternion* nonnull align 4 dereferenceable(16) %q) #2 comdat {
entry:
  %this.addr = alloca %class.btQuaternion*, align 4
  %q.addr = alloca %class.btQuaternion*, align 4
  store %class.btQuaternion* %this, %class.btQuaternion** %this.addr, align 4
  store %class.btQuaternion* %q, %class.btQuaternion** %q.addr, align 4
  %this1 = load %class.btQuaternion*, %class.btQuaternion** %this.addr, align 4
  %0 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %3 = bitcast %class.btQuaternion* %2 to %class.btQuadWord*
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1xEv(%class.btQuadWord* %3)
  %4 = load float, float* %call, align 4
  %mul = fmul float %1, %4
  %5 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats2 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %5, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %6 = load float, float* %arrayidx3, align 4
  %7 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %8 = bitcast %class.btQuaternion* %7 to %class.btQuadWord*
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1yEv(%class.btQuadWord* %8)
  %9 = load float, float* %call4, align 4
  %mul5 = fmul float %6, %9
  %add = fadd float %mul, %mul5
  %10 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats6 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %10, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %11 = load float, float* %arrayidx7, align 4
  %12 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %13 = bitcast %class.btQuaternion* %12 to %class.btQuadWord*
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK10btQuadWord1zEv(%class.btQuadWord* %13)
  %14 = load float, float* %call8, align 4
  %mul9 = fmul float %11, %14
  %add10 = fadd float %add, %mul9
  %15 = bitcast %class.btQuaternion* %this1 to %class.btQuadWord*
  %m_floats11 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %15, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 3
  %16 = load float, float* %arrayidx12, align 4
  %17 = load %class.btQuaternion*, %class.btQuaternion** %q.addr, align 4
  %18 = bitcast %class.btQuaternion* %17 to %class.btQuadWord*
  %m_floats13 = getelementptr inbounds %class.btQuadWord, %class.btQuadWord* %18, i32 0, i32 0
  %arrayidx14 = getelementptr inbounds [4 x float], [4 x float]* %m_floats13, i32 0, i32 3
  %19 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %16, %19
  %add16 = fadd float %add10, %mul15
  ret float %add16
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35cofacEiiii(%class.btMatrix3x3* %this, i32 %r1, i32 %c1, i32 %r2, i32 %c2) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %r1.addr = alloca i32, align 4
  %c1.addr = alloca i32, align 4
  %r2.addr = alloca i32, align 4
  %c2.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %r1, i32* %r1.addr, align 4
  store i32 %c1, i32* %c1.addr, align 4
  store i32 %r2, i32* %r2.addr, align 4
  store i32 %c2, i32* %c2.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %r1.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx)
  %1 = load i32, i32* %c1.addr, align 4
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx2, align 4
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %3 = load i32, i32* %r2.addr, align 4
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 %3
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx4)
  %4 = load i32, i32* %c2.addr, align 4
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 %4
  %5 = load float, float* %arrayidx6, align 4
  %mul = fmul float %2, %5
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %6 = load i32, i32* %r1.addr, align 4
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 %6
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx8)
  %7 = load i32, i32* %c2.addr, align 4
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 %7
  %8 = load float, float* %arrayidx10, align 4
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %9 = load i32, i32* %r2.addr, align 4
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 %9
  %call13 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %arrayidx12)
  %10 = load i32, i32* %c1.addr, align 4
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 %10
  %11 = load float, float* %arrayidx14, align 4
  %mul15 = fmul float %8, %11
  %sub = fsub float %mul, %mul15
  ret float %sub
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %1)
  %2 = bitcast i8* %call2 to float*
  store float* %2, float** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %3 = load float*, float** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, float* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load float*, float** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store float* %4, float** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.9* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, float* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store float* %dest, float** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  %5 = bitcast float* %arrayidx to i8*
  %6 = bitcast i8* %5 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %7 = load float*, float** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx2, align 4
  store float %9, float* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %3 = load float*, float** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.9* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.9* %this, i32 %n, float** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store float** %hint, float*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.9* %this, float* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store float* %ptr, float** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.25* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.25*, align 4
  store %class.btAlignedObjectArray.25* %this, %class.btAlignedObjectArray.25** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.25*, %class.btAlignedObjectArray.25** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.25* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.25*, align 4
  store %class.btAlignedObjectArray.25* %this, %class.btAlignedObjectArray.25** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.25*, %class.btAlignedObjectArray.25** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7reserveEi(%class.btAlignedObjectArray.25* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.25*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %class.btAlignedObjectArray.25* %this, %class.btAlignedObjectArray.25** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.25*, %class.btAlignedObjectArray.25** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE8capacityEv(%class.btAlignedObjectArray.25* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi(%class.btAlignedObjectArray.25* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btMultiBodySolverConstraint*
  store %struct.btMultiBodySolverConstraint* %2, %struct.btMultiBodySolverConstraint** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.25* %this1)
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.25* %this1, i32 0, i32 %call3, %struct.btMultiBodySolverConstraint* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4sizeEv(%class.btAlignedObjectArray.25* %this1)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.25* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.25* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* %4, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE9allocSizeEi(%class.btAlignedObjectArray.25* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.25*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.25* %this, %class.btAlignedObjectArray.25** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.25*, %class.btAlignedObjectArray.25** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE8allocateEi(%class.btAlignedObjectArray.25* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.25*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.25* %this, %class.btAlignedObjectArray.25** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.25*, %class.btAlignedObjectArray.25** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btMultiBodySolverConstraint* @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.26* %m_allocator, i32 %1, %struct.btMultiBodySolverConstraint** null)
  %2 = bitcast %struct.btMultiBodySolverConstraint* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI27btMultiBodySolverConstraintE4copyEiiPS0_(%class.btAlignedObjectArray.25* %this, i32 %start, i32 %end, %struct.btMultiBodySolverConstraint* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.25*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.25* %this, %class.btAlignedObjectArray.25** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btMultiBodySolverConstraint* %dest, %struct.btMultiBodySolverConstraint** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.25*, %class.btAlignedObjectArray.25** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 %4
  %5 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx to i8*
  %call = call i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 192, i8* %5)
  %6 = bitcast i8* %call to %struct.btMultiBodySolverConstraint*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 4
  %7 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %7, i32 %8
  %9 = bitcast %struct.btMultiBodySolverConstraint* %6 to i8*
  %10 = bitcast %struct.btMultiBodySolverConstraint* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 192, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE7destroyEii(%class.btAlignedObjectArray.25* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.25*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.25* %this, %class.btAlignedObjectArray.25** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.25*, %class.btAlignedObjectArray.25** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 4
  %3 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btMultiBodySolverConstraint, %struct.btMultiBodySolverConstraint* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI27btMultiBodySolverConstraintE10deallocateEv(%class.btAlignedObjectArray.25* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.25*, align 4
  store %class.btAlignedObjectArray.25* %this, %class.btAlignedObjectArray.25** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.25*, %class.btAlignedObjectArray.25** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data, align 4
  %tobool = icmp ne %struct.btMultiBodySolverConstraint* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 4
  %2 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.26* %m_allocator, %struct.btMultiBodySolverConstraint* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.25* %this1, i32 0, i32 4
  store %struct.btMultiBodySolverConstraint* null, %struct.btMultiBodySolverConstraint** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btMultiBodySolverConstraint* @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.26* %this, i32 %n, %struct.btMultiBodySolverConstraint** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.26*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btMultiBodySolverConstraint**, align 4
  store %class.btAlignedAllocator.26* %this, %class.btAlignedAllocator.26** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btMultiBodySolverConstraint** %hint, %struct.btMultiBodySolverConstraint*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.26*, %class.btAlignedAllocator.26** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 192, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btMultiBodySolverConstraint*
  ret %struct.btMultiBodySolverConstraint* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN27btMultiBodySolverConstraintnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI27btMultiBodySolverConstraintLj16EE10deallocateEPS0_(%class.btAlignedAllocator.26* %this, %struct.btMultiBodySolverConstraint* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.26*, align 4
  %ptr.addr = alloca %struct.btMultiBodySolverConstraint*, align 4
  store %class.btAlignedAllocator.26* %this, %class.btAlignedAllocator.26** %this.addr, align 4
  store %struct.btMultiBodySolverConstraint* %ptr, %struct.btMultiBodySolverConstraint** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.26*, %class.btAlignedAllocator.26** %this.addr, align 4
  %0 = load %struct.btMultiBodySolverConstraint*, %struct.btMultiBodySolverConstraint** %ptr.addr, align 4
  %1 = bitcast %struct.btMultiBodySolverConstraint* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btMultiBodyFixedConstraint.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
