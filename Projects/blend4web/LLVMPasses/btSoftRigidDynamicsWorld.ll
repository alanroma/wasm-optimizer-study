; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletSoftBody/btSoftRigidDynamicsWorld.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletSoftBody/btSoftRigidDynamicsWorld.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btSoftRigidDynamicsWorld = type <{ %class.btDiscreteDynamicsWorld, %class.btAlignedObjectArray.22, i32, i8, i8, i8, i8, %struct.btSoftBodyWorldInfo, %class.btSoftBodySolver*, i8, [3 x i8] }>
%class.btDiscreteDynamicsWorld = type { %class.btDynamicsWorld, %class.btAlignedObjectArray.4, %struct.InplaceSolverIslandCallback*, %class.btConstraintSolver*, %class.btSimulationIslandManager*, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btVector3, float, float, i8, i8, i8, i8, %class.btAlignedObjectArray.12, i32, i8, [3 x i8], %class.btAlignedObjectArray.16, %class.btSpinMutex }
%class.btDynamicsWorld = type { %class.btCollisionWorld.base, void (%class.btDynamicsWorld*, float)*, void (%class.btDynamicsWorld*, float)*, i8*, %struct.btContactSolverInfo }
%class.btCollisionWorld.base = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8 }>
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.0, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type { i32 (...)** }
%struct.btContactSolverInfo = type { %struct.btContactSolverInfoData }
%struct.btContactSolverInfoData = type { float, float, float, float, float, i32, float, float, float, float, float, i32, float, float, float, float, i32, i32, i32, float, float, float }
%struct.InplaceSolverIslandCallback = type opaque
%class.btSimulationIslandManager = type opaque
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btTypedConstraint = type opaque
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btRigidBody**, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.4, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %class.btActionInterface**, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btActionInterface = type opaque
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.19, %union.anon.20, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.19 = type { float }
%union.anon.20 = type { float }
%class.btSpinMutex = type { i32 }
%class.btAlignedObjectArray.22 = type <{ %class.btAlignedAllocator.23, [3 x i8], i32, i32, %class.btSoftBody**, i8, [3 x i8] }>
%class.btAlignedAllocator.23 = type { i8 }
%class.btSoftBody = type { %class.btCollisionObject, %class.btAlignedObjectArray.0, %class.btSoftBodySolver*, %"struct.btSoftBody::Config", %"struct.btSoftBody::SolverState", %"struct.btSoftBody::Pose", i8*, %struct.btSoftBodyWorldInfo*, %class.btAlignedObjectArray.45, %class.btAlignedObjectArray.50, %class.btAlignedObjectArray.54, %class.btAlignedObjectArray.58, %class.btAlignedObjectArray.62, %class.btAlignedObjectArray.66, %class.btAlignedObjectArray.70, %class.btAlignedObjectArray.74, %class.btAlignedObjectArray.78, %class.btAlignedObjectArray.86, float, [2 x %class.btVector3], i8, %struct.btDbvt, %struct.btDbvt, %struct.btDbvt, %class.btAlignedObjectArray.94, %class.btAlignedObjectArray.98, %class.btTransform, %class.btVector3, float, %class.btAlignedObjectArray.102 }
%"struct.btSoftBody::Config" = type { i32, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, float, i32, i32, i32, i32, i32, %class.btAlignedObjectArray.25, %class.btAlignedObjectArray.29, %class.btAlignedObjectArray.29 }
%class.btAlignedObjectArray.25 = type <{ %class.btAlignedAllocator.26, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.26 = type { i8 }
%class.btAlignedObjectArray.29 = type <{ %class.btAlignedAllocator.30, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.30 = type { i8 }
%"struct.btSoftBody::SolverState" = type { float, float, float, float, float }
%"struct.btSoftBody::Pose" = type { i8, i8, float, %class.btAlignedObjectArray.33, %class.btAlignedObjectArray.37, %class.btVector3, %class.btMatrix3x3, %class.btMatrix3x3, %class.btMatrix3x3 }
%class.btAlignedObjectArray.33 = type <{ %class.btAlignedAllocator.34, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.34 = type { i8 }
%class.btAlignedObjectArray.37 = type <{ %class.btAlignedAllocator.38, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.38 = type { i8 }
%class.btAlignedObjectArray.45 = type <{ %class.btAlignedAllocator.46, [3 x i8], i32, i32, %"struct.btSoftBody::Note"*, i8, [3 x i8] }>
%class.btAlignedAllocator.46 = type { i8 }
%"struct.btSoftBody::Note" = type { %"struct.btSoftBody::Element", i8*, %class.btVector3, i32, [4 x %"struct.btSoftBody::Node"*], [4 x float] }
%"struct.btSoftBody::Element" = type { i8* }
%"struct.btSoftBody::Node" = type <{ %"struct.btSoftBody::Feature", %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, %struct.btDbvtNode*, i8, [3 x i8] }>
%"struct.btSoftBody::Feature" = type { %"struct.btSoftBody::Element", %"struct.btSoftBody::Material"* }
%"struct.btSoftBody::Material" = type { %"struct.btSoftBody::Element", float, float, float, i32 }
%struct.btDbvtNode = type { %struct.btDbvtAabbMm, %struct.btDbvtNode*, %union.anon.48 }
%struct.btDbvtAabbMm = type { %class.btVector3, %class.btVector3 }
%union.anon.48 = type { [2 x %struct.btDbvtNode*] }
%class.btAlignedObjectArray.50 = type <{ %class.btAlignedAllocator.51, [3 x i8], i32, i32, %"struct.btSoftBody::Node"*, i8, [3 x i8] }>
%class.btAlignedAllocator.51 = type { i8 }
%class.btAlignedObjectArray.54 = type <{ %class.btAlignedAllocator.55, [3 x i8], i32, i32, %"struct.btSoftBody::Link"*, i8, [3 x i8] }>
%class.btAlignedAllocator.55 = type { i8 }
%"struct.btSoftBody::Link" = type { %"struct.btSoftBody::Feature", %class.btVector3, [2 x %"struct.btSoftBody::Node"*], float, i8, float, float, float }
%class.btAlignedObjectArray.58 = type <{ %class.btAlignedAllocator.59, [3 x i8], i32, i32, %"struct.btSoftBody::Face"*, i8, [3 x i8] }>
%class.btAlignedAllocator.59 = type { i8 }
%"struct.btSoftBody::Face" = type { %"struct.btSoftBody::Feature", [3 x %"struct.btSoftBody::Node"*], %class.btVector3, float, %struct.btDbvtNode* }
%class.btAlignedObjectArray.62 = type <{ %class.btAlignedAllocator.63, [3 x i8], i32, i32, %"struct.btSoftBody::Tetra"*, i8, [3 x i8] }>
%class.btAlignedAllocator.63 = type { i8 }
%"struct.btSoftBody::Tetra" = type { %"struct.btSoftBody::Feature", [4 x %"struct.btSoftBody::Node"*], float, %struct.btDbvtNode*, [4 x %class.btVector3], float, float }
%class.btAlignedObjectArray.66 = type <{ %class.btAlignedAllocator.67, [3 x i8], i32, i32, %"struct.btSoftBody::Anchor"*, i8, [3 x i8] }>
%class.btAlignedAllocator.67 = type { i8 }
%"struct.btSoftBody::Anchor" = type { %"struct.btSoftBody::Node"*, %class.btVector3, %class.btRigidBody*, float, %class.btMatrix3x3, %class.btVector3, float }
%class.btAlignedObjectArray.70 = type <{ %class.btAlignedAllocator.71, [3 x i8], i32, i32, %"struct.btSoftBody::RContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.71 = type { i8 }
%"struct.btSoftBody::RContact" = type { %"struct.btSoftBody::sCti", %"struct.btSoftBody::Node"*, %class.btMatrix3x3, %class.btVector3, float, float, float }
%"struct.btSoftBody::sCti" = type { %class.btCollisionObject*, %class.btVector3, float }
%class.btAlignedObjectArray.74 = type <{ %class.btAlignedAllocator.75, [3 x i8], i32, i32, %"struct.btSoftBody::SContact"*, i8, [3 x i8] }>
%class.btAlignedAllocator.75 = type { i8 }
%"struct.btSoftBody::SContact" = type { %"struct.btSoftBody::Node"*, %"struct.btSoftBody::Face"*, %class.btVector3, %class.btVector3, float, float, [2 x float] }
%class.btAlignedObjectArray.78 = type <{ %class.btAlignedAllocator.79, [3 x i8], i32, i32, %"struct.btSoftBody::Joint"**, i8, [3 x i8] }>
%class.btAlignedAllocator.79 = type { i8 }
%"struct.btSoftBody::Joint" = type <{ i32 (...)**, [2 x %"struct.btSoftBody::Body"], [2 x %class.btVector3], float, float, float, %class.btVector3, %class.btVector3, %class.btMatrix3x3, i8, [3 x i8] }>
%"struct.btSoftBody::Body" = type { %"struct.btSoftBody::Cluster"*, %class.btRigidBody*, %class.btCollisionObject* }
%"struct.btSoftBody::Cluster" = type { %class.btAlignedObjectArray.37, %class.btAlignedObjectArray.81, %class.btAlignedObjectArray.33, %class.btTransform, float, float, %class.btMatrix3x3, %class.btMatrix3x3, %class.btVector3, [2 x %class.btVector3], [2 x %class.btVector3], i32, i32, %class.btVector3, %class.btVector3, %struct.btDbvtNode*, float, float, float, float, float, float, i8, i8, i32 }
%class.btAlignedObjectArray.81 = type <{ %class.btAlignedAllocator.82, [3 x i8], i32, i32, %"struct.btSoftBody::Node"**, i8, [3 x i8] }>
%class.btAlignedAllocator.82 = type { i8 }
%class.btAlignedObjectArray.86 = type <{ %class.btAlignedAllocator.87, [3 x i8], i32, i32, %"struct.btSoftBody::Material"**, i8, [3 x i8] }>
%class.btAlignedAllocator.87 = type { i8 }
%struct.btDbvt = type { %struct.btDbvtNode*, %struct.btDbvtNode*, i32, i32, i32, %class.btAlignedObjectArray.90 }
%class.btAlignedObjectArray.90 = type <{ %class.btAlignedAllocator.91, [3 x i8], i32, i32, %"struct.btDbvt::sStkNN"*, i8, [3 x i8] }>
%class.btAlignedAllocator.91 = type { i8 }
%"struct.btDbvt::sStkNN" = type { %struct.btDbvtNode*, %struct.btDbvtNode* }
%class.btAlignedObjectArray.94 = type <{ %class.btAlignedAllocator.95, [3 x i8], i32, i32, %"struct.btSoftBody::Cluster"**, i8, [3 x i8] }>
%class.btAlignedAllocator.95 = type { i8 }
%class.btAlignedObjectArray.98 = type <{ %class.btAlignedAllocator.99, [3 x i8], i32, i32, i8*, i8, [3 x i8] }>
%class.btAlignedAllocator.99 = type { i8 }
%class.btAlignedObjectArray.102 = type <{ %class.btAlignedAllocator.103, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.103 = type { i8 }
%struct.btSoftBodyWorldInfo = type { float, float, float, float, %class.btVector3, %class.btBroadphaseInterface*, %class.btDispatcher*, %class.btVector3, %struct.btSparseSdf }
%struct.btSparseSdf = type { %class.btAlignedObjectArray.41, float, i32, i32, i32, i32, i32 }
%class.btAlignedObjectArray.41 = type <{ %class.btAlignedAllocator.42, [3 x i8], i32, i32, %"struct.btSparseSdf<3>::Cell"**, i8, [3 x i8] }>
%class.btAlignedAllocator.42 = type { i8 }
%"struct.btSparseSdf<3>::Cell" = type { [4 x [4 x [4 x float]]], [3 x i32], i32, i32, %class.btCollisionShape*, %"struct.btSparseSdf<3>::Cell"* }
%class.btDispatcher = type { i32 (...)** }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btConstraintSolver = type opaque
%class.btCollisionConfiguration = type opaque
%class.btSoftBodySolver = type { i32 (...)**, i32, i32, float }
%class.btDefaultSoftBodySolver = type { %class.btSoftBodySolver, i8, [3 x i8], %class.btAlignedObjectArray.22 }
%class.CProfileSample = type { i8 }
%class.btCollisionWorld = type <{ i32 (...)**, %class.btAlignedObjectArray, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8, [3 x i8] }>
%"struct.btCollisionWorld::RayResultCallback" = type { i32 (...)**, float, %class.btCollisionObject*, i32, i32, i32 }
%struct.btSoftSingleRayCallback = type { %struct.btBroadphaseRayCallback, %class.btVector3, %class.btVector3, %class.btTransform, %class.btTransform, %class.btVector3, %class.btSoftRigidDynamicsWorld*, %"struct.btCollisionWorld::RayResultCallback"* }
%struct.btBroadphaseRayCallback = type { %struct.btBroadphaseAabbCallback, %class.btVector3, [3 x i32], float }
%struct.btBroadphaseAabbCallback = type { i32 (...)** }
%"struct.btSoftBody::sRayCast" = type { %class.btSoftBody*, i32, i32, float }
%"struct.btCollisionWorld::LocalShapeInfo" = type { i32, i32 }
%"struct.btCollisionWorld::LocalRayResult" = type { %class.btCollisionObject*, %"struct.btCollisionWorld::LocalShapeInfo"*, %class.btVector3, float }
%class.btSerializer = type { i32 (...)** }
%class.btChunk = type { i32, i32, i8*, i32, i32 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyEC2Ev = comdat any

$_ZN19btSoftBodyWorldInfoC2Ev = comdat any

$_ZN11btSparseSdfILi3EE10InitializeEii = comdat any

$_ZN11btSparseSdfILi3EE5ResetEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN19btSoftBodyWorldInfoD2Ev = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyED2Ev = comdat any

$_ZN23btDiscreteDynamicsWorlddlEPv = comdat any

$_ZN24btSoftRigidDynamicsWorld16getSoftBodyArrayEv = comdat any

$_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi = comdat any

$_ZN16btSoftBodySolver12getTimeScaleEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE9push_backERKS1_ = comdat any

$_ZN10btSoftBody17setSoftBodySolverEP16btSoftBodySolver = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE6removeERKS1_ = comdat any

$_ZN10btSoftBody6upcastEP17btCollisionObject = comdat any

$_ZN23btSoftSingleRayCallbackC2ERK9btVector3S2_PK24btSoftRigidDynamicsWorldRN16btCollisionWorld17RayResultCallbackE = comdat any

$_ZN23btSoftSingleRayCallbackD2Ev = comdat any

$_ZNK16btCollisionShape10isSoftBodyEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZN20btAlignedObjectArrayIN10btSoftBody4FaceEEixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN16btCollisionWorld14LocalRayResultC2EPK17btCollisionObjectPNS_14LocalShapeInfoERK9btVector3f = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi = comdat any

$_ZNK17btCollisionObject15getInternalTypeEv = comdat any

$_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw = comdat any

$_ZN16btCollisionWorld14getDebugDrawerEv = comdat any

$_ZNK24btSoftRigidDynamicsWorld12getWorldTypeEv = comdat any

$_ZN23btDiscreteDynamicsWorld11setNumTasksEi = comdat any

$_ZN23btDiscreteDynamicsWorld14updateVehiclesEf = comdat any

$_ZN11btSparseSdfILi3EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEC2Ev = comdat any

$_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4initEv = comdat any

$_ZN11btSparseSdfILi3EED2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEED2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EE10deallocateEPS3_ = comdat any

$_ZN23btBroadphaseRayCallbackC2Ev = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btTransform11setIdentityEv = comdat any

$_ZN11btTransform9setOriginERK9btVector3 = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN23btSoftSingleRayCallbackD0Ev = comdat any

$_ZN23btSoftSingleRayCallback7processEPK17btBroadphaseProxy = comdat any

$_ZN24btBroadphaseAabbCallbackC2Ev = comdat any

$_ZN23btBroadphaseRayCallbackD2Ev = comdat any

$_ZN23btBroadphaseRayCallbackD0Ev = comdat any

$_ZN24btBroadphaseAabbCallbackD2Ev = comdat any

$_ZN24btBroadphaseAabbCallbackD0Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x311setIdentityEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN17btCollisionObject19getBroadphaseHandleEv = comdat any

$_ZN17btCollisionObject17getCollisionShapeEv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZN17btBroadphaseProxy10isSoftBodyEi = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN18btAlignedAllocatorIP10btSoftBodyLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE6resizeEiRKS3_ = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4copyEiiPS3_ = comdat any

$_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EE8allocateEiPPKS3_ = comdat any

$_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEixEi = comdat any

$_ZNK20btAlignedObjectArrayIP10btSoftBodyE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE8allocateEiPPKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP10btSoftBodyE16findLinearSearchERKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE13removeAtIndexEi = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIP10btSoftBodyE8pop_backEv = comdat any

$_ZTV23btSoftSingleRayCallback = comdat any

$_ZTS23btSoftSingleRayCallback = comdat any

$_ZTS23btBroadphaseRayCallback = comdat any

$_ZTS24btBroadphaseAabbCallback = comdat any

$_ZTI24btBroadphaseAabbCallback = comdat any

$_ZTI23btBroadphaseRayCallback = comdat any

$_ZTI23btSoftSingleRayCallback = comdat any

$_ZTV23btBroadphaseRayCallback = comdat any

$_ZTV24btBroadphaseAabbCallback = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV24btSoftRigidDynamicsWorld = hidden unnamed_addr constant { [49 x i8*] } { [49 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI24btSoftRigidDynamicsWorld to i8*), i8* bitcast (%class.btSoftRigidDynamicsWorld* (%class.btSoftRigidDynamicsWorld*)* @_ZN24btSoftRigidDynamicsWorldD1Ev to i8*), i8* bitcast (void (%class.btSoftRigidDynamicsWorld*)* @_ZN24btSoftRigidDynamicsWorldD0Ev to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld11updateAabbsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld23computeOverlappingPairsEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btIDebugDraw*)* @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw to i8*), i8* bitcast (%class.btIDebugDraw* (%class.btCollisionWorld*)* @_ZN16btCollisionWorld14getDebugDrawerEv to i8*), i8* bitcast (void (%class.btSoftRigidDynamicsWorld*)* @_ZN24btSoftRigidDynamicsWorld14debugDrawWorldEv to i8*), i8* bitcast (void (%class.btCollisionWorld*, %class.btTransform*, %class.btCollisionShape*, %class.btVector3*)* @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3 to i8*), i8* bitcast (void (%class.btSoftRigidDynamicsWorld*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)* @_ZNK24btSoftRigidDynamicsWorld7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32)* @_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectii to i8*), i8* bitcast (void (%class.btSoftRigidDynamicsWorld*, %class.btCollisionObject*)* @_ZN24btSoftRigidDynamicsWorld21removeCollisionObjectEP17btCollisionObject to i8*), i8* bitcast (void (%class.btCollisionWorld*)* @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv to i8*), i8* bitcast (void (%class.btSoftRigidDynamicsWorld*, %class.btSerializer*)* @_ZN24btSoftRigidDynamicsWorld9serializeEP12btSerializer to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*, float, i32, float)* @_ZN23btDiscreteDynamicsWorld14stepSimulationEfif to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*, i1)* @_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)* @_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btVector3*)* @_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3 to i8*), i8* bitcast (void (%class.btVector3*, %class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld10getGravityEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)* @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*, i32, i32)* @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyii to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btRigidBody*)* @_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btConstraintSolver*)* @_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver to i8*), i8* bitcast (%class.btConstraintSolver* (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv to i8*), i8* bitcast (i32 (%class.btDiscreteDynamicsWorld*)* @_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)* @_ZN23btDiscreteDynamicsWorld13getConstraintEi to i8*), i8* bitcast (%class.btTypedConstraint* (%class.btDiscreteDynamicsWorld*, i32)* @_ZNK23btDiscreteDynamicsWorld13getConstraintEi to i8*), i8* bitcast (i32 (%class.btSoftRigidDynamicsWorld*)* @_ZNK24btSoftRigidDynamicsWorld12getWorldTypeEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld11clearForcesEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btActionInterface*)* @_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface to i8*), i8* bitcast (void (%class.btSoftRigidDynamicsWorld*, float)* @_ZN24btSoftRigidDynamicsWorld25predictUnconstraintMotionEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld19integrateTransformsEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %struct.btContactSolverInfo*)* @_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld21updateActivationStateEf to i8*), i8* bitcast (void (%class.btSoftRigidDynamicsWorld*, float)* @_ZN24btSoftRigidDynamicsWorld28internalSingleStepSimulationEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld24createPredictiveContactsEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*)* @_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*)* @_ZN23btDiscreteDynamicsWorld12applyGravityEv to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, i32)* @_ZN23btDiscreteDynamicsWorld11setNumTasksEi to i8*), i8* bitcast (void (%class.btDiscreteDynamicsWorld*, float)* @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf to i8*)] }, align 4
@.str = private unnamed_addr constant [34 x i8] c"predictUnconstraintMotionSoftBody\00", align 1
@.str.1 = private unnamed_addr constant [21 x i8] c"solveSoftConstraints\00", align 1
@.str.2 = private unnamed_addr constant [8 x i8] c"rayTest\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS24btSoftRigidDynamicsWorld = hidden constant [27 x i8] c"24btSoftRigidDynamicsWorld\00", align 1
@_ZTI23btDiscreteDynamicsWorld = external constant i8*
@_ZTI24btSoftRigidDynamicsWorld = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @_ZTS24btSoftRigidDynamicsWorld, i32 0, i32 0), i8* bitcast (i8** @_ZTI23btDiscreteDynamicsWorld to i8*) }, align 4
@_ZTV23btSoftSingleRayCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btSoftSingleRayCallback to i8*), i8* bitcast (%struct.btSoftSingleRayCallback* (%struct.btSoftSingleRayCallback*)* @_ZN23btSoftSingleRayCallbackD2Ev to i8*), i8* bitcast (void (%struct.btSoftSingleRayCallback*)* @_ZN23btSoftSingleRayCallbackD0Ev to i8*), i8* bitcast (i1 (%struct.btSoftSingleRayCallback*, %struct.btBroadphaseProxy*)* @_ZN23btSoftSingleRayCallback7processEPK17btBroadphaseProxy to i8*)] }, comdat, align 4
@_ZTS23btSoftSingleRayCallback = linkonce_odr hidden constant [26 x i8] c"23btSoftSingleRayCallback\00", comdat, align 1
@_ZTS23btBroadphaseRayCallback = linkonce_odr hidden constant [26 x i8] c"23btBroadphaseRayCallback\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS24btBroadphaseAabbCallback = linkonce_odr hidden constant [27 x i8] c"24btBroadphaseAabbCallback\00", comdat, align 1
@_ZTI24btBroadphaseAabbCallback = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([27 x i8], [27 x i8]* @_ZTS24btBroadphaseAabbCallback, i32 0, i32 0) }, comdat, align 4
@_ZTI23btBroadphaseRayCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btBroadphaseRayCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI24btBroadphaseAabbCallback to i8*) }, comdat, align 4
@_ZTI23btSoftSingleRayCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btSoftSingleRayCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btBroadphaseRayCallback to i8*) }, comdat, align 4
@_ZTV23btBroadphaseRayCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btBroadphaseRayCallback to i8*), i8* bitcast (%struct.btBroadphaseRayCallback* (%struct.btBroadphaseRayCallback*)* @_ZN23btBroadphaseRayCallbackD2Ev to i8*), i8* bitcast (void (%struct.btBroadphaseRayCallback*)* @_ZN23btBroadphaseRayCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTV24btBroadphaseAabbCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI24btBroadphaseAabbCallback to i8*), i8* bitcast (%struct.btBroadphaseAabbCallback* (%struct.btBroadphaseAabbCallback*)* @_ZN24btBroadphaseAabbCallbackD2Ev to i8*), i8* bitcast (void (%struct.btBroadphaseAabbCallback*)* @_ZN24btBroadphaseAabbCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btSoftRigidDynamicsWorld.cpp, i8* null }]

@_ZN24btSoftRigidDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfigurationP16btSoftBodySolver = hidden unnamed_addr alias %class.btSoftRigidDynamicsWorld* (%class.btSoftRigidDynamicsWorld*, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*, %class.btSoftBodySolver*), %class.btSoftRigidDynamicsWorld* (%class.btSoftRigidDynamicsWorld*, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*, %class.btSoftBodySolver*)* @_ZN24btSoftRigidDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfigurationP16btSoftBodySolver
@_ZN24btSoftRigidDynamicsWorldD1Ev = hidden unnamed_addr alias %class.btSoftRigidDynamicsWorld* (%class.btSoftRigidDynamicsWorld*), %class.btSoftRigidDynamicsWorld* (%class.btSoftRigidDynamicsWorld*)* @_ZN24btSoftRigidDynamicsWorldD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btSoftRigidDynamicsWorld* @_ZN24btSoftRigidDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfigurationP16btSoftBodySolver(%class.btSoftRigidDynamicsWorld* returned %this, %class.btDispatcher* %dispatcher, %class.btBroadphaseInterface* %pairCache, %class.btConstraintSolver* %constraintSolver, %class.btCollisionConfiguration* %collisionConfiguration, %class.btSoftBodySolver* %softBodySolver) unnamed_addr #2 {
entry:
  %retval = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %pairCache.addr = alloca %class.btBroadphaseInterface*, align 4
  %constraintSolver.addr = alloca %class.btConstraintSolver*, align 4
  %collisionConfiguration.addr = alloca %class.btCollisionConfiguration*, align 4
  %softBodySolver.addr = alloca %class.btSoftBodySolver*, align 4
  %ptr = alloca i8*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btBroadphaseInterface* %pairCache, %class.btBroadphaseInterface** %pairCache.addr, align 4
  store %class.btConstraintSolver* %constraintSolver, %class.btConstraintSolver** %constraintSolver.addr, align 4
  store %class.btCollisionConfiguration* %collisionConfiguration, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4
  store %class.btSoftBodySolver* %softBodySolver, %class.btSoftBodySolver** %softBodySolver.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store %class.btSoftRigidDynamicsWorld* %this1, %class.btSoftRigidDynamicsWorld** %retval, align 4
  %0 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %1 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %2 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %pairCache.addr, align 4
  %3 = load %class.btConstraintSolver*, %class.btConstraintSolver** %constraintSolver.addr, align 4
  %4 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4
  %call = call %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btDiscreteDynamicsWorld* %0, %class.btDispatcher* %1, %class.btBroadphaseInterface* %2, %class.btConstraintSolver* %3, %class.btCollisionConfiguration* %4)
  %5 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [49 x i8*] }, { [49 x i8*] }* @_ZTV24btSoftRigidDynamicsWorld, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %5, align 4
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIP10btSoftBodyEC2Ev(%class.btAlignedObjectArray.22* %m_softBodies)
  %m_sbi = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %call3 = call %struct.btSoftBodyWorldInfo* @_ZN19btSoftBodyWorldInfoC2Ev(%struct.btSoftBodyWorldInfo* %m_sbi)
  %m_softBodySolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %6 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %softBodySolver.addr, align 4
  store %class.btSoftBodySolver* %6, %class.btSoftBodySolver** %m_softBodySolver, align 4
  %m_ownsSolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 9
  store i8 0, i8* %m_ownsSolver, align 4
  %m_softBodySolver4 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %7 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver4, align 4
  %tobool = icmp ne %class.btSoftBodySolver* %7, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %call5 = call i8* @_Z22btAlignedAllocInternalmi(i32 40, i32 16)
  store i8* %call5, i8** %ptr, align 4
  %8 = load i8*, i8** %ptr, align 4
  %9 = bitcast i8* %8 to %class.btDefaultSoftBodySolver*
  %call6 = call %class.btDefaultSoftBodySolver* @_ZN23btDefaultSoftBodySolverC1Ev(%class.btDefaultSoftBodySolver* %9)
  %10 = bitcast %class.btDefaultSoftBodySolver* %9 to %class.btSoftBodySolver*
  %m_softBodySolver7 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  store %class.btSoftBodySolver* %10, %class.btSoftBodySolver** %m_softBodySolver7, align 4
  %m_ownsSolver8 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 9
  store i8 1, i8* %m_ownsSolver8, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_drawFlags = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 2
  store i32 4302, i32* %m_drawFlags, align 4
  %m_drawNodeTree = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 3
  store i8 1, i8* %m_drawNodeTree, align 4
  %m_drawFaceTree = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 4
  store i8 0, i8* %m_drawFaceTree, align 1
  %m_drawClusterTree = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 5
  store i8 0, i8* %m_drawClusterTree, align 2
  %11 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %pairCache.addr, align 4
  %m_sbi9 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %m_broadphase = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi9, i32 0, i32 5
  store %class.btBroadphaseInterface* %11, %class.btBroadphaseInterface** %m_broadphase, align 4
  %12 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %m_sbi10 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %m_dispatcher = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi10, i32 0, i32 6
  store %class.btDispatcher* %12, %class.btDispatcher** %m_dispatcher, align 4
  %m_sbi11 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %m_sparsesdf = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi11, i32 0, i32 8
  call void @_ZN11btSparseSdfILi3EE10InitializeEii(%struct.btSparseSdf* %m_sparsesdf, i32 2383, i32 262144)
  %m_sbi12 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %m_sparsesdf13 = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi12, i32 0, i32 8
  call void @_ZN11btSparseSdfILi3EE5ResetEv(%struct.btSparseSdf* %m_sparsesdf13)
  %m_sbi14 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %air_density = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi14, i32 0, i32 0
  store float 0x3FF3333340000000, float* %air_density, align 4
  %m_sbi15 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %water_density = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi15, i32 0, i32 1
  store float 0.000000e+00, float* %water_density, align 4
  %m_sbi16 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %water_offset = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi16, i32 0, i32 2
  store float 0.000000e+00, float* %water_offset, align 4
  store float 0.000000e+00, float* %ref.tmp17, align 4
  store float 0.000000e+00, float* %ref.tmp18, align 4
  store float 0.000000e+00, float* %ref.tmp19, align 4
  %call20 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp19)
  %m_sbi21 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %water_normal = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi21, i32 0, i32 4
  %13 = bitcast %class.btVector3* %water_normal to i8*
  %14 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false)
  %m_sbi22 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %m_gravity = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi22, i32 0, i32 7
  store float 0.000000e+00, float* %ref.tmp23, align 4
  store float -1.000000e+01, float* %ref.tmp24, align 4
  store float 0.000000e+00, float* %ref.tmp25, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_gravity, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  %m_sbi26 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %m_sparsesdf27 = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %m_sbi26, i32 0, i32 8
  call void @_ZN11btSparseSdfILi3EE10InitializeEii(%struct.btSparseSdf* %m_sparsesdf27, i32 2383, i32 262144)
  %15 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %retval, align 4
  ret %class.btSoftRigidDynamicsWorld* %15
}

declare %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration(%class.btDiscreteDynamicsWorld* returned, %class.btDispatcher*, %class.btBroadphaseInterface*, %class.btConstraintSolver*, %class.btCollisionConfiguration*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIP10btSoftBodyEC2Ev(%class.btAlignedObjectArray.22* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.23* @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EEC2Ev(%class.btAlignedAllocator.23* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv(%class.btAlignedObjectArray.22* %this1)
  ret %class.btAlignedObjectArray.22* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btSoftBodyWorldInfo* @_ZN19btSoftBodyWorldInfoC2Ev(%struct.btSoftBodyWorldInfo* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %struct.btSoftBodyWorldInfo* %this, %struct.btSoftBodyWorldInfo** %this.addr, align 4
  %this1 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %this.addr, align 4
  %air_density = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 0
  store float 0x3FF3333340000000, float* %air_density, align 4
  %water_density = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %water_density, align 4
  %water_offset = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 2
  store float 0.000000e+00, float* %water_offset, align 4
  %m_maxDisplacement = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 3
  store float 1.000000e+03, float* %m_maxDisplacement, align 4
  %water_normal = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %water_normal, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %m_broadphase = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 5
  store %class.btBroadphaseInterface* null, %class.btBroadphaseInterface** %m_broadphase, align 4
  %m_dispatcher = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 6
  store %class.btDispatcher* null, %class.btDispatcher** %m_dispatcher, align 4
  %m_gravity = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 7
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float -1.000000e+01, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  %call7 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_gravity, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  %m_sparsesdf = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 8
  %call8 = call %struct.btSparseSdf* @_ZN11btSparseSdfILi3EEC2Ev(%struct.btSparseSdf* %m_sparsesdf)
  ret %struct.btSoftBodyWorldInfo* %this1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

declare %class.btDefaultSoftBodySolver* @_ZN23btDefaultSoftBodySolverC1Ev(%class.btDefaultSoftBodySolver* returned) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btSparseSdfILi3EE10InitializeEii(%struct.btSparseSdf* %this, i32 %hashsize, i32 %clampCells) #2 comdat {
entry:
  %this.addr = alloca %struct.btSparseSdf*, align 4
  %hashsize.addr = alloca i32, align 4
  %clampCells.addr = alloca i32, align 4
  %ref.tmp = alloca %"struct.btSparseSdf<3>::Cell"*, align 4
  store %struct.btSparseSdf* %this, %struct.btSparseSdf** %this.addr, align 4
  store i32 %hashsize, i32* %hashsize.addr, align 4
  store i32 %clampCells, i32* %clampCells.addr, align 4
  %this1 = load %struct.btSparseSdf*, %struct.btSparseSdf** %this.addr, align 4
  %0 = load i32, i32* %clampCells.addr, align 4
  %m_clampCells = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 4
  store i32 %0, i32* %m_clampCells, align 4
  %cells = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 0
  %1 = load i32, i32* %hashsize.addr, align 4
  store %"struct.btSparseSdf<3>::Cell"* null, %"struct.btSparseSdf<3>::Cell"** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE6resizeEiRKS3_(%class.btAlignedObjectArray.41* %cells, i32 %1, %"struct.btSparseSdf<3>::Cell"** nonnull align 4 dereferenceable(4) %ref.tmp)
  call void @_ZN11btSparseSdfILi3EE5ResetEv(%struct.btSparseSdf* %this1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btSparseSdfILi3EE5ResetEv(%struct.btSparseSdf* %this) #2 comdat {
entry:
  %this.addr = alloca %struct.btSparseSdf*, align 4
  %i = alloca i32, align 4
  %ni = alloca i32, align 4
  %pc = alloca %"struct.btSparseSdf<3>::Cell"*, align 4
  %pn = alloca %"struct.btSparseSdf<3>::Cell"*, align 4
  store %struct.btSparseSdf* %this, %struct.btSparseSdf** %this.addr, align 4
  %this1 = load %struct.btSparseSdf*, %struct.btSparseSdf** %this.addr, align 4
  store i32 0, i32* %i, align 4
  %cells = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.41* %cells)
  store i32 %call, i32* %ni, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %ni, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %cells2 = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 0
  %2 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %"struct.btSparseSdf<3>::Cell"** @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEixEi(%class.btAlignedObjectArray.41* %cells2, i32 %2)
  %3 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %call3, align 4
  store %"struct.btSparseSdf<3>::Cell"* %3, %"struct.btSparseSdf<3>::Cell"** %pc, align 4
  %cells4 = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(4) %"struct.btSparseSdf<3>::Cell"** @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEixEi(%class.btAlignedObjectArray.41* %cells4, i32 %4)
  store %"struct.btSparseSdf<3>::Cell"* null, %"struct.btSparseSdf<3>::Cell"** %call5, align 4
  br label %while.cond

while.cond:                                       ; preds = %delete.end, %for.body
  %5 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pc, align 4
  %tobool = icmp ne %"struct.btSparseSdf<3>::Cell"* %5, null
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %6 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pc, align 4
  %next = getelementptr inbounds %"struct.btSparseSdf<3>::Cell", %"struct.btSparseSdf<3>::Cell"* %6, i32 0, i32 5
  %7 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %next, align 4
  store %"struct.btSparseSdf<3>::Cell"* %7, %"struct.btSparseSdf<3>::Cell"** %pn, align 4
  %8 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pc, align 4
  %isnull = icmp eq %"struct.btSparseSdf<3>::Cell"* %8, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %while.body
  %9 = bitcast %"struct.btSparseSdf<3>::Cell"* %8 to i8*
  call void @_ZdlPv(i8* %9) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %while.body
  %10 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %pn, align 4
  store %"struct.btSparseSdf<3>::Cell"* %10, %"struct.btSparseSdf<3>::Cell"** %pc, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %for.inc

for.inc:                                          ; preds = %while.end
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %voxelsz = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 1
  store float 2.500000e-01, float* %voxelsz, align 4
  %puid = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 2
  store i32 0, i32* %puid, align 4
  %ncells = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 3
  store i32 0, i32* %ncells, align 4
  %nprobes = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 5
  store i32 1, i32* %nprobes, align 4
  %nqueries = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 6
  store i32 1, i32* %nqueries, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btSoftRigidDynamicsWorld* @_ZN24btSoftRigidDynamicsWorldD2Ev(%class.btSoftRigidDynamicsWorld* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store %class.btSoftRigidDynamicsWorld* %this1, %class.btSoftRigidDynamicsWorld** %retval, align 4
  %0 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [49 x i8*] }, { [49 x i8*] }* @_ZTV24btSoftRigidDynamicsWorld, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_ownsSolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 9
  %1 = load i8, i8* %m_ownsSolver, align 4
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_softBodySolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %2 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver, align 4
  %3 = bitcast %class.btSoftBodySolver* %2 to %class.btSoftBodySolver* (%class.btSoftBodySolver*)***
  %vtable = load %class.btSoftBodySolver* (%class.btSoftBodySolver*)**, %class.btSoftBodySolver* (%class.btSoftBodySolver*)*** %3, align 4
  %vfn = getelementptr inbounds %class.btSoftBodySolver* (%class.btSoftBodySolver*)*, %class.btSoftBodySolver* (%class.btSoftBodySolver*)** %vtable, i64 0
  %4 = load %class.btSoftBodySolver* (%class.btSoftBodySolver*)*, %class.btSoftBodySolver* (%class.btSoftBodySolver*)** %vfn, align 4
  %call = call %class.btSoftBodySolver* %4(%class.btSoftBodySolver* %2) #10
  %m_softBodySolver2 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %5 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver2, align 4
  %6 = bitcast %class.btSoftBodySolver* %5 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %6)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_sbi = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 7
  %call3 = call %struct.btSoftBodyWorldInfo* @_ZN19btSoftBodyWorldInfoD2Ev(%struct.btSoftBodyWorldInfo* %m_sbi) #10
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  %call4 = call %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIP10btSoftBodyED2Ev(%class.btAlignedObjectArray.22* %m_softBodies) #10
  %7 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %call5 = call %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldD2Ev(%class.btDiscreteDynamicsWorld* %7) #10
  %8 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %retval, align 4
  ret %class.btSoftRigidDynamicsWorld* %8
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btSoftBodyWorldInfo* @_ZN19btSoftBodyWorldInfoD2Ev(%struct.btSoftBodyWorldInfo* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btSoftBodyWorldInfo*, align 4
  store %struct.btSoftBodyWorldInfo* %this, %struct.btSoftBodyWorldInfo** %this.addr, align 4
  %this1 = load %struct.btSoftBodyWorldInfo*, %struct.btSoftBodyWorldInfo** %this.addr, align 4
  %m_sparsesdf = getelementptr inbounds %struct.btSoftBodyWorldInfo, %struct.btSoftBodyWorldInfo* %this1, i32 0, i32 8
  %call = call %struct.btSparseSdf* @_ZN11btSparseSdfILi3EED2Ev(%struct.btSparseSdf* %m_sparsesdf) #10
  ret %struct.btSoftBodyWorldInfo* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.22* @_ZN20btAlignedObjectArrayIP10btSoftBodyED2Ev(%class.btAlignedObjectArray.22* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE5clearEv(%class.btAlignedObjectArray.22* %this1)
  ret %class.btAlignedObjectArray.22* %this1
}

; Function Attrs: nounwind
declare %class.btDiscreteDynamicsWorld* @_ZN23btDiscreteDynamicsWorldD2Ev(%class.btDiscreteDynamicsWorld* returned) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN24btSoftRigidDynamicsWorldD0Ev(%class.btSoftRigidDynamicsWorld* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %call = call %class.btSoftRigidDynamicsWorld* @_ZN24btSoftRigidDynamicsWorldD1Ev(%class.btSoftRigidDynamicsWorld* %this1) #10
  %0 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to i8*
  call void @_ZN23btDiscreteDynamicsWorlddlEPv(i8* %0) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorlddlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN24btSoftRigidDynamicsWorld25predictUnconstraintMotionEf(%class.btSoftRigidDynamicsWorld* %this, float %timeStep) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %1 = load float, float* %timeStep.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf(%class.btDiscreteDynamicsWorld* %0, float %1)
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([34 x i8], [34 x i8]* @.str, i32 0, i32 0))
  %m_softBodySolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %2 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver, align 4
  %3 = load float, float* %timeStep.addr, align 4
  %4 = bitcast %class.btSoftBodySolver* %2 to void (%class.btSoftBodySolver*, float)***
  %vtable = load void (%class.btSoftBodySolver*, float)**, void (%class.btSoftBodySolver*, float)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btSoftBodySolver*, float)*, void (%class.btSoftBodySolver*, float)** %vtable, i64 6
  %5 = load void (%class.btSoftBodySolver*, float)*, void (%class.btSoftBodySolver*, float)** %vfn, align 4
  call void %5(%class.btSoftBodySolver* %2, float %3)
  %call2 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

declare void @_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* returned, i8*) unnamed_addr #3

; Function Attrs: nounwind
declare %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* returned) unnamed_addr #5

; Function Attrs: noinline optnone
define hidden void @_ZN24btSoftRigidDynamicsWorld28internalSingleStepSimulationEf(%class.btSoftRigidDynamicsWorld* %this, float %timeStep) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %i = alloca i32, align 4
  %psb = alloca %class.btSoftBody*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %m_softBodySolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %0 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver, align 4
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.22* @_ZN24btSoftRigidDynamicsWorld16getSoftBodyArrayEv(%class.btSoftRigidDynamicsWorld* %this1)
  %1 = bitcast %class.btSoftBodySolver* %0 to void (%class.btSoftBodySolver*, %class.btAlignedObjectArray.22*, i1)***
  %vtable = load void (%class.btSoftBodySolver*, %class.btAlignedObjectArray.22*, i1)**, void (%class.btSoftBodySolver*, %class.btAlignedObjectArray.22*, i1)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btSoftBodySolver*, %class.btAlignedObjectArray.22*, i1)*, void (%class.btSoftBodySolver*, %class.btAlignedObjectArray.22*, i1)** %vtable, i64 4
  %2 = load void (%class.btSoftBodySolver*, %class.btAlignedObjectArray.22*, i1)*, void (%class.btSoftBodySolver*, %class.btAlignedObjectArray.22*, i1)** %vfn, align 4
  call void %2(%class.btSoftBodySolver* %0, %class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %call, i1 zeroext false)
  %m_softBodySolver2 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %3 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver2, align 4
  %4 = bitcast %class.btSoftBodySolver* %3 to i1 (%class.btSoftBodySolver*)***
  %vtable3 = load i1 (%class.btSoftBodySolver*)**, i1 (%class.btSoftBodySolver*)*** %4, align 4
  %vfn4 = getelementptr inbounds i1 (%class.btSoftBodySolver*)*, i1 (%class.btSoftBodySolver*)** %vtable3, i64 3
  %5 = load i1 (%class.btSoftBodySolver*)*, i1 (%class.btSoftBodySolver*)** %vfn4, align 4
  %call5 = call zeroext i1 %5(%class.btSoftBodySolver* %3)
  br i1 %call5, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %6 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %7 = load float, float* %timeStep.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf(%class.btDiscreteDynamicsWorld* %6, float %7)
  %8 = load float, float* %timeStep.addr, align 4
  call void @_ZN24btSoftRigidDynamicsWorld26solveSoftBodiesConstraintsEf(%class.btSoftRigidDynamicsWorld* %this1, float %8)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.end
  %9 = load i32, i32* %i, align 4
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  %call6 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.22* %m_softBodies)
  %cmp = icmp slt i32 %9, %call6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_softBodies7 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  %10 = load i32, i32* %i, align 4
  %call8 = call nonnull align 4 dereferenceable(4) %class.btSoftBody** @_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi(%class.btAlignedObjectArray.22* %m_softBodies7, i32 %10)
  %11 = load %class.btSoftBody*, %class.btSoftBody** %call8, align 4
  store %class.btSoftBody* %11, %class.btSoftBody** %psb, align 4
  %12 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %13 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  call void @_ZN10btSoftBody23defaultCollisionHandlerEPS_(%class.btSoftBody* %12, %class.btSoftBody* %13)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %14 = load i32, i32* %i, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_softBodySolver9 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %15 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver9, align 4
  %16 = bitcast %class.btSoftBodySolver* %15 to void (%class.btSoftBodySolver*)***
  %vtable10 = load void (%class.btSoftBodySolver*)**, void (%class.btSoftBodySolver*)*** %16, align 4
  %vfn11 = getelementptr inbounds void (%class.btSoftBodySolver*)*, void (%class.btSoftBodySolver*)** %vtable10, i64 8
  %17 = load void (%class.btSoftBodySolver*)*, void (%class.btSoftBodySolver*)** %vfn11, align 4
  call void %17(%class.btSoftBodySolver* %15)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.22* @_ZN24btSoftRigidDynamicsWorld16getSoftBodyArrayEv(%class.btSoftRigidDynamicsWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray.22* %m_softBodies
}

declare void @_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN24btSoftRigidDynamicsWorld26solveSoftBodiesConstraintsEf(%class.btSoftRigidDynamicsWorld* %this, float %timeStep) #2 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  %__profile = alloca %class.CProfileSample, align 1
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.1, i32 0, i32 0))
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.22* %m_softBodies)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_softBodies3 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  call void @_ZN10btSoftBody13solveClustersERK20btAlignedObjectArrayIPS_E(%class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17) %m_softBodies3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_softBodySolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %0 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver, align 4
  %1 = load float, float* %timeStep.addr, align 4
  %m_softBodySolver4 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %2 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver4, align 4
  %call5 = call float @_ZN16btSoftBodySolver12getTimeScaleEv(%class.btSoftBodySolver* %2)
  %mul = fmul float %1, %call5
  %3 = bitcast %class.btSoftBodySolver* %0 to void (%class.btSoftBodySolver*, float)***
  %vtable = load void (%class.btSoftBodySolver*, float)**, void (%class.btSoftBodySolver*, float)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btSoftBodySolver*, float)*, void (%class.btSoftBodySolver*, float)** %vtable, i64 7
  %4 = load void (%class.btSoftBodySolver*, float)*, void (%class.btSoftBodySolver*, float)** %vfn, align 4
  call void %4(%class.btSoftBodySolver* %0, float %mul)
  %call6 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.22* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btSoftBody** @_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi(%class.btAlignedObjectArray.22* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %0 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %0, i32 %1
  ret %class.btSoftBody** %arrayidx
}

declare void @_ZN10btSoftBody23defaultCollisionHandlerEPS_(%class.btSoftBody*, %class.btSoftBody*) #3

declare void @_ZN10btSoftBody13solveClustersERK20btAlignedObjectArrayIPS_E(%class.btAlignedObjectArray.22* nonnull align 4 dereferenceable(17)) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZN16btSoftBodySolver12getTimeScaleEv(%class.btSoftBodySolver* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBodySolver*, align 4
  store %class.btSoftBodySolver* %this, %class.btSoftBodySolver** %this.addr, align 4
  %this1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %this.addr, align 4
  %m_timeScale = getelementptr inbounds %class.btSoftBodySolver, %class.btSoftBodySolver* %this1, i32 0, i32 3
  %0 = load float, float* %m_timeScale, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN24btSoftRigidDynamicsWorld11addSoftBodyEP10btSoftBodyii(%class.btSoftRigidDynamicsWorld* %this, %class.btSoftBody* %body, i32 %collisionFilterGroup, i32 %collisionFilterMask) #2 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %body.addr = alloca %class.btSoftBody*, align 4
  %collisionFilterGroup.addr = alloca i32, align 4
  %collisionFilterMask.addr = alloca i32, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store %class.btSoftBody* %body, %class.btSoftBody** %body.addr, align 4
  store i32 %collisionFilterGroup, i32* %collisionFilterGroup.addr, align 4
  store i32 %collisionFilterMask, i32* %collisionFilterMask.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE9push_backERKS1_(%class.btAlignedObjectArray.22* %m_softBodies, %class.btSoftBody** nonnull align 4 dereferenceable(4) %body.addr)
  %0 = load %class.btSoftBody*, %class.btSoftBody** %body.addr, align 4
  %m_softBodySolver = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 8
  %1 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %m_softBodySolver, align 4
  call void @_ZN10btSoftBody17setSoftBodySolverEP16btSoftBodySolver(%class.btSoftBody* %0, %class.btSoftBodySolver* %1)
  %2 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %3 = load %class.btSoftBody*, %class.btSoftBody** %body.addr, align 4
  %4 = bitcast %class.btSoftBody* %3 to %class.btCollisionObject*
  %5 = load i32, i32* %collisionFilterGroup.addr, align 4
  %6 = load i32, i32* %collisionFilterMask.addr, align 4
  call void @_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectii(%class.btCollisionWorld* %2, %class.btCollisionObject* %4, i32 %5, i32 %6)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE9push_backERKS1_(%class.btAlignedObjectArray.22* %this, %class.btSoftBody** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %_Val.addr = alloca %class.btSoftBody**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store %class.btSoftBody** %_Val, %class.btSoftBody*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE8capacityEv(%class.btAlignedObjectArray.22* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP10btSoftBodyE9allocSizeEi(%class.btAlignedObjectArray.22* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7reserveEi(%class.btAlignedObjectArray.22* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %1 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %1, i32 %2
  %3 = bitcast %class.btSoftBody** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btSoftBody**
  %5 = load %class.btSoftBody**, %class.btSoftBody*** %_Val.addr, align 4
  %6 = load %class.btSoftBody*, %class.btSoftBody** %5, align 4
  store %class.btSoftBody* %6, %class.btSoftBody** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN10btSoftBody17setSoftBodySolverEP16btSoftBodySolver(%class.btSoftBody* %this, %class.btSoftBodySolver* %softBodySolver) #1 comdat {
entry:
  %this.addr = alloca %class.btSoftBody*, align 4
  %softBodySolver.addr = alloca %class.btSoftBodySolver*, align 4
  store %class.btSoftBody* %this, %class.btSoftBody** %this.addr, align 4
  store %class.btSoftBodySolver* %softBodySolver, %class.btSoftBodySolver** %softBodySolver.addr, align 4
  %this1 = load %class.btSoftBody*, %class.btSoftBody** %this.addr, align 4
  %0 = load %class.btSoftBodySolver*, %class.btSoftBodySolver** %softBodySolver.addr, align 4
  %m_softBodySolver = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %this1, i32 0, i32 2
  store %class.btSoftBodySolver* %0, %class.btSoftBodySolver** %m_softBodySolver, align 4
  ret void
}

declare void @_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectii(%class.btCollisionWorld*, %class.btCollisionObject*, i32, i32) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN24btSoftRigidDynamicsWorld14removeSoftBodyEP10btSoftBody(%class.btSoftRigidDynamicsWorld* %this, %class.btSoftBody* %body) #2 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %body.addr = alloca %class.btSoftBody*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store %class.btSoftBody* %body, %class.btSoftBody** %body.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE6removeERKS1_(%class.btAlignedObjectArray.22* %m_softBodies, %class.btSoftBody** nonnull align 4 dereferenceable(4) %body.addr)
  %0 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %1 = load %class.btSoftBody*, %class.btSoftBody** %body.addr, align 4
  %2 = bitcast %class.btSoftBody* %1 to %class.btCollisionObject*
  call void @_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject(%class.btCollisionWorld* %0, %class.btCollisionObject* %2)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE6removeERKS1_(%class.btAlignedObjectArray.22* %this, %class.btSoftBody** nonnull align 4 dereferenceable(4) %key) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %key.addr = alloca %class.btSoftBody**, align 4
  %findIndex = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store %class.btSoftBody** %key, %class.btSoftBody*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load %class.btSoftBody**, %class.btSoftBody*** %key.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE16findLinearSearchERKS1_(%class.btAlignedObjectArray.22* %this1, %class.btSoftBody** nonnull align 4 dereferenceable(4) %0)
  store i32 %call, i32* %findIndex, align 4
  %1 = load i32, i32* %findIndex, align 4
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE13removeAtIndexEi(%class.btAlignedObjectArray.22* %this1, i32 %1)
  ret void
}

declare void @_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject(%class.btCollisionWorld*, %class.btCollisionObject*) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN24btSoftRigidDynamicsWorld21removeCollisionObjectEP17btCollisionObject(%class.btSoftRigidDynamicsWorld* %this, %class.btCollisionObject* %collisionObject) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %body = alloca %class.btSoftBody*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4
  %call = call %class.btSoftBody* @_ZN10btSoftBody6upcastEP17btCollisionObject(%class.btCollisionObject* %0)
  store %class.btSoftBody* %call, %class.btSoftBody** %body, align 4
  %1 = load %class.btSoftBody*, %class.btSoftBody** %body, align 4
  %tobool = icmp ne %class.btSoftBody* %1, null
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load %class.btSoftBody*, %class.btSoftBody** %body, align 4
  call void @_ZN24btSoftRigidDynamicsWorld14removeSoftBodyEP10btSoftBody(%class.btSoftRigidDynamicsWorld* %this1, %class.btSoftBody* %2)
  br label %if.end

if.else:                                          ; preds = %entry
  %3 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject(%class.btDiscreteDynamicsWorld* %3, %class.btCollisionObject* %4)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btSoftBody* @_ZN10btSoftBody6upcastEP17btCollisionObject(%class.btCollisionObject* %colObj) #2 comdat {
entry:
  %retval = alloca %class.btSoftBody*, align 4
  %colObj.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %colObj, %class.btCollisionObject** %colObj.addr, align 4
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %0)
  %cmp = icmp eq i32 %call, 8
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj.addr, align 4
  %2 = bitcast %class.btCollisionObject* %1 to %class.btSoftBody*
  store %class.btSoftBody* %2, %class.btSoftBody** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store %class.btSoftBody* null, %class.btSoftBody** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %class.btSoftBody*, %class.btSoftBody** %retval, align 4
  ret %class.btSoftBody* %3
}

declare void @_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject(%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*) unnamed_addr #3

; Function Attrs: noinline optnone
define hidden void @_ZN24btSoftRigidDynamicsWorld14debugDrawWorldEv(%class.btSoftRigidDynamicsWorld* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %i = alloca i32, align 4
  %psb = alloca %class.btSoftBody*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %0 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  call void @_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv(%class.btDiscreteDynamicsWorld* %0)
  %1 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %2 = bitcast %class.btCollisionWorld* %1 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %2, align 4
  %vfn = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable, i64 5
  %3 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn, align 4
  %call = call %class.btIDebugDraw* %3(%class.btCollisionWorld* %1)
  %tobool = icmp ne %class.btIDebugDraw* %call, null
  br i1 %tobool, label %if.then, label %if.end41

if.then:                                          ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %4 = load i32, i32* %i, align 4
  %m_softBodies = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.22* %m_softBodies)
  %cmp = icmp slt i32 %4, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_softBodies3 = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 1
  %5 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %class.btSoftBody** @_ZN20btAlignedObjectArrayIP10btSoftBodyEixEi(%class.btAlignedObjectArray.22* %m_softBodies3, i32 %5)
  %6 = load %class.btSoftBody*, %class.btSoftBody** %call4, align 4
  store %class.btSoftBody* %6, %class.btSoftBody** %psb, align 4
  %7 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %8 = bitcast %class.btCollisionWorld* %7 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable5 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %8, align 4
  %vfn6 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable5, i64 5
  %9 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn6, align 4
  %call7 = call %class.btIDebugDraw* %9(%class.btCollisionWorld* %7)
  %tobool8 = icmp ne %class.btIDebugDraw* %call7, null
  br i1 %tobool8, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body
  %10 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %11 = bitcast %class.btCollisionWorld* %10 to %class.btIDebugDraw* (%class.btCollisionWorld*)***
  %vtable9 = load %class.btIDebugDraw* (%class.btCollisionWorld*)**, %class.btIDebugDraw* (%class.btCollisionWorld*)*** %11, align 4
  %vfn10 = getelementptr inbounds %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vtable9, i64 5
  %12 = load %class.btIDebugDraw* (%class.btCollisionWorld*)*, %class.btIDebugDraw* (%class.btCollisionWorld*)** %vfn10, align 4
  %call11 = call %class.btIDebugDraw* %12(%class.btCollisionWorld* %10)
  %13 = bitcast %class.btIDebugDraw* %call11 to i32 (%class.btIDebugDraw*)***
  %vtable12 = load i32 (%class.btIDebugDraw*)**, i32 (%class.btIDebugDraw*)*** %13, align 4
  %vfn13 = getelementptr inbounds i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vtable12, i64 14
  %14 = load i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vfn13, align 4
  %call14 = call i32 %14(%class.btIDebugDraw* %call11)
  %and = and i32 %call14, 1
  %tobool15 = icmp ne i32 %and, 0
  br i1 %tobool15, label %if.then16, label %if.end

if.then16:                                        ; preds = %land.lhs.true
  %15 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %16 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %16, i32 0, i32 5
  %17 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  call void @_ZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDraw(%class.btSoftBody* %15, %class.btIDebugDraw* %17)
  %18 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %19 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer17 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %19, i32 0, i32 5
  %20 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer17, align 4
  %m_drawFlags = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 2
  %21 = load i32, i32* %m_drawFlags, align 4
  call void @_ZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawi(%class.btSoftBody* %18, %class.btIDebugDraw* %20, i32 %21)
  br label %if.end

if.end:                                           ; preds = %if.then16, %land.lhs.true, %for.body
  %22 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer18 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %22, i32 0, i32 5
  %23 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer18, align 4
  %tobool19 = icmp ne %class.btIDebugDraw* %23, null
  br i1 %tobool19, label %land.lhs.true20, label %if.end40

land.lhs.true20:                                  ; preds = %if.end
  %24 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer21 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %24, i32 0, i32 5
  %25 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer21, align 4
  %26 = bitcast %class.btIDebugDraw* %25 to i32 (%class.btIDebugDraw*)***
  %vtable22 = load i32 (%class.btIDebugDraw*)**, i32 (%class.btIDebugDraw*)*** %26, align 4
  %vfn23 = getelementptr inbounds i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vtable22, i64 14
  %27 = load i32 (%class.btIDebugDraw*)*, i32 (%class.btIDebugDraw*)** %vfn23, align 4
  %call24 = call i32 %27(%class.btIDebugDraw* %25)
  %and25 = and i32 %call24, 2
  %tobool26 = icmp ne i32 %and25, 0
  br i1 %tobool26, label %if.then27, label %if.end40

if.then27:                                        ; preds = %land.lhs.true20
  %m_drawNodeTree = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 3
  %28 = load i8, i8* %m_drawNodeTree, align 4
  %tobool28 = trunc i8 %28 to i1
  br i1 %tobool28, label %if.then29, label %if.end31

if.then29:                                        ; preds = %if.then27
  %29 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %30 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer30 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %30, i32 0, i32 5
  %31 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer30, align 4
  call void @_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %29, %class.btIDebugDraw* %31, i32 0, i32 -1)
  br label %if.end31

if.end31:                                         ; preds = %if.then29, %if.then27
  %m_drawFaceTree = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 4
  %32 = load i8, i8* %m_drawFaceTree, align 1
  %tobool32 = trunc i8 %32 to i1
  br i1 %tobool32, label %if.then33, label %if.end35

if.then33:                                        ; preds = %if.end31
  %33 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %34 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer34 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %34, i32 0, i32 5
  %35 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer34, align 4
  call void @_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %33, %class.btIDebugDraw* %35, i32 0, i32 -1)
  br label %if.end35

if.end35:                                         ; preds = %if.then33, %if.end31
  %m_drawClusterTree = getelementptr inbounds %class.btSoftRigidDynamicsWorld, %class.btSoftRigidDynamicsWorld* %this1, i32 0, i32 5
  %36 = load i8, i8* %m_drawClusterTree, align 2
  %tobool36 = trunc i8 %36 to i1
  br i1 %tobool36, label %if.then37, label %if.end39

if.then37:                                        ; preds = %if.end35
  %37 = load %class.btSoftBody*, %class.btSoftBody** %psb, align 4
  %38 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_debugDrawer38 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %38, i32 0, i32 5
  %39 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer38, align 4
  call void @_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody* %37, %class.btIDebugDraw* %39, i32 0, i32 -1)
  br label %if.end39

if.end39:                                         ; preds = %if.then37, %if.end35
  br label %if.end40

if.end40:                                         ; preds = %if.end39, %land.lhs.true20, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end40
  %40 = load i32, i32* %i, align 4
  %inc = add nsw i32 %40, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end41

if.end41:                                         ; preds = %for.end, %entry
  ret void
}

declare void @_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare void @_ZN17btSoftBodyHelpers9DrawFrameEP10btSoftBodyP12btIDebugDraw(%class.btSoftBody*, %class.btIDebugDraw*) #3

declare void @_ZN17btSoftBodyHelpers4DrawEP10btSoftBodyP12btIDebugDrawi(%class.btSoftBody*, %class.btIDebugDraw*, i32) #3

declare void @_ZN17btSoftBodyHelpers12DrawNodeTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody*, %class.btIDebugDraw*, i32, i32) #3

declare void @_ZN17btSoftBodyHelpers12DrawFaceTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody*, %class.btIDebugDraw*, i32, i32) #3

declare void @_ZN17btSoftBodyHelpers15DrawClusterTreeEP10btSoftBodyP12btIDebugDrawii(%class.btSoftBody*, %class.btIDebugDraw*, i32, i32) #3

; Function Attrs: noinline optnone
define hidden void @_ZNK24btSoftRigidDynamicsWorld7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE(%class.btSoftRigidDynamicsWorld* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFromWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %rayToWorld, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(24) %resultCallback) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %rayFromWorld.addr = alloca %class.btVector3*, align 4
  %rayToWorld.addr = alloca %class.btVector3*, align 4
  %resultCallback.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %rayCB = alloca %struct.btSoftSingleRayCallback, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp10 = alloca float, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store %class.btVector3* %rayFromWorld, %class.btVector3** %rayFromWorld.addr, align 4
  store %class.btVector3* %rayToWorld, %class.btVector3** %rayToWorld.addr, align 4
  store %"struct.btCollisionWorld::RayResultCallback"* %resultCallback, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([8 x i8], [8 x i8]* @.str.2, i32 0, i32 0))
  %0 = load %class.btVector3*, %class.btVector3** %rayFromWorld.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %rayToWorld.addr, align 4
  %2 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4
  %call2 = call %struct.btSoftSingleRayCallback* @_ZN23btSoftSingleRayCallbackC2ERK9btVector3S2_PK24btSoftRigidDynamicsWorldRN16btCollisionWorld17RayResultCallbackE(%struct.btSoftSingleRayCallback* %rayCB, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btSoftRigidDynamicsWorld* %this1, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(24) %2)
  %3 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_broadphasePairCache = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %3, i32 0, i32 4
  %4 = load %class.btBroadphaseInterface*, %class.btBroadphaseInterface** %m_broadphasePairCache, align 4
  %5 = load %class.btVector3*, %class.btVector3** %rayFromWorld.addr, align 4
  %6 = load %class.btVector3*, %class.btVector3** %rayToWorld.addr, align 4
  %7 = bitcast %struct.btSoftSingleRayCallback* %rayCB to %struct.btBroadphaseRayCallback*
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 0.000000e+00, float* %ref.tmp9, align 4
  store float 0.000000e+00, float* %ref.tmp10, align 4
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp10)
  %8 = bitcast %class.btBroadphaseInterface* %4 to void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*** %8, align 4
  %vfn = getelementptr inbounds void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 6
  %9 = load void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btBroadphaseInterface*, %class.btVector3*, %class.btVector3*, %struct.btBroadphaseRayCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %9(%class.btBroadphaseInterface* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btVector3* nonnull align 4 dereferenceable(16) %6, %struct.btBroadphaseRayCallback* nonnull align 4 dereferenceable(36) %7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7)
  %call12 = call %struct.btSoftSingleRayCallback* @_ZN23btSoftSingleRayCallbackD2Ev(%struct.btSoftSingleRayCallback* %rayCB) #10
  %call13 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #10
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btSoftSingleRayCallback* @_ZN23btSoftSingleRayCallbackC2ERK9btVector3S2_PK24btSoftRigidDynamicsWorldRN16btCollisionWorld17RayResultCallbackE(%struct.btSoftSingleRayCallback* returned %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFromWorld, %class.btVector3* nonnull align 4 dereferenceable(16) %rayToWorld, %class.btSoftRigidDynamicsWorld* %world, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(24) %resultCallback) unnamed_addr #2 comdat {
entry:
  %retval = alloca %struct.btSoftSingleRayCallback*, align 4
  %this.addr = alloca %struct.btSoftSingleRayCallback*, align 4
  %rayFromWorld.addr = alloca %class.btVector3*, align 4
  %rayToWorld.addr = alloca %class.btVector3*, align 4
  %world.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %resultCallback.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  %rayDir = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %struct.btSoftSingleRayCallback* %this, %struct.btSoftSingleRayCallback** %this.addr, align 4
  store %class.btVector3* %rayFromWorld, %class.btVector3** %rayFromWorld.addr, align 4
  store %class.btVector3* %rayToWorld, %class.btVector3** %rayToWorld.addr, align 4
  store %class.btSoftRigidDynamicsWorld* %world, %class.btSoftRigidDynamicsWorld** %world.addr, align 4
  store %"struct.btCollisionWorld::RayResultCallback"* %resultCallback, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4
  %this1 = load %struct.btSoftSingleRayCallback*, %struct.btSoftSingleRayCallback** %this.addr, align 4
  store %struct.btSoftSingleRayCallback* %this1, %struct.btSoftSingleRayCallback** %retval, align 4
  %0 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %call = call %struct.btBroadphaseRayCallback* @_ZN23btBroadphaseRayCallbackC2Ev(%struct.btBroadphaseRayCallback* %0)
  %1 = bitcast %struct.btSoftSingleRayCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV23btSoftSingleRayCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_rayFromWorld = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 1
  %2 = load %class.btVector3*, %class.btVector3** %rayFromWorld.addr, align 4
  %3 = bitcast %class.btVector3* %m_rayFromWorld to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %m_rayToWorld = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 2
  %5 = load %class.btVector3*, %class.btVector3** %rayToWorld.addr, align 4
  %6 = bitcast %class.btVector3* %m_rayToWorld to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_rayFromTrans = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 3
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_rayFromTrans)
  %m_rayToTrans = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 4
  %call3 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_rayToTrans)
  %m_hitNormal = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 5
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_hitNormal)
  %m_world = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 6
  %8 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %world.addr, align 4
  store %class.btSoftRigidDynamicsWorld* %8, %class.btSoftRigidDynamicsWorld** %m_world, align 4
  %m_resultCallback = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 7
  %9 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4
  store %"struct.btCollisionWorld::RayResultCallback"* %9, %"struct.btCollisionWorld::RayResultCallback"** %m_resultCallback, align 4
  %m_rayFromTrans5 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 3
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %m_rayFromTrans5)
  %m_rayFromTrans6 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 3
  %m_rayFromWorld7 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 1
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %m_rayFromTrans6, %class.btVector3* nonnull align 4 dereferenceable(16) %m_rayFromWorld7)
  %m_rayToTrans8 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 4
  call void @_ZN11btTransform11setIdentityEv(%class.btTransform* %m_rayToTrans8)
  %m_rayToTrans9 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 4
  %m_rayToWorld10 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 2
  call void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %m_rayToTrans9, %class.btVector3* nonnull align 4 dereferenceable(16) %m_rayToWorld10)
  %10 = load %class.btVector3*, %class.btVector3** %rayToWorld.addr, align 4
  %11 = load %class.btVector3*, %class.btVector3** %rayFromWorld.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rayDir, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %11)
  %call11 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %rayDir)
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx = getelementptr inbounds float, float* %call12, i32 0
  %12 = load float, float* %arrayidx, align 4
  %cmp = fcmp oeq float %12, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  br label %cond.end

cond.false:                                       ; preds = %entry
  %call13 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx14 = getelementptr inbounds float, float* %call13, i32 0
  %13 = load float, float* %arrayidx14, align 4
  %div = fdiv float 1.000000e+00, %13
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ 0x46293E5940000000, %cond.true ], [ %div, %cond.false ]
  %14 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_rayDirectionInverse = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %14, i32 0, i32 1
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_rayDirectionInverse)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 0
  store float %cond, float* %arrayidx16, align 4
  %call17 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx18 = getelementptr inbounds float, float* %call17, i32 1
  %15 = load float, float* %arrayidx18, align 4
  %cmp19 = fcmp oeq float %15, 0.000000e+00
  br i1 %cmp19, label %cond.true20, label %cond.false21

cond.true20:                                      ; preds = %cond.end
  br label %cond.end25

cond.false21:                                     ; preds = %cond.end
  %call22 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx23 = getelementptr inbounds float, float* %call22, i32 1
  %16 = load float, float* %arrayidx23, align 4
  %div24 = fdiv float 1.000000e+00, %16
  br label %cond.end25

cond.end25:                                       ; preds = %cond.false21, %cond.true20
  %cond26 = phi float [ 0x46293E5940000000, %cond.true20 ], [ %div24, %cond.false21 ]
  %17 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_rayDirectionInverse27 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %17, i32 0, i32 1
  %call28 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_rayDirectionInverse27)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 1
  store float %cond26, float* %arrayidx29, align 4
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 2
  %18 = load float, float* %arrayidx31, align 4
  %cmp32 = fcmp oeq float %18, 0.000000e+00
  br i1 %cmp32, label %cond.true33, label %cond.false34

cond.true33:                                      ; preds = %cond.end25
  br label %cond.end38

cond.false34:                                     ; preds = %cond.end25
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %rayDir)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 2
  %19 = load float, float* %arrayidx36, align 4
  %div37 = fdiv float 1.000000e+00, %19
  br label %cond.end38

cond.end38:                                       ; preds = %cond.false34, %cond.true33
  %cond39 = phi float [ 0x46293E5940000000, %cond.true33 ], [ %div37, %cond.false34 ]
  %20 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_rayDirectionInverse40 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %20, i32 0, i32 1
  %call41 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_rayDirectionInverse40)
  %arrayidx42 = getelementptr inbounds float, float* %call41, i32 2
  store float %cond39, float* %arrayidx42, align 4
  %21 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_rayDirectionInverse43 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %21, i32 0, i32 1
  %call44 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_rayDirectionInverse43)
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 0
  %22 = load float, float* %arrayidx45, align 4
  %conv = fpext float %22 to double
  %cmp46 = fcmp olt double %conv, 0.000000e+00
  %conv47 = zext i1 %cmp46 to i32
  %23 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_signs = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %23, i32 0, i32 2
  %arrayidx48 = getelementptr inbounds [3 x i32], [3 x i32]* %m_signs, i32 0, i32 0
  store i32 %conv47, i32* %arrayidx48, align 4
  %24 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_rayDirectionInverse49 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %24, i32 0, i32 1
  %call50 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_rayDirectionInverse49)
  %arrayidx51 = getelementptr inbounds float, float* %call50, i32 1
  %25 = load float, float* %arrayidx51, align 4
  %conv52 = fpext float %25 to double
  %cmp53 = fcmp olt double %conv52, 0.000000e+00
  %conv54 = zext i1 %cmp53 to i32
  %26 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_signs55 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %26, i32 0, i32 2
  %arrayidx56 = getelementptr inbounds [3 x i32], [3 x i32]* %m_signs55, i32 0, i32 1
  store i32 %conv54, i32* %arrayidx56, align 4
  %27 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_rayDirectionInverse57 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %27, i32 0, i32 1
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_rayDirectionInverse57)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 2
  %28 = load float, float* %arrayidx59, align 4
  %conv60 = fpext float %28 to double
  %cmp61 = fcmp olt double %conv60, 0.000000e+00
  %conv62 = zext i1 %cmp61 to i32
  %29 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_signs63 = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %29, i32 0, i32 2
  %arrayidx64 = getelementptr inbounds [3 x i32], [3 x i32]* %m_signs63, i32 0, i32 2
  store i32 %conv62, i32* %arrayidx64, align 4
  %m_rayToWorld65 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 2
  %m_rayFromWorld66 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_rayToWorld65, %class.btVector3* nonnull align 4 dereferenceable(16) %m_rayFromWorld66)
  %call67 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %rayDir, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %30 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %m_lambda_max = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %30, i32 0, i32 3
  store float %call67, float* %m_lambda_max, align 4
  %31 = load %struct.btSoftSingleRayCallback*, %struct.btSoftSingleRayCallback** %retval, align 4
  ret %struct.btSoftSingleRayCallback* %31
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btSoftSingleRayCallback* @_ZN23btSoftSingleRayCallbackD2Ev(%struct.btSoftSingleRayCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btSoftSingleRayCallback*, align 4
  store %struct.btSoftSingleRayCallback* %this, %struct.btSoftSingleRayCallback** %this.addr, align 4
  %this1 = load %struct.btSoftSingleRayCallback*, %struct.btSoftSingleRayCallback** %this.addr, align 4
  %0 = bitcast %struct.btSoftSingleRayCallback* %this1 to %struct.btBroadphaseRayCallback*
  %call = call %struct.btBroadphaseRayCallback* @_ZN23btBroadphaseRayCallbackD2Ev(%struct.btBroadphaseRayCallback* %0) #10
  ret %struct.btSoftSingleRayCallback* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZN24btSoftRigidDynamicsWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RN16btCollisionWorld17RayResultCallbackE(%class.btTransform* nonnull align 4 dereferenceable(64) %rayFromTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %rayToTrans, %class.btCollisionObject* %collisionObject, %class.btCollisionShape* %collisionShape, %class.btTransform* nonnull align 4 dereferenceable(64) %colObjWorldTransform, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(24) %resultCallback) #2 {
entry:
  %rayFromTrans.addr = alloca %class.btTransform*, align 4
  %rayToTrans.addr = alloca %class.btTransform*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %collisionShape.addr = alloca %class.btCollisionShape*, align 4
  %colObjWorldTransform.addr = alloca %class.btTransform*, align 4
  %resultCallback.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  %softBody = alloca %class.btSoftBody*, align 4
  %softResult = alloca %"struct.btSoftBody::sRayCast", align 4
  %shapeInfo = alloca %"struct.btCollisionWorld::LocalShapeInfo", align 4
  %rayDir = alloca %class.btVector3, align 4
  %normal = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %rayResult = alloca %"struct.btCollisionWorld::LocalRayResult", align 4
  %normalInWorldSpace = alloca i8, align 1
  store %class.btTransform* %rayFromTrans, %class.btTransform** %rayFromTrans.addr, align 4
  store %class.btTransform* %rayToTrans, %class.btTransform** %rayToTrans.addr, align 4
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4
  store %class.btCollisionShape* %collisionShape, %class.btCollisionShape** %collisionShape.addr, align 4
  store %class.btTransform* %colObjWorldTransform, %class.btTransform** %colObjWorldTransform.addr, align 4
  store %"struct.btCollisionWorld::RayResultCallback"* %resultCallback, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4
  %call = call zeroext i1 @_ZNK16btCollisionShape10isSoftBodyEv(%class.btCollisionShape* %0)
  br i1 %call, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4
  %call1 = call %class.btSoftBody* @_ZN10btSoftBody6upcastEP17btCollisionObject(%class.btCollisionObject* %1)
  store %class.btSoftBody* %call1, %class.btSoftBody** %softBody, align 4
  %2 = load %class.btSoftBody*, %class.btSoftBody** %softBody, align 4
  %tobool = icmp ne %class.btSoftBody* %2, null
  br i1 %tobool, label %if.then2, label %if.end25

if.then2:                                         ; preds = %if.then
  %3 = load %class.btSoftBody*, %class.btSoftBody** %softBody, align 4
  %4 = load %class.btTransform*, %class.btTransform** %rayFromTrans.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %4)
  %5 = load %class.btTransform*, %class.btTransform** %rayToTrans.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %5)
  %call5 = call zeroext i1 @_ZN10btSoftBody7rayTestERK9btVector3S2_RNS_8sRayCastE(%class.btSoftBody* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call4, %"struct.btSoftBody::sRayCast"* nonnull align 4 dereferenceable(16) %softResult)
  br i1 %call5, label %if.then6, label %if.end24

if.then6:                                         ; preds = %if.then2
  %fraction = getelementptr inbounds %"struct.btSoftBody::sRayCast", %"struct.btSoftBody::sRayCast"* %softResult, i32 0, i32 3
  %6 = load float, float* %fraction, align 4
  %7 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %7, i32 0, i32 1
  %8 = load float, float* %m_closestHitFraction, align 4
  %cmp = fcmp ole float %6, %8
  br i1 %cmp, label %if.then7, label %if.end23

if.then7:                                         ; preds = %if.then6
  %m_shapePart = getelementptr inbounds %"struct.btCollisionWorld::LocalShapeInfo", %"struct.btCollisionWorld::LocalShapeInfo"* %shapeInfo, i32 0, i32 0
  store i32 0, i32* %m_shapePart, align 4
  %index = getelementptr inbounds %"struct.btSoftBody::sRayCast", %"struct.btSoftBody::sRayCast"* %softResult, i32 0, i32 2
  %9 = load i32, i32* %index, align 4
  %m_triangleIndex = getelementptr inbounds %"struct.btCollisionWorld::LocalShapeInfo", %"struct.btCollisionWorld::LocalShapeInfo"* %shapeInfo, i32 0, i32 1
  store i32 %9, i32* %m_triangleIndex, align 4
  %10 = load %class.btTransform*, %class.btTransform** %rayToTrans.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %10)
  %11 = load %class.btTransform*, %class.btTransform** %rayFromTrans.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %11)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rayDir, %class.btVector3* nonnull align 4 dereferenceable(16) %call8, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %normal, %class.btVector3* nonnull align 4 dereferenceable(16) %rayDir)
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %normal)
  %feature = getelementptr inbounds %"struct.btSoftBody::sRayCast", %"struct.btSoftBody::sRayCast"* %softResult, i32 0, i32 1
  %12 = load i32, i32* %feature, align 4
  %cmp11 = icmp eq i32 %12, 3
  br i1 %cmp11, label %if.then12, label %if.end18

if.then12:                                        ; preds = %if.then7
  %13 = load %class.btSoftBody*, %class.btSoftBody** %softBody, align 4
  %m_faces = getelementptr inbounds %class.btSoftBody, %class.btSoftBody* %13, i32 0, i32 11
  %index13 = getelementptr inbounds %"struct.btSoftBody::sRayCast", %"struct.btSoftBody::sRayCast"* %softResult, i32 0, i32 2
  %14 = load i32, i32* %index13, align 4
  %call14 = call nonnull align 4 dereferenceable(44) %"struct.btSoftBody::Face"* @_ZN20btAlignedObjectArrayIN10btSoftBody4FaceEEixEi(%class.btAlignedObjectArray.58* %m_faces, i32 %14)
  %m_normal = getelementptr inbounds %"struct.btSoftBody::Face", %"struct.btSoftBody::Face"* %call14, i32 0, i32 2
  %15 = bitcast %class.btVector3* %normal to i8*
  %16 = bitcast %class.btVector3* %m_normal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  %call15 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %normal, %class.btVector3* nonnull align 4 dereferenceable(16) %rayDir)
  %cmp16 = fcmp ogt float %call15, 0.000000e+00
  br i1 %cmp16, label %if.then17, label %if.end

if.then17:                                        ; preds = %if.then12
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %normal)
  %17 = bitcast %class.btVector3* %normal to i8*
  %18 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %17, i8* align 4 %18, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then17, %if.then12
  br label %if.end18

if.end18:                                         ; preds = %if.end, %if.then7
  %19 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4
  %fraction19 = getelementptr inbounds %"struct.btSoftBody::sRayCast", %"struct.btSoftBody::sRayCast"* %softResult, i32 0, i32 3
  %20 = load float, float* %fraction19, align 4
  %call20 = call %"struct.btCollisionWorld::LocalRayResult"* @_ZN16btCollisionWorld14LocalRayResultC2EPK17btCollisionObjectPNS_14LocalShapeInfoERK9btVector3f(%"struct.btCollisionWorld::LocalRayResult"* %rayResult, %class.btCollisionObject* %19, %"struct.btCollisionWorld::LocalShapeInfo"* %shapeInfo, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float %20)
  store i8 1, i8* %normalInWorldSpace, align 1
  %21 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4
  %22 = load i8, i8* %normalInWorldSpace, align 1
  %tobool21 = trunc i8 %22 to i1
  %23 = bitcast %"struct.btCollisionWorld::RayResultCallback"* %21 to float (%"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)***
  %vtable = load float (%"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)**, float (%"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)*** %23, align 4
  %vfn = getelementptr inbounds float (%"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)*, float (%"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)** %vtable, i64 3
  %24 = load float (%"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)*, float (%"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::LocalRayResult"*, i1)** %vfn, align 4
  %call22 = call float %24(%"struct.btCollisionWorld::RayResultCallback"* %21, %"struct.btCollisionWorld::LocalRayResult"* nonnull align 4 dereferenceable(28) %rayResult, i1 zeroext %tobool21)
  br label %if.end23

if.end23:                                         ; preds = %if.end18, %if.then6
  br label %if.end24

if.end24:                                         ; preds = %if.end23, %if.then2
  br label %if.end25

if.end25:                                         ; preds = %if.end24, %if.then
  br label %if.end26

if.else:                                          ; preds = %entry
  %25 = load %class.btTransform*, %class.btTransform** %rayFromTrans.addr, align 4
  %26 = load %class.btTransform*, %class.btTransform** %rayToTrans.addr, align 4
  %27 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4
  %28 = load %class.btCollisionShape*, %class.btCollisionShape** %collisionShape.addr, align 4
  %29 = load %class.btTransform*, %class.btTransform** %colObjWorldTransform.addr, align 4
  %30 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4
  call void @_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE(%class.btTransform* nonnull align 4 dereferenceable(64) %25, %class.btTransform* nonnull align 4 dereferenceable(64) %26, %class.btCollisionObject* %27, %class.btCollisionShape* %28, %class.btTransform* nonnull align 4 dereferenceable(64) %29, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(24) %30)
  br label %if.end26

if.end26:                                         ; preds = %if.else, %if.end25
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK16btCollisionShape10isSoftBodyEv(%class.btCollisionShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %call = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this1)
  %call2 = call zeroext i1 @_ZN17btBroadphaseProxy10isSoftBodyEi(i32 %call)
  ret i1 %call2
}

declare zeroext i1 @_ZN10btSoftBody7rayTestERK9btVector3S2_RNS_8sRayCastE(%class.btSoftBody*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %"struct.btSoftBody::sRayCast"* nonnull align 4 dereferenceable(16)) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(44) %"struct.btSoftBody::Face"* @_ZN20btAlignedObjectArrayIN10btSoftBody4FaceEEixEi(%class.btAlignedObjectArray.58* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.58*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.58* %this, %class.btAlignedObjectArray.58** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.58*, %class.btAlignedObjectArray.58** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.58, %class.btAlignedObjectArray.58* %this1, i32 0, i32 4
  %0 = load %"struct.btSoftBody::Face"*, %"struct.btSoftBody::Face"** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSoftBody::Face", %"struct.btSoftBody::Face"* %0, i32 %1
  ret %"struct.btSoftBody::Face"* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btCollisionWorld::LocalRayResult"* @_ZN16btCollisionWorld14LocalRayResultC2EPK17btCollisionObjectPNS_14LocalShapeInfoERK9btVector3f(%"struct.btCollisionWorld::LocalRayResult"* returned %this, %class.btCollisionObject* %collisionObject, %"struct.btCollisionWorld::LocalShapeInfo"* %localShapeInfo, %class.btVector3* nonnull align 4 dereferenceable(16) %hitNormalLocal, float %hitFraction) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btCollisionWorld::LocalRayResult"*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %localShapeInfo.addr = alloca %"struct.btCollisionWorld::LocalShapeInfo"*, align 4
  %hitNormalLocal.addr = alloca %class.btVector3*, align 4
  %hitFraction.addr = alloca float, align 4
  store %"struct.btCollisionWorld::LocalRayResult"* %this, %"struct.btCollisionWorld::LocalRayResult"** %this.addr, align 4
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4
  store %"struct.btCollisionWorld::LocalShapeInfo"* %localShapeInfo, %"struct.btCollisionWorld::LocalShapeInfo"** %localShapeInfo.addr, align 4
  store %class.btVector3* %hitNormalLocal, %class.btVector3** %hitNormalLocal.addr, align 4
  store float %hitFraction, float* %hitFraction.addr, align 4
  %this1 = load %"struct.btCollisionWorld::LocalRayResult"*, %"struct.btCollisionWorld::LocalRayResult"** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %this1, i32 0, i32 0
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4
  store %class.btCollisionObject* %0, %class.btCollisionObject** %m_collisionObject, align 4
  %m_localShapeInfo = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %this1, i32 0, i32 1
  %1 = load %"struct.btCollisionWorld::LocalShapeInfo"*, %"struct.btCollisionWorld::LocalShapeInfo"** %localShapeInfo.addr, align 4
  store %"struct.btCollisionWorld::LocalShapeInfo"* %1, %"struct.btCollisionWorld::LocalShapeInfo"** %m_localShapeInfo, align 4
  %m_hitNormalLocal = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %this1, i32 0, i32 2
  %2 = load %class.btVector3*, %class.btVector3** %hitNormalLocal.addr, align 4
  %3 = bitcast %class.btVector3* %m_hitNormalLocal to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %m_hitFraction = getelementptr inbounds %"struct.btCollisionWorld::LocalRayResult", %"struct.btCollisionWorld::LocalRayResult"* %this1, i32 0, i32 3
  %5 = load float, float* %hitFraction.addr, align 4
  store float %5, float* %m_hitFraction, align 4
  ret %"struct.btCollisionWorld::LocalRayResult"* %this1
}

declare void @_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE(%class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64), %class.btCollisionObject*, %class.btCollisionShape*, %class.btTransform* nonnull align 4 dereferenceable(64), %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(24)) #3

; Function Attrs: noinline optnone
define hidden void @_ZN24btSoftRigidDynamicsWorld19serializeSoftBodiesEP12btSerializer(%class.btSoftRigidDynamicsWorld* %this, %class.btSerializer* %serializer) #2 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %i = alloca i32, align 4
  %colObj = alloca %class.btCollisionObject*, align 4
  %len = alloca i32, align 4
  %chunk = alloca %class.btChunk*, align 4
  %structType = alloca i8*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %m_collisionObjects)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %m_collisionObjects2 = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %2, i32 0, i32 1
  %3 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %m_collisionObjects2, i32 %3)
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %call3, align 4
  store %class.btCollisionObject* %4, %class.btCollisionObject** %colObj, align 4
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %call4 = call i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %5)
  %and = and i32 %call4, 8
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %7 = bitcast %class.btCollisionObject* %6 to i32 (%class.btCollisionObject*)***
  %vtable = load i32 (%class.btCollisionObject*)**, i32 (%class.btCollisionObject*)*** %7, align 4
  %vfn = getelementptr inbounds i32 (%class.btCollisionObject*)*, i32 (%class.btCollisionObject*)** %vtable, i64 4
  %8 = load i32 (%class.btCollisionObject*)*, i32 (%class.btCollisionObject*)** %vfn, align 4
  %call5 = call i32 %8(%class.btCollisionObject* %6)
  store i32 %call5, i32* %len, align 4
  %9 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %10 = load i32, i32* %len, align 4
  %11 = bitcast %class.btSerializer* %9 to %class.btChunk* (%class.btSerializer*, i32, i32)***
  %vtable6 = load %class.btChunk* (%class.btSerializer*, i32, i32)**, %class.btChunk* (%class.btSerializer*, i32, i32)*** %11, align 4
  %vfn7 = getelementptr inbounds %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vtable6, i64 4
  %12 = load %class.btChunk* (%class.btSerializer*, i32, i32)*, %class.btChunk* (%class.btSerializer*, i32, i32)** %vfn7, align 4
  %call8 = call %class.btChunk* %12(%class.btSerializer* %9, i32 %10, i32 1)
  store %class.btChunk* %call8, %class.btChunk** %chunk, align 4
  %13 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %14 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %m_oldPtr = getelementptr inbounds %class.btChunk, %class.btChunk* %14, i32 0, i32 2
  %15 = load i8*, i8** %m_oldPtr, align 4
  %16 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %17 = bitcast %class.btCollisionObject* %13 to i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)***
  %vtable9 = load i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)**, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*** %17, align 4
  %vfn10 = getelementptr inbounds i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)** %vtable9, i64 5
  %18 = load i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)*, i8* (%class.btCollisionObject*, i8*, %class.btSerializer*)** %vfn10, align 4
  %call11 = call i8* %18(%class.btCollisionObject* %13, i8* %15, %class.btSerializer* %16)
  store i8* %call11, i8** %structType, align 4
  %19 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %20 = load %class.btChunk*, %class.btChunk** %chunk, align 4
  %21 = load i8*, i8** %structType, align 4
  %22 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %23 = bitcast %class.btCollisionObject* %22 to i8*
  %24 = bitcast %class.btSerializer* %19 to void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)***
  %vtable12 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)**, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*** %24, align 4
  %vfn13 = getelementptr inbounds void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vtable12, i64 5
  %25 = load void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)*, void (%class.btSerializer*, %class.btChunk*, i8*, i32, i8*)** %vfn13, align 4
  call void %25(%class.btSerializer* %19, %class.btChunk* %20, i8* %21, i32 1497645651, i8* %23)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %26 = load i32, i32* %i, align 4
  %inc = add nsw i32 %26, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %0, i32 %1
  ret %class.btCollisionObject** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject15getInternalTypeEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_internalType = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 24
  %0 = load i32, i32* %m_internalType, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN24btSoftRigidDynamicsWorld9serializeEP12btSerializer(%class.btSoftRigidDynamicsWorld* %this, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %0 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %1 = bitcast %class.btSerializer* %0 to void (%class.btSerializer*)***
  %vtable = load void (%class.btSerializer*)**, void (%class.btSerializer*)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btSerializer*)*, void (%class.btSerializer*)** %vtable, i64 8
  %2 = load void (%class.btSerializer*)*, void (%class.btSerializer*)** %vfn, align 4
  call void %2(%class.btSerializer* %0)
  %3 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %4 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld26serializeDynamicsWorldInfoEP12btSerializer(%class.btDiscreteDynamicsWorld* %3, %class.btSerializer* %4)
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  call void @_ZN24btSoftRigidDynamicsWorld19serializeSoftBodiesEP12btSerializer(%class.btSoftRigidDynamicsWorld* %this1, %class.btSerializer* %5)
  %6 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btDiscreteDynamicsWorld*
  %7 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld20serializeRigidBodiesEP12btSerializer(%class.btDiscreteDynamicsWorld* %6, %class.btSerializer* %7)
  %8 = bitcast %class.btSoftRigidDynamicsWorld* %this1 to %class.btCollisionWorld*
  %9 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  call void @_ZN16btCollisionWorld25serializeCollisionObjectsEP12btSerializer(%class.btCollisionWorld* %8, %class.btSerializer* %9)
  %10 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %11 = bitcast %class.btSerializer* %10 to void (%class.btSerializer*)***
  %vtable2 = load void (%class.btSerializer*)**, void (%class.btSerializer*)*** %11, align 4
  %vfn3 = getelementptr inbounds void (%class.btSerializer*)*, void (%class.btSerializer*)** %vtable2, i64 9
  %12 = load void (%class.btSerializer*)*, void (%class.btSerializer*)** %vfn3, align 4
  call void %12(%class.btSerializer* %10)
  ret void
}

declare void @_ZN23btDiscreteDynamicsWorld26serializeDynamicsWorldInfoEP12btSerializer(%class.btDiscreteDynamicsWorld*, %class.btSerializer*) #3

declare void @_ZN23btDiscreteDynamicsWorld20serializeRigidBodiesEP12btSerializer(%class.btDiscreteDynamicsWorld*, %class.btSerializer*) #3

declare void @_ZN16btCollisionWorld25serializeCollisionObjectsEP12btSerializer(%class.btCollisionWorld*, %class.btSerializer*) #3

declare void @_ZN16btCollisionWorld11updateAabbsEv(%class.btCollisionWorld*) unnamed_addr #3

declare void @_ZN16btCollisionWorld23computeOverlappingPairsEv(%class.btCollisionWorld*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw(%class.btCollisionWorld* %this, %class.btIDebugDraw* %debugDrawer) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  %debugDrawer.addr = alloca %class.btIDebugDraw*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  store %class.btIDebugDraw* %debugDrawer, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %debugDrawer.addr, align 4
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 5
  store %class.btIDebugDraw* %0, %class.btIDebugDraw** %m_debugDrawer, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btIDebugDraw* @_ZN16btCollisionWorld14getDebugDrawerEv(%class.btCollisionWorld* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_debugDrawer = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 5
  %0 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDrawer, align 4
  ret %class.btIDebugDraw* %0
}

declare void @_ZN16btCollisionWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3(%class.btCollisionWorld*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectii(%class.btDiscreteDynamicsWorld*, %class.btCollisionObject*, i32, i32) unnamed_addr #3

declare void @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv(%class.btCollisionWorld*) unnamed_addr #3

declare i32 @_ZN23btDiscreteDynamicsWorld14stepSimulationEfif(%class.btDiscreteDynamicsWorld*, float, i32, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb(%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*, i1 zeroext) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint(%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3(%class.btDiscreteDynamicsWorld*, %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZNK23btDiscreteDynamicsWorld10getGravityEv(%class.btVector3* sret align 4, %class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyii(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*, i32, i32) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody(%class.btDiscreteDynamicsWorld*, %class.btRigidBody*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver(%class.btDiscreteDynamicsWorld*, %class.btConstraintSolver*) unnamed_addr #3

declare %class.btConstraintSolver* @_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare i32 @_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare %class.btTypedConstraint* @_ZN23btDiscreteDynamicsWorld13getConstraintEi(%class.btDiscreteDynamicsWorld*, i32) unnamed_addr #3

declare %class.btTypedConstraint* @_ZNK23btDiscreteDynamicsWorld13getConstraintEi(%class.btDiscreteDynamicsWorld*, i32) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK24btSoftRigidDynamicsWorld12getWorldTypeEv(%class.btSoftRigidDynamicsWorld* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btSoftRigidDynamicsWorld*, align 4
  store %class.btSoftRigidDynamicsWorld* %this, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  %this1 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %this.addr, align 4
  ret i32 4
}

declare void @_ZN23btDiscreteDynamicsWorld11clearForcesEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface(%class.btDiscreteDynamicsWorld*, %class.btActionInterface*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld19integrateTransformsEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo(%class.btDiscreteDynamicsWorld*, %struct.btContactSolverInfo* nonnull align 4 dereferenceable(88)) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld21updateActivationStateEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld24createPredictiveContactsEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf(%class.btDiscreteDynamicsWorld*, float) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint(%class.btDiscreteDynamicsWorld*, %class.btTypedConstraint*) unnamed_addr #3

declare void @_ZN23btDiscreteDynamicsWorld12applyGravityEv(%class.btDiscreteDynamicsWorld*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorld11setNumTasksEi(%class.btDiscreteDynamicsWorld* %this, i32 %numTasks) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %numTasks.addr = alloca i32, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store i32 %numTasks, i32* %numTasks.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf(%class.btDiscreteDynamicsWorld* %this, float %timeStep) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btDiscreteDynamicsWorld*, align 4
  %timeStep.addr = alloca float, align 4
  store %class.btDiscreteDynamicsWorld* %this, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  store float %timeStep, float* %timeStep.addr, align 4
  %this1 = load %class.btDiscreteDynamicsWorld*, %class.btDiscreteDynamicsWorld** %this.addr, align 4
  %0 = load float, float* %timeStep.addr, align 4
  call void @_ZN23btDiscreteDynamicsWorld13updateActionsEf(%class.btDiscreteDynamicsWorld* %this1, float %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btSparseSdf* @_ZN11btSparseSdfILi3EEC2Ev(%struct.btSparseSdf* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btSparseSdf*, align 4
  store %struct.btSparseSdf* %this, %struct.btSparseSdf** %this.addr, align 4
  %this1 = load %struct.btSparseSdf*, %struct.btSparseSdf** %this.addr, align 4
  %cells = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.41* @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEC2Ev(%class.btAlignedObjectArray.41* %cells)
  ret %struct.btSparseSdf* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.41* @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEC2Ev(%class.btAlignedObjectArray.41* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.42* @_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EEC2Ev(%class.btAlignedAllocator.42* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4initEv(%class.btAlignedObjectArray.41* %this1)
  ret %class.btAlignedObjectArray.41* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.42* @_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EEC2Ev(%class.btAlignedAllocator.42* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.42*, align 4
  store %class.btAlignedAllocator.42* %this, %class.btAlignedAllocator.42** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.42*, %class.btAlignedAllocator.42** %this.addr, align 4
  ret %class.btAlignedAllocator.42* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4initEv(%class.btAlignedObjectArray.41* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 4
  store %"struct.btSparseSdf<3>::Cell"** null, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btSparseSdf* @_ZN11btSparseSdfILi3EED2Ev(%struct.btSparseSdf* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btSparseSdf*, align 4
  store %struct.btSparseSdf* %this, %struct.btSparseSdf** %this.addr, align 4
  %this1 = load %struct.btSparseSdf*, %struct.btSparseSdf** %this.addr, align 4
  %cells = getelementptr inbounds %struct.btSparseSdf, %struct.btSparseSdf* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.41* @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEED2Ev(%class.btAlignedObjectArray.41* %cells) #10
  ret %struct.btSparseSdf* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.41* @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEED2Ev(%class.btAlignedObjectArray.41* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE5clearEv(%class.btAlignedObjectArray.41* %this1)
  ret %class.btAlignedObjectArray.41* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE5clearEv(%class.btAlignedObjectArray.41* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.41* %this1)
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7destroyEii(%class.btAlignedObjectArray.41* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE10deallocateEv(%class.btAlignedObjectArray.41* %this1)
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4initEv(%class.btAlignedObjectArray.41* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7destroyEii(%class.btAlignedObjectArray.41* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 4
  %3 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.41* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE10deallocateEv(%class.btAlignedObjectArray.41* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 4
  %0 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4
  %tobool = icmp ne %"struct.btSparseSdf<3>::Cell"** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 4
  %2 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EE10deallocateEPS3_(%class.btAlignedAllocator.42* %m_allocator, %"struct.btSparseSdf<3>::Cell"** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 4
  store %"struct.btSparseSdf<3>::Cell"** null, %"struct.btSparseSdf<3>::Cell"*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EE10deallocateEPS3_(%class.btAlignedAllocator.42* %this, %"struct.btSparseSdf<3>::Cell"** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.42*, align 4
  %ptr.addr = alloca %"struct.btSparseSdf<3>::Cell"**, align 4
  store %class.btAlignedAllocator.42* %this, %class.btAlignedAllocator.42** %this.addr, align 4
  store %"struct.btSparseSdf<3>::Cell"** %ptr, %"struct.btSparseSdf<3>::Cell"*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.42*, %class.btAlignedAllocator.42** %this.addr, align 4
  %0 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %ptr.addr, align 4
  %1 = bitcast %"struct.btSparseSdf<3>::Cell"** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btBroadphaseRayCallback* @_ZN23btBroadphaseRayCallbackC2Ev(%struct.btBroadphaseRayCallback* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseRayCallback*, align 4
  store %struct.btBroadphaseRayCallback* %this, %struct.btBroadphaseRayCallback** %this.addr, align 4
  %this1 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %this.addr, align 4
  %0 = bitcast %struct.btBroadphaseRayCallback* %this1 to %struct.btBroadphaseAabbCallback*
  %call = call %struct.btBroadphaseAabbCallback* @_ZN24btBroadphaseAabbCallbackC2Ev(%struct.btBroadphaseAabbCallback* %0) #10
  %1 = bitcast %struct.btBroadphaseRayCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV23btBroadphaseRayCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_rayDirectionInverse = getelementptr inbounds %struct.btBroadphaseRayCallback, %struct.btBroadphaseRayCallback* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_rayDirectionInverse)
  ret %struct.btBroadphaseRayCallback* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btTransform11setIdentityEv(%class.btTransform* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  call void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_origin, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btTransform9setOriginERK9btVector3(%class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %origin) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %origin.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %origin, %class.btVector3** %origin.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %origin.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = bitcast %class.btVector3* %m_origin to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btSoftSingleRayCallbackD0Ev(%struct.btSoftSingleRayCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btSoftSingleRayCallback*, align 4
  store %struct.btSoftSingleRayCallback* %this, %struct.btSoftSingleRayCallback** %this.addr, align 4
  %this1 = load %struct.btSoftSingleRayCallback*, %struct.btSoftSingleRayCallback** %this.addr, align 4
  %call = call %struct.btSoftSingleRayCallback* @_ZN23btSoftSingleRayCallbackD2Ev(%struct.btSoftSingleRayCallback* %this1) #10
  %0 = bitcast %struct.btSoftSingleRayCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZN23btSoftSingleRayCallback7processEPK17btBroadphaseProxy(%struct.btSoftSingleRayCallback* %this, %struct.btBroadphaseProxy* %proxy) unnamed_addr #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %struct.btSoftSingleRayCallback*, align 4
  %proxy.addr = alloca %struct.btBroadphaseProxy*, align 4
  %collisionObject = alloca %class.btCollisionObject*, align 4
  store %struct.btSoftSingleRayCallback* %this, %struct.btSoftSingleRayCallback** %this.addr, align 4
  store %struct.btBroadphaseProxy* %proxy, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %this1 = load %struct.btSoftSingleRayCallback*, %struct.btSoftSingleRayCallback** %this.addr, align 4
  %m_resultCallback = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 7
  %0 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %m_resultCallback, align 4
  %m_closestHitFraction = getelementptr inbounds %"struct.btCollisionWorld::RayResultCallback", %"struct.btCollisionWorld::RayResultCallback"* %0, i32 0, i32 1
  %1 = load float, float* %m_closestHitFraction, align 4
  %cmp = fcmp oeq float %1, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  %2 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %proxy.addr, align 4
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %2, i32 0, i32 0
  %3 = load i8*, i8** %m_clientObject, align 4
  %4 = bitcast i8* %3 to %class.btCollisionObject*
  store %class.btCollisionObject* %4, %class.btCollisionObject** %collisionObject, align 4
  %m_resultCallback2 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 7
  %5 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %m_resultCallback2, align 4
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4
  %call = call %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %6)
  %7 = bitcast %"struct.btCollisionWorld::RayResultCallback"* %5 to i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)***
  %vtable = load i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)**, i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)*** %7, align 4
  %vfn = getelementptr inbounds i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)*, i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)** %vtable, i64 2
  %8 = load i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)*, i1 (%"struct.btCollisionWorld::RayResultCallback"*, %struct.btBroadphaseProxy*)** %vfn, align 4
  %call3 = call zeroext i1 %8(%"struct.btCollisionWorld::RayResultCallback"* %5, %struct.btBroadphaseProxy* %call)
  br i1 %call3, label %if.then4, label %if.end8

if.then4:                                         ; preds = %if.end
  %m_world = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 6
  %9 = load %class.btSoftRigidDynamicsWorld*, %class.btSoftRigidDynamicsWorld** %m_world, align 4
  %m_rayFromTrans = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 3
  %m_rayToTrans = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 4
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4
  %11 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4
  %call5 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %11)
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject, align 4
  %call6 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %12)
  %m_resultCallback7 = getelementptr inbounds %struct.btSoftSingleRayCallback, %struct.btSoftSingleRayCallback* %this1, i32 0, i32 7
  %13 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %m_resultCallback7, align 4
  call void @_ZN24btSoftRigidDynamicsWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RN16btCollisionWorld17RayResultCallbackE(%class.btTransform* nonnull align 4 dereferenceable(64) %m_rayFromTrans, %class.btTransform* nonnull align 4 dereferenceable(64) %m_rayToTrans, %class.btCollisionObject* %10, %class.btCollisionShape* %call5, %class.btTransform* nonnull align 4 dereferenceable(64) %call6, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(24) %13)
  br label %if.end8

if.end8:                                          ; preds = %if.then4, %if.end
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end8, %if.then
  %14 = load i1, i1* %retval, align 1
  ret i1 %14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphaseAabbCallback* @_ZN24btBroadphaseAabbCallbackC2Ev(%struct.btBroadphaseAabbCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseAabbCallback*, align 4
  store %struct.btBroadphaseAabbCallback* %this, %struct.btBroadphaseAabbCallback** %this.addr, align 4
  %this1 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %this.addr, align 4
  %0 = bitcast %struct.btBroadphaseAabbCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV24btBroadphaseAabbCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %struct.btBroadphaseAabbCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphaseRayCallback* @_ZN23btBroadphaseRayCallbackD2Ev(%struct.btBroadphaseRayCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseRayCallback*, align 4
  store %struct.btBroadphaseRayCallback* %this, %struct.btBroadphaseRayCallback** %this.addr, align 4
  %this1 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %this.addr, align 4
  %0 = bitcast %struct.btBroadphaseRayCallback* %this1 to %struct.btBroadphaseAabbCallback*
  %call = call %struct.btBroadphaseAabbCallback* @_ZN24btBroadphaseAabbCallbackD2Ev(%struct.btBroadphaseAabbCallback* %0) #10
  ret %struct.btBroadphaseRayCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btBroadphaseRayCallbackD0Ev(%struct.btBroadphaseRayCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseRayCallback*, align 4
  store %struct.btBroadphaseRayCallback* %this, %struct.btBroadphaseRayCallback** %this.addr, align 4
  %this1 = load %struct.btBroadphaseRayCallback*, %struct.btBroadphaseRayCallback** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphaseAabbCallback* @_ZN24btBroadphaseAabbCallbackD2Ev(%struct.btBroadphaseAabbCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseAabbCallback*, align 4
  store %struct.btBroadphaseAabbCallback* %this, %struct.btBroadphaseAabbCallback** %this.addr, align 4
  %this1 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %this.addr, align 4
  ret %struct.btBroadphaseAabbCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN24btBroadphaseAabbCallbackD0Ev(%struct.btBroadphaseAabbCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btBroadphaseAabbCallback*, align 4
  store %struct.btBroadphaseAabbCallback* %this, %struct.btBroadphaseAabbCallback** %this.addr, align 4
  %this1 = load %struct.btBroadphaseAabbCallback*, %struct.btBroadphaseAabbCallback** %this.addr, align 4
  call void @llvm.trap() #11
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #6

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x311setIdentityEv(%class.btMatrix3x3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 0.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 0.000000e+00, float* %ref.tmp8, align 4
  store float 1.000000e+00, float* %ref.tmp9, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btBroadphaseProxy* @_ZN17btCollisionObject19getBroadphaseHandleEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_broadphaseHandle = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 8
  %0 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_broadphaseHandle, align 4
  ret %struct.btBroadphaseProxy* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4
  ret %class.btCollisionShape* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN17btBroadphaseProxy10isSoftBodyEi(i32 %proxyType) #1 comdat {
entry:
  %proxyType.addr = alloca i32, align 4
  store i32 %proxyType, i32* %proxyType.addr, align 4
  %0 = load i32, i32* %proxyType.addr, align 4
  %cmp = icmp eq i32 %0, 32
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #8

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

declare void @_ZN23btDiscreteDynamicsWorld13updateActionsEf(%class.btDiscreteDynamicsWorld*, float) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.23* @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EEC2Ev(%class.btAlignedAllocator.23* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.23*, align 4
  store %class.btAlignedAllocator.23* %this, %class.btAlignedAllocator.23** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.23*, %class.btAlignedAllocator.23** %this.addr, align 4
  ret %class.btAlignedAllocator.23* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv(%class.btAlignedObjectArray.22* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store %class.btSoftBody** null, %class.btSoftBody*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE5clearEv(%class.btAlignedObjectArray.22* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4initEv(%class.btAlignedObjectArray.22* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii(%class.btAlignedObjectArray.22* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %3 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv(%class.btAlignedObjectArray.22* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %0 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4
  %tobool = icmp ne %class.btSoftBody** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %2 = load %class.btSoftBody**, %class.btSoftBody*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE10deallocateEPS1_(%class.btAlignedAllocator.23* %m_allocator, %class.btSoftBody** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store %class.btSoftBody** null, %class.btSoftBody*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE10deallocateEPS1_(%class.btAlignedAllocator.23* %this, %class.btSoftBody** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.23*, align 4
  %ptr.addr = alloca %class.btSoftBody**, align 4
  store %class.btAlignedAllocator.23* %this, %class.btAlignedAllocator.23** %this.addr, align 4
  store %class.btSoftBody** %ptr, %class.btSoftBody*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.23*, %class.btAlignedAllocator.23** %this.addr, align 4
  %0 = load %class.btSoftBody**, %class.btSoftBody*** %ptr.addr, align 4
  %1 = bitcast %class.btSoftBody** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE6resizeEiRKS3_(%class.btAlignedObjectArray.41* %this, i32 %newsize, %"struct.btSparseSdf<3>::Cell"** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %"struct.btSparseSdf<3>::Cell"**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %"struct.btSparseSdf<3>::Cell"** %fillData, %"struct.btSparseSdf<3>::Cell"*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.41* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 4
  %5 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7reserveEi(%class.btAlignedObjectArray.41* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 4
  %14 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %14, i32 %15
  %16 = bitcast %"struct.btSparseSdf<3>::Cell"** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %"struct.btSparseSdf<3>::Cell"**
  %18 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %fillData.addr, align 4
  %19 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %18, align 4
  store %"struct.btSparseSdf<3>::Cell"* %19, %"struct.btSparseSdf<3>::Cell"** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7reserveEi(%class.btAlignedObjectArray.41* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"struct.btSparseSdf<3>::Cell"**, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE8capacityEv(%class.btAlignedObjectArray.41* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE8allocateEi(%class.btAlignedObjectArray.41* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %"struct.btSparseSdf<3>::Cell"**
  store %"struct.btSparseSdf<3>::Cell"** %2, %"struct.btSparseSdf<3>::Cell"*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.41* %this1)
  %3 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4copyEiiPS3_(%class.btAlignedObjectArray.41* %this1, i32 0, i32 %call3, %"struct.btSparseSdf<3>::Cell"** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4sizeEv(%class.btAlignedObjectArray.41* %this1)
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE7destroyEii(%class.btAlignedObjectArray.41* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE10deallocateEv(%class.btAlignedObjectArray.41* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 4
  store %"struct.btSparseSdf<3>::Cell"** %4, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE8capacityEv(%class.btAlignedObjectArray.41* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE8allocateEi(%class.btAlignedObjectArray.41* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %"struct.btSparseSdf<3>::Cell"** @_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EE8allocateEiPPKS3_(%class.btAlignedAllocator.42* %m_allocator, i32 %1, %"struct.btSparseSdf<3>::Cell"*** null)
  %2 = bitcast %"struct.btSparseSdf<3>::Cell"** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEE4copyEiiPS3_(%class.btAlignedObjectArray.41* %this, i32 %start, i32 %end, %"struct.btSparseSdf<3>::Cell"** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"struct.btSparseSdf<3>::Cell"**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %"struct.btSparseSdf<3>::Cell"** %dest, %"struct.btSparseSdf<3>::Cell"*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %3, i32 %4
  %5 = bitcast %"struct.btSparseSdf<3>::Cell"** %arrayidx to i8*
  %6 = bitcast i8* %5 to %"struct.btSparseSdf<3>::Cell"**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 4
  %7 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %7, i32 %8
  %9 = load %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %arrayidx2, align 4
  store %"struct.btSparseSdf<3>::Cell"* %9, %"struct.btSparseSdf<3>::Cell"** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btSparseSdf<3>::Cell"** @_ZN18btAlignedAllocatorIPN11btSparseSdfILi3EE4CellELj16EE8allocateEiPPKS3_(%class.btAlignedAllocator.42* %this, i32 %n, %"struct.btSparseSdf<3>::Cell"*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.42*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"struct.btSparseSdf<3>::Cell"***, align 4
  store %class.btAlignedAllocator.42* %this, %class.btAlignedAllocator.42** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %"struct.btSparseSdf<3>::Cell"*** %hint, %"struct.btSparseSdf<3>::Cell"**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.42*, %class.btAlignedAllocator.42** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"struct.btSparseSdf<3>::Cell"**
  ret %"struct.btSparseSdf<3>::Cell"** %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.btSparseSdf<3>::Cell"** @_ZN20btAlignedObjectArrayIPN11btSparseSdfILi3EE4CellEEixEi(%class.btAlignedObjectArray.41* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.41*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.41* %this, %class.btAlignedObjectArray.41** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.41*, %class.btAlignedObjectArray.41** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.41, %class.btAlignedObjectArray.41* %this1, i32 0, i32 4
  %0 = load %"struct.btSparseSdf<3>::Cell"**, %"struct.btSparseSdf<3>::Cell"*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSparseSdf<3>::Cell"*, %"struct.btSparseSdf<3>::Cell"** %0, i32 %1
  ret %"struct.btSparseSdf<3>::Cell"** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE8capacityEv(%class.btAlignedObjectArray.22* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7reserveEi(%class.btAlignedObjectArray.22* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btSoftBody**, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE8capacityEv(%class.btAlignedObjectArray.22* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP10btSoftBodyE8allocateEi(%class.btAlignedObjectArray.22* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btSoftBody**
  store %class.btSoftBody** %2, %class.btSoftBody*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  %3 = load %class.btSoftBody**, %class.btSoftBody*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call3, %class.btSoftBody** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE7destroyEii(%class.btAlignedObjectArray.22* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE10deallocateEv(%class.btAlignedObjectArray.22* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btSoftBody**, %class.btSoftBody*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  store %class.btSoftBody** %4, %class.btSoftBody*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP10btSoftBodyE9allocSizeEi(%class.btAlignedObjectArray.22* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP10btSoftBodyE8allocateEi(%class.btAlignedObjectArray.22* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btSoftBody** @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.23* %m_allocator, i32 %1, %class.btSoftBody*** null)
  %2 = bitcast %class.btSoftBody** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4copyEiiPS1_(%class.btAlignedObjectArray.22* %this, i32 %start, i32 %end, %class.btSoftBody** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btSoftBody**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btSoftBody** %dest, %class.btSoftBody*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btSoftBody**, %class.btSoftBody*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %3, i32 %4
  %5 = bitcast %class.btSoftBody** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btSoftBody**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %7 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %7, i32 %8
  %9 = load %class.btSoftBody*, %class.btSoftBody** %arrayidx2, align 4
  store %class.btSoftBody* %9, %class.btSoftBody** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btSoftBody** @_ZN18btAlignedAllocatorIP10btSoftBodyLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.23* %this, i32 %n, %class.btSoftBody*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.23*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btSoftBody***, align 4
  store %class.btAlignedAllocator.23* %this, %class.btAlignedAllocator.23** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btSoftBody*** %hint, %class.btSoftBody**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.23*, %class.btAlignedAllocator.23** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btSoftBody**
  ret %class.btSoftBody** %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE16findLinearSearchERKS1_(%class.btAlignedObjectArray.22* %this, %class.btSoftBody** nonnull align 4 dereferenceable(4) %key) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %key.addr = alloca %class.btSoftBody**, align 4
  %index = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store %class.btSoftBody** %key, %class.btSoftBody*** %key.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  store i32 %call, i32* %index, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  %cmp = icmp slt i32 %0, %call2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %1 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %1, i32 %2
  %3 = load %class.btSoftBody*, %class.btSoftBody** %arrayidx, align 4
  %4 = load %class.btSoftBody**, %class.btSoftBody*** %key.addr, align 4
  %5 = load %class.btSoftBody*, %class.btSoftBody** %4, align 4
  %cmp3 = icmp eq %class.btSoftBody* %3, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  store i32 %6, i32* %index, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %8 = load i32, i32* %index, align 4
  ret i32 %8
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE13removeAtIndexEi(%class.btAlignedObjectArray.22* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %index.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %0 = load i32, i32* %index.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %index.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP10btSoftBodyE4sizeEv(%class.btAlignedObjectArray.22* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4swapEii(%class.btAlignedObjectArray.22* %this1, i32 %1, i32 %sub)
  call void @_ZN20btAlignedObjectArrayIP10btSoftBodyE8pop_backEv(%class.btAlignedObjectArray.22* %this1)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE4swapEii(%class.btAlignedObjectArray.22* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btSoftBody*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %0 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %0, i32 %1
  %2 = load %class.btSoftBody*, %class.btSoftBody** %arrayidx, align 4
  store %class.btSoftBody* %2, %class.btSoftBody** %temp, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %3 = load %class.btSoftBody**, %class.btSoftBody*** %m_data2, align 4
  %4 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %3, i32 %4
  %5 = load %class.btSoftBody*, %class.btSoftBody** %arrayidx3, align 4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %6 = load %class.btSoftBody**, %class.btSoftBody*** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %6, i32 %7
  store %class.btSoftBody* %5, %class.btSoftBody** %arrayidx5, align 4
  %8 = load %class.btSoftBody*, %class.btSoftBody** %temp, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %9 = load %class.btSoftBody**, %class.btSoftBody*** %m_data6, align 4
  %10 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %9, i32 %10
  store %class.btSoftBody* %8, %class.btSoftBody** %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP10btSoftBodyE8pop_backEv(%class.btAlignedObjectArray.22* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.22*, align 4
  store %class.btAlignedObjectArray.22* %this, %class.btAlignedObjectArray.22** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.22*, %class.btAlignedObjectArray.22** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 4
  %1 = load %class.btSoftBody**, %class.btSoftBody*** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.22, %class.btAlignedObjectArray.22* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %class.btSoftBody*, %class.btSoftBody** %1, i32 %2
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btSoftRigidDynamicsWorld.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { cold noreturn nounwind }
attributes #7 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind readnone speculatable willreturn }
attributes #9 = { builtin nounwind }
attributes #10 = { nounwind }
attributes #11 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
