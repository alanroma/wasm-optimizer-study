; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btSphereTriangleCollisionAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btSphereTriangleCollisionAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btSphereTriangleCollisionAlgorithm = type <{ %class.btActivatingCollisionAlgorithm, i8, [3 x i8], %class.btPersistentManifold*, i8, [3 x i8] }>
%class.btActivatingCollisionAlgorithm = type { %class.btCollisionAlgorithm }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.0, %union.anon.1, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.0 = type { float }
%union.anon.1 = type { float }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type opaque
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32, float }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%class.btSphereShape = type { %class.btConvexInternalShape }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btTriangleShape = type opaque
%struct.SphereTriangleDetector = type { %struct.btDiscreteCollisionDetectorInterface, %class.btSphereShape*, %class.btTriangleShape*, float }
%struct.btDiscreteCollisionDetectorInterface = type { i32 (...)** }
%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput" = type { %class.btTransform, %class.btTransform, float }
%class.btAlignedObjectArray.2 = type <{ %class.btAlignedAllocator.3, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.3 = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZNK24btCollisionObjectWrapper17getCollisionShapeEv = comdat any

$_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold = comdat any

$_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev = comdat any

$_ZNK24btCollisionObjectWrapper17getWorldTransformEv = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN16btManifoldResult20refreshContactPointsEv = comdat any

$_ZN22SphereTriangleDetectorD2Ev = comdat any

$_ZN34btSphereTriangleCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZN36btDiscreteCollisionDetectorInterfaceD2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV34btSphereTriangleCollisionAlgorithm = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI34btSphereTriangleCollisionAlgorithm to i8*), i8* bitcast (%class.btSphereTriangleCollisionAlgorithm* (%class.btSphereTriangleCollisionAlgorithm*)* @_ZN34btSphereTriangleCollisionAlgorithmD1Ev to i8*), i8* bitcast (void (%class.btSphereTriangleCollisionAlgorithm*)* @_ZN34btSphereTriangleCollisionAlgorithmD0Ev to i8*), i8* bitcast (void (%class.btSphereTriangleCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN34btSphereTriangleCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (float (%class.btSphereTriangleCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN34btSphereTriangleCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (void (%class.btSphereTriangleCollisionAlgorithm*, %class.btAlignedObjectArray.2*)* @_ZN34btSphereTriangleCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS34btSphereTriangleCollisionAlgorithm = hidden constant [37 x i8] c"34btSphereTriangleCollisionAlgorithm\00", align 1
@_ZTI30btActivatingCollisionAlgorithm = external constant i8*
@_ZTI34btSphereTriangleCollisionAlgorithm = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([37 x i8], [37 x i8]* @_ZTS34btSphereTriangleCollisionAlgorithm, i32 0, i32 0), i8* bitcast (i8** @_ZTI30btActivatingCollisionAlgorithm to i8*) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btSphereTriangleCollisionAlgorithm.cpp, i8* null }]

@_ZN34btSphereTriangleCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b = hidden unnamed_addr alias %class.btSphereTriangleCollisionAlgorithm* (%class.btSphereTriangleCollisionAlgorithm*, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1), %class.btSphereTriangleCollisionAlgorithm* (%class.btSphereTriangleCollisionAlgorithm*, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i1)* @_ZN34btSphereTriangleCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b
@_ZN34btSphereTriangleCollisionAlgorithmD1Ev = hidden unnamed_addr alias %class.btSphereTriangleCollisionAlgorithm* (%class.btSphereTriangleCollisionAlgorithm*), %class.btSphereTriangleCollisionAlgorithm* (%class.btSphereTriangleCollisionAlgorithm*)* @_ZN34btSphereTriangleCollisionAlgorithmD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btSphereTriangleCollisionAlgorithm* @_ZN34btSphereTriangleCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_b(%class.btSphereTriangleCollisionAlgorithm* returned %this, %class.btPersistentManifold* %mf, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, i1 zeroext %swapped) unnamed_addr #2 {
entry:
  %retval = alloca %class.btSphereTriangleCollisionAlgorithm*, align 4
  %this.addr = alloca %class.btSphereTriangleCollisionAlgorithm*, align 4
  %mf.addr = alloca %class.btPersistentManifold*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %swapped.addr = alloca i8, align 1
  store %class.btSphereTriangleCollisionAlgorithm* %this, %class.btSphereTriangleCollisionAlgorithm** %this.addr, align 4
  store %class.btPersistentManifold* %mf, %class.btPersistentManifold** %mf.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %frombool = zext i1 %swapped to i8
  store i8 %frombool, i8* %swapped.addr, align 1
  %this1 = load %class.btSphereTriangleCollisionAlgorithm*, %class.btSphereTriangleCollisionAlgorithm** %this.addr, align 4
  store %class.btSphereTriangleCollisionAlgorithm* %this1, %class.btSphereTriangleCollisionAlgorithm** %retval, align 4
  %0 = bitcast %class.btSphereTriangleCollisionAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* %0, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %1, %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper* %3)
  %4 = bitcast %class.btSphereTriangleCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV34btSphereTriangleCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4
  %m_ownManifold = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 1
  store i8 0, i8* %m_ownManifold, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 3
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %mf.addr, align 4
  store %class.btPersistentManifold* %5, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %m_swapped = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 4
  %6 = load i8, i8* %swapped.addr, align 1
  %tobool = trunc i8 %6 to i1
  %frombool2 = zext i1 %tobool to i8
  store i8 %frombool2, i8* %m_swapped, align 4
  %m_manifoldPtr3 = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 3
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr3, align 4
  %tobool4 = icmp ne %class.btPersistentManifold* %7, null
  br i1 %tobool4, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %8 = bitcast %class.btSphereTriangleCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %8, i32 0, i32 1
  %9 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call5 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %10)
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call6 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %11)
  %12 = bitcast %class.btDispatcher* %9 to %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable = load %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)**, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*** %12, align 4
  %vfn = getelementptr inbounds %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable, i64 3
  %13 = load %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn, align 4
  %call7 = call %class.btPersistentManifold* %13(%class.btDispatcher* %9, %class.btCollisionObject* %call5, %class.btCollisionObject* %call6)
  %m_manifoldPtr8 = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 3
  store %class.btPersistentManifold* %call7, %class.btPersistentManifold** %m_manifoldPtr8, align 4
  %m_ownManifold9 = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 1
  store i8 1, i8* %m_ownManifold9, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %14 = load %class.btSphereTriangleCollisionAlgorithm*, %class.btSphereTriangleCollisionAlgorithm** %retval, align 4
  ret %class.btSphereTriangleCollisionAlgorithm* %14
}

declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btSphereTriangleCollisionAlgorithm* @_ZN34btSphereTriangleCollisionAlgorithmD2Ev(%class.btSphereTriangleCollisionAlgorithm* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btSphereTriangleCollisionAlgorithm*, align 4
  %this.addr = alloca %class.btSphereTriangleCollisionAlgorithm*, align 4
  store %class.btSphereTriangleCollisionAlgorithm* %this, %class.btSphereTriangleCollisionAlgorithm** %this.addr, align 4
  %this1 = load %class.btSphereTriangleCollisionAlgorithm*, %class.btSphereTriangleCollisionAlgorithm** %this.addr, align 4
  store %class.btSphereTriangleCollisionAlgorithm* %this1, %class.btSphereTriangleCollisionAlgorithm** %retval, align 4
  %0 = bitcast %class.btSphereTriangleCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV34btSphereTriangleCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_ownManifold = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 1
  %1 = load i8, i8* %m_ownManifold, align 4
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %m_manifoldPtr = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 3
  %2 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %tobool2 = icmp ne %class.btPersistentManifold* %2, null
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %3 = bitcast %class.btSphereTriangleCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %3, i32 0, i32 1
  %4 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %m_manifoldPtr4 = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 3
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr4, align 4
  %6 = bitcast %class.btDispatcher* %4 to void (%class.btDispatcher*, %class.btPersistentManifold*)***
  %vtable = load void (%class.btDispatcher*, %class.btPersistentManifold*)**, void (%class.btDispatcher*, %class.btPersistentManifold*)*** %6, align 4
  %vfn = getelementptr inbounds void (%class.btDispatcher*, %class.btPersistentManifold*)*, void (%class.btDispatcher*, %class.btPersistentManifold*)** %vtable, i64 4
  %7 = load void (%class.btDispatcher*, %class.btPersistentManifold*)*, void (%class.btDispatcher*, %class.btPersistentManifold*)** %vfn, align 4
  call void %7(%class.btDispatcher* %4, %class.btPersistentManifold* %5)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  br label %if.end5

if.end5:                                          ; preds = %if.end, %entry
  %8 = bitcast %class.btSphereTriangleCollisionAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %call = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* %8) #7
  %9 = load %class.btSphereTriangleCollisionAlgorithm*, %class.btSphereTriangleCollisionAlgorithm** %retval, align 4
  ret %class.btSphereTriangleCollisionAlgorithm* %9
}

; Function Attrs: nounwind
declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN34btSphereTriangleCollisionAlgorithmD0Ev(%class.btSphereTriangleCollisionAlgorithm* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSphereTriangleCollisionAlgorithm*, align 4
  store %class.btSphereTriangleCollisionAlgorithm* %this, %class.btSphereTriangleCollisionAlgorithm** %this.addr, align 4
  %this1 = load %class.btSphereTriangleCollisionAlgorithm*, %class.btSphereTriangleCollisionAlgorithm** %this.addr, align 4
  %call = call %class.btSphereTriangleCollisionAlgorithm* @_ZN34btSphereTriangleCollisionAlgorithmD1Ev(%class.btSphereTriangleCollisionAlgorithm* %this1) #7
  %0 = bitcast %class.btSphereTriangleCollisionAlgorithm* %this1 to i8*
  call void @_ZdlPv(i8* %0) #8
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline optnone
define hidden void @_ZN34btSphereTriangleCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btSphereTriangleCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %col0Wrap, %struct.btCollisionObjectWrapper* %col1Wrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSphereTriangleCollisionAlgorithm*, align 4
  %col0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %col1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %sphereObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %triObjWrap = alloca %struct.btCollisionObjectWrapper*, align 4
  %sphere = alloca %class.btSphereShape*, align 4
  %triangle = alloca %class.btTriangleShape*, align 4
  %detector = alloca %struct.SphereTriangleDetector, align 4
  %input = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", align 4
  %swapResults = alloca i8, align 1
  store %class.btSphereTriangleCollisionAlgorithm* %this, %class.btSphereTriangleCollisionAlgorithm** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %col0Wrap, %struct.btCollisionObjectWrapper** %col0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %col1Wrap, %struct.btCollisionObjectWrapper** %col1Wrap.addr, align 4
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4
  %this1 = load %class.btSphereTriangleCollisionAlgorithm*, %class.btSphereTriangleCollisionAlgorithm** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 3
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %tobool = icmp ne %class.btPersistentManifold* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %m_swapped = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 4
  %1 = load i8, i8* %m_swapped, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1Wrap.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %if.end
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0Wrap.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %struct.btCollisionObjectWrapper* [ %2, %cond.true ], [ %3, %cond.false ]
  store %struct.btCollisionObjectWrapper* %cond, %struct.btCollisionObjectWrapper** %sphereObjWrap, align 4
  %m_swapped3 = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 4
  %4 = load i8, i8* %m_swapped3, align 4
  %tobool4 = trunc i8 %4 to i1
  br i1 %tobool4, label %cond.true5, label %cond.false6

cond.true5:                                       ; preds = %cond.end
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col0Wrap.addr, align 4
  br label %cond.end7

cond.false6:                                      ; preds = %cond.end
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %col1Wrap.addr, align 4
  br label %cond.end7

cond.end7:                                        ; preds = %cond.false6, %cond.true5
  %cond8 = phi %struct.btCollisionObjectWrapper* [ %5, %cond.true5 ], [ %6, %cond.false6 ]
  store %struct.btCollisionObjectWrapper* %cond8, %struct.btCollisionObjectWrapper** %triObjWrap, align 4
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %sphereObjWrap, align 4
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %7)
  %8 = bitcast %class.btCollisionShape* %call to %class.btSphereShape*
  store %class.btSphereShape* %8, %class.btSphereShape** %sphere, align 4
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %triObjWrap, align 4
  %call9 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %9)
  %10 = bitcast %class.btCollisionShape* %call9 to %class.btTriangleShape*
  store %class.btTriangleShape* %10, %class.btTriangleShape** %triangle, align 4
  %11 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_manifoldPtr10 = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 3
  %12 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr10, align 4
  call void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %11, %class.btPersistentManifold* %12)
  %13 = load %class.btSphereShape*, %class.btSphereShape** %sphere, align 4
  %14 = load %class.btTriangleShape*, %class.btTriangleShape** %triangle, align 4
  %m_manifoldPtr11 = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 3
  %15 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr11, align 4
  %call12 = call float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold* %15)
  %16 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_closestPointDistanceThreshold = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %16, i32 0, i32 8
  %17 = load float, float* %m_closestPointDistanceThreshold, align 4
  %add = fadd float %call12, %17
  %call13 = call %struct.SphereTriangleDetector* @_ZN22SphereTriangleDetectorC1EP13btSphereShapeP15btTriangleShapef(%struct.SphereTriangleDetector* %detector, %class.btSphereShape* %13, %class.btTriangleShape* %14, float %add)
  %call14 = call %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* @_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev(%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input)
  %m_maximumDistanceSquared = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 2
  store float 0x43ABC16D60000000, float* %m_maximumDistanceSquared, align 4
  %18 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %sphereObjWrap, align 4
  %call15 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %18)
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 0
  %call16 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformA, %class.btTransform* nonnull align 4 dereferenceable(64) %call15)
  %19 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %triObjWrap, align 4
  %call17 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %19)
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %input, i32 0, i32 1
  %call18 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %m_transformB, %class.btTransform* nonnull align 4 dereferenceable(64) %call17)
  %m_swapped19 = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 4
  %20 = load i8, i8* %m_swapped19, align 4
  %tobool20 = trunc i8 %20 to i1
  %frombool = zext i1 %tobool20 to i8
  store i8 %frombool, i8* %swapResults, align 1
  %21 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %22 = bitcast %class.btManifoldResult* %21 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %23 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %m_debugDraw = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %23, i32 0, i32 5
  %24 = load %class.btIDebugDraw*, %class.btIDebugDraw** %m_debugDraw, align 4
  %25 = load i8, i8* %swapResults, align 1
  %tobool21 = trunc i8 %25 to i1
  call void @_ZN22SphereTriangleDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%struct.SphereTriangleDetector* %detector, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132) %input, %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4) %22, %class.btIDebugDraw* %24, i1 zeroext %tobool21)
  %m_ownManifold = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 1
  %26 = load i8, i8* %m_ownManifold, align 4
  %tobool22 = trunc i8 %26 to i1
  br i1 %tobool22, label %if.then23, label %if.end24

if.then23:                                        ; preds = %cond.end7
  %27 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  call void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %27)
  br label %if.end24

if.end24:                                         ; preds = %if.then23, %cond.end7
  %call25 = call %struct.SphereTriangleDetector* @_ZN22SphereTriangleDetectorD2Ev(%struct.SphereTriangleDetector* %detector) #7
  br label %return

return:                                           ; preds = %if.end24, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_shape, align 4
  ret %class.btCollisionShape* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %this, %class.btPersistentManifold* %manifoldPtr) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  store %class.btPersistentManifold* %manifoldPtr, %class.btPersistentManifold** %manifoldPtr.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifoldPtr.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  store %class.btPersistentManifold* %0, %class.btPersistentManifold** %m_manifoldPtr, align 4
  ret void
}

declare float @_ZNK20btPersistentManifold27getContactBreakingThresholdEv(%class.btPersistentManifold*) #3

declare %struct.SphereTriangleDetector* @_ZN22SphereTriangleDetectorC1EP13btSphereShapeP15btTriangleShapef(%struct.SphereTriangleDetector* returned, %class.btSphereShape*, %class.btTriangleShape*, float) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* @_ZN36btDiscreteCollisionDetectorInterface17ClosestPointInputC2Ev(%"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"** %this.addr, align 4
  %m_transformA = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 0
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transformA)
  %m_transformB = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 1
  %call2 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %m_transformB)
  %m_maximumDistanceSquared = getelementptr inbounds %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput", %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1, i32 0, i32 2
  store float 0x43ABC16D60000000, float* %m_maximumDistanceSquared, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %0 = load %class.btTransform*, %class.btTransform** %m_worldTransform, align 4
  ret %class.btTransform* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

declare void @_ZN22SphereTriangleDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb(%struct.SphereTriangleDetector*, %"struct.btDiscreteCollisionDetectorInterface::ClosestPointInput"* nonnull align 4 dereferenceable(132), %"struct.btDiscreteCollisionDetectorInterface::Result"* nonnull align 4 dereferenceable(4), %class.btIDebugDraw*, i1 zeroext) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %isSwapped = alloca i8, align 1
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %call = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %if.end20

if.end:                                           ; preds = %entry
  %m_manifoldPtr2 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr2, align 4
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %1)
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4
  %call4 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %2)
  %cmp = icmp ne %class.btCollisionObject* %call3, %call4
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %isSwapped, align 1
  %3 = load i8, i8* %isSwapped, align 1
  %tobool5 = trunc i8 %3 to i1
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.end
  %m_manifoldPtr7 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr7, align 4
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4
  %call8 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %5)
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call8)
  %m_body0Wrap10 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap10, align 4
  %call11 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %6)
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call11)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %4, %class.btTransform* nonnull align 4 dereferenceable(64) %call9, %class.btTransform* nonnull align 4 dereferenceable(64) %call12)
  br label %if.end20

if.else:                                          ; preds = %if.end
  %m_manifoldPtr13 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr13, align 4
  %m_body0Wrap14 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap14, align 4
  %call15 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %8)
  %call16 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call15)
  %m_body1Wrap17 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap17, align 4
  %call18 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %9)
  %call19 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call18)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %7, %class.btTransform* nonnull align 4 dereferenceable(64) %call16, %class.btTransform* nonnull align 4 dereferenceable(64) %call19)
  br label %if.end20

if.end20:                                         ; preds = %if.then, %if.else, %if.then6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.SphereTriangleDetector* @_ZN22SphereTriangleDetectorD2Ev(%struct.SphereTriangleDetector* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.SphereTriangleDetector*, align 4
  store %struct.SphereTriangleDetector* %this, %struct.SphereTriangleDetector** %this.addr, align 4
  %this1 = load %struct.SphereTriangleDetector*, %struct.SphereTriangleDetector** %this.addr, align 4
  %0 = bitcast %struct.SphereTriangleDetector* %this1 to %struct.btDiscreteCollisionDetectorInterface*
  %call = call %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* %0) #7
  ret %struct.SphereTriangleDetector* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZN34btSphereTriangleCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult(%class.btSphereTriangleCollisionAlgorithm* %this, %class.btCollisionObject* %col0, %class.btCollisionObject* %col1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSphereTriangleCollisionAlgorithm*, align 4
  %col0.addr = alloca %class.btCollisionObject*, align 4
  %col1.addr = alloca %class.btCollisionObject*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  store %class.btSphereTriangleCollisionAlgorithm* %this, %class.btSphereTriangleCollisionAlgorithm** %this.addr, align 4
  store %class.btCollisionObject* %col0, %class.btCollisionObject** %col0.addr, align 4
  store %class.btCollisionObject* %col1, %class.btCollisionObject** %col1.addr, align 4
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4
  %this1 = load %class.btSphereTriangleCollisionAlgorithm*, %class.btSphereTriangleCollisionAlgorithm** %this.addr, align 4
  %0 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  ret float 1.000000e+00
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN34btSphereTriangleCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE(%class.btSphereTriangleCollisionAlgorithm* %this, %class.btAlignedObjectArray.2* nonnull align 4 dereferenceable(17) %manifoldArray) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btSphereTriangleCollisionAlgorithm*, align 4
  %manifoldArray.addr = alloca %class.btAlignedObjectArray.2*, align 4
  store %class.btSphereTriangleCollisionAlgorithm* %this, %class.btSphereTriangleCollisionAlgorithm** %this.addr, align 4
  store %class.btAlignedObjectArray.2* %manifoldArray, %class.btAlignedObjectArray.2** %manifoldArray.addr, align 4
  %this1 = load %class.btSphereTriangleCollisionAlgorithm*, %class.btSphereTriangleCollisionAlgorithm** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 3
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %tobool = icmp ne %class.btPersistentManifold* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %m_ownManifold = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 1
  %1 = load i8, i8* %m_ownManifold, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %2 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %manifoldArray.addr, align 4
  %m_manifoldPtr3 = getelementptr inbounds %class.btSphereTriangleCollisionAlgorithm, %class.btSphereTriangleCollisionAlgorithm* %this1, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.2* %2, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %m_manifoldPtr3)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4
  ret %class.btCollisionObject* %0
}

declare void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64)) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btDiscreteCollisionDetectorInterface* @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev(%struct.btDiscreteCollisionDetectorInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btDiscreteCollisionDetectorInterface*, align 4
  store %struct.btDiscreteCollisionDetectorInterface* %this, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  %this1 = load %struct.btDiscreteCollisionDetectorInterface*, %struct.btDiscreteCollisionDetectorInterface** %this.addr, align 4
  ret %struct.btDiscreteCollisionDetectorInterface* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.2* %this, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  %_Val.addr = alloca %class.btPersistentManifold**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  store %class.btPersistentManifold** %_Val, %class.btPersistentManifold*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.2* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.2* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.2* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.2* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.2* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 4
  %1 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %1, i32 %2
  %3 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btPersistentManifold**
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %_Val.addr, align 4
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %5, align 4
  store %class.btPersistentManifold* %6, %class.btPersistentManifold** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.2* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.2* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.2* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.2* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.2* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %2, %class.btPersistentManifold*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.2* %this1)
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.2* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.2* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.2* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.2* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %4, %class.btPersistentManifold*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.2* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.2* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.3* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.2* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  %5 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 4
  %7 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %7, i32 %8
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4
  store %class.btPersistentManifold* %9, %class.btPersistentManifold** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.2* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.2* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.3* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.3* %this, i32 %n, %class.btPersistentManifold*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.3*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.3* %this, %class.btAlignedAllocator.3** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.3*, %class.btAlignedAllocator.3** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.3* %this, %class.btPersistentManifold** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.3*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.3* %this, %class.btAlignedAllocator.3** %this.addr, align 4
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.3*, %class.btAlignedAllocator.3** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btSphereTriangleCollisionAlgorithm.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { nounwind }
attributes #8 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
