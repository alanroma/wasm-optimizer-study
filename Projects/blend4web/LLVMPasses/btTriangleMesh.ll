; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btTriangleMesh.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btTriangleMesh.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btTriangleMesh = type { %class.btTriangleIndexVertexArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12, i8, i8, float }
%class.btTriangleIndexVertexArray = type { %class.btStridingMeshInterface, %class.btAlignedObjectArray, [2 x i32], i32, %class.btVector3, %class.btVector3 }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btIndexedMesh*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btIndexedMesh = type { i32, i8*, i32, i32, i8*, i32, i32, i32 }
%class.btVector3 = type { [4 x float] }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, float*, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, i16*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btInternalTriangleIndexCallback = type { i32 (...)** }
%class.btSerializer = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN26btTriangleIndexVertexArrayC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayIfEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIjEC2Ev = comdat any

$_ZN20btAlignedObjectArrayItEC2Ev = comdat any

$_ZN13btIndexedMeshC2Ev = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE9push_backERKS0_ = comdat any

$_ZNK20btAlignedObjectArrayIjE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi = comdat any

$_ZNK20btAlignedObjectArrayItE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIfE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIjE9push_backERKj = comdat any

$_ZN20btAlignedObjectArrayIjEixEi = comdat any

$_ZN20btAlignedObjectArrayItE9push_backERKt = comdat any

$_ZN20btAlignedObjectArrayItEixEi = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_ = comdat any

$_ZN20btAlignedObjectArrayIfEixEi = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayIfE9push_backERKf = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIfE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIjE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayItE7reserveEi = comdat any

$_ZN14btTriangleMeshD2Ev = comdat any

$_ZN14btTriangleMeshD0Ev = comdat any

$_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi = comdat any

$_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi = comdat any

$_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv = comdat any

$_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv = comdat any

$_ZN23btStridingMeshInterfaceC2Ev = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshEC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN18btAlignedAllocatorI13btIndexedMeshLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE4initEv = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN20btAlignedObjectArrayItED2Ev = comdat any

$_ZN20btAlignedObjectArrayIjED2Ev = comdat any

$_ZN20btAlignedObjectArrayIfED2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZN20btAlignedObjectArrayItE5clearEv = comdat any

$_ZN20btAlignedObjectArrayItE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayItE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayItE4initEv = comdat any

$_ZN18btAlignedAllocatorItLj16EE10deallocateEPt = comdat any

$_ZN20btAlignedObjectArrayIjE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIjE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIjE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIjE4initEv = comdat any

$_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj = comdat any

$_ZN20btAlignedObjectArrayIfE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIfE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIfE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIfE4initEv = comdat any

$_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN26btTriangleIndexVertexArraydlEPv = comdat any

$_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI13btIndexedMeshE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE9allocSizeEi = comdat any

$_ZN13btIndexedMeshnwEmPv = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI13btIndexedMeshE4copyEiiPS0_ = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI13btIndexedMeshE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI13btIndexedMeshLj16EE8allocateEiPPKS0_ = comdat any

$_ZN18btAlignedAllocatorI13btIndexedMeshLj16EE10deallocateEPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN18btAlignedAllocatorIfLj16EEC2Ev = comdat any

$_ZN18btAlignedAllocatorIjLj16EEC2Ev = comdat any

$_ZN18btAlignedAllocatorItLj16EEC2Ev = comdat any

$_ZNK20btAlignedObjectArrayIjE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIjE9allocSizeEi = comdat any

$_ZNK20btAlignedObjectArrayItE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayItE9allocSizeEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZNK20btAlignedObjectArrayIfE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIfE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIfE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIfE4copyEiiPf = comdat any

$_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf = comdat any

$_ZN20btAlignedObjectArrayIjE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIjE4copyEiiPj = comdat any

$_ZN18btAlignedAllocatorIjLj16EE8allocateEiPPKj = comdat any

$_ZN20btAlignedObjectArrayItE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayItE4copyEiiPt = comdat any

$_ZN18btAlignedAllocatorItLj16EE8allocateEiPPKt = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV14btTriangleMesh = hidden unnamed_addr constant { [17 x i8*] } { [17 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI14btTriangleMesh to i8*), i8* bitcast (%class.btTriangleMesh* (%class.btTriangleMesh*)* @_ZN14btTriangleMeshD2Ev to i8*), i8* bitcast (void (%class.btTriangleMesh*)* @_ZN14btTriangleMeshD0Ev to i8*), i8* bitcast (void (%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_ to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)* @_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)* @_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i32)* @_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, i32)* @_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi to i8*), i8* bitcast (i32 (%class.btTriangleIndexVertexArray*)* @_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv to i8*), i8* bitcast (void (%class.btTriangleMesh*, i32)* @_ZN14btTriangleMesh19preallocateVerticesEi to i8*), i8* bitcast (void (%class.btTriangleMesh*, i32)* @_ZN14btTriangleMesh18preallocateIndicesEi to i8*), i8* bitcast (i1 (%class.btTriangleIndexVertexArray*)* @_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, %class.btVector3*, %class.btVector3*)* @_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_ to i8*), i8* bitcast (void (%class.btTriangleIndexVertexArray*, %class.btVector3*, %class.btVector3*)* @_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_ to i8*), i8* bitcast (i32 (%class.btStridingMeshInterface*)* @_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)* @_ZNK23btStridingMeshInterface9serializeEPvP12btSerializer to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS14btTriangleMesh = hidden constant [17 x i8] c"14btTriangleMesh\00", align 1
@_ZTI26btTriangleIndexVertexArray = external constant i8*
@_ZTI14btTriangleMesh = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([17 x i8], [17 x i8]* @_ZTS14btTriangleMesh, i32 0, i32 0), i8* bitcast (i8** @_ZTI26btTriangleIndexVertexArray to i8*) }, align 4
@_ZTV26btTriangleIndexVertexArray = external unnamed_addr constant { [17 x i8*] }, align 4
@_ZTV23btStridingMeshInterface = external unnamed_addr constant { [17 x i8*] }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btTriangleMesh.cpp, i8* null }]

@_ZN14btTriangleMeshC1Ebb = hidden unnamed_addr alias %class.btTriangleMesh* (%class.btTriangleMesh*, i1, i1), %class.btTriangleMesh* (%class.btTriangleMesh*, i1, i1)* @_ZN14btTriangleMeshC2Ebb

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btTriangleMesh* @_ZN14btTriangleMeshC2Ebb(%class.btTriangleMesh* returned %this, i1 zeroext %use32bitIndices, i1 zeroext %use4componentVertices) unnamed_addr #2 {
entry:
  %retval = alloca %class.btTriangleMesh*, align 4
  %this.addr = alloca %class.btTriangleMesh*, align 4
  %use32bitIndices.addr = alloca i8, align 1
  %use4componentVertices.addr = alloca i8, align 1
  %meshIndex = alloca %struct.btIndexedMesh, align 4
  store %class.btTriangleMesh* %this, %class.btTriangleMesh** %this.addr, align 4
  %frombool = zext i1 %use32bitIndices to i8
  store i8 %frombool, i8* %use32bitIndices.addr, align 1
  %frombool1 = zext i1 %use4componentVertices to i8
  store i8 %frombool1, i8* %use4componentVertices.addr, align 1
  %this2 = load %class.btTriangleMesh*, %class.btTriangleMesh** %this.addr, align 4
  store %class.btTriangleMesh* %this2, %class.btTriangleMesh** %retval, align 4
  %0 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %call = call %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayC2Ev(%class.btTriangleIndexVertexArray* %0)
  %1 = bitcast %class.btTriangleMesh* %this2 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [17 x i8*] }, { [17 x i8*] }* @_ZTV14btTriangleMesh, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_4componentVertices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this2, i32 0, i32 1
  %call3 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.0* %m_4componentVertices)
  %m_3componentVertices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this2, i32 0, i32 2
  %call4 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.4* %m_3componentVertices)
  %m_32bitIndices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this2, i32 0, i32 3
  %call5 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.8* %m_32bitIndices)
  %m_16bitIndices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this2, i32 0, i32 4
  %call6 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayItEC2Ev(%class.btAlignedObjectArray.12* %m_16bitIndices)
  %m_use32bitIndices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this2, i32 0, i32 5
  %2 = load i8, i8* %use32bitIndices.addr, align 1
  %tobool = trunc i8 %2 to i1
  %frombool7 = zext i1 %tobool to i8
  store i8 %frombool7, i8* %m_use32bitIndices, align 4
  %m_use4componentVertices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this2, i32 0, i32 6
  %3 = load i8, i8* %use4componentVertices.addr, align 1
  %tobool8 = trunc i8 %3 to i1
  %frombool9 = zext i1 %tobool8 to i8
  store i8 %frombool9, i8* %m_use4componentVertices, align 1
  %m_weldingThreshold = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this2, i32 0, i32 7
  store float 0.000000e+00, float* %m_weldingThreshold, align 4
  %call10 = call %struct.btIndexedMesh* @_ZN13btIndexedMeshC2Ev(%struct.btIndexedMesh* %meshIndex)
  %m_numTriangles = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %meshIndex, i32 0, i32 0
  store i32 0, i32* %m_numTriangles, align 4
  %m_numVertices = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %meshIndex, i32 0, i32 3
  store i32 0, i32* %m_numVertices, align 4
  %m_indexType = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %meshIndex, i32 0, i32 6
  store i32 2, i32* %m_indexType, align 4
  %m_triangleIndexBase = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %meshIndex, i32 0, i32 1
  store i8* null, i8** %m_triangleIndexBase, align 4
  %m_triangleIndexStride = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %meshIndex, i32 0, i32 2
  store i32 12, i32* %m_triangleIndexStride, align 4
  %m_vertexBase = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %meshIndex, i32 0, i32 4
  store i8* null, i8** %m_vertexBase, align 4
  %m_vertexStride = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %meshIndex, i32 0, i32 5
  store i32 16, i32* %m_vertexStride, align 4
  %4 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %4, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayI13btIndexedMeshE9push_backERKS0_(%class.btAlignedObjectArray* %m_indexedMeshes, %struct.btIndexedMesh* nonnull align 4 dereferenceable(32) %meshIndex)
  %m_use32bitIndices11 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this2, i32 0, i32 5
  %5 = load i8, i8* %m_use32bitIndices11, align 4
  %tobool12 = trunc i8 %5 to i1
  br i1 %tobool12, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_32bitIndices13 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this2, i32 0, i32 3
  %call14 = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.8* %m_32bitIndices13)
  %div = sdiv i32 %call14, 3
  %6 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes15 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %6, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes15, i32 0)
  %m_numTriangles17 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call16, i32 0, i32 0
  store i32 %div, i32* %m_numTriangles17, align 4
  %7 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes18 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %7, i32 0, i32 1
  %call19 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes18, i32 0)
  %m_triangleIndexBase20 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call19, i32 0, i32 1
  store i8* null, i8** %m_triangleIndexBase20, align 4
  %8 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes21 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %8, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes21, i32 0)
  %m_indexType23 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call22, i32 0, i32 6
  store i32 2, i32* %m_indexType23, align 4
  %9 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes24 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %9, i32 0, i32 1
  %call25 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes24, i32 0)
  %m_triangleIndexStride26 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call25, i32 0, i32 2
  store i32 12, i32* %m_triangleIndexStride26, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %m_16bitIndices27 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this2, i32 0, i32 4
  %call28 = call i32 @_ZNK20btAlignedObjectArrayItE4sizeEv(%class.btAlignedObjectArray.12* %m_16bitIndices27)
  %div29 = sdiv i32 %call28, 3
  %10 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes30 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %10, i32 0, i32 1
  %call31 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes30, i32 0)
  %m_numTriangles32 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call31, i32 0, i32 0
  store i32 %div29, i32* %m_numTriangles32, align 4
  %11 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes33 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %11, i32 0, i32 1
  %call34 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes33, i32 0)
  %m_triangleIndexBase35 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call34, i32 0, i32 1
  store i8* null, i8** %m_triangleIndexBase35, align 4
  %12 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes36 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %12, i32 0, i32 1
  %call37 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes36, i32 0)
  %m_indexType38 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call37, i32 0, i32 6
  store i32 3, i32* %m_indexType38, align 4
  %13 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes39 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %13, i32 0, i32 1
  %call40 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes39, i32 0)
  %m_triangleIndexStride41 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call40, i32 0, i32 2
  store i32 6, i32* %m_triangleIndexStride41, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %m_use4componentVertices42 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this2, i32 0, i32 6
  %14 = load i8, i8* %m_use4componentVertices42, align 1
  %tobool43 = trunc i8 %14 to i1
  br i1 %tobool43, label %if.then44, label %if.else56

if.then44:                                        ; preds = %if.end
  %m_4componentVertices45 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this2, i32 0, i32 1
  %call46 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.0* %m_4componentVertices45)
  %15 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes47 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %15, i32 0, i32 1
  %call48 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes47, i32 0)
  %m_numVertices49 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call48, i32 0, i32 3
  store i32 %call46, i32* %m_numVertices49, align 4
  %16 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes50 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %16, i32 0, i32 1
  %call51 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes50, i32 0)
  %m_vertexBase52 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call51, i32 0, i32 4
  store i8* null, i8** %m_vertexBase52, align 4
  %17 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes53 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %17, i32 0, i32 1
  %call54 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes53, i32 0)
  %m_vertexStride55 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call54, i32 0, i32 5
  store i32 16, i32* %m_vertexStride55, align 4
  br label %if.end69

if.else56:                                        ; preds = %if.end
  %m_3componentVertices57 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this2, i32 0, i32 2
  %call58 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_3componentVertices57)
  %div59 = sdiv i32 %call58, 3
  %18 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes60 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %18, i32 0, i32 1
  %call61 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes60, i32 0)
  %m_numVertices62 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call61, i32 0, i32 3
  store i32 %div59, i32* %m_numVertices62, align 4
  %19 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes63 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %19, i32 0, i32 1
  %call64 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes63, i32 0)
  %m_vertexBase65 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call64, i32 0, i32 4
  store i8* null, i8** %m_vertexBase65, align 4
  %20 = bitcast %class.btTriangleMesh* %this2 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes66 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %20, i32 0, i32 1
  %call67 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes66, i32 0)
  %m_vertexStride68 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call67, i32 0, i32 5
  store i32 12, i32* %m_vertexStride68, align 4
  br label %if.end69

if.end69:                                         ; preds = %if.else56, %if.then44
  %21 = load %class.btTriangleMesh*, %class.btTriangleMesh** %retval, align 4
  ret %class.btTriangleMesh* %21
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayC2Ev(%class.btTriangleIndexVertexArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %0 = bitcast %class.btTriangleIndexVertexArray* %this1 to %class.btStridingMeshInterface*
  %call = call %class.btStridingMeshInterface* @_ZN23btStridingMeshInterfaceC2Ev(%class.btStridingMeshInterface* %0)
  %1 = bitcast %class.btTriangleIndexVertexArray* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [17 x i8*] }, { [17 x i8*] }* @_ZTV26btTriangleIndexVertexArray, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI13btIndexedMeshEC2Ev(%class.btAlignedObjectArray* %m_indexedMeshes)
  %m_hasAabb = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_hasAabb, align 4
  %m_aabbMin = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 4
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMin)
  %m_aabbMax = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 5
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_aabbMax)
  ret %class.btTriangleIndexVertexArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorIjLj16EEC2Ev(%class.btAlignedAllocator.9* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayItEC2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorItLj16EEC2Ev(%class.btAlignedAllocator.13* %m_allocator)
  call void @_ZN20btAlignedObjectArrayItE4initEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btIndexedMesh* @_ZN13btIndexedMeshC2Ev(%struct.btIndexedMesh* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btIndexedMesh*, align 4
  store %struct.btIndexedMesh* %this, %struct.btIndexedMesh** %this.addr, align 4
  %this1 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %this.addr, align 4
  %m_indexType = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %this1, i32 0, i32 6
  store i32 2, i32* %m_indexType, align 4
  %m_vertexType = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %this1, i32 0, i32 7
  store i32 0, i32* %m_vertexType, align 4
  ret %struct.btIndexedMesh* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13btIndexedMeshE9push_backERKS0_(%class.btAlignedObjectArray* %this, %struct.btIndexedMesh* nonnull align 4 dereferenceable(32) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %struct.btIndexedMesh*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %struct.btIndexedMesh* %_Val, %struct.btIndexedMesh** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI13btIndexedMeshE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI13btIndexedMeshE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %1, i32 %2
  %3 = bitcast %struct.btIndexedMesh* %arrayidx to i8*
  %call5 = call i8* @_ZN13btIndexedMeshnwEmPv(i32 32, i8* %3)
  %4 = bitcast i8* %call5 to %struct.btIndexedMesh*
  %5 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %_Val.addr, align 4
  %6 = bitcast %struct.btIndexedMesh* %4 to i8*
  %7 = bitcast %struct.btIndexedMesh* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 32, i1 false)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %0, i32 %1
  ret %struct.btIndexedMesh* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayItE4sizeEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btTriangleMesh8addIndexEi(%class.btTriangleMesh* %this, i32 %index) #2 {
entry:
  %this.addr = alloca %class.btTriangleMesh*, align 4
  %index.addr = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp4 = alloca i16, align 2
  store %class.btTriangleMesh* %this, %class.btTriangleMesh** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btTriangleMesh*, %class.btTriangleMesh** %this.addr, align 4
  %m_use32bitIndices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 5
  %0 = load i8, i8* %m_use32bitIndices, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_32bitIndices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 3
  %1 = load i32, i32* %index.addr, align 4
  store i32 %1, i32* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIjE9push_backERKj(%class.btAlignedObjectArray.8* %m_32bitIndices, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %m_32bitIndices2 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 3
  %call = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.8* %m_32bitIndices2, i32 0)
  %2 = bitcast i32* %call to i8*
  %3 = bitcast %class.btTriangleMesh* %this1 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %3, i32 0, i32 1
  %call3 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes, i32 0)
  %m_triangleIndexBase = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call3, i32 0, i32 1
  store i8* %2, i8** %m_triangleIndexBase, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %m_16bitIndices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 4
  %4 = load i32, i32* %index.addr, align 4
  %conv = trunc i32 %4 to i16
  store i16 %conv, i16* %ref.tmp4, align 2
  call void @_ZN20btAlignedObjectArrayItE9push_backERKt(%class.btAlignedObjectArray.12* %m_16bitIndices, i16* nonnull align 2 dereferenceable(2) %ref.tmp4)
  %m_16bitIndices5 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 4
  %call6 = call nonnull align 2 dereferenceable(2) i16* @_ZN20btAlignedObjectArrayItEixEi(%class.btAlignedObjectArray.12* %m_16bitIndices5, i32 0)
  %5 = bitcast i16* %call6 to i8*
  %6 = bitcast %class.btTriangleMesh* %this1 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes7 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %6, i32 0, i32 1
  %call8 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes7, i32 0)
  %m_triangleIndexBase9 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call8, i32 0, i32 1
  store i8* %5, i8** %m_triangleIndexBase9, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE9push_backERKj(%class.btAlignedObjectArray.8* %this, i32* nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Val.addr = alloca i32*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32* %_Val, i32** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIjE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIjE9allocSizeEi(%class.btAlignedObjectArray.8* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIjE7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %1 = load i32*, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds i32, i32* %1, i32 %2
  %3 = bitcast i32* %arrayidx to i8*
  %4 = bitcast i8* %3 to i32*
  %5 = load i32*, i32** %_Val.addr, align 4
  %6 = load i32, i32* %5, align 4
  store i32 %6, i32* %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayItE9push_backERKt(%class.btAlignedObjectArray.12* %this, i16* nonnull align 2 dereferenceable(2) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Val.addr = alloca i16*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i16* %_Val, i16** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayItE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayItE8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayItE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayItE9allocSizeEi(%class.btAlignedObjectArray.12* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayItE7reserveEi(%class.btAlignedObjectArray.12* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %1 = load i16*, i16** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds i16, i16* %1, i32 %2
  %3 = bitcast i16* %arrayidx to i8*
  %4 = bitcast i8* %3 to i16*
  %5 = load i16*, i16** %_Val.addr, align 4
  %6 = load i16, i16* %5, align 2
  store i16 %6, i16* %4, align 2
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 2 dereferenceable(2) i16* @_ZN20btAlignedObjectArrayItEixEi(%class.btAlignedObjectArray.12* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load i16*, i16** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i16, i16* %0, i32 %1
  ret i16* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btTriangleMesh18addTriangleIndicesEiii(%class.btTriangleMesh* %this, i32 %index1, i32 %index2, i32 %index3) #2 {
entry:
  %this.addr = alloca %class.btTriangleMesh*, align 4
  %index1.addr = alloca i32, align 4
  %index2.addr = alloca i32, align 4
  %index3.addr = alloca i32, align 4
  store %class.btTriangleMesh* %this, %class.btTriangleMesh** %this.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  store i32 %index2, i32* %index2.addr, align 4
  store i32 %index3, i32* %index3.addr, align 4
  %this1 = load %class.btTriangleMesh*, %class.btTriangleMesh** %this.addr, align 4
  %0 = bitcast %class.btTriangleMesh* %this1 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %0, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes, i32 0)
  %m_numTriangles = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call, i32 0, i32 0
  %1 = load i32, i32* %m_numTriangles, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_numTriangles, align 4
  %2 = load i32, i32* %index1.addr, align 4
  call void @_ZN14btTriangleMesh8addIndexEi(%class.btTriangleMesh* %this1, i32 %2)
  %3 = load i32, i32* %index2.addr, align 4
  call void @_ZN14btTriangleMesh8addIndexEi(%class.btTriangleMesh* %this1, i32 %3)
  %4 = load i32, i32* %index3.addr, align 4
  call void @_ZN14btTriangleMesh8addIndexEi(%class.btTriangleMesh* %this1, i32 %4)
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_ZN14btTriangleMesh15findOrAddVertexERK9btVector3b(%class.btTriangleMesh* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vertex, i1 zeroext %removeDuplicateVertices) #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btTriangleMesh*, align 4
  %vertex.addr = alloca %class.btVector3*, align 4
  %removeDuplicateVertices.addr = alloca i8, align 1
  %i = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %i21 = alloca i32, align 4
  %vtx = alloca %class.btVector3, align 4
  %ref.tmp34 = alloca %class.btVector3, align 4
  store %class.btTriangleMesh* %this, %class.btTriangleMesh** %this.addr, align 4
  store %class.btVector3* %vertex, %class.btVector3** %vertex.addr, align 4
  %frombool = zext i1 %removeDuplicateVertices to i8
  store i8 %frombool, i8* %removeDuplicateVertices.addr, align 1
  %this1 = load %class.btTriangleMesh*, %class.btTriangleMesh** %this.addr, align 4
  %m_use4componentVertices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_use4componentVertices, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %1 = load i8, i8* %removeDuplicateVertices.addr, align 1
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end9

if.then3:                                         ; preds = %if.then
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then3
  %2 = load i32, i32* %i, align 4
  %m_4componentVertices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.0* %m_4componentVertices)
  %cmp = icmp slt i32 %2, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_4componentVertices4 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 1
  %3 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.0* %m_4componentVertices4, i32 %3)
  %4 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %call5, %class.btVector3* nonnull align 4 dereferenceable(16) %4)
  %call6 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp)
  %m_weldingThreshold = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 7
  %5 = load float, float* %m_weldingThreshold, align 4
  %cmp7 = fcmp ole float %call6, %5
  br i1 %cmp7, label %if.then8, label %if.end

if.then8:                                         ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  store i32 %6, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end9

if.end9:                                          ; preds = %for.end, %if.then
  %8 = bitcast %class.btTriangleMesh* %this1 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %8, i32 0, i32 1
  %call10 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes, i32 0)
  %m_numVertices = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call10, i32 0, i32 3
  %9 = load i32, i32* %m_numVertices, align 4
  %inc11 = add nsw i32 %9, 1
  store i32 %inc11, i32* %m_numVertices, align 4
  %m_4componentVertices12 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 1
  %10 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray.0* %m_4componentVertices12, %class.btVector3* nonnull align 4 dereferenceable(16) %10)
  %m_4componentVertices13 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 1
  %call14 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.0* %m_4componentVertices13, i32 0)
  %11 = bitcast %class.btVector3* %call14 to i8*
  %12 = bitcast %class.btTriangleMesh* %this1 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes15 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %12, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes15, i32 0)
  %m_vertexBase = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call16, i32 0, i32 4
  store i8* %11, i8** %m_vertexBase, align 4
  %m_4componentVertices17 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 1
  %call18 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.0* %m_4componentVertices17)
  %sub = sub nsw i32 %call18, 1
  store i32 %sub, i32* %retval, align 4
  br label %return

if.else:                                          ; preds = %entry
  %13 = load i8, i8* %removeDuplicateVertices.addr, align 1
  %tobool19 = trunc i8 %13 to i1
  br i1 %tobool19, label %if.then20, label %if.end43

if.then20:                                        ; preds = %if.else
  store i32 0, i32* %i21, align 4
  br label %for.cond22

for.cond22:                                       ; preds = %for.inc40, %if.then20
  %14 = load i32, i32* %i21, align 4
  %m_3componentVertices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 2
  %call23 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_3componentVertices)
  %cmp24 = icmp slt i32 %14, %call23
  br i1 %cmp24, label %for.body25, label %for.end42

for.body25:                                       ; preds = %for.cond22
  %m_3componentVertices26 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 2
  %15 = load i32, i32* %i21, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_3componentVertices26, i32 %15)
  %m_3componentVertices28 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 2
  %16 = load i32, i32* %i21, align 4
  %add = add nsw i32 %16, 1
  %call29 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_3componentVertices28, i32 %add)
  %m_3componentVertices30 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 2
  %17 = load i32, i32* %i21, align 4
  %add31 = add nsw i32 %17, 2
  %call32 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_3componentVertices30, i32 %add31)
  %call33 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %vtx, float* nonnull align 4 dereferenceable(4) %call27, float* nonnull align 4 dereferenceable(4) %call29, float* nonnull align 4 dereferenceable(4) %call32)
  %18 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp34, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx, %class.btVector3* nonnull align 4 dereferenceable(16) %18)
  %call35 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %ref.tmp34)
  %m_weldingThreshold36 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 7
  %19 = load float, float* %m_weldingThreshold36, align 4
  %cmp37 = fcmp ole float %call35, %19
  br i1 %cmp37, label %if.then38, label %if.end39

if.then38:                                        ; preds = %for.body25
  %20 = load i32, i32* %i21, align 4
  %div = sdiv i32 %20, 3
  store i32 %div, i32* %retval, align 4
  br label %return

if.end39:                                         ; preds = %for.body25
  br label %for.inc40

for.inc40:                                        ; preds = %if.end39
  %21 = load i32, i32* %i21, align 4
  %add41 = add nsw i32 %21, 3
  store i32 %add41, i32* %i21, align 4
  br label %for.cond22

for.end42:                                        ; preds = %for.cond22
  br label %if.end43

if.end43:                                         ; preds = %for.end42, %if.else
  %m_3componentVertices44 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 2
  %22 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4
  %call45 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %22)
  call void @_ZN20btAlignedObjectArrayIfE9push_backERKf(%class.btAlignedObjectArray.4* %m_3componentVertices44, float* nonnull align 4 dereferenceable(4) %call45)
  %m_3componentVertices46 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 2
  %23 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4
  %call47 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %23)
  call void @_ZN20btAlignedObjectArrayIfE9push_backERKf(%class.btAlignedObjectArray.4* %m_3componentVertices46, float* nonnull align 4 dereferenceable(4) %call47)
  %m_3componentVertices48 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 2
  %24 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %24)
  call void @_ZN20btAlignedObjectArrayIfE9push_backERKf(%class.btAlignedObjectArray.4* %m_3componentVertices48, float* nonnull align 4 dereferenceable(4) %call49)
  %25 = bitcast %class.btTriangleMesh* %this1 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes50 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %25, i32 0, i32 1
  %call51 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes50, i32 0)
  %m_numVertices52 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call51, i32 0, i32 3
  %26 = load i32, i32* %m_numVertices52, align 4
  %inc53 = add nsw i32 %26, 1
  store i32 %inc53, i32* %m_numVertices52, align 4
  %m_3componentVertices54 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 2
  %call55 = call nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %m_3componentVertices54, i32 0)
  %27 = bitcast float* %call55 to i8*
  %28 = bitcast %class.btTriangleMesh* %this1 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes56 = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %28, i32 0, i32 1
  %call57 = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes56, i32 0)
  %m_vertexBase58 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call57, i32 0, i32 4
  store i8* %27, i8** %m_vertexBase58, align 4
  %m_3componentVertices59 = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 2
  %call60 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %m_3componentVertices59)
  %div61 = sdiv i32 %call60, 3
  %sub62 = sub nsw i32 %div61, 1
  store i32 %sub62, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end43, %if.then38, %if.end9, %if.then8
  %29 = load i32, i32* %retval, align 4
  ret i32 %29
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray.0* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %class.btVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store %class.btVector3* %_Val, %class.btVector3** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %1 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 %2
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %3)
  %4 = bitcast i8* %call5 to %class.btVector3*
  %5 = load %class.btVector3*, %class.btVector3** %_Val.addr, align 4
  %6 = bitcast %class.btVector3* %4 to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZN20btAlignedObjectArrayIfEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds float, float* %0, i32 %1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE9push_backERKf(%class.btAlignedObjectArray.4* %this, float* nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Val.addr = alloca float*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store float* %_Val, float** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIfE9allocSizeEi(%class.btAlignedObjectArray.4* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.4* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %1 = load float*, float** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds float, float* %1, i32 %2
  %3 = bitcast float* %arrayidx to i8*
  %4 = bitcast i8* %3 to float*
  %5 = load float*, float** %_Val.addr, align 4
  %6 = load float, float* %5, align 4
  store float %6, float* %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btTriangleMesh11addTriangleERK9btVector3S2_S2_b(%class.btTriangleMesh* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %vertex0, %class.btVector3* nonnull align 4 dereferenceable(16) %vertex1, %class.btVector3* nonnull align 4 dereferenceable(16) %vertex2, i1 zeroext %removeDuplicateVertices) #2 {
entry:
  %this.addr = alloca %class.btTriangleMesh*, align 4
  %vertex0.addr = alloca %class.btVector3*, align 4
  %vertex1.addr = alloca %class.btVector3*, align 4
  %vertex2.addr = alloca %class.btVector3*, align 4
  %removeDuplicateVertices.addr = alloca i8, align 1
  store %class.btTriangleMesh* %this, %class.btTriangleMesh** %this.addr, align 4
  store %class.btVector3* %vertex0, %class.btVector3** %vertex0.addr, align 4
  store %class.btVector3* %vertex1, %class.btVector3** %vertex1.addr, align 4
  store %class.btVector3* %vertex2, %class.btVector3** %vertex2.addr, align 4
  %frombool = zext i1 %removeDuplicateVertices to i8
  store i8 %frombool, i8* %removeDuplicateVertices.addr, align 1
  %this1 = load %class.btTriangleMesh*, %class.btTriangleMesh** %this.addr, align 4
  %0 = bitcast %class.btTriangleMesh* %this1 to %class.btTriangleIndexVertexArray*
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %0, i32 0, i32 1
  %call = call nonnull align 4 dereferenceable(32) %struct.btIndexedMesh* @_ZN20btAlignedObjectArrayI13btIndexedMeshEixEi(%class.btAlignedObjectArray* %m_indexedMeshes, i32 0)
  %m_numTriangles = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %call, i32 0, i32 0
  %1 = load i32, i32* %m_numTriangles, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_numTriangles, align 4
  %2 = load %class.btVector3*, %class.btVector3** %vertex0.addr, align 4
  %3 = load i8, i8* %removeDuplicateVertices.addr, align 1
  %tobool = trunc i8 %3 to i1
  %call2 = call i32 @_ZN14btTriangleMesh15findOrAddVertexERK9btVector3b(%class.btTriangleMesh* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, i1 zeroext %tobool)
  call void @_ZN14btTriangleMesh8addIndexEi(%class.btTriangleMesh* %this1, i32 %call2)
  %4 = load %class.btVector3*, %class.btVector3** %vertex1.addr, align 4
  %5 = load i8, i8* %removeDuplicateVertices.addr, align 1
  %tobool3 = trunc i8 %5 to i1
  %call4 = call i32 @_ZN14btTriangleMesh15findOrAddVertexERK9btVector3b(%class.btTriangleMesh* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %4, i1 zeroext %tobool3)
  call void @_ZN14btTriangleMesh8addIndexEi(%class.btTriangleMesh* %this1, i32 %call4)
  %6 = load %class.btVector3*, %class.btVector3** %vertex2.addr, align 4
  %7 = load i8, i8* %removeDuplicateVertices.addr, align 1
  %tobool5 = trunc i8 %7 to i1
  %call6 = call i32 @_ZN14btTriangleMesh15findOrAddVertexERK9btVector3b(%class.btTriangleMesh* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %6, i1 zeroext %tobool5)
  call void @_ZN14btTriangleMesh8addIndexEi(%class.btTriangleMesh* %this1, i32 %call6)
  ret void
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK14btTriangleMesh15getNumTrianglesEv(%class.btTriangleMesh* %this) #2 {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btTriangleMesh*, align 4
  store %class.btTriangleMesh* %this, %class.btTriangleMesh** %this.addr, align 4
  %this1 = load %class.btTriangleMesh*, %class.btTriangleMesh** %this.addr, align 4
  %m_use32bitIndices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 5
  %0 = load i8, i8* %m_use32bitIndices, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_32bitIndices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 3
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.8* %m_32bitIndices)
  %div = sdiv i32 %call, 3
  store i32 %div, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %m_16bitIndices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayItE4sizeEv(%class.btAlignedObjectArray.12* %m_16bitIndices)
  %div3 = sdiv i32 %call2, 3
  store i32 %div3, i32* %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %1 = load i32, i32* %retval, align 4
  ret i32 %1
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btTriangleMesh19preallocateVerticesEi(%class.btTriangleMesh* %this, i32 %numverts) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btTriangleMesh*, align 4
  %numverts.addr = alloca i32, align 4
  store %class.btTriangleMesh* %this, %class.btTriangleMesh** %this.addr, align 4
  store i32 %numverts, i32* %numverts.addr, align 4
  %this1 = load %class.btTriangleMesh*, %class.btTriangleMesh** %this.addr, align 4
  %m_use4componentVertices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 6
  %0 = load i8, i8* %m_use4componentVertices, align 1
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_4componentVertices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 1
  %1 = load i32, i32* %numverts.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.0* %m_4componentVertices, i32 %1)
  br label %if.end

if.else:                                          ; preds = %entry
  %m_3componentVertices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 2
  %2 = load i32, i32* %numverts.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.4* %m_3componentVertices, i32 %2)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %2, %class.btVector3** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %3 = load %class.btVector3*, %class.btVector3** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %class.btVector3* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btVector3* %4, %class.btVector3** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7reserveEi(%class.btAlignedObjectArray.4* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca float*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.4* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.4* %this1, i32 %1)
  %2 = bitcast i8* %call2 to float*
  store float* %2, float** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  %3 = load float*, float** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call3, float* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load float*, float** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store float* %4, float** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN14btTriangleMesh18preallocateIndicesEi(%class.btTriangleMesh* %this, i32 %numindices) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btTriangleMesh*, align 4
  %numindices.addr = alloca i32, align 4
  store %class.btTriangleMesh* %this, %class.btTriangleMesh** %this.addr, align 4
  store i32 %numindices, i32* %numindices.addr, align 4
  %this1 = load %class.btTriangleMesh*, %class.btTriangleMesh** %this.addr, align 4
  %m_use32bitIndices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 5
  %0 = load i8, i8* %m_use32bitIndices, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_32bitIndices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 3
  %1 = load i32, i32* %numindices.addr, align 4
  call void @_ZN20btAlignedObjectArrayIjE7reserveEi(%class.btAlignedObjectArray.8* %m_32bitIndices, i32 %1)
  br label %if.end

if.else:                                          ; preds = %entry
  %m_16bitIndices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 4
  %2 = load i32, i32* %numindices.addr, align 4
  call void @_ZN20btAlignedObjectArrayItE7reserveEi(%class.btAlignedObjectArray.12* %m_16bitIndices, i32 %2)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIjE8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i32*
  store i32* %2, i32** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %3 = load i32*, i32** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIjE4copyEiiPj(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, i32* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i32*, i32** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i32* %4, i32** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayItE7reserveEi(%class.btAlignedObjectArray.12* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i16*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayItE8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayItE8allocateEi(%class.btAlignedObjectArray.12* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i16*
  store i16* %2, i16** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayItE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %3 = load i16*, i16** %s, align 4
  call void @_ZNK20btAlignedObjectArrayItE4copyEiiPt(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call3, i16* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayItE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayItE7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayItE10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i16*, i16** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store i16* %4, i16** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btTriangleMesh* @_ZN14btTriangleMeshD2Ev(%class.btTriangleMesh* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleMesh*, align 4
  store %class.btTriangleMesh* %this, %class.btTriangleMesh** %this.addr, align 4
  %this1 = load %class.btTriangleMesh*, %class.btTriangleMesh** %this.addr, align 4
  %0 = bitcast %class.btTriangleMesh* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [17 x i8*] }, { [17 x i8*] }* @_ZTV14btTriangleMesh, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_16bitIndices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 4
  %call = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayItED2Ev(%class.btAlignedObjectArray.12* %m_16bitIndices) #6
  %m_32bitIndices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 3
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.8* %m_32bitIndices) #6
  %m_3componentVertices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.4* %m_3componentVertices) #6
  %m_4componentVertices = getelementptr inbounds %class.btTriangleMesh, %class.btTriangleMesh* %this1, i32 0, i32 1
  %call4 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.0* %m_4componentVertices) #6
  %1 = bitcast %class.btTriangleMesh* %this1 to %class.btTriangleIndexVertexArray*
  %call5 = call %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayD2Ev(%class.btTriangleIndexVertexArray* %1) #6
  ret %class.btTriangleMesh* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN14btTriangleMeshD0Ev(%class.btTriangleMesh* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleMesh*, align 4
  store %class.btTriangleMesh* %this, %class.btTriangleMesh** %this.addr, align 4
  %this1 = load %class.btTriangleMesh*, %class.btTriangleMesh** %this.addr, align 4
  %call = call %class.btTriangleMesh* @_ZN14btTriangleMeshD2Ev(%class.btTriangleMesh* %this1) #6
  %0 = bitcast %class.btTriangleMesh* %this1 to i8*
  call void @_ZN26btTriangleIndexVertexArraydlEPv(i8* %0) #6
  ret void
}

declare void @_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_(%class.btStridingMeshInterface*, %class.btInternalTriangleIndexCallback*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i(%class.btTriangleIndexVertexArray*, i8**, i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i8**, i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32) unnamed_addr #3

declare void @_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i(%class.btTriangleIndexVertexArray*, i8**, i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i8**, i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32* nonnull align 4 dereferenceable(4), i32) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi(%class.btTriangleIndexVertexArray* %this, i32 %subpart) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %subpart.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store i32 %subpart, i32* %subpart.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi(%class.btTriangleIndexVertexArray* %this, i32 %subpart) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  %subpart.addr = alloca i32, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  store i32 %subpart, i32* %subpart.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv(%class.btTriangleIndexVertexArray* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTriangleIndexVertexArray*, align 4
  store %class.btTriangleIndexVertexArray* %this, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %this1 = load %class.btTriangleIndexVertexArray*, %class.btTriangleIndexVertexArray** %this.addr, align 4
  %m_indexedMeshes = getelementptr inbounds %class.btTriangleIndexVertexArray, %class.btTriangleIndexVertexArray* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %m_indexedMeshes)
  ret i32 %call
}

declare zeroext i1 @_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv(%class.btTriangleIndexVertexArray*) unnamed_addr #3

declare void @_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_(%class.btTriangleIndexVertexArray*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16)) unnamed_addr #3

declare void @_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_(%class.btTriangleIndexVertexArray*, %class.btVector3*, %class.btVector3*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK23btStridingMeshInterface28calculateSerializeBufferSizeEv(%class.btStridingMeshInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  ret i32 28
}

declare i8* @_ZNK23btStridingMeshInterface9serializeEPvP12btSerializer(%class.btStridingMeshInterface*, i8*, %class.btSerializer*) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btStridingMeshInterface* @_ZN23btStridingMeshInterfaceC2Ev(%class.btStridingMeshInterface* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btStridingMeshInterface*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btStridingMeshInterface* %this, %class.btStridingMeshInterface** %this.addr, align 4
  %this1 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %this.addr, align 4
  %0 = bitcast %class.btStridingMeshInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [17 x i8*] }, { [17 x i8*] }* @_ZTV23btStridingMeshInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_scaling = getelementptr inbounds %class.btStridingMeshInterface, %class.btStridingMeshInterface* %this1, i32 0, i32 1
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %m_scaling, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret %class.btStridingMeshInterface* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI13btIndexedMeshEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI13btIndexedMeshLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI13btIndexedMeshE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI13btIndexedMeshLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13btIndexedMeshE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btIndexedMesh* null, %struct.btIndexedMesh** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayItED2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayItE5clearEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIfED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: nounwind
declare %class.btTriangleIndexVertexArray* @_ZN26btTriangleIndexVertexArrayD2Ev(%class.btTriangleIndexVertexArray* returned) unnamed_addr #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayItE5clearEv(%class.btAlignedObjectArray.12* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayItE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayItE7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayItE10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayItE4initEv(%class.btAlignedObjectArray.12* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayItE7destroyEii(%class.btAlignedObjectArray.12* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %3 = load i16*, i16** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i16, i16* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayItE10deallocateEv(%class.btAlignedObjectArray.12* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load i16*, i16** %m_data, align 4
  %tobool = icmp ne i16* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load i16*, i16** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorItLj16EE10deallocateEPt(%class.btAlignedAllocator.13* %m_allocator, i16* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store i16* null, i16** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayItE4initEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store i16* null, i16** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorItLj16EE10deallocateEPt(%class.btAlignedAllocator.13* %this, i16* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %ptr.addr = alloca i16*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  store i16* %ptr, i16** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load i16*, i16** %ptr.addr, align 4
  %1 = bitcast i16* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj(%class.btAlignedAllocator.9* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj(%class.btAlignedAllocator.9* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE5clearEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIfE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load float*, float** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE10deallocateEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load float*, float** %m_data, align 4
  %tobool = icmp ne float* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load float*, float** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.5* %m_allocator, float* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store float* null, float** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIfE4initEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store float* null, float** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIfLj16EE10deallocateEPf(%class.btAlignedAllocator.5* %this, float* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca float*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store float* %ptr, float** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load float*, float** %ptr.addr, align 4
  %1 = bitcast float* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %class.btVector3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN26btTriangleIndexVertexArraydlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13btIndexedMeshE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btIndexedMesh*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI13btIndexedMeshE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btIndexedMesh*
  store %struct.btIndexedMesh* %2, %struct.btIndexedMesh** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %struct.btIndexedMesh* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI13btIndexedMeshE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI13btIndexedMeshE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btIndexedMesh* %4, %struct.btIndexedMesh** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI13btIndexedMeshE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN13btIndexedMeshnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI13btIndexedMeshE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btIndexedMesh* @_ZN18btAlignedAllocatorI13btIndexedMeshLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %struct.btIndexedMesh** null)
  %2 = bitcast %struct.btIndexedMesh* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI13btIndexedMeshE4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %struct.btIndexedMesh* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btIndexedMesh*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btIndexedMesh* %dest, %struct.btIndexedMesh** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %3, i32 %4
  %5 = bitcast %struct.btIndexedMesh* %arrayidx to i8*
  %call = call i8* @_ZN13btIndexedMeshnwEmPv(i32 32, i8* %5)
  %6 = bitcast i8* %call to %struct.btIndexedMesh*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %7, i32 %8
  %9 = bitcast %struct.btIndexedMesh* %6 to i8*
  %10 = bitcast %struct.btIndexedMesh* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 32, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13btIndexedMeshE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btIndexedMesh, %struct.btIndexedMesh* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI13btIndexedMeshE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %m_data, align 4
  %tobool = icmp ne %struct.btIndexedMesh* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI13btIndexedMeshLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.btIndexedMesh* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.btIndexedMesh* null, %struct.btIndexedMesh** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btIndexedMesh* @_ZN18btAlignedAllocatorI13btIndexedMeshLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %struct.btIndexedMesh** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btIndexedMesh**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btIndexedMesh** %hint, %struct.btIndexedMesh*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 32, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btIndexedMesh*
  ret %struct.btIndexedMesh* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI13btIndexedMeshLj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.btIndexedMesh* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.btIndexedMesh*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %struct.btIndexedMesh* %ptr, %struct.btIndexedMesh** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.btIndexedMesh*, %struct.btIndexedMesh** %ptr.addr, align 4
  %1 = bitcast %struct.btIndexedMesh* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIfLj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorIjLj16EEC2Ev(%class.btAlignedAllocator.9* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  ret %class.btAlignedAllocator.9* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorItLj16EEC2Ev(%class.btAlignedAllocator.13* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  ret %class.btAlignedAllocator.13* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIjE8capacityEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIjE9allocSizeEi(%class.btAlignedObjectArray.8* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayItE8capacityEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayItE9allocSizeEi(%class.btAlignedObjectArray.12* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIfE8capacityEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIfE9allocSizeEi(%class.btAlignedObjectArray.4* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %class.btVector3* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %9 = bitcast %class.btVector3* %6 to i8*
  %10 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.1* %this, i32 %n, %class.btVector3** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIfE8allocateEi(%class.btAlignedObjectArray.4* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.5* %m_allocator, i32 %1, float** null)
  %2 = bitcast float* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIfE4copyEiiPf(%class.btAlignedObjectArray.4* %this, i32 %start, i32 %end, float* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca float*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store float* %dest, float** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load float*, float** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %3, i32 %4
  %5 = bitcast float* %arrayidx to i8*
  %6 = bitcast i8* %5 to float*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %7 = load float*, float** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds float, float* %7, i32 %8
  %9 = load float, float* %arrayidx2, align 4
  store float %9, float* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float* @_ZN18btAlignedAllocatorIfLj16EE8allocateEiPPKf(%class.btAlignedAllocator.5* %this, i32 %n, float** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca float**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store float** %hint, float*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to float*
  ret float* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIjE8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32* @_ZN18btAlignedAllocatorIjLj16EE8allocateEiPPKj(%class.btAlignedAllocator.9* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIjE4copyEiiPj(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i32* %dest, i32** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = bitcast i32* %arrayidx to i8*
  %6 = bitcast i8* %5 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx2, align 4
  store i32 %9, i32* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIjLj16EE8allocateEiPPKj(%class.btAlignedAllocator.9* %this, i32 %n, i32** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32** %hint, i32*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayItE8allocateEi(%class.btAlignedObjectArray.12* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i16* @_ZN18btAlignedAllocatorItLj16EE8allocateEiPPKt(%class.btAlignedAllocator.13* %m_allocator, i32 %1, i16** null)
  %2 = bitcast i16* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayItE4copyEiiPt(%class.btAlignedObjectArray.12* %this, i32 %start, i32 %end, i16* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i16*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i16* %dest, i16** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i16*, i16** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i16, i16* %3, i32 %4
  %5 = bitcast i16* %arrayidx to i8*
  %6 = bitcast i8* %5 to i16*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %7 = load i16*, i16** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i16, i16* %7, i32 %8
  %9 = load i16, i16* %arrayidx2, align 2
  store i16 %9, i16* %6, align 2
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i16* @_ZN18btAlignedAllocatorItLj16EE8allocateEiPPKt(%class.btAlignedAllocator.13* %this, i32 %n, i16** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i16**, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i16** %hint, i16*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 2, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i16*
  ret i16* %1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btTriangleMesh.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind willreturn }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
