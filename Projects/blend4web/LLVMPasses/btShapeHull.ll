; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btShapeHull.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btShapeHull.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btVector3 = type { [4 x float] }
%class.btShapeHull = type { %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, i32, %class.btConvexShape* }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.HullDesc = type { i32, i32, %class.btVector3*, i32, float, i32, i32 }
%class.HullLibrary = type { %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btHullTriangle**, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btHullTriangle = type opaque
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%class.HullResult = type { i8, i32, %class.btAlignedObjectArray, i32, i32, %class.btAlignedObjectArray.0 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayIjEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayIjE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIjED2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN8HullDescC2Ev = comdat any

$_ZN11HullLibraryC2Ev = comdat any

$_ZN10HullResultC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZN20btAlignedObjectArrayIjE6resizeEiRKj = comdat any

$_ZN20btAlignedObjectArrayIjEixEi = comdat any

$_ZN10HullResultD2Ev = comdat any

$_ZN11HullLibraryD2Ev = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN18btAlignedAllocatorIP14btHullTriangleLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleED2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorIjLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIjE4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIjE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIjE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIjE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIjE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIjE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIjE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIjE4copyEiiPj = comdat any

$_ZN18btAlignedAllocatorIjLj16EE8allocateEiPPKj = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints = internal global [62 x %class.btVector3] zeroinitializer, align 16
@_ZGVZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints = internal global i32 0, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btShapeHull.cpp, i8* null }]

@_ZN11btShapeHullC1EPK13btConvexShape = hidden unnamed_addr alias %class.btShapeHull* (%class.btShapeHull*, %class.btConvexShape*), %class.btShapeHull* (%class.btShapeHull*, %class.btConvexShape*)* @_ZN11btShapeHullC2EPK13btConvexShape
@_ZN11btShapeHullD1Ev = hidden unnamed_addr alias %class.btShapeHull* (%class.btShapeHull*), %class.btShapeHull* (%class.btShapeHull*)* @_ZN11btShapeHullD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btShapeHull* @_ZN11btShapeHullC2EPK13btConvexShape(%class.btShapeHull* returned %this, %class.btConvexShape* %shape) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btShapeHull*, align 4
  %shape.addr = alloca %class.btConvexShape*, align 4
  store %class.btShapeHull* %this, %class.btShapeHull** %this.addr, align 4
  store %class.btConvexShape* %shape, %class.btConvexShape** %shape.addr, align 4
  %this1 = load %class.btShapeHull*, %class.btShapeHull** %this.addr, align 4
  %m_vertices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %m_vertices)
  %m_indices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.0* %m_indices)
  %0 = load %class.btConvexShape*, %class.btConvexShape** %shape.addr, align 4
  %m_shape = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 3
  store %class.btConvexShape* %0, %class.btConvexShape** %m_shape, align 4
  %m_vertices3 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %m_vertices3)
  %m_indices4 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.0* %m_indices4)
  %m_numIndices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 2
  store i32 0, i32* %m_numIndices, align 4
  ret %class.btShapeHull* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIjLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btShapeHull* @_ZN11btShapeHullD2Ev(%class.btShapeHull* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btShapeHull*, align 4
  store %class.btShapeHull* %this, %class.btShapeHull** %this.addr, align 4
  %this1 = load %class.btShapeHull*, %class.btShapeHull** %this.addr, align 4
  %m_indices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.0* %m_indices)
  %m_vertices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %m_vertices)
  %m_indices2 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.0* %m_indices2) #5
  %m_vertices3 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 0
  %call4 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %m_vertices3) #5
  ret %class.btShapeHull* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIjE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN11btShapeHull9buildHullEf(%class.btShapeHull* %this, float %0) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btShapeHull*, align 4
  %.addr = alloca float, align 4
  %numSampleDirections = alloca i32, align 4
  %numPDA = alloca i32, align 4
  %i = alloca i32, align 4
  %norm = alloca %class.btVector3, align 4
  %supportPoints = alloca [62 x %class.btVector3], align 16
  %i9 = alloca i32, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %hd = alloca %class.HullDesc, align 4
  %hl = alloca %class.HullLibrary, align 4
  %hr = alloca %class.HullResult, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %ref.tmp30 = alloca %class.btVector3, align 4
  %ref.tmp43 = alloca i32, align 4
  store %class.btShapeHull* %this, %class.btShapeHull** %this.addr, align 4
  store float %0, float* %.addr, align 4
  %this1 = load %class.btShapeHull*, %class.btShapeHull** %this.addr, align 4
  store i32 42, i32* %numSampleDirections, align 4
  %m_shape = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 3
  %1 = load %class.btConvexShape*, %class.btConvexShape** %m_shape, align 4
  %2 = bitcast %class.btConvexShape* %1 to i32 (%class.btConvexShape*)***
  %vtable = load i32 (%class.btConvexShape*)**, i32 (%class.btConvexShape*)*** %2, align 4
  %vfn = getelementptr inbounds i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vtable, i64 21
  %3 = load i32 (%class.btConvexShape*)*, i32 (%class.btConvexShape*)** %vfn, align 4
  %call = call i32 %3(%class.btConvexShape* %1)
  store i32 %call, i32* %numPDA, align 4
  %4 = load i32, i32* %numPDA, align 4
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %5 = load i32, i32* %i, align 4
  %6 = load i32, i32* %numPDA, align 4
  %cmp = icmp slt i32 %5, %6
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %norm)
  %m_shape3 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 3
  %7 = load %class.btConvexShape*, %class.btConvexShape** %m_shape3, align 4
  %8 = load i32, i32* %i, align 4
  %9 = bitcast %class.btConvexShape* %7 to void (%class.btConvexShape*, i32, %class.btVector3*)***
  %vtable4 = load void (%class.btConvexShape*, i32, %class.btVector3*)**, void (%class.btConvexShape*, i32, %class.btVector3*)*** %9, align 4
  %vfn5 = getelementptr inbounds void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vtable4, i64 22
  %10 = load void (%class.btConvexShape*, i32, %class.btVector3*)*, void (%class.btConvexShape*, i32, %class.btVector3*)** %vfn5, align 4
  call void %10(%class.btConvexShape* %7, i32 %8, %class.btVector3* nonnull align 4 dereferenceable(16) %norm)
  %call6 = call %class.btVector3* @_ZN11btShapeHull19getUnitSpherePointsEv()
  %11 = load i32, i32* %numSampleDirections, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %call6, i32 %11
  %12 = bitcast %class.btVector3* %arrayidx to i8*
  %13 = bitcast %class.btVector3* %norm to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  %14 = load i32, i32* %numSampleDirections, align 4
  %inc = add nsw i32 %14, 1
  store i32 %inc, i32* %numSampleDirections, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %15 = load i32, i32* %i, align 4
  %inc7 = add nsw i32 %15, 1
  store i32 %inc7, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end

if.end:                                           ; preds = %for.end, %entry
  %array.begin = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportPoints, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 62
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %if.end
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %if.end ], [ %arrayctor.next, %arrayctor.loop ]
  %call8 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  store i32 0, i32* %i9, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc19, %arrayctor.cont
  %16 = load i32, i32* %i9, align 4
  %17 = load i32, i32* %numSampleDirections, align 4
  %cmp11 = icmp slt i32 %16, %17
  br i1 %cmp11, label %for.body12, label %for.end21

for.body12:                                       ; preds = %for.cond10
  %m_shape13 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 3
  %18 = load %class.btConvexShape*, %class.btConvexShape** %m_shape13, align 4
  %call14 = call %class.btVector3* @_ZN11btShapeHull19getUnitSpherePointsEv()
  %19 = load i32, i32* %i9, align 4
  %arrayidx15 = getelementptr inbounds %class.btVector3, %class.btVector3* %call14, i32 %19
  %20 = bitcast %class.btConvexShape* %18 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable16 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %20, align 4
  %vfn17 = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable16, i64 16
  %21 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn17, align 4
  call void %21(%class.btVector3* sret align 4 %ref.tmp, %class.btConvexShape* %18, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx15)
  %22 = load i32, i32* %i9, align 4
  %arrayidx18 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportPoints, i32 0, i32 %22
  %23 = bitcast %class.btVector3* %arrayidx18 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 16 %23, i8* align 4 %24, i32 16, i1 false)
  br label %for.inc19

for.inc19:                                        ; preds = %for.body12
  %25 = load i32, i32* %i9, align 4
  %inc20 = add nsw i32 %25, 1
  store i32 %inc20, i32* %i9, align 4
  br label %for.cond10

for.end21:                                        ; preds = %for.cond10
  %call22 = call %class.HullDesc* @_ZN8HullDescC2Ev(%class.HullDesc* %hd)
  %mFlags = getelementptr inbounds %class.HullDesc, %class.HullDesc* %hd, i32 0, i32 0
  store i32 1, i32* %mFlags, align 4
  %26 = load i32, i32* %numSampleDirections, align 4
  %mVcount = getelementptr inbounds %class.HullDesc, %class.HullDesc* %hd, i32 0, i32 1
  store i32 %26, i32* %mVcount, align 4
  %arrayidx23 = getelementptr inbounds [62 x %class.btVector3], [62 x %class.btVector3]* %supportPoints, i32 0, i32 0
  %mVertices = getelementptr inbounds %class.HullDesc, %class.HullDesc* %hd, i32 0, i32 2
  store %class.btVector3* %arrayidx23, %class.btVector3** %mVertices, align 4
  %mVertexStride = getelementptr inbounds %class.HullDesc, %class.HullDesc* %hd, i32 0, i32 3
  store i32 16, i32* %mVertexStride, align 4
  %call24 = call %class.HullLibrary* @_ZN11HullLibraryC2Ev(%class.HullLibrary* %hl)
  %call25 = call %class.HullResult* @_ZN10HullResultC2Ev(%class.HullResult* %hr)
  %call26 = call i32 @_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult(%class.HullLibrary* %hl, %class.HullDesc* nonnull align 4 dereferenceable(28) %hd, %class.HullResult* nonnull align 4 dereferenceable(56) %hr)
  %cmp27 = icmp eq i32 %call26, 1
  br i1 %cmp27, label %if.then28, label %if.end29

if.then28:                                        ; preds = %for.end21
  store i1 false, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end29:                                         ; preds = %for.end21
  %m_vertices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 0
  %mNumOutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %hr, i32 0, i32 1
  %27 = load i32, i32* %mNumOutputVertices, align 4
  %call31 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %ref.tmp30)
  call void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %m_vertices, i32 %27, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp30)
  store i32 0, i32* %i9, align 4
  br label %for.cond32

for.cond32:                                       ; preds = %for.inc39, %if.end29
  %28 = load i32, i32* %i9, align 4
  %mNumOutputVertices33 = getelementptr inbounds %class.HullResult, %class.HullResult* %hr, i32 0, i32 1
  %29 = load i32, i32* %mNumOutputVertices33, align 4
  %cmp34 = icmp slt i32 %28, %29
  br i1 %cmp34, label %for.body35, label %for.end41

for.body35:                                       ; preds = %for.cond32
  %m_OutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %hr, i32 0, i32 2
  %30 = load i32, i32* %i9, align 4
  %call36 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_OutputVertices, i32 %30)
  %m_vertices37 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 0
  %31 = load i32, i32* %i9, align 4
  %call38 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices37, i32 %31)
  %32 = bitcast %class.btVector3* %call38 to i8*
  %33 = bitcast %class.btVector3* %call36 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 16, i1 false)
  br label %for.inc39

for.inc39:                                        ; preds = %for.body35
  %34 = load i32, i32* %i9, align 4
  %inc40 = add nsw i32 %34, 1
  store i32 %inc40, i32* %i9, align 4
  br label %for.cond32

for.end41:                                        ; preds = %for.cond32
  %mNumIndices = getelementptr inbounds %class.HullResult, %class.HullResult* %hr, i32 0, i32 4
  %35 = load i32, i32* %mNumIndices, align 4
  %m_numIndices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 2
  store i32 %35, i32* %m_numIndices, align 4
  %m_indices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 1
  %m_numIndices42 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 2
  %36 = load i32, i32* %m_numIndices42, align 4
  store i32 0, i32* %ref.tmp43, align 4
  call void @_ZN20btAlignedObjectArrayIjE6resizeEiRKj(%class.btAlignedObjectArray.0* %m_indices, i32 %36, i32* nonnull align 4 dereferenceable(4) %ref.tmp43)
  store i32 0, i32* %i9, align 4
  br label %for.cond44

for.cond44:                                       ; preds = %for.inc51, %for.end41
  %37 = load i32, i32* %i9, align 4
  %m_numIndices45 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 2
  %38 = load i32, i32* %m_numIndices45, align 4
  %cmp46 = icmp slt i32 %37, %38
  br i1 %cmp46, label %for.body47, label %for.end53

for.body47:                                       ; preds = %for.cond44
  %m_Indices = getelementptr inbounds %class.HullResult, %class.HullResult* %hr, i32 0, i32 5
  %39 = load i32, i32* %i9, align 4
  %call48 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.0* %m_Indices, i32 %39)
  %40 = load i32, i32* %call48, align 4
  %m_indices49 = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 1
  %41 = load i32, i32* %i9, align 4
  %call50 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.0* %m_indices49, i32 %41)
  store i32 %40, i32* %call50, align 4
  br label %for.inc51

for.inc51:                                        ; preds = %for.body47
  %42 = load i32, i32* %i9, align 4
  %inc52 = add nsw i32 %42, 1
  store i32 %inc52, i32* %i9, align 4
  br label %for.cond44

for.end53:                                        ; preds = %for.cond44
  %call54 = call i32 @_ZN11HullLibrary13ReleaseResultER10HullResult(%class.HullLibrary* %hl, %class.HullResult* nonnull align 4 dereferenceable(56) %hr)
  store i1 true, i1* %retval, align 1
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %for.end53, %if.then28
  %call55 = call %class.HullResult* @_ZN10HullResultD2Ev(%class.HullResult* %hr) #5
  %call57 = call %class.HullLibrary* @_ZN11HullLibraryD2Ev(%class.HullLibrary* %hl) #5
  %43 = load i1, i1* %retval, align 1
  ret i1 %43
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btVector3* @_ZN11btShapeHull19getUnitSpherePointsEv() #2 {
entry:
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp12 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp16 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp19 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp25 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp29 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp32 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp35 = alloca float, align 4
  %ref.tmp36 = alloca float, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp40 = alloca float, align 4
  %ref.tmp41 = alloca float, align 4
  %ref.tmp43 = alloca float, align 4
  %ref.tmp44 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp48 = alloca float, align 4
  %ref.tmp49 = alloca float, align 4
  %ref.tmp51 = alloca float, align 4
  %ref.tmp52 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %ref.tmp55 = alloca float, align 4
  %ref.tmp56 = alloca float, align 4
  %ref.tmp57 = alloca float, align 4
  %ref.tmp59 = alloca float, align 4
  %ref.tmp60 = alloca float, align 4
  %ref.tmp61 = alloca float, align 4
  %ref.tmp63 = alloca float, align 4
  %ref.tmp64 = alloca float, align 4
  %ref.tmp65 = alloca float, align 4
  %ref.tmp67 = alloca float, align 4
  %ref.tmp68 = alloca float, align 4
  %ref.tmp69 = alloca float, align 4
  %ref.tmp71 = alloca float, align 4
  %ref.tmp72 = alloca float, align 4
  %ref.tmp73 = alloca float, align 4
  %ref.tmp75 = alloca float, align 4
  %ref.tmp76 = alloca float, align 4
  %ref.tmp77 = alloca float, align 4
  %ref.tmp79 = alloca float, align 4
  %ref.tmp80 = alloca float, align 4
  %ref.tmp81 = alloca float, align 4
  %ref.tmp83 = alloca float, align 4
  %ref.tmp84 = alloca float, align 4
  %ref.tmp85 = alloca float, align 4
  %ref.tmp87 = alloca float, align 4
  %ref.tmp88 = alloca float, align 4
  %ref.tmp89 = alloca float, align 4
  %ref.tmp91 = alloca float, align 4
  %ref.tmp92 = alloca float, align 4
  %ref.tmp93 = alloca float, align 4
  %ref.tmp95 = alloca float, align 4
  %ref.tmp96 = alloca float, align 4
  %ref.tmp97 = alloca float, align 4
  %ref.tmp99 = alloca float, align 4
  %ref.tmp100 = alloca float, align 4
  %ref.tmp101 = alloca float, align 4
  %ref.tmp103 = alloca float, align 4
  %ref.tmp104 = alloca float, align 4
  %ref.tmp105 = alloca float, align 4
  %ref.tmp107 = alloca float, align 4
  %ref.tmp108 = alloca float, align 4
  %ref.tmp109 = alloca float, align 4
  %ref.tmp111 = alloca float, align 4
  %ref.tmp112 = alloca float, align 4
  %ref.tmp113 = alloca float, align 4
  %ref.tmp115 = alloca float, align 4
  %ref.tmp116 = alloca float, align 4
  %ref.tmp117 = alloca float, align 4
  %ref.tmp119 = alloca float, align 4
  %ref.tmp120 = alloca float, align 4
  %ref.tmp121 = alloca float, align 4
  %ref.tmp123 = alloca float, align 4
  %ref.tmp124 = alloca float, align 4
  %ref.tmp125 = alloca float, align 4
  %ref.tmp127 = alloca float, align 4
  %ref.tmp128 = alloca float, align 4
  %ref.tmp129 = alloca float, align 4
  %ref.tmp131 = alloca float, align 4
  %ref.tmp132 = alloca float, align 4
  %ref.tmp133 = alloca float, align 4
  %ref.tmp135 = alloca float, align 4
  %ref.tmp136 = alloca float, align 4
  %ref.tmp137 = alloca float, align 4
  %ref.tmp139 = alloca float, align 4
  %ref.tmp140 = alloca float, align 4
  %ref.tmp141 = alloca float, align 4
  %ref.tmp143 = alloca float, align 4
  %ref.tmp144 = alloca float, align 4
  %ref.tmp145 = alloca float, align 4
  %ref.tmp147 = alloca float, align 4
  %ref.tmp148 = alloca float, align 4
  %ref.tmp149 = alloca float, align 4
  %ref.tmp151 = alloca float, align 4
  %ref.tmp152 = alloca float, align 4
  %ref.tmp153 = alloca float, align 4
  %ref.tmp155 = alloca float, align 4
  %ref.tmp156 = alloca float, align 4
  %ref.tmp157 = alloca float, align 4
  %ref.tmp159 = alloca float, align 4
  %ref.tmp160 = alloca float, align 4
  %ref.tmp161 = alloca float, align 4
  %ref.tmp163 = alloca float, align 4
  %ref.tmp164 = alloca float, align 4
  %ref.tmp165 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints) #5
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float -0.000000e+00, float* %ref.tmp1, align 4
  store float -1.000000e+00, float* %ref.tmp2, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 0), float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  store float 0x3FE727CC00000000, float* %ref.tmp3, align 4
  store float 0xBFE0D2BD40000000, float* %ref.tmp4, align 4
  store float 0xBFDC9F3C80000000, float* %ref.tmp5, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 1), float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  store float 0xBFD1B05740000000, float* %ref.tmp7, align 4
  store float 0xBFEB388440000000, float* %ref.tmp8, align 4
  store float 0xBFDC9F3C80000000, float* %ref.tmp9, align 4
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 2), float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  store float 0xBFEC9F2340000000, float* %ref.tmp11, align 4
  store float -0.000000e+00, float* %ref.tmp12, align 4
  store float 0xBFDC9F2FE0000000, float* %ref.tmp13, align 4
  %call14 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 3), float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp12, float* nonnull align 4 dereferenceable(4) %ref.tmp13)
  store float 0xBFD1B05740000000, float* %ref.tmp15, align 4
  store float 0x3FEB388440000000, float* %ref.tmp16, align 4
  store float 0xBFDC9F40A0000000, float* %ref.tmp17, align 4
  %call18 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 4), float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp16, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  store float 0x3FE727CC00000000, float* %ref.tmp19, align 4
  store float 0x3FE0D2BD40000000, float* %ref.tmp20, align 4
  store float 0xBFDC9F3C80000000, float* %ref.tmp21, align 4
  %call22 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 5), float* nonnull align 4 dereferenceable(4) %ref.tmp19, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  store float 0x3FD1B05740000000, float* %ref.tmp23, align 4
  store float 0xBFEB388440000000, float* %ref.tmp24, align 4
  store float 0x3FDC9F40A0000000, float* %ref.tmp25, align 4
  %call26 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 6), float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp25)
  store float 0xBFE727CC00000000, float* %ref.tmp27, align 4
  store float 0xBFE0D2BD40000000, float* %ref.tmp28, align 4
  store float 0x3FDC9F3C80000000, float* %ref.tmp29, align 4
  %call30 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 7), float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp29)
  store float 0xBFE727CC00000000, float* %ref.tmp31, align 4
  store float 0x3FE0D2BD40000000, float* %ref.tmp32, align 4
  store float 0x3FDC9F3C80000000, float* %ref.tmp33, align 4
  %call34 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 8), float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp32, float* nonnull align 4 dereferenceable(4) %ref.tmp33)
  store float 0x3FD1B05740000000, float* %ref.tmp35, align 4
  store float 0x3FEB388440000000, float* %ref.tmp36, align 4
  store float 0x3FDC9F3C80000000, float* %ref.tmp37, align 4
  %call38 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 9), float* nonnull align 4 dereferenceable(4) %ref.tmp35, float* nonnull align 4 dereferenceable(4) %ref.tmp36, float* nonnull align 4 dereferenceable(4) %ref.tmp37)
  store float 0x3FEC9F2340000000, float* %ref.tmp39, align 4
  store float 0.000000e+00, float* %ref.tmp40, align 4
  store float 0x3FDC9F2FE0000000, float* %ref.tmp41, align 4
  %call42 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 10), float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp40, float* nonnull align 4 dereferenceable(4) %ref.tmp41)
  store float -0.000000e+00, float* %ref.tmp43, align 4
  store float 0.000000e+00, float* %ref.tmp44, align 4
  store float 1.000000e+00, float* %ref.tmp45, align 4
  %call46 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 11), float* nonnull align 4 dereferenceable(4) %ref.tmp43, float* nonnull align 4 dereferenceable(4) %ref.tmp44, float* nonnull align 4 dereferenceable(4) %ref.tmp45)
  store float 0x3FDB387E00000000, float* %ref.tmp47, align 4
  store float 0xBFD3C6D620000000, float* %ref.tmp48, align 4
  store float 0xBFEB388EC0000000, float* %ref.tmp49, align 4
  %call50 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 12), float* nonnull align 4 dereferenceable(4) %ref.tmp47, float* nonnull align 4 dereferenceable(4) %ref.tmp48, float* nonnull align 4 dereferenceable(4) %ref.tmp49)
  store float 0xBFC4CB5BC0000000, float* %ref.tmp51, align 4
  store float 0xBFDFFFEB00000000, float* %ref.tmp52, align 4
  store float 0xBFEB388EC0000000, float* %ref.tmp53, align 4
  %call54 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 13), float* nonnull align 4 dereferenceable(4) %ref.tmp51, float* nonnull align 4 dereferenceable(4) %ref.tmp52, float* nonnull align 4 dereferenceable(4) %ref.tmp53)
  store float 0x3FD0D2D880000000, float* %ref.tmp55, align 4
  store float 0xBFE9E36D20000000, float* %ref.tmp56, align 4
  store float 0xBFE0D2D880000000, float* %ref.tmp57, align 4
  %call58 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 14), float* nonnull align 4 dereferenceable(4) %ref.tmp55, float* nonnull align 4 dereferenceable(4) %ref.tmp56, float* nonnull align 4 dereferenceable(4) %ref.tmp57)
  store float 0x3FDB387E00000000, float* %ref.tmp59, align 4
  store float 0x3FD3C6D620000000, float* %ref.tmp60, align 4
  store float 0xBFEB388EC0000000, float* %ref.tmp61, align 4
  %call62 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 15), float* nonnull align 4 dereferenceable(4) %ref.tmp59, float* nonnull align 4 dereferenceable(4) %ref.tmp60, float* nonnull align 4 dereferenceable(4) %ref.tmp61)
  store float 0x3FEB388220000000, float* %ref.tmp63, align 4
  store float -0.000000e+00, float* %ref.tmp64, align 4
  store float 0xBFE0D2D440000000, float* %ref.tmp65, align 4
  %call66 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 16), float* nonnull align 4 dereferenceable(4) %ref.tmp63, float* nonnull align 4 dereferenceable(4) %ref.tmp64, float* nonnull align 4 dereferenceable(4) %ref.tmp65)
  store float 0xBFE0D2C7C0000000, float* %ref.tmp67, align 4
  store float -0.000000e+00, float* %ref.tmp68, align 4
  store float 0xBFEB388A80000000, float* %ref.tmp69, align 4
  %call70 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 17), float* nonnull align 4 dereferenceable(4) %ref.tmp67, float* nonnull align 4 dereferenceable(4) %ref.tmp68, float* nonnull align 4 dereferenceable(4) %ref.tmp69)
  store float 0xBFE605A700000000, float* %ref.tmp71, align 4
  store float 0xBFDFFFF360000000, float* %ref.tmp72, align 4
  store float 0xBFE0D2D440000000, float* %ref.tmp73, align 4
  %call74 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 18), float* nonnull align 4 dereferenceable(4) %ref.tmp71, float* nonnull align 4 dereferenceable(4) %ref.tmp72, float* nonnull align 4 dereferenceable(4) %ref.tmp73)
  store float 0xBFC4CB5BC0000000, float* %ref.tmp75, align 4
  store float 0x3FDFFFEB00000000, float* %ref.tmp76, align 4
  store float 0xBFEB388EC0000000, float* %ref.tmp77, align 4
  %call78 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 19), float* nonnull align 4 dereferenceable(4) %ref.tmp75, float* nonnull align 4 dereferenceable(4) %ref.tmp76, float* nonnull align 4 dereferenceable(4) %ref.tmp77)
  store float 0xBFE605A700000000, float* %ref.tmp79, align 4
  store float 0x3FDFFFF360000000, float* %ref.tmp80, align 4
  store float 0xBFE0D2D440000000, float* %ref.tmp81, align 4
  %call82 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 20), float* nonnull align 4 dereferenceable(4) %ref.tmp79, float* nonnull align 4 dereferenceable(4) %ref.tmp80, float* nonnull align 4 dereferenceable(4) %ref.tmp81)
  store float 0x3FD0D2D880000000, float* %ref.tmp83, align 4
  store float 0x3FE9E36D20000000, float* %ref.tmp84, align 4
  store float 0xBFE0D2D880000000, float* %ref.tmp85, align 4
  %call86 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 21), float* nonnull align 4 dereferenceable(4) %ref.tmp83, float* nonnull align 4 dereferenceable(4) %ref.tmp84, float* nonnull align 4 dereferenceable(4) %ref.tmp85)
  store float 0x3FEE6F1120000000, float* %ref.tmp87, align 4
  store float 0x3FD3C6DE80000000, float* %ref.tmp88, align 4
  store float 0.000000e+00, float* %ref.tmp89, align 4
  %call90 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 22), float* nonnull align 4 dereferenceable(4) %ref.tmp87, float* nonnull align 4 dereferenceable(4) %ref.tmp88, float* nonnull align 4 dereferenceable(4) %ref.tmp89)
  store float 0x3FEE6F1120000000, float* %ref.tmp91, align 4
  store float 0xBFD3C6DE80000000, float* %ref.tmp92, align 4
  store float 0.000000e+00, float* %ref.tmp93, align 4
  %call94 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 23), float* nonnull align 4 dereferenceable(4) %ref.tmp91, float* nonnull align 4 dereferenceable(4) %ref.tmp92, float* nonnull align 4 dereferenceable(4) %ref.tmp93)
  store float 0x3FE2CF24A0000000, float* %ref.tmp95, align 4
  store float 0xBFE9E377A0000000, float* %ref.tmp96, align 4
  store float 0.000000e+00, float* %ref.tmp97, align 4
  %call98 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 24), float* nonnull align 4 dereferenceable(4) %ref.tmp95, float* nonnull align 4 dereferenceable(4) %ref.tmp96, float* nonnull align 4 dereferenceable(4) %ref.tmp97)
  store float 0.000000e+00, float* %ref.tmp99, align 4
  store float -1.000000e+00, float* %ref.tmp100, align 4
  store float 0.000000e+00, float* %ref.tmp101, align 4
  %call102 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 25), float* nonnull align 4 dereferenceable(4) %ref.tmp99, float* nonnull align 4 dereferenceable(4) %ref.tmp100, float* nonnull align 4 dereferenceable(4) %ref.tmp101)
  store float 0xBFE2CF24A0000000, float* %ref.tmp103, align 4
  store float 0xBFE9E377A0000000, float* %ref.tmp104, align 4
  store float 0.000000e+00, float* %ref.tmp105, align 4
  %call106 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 26), float* nonnull align 4 dereferenceable(4) %ref.tmp103, float* nonnull align 4 dereferenceable(4) %ref.tmp104, float* nonnull align 4 dereferenceable(4) %ref.tmp105)
  store float 0xBFEE6F1120000000, float* %ref.tmp107, align 4
  store float 0xBFD3C6DE80000000, float* %ref.tmp108, align 4
  store float -0.000000e+00, float* %ref.tmp109, align 4
  %call110 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 27), float* nonnull align 4 dereferenceable(4) %ref.tmp107, float* nonnull align 4 dereferenceable(4) %ref.tmp108, float* nonnull align 4 dereferenceable(4) %ref.tmp109)
  store float 0xBFEE6F1120000000, float* %ref.tmp111, align 4
  store float 0x3FD3C6DE80000000, float* %ref.tmp112, align 4
  store float -0.000000e+00, float* %ref.tmp113, align 4
  %call114 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 28), float* nonnull align 4 dereferenceable(4) %ref.tmp111, float* nonnull align 4 dereferenceable(4) %ref.tmp112, float* nonnull align 4 dereferenceable(4) %ref.tmp113)
  store float 0xBFE2CF24A0000000, float* %ref.tmp115, align 4
  store float 0x3FE9E377A0000000, float* %ref.tmp116, align 4
  store float -0.000000e+00, float* %ref.tmp117, align 4
  %call118 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 29), float* nonnull align 4 dereferenceable(4) %ref.tmp115, float* nonnull align 4 dereferenceable(4) %ref.tmp116, float* nonnull align 4 dereferenceable(4) %ref.tmp117)
  store float -0.000000e+00, float* %ref.tmp119, align 4
  store float 1.000000e+00, float* %ref.tmp120, align 4
  store float -0.000000e+00, float* %ref.tmp121, align 4
  %call122 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 30), float* nonnull align 4 dereferenceable(4) %ref.tmp119, float* nonnull align 4 dereferenceable(4) %ref.tmp120, float* nonnull align 4 dereferenceable(4) %ref.tmp121)
  store float 0x3FE2CF24A0000000, float* %ref.tmp123, align 4
  store float 0x3FE9E377A0000000, float* %ref.tmp124, align 4
  store float -0.000000e+00, float* %ref.tmp125, align 4
  %call126 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 31), float* nonnull align 4 dereferenceable(4) %ref.tmp123, float* nonnull align 4 dereferenceable(4) %ref.tmp124, float* nonnull align 4 dereferenceable(4) %ref.tmp125)
  store float 0x3FE605A700000000, float* %ref.tmp127, align 4
  store float 0xBFDFFFF360000000, float* %ref.tmp128, align 4
  store float 0x3FE0D2D440000000, float* %ref.tmp129, align 4
  %call130 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 32), float* nonnull align 4 dereferenceable(4) %ref.tmp127, float* nonnull align 4 dereferenceable(4) %ref.tmp128, float* nonnull align 4 dereferenceable(4) %ref.tmp129)
  store float 0xBFD0D2D880000000, float* %ref.tmp131, align 4
  store float 0xBFE9E36D20000000, float* %ref.tmp132, align 4
  store float 0x3FE0D2D880000000, float* %ref.tmp133, align 4
  %call134 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 33), float* nonnull align 4 dereferenceable(4) %ref.tmp131, float* nonnull align 4 dereferenceable(4) %ref.tmp132, float* nonnull align 4 dereferenceable(4) %ref.tmp133)
  store float 0xBFEB388220000000, float* %ref.tmp135, align 4
  store float 0.000000e+00, float* %ref.tmp136, align 4
  store float 0x3FE0D2D440000000, float* %ref.tmp137, align 4
  %call138 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 34), float* nonnull align 4 dereferenceable(4) %ref.tmp135, float* nonnull align 4 dereferenceable(4) %ref.tmp136, float* nonnull align 4 dereferenceable(4) %ref.tmp137)
  store float 0xBFD0D2D880000000, float* %ref.tmp139, align 4
  store float 0x3FE9E36D20000000, float* %ref.tmp140, align 4
  store float 0x3FE0D2D880000000, float* %ref.tmp141, align 4
  %call142 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 35), float* nonnull align 4 dereferenceable(4) %ref.tmp139, float* nonnull align 4 dereferenceable(4) %ref.tmp140, float* nonnull align 4 dereferenceable(4) %ref.tmp141)
  store float 0x3FE605A700000000, float* %ref.tmp143, align 4
  store float 0x3FDFFFF360000000, float* %ref.tmp144, align 4
  store float 0x3FE0D2D440000000, float* %ref.tmp145, align 4
  %call146 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 36), float* nonnull align 4 dereferenceable(4) %ref.tmp143, float* nonnull align 4 dereferenceable(4) %ref.tmp144, float* nonnull align 4 dereferenceable(4) %ref.tmp145)
  store float 0x3FE0D2C7C0000000, float* %ref.tmp147, align 4
  store float 0.000000e+00, float* %ref.tmp148, align 4
  store float 0x3FEB388A80000000, float* %ref.tmp149, align 4
  %call150 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 37), float* nonnull align 4 dereferenceable(4) %ref.tmp147, float* nonnull align 4 dereferenceable(4) %ref.tmp148, float* nonnull align 4 dereferenceable(4) %ref.tmp149)
  store float 0x3FC4CB5BC0000000, float* %ref.tmp151, align 4
  store float 0xBFDFFFEB00000000, float* %ref.tmp152, align 4
  store float 0x3FEB388EC0000000, float* %ref.tmp153, align 4
  %call154 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 38), float* nonnull align 4 dereferenceable(4) %ref.tmp151, float* nonnull align 4 dereferenceable(4) %ref.tmp152, float* nonnull align 4 dereferenceable(4) %ref.tmp153)
  store float 0xBFDB387E00000000, float* %ref.tmp155, align 4
  store float 0xBFD3C6D620000000, float* %ref.tmp156, align 4
  store float 0x3FEB388EC0000000, float* %ref.tmp157, align 4
  %call158 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 39), float* nonnull align 4 dereferenceable(4) %ref.tmp155, float* nonnull align 4 dereferenceable(4) %ref.tmp156, float* nonnull align 4 dereferenceable(4) %ref.tmp157)
  store float 0xBFDB387E00000000, float* %ref.tmp159, align 4
  store float 0x3FD3C6D620000000, float* %ref.tmp160, align 4
  store float 0x3FEB388EC0000000, float* %ref.tmp161, align 4
  %call162 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 40), float* nonnull align 4 dereferenceable(4) %ref.tmp159, float* nonnull align 4 dereferenceable(4) %ref.tmp160, float* nonnull align 4 dereferenceable(4) %ref.tmp161)
  store float 0x3FC4CB5BC0000000, float* %ref.tmp163, align 4
  store float 0x3FDFFFEB00000000, float* %ref.tmp164, align 4
  store float 0x3FEB388EC0000000, float* %ref.tmp165, align 4
  %call166 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 41), float* nonnull align 4 dereferenceable(4) %ref.tmp163, float* nonnull align 4 dereferenceable(4) %ref.tmp164, float* nonnull align 4 dereferenceable(4) %ref.tmp165)
  br label %arrayinit.body

arrayinit.body:                                   ; preds = %arrayinit.body, %init
  %arrayinit.cur = phi %class.btVector3* [ getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 42), %init ], [ %arrayinit.next, %arrayinit.body ]
  %call167 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayinit.cur)
  %arrayinit.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayinit.cur, i32 1
  %arrayinit.done = icmp eq %class.btVector3* %arrayinit.next, getelementptr inbounds (%class.btVector3, %class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 0), i32 62)
  br i1 %arrayinit.done, label %arrayinit.end, label %arrayinit.body

arrayinit.end:                                    ; preds = %arrayinit.body
  call void @__cxa_guard_release(i32* @_ZGVZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints) #5
  br label %init.end

init.end:                                         ; preds = %arrayinit.end, %init.check, %entry
  ret %class.btVector3* getelementptr inbounds ([62 x %class.btVector3], [62 x %class.btVector3]* @_ZZN11btShapeHull19getUnitSpherePointsEvE17sUnitSpherePoints, i32 0, i32 0)
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.HullDesc* @_ZN8HullDescC2Ev(%class.HullDesc* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.HullDesc*, align 4
  store %class.HullDesc* %this, %class.HullDesc** %this.addr, align 4
  %this1 = load %class.HullDesc*, %class.HullDesc** %this.addr, align 4
  %mFlags = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 0
  store i32 1, i32* %mFlags, align 4
  %mVcount = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 1
  store i32 0, i32* %mVcount, align 4
  %mVertices = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 2
  store %class.btVector3* null, %class.btVector3** %mVertices, align 4
  %mVertexStride = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 3
  store i32 16, i32* %mVertexStride, align 4
  %mNormalEpsilon = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 4
  store float 0x3F50624DE0000000, float* %mNormalEpsilon, align 4
  %mMaxVertices = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 5
  store i32 4096, i32* %mMaxVertices, align 4
  %mMaxFaces = getelementptr inbounds %class.HullDesc, %class.HullDesc* %this1, i32 0, i32 6
  store i32 4096, i32* %mMaxFaces, align 4
  ret %class.HullDesc* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.HullLibrary* @_ZN11HullLibraryC2Ev(%class.HullLibrary* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP14btHullTriangleEC2Ev(%class.btAlignedObjectArray.4* %m_tris)
  %m_vertexIndexMapping = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.8* %m_vertexIndexMapping)
  ret %class.HullLibrary* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.HullResult* @_ZN10HullResultC2Ev(%class.HullResult* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.HullResult*, align 4
  store %class.HullResult* %this, %class.HullResult** %this.addr, align 4
  %this1 = load %class.HullResult*, %class.HullResult** %this.addr, align 4
  %m_OutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 2
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %m_OutputVertices)
  %m_Indices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 5
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIjEC2Ev(%class.btAlignedObjectArray.0* %m_Indices)
  %mPolygons = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 0
  store i8 1, i8* %mPolygons, align 4
  %mNumOutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 1
  store i32 0, i32* %mNumOutputVertices, align 4
  %mNumFaces = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 3
  store i32 0, i32* %mNumFaces, align 4
  %mNumIndices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 4
  store i32 0, i32* %mNumIndices, align 4
  ret %class.HullResult* %this1
}

declare i32 @_ZN11HullLibrary16CreateConvexHullERK8HullDescR10HullResult(%class.HullLibrary*, %class.HullDesc* nonnull align 4 dereferenceable(28), %class.HullResult* nonnull align 4 dereferenceable(56)) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E6resizeEiRKS0_(%class.btAlignedObjectArray* %this, i32 %newsize, %class.btVector3* nonnull align 4 dereferenceable(16) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btVector3*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btVector3* %fillData, %class.btVector3** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %5 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end15

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc12, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end14

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %14 = load %class.btVector3*, %class.btVector3** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btVector3, %class.btVector3* %14, i32 %15
  %16 = bitcast %class.btVector3* %arrayidx10 to i8*
  %call11 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %16)
  %17 = bitcast i8* %call11 to %class.btVector3*
  %18 = load %class.btVector3*, %class.btVector3** %fillData.addr, align 4
  %19 = bitcast %class.btVector3* %17 to i8*
  %20 = bitcast %class.btVector3* %18 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %for.inc12

for.inc12:                                        ; preds = %for.body8
  %21 = load i32, i32* %i5, align 4
  %inc13 = add nsw i32 %21, 1
  store i32 %inc13, i32* %i5, align 4
  br label %for.cond6

for.end14:                                        ; preds = %for.cond6
  br label %if.end15

if.end15:                                         ; preds = %for.end14, %for.end
  %22 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 %22, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE6resizeEiRKj(%class.btAlignedObjectArray.0* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store i32* %fillData, i32** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %5 = load i32*, i32** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIjE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %14 = load i32*, i32** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %14, i32 %15
  %16 = bitcast i32* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to i32*
  %18 = load i32*, i32** %fillData.addr, align 4
  %19 = load i32, i32* %18, align 4
  store i32 %19, i32* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIjEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

declare i32 @_ZN11HullLibrary13ReleaseResultER10HullResult(%class.HullLibrary*, %class.HullResult* nonnull align 4 dereferenceable(56)) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.HullResult* @_ZN10HullResultD2Ev(%class.HullResult* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.HullResult*, align 4
  store %class.HullResult* %this, %class.HullResult** %this.addr, align 4
  %this1 = load %class.HullResult*, %class.HullResult** %this.addr, align 4
  %m_Indices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 5
  %call = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIjED2Ev(%class.btAlignedObjectArray.0* %m_Indices) #5
  %m_OutputVertices = getelementptr inbounds %class.HullResult, %class.HullResult* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %m_OutputVertices) #5
  ret %class.HullResult* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.HullLibrary* @_ZN11HullLibraryD2Ev(%class.HullLibrary* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.HullLibrary*, align 4
  store %class.HullLibrary* %this, %class.HullLibrary** %this.addr, align 4
  %this1 = load %class.HullLibrary*, %class.HullLibrary** %this.addr, align 4
  %m_vertexIndexMapping = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.8* %m_vertexIndexMapping) #5
  %m_tris = getelementptr inbounds %class.HullLibrary, %class.HullLibrary* %this1, i32 0, i32 0
  %call2 = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP14btHullTriangleED2Ev(%class.btAlignedObjectArray.4* %m_tris) #5
  ret %class.HullLibrary* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK11btShapeHull12numTrianglesEv(%class.btShapeHull* %this) #1 {
entry:
  %this.addr = alloca %class.btShapeHull*, align 4
  store %class.btShapeHull* %this, %class.btShapeHull** %this.addr, align 4
  %this1 = load %class.btShapeHull*, %class.btShapeHull** %this.addr, align 4
  %m_numIndices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_numIndices, align 4
  %div = udiv i32 %0, 3
  ret i32 %div
}

; Function Attrs: noinline optnone
define hidden i32 @_ZNK11btShapeHull11numVerticesEv(%class.btShapeHull* %this) #2 {
entry:
  %this.addr = alloca %class.btShapeHull*, align 4
  store %class.btShapeHull* %this, %class.btShapeHull** %this.addr, align 4
  %this1 = load %class.btShapeHull*, %class.btShapeHull** %this.addr, align 4
  %m_vertices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_vertices)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define hidden i32 @_ZNK11btShapeHull10numIndicesEv(%class.btShapeHull* %this) #1 {
entry:
  %this.addr = alloca %class.btShapeHull*, align 4
  store %class.btShapeHull* %this, %class.btShapeHull** %this.addr, align 4
  %this1 = load %class.btShapeHull*, %class.btShapeHull** %this.addr, align 4
  %m_numIndices = getelementptr inbounds %class.btShapeHull, %class.btShapeHull* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_numIndices, align 4
  ret i32 %0
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP14btHullTriangleEC2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EEC2Ev(%class.btAlignedAllocator.5* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.9* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.5* @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EEC2Ev(%class.btAlignedAllocator.5* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  ret %class.btAlignedAllocator.5* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btHullTriangle** null, %class.btHullTriangle*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.9* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  ret %class.btAlignedAllocator.9* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP14btHullTriangleED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.9* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.9* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE5clearEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP14btHullTriangleE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btHullTriangle*, %class.btHullTriangle** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP14btHullTriangleE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP14btHullTriangleE10deallocateEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data, align 4
  %tobool = icmp ne %class.btHullTriangle** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btHullTriangle**, %class.btHullTriangle*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_(%class.btAlignedAllocator.5* %m_allocator, %class.btHullTriangle** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btHullTriangle** null, %class.btHullTriangle*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP14btHullTriangleLj16EE10deallocateEPS1_(%class.btAlignedAllocator.5* %this, %class.btHullTriangle** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca %class.btHullTriangle**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store %class.btHullTriangle** %ptr, %class.btHullTriangle*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load %class.btHullTriangle**, %class.btHullTriangle*** %ptr.addr, align 4
  %1 = bitcast %class.btHullTriangle** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIjLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.btVector3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj(%class.btAlignedAllocator.1* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIjLj16EE10deallocateEPj(%class.btAlignedAllocator.1* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %2, %class.btVector3** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %class.btVector3*, %class.btVector3** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btVector3* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* %4, %class.btVector3** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btVector3* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %9 = bitcast %class.btVector3* %6 to i8*
  %10 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.btVector3** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIjE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIjE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIjE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i32*
  store i32* %2, i32** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %3 = load i32*, i32** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIjE4copyEiiPj(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, i32* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIjE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIjE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIjE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i32*, i32** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store i32* %4, i32** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIjE8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIjE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32* @_ZN18btAlignedAllocatorIjLj16EE8allocateEiPPKj(%class.btAlignedAllocator.1* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIjE4copyEiiPj(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i32* %dest, i32** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = bitcast i32* %arrayidx to i8*
  %6 = bitcast i8* %5 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx2, align 4
  store i32 %9, i32* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIjLj16EE8allocateEiPPKj(%class.btAlignedAllocator.1* %this, i32 %n, i32** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32** %hint, i32*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btShapeHull.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1048575}
