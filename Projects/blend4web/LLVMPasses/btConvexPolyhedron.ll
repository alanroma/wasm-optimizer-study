; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btConvexPolyhedron.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btConvexPolyhedron.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btConvexPolyhedron = type { i32 (...)**, %class.btAlignedObjectArray, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %struct.btFace*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%struct.btFace = type { %class.btAlignedObjectArray.3, [4 x float] }
%class.btAlignedObjectArray.3 = type <{ %class.btAlignedAllocator.4, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.4 = type { i8 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btVector3 = type { [4 x float] }
%class.btHashMap = type { %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.12 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %struct.btInternalEdge*, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%struct.btInternalEdge = type { i16, i16 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %struct.btInternalVertexPair*, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%struct.btInternalVertexPair = type { i16, i16 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EC2Ev = comdat any

$_ZN20btAlignedObjectArrayI6btFaceEC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3ED2Ev = comdat any

$_ZN20btAlignedObjectArrayI6btFaceED2Ev = comdat any

$_ZN18btConvexPolyhedrondlEPv = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayI6btFaceEixEi = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeEC2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN20btAlignedObjectArrayI6btFaceEixEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN20btInternalVertexPairC2Ess = comdat any

$_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE4findERKS0_ = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_Z12IsAlmostZeroRK9btVector3 = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_ = comdat any

$_ZN14btInternalEdgeC2Ev = comdat any

$_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE6insertERKS0_RKS1_ = comdat any

$_ZNK9btVector35crossERKS_ = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZdvRK9btVector3RKf = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btFabsf = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeED2Ev = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_Z6btSwapIfEvRT_S1_ = comdat any

$_Z6btSwapI9btVector3EvRT_S2_ = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairEC2Ev = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN18btAlignedAllocatorI14btInternalEdgeLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE4initEv = comdat any

$_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE4initEv = comdat any

$_Z6btSwapIsEvRT_S1_ = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairED2Ev = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeED2Ev = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI14btInternalEdgeLj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E4initEv = comdat any

$_ZN18btAlignedAllocatorI6btFaceLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE4initEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E5clearEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_ = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayI6btFaceE10deallocateEv = comdat any

$_ZN6btFaceD2Ev = comdat any

$_ZN18btAlignedAllocatorI6btFaceLj16EE10deallocateEPS0_ = comdat any

$_ZNK9btHashMapI20btInternalVertexPair14btInternalEdgeE9findIndexERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeEixEi = comdat any

$_ZNK20btInternalVertexPair7getHashEv = comdat any

$_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv = comdat any

$_ZNK20btAlignedObjectArrayIiEixEi = comdat any

$_ZNK20btInternalVertexPair6equalsERKS_ = comdat any

$_ZNK20btAlignedObjectArrayI20btInternalVertexPairEixEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi = comdat any

$_ZN9btVector3nwEmPv = comdat any

$_ZN20btAlignedObjectArrayI9btVector3E8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE9push_backERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE9push_backERKS0_ = comdat any

$_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE10growTablesERKS0_ = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI14btInternalEdgeE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI14btInternalEdgeE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI14btInternalEdgeLj16EE8allocateEiPPKS0_ = comdat any

$_ZNK20btAlignedObjectArrayI20btInternalVertexPairE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4copyEiiPS0_ = comdat any

$_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EE8allocateEiPPKS0_ = comdat any

$_ZN20btAlignedObjectArrayIiE6resizeEiRKi = comdat any

$_ZN20btAlignedObjectArrayI20btInternalVertexPairEixEi = comdat any

$_ZN20btAlignedObjectArrayIiE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIiE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIiE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIiE4copyEiiPi = comdat any

$_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV18btConvexPolyhedron = hidden unnamed_addr constant { [4 x i8*] } { [4 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI18btConvexPolyhedron to i8*), i8* bitcast (%class.btConvexPolyhedron* (%class.btConvexPolyhedron*)* @_ZN18btConvexPolyhedronD1Ev to i8*), i8* bitcast (void (%class.btConvexPolyhedron*)* @_ZN18btConvexPolyhedronD0Ev to i8*)] }, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS18btConvexPolyhedron = hidden constant [21 x i8] c"18btConvexPolyhedron\00", align 1
@_ZTI18btConvexPolyhedron = hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @_ZTS18btConvexPolyhedron, i32 0, i32 0) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btConvexPolyhedron.cpp, i8* null }]

@_ZN18btConvexPolyhedronC1Ev = hidden unnamed_addr alias %class.btConvexPolyhedron* (%class.btConvexPolyhedron*), %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)* @_ZN18btConvexPolyhedronC2Ev
@_ZN18btConvexPolyhedronD1Ev = hidden unnamed_addr alias %class.btConvexPolyhedron* (%class.btConvexPolyhedron*), %class.btConvexPolyhedron* (%class.btConvexPolyhedron*)* @_ZN18btConvexPolyhedronD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btConvexPolyhedron* @_ZN18btConvexPolyhedronC2Ev(%class.btConvexPolyhedron* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvexPolyhedron*, align 4
  store %class.btConvexPolyhedron* %this, %class.btConvexPolyhedron** %this.addr, align 4
  %this1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %this.addr, align 4
  %0 = bitcast %class.btConvexPolyhedron* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [4 x i8*] }, { [4 x i8*] }* @_ZTV18btConvexPolyhedron, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_vertices = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %m_vertices)
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceEC2Ev(%class.btAlignedObjectArray.0* %m_faces)
  %m_uniqueEdges = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 3
  %call3 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* %m_uniqueEdges)
  %m_localCenter = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localCenter)
  %m_extents = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_extents)
  %mC = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 7
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mC)
  %mE = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 8
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %mE)
  ret %class.btConvexPolyhedron* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3EC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI6btFaceLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI6btFaceE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btConvexPolyhedron* @_ZN18btConvexPolyhedronD2Ev(%class.btConvexPolyhedron* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvexPolyhedron*, align 4
  store %class.btConvexPolyhedron* %this, %class.btConvexPolyhedron** %this.addr, align 4
  %this1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %this.addr, align 4
  %0 = bitcast %class.btConvexPolyhedron* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [4 x i8*] }, { [4 x i8*] }* @_ZTV18btConvexPolyhedron, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_uniqueEdges = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 3
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %m_uniqueEdges) #6
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceED2Ev(%class.btAlignedObjectArray.0* %m_faces) #6
  %m_vertices = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %call3 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* %m_vertices) #6
  ret %class.btConvexPolyhedron* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI9btVector3ED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI6btFaceED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI6btFaceE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN18btConvexPolyhedronD0Ev(%class.btConvexPolyhedron* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvexPolyhedron*, align 4
  store %class.btConvexPolyhedron* %this, %class.btConvexPolyhedron** %this.addr, align 4
  %this1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %this.addr, align 4
  %call = call %class.btConvexPolyhedron* @_ZN18btConvexPolyhedronD1Ev(%class.btConvexPolyhedron* %this1) #6
  %0 = bitcast %class.btConvexPolyhedron* %this1 to i8*
  call void @_ZN18btConvexPolyhedrondlEPv(i8* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btConvexPolyhedrondlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZNK18btConvexPolyhedron15testContainmentEv(%class.btConvexPolyhedron* %this) #2 {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btConvexPolyhedron*, align 4
  %p = alloca i32, align 4
  %LocalPt = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %ref.tmp14 = alloca %class.btVector3, align 4
  %ref.tmp16 = alloca %class.btVector3, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %ref.tmp37 = alloca float, align 4
  %ref.tmp49 = alloca %class.btVector3, align 4
  %ref.tmp51 = alloca %class.btVector3, align 4
  %ref.tmp55 = alloca float, align 4
  %ref.tmp60 = alloca float, align 4
  %ref.tmp69 = alloca %class.btVector3, align 4
  %ref.tmp71 = alloca %class.btVector3, align 4
  %ref.tmp72 = alloca float, align 4
  %ref.tmp87 = alloca %class.btVector3, align 4
  %ref.tmp89 = alloca %class.btVector3, align 4
  %ref.tmp90 = alloca float, align 4
  %ref.tmp98 = alloca float, align 4
  %ref.tmp107 = alloca %class.btVector3, align 4
  %ref.tmp109 = alloca %class.btVector3, align 4
  %ref.tmp110 = alloca float, align 4
  %ref.tmp115 = alloca float, align 4
  %ref.tmp127 = alloca %class.btVector3, align 4
  %ref.tmp129 = alloca %class.btVector3, align 4
  %ref.tmp130 = alloca float, align 4
  %ref.tmp135 = alloca float, align 4
  %ref.tmp140 = alloca float, align 4
  %i = alloca i32, align 4
  %Normal = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  store %class.btConvexPolyhedron* %this, %class.btConvexPolyhedron** %this.addr, align 4
  %this1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %this.addr, align 4
  store i32 0, i32* %p, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc177, %entry
  %0 = load i32, i32* %p, align 4
  %cmp = icmp slt i32 %0, 8
  br i1 %cmp, label %for.body, label %for.end179

for.body:                                         ; preds = %for.cond
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %LocalPt)
  %1 = load i32, i32* %p, align 4
  %cmp2 = icmp eq i32 %1, 0
  br i1 %cmp2, label %if.then, label %if.else

if.then:                                          ; preds = %for.body
  %m_localCenter = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %m_extents = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents)
  %arrayidx = getelementptr inbounds float, float* %call4, i32 0
  %m_extents5 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 1
  %m_extents8 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 2
  %call11 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp3, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx7, float* nonnull align 4 dereferenceable(4) %arrayidx10)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %2 = bitcast %class.btVector3* %LocalPt to i8*
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  br label %if.end152

if.else:                                          ; preds = %for.body
  %4 = load i32, i32* %p, align 4
  %cmp12 = icmp eq i32 %4, 1
  br i1 %cmp12, label %if.then13, label %if.else28

if.then13:                                        ; preds = %if.else
  %m_localCenter15 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %m_extents17 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 0
  %m_extents20 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call21 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents20)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 1
  %m_extents24 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call25 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents24)
  %arrayidx26 = getelementptr inbounds float, float* %call25, i32 2
  %5 = load float, float* %arrayidx26, align 4
  %fneg = fneg float %5
  store float %fneg, float* %ref.tmp23, align 4
  %call27 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp16, float* nonnull align 4 dereferenceable(4) %arrayidx19, float* nonnull align 4 dereferenceable(4) %arrayidx22, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp14, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter15, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp16)
  %6 = bitcast %class.btVector3* %LocalPt to i8*
  %7 = bitcast %class.btVector3* %ref.tmp14 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  br label %if.end151

if.else28:                                        ; preds = %if.else
  %8 = load i32, i32* %p, align 4
  %cmp29 = icmp eq i32 %8, 2
  br i1 %cmp29, label %if.then30, label %if.else46

if.then30:                                        ; preds = %if.else28
  %m_localCenter32 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %m_extents34 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call35 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents34)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 0
  %m_extents38 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call39 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents38)
  %arrayidx40 = getelementptr inbounds float, float* %call39, i32 1
  %9 = load float, float* %arrayidx40, align 4
  %fneg41 = fneg float %9
  store float %fneg41, float* %ref.tmp37, align 4
  %m_extents42 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents42)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 2
  %call45 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp33, float* nonnull align 4 dereferenceable(4) %arrayidx36, float* nonnull align 4 dereferenceable(4) %ref.tmp37, float* nonnull align 4 dereferenceable(4) %arrayidx44)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter32, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp33)
  %10 = bitcast %class.btVector3* %LocalPt to i8*
  %11 = bitcast %class.btVector3* %ref.tmp31 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  br label %if.end150

if.else46:                                        ; preds = %if.else28
  %12 = load i32, i32* %p, align 4
  %cmp47 = icmp eq i32 %12, 3
  br i1 %cmp47, label %if.then48, label %if.else66

if.then48:                                        ; preds = %if.else46
  %m_localCenter50 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %m_extents52 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call53 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents52)
  %arrayidx54 = getelementptr inbounds float, float* %call53, i32 0
  %m_extents56 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents56)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 1
  %13 = load float, float* %arrayidx58, align 4
  %fneg59 = fneg float %13
  store float %fneg59, float* %ref.tmp55, align 4
  %m_extents61 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call62 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents61)
  %arrayidx63 = getelementptr inbounds float, float* %call62, i32 2
  %14 = load float, float* %arrayidx63, align 4
  %fneg64 = fneg float %14
  store float %fneg64, float* %ref.tmp60, align 4
  %call65 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp51, float* nonnull align 4 dereferenceable(4) %arrayidx54, float* nonnull align 4 dereferenceable(4) %ref.tmp55, float* nonnull align 4 dereferenceable(4) %ref.tmp60)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp49, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter50, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp51)
  %15 = bitcast %class.btVector3* %LocalPt to i8*
  %16 = bitcast %class.btVector3* %ref.tmp49 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %15, i8* align 4 %16, i32 16, i1 false)
  br label %if.end149

if.else66:                                        ; preds = %if.else46
  %17 = load i32, i32* %p, align 4
  %cmp67 = icmp eq i32 %17, 4
  br i1 %cmp67, label %if.then68, label %if.else84

if.then68:                                        ; preds = %if.else66
  %m_localCenter70 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %m_extents73 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call74 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents73)
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 0
  %18 = load float, float* %arrayidx75, align 4
  %fneg76 = fneg float %18
  store float %fneg76, float* %ref.tmp72, align 4
  %m_extents77 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents77)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 1
  %m_extents80 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call81 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents80)
  %arrayidx82 = getelementptr inbounds float, float* %call81, i32 2
  %call83 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp71, float* nonnull align 4 dereferenceable(4) %ref.tmp72, float* nonnull align 4 dereferenceable(4) %arrayidx79, float* nonnull align 4 dereferenceable(4) %arrayidx82)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp69, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter70, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp71)
  %19 = bitcast %class.btVector3* %LocalPt to i8*
  %20 = bitcast %class.btVector3* %ref.tmp69 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %if.end148

if.else84:                                        ; preds = %if.else66
  %21 = load i32, i32* %p, align 4
  %cmp85 = icmp eq i32 %21, 5
  br i1 %cmp85, label %if.then86, label %if.else104

if.then86:                                        ; preds = %if.else84
  %m_localCenter88 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %m_extents91 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call92 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents91)
  %arrayidx93 = getelementptr inbounds float, float* %call92, i32 0
  %22 = load float, float* %arrayidx93, align 4
  %fneg94 = fneg float %22
  store float %fneg94, float* %ref.tmp90, align 4
  %m_extents95 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call96 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents95)
  %arrayidx97 = getelementptr inbounds float, float* %call96, i32 1
  %m_extents99 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call100 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents99)
  %arrayidx101 = getelementptr inbounds float, float* %call100, i32 2
  %23 = load float, float* %arrayidx101, align 4
  %fneg102 = fneg float %23
  store float %fneg102, float* %ref.tmp98, align 4
  %call103 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp89, float* nonnull align 4 dereferenceable(4) %ref.tmp90, float* nonnull align 4 dereferenceable(4) %arrayidx97, float* nonnull align 4 dereferenceable(4) %ref.tmp98)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp87, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter88, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp89)
  %24 = bitcast %class.btVector3* %LocalPt to i8*
  %25 = bitcast %class.btVector3* %ref.tmp87 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %24, i8* align 4 %25, i32 16, i1 false)
  br label %if.end147

if.else104:                                       ; preds = %if.else84
  %26 = load i32, i32* %p, align 4
  %cmp105 = icmp eq i32 %26, 6
  br i1 %cmp105, label %if.then106, label %if.else124

if.then106:                                       ; preds = %if.else104
  %m_localCenter108 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %m_extents111 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents111)
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 0
  %27 = load float, float* %arrayidx113, align 4
  %fneg114 = fneg float %27
  store float %fneg114, float* %ref.tmp110, align 4
  %m_extents116 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call117 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents116)
  %arrayidx118 = getelementptr inbounds float, float* %call117, i32 1
  %28 = load float, float* %arrayidx118, align 4
  %fneg119 = fneg float %28
  store float %fneg119, float* %ref.tmp115, align 4
  %m_extents120 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call121 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents120)
  %arrayidx122 = getelementptr inbounds float, float* %call121, i32 2
  %call123 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp109, float* nonnull align 4 dereferenceable(4) %ref.tmp110, float* nonnull align 4 dereferenceable(4) %ref.tmp115, float* nonnull align 4 dereferenceable(4) %arrayidx122)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp107, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter108, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp109)
  %29 = bitcast %class.btVector3* %LocalPt to i8*
  %30 = bitcast %class.btVector3* %ref.tmp107 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false)
  br label %if.end146

if.else124:                                       ; preds = %if.else104
  %31 = load i32, i32* %p, align 4
  %cmp125 = icmp eq i32 %31, 7
  br i1 %cmp125, label %if.then126, label %if.end

if.then126:                                       ; preds = %if.else124
  %m_localCenter128 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %m_extents131 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call132 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents131)
  %arrayidx133 = getelementptr inbounds float, float* %call132, i32 0
  %32 = load float, float* %arrayidx133, align 4
  %fneg134 = fneg float %32
  store float %fneg134, float* %ref.tmp130, align 4
  %m_extents136 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call137 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents136)
  %arrayidx138 = getelementptr inbounds float, float* %call137, i32 1
  %33 = load float, float* %arrayidx138, align 4
  %fneg139 = fneg float %33
  store float %fneg139, float* %ref.tmp135, align 4
  %m_extents141 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call142 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_extents141)
  %arrayidx143 = getelementptr inbounds float, float* %call142, i32 2
  %34 = load float, float* %arrayidx143, align 4
  %fneg144 = fneg float %34
  store float %fneg144, float* %ref.tmp140, align 4
  %call145 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp129, float* nonnull align 4 dereferenceable(4) %ref.tmp130, float* nonnull align 4 dereferenceable(4) %ref.tmp135, float* nonnull align 4 dereferenceable(4) %ref.tmp140)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp127, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localCenter128, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp129)
  %35 = bitcast %class.btVector3* %LocalPt to i8*
  %36 = bitcast %class.btVector3* %ref.tmp127 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %35, i8* align 4 %36, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then126, %if.else124
  br label %if.end146

if.end146:                                        ; preds = %if.end, %if.then106
  br label %if.end147

if.end147:                                        ; preds = %if.end146, %if.then86
  br label %if.end148

if.end148:                                        ; preds = %if.end147, %if.then68
  br label %if.end149

if.end149:                                        ; preds = %if.end148, %if.then48
  br label %if.end150

if.end150:                                        ; preds = %if.end149, %if.then30
  br label %if.end151

if.end151:                                        ; preds = %if.end150, %if.then13
  br label %if.end152

if.end152:                                        ; preds = %if.end151, %if.then
  store i32 0, i32* %i, align 4
  br label %for.cond153

for.cond153:                                      ; preds = %for.inc, %if.end152
  %37 = load i32, i32* %i, align 4
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %call154 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces)
  %cmp155 = icmp slt i32 %37, %call154
  br i1 %cmp155, label %for.body156, label %for.end

for.body156:                                      ; preds = %for.cond153
  %m_faces157 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %38 = load i32, i32* %i, align 4
  %call158 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces157, i32 %38)
  %m_plane = getelementptr inbounds %struct.btFace, %struct.btFace* %call158, i32 0, i32 1
  %arrayidx159 = getelementptr inbounds [4 x float], [4 x float]* %m_plane, i32 0, i32 0
  %m_faces160 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %39 = load i32, i32* %i, align 4
  %call161 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces160, i32 %39)
  %m_plane162 = getelementptr inbounds %struct.btFace, %struct.btFace* %call161, i32 0, i32 1
  %arrayidx163 = getelementptr inbounds [4 x float], [4 x float]* %m_plane162, i32 0, i32 1
  %m_faces164 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %40 = load i32, i32* %i, align 4
  %call165 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces164, i32 %40)
  %m_plane166 = getelementptr inbounds %struct.btFace, %struct.btFace* %call165, i32 0, i32 1
  %arrayidx167 = getelementptr inbounds [4 x float], [4 x float]* %m_plane166, i32 0, i32 2
  %call168 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %Normal, float* nonnull align 4 dereferenceable(4) %arrayidx159, float* nonnull align 4 dereferenceable(4) %arrayidx163, float* nonnull align 4 dereferenceable(4) %arrayidx167)
  %call169 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %LocalPt, %class.btVector3* nonnull align 4 dereferenceable(16) %Normal)
  %m_faces170 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %41 = load i32, i32* %i, align 4
  %call171 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces170, i32 %41)
  %m_plane172 = getelementptr inbounds %struct.btFace, %struct.btFace* %call171, i32 0, i32 1
  %arrayidx173 = getelementptr inbounds [4 x float], [4 x float]* %m_plane172, i32 0, i32 3
  %42 = load float, float* %arrayidx173, align 4
  %add = fadd float %call169, %42
  store float %add, float* %d, align 4
  %43 = load float, float* %d, align 4
  %cmp174 = fcmp ogt float %43, 0.000000e+00
  br i1 %cmp174, label %if.then175, label %if.end176

if.then175:                                       ; preds = %for.body156
  store i1 false, i1* %retval, align 1
  br label %return

if.end176:                                        ; preds = %for.body156
  br label %for.inc

for.inc:                                          ; preds = %if.end176
  %44 = load i32, i32* %i, align 4
  %inc = add nsw i32 %44, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond153

for.end:                                          ; preds = %for.cond153
  br label %for.inc177

for.inc177:                                       ; preds = %for.end
  %45 = load i32, i32* %p, align 4
  %inc178 = add nsw i32 %45, 1
  store i32 %inc178, i32* %p, align 4
  br label %for.cond

for.end179:                                       ; preds = %for.cond
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end179, %if.then175
  %46 = load i1, i1* %retval, align 1
  ret i1 %46
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %struct.btFace* @_ZNK20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btFace*, %struct.btFace** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %0, i32 %1
  ret %struct.btFace* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline optnone
define hidden void @_ZN18btConvexPolyhedron10initializeEv(%class.btConvexPolyhedron* %this) #2 {
entry:
  %this.addr = alloca %class.btConvexPolyhedron*, align 4
  %edges = alloca %class.btHashMap, align 4
  %TotalArea = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %i = alloca i32, align 4
  %numVertices = alloca i32, align 4
  %NbTris = alloca i32, align 4
  %j = alloca i32, align 4
  %k = alloca i32, align 4
  %vp = alloca %struct.btInternalVertexPair, align 2
  %edptr = alloca %struct.btInternalEdge*, align 4
  %edge = alloca %class.btVector3, align 4
  %found = alloca i8, align 1
  %p = alloca i32, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp36 = alloca %class.btVector3, align 4
  %ed = alloca %struct.btInternalEdge, align 2
  %i55 = alloca i32, align 4
  %numVertices61 = alloca i32, align 4
  %NbTris66 = alloca i32, align 4
  %p0 = alloca %class.btVector3*, align 4
  %j73 = alloca i32, align 4
  %k77 = alloca i32, align 4
  %p1 = alloca %class.btVector3*, align 4
  %p2 = alloca %class.btVector3*, align 4
  %Area = alloca float, align 4
  %ref.tmp92 = alloca %class.btVector3, align 4
  %ref.tmp93 = alloca %class.btVector3, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  %Center = alloca %class.btVector3, align 4
  %ref.tmp96 = alloca %class.btVector3, align 4
  %ref.tmp97 = alloca %class.btVector3, align 4
  %ref.tmp98 = alloca float, align 4
  %ref.tmp99 = alloca %class.btVector3, align 4
  %i111 = alloca i32, align 4
  %Normal = alloca %class.btVector3, align 4
  %dist = alloca float, align 4
  %MinX = alloca float, align 4
  %MinY = alloca float, align 4
  %MinZ = alloca float, align 4
  %MaxX = alloca float, align 4
  %MaxY = alloca float, align 4
  %MaxZ = alloca float, align 4
  %i144 = alloca i32, align 4
  %pt = alloca %class.btVector3*, align 4
  %ref.tmp185 = alloca float, align 4
  %ref.tmp187 = alloca float, align 4
  %ref.tmp189 = alloca float, align 4
  %ref.tmp191 = alloca float, align 4
  %ref.tmp193 = alloca float, align 4
  %ref.tmp195 = alloca float, align 4
  %r = alloca float, align 4
  %LargestExtent = alloca i32, align 4
  %Step = alloca float, align 4
  %FoundBox = alloca i8, align 1
  %j221 = alloca i32, align 4
  %Step247 = alloca float, align 4
  %e0 = alloca i32, align 4
  %e1 = alloca i32, align 4
  %j253 = alloca i32, align 4
  %Saved0 = alloca float, align 4
  %Saved1 = alloca float, align 4
  store %class.btConvexPolyhedron* %this, %class.btConvexPolyhedron** %this.addr, align 4
  %this1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %this.addr, align 4
  %call = call %class.btHashMap* @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeEC2Ev(%class.btHashMap* %edges)
  store float 0.000000e+00, float* %TotalArea, align 4
  %m_localCenter = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %m_localCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc52, %entry
  %0 = load i32, i32* %i, align 4
  %m_faces = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces)
  %cmp = icmp slt i32 %0, %call4
  br i1 %cmp, label %for.body, label %for.end54

for.body:                                         ; preds = %for.cond
  %m_faces5 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %1 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces5, i32 %1)
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %call6, i32 0, i32 0
  %call7 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_indices)
  store i32 %call7, i32* %numVertices, align 4
  %2 = load i32, i32* %numVertices, align 4
  store i32 %2, i32* %NbTris, align 4
  store i32 0, i32* %j, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc49, %for.body
  %3 = load i32, i32* %j, align 4
  %4 = load i32, i32* %NbTris, align 4
  %cmp9 = icmp slt i32 %3, %4
  br i1 %cmp9, label %for.body10, label %for.end51

for.body10:                                       ; preds = %for.cond8
  %5 = load i32, i32* %j, align 4
  %add = add nsw i32 %5, 1
  %6 = load i32, i32* %numVertices, align 4
  %rem = srem i32 %add, %6
  store i32 %rem, i32* %k, align 4
  %m_faces11 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %7 = load i32, i32* %i, align 4
  %call12 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces11, i32 %7)
  %m_indices13 = getelementptr inbounds %struct.btFace, %struct.btFace* %call12, i32 0, i32 0
  %8 = load i32, i32* %j, align 4
  %call14 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices13, i32 %8)
  %9 = load i32, i32* %call14, align 4
  %conv = trunc i32 %9 to i16
  %m_faces15 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %10 = load i32, i32* %i, align 4
  %call16 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces15, i32 %10)
  %m_indices17 = getelementptr inbounds %struct.btFace, %struct.btFace* %call16, i32 0, i32 0
  %11 = load i32, i32* %k, align 4
  %call18 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices17, i32 %11)
  %12 = load i32, i32* %call18, align 4
  %conv19 = trunc i32 %12 to i16
  %call20 = call %struct.btInternalVertexPair* @_ZN20btInternalVertexPairC2Ess(%struct.btInternalVertexPair* %vp, i16 signext %conv, i16 signext %conv19)
  %call21 = call %struct.btInternalEdge* @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE4findERKS0_(%class.btHashMap* %edges, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %vp)
  store %struct.btInternalEdge* %call21, %struct.btInternalEdge** %edptr, align 4
  %m_vertices = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %m_v1 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %vp, i32 0, i32 1
  %13 = load i16, i16* %m_v1, align 2
  %conv22 = sext i16 %13 to i32
  %call23 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices, i32 %conv22)
  %m_vertices24 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %m_v0 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %vp, i32 0, i32 0
  %14 = load i16, i16* %m_v0, align 2
  %conv25 = sext i16 %14 to i32
  %call26 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices24, i32 %conv25)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %edge, %class.btVector3* nonnull align 4 dereferenceable(16) %call23, %class.btVector3* nonnull align 4 dereferenceable(16) %call26)
  %call27 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %edge)
  store i8 0, i8* %found, align 1
  store i32 0, i32* %p, align 4
  br label %for.cond28

for.cond28:                                       ; preds = %for.inc, %for.body10
  %15 = load i32, i32* %p, align 4
  %m_uniqueEdges = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 3
  %call29 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_uniqueEdges)
  %cmp30 = icmp slt i32 %15, %call29
  br i1 %cmp30, label %for.body31, label %for.end

for.body31:                                       ; preds = %for.cond28
  %m_uniqueEdges33 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 3
  %16 = load i32, i32* %p, align 4
  %call34 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_uniqueEdges33, i32 %16)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp32, %class.btVector3* nonnull align 4 dereferenceable(16) %call34, %class.btVector3* nonnull align 4 dereferenceable(16) %edge)
  %call35 = call zeroext i1 @_Z12IsAlmostZeroRK9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp32)
  br i1 %call35, label %lor.end, label %lor.rhs

lor.rhs:                                          ; preds = %for.body31
  %m_uniqueEdges37 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 3
  %17 = load i32, i32* %p, align 4
  %call38 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_uniqueEdges37, i32 %17)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp36, %class.btVector3* nonnull align 4 dereferenceable(16) %call38, %class.btVector3* nonnull align 4 dereferenceable(16) %edge)
  %call39 = call zeroext i1 @_Z12IsAlmostZeroRK9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp36)
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %for.body31
  %18 = phi i1 [ true, %for.body31 ], [ %call39, %lor.rhs ]
  br i1 %18, label %if.then, label %if.end

if.then:                                          ; preds = %lor.end
  store i8 1, i8* %found, align 1
  br label %for.end

if.end:                                           ; preds = %lor.end
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %19 = load i32, i32* %p, align 4
  %inc = add nsw i32 %19, 1
  store i32 %inc, i32* %p, align 4
  br label %for.cond28

for.end:                                          ; preds = %if.then, %for.cond28
  %20 = load i8, i8* %found, align 1
  %tobool = trunc i8 %20 to i1
  br i1 %tobool, label %if.end42, label %if.then40

if.then40:                                        ; preds = %for.end
  %m_uniqueEdges41 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %m_uniqueEdges41, %class.btVector3* nonnull align 4 dereferenceable(16) %edge)
  br label %if.end42

if.end42:                                         ; preds = %if.then40, %for.end
  %21 = load %struct.btInternalEdge*, %struct.btInternalEdge** %edptr, align 4
  %tobool43 = icmp ne %struct.btInternalEdge* %21, null
  br i1 %tobool43, label %if.then44, label %if.else

if.then44:                                        ; preds = %if.end42
  %22 = load i32, i32* %i, align 4
  %conv45 = trunc i32 %22 to i16
  %23 = load %struct.btInternalEdge*, %struct.btInternalEdge** %edptr, align 4
  %m_face1 = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %23, i32 0, i32 1
  store i16 %conv45, i16* %m_face1, align 2
  br label %if.end48

if.else:                                          ; preds = %if.end42
  %call46 = call %struct.btInternalEdge* @_ZN14btInternalEdgeC2Ev(%struct.btInternalEdge* %ed)
  %24 = load i32, i32* %i, align 4
  %conv47 = trunc i32 %24 to i16
  %m_face0 = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %ed, i32 0, i32 0
  store i16 %conv47, i16* %m_face0, align 2
  call void @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE6insertERKS0_RKS1_(%class.btHashMap* %edges, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %vp, %struct.btInternalEdge* nonnull align 2 dereferenceable(4) %ed)
  br label %if.end48

if.end48:                                         ; preds = %if.else, %if.then44
  br label %for.inc49

for.inc49:                                        ; preds = %if.end48
  %25 = load i32, i32* %j, align 4
  %inc50 = add nsw i32 %25, 1
  store i32 %inc50, i32* %j, align 4
  br label %for.cond8

for.end51:                                        ; preds = %for.cond8
  br label %for.inc52

for.inc52:                                        ; preds = %for.end51
  %26 = load i32, i32* %i, align 4
  %inc53 = add nsw i32 %26, 1
  store i32 %inc53, i32* %i, align 4
  br label %for.cond

for.end54:                                        ; preds = %for.cond
  store i32 0, i32* %i55, align 4
  br label %for.cond56

for.cond56:                                       ; preds = %for.inc106, %for.end54
  %27 = load i32, i32* %i55, align 4
  %m_faces57 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %call58 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces57)
  %cmp59 = icmp slt i32 %27, %call58
  br i1 %cmp59, label %for.body60, label %for.end108

for.body60:                                       ; preds = %for.cond56
  %m_faces62 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %28 = load i32, i32* %i55, align 4
  %call63 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces62, i32 %28)
  %m_indices64 = getelementptr inbounds %struct.btFace, %struct.btFace* %call63, i32 0, i32 0
  %call65 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_indices64)
  store i32 %call65, i32* %numVertices61, align 4
  %29 = load i32, i32* %numVertices61, align 4
  %sub = sub nsw i32 %29, 2
  store i32 %sub, i32* %NbTris66, align 4
  %m_vertices67 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %m_faces68 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %30 = load i32, i32* %i55, align 4
  %call69 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces68, i32 %30)
  %m_indices70 = getelementptr inbounds %struct.btFace, %struct.btFace* %call69, i32 0, i32 0
  %call71 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices70, i32 0)
  %31 = load i32, i32* %call71, align 4
  %call72 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices67, i32 %31)
  store %class.btVector3* %call72, %class.btVector3** %p0, align 4
  store i32 1, i32* %j73, align 4
  br label %for.cond74

for.cond74:                                       ; preds = %for.inc103, %for.body60
  %32 = load i32, i32* %j73, align 4
  %33 = load i32, i32* %NbTris66, align 4
  %cmp75 = icmp sle i32 %32, %33
  br i1 %cmp75, label %for.body76, label %for.end105

for.body76:                                       ; preds = %for.cond74
  %34 = load i32, i32* %j73, align 4
  %add78 = add nsw i32 %34, 1
  %35 = load i32, i32* %numVertices61, align 4
  %rem79 = srem i32 %add78, %35
  store i32 %rem79, i32* %k77, align 4
  %m_vertices80 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %m_faces81 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %36 = load i32, i32* %i55, align 4
  %call82 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces81, i32 %36)
  %m_indices83 = getelementptr inbounds %struct.btFace, %struct.btFace* %call82, i32 0, i32 0
  %37 = load i32, i32* %j73, align 4
  %call84 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices83, i32 %37)
  %38 = load i32, i32* %call84, align 4
  %call85 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices80, i32 %38)
  store %class.btVector3* %call85, %class.btVector3** %p1, align 4
  %m_vertices86 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %m_faces87 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %39 = load i32, i32* %i55, align 4
  %call88 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces87, i32 %39)
  %m_indices89 = getelementptr inbounds %struct.btFace, %struct.btFace* %call88, i32 0, i32 0
  %40 = load i32, i32* %k77, align 4
  %call90 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_indices89, i32 %40)
  %41 = load i32, i32* %call90, align 4
  %call91 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices86, i32 %41)
  store %class.btVector3* %call91, %class.btVector3** %p2, align 4
  %42 = load %class.btVector3*, %class.btVector3** %p0, align 4
  %43 = load %class.btVector3*, %class.btVector3** %p1, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp93, %class.btVector3* nonnull align 4 dereferenceable(16) %42, %class.btVector3* nonnull align 4 dereferenceable(16) %43)
  %44 = load %class.btVector3*, %class.btVector3** %p0, align 4
  %45 = load %class.btVector3*, %class.btVector3** %p2, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %44, %class.btVector3* nonnull align 4 dereferenceable(16) %45)
  call void @_ZNK9btVector35crossERKS_(%class.btVector3* sret align 4 %ref.tmp92, %class.btVector3* %ref.tmp93, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp94)
  %call95 = call float @_ZNK9btVector36lengthEv(%class.btVector3* %ref.tmp92)
  %mul = fmul float %call95, 5.000000e-01
  store float %mul, float* %Area, align 4
  %46 = load %class.btVector3*, %class.btVector3** %p0, align 4
  %47 = load %class.btVector3*, %class.btVector3** %p1, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp97, %class.btVector3* nonnull align 4 dereferenceable(16) %46, %class.btVector3* nonnull align 4 dereferenceable(16) %47)
  %48 = load %class.btVector3*, %class.btVector3** %p2, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp96, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp97, %class.btVector3* nonnull align 4 dereferenceable(16) %48)
  store float 3.000000e+00, float* %ref.tmp98, align 4
  call void @_ZdvRK9btVector3RKf(%class.btVector3* sret align 4 %Center, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp96, float* nonnull align 4 dereferenceable(4) %ref.tmp98)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp99, float* nonnull align 4 dereferenceable(4) %Area, %class.btVector3* nonnull align 4 dereferenceable(16) %Center)
  %m_localCenter100 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %call101 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %m_localCenter100, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp99)
  %49 = load float, float* %Area, align 4
  %50 = load float, float* %TotalArea, align 4
  %add102 = fadd float %50, %49
  store float %add102, float* %TotalArea, align 4
  br label %for.inc103

for.inc103:                                       ; preds = %for.body76
  %51 = load i32, i32* %j73, align 4
  %inc104 = add nsw i32 %51, 1
  store i32 %inc104, i32* %j73, align 4
  br label %for.cond74

for.end105:                                       ; preds = %for.cond74
  br label %for.inc106

for.inc106:                                       ; preds = %for.end105
  %52 = load i32, i32* %i55, align 4
  %inc107 = add nsw i32 %52, 1
  store i32 %inc107, i32* %i55, align 4
  br label %for.cond56

for.end108:                                       ; preds = %for.cond56
  %m_localCenter109 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %call110 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %m_localCenter109, float* nonnull align 4 dereferenceable(4) %TotalArea)
  %m_radius = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 6
  store float 0x47EFFFFFE0000000, float* %m_radius, align 4
  store i32 0, i32* %i111, align 4
  br label %for.cond112

for.cond112:                                      ; preds = %for.inc141, %for.end108
  %53 = load i32, i32* %i111, align 4
  %m_faces113 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %call114 = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %m_faces113)
  %cmp115 = icmp slt i32 %53, %call114
  br i1 %cmp115, label %for.body116, label %for.end143

for.body116:                                      ; preds = %for.cond112
  %m_faces117 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %54 = load i32, i32* %i111, align 4
  %call118 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces117, i32 %54)
  %m_plane = getelementptr inbounds %struct.btFace, %struct.btFace* %call118, i32 0, i32 1
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_plane, i32 0, i32 0
  %m_faces119 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %55 = load i32, i32* %i111, align 4
  %call120 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces119, i32 %55)
  %m_plane121 = getelementptr inbounds %struct.btFace, %struct.btFace* %call120, i32 0, i32 1
  %arrayidx122 = getelementptr inbounds [4 x float], [4 x float]* %m_plane121, i32 0, i32 1
  %m_faces123 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %56 = load i32, i32* %i111, align 4
  %call124 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces123, i32 %56)
  %m_plane125 = getelementptr inbounds %struct.btFace, %struct.btFace* %call124, i32 0, i32 1
  %arrayidx126 = getelementptr inbounds [4 x float], [4 x float]* %m_plane125, i32 0, i32 2
  %call127 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %Normal, float* nonnull align 4 dereferenceable(4) %arrayidx, float* nonnull align 4 dereferenceable(4) %arrayidx122, float* nonnull align 4 dereferenceable(4) %arrayidx126)
  %m_localCenter128 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 4
  %call129 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %m_localCenter128, %class.btVector3* nonnull align 4 dereferenceable(16) %Normal)
  %m_faces130 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 2
  %57 = load i32, i32* %i111, align 4
  %call131 = call nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %m_faces130, i32 %57)
  %m_plane132 = getelementptr inbounds %struct.btFace, %struct.btFace* %call131, i32 0, i32 1
  %arrayidx133 = getelementptr inbounds [4 x float], [4 x float]* %m_plane132, i32 0, i32 3
  %58 = load float, float* %arrayidx133, align 4
  %add134 = fadd float %call129, %58
  %call135 = call float @_Z6btFabsf(float %add134)
  store float %call135, float* %dist, align 4
  %59 = load float, float* %dist, align 4
  %m_radius136 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 6
  %60 = load float, float* %m_radius136, align 4
  %cmp137 = fcmp olt float %59, %60
  br i1 %cmp137, label %if.then138, label %if.end140

if.then138:                                       ; preds = %for.body116
  %61 = load float, float* %dist, align 4
  %m_radius139 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 6
  store float %61, float* %m_radius139, align 4
  br label %if.end140

if.end140:                                        ; preds = %if.then138, %for.body116
  br label %for.inc141

for.inc141:                                       ; preds = %if.end140
  %62 = load i32, i32* %i111, align 4
  %inc142 = add nsw i32 %62, 1
  store i32 %inc142, i32* %i111, align 4
  br label %for.cond112

for.end143:                                       ; preds = %for.cond112
  store float 0x47EFFFFFE0000000, float* %MinX, align 4
  store float 0x47EFFFFFE0000000, float* %MinY, align 4
  store float 0x47EFFFFFE0000000, float* %MinZ, align 4
  store float 0xC7EFFFFFE0000000, float* %MaxX, align 4
  store float 0xC7EFFFFFE0000000, float* %MaxY, align 4
  store float 0xC7EFFFFFE0000000, float* %MaxZ, align 4
  store i32 0, i32* %i144, align 4
  br label %for.cond145

for.cond145:                                      ; preds = %for.inc182, %for.end143
  %63 = load i32, i32* %i144, align 4
  %m_vertices146 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %call147 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_vertices146)
  %cmp148 = icmp slt i32 %63, %call147
  br i1 %cmp148, label %for.body149, label %for.end184

for.body149:                                      ; preds = %for.cond145
  %m_vertices150 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %64 = load i32, i32* %i144, align 4
  %call151 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices150, i32 %64)
  store %class.btVector3* %call151, %class.btVector3** %pt, align 4
  %65 = load %class.btVector3*, %class.btVector3** %pt, align 4
  %call152 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %65)
  %66 = load float, float* %call152, align 4
  %67 = load float, float* %MinX, align 4
  %cmp153 = fcmp olt float %66, %67
  br i1 %cmp153, label %if.then154, label %if.end156

if.then154:                                       ; preds = %for.body149
  %68 = load %class.btVector3*, %class.btVector3** %pt, align 4
  %call155 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %68)
  %69 = load float, float* %call155, align 4
  store float %69, float* %MinX, align 4
  br label %if.end156

if.end156:                                        ; preds = %if.then154, %for.body149
  %70 = load %class.btVector3*, %class.btVector3** %pt, align 4
  %call157 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %70)
  %71 = load float, float* %call157, align 4
  %72 = load float, float* %MaxX, align 4
  %cmp158 = fcmp ogt float %71, %72
  br i1 %cmp158, label %if.then159, label %if.end161

if.then159:                                       ; preds = %if.end156
  %73 = load %class.btVector3*, %class.btVector3** %pt, align 4
  %call160 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %73)
  %74 = load float, float* %call160, align 4
  store float %74, float* %MaxX, align 4
  br label %if.end161

if.end161:                                        ; preds = %if.then159, %if.end156
  %75 = load %class.btVector3*, %class.btVector3** %pt, align 4
  %call162 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %75)
  %76 = load float, float* %call162, align 4
  %77 = load float, float* %MinY, align 4
  %cmp163 = fcmp olt float %76, %77
  br i1 %cmp163, label %if.then164, label %if.end166

if.then164:                                       ; preds = %if.end161
  %78 = load %class.btVector3*, %class.btVector3** %pt, align 4
  %call165 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %78)
  %79 = load float, float* %call165, align 4
  store float %79, float* %MinY, align 4
  br label %if.end166

if.end166:                                        ; preds = %if.then164, %if.end161
  %80 = load %class.btVector3*, %class.btVector3** %pt, align 4
  %call167 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %80)
  %81 = load float, float* %call167, align 4
  %82 = load float, float* %MaxY, align 4
  %cmp168 = fcmp ogt float %81, %82
  br i1 %cmp168, label %if.then169, label %if.end171

if.then169:                                       ; preds = %if.end166
  %83 = load %class.btVector3*, %class.btVector3** %pt, align 4
  %call170 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %83)
  %84 = load float, float* %call170, align 4
  store float %84, float* %MaxY, align 4
  br label %if.end171

if.end171:                                        ; preds = %if.then169, %if.end166
  %85 = load %class.btVector3*, %class.btVector3** %pt, align 4
  %call172 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %85)
  %86 = load float, float* %call172, align 4
  %87 = load float, float* %MinZ, align 4
  %cmp173 = fcmp olt float %86, %87
  br i1 %cmp173, label %if.then174, label %if.end176

if.then174:                                       ; preds = %if.end171
  %88 = load %class.btVector3*, %class.btVector3** %pt, align 4
  %call175 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %88)
  %89 = load float, float* %call175, align 4
  store float %89, float* %MinZ, align 4
  br label %if.end176

if.end176:                                        ; preds = %if.then174, %if.end171
  %90 = load %class.btVector3*, %class.btVector3** %pt, align 4
  %call177 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %90)
  %91 = load float, float* %call177, align 4
  %92 = load float, float* %MaxZ, align 4
  %cmp178 = fcmp ogt float %91, %92
  br i1 %cmp178, label %if.then179, label %if.end181

if.then179:                                       ; preds = %if.end176
  %93 = load %class.btVector3*, %class.btVector3** %pt, align 4
  %call180 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %93)
  %94 = load float, float* %call180, align 4
  store float %94, float* %MaxZ, align 4
  br label %if.end181

if.end181:                                        ; preds = %if.then179, %if.end176
  br label %for.inc182

for.inc182:                                       ; preds = %if.end181
  %95 = load i32, i32* %i144, align 4
  %inc183 = add nsw i32 %95, 1
  store i32 %inc183, i32* %i144, align 4
  br label %for.cond145

for.end184:                                       ; preds = %for.cond145
  %mC = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 7
  %96 = load float, float* %MaxX, align 4
  %97 = load float, float* %MinX, align 4
  %add186 = fadd float %96, %97
  store float %add186, float* %ref.tmp185, align 4
  %98 = load float, float* %MaxY, align 4
  %99 = load float, float* %MinY, align 4
  %add188 = fadd float %98, %99
  store float %add188, float* %ref.tmp187, align 4
  %100 = load float, float* %MaxZ, align 4
  %101 = load float, float* %MinZ, align 4
  %add190 = fadd float %100, %101
  store float %add190, float* %ref.tmp189, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %mC, float* nonnull align 4 dereferenceable(4) %ref.tmp185, float* nonnull align 4 dereferenceable(4) %ref.tmp187, float* nonnull align 4 dereferenceable(4) %ref.tmp189)
  %mE = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 8
  %102 = load float, float* %MaxX, align 4
  %103 = load float, float* %MinX, align 4
  %sub192 = fsub float %102, %103
  store float %sub192, float* %ref.tmp191, align 4
  %104 = load float, float* %MaxY, align 4
  %105 = load float, float* %MinY, align 4
  %sub194 = fsub float %104, %105
  store float %sub194, float* %ref.tmp193, align 4
  %106 = load float, float* %MaxZ, align 4
  %107 = load float, float* %MinZ, align 4
  %sub196 = fsub float %106, %107
  store float %sub196, float* %ref.tmp195, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %mE, float* nonnull align 4 dereferenceable(4) %ref.tmp191, float* nonnull align 4 dereferenceable(4) %ref.tmp193, float* nonnull align 4 dereferenceable(4) %ref.tmp195)
  %m_radius197 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 6
  %108 = load float, float* %m_radius197, align 4
  %109 = call float @llvm.sqrt.f32(float 3.000000e+00)
  %div = fdiv float %108, %109
  store float %div, float* %r, align 4
  %mE198 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 8
  %call199 = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %mE198)
  store i32 %call199, i32* %LargestExtent, align 4
  %mE200 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 8
  %call201 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %mE200)
  %110 = load i32, i32* %LargestExtent, align 4
  %arrayidx202 = getelementptr inbounds float, float* %call201, i32 %110
  %111 = load float, float* %arrayidx202, align 4
  %mul203 = fmul float %111, 5.000000e-01
  %112 = load float, float* %r, align 4
  %sub204 = fsub float %mul203, %112
  %div205 = fdiv float %sub204, 1.024000e+03
  store float %div205, float* %Step, align 4
  %113 = load float, float* %r, align 4
  %m_extents = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call206 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents)
  %arrayidx207 = getelementptr inbounds float, float* %call206, i32 2
  store float %113, float* %arrayidx207, align 4
  %m_extents208 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call209 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents208)
  %arrayidx210 = getelementptr inbounds float, float* %call209, i32 1
  store float %113, float* %arrayidx210, align 4
  %m_extents211 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call212 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents211)
  %arrayidx213 = getelementptr inbounds float, float* %call212, i32 0
  store float %113, float* %arrayidx213, align 4
  %mE214 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 8
  %call215 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %mE214)
  %114 = load i32, i32* %LargestExtent, align 4
  %arrayidx216 = getelementptr inbounds float, float* %call215, i32 %114
  %115 = load float, float* %arrayidx216, align 4
  %mul217 = fmul float %115, 5.000000e-01
  %m_extents218 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call219 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents218)
  %116 = load i32, i32* %LargestExtent, align 4
  %arrayidx220 = getelementptr inbounds float, float* %call219, i32 %116
  store float %mul217, float* %arrayidx220, align 4
  store i8 0, i8* %FoundBox, align 1
  store i32 0, i32* %j221, align 4
  br label %for.cond222

for.cond222:                                      ; preds = %for.inc232, %for.end184
  %117 = load i32, i32* %j221, align 4
  %cmp223 = icmp slt i32 %117, 1024
  br i1 %cmp223, label %for.body224, label %for.end234

for.body224:                                      ; preds = %for.cond222
  %call225 = call zeroext i1 @_ZNK18btConvexPolyhedron15testContainmentEv(%class.btConvexPolyhedron* %this1)
  br i1 %call225, label %if.then226, label %if.end227

if.then226:                                       ; preds = %for.body224
  store i8 1, i8* %FoundBox, align 1
  br label %for.end234

if.end227:                                        ; preds = %for.body224
  %118 = load float, float* %Step, align 4
  %m_extents228 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call229 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents228)
  %119 = load i32, i32* %LargestExtent, align 4
  %arrayidx230 = getelementptr inbounds float, float* %call229, i32 %119
  %120 = load float, float* %arrayidx230, align 4
  %sub231 = fsub float %120, %118
  store float %sub231, float* %arrayidx230, align 4
  br label %for.inc232

for.inc232:                                       ; preds = %if.end227
  %121 = load i32, i32* %j221, align 4
  %inc233 = add nsw i32 %121, 1
  store i32 %inc233, i32* %j221, align 4
  br label %for.cond222

for.end234:                                       ; preds = %if.then226, %for.cond222
  %122 = load i8, i8* %FoundBox, align 1
  %tobool235 = trunc i8 %122 to i1
  br i1 %tobool235, label %if.else246, label %if.then236

if.then236:                                       ; preds = %for.end234
  %123 = load float, float* %r, align 4
  %m_extents237 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call238 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents237)
  %arrayidx239 = getelementptr inbounds float, float* %call238, i32 2
  store float %123, float* %arrayidx239, align 4
  %m_extents240 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call241 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents240)
  %arrayidx242 = getelementptr inbounds float, float* %call241, i32 1
  store float %123, float* %arrayidx242, align 4
  %m_extents243 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call244 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents243)
  %arrayidx245 = getelementptr inbounds float, float* %call244, i32 0
  store float %123, float* %arrayidx245, align 4
  br label %if.end283

if.else246:                                       ; preds = %for.end234
  %m_radius248 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 6
  %124 = load float, float* %m_radius248, align 4
  %125 = load float, float* %r, align 4
  %sub249 = fsub float %124, %125
  %div250 = fdiv float %sub249, 1.024000e+03
  store float %div250, float* %Step247, align 4
  %126 = load i32, i32* %LargestExtent, align 4
  %shl = shl i32 1, %126
  %and = and i32 %shl, 3
  store i32 %and, i32* %e0, align 4
  %127 = load i32, i32* %e0, align 4
  %shl251 = shl i32 1, %127
  %and252 = and i32 %shl251, 3
  store i32 %and252, i32* %e1, align 4
  store i32 0, i32* %j253, align 4
  br label %for.cond254

for.cond254:                                      ; preds = %for.inc280, %if.else246
  %128 = load i32, i32* %j253, align 4
  %cmp255 = icmp slt i32 %128, 1024
  br i1 %cmp255, label %for.body256, label %for.end282

for.body256:                                      ; preds = %for.cond254
  %m_extents257 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call258 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents257)
  %129 = load i32, i32* %e0, align 4
  %arrayidx259 = getelementptr inbounds float, float* %call258, i32 %129
  %130 = load float, float* %arrayidx259, align 4
  store float %130, float* %Saved0, align 4
  %m_extents260 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call261 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents260)
  %131 = load i32, i32* %e1, align 4
  %arrayidx262 = getelementptr inbounds float, float* %call261, i32 %131
  %132 = load float, float* %arrayidx262, align 4
  store float %132, float* %Saved1, align 4
  %133 = load float, float* %Step247, align 4
  %m_extents263 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call264 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents263)
  %134 = load i32, i32* %e0, align 4
  %arrayidx265 = getelementptr inbounds float, float* %call264, i32 %134
  %135 = load float, float* %arrayidx265, align 4
  %add266 = fadd float %135, %133
  store float %add266, float* %arrayidx265, align 4
  %136 = load float, float* %Step247, align 4
  %m_extents267 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call268 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents267)
  %137 = load i32, i32* %e1, align 4
  %arrayidx269 = getelementptr inbounds float, float* %call268, i32 %137
  %138 = load float, float* %arrayidx269, align 4
  %add270 = fadd float %138, %136
  store float %add270, float* %arrayidx269, align 4
  %call271 = call zeroext i1 @_ZNK18btConvexPolyhedron15testContainmentEv(%class.btConvexPolyhedron* %this1)
  br i1 %call271, label %if.end279, label %if.then272

if.then272:                                       ; preds = %for.body256
  %139 = load float, float* %Saved0, align 4
  %m_extents273 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call274 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents273)
  %140 = load i32, i32* %e0, align 4
  %arrayidx275 = getelementptr inbounds float, float* %call274, i32 %140
  store float %139, float* %arrayidx275, align 4
  %141 = load float, float* %Saved1, align 4
  %m_extents276 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 5
  %call277 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_extents276)
  %142 = load i32, i32* %e1, align 4
  %arrayidx278 = getelementptr inbounds float, float* %call277, i32 %142
  store float %141, float* %arrayidx278, align 4
  br label %for.end282

if.end279:                                        ; preds = %for.body256
  br label %for.inc280

for.inc280:                                       ; preds = %if.end279
  %143 = load i32, i32* %j253, align 4
  %inc281 = add nsw i32 %143, 1
  store i32 %inc281, i32* %j253, align 4
  br label %for.cond254

for.end282:                                       ; preds = %if.then272, %for.cond254
  br label %if.end283

if.end283:                                        ; preds = %for.end282, %if.then236
  %call284 = call %class.btHashMap* @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeED2Ev(%class.btHashMap* %edges) #6
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btHashMap* @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeEC2Ev(%class.btHashMap* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* %m_hashTable)
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* %m_next)
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI14btInternalEdgeEC2Ev(%class.btAlignedObjectArray.8* %m_valueArray)
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %call4 = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI20btInternalVertexPairEC2Ev(%class.btAlignedObjectArray.12* %m_keyArray)
  ret %class.btHashMap* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(36) %struct.btFace* @_ZN20btAlignedObjectArrayI6btFaceEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btFace*, %struct.btFace** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %0, i32 %1
  ret %struct.btFace* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btInternalVertexPair* @_ZN20btInternalVertexPairC2Ess(%struct.btInternalVertexPair* returned %this, i16 signext %v0, i16 signext %v1) unnamed_addr #2 comdat {
entry:
  %retval = alloca %struct.btInternalVertexPair*, align 4
  %this.addr = alloca %struct.btInternalVertexPair*, align 4
  %v0.addr = alloca i16, align 2
  %v1.addr = alloca i16, align 2
  store %struct.btInternalVertexPair* %this, %struct.btInternalVertexPair** %this.addr, align 4
  store i16 %v0, i16* %v0.addr, align 2
  store i16 %v1, i16* %v1.addr, align 2
  %this1 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %this.addr, align 4
  store %struct.btInternalVertexPair* %this1, %struct.btInternalVertexPair** %retval, align 4
  %m_v0 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 0
  %0 = load i16, i16* %v0.addr, align 2
  store i16 %0, i16* %m_v0, align 2
  %m_v1 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 1
  %1 = load i16, i16* %v1.addr, align 2
  store i16 %1, i16* %m_v1, align 2
  %m_v12 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 1
  %2 = load i16, i16* %m_v12, align 2
  %conv = sext i16 %2 to i32
  %m_v03 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 0
  %3 = load i16, i16* %m_v03, align 2
  %conv4 = sext i16 %3 to i32
  %cmp = icmp sgt i32 %conv, %conv4
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_v05 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 0
  %m_v16 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 1
  call void @_Z6btSwapIsEvRT_S1_(i16* nonnull align 2 dereferenceable(2) %m_v05, i16* nonnull align 2 dereferenceable(2) %m_v16)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %4 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %retval, align 4
  ret %struct.btInternalVertexPair* %4
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btInternalEdge* @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE4findERKS0_(%class.btHashMap* %this, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %key) #2 comdat {
entry:
  %retval = alloca %struct.btInternalEdge*, align 4
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %struct.btInternalVertexPair*, align 4
  %index = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4
  store %struct.btInternalVertexPair* %key, %struct.btInternalVertexPair** %key.addr, align 4
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4
  %call = call i32 @_ZNK9btHashMapI20btInternalVertexPair14btInternalEdgeE9findIndexERKS0_(%class.btHashMap* %this1, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %0)
  store i32 %call, i32* %index, align 4
  %1 = load i32, i32* %index, align 4
  %cmp = icmp eq i32 %1, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store %struct.btInternalEdge* null, %struct.btInternalEdge** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %2 = load i32, i32* %index, align 4
  %call2 = call nonnull align 2 dereferenceable(4) %struct.btInternalEdge* @_ZN20btAlignedObjectArrayI14btInternalEdgeEixEi(%class.btAlignedObjectArray.8* %m_valueArray, i32 %2)
  store %struct.btInternalEdge* %call2, %struct.btInternalEdge** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load %struct.btInternalEdge*, %struct.btInternalEdge** %retval, align 4
  ret %struct.btInternalEdge* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_Z12IsAlmostZeroRK9btVector3(%class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %0)
  %1 = load float, float* %call, align 4
  %call1 = call float @_Z6btFabsf(float %1)
  %conv = fpext float %call1 to double
  %cmp = fcmp ogt double %conv, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %entry
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %2)
  %3 = load float, float* %call2, align 4
  %call3 = call float @_Z6btFabsf(float %3)
  %conv4 = fpext float %call3 to double
  %cmp5 = fcmp ogt double %conv4, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp5, label %if.then, label %lor.lhs.false6

lor.lhs.false6:                                   ; preds = %lor.lhs.false
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %4)
  %5 = load float, float* %call7, align 4
  %call8 = call float @_Z6btFabsf(float %5)
  %conv9 = fpext float %call8 to double
  %cmp10 = fcmp ogt double %conv9, 0x3EB0C6F7A0B5ED8D
  br i1 %cmp10, label %if.then, label %if.end

if.then:                                          ; preds = %lor.lhs.false6, %lor.lhs.false, %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %lor.lhs.false6
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %6 = load i1, i1* %retval, align 1
  ret i1 %6
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E9push_backERKS0_(%class.btAlignedObjectArray* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btVector3*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btVector3* %_Val, %class.btVector3** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 %2
  %3 = bitcast %class.btVector3* %arrayidx to i8*
  %call5 = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %3)
  %4 = bitcast i8* %call5 to %class.btVector3*
  %5 = load %class.btVector3*, %class.btVector3** %_Val.addr, align 4
  %6 = bitcast %class.btVector3* %4 to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %m_size6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size6, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInternalEdge* @_ZN14btInternalEdgeC2Ev(%struct.btInternalEdge* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInternalEdge*, align 4
  store %struct.btInternalEdge* %this, %struct.btInternalEdge** %this.addr, align 4
  %this1 = load %struct.btInternalEdge*, %struct.btInternalEdge** %this.addr, align 4
  %m_face0 = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %this1, i32 0, i32 0
  store i16 -1, i16* %m_face0, align 2
  %m_face1 = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %this1, i32 0, i32 1
  store i16 -1, i16* %m_face1, align 2
  ret %struct.btInternalEdge* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE6insertERKS0_RKS1_(%class.btHashMap* %this, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %key, %struct.btInternalEdge* nonnull align 2 dereferenceable(4) %value) #2 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %struct.btInternalVertexPair*, align 4
  %value.addr = alloca %struct.btInternalEdge*, align 4
  %hash = alloca i32, align 4
  %index = alloca i32, align 4
  %count = alloca i32, align 4
  %oldCapacity = alloca i32, align 4
  %newCapacity = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4
  store %struct.btInternalVertexPair* %key, %struct.btInternalVertexPair** %key.addr, align 4
  store %struct.btInternalEdge* %value, %struct.btInternalEdge** %value.addr, align 4
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4
  %call = call i32 @_ZNK20btInternalVertexPair7getHashEv(%struct.btInternalVertexPair* %0)
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %m_valueArray)
  %sub = sub nsw i32 %call2, 1
  %and = and i32 %call, %sub
  store i32 %and, i32* %hash, align 4
  %1 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4
  %call3 = call i32 @_ZNK9btHashMapI20btInternalVertexPair14btInternalEdgeE9findIndexERKS0_(%class.btHashMap* %this1, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %1)
  store i32 %call3, i32* %index, align 4
  %2 = load i32, i32* %index, align 4
  %cmp = icmp ne i32 %2, -1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load %struct.btInternalEdge*, %struct.btInternalEdge** %value.addr, align 4
  %m_valueArray4 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %4 = load i32, i32* %index, align 4
  %call5 = call nonnull align 2 dereferenceable(4) %struct.btInternalEdge* @_ZN20btAlignedObjectArrayI14btInternalEdgeEixEi(%class.btAlignedObjectArray.8* %m_valueArray4, i32 %4)
  %5 = bitcast %struct.btInternalEdge* %call5 to i8*
  %6 = bitcast %struct.btInternalEdge* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %5, i8* align 2 %6, i32 4, i1 false)
  br label %return

if.end:                                           ; preds = %entry
  %m_valueArray6 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call7 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv(%class.btAlignedObjectArray.8* %m_valueArray6)
  store i32 %call7, i32* %count, align 4
  %m_valueArray8 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call9 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %m_valueArray8)
  store i32 %call9, i32* %oldCapacity, align 4
  %m_valueArray10 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %7 = load %struct.btInternalEdge*, %struct.btInternalEdge** %value.addr, align 4
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE9push_backERKS0_(%class.btAlignedObjectArray.8* %m_valueArray10, %struct.btInternalEdge* nonnull align 2 dereferenceable(4) %7)
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %8 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE9push_backERKS0_(%class.btAlignedObjectArray.12* %m_keyArray, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %8)
  %m_valueArray11 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call12 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %m_valueArray11)
  store i32 %call12, i32* %newCapacity, align 4
  %9 = load i32, i32* %oldCapacity, align 4
  %10 = load i32, i32* %newCapacity, align 4
  %cmp13 = icmp slt i32 %9, %10
  br i1 %cmp13, label %if.then14, label %if.end20

if.then14:                                        ; preds = %if.end
  %11 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4
  call void @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE10growTablesERKS0_(%class.btHashMap* %this1, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %11)
  %12 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4
  %call15 = call i32 @_ZNK20btInternalVertexPair7getHashEv(%struct.btInternalVertexPair* %12)
  %m_valueArray16 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call17 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %m_valueArray16)
  %sub18 = sub nsw i32 %call17, 1
  %and19 = and i32 %call15, %sub18
  store i32 %and19, i32* %hash, align 4
  br label %if.end20

if.end20:                                         ; preds = %if.then14, %if.end
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %13 = load i32, i32* %hash, align 4
  %call21 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_hashTable, i32 %13)
  %14 = load i32, i32* %call21, align 4
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %15 = load i32, i32* %count, align 4
  %call22 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_next, i32 %15)
  store i32 %14, i32* %call22, align 4
  %16 = load i32, i32* %count, align 4
  %m_hashTable23 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %17 = load i32, i32* %hash, align 4
  %call24 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_hashTable23, i32 %17)
  store i32 %16, i32* %call24, align 4
  br label %return

return:                                           ; preds = %if.end20, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector35crossERKS_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 2
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %sub = fsub float %mul, %mul8
  store float %sub, float* %ref.tmp, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %6 = load float, float* %arrayidx11, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 0
  %8 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %6, %8
  %m_floats15 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx16 = getelementptr inbounds [4 x float], [4 x float]* %m_floats15, i32 0, i32 0
  %9 = load float, float* %arrayidx16, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats17 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [4 x float], [4 x float]* %m_floats17, i32 0, i32 2
  %11 = load float, float* %arrayidx18, align 4
  %mul19 = fmul float %9, %11
  %sub20 = fsub float %mul14, %mul19
  store float %sub20, float* %ref.tmp9, align 4
  %m_floats22 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [4 x float], [4 x float]* %m_floats22, i32 0, i32 0
  %12 = load float, float* %arrayidx23, align 4
  %13 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats24 = getelementptr inbounds %class.btVector3, %class.btVector3* %13, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [4 x float], [4 x float]* %m_floats24, i32 0, i32 1
  %14 = load float, float* %arrayidx25, align 4
  %mul26 = fmul float %12, %14
  %m_floats27 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx28 = getelementptr inbounds [4 x float], [4 x float]* %m_floats27, i32 0, i32 1
  %15 = load float, float* %arrayidx28, align 4
  %16 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats29 = getelementptr inbounds %class.btVector3, %class.btVector3* %16, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [4 x float], [4 x float]* %m_floats29, i32 0, i32 0
  %17 = load float, float* %arrayidx30, align 4
  %mul31 = fmul float %15, %17
  %sub32 = fsub float %mul26, %mul31
  store float %sub32, float* %ref.tmp21, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp21)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZdvRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  %2 = load float, float* %1, align 4
  %div = fdiv float 1.000000e+00, %2
  store float %div, float* %ref.tmp, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btHashMap* @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeED2Ev(%class.btHashMap* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %call = call %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI20btInternalVertexPairED2Ev(%class.btAlignedObjectArray.12* %m_keyArray) #6
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI14btInternalEdgeED2Ev(%class.btAlignedObjectArray.8* %m_valueArray) #6
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %call3 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %m_next) #6
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call4 = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %m_hashTable) #6
  ret %class.btHashMap* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK18btConvexPolyhedron7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexPolyhedron* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %dir, float* nonnull align 4 dereferenceable(4) %minProj, float* nonnull align 4 dereferenceable(4) %maxProj, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMin, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMax) #2 {
entry:
  %this.addr = alloca %class.btConvexPolyhedron*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %dir.addr = alloca %class.btVector3*, align 4
  %minProj.addr = alloca float*, align 4
  %maxProj.addr = alloca float*, align 4
  %witnesPtMin.addr = alloca %class.btVector3*, align 4
  %witnesPtMax.addr = alloca %class.btVector3*, align 4
  %numVerts = alloca i32, align 4
  %i = alloca i32, align 4
  %pt = alloca %class.btVector3, align 4
  %dp = alloca float, align 4
  store %class.btConvexPolyhedron* %this, %class.btConvexPolyhedron** %this.addr, align 4
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4
  store %class.btVector3* %dir, %class.btVector3** %dir.addr, align 4
  store float* %minProj, float** %minProj.addr, align 4
  store float* %maxProj, float** %maxProj.addr, align 4
  store %class.btVector3* %witnesPtMin, %class.btVector3** %witnesPtMin.addr, align 4
  store %class.btVector3* %witnesPtMax, %class.btVector3** %witnesPtMax.addr, align 4
  %this1 = load %class.btConvexPolyhedron*, %class.btConvexPolyhedron** %this.addr, align 4
  %0 = load float*, float** %minProj.addr, align 4
  store float 0x47EFFFFFE0000000, float* %0, align 4
  %1 = load float*, float** %maxProj.addr, align 4
  store float 0xC7EFFFFFE0000000, float* %1, align 4
  %m_vertices = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_vertices)
  store i32 %call, i32* %numVerts, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %3 = load i32, i32* %numVerts, align 4
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %4 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %m_vertices2 = getelementptr inbounds %class.btConvexPolyhedron, %class.btConvexPolyhedron* %this1, i32 0, i32 1
  %5 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_vertices2, i32 %5)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %pt, %class.btTransform* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  %6 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %pt, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  store float %call4, float* %dp, align 4
  %7 = load float, float* %dp, align 4
  %8 = load float*, float** %minProj.addr, align 4
  %9 = load float, float* %8, align 4
  %cmp5 = fcmp olt float %7, %9
  br i1 %cmp5, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %10 = load float, float* %dp, align 4
  %11 = load float*, float** %minProj.addr, align 4
  store float %10, float* %11, align 4
  %12 = load %class.btVector3*, %class.btVector3** %witnesPtMin.addr, align 4
  %13 = bitcast %class.btVector3* %12 to i8*
  %14 = bitcast %class.btVector3* %pt to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  %15 = load float, float* %dp, align 4
  %16 = load float*, float** %maxProj.addr, align 4
  %17 = load float, float* %16, align 4
  %cmp6 = fcmp ogt float %15, %17
  br i1 %cmp6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end
  %18 = load float, float* %dp, align 4
  %19 = load float*, float** %maxProj.addr, align 4
  store float %18, float* %19, align 4
  %20 = load %class.btVector3*, %class.btVector3** %witnesPtMax.addr, align 4
  %21 = bitcast %class.btVector3* %20 to i8*
  %22 = bitcast %class.btVector3* %pt to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %21, i8* align 4 %22, i32 16, i1 false)
  br label %if.end8

if.end8:                                          ; preds = %if.then7, %if.end
  br label %for.inc

for.inc:                                          ; preds = %if.end8
  %23 = load i32, i32* %i, align 4
  %inc = add nsw i32 %23, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %24 = load float*, float** %minProj.addr, align 4
  %25 = load float, float* %24, align 4
  %26 = load float*, float** %maxProj.addr, align 4
  %27 = load float, float* %26, align 4
  %cmp9 = fcmp ogt float %25, %27
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %for.end
  %28 = load float*, float** %minProj.addr, align 4
  %29 = load float*, float** %maxProj.addr, align 4
  call void @_Z6btSwapIfEvRT_S1_(float* nonnull align 4 dereferenceable(4) %28, float* nonnull align 4 dereferenceable(4) %29)
  %30 = load %class.btVector3*, %class.btVector3** %witnesPtMin.addr, align 4
  %31 = load %class.btVector3*, %class.btVector3** %witnesPtMax.addr, align 4
  call void @_Z6btSwapI9btVector3EvRT_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %30, %class.btVector3* nonnull align 4 dereferenceable(16) %31)
  br label %if.end11

if.end11:                                         ; preds = %if.then10, %for.end
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z6btSwapIfEvRT_S1_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  %tmp = alloca float, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  store float %1, float* %tmp, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %4 = load float*, float** %a.addr, align 4
  store float %3, float* %4, align 4
  %5 = load float, float* %tmp, align 4
  %6 = load float*, float** %b.addr, align 4
  store float %5, float* %6, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z6btSwapI9btVector3EvRT_S2_(%class.btVector3* nonnull align 4 dereferenceable(16) %a, %class.btVector3* nonnull align 4 dereferenceable(16) %b) #1 comdat {
entry:
  %a.addr = alloca %class.btVector3*, align 4
  %b.addr = alloca %class.btVector3*, align 4
  %tmp = alloca %class.btVector3, align 4
  store %class.btVector3* %a, %class.btVector3** %a.addr, align 4
  store %class.btVector3* %b, %class.btVector3** %b.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %1 = bitcast %class.btVector3* %tmp to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %a.addr, align 4
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  %7 = load %class.btVector3*, %class.btVector3** %b.addr, align 4
  %8 = bitcast %class.btVector3* %7 to i8*
  %9 = bitcast %class.btVector3* %tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.3* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.4* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.4* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this1)
  ret %class.btAlignedObjectArray.3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI14btInternalEdgeEC2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorI14btInternalEdgeLj16EEC2Ev(%class.btAlignedAllocator.9* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI20btInternalVertexPairEC2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EEC2Ev(%class.btAlignedAllocator.13* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE4initEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.4* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  ret %class.btAlignedAllocator.4* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.9* @_ZN18btAlignedAllocatorI14btInternalEdgeLj16EEC2Ev(%class.btAlignedAllocator.9* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  ret %class.btAlignedAllocator.9* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btInternalEdgeE4initEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %struct.btInternalEdge* null, %struct.btInternalEdge** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.13* @_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EEC2Ev(%class.btAlignedAllocator.13* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  ret %class.btAlignedAllocator.13* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE4initEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.btInternalVertexPair* null, %struct.btInternalVertexPair** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z6btSwapIsEvRT_S1_(i16* nonnull align 2 dereferenceable(2) %a, i16* nonnull align 2 dereferenceable(2) %b) #1 comdat {
entry:
  %a.addr = alloca i16*, align 4
  %b.addr = alloca i16*, align 4
  %tmp = alloca i16, align 2
  store i16* %a, i16** %a.addr, align 4
  store i16* %b, i16** %b.addr, align 4
  %0 = load i16*, i16** %a.addr, align 4
  %1 = load i16, i16* %0, align 2
  store i16 %1, i16* %tmp, align 2
  %2 = load i16*, i16** %b.addr, align 4
  %3 = load i16, i16* %2, align 2
  %4 = load i16*, i16** %a.addr, align 4
  store i16 %3, i16* %4, align 2
  %5 = load i16, i16* %tmp, align 2
  %6 = load i16*, i16** %b.addr, align 4
  store i16 %5, i16* %6, align 2
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.12* @_ZN20btAlignedObjectArrayI20btInternalVertexPairED2Ev(%class.btAlignedObjectArray.12* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE5clearEv(%class.btAlignedObjectArray.12* %this1)
  ret %class.btAlignedObjectArray.12* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayI14btInternalEdgeED2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE5clearEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.3* %this1)
  ret %class.btAlignedObjectArray.3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE5clearEv(%class.btAlignedObjectArray.12* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE4initEv(%class.btAlignedObjectArray.12* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE7destroyEii(%class.btAlignedObjectArray.12* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %3 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4sizeEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE10deallocateEv(%class.btAlignedObjectArray.12* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %m_data, align 4
  %tobool = icmp ne %struct.btInternalVertexPair* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %2 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %m_allocator, %struct.btInternalVertexPair* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.btInternalVertexPair* null, %struct.btInternalVertexPair** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EE10deallocateEPS0_(%class.btAlignedAllocator.13* %this, %struct.btInternalVertexPair* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %ptr.addr = alloca %struct.btInternalVertexPair*, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  store %struct.btInternalVertexPair* %ptr, %struct.btInternalVertexPair** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %ptr.addr, align 4
  %1 = bitcast %struct.btInternalVertexPair* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btInternalEdgeE5clearEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btInternalEdgeE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %3 = load %struct.btInternalEdge*, %struct.btInternalEdge** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btInternalEdgeE10deallocateEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %struct.btInternalEdge*, %struct.btInternalEdge** %m_data, align 4
  %tobool = icmp ne %struct.btInternalEdge* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %struct.btInternalEdge*, %struct.btInternalEdge** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI14btInternalEdgeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.9* %m_allocator, %struct.btInternalEdge* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %struct.btInternalEdge* null, %struct.btInternalEdge** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI14btInternalEdgeLj16EE10deallocateEPS0_(%class.btAlignedAllocator.9* %this, %struct.btInternalEdge* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca %struct.btInternalEdge*, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store %struct.btInternalEdge* %ptr, %struct.btInternalEdge** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load %struct.btInternalEdge*, %struct.btInternalEdge** %ptr.addr, align 4
  %1 = bitcast %struct.btInternalEdge* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.3* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.4* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.4* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI9btVector3Lj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorI6btFaceLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btFace* null, %struct.btFace** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %tobool = icmp ne %class.btVector3* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btVector3*, %class.btVector3** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %class.btVector3* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* null, %class.btVector3** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI9btVector3Lj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %class.btVector3* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btVector3*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.btVector3* %ptr, %class.btVector3** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %ptr.addr, align 4
  %1 = bitcast %class.btVector3* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI6btFaceE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI6btFaceE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI6btFaceE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI6btFaceE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %struct.btFace*, %struct.btFace** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btFace, %struct.btFace* %3, i32 %4
  %call = call %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* %arrayidx) #6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI6btFaceE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %struct.btFace*, %struct.btFace** %m_data, align 4
  %tobool = icmp ne %struct.btFace* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %struct.btFace*, %struct.btFace** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI6btFaceLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %struct.btFace* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %struct.btFace* null, %struct.btFace** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btFace* @_ZN6btFaceD2Ev(%struct.btFace* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btFace*, align 4
  store %struct.btFace* %this, %struct.btFace** %this.addr, align 4
  %this1 = load %struct.btFace*, %struct.btFace** %this.addr, align 4
  %m_indices = getelementptr inbounds %struct.btFace, %struct.btFace* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.3* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.3* %m_indices) #6
  ret %struct.btFace* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI6btFaceLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %struct.btFace* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %struct.btFace*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %struct.btFace* %ptr, %struct.btFace** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %struct.btFace*, %struct.btFace** %ptr.addr, align 4
  %1 = bitcast %struct.btFace* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK9btHashMapI20btInternalVertexPair14btInternalEdgeE9findIndexERKS0_(%class.btHashMap* %this, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %key) #2 comdat {
entry:
  %retval = alloca i32, align 4
  %this.addr = alloca %class.btHashMap*, align 4
  %key.addr = alloca %struct.btInternalVertexPair*, align 4
  %hash = alloca i32, align 4
  %index = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4
  store %struct.btInternalVertexPair* %key, %struct.btInternalVertexPair** %key.addr, align 4
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %0 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4
  %call = call i32 @_ZNK20btInternalVertexPair7getHashEv(%struct.btInternalVertexPair* %0)
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %m_valueArray)
  %sub = sub nsw i32 %call2, 1
  %and = and i32 %call, %sub
  store i32 %and, i32* %hash, align 4
  %1 = load i32, i32* %hash, align 4
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_hashTable)
  %cmp = icmp uge i32 %1, %call3
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i32 -1, i32* %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %m_hashTable4 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %2 = load i32, i32* %hash, align 4
  %call5 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_hashTable4, i32 %2)
  %3 = load i32, i32* %call5, align 4
  store i32 %3, i32* %index, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %4 = load i32, i32* %index, align 4
  %cmp6 = icmp ne i32 %4, -1
  br i1 %cmp6, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %while.cond
  %5 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %key.addr, align 4
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %6 = load i32, i32* %index, align 4
  %call7 = call nonnull align 2 dereferenceable(4) %struct.btInternalVertexPair* @_ZNK20btAlignedObjectArrayI20btInternalVertexPairEixEi(%class.btAlignedObjectArray.12* %m_keyArray, i32 %6)
  %call8 = call zeroext i1 @_ZNK20btInternalVertexPair6equalsERKS_(%struct.btInternalVertexPair* %5, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %call7)
  %conv = zext i1 %call8 to i32
  %cmp9 = icmp eq i32 %conv, 0
  br label %land.end

land.end:                                         ; preds = %land.rhs, %while.cond
  %7 = phi i1 [ false, %while.cond ], [ %cmp9, %land.rhs ]
  br i1 %7, label %while.body, label %while.end

while.body:                                       ; preds = %land.end
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %8 = load i32, i32* %index, align 4
  %call10 = call nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_next, i32 %8)
  %9 = load i32, i32* %call10, align 4
  store i32 %9, i32* %index, align 4
  br label %while.cond

while.end:                                        ; preds = %land.end
  %10 = load i32, i32* %index, align 4
  store i32 %10, i32* %retval, align 4
  br label %return

return:                                           ; preds = %while.end, %if.then
  %11 = load i32, i32* %retval, align 4
  ret i32 %11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 2 dereferenceable(4) %struct.btInternalEdge* @_ZN20btAlignedObjectArrayI14btInternalEdgeEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %struct.btInternalEdge*, %struct.btInternalEdge** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %0, i32 %1
  ret %struct.btInternalEdge* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btInternalVertexPair7getHashEv(%struct.btInternalVertexPair* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btInternalVertexPair*, align 4
  store %struct.btInternalVertexPair* %this, %struct.btInternalVertexPair** %this.addr, align 4
  %this1 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %this.addr, align 4
  %m_v0 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 0
  %0 = load i16, i16* %m_v0, align 2
  %conv = sext i16 %0 to i32
  %m_v1 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 1
  %1 = load i16, i16* %m_v1, align 2
  %conv2 = sext i16 %1 to i32
  %shl = shl i32 %conv2, 16
  %add = add nsw i32 %conv, %shl
  ret i32 %add
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZNK20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK20btInternalVertexPair6equalsERKS_(%struct.btInternalVertexPair* %this, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %other) #1 comdat {
entry:
  %this.addr = alloca %struct.btInternalVertexPair*, align 4
  %other.addr = alloca %struct.btInternalVertexPair*, align 4
  store %struct.btInternalVertexPair* %this, %struct.btInternalVertexPair** %this.addr, align 4
  store %struct.btInternalVertexPair* %other, %struct.btInternalVertexPair** %other.addr, align 4
  %this1 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %this.addr, align 4
  %m_v0 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 0
  %0 = load i16, i16* %m_v0, align 2
  %conv = sext i16 %0 to i32
  %1 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %other.addr, align 4
  %m_v02 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %1, i32 0, i32 0
  %2 = load i16, i16* %m_v02, align 2
  %conv3 = sext i16 %2 to i32
  %cmp = icmp eq i32 %conv, %conv3
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %m_v1 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %this1, i32 0, i32 1
  %3 = load i16, i16* %m_v1, align 2
  %conv4 = sext i16 %3 to i32
  %4 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %other.addr, align 4
  %m_v15 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %4, i32 0, i32 1
  %5 = load i16, i16* %m_v15, align 2
  %conv6 = sext i16 %5 to i32
  %cmp7 = icmp eq i32 %conv4, %conv6
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %6 = phi i1 [ false, %entry ], [ %cmp7, %land.rhs ]
  ret i1 %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 2 dereferenceable(4) %struct.btInternalVertexPair* @_ZNK20btAlignedObjectArrayI20btInternalVertexPairEixEi(%class.btAlignedObjectArray.12* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %0, i32 %1
  ret %struct.btInternalVertexPair* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI9btVector3E7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btVector3*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btVector3*
  store %class.btVector3* %2, %class.btVector3** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %class.btVector3*, %class.btVector3** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btVector3* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI9btVector3E7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI9btVector3E10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btVector3*, %class.btVector3** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btVector3* %4, %class.btVector3** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI9btVector3E9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN9btVector3nwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI9btVector3E8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btVector3** null)
  %2 = bitcast %class.btVector3* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI9btVector3E4copyEiiPS0_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btVector3* %dest) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btVector3* %dest, %class.btVector3** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btVector3*, %class.btVector3** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 %4
  %5 = bitcast %class.btVector3* %arrayidx to i8*
  %call = call i8* @_ZN9btVector3nwEmPv(i32 16, i8* %5)
  %6 = bitcast i8* %call to %class.btVector3*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 %8
  %9 = bitcast %class.btVector3* %6 to i8*
  %10 = bitcast %class.btVector3* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %9, i8* align 4 %10, i32 16, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btVector3* @_ZN18btAlignedAllocatorI9btVector3Lj16EE8allocateEiPPKS0_(%class.btAlignedAllocator* %this, i32 %n, %class.btVector3** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btVector3**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btVector3** %hint, %class.btVector3*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 16, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btVector3*
  ret %class.btVector3* %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btInternalEdgeE9push_backERKS0_(%class.btAlignedObjectArray.8* %this, %struct.btInternalEdge* nonnull align 2 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Val.addr = alloca %struct.btInternalEdge*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store %struct.btInternalEdge* %_Val, %struct.btInternalEdge** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI14btInternalEdgeE9allocSizeEi(%class.btAlignedObjectArray.8* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE7reserveEi(%class.btAlignedObjectArray.8* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %1 = load %struct.btInternalEdge*, %struct.btInternalEdge** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %1, i32 %2
  %3 = bitcast %struct.btInternalEdge* %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.btInternalEdge*
  %5 = load %struct.btInternalEdge*, %struct.btInternalEdge** %_Val.addr, align 4
  %6 = bitcast %struct.btInternalEdge* %4 to i8*
  %7 = bitcast %struct.btInternalEdge* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %6, i8* align 2 %7, i32 4, i1 false)
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE9push_backERKS0_(%class.btAlignedObjectArray.12* %this, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Val.addr = alloca %struct.btInternalVertexPair*, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store %struct.btInternalVertexPair* %_Val, %struct.btInternalVertexPair** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayI20btInternalVertexPairE9allocSizeEi(%class.btAlignedObjectArray.12* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE7reserveEi(%class.btAlignedObjectArray.12* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %1 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %1, i32 %2
  %3 = bitcast %struct.btInternalVertexPair* %arrayidx to i8*
  %4 = bitcast i8* %3 to %struct.btInternalVertexPair*
  %5 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %_Val.addr, align 4
  %6 = bitcast %struct.btInternalVertexPair* %4 to i8*
  %7 = bitcast %struct.btInternalVertexPair* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %6, i8* align 2 %7, i32 4, i1 false)
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 2
  %8 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN9btHashMapI20btInternalVertexPair14btInternalEdgeE10growTablesERKS0_(%class.btHashMap* %this, %struct.btInternalVertexPair* nonnull align 2 dereferenceable(4) %0) #2 comdat {
entry:
  %this.addr = alloca %class.btHashMap*, align 4
  %.addr = alloca %struct.btInternalVertexPair*, align 4
  %newCapacity = alloca i32, align 4
  %curHashtableSize = alloca i32, align 4
  %ref.tmp = alloca i32, align 4
  %ref.tmp6 = alloca i32, align 4
  %i = alloca i32, align 4
  %hashValue = alloca i32, align 4
  store %class.btHashMap* %this, %class.btHashMap** %this.addr, align 4
  store %struct.btInternalVertexPair* %0, %struct.btInternalVertexPair** %.addr, align 4
  %this1 = load %class.btHashMap*, %class.btHashMap** %this.addr, align 4
  %m_valueArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %m_valueArray)
  store i32 %call, i32* %newCapacity, align 4
  %m_hashTable = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_hashTable)
  %1 = load i32, i32* %newCapacity, align 4
  %cmp = icmp slt i32 %call2, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_hashTable3 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %m_hashTable3)
  store i32 %call4, i32* %curHashtableSize, align 4
  %m_hashTable5 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %2 = load i32, i32* %newCapacity, align 4
  store i32 0, i32* %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.3* %m_hashTable5, i32 %2, i32* nonnull align 4 dereferenceable(4) %ref.tmp)
  %m_next = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %3 = load i32, i32* %newCapacity, align 4
  store i32 0, i32* %ref.tmp6, align 4
  call void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.3* %m_next, i32 %3, i32* nonnull align 4 dereferenceable(4) %ref.tmp6)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %4 = load i32, i32* %i, align 4
  %5 = load i32, i32* %newCapacity, align 4
  %cmp7 = icmp slt i32 %4, %5
  br i1 %cmp7, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_hashTable8 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %6 = load i32, i32* %i, align 4
  %call9 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_hashTable8, i32 %6)
  store i32 -1, i32* %call9, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc15, %for.end
  %8 = load i32, i32* %i, align 4
  %9 = load i32, i32* %newCapacity, align 4
  %cmp11 = icmp slt i32 %8, %9
  br i1 %cmp11, label %for.body12, label %for.end17

for.body12:                                       ; preds = %for.cond10
  %m_next13 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %10 = load i32, i32* %i, align 4
  %call14 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_next13, i32 %10)
  store i32 -1, i32* %call14, align 4
  br label %for.inc15

for.inc15:                                        ; preds = %for.body12
  %11 = load i32, i32* %i, align 4
  %inc16 = add nsw i32 %11, 1
  store i32 %inc16, i32* %i, align 4
  br label %for.cond10

for.end17:                                        ; preds = %for.cond10
  store i32 0, i32* %i, align 4
  br label %for.cond18

for.cond18:                                       ; preds = %for.inc31, %for.end17
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %curHashtableSize, align 4
  %cmp19 = icmp slt i32 %12, %13
  br i1 %cmp19, label %for.body20, label %for.end33

for.body20:                                       ; preds = %for.cond18
  %m_keyArray = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 3
  %14 = load i32, i32* %i, align 4
  %call21 = call nonnull align 2 dereferenceable(4) %struct.btInternalVertexPair* @_ZN20btAlignedObjectArrayI20btInternalVertexPairEixEi(%class.btAlignedObjectArray.12* %m_keyArray, i32 %14)
  %call22 = call i32 @_ZNK20btInternalVertexPair7getHashEv(%struct.btInternalVertexPair* %call21)
  %m_valueArray23 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 2
  %call24 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %m_valueArray23)
  %sub = sub nsw i32 %call24, 1
  %and = and i32 %call22, %sub
  store i32 %and, i32* %hashValue, align 4
  %m_hashTable25 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %15 = load i32, i32* %hashValue, align 4
  %call26 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_hashTable25, i32 %15)
  %16 = load i32, i32* %call26, align 4
  %m_next27 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 1
  %17 = load i32, i32* %i, align 4
  %call28 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_next27, i32 %17)
  store i32 %16, i32* %call28, align 4
  %18 = load i32, i32* %i, align 4
  %m_hashTable29 = getelementptr inbounds %class.btHashMap, %class.btHashMap* %this1, i32 0, i32 0
  %19 = load i32, i32* %hashValue, align 4
  %call30 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.3* %m_hashTable29, i32 %19)
  store i32 %18, i32* %call30, align 4
  br label %for.inc31

for.inc31:                                        ; preds = %for.body20
  %20 = load i32, i32* %i, align 4
  %inc32 = add nsw i32 %20, 1
  store i32 %inc32, i32* %i, align 4
  br label %for.cond18

for.end33:                                        ; preds = %for.cond18
  br label %if.end

if.end:                                           ; preds = %for.end33, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI14btInternalEdgeE7reserveEi(%class.btAlignedObjectArray.8* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btInternalEdge*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE8capacityEv(%class.btAlignedObjectArray.8* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI14btInternalEdgeE8allocateEi(%class.btAlignedObjectArray.8* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btInternalEdge*
  store %struct.btInternalEdge* %2, %struct.btInternalEdge** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  %3 = load %struct.btInternalEdge*, %struct.btInternalEdge** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4copyEiiPS0_(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call3, %struct.btInternalEdge* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI14btInternalEdgeE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btInternalEdge*, %struct.btInternalEdge** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %struct.btInternalEdge* %4, %struct.btInternalEdge** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI14btInternalEdgeE9allocSizeEi(%class.btAlignedObjectArray.8* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI14btInternalEdgeE8allocateEi(%class.btAlignedObjectArray.8* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btInternalEdge* @_ZN18btAlignedAllocatorI14btInternalEdgeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.9* %m_allocator, i32 %1, %struct.btInternalEdge** null)
  %2 = bitcast %struct.btInternalEdge* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI14btInternalEdgeE4copyEiiPS0_(%class.btAlignedObjectArray.8* %this, i32 %start, i32 %end, %struct.btInternalEdge* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btInternalEdge*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btInternalEdge* %dest, %struct.btInternalEdge** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btInternalEdge*, %struct.btInternalEdge** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %3, i32 %4
  %5 = bitcast %struct.btInternalEdge* %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btInternalEdge*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %7 = load %struct.btInternalEdge*, %struct.btInternalEdge** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btInternalEdge, %struct.btInternalEdge* %7, i32 %8
  %9 = bitcast %struct.btInternalEdge* %6 to i8*
  %10 = bitcast %struct.btInternalEdge* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %9, i8* align 2 %10, i32 4, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btInternalEdge* @_ZN18btAlignedAllocatorI14btInternalEdgeLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.9* %this, i32 %n, %struct.btInternalEdge** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btInternalEdge**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btInternalEdge** %hint, %struct.btInternalEdge*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btInternalEdge*
  ret %struct.btInternalEdge* %1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE8capacityEv(%class.btAlignedObjectArray.12* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE7reserveEi(%class.btAlignedObjectArray.12* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %struct.btInternalVertexPair*, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE8capacityEv(%class.btAlignedObjectArray.12* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayI20btInternalVertexPairE8allocateEi(%class.btAlignedObjectArray.12* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %struct.btInternalVertexPair*
  store %struct.btInternalVertexPair* %2, %struct.btInternalVertexPair** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  %3 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %s, align 4
  call void @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4copyEiiPS0_(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call3, %struct.btInternalVertexPair* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4sizeEv(%class.btAlignedObjectArray.12* %this1)
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE7destroyEii(%class.btAlignedObjectArray.12* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayI20btInternalVertexPairE10deallocateEv(%class.btAlignedObjectArray.12* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  store %struct.btInternalVertexPair* %4, %struct.btInternalVertexPair** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayI20btInternalVertexPairE9allocSizeEi(%class.btAlignedObjectArray.12* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayI20btInternalVertexPairE8allocateEi(%class.btAlignedObjectArray.12* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %struct.btInternalVertexPair* @_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %m_allocator, i32 %1, %struct.btInternalVertexPair** null)
  %2 = bitcast %struct.btInternalVertexPair* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayI20btInternalVertexPairE4copyEiiPS0_(%class.btAlignedObjectArray.12* %this, i32 %start, i32 %end, %struct.btInternalVertexPair* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %struct.btInternalVertexPair*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %struct.btInternalVertexPair* %dest, %struct.btInternalVertexPair** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %3, i32 %4
  %5 = bitcast %struct.btInternalVertexPair* %arrayidx to i8*
  %6 = bitcast i8* %5 to %struct.btInternalVertexPair*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %7 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %7, i32 %8
  %9 = bitcast %struct.btInternalVertexPair* %6 to i8*
  %10 = bitcast %struct.btInternalVertexPair* %arrayidx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 2 %9, i8* align 2 %10, i32 4, i1 false)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.btInternalVertexPair* @_ZN18btAlignedAllocatorI20btInternalVertexPairLj16EE8allocateEiPPKS0_(%class.btAlignedAllocator.13* %this, i32 %n, %struct.btInternalVertexPair** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.13*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %struct.btInternalVertexPair**, align 4
  store %class.btAlignedAllocator.13* %this, %class.btAlignedAllocator.13** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %struct.btInternalVertexPair** %hint, %struct.btInternalVertexPair*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.13*, %class.btAlignedAllocator.13** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %struct.btInternalVertexPair*
  ret %struct.btInternalVertexPair* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE6resizeEiRKi(%class.btAlignedObjectArray.3* %this, i32 %newsize, i32* nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca i32*, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store i32* %fillData, i32** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %5 = load i32*, i32** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.3* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %14 = load i32*, i32** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %14, i32 %15
  %16 = bitcast i32* %arrayidx10 to i8*
  %17 = bitcast i8* %16 to i32*
  %18 = load i32*, i32** %fillData.addr, align 4
  %19 = load i32, i32* %18, align 4
  store i32 %19, i32* %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 2 dereferenceable(4) %struct.btInternalVertexPair* @_ZN20btAlignedObjectArrayI20btInternalVertexPairEixEi(%class.btAlignedObjectArray.12* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.12*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.12* %this, %class.btAlignedObjectArray.12** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.12*, %class.btAlignedObjectArray.12** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.12, %class.btAlignedObjectArray.12* %this1, i32 0, i32 4
  %0 = load %struct.btInternalVertexPair*, %struct.btInternalVertexPair** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btInternalVertexPair, %struct.btInternalVertexPair* %0, i32 %1
  ret %struct.btInternalVertexPair* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7reserveEi(%class.btAlignedObjectArray.3* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca i32*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.3* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.3* %this1, i32 %1)
  %2 = bitcast i8* %call2 to i32*
  store i32* %2, i32** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  %3 = load i32*, i32** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call3, i32* %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.3* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.3* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.3* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load i32*, i32** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  store i32* %4, i32** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE8capacityEv(%class.btAlignedObjectArray.3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIiE8allocateEi(%class.btAlignedObjectArray.3* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.4* %m_allocator, i32 %1, i32** null)
  %2 = bitcast i32* %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIiE4copyEiiPi(%class.btAlignedObjectArray.3* %this, i32 %start, i32 %end, i32* %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.3*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca i32*, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.3* %this, %class.btAlignedObjectArray.3** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store i32* %dest, i32** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.3*, %class.btAlignedObjectArray.3** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load i32*, i32** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  %5 = bitcast i32* %arrayidx to i8*
  %6 = bitcast i8* %5 to i32*
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.3, %class.btAlignedObjectArray.3* %this1, i32 0, i32 4
  %7 = load i32*, i32** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds i32, i32* %7, i32 %8
  %9 = load i32, i32* %arrayidx2, align 4
  store i32 %9, i32* %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32* @_ZN18btAlignedAllocatorIiLj16EE8allocateEiPPKi(%class.btAlignedAllocator.4* %this, i32 %n, i32** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.4*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca i32**, align 4
  store %class.btAlignedAllocator.4* %this, %class.btAlignedAllocator.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store i32** %hint, i32*** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.4*, %class.btAlignedAllocator.4** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to i32*
  ret i32* %1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btConvexPolyhedron.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { nounwind readnone speculatable willreturn }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
