; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btBox2dBox2dCollisionAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btBox2dBox2dCollisionAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btBox2dBox2dCollisionAlgorithm = type { %class.btActivatingCollisionAlgorithm, i8, %class.btPersistentManifold* }
%class.btActivatingCollisionAlgorithm = type { %class.btCollisionAlgorithm }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.0, %union.anon.1, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.0 = type { float }
%union.anon.1 = type { float }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type opaque
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32, float }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%class.btBox2dShape = type { %class.btPolyhedralConvexShape, %class.btVector3, [4 x %class.btVector3], [4 x %class.btVector3] }
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btConvexPolyhedron = type opaque
%struct.ClipVertex = type { %class.btVector3, i32 }
%class.btAlignedObjectArray.2 = type <{ %class.btAlignedAllocator.3, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.3 = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZNK24btCollisionObjectWrapper18getCollisionObjectEv = comdat any

$_ZNK24btCollisionObjectWrapper17getCollisionShapeEv = comdat any

$_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold = comdat any

$_ZNK24btCollisionObjectWrapper17getWorldTransformEv = comdat any

$_ZN16btManifoldResult20refreshContactPointsEv = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN10ClipVertexC2Ev = comdat any

$_ZNK12btBox2dShape14getVertexCountEv = comdat any

$_ZNK12btBox2dShape11getVerticesEv = comdat any

$_ZmlRK11btMatrix3x3RK9btVector3 = comdat any

$_ZN11btTransform8getBasisEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK11btTransformmlERK9btVector3 = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZngRK9btVector3 = comdat any

$_ZN30btBox2dBox2dCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE = comdat any

$_ZNK20btPersistentManifold14getNumContactsEv = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK17btCollisionObject17getWorldTransformEv = comdat any

$_ZNK12btBox2dShape10getNormalsEv = comdat any

$_ZNK12btBox2dShape11getCentroidEv = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZNK9btVector36maxDotEPKS_lRf = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector36minDotEPKS_lRf = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV30btBox2dBox2dCollisionAlgorithm = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI30btBox2dBox2dCollisionAlgorithm to i8*), i8* bitcast (%class.btBox2dBox2dCollisionAlgorithm* (%class.btBox2dBox2dCollisionAlgorithm*)* @_ZN30btBox2dBox2dCollisionAlgorithmD1Ev to i8*), i8* bitcast (void (%class.btBox2dBox2dCollisionAlgorithm*)* @_ZN30btBox2dBox2dCollisionAlgorithmD0Ev to i8*), i8* bitcast (void (%class.btBox2dBox2dCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN30btBox2dBox2dCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (float (%class.btBox2dBox2dCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN30btBox2dBox2dCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (void (%class.btBox2dBox2dCollisionAlgorithm*, %class.btAlignedObjectArray.2*)* @_ZN30btBox2dBox2dCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE to i8*)] }, align 4
@b2_maxManifoldPoints = hidden global i32 2, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS30btBox2dBox2dCollisionAlgorithm = hidden constant [33 x i8] c"30btBox2dBox2dCollisionAlgorithm\00", align 1
@_ZTI30btActivatingCollisionAlgorithm = external constant i8*
@_ZTI30btBox2dBox2dCollisionAlgorithm = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([33 x i8], [33 x i8]* @_ZTS30btBox2dBox2dCollisionAlgorithm, i32 0, i32 0), i8* bitcast (i8** @_ZTI30btActivatingCollisionAlgorithm to i8*) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btBox2dBox2dCollisionAlgorithm.cpp, i8* null }]

@_ZN30btBox2dBox2dCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_ = hidden unnamed_addr alias %class.btBox2dBox2dCollisionAlgorithm* (%class.btBox2dBox2dCollisionAlgorithm*, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*), %class.btBox2dBox2dCollisionAlgorithm* (%class.btBox2dBox2dCollisionAlgorithm*, %class.btPersistentManifold*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)* @_ZN30btBox2dBox2dCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_
@_ZN30btBox2dBox2dCollisionAlgorithmD1Ev = hidden unnamed_addr alias %class.btBox2dBox2dCollisionAlgorithm* (%class.btBox2dBox2dCollisionAlgorithm*), %class.btBox2dBox2dCollisionAlgorithm* (%class.btBox2dBox2dCollisionAlgorithm*)* @_ZN30btBox2dBox2dCollisionAlgorithmD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btBox2dBox2dCollisionAlgorithm* @_ZN30btBox2dBox2dCollisionAlgorithmC2EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS7_(%class.btBox2dBox2dCollisionAlgorithm* returned %this, %class.btPersistentManifold* %mf, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper* %obj1Wrap) unnamed_addr #2 {
entry:
  %retval = alloca %class.btBox2dBox2dCollisionAlgorithm*, align 4
  %this.addr = alloca %class.btBox2dBox2dCollisionAlgorithm*, align 4
  %mf.addr = alloca %class.btPersistentManifold*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  %obj0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %obj1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %class.btBox2dBox2dCollisionAlgorithm* %this, %class.btBox2dBox2dCollisionAlgorithm** %this.addr, align 4
  store %class.btPersistentManifold* %mf, %class.btPersistentManifold** %mf.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  store %struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper** %obj0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %obj1Wrap, %struct.btCollisionObjectWrapper** %obj1Wrap.addr, align 4
  %this1 = load %class.btBox2dBox2dCollisionAlgorithm*, %class.btBox2dBox2dCollisionAlgorithm** %this.addr, align 4
  store %class.btBox2dBox2dCollisionAlgorithm* %this1, %class.btBox2dBox2dCollisionAlgorithm** %retval, align 4
  %0 = bitcast %class.btBox2dBox2dCollisionAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj0Wrap.addr, align 4
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj1Wrap.addr, align 4
  %call = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* %0, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %1, %struct.btCollisionObjectWrapper* %2, %struct.btCollisionObjectWrapper* %3)
  %4 = bitcast %class.btBox2dBox2dCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV30btBox2dBox2dCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %4, align 4
  %m_ownManifold = getelementptr inbounds %class.btBox2dBox2dCollisionAlgorithm, %class.btBox2dBox2dCollisionAlgorithm* %this1, i32 0, i32 1
  store i8 0, i8* %m_ownManifold, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btBox2dBox2dCollisionAlgorithm, %class.btBox2dBox2dCollisionAlgorithm* %this1, i32 0, i32 2
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %mf.addr, align 4
  store %class.btPersistentManifold* %5, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %m_manifoldPtr2 = getelementptr inbounds %class.btBox2dBox2dCollisionAlgorithm, %class.btBox2dBox2dCollisionAlgorithm* %this1, i32 0, i32 2
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr2, align 4
  %tobool = icmp ne %class.btPersistentManifold* %6, null
  br i1 %tobool, label %if.end, label %land.lhs.true

land.lhs.true:                                    ; preds = %entry
  %7 = bitcast %class.btBox2dBox2dCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %7, i32 0, i32 1
  %8 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj0Wrap.addr, align 4
  %call3 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %9)
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj1Wrap.addr, align 4
  %call4 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %10)
  %11 = bitcast %class.btDispatcher* %8 to i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable = load i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*** %11, align 4
  %vfn = getelementptr inbounds i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable, i64 6
  %12 = load i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn, align 4
  %call5 = call zeroext i1 %12(%class.btDispatcher* %8, %class.btCollisionObject* %call3, %class.btCollisionObject* %call4)
  br i1 %call5, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %13 = bitcast %class.btBox2dBox2dCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher6 = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %13, i32 0, i32 1
  %14 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher6, align 4
  %15 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj0Wrap.addr, align 4
  %call7 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %15)
  %16 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %obj1Wrap.addr, align 4
  %call8 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %16)
  %17 = bitcast %class.btDispatcher* %14 to %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable9 = load %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)**, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*** %17, align 4
  %vfn10 = getelementptr inbounds %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable9, i64 3
  %18 = load %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, %class.btPersistentManifold* (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn10, align 4
  %call11 = call %class.btPersistentManifold* %18(%class.btDispatcher* %14, %class.btCollisionObject* %call7, %class.btCollisionObject* %call8)
  %m_manifoldPtr12 = getelementptr inbounds %class.btBox2dBox2dCollisionAlgorithm, %class.btBox2dBox2dCollisionAlgorithm* %this1, i32 0, i32 2
  store %class.btPersistentManifold* %call11, %class.btPersistentManifold** %m_manifoldPtr12, align 4
  %m_ownManifold13 = getelementptr inbounds %class.btBox2dBox2dCollisionAlgorithm, %class.btBox2dBox2dCollisionAlgorithm* %this1, i32 0, i32 1
  store i8 1, i8* %m_ownManifold13, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  %19 = load %class.btBox2dBox2dCollisionAlgorithm*, %class.btBox2dBox2dCollisionAlgorithm** %retval, align 4
  ret %class.btBox2dBox2dCollisionAlgorithm* %19
}

declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfoPK24btCollisionObjectWrapperS5_(%class.btActivatingCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8), %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_collisionObject, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btBox2dBox2dCollisionAlgorithm* @_ZN30btBox2dBox2dCollisionAlgorithmD2Ev(%class.btBox2dBox2dCollisionAlgorithm* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btBox2dBox2dCollisionAlgorithm*, align 4
  %this.addr = alloca %class.btBox2dBox2dCollisionAlgorithm*, align 4
  store %class.btBox2dBox2dCollisionAlgorithm* %this, %class.btBox2dBox2dCollisionAlgorithm** %this.addr, align 4
  %this1 = load %class.btBox2dBox2dCollisionAlgorithm*, %class.btBox2dBox2dCollisionAlgorithm** %this.addr, align 4
  store %class.btBox2dBox2dCollisionAlgorithm* %this1, %class.btBox2dBox2dCollisionAlgorithm** %retval, align 4
  %0 = bitcast %class.btBox2dBox2dCollisionAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV30btBox2dBox2dCollisionAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_ownManifold = getelementptr inbounds %class.btBox2dBox2dCollisionAlgorithm, %class.btBox2dBox2dCollisionAlgorithm* %this1, i32 0, i32 1
  %1 = load i8, i8* %m_ownManifold, align 4
  %tobool = trunc i8 %1 to i1
  br i1 %tobool, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %m_manifoldPtr = getelementptr inbounds %class.btBox2dBox2dCollisionAlgorithm, %class.btBox2dBox2dCollisionAlgorithm* %this1, i32 0, i32 2
  %2 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %tobool2 = icmp ne %class.btPersistentManifold* %2, null
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %3 = bitcast %class.btBox2dBox2dCollisionAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %m_dispatcher = getelementptr inbounds %class.btCollisionAlgorithm, %class.btCollisionAlgorithm* %3, i32 0, i32 1
  %4 = load %class.btDispatcher*, %class.btDispatcher** %m_dispatcher, align 4
  %m_manifoldPtr4 = getelementptr inbounds %class.btBox2dBox2dCollisionAlgorithm, %class.btBox2dBox2dCollisionAlgorithm* %this1, i32 0, i32 2
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr4, align 4
  %6 = bitcast %class.btDispatcher* %4 to void (%class.btDispatcher*, %class.btPersistentManifold*)***
  %vtable = load void (%class.btDispatcher*, %class.btPersistentManifold*)**, void (%class.btDispatcher*, %class.btPersistentManifold*)*** %6, align 4
  %vfn = getelementptr inbounds void (%class.btDispatcher*, %class.btPersistentManifold*)*, void (%class.btDispatcher*, %class.btPersistentManifold*)** %vtable, i64 4
  %7 = load void (%class.btDispatcher*, %class.btPersistentManifold*)*, void (%class.btDispatcher*, %class.btPersistentManifold*)** %vfn, align 4
  call void %7(%class.btDispatcher* %4, %class.btPersistentManifold* %5)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  br label %if.end5

if.end5:                                          ; preds = %if.end, %entry
  %8 = bitcast %class.btBox2dBox2dCollisionAlgorithm* %this1 to %class.btActivatingCollisionAlgorithm*
  %call = call %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* %8) #8
  %9 = load %class.btBox2dBox2dCollisionAlgorithm*, %class.btBox2dBox2dCollisionAlgorithm** %retval, align 4
  ret %class.btBox2dBox2dCollisionAlgorithm* %9
}

; Function Attrs: nounwind
declare %class.btActivatingCollisionAlgorithm* @_ZN30btActivatingCollisionAlgorithmD2Ev(%class.btActivatingCollisionAlgorithm* returned) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN30btBox2dBox2dCollisionAlgorithmD0Ev(%class.btBox2dBox2dCollisionAlgorithm* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btBox2dBox2dCollisionAlgorithm*, align 4
  store %class.btBox2dBox2dCollisionAlgorithm* %this, %class.btBox2dBox2dCollisionAlgorithm** %this.addr, align 4
  %this1 = load %class.btBox2dBox2dCollisionAlgorithm*, %class.btBox2dBox2dCollisionAlgorithm** %this.addr, align 4
  %call = call %class.btBox2dBox2dCollisionAlgorithm* @_ZN30btBox2dBox2dCollisionAlgorithmD1Ev(%class.btBox2dBox2dCollisionAlgorithm* %this1) #8
  %0 = bitcast %class.btBox2dBox2dCollisionAlgorithm* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #5

; Function Attrs: noinline optnone
define hidden void @_ZN30btBox2dBox2dCollisionAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btBox2dBox2dCollisionAlgorithm* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btManifoldResult* %resultOut) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btBox2dBox2dCollisionAlgorithm*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %resultOut.addr = alloca %class.btManifoldResult*, align 4
  %box0 = alloca %class.btBox2dShape*, align 4
  %box1 = alloca %class.btBox2dShape*, align 4
  store %class.btBox2dBox2dCollisionAlgorithm* %this, %class.btBox2dBox2dCollisionAlgorithm** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %class.btManifoldResult* %resultOut, %class.btManifoldResult** %resultOut.addr, align 4
  %this1 = load %class.btBox2dBox2dCollisionAlgorithm*, %class.btBox2dBox2dCollisionAlgorithm** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btBox2dBox2dCollisionAlgorithm, %class.btBox2dBox2dCollisionAlgorithm* %this1, i32 0, i32 2
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %tobool = icmp ne %class.btPersistentManifold* %0, null
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %if.end8

if.end:                                           ; preds = %entry
  %1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %1)
  %2 = bitcast %class.btCollisionShape* %call to %class.btBox2dShape*
  store %class.btBox2dShape* %2, %class.btBox2dShape** %box0, align 4
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call2 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %3)
  %4 = bitcast %class.btCollisionShape* %call2 to %class.btBox2dShape*
  store %class.btBox2dShape* %4, %class.btBox2dShape** %box1, align 4
  %5 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %m_manifoldPtr3 = getelementptr inbounds %class.btBox2dBox2dCollisionAlgorithm, %class.btBox2dBox2dCollisionAlgorithm* %this1, i32 0, i32 2
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr3, align 4
  call void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %5, %class.btPersistentManifold* %6)
  %7 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  %8 = load %class.btBox2dShape*, %class.btBox2dShape** %box0, align 4
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %9)
  %10 = load %class.btBox2dShape*, %class.btBox2dShape** %box1, align 4
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %11)
  call void @_Z17b2CollidePolygonsP16btManifoldResultPK12btBox2dShapeRK11btTransformS3_S6_(%class.btManifoldResult* %7, %class.btBox2dShape* %8, %class.btTransform* nonnull align 4 dereferenceable(64) %call4, %class.btBox2dShape* %10, %class.btTransform* nonnull align 4 dereferenceable(64) %call5)
  %m_ownManifold = getelementptr inbounds %class.btBox2dBox2dCollisionAlgorithm, %class.btBox2dBox2dCollisionAlgorithm* %this1, i32 0, i32 1
  %12 = load i8, i8* %m_ownManifold, align 4
  %tobool6 = trunc i8 %12 to i1
  br i1 %tobool6, label %if.then7, label %if.end8

if.then7:                                         ; preds = %if.end
  %13 = load %class.btManifoldResult*, %class.btManifoldResult** %resultOut.addr, align 4
  call void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %13)
  br label %if.end8

if.end8:                                          ; preds = %if.then, %if.then7, %if.end
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_shape, align 4
  ret %class.btCollisionShape* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btManifoldResult21setPersistentManifoldEP20btPersistentManifold(%class.btManifoldResult* %this, %class.btPersistentManifold* %manifoldPtr) #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %manifoldPtr.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  store %class.btPersistentManifold* %manifoldPtr, %class.btPersistentManifold** %manifoldPtr.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifoldPtr.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  store %class.btPersistentManifold* %0, %class.btPersistentManifold** %m_manifoldPtr, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_Z17b2CollidePolygonsP16btManifoldResultPK12btBox2dShapeRK11btTransformS3_S6_(%class.btManifoldResult* %manifold, %class.btBox2dShape* %polyA, %class.btTransform* nonnull align 4 dereferenceable(64) %xfA, %class.btBox2dShape* %polyB, %class.btTransform* nonnull align 4 dereferenceable(64) %xfB) #2 {
entry:
  %manifold.addr = alloca %class.btManifoldResult*, align 4
  %polyA.addr = alloca %class.btBox2dShape*, align 4
  %xfA.addr = alloca %class.btTransform*, align 4
  %polyB.addr = alloca %class.btBox2dShape*, align 4
  %xfB.addr = alloca %class.btTransform*, align 4
  %edgeA = alloca i32, align 4
  %separationA = alloca float, align 4
  %edgeB = alloca i32, align 4
  %separationB = alloca float, align 4
  %poly1 = alloca %class.btBox2dShape*, align 4
  %poly2 = alloca %class.btBox2dShape*, align 4
  %xf1 = alloca %class.btTransform, align 4
  %xf2 = alloca %class.btTransform, align 4
  %edge1 = alloca i32, align 4
  %flip = alloca i8, align 1
  %k_relativeTol = alloca float, align 4
  %k_absoluteTol = alloca float, align 4
  %incidentEdge = alloca [2 x %struct.ClipVertex], align 16
  %count1 = alloca i32, align 4
  %vertices1 = alloca %class.btVector3*, align 4
  %v11 = alloca %class.btVector3, align 4
  %v12 = alloca %class.btVector3, align 4
  %sideNormal = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %frontNormal = alloca %class.btVector3, align 4
  %ref.tmp24 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp30 = alloca float, align 4
  %ref.tmp32 = alloca %class.btVector3, align 4
  %ref.tmp33 = alloca %class.btVector3, align 4
  %frontOffset = alloca float, align 4
  %sideOffset1 = alloca float, align 4
  %sideOffset2 = alloca float, align 4
  %clipPoints1 = alloca [2 x %struct.ClipVertex], align 16
  %ref.tmp46 = alloca float, align 4
  %ref.tmp47 = alloca float, align 4
  %ref.tmp48 = alloca float, align 4
  %ref.tmp51 = alloca float, align 4
  %ref.tmp52 = alloca float, align 4
  %ref.tmp53 = alloca float, align 4
  %clipPoints2 = alloca [2 x %struct.ClipVertex], align 16
  %ref.tmp64 = alloca float, align 4
  %ref.tmp65 = alloca float, align 4
  %ref.tmp66 = alloca float, align 4
  %ref.tmp69 = alloca float, align 4
  %ref.tmp70 = alloca float, align 4
  %ref.tmp71 = alloca float, align 4
  %np = alloca i32, align 4
  %ref.tmp74 = alloca %class.btVector3, align 4
  %manifoldNormal = alloca %class.btVector3, align 4
  %pointCount = alloca i32, align 4
  %i = alloca i32, align 4
  %separation = alloca float, align 4
  %ref.tmp94 = alloca %class.btVector3, align 4
  store %class.btManifoldResult* %manifold, %class.btManifoldResult** %manifold.addr, align 4
  store %class.btBox2dShape* %polyA, %class.btBox2dShape** %polyA.addr, align 4
  store %class.btTransform* %xfA, %class.btTransform** %xfA.addr, align 4
  store %class.btBox2dShape* %polyB, %class.btBox2dShape** %polyB.addr, align 4
  store %class.btTransform* %xfB, %class.btTransform** %xfB.addr, align 4
  store i32 0, i32* %edgeA, align 4
  %0 = load %class.btBox2dShape*, %class.btBox2dShape** %polyA.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %xfA.addr, align 4
  %2 = load %class.btBox2dShape*, %class.btBox2dShape** %polyB.addr, align 4
  %3 = load %class.btTransform*, %class.btTransform** %xfB.addr, align 4
  %call = call float @_ZL17FindMaxSeparationPiPK12btBox2dShapeRK11btTransformS2_S5_(i32* %edgeA, %class.btBox2dShape* %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btBox2dShape* %2, %class.btTransform* nonnull align 4 dereferenceable(64) %3)
  store float %call, float* %separationA, align 4
  %4 = load float, float* %separationA, align 4
  %cmp = fcmp ogt float %4, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %for.end

if.end:                                           ; preds = %entry
  store i32 0, i32* %edgeB, align 4
  %5 = load %class.btBox2dShape*, %class.btBox2dShape** %polyB.addr, align 4
  %6 = load %class.btTransform*, %class.btTransform** %xfB.addr, align 4
  %7 = load %class.btBox2dShape*, %class.btBox2dShape** %polyA.addr, align 4
  %8 = load %class.btTransform*, %class.btTransform** %xfA.addr, align 4
  %call1 = call float @_ZL17FindMaxSeparationPiPK12btBox2dShapeRK11btTransformS2_S5_(i32* %edgeB, %class.btBox2dShape* %5, %class.btTransform* nonnull align 4 dereferenceable(64) %6, %class.btBox2dShape* %7, %class.btTransform* nonnull align 4 dereferenceable(64) %8)
  store float %call1, float* %separationB, align 4
  %9 = load float, float* %separationB, align 4
  %cmp2 = fcmp ogt float %9, 0.000000e+00
  br i1 %cmp2, label %if.then3, label %if.end4

if.then3:                                         ; preds = %if.end
  br label %for.end

if.end4:                                          ; preds = %if.end
  %call5 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %xf1)
  %call6 = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %xf2)
  store float 0x3FEF5C2900000000, float* %k_relativeTol, align 4
  store float 0x3F50624DE0000000, float* %k_absoluteTol, align 4
  %10 = load float, float* %separationB, align 4
  %11 = load float, float* %separationA, align 4
  %mul = fmul float 0x3FEF5C2900000000, %11
  %add = fadd float %mul, 0x3F50624DE0000000
  %cmp7 = fcmp ogt float %10, %add
  br i1 %cmp7, label %if.then8, label %if.else

if.then8:                                         ; preds = %if.end4
  %12 = load %class.btBox2dShape*, %class.btBox2dShape** %polyB.addr, align 4
  store %class.btBox2dShape* %12, %class.btBox2dShape** %poly1, align 4
  %13 = load %class.btBox2dShape*, %class.btBox2dShape** %polyA.addr, align 4
  store %class.btBox2dShape* %13, %class.btBox2dShape** %poly2, align 4
  %14 = load %class.btTransform*, %class.btTransform** %xfB.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %xf1, %class.btTransform* nonnull align 4 dereferenceable(64) %14)
  %15 = load %class.btTransform*, %class.btTransform** %xfA.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %xf2, %class.btTransform* nonnull align 4 dereferenceable(64) %15)
  %16 = load i32, i32* %edgeB, align 4
  store i32 %16, i32* %edge1, align 4
  store i8 1, i8* %flip, align 1
  br label %if.end13

if.else:                                          ; preds = %if.end4
  %17 = load %class.btBox2dShape*, %class.btBox2dShape** %polyA.addr, align 4
  store %class.btBox2dShape* %17, %class.btBox2dShape** %poly1, align 4
  %18 = load %class.btBox2dShape*, %class.btBox2dShape** %polyB.addr, align 4
  store %class.btBox2dShape* %18, %class.btBox2dShape** %poly2, align 4
  %19 = load %class.btTransform*, %class.btTransform** %xfA.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %xf1, %class.btTransform* nonnull align 4 dereferenceable(64) %19)
  %20 = load %class.btTransform*, %class.btTransform** %xfB.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %xf2, %class.btTransform* nonnull align 4 dereferenceable(64) %20)
  %21 = load i32, i32* %edgeA, align 4
  store i32 %21, i32* %edge1, align 4
  store i8 0, i8* %flip, align 1
  br label %if.end13

if.end13:                                         ; preds = %if.else, %if.then8
  %array.begin = getelementptr inbounds [2 x %struct.ClipVertex], [2 x %struct.ClipVertex]* %incidentEdge, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %array.begin, i32 2
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %if.end13
  %arrayctor.cur = phi %struct.ClipVertex* [ %array.begin, %if.end13 ], [ %arrayctor.next, %arrayctor.loop ]
  %call14 = call %struct.ClipVertex* @_ZN10ClipVertexC2Ev(%struct.ClipVertex* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %struct.ClipVertex* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %arraydecay = getelementptr inbounds [2 x %struct.ClipVertex], [2 x %struct.ClipVertex]* %incidentEdge, i32 0, i32 0
  %22 = load %class.btBox2dShape*, %class.btBox2dShape** %poly1, align 4
  %23 = load i32, i32* %edge1, align 4
  %24 = load %class.btBox2dShape*, %class.btBox2dShape** %poly2, align 4
  call void @_ZL16FindIncidentEdgeP10ClipVertexPK12btBox2dShapeRK11btTransformiS3_S6_(%struct.ClipVertex* %arraydecay, %class.btBox2dShape* %22, %class.btTransform* nonnull align 4 dereferenceable(64) %xf1, i32 %23, %class.btBox2dShape* %24, %class.btTransform* nonnull align 4 dereferenceable(64) %xf2)
  %25 = load %class.btBox2dShape*, %class.btBox2dShape** %poly1, align 4
  %call15 = call i32 @_ZNK12btBox2dShape14getVertexCountEv(%class.btBox2dShape* %25)
  store i32 %call15, i32* %count1, align 4
  %26 = load %class.btBox2dShape*, %class.btBox2dShape** %poly1, align 4
  %call16 = call %class.btVector3* @_ZNK12btBox2dShape11getVerticesEv(%class.btBox2dShape* %26)
  store %class.btVector3* %call16, %class.btVector3** %vertices1, align 4
  %27 = load %class.btVector3*, %class.btVector3** %vertices1, align 4
  %28 = load i32, i32* %edge1, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %27, i32 %28
  %29 = bitcast %class.btVector3* %v11 to i8*
  %30 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %29, i8* align 4 %30, i32 16, i1 false)
  %31 = load i32, i32* %edge1, align 4
  %add17 = add nsw i32 %31, 1
  %32 = load i32, i32* %count1, align 4
  %cmp18 = icmp slt i32 %add17, %32
  br i1 %cmp18, label %cond.true, label %cond.false

cond.true:                                        ; preds = %arrayctor.cont
  %33 = load %class.btVector3*, %class.btVector3** %vertices1, align 4
  %34 = load i32, i32* %edge1, align 4
  %add19 = add nsw i32 %34, 1
  %arrayidx20 = getelementptr inbounds %class.btVector3, %class.btVector3* %33, i32 %add19
  br label %cond.end

cond.false:                                       ; preds = %arrayctor.cont
  %35 = load %class.btVector3*, %class.btVector3** %vertices1, align 4
  %arrayidx21 = getelementptr inbounds %class.btVector3, %class.btVector3* %35, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi %class.btVector3* [ %arrayidx20, %cond.true ], [ %arrayidx21, %cond.false ]
  %36 = bitcast %class.btVector3* %v12 to i8*
  %37 = bitcast %class.btVector3* %cond-lvalue to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %36, i8* align 4 %37, i32 16, i1 false)
  %call22 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %xf1)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %v12, %class.btVector3* nonnull align 4 dereferenceable(16) %v11)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %sideNormal, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call22, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %call23 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %sideNormal)
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %sideNormal)
  %38 = load float, float* %call25, align 4
  %mul26 = fmul float 1.000000e+00, %38
  store float %mul26, float* %ref.tmp24, align 4
  %call28 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %sideNormal)
  %39 = load float, float* %call28, align 4
  %mul29 = fmul float -1.000000e+00, %39
  store float %mul29, float* %ref.tmp27, align 4
  store float 0.000000e+00, float* %ref.tmp30, align 4
  %call31 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %frontNormal, float* nonnull align 4 dereferenceable(4) %ref.tmp24, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp30)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp32, %class.btTransform* %xf1, %class.btVector3* nonnull align 4 dereferenceable(16) %v11)
  %40 = bitcast %class.btVector3* %v11 to i8*
  %41 = bitcast %class.btVector3* %ref.tmp32 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %40, i8* align 4 %41, i32 16, i1 false)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp33, %class.btTransform* %xf1, %class.btVector3* nonnull align 4 dereferenceable(16) %v12)
  %42 = bitcast %class.btVector3* %v12 to i8*
  %43 = bitcast %class.btVector3* %ref.tmp33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %42, i8* align 4 %43, i32 16, i1 false)
  %call34 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %frontNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %v11)
  store float %call34, float* %frontOffset, align 4
  %call35 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %sideNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %v11)
  %fneg = fneg float %call35
  store float %fneg, float* %sideOffset1, align 4
  %call36 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %sideNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %v12)
  store float %call36, float* %sideOffset2, align 4
  %array.begin37 = getelementptr inbounds [2 x %struct.ClipVertex], [2 x %struct.ClipVertex]* %clipPoints1, i32 0, i32 0
  %arrayctor.end38 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %array.begin37, i32 2
  br label %arrayctor.loop39

arrayctor.loop39:                                 ; preds = %arrayctor.loop39, %cond.end
  %arrayctor.cur40 = phi %struct.ClipVertex* [ %array.begin37, %cond.end ], [ %arrayctor.next42, %arrayctor.loop39 ]
  %call41 = call %struct.ClipVertex* @_ZN10ClipVertexC2Ev(%struct.ClipVertex* %arrayctor.cur40)
  %arrayctor.next42 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayctor.cur40, i32 1
  %arrayctor.done43 = icmp eq %struct.ClipVertex* %arrayctor.next42, %arrayctor.end38
  br i1 %arrayctor.done43, label %arrayctor.cont44, label %arrayctor.loop39

arrayctor.cont44:                                 ; preds = %arrayctor.loop39
  %arrayidx45 = getelementptr inbounds [2 x %struct.ClipVertex], [2 x %struct.ClipVertex]* %clipPoints1, i32 0, i32 0
  %v = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx45, i32 0, i32 0
  store float 0.000000e+00, float* %ref.tmp46, align 4
  store float 0.000000e+00, float* %ref.tmp47, align 4
  store float 0.000000e+00, float* %ref.tmp48, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %v, float* nonnull align 4 dereferenceable(4) %ref.tmp46, float* nonnull align 4 dereferenceable(4) %ref.tmp47, float* nonnull align 4 dereferenceable(4) %ref.tmp48)
  %arrayidx49 = getelementptr inbounds [2 x %struct.ClipVertex], [2 x %struct.ClipVertex]* %clipPoints1, i32 0, i32 1
  %v50 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx49, i32 0, i32 0
  store float 0.000000e+00, float* %ref.tmp51, align 4
  store float 0.000000e+00, float* %ref.tmp52, align 4
  store float 0.000000e+00, float* %ref.tmp53, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %v50, float* nonnull align 4 dereferenceable(4) %ref.tmp51, float* nonnull align 4 dereferenceable(4) %ref.tmp52, float* nonnull align 4 dereferenceable(4) %ref.tmp53)
  %array.begin54 = getelementptr inbounds [2 x %struct.ClipVertex], [2 x %struct.ClipVertex]* %clipPoints2, i32 0, i32 0
  %arrayctor.end55 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %array.begin54, i32 2
  br label %arrayctor.loop56

arrayctor.loop56:                                 ; preds = %arrayctor.loop56, %arrayctor.cont44
  %arrayctor.cur57 = phi %struct.ClipVertex* [ %array.begin54, %arrayctor.cont44 ], [ %arrayctor.next59, %arrayctor.loop56 ]
  %call58 = call %struct.ClipVertex* @_ZN10ClipVertexC2Ev(%struct.ClipVertex* %arrayctor.cur57)
  %arrayctor.next59 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayctor.cur57, i32 1
  %arrayctor.done60 = icmp eq %struct.ClipVertex* %arrayctor.next59, %arrayctor.end55
  br i1 %arrayctor.done60, label %arrayctor.cont61, label %arrayctor.loop56

arrayctor.cont61:                                 ; preds = %arrayctor.loop56
  %arrayidx62 = getelementptr inbounds [2 x %struct.ClipVertex], [2 x %struct.ClipVertex]* %clipPoints2, i32 0, i32 0
  %v63 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx62, i32 0, i32 0
  store float 0.000000e+00, float* %ref.tmp64, align 4
  store float 0.000000e+00, float* %ref.tmp65, align 4
  store float 0.000000e+00, float* %ref.tmp66, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %v63, float* nonnull align 4 dereferenceable(4) %ref.tmp64, float* nonnull align 4 dereferenceable(4) %ref.tmp65, float* nonnull align 4 dereferenceable(4) %ref.tmp66)
  %arrayidx67 = getelementptr inbounds [2 x %struct.ClipVertex], [2 x %struct.ClipVertex]* %clipPoints2, i32 0, i32 1
  %v68 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx67, i32 0, i32 0
  store float 0.000000e+00, float* %ref.tmp69, align 4
  store float 0.000000e+00, float* %ref.tmp70, align 4
  store float 0.000000e+00, float* %ref.tmp71, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %v68, float* nonnull align 4 dereferenceable(4) %ref.tmp69, float* nonnull align 4 dereferenceable(4) %ref.tmp70, float* nonnull align 4 dereferenceable(4) %ref.tmp71)
  %arraydecay72 = getelementptr inbounds [2 x %struct.ClipVertex], [2 x %struct.ClipVertex]* %clipPoints1, i32 0, i32 0
  %arraydecay73 = getelementptr inbounds [2 x %struct.ClipVertex], [2 x %struct.ClipVertex]* %incidentEdge, i32 0, i32 0
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp74, %class.btVector3* nonnull align 4 dereferenceable(16) %sideNormal)
  %44 = load float, float* %sideOffset1, align 4
  %call75 = call i32 @_ZL17ClipSegmentToLineP10ClipVertexS0_RK9btVector3f(%struct.ClipVertex* %arraydecay72, %struct.ClipVertex* %arraydecay73, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp74, float %44)
  store i32 %call75, i32* %np, align 4
  %45 = load i32, i32* %np, align 4
  %cmp76 = icmp slt i32 %45, 2
  br i1 %cmp76, label %if.then77, label %if.end78

if.then77:                                        ; preds = %arrayctor.cont61
  br label %for.end

if.end78:                                         ; preds = %arrayctor.cont61
  %arraydecay79 = getelementptr inbounds [2 x %struct.ClipVertex], [2 x %struct.ClipVertex]* %clipPoints2, i32 0, i32 0
  %arraydecay80 = getelementptr inbounds [2 x %struct.ClipVertex], [2 x %struct.ClipVertex]* %clipPoints1, i32 0, i32 0
  %46 = load float, float* %sideOffset2, align 4
  %call81 = call i32 @_ZL17ClipSegmentToLineP10ClipVertexS0_RK9btVector3f(%struct.ClipVertex* %arraydecay79, %struct.ClipVertex* %arraydecay80, %class.btVector3* nonnull align 4 dereferenceable(16) %sideNormal, float %46)
  store i32 %call81, i32* %np, align 4
  %47 = load i32, i32* %np, align 4
  %cmp82 = icmp slt i32 %47, 2
  br i1 %cmp82, label %if.then83, label %if.end84

if.then83:                                        ; preds = %if.end78
  br label %for.end

if.end84:                                         ; preds = %if.end78
  %48 = load i8, i8* %flip, align 1
  %tobool = icmp ne i8 %48, 0
  br i1 %tobool, label %cond.true85, label %cond.false86

cond.true85:                                      ; preds = %if.end84
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %manifoldNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %frontNormal)
  br label %cond.end87

cond.false86:                                     ; preds = %if.end84
  %49 = bitcast %class.btVector3* %manifoldNormal to i8*
  %50 = bitcast %class.btVector3* %frontNormal to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %49, i8* align 4 %50, i32 16, i1 false)
  br label %cond.end87

cond.end87:                                       ; preds = %cond.false86, %cond.true85
  store i32 0, i32* %pointCount, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %cond.end87
  %51 = load i32, i32* %i, align 4
  %52 = load i32, i32* @b2_maxManifoldPoints, align 4
  %cmp88 = icmp slt i32 %51, %52
  br i1 %cmp88, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %53 = load i32, i32* %i, align 4
  %arrayidx89 = getelementptr inbounds [2 x %struct.ClipVertex], [2 x %struct.ClipVertex]* %clipPoints2, i32 0, i32 %53
  %v90 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx89, i32 0, i32 0
  %call91 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %frontNormal, %class.btVector3* nonnull align 4 dereferenceable(16) %v90)
  %54 = load float, float* %frontOffset, align 4
  %sub = fsub float %call91, %54
  store float %sub, float* %separation, align 4
  %55 = load float, float* %separation, align 4
  %cmp92 = fcmp ole float %55, 0.000000e+00
  br i1 %cmp92, label %if.then93, label %if.end97

if.then93:                                        ; preds = %for.body
  %56 = load %class.btManifoldResult*, %class.btManifoldResult** %manifold.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %manifoldNormal)
  %57 = load i32, i32* %i, align 4
  %arrayidx95 = getelementptr inbounds [2 x %struct.ClipVertex], [2 x %struct.ClipVertex]* %clipPoints2, i32 0, i32 %57
  %v96 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx95, i32 0, i32 0
  %58 = load float, float* %separation, align 4
  %59 = bitcast %class.btManifoldResult* %56 to void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)***
  %vtable = load void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)**, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*** %59, align 4
  %vfn = getelementptr inbounds void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)** %vtable, i64 4
  %60 = load void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)*, void (%class.btManifoldResult*, %class.btVector3*, %class.btVector3*, float)** %vfn, align 4
  call void %60(%class.btManifoldResult* %56, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp94, %class.btVector3* nonnull align 4 dereferenceable(16) %v96, float %58)
  %61 = load i32, i32* %pointCount, align 4
  %inc = add nsw i32 %61, 1
  store i32 %inc, i32* %pointCount, align 4
  br label %if.end97

if.end97:                                         ; preds = %if.then93, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end97
  %62 = load i32, i32* %i, align 4
  %inc98 = add nsw i32 %62, 1
  store i32 %inc98, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %if.then3, %if.then77, %if.then83, %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK24btCollisionObjectWrapper17getWorldTransformEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %0 = load %class.btTransform*, %class.btTransform** %m_worldTransform, align 4
  ret %class.btTransform* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN16btManifoldResult20refreshContactPointsEv(%class.btManifoldResult* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  %isSwapped = alloca i8, align 1
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %call = call i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %0)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %if.end20

if.end:                                           ; preds = %entry
  %m_manifoldPtr2 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr2, align 4
  %call3 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %1)
  %m_body0Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %2 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap, align 4
  %call4 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %2)
  %cmp = icmp ne %class.btCollisionObject* %call3, %call4
  %frombool = zext i1 %cmp to i8
  store i8 %frombool, i8* %isSwapped, align 1
  %3 = load i8, i8* %isSwapped, align 1
  %tobool5 = trunc i8 %3 to i1
  br i1 %tobool5, label %if.then6, label %if.else

if.then6:                                         ; preds = %if.end
  %m_manifoldPtr7 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr7, align 4
  %m_body1Wrap = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %5 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap, align 4
  %call8 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %5)
  %call9 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call8)
  %m_body0Wrap10 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap10, align 4
  %call11 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %6)
  %call12 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call11)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %4, %class.btTransform* nonnull align 4 dereferenceable(64) %call9, %class.btTransform* nonnull align 4 dereferenceable(64) %call12)
  br label %if.end20

if.else:                                          ; preds = %if.end
  %m_manifoldPtr13 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 1
  %7 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr13, align 4
  %m_body0Wrap14 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 2
  %8 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body0Wrap14, align 4
  %call15 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %8)
  %call16 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call15)
  %m_body1Wrap17 = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 3
  %9 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %m_body1Wrap17, align 4
  %call18 = call %class.btCollisionObject* @_ZNK24btCollisionObjectWrapper18getCollisionObjectEv(%struct.btCollisionObjectWrapper* %9)
  %call19 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %call18)
  call void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold* %7, %class.btTransform* nonnull align 4 dereferenceable(64) %call16, %class.btTransform* nonnull align 4 dereferenceable(64) %call19)
  br label %if.end20

if.end20:                                         ; preds = %if.then, %if.else, %if.then6
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZN30btBox2dBox2dCollisionAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult(%class.btBox2dBox2dCollisionAlgorithm* %this, %class.btCollisionObject* %0, %class.btCollisionObject* %1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %2, %class.btManifoldResult* %3) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btBox2dBox2dCollisionAlgorithm*, align 4
  %.addr = alloca %class.btCollisionObject*, align 4
  %.addr1 = alloca %class.btCollisionObject*, align 4
  %.addr2 = alloca %struct.btDispatcherInfo*, align 4
  %.addr3 = alloca %class.btManifoldResult*, align 4
  store %class.btBox2dBox2dCollisionAlgorithm* %this, %class.btBox2dBox2dCollisionAlgorithm** %this.addr, align 4
  store %class.btCollisionObject* %0, %class.btCollisionObject** %.addr, align 4
  store %class.btCollisionObject* %1, %class.btCollisionObject** %.addr1, align 4
  store %struct.btDispatcherInfo* %2, %struct.btDispatcherInfo** %.addr2, align 4
  store %class.btManifoldResult* %3, %class.btManifoldResult** %.addr3, align 4
  %this4 = load %class.btBox2dBox2dCollisionAlgorithm*, %class.btBox2dBox2dCollisionAlgorithm** %this.addr, align 4
  ret float 1.000000e+00
}

; Function Attrs: noinline optnone
define internal float @_ZL17FindMaxSeparationPiPK12btBox2dShapeRK11btTransformS2_S5_(i32* %edgeIndex, %class.btBox2dShape* %poly1, %class.btTransform* nonnull align 4 dereferenceable(64) %xf1, %class.btBox2dShape* %poly2, %class.btTransform* nonnull align 4 dereferenceable(64) %xf2) #2 {
entry:
  %retval = alloca float, align 4
  %edgeIndex.addr = alloca i32*, align 4
  %poly1.addr = alloca %class.btBox2dShape*, align 4
  %xf1.addr = alloca %class.btTransform*, align 4
  %poly2.addr = alloca %class.btBox2dShape*, align 4
  %xf2.addr = alloca %class.btTransform*, align 4
  %count1 = alloca i32, align 4
  %normals1 = alloca %class.btVector3*, align 4
  %d = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %dLocal1 = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btMatrix3x3, align 4
  %edge = alloca i32, align 4
  %maxDot = alloca float, align 4
  %s = alloca float, align 4
  %prevEdge = alloca i32, align 4
  %sPrev = alloca float, align 4
  %nextEdge = alloca i32, align 4
  %sNext = alloca float, align 4
  %bestEdge = alloca i32, align 4
  %bestSeparation = alloca float, align 4
  %increment = alloca i32, align 4
  store i32* %edgeIndex, i32** %edgeIndex.addr, align 4
  store %class.btBox2dShape* %poly1, %class.btBox2dShape** %poly1.addr, align 4
  store %class.btTransform* %xf1, %class.btTransform** %xf1.addr, align 4
  store %class.btBox2dShape* %poly2, %class.btBox2dShape** %poly2.addr, align 4
  store %class.btTransform* %xf2, %class.btTransform** %xf2.addr, align 4
  %0 = load %class.btBox2dShape*, %class.btBox2dShape** %poly1.addr, align 4
  %call = call i32 @_ZNK12btBox2dShape14getVertexCountEv(%class.btBox2dShape* %0)
  store i32 %call, i32* %count1, align 4
  %1 = load %class.btBox2dShape*, %class.btBox2dShape** %poly1.addr, align 4
  %call1 = call %class.btVector3* @_ZNK12btBox2dShape10getNormalsEv(%class.btBox2dShape* %1)
  store %class.btVector3* %call1, %class.btVector3** %normals1, align 4
  %2 = load %class.btTransform*, %class.btTransform** %xf2.addr, align 4
  %3 = load %class.btBox2dShape*, %class.btBox2dShape** %poly2.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btBox2dShape11getCentroidEv(%class.btBox2dShape* %3)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp, %class.btTransform* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %call2)
  %4 = load %class.btTransform*, %class.btTransform** %xf1.addr, align 4
  %5 = load %class.btBox2dShape*, %class.btBox2dShape** %poly1.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btBox2dShape11getCentroidEv(%class.btBox2dShape* %5)
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %call4)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %d, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  %6 = load %class.btTransform*, %class.btTransform** %xf1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %6)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp5, %class.btMatrix3x3* %call6)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %dLocal1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %d)
  store i32 0, i32* %edge, align 4
  %7 = load i32, i32* %count1, align 4
  %cmp = icmp sgt i32 %7, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %8 = load %class.btVector3*, %class.btVector3** %normals1, align 4
  %9 = load i32, i32* %count1, align 4
  %call7 = call i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %dLocal1, %class.btVector3* %8, i32 %9, float* nonnull align 4 dereferenceable(4) %maxDot)
  store i32 %call7, i32* %edge, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %10 = load %class.btBox2dShape*, %class.btBox2dShape** %poly1.addr, align 4
  %11 = load %class.btTransform*, %class.btTransform** %xf1.addr, align 4
  %12 = load i32, i32* %edge, align 4
  %13 = load %class.btBox2dShape*, %class.btBox2dShape** %poly2.addr, align 4
  %14 = load %class.btTransform*, %class.btTransform** %xf2.addr, align 4
  %call8 = call float @_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_(%class.btBox2dShape* %10, %class.btTransform* nonnull align 4 dereferenceable(64) %11, i32 %12, %class.btBox2dShape* %13, %class.btTransform* nonnull align 4 dereferenceable(64) %14)
  store float %call8, float* %s, align 4
  %15 = load float, float* %s, align 4
  %cmp9 = fcmp ogt float %15, 0.000000e+00
  br i1 %cmp9, label %if.then10, label %if.end11

if.then10:                                        ; preds = %if.end
  %16 = load float, float* %s, align 4
  store float %16, float* %retval, align 4
  br label %return

if.end11:                                         ; preds = %if.end
  %17 = load i32, i32* %edge, align 4
  %sub = sub nsw i32 %17, 1
  %cmp12 = icmp sge i32 %sub, 0
  br i1 %cmp12, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.end11
  %18 = load i32, i32* %edge, align 4
  %sub13 = sub nsw i32 %18, 1
  br label %cond.end

cond.false:                                       ; preds = %if.end11
  %19 = load i32, i32* %count1, align 4
  %sub14 = sub nsw i32 %19, 1
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %sub13, %cond.true ], [ %sub14, %cond.false ]
  store i32 %cond, i32* %prevEdge, align 4
  %20 = load %class.btBox2dShape*, %class.btBox2dShape** %poly1.addr, align 4
  %21 = load %class.btTransform*, %class.btTransform** %xf1.addr, align 4
  %22 = load i32, i32* %prevEdge, align 4
  %23 = load %class.btBox2dShape*, %class.btBox2dShape** %poly2.addr, align 4
  %24 = load %class.btTransform*, %class.btTransform** %xf2.addr, align 4
  %call15 = call float @_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_(%class.btBox2dShape* %20, %class.btTransform* nonnull align 4 dereferenceable(64) %21, i32 %22, %class.btBox2dShape* %23, %class.btTransform* nonnull align 4 dereferenceable(64) %24)
  store float %call15, float* %sPrev, align 4
  %25 = load float, float* %sPrev, align 4
  %cmp16 = fcmp ogt float %25, 0.000000e+00
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %cond.end
  %26 = load float, float* %sPrev, align 4
  store float %26, float* %retval, align 4
  br label %return

if.end18:                                         ; preds = %cond.end
  %27 = load i32, i32* %edge, align 4
  %add = add nsw i32 %27, 1
  %28 = load i32, i32* %count1, align 4
  %cmp19 = icmp slt i32 %add, %28
  br i1 %cmp19, label %cond.true20, label %cond.false22

cond.true20:                                      ; preds = %if.end18
  %29 = load i32, i32* %edge, align 4
  %add21 = add nsw i32 %29, 1
  br label %cond.end23

cond.false22:                                     ; preds = %if.end18
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false22, %cond.true20
  %cond24 = phi i32 [ %add21, %cond.true20 ], [ 0, %cond.false22 ]
  store i32 %cond24, i32* %nextEdge, align 4
  %30 = load %class.btBox2dShape*, %class.btBox2dShape** %poly1.addr, align 4
  %31 = load %class.btTransform*, %class.btTransform** %xf1.addr, align 4
  %32 = load i32, i32* %nextEdge, align 4
  %33 = load %class.btBox2dShape*, %class.btBox2dShape** %poly2.addr, align 4
  %34 = load %class.btTransform*, %class.btTransform** %xf2.addr, align 4
  %call25 = call float @_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_(%class.btBox2dShape* %30, %class.btTransform* nonnull align 4 dereferenceable(64) %31, i32 %32, %class.btBox2dShape* %33, %class.btTransform* nonnull align 4 dereferenceable(64) %34)
  store float %call25, float* %sNext, align 4
  %35 = load float, float* %sNext, align 4
  %cmp26 = fcmp ogt float %35, 0.000000e+00
  br i1 %cmp26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %cond.end23
  %36 = load float, float* %sNext, align 4
  store float %36, float* %retval, align 4
  br label %return

if.end28:                                         ; preds = %cond.end23
  %37 = load float, float* %sPrev, align 4
  %38 = load float, float* %s, align 4
  %cmp29 = fcmp ogt float %37, %38
  br i1 %cmp29, label %land.lhs.true, label %if.else

land.lhs.true:                                    ; preds = %if.end28
  %39 = load float, float* %sPrev, align 4
  %40 = load float, float* %sNext, align 4
  %cmp30 = fcmp ogt float %39, %40
  br i1 %cmp30, label %if.then31, label %if.else

if.then31:                                        ; preds = %land.lhs.true
  store i32 -1, i32* %increment, align 4
  %41 = load i32, i32* %prevEdge, align 4
  store i32 %41, i32* %bestEdge, align 4
  %42 = load float, float* %sPrev, align 4
  store float %42, float* %bestSeparation, align 4
  br label %if.end36

if.else:                                          ; preds = %land.lhs.true, %if.end28
  %43 = load float, float* %sNext, align 4
  %44 = load float, float* %s, align 4
  %cmp32 = fcmp ogt float %43, %44
  br i1 %cmp32, label %if.then33, label %if.else34

if.then33:                                        ; preds = %if.else
  store i32 1, i32* %increment, align 4
  %45 = load i32, i32* %nextEdge, align 4
  store i32 %45, i32* %bestEdge, align 4
  %46 = load float, float* %sNext, align 4
  store float %46, float* %bestSeparation, align 4
  br label %if.end35

if.else34:                                        ; preds = %if.else
  %47 = load i32, i32* %edge, align 4
  %48 = load i32*, i32** %edgeIndex.addr, align 4
  store i32 %47, i32* %48, align 4
  %49 = load float, float* %s, align 4
  store float %49, float* %retval, align 4
  br label %return

if.end35:                                         ; preds = %if.then33
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %if.then31
  br label %for.cond

for.cond:                                         ; preds = %if.end63, %if.end36
  %50 = load i32, i32* %increment, align 4
  %cmp37 = icmp eq i32 %50, -1
  br i1 %cmp37, label %if.then38, label %if.else47

if.then38:                                        ; preds = %for.cond
  %51 = load i32, i32* %bestEdge, align 4
  %sub39 = sub nsw i32 %51, 1
  %cmp40 = icmp sge i32 %sub39, 0
  br i1 %cmp40, label %cond.true41, label %cond.false43

cond.true41:                                      ; preds = %if.then38
  %52 = load i32, i32* %bestEdge, align 4
  %sub42 = sub nsw i32 %52, 1
  br label %cond.end45

cond.false43:                                     ; preds = %if.then38
  %53 = load i32, i32* %count1, align 4
  %sub44 = sub nsw i32 %53, 1
  br label %cond.end45

cond.end45:                                       ; preds = %cond.false43, %cond.true41
  %cond46 = phi i32 [ %sub42, %cond.true41 ], [ %sub44, %cond.false43 ]
  store i32 %cond46, i32* %edge, align 4
  br label %if.end55

if.else47:                                        ; preds = %for.cond
  %54 = load i32, i32* %bestEdge, align 4
  %add48 = add nsw i32 %54, 1
  %55 = load i32, i32* %count1, align 4
  %cmp49 = icmp slt i32 %add48, %55
  br i1 %cmp49, label %cond.true50, label %cond.false52

cond.true50:                                      ; preds = %if.else47
  %56 = load i32, i32* %bestEdge, align 4
  %add51 = add nsw i32 %56, 1
  br label %cond.end53

cond.false52:                                     ; preds = %if.else47
  br label %cond.end53

cond.end53:                                       ; preds = %cond.false52, %cond.true50
  %cond54 = phi i32 [ %add51, %cond.true50 ], [ 0, %cond.false52 ]
  store i32 %cond54, i32* %edge, align 4
  br label %if.end55

if.end55:                                         ; preds = %cond.end53, %cond.end45
  %57 = load %class.btBox2dShape*, %class.btBox2dShape** %poly1.addr, align 4
  %58 = load %class.btTransform*, %class.btTransform** %xf1.addr, align 4
  %59 = load i32, i32* %edge, align 4
  %60 = load %class.btBox2dShape*, %class.btBox2dShape** %poly2.addr, align 4
  %61 = load %class.btTransform*, %class.btTransform** %xf2.addr, align 4
  %call56 = call float @_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_(%class.btBox2dShape* %57, %class.btTransform* nonnull align 4 dereferenceable(64) %58, i32 %59, %class.btBox2dShape* %60, %class.btTransform* nonnull align 4 dereferenceable(64) %61)
  store float %call56, float* %s, align 4
  %62 = load float, float* %s, align 4
  %cmp57 = fcmp ogt float %62, 0.000000e+00
  br i1 %cmp57, label %if.then58, label %if.end59

if.then58:                                        ; preds = %if.end55
  %63 = load float, float* %s, align 4
  store float %63, float* %retval, align 4
  br label %return

if.end59:                                         ; preds = %if.end55
  %64 = load float, float* %s, align 4
  %65 = load float, float* %bestSeparation, align 4
  %cmp60 = fcmp ogt float %64, %65
  br i1 %cmp60, label %if.then61, label %if.else62

if.then61:                                        ; preds = %if.end59
  %66 = load i32, i32* %edge, align 4
  store i32 %66, i32* %bestEdge, align 4
  %67 = load float, float* %s, align 4
  store float %67, float* %bestSeparation, align 4
  br label %if.end63

if.else62:                                        ; preds = %if.end59
  br label %for.end

if.end63:                                         ; preds = %if.then61
  br label %for.cond

for.end:                                          ; preds = %if.else62
  %68 = load i32, i32* %bestEdge, align 4
  %69 = load i32*, i32** %edgeIndex.addr, align 4
  store i32 %68, i32* %69, align 4
  %70 = load float, float* %bestSeparation, align 4
  store float %70, float* %retval, align 4
  br label %return

return:                                           ; preds = %for.end, %if.then58, %if.else34, %if.then27, %if.then17, %if.then10
  %71 = load float, float* %retval, align 4
  ret float %71
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %struct.ClipVertex* @_ZN10ClipVertexC2Ev(%struct.ClipVertex* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %struct.ClipVertex*, align 4
  store %struct.ClipVertex* %this, %struct.ClipVertex** %this.addr, align 4
  %this1 = load %struct.ClipVertex*, %struct.ClipVertex** %this.addr, align 4
  %v = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %v)
  ret %struct.ClipVertex* %this1
}

; Function Attrs: noinline optnone
define internal void @_ZL16FindIncidentEdgeP10ClipVertexPK12btBox2dShapeRK11btTransformiS3_S6_(%struct.ClipVertex* %c, %class.btBox2dShape* %poly1, %class.btTransform* nonnull align 4 dereferenceable(64) %xf1, i32 %edge1, %class.btBox2dShape* %poly2, %class.btTransform* nonnull align 4 dereferenceable(64) %xf2) #2 {
entry:
  %c.addr = alloca %struct.ClipVertex*, align 4
  %poly1.addr = alloca %class.btBox2dShape*, align 4
  %xf1.addr = alloca %class.btTransform*, align 4
  %edge1.addr = alloca i32, align 4
  %poly2.addr = alloca %class.btBox2dShape*, align 4
  %xf2.addr = alloca %class.btTransform*, align 4
  %normals1 = alloca %class.btVector3*, align 4
  %count2 = alloca i32, align 4
  %vertices2 = alloca %class.btVector3*, align 4
  %normals2 = alloca %class.btVector3*, align 4
  %normal1 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %index = alloca i32, align 4
  %minDot = alloca float, align 4
  %i = alloca i32, align 4
  %dot = alloca float, align 4
  %i1 = alloca i32, align 4
  %i2 = alloca i32, align 4
  %ref.tmp12 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  store %struct.ClipVertex* %c, %struct.ClipVertex** %c.addr, align 4
  store %class.btBox2dShape* %poly1, %class.btBox2dShape** %poly1.addr, align 4
  store %class.btTransform* %xf1, %class.btTransform** %xf1.addr, align 4
  store i32 %edge1, i32* %edge1.addr, align 4
  store %class.btBox2dShape* %poly2, %class.btBox2dShape** %poly2.addr, align 4
  store %class.btTransform* %xf2, %class.btTransform** %xf2.addr, align 4
  %0 = load %class.btBox2dShape*, %class.btBox2dShape** %poly1.addr, align 4
  %call = call %class.btVector3* @_ZNK12btBox2dShape10getNormalsEv(%class.btBox2dShape* %0)
  store %class.btVector3* %call, %class.btVector3** %normals1, align 4
  %1 = load %class.btBox2dShape*, %class.btBox2dShape** %poly2.addr, align 4
  %call1 = call i32 @_ZNK12btBox2dShape14getVertexCountEv(%class.btBox2dShape* %1)
  store i32 %call1, i32* %count2, align 4
  %2 = load %class.btBox2dShape*, %class.btBox2dShape** %poly2.addr, align 4
  %call2 = call %class.btVector3* @_ZNK12btBox2dShape11getVerticesEv(%class.btBox2dShape* %2)
  store %class.btVector3* %call2, %class.btVector3** %vertices2, align 4
  %3 = load %class.btBox2dShape*, %class.btBox2dShape** %poly2.addr, align 4
  %call3 = call %class.btVector3* @_ZNK12btBox2dShape10getNormalsEv(%class.btBox2dShape* %3)
  store %class.btVector3* %call3, %class.btVector3** %normals2, align 4
  %4 = load %class.btTransform*, %class.btTransform** %xf2.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %4)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call4)
  %5 = load %class.btTransform*, %class.btTransform** %xf1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %5)
  %6 = load %class.btVector3*, %class.btVector3** %normals1, align 4
  %7 = load i32, i32* %edge1.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 %7
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %ref.tmp5, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %normal1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  store i32 0, i32* %index, align 4
  store float 0x43ABC16D60000000, float* %minDot, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %8 = load i32, i32* %i, align 4
  %9 = load i32, i32* %count2, align 4
  %cmp = icmp slt i32 %8, %9
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %10 = load %class.btVector3*, %class.btVector3** %normals2, align 4
  %11 = load i32, i32* %i, align 4
  %arrayidx7 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 %11
  %call8 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %normal1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7)
  store float %call8, float* %dot, align 4
  %12 = load float, float* %dot, align 4
  %13 = load float, float* %minDot, align 4
  %cmp9 = fcmp olt float %12, %13
  br i1 %cmp9, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %14 = load float, float* %dot, align 4
  store float %14, float* %minDot, align 4
  %15 = load i32, i32* %i, align 4
  store i32 %15, i32* %index, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %16 = load i32, i32* %i, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %17 = load i32, i32* %index, align 4
  store i32 %17, i32* %i1, align 4
  %18 = load i32, i32* %i1, align 4
  %add = add nsw i32 %18, 1
  %19 = load i32, i32* %count2, align 4
  %cmp10 = icmp slt i32 %add, %19
  br i1 %cmp10, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end
  %20 = load i32, i32* %i1, align 4
  %add11 = add nsw i32 %20, 1
  br label %cond.end

cond.false:                                       ; preds = %for.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %add11, %cond.true ], [ 0, %cond.false ]
  store i32 %cond, i32* %i2, align 4
  %21 = load %class.btTransform*, %class.btTransform** %xf2.addr, align 4
  %22 = load %class.btVector3*, %class.btVector3** %vertices2, align 4
  %23 = load i32, i32* %i1, align 4
  %arrayidx13 = getelementptr inbounds %class.btVector3, %class.btVector3* %22, i32 %23
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp12, %class.btTransform* %21, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx13)
  %24 = load %struct.ClipVertex*, %struct.ClipVertex** %c.addr, align 4
  %arrayidx14 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %24, i32 0
  %v = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx14, i32 0, i32 0
  %25 = bitcast %class.btVector3* %v to i8*
  %26 = bitcast %class.btVector3* %ref.tmp12 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %25, i8* align 4 %26, i32 16, i1 false)
  %27 = load %class.btTransform*, %class.btTransform** %xf2.addr, align 4
  %28 = load %class.btVector3*, %class.btVector3** %vertices2, align 4
  %29 = load i32, i32* %i2, align 4
  %arrayidx16 = getelementptr inbounds %class.btVector3, %class.btVector3* %28, i32 %29
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %ref.tmp15, %class.btTransform* %27, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx16)
  %30 = load %struct.ClipVertex*, %struct.ClipVertex** %c.addr, align 4
  %arrayidx17 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %30, i32 1
  %v18 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx17, i32 0, i32 0
  %31 = bitcast %class.btVector3* %v18 to i8*
  %32 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %31, i8* align 4 %32, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK12btBox2dShape14getVertexCountEv(%class.btBox2dShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btBox2dShape*, align 4
  store %class.btBox2dShape* %this, %class.btBox2dShape** %this.addr, align 4
  %this1 = load %class.btBox2dShape*, %class.btBox2dShape** %this.addr, align 4
  ret i32 4
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZNK12btBox2dShape11getVerticesEv(%class.btBox2dShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btBox2dShape*, align 4
  store %class.btBox2dShape* %this, %class.btBox2dShape** %this.addr, align 4
  %this1 = load %class.btBox2dShape*, %class.btBox2dShape** %this.addr, align 4
  %m_vertices = getelementptr inbounds %class.btBox2dShape, %class.btBox2dShape* %this1, i32 0, i32 2
  %arrayidx = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_vertices, i32 0, i32 0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %0, i32 0)
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call1 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call1, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 1)
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call4, float* %ref.tmp2, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 2)
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %call6, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call7, float* %ref.tmp5, align 4
  %call8 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %agg.result, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline optnone
define internal i32 @_ZL17ClipSegmentToLineP10ClipVertexS0_RK9btVector3f(%struct.ClipVertex* %vOut, %struct.ClipVertex* %vIn, %class.btVector3* nonnull align 4 dereferenceable(16) %normal, float %offset) #2 {
entry:
  %vOut.addr = alloca %struct.ClipVertex*, align 4
  %vIn.addr = alloca %struct.ClipVertex*, align 4
  %normal.addr = alloca %class.btVector3*, align 4
  %offset.addr = alloca float, align 4
  %numOut = alloca i32, align 4
  %distance0 = alloca float, align 4
  %distance1 = alloca float, align 4
  %interp = alloca float, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp18 = alloca %class.btVector3, align 4
  %ref.tmp19 = alloca %class.btVector3, align 4
  store %struct.ClipVertex* %vOut, %struct.ClipVertex** %vOut.addr, align 4
  store %struct.ClipVertex* %vIn, %struct.ClipVertex** %vIn.addr, align 4
  store %class.btVector3* %normal, %class.btVector3** %normal.addr, align 4
  store float %offset, float* %offset.addr, align 4
  store i32 0, i32* %numOut, align 4
  %0 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %1 = load %struct.ClipVertex*, %struct.ClipVertex** %vIn.addr, align 4
  %arrayidx = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %1, i32 0
  %v = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx, i32 0, i32 0
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %v)
  %2 = load float, float* %offset.addr, align 4
  %sub = fsub float %call, %2
  store float %sub, float* %distance0, align 4
  %3 = load %class.btVector3*, %class.btVector3** %normal.addr, align 4
  %4 = load %struct.ClipVertex*, %struct.ClipVertex** %vIn.addr, align 4
  %arrayidx1 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %4, i32 1
  %v2 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx1, i32 0, i32 0
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %v2)
  %5 = load float, float* %offset.addr, align 4
  %sub4 = fsub float %call3, %5
  store float %sub4, float* %distance1, align 4
  %6 = load float, float* %distance0, align 4
  %cmp = fcmp ole float %6, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %7 = load %struct.ClipVertex*, %struct.ClipVertex** %vIn.addr, align 4
  %arrayidx5 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %7, i32 0
  %8 = load %struct.ClipVertex*, %struct.ClipVertex** %vOut.addr, align 4
  %9 = load i32, i32* %numOut, align 4
  %inc = add nsw i32 %9, 1
  store i32 %inc, i32* %numOut, align 4
  %arrayidx6 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %8, i32 %9
  %10 = bitcast %struct.ClipVertex* %arrayidx6 to i8*
  %11 = bitcast %struct.ClipVertex* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 20, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %12 = load float, float* %distance1, align 4
  %cmp7 = fcmp ole float %12, 0.000000e+00
  br i1 %cmp7, label %if.then8, label %if.end12

if.then8:                                         ; preds = %if.end
  %13 = load %struct.ClipVertex*, %struct.ClipVertex** %vIn.addr, align 4
  %arrayidx9 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %13, i32 1
  %14 = load %struct.ClipVertex*, %struct.ClipVertex** %vOut.addr, align 4
  %15 = load i32, i32* %numOut, align 4
  %inc10 = add nsw i32 %15, 1
  store i32 %inc10, i32* %numOut, align 4
  %arrayidx11 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %14, i32 %15
  %16 = bitcast %struct.ClipVertex* %arrayidx11 to i8*
  %17 = bitcast %struct.ClipVertex* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 20, i1 false)
  br label %if.end12

if.end12:                                         ; preds = %if.then8, %if.end
  %18 = load float, float* %distance0, align 4
  %19 = load float, float* %distance1, align 4
  %mul = fmul float %18, %19
  %cmp13 = fcmp olt float %mul, 0.000000e+00
  br i1 %cmp13, label %if.then14, label %if.end37

if.then14:                                        ; preds = %if.end12
  %20 = load float, float* %distance0, align 4
  %21 = load float, float* %distance0, align 4
  %22 = load float, float* %distance1, align 4
  %sub15 = fsub float %21, %22
  %div = fdiv float %20, %sub15
  store float %div, float* %interp, align 4
  %23 = load %struct.ClipVertex*, %struct.ClipVertex** %vIn.addr, align 4
  %arrayidx16 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %23, i32 0
  %v17 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx16, i32 0, i32 0
  %24 = load %struct.ClipVertex*, %struct.ClipVertex** %vIn.addr, align 4
  %arrayidx20 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %24, i32 1
  %v21 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx20, i32 0, i32 0
  %25 = load %struct.ClipVertex*, %struct.ClipVertex** %vIn.addr, align 4
  %arrayidx22 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %25, i32 0
  %v23 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx22, i32 0, i32 0
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp19, %class.btVector3* nonnull align 4 dereferenceable(16) %v21, %class.btVector3* nonnull align 4 dereferenceable(16) %v23)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp18, float* nonnull align 4 dereferenceable(4) %interp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp19)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %v17, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp18)
  %26 = load %struct.ClipVertex*, %struct.ClipVertex** %vOut.addr, align 4
  %27 = load i32, i32* %numOut, align 4
  %arrayidx24 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %26, i32 %27
  %v25 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx24, i32 0, i32 0
  %28 = bitcast %class.btVector3* %v25 to i8*
  %29 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %28, i8* align 4 %29, i32 16, i1 false)
  %30 = load float, float* %distance0, align 4
  %cmp26 = fcmp ogt float %30, 0.000000e+00
  br i1 %cmp26, label %if.then27, label %if.else

if.then27:                                        ; preds = %if.then14
  %31 = load %struct.ClipVertex*, %struct.ClipVertex** %vIn.addr, align 4
  %arrayidx28 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %31, i32 0
  %id = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx28, i32 0, i32 1
  %32 = load i32, i32* %id, align 4
  %33 = load %struct.ClipVertex*, %struct.ClipVertex** %vOut.addr, align 4
  %34 = load i32, i32* %numOut, align 4
  %arrayidx29 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %33, i32 %34
  %id30 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx29, i32 0, i32 1
  store i32 %32, i32* %id30, align 4
  br label %if.end35

if.else:                                          ; preds = %if.then14
  %35 = load %struct.ClipVertex*, %struct.ClipVertex** %vIn.addr, align 4
  %arrayidx31 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %35, i32 1
  %id32 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx31, i32 0, i32 1
  %36 = load i32, i32* %id32, align 4
  %37 = load %struct.ClipVertex*, %struct.ClipVertex** %vOut.addr, align 4
  %38 = load i32, i32* %numOut, align 4
  %arrayidx33 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %37, i32 %38
  %id34 = getelementptr inbounds %struct.ClipVertex, %struct.ClipVertex* %arrayidx33, i32 0, i32 1
  store i32 %36, i32* %id34, align 4
  br label %if.end35

if.end35:                                         ; preds = %if.else, %if.then27
  %39 = load i32, i32* %numOut, align 4
  %inc36 = add nsw i32 %39, 1
  store i32 %inc36, i32* %numOut, align 4
  br label %if.end37

if.end37:                                         ; preds = %if.end35, %if.end12
  %40 = load i32, i32* %numOut, align 4
  ret i32 %40
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN30btBox2dBox2dCollisionAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE(%class.btBox2dBox2dCollisionAlgorithm* %this, %class.btAlignedObjectArray.2* nonnull align 4 dereferenceable(17) %manifoldArray) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btBox2dBox2dCollisionAlgorithm*, align 4
  %manifoldArray.addr = alloca %class.btAlignedObjectArray.2*, align 4
  store %class.btBox2dBox2dCollisionAlgorithm* %this, %class.btBox2dBox2dCollisionAlgorithm** %this.addr, align 4
  store %class.btAlignedObjectArray.2* %manifoldArray, %class.btAlignedObjectArray.2** %manifoldArray.addr, align 4
  %this1 = load %class.btBox2dBox2dCollisionAlgorithm*, %class.btBox2dBox2dCollisionAlgorithm** %this.addr, align 4
  %m_manifoldPtr = getelementptr inbounds %class.btBox2dBox2dCollisionAlgorithm, %class.btBox2dBox2dCollisionAlgorithm* %this1, i32 0, i32 2
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %m_manifoldPtr, align 4
  %tobool = icmp ne %class.btPersistentManifold* %0, null
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %entry
  %m_ownManifold = getelementptr inbounds %class.btBox2dBox2dCollisionAlgorithm, %class.btBox2dBox2dCollisionAlgorithm* %this1, i32 0, i32 1
  %1 = load i8, i8* %m_ownManifold, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  %2 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %manifoldArray.addr, align 4
  %m_manifoldPtr3 = getelementptr inbounds %class.btBox2dBox2dCollisionAlgorithm, %class.btBox2dBox2dCollisionAlgorithm* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.2* %2, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %m_manifoldPtr3)
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btPersistentManifold14getNumContactsEv(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %0 = load i32, i32* %m_cachedPoints, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4
  ret %class.btCollisionObject* %0
}

declare void @_ZN20btPersistentManifold20refreshContactPointsERK11btTransformS2_(%class.btPersistentManifold*, %class.btTransform* nonnull align 4 dereferenceable(64), %class.btTransform* nonnull align 4 dereferenceable(64)) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZNK12btBox2dShape10getNormalsEv(%class.btBox2dShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btBox2dShape*, align 4
  store %class.btBox2dShape* %this, %class.btBox2dShape** %this.addr, align 4
  %this1 = load %class.btBox2dShape*, %class.btBox2dShape** %this.addr, align 4
  %m_normals = getelementptr inbounds %class.btBox2dShape, %class.btBox2dShape* %this1, i32 0, i32 3
  %arrayidx = getelementptr inbounds [4 x %class.btVector3], [4 x %class.btVector3]* %m_normals, i32 0, i32 0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK12btBox2dShape11getCentroidEv(%class.btBox2dShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btBox2dShape*, align 4
  store %class.btBox2dShape* %this, %class.btBox2dShape** %this.addr, align 4
  %this1 = load %class.btBox2dShape*, %class.btBox2dShape** %this.addr, align 4
  %m_centroid = getelementptr inbounds %class.btBox2dShape, %class.btBox2dShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_centroid
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %this, %class.btVector3* %array, i32 %array_count, float* nonnull align 4 dereferenceable(4) %dotOut) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %array.addr = alloca %class.btVector3*, align 4
  %array_count.addr = alloca i32, align 4
  %dotOut.addr = alloca float*, align 4
  %maxDot1 = alloca float, align 4
  %i = alloca i32, align 4
  %ptIndex = alloca i32, align 4
  %dot = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %array, %class.btVector3** %array.addr, align 4
  store i32 %array_count, i32* %array_count.addr, align 4
  store float* %dotOut, float** %dotOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store float 0xC7EFFFFFE0000000, float* %maxDot1, align 4
  store i32 0, i32* %i, align 4
  store i32 -1, i32* %ptIndex, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %array_count.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btVector3*, %class.btVector3** %array.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 %3
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  store float %call, float* %dot, align 4
  %4 = load float, float* %dot, align 4
  %5 = load float, float* %maxDot1, align 4
  %cmp2 = fcmp ogt float %4, %5
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load float, float* %dot, align 4
  store float %6, float* %maxDot1, align 4
  %7 = load i32, i32* %i, align 4
  store i32 %7, i32* %ptIndex, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load float, float* %maxDot1, align 4
  %10 = load float*, float** %dotOut.addr, align 4
  store float %9, float* %10, align 4
  %11 = load i32, i32* %ptIndex, align 4
  ret i32 %11
}

; Function Attrs: noinline optnone
define internal float @_ZL14EdgeSeparationPK12btBox2dShapeRK11btTransformiS1_S4_(%class.btBox2dShape* %poly1, %class.btTransform* nonnull align 4 dereferenceable(64) %xf1, i32 %edge1, %class.btBox2dShape* %poly2, %class.btTransform* nonnull align 4 dereferenceable(64) %xf2) #2 {
entry:
  %poly1.addr = alloca %class.btBox2dShape*, align 4
  %xf1.addr = alloca %class.btTransform*, align 4
  %edge1.addr = alloca i32, align 4
  %poly2.addr = alloca %class.btBox2dShape*, align 4
  %xf2.addr = alloca %class.btTransform*, align 4
  %vertices1 = alloca %class.btVector3*, align 4
  %normals1 = alloca %class.btVector3*, align 4
  %count2 = alloca i32, align 4
  %vertices2 = alloca %class.btVector3*, align 4
  %normal1World = alloca %class.btVector3, align 4
  %normal1 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %index = alloca i32, align 4
  %minDot = alloca float, align 4
  %v1 = alloca %class.btVector3, align 4
  %v2 = alloca %class.btVector3, align 4
  %separation = alloca float, align 4
  %ref.tmp9 = alloca %class.btVector3, align 4
  store %class.btBox2dShape* %poly1, %class.btBox2dShape** %poly1.addr, align 4
  store %class.btTransform* %xf1, %class.btTransform** %xf1.addr, align 4
  store i32 %edge1, i32* %edge1.addr, align 4
  store %class.btBox2dShape* %poly2, %class.btBox2dShape** %poly2.addr, align 4
  store %class.btTransform* %xf2, %class.btTransform** %xf2.addr, align 4
  %0 = load %class.btBox2dShape*, %class.btBox2dShape** %poly1.addr, align 4
  %call = call %class.btVector3* @_ZNK12btBox2dShape11getVerticesEv(%class.btBox2dShape* %0)
  store %class.btVector3* %call, %class.btVector3** %vertices1, align 4
  %1 = load %class.btBox2dShape*, %class.btBox2dShape** %poly1.addr, align 4
  %call1 = call %class.btVector3* @_ZNK12btBox2dShape10getNormalsEv(%class.btBox2dShape* %1)
  store %class.btVector3* %call1, %class.btVector3** %normals1, align 4
  %2 = load %class.btBox2dShape*, %class.btBox2dShape** %poly2.addr, align 4
  %call2 = call i32 @_ZNK12btBox2dShape14getVertexCountEv(%class.btBox2dShape* %2)
  store i32 %call2, i32* %count2, align 4
  %3 = load %class.btBox2dShape*, %class.btBox2dShape** %poly2.addr, align 4
  %call3 = call %class.btVector3* @_ZNK12btBox2dShape11getVerticesEv(%class.btBox2dShape* %3)
  store %class.btVector3* %call3, %class.btVector3** %vertices2, align 4
  %4 = load %class.btTransform*, %class.btTransform** %xf1.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %4)
  %5 = load %class.btVector3*, %class.btVector3** %normals1, align 4
  %6 = load i32, i32* %edge1.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %5, i32 %6
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %normal1World, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call4, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx)
  %7 = load %class.btTransform*, %class.btTransform** %xf2.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %7)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call5)
  call void @_ZmlRK11btMatrix3x3RK9btVector3(%class.btVector3* sret align 4 %normal1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %normal1World)
  store i32 0, i32* %index, align 4
  store float 0x43ABC16D60000000, float* %minDot, align 4
  %8 = load i32, i32* %count2, align 4
  %cmp = icmp sgt i32 %8, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %9 = load %class.btVector3*, %class.btVector3** %vertices2, align 4
  %10 = load i32, i32* %count2, align 4
  %call6 = call i32 @_ZNK9btVector36minDotEPKS_lRf(%class.btVector3* %normal1, %class.btVector3* %9, i32 %10, float* nonnull align 4 dereferenceable(4) %minDot)
  store i32 %call6, i32* %index, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %11 = load %class.btTransform*, %class.btTransform** %xf1.addr, align 4
  %12 = load %class.btVector3*, %class.btVector3** %vertices1, align 4
  %13 = load i32, i32* %edge1.addr, align 4
  %arrayidx7 = getelementptr inbounds %class.btVector3, %class.btVector3* %12, i32 %13
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %v1, %class.btTransform* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx7)
  %14 = load %class.btTransform*, %class.btTransform** %xf2.addr, align 4
  %15 = load %class.btVector3*, %class.btVector3** %vertices2, align 4
  %16 = load i32, i32* %index, align 4
  %arrayidx8 = getelementptr inbounds %class.btVector3, %class.btVector3* %15, i32 %16
  call void @_ZNK11btTransformmlERK9btVector3(%class.btVector3* sret align 4 %v2, %class.btTransform* %14, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %v2, %class.btVector3* nonnull align 4 dereferenceable(16) %v1)
  %call10 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %ref.tmp9, %class.btVector3* nonnull align 4 dereferenceable(16) %normal1World)
  store float %call10, float* %separation, align 4
  %17 = load float, float* %separation, align 4
  ret float %17
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK9btVector36minDotEPKS_lRf(%class.btVector3* %this, %class.btVector3* %array, i32 %array_count, float* nonnull align 4 dereferenceable(4) %dotOut) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %array.addr = alloca %class.btVector3*, align 4
  %array_count.addr = alloca i32, align 4
  %dotOut.addr = alloca float*, align 4
  %minDot = alloca float, align 4
  %i = alloca i32, align 4
  %ptIndex = alloca i32, align 4
  %dot = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %array, %class.btVector3** %array.addr, align 4
  store i32 %array_count, i32* %array_count.addr, align 4
  store float* %dotOut, float** %dotOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store float 0x47EFFFFFE0000000, float* %minDot, align 4
  store i32 0, i32* %i, align 4
  store i32 -1, i32* %ptIndex, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %array_count.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btVector3*, %class.btVector3** %array.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 %3
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  store float %call, float* %dot, align 4
  %4 = load float, float* %dot, align 4
  %5 = load float, float* %minDot, align 4
  %cmp2 = fcmp olt float %4, %5
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load float, float* %dot, align 4
  store float %6, float* %minDot, align 4
  %7 = load i32, i32* %i, align 4
  store i32 %7, i32* %ptIndex, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load float, float* %minDot, align 4
  %10 = load float*, float** %dotOut.addr, align 4
  store float %9, float* %10, align 4
  %11 = load i32, i32* %ptIndex, align 4
  ret i32 %11
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.2* %this, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  %_Val.addr = alloca %class.btPersistentManifold**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  store %class.btPersistentManifold** %_Val, %class.btPersistentManifold*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.2* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.2* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.2* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.2* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.2* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 4
  %1 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %1, i32 %2
  %3 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btPersistentManifold**
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %_Val.addr, align 4
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %5, align 4
  store %class.btPersistentManifold* %6, %class.btPersistentManifold** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.2* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.2* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.2* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.2* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.2* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %2, %class.btPersistentManifold*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.2* %this1)
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.2* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.2* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.2* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.2* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %4, %class.btPersistentManifold*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.2* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.2* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.3* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.2* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  %5 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 4
  %7 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %7, i32 %8
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4
  store %class.btPersistentManifold* %9, %class.btPersistentManifold** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.2* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.2* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.2*, align 4
  store %class.btAlignedObjectArray.2* %this, %class.btAlignedObjectArray.2** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.2*, %class.btAlignedObjectArray.2** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.3* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.2, %class.btAlignedObjectArray.2* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.3* %this, i32 %n, %class.btPersistentManifold*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.3*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.3* %this, %class.btAlignedAllocator.3** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.3*, %class.btAlignedAllocator.3** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.3* %this, %class.btPersistentManifold** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.3*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.3* %this, %class.btAlignedAllocator.3** %this.addr, align 4
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.3*, %class.btAlignedAllocator.3** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btBox2dBox2dCollisionAlgorithm.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { argmemonly nounwind willreturn }
attributes #7 = { nounwind readnone speculatable willreturn }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
