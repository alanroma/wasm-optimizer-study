; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/Gimpact/btGImpactShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/Gimpact/btGImpactShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%class.btGImpactMeshShapePart = type { %class.btGImpactShapeInterface, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager" }
%class.btGImpactShapeInterface = type { %class.btConcaveShape, %class.btAABB, i8, %class.btVector3, %class.btGImpactQuantizedBvh }
%class.btConcaveShape = type { %class.btCollisionShape, float }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btAABB = type { %class.btVector3, %class.btVector3 }
%class.btGImpactQuantizedBvh = type { %class.btQuantizedBvhTree, %class.btPrimitiveManagerBase* }
%class.btQuantizedBvhTree = type { i32, %class.GIM_QUANTIZED_BVH_NODE_ARRAY, %class.btAABB, %class.btVector3 }
%class.GIM_QUANTIZED_BVH_NODE_ARRAY = type { %class.btAlignedObjectArray.base, [3 x i8] }
%class.btAlignedObjectArray.base = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.BT_QUANTIZED_BVH_NODE*, i8 }>
%class.btAlignedAllocator = type { i8 }
%struct.BT_QUANTIZED_BVH_NODE = type { [3 x i16], [3 x i16], i32 }
%class.btPrimitiveManagerBase = type { i32 (...)** }
%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager" = type { %class.btPrimitiveManagerBase, float, %class.btStridingMeshInterface*, %class.btVector3, i32, i32, i8*, i32, i32, i32, i8*, i32, i32, i32 }
%class.btStridingMeshInterface = type { i32 (...)**, %class.btVector3 }
%class.btGImpactCompoundShape = type { %class.btGImpactShapeInterface, %"class.btGImpactCompoundShape::CompoundPrimitiveManager", %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.4 }
%"class.btGImpactCompoundShape::CompoundPrimitiveManager" = type { %class.btPrimitiveManagerBase, %class.btGImpactCompoundShape* }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btTransform*, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btAlignedObjectArray.4 = type <{ %class.btAlignedAllocator.5, [3 x i8], i32, i32, %class.btCollisionShape**, i8, [3 x i8] }>
%class.btAlignedAllocator.5 = type { i8 }
%class.btGImpactMeshShape = type { %class.btGImpactShapeInterface, %class.btStridingMeshInterface*, %class.btAlignedObjectArray.8 }
%class.btAlignedObjectArray.8 = type <{ %class.btAlignedAllocator.9, [3 x i8], i32, i32, %class.btGImpactMeshShapePart**, i8, [3 x i8] }>
%class.btAlignedAllocator.9 = type { i8 }
%"struct.btCollisionWorld::RayResultCallback" = type { i32 (...)**, float, %class.btCollisionObject*, i32, i32, i32 }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.12, i32, %class.btVector3 }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btAlignedObjectArray.12 = type <{ %class.btAlignedAllocator.13, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.13 = type { i8 }
%class.btTriangleCallback = type { i32 (...)** }
%class.btAlignedObjectArray.16 = type <{ %class.btAlignedAllocator.17, [3 x i8], i32, i32, i32*, i8, [3 x i8] }>
%class.btAlignedAllocator.17 = type { i8 }
%class.btPrimitiveTriangle = type { [3 x %class.btVector3], %class.btVector4, float, float }
%class.btVector4 = type { %class.btVector3 }
%class.btSerializer = type opaque
%struct.btGImpactMeshShapeData = type { %struct.btCollisionShapeData, %struct.btStridingMeshInterfaceData, %struct.btVector3FloatData, float, i32 }
%struct.btCollisionShapeData = type { i8*, i32, [4 x i8] }
%struct.btStridingMeshInterfaceData = type { %struct.btMeshPartData*, %struct.btVector3FloatData, i32, [4 x i8] }
%struct.btMeshPartData = type { %struct.btVector3FloatData*, %struct.btVector3DoubleData*, %struct.btIntIndexData*, %struct.btShortIntIndexTripletData*, %struct.btCharIndexTripletData*, %struct.btShortIntIndexData*, i32, i32 }
%struct.btVector3DoubleData = type { [4 x double] }
%struct.btIntIndexData = type { i32 }
%struct.btShortIntIndexTripletData = type { [3 x i16], [2 x i8] }
%struct.btCharIndexTripletData = type { [3 x i8], i8 }
%struct.btShortIntIndexData = type { i16, [2 x i8] }
%struct.btVector3FloatData = type { [4 x float] }
%class.btTriangleShapeEx = type { %class.btTriangleShape }
%class.btTriangleShape = type { %class.btPolyhedralConvexShape, [3 x %class.btVector3] }
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btConvexPolyhedron = type opaque
%class.btTetrahedronShapeEx = type { %class.btBU_Simplex1to4 }
%class.btBU_Simplex1to4 = type { %class.btPolyhedralConvexAabbCachingShape.base, i32, [4 x %class.btVector3] }
%class.btPolyhedralConvexAabbCachingShape.base = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8 }>
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.BT_QUANTIZED_BVH_NODE*, i8, [3 x i8] }>

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN23btGImpactShapeInterfaceC2Ev = comdat any

$_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManagerC2Ev = comdat any

$_ZN21btGImpactQuantizedBvh19setPrimitiveManagerEP22btPrimitiveManagerBase = comdat any

$_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManagerD2Ev = comdat any

$_ZN14btConcaveShapedlEPv = comdat any

$_ZNK21btGImpactQuantizedBvh19getPrimitiveManagerEv = comdat any

$_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManager4lockEv = comdat any

$_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManager6unlockEv = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK20btAlignedObjectArrayIP16btCollisionShapeEixEi = comdat any

$_Z27gim_inertia_add_transformedRK9btVector3S1_RK11btTransform = comdat any

$_ZNK20btAlignedObjectArrayI11btTransformEixEi = comdat any

$_ZN11btTransform11getIdentityEv = comdat any

$_ZNK22btGImpactMeshShapePart14getVertexCountEv = comdat any

$_ZNK22btGImpactMeshShapePart9getVertexEiR9btVector3 = comdat any

$_Z21gim_get_point_inertiaRK9btVector3f = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK18btGImpactMeshShape16getMeshPartCountEv = comdat any

$_ZNK18btGImpactMeshShape11getMeshPartEi = comdat any

$_ZN20btAlignedObjectArrayIiEC2Ev = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZNK20btAlignedObjectArrayIiE4sizeEv = comdat any

$_ZNK22btGImpactMeshShapePart7getPartEv = comdat any

$_ZN19btPrimitiveTriangleC2Ev = comdat any

$_ZNK23btGImpactShapeInterface20getPrimitiveTriangleEiR19btPrimitiveTriangle = comdat any

$_ZN20btAlignedObjectArrayIiEixEi = comdat any

$_ZN20btAlignedObjectArrayIiED2Ev = comdat any

$_ZN6btAABBC2Ev = comdat any

$_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartE4sizeEv = comdat any

$_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi = comdat any

$_ZNK9btVector314serializeFloatER18btVector3FloatData = comdat any

$_ZN22btGImpactCompoundShapeD2Ev = comdat any

$_ZN22btGImpactCompoundShapeD0Ev = comdat any

$_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_ = comdat any

$_ZN23btGImpactShapeInterface15setLocalScalingERK9btVector3 = comdat any

$_ZNK23btGImpactShapeInterface15getLocalScalingEv = comdat any

$_ZNK22btGImpactCompoundShape7getNameEv = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZN23btGImpactShapeInterface9setMarginEf = comdat any

$_ZNK14btConcaveShape9getMarginEv = comdat any

$_ZNK16btCollisionShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK23btGImpactShapeInterface19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_ = comdat any

$_ZN23btGImpactShapeInterface13calcLocalAABBEv = comdat any

$_ZN23btGImpactShapeInterface10postUpdateEv = comdat any

$_ZNK23btGImpactShapeInterface12getShapeTypeEv = comdat any

$_ZNK22btGImpactCompoundShape19getGImpactShapeTypeEv = comdat any

$_ZNK22btGImpactCompoundShape19getPrimitiveManagerEv = comdat any

$_ZNK22btGImpactCompoundShape17getNumChildShapesEv = comdat any

$_ZNK22btGImpactCompoundShape20childrenHasTransformEv = comdat any

$_ZNK22btGImpactCompoundShape22needsRetrieveTrianglesEv = comdat any

$_ZNK22btGImpactCompoundShape25needsRetrieveTetrahedronsEv = comdat any

$_ZNK22btGImpactCompoundShape17getBulletTriangleEiR17btTriangleShapeEx = comdat any

$_ZNK22btGImpactCompoundShape20getBulletTetrahedronEiR20btTetrahedronShapeEx = comdat any

$_ZNK23btGImpactShapeInterface15lockChildShapesEv = comdat any

$_ZNK23btGImpactShapeInterface17unlockChildShapesEv = comdat any

$_ZNK22btGImpactCompoundShape12getChildAabbEiRK11btTransformR9btVector3S4_ = comdat any

$_ZN22btGImpactCompoundShape13getChildShapeEi = comdat any

$_ZNK22btGImpactCompoundShape13getChildShapeEi = comdat any

$_ZNK22btGImpactCompoundShape17getChildTransformEi = comdat any

$_ZN22btGImpactCompoundShape17setChildTransformEiRK11btTransform = comdat any

$_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE = comdat any

$_ZNK23btGImpactShapeInterface22processAllTrianglesRayEP18btTriangleCallbackRK9btVector3S4_ = comdat any

$_ZN22btGImpactMeshShapePart15setLocalScalingERK9btVector3 = comdat any

$_ZNK22btGImpactMeshShapePart15getLocalScalingEv = comdat any

$_ZNK22btGImpactMeshShapePart7getNameEv = comdat any

$_ZN22btGImpactMeshShapePart9setMarginEf = comdat any

$_ZNK22btGImpactMeshShapePart9getMarginEv = comdat any

$_ZNK22btGImpactMeshShapePart19getGImpactShapeTypeEv = comdat any

$_ZNK22btGImpactMeshShapePart19getPrimitiveManagerEv = comdat any

$_ZNK22btGImpactMeshShapePart17getNumChildShapesEv = comdat any

$_ZNK22btGImpactMeshShapePart20childrenHasTransformEv = comdat any

$_ZNK22btGImpactMeshShapePart22needsRetrieveTrianglesEv = comdat any

$_ZNK22btGImpactMeshShapePart25needsRetrieveTetrahedronsEv = comdat any

$_ZNK22btGImpactMeshShapePart17getBulletTriangleEiR17btTriangleShapeEx = comdat any

$_ZNK22btGImpactMeshShapePart20getBulletTetrahedronEiR20btTetrahedronShapeEx = comdat any

$_ZNK23btGImpactShapeInterface12getChildAabbEiRK11btTransformR9btVector3S4_ = comdat any

$_ZN22btGImpactMeshShapePart13getChildShapeEi = comdat any

$_ZNK22btGImpactMeshShapePart13getChildShapeEi = comdat any

$_ZNK22btGImpactMeshShapePart17getChildTransformEi = comdat any

$_ZN22btGImpactMeshShapePart17setChildTransformEiRK11btTransform = comdat any

$_ZN18btGImpactMeshShapeD2Ev = comdat any

$_ZN18btGImpactMeshShapeD0Ev = comdat any

$_ZN18btGImpactMeshShape15setLocalScalingERK9btVector3 = comdat any

$_ZNK18btGImpactMeshShape7getNameEv = comdat any

$_ZN18btGImpactMeshShape9setMarginEf = comdat any

$_ZNK18btGImpactMeshShape28calculateSerializeBufferSizeEv = comdat any

$_ZN18btGImpactMeshShape13calcLocalAABBEv = comdat any

$_ZN18btGImpactMeshShape10postUpdateEv = comdat any

$_ZNK18btGImpactMeshShape19getGImpactShapeTypeEv = comdat any

$_ZNK18btGImpactMeshShape19getPrimitiveManagerEv = comdat any

$_ZNK18btGImpactMeshShape17getNumChildShapesEv = comdat any

$_ZNK18btGImpactMeshShape20childrenHasTransformEv = comdat any

$_ZNK18btGImpactMeshShape22needsRetrieveTrianglesEv = comdat any

$_ZNK18btGImpactMeshShape25needsRetrieveTetrahedronsEv = comdat any

$_ZNK18btGImpactMeshShape17getBulletTriangleEiR17btTriangleShapeEx = comdat any

$_ZNK18btGImpactMeshShape20getBulletTetrahedronEiR20btTetrahedronShapeEx = comdat any

$_ZNK18btGImpactMeshShape15lockChildShapesEv = comdat any

$_ZNK18btGImpactMeshShape17unlockChildShapesEv = comdat any

$_ZNK18btGImpactMeshShape12getChildAabbEiRK11btTransformR9btVector3S4_ = comdat any

$_ZN18btGImpactMeshShape13getChildShapeEi = comdat any

$_ZNK18btGImpactMeshShape13getChildShapeEi = comdat any

$_ZNK18btGImpactMeshShape17getChildTransformEi = comdat any

$_ZN18btGImpactMeshShape17setChildTransformEiRK11btTransform = comdat any

$_ZN21btGImpactQuantizedBvhC2Ev = comdat any

$_ZN6btAABB10invalidateEv = comdat any

$_ZN23btGImpactShapeInterfaceD2Ev = comdat any

$_ZN23btGImpactShapeInterfaceD0Ev = comdat any

$_ZN18btQuantizedBvhTreeC2Ev = comdat any

$_ZN28GIM_QUANTIZED_BVH_NODE_ARRAYC2Ev = comdat any

$_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEEC2Ev = comdat any

$_ZN18btAlignedAllocatorI21BT_QUANTIZED_BVH_NODELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE4initEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN22btPrimitiveManagerBaseC2Ev = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManagerD0Ev = comdat any

$_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10is_trimeshEv = comdat any

$_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_primitive_countEv = comdat any

$_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager17get_primitive_boxEiR6btAABB = comdat any

$_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager22get_primitive_triangleEiR19btPrimitiveTriangle = comdat any

$_ZN22btPrimitiveManagerBaseD2Ev = comdat any

$_ZN22btPrimitiveManagerBaseD0Ev = comdat any

$_ZN6btAABB25calc_from_triangle_marginI9btVector3EEvRKT_S4_S4_f = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager11get_indicesEiRjS1_S1_ = comdat any

$_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEjR9btVector3 = comdat any

$_ZN21btGImpactQuantizedBvhD2Ev = comdat any

$_ZN18btQuantizedBvhTreeD2Ev = comdat any

$_ZN28GIM_QUANTIZED_BVH_NODE_ARRAYD2Ev = comdat any

$_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEED2Ev = comdat any

$_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorI21BT_QUANTIZED_BVH_NODELj16EE10deallocateEPS0_ = comdat any

$_ZmlRK11btMatrix3x3S1_ = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btMatrix3x36scaledERK9btVector3 = comdat any

$_ZNK11btMatrix3x39transposeEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x311getIdentityEv = comdat any

$_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3 = comdat any

$_ZN11btMatrix3x3C2ERKS_ = comdat any

$_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager16get_vertex_countEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_Z6btSqrtf = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN9btVector4C2Ev = comdat any

$_ZN20btAlignedObjectArrayIP16btCollisionShapeED2Ev = comdat any

$_ZN20btAlignedObjectArrayI11btTransformED2Ev = comdat any

$_ZN22btGImpactCompoundShape24CompoundPrimitiveManagerD2Ev = comdat any

$_ZN20btAlignedObjectArrayIP16btCollisionShapeE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP16btCollisionShapeE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayIP16btCollisionShapeE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP16btCollisionShapeE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIP16btCollisionShapeE4initEv = comdat any

$_ZN18btAlignedAllocatorIP16btCollisionShapeLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayI11btTransformE5clearEv = comdat any

$_ZN20btAlignedObjectArrayI11btTransformE7destroyEii = comdat any

$_ZNK20btAlignedObjectArrayI11btTransformE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayI11btTransformE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayI11btTransformE4initEv = comdat any

$_ZN18btAlignedAllocatorI11btTransformLj16EE10deallocateEPS0_ = comdat any

$_ZN6btAABBC2ERKS_ = comdat any

$_ZN6btAABB14appy_transformERK11btTransform = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK11btMatrix3x36getRowEi = comdat any

$_ZNK9btVector38absoluteEv = comdat any

$_Z6btFabsf = comdat any

$_ZNK21btGImpactQuantizedBvh12getNodeCountEv = comdat any

$_ZN21btGImpactQuantizedBvh6updateEv = comdat any

$_ZNK21btGImpactQuantizedBvh12getGlobalBoxEv = comdat any

$_ZNK18btQuantizedBvhTree12getNodeCountEv = comdat any

$_ZNK21btGImpactQuantizedBvh12getNodeBoundEiR6btAABB = comdat any

$_ZNK18btQuantizedBvhTree12getNodeBoundEiR6btAABB = comdat any

$_Z13bt_unquantizePKtRK9btVector3S3_ = comdat any

$_ZNK20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEEixEi = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK11btTransformmlERKS_ = comdat any

$_ZN20btAlignedObjectArrayIP16btCollisionShapeEixEi = comdat any

$_ZN11btTransformC2ERKS_ = comdat any

$_ZN20btAlignedObjectArrayI11btTransformEixEi = comdat any

$_ZN11btTransformaSERKS_ = comdat any

$_ZN11btMatrix3x3aSERKS_ = comdat any

$_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_bullet_triangleEiR17btTriangleShapeEx = comdat any

$_ZN11btTransformC2Ev = comdat any

$_ZN11btMatrix3x3C2Ev = comdat any

$_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi = comdat any

$_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartE4initEv = comdat any

$_ZN18btAlignedAllocatorIP22btGImpactMeshShapePartLj16EE10deallocateEPS1_ = comdat any

$_ZN23btGImpactShapeInterface11updateBoundEv = comdat any

$_ZN6btAABB5mergeERKS_ = comdat any

$_ZN23btGImpactShapeInterface11getLocalBoxEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIiE4initEv = comdat any

$_ZN20btAlignedObjectArrayIiE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIiE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIiE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi = comdat any

$_ZTS23btGImpactShapeInterface = comdat any

$_ZTI23btGImpactShapeInterface = comdat any

$_ZTV23btGImpactShapeInterface = comdat any

$_ZTVN22btGImpactMeshShapePart23TrimeshPrimitiveManagerE = comdat any

$_ZTSN22btGImpactMeshShapePart23TrimeshPrimitiveManagerE = comdat any

$_ZTS22btPrimitiveManagerBase = comdat any

$_ZTI22btPrimitiveManagerBase = comdat any

$_ZTIN22btGImpactMeshShapePart23TrimeshPrimitiveManagerE = comdat any

$_ZTV22btPrimitiveManagerBase = comdat any

$_ZZN11btTransform11getIdentityEvE17identityTransform = comdat any

$_ZGVZN11btTransform11getIdentityEvE17identityTransform = comdat any

$_ZZN11btMatrix3x311getIdentityEvE14identityMatrix = comdat any

$_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV22btGImpactMeshShapePart = hidden unnamed_addr constant { [39 x i8*] } { [39 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btGImpactMeshShapePart to i8*), i8* bitcast (%class.btGImpactMeshShapePart* (%class.btGImpactMeshShapePart*)* @_ZN22btGImpactMeshShapePartD1Ev to i8*), i8* bitcast (void (%class.btGImpactMeshShapePart*)* @_ZN22btGImpactMeshShapePartD0Ev to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btGImpactMeshShapePart*, %class.btVector3*)* @_ZN22btGImpactMeshShapePart15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btGImpactMeshShapePart*)* @_ZNK22btGImpactMeshShapePart15getLocalScalingEv to i8*), i8* bitcast (void (%class.btGImpactMeshShapePart*, float, %class.btVector3*)* @_ZNK22btGImpactMeshShapePart21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btGImpactMeshShapePart*)* @_ZNK22btGImpactMeshShapePart7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btGImpactMeshShapePart*, float)* @_ZN22btGImpactMeshShapePart9setMarginEf to i8*), i8* bitcast (float (%class.btGImpactMeshShapePart*)* @_ZNK22btGImpactMeshShapePart9getMarginEv to i8*), i8* bitcast (i32 (%class.btCollisionShape*)* @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)* @_ZNK16btCollisionShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK22btGImpactMeshShapePart19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_ to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*)* @_ZN23btGImpactShapeInterface13calcLocalAABBEv to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*)* @_ZN23btGImpactShapeInterface10postUpdateEv to i8*), i8* bitcast (i32 (%class.btGImpactShapeInterface*)* @_ZNK23btGImpactShapeInterface12getShapeTypeEv to i8*), i8* bitcast (i32 (%class.btGImpactMeshShapePart*)* @_ZNK22btGImpactMeshShapePart19getGImpactShapeTypeEv to i8*), i8* bitcast (%class.btPrimitiveManagerBase* (%class.btGImpactMeshShapePart*)* @_ZNK22btGImpactMeshShapePart19getPrimitiveManagerEv to i8*), i8* bitcast (i32 (%class.btGImpactMeshShapePart*)* @_ZNK22btGImpactMeshShapePart17getNumChildShapesEv to i8*), i8* bitcast (i1 (%class.btGImpactMeshShapePart*)* @_ZNK22btGImpactMeshShapePart20childrenHasTransformEv to i8*), i8* bitcast (i1 (%class.btGImpactMeshShapePart*)* @_ZNK22btGImpactMeshShapePart22needsRetrieveTrianglesEv to i8*), i8* bitcast (i1 (%class.btGImpactMeshShapePart*)* @_ZNK22btGImpactMeshShapePart25needsRetrieveTetrahedronsEv to i8*), i8* bitcast (void (%class.btGImpactMeshShapePart*, i32, %class.btTriangleShapeEx*)* @_ZNK22btGImpactMeshShapePart17getBulletTriangleEiR17btTriangleShapeEx to i8*), i8* bitcast (void (%class.btGImpactMeshShapePart*, i32, %class.btTetrahedronShapeEx*)* @_ZNK22btGImpactMeshShapePart20getBulletTetrahedronEiR20btTetrahedronShapeEx to i8*), i8* bitcast (void (%class.btGImpactMeshShapePart*)* @_ZNK22btGImpactMeshShapePart15lockChildShapesEv to i8*), i8* bitcast (void (%class.btGImpactMeshShapePart*)* @_ZNK22btGImpactMeshShapePart17unlockChildShapesEv to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK23btGImpactShapeInterface12getChildAabbEiRK11btTransformR9btVector3S4_ to i8*), i8* bitcast (%class.btCollisionShape* (%class.btGImpactMeshShapePart*, i32)* @_ZN22btGImpactMeshShapePart13getChildShapeEi to i8*), i8* bitcast (%class.btCollisionShape* (%class.btGImpactMeshShapePart*, i32)* @_ZNK22btGImpactMeshShapePart13getChildShapeEi to i8*), i8* bitcast (void (%class.btTransform*, %class.btGImpactMeshShapePart*, i32)* @_ZNK22btGImpactMeshShapePart17getChildTransformEi to i8*), i8* bitcast (void (%class.btGImpactMeshShapePart*, i32, %class.btTransform*)* @_ZN22btGImpactMeshShapePart17setChildTransformEiRK11btTransform to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)* @_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE to i8*), i8* bitcast (void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK22btGImpactMeshShapePart22processAllTrianglesRayEP18btTriangleCallbackRK9btVector3S4_ to i8*)] }, align 4
@.str = private unnamed_addr constant [23 x i8] c"btGImpactMeshShapeData\00", align 1
@_ZTV22btGImpactCompoundShape = hidden unnamed_addr constant { [39 x i8*] } { [39 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI22btGImpactCompoundShape to i8*), i8* bitcast (%class.btGImpactCompoundShape* (%class.btGImpactCompoundShape*)* @_ZN22btGImpactCompoundShapeD2Ev to i8*), i8* bitcast (void (%class.btGImpactCompoundShape*)* @_ZN22btGImpactCompoundShapeD0Ev to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, %class.btVector3*)* @_ZN23btGImpactShapeInterface15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btGImpactShapeInterface*)* @_ZNK23btGImpactShapeInterface15getLocalScalingEv to i8*), i8* bitcast (void (%class.btGImpactCompoundShape*, float, %class.btVector3*)* @_ZNK22btGImpactCompoundShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btGImpactCompoundShape*)* @_ZNK22btGImpactCompoundShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, float)* @_ZN23btGImpactShapeInterface9setMarginEf to i8*), i8* bitcast (float (%class.btConcaveShape*)* @_ZNK14btConcaveShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCollisionShape*)* @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)* @_ZNK16btCollisionShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK23btGImpactShapeInterface19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_ to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*)* @_ZN23btGImpactShapeInterface13calcLocalAABBEv to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*)* @_ZN23btGImpactShapeInterface10postUpdateEv to i8*), i8* bitcast (i32 (%class.btGImpactShapeInterface*)* @_ZNK23btGImpactShapeInterface12getShapeTypeEv to i8*), i8* bitcast (i32 (%class.btGImpactCompoundShape*)* @_ZNK22btGImpactCompoundShape19getGImpactShapeTypeEv to i8*), i8* bitcast (%class.btPrimitiveManagerBase* (%class.btGImpactCompoundShape*)* @_ZNK22btGImpactCompoundShape19getPrimitiveManagerEv to i8*), i8* bitcast (i32 (%class.btGImpactCompoundShape*)* @_ZNK22btGImpactCompoundShape17getNumChildShapesEv to i8*), i8* bitcast (i1 (%class.btGImpactCompoundShape*)* @_ZNK22btGImpactCompoundShape20childrenHasTransformEv to i8*), i8* bitcast (i1 (%class.btGImpactCompoundShape*)* @_ZNK22btGImpactCompoundShape22needsRetrieveTrianglesEv to i8*), i8* bitcast (i1 (%class.btGImpactCompoundShape*)* @_ZNK22btGImpactCompoundShape25needsRetrieveTetrahedronsEv to i8*), i8* bitcast (void (%class.btGImpactCompoundShape*, i32, %class.btTriangleShapeEx*)* @_ZNK22btGImpactCompoundShape17getBulletTriangleEiR17btTriangleShapeEx to i8*), i8* bitcast (void (%class.btGImpactCompoundShape*, i32, %class.btTetrahedronShapeEx*)* @_ZNK22btGImpactCompoundShape20getBulletTetrahedronEiR20btTetrahedronShapeEx to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*)* @_ZNK23btGImpactShapeInterface15lockChildShapesEv to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*)* @_ZNK23btGImpactShapeInterface17unlockChildShapesEv to i8*), i8* bitcast (void (%class.btGImpactCompoundShape*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK22btGImpactCompoundShape12getChildAabbEiRK11btTransformR9btVector3S4_ to i8*), i8* bitcast (%class.btCollisionShape* (%class.btGImpactCompoundShape*, i32)* @_ZN22btGImpactCompoundShape13getChildShapeEi to i8*), i8* bitcast (%class.btCollisionShape* (%class.btGImpactCompoundShape*, i32)* @_ZNK22btGImpactCompoundShape13getChildShapeEi to i8*), i8* bitcast (void (%class.btTransform*, %class.btGImpactCompoundShape*, i32)* @_ZNK22btGImpactCompoundShape17getChildTransformEi to i8*), i8* bitcast (void (%class.btGImpactCompoundShape*, i32, %class.btTransform*)* @_ZN22btGImpactCompoundShape17setChildTransformEiRK11btTransform to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)* @_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK23btGImpactShapeInterface22processAllTrianglesRayEP18btTriangleCallbackRK9btVector3S4_ to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS22btGImpactCompoundShape = hidden constant [25 x i8] c"22btGImpactCompoundShape\00", align 1
@_ZTS23btGImpactShapeInterface = linkonce_odr hidden constant [26 x i8] c"23btGImpactShapeInterface\00", comdat, align 1
@_ZTI14btConcaveShape = external constant i8*
@_ZTI23btGImpactShapeInterface = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btGImpactShapeInterface, i32 0, i32 0), i8* bitcast (i8** @_ZTI14btConcaveShape to i8*) }, comdat, align 4
@_ZTI22btGImpactCompoundShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btGImpactCompoundShape, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btGImpactShapeInterface to i8*) }, align 4
@_ZTS22btGImpactMeshShapePart = hidden constant [25 x i8] c"22btGImpactMeshShapePart\00", align 1
@_ZTI22btGImpactMeshShapePart = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btGImpactMeshShapePart, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btGImpactShapeInterface to i8*) }, align 4
@_ZTV18btGImpactMeshShape = hidden unnamed_addr constant { [39 x i8*] } { [39 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI18btGImpactMeshShape to i8*), i8* bitcast (%class.btGImpactMeshShape* (%class.btGImpactMeshShape*)* @_ZN18btGImpactMeshShapeD2Ev to i8*), i8* bitcast (void (%class.btGImpactMeshShape*)* @_ZN18btGImpactMeshShapeD0Ev to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btGImpactMeshShape*, %class.btVector3*)* @_ZN18btGImpactMeshShape15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btGImpactShapeInterface*)* @_ZNK23btGImpactShapeInterface15getLocalScalingEv to i8*), i8* bitcast (void (%class.btGImpactMeshShape*, float, %class.btVector3*)* @_ZNK18btGImpactMeshShape21calculateLocalInertiaEfR9btVector3 to i8*), i8* bitcast (i8* (%class.btGImpactMeshShape*)* @_ZNK18btGImpactMeshShape7getNameEv to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btGImpactMeshShape*, float)* @_ZN18btGImpactMeshShape9setMarginEf to i8*), i8* bitcast (float (%class.btConcaveShape*)* @_ZNK14btConcaveShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btGImpactMeshShape*)* @_ZNK18btGImpactMeshShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btGImpactMeshShape*, i8*, %class.btSerializer*)* @_ZNK18btGImpactMeshShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btGImpactMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK18btGImpactMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_ to i8*), i8* bitcast (void (%class.btGImpactMeshShape*)* @_ZN18btGImpactMeshShape13calcLocalAABBEv to i8*), i8* bitcast (void (%class.btGImpactMeshShape*)* @_ZN18btGImpactMeshShape10postUpdateEv to i8*), i8* bitcast (i32 (%class.btGImpactShapeInterface*)* @_ZNK23btGImpactShapeInterface12getShapeTypeEv to i8*), i8* bitcast (i32 (%class.btGImpactMeshShape*)* @_ZNK18btGImpactMeshShape19getGImpactShapeTypeEv to i8*), i8* bitcast (%class.btPrimitiveManagerBase* (%class.btGImpactMeshShape*)* @_ZNK18btGImpactMeshShape19getPrimitiveManagerEv to i8*), i8* bitcast (i32 (%class.btGImpactMeshShape*)* @_ZNK18btGImpactMeshShape17getNumChildShapesEv to i8*), i8* bitcast (i1 (%class.btGImpactMeshShape*)* @_ZNK18btGImpactMeshShape20childrenHasTransformEv to i8*), i8* bitcast (i1 (%class.btGImpactMeshShape*)* @_ZNK18btGImpactMeshShape22needsRetrieveTrianglesEv to i8*), i8* bitcast (i1 (%class.btGImpactMeshShape*)* @_ZNK18btGImpactMeshShape25needsRetrieveTetrahedronsEv to i8*), i8* bitcast (void (%class.btGImpactMeshShape*, i32, %class.btTriangleShapeEx*)* @_ZNK18btGImpactMeshShape17getBulletTriangleEiR17btTriangleShapeEx to i8*), i8* bitcast (void (%class.btGImpactMeshShape*, i32, %class.btTetrahedronShapeEx*)* @_ZNK18btGImpactMeshShape20getBulletTetrahedronEiR20btTetrahedronShapeEx to i8*), i8* bitcast (void (%class.btGImpactMeshShape*)* @_ZNK18btGImpactMeshShape15lockChildShapesEv to i8*), i8* bitcast (void (%class.btGImpactMeshShape*)* @_ZNK18btGImpactMeshShape17unlockChildShapesEv to i8*), i8* bitcast (void (%class.btGImpactMeshShape*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK18btGImpactMeshShape12getChildAabbEiRK11btTransformR9btVector3S4_ to i8*), i8* bitcast (%class.btCollisionShape* (%class.btGImpactMeshShape*, i32)* @_ZN18btGImpactMeshShape13getChildShapeEi to i8*), i8* bitcast (%class.btCollisionShape* (%class.btGImpactMeshShape*, i32)* @_ZNK18btGImpactMeshShape13getChildShapeEi to i8*), i8* bitcast (void (%class.btTransform*, %class.btGImpactMeshShape*, i32)* @_ZNK18btGImpactMeshShape17getChildTransformEi to i8*), i8* bitcast (void (%class.btGImpactMeshShape*, i32, %class.btTransform*)* @_ZN18btGImpactMeshShape17setChildTransformEiRK11btTransform to i8*), i8* bitcast (void (%class.btGImpactMeshShape*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)* @_ZNK18btGImpactMeshShape7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE to i8*), i8* bitcast (void (%class.btGImpactMeshShape*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK18btGImpactMeshShape22processAllTrianglesRayEP18btTriangleCallbackRK9btVector3S4_ to i8*)] }, align 4
@_ZTS18btGImpactMeshShape = hidden constant [21 x i8] c"18btGImpactMeshShape\00", align 1
@_ZTI18btGImpactMeshShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([21 x i8], [21 x i8]* @_ZTS18btGImpactMeshShape, i32 0, i32 0), i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btGImpactShapeInterface to i8*) }, align 4
@_ZTV23btGImpactShapeInterface = linkonce_odr hidden unnamed_addr constant { [39 x i8*] } { [39 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btGImpactShapeInterface to i8*), i8* bitcast (%class.btGImpactShapeInterface* (%class.btGImpactShapeInterface*)* @_ZN23btGImpactShapeInterfaceD2Ev to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*)* @_ZN23btGImpactShapeInterfaceD0Ev to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, %class.btVector3*)* @_ZN23btGImpactShapeInterface15setLocalScalingERK9btVector3 to i8*), i8* bitcast (%class.btVector3* (%class.btGImpactShapeInterface*)* @_ZNK23btGImpactShapeInterface15getLocalScalingEv to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, float)* @_ZN23btGImpactShapeInterface9setMarginEf to i8*), i8* bitcast (float (%class.btConcaveShape*)* @_ZNK14btConcaveShape9getMarginEv to i8*), i8* bitcast (i32 (%class.btCollisionShape*)* @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)* @_ZNK16btCollisionShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK23btGImpactShapeInterface19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_ to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*)* @_ZN23btGImpactShapeInterface13calcLocalAABBEv to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*)* @_ZN23btGImpactShapeInterface10postUpdateEv to i8*), i8* bitcast (i32 (%class.btGImpactShapeInterface*)* @_ZNK23btGImpactShapeInterface12getShapeTypeEv to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*)* @_ZNK23btGImpactShapeInterface15lockChildShapesEv to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*)* @_ZNK23btGImpactShapeInterface17unlockChildShapesEv to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, i32, %class.btTransform*, %class.btVector3*, %class.btVector3*)* @_ZNK23btGImpactShapeInterface12getChildAabbEiRK11btTransformR9btVector3S4_ to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, %class.btVector3*, %class.btVector3*, %"struct.btCollisionWorld::RayResultCallback"*)* @_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE to i8*), i8* bitcast (void (%class.btGImpactShapeInterface*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)* @_ZNK23btGImpactShapeInterface22processAllTrianglesRayEP18btTriangleCallbackRK9btVector3S4_ to i8*)] }, comdat, align 4
@_ZTVN22btGImpactMeshShapePart23TrimeshPrimitiveManagerE = linkonce_odr hidden unnamed_addr constant { [8 x i8*] } { [8 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTIN22btGImpactMeshShapePart23TrimeshPrimitiveManagerE to i8*), i8* bitcast (%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* (%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*)* @_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManagerD2Ev to i8*), i8* bitcast (void (%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*)* @_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManagerD0Ev to i8*), i8* bitcast (i1 (%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*)* @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10is_trimeshEv to i8*), i8* bitcast (i32 (%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*)* @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_primitive_countEv to i8*), i8* bitcast (void (%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, i32, %class.btAABB*)* @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager17get_primitive_boxEiR6btAABB to i8*), i8* bitcast (void (%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, i32, %class.btPrimitiveTriangle*)* @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager22get_primitive_triangleEiR19btPrimitiveTriangle to i8*)] }, comdat, align 4
@_ZTSN22btGImpactMeshShapePart23TrimeshPrimitiveManagerE = linkonce_odr hidden constant [52 x i8] c"N22btGImpactMeshShapePart23TrimeshPrimitiveManagerE\00", comdat, align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS22btPrimitiveManagerBase = linkonce_odr hidden constant [25 x i8] c"22btPrimitiveManagerBase\00", comdat, align 1
@_ZTI22btPrimitiveManagerBase = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([25 x i8], [25 x i8]* @_ZTS22btPrimitiveManagerBase, i32 0, i32 0) }, comdat, align 4
@_ZTIN22btGImpactMeshShapePart23TrimeshPrimitiveManagerE = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([52 x i8], [52 x i8]* @_ZTSN22btGImpactMeshShapePart23TrimeshPrimitiveManagerE, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI22btPrimitiveManagerBase to i8*) }, comdat, align 4
@_ZTV22btPrimitiveManagerBase = linkonce_odr hidden unnamed_addr constant { [8 x i8*] } { [8 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI22btPrimitiveManagerBase to i8*), i8* bitcast (%class.btPrimitiveManagerBase* (%class.btPrimitiveManagerBase*)* @_ZN22btPrimitiveManagerBaseD2Ev to i8*), i8* bitcast (void (%class.btPrimitiveManagerBase*)* @_ZN22btPrimitiveManagerBaseD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZZN11btTransform11getIdentityEvE17identityTransform = linkonce_odr hidden global %class.btTransform zeroinitializer, comdat, align 4
@_ZGVZN11btTransform11getIdentityEvE17identityTransform = linkonce_odr hidden global i32 0, comdat, align 4
@_ZZN11btMatrix3x311getIdentityEvE14identityMatrix = linkonce_odr hidden global %class.btMatrix3x3 zeroinitializer, comdat, align 4
@_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix = linkonce_odr hidden global i32 0, comdat, align 4
@.str.1 = private unnamed_addr constant [16 x i8] c"GImpactCompound\00", align 1
@.str.2 = private unnamed_addr constant [21 x i8] c"GImpactMeshShapePart\00", align 1
@.str.3 = private unnamed_addr constant [12 x i8] c"GImpactMesh\00", align 1
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btGImpactShape.cpp, i8* null }]

@_ZN22btGImpactMeshShapePartC1EP23btStridingMeshInterfacei = hidden unnamed_addr alias %class.btGImpactMeshShapePart* (%class.btGImpactMeshShapePart*, %class.btStridingMeshInterface*, i32), %class.btGImpactMeshShapePart* (%class.btGImpactMeshShapePart*, %class.btStridingMeshInterface*, i32)* @_ZN22btGImpactMeshShapePartC2EP23btStridingMeshInterfacei
@_ZN22btGImpactMeshShapePartD1Ev = hidden unnamed_addr alias %class.btGImpactMeshShapePart* (%class.btGImpactMeshShapePart*), %class.btGImpactMeshShapePart* (%class.btGImpactMeshShapePart*)* @_ZN22btGImpactMeshShapePartD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btGImpactMeshShapePart* @_ZN22btGImpactMeshShapePartC2EP23btStridingMeshInterfacei(%class.btGImpactMeshShapePart* returned %this, %class.btStridingMeshInterface* %meshInterface, i32 %part) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %meshInterface.addr = alloca %class.btStridingMeshInterface*, align 4
  %part.addr = alloca i32, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  store %class.btStridingMeshInterface* %meshInterface, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  store i32 %part, i32* %part.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %0 = bitcast %class.btGImpactMeshShapePart* %this1 to %class.btGImpactShapeInterface*
  %call = call %class.btGImpactShapeInterface* @_ZN23btGImpactShapeInterfaceC2Ev(%class.btGImpactShapeInterface* %0)
  %1 = bitcast %class.btGImpactMeshShapePart* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [39 x i8*] }, { [39 x i8*] }* @_ZTV22btGImpactMeshShapePart, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %call2 = call %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* @_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManagerC2Ev(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager)
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %meshInterface.addr, align 4
  %m_primitive_manager3 = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %m_meshInterface = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager3, i32 0, i32 2
  store %class.btStridingMeshInterface* %2, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %3 = load i32, i32* %part.addr, align 4
  %m_primitive_manager4 = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %m_part = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager4, i32 0, i32 4
  store i32 %3, i32* %m_part, align 4
  %4 = bitcast %class.btGImpactMeshShapePart* %this1 to %class.btGImpactShapeInterface*
  %m_box_set = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %4, i32 0, i32 4
  %m_primitive_manager5 = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %5 = bitcast %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager5 to %class.btPrimitiveManagerBase*
  call void @_ZN21btGImpactQuantizedBvh19setPrimitiveManagerEP22btPrimitiveManagerBase(%class.btGImpactQuantizedBvh* %m_box_set, %class.btPrimitiveManagerBase* %5)
  ret %class.btGImpactMeshShapePart* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btGImpactShapeInterface* @_ZN23btGImpactShapeInterfaceC2Ev(%class.btGImpactShapeInterface* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %0 = bitcast %class.btGImpactShapeInterface* %this1 to %class.btConcaveShape*
  %call = call %class.btConcaveShape* @_ZN14btConcaveShapeC2Ev(%class.btConcaveShape* %0)
  %1 = bitcast %class.btGImpactShapeInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [39 x i8*] }, { [39 x i8*] }* @_ZTV23btGImpactShapeInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_localAABB = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 1
  %call2 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %m_localAABB)
  %localScaling = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %localScaling)
  %m_box_set = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 4
  %call4 = call %class.btGImpactQuantizedBvh* @_ZN21btGImpactQuantizedBvhC2Ev(%class.btGImpactQuantizedBvh* %m_box_set)
  %2 = bitcast %class.btGImpactShapeInterface* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %2, i32 0, i32 1
  store i32 25, i32* %m_shapeType, align 4
  %m_localAABB5 = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 1
  call void @_ZN6btAABB10invalidateEv(%class.btAABB* %m_localAABB5)
  %m_needs_update = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 2
  store i8 1, i8* %m_needs_update, align 4
  %localScaling6 = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 3
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp7, align 4
  store float 1.000000e+00, float* %ref.tmp8, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %localScaling6, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  ret %class.btGImpactShapeInterface* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* @_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManagerC2Ev(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %0 = bitcast %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1 to %class.btPrimitiveManagerBase*
  %call = call %class.btPrimitiveManagerBase* @_ZN22btPrimitiveManagerBaseC2Ev(%class.btPrimitiveManagerBase* %0) #8
  %1 = bitcast %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [8 x i8*] }, { [8 x i8*] }* @_ZTVN22btGImpactMeshShapePart23TrimeshPrimitiveManagerE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_scale = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 3
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_scale)
  %m_meshInterface = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 2
  store %class.btStridingMeshInterface* null, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %m_part = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 4
  store i32 0, i32* %m_part, align 4
  %m_margin = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 1
  store float 0x3F847AE140000000, float* %m_margin, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  store float 1.000000e+00, float* %ref.tmp4, align 4
  store float 1.000000e+00, float* %ref.tmp5, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  %m_scale7 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 3
  %2 = bitcast %class.btVector3* %m_scale7 to i8*
  %3 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %m_lock_count = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 5
  store i32 0, i32* %m_lock_count, align 4
  %vertexbase = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 6
  store i8* null, i8** %vertexbase, align 4
  %numverts = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 7
  store i32 0, i32* %numverts, align 4
  %stride = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 9
  store i32 0, i32* %stride, align 4
  %indexbase = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 10
  store i8* null, i8** %indexbase, align 4
  %indexstride = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 11
  store i32 0, i32* %indexstride, align 4
  %numfaces = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 12
  store i32 0, i32* %numfaces, align 4
  ret %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btGImpactQuantizedBvh19setPrimitiveManagerEP22btPrimitiveManagerBase(%class.btGImpactQuantizedBvh* %this, %class.btPrimitiveManagerBase* %primitive_manager) #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactQuantizedBvh*, align 4
  %primitive_manager.addr = alloca %class.btPrimitiveManagerBase*, align 4
  store %class.btGImpactQuantizedBvh* %this, %class.btGImpactQuantizedBvh** %this.addr, align 4
  store %class.btPrimitiveManagerBase* %primitive_manager, %class.btPrimitiveManagerBase** %primitive_manager.addr, align 4
  %this1 = load %class.btGImpactQuantizedBvh*, %class.btGImpactQuantizedBvh** %this.addr, align 4
  %0 = load %class.btPrimitiveManagerBase*, %class.btPrimitiveManagerBase** %primitive_manager.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactQuantizedBvh, %class.btGImpactQuantizedBvh* %this1, i32 0, i32 1
  store %class.btPrimitiveManagerBase* %0, %class.btPrimitiveManagerBase** %m_primitive_manager, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btGImpactMeshShapePart* @_ZN22btGImpactMeshShapePartD2Ev(%class.btGImpactMeshShapePart* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %call = call %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* @_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManagerD2Ev(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager) #8
  %0 = bitcast %class.btGImpactMeshShapePart* %this1 to %class.btGImpactShapeInterface*
  %call2 = call %class.btGImpactShapeInterface* @_ZN23btGImpactShapeInterfaceD2Ev(%class.btGImpactShapeInterface* %0) #8
  ret %class.btGImpactMeshShapePart* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* @_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManagerD2Ev(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %0 = bitcast %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1 to %class.btPrimitiveManagerBase*
  %call = call %class.btPrimitiveManagerBase* @_ZN22btPrimitiveManagerBaseD2Ev(%class.btPrimitiveManagerBase* %0) #8
  ret %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN22btGImpactMeshShapePartD0Ev(%class.btGImpactMeshShapePart* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %call = call %class.btGImpactMeshShapePart* @_ZN22btGImpactMeshShapePartD1Ev(%class.btGImpactMeshShapePart* %this1) #8
  %0 = bitcast %class.btGImpactMeshShapePart* %this1 to i8*
  call void @_ZN14btConcaveShapedlEPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN14btConcaveShapedlEPv(i8* %ptr) #1 comdat {
entry:
  %ptr.addr = alloca i8*, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %0)
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK22btGImpactMeshShapePart15lockChildShapesEv(%class.btGImpactMeshShapePart* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %dummy = alloca i8*, align 4
  %dummymanager = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %0 = bitcast %class.btGImpactMeshShapePart* %this1 to %class.btGImpactShapeInterface*
  %m_box_set = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %0, i32 0, i32 4
  %call = call %class.btPrimitiveManagerBase* @_ZNK21btGImpactQuantizedBvh19getPrimitiveManagerEv(%class.btGImpactQuantizedBvh* %m_box_set)
  %1 = bitcast %class.btPrimitiveManagerBase* %call to i8*
  store i8* %1, i8** %dummy, align 4
  %2 = load i8*, i8** %dummy, align 4
  %3 = bitcast i8* %2 to %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %3, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %dummymanager, align 4
  %4 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %dummymanager, align 4
  call void @_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManager4lockEv(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPrimitiveManagerBase* @_ZNK21btGImpactQuantizedBvh19getPrimitiveManagerEv(%class.btGImpactQuantizedBvh* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactQuantizedBvh*, align 4
  store %class.btGImpactQuantizedBvh* %this, %class.btGImpactQuantizedBvh** %this.addr, align 4
  %this1 = load %class.btGImpactQuantizedBvh*, %class.btGImpactQuantizedBvh** %this.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactQuantizedBvh, %class.btGImpactQuantizedBvh* %this1, i32 0, i32 1
  %0 = load %class.btPrimitiveManagerBase*, %class.btPrimitiveManagerBase** %m_primitive_manager, align 4
  ret %class.btPrimitiveManagerBase* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManager4lockEv(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %m_lock_count = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_lock_count, align 4
  %cmp = icmp sgt i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_lock_count2 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 5
  %1 = load i32, i32* %m_lock_count2, align 4
  %inc = add nsw i32 %1, 1
  store i32 %inc, i32* %m_lock_count2, align 4
  br label %return

if.end:                                           ; preds = %entry
  %m_meshInterface = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 2
  %2 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %vertexbase = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 6
  %numverts = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 7
  %type = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 8
  %stride = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 9
  %indexbase = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 10
  %indexstride = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 11
  %numfaces = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 12
  %indicestype = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 13
  %m_part = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 4
  %3 = load i32, i32* %m_part, align 4
  %4 = bitcast %class.btStridingMeshInterface* %2 to void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)***
  %vtable = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)**, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vtable, i64 4
  %5 = load void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)*, void (%class.btStridingMeshInterface*, i8**, i32*, i32*, i32*, i8**, i32*, i32*, i32*, i32)** %vfn, align 4
  call void %5(%class.btStridingMeshInterface* %2, i8** %vertexbase, i32* nonnull align 4 dereferenceable(4) %numverts, i32* nonnull align 4 dereferenceable(4) %type, i32* nonnull align 4 dereferenceable(4) %stride, i8** %indexbase, i32* nonnull align 4 dereferenceable(4) %indexstride, i32* nonnull align 4 dereferenceable(4) %numfaces, i32* nonnull align 4 dereferenceable(4) %indicestype, i32 %3)
  %m_lock_count3 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 5
  store i32 1, i32* %m_lock_count3, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK22btGImpactMeshShapePart17unlockChildShapesEv(%class.btGImpactMeshShapePart* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %dummy = alloca i8*, align 4
  %dummymanager = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %0 = bitcast %class.btGImpactMeshShapePart* %this1 to %class.btGImpactShapeInterface*
  %m_box_set = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %0, i32 0, i32 4
  %call = call %class.btPrimitiveManagerBase* @_ZNK21btGImpactQuantizedBvh19getPrimitiveManagerEv(%class.btGImpactQuantizedBvh* %m_box_set)
  %1 = bitcast %class.btPrimitiveManagerBase* %call to i8*
  store i8* %1, i8** %dummy, align 4
  %2 = load i8*, i8** %dummy, align 4
  %3 = bitcast i8* %2 to %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %3, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %dummymanager, align 4
  %4 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %dummymanager, align 4
  call void @_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManager6unlockEv(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %4)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManager6unlockEv(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this) #2 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %m_lock_count = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 5
  %0 = load i32, i32* %m_lock_count, align 4
  %cmp = icmp eq i32 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %m_lock_count2 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 5
  %1 = load i32, i32* %m_lock_count2, align 4
  %cmp3 = icmp sgt i32 %1, 1
  br i1 %cmp3, label %if.then4, label %if.end6

if.then4:                                         ; preds = %if.end
  %m_lock_count5 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 5
  %2 = load i32, i32* %m_lock_count5, align 4
  %dec = add nsw i32 %2, -1
  store i32 %dec, i32* %m_lock_count5, align 4
  br label %return

if.end6:                                          ; preds = %if.end
  %m_meshInterface = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 2
  %3 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %m_part = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 4
  %4 = load i32, i32* %m_part, align 4
  %5 = bitcast %class.btStridingMeshInterface* %3 to void (%class.btStridingMeshInterface*, i32)***
  %vtable = load void (%class.btStridingMeshInterface*, i32)**, void (%class.btStridingMeshInterface*, i32)*** %5, align 4
  %vfn = getelementptr inbounds void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vtable, i64 6
  %6 = load void (%class.btStridingMeshInterface*, i32)*, void (%class.btStridingMeshInterface*, i32)** %vfn, align 4
  call void %6(%class.btStridingMeshInterface* %3, i32 %4)
  %vertexbase = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 6
  store i8* null, i8** %vertexbase, align 4
  %m_lock_count7 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 5
  store i32 0, i32* %m_lock_count7, align 4
  br label %return

return:                                           ; preds = %if.end6, %if.then4, %if.then
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK22btGImpactCompoundShape21calculateLocalInertiaEfR9btVector3(%class.btGImpactCompoundShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %i = alloca i32, align 4
  %shapemass = alloca float, align 4
  %temp_inertia = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  store float %mass, float* %mass.addr, align 4
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  %0 = bitcast %class.btGImpactCompoundShape* %this1 to %class.btGImpactShapeInterface*
  %1 = bitcast %class.btGImpactShapeInterface* %0 to void (%class.btGImpactShapeInterface*)***
  %vtable = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable, i64 28
  %2 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn, align 4
  call void %2(%class.btGImpactShapeInterface* %0)
  %3 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %3, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %4 = bitcast %class.btGImpactCompoundShape* %this1 to i32 (%class.btGImpactCompoundShape*)***
  %vtable4 = load i32 (%class.btGImpactCompoundShape*)**, i32 (%class.btGImpactCompoundShape*)*** %4, align 4
  %vfn5 = getelementptr inbounds i32 (%class.btGImpactCompoundShape*)*, i32 (%class.btGImpactCompoundShape*)** %vtable4, i64 22
  %5 = load i32 (%class.btGImpactCompoundShape*)*, i32 (%class.btGImpactCompoundShape*)** %vfn5, align 4
  %call = call i32 %5(%class.btGImpactCompoundShape* %this1)
  store i32 %call, i32* %i, align 4
  %6 = load float, float* %mass.addr, align 4
  %7 = load i32, i32* %i, align 4
  %conv = sitofp i32 %7 to float
  %div = fdiv float %6, %conv
  store float %div, float* %shapemass, align 4
  br label %while.cond

while.cond:                                       ; preds = %if.end, %entry
  %8 = load i32, i32* %i, align 4
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %temp_inertia)
  %m_childShapes = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 3
  %9 = load i32, i32* %i, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %class.btCollisionShape** @_ZNK20btAlignedObjectArrayIP16btCollisionShapeEixEi(%class.btAlignedObjectArray.4* %m_childShapes, i32 %9)
  %10 = load %class.btCollisionShape*, %class.btCollisionShape** %call7, align 4
  %11 = load float, float* %shapemass, align 4
  %12 = bitcast %class.btCollisionShape* %10 to void (%class.btCollisionShape*, float, %class.btVector3*)***
  %vtable8 = load void (%class.btCollisionShape*, float, %class.btVector3*)**, void (%class.btCollisionShape*, float, %class.btVector3*)*** %12, align 4
  %vfn9 = getelementptr inbounds void (%class.btCollisionShape*, float, %class.btVector3*)*, void (%class.btCollisionShape*, float, %class.btVector3*)** %vtable8, i64 8
  %13 = load void (%class.btCollisionShape*, float, %class.btVector3*)*, void (%class.btCollisionShape*, float, %class.btVector3*)** %vfn9, align 4
  call void %13(%class.btCollisionShape* %10, float %11, %class.btVector3* nonnull align 4 dereferenceable(16) %temp_inertia)
  %14 = bitcast %class.btGImpactCompoundShape* %this1 to i1 (%class.btGImpactCompoundShape*)***
  %vtable10 = load i1 (%class.btGImpactCompoundShape*)**, i1 (%class.btGImpactCompoundShape*)*** %14, align 4
  %vfn11 = getelementptr inbounds i1 (%class.btGImpactCompoundShape*)*, i1 (%class.btGImpactCompoundShape*)** %vtable10, i64 23
  %15 = load i1 (%class.btGImpactCompoundShape*)*, i1 (%class.btGImpactCompoundShape*)** %vfn11, align 4
  %call12 = call zeroext i1 %15(%class.btGImpactCompoundShape* %this1)
  br i1 %call12, label %if.then, label %if.else

if.then:                                          ; preds = %while.body
  %16 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %m_childTransforms = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 2
  %17 = load i32, i32* %i, align 4
  %call14 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK20btAlignedObjectArrayI11btTransformEixEi(%class.btAlignedObjectArray.0* %m_childTransforms, i32 %17)
  call void @_Z27gim_inertia_add_transformedRK9btVector3S1_RK11btTransform(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %16, %class.btVector3* nonnull align 4 dereferenceable(16) %temp_inertia, %class.btTransform* nonnull align 4 dereferenceable(64) %call14)
  %18 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %19 = bitcast %class.btVector3* %18 to i8*
  %20 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %19, i8* align 4 %20, i32 16, i1 false)
  br label %if.end

if.else:                                          ; preds = %while.body
  %21 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %call16 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransform11getIdentityEv()
  call void @_Z27gim_inertia_add_transformedRK9btVector3S1_RK11btTransform(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %21, %class.btVector3* nonnull align 4 dereferenceable(16) %temp_inertia, %class.btTransform* nonnull align 4 dereferenceable(64) %call16)
  %22 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %23 = bitcast %class.btVector3* %22 to i8*
  %24 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %25 = bitcast %class.btGImpactCompoundShape* %this1 to %class.btGImpactShapeInterface*
  %26 = bitcast %class.btGImpactShapeInterface* %25 to void (%class.btGImpactShapeInterface*)***
  %vtable17 = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %26, align 4
  %vfn18 = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable17, i64 29
  %27 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn18, align 4
  call void %27(%class.btGImpactShapeInterface* %25)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionShape** @_ZNK20btAlignedObjectArrayIP16btCollisionShapeEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btCollisionShape**, %class.btCollisionShape*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionShape*, %class.btCollisionShape** %0, i32 %1
  ret %class.btCollisionShape** %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z27gim_inertia_add_transformedRK9btVector3S1_RK11btTransform(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %source_inertia, %class.btVector3* nonnull align 4 dereferenceable(16) %added_inertia, %class.btTransform* nonnull align 4 dereferenceable(64) %transform) #2 comdat {
entry:
  %source_inertia.addr = alloca %class.btVector3*, align 4
  %added_inertia.addr = alloca %class.btVector3*, align 4
  %transform.addr = alloca %class.btTransform*, align 4
  %rotatedTensor = alloca %class.btMatrix3x3, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp1 = alloca %class.btMatrix3x3, align 4
  %x2 = alloca float, align 4
  %y2 = alloca float, align 4
  %z2 = alloca float, align 4
  %ix = alloca float, align 4
  %iy = alloca float, align 4
  %iz = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp31 = alloca float, align 4
  %ref.tmp35 = alloca float, align 4
  store %class.btVector3* %source_inertia, %class.btVector3** %source_inertia.addr, align 4
  store %class.btVector3* %added_inertia, %class.btVector3** %added_inertia.addr, align 4
  store %class.btTransform* %transform, %class.btTransform** %transform.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transform.addr, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %0)
  %1 = load %class.btVector3*, %class.btVector3** %added_inertia.addr, align 4
  call void @_ZNK11btMatrix3x36scaledERK9btVector3(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* %call, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btTransform*, %class.btTransform** %transform.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %2)
  call void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* sret align 4 %ref.tmp1, %class.btMatrix3x3* %call2)
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %rotatedTensor, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp1)
  %3 = load %class.btTransform*, %class.btTransform** %transform.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %3)
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call3)
  %arrayidx = getelementptr inbounds float, float* %call4, i32 0
  %4 = load float, float* %arrayidx, align 4
  store float %4, float* %x2, align 4
  %5 = load float, float* %x2, align 4
  %6 = load float, float* %x2, align 4
  %mul = fmul float %6, %5
  store float %mul, float* %x2, align 4
  %7 = load %class.btTransform*, %class.btTransform** %transform.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %7)
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 1
  %8 = load float, float* %arrayidx7, align 4
  store float %8, float* %y2, align 4
  %9 = load float, float* %y2, align 4
  %10 = load float, float* %y2, align 4
  %mul8 = fmul float %10, %9
  store float %mul8, float* %y2, align 4
  %11 = load %class.btTransform*, %class.btTransform** %transform.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %11)
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %call9)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 2
  %12 = load float, float* %arrayidx11, align 4
  store float %12, float* %z2, align 4
  %13 = load float, float* %z2, align 4
  %14 = load float, float* %z2, align 4
  %mul12 = fmul float %14, %13
  store float %mul12, float* %z2, align 4
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %rotatedTensor, i32 0)
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call13)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 0
  %15 = load float, float* %arrayidx15, align 4
  %16 = load float, float* %y2, align 4
  %17 = load float, float* %z2, align 4
  %add = fadd float %16, %17
  %mul16 = fmul float %15, %add
  store float %mul16, float* %ix, align 4
  %call17 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %rotatedTensor, i32 1)
  %call18 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 1
  %18 = load float, float* %arrayidx19, align 4
  %19 = load float, float* %x2, align 4
  %20 = load float, float* %z2, align 4
  %add20 = fadd float %19, %20
  %mul21 = fmul float %18, %add20
  store float %mul21, float* %iy, align 4
  %call22 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %rotatedTensor, i32 2)
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %call22)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 2
  %21 = load float, float* %arrayidx24, align 4
  %22 = load float, float* %x2, align 4
  %23 = load float, float* %y2, align 4
  %add25 = fadd float %22, %23
  %mul26 = fmul float %21, %add25
  store float %mul26, float* %iz, align 4
  %24 = load %class.btVector3*, %class.btVector3** %source_inertia.addr, align 4
  %call28 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %24)
  %arrayidx29 = getelementptr inbounds float, float* %call28, i32 0
  %25 = load float, float* %arrayidx29, align 4
  %26 = load float, float* %ix, align 4
  %add30 = fadd float %25, %26
  store float %add30, float* %ref.tmp27, align 4
  %27 = load %class.btVector3*, %class.btVector3** %source_inertia.addr, align 4
  %call32 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %27)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 1
  %28 = load float, float* %arrayidx33, align 4
  %29 = load float, float* %iy, align 4
  %add34 = fadd float %28, %29
  store float %add34, float* %ref.tmp31, align 4
  %30 = load %class.btVector3*, %class.btVector3** %source_inertia.addr, align 4
  %call36 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %30)
  %arrayidx37 = getelementptr inbounds float, float* %call36, i32 2
  %31 = load float, float* %arrayidx37, align 4
  %32 = load float, float* %iz, align 4
  %add38 = fadd float %31, %32
  store float %add38, float* %ref.tmp35, align 4
  %call39 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp31, float* nonnull align 4 dereferenceable(4) %ref.tmp35)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK20btAlignedObjectArrayI11btTransformEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btTransform*, %class.btTransform** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 %1
  ret %class.btTransform* %arrayidx
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransform11getIdentityEv() #2 comdat {
entry:
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN11btTransform11getIdentityEvE17identityTransform to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN11btTransform11getIdentityEvE17identityTransform) #8
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x311getIdentityEv()
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call4 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %call5 = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* @_ZZN11btTransform11getIdentityEvE17identityTransform, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  call void @__cxa_guard_release(i32* @_ZGVZN11btTransform11getIdentityEvE17identityTransform) #8
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  ret %class.btTransform* @_ZZN11btTransform11getIdentityEvE17identityTransform
}

; Function Attrs: noinline optnone
define hidden void @_ZNK22btGImpactMeshShapePart21calculateLocalInertiaEfR9btVector3(%class.btGImpactMeshShapePart* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %i = alloca i32, align 4
  %pointmass = alloca float, align 4
  %pointintertia = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  store float %mass, float* %mass.addr, align 4
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %0 = bitcast %class.btGImpactMeshShapePart* %this1 to void (%class.btGImpactMeshShapePart*)***
  %vtable = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %0, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable, i64 28
  %1 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn, align 4
  call void %1(%class.btGImpactMeshShapePart* %this1)
  %2 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %2, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %call = call i32 @_ZNK22btGImpactMeshShapePart14getVertexCountEv(%class.btGImpactMeshShapePart* %this1)
  store i32 %call, i32* %i, align 4
  %3 = load float, float* %mass.addr, align 4
  %4 = load i32, i32* %i, align 4
  %conv = sitofp i32 %4 to float
  %div = fdiv float %3, %conv
  store float %div, float* %pointmass, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %5 = load i32, i32* %i, align 4
  %dec = add nsw i32 %5, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %5, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %pointintertia)
  %6 = load i32, i32* %i, align 4
  call void @_ZNK22btGImpactMeshShapePart9getVertexEiR9btVector3(%class.btGImpactMeshShapePart* %this1, i32 %6, %class.btVector3* nonnull align 4 dereferenceable(16) %pointintertia)
  %7 = load float, float* %pointmass, align 4
  call void @_Z21gim_get_point_inertiaRK9btVector3f(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %pointintertia, float %7)
  %8 = bitcast %class.btVector3* %pointintertia to i8*
  %9 = bitcast %class.btVector3* %ref.tmp5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  %10 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %10, %class.btVector3* nonnull align 4 dereferenceable(16) %pointintertia)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %11 = bitcast %class.btGImpactMeshShapePart* %this1 to void (%class.btGImpactMeshShapePart*)***
  %vtable7 = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %11, align 4
  %vfn8 = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable7, i64 29
  %12 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn8, align 4
  call void %12(%class.btGImpactMeshShapePart* %this1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK22btGImpactMeshShapePart14getVertexCountEv(%class.btGImpactMeshShapePart* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %call = call i32 @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager16get_vertex_countEv(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK22btGImpactMeshShapePart9getVertexEiR9btVector3(%class.btGImpactMeshShapePart* %this, i32 %vertex_index, %class.btVector3* nonnull align 4 dereferenceable(16) %vertex) #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %vertex_index.addr = alloca i32, align 4
  %vertex.addr = alloca %class.btVector3*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  store i32 %vertex_index, i32* %vertex_index.addr, align 4
  store %class.btVector3* %vertex, %class.btVector3** %vertex.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %0 = load i32, i32* %vertex_index.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4
  call void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEjR9btVector3(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager, i32 %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z21gim_get_point_inertiaRK9btVector3f(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %point, float %mass) #2 comdat {
entry:
  %point.addr = alloca %class.btVector3*, align 4
  %mass.addr = alloca float, align 4
  %x2 = alloca float, align 4
  %y2 = alloca float, align 4
  %z2 = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  store %class.btVector3* %point, %class.btVector3** %point.addr, align 4
  store float %mass, float* %mass.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %call1 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx2 = getelementptr inbounds float, float* %call1, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %x2, align 4
  %4 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 1
  %5 = load float, float* %arrayidx4, align 4
  %6 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %call5 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 1
  %7 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %5, %7
  store float %mul7, float* %y2, align 4
  %8 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 2
  %9 = load float, float* %arrayidx9, align 4
  %10 = load %class.btVector3*, %class.btVector3** %point.addr, align 4
  %call10 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %10)
  %arrayidx11 = getelementptr inbounds float, float* %call10, i32 2
  %11 = load float, float* %arrayidx11, align 4
  %mul12 = fmul float %9, %11
  store float %mul12, float* %z2, align 4
  %12 = load float, float* %mass.addr, align 4
  %13 = load float, float* %y2, align 4
  %14 = load float, float* %z2, align 4
  %add = fadd float %13, %14
  %mul13 = fmul float %12, %add
  store float %mul13, float* %ref.tmp, align 4
  %15 = load float, float* %mass.addr, align 4
  %16 = load float, float* %x2, align 4
  %17 = load float, float* %z2, align 4
  %add15 = fadd float %16, %17
  %mul16 = fmul float %15, %add15
  store float %mul16, float* %ref.tmp14, align 4
  %18 = load float, float* %mass.addr, align 4
  %19 = load float, float* %x2, align 4
  %20 = load float, float* %y2, align 4
  %add18 = fadd float %19, %20
  %mul19 = fmul float %18, %add18
  store float %mul19, float* %ref.tmp17, align 4
  %call20 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK18btGImpactMeshShape21calculateLocalInertiaEfR9btVector3(%class.btGImpactMeshShape* %this, float %mass, %class.btVector3* nonnull align 4 dereferenceable(16) %inertia) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %mass.addr = alloca float, align 4
  %inertia.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %i = alloca i32, align 4
  %partmass = alloca float, align 4
  %partinertia = alloca %class.btVector3, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store float %mass, float* %mass.addr, align 4
  store %class.btVector3* %inertia, %class.btVector3** %inertia.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %0, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %call = call i32 @_ZNK18btGImpactMeshShape16getMeshPartCountEv(%class.btGImpactMeshShape* %this1)
  store i32 %call, i32* %i, align 4
  %1 = load float, float* %mass.addr, align 4
  %2 = load i32, i32* %i, align 4
  %conv = sitofp i32 %2 to float
  %div = fdiv float %1, %conv
  store float %div, float* %partmass, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %3 = load i32, i32* %i, align 4
  %dec = add nsw i32 %3, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %3, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %partinertia)
  %4 = load i32, i32* %i, align 4
  %call5 = call %class.btGImpactMeshShapePart* @_ZNK18btGImpactMeshShape11getMeshPartEi(%class.btGImpactMeshShape* %this1, i32 %4)
  %5 = load float, float* %partmass, align 4
  %6 = bitcast %class.btGImpactMeshShapePart* %call5 to void (%class.btGImpactMeshShapePart*, float, %class.btVector3*)***
  %vtable = load void (%class.btGImpactMeshShapePart*, float, %class.btVector3*)**, void (%class.btGImpactMeshShapePart*, float, %class.btVector3*)*** %6, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactMeshShapePart*, float, %class.btVector3*)*, void (%class.btGImpactMeshShapePart*, float, %class.btVector3*)** %vtable, i64 8
  %7 = load void (%class.btGImpactMeshShapePart*, float, %class.btVector3*)*, void (%class.btGImpactMeshShapePart*, float, %class.btVector3*)** %vfn, align 4
  call void %7(%class.btGImpactMeshShapePart* %call5, float %5, %class.btVector3* nonnull align 4 dereferenceable(16) %partinertia)
  %8 = load %class.btVector3*, %class.btVector3** %inertia.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %8, %class.btVector3* nonnull align 4 dereferenceable(16) %partinertia)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK18btGImpactMeshShape16getMeshPartCountEv(%class.btGImpactMeshShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %m_mesh_parts = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartE4sizeEv(%class.btAlignedObjectArray.8* %m_mesh_parts)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btGImpactMeshShapePart* @_ZNK18btGImpactMeshShape11getMeshPartEi(%class.btGImpactMeshShape* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %m_mesh_parts = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %class.btGImpactMeshShapePart** @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi(%class.btAlignedObjectArray.8* %m_mesh_parts, i32 %0)
  %1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %call, align 4
  ret %class.btGImpactMeshShapePart* %1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZNK18btGImpactMeshShape7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE(%class.btGImpactMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTo, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(24) %resultCallback) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayTo.addr = alloca %class.btVector3*, align 4
  %resultCallback.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4
  store %class.btVector3* %rayTo, %class.btVector3** %rayTo.addr, align 4
  store %"struct.btCollisionWorld::RayResultCallback"* %resultCallback, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZNK22btGImpactMeshShapePart22processAllTrianglesRayEP18btTriangleCallbackRK9btVector3S4_(%class.btGImpactMeshShapePart* %this, %class.btTriangleCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTo) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayTo.addr = alloca %class.btVector3*, align 4
  %collided = alloca %class.btAlignedObjectArray.16, align 4
  %rayDir = alloca %class.btVector3, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %part = alloca i32, align 4
  %triangle = alloca %class.btPrimitiveTriangle, align 4
  %i = alloca i32, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4
  store %class.btVector3* %rayTo, %class.btVector3** %rayTo.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %0 = bitcast %class.btGImpactMeshShapePart* %this1 to void (%class.btGImpactMeshShapePart*)***
  %vtable = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %0, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable, i64 28
  %1 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn, align 4
  call void %1(%class.btGImpactMeshShapePart* %this1)
  %call = call %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.16* %collided)
  %2 = load %class.btVector3*, %class.btVector3** %rayTo.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %rayDir, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %rayDir)
  %4 = bitcast %class.btGImpactMeshShapePart* %this1 to %class.btGImpactShapeInterface*
  %m_box_set = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %4, i32 0, i32 4
  %5 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %call3 = call zeroext i1 @_ZNK21btGImpactQuantizedBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE(%class.btGImpactQuantizedBvh* %m_box_set, %class.btVector3* nonnull align 4 dereferenceable(16) %rayDir, %class.btVector3* nonnull align 4 dereferenceable(16) %5, %class.btAlignedObjectArray.16* nonnull align 4 dereferenceable(17) %collided)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.16* %collided)
  %cmp = icmp eq i32 %call4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %6 = bitcast %class.btGImpactMeshShapePart* %this1 to void (%class.btGImpactMeshShapePart*)***
  %vtable5 = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %6, align 4
  %vfn6 = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable5, i64 29
  %7 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn6, align 4
  call void %7(%class.btGImpactMeshShapePart* %this1)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %call7 = call i32 @_ZNK22btGImpactMeshShapePart7getPartEv(%class.btGImpactMeshShapePart* %this1)
  store i32 %call7, i32* %part, align 4
  %call8 = call %class.btPrimitiveTriangle* @_ZN19btPrimitiveTriangleC2Ev(%class.btPrimitiveTriangle* %triangle)
  %call9 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.16* %collided)
  store i32 %call9, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %8 = load i32, i32* %i, align 4
  %dec = add nsw i32 %8, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %8, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %9 = bitcast %class.btGImpactMeshShapePart* %this1 to %class.btGImpactShapeInterface*
  %10 = load i32, i32* %i, align 4
  %call10 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.16* %collided, i32 %10)
  %11 = load i32, i32* %call10, align 4
  call void @_ZNK23btGImpactShapeInterface20getPrimitiveTriangleEiR19btPrimitiveTriangle(%class.btGImpactShapeInterface* %9, i32 %11, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %triangle)
  %12 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4
  %m_vertices = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %triangle, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %13 = load i32, i32* %part, align 4
  %14 = load i32, i32* %i, align 4
  %call11 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.16* %collided, i32 %14)
  %15 = load i32, i32* %call11, align 4
  %16 = bitcast %class.btTriangleCallback* %12 to void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)***
  %vtable12 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)**, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*** %16, align 4
  %vfn13 = getelementptr inbounds void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vtable12, i64 2
  %17 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vfn13, align 4
  call void %17(%class.btTriangleCallback* %12, %class.btVector3* %arraydecay, i32 %13, i32 %15)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %18 = bitcast %class.btGImpactMeshShapePart* %this1 to void (%class.btGImpactMeshShapePart*)***
  %vtable14 = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %18, align 4
  %vfn15 = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable14, i64 29
  %19 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn15, align 4
  call void %19(%class.btGImpactMeshShapePart* %this1)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end, %if.then
  %call16 = call %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.16* %collided) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.16* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.17* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.17* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.16* %this1)
  ret %class.btAlignedObjectArray.16* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

declare zeroext i1 @_ZNK21btGImpactQuantizedBvh8rayQueryERK9btVector3S2_R20btAlignedObjectArrayIiE(%class.btGImpactQuantizedBvh*, %class.btVector3* nonnull align 4 dereferenceable(16), %class.btVector3* nonnull align 4 dereferenceable(16), %class.btAlignedObjectArray.16* nonnull align 4 dereferenceable(17)) #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.16* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK22btGImpactMeshShapePart7getPartEv(%class.btGImpactMeshShapePart* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %m_part = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager, i32 0, i32 4
  %0 = load i32, i32* %m_part, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPrimitiveTriangle* @_ZN19btPrimitiveTriangleC2Ev(%class.btPrimitiveTriangle* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btPrimitiveTriangle*, align 4
  %this.addr = alloca %class.btPrimitiveTriangle*, align 4
  store %class.btPrimitiveTriangle* %this, %class.btPrimitiveTriangle** %this.addr, align 4
  %this1 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %this.addr, align 4
  store %class.btPrimitiveTriangle* %this1, %class.btPrimitiveTriangle** %retval, align 4
  %m_vertices = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_plane = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 1
  %call2 = call %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* %m_plane)
  %m_margin = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %this1, i32 0, i32 2
  store float 0x3F847AE140000000, float* %m_margin, align 4
  %0 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %retval, align 4
  ret %class.btPrimitiveTriangle* %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK23btGImpactShapeInterface20getPrimitiveTriangleEiR19btPrimitiveTriangle(%class.btGImpactShapeInterface* %this, i32 %index, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %triangle) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  %index.addr = alloca i32, align 4
  %triangle.addr = alloca %class.btPrimitiveTriangle*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store %class.btPrimitiveTriangle* %triangle, %class.btPrimitiveTriangle** %triangle.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %0 = bitcast %class.btGImpactShapeInterface* %this1 to %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)***
  %vtable = load %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)**, %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)*** %0, align 4
  %vfn = getelementptr inbounds %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)*, %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)** %vtable, i64 21
  %1 = load %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)*, %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)** %vfn, align 4
  %call = call %class.btPrimitiveManagerBase* %1(%class.btGImpactShapeInterface* %this1)
  %2 = load i32, i32* %index.addr, align 4
  %3 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %triangle.addr, align 4
  %4 = bitcast %class.btPrimitiveManagerBase* %call to void (%class.btPrimitiveManagerBase*, i32, %class.btPrimitiveTriangle*)***
  %vtable2 = load void (%class.btPrimitiveManagerBase*, i32, %class.btPrimitiveTriangle*)**, void (%class.btPrimitiveManagerBase*, i32, %class.btPrimitiveTriangle*)*** %4, align 4
  %vfn3 = getelementptr inbounds void (%class.btPrimitiveManagerBase*, i32, %class.btPrimitiveTriangle*)*, void (%class.btPrimitiveManagerBase*, i32, %class.btPrimitiveTriangle*)** %vtable2, i64 5
  %5 = load void (%class.btPrimitiveManagerBase*, i32, %class.btPrimitiveTriangle*)*, void (%class.btPrimitiveManagerBase*, i32, %class.btPrimitiveTriangle*)** %vfn3, align 4
  call void %5(%class.btPrimitiveManagerBase* %call, i32 %2, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.16* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds i32, i32* %0, i32 %1
  ret i32* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.16* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.16* %this1)
  ret %class.btAlignedObjectArray.16* %this1
}

; Function Attrs: noinline optnone
define hidden void @_ZNK22btGImpactMeshShapePart19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_(%class.btGImpactMeshShapePart* %this, %class.btTriangleCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %box = alloca %class.btAABB, align 4
  %collided = alloca %class.btAlignedObjectArray.16, align 4
  %cleanup.dest.slot = alloca i32, align 4
  %part = alloca i32, align 4
  %triangle = alloca %class.btPrimitiveTriangle, align 4
  %i = alloca i32, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %0 = bitcast %class.btGImpactMeshShapePart* %this1 to void (%class.btGImpactMeshShapePart*)***
  %vtable = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %0, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable, i64 28
  %1 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn, align 4
  call void %1(%class.btGImpactMeshShapePart* %this1)
  %call = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %box)
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %box, i32 0, i32 0
  %3 = bitcast %class.btVector3* %m_min to i8*
  %4 = bitcast %class.btVector3* %2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %5 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %box, i32 0, i32 1
  %6 = bitcast %class.btVector3* %m_max to i8*
  %7 = bitcast %class.btVector3* %5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  %call2 = call %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIiEC2Ev(%class.btAlignedObjectArray.16* %collided)
  %8 = bitcast %class.btGImpactMeshShapePart* %this1 to %class.btGImpactShapeInterface*
  %m_box_set = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %8, i32 0, i32 4
  %call3 = call zeroext i1 @_ZNK21btGImpactQuantizedBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE(%class.btGImpactQuantizedBvh* %m_box_set, %class.btAABB* nonnull align 4 dereferenceable(32) %box, %class.btAlignedObjectArray.16* nonnull align 4 dereferenceable(17) %collided)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.16* %collided)
  %cmp = icmp eq i32 %call4, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %9 = bitcast %class.btGImpactMeshShapePart* %this1 to void (%class.btGImpactMeshShapePart*)***
  %vtable5 = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %9, align 4
  %vfn6 = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable5, i64 29
  %10 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn6, align 4
  call void %10(%class.btGImpactMeshShapePart* %this1)
  store i32 1, i32* %cleanup.dest.slot, align 4
  br label %cleanup

if.end:                                           ; preds = %entry
  %call7 = call i32 @_ZNK22btGImpactMeshShapePart7getPartEv(%class.btGImpactMeshShapePart* %this1)
  store i32 %call7, i32* %part, align 4
  %call8 = call %class.btPrimitiveTriangle* @_ZN19btPrimitiveTriangleC2Ev(%class.btPrimitiveTriangle* %triangle)
  %call9 = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.16* %collided)
  store i32 %call9, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.end
  %11 = load i32, i32* %i, align 4
  %dec = add nsw i32 %11, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %11, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %12 = bitcast %class.btGImpactMeshShapePart* %this1 to %class.btGImpactShapeInterface*
  %13 = load i32, i32* %i, align 4
  %call10 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.16* %collided, i32 %13)
  %14 = load i32, i32* %call10, align 4
  call void @_ZNK23btGImpactShapeInterface20getPrimitiveTriangleEiR19btPrimitiveTriangle(%class.btGImpactShapeInterface* %12, i32 %14, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %triangle)
  %15 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4
  %m_vertices = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %triangle, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %16 = load i32, i32* %part, align 4
  %17 = load i32, i32* %i, align 4
  %call11 = call nonnull align 4 dereferenceable(4) i32* @_ZN20btAlignedObjectArrayIiEixEi(%class.btAlignedObjectArray.16* %collided, i32 %17)
  %18 = load i32, i32* %call11, align 4
  %19 = bitcast %class.btTriangleCallback* %15 to void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)***
  %vtable12 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)**, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*** %19, align 4
  %vfn13 = getelementptr inbounds void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vtable12, i64 2
  %20 = load void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)*, void (%class.btTriangleCallback*, %class.btVector3*, i32, i32)** %vfn13, align 4
  call void %20(%class.btTriangleCallback* %15, %class.btVector3* %arraydecay, i32 %16, i32 %18)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %21 = bitcast %class.btGImpactMeshShapePart* %this1 to void (%class.btGImpactMeshShapePart*)***
  %vtable14 = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %21, align 4
  %vfn15 = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable14, i64 29
  %22 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn15, align 4
  call void %22(%class.btGImpactMeshShapePart* %this1)
  store i32 0, i32* %cleanup.dest.slot, align 4
  br label %cleanup

cleanup:                                          ; preds = %while.end, %if.then
  %call16 = call %class.btAlignedObjectArray.16* @_ZN20btAlignedObjectArrayIiED2Ev(%class.btAlignedObjectArray.16* %collided) #8
  %cleanup.dest = load i32, i32* %cleanup.dest.slot, align 4
  switch i32 %cleanup.dest, label %unreachable [
    i32 0, label %cleanup.cont
    i32 1, label %cleanup.cont
  ]

cleanup.cont:                                     ; preds = %cleanup, %cleanup
  ret void

unreachable:                                      ; preds = %cleanup
  unreachable
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_min)
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_max)
  ret %class.btAABB* %this1
}

declare zeroext i1 @_ZNK21btGImpactQuantizedBvh8boxQueryERK6btAABBR20btAlignedObjectArrayIiE(%class.btGImpactQuantizedBvh*, %class.btAABB* nonnull align 4 dereferenceable(32), %class.btAlignedObjectArray.16* nonnull align 4 dereferenceable(17)) #4

; Function Attrs: noinline optnone
define hidden void @_ZNK18btGImpactMeshShape19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_(%class.btGImpactMeshShape* %this, %class.btTriangleCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %m_mesh_parts = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartE4sizeEv(%class.btAlignedObjectArray.8* %m_mesh_parts)
  store i32 %call, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %i, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_mesh_parts2 = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %1 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btGImpactMeshShapePart** @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi(%class.btAlignedObjectArray.8* %m_mesh_parts2, i32 %1)
  %2 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %call3, align 4
  %3 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %6 = bitcast %class.btGImpactMeshShapePart* %2 to void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*** %6, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 16
  %7 = load void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %7(%class.btGImpactMeshShapePart* %2, %class.btTriangleCallback* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartE4sizeEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btGImpactMeshShapePart** @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %class.btGImpactMeshShapePart**, %class.btGImpactMeshShapePart*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %0, i32 %1
  ret %class.btGImpactMeshShapePart** %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZNK18btGImpactMeshShape22processAllTrianglesRayEP18btTriangleCallbackRK9btVector3S4_(%class.btGImpactMeshShape* %this, %class.btTriangleCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTo) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayTo.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4
  store %class.btVector3* %rayTo, %class.btVector3** %rayTo.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %m_mesh_parts = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartE4sizeEv(%class.btAlignedObjectArray.8* %m_mesh_parts)
  store i32 %call, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %i, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_mesh_parts2 = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %1 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btGImpactMeshShapePart** @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi(%class.btAlignedObjectArray.8* %m_mesh_parts2, i32 %1)
  %2 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %call3, align 4
  %3 = load %class.btTriangleCallback*, %class.btTriangleCallback** %callback.addr, align 4
  %4 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %rayTo.addr, align 4
  %6 = bitcast %class.btGImpactMeshShapePart* %2 to void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)**, void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*** %6, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vtable, i64 36
  %7 = load void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)*, void (%class.btGImpactMeshShapePart*, %class.btTriangleCallback*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %7(%class.btGImpactMeshShapePart* %2, %class.btTriangleCallback* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline optnone
define hidden i8* @_ZNK18btGImpactMeshShape9serializeEPvP12btSerializer(%class.btGImpactMeshShape* %this, i8* %dataBuffer, %class.btSerializer* %serializer) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %dataBuffer.addr = alloca i8*, align 4
  %serializer.addr = alloca %class.btSerializer*, align 4
  %trimeshData = alloca %struct.btGImpactMeshShapeData*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store i8* %dataBuffer, i8** %dataBuffer.addr, align 4
  store %class.btSerializer* %serializer, %class.btSerializer** %serializer.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %0 = load i8*, i8** %dataBuffer.addr, align 4
  %1 = bitcast i8* %0 to %struct.btGImpactMeshShapeData*
  store %struct.btGImpactMeshShapeData* %1, %struct.btGImpactMeshShapeData** %trimeshData, align 4
  %2 = bitcast %class.btGImpactMeshShape* %this1 to %class.btCollisionShape*
  %3 = load %struct.btGImpactMeshShapeData*, %struct.btGImpactMeshShapeData** %trimeshData, align 4
  %m_collisionShapeData = getelementptr inbounds %struct.btGImpactMeshShapeData, %struct.btGImpactMeshShapeData* %3, i32 0, i32 0
  %4 = bitcast %struct.btCollisionShapeData* %m_collisionShapeData to i8*
  %5 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %call = call i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape* %2, i8* %4, %class.btSerializer* %5)
  %m_meshInterface = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 1
  %6 = load %class.btStridingMeshInterface*, %class.btStridingMeshInterface** %m_meshInterface, align 4
  %7 = load %struct.btGImpactMeshShapeData*, %struct.btGImpactMeshShapeData** %trimeshData, align 4
  %m_meshInterface2 = getelementptr inbounds %struct.btGImpactMeshShapeData, %struct.btGImpactMeshShapeData* %7, i32 0, i32 1
  %8 = bitcast %struct.btStridingMeshInterfaceData* %m_meshInterface2 to i8*
  %9 = load %class.btSerializer*, %class.btSerializer** %serializer.addr, align 4
  %10 = bitcast %class.btStridingMeshInterface* %6 to i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)***
  %vtable = load i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)**, i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)*** %10, align 4
  %vfn = getelementptr inbounds i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)*, i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)** %vtable, i64 14
  %11 = load i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)*, i8* (%class.btStridingMeshInterface*, i8*, %class.btSerializer*)** %vfn, align 4
  %call3 = call i8* %11(%class.btStridingMeshInterface* %6, i8* %8, %class.btSerializer* %9)
  %12 = bitcast %class.btGImpactMeshShape* %this1 to %class.btConcaveShape*
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %12, i32 0, i32 1
  %13 = load float, float* %m_collisionMargin, align 4
  %14 = load %struct.btGImpactMeshShapeData*, %struct.btGImpactMeshShapeData** %trimeshData, align 4
  %m_collisionMargin4 = getelementptr inbounds %struct.btGImpactMeshShapeData, %struct.btGImpactMeshShapeData* %14, i32 0, i32 3
  store float %13, float* %m_collisionMargin4, align 4
  %15 = bitcast %class.btGImpactMeshShape* %this1 to %class.btGImpactShapeInterface*
  %localScaling = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %15, i32 0, i32 3
  %16 = load %struct.btGImpactMeshShapeData*, %struct.btGImpactMeshShapeData** %trimeshData, align 4
  %m_localScaling = getelementptr inbounds %struct.btGImpactMeshShapeData, %struct.btGImpactMeshShapeData* %16, i32 0, i32 2
  call void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %localScaling, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %m_localScaling)
  %17 = bitcast %class.btGImpactMeshShape* %this1 to i32 (%class.btGImpactMeshShape*)***
  %vtable5 = load i32 (%class.btGImpactMeshShape*)**, i32 (%class.btGImpactMeshShape*)*** %17, align 4
  %vfn6 = getelementptr inbounds i32 (%class.btGImpactMeshShape*)*, i32 (%class.btGImpactMeshShape*)** %vtable5, i64 20
  %18 = load i32 (%class.btGImpactMeshShape*)*, i32 (%class.btGImpactMeshShape*)** %vfn6, align 4
  %call7 = call i32 %18(%class.btGImpactMeshShape* %this1)
  %19 = load %struct.btGImpactMeshShapeData*, %struct.btGImpactMeshShapeData** %trimeshData, align 4
  %m_gimpactSubType = getelementptr inbounds %struct.btGImpactMeshShapeData, %struct.btGImpactMeshShapeData* %19, i32 0, i32 4
  store i32 %call7, i32* %m_gimpactSubType, align 4
  ret i8* getelementptr inbounds ([23 x i8], [23 x i8]* @.str, i32 0, i32 0)
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK9btVector314serializeFloatER18btVector3FloatData(%class.btVector3* %this, %struct.btVector3FloatData* nonnull align 4 dereferenceable(16) %dataOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %dataOut.addr = alloca %struct.btVector3FloatData*, align 4
  %i = alloca i32, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %struct.btVector3FloatData* %dataOut, %struct.btVector3FloatData** %dataOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %0, 4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %1 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 %1
  %2 = load float, float* %arrayidx, align 4
  %3 = load %struct.btVector3FloatData*, %struct.btVector3FloatData** %dataOut.addr, align 4
  %m_floats2 = getelementptr inbounds %struct.btVector3FloatData, %struct.btVector3FloatData* %3, i32 0, i32 0
  %4 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 %4
  store float %2, float* %arrayidx3, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGImpactCompoundShape* @_ZN22btGImpactCompoundShapeD2Ev(%class.btGImpactCompoundShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  %0 = bitcast %class.btGImpactCompoundShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [39 x i8*] }, { [39 x i8*] }* @_ZTV22btGImpactCompoundShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_childShapes = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 3
  %call = call %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP16btCollisionShapeED2Ev(%class.btAlignedObjectArray.4* %m_childShapes) #8
  %m_childTransforms = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI11btTransformED2Ev(%class.btAlignedObjectArray.0* %m_childTransforms) #8
  %m_primitive_manager = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 1
  %call3 = call %"class.btGImpactCompoundShape::CompoundPrimitiveManager"* @_ZN22btGImpactCompoundShape24CompoundPrimitiveManagerD2Ev(%"class.btGImpactCompoundShape::CompoundPrimitiveManager"* %m_primitive_manager) #8
  %1 = bitcast %class.btGImpactCompoundShape* %this1 to %class.btGImpactShapeInterface*
  %call4 = call %class.btGImpactShapeInterface* @_ZN23btGImpactShapeInterfaceD2Ev(%class.btGImpactShapeInterface* %1) #8
  ret %class.btGImpactCompoundShape* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN22btGImpactCompoundShapeD0Ev(%class.btGImpactCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  %call = call %class.btGImpactCompoundShape* @_ZN22btGImpactCompoundShapeD2Ev(%class.btGImpactCompoundShape* %this1) #8
  %0 = bitcast %class.btGImpactCompoundShape* %this1 to i8*
  call void @_ZN14btConcaveShapedlEPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK23btGImpactShapeInterface7getAabbERK11btTransformR9btVector3S4_(%class.btGImpactShapeInterface* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %transformedbox = alloca %class.btAABB, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %m_localAABB = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 1
  %call = call %class.btAABB* @_ZN6btAABBC2ERKS_(%class.btAABB* %transformedbox, %class.btAABB* nonnull align 4 dereferenceable(32) %m_localAABB)
  %0 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  call void @_ZN6btAABB14appy_transformERK11btTransform(%class.btAABB* %transformedbox, %class.btTransform* nonnull align 4 dereferenceable(64) %0)
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %transformedbox, i32 0, i32 0
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %2 = bitcast %class.btVector3* %1 to i8*
  %3 = bitcast %class.btVector3* %m_min to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %transformedbox, i32 0, i32 1
  %4 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %5 = bitcast %class.btVector3* %4 to i8*
  %6 = bitcast %class.btVector3* %m_max to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %5, i8* align 4 %6, i32 16, i1 false)
  ret void
}

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #4

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #4

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN23btGImpactShapeInterface15setLocalScalingERK9btVector3(%class.btGImpactShapeInterface* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  %localScaling = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 3
  %1 = bitcast %class.btVector3* %localScaling to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = bitcast %class.btGImpactShapeInterface* %this1 to void (%class.btGImpactShapeInterface*)***
  %vtable = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable, i64 18
  %4 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn, align 4
  call void %4(%class.btGImpactShapeInterface* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK23btGImpactShapeInterface15getLocalScalingEv(%class.btGImpactShapeInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %localScaling = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 3
  ret %class.btVector3* %localScaling
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK22btGImpactCompoundShape7getNameEv(%class.btGImpactCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([16 x i8], [16 x i8]* @.str.1, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN23btGImpactShapeInterface9setMarginEf(%class.btGImpactShapeInterface* %this, float %margin) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  %margin.addr = alloca float, align 4
  %i = alloca i32, align 4
  %child = alloca %class.btCollisionShape*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4
  %1 = bitcast %class.btGImpactShapeInterface* %this1 to %class.btConcaveShape*
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %1, i32 0, i32 1
  store float %0, float* %m_collisionMargin, align 4
  %2 = bitcast %class.btGImpactShapeInterface* %this1 to i32 (%class.btGImpactShapeInterface*)***
  %vtable = load i32 (%class.btGImpactShapeInterface*)**, i32 (%class.btGImpactShapeInterface*)*** %2, align 4
  %vfn = getelementptr inbounds i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vtable, i64 22
  %3 = load i32 (%class.btGImpactShapeInterface*)*, i32 (%class.btGImpactShapeInterface*)** %vfn, align 4
  %call = call i32 %3(%class.btGImpactShapeInterface* %this1)
  store i32 %call, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %4 = load i32, i32* %i, align 4
  %dec = add nsw i32 %4, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %5 = load i32, i32* %i, align 4
  %6 = bitcast %class.btGImpactShapeInterface* %this1 to %class.btCollisionShape* (%class.btGImpactShapeInterface*, i32)***
  %vtable2 = load %class.btCollisionShape* (%class.btGImpactShapeInterface*, i32)**, %class.btCollisionShape* (%class.btGImpactShapeInterface*, i32)*** %6, align 4
  %vfn3 = getelementptr inbounds %class.btCollisionShape* (%class.btGImpactShapeInterface*, i32)*, %class.btCollisionShape* (%class.btGImpactShapeInterface*, i32)** %vtable2, i64 31
  %7 = load %class.btCollisionShape* (%class.btGImpactShapeInterface*, i32)*, %class.btCollisionShape* (%class.btGImpactShapeInterface*, i32)** %vfn3, align 4
  %call4 = call %class.btCollisionShape* %7(%class.btGImpactShapeInterface* %this1, i32 %5)
  store %class.btCollisionShape* %call4, %class.btCollisionShape** %child, align 4
  %8 = load %class.btCollisionShape*, %class.btCollisionShape** %child, align 4
  %9 = load float, float* %margin.addr, align 4
  %10 = bitcast %class.btCollisionShape* %8 to void (%class.btCollisionShape*, float)***
  %vtable5 = load void (%class.btCollisionShape*, float)**, void (%class.btCollisionShape*, float)*** %10, align 4
  %vfn6 = getelementptr inbounds void (%class.btCollisionShape*, float)*, void (%class.btCollisionShape*, float)** %vtable5, i64 11
  %11 = load void (%class.btCollisionShape*, float)*, void (%class.btCollisionShape*, float)** %vfn6, align 4
  call void %11(%class.btCollisionShape* %8, float %9)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %m_needs_update = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 2
  store i8 1, i8* %m_needs_update, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK14btConcaveShape9getMarginEv(%class.btConcaveShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btConcaveShape*, align 4
  store %class.btConcaveShape* %this, %class.btConcaveShape** %this.addr, align 4
  %this1 = load %class.btConcaveShape*, %class.btConcaveShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %this1, i32 0, i32 1
  %0 = load float, float* %m_collisionMargin, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv(%class.btCollisionShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  ret i32 12
}

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #4

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK23btGImpactShapeInterface19processAllTrianglesEP18btTriangleCallbackRK9btVector3S4_(%class.btGImpactShapeInterface* %this, %class.btTriangleCallback* %callback, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  %callback.addr = alloca %class.btTriangleCallback*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  store %class.btTriangleCallback* %callback, %class.btTriangleCallback** %callback.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN23btGImpactShapeInterface13calcLocalAABBEv(%class.btGImpactShapeInterface* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  %ref.tmp = alloca %class.btAABB, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %0 = bitcast %class.btGImpactShapeInterface* %this1 to void (%class.btGImpactShapeInterface*)***
  %vtable = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %0, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable, i64 28
  %1 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn, align 4
  call void %1(%class.btGImpactShapeInterface* %this1)
  %m_box_set = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 4
  %call = call i32 @_ZNK21btGImpactQuantizedBvh12getNodeCountEv(%class.btGImpactQuantizedBvh* %m_box_set)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_box_set2 = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 4
  call void @_ZN21btGImpactQuantizedBvh8buildSetEv(%class.btGImpactQuantizedBvh* %m_box_set2)
  br label %if.end

if.else:                                          ; preds = %entry
  %m_box_set3 = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 4
  call void @_ZN21btGImpactQuantizedBvh6updateEv(%class.btGImpactQuantizedBvh* %m_box_set3)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %2 = bitcast %class.btGImpactShapeInterface* %this1 to void (%class.btGImpactShapeInterface*)***
  %vtable4 = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %2, align 4
  %vfn5 = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable4, i64 29
  %3 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn5, align 4
  call void %3(%class.btGImpactShapeInterface* %this1)
  %m_box_set6 = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 4
  call void @_ZNK21btGImpactQuantizedBvh12getGlobalBoxEv(%class.btAABB* sret align 4 %ref.tmp, %class.btGImpactQuantizedBvh* %m_box_set6)
  %m_localAABB = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 1
  %4 = bitcast %class.btAABB* %m_localAABB to i8*
  %5 = bitcast %class.btAABB* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 32, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btGImpactShapeInterface10postUpdateEv(%class.btGImpactShapeInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %m_needs_update = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 2
  store i8 1, i8* %m_needs_update, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK23btGImpactShapeInterface12getShapeTypeEv(%class.btGImpactShapeInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  ret i32 25
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK22btGImpactCompoundShape19getGImpactShapeTypeEv(%class.btGImpactCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPrimitiveManagerBase* @_ZNK22btGImpactCompoundShape19getPrimitiveManagerEv(%class.btGImpactCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 1
  %0 = bitcast %"class.btGImpactCompoundShape::CompoundPrimitiveManager"* %m_primitive_manager to %class.btPrimitiveManagerBase*
  ret %class.btPrimitiveManagerBase* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK22btGImpactCompoundShape17getNumChildShapesEv(%class.btGImpactCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  %m_childShapes = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 3
  %call = call i32 @_ZNK20btAlignedObjectArrayIP16btCollisionShapeE4sizeEv(%class.btAlignedObjectArray.4* %m_childShapes)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK22btGImpactCompoundShape20childrenHasTransformEv(%class.btGImpactCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  %m_childTransforms = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btTransformE4sizeEv(%class.btAlignedObjectArray.0* %m_childTransforms)
  %cmp = icmp eq i32 %call, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store i1 false, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %0 = load i1, i1* %retval, align 1
  ret i1 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK22btGImpactCompoundShape22needsRetrieveTrianglesEv(%class.btGImpactCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  ret i1 false
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK22btGImpactCompoundShape25needsRetrieveTetrahedronsEv(%class.btGImpactCompoundShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  ret i1 false
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK22btGImpactCompoundShape17getBulletTriangleEiR17btTriangleShapeEx(%class.btGImpactCompoundShape* %this, i32 %prim_index, %class.btTriangleShapeEx* nonnull align 4 dereferenceable(108) %triangle) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  %prim_index.addr = alloca i32, align 4
  %triangle.addr = alloca %class.btTriangleShapeEx*, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  store i32 %prim_index, i32* %prim_index.addr, align 4
  store %class.btTriangleShapeEx* %triangle, %class.btTriangleShapeEx** %triangle.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  %0 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %triangle.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK22btGImpactCompoundShape20getBulletTetrahedronEiR20btTetrahedronShapeEx(%class.btGImpactCompoundShape* %this, i32 %prim_index, %class.btTetrahedronShapeEx* nonnull align 4 dereferenceable(164) %tetrahedron) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  %prim_index.addr = alloca i32, align 4
  %tetrahedron.addr = alloca %class.btTetrahedronShapeEx*, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  store i32 %prim_index, i32* %prim_index.addr, align 4
  store %class.btTetrahedronShapeEx* %tetrahedron, %class.btTetrahedronShapeEx** %tetrahedron.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  %0 = load %class.btTetrahedronShapeEx*, %class.btTetrahedronShapeEx** %tetrahedron.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK23btGImpactShapeInterface15lockChildShapesEv(%class.btGImpactShapeInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK23btGImpactShapeInterface17unlockChildShapesEv(%class.btGImpactShapeInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK22btGImpactCompoundShape12getChildAabbEiRK11btTransformR9btVector3S4_(%class.btGImpactCompoundShape* %this, i32 %child_index, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  %child_index.addr = alloca i32, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btTransform, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  store i32 %child_index, i32* %child_index.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  %0 = bitcast %class.btGImpactCompoundShape* %this1 to i1 (%class.btGImpactCompoundShape*)***
  %vtable = load i1 (%class.btGImpactCompoundShape*)**, i1 (%class.btGImpactCompoundShape*)*** %0, align 4
  %vfn = getelementptr inbounds i1 (%class.btGImpactCompoundShape*)*, i1 (%class.btGImpactCompoundShape*)** %vtable, i64 23
  %1 = load i1 (%class.btGImpactCompoundShape*)*, i1 (%class.btGImpactCompoundShape*)** %vfn, align 4
  %call = call zeroext i1 %1(%class.btGImpactCompoundShape* %this1)
  br i1 %call, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_childShapes = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 3
  %2 = load i32, i32* %child_index.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %class.btCollisionShape** @_ZNK20btAlignedObjectArrayIP16btCollisionShapeEixEi(%class.btAlignedObjectArray.4* %m_childShapes, i32 %2)
  %3 = load %class.btCollisionShape*, %class.btCollisionShape** %call2, align 4
  %4 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_childTransforms = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 2
  %5 = load i32, i32* %child_index.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK20btAlignedObjectArrayI11btTransformEixEi(%class.btAlignedObjectArray.0* %m_childTransforms, i32 %5)
  call void @_ZNK11btTransformmlERKS_(%class.btTransform* sret align 4 %ref.tmp, %class.btTransform* %4, %class.btTransform* nonnull align 4 dereferenceable(64) %call3)
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %7 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %8 = bitcast %class.btCollisionShape* %3 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable4 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %8, align 4
  %vfn5 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable4, i64 2
  %9 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn5, align 4
  call void %9(%class.btCollisionShape* %3, %class.btTransform* nonnull align 4 dereferenceable(64) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %6, %class.btVector3* nonnull align 4 dereferenceable(16) %7)
  br label %if.end

if.else:                                          ; preds = %entry
  %m_childShapes6 = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 3
  %10 = load i32, i32* %child_index.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) %class.btCollisionShape** @_ZNK20btAlignedObjectArrayIP16btCollisionShapeEixEi(%class.btAlignedObjectArray.4* %m_childShapes6, i32 %10)
  %11 = load %class.btCollisionShape*, %class.btCollisionShape** %call7, align 4
  %12 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %13 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %14 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %15 = bitcast %class.btCollisionShape* %11 to void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable8 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %15, align 4
  %vfn9 = getelementptr inbounds void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable8, i64 2
  %16 = load void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btCollisionShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn9, align 4
  call void %16(%class.btCollisionShape* %11, %class.btTransform* nonnull align 4 dereferenceable(64) %12, %class.btVector3* nonnull align 4 dereferenceable(16) %13, %class.btVector3* nonnull align 4 dereferenceable(16) %14)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZN22btGImpactCompoundShape13getChildShapeEi(%class.btGImpactCompoundShape* %this, i32 %index) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  %m_childShapes = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 3
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %class.btCollisionShape** @_ZN20btAlignedObjectArrayIP16btCollisionShapeEixEi(%class.btAlignedObjectArray.4* %m_childShapes, i32 %0)
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %call, align 4
  ret %class.btCollisionShape* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK22btGImpactCompoundShape13getChildShapeEi(%class.btGImpactCompoundShape* %this, i32 %index) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  %m_childShapes = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 3
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %class.btCollisionShape** @_ZNK20btAlignedObjectArrayIP16btCollisionShapeEixEi(%class.btAlignedObjectArray.4* %m_childShapes, i32 %0)
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %call, align 4
  ret %class.btCollisionShape* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK22btGImpactCompoundShape17getChildTransformEi(%class.btTransform* noalias sret align 4 %agg.result, %class.btGImpactCompoundShape* %this, i32 %index) unnamed_addr #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  %m_childTransforms = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 2
  %1 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZNK20btAlignedObjectArrayI11btTransformEixEi(%class.btAlignedObjectArray.0* %m_childTransforms, i32 %1)
  %call2 = call %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* %agg.result, %class.btTransform* nonnull align 4 dereferenceable(64) %call)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN22btGImpactCompoundShape17setChildTransformEiRK11btTransform(%class.btGImpactCompoundShape* %this, i32 %index, %class.btTransform* nonnull align 4 dereferenceable(64) %transform) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactCompoundShape*, align 4
  %index.addr = alloca i32, align 4
  %transform.addr = alloca %class.btTransform*, align 4
  store %class.btGImpactCompoundShape* %this, %class.btGImpactCompoundShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store %class.btTransform* %transform, %class.btTransform** %transform.addr, align 4
  %this1 = load %class.btGImpactCompoundShape*, %class.btGImpactCompoundShape** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transform.addr, align 4
  %m_childTransforms = getelementptr inbounds %class.btGImpactCompoundShape, %class.btGImpactCompoundShape* %this1, i32 0, i32 2
  %1 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN20btAlignedObjectArrayI11btTransformEixEi(%class.btAlignedObjectArray.0* %m_childTransforms, i32 %1)
  %call2 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %call, %class.btTransform* nonnull align 4 dereferenceable(64) %0)
  %2 = bitcast %class.btGImpactCompoundShape* %this1 to %class.btGImpactShapeInterface*
  %3 = bitcast %class.btGImpactShapeInterface* %2 to void (%class.btGImpactShapeInterface*)***
  %vtable = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable, i64 18
  %4 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn, align 4
  call void %4(%class.btGImpactShapeInterface* %2)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK23btGImpactShapeInterface7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE(%class.btGImpactShapeInterface* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %rayFrom, %class.btVector3* nonnull align 4 dereferenceable(16) %rayTo, %"struct.btCollisionWorld::RayResultCallback"* nonnull align 4 dereferenceable(24) %resultCallback) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  %rayFrom.addr = alloca %class.btVector3*, align 4
  %rayTo.addr = alloca %class.btVector3*, align 4
  %resultCallback.addr = alloca %"struct.btCollisionWorld::RayResultCallback"*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  store %class.btVector3* %rayFrom, %class.btVector3** %rayFrom.addr, align 4
  store %class.btVector3* %rayTo, %class.btVector3** %rayTo.addr, align 4
  store %"struct.btCollisionWorld::RayResultCallback"* %resultCallback, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %rayFrom.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %rayTo.addr, align 4
  %2 = load %"struct.btCollisionWorld::RayResultCallback"*, %"struct.btCollisionWorld::RayResultCallback"** %resultCallback.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK23btGImpactShapeInterface22processAllTrianglesRayEP18btTriangleCallbackRK9btVector3S4_(%class.btGImpactShapeInterface* %this, %class.btTriangleCallback* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  %.addr = alloca %class.btTriangleCallback*, align 4
  %.addr1 = alloca %class.btVector3*, align 4
  %.addr2 = alloca %class.btVector3*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  store %class.btTriangleCallback* %0, %class.btTriangleCallback** %.addr, align 4
  store %class.btVector3* %1, %class.btVector3** %.addr1, align 4
  store %class.btVector3* %2, %class.btVector3** %.addr2, align 4
  %this3 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN22btGImpactMeshShapePart15setLocalScalingERK9btVector3(%class.btGImpactMeshShapePart* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %m_scale = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager, i32 0, i32 3
  %1 = bitcast %class.btVector3* %m_scale to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = bitcast %class.btGImpactMeshShapePart* %this1 to %class.btGImpactShapeInterface*
  %4 = bitcast %class.btGImpactShapeInterface* %3 to void (%class.btGImpactShapeInterface*)***
  %vtable = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable, i64 18
  %5 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn, align 4
  call void %5(%class.btGImpactShapeInterface* %3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK22btGImpactMeshShapePart15getLocalScalingEv(%class.btGImpactMeshShapePart* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %m_scale = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager, i32 0, i32 3
  ret %class.btVector3* %m_scale
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK22btGImpactMeshShapePart7getNameEv(%class.btGImpactMeshShapePart* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  ret i8* getelementptr inbounds ([21 x i8], [21 x i8]* @.str.2, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN22btGImpactMeshShapePart9setMarginEf(%class.btGImpactMeshShapePart* %this, float %margin) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %margin.addr = alloca float, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %m_margin = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager, i32 0, i32 1
  store float %0, float* %m_margin, align 4
  %1 = bitcast %class.btGImpactMeshShapePart* %this1 to %class.btGImpactShapeInterface*
  %2 = bitcast %class.btGImpactShapeInterface* %1 to void (%class.btGImpactShapeInterface*)***
  %vtable = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable, i64 18
  %3 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn, align 4
  call void %3(%class.btGImpactShapeInterface* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK22btGImpactMeshShapePart9getMarginEv(%class.btGImpactMeshShapePart* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %m_margin = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager, i32 0, i32 1
  %0 = load float, float* %m_margin, align 4
  ret float %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK22btGImpactMeshShapePart19getGImpactShapeTypeEv(%class.btGImpactMeshShapePart* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  ret i32 1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPrimitiveManagerBase* @_ZNK22btGImpactMeshShapePart19getPrimitiveManagerEv(%class.btGImpactMeshShapePart* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %0 = bitcast %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager to %class.btPrimitiveManagerBase*
  ret %class.btPrimitiveManagerBase* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK22btGImpactMeshShapePart17getNumChildShapesEv(%class.btGImpactMeshShapePart* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %call = call i32 @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_primitive_countEv(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK22btGImpactMeshShapePart20childrenHasTransformEv(%class.btGImpactMeshShapePart* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  ret i1 false
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK22btGImpactMeshShapePart22needsRetrieveTrianglesEv(%class.btGImpactMeshShapePart* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK22btGImpactMeshShapePart25needsRetrieveTetrahedronsEv(%class.btGImpactMeshShapePart* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  ret i1 false
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK22btGImpactMeshShapePart17getBulletTriangleEiR17btTriangleShapeEx(%class.btGImpactMeshShapePart* %this, i32 %prim_index, %class.btTriangleShapeEx* nonnull align 4 dereferenceable(108) %triangle) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %prim_index.addr = alloca i32, align 4
  %triangle.addr = alloca %class.btTriangleShapeEx*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  store i32 %prim_index, i32* %prim_index.addr, align 4
  store %class.btTriangleShapeEx* %triangle, %class.btTriangleShapeEx** %triangle.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %m_primitive_manager = getelementptr inbounds %class.btGImpactMeshShapePart, %class.btGImpactMeshShapePart* %this1, i32 0, i32 1
  %0 = load i32, i32* %prim_index.addr, align 4
  %1 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %triangle.addr, align 4
  call void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_bullet_triangleEiR17btTriangleShapeEx(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %m_primitive_manager, i32 %0, %class.btTriangleShapeEx* nonnull align 4 dereferenceable(108) %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK22btGImpactMeshShapePart20getBulletTetrahedronEiR20btTetrahedronShapeEx(%class.btGImpactMeshShapePart* %this, i32 %prim_index, %class.btTetrahedronShapeEx* nonnull align 4 dereferenceable(164) %tetrahedron) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %prim_index.addr = alloca i32, align 4
  %tetrahedron.addr = alloca %class.btTetrahedronShapeEx*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  store i32 %prim_index, i32* %prim_index.addr, align 4
  store %class.btTetrahedronShapeEx* %tetrahedron, %class.btTetrahedronShapeEx** %tetrahedron.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %0 = load %class.btTetrahedronShapeEx*, %class.btTetrahedronShapeEx** %tetrahedron.addr, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK23btGImpactShapeInterface12getChildAabbEiRK11btTransformR9btVector3S4_(%class.btGImpactShapeInterface* %this, i32 %child_index, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  %child_index.addr = alloca i32, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %child_aabb = alloca %class.btAABB, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  store i32 %child_index, i32* %child_index.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %call = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %child_aabb)
  %0 = bitcast %class.btGImpactShapeInterface* %this1 to %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)***
  %vtable = load %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)**, %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)*** %0, align 4
  %vfn = getelementptr inbounds %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)*, %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)** %vtable, i64 21
  %1 = load %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)*, %class.btPrimitiveManagerBase* (%class.btGImpactShapeInterface*)** %vfn, align 4
  %call2 = call %class.btPrimitiveManagerBase* %1(%class.btGImpactShapeInterface* %this1)
  %2 = load i32, i32* %child_index.addr, align 4
  %3 = bitcast %class.btPrimitiveManagerBase* %call2 to void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)***
  %vtable3 = load void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)**, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*** %3, align 4
  %vfn4 = getelementptr inbounds void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)** %vtable3, i64 4
  %4 = load void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)*, void (%class.btPrimitiveManagerBase*, i32, %class.btAABB*)** %vfn4, align 4
  call void %4(%class.btPrimitiveManagerBase* %call2, i32 %2, %class.btAABB* nonnull align 4 dereferenceable(32) %child_aabb)
  %5 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  call void @_ZN6btAABB14appy_transformERK11btTransform(%class.btAABB* %child_aabb, %class.btTransform* nonnull align 4 dereferenceable(64) %5)
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %child_aabb, i32 0, i32 0
  %6 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %7 = bitcast %class.btVector3* %6 to i8*
  %8 = bitcast %class.btVector3* %m_min to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %child_aabb, i32 0, i32 1
  %9 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %10 = bitcast %class.btVector3* %9 to i8*
  %11 = bitcast %class.btVector3* %m_max to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZN22btGImpactMeshShapePart13getChildShapeEi(%class.btGImpactMeshShapePart* %this, i32 %index) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %index.addr = alloca i32, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  ret %class.btCollisionShape* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK22btGImpactMeshShapePart13getChildShapeEi(%class.btGImpactMeshShapePart* %this, i32 %index) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %index.addr = alloca i32, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  ret %class.btCollisionShape* null
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK22btGImpactMeshShapePart17getChildTransformEi(%class.btTransform* noalias sret align 4 %agg.result, %class.btGImpactMeshShapePart* %this, i32 %index) unnamed_addr #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %index.addr = alloca i32, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %agg.result)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN22btGImpactMeshShapePart17setChildTransformEiRK11btTransform(%class.btGImpactMeshShapePart* %this, i32 %index, %class.btTransform* nonnull align 4 dereferenceable(64) %transform) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShapePart*, align 4
  %index.addr = alloca i32, align 4
  %transform.addr = alloca %class.btTransform*, align 4
  store %class.btGImpactMeshShapePart* %this, %class.btGImpactMeshShapePart** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store %class.btTransform* %transform, %class.btTransform** %transform.addr, align 4
  %this1 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transform.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGImpactMeshShape* @_ZN18btGImpactMeshShapeD2Ev(%class.btGImpactMeshShape* returned %this) unnamed_addr #1 comdat {
entry:
  %retval = alloca %class.btGImpactMeshShape*, align 4
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %i = alloca i32, align 4
  %part = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  store %class.btGImpactMeshShape* %this1, %class.btGImpactMeshShape** %retval, align 4
  %0 = bitcast %class.btGImpactMeshShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [39 x i8*] }, { [39 x i8*] }* @_ZTV18btGImpactMeshShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_mesh_parts = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartE4sizeEv(%class.btAlignedObjectArray.8* %m_mesh_parts)
  store i32 %call, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %delete.end, %entry
  %1 = load i32, i32* %i, align 4
  %dec = add nsw i32 %1, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_mesh_parts2 = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %2 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btGImpactMeshShapePart** @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi(%class.btAlignedObjectArray.8* %m_mesh_parts2, i32 %2)
  %3 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %call3, align 4
  store %class.btGImpactMeshShapePart* %3, %class.btGImpactMeshShapePart** %part, align 4
  %4 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %part, align 4
  %isnull = icmp eq %class.btGImpactMeshShapePart* %4, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %while.body
  %5 = bitcast %class.btGImpactMeshShapePart* %4 to void (%class.btGImpactMeshShapePart*)***
  %vtable = load void (%class.btGImpactMeshShapePart*)**, void (%class.btGImpactMeshShapePart*)*** %5, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vtable, i64 1
  %6 = load void (%class.btGImpactMeshShapePart*)*, void (%class.btGImpactMeshShapePart*)** %vfn, align 4
  call void %6(%class.btGImpactMeshShapePart* %4) #8
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %while.body
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %m_mesh_parts4 = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartE5clearEv(%class.btAlignedObjectArray.8* %m_mesh_parts4)
  %m_mesh_parts5 = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %call6 = call %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartED2Ev(%class.btAlignedObjectArray.8* %m_mesh_parts5) #8
  %7 = bitcast %class.btGImpactMeshShape* %this1 to %class.btGImpactShapeInterface*
  %call7 = call %class.btGImpactShapeInterface* @_ZN23btGImpactShapeInterfaceD2Ev(%class.btGImpactShapeInterface* %7) #8
  %8 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %retval, align 4
  ret %class.btGImpactMeshShape* %8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btGImpactMeshShapeD0Ev(%class.btGImpactMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %call = call %class.btGImpactMeshShape* @_ZN18btGImpactMeshShapeD2Ev(%class.btGImpactMeshShape* %this1) #8
  %0 = bitcast %class.btGImpactMeshShape* %this1 to i8*
  call void @_ZN14btConcaveShapedlEPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btGImpactMeshShape15setLocalScalingERK9btVector3(%class.btGImpactMeshShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %scaling) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %scaling.addr = alloca %class.btVector3*, align 4
  %i = alloca i32, align 4
  %part = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store %class.btVector3* %scaling, %class.btVector3** %scaling.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  %1 = bitcast %class.btGImpactMeshShape* %this1 to %class.btGImpactShapeInterface*
  %localScaling = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %1, i32 0, i32 3
  %2 = bitcast %class.btVector3* %localScaling to i8*
  %3 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  %m_mesh_parts = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartE4sizeEv(%class.btAlignedObjectArray.8* %m_mesh_parts)
  store i32 %call, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %4 = load i32, i32* %i, align 4
  %dec = add nsw i32 %4, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %4, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_mesh_parts2 = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %5 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btGImpactMeshShapePart** @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi(%class.btAlignedObjectArray.8* %m_mesh_parts2, i32 %5)
  %6 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %call3, align 4
  store %class.btGImpactMeshShapePart* %6, %class.btGImpactMeshShapePart** %part, align 4
  %7 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %part, align 4
  %8 = load %class.btVector3*, %class.btVector3** %scaling.addr, align 4
  %9 = bitcast %class.btGImpactMeshShapePart* %7 to void (%class.btGImpactMeshShapePart*, %class.btVector3*)***
  %vtable = load void (%class.btGImpactMeshShapePart*, %class.btVector3*)**, void (%class.btGImpactMeshShapePart*, %class.btVector3*)*** %9, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactMeshShapePart*, %class.btVector3*)*, void (%class.btGImpactMeshShapePart*, %class.btVector3*)** %vtable, i64 6
  %10 = load void (%class.btGImpactMeshShapePart*, %class.btVector3*)*, void (%class.btGImpactMeshShapePart*, %class.btVector3*)** %vfn, align 4
  call void %10(%class.btGImpactMeshShapePart* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %11 = bitcast %class.btGImpactMeshShape* %this1 to %class.btGImpactShapeInterface*
  %m_needs_update = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %11, i32 0, i32 2
  store i8 1, i8* %m_needs_update, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZNK18btGImpactMeshShape7getNameEv(%class.btGImpactMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  ret i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.3, i32 0, i32 0)
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btGImpactMeshShape9setMarginEf(%class.btGImpactMeshShape* %this, float %margin) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %margin.addr = alloca float, align 4
  %i = alloca i32, align 4
  %part = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %0 = load float, float* %margin.addr, align 4
  %1 = bitcast %class.btGImpactMeshShape* %this1 to %class.btConcaveShape*
  %m_collisionMargin = getelementptr inbounds %class.btConcaveShape, %class.btConcaveShape* %1, i32 0, i32 1
  store float %0, float* %m_collisionMargin, align 4
  %m_mesh_parts = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartE4sizeEv(%class.btAlignedObjectArray.8* %m_mesh_parts)
  store i32 %call, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %2 = load i32, i32* %i, align 4
  %dec = add nsw i32 %2, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_mesh_parts2 = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %3 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btGImpactMeshShapePart** @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi(%class.btAlignedObjectArray.8* %m_mesh_parts2, i32 %3)
  %4 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %call3, align 4
  store %class.btGImpactMeshShapePart* %4, %class.btGImpactMeshShapePart** %part, align 4
  %5 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %part, align 4
  %6 = load float, float* %margin.addr, align 4
  %7 = bitcast %class.btGImpactMeshShapePart* %5 to void (%class.btGImpactMeshShapePart*, float)***
  %vtable = load void (%class.btGImpactMeshShapePart*, float)**, void (%class.btGImpactMeshShapePart*, float)*** %7, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactMeshShapePart*, float)*, void (%class.btGImpactMeshShapePart*, float)** %vtable, i64 11
  %8 = load void (%class.btGImpactMeshShapePart*, float)*, void (%class.btGImpactMeshShapePart*, float)** %vfn, align 4
  call void %8(%class.btGImpactMeshShapePart* %5, float %6)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %9 = bitcast %class.btGImpactMeshShape* %this1 to %class.btGImpactShapeInterface*
  %m_needs_update = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %9, i32 0, i32 2
  store i8 1, i8* %m_needs_update, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK18btGImpactMeshShape28calculateSerializeBufferSizeEv(%class.btGImpactMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  ret i32 64
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btGImpactMeshShape13calcLocalAABBEv(%class.btGImpactMeshShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %i = alloca i32, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %0 = bitcast %class.btGImpactMeshShape* %this1 to %class.btGImpactShapeInterface*
  %m_localAABB = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %0, i32 0, i32 1
  call void @_ZN6btAABB10invalidateEv(%class.btAABB* %m_localAABB)
  %m_mesh_parts = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartE4sizeEv(%class.btAlignedObjectArray.8* %m_mesh_parts)
  store i32 %call, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %1 = load i32, i32* %i, align 4
  %dec = add nsw i32 %1, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %1, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_mesh_parts2 = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %2 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btGImpactMeshShapePart** @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi(%class.btAlignedObjectArray.8* %m_mesh_parts2, i32 %2)
  %3 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %call3, align 4
  %4 = bitcast %class.btGImpactMeshShapePart* %3 to %class.btGImpactShapeInterface*
  call void @_ZN23btGImpactShapeInterface11updateBoundEv(%class.btGImpactShapeInterface* %4)
  %5 = bitcast %class.btGImpactMeshShape* %this1 to %class.btGImpactShapeInterface*
  %m_localAABB4 = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %5, i32 0, i32 1
  %m_mesh_parts5 = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %6 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(4) %class.btGImpactMeshShapePart** @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi(%class.btAlignedObjectArray.8* %m_mesh_parts5, i32 %6)
  %7 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %call6, align 4
  %8 = bitcast %class.btGImpactMeshShapePart* %7 to %class.btGImpactShapeInterface*
  %call7 = call nonnull align 4 dereferenceable(32) %class.btAABB* @_ZN23btGImpactShapeInterface11getLocalBoxEv(%class.btGImpactShapeInterface* %8)
  call void @_ZN6btAABB5mergeERKS_(%class.btAABB* %m_localAABB4, %class.btAABB* nonnull align 4 dereferenceable(32) %call7)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btGImpactMeshShape10postUpdateEv(%class.btGImpactMeshShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %i = alloca i32, align 4
  %part = alloca %class.btGImpactMeshShapePart*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %m_mesh_parts = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartE4sizeEv(%class.btAlignedObjectArray.8* %m_mesh_parts)
  store i32 %call, i32* %i, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %entry
  %0 = load i32, i32* %i, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %i, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %m_mesh_parts2 = getelementptr inbounds %class.btGImpactMeshShape, %class.btGImpactMeshShape* %this1, i32 0, i32 2
  %1 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %class.btGImpactMeshShapePart** @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi(%class.btAlignedObjectArray.8* %m_mesh_parts2, i32 %1)
  %2 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %call3, align 4
  store %class.btGImpactMeshShapePart* %2, %class.btGImpactMeshShapePart** %part, align 4
  %3 = load %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %part, align 4
  %4 = bitcast %class.btGImpactMeshShapePart* %3 to %class.btGImpactShapeInterface*
  %5 = bitcast %class.btGImpactShapeInterface* %4 to void (%class.btGImpactShapeInterface*)***
  %vtable = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %5, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable, i64 18
  %6 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn, align 4
  call void %6(%class.btGImpactShapeInterface* %4)
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %7 = bitcast %class.btGImpactMeshShape* %this1 to %class.btGImpactShapeInterface*
  %m_needs_update = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %7, i32 0, i32 2
  store i8 1, i8* %m_needs_update, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK18btGImpactMeshShape19getGImpactShapeTypeEv(%class.btGImpactMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  ret i32 2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPrimitiveManagerBase* @_ZNK18btGImpactMeshShape19getPrimitiveManagerEv(%class.btGImpactMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  ret %class.btPrimitiveManagerBase* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK18btGImpactMeshShape17getNumChildShapesEv(%class.btGImpactMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  ret i32 0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK18btGImpactMeshShape20childrenHasTransformEv(%class.btGImpactMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  ret i1 false
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK18btGImpactMeshShape22needsRetrieveTrianglesEv(%class.btGImpactMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  ret i1 false
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK18btGImpactMeshShape25needsRetrieveTetrahedronsEv(%class.btGImpactMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  ret i1 false
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK18btGImpactMeshShape17getBulletTriangleEiR17btTriangleShapeEx(%class.btGImpactMeshShape* %this, i32 %prim_index, %class.btTriangleShapeEx* nonnull align 4 dereferenceable(108) %triangle) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %prim_index.addr = alloca i32, align 4
  %triangle.addr = alloca %class.btTriangleShapeEx*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store i32 %prim_index, i32* %prim_index.addr, align 4
  store %class.btTriangleShapeEx* %triangle, %class.btTriangleShapeEx** %triangle.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %0 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %triangle.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK18btGImpactMeshShape20getBulletTetrahedronEiR20btTetrahedronShapeEx(%class.btGImpactMeshShape* %this, i32 %prim_index, %class.btTetrahedronShapeEx* nonnull align 4 dereferenceable(164) %tetrahedron) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %prim_index.addr = alloca i32, align 4
  %tetrahedron.addr = alloca %class.btTetrahedronShapeEx*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store i32 %prim_index, i32* %prim_index.addr, align 4
  store %class.btTetrahedronShapeEx* %tetrahedron, %class.btTetrahedronShapeEx** %tetrahedron.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %0 = load %class.btTetrahedronShapeEx*, %class.btTetrahedronShapeEx** %tetrahedron.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK18btGImpactMeshShape15lockChildShapesEv(%class.btGImpactMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK18btGImpactMeshShape17unlockChildShapesEv(%class.btGImpactMeshShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK18btGImpactMeshShape12getChildAabbEiRK11btTransformR9btVector3S4_(%class.btGImpactMeshShape* %this, i32 %child_index, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %child_index.addr = alloca i32, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store i32 %child_index, i32* %child_index.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZN18btGImpactMeshShape13getChildShapeEi(%class.btGImpactMeshShape* %this, i32 %index) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  ret %class.btCollisionShape* null
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK18btGImpactMeshShape13getChildShapeEi(%class.btGImpactMeshShape* %this, i32 %index) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %index.addr = alloca i32, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  ret %class.btCollisionShape* null
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK18btGImpactMeshShape17getChildTransformEi(%class.btTransform* noalias sret align 4 %agg.result, %class.btGImpactMeshShape* %this, i32 %index) unnamed_addr #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %index.addr = alloca i32, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %call = call %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* %agg.result)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN18btGImpactMeshShape17setChildTransformEiRK11btTransform(%class.btGImpactMeshShape* %this, i32 %index, %class.btTransform* nonnull align 4 dereferenceable(64) %transform) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactMeshShape*, align 4
  %index.addr = alloca i32, align 4
  %transform.addr = alloca %class.btTransform*, align 4
  store %class.btGImpactMeshShape* %this, %class.btGImpactMeshShape** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  store %class.btTransform* %transform, %class.btTransform** %transform.addr, align 4
  %this1 = load %class.btGImpactMeshShape*, %class.btGImpactMeshShape** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %transform.addr, align 4
  ret void
}

declare %class.btConcaveShape* @_ZN14btConcaveShapeC2Ev(%class.btConcaveShape* returned) unnamed_addr #4

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btGImpactQuantizedBvh* @_ZN21btGImpactQuantizedBvhC2Ev(%class.btGImpactQuantizedBvh* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactQuantizedBvh*, align 4
  store %class.btGImpactQuantizedBvh* %this, %class.btGImpactQuantizedBvh** %this.addr, align 4
  %this1 = load %class.btGImpactQuantizedBvh*, %class.btGImpactQuantizedBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactQuantizedBvh, %class.btGImpactQuantizedBvh* %this1, i32 0, i32 0
  %call = call %class.btQuantizedBvhTree* @_ZN18btQuantizedBvhTreeC2Ev(%class.btQuantizedBvhTree* %m_box_tree)
  %m_primitive_manager = getelementptr inbounds %class.btGImpactQuantizedBvh, %class.btGImpactQuantizedBvh* %this1, i32 0, i32 1
  store %class.btPrimitiveManagerBase* null, %class.btPrimitiveManagerBase** %m_primitive_manager, align 4
  ret %class.btGImpactQuantizedBvh* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN6btAABB10invalidateEv(%class.btAABB* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  store float 0x47EFFFFFE0000000, float* %arrayidx, align 4
  %m_min2 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call3 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 1
  store float 0x47EFFFFFE0000000, float* %arrayidx4, align 4
  %m_min5 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call6 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 2
  store float 0x47EFFFFFE0000000, float* %arrayidx7, align 4
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call8 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  store float 0xC7EFFFFFE0000000, float* %arrayidx9, align 4
  %m_max10 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call11 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max10)
  %arrayidx12 = getelementptr inbounds float, float* %call11, i32 1
  store float 0xC7EFFFFFE0000000, float* %arrayidx12, align 4
  %m_max13 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max13)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 2
  store float 0xC7EFFFFFE0000000, float* %arrayidx15, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGImpactShapeInterface* @_ZN23btGImpactShapeInterfaceD2Ev(%class.btGImpactShapeInterface* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %0 = bitcast %class.btGImpactShapeInterface* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [39 x i8*] }, { [39 x i8*] }* @_ZTV23btGImpactShapeInterface, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_box_set = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 4
  %call = call %class.btGImpactQuantizedBvh* @_ZN21btGImpactQuantizedBvhD2Ev(%class.btGImpactQuantizedBvh* %m_box_set) #8
  %1 = bitcast %class.btGImpactShapeInterface* %this1 to %class.btConcaveShape*
  %call2 = call %class.btConcaveShape* @_ZN14btConcaveShapeD2Ev(%class.btConcaveShape* %1) #8
  ret %class.btGImpactShapeInterface* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btGImpactShapeInterfaceD0Ev(%class.btGImpactShapeInterface* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btQuantizedBvhTree* @_ZN18btQuantizedBvhTreeC2Ev(%class.btQuantizedBvhTree* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvhTree*, align 4
  store %class.btQuantizedBvhTree* %this, %class.btQuantizedBvhTree** %this.addr, align 4
  %this1 = load %class.btQuantizedBvhTree*, %class.btQuantizedBvhTree** %this.addr, align 4
  %m_node_array = getelementptr inbounds %class.btQuantizedBvhTree, %class.btQuantizedBvhTree* %this1, i32 0, i32 1
  %call = call %class.GIM_QUANTIZED_BVH_NODE_ARRAY* @_ZN28GIM_QUANTIZED_BVH_NODE_ARRAYC2Ev(%class.GIM_QUANTIZED_BVH_NODE_ARRAY* %m_node_array)
  %m_global_bound = getelementptr inbounds %class.btQuantizedBvhTree, %class.btQuantizedBvhTree* %this1, i32 0, i32 2
  %call2 = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %m_global_bound)
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvhTree, %class.btQuantizedBvhTree* %this1, i32 0, i32 3
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_bvhQuantization)
  %m_num_nodes = getelementptr inbounds %class.btQuantizedBvhTree, %class.btQuantizedBvhTree* %this1, i32 0, i32 0
  store i32 0, i32* %m_num_nodes, align 4
  ret %class.btQuantizedBvhTree* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.GIM_QUANTIZED_BVH_NODE_ARRAY* @_ZN28GIM_QUANTIZED_BVH_NODE_ARRAYC2Ev(%class.GIM_QUANTIZED_BVH_NODE_ARRAY* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.GIM_QUANTIZED_BVH_NODE_ARRAY*, align 4
  store %class.GIM_QUANTIZED_BVH_NODE_ARRAY* %this, %class.GIM_QUANTIZED_BVH_NODE_ARRAY** %this.addr, align 4
  %this1 = load %class.GIM_QUANTIZED_BVH_NODE_ARRAY*, %class.GIM_QUANTIZED_BVH_NODE_ARRAY** %this.addr, align 4
  %0 = bitcast %class.GIM_QUANTIZED_BVH_NODE_ARRAY* %this1 to %class.btAlignedObjectArray*
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEEC2Ev(%class.btAlignedObjectArray* %0)
  ret %class.GIM_QUANTIZED_BVH_NODE_ARRAY* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI21BT_QUANTIZED_BVH_NODELj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorI21BT_QUANTIZED_BVH_NODELj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.BT_QUANTIZED_BVH_NODE* null, %struct.BT_QUANTIZED_BVH_NODE** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPrimitiveManagerBase* @_ZN22btPrimitiveManagerBaseC2Ev(%class.btPrimitiveManagerBase* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btPrimitiveManagerBase*, align 4
  store %class.btPrimitiveManagerBase* %this, %class.btPrimitiveManagerBase** %this.addr, align 4
  %this1 = load %class.btPrimitiveManagerBase*, %class.btPrimitiveManagerBase** %this.addr, align 4
  %0 = bitcast %class.btPrimitiveManagerBase* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [8 x i8*] }, { [8 x i8*] }* @_ZTV22btPrimitiveManagerBase, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btPrimitiveManagerBase* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManagerD0Ev(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %call = call %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* @_ZN22btGImpactMeshShapePart23TrimeshPrimitiveManagerD2Ev(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1) #8
  %0 = bitcast %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1 to i8*
  call void @_ZdlPv(i8* %0) #11
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10is_trimeshEv(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  ret i1 true
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_primitive_countEv(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %numfaces = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 12
  %0 = load i32, i32* %numfaces, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager17get_primitive_boxEiR6btAABB(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, i32 %prim_index, %class.btAABB* nonnull align 4 dereferenceable(32) %primbox) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  %prim_index.addr = alloca i32, align 4
  %primbox.addr = alloca %class.btAABB*, align 4
  %triangle = alloca %class.btPrimitiveTriangle, align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  store i32 %prim_index, i32* %prim_index.addr, align 4
  store %class.btAABB* %primbox, %class.btAABB** %primbox.addr, align 4
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %call = call %class.btPrimitiveTriangle* @_ZN19btPrimitiveTriangleC2Ev(%class.btPrimitiveTriangle* %triangle)
  %0 = load i32, i32* %prim_index.addr, align 4
  %1 = bitcast %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1 to void (%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, i32, %class.btPrimitiveTriangle*)***
  %vtable = load void (%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, i32, %class.btPrimitiveTriangle*)**, void (%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, i32, %class.btPrimitiveTriangle*)*** %1, align 4
  %vfn = getelementptr inbounds void (%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, i32, %class.btPrimitiveTriangle*)*, void (%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, i32, %class.btPrimitiveTriangle*)** %vtable, i64 5
  %2 = load void (%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, i32, %class.btPrimitiveTriangle*)*, void (%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, i32, %class.btPrimitiveTriangle*)** %vfn, align 4
  call void %2(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 %0, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %triangle)
  %3 = load %class.btAABB*, %class.btAABB** %primbox.addr, align 4
  %m_vertices = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %triangle, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices, i32 0, i32 0
  %m_vertices2 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %triangle, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices2, i32 0, i32 1
  %m_vertices4 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %triangle, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices4, i32 0, i32 2
  %m_margin = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %triangle, i32 0, i32 2
  %4 = load float, float* %m_margin, align 4
  call void @_ZN6btAABB25calc_from_triangle_marginI9btVector3EEvRKT_S4_S4_f(%class.btAABB* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5, float %4)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager22get_primitive_triangleEiR19btPrimitiveTriangle(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, i32 %prim_index, %class.btPrimitiveTriangle* nonnull align 4 dereferenceable(72) %triangle) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  %prim_index.addr = alloca i32, align 4
  %triangle.addr = alloca %class.btPrimitiveTriangle*, align 4
  %indices = alloca [3 x i32], align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  store i32 %prim_index, i32* %prim_index.addr, align 4
  store %class.btPrimitiveTriangle* %triangle, %class.btPrimitiveTriangle** %triangle.addr, align 4
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %0 = load i32, i32* %prim_index.addr, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %indices, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [3 x i32], [3 x i32]* %indices, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [3 x i32], [3 x i32]* %indices, i32 0, i32 2
  call void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager11get_indicesEiRjS1_S1_(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 %0, i32* nonnull align 4 dereferenceable(4) %arrayidx, i32* nonnull align 4 dereferenceable(4) %arrayidx2, i32* nonnull align 4 dereferenceable(4) %arrayidx3)
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %indices, i32 0, i32 0
  %1 = load i32, i32* %arrayidx4, align 4
  %2 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %triangle.addr, align 4
  %m_vertices = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %2, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices, i32 0, i32 0
  call void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEjR9btVector3(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 %1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5)
  %arrayidx6 = getelementptr inbounds [3 x i32], [3 x i32]* %indices, i32 0, i32 1
  %3 = load i32, i32* %arrayidx6, align 4
  %4 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %triangle.addr, align 4
  %m_vertices7 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %4, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices7, i32 0, i32 1
  call void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEjR9btVector3(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 %3, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8)
  %arrayidx9 = getelementptr inbounds [3 x i32], [3 x i32]* %indices, i32 0, i32 2
  %5 = load i32, i32* %arrayidx9, align 4
  %6 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %triangle.addr, align 4
  %m_vertices10 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %6, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices10, i32 0, i32 2
  call void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEjR9btVector3(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 %5, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx11)
  %m_margin = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 1
  %7 = load float, float* %m_margin, align 4
  %8 = load %class.btPrimitiveTriangle*, %class.btPrimitiveTriangle** %triangle.addr, align 4
  %m_margin12 = getelementptr inbounds %class.btPrimitiveTriangle, %class.btPrimitiveTriangle* %8, i32 0, i32 2
  store float %7, float* %m_margin12, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPrimitiveManagerBase* @_ZN22btPrimitiveManagerBaseD2Ev(%class.btPrimitiveManagerBase* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btPrimitiveManagerBase*, align 4
  store %class.btPrimitiveManagerBase* %this, %class.btPrimitiveManagerBase** %this.addr, align 4
  %this1 = load %class.btPrimitiveManagerBase*, %class.btPrimitiveManagerBase** %this.addr, align 4
  ret %class.btPrimitiveManagerBase* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN22btPrimitiveManagerBaseD0Ev(%class.btPrimitiveManagerBase* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btPrimitiveManagerBase*, align 4
  store %class.btPrimitiveManagerBase* %this, %class.btPrimitiveManagerBase** %this.addr, align 4
  %this1 = load %class.btPrimitiveManagerBase*, %class.btPrimitiveManagerBase** %this.addr, align 4
  call void @llvm.trap() #10
  unreachable
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #6

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN6btAABB25calc_from_triangle_marginI9btVector3EEvRKT_S4_S4_f(%class.btAABB* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %V1, %class.btVector3* nonnull align 4 dereferenceable(16) %V2, %class.btVector3* nonnull align 4 dereferenceable(16) %V3, float %margin) #2 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  %V1.addr = alloca %class.btVector3*, align 4
  %V2.addr = alloca %class.btVector3*, align 4
  %V3.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4
  store %class.btVector3* %V1, %class.btVector3** %V1.addr, align 4
  store %class.btVector3* %V2, %class.btVector3** %V2.addr, align 4
  store %class.btVector3* %V3, %class.btVector3** %V3.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %0)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call2 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %2)
  %arrayidx3 = getelementptr inbounds float, float* %call2, i32 0
  %3 = load float, float* %arrayidx3, align 4
  %4 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call4 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %4)
  %arrayidx5 = getelementptr inbounds float, float* %call4, i32 0
  %5 = load float, float* %arrayidx5, align 4
  %cmp = fcmp ogt float %3, %5
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %6 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %6)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 0
  %7 = load float, float* %arrayidx7, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %8 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call8 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %8)
  %arrayidx9 = getelementptr inbounds float, float* %call8, i32 0
  %9 = load float, float* %arrayidx9, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %7, %cond.true ], [ %9, %cond.false ]
  %cmp10 = fcmp ogt float %1, %cond
  br i1 %cmp10, label %cond.true11, label %cond.false25

cond.true11:                                      ; preds = %cond.end
  %10 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call12 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %10)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  %11 = load float, float* %arrayidx13, align 4
  %12 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call14 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %12)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 0
  %13 = load float, float* %arrayidx15, align 4
  %cmp16 = fcmp ogt float %11, %13
  br i1 %cmp16, label %cond.true17, label %cond.false20

cond.true17:                                      ; preds = %cond.true11
  %14 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %14)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 0
  %15 = load float, float* %arrayidx19, align 4
  br label %cond.end23

cond.false20:                                     ; preds = %cond.true11
  %16 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call21 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %16)
  %arrayidx22 = getelementptr inbounds float, float* %call21, i32 0
  %17 = load float, float* %arrayidx22, align 4
  br label %cond.end23

cond.end23:                                       ; preds = %cond.false20, %cond.true17
  %cond24 = phi float [ %15, %cond.true17 ], [ %17, %cond.false20 ]
  br label %cond.end28

cond.false25:                                     ; preds = %cond.end
  %18 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4
  %call26 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %18)
  %arrayidx27 = getelementptr inbounds float, float* %call26, i32 0
  %19 = load float, float* %arrayidx27, align 4
  br label %cond.end28

cond.end28:                                       ; preds = %cond.false25, %cond.end23
  %cond29 = phi float [ %cond24, %cond.end23 ], [ %19, %cond.false25 ]
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min)
  %arrayidx31 = getelementptr inbounds float, float* %call30, i32 0
  store float %cond29, float* %arrayidx31, align 4
  %20 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4
  %call32 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %20)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 1
  %21 = load float, float* %arrayidx33, align 4
  %22 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call34 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %22)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 1
  %23 = load float, float* %arrayidx35, align 4
  %24 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call36 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %24)
  %arrayidx37 = getelementptr inbounds float, float* %call36, i32 1
  %25 = load float, float* %arrayidx37, align 4
  %cmp38 = fcmp ogt float %23, %25
  br i1 %cmp38, label %cond.true39, label %cond.false42

cond.true39:                                      ; preds = %cond.end28
  %26 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call40 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %26)
  %arrayidx41 = getelementptr inbounds float, float* %call40, i32 1
  %27 = load float, float* %arrayidx41, align 4
  br label %cond.end45

cond.false42:                                     ; preds = %cond.end28
  %28 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %28)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 1
  %29 = load float, float* %arrayidx44, align 4
  br label %cond.end45

cond.end45:                                       ; preds = %cond.false42, %cond.true39
  %cond46 = phi float [ %27, %cond.true39 ], [ %29, %cond.false42 ]
  %cmp47 = fcmp ogt float %21, %cond46
  br i1 %cmp47, label %cond.true48, label %cond.false62

cond.true48:                                      ; preds = %cond.end45
  %30 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call49 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %30)
  %arrayidx50 = getelementptr inbounds float, float* %call49, i32 1
  %31 = load float, float* %arrayidx50, align 4
  %32 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call51 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %32)
  %arrayidx52 = getelementptr inbounds float, float* %call51, i32 1
  %33 = load float, float* %arrayidx52, align 4
  %cmp53 = fcmp ogt float %31, %33
  br i1 %cmp53, label %cond.true54, label %cond.false57

cond.true54:                                      ; preds = %cond.true48
  %34 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call55 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %34)
  %arrayidx56 = getelementptr inbounds float, float* %call55, i32 1
  %35 = load float, float* %arrayidx56, align 4
  br label %cond.end60

cond.false57:                                     ; preds = %cond.true48
  %36 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call58 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %36)
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 1
  %37 = load float, float* %arrayidx59, align 4
  br label %cond.end60

cond.end60:                                       ; preds = %cond.false57, %cond.true54
  %cond61 = phi float [ %35, %cond.true54 ], [ %37, %cond.false57 ]
  br label %cond.end65

cond.false62:                                     ; preds = %cond.end45
  %38 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4
  %call63 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %38)
  %arrayidx64 = getelementptr inbounds float, float* %call63, i32 1
  %39 = load float, float* %arrayidx64, align 4
  br label %cond.end65

cond.end65:                                       ; preds = %cond.false62, %cond.end60
  %cond66 = phi float [ %cond61, %cond.end60 ], [ %39, %cond.false62 ]
  %m_min67 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call68 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min67)
  %arrayidx69 = getelementptr inbounds float, float* %call68, i32 1
  store float %cond66, float* %arrayidx69, align 4
  %40 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4
  %call70 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %40)
  %arrayidx71 = getelementptr inbounds float, float* %call70, i32 2
  %41 = load float, float* %arrayidx71, align 4
  %42 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call72 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %42)
  %arrayidx73 = getelementptr inbounds float, float* %call72, i32 2
  %43 = load float, float* %arrayidx73, align 4
  %44 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call74 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %44)
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 2
  %45 = load float, float* %arrayidx75, align 4
  %cmp76 = fcmp ogt float %43, %45
  br i1 %cmp76, label %cond.true77, label %cond.false80

cond.true77:                                      ; preds = %cond.end65
  %46 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call78 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %46)
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 2
  %47 = load float, float* %arrayidx79, align 4
  br label %cond.end83

cond.false80:                                     ; preds = %cond.end65
  %48 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call81 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %48)
  %arrayidx82 = getelementptr inbounds float, float* %call81, i32 2
  %49 = load float, float* %arrayidx82, align 4
  br label %cond.end83

cond.end83:                                       ; preds = %cond.false80, %cond.true77
  %cond84 = phi float [ %47, %cond.true77 ], [ %49, %cond.false80 ]
  %cmp85 = fcmp ogt float %41, %cond84
  br i1 %cmp85, label %cond.true86, label %cond.false100

cond.true86:                                      ; preds = %cond.end83
  %50 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call87 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %50)
  %arrayidx88 = getelementptr inbounds float, float* %call87, i32 2
  %51 = load float, float* %arrayidx88, align 4
  %52 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call89 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %52)
  %arrayidx90 = getelementptr inbounds float, float* %call89, i32 2
  %53 = load float, float* %arrayidx90, align 4
  %cmp91 = fcmp ogt float %51, %53
  br i1 %cmp91, label %cond.true92, label %cond.false95

cond.true92:                                      ; preds = %cond.true86
  %54 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call93 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %54)
  %arrayidx94 = getelementptr inbounds float, float* %call93, i32 2
  %55 = load float, float* %arrayidx94, align 4
  br label %cond.end98

cond.false95:                                     ; preds = %cond.true86
  %56 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call96 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %56)
  %arrayidx97 = getelementptr inbounds float, float* %call96, i32 2
  %57 = load float, float* %arrayidx97, align 4
  br label %cond.end98

cond.end98:                                       ; preds = %cond.false95, %cond.true92
  %cond99 = phi float [ %55, %cond.true92 ], [ %57, %cond.false95 ]
  br label %cond.end103

cond.false100:                                    ; preds = %cond.end83
  %58 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4
  %call101 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %58)
  %arrayidx102 = getelementptr inbounds float, float* %call101, i32 2
  %59 = load float, float* %arrayidx102, align 4
  br label %cond.end103

cond.end103:                                      ; preds = %cond.false100, %cond.end98
  %cond104 = phi float [ %cond99, %cond.end98 ], [ %59, %cond.false100 ]
  %m_min105 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call106 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min105)
  %arrayidx107 = getelementptr inbounds float, float* %call106, i32 2
  store float %cond104, float* %arrayidx107, align 4
  %60 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4
  %call108 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %60)
  %arrayidx109 = getelementptr inbounds float, float* %call108, i32 0
  %61 = load float, float* %arrayidx109, align 4
  %62 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call110 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %62)
  %arrayidx111 = getelementptr inbounds float, float* %call110, i32 0
  %63 = load float, float* %arrayidx111, align 4
  %64 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call112 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %64)
  %arrayidx113 = getelementptr inbounds float, float* %call112, i32 0
  %65 = load float, float* %arrayidx113, align 4
  %cmp114 = fcmp olt float %63, %65
  br i1 %cmp114, label %cond.true115, label %cond.false118

cond.true115:                                     ; preds = %cond.end103
  %66 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call116 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %66)
  %arrayidx117 = getelementptr inbounds float, float* %call116, i32 0
  %67 = load float, float* %arrayidx117, align 4
  br label %cond.end121

cond.false118:                                    ; preds = %cond.end103
  %68 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call119 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %68)
  %arrayidx120 = getelementptr inbounds float, float* %call119, i32 0
  %69 = load float, float* %arrayidx120, align 4
  br label %cond.end121

cond.end121:                                      ; preds = %cond.false118, %cond.true115
  %cond122 = phi float [ %67, %cond.true115 ], [ %69, %cond.false118 ]
  %cmp123 = fcmp olt float %61, %cond122
  br i1 %cmp123, label %cond.true124, label %cond.false138

cond.true124:                                     ; preds = %cond.end121
  %70 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call125 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %70)
  %arrayidx126 = getelementptr inbounds float, float* %call125, i32 0
  %71 = load float, float* %arrayidx126, align 4
  %72 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call127 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %72)
  %arrayidx128 = getelementptr inbounds float, float* %call127, i32 0
  %73 = load float, float* %arrayidx128, align 4
  %cmp129 = fcmp olt float %71, %73
  br i1 %cmp129, label %cond.true130, label %cond.false133

cond.true130:                                     ; preds = %cond.true124
  %74 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call131 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %74)
  %arrayidx132 = getelementptr inbounds float, float* %call131, i32 0
  %75 = load float, float* %arrayidx132, align 4
  br label %cond.end136

cond.false133:                                    ; preds = %cond.true124
  %76 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call134 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %76)
  %arrayidx135 = getelementptr inbounds float, float* %call134, i32 0
  %77 = load float, float* %arrayidx135, align 4
  br label %cond.end136

cond.end136:                                      ; preds = %cond.false133, %cond.true130
  %cond137 = phi float [ %75, %cond.true130 ], [ %77, %cond.false133 ]
  br label %cond.end141

cond.false138:                                    ; preds = %cond.end121
  %78 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4
  %call139 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %78)
  %arrayidx140 = getelementptr inbounds float, float* %call139, i32 0
  %79 = load float, float* %arrayidx140, align 4
  br label %cond.end141

cond.end141:                                      ; preds = %cond.false138, %cond.end136
  %cond142 = phi float [ %cond137, %cond.end136 ], [ %79, %cond.false138 ]
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call143 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max)
  %arrayidx144 = getelementptr inbounds float, float* %call143, i32 0
  store float %cond142, float* %arrayidx144, align 4
  %80 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4
  %call145 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %80)
  %arrayidx146 = getelementptr inbounds float, float* %call145, i32 1
  %81 = load float, float* %arrayidx146, align 4
  %82 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call147 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %82)
  %arrayidx148 = getelementptr inbounds float, float* %call147, i32 1
  %83 = load float, float* %arrayidx148, align 4
  %84 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call149 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %84)
  %arrayidx150 = getelementptr inbounds float, float* %call149, i32 1
  %85 = load float, float* %arrayidx150, align 4
  %cmp151 = fcmp olt float %83, %85
  br i1 %cmp151, label %cond.true152, label %cond.false155

cond.true152:                                     ; preds = %cond.end141
  %86 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call153 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %86)
  %arrayidx154 = getelementptr inbounds float, float* %call153, i32 1
  %87 = load float, float* %arrayidx154, align 4
  br label %cond.end158

cond.false155:                                    ; preds = %cond.end141
  %88 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call156 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %88)
  %arrayidx157 = getelementptr inbounds float, float* %call156, i32 1
  %89 = load float, float* %arrayidx157, align 4
  br label %cond.end158

cond.end158:                                      ; preds = %cond.false155, %cond.true152
  %cond159 = phi float [ %87, %cond.true152 ], [ %89, %cond.false155 ]
  %cmp160 = fcmp olt float %81, %cond159
  br i1 %cmp160, label %cond.true161, label %cond.false175

cond.true161:                                     ; preds = %cond.end158
  %90 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call162 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %90)
  %arrayidx163 = getelementptr inbounds float, float* %call162, i32 1
  %91 = load float, float* %arrayidx163, align 4
  %92 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call164 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %92)
  %arrayidx165 = getelementptr inbounds float, float* %call164, i32 1
  %93 = load float, float* %arrayidx165, align 4
  %cmp166 = fcmp olt float %91, %93
  br i1 %cmp166, label %cond.true167, label %cond.false170

cond.true167:                                     ; preds = %cond.true161
  %94 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call168 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %94)
  %arrayidx169 = getelementptr inbounds float, float* %call168, i32 1
  %95 = load float, float* %arrayidx169, align 4
  br label %cond.end173

cond.false170:                                    ; preds = %cond.true161
  %96 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call171 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %96)
  %arrayidx172 = getelementptr inbounds float, float* %call171, i32 1
  %97 = load float, float* %arrayidx172, align 4
  br label %cond.end173

cond.end173:                                      ; preds = %cond.false170, %cond.true167
  %cond174 = phi float [ %95, %cond.true167 ], [ %97, %cond.false170 ]
  br label %cond.end178

cond.false175:                                    ; preds = %cond.end158
  %98 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4
  %call176 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %98)
  %arrayidx177 = getelementptr inbounds float, float* %call176, i32 1
  %99 = load float, float* %arrayidx177, align 4
  br label %cond.end178

cond.end178:                                      ; preds = %cond.false175, %cond.end173
  %cond179 = phi float [ %cond174, %cond.end173 ], [ %99, %cond.false175 ]
  %m_max180 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call181 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max180)
  %arrayidx182 = getelementptr inbounds float, float* %call181, i32 1
  store float %cond179, float* %arrayidx182, align 4
  %100 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4
  %call183 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %100)
  %arrayidx184 = getelementptr inbounds float, float* %call183, i32 2
  %101 = load float, float* %arrayidx184, align 4
  %102 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call185 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %102)
  %arrayidx186 = getelementptr inbounds float, float* %call185, i32 2
  %103 = load float, float* %arrayidx186, align 4
  %104 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call187 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %104)
  %arrayidx188 = getelementptr inbounds float, float* %call187, i32 2
  %105 = load float, float* %arrayidx188, align 4
  %cmp189 = fcmp olt float %103, %105
  br i1 %cmp189, label %cond.true190, label %cond.false193

cond.true190:                                     ; preds = %cond.end178
  %106 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call191 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %106)
  %arrayidx192 = getelementptr inbounds float, float* %call191, i32 2
  %107 = load float, float* %arrayidx192, align 4
  br label %cond.end196

cond.false193:                                    ; preds = %cond.end178
  %108 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call194 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %108)
  %arrayidx195 = getelementptr inbounds float, float* %call194, i32 2
  %109 = load float, float* %arrayidx195, align 4
  br label %cond.end196

cond.end196:                                      ; preds = %cond.false193, %cond.true190
  %cond197 = phi float [ %107, %cond.true190 ], [ %109, %cond.false193 ]
  %cmp198 = fcmp olt float %101, %cond197
  br i1 %cmp198, label %cond.true199, label %cond.false213

cond.true199:                                     ; preds = %cond.end196
  %110 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call200 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %110)
  %arrayidx201 = getelementptr inbounds float, float* %call200, i32 2
  %111 = load float, float* %arrayidx201, align 4
  %112 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call202 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %112)
  %arrayidx203 = getelementptr inbounds float, float* %call202, i32 2
  %113 = load float, float* %arrayidx203, align 4
  %cmp204 = fcmp olt float %111, %113
  br i1 %cmp204, label %cond.true205, label %cond.false208

cond.true205:                                     ; preds = %cond.true199
  %114 = load %class.btVector3*, %class.btVector3** %V3.addr, align 4
  %call206 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %114)
  %arrayidx207 = getelementptr inbounds float, float* %call206, i32 2
  %115 = load float, float* %arrayidx207, align 4
  br label %cond.end211

cond.false208:                                    ; preds = %cond.true199
  %116 = load %class.btVector3*, %class.btVector3** %V2.addr, align 4
  %call209 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %116)
  %arrayidx210 = getelementptr inbounds float, float* %call209, i32 2
  %117 = load float, float* %arrayidx210, align 4
  br label %cond.end211

cond.end211:                                      ; preds = %cond.false208, %cond.true205
  %cond212 = phi float [ %115, %cond.true205 ], [ %117, %cond.false208 ]
  br label %cond.end216

cond.false213:                                    ; preds = %cond.end196
  %118 = load %class.btVector3*, %class.btVector3** %V1.addr, align 4
  %call214 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %118)
  %arrayidx215 = getelementptr inbounds float, float* %call214, i32 2
  %119 = load float, float* %arrayidx215, align 4
  br label %cond.end216

cond.end216:                                      ; preds = %cond.false213, %cond.end211
  %cond217 = phi float [ %cond212, %cond.end211 ], [ %119, %cond.false213 ]
  %m_max218 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call219 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max218)
  %arrayidx220 = getelementptr inbounds float, float* %call219, i32 2
  store float %cond217, float* %arrayidx220, align 4
  %120 = load float, float* %margin.addr, align 4
  %m_min221 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call222 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min221)
  %arrayidx223 = getelementptr inbounds float, float* %call222, i32 0
  %121 = load float, float* %arrayidx223, align 4
  %sub = fsub float %121, %120
  store float %sub, float* %arrayidx223, align 4
  %122 = load float, float* %margin.addr, align 4
  %m_min224 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call225 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min224)
  %arrayidx226 = getelementptr inbounds float, float* %call225, i32 1
  %123 = load float, float* %arrayidx226, align 4
  %sub227 = fsub float %123, %122
  store float %sub227, float* %arrayidx226, align 4
  %124 = load float, float* %margin.addr, align 4
  %m_min228 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call229 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min228)
  %arrayidx230 = getelementptr inbounds float, float* %call229, i32 2
  %125 = load float, float* %arrayidx230, align 4
  %sub231 = fsub float %125, %124
  store float %sub231, float* %arrayidx230, align 4
  %126 = load float, float* %margin.addr, align 4
  %m_max232 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call233 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max232)
  %arrayidx234 = getelementptr inbounds float, float* %call233, i32 0
  %127 = load float, float* %arrayidx234, align 4
  %add = fadd float %127, %126
  store float %add, float* %arrayidx234, align 4
  %128 = load float, float* %margin.addr, align 4
  %m_max235 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call236 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max235)
  %arrayidx237 = getelementptr inbounds float, float* %call236, i32 1
  %129 = load float, float* %arrayidx237, align 4
  %add238 = fadd float %129, %128
  store float %add238, float* %arrayidx237, align 4
  %130 = load float, float* %margin.addr, align 4
  %m_max239 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call240 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max239)
  %arrayidx241 = getelementptr inbounds float, float* %call240, i32 2
  %131 = load float, float* %arrayidx241, align 4
  %add242 = fadd float %131, %130
  store float %add242, float* %arrayidx241, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager11get_indicesEiRjS1_S1_(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, i32 %face_index, i32* nonnull align 4 dereferenceable(4) %i0, i32* nonnull align 4 dereferenceable(4) %i1, i32* nonnull align 4 dereferenceable(4) %i2) #1 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  %face_index.addr = alloca i32, align 4
  %i0.addr = alloca i32*, align 4
  %i1.addr = alloca i32*, align 4
  %i2.addr = alloca i32*, align 4
  %s_indices = alloca i16*, align 4
  %i_indices = alloca i32*, align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  store i32 %face_index, i32* %face_index.addr, align 4
  store i32* %i0, i32** %i0.addr, align 4
  store i32* %i1, i32** %i1.addr, align 4
  store i32* %i2, i32** %i2.addr, align 4
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %indicestype = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 13
  %0 = load i32, i32* %indicestype, align 4
  %cmp = icmp eq i32 %0, 3
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %indexbase = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 10
  %1 = load i8*, i8** %indexbase, align 4
  %2 = load i32, i32* %face_index.addr, align 4
  %indexstride = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 11
  %3 = load i32, i32* %indexstride, align 4
  %mul = mul nsw i32 %2, %3
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %mul
  %4 = bitcast i8* %add.ptr to i16*
  store i16* %4, i16** %s_indices, align 4
  %5 = load i16*, i16** %s_indices, align 4
  %arrayidx = getelementptr inbounds i16, i16* %5, i32 0
  %6 = load i16, i16* %arrayidx, align 2
  %conv = zext i16 %6 to i32
  %7 = load i32*, i32** %i0.addr, align 4
  store i32 %conv, i32* %7, align 4
  %8 = load i16*, i16** %s_indices, align 4
  %arrayidx2 = getelementptr inbounds i16, i16* %8, i32 1
  %9 = load i16, i16* %arrayidx2, align 2
  %conv3 = zext i16 %9 to i32
  %10 = load i32*, i32** %i1.addr, align 4
  store i32 %conv3, i32* %10, align 4
  %11 = load i16*, i16** %s_indices, align 4
  %arrayidx4 = getelementptr inbounds i16, i16* %11, i32 2
  %12 = load i16, i16* %arrayidx4, align 2
  %conv5 = zext i16 %12 to i32
  %13 = load i32*, i32** %i2.addr, align 4
  store i32 %conv5, i32* %13, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %indexbase6 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 10
  %14 = load i8*, i8** %indexbase6, align 4
  %15 = load i32, i32* %face_index.addr, align 4
  %indexstride7 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 11
  %16 = load i32, i32* %indexstride7, align 4
  %mul8 = mul nsw i32 %15, %16
  %add.ptr9 = getelementptr inbounds i8, i8* %14, i32 %mul8
  %17 = bitcast i8* %add.ptr9 to i32*
  store i32* %17, i32** %i_indices, align 4
  %18 = load i32*, i32** %i_indices, align 4
  %arrayidx10 = getelementptr inbounds i32, i32* %18, i32 0
  %19 = load i32, i32* %arrayidx10, align 4
  %20 = load i32*, i32** %i0.addr, align 4
  store i32 %19, i32* %20, align 4
  %21 = load i32*, i32** %i_indices, align 4
  %arrayidx11 = getelementptr inbounds i32, i32* %21, i32 1
  %22 = load i32, i32* %arrayidx11, align 4
  %23 = load i32*, i32** %i1.addr, align 4
  store i32 %22, i32* %23, align 4
  %24 = load i32*, i32** %i_indices, align 4
  %arrayidx12 = getelementptr inbounds i32, i32* %24, i32 2
  %25 = load i32, i32* %arrayidx12, align 4
  %26 = load i32*, i32** %i2.addr, align 4
  store i32 %25, i32* %26, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEjR9btVector3(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, i32 %vertex_index, %class.btVector3* nonnull align 4 dereferenceable(16) %vertex) #1 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  %vertex_index.addr = alloca i32, align 4
  %vertex.addr = alloca %class.btVector3*, align 4
  %dvertices = alloca double*, align 4
  %svertices = alloca float*, align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  store i32 %vertex_index, i32* %vertex_index.addr, align 4
  store %class.btVector3* %vertex, %class.btVector3** %vertex.addr, align 4
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %type = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 8
  %0 = load i32, i32* %type, align 4
  %cmp = icmp eq i32 %0, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %vertexbase = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 6
  %1 = load i8*, i8** %vertexbase, align 4
  %2 = load i32, i32* %vertex_index.addr, align 4
  %stride = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 9
  %3 = load i32, i32* %stride, align 4
  %mul = mul i32 %2, %3
  %add.ptr = getelementptr inbounds i8, i8* %1, i32 %mul
  %4 = bitcast i8* %add.ptr to double*
  store double* %4, double** %dvertices, align 4
  %5 = load double*, double** %dvertices, align 4
  %arrayidx = getelementptr inbounds double, double* %5, i32 0
  %6 = load double, double* %arrayidx, align 8
  %m_scale = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 3
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_scale)
  %arrayidx2 = getelementptr inbounds float, float* %call, i32 0
  %7 = load float, float* %arrayidx2, align 4
  %conv = fpext float %7 to double
  %mul3 = fmul double %6, %conv
  %conv4 = fptrunc double %mul3 to float
  %8 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4
  %call5 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %8)
  %arrayidx6 = getelementptr inbounds float, float* %call5, i32 0
  store float %conv4, float* %arrayidx6, align 4
  %9 = load double*, double** %dvertices, align 4
  %arrayidx7 = getelementptr inbounds double, double* %9, i32 1
  %10 = load double, double* %arrayidx7, align 8
  %m_scale8 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 3
  %call9 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_scale8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 1
  %11 = load float, float* %arrayidx10, align 4
  %conv11 = fpext float %11 to double
  %mul12 = fmul double %10, %conv11
  %conv13 = fptrunc double %mul12 to float
  %12 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4
  %call14 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %12)
  %arrayidx15 = getelementptr inbounds float, float* %call14, i32 1
  store float %conv13, float* %arrayidx15, align 4
  %13 = load double*, double** %dvertices, align 4
  %arrayidx16 = getelementptr inbounds double, double* %13, i32 2
  %14 = load double, double* %arrayidx16, align 8
  %m_scale17 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 3
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_scale17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 2
  %15 = load float, float* %arrayidx19, align 4
  %conv20 = fpext float %15 to double
  %mul21 = fmul double %14, %conv20
  %conv22 = fptrunc double %mul21 to float
  %16 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4
  %call23 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %16)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 2
  store float %conv22, float* %arrayidx24, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %vertexbase25 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 6
  %17 = load i8*, i8** %vertexbase25, align 4
  %18 = load i32, i32* %vertex_index.addr, align 4
  %stride26 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 9
  %19 = load i32, i32* %stride26, align 4
  %mul27 = mul i32 %18, %19
  %add.ptr28 = getelementptr inbounds i8, i8* %17, i32 %mul27
  %20 = bitcast i8* %add.ptr28 to float*
  store float* %20, float** %svertices, align 4
  %21 = load float*, float** %svertices, align 4
  %arrayidx29 = getelementptr inbounds float, float* %21, i32 0
  %22 = load float, float* %arrayidx29, align 4
  %m_scale30 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 3
  %call31 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_scale30)
  %arrayidx32 = getelementptr inbounds float, float* %call31, i32 0
  %23 = load float, float* %arrayidx32, align 4
  %mul33 = fmul float %22, %23
  %24 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4
  %call34 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %24)
  %arrayidx35 = getelementptr inbounds float, float* %call34, i32 0
  store float %mul33, float* %arrayidx35, align 4
  %25 = load float*, float** %svertices, align 4
  %arrayidx36 = getelementptr inbounds float, float* %25, i32 1
  %26 = load float, float* %arrayidx36, align 4
  %m_scale37 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 3
  %call38 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_scale37)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 1
  %27 = load float, float* %arrayidx39, align 4
  %mul40 = fmul float %26, %27
  %28 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4
  %call41 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %28)
  %arrayidx42 = getelementptr inbounds float, float* %call41, i32 1
  store float %mul40, float* %arrayidx42, align 4
  %29 = load float*, float** %svertices, align 4
  %arrayidx43 = getelementptr inbounds float, float* %29, i32 2
  %30 = load float, float* %arrayidx43, align 4
  %m_scale44 = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 3
  %call45 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_scale44)
  %arrayidx46 = getelementptr inbounds float, float* %call45, i32 2
  %31 = load float, float* %arrayidx46, align 4
  %mul47 = fmul float %30, %31
  %32 = load %class.btVector3*, %class.btVector3** %vertex.addr, align 4
  %call48 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %32)
  %arrayidx49 = getelementptr inbounds float, float* %call48, i32 2
  store float %mul47, float* %arrayidx49, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btGImpactQuantizedBvh* @_ZN21btGImpactQuantizedBvhD2Ev(%class.btGImpactQuantizedBvh* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactQuantizedBvh*, align 4
  store %class.btGImpactQuantizedBvh* %this, %class.btGImpactQuantizedBvh** %this.addr, align 4
  %this1 = load %class.btGImpactQuantizedBvh*, %class.btGImpactQuantizedBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactQuantizedBvh, %class.btGImpactQuantizedBvh* %this1, i32 0, i32 0
  %call = call %class.btQuantizedBvhTree* @_ZN18btQuantizedBvhTreeD2Ev(%class.btQuantizedBvhTree* %m_box_tree) #8
  ret %class.btGImpactQuantizedBvh* %this1
}

; Function Attrs: nounwind
declare %class.btConcaveShape* @_ZN14btConcaveShapeD2Ev(%class.btConcaveShape* returned) unnamed_addr #7

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btQuantizedBvhTree* @_ZN18btQuantizedBvhTreeD2Ev(%class.btQuantizedBvhTree* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvhTree*, align 4
  store %class.btQuantizedBvhTree* %this, %class.btQuantizedBvhTree** %this.addr, align 4
  %this1 = load %class.btQuantizedBvhTree*, %class.btQuantizedBvhTree** %this.addr, align 4
  %m_node_array = getelementptr inbounds %class.btQuantizedBvhTree, %class.btQuantizedBvhTree* %this1, i32 0, i32 1
  %call = call %class.GIM_QUANTIZED_BVH_NODE_ARRAY* @_ZN28GIM_QUANTIZED_BVH_NODE_ARRAYD2Ev(%class.GIM_QUANTIZED_BVH_NODE_ARRAY* %m_node_array) #8
  ret %class.btQuantizedBvhTree* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.GIM_QUANTIZED_BVH_NODE_ARRAY* @_ZN28GIM_QUANTIZED_BVH_NODE_ARRAYD2Ev(%class.GIM_QUANTIZED_BVH_NODE_ARRAY* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.GIM_QUANTIZED_BVH_NODE_ARRAY*, align 4
  store %class.GIM_QUANTIZED_BVH_NODE_ARRAY* %this, %class.GIM_QUANTIZED_BVH_NODE_ARRAY** %this.addr, align 4
  %this1 = load %class.GIM_QUANTIZED_BVH_NODE_ARRAY*, %class.GIM_QUANTIZED_BVH_NODE_ARRAY** %this.addr, align 4
  %0 = bitcast %class.GIM_QUANTIZED_BVH_NODE_ARRAY* %this1 to %class.btAlignedObjectArray*
  %call = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEED2Ev(%class.btAlignedObjectArray* %0) #8
  ret %class.GIM_QUANTIZED_BVH_NODE_ARRAY* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %struct.BT_QUANTIZED_BVH_NODE*, %struct.BT_QUANTIZED_BVH_NODE** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %struct.BT_QUANTIZED_BVH_NODE, %struct.BT_QUANTIZED_BVH_NODE* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.BT_QUANTIZED_BVH_NODE*, %struct.BT_QUANTIZED_BVH_NODE** %m_data, align 4
  %tobool = icmp ne %struct.BT_QUANTIZED_BVH_NODE* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %struct.BT_QUANTIZED_BVH_NODE*, %struct.BT_QUANTIZED_BVH_NODE** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI21BT_QUANTIZED_BVH_NODELj16EE10deallocateEPS0_(%class.btAlignedAllocator* %m_allocator, %struct.BT_QUANTIZED_BVH_NODE* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %struct.BT_QUANTIZED_BVH_NODE* null, %struct.BT_QUANTIZED_BVH_NODE** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI21BT_QUANTIZED_BVH_NODELj16EE10deallocateEPS0_(%class.btAlignedAllocator* %this, %struct.BT_QUANTIZED_BVH_NODE* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %struct.BT_QUANTIZED_BVH_NODE*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %struct.BT_QUANTIZED_BVH_NODE* %ptr, %struct.BT_QUANTIZED_BVH_NODE** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %struct.BT_QUANTIZED_BVH_NODE*, %struct.BT_QUANTIZED_BVH_NODE** %ptr.addr, align 4
  %1 = bitcast %struct.BT_QUANTIZED_BVH_NODE* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m1, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m2) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %m1.addr = alloca %class.btMatrix3x3*, align 4
  %m2.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp14 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %ref.tmp20 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %m1, %class.btMatrix3x3** %m1.addr, align 4
  store %class.btMatrix3x3* %m2, %class.btMatrix3x3** %m2.addr, align 4
  %1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %2, i32 0)
  %call1 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %1, %class.btVector3* nonnull align 4 dereferenceable(16) %call)
  store float %call1, float* %ref.tmp, align 4
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %4, i32 0)
  %call4 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %3, %class.btVector3* nonnull align 4 dereferenceable(16) %call3)
  store float %call4, float* %ref.tmp2, align 4
  %5 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %6, i32 0)
  %call7 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %call6)
  store float %call7, float* %ref.tmp5, align 4
  %7 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %8 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %8, i32 1)
  %call10 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %7, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  store float %call10, float* %ref.tmp8, align 4
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %10 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %10, i32 1)
  %call13 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %9, %class.btVector3* nonnull align 4 dereferenceable(16) %call12)
  store float %call13, float* %ref.tmp11, align 4
  %11 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %12 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call15 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %12, i32 1)
  %call16 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %11, %class.btVector3* nonnull align 4 dereferenceable(16) %call15)
  store float %call16, float* %ref.tmp14, align 4
  %13 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %14 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %14, i32 2)
  %call19 = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %13, %class.btVector3* nonnull align 4 dereferenceable(16) %call18)
  store float %call19, float* %ref.tmp17, align 4
  %15 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %16 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call21 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %16, i32 2)
  %call22 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %15, %class.btVector3* nonnull align 4 dereferenceable(16) %call21)
  store float %call22, float* %ref.tmp20, align 4
  %17 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m2.addr, align 4
  %18 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m1.addr, align 4
  %call24 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %18, i32 2)
  %call25 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %17, %class.btVector3* nonnull align 4 dereferenceable(16) %call24)
  store float %call25, float* %ref.tmp23, align 4
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp14, float* nonnull align 4 dereferenceable(4) %ref.tmp17, float* nonnull align 4 dereferenceable(4) %ref.tmp20, float* nonnull align 4 dereferenceable(4) %ref.tmp23)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x36scaledERK9btVector3(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %s) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %s.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp15 = alloca float, align 4
  %ref.tmp21 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp39 = alloca float, align 4
  %ref.tmp45 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %s, %class.btVector3** %s.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4
  %2 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %2)
  %3 = load float, float* %call2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %4 = load float, float* %call6, align 4
  %5 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %5)
  %6 = load float, float* %call7, align 4
  %mul8 = fmul float %4, %6
  store float %mul8, float* %ref.tmp3, align 4
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 0
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx11)
  %7 = load float, float* %call12, align 4
  %8 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %8)
  %9 = load float, float* %call13, align 4
  %mul14 = fmul float %7, %9
  store float %mul14, float* %ref.tmp9, align 4
  %m_el16 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx17 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el16, i32 0, i32 1
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx17)
  %10 = load float, float* %call18, align 4
  %11 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %11)
  %12 = load float, float* %call19, align 4
  %mul20 = fmul float %10, %12
  store float %mul20, float* %ref.tmp15, align 4
  %m_el22 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx23 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el22, i32 0, i32 1
  %call24 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx23)
  %13 = load float, float* %call24, align 4
  %14 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %14)
  %15 = load float, float* %call25, align 4
  %mul26 = fmul float %13, %15
  store float %mul26, float* %ref.tmp21, align 4
  %m_el28 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx29 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el28, i32 0, i32 1
  %call30 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx29)
  %16 = load float, float* %call30, align 4
  %17 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %17)
  %18 = load float, float* %call31, align 4
  %mul32 = fmul float %16, %18
  store float %mul32, float* %ref.tmp27, align 4
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx35)
  %19 = load float, float* %call36, align 4
  %20 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call37 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %20)
  %21 = load float, float* %call37, align 4
  %mul38 = fmul float %19, %21
  store float %mul38, float* %ref.tmp33, align 4
  %m_el40 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx41 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el40, i32 0, i32 2
  %call42 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx41)
  %22 = load float, float* %call42, align 4
  %23 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %23)
  %24 = load float, float* %call43, align 4
  %mul44 = fmul float %22, %24
  store float %mul44, float* %ref.tmp39, align 4
  %m_el46 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx47 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el46, i32 0, i32 2
  %call48 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx47)
  %25 = load float, float* %call48, align 4
  %26 = load %class.btVector3*, %class.btVector3** %s.addr, align 4
  %call49 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %26)
  %27 = load float, float* %call49, align 4
  %mul50 = fmul float %25, %27
  store float %mul50, float* %ref.tmp45, align 4
  %call51 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp15, float* nonnull align 4 dereferenceable(4) %ref.tmp21, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp39, float* nonnull align 4 dereferenceable(4) %ref.tmp45)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x39transposeEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx3)
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 2
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx6)
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 0
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 1
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx12)
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 2
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx15)
  %m_el17 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx18 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el17, i32 0, i32 0
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx18)
  %m_el20 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx21 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el20, i32 0, i32 1
  %call22 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx21)
  %m_el23 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx24 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el23, i32 0, i32 2
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx24)
  %call26 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %call, float* nonnull align 4 dereferenceable(4) %call4, float* nonnull align 4 dereferenceable(4) %call7, float* nonnull align 4 dereferenceable(4) %call10, float* nonnull align 4 dereferenceable(4) %call13, float* nonnull align 4 dereferenceable(4) %call16, float* nonnull align 4 dereferenceable(4) %call19, float* nonnull align 4 dereferenceable(4) %call22, float* nonnull align 4 dereferenceable(4) %call25)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: nounwind
declare i32 @__cxa_guard_acquire(i32*) #8

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x311getIdentityEv() #2 comdat {
entry:
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %0 = load atomic i8, i8* bitcast (i32* @_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix to i8*) acquire, align 4
  %1 = and i8 %0, 1
  %guard.uninitialized = icmp eq i8 %1, 0
  br i1 %guard.uninitialized, label %init.check, label %init.end, !prof !2

init.check:                                       ; preds = %entry
  %2 = call i32 @__cxa_guard_acquire(i32* @_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix) #8
  %tobool = icmp ne i32 %2, 0
  br i1 %tobool, label %init, label %init.end

init:                                             ; preds = %init.check
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp1, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  store float 1.000000e+00, float* %ref.tmp4, align 4
  store float 0.000000e+00, float* %ref.tmp5, align 4
  store float 0.000000e+00, float* %ref.tmp6, align 4
  store float 0.000000e+00, float* %ref.tmp7, align 4
  store float 1.000000e+00, float* %ref.tmp8, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* @_ZZN11btMatrix3x311getIdentityEvE14identityMatrix, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp4, float* nonnull align 4 dereferenceable(4) %ref.tmp5, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, float* nonnull align 4 dereferenceable(4) %ref.tmp8)
  call void @__cxa_guard_release(i32* @_ZGVZN11btMatrix3x311getIdentityEvE14identityMatrix) #8
  br label %init.end

init.end:                                         ; preds = %init, %init.check, %entry
  ret %class.btMatrix3x3* @_ZZN11btMatrix3x311getIdentityEvE14identityMatrix
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %b, %class.btVector3* nonnull align 4 dereferenceable(16) %c) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %b.addr = alloca %class.btMatrix3x3*, align 4
  %c.addr = alloca %class.btVector3*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btMatrix3x3* %b, %class.btMatrix3x3** %b.addr, align 4
  store %class.btVector3* %c, %class.btVector3** %c.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %b.addr, align 4
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %0)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btVector3*, %class.btVector3** %c.addr, align 4
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: nounwind
declare void @__cxa_guard_release(i32*) #8

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* returned %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx4 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el5 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el5, i32 0, i32 1
  %m_el7 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el7, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx8 to i8*
  %5 = bitcast %class.btVector3* %arrayidx6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 2
  %m_el11 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el11, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx12 to i8*
  %8 = bitcast %class.btVector3* %arrayidx10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager16get_vertex_countEv(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this) #1 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %numverts = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 7
  %0 = load i32, i32* %numverts, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #9

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector4* @_ZN9btVector4C2Ev(%class.btVector4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector4*, align 4
  store %class.btVector4* %this, %class.btVector4** %this.addr, align 4
  %this1 = load %class.btVector4*, %class.btVector4** %this.addr, align 4
  %0 = bitcast %class.btVector4* %this1 to %class.btVector3*
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %0)
  ret %class.btVector4* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.4* @_ZN20btAlignedObjectArrayIP16btCollisionShapeED2Ev(%class.btAlignedObjectArray.4* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP16btCollisionShapeE5clearEv(%class.btAlignedObjectArray.4* %this1)
  ret %class.btAlignedObjectArray.4* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayI11btTransformED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayI11btTransformE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"class.btGImpactCompoundShape::CompoundPrimitiveManager"* @_ZN22btGImpactCompoundShape24CompoundPrimitiveManagerD2Ev(%"class.btGImpactCompoundShape::CompoundPrimitiveManager"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"class.btGImpactCompoundShape::CompoundPrimitiveManager"*, align 4
  store %"class.btGImpactCompoundShape::CompoundPrimitiveManager"* %this, %"class.btGImpactCompoundShape::CompoundPrimitiveManager"** %this.addr, align 4
  %this1 = load %"class.btGImpactCompoundShape::CompoundPrimitiveManager"*, %"class.btGImpactCompoundShape::CompoundPrimitiveManager"** %this.addr, align 4
  %0 = bitcast %"class.btGImpactCompoundShape::CompoundPrimitiveManager"* %this1 to %class.btPrimitiveManagerBase*
  %call = call %class.btPrimitiveManagerBase* @_ZN22btPrimitiveManagerBaseD2Ev(%class.btPrimitiveManagerBase* %0) #8
  ret %"class.btGImpactCompoundShape::CompoundPrimitiveManager"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP16btCollisionShapeE5clearEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP16btCollisionShapeE4sizeEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP16btCollisionShapeE7destroyEii(%class.btAlignedObjectArray.4* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP16btCollisionShapeE10deallocateEv(%class.btAlignedObjectArray.4* %this1)
  call void @_ZN20btAlignedObjectArrayIP16btCollisionShapeE4initEv(%class.btAlignedObjectArray.4* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP16btCollisionShapeE7destroyEii(%class.btAlignedObjectArray.4* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %3 = load %class.btCollisionShape**, %class.btCollisionShape*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionShape*, %class.btCollisionShape** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP16btCollisionShapeE4sizeEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP16btCollisionShapeE10deallocateEv(%class.btAlignedObjectArray.4* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btCollisionShape**, %class.btCollisionShape*** %m_data, align 4
  %tobool = icmp ne %class.btCollisionShape** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %2 = load %class.btCollisionShape**, %class.btCollisionShape*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP16btCollisionShapeLj16EE10deallocateEPS1_(%class.btAlignedAllocator.5* %m_allocator, %class.btCollisionShape** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btCollisionShape** null, %class.btCollisionShape*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP16btCollisionShapeE4initEv(%class.btAlignedObjectArray.4* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  store %class.btCollisionShape** null, %class.btCollisionShape*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP16btCollisionShapeLj16EE10deallocateEPS1_(%class.btAlignedAllocator.5* %this, %class.btCollisionShape** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.5*, align 4
  %ptr.addr = alloca %class.btCollisionShape**, align 4
  store %class.btAlignedAllocator.5* %this, %class.btAlignedAllocator.5** %this.addr, align 4
  store %class.btCollisionShape** %ptr, %class.btCollisionShape*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.5*, %class.btAlignedAllocator.5** %this.addr, align 4
  %0 = load %class.btCollisionShape**, %class.btCollisionShape*** %ptr.addr, align 4
  %1 = bitcast %class.btCollisionShape** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btTransformE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayI11btTransformE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI11btTransformE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayI11btTransformE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayI11btTransformE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btTransformE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %class.btTransform*, %class.btTransform** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTransform, %class.btTransform* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI11btTransformE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btTransformE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btTransform*, %class.btTransform** %m_data, align 4
  %tobool = icmp ne %class.btTransform* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %class.btTransform*, %class.btTransform** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorI11btTransformLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %m_allocator, %class.btTransform* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btTransform* null, %class.btTransform** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayI11btTransformE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btTransform* null, %class.btTransform** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorI11btTransformLj16EE10deallocateEPS0_(%class.btAlignedAllocator.1* %this, %class.btTransform* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %class.btTransform*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %class.btTransform* %ptr, %class.btTransform** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %ptr.addr, align 4
  %1 = bitcast %class.btTransform* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAABB* @_ZN6btAABBC2ERKS_(%class.btAABB* returned %this, %class.btAABB* nonnull align 4 dereferenceable(32) %other) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  %other.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4
  store %class.btAABB* %other, %class.btAABB** %other.addr, align 4
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %0 = load %class.btAABB*, %class.btAABB** %other.addr, align 4
  %m_min2 = getelementptr inbounds %class.btAABB, %class.btAABB* %0, i32 0, i32 0
  %1 = bitcast %class.btVector3* %m_min to i8*
  %2 = bitcast %class.btVector3* %m_min2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %3 = load %class.btAABB*, %class.btAABB** %other.addr, align 4
  %m_max3 = getelementptr inbounds %class.btAABB, %class.btAABB* %3, i32 0, i32 1
  %4 = bitcast %class.btVector3* %m_max to i8*
  %5 = bitcast %class.btVector3* %m_max3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  ret %class.btAABB* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN6btAABB14appy_transformERK11btTransform(%class.btAABB* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans) #2 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %center = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca float, align 4
  %extends = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca %class.btVector3, align 4
  %textends = alloca %class.btVector3, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp13 = alloca %class.btVector3, align 4
  %ref.tmp15 = alloca %class.btVector3, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min)
  store float 5.000000e-01, float* %ref.tmp2, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %center, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2)
  %m_max3 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %extends, %class.btVector3* nonnull align 4 dereferenceable(16) %m_max3, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  %0 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp4, %class.btTransform* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %center)
  %1 = bitcast %class.btVector3* %center to i8*
  %2 = bitcast %class.btVector3* %ref.tmp4 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %3)
  %call6 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x36getRowEi(%class.btMatrix3x3* %call, i32 0)
  call void @_ZNK9btVector38absoluteEv(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* %call6)
  %4 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %call8 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %4)
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x36getRowEi(%class.btMatrix3x3* %call8, i32 1)
  call void @_ZNK9btVector38absoluteEv(%class.btVector3* sret align 4 %ref.tmp7, %class.btVector3* %call9)
  %5 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %5)
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x36getRowEi(%class.btMatrix3x3* %call11, i32 2)
  call void @_ZNK9btVector38absoluteEv(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* %call12)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %textends, %class.btVector3* %extends, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp10)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp13, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %textends)
  %m_min14 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %6 = bitcast %class.btVector3* %m_min14 to i8*
  %7 = bitcast %class.btVector3* %ref.tmp13 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %6, i8* align 4 %7, i32 16, i1 false)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp15, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %textends)
  %m_max16 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %8 = bitcast %class.btVector3* %m_max16 to i8*
  %9 = bitcast %class.btVector3* %ref.tmp15 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x36getRowEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector38absoluteEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp6 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %call = call float @_Z6btFabsf(float %0)
  store float %call, float* %ref.tmp, align 4
  %m_floats3 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [4 x float], [4 x float]* %m_floats3, i32 0, i32 1
  %1 = load float, float* %arrayidx4, align 4
  %call5 = call float @_Z6btFabsf(float %1)
  store float %call5, float* %ref.tmp2, align 4
  %m_floats7 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx8 = getelementptr inbounds [4 x float], [4 x float]* %m_floats7, i32 0, i32 2
  %2 = load float, float* %arrayidx8, align 4
  %call9 = call float @_Z6btFabsf(float %2)
  store float %call9, float* %ref.tmp6, align 4
  %call10 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp6)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #9

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK21btGImpactQuantizedBvh12getNodeCountEv(%class.btGImpactQuantizedBvh* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactQuantizedBvh*, align 4
  store %class.btGImpactQuantizedBvh* %this, %class.btGImpactQuantizedBvh** %this.addr, align 4
  %this1 = load %class.btGImpactQuantizedBvh*, %class.btGImpactQuantizedBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactQuantizedBvh, %class.btGImpactQuantizedBvh* %this1, i32 0, i32 0
  %call = call i32 @_ZNK18btQuantizedBvhTree12getNodeCountEv(%class.btQuantizedBvhTree* %m_box_tree)
  ret i32 %call
}

declare void @_ZN21btGImpactQuantizedBvh8buildSetEv(%class.btGImpactQuantizedBvh*) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN21btGImpactQuantizedBvh6updateEv(%class.btGImpactQuantizedBvh* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactQuantizedBvh*, align 4
  store %class.btGImpactQuantizedBvh* %this, %class.btGImpactQuantizedBvh** %this.addr, align 4
  %this1 = load %class.btGImpactQuantizedBvh*, %class.btGImpactQuantizedBvh** %this.addr, align 4
  call void @_ZN21btGImpactQuantizedBvh5refitEv(%class.btGImpactQuantizedBvh* %this1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK21btGImpactQuantizedBvh12getGlobalBoxEv(%class.btAABB* noalias sret align 4 %agg.result, %class.btGImpactQuantizedBvh* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btGImpactQuantizedBvh*, align 4
  %0 = bitcast %class.btAABB* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btGImpactQuantizedBvh* %this, %class.btGImpactQuantizedBvh** %this.addr, align 4
  %this1 = load %class.btGImpactQuantizedBvh*, %class.btGImpactQuantizedBvh** %this.addr, align 4
  %call = call %class.btAABB* @_ZN6btAABBC2Ev(%class.btAABB* %agg.result)
  call void @_ZNK21btGImpactQuantizedBvh12getNodeBoundEiR6btAABB(%class.btGImpactQuantizedBvh* %this1, i32 0, %class.btAABB* nonnull align 4 dereferenceable(32) %agg.result)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK18btQuantizedBvhTree12getNodeCountEv(%class.btQuantizedBvhTree* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvhTree*, align 4
  store %class.btQuantizedBvhTree* %this, %class.btQuantizedBvhTree** %this.addr, align 4
  %this1 = load %class.btQuantizedBvhTree*, %class.btQuantizedBvhTree** %this.addr, align 4
  %m_num_nodes = getelementptr inbounds %class.btQuantizedBvhTree, %class.btQuantizedBvhTree* %this1, i32 0, i32 0
  %0 = load i32, i32* %m_num_nodes, align 4
  ret i32 %0
}

declare void @_ZN21btGImpactQuantizedBvh5refitEv(%class.btGImpactQuantizedBvh*) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK21btGImpactQuantizedBvh12getNodeBoundEiR6btAABB(%class.btGImpactQuantizedBvh* %this, i32 %nodeindex, %class.btAABB* nonnull align 4 dereferenceable(32) %bound) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactQuantizedBvh*, align 4
  %nodeindex.addr = alloca i32, align 4
  %bound.addr = alloca %class.btAABB*, align 4
  store %class.btGImpactQuantizedBvh* %this, %class.btGImpactQuantizedBvh** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  store %class.btAABB* %bound, %class.btAABB** %bound.addr, align 4
  %this1 = load %class.btGImpactQuantizedBvh*, %class.btGImpactQuantizedBvh** %this.addr, align 4
  %m_box_tree = getelementptr inbounds %class.btGImpactQuantizedBvh, %class.btGImpactQuantizedBvh* %this1, i32 0, i32 0
  %0 = load i32, i32* %nodeindex.addr, align 4
  %1 = load %class.btAABB*, %class.btAABB** %bound.addr, align 4
  call void @_ZNK18btQuantizedBvhTree12getNodeBoundEiR6btAABB(%class.btQuantizedBvhTree* %m_box_tree, i32 %0, %class.btAABB* nonnull align 4 dereferenceable(32) %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK18btQuantizedBvhTree12getNodeBoundEiR6btAABB(%class.btQuantizedBvhTree* %this, i32 %nodeindex, %class.btAABB* nonnull align 4 dereferenceable(32) %bound) #2 comdat {
entry:
  %this.addr = alloca %class.btQuantizedBvhTree*, align 4
  %nodeindex.addr = alloca i32, align 4
  %bound.addr = alloca %class.btAABB*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  store %class.btQuantizedBvhTree* %this, %class.btQuantizedBvhTree** %this.addr, align 4
  store i32 %nodeindex, i32* %nodeindex.addr, align 4
  store %class.btAABB* %bound, %class.btAABB** %bound.addr, align 4
  %this1 = load %class.btQuantizedBvhTree*, %class.btQuantizedBvhTree** %this.addr, align 4
  %m_node_array = getelementptr inbounds %class.btQuantizedBvhTree, %class.btQuantizedBvhTree* %this1, i32 0, i32 1
  %0 = bitcast %class.GIM_QUANTIZED_BVH_NODE_ARRAY* %m_node_array to %class.btAlignedObjectArray*
  %1 = load i32, i32* %nodeindex.addr, align 4
  %call = call nonnull align 4 dereferenceable(16) %struct.BT_QUANTIZED_BVH_NODE* @_ZNK20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEEixEi(%class.btAlignedObjectArray* %0, i32 %1)
  %m_quantizedAabbMin = getelementptr inbounds %struct.BT_QUANTIZED_BVH_NODE, %struct.BT_QUANTIZED_BVH_NODE* %call, i32 0, i32 0
  %arraydecay = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMin, i32 0, i32 0
  %m_global_bound = getelementptr inbounds %class.btQuantizedBvhTree, %class.btQuantizedBvhTree* %this1, i32 0, i32 2
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %m_global_bound, i32 0, i32 0
  %m_bvhQuantization = getelementptr inbounds %class.btQuantizedBvhTree, %class.btQuantizedBvhTree* %this1, i32 0, i32 3
  call void @_Z13bt_unquantizePKtRK9btVector3S3_(%class.btVector3* sret align 4 %ref.tmp, i16* %arraydecay, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhQuantization)
  %2 = load %class.btAABB*, %class.btAABB** %bound.addr, align 4
  %m_min2 = getelementptr inbounds %class.btAABB, %class.btAABB* %2, i32 0, i32 0
  %3 = bitcast %class.btVector3* %m_min2 to i8*
  %4 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %3, i8* align 4 %4, i32 16, i1 false)
  %m_node_array4 = getelementptr inbounds %class.btQuantizedBvhTree, %class.btQuantizedBvhTree* %this1, i32 0, i32 1
  %5 = bitcast %class.GIM_QUANTIZED_BVH_NODE_ARRAY* %m_node_array4 to %class.btAlignedObjectArray*
  %6 = load i32, i32* %nodeindex.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(16) %struct.BT_QUANTIZED_BVH_NODE* @_ZNK20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEEixEi(%class.btAlignedObjectArray* %5, i32 %6)
  %m_quantizedAabbMax = getelementptr inbounds %struct.BT_QUANTIZED_BVH_NODE, %struct.BT_QUANTIZED_BVH_NODE* %call5, i32 0, i32 1
  %arraydecay6 = getelementptr inbounds [3 x i16], [3 x i16]* %m_quantizedAabbMax, i32 0, i32 0
  %m_global_bound7 = getelementptr inbounds %class.btQuantizedBvhTree, %class.btQuantizedBvhTree* %this1, i32 0, i32 2
  %m_min8 = getelementptr inbounds %class.btAABB, %class.btAABB* %m_global_bound7, i32 0, i32 0
  %m_bvhQuantization9 = getelementptr inbounds %class.btQuantizedBvhTree, %class.btQuantizedBvhTree* %this1, i32 0, i32 3
  call void @_Z13bt_unquantizePKtRK9btVector3S3_(%class.btVector3* sret align 4 %ref.tmp3, i16* %arraydecay6, %class.btVector3* nonnull align 4 dereferenceable(16) %m_min8, %class.btVector3* nonnull align 4 dereferenceable(16) %m_bvhQuantization9)
  %7 = load %class.btAABB*, %class.btAABB** %bound.addr, align 4
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %7, i32 0, i32 1
  %8 = bitcast %class.btVector3* %m_max to i8*
  %9 = bitcast %class.btVector3* %ref.tmp3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %8, i8* align 4 %9, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z13bt_unquantizePKtRK9btVector3S3_(%class.btVector3* noalias sret align 4 %agg.result, i16* %vecIn, %class.btVector3* nonnull align 4 dereferenceable(16) %offset, %class.btVector3* nonnull align 4 dereferenceable(16) %bvhQuantization) #2 comdat {
entry:
  %vecIn.addr = alloca i16*, align 4
  %offset.addr = alloca %class.btVector3*, align 4
  %bvhQuantization.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp7 = alloca float, align 4
  store i16* %vecIn, i16** %vecIn.addr, align 4
  store %class.btVector3* %offset, %class.btVector3** %offset.addr, align 4
  store %class.btVector3* %bvhQuantization, %class.btVector3** %bvhQuantization.addr, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %agg.result)
  %0 = load i16*, i16** %vecIn.addr, align 4
  %arrayidx = getelementptr inbounds i16, i16* %0, i32 0
  %1 = load i16, i16* %arrayidx, align 2
  %conv = uitofp i16 %1 to float
  %2 = load %class.btVector3*, %class.btVector3** %bvhQuantization.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %2)
  %3 = load float, float* %call1, align 4
  %div = fdiv float %conv, %3
  store float %div, float* %ref.tmp, align 4
  %4 = load i16*, i16** %vecIn.addr, align 4
  %arrayidx3 = getelementptr inbounds i16, i16* %4, i32 1
  %5 = load i16, i16* %arrayidx3, align 2
  %conv4 = uitofp i16 %5 to float
  %6 = load %class.btVector3*, %class.btVector3** %bvhQuantization.addr, align 4
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %6)
  %7 = load float, float* %call5, align 4
  %div6 = fdiv float %conv4, %7
  store float %div6, float* %ref.tmp2, align 4
  %8 = load i16*, i16** %vecIn.addr, align 4
  %arrayidx8 = getelementptr inbounds i16, i16* %8, i32 2
  %9 = load i16, i16* %arrayidx8, align 2
  %conv9 = uitofp i16 %9 to float
  %10 = load %class.btVector3*, %class.btVector3** %bvhQuantization.addr, align 4
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %10)
  %11 = load float, float* %call10, align 4
  %div11 = fdiv float %conv9, %11
  store float %div11, float* %ref.tmp7, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp7)
  %12 = load %class.btVector3*, %class.btVector3** %offset.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %12)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %struct.BT_QUANTIZED_BVH_NODE* @_ZNK20btAlignedObjectArrayI21BT_QUANTIZED_BVH_NODEEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.BT_QUANTIZED_BVH_NODE*, %struct.BT_QUANTIZED_BVH_NODE** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.BT_QUANTIZED_BVH_NODE, %struct.BT_QUANTIZED_BVH_NODE* %0, i32 %1
  ret %struct.BT_QUANTIZED_BVH_NODE* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformmlERKS_(%class.btTransform* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btTransform*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %ref.tmp = alloca %class.btMatrix3x3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %0 = bitcast %class.btTransform* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %1 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 0
  call void @_ZmlRK11btMatrix3x3S1_(%class.btMatrix3x3* sret align 4 %ref.tmp, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %2 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %2, i32 0, i32 1
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btTransform* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  %call = call %class.btTransform* @_ZN11btTransformC2ERK11btMatrix3x3RK9btVector3(%class.btTransform* %agg.result, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionShape** @_ZN20btAlignedObjectArrayIP16btCollisionShapeEixEi(%class.btAlignedObjectArray.4* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.4*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.4* %this, %class.btAlignedObjectArray.4** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.4*, %class.btAlignedObjectArray.4** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.4, %class.btAlignedObjectArray.4* %this1, i32 0, i32 4
  %0 = load %class.btCollisionShape**, %class.btCollisionShape*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionShape*, %class.btCollisionShape** %0, i32 %1
  ret %class.btCollisionShape** %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2ERKS_(%class.btTransform* returned %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKS_(%class.btMatrix3x3* %m_basis, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis2)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin to i8*
  %3 = bitcast %class.btVector3* %m_origin3 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN20btAlignedObjectArrayI11btTransformEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btTransform*, %class.btTransform** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 %1
  ret %class.btTransform* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN11btTransformaSERKS_(%class.btTransform* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %other) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %other.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btTransform* %other, %class.btTransform** %other.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %0, i32 0, i32 0
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %m_basis2, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m_basis)
  %1 = load %class.btTransform*, %class.btTransform** %other.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %1, i32 0, i32 1
  %m_origin3 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %2 = bitcast %class.btVector3* %m_origin3 to i8*
  %3 = bitcast %class.btVector3* %m_origin to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %2, i8* align 4 %3, i32 16, i1 false)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZN11btMatrix3x3aSERKS_(%class.btMatrix3x3* %this, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %other) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %other.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %other, %class.btMatrix3x3** %other.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 0
  %1 = bitcast %class.btVector3* %arrayidx3 to i8*
  %2 = bitcast %class.btVector3* %arrayidx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %3 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 1
  %m_el6 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el6, i32 0, i32 1
  %4 = bitcast %class.btVector3* %arrayidx7 to i8*
  %5 = bitcast %class.btVector3* %arrayidx5 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %4, i8* align 4 %5, i32 16, i1 false)
  %6 = load %class.btMatrix3x3*, %class.btMatrix3x3** %other.addr, align 4
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %6, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %m_el10 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el10, i32 0, i32 2
  %7 = bitcast %class.btVector3* %arrayidx11 to i8*
  %8 = bitcast %class.btVector3* %arrayidx9 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  ret %class.btMatrix3x3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager19get_bullet_triangleEiR17btTriangleShapeEx(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, i32 %prim_index, %class.btTriangleShapeEx* nonnull align 4 dereferenceable(108) %triangle) #2 comdat {
entry:
  %this.addr = alloca %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, align 4
  %prim_index.addr = alloca i32, align 4
  %triangle.addr = alloca %class.btTriangleShapeEx*, align 4
  %indices = alloca [3 x i32], align 4
  store %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  store i32 %prim_index, i32* %prim_index.addr, align 4
  store %class.btTriangleShapeEx* %triangle, %class.btTriangleShapeEx** %triangle.addr, align 4
  %this1 = load %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"*, %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"** %this.addr, align 4
  %0 = load i32, i32* %prim_index.addr, align 4
  %arrayidx = getelementptr inbounds [3 x i32], [3 x i32]* %indices, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [3 x i32], [3 x i32]* %indices, i32 0, i32 1
  %arrayidx3 = getelementptr inbounds [3 x i32], [3 x i32]* %indices, i32 0, i32 2
  call void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager11get_indicesEiRjS1_S1_(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 %0, i32* nonnull align 4 dereferenceable(4) %arrayidx, i32* nonnull align 4 dereferenceable(4) %arrayidx2, i32* nonnull align 4 dereferenceable(4) %arrayidx3)
  %arrayidx4 = getelementptr inbounds [3 x i32], [3 x i32]* %indices, i32 0, i32 0
  %1 = load i32, i32* %arrayidx4, align 4
  %2 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %triangle.addr, align 4
  %3 = bitcast %class.btTriangleShapeEx* %2 to %class.btTriangleShape*
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %3, i32 0, i32 1
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  call void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEjR9btVector3(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 %1, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx5)
  %arrayidx6 = getelementptr inbounds [3 x i32], [3 x i32]* %indices, i32 0, i32 1
  %4 = load i32, i32* %arrayidx6, align 4
  %5 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %triangle.addr, align 4
  %6 = bitcast %class.btTriangleShapeEx* %5 to %class.btTriangleShape*
  %m_vertices17 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %6, i32 0, i32 1
  %arrayidx8 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices17, i32 0, i32 1
  call void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEjR9btVector3(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 %4, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx8)
  %arrayidx9 = getelementptr inbounds [3 x i32], [3 x i32]* %indices, i32 0, i32 2
  %7 = load i32, i32* %arrayidx9, align 4
  %8 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %triangle.addr, align 4
  %9 = bitcast %class.btTriangleShapeEx* %8 to %class.btTriangleShape*
  %m_vertices110 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %9, i32 0, i32 1
  %arrayidx11 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices110, i32 0, i32 2
  call void @_ZNK22btGImpactMeshShapePart23TrimeshPrimitiveManager10get_vertexEjR9btVector3(%"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 %7, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx11)
  %10 = load %class.btTriangleShapeEx*, %class.btTriangleShapeEx** %triangle.addr, align 4
  %11 = bitcast %class.btTriangleShapeEx* %10 to %class.btConvexInternalShape*
  %m_margin = getelementptr inbounds %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager", %"class.btGImpactMeshShapePart::TrimeshPrimitiveManager"* %this1, i32 0, i32 1
  %12 = load float, float* %m_margin, align 4
  %13 = bitcast %class.btConvexInternalShape* %11 to void (%class.btConvexInternalShape*, float)***
  %vtable = load void (%class.btConvexInternalShape*, float)**, void (%class.btConvexInternalShape*, float)*** %13, align 4
  %vfn = getelementptr inbounds void (%class.btConvexInternalShape*, float)*, void (%class.btConvexInternalShape*, float)** %vtable, i64 11
  %14 = load void (%class.btConvexInternalShape*, float)*, void (%class.btConvexInternalShape*, float)** %vfn, align 4
  call void %14(%class.btConvexInternalShape* %11, float %12)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTransform* @_ZN11btTransformC2Ev(%class.btTransform* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* %m_basis)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_origin)
  ret %class.btTransform* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2Ev(%class.btMatrix3x3* returned %this) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btGImpactMeshShapePart** @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartEixEi(%class.btAlignedObjectArray.8* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %class.btGImpactMeshShapePart**, %class.btGImpactMeshShapePart*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %0, i32 %1
  ret %class.btGImpactMeshShapePart** %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartE5clearEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP22btGImpactMeshShapePartE4sizeEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartE7destroyEii(%class.btAlignedObjectArray.8* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartE10deallocateEv(%class.btAlignedObjectArray.8* %this1)
  call void @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartE4initEv(%class.btAlignedObjectArray.8* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.8* @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartED2Ev(%class.btAlignedObjectArray.8* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartE5clearEv(%class.btAlignedObjectArray.8* %this1)
  ret %class.btAlignedObjectArray.8* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartE7destroyEii(%class.btAlignedObjectArray.8* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %3 = load %class.btGImpactMeshShapePart**, %class.btGImpactMeshShapePart*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btGImpactMeshShapePart*, %class.btGImpactMeshShapePart** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartE10deallocateEv(%class.btAlignedObjectArray.8* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %0 = load %class.btGImpactMeshShapePart**, %class.btGImpactMeshShapePart*** %m_data, align 4
  %tobool = icmp ne %class.btGImpactMeshShapePart** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  %2 = load %class.btGImpactMeshShapePart**, %class.btGImpactMeshShapePart*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP22btGImpactMeshShapePartLj16EE10deallocateEPS1_(%class.btAlignedAllocator.9* %m_allocator, %class.btGImpactMeshShapePart** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %class.btGImpactMeshShapePart** null, %class.btGImpactMeshShapePart*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP22btGImpactMeshShapePartE4initEv(%class.btAlignedObjectArray.8* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.8*, align 4
  store %class.btAlignedObjectArray.8* %this, %class.btAlignedObjectArray.8** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.8*, %class.btAlignedObjectArray.8** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 4
  store %class.btGImpactMeshShapePart** null, %class.btGImpactMeshShapePart*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.8, %class.btAlignedObjectArray.8* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP22btGImpactMeshShapePartLj16EE10deallocateEPS1_(%class.btAlignedAllocator.9* %this, %class.btGImpactMeshShapePart** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.9*, align 4
  %ptr.addr = alloca %class.btGImpactMeshShapePart**, align 4
  store %class.btAlignedAllocator.9* %this, %class.btAlignedAllocator.9** %this.addr, align 4
  store %class.btGImpactMeshShapePart** %ptr, %class.btGImpactMeshShapePart*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.9*, %class.btAlignedAllocator.9** %this.addr, align 4
  %0 = load %class.btGImpactMeshShapePart**, %class.btGImpactMeshShapePart*** %ptr.addr, align 4
  %1 = bitcast %class.btGImpactMeshShapePart** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN23btGImpactShapeInterface11updateBoundEv(%class.btGImpactShapeInterface* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %m_needs_update = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 2
  %0 = load i8, i8* %m_needs_update, align 4
  %tobool = trunc i8 %0 to i1
  br i1 %tobool, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  br label %return

if.end:                                           ; preds = %entry
  %1 = bitcast %class.btGImpactShapeInterface* %this1 to void (%class.btGImpactShapeInterface*)***
  %vtable = load void (%class.btGImpactShapeInterface*)**, void (%class.btGImpactShapeInterface*)*** %1, align 4
  %vfn = getelementptr inbounds void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vtable, i64 17
  %2 = load void (%class.btGImpactShapeInterface*)*, void (%class.btGImpactShapeInterface*)** %vfn, align 4
  call void %2(%class.btGImpactShapeInterface* %this1)
  %m_needs_update2 = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 2
  store i8 0, i8* %m_needs_update2, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN6btAABB5mergeERKS_(%class.btAABB* %this, %class.btAABB* nonnull align 4 dereferenceable(32) %box) #1 comdat {
entry:
  %this.addr = alloca %class.btAABB*, align 4
  %box.addr = alloca %class.btAABB*, align 4
  store %class.btAABB* %this, %class.btAABB** %this.addr, align 4
  store %class.btAABB* %box, %class.btAABB** %box.addr, align 4
  %this1 = load %class.btAABB*, %class.btAABB** %this.addr, align 4
  %m_min = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min)
  %arrayidx = getelementptr inbounds float, float* %call, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_min2 = getelementptr inbounds %class.btAABB, %class.btAABB* %1, i32 0, i32 0
  %call3 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min2)
  %arrayidx4 = getelementptr inbounds float, float* %call3, i32 0
  %2 = load float, float* %arrayidx4, align 4
  %cmp = fcmp ogt float %0, %2
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_min5 = getelementptr inbounds %class.btAABB, %class.btAABB* %3, i32 0, i32 0
  %call6 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min5)
  %arrayidx7 = getelementptr inbounds float, float* %call6, i32 0
  %4 = load float, float* %arrayidx7, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_min8 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call9 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min8)
  %arrayidx10 = getelementptr inbounds float, float* %call9, i32 0
  %5 = load float, float* %arrayidx10, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %4, %cond.true ], [ %5, %cond.false ]
  %m_min11 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call12 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min11)
  %arrayidx13 = getelementptr inbounds float, float* %call12, i32 0
  store float %cond, float* %arrayidx13, align 4
  %m_min14 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call15 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min14)
  %arrayidx16 = getelementptr inbounds float, float* %call15, i32 1
  %6 = load float, float* %arrayidx16, align 4
  %7 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_min17 = getelementptr inbounds %class.btAABB, %class.btAABB* %7, i32 0, i32 0
  %call18 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min17)
  %arrayidx19 = getelementptr inbounds float, float* %call18, i32 1
  %8 = load float, float* %arrayidx19, align 4
  %cmp20 = fcmp ogt float %6, %8
  br i1 %cmp20, label %cond.true21, label %cond.false25

cond.true21:                                      ; preds = %cond.end
  %9 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_min22 = getelementptr inbounds %class.btAABB, %class.btAABB* %9, i32 0, i32 0
  %call23 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min22)
  %arrayidx24 = getelementptr inbounds float, float* %call23, i32 1
  %10 = load float, float* %arrayidx24, align 4
  br label %cond.end29

cond.false25:                                     ; preds = %cond.end
  %m_min26 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call27 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min26)
  %arrayidx28 = getelementptr inbounds float, float* %call27, i32 1
  %11 = load float, float* %arrayidx28, align 4
  br label %cond.end29

cond.end29:                                       ; preds = %cond.false25, %cond.true21
  %cond30 = phi float [ %10, %cond.true21 ], [ %11, %cond.false25 ]
  %m_min31 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call32 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min31)
  %arrayidx33 = getelementptr inbounds float, float* %call32, i32 1
  store float %cond30, float* %arrayidx33, align 4
  %m_min34 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call35 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min34)
  %arrayidx36 = getelementptr inbounds float, float* %call35, i32 2
  %12 = load float, float* %arrayidx36, align 4
  %13 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_min37 = getelementptr inbounds %class.btAABB, %class.btAABB* %13, i32 0, i32 0
  %call38 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min37)
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 2
  %14 = load float, float* %arrayidx39, align 4
  %cmp40 = fcmp ogt float %12, %14
  br i1 %cmp40, label %cond.true41, label %cond.false45

cond.true41:                                      ; preds = %cond.end29
  %15 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_min42 = getelementptr inbounds %class.btAABB, %class.btAABB* %15, i32 0, i32 0
  %call43 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_min42)
  %arrayidx44 = getelementptr inbounds float, float* %call43, i32 2
  %16 = load float, float* %arrayidx44, align 4
  br label %cond.end49

cond.false45:                                     ; preds = %cond.end29
  %m_min46 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call47 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min46)
  %arrayidx48 = getelementptr inbounds float, float* %call47, i32 2
  %17 = load float, float* %arrayidx48, align 4
  br label %cond.end49

cond.end49:                                       ; preds = %cond.false45, %cond.true41
  %cond50 = phi float [ %16, %cond.true41 ], [ %17, %cond.false45 ]
  %m_min51 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 0
  %call52 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_min51)
  %arrayidx53 = getelementptr inbounds float, float* %call52, i32 2
  store float %cond50, float* %arrayidx53, align 4
  %m_max = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call54 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max)
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 0
  %18 = load float, float* %arrayidx55, align 4
  %19 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_max56 = getelementptr inbounds %class.btAABB, %class.btAABB* %19, i32 0, i32 1
  %call57 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max56)
  %arrayidx58 = getelementptr inbounds float, float* %call57, i32 0
  %20 = load float, float* %arrayidx58, align 4
  %cmp59 = fcmp olt float %18, %20
  br i1 %cmp59, label %cond.true60, label %cond.false64

cond.true60:                                      ; preds = %cond.end49
  %21 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_max61 = getelementptr inbounds %class.btAABB, %class.btAABB* %21, i32 0, i32 1
  %call62 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max61)
  %arrayidx63 = getelementptr inbounds float, float* %call62, i32 0
  %22 = load float, float* %arrayidx63, align 4
  br label %cond.end68

cond.false64:                                     ; preds = %cond.end49
  %m_max65 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call66 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max65)
  %arrayidx67 = getelementptr inbounds float, float* %call66, i32 0
  %23 = load float, float* %arrayidx67, align 4
  br label %cond.end68

cond.end68:                                       ; preds = %cond.false64, %cond.true60
  %cond69 = phi float [ %22, %cond.true60 ], [ %23, %cond.false64 ]
  %m_max70 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call71 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max70)
  %arrayidx72 = getelementptr inbounds float, float* %call71, i32 0
  store float %cond69, float* %arrayidx72, align 4
  %m_max73 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call74 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max73)
  %arrayidx75 = getelementptr inbounds float, float* %call74, i32 1
  %24 = load float, float* %arrayidx75, align 4
  %25 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_max76 = getelementptr inbounds %class.btAABB, %class.btAABB* %25, i32 0, i32 1
  %call77 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max76)
  %arrayidx78 = getelementptr inbounds float, float* %call77, i32 1
  %26 = load float, float* %arrayidx78, align 4
  %cmp79 = fcmp olt float %24, %26
  br i1 %cmp79, label %cond.true80, label %cond.false84

cond.true80:                                      ; preds = %cond.end68
  %27 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_max81 = getelementptr inbounds %class.btAABB, %class.btAABB* %27, i32 0, i32 1
  %call82 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max81)
  %arrayidx83 = getelementptr inbounds float, float* %call82, i32 1
  %28 = load float, float* %arrayidx83, align 4
  br label %cond.end88

cond.false84:                                     ; preds = %cond.end68
  %m_max85 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max85)
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 1
  %29 = load float, float* %arrayidx87, align 4
  br label %cond.end88

cond.end88:                                       ; preds = %cond.false84, %cond.true80
  %cond89 = phi float [ %28, %cond.true80 ], [ %29, %cond.false84 ]
  %m_max90 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call91 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max90)
  %arrayidx92 = getelementptr inbounds float, float* %call91, i32 1
  store float %cond89, float* %arrayidx92, align 4
  %m_max93 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call94 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max93)
  %arrayidx95 = getelementptr inbounds float, float* %call94, i32 2
  %30 = load float, float* %arrayidx95, align 4
  %31 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_max96 = getelementptr inbounds %class.btAABB, %class.btAABB* %31, i32 0, i32 1
  %call97 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max96)
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 2
  %32 = load float, float* %arrayidx98, align 4
  %cmp99 = fcmp olt float %30, %32
  br i1 %cmp99, label %cond.true100, label %cond.false104

cond.true100:                                     ; preds = %cond.end88
  %33 = load %class.btAABB*, %class.btAABB** %box.addr, align 4
  %m_max101 = getelementptr inbounds %class.btAABB, %class.btAABB* %33, i32 0, i32 1
  %call102 = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_max101)
  %arrayidx103 = getelementptr inbounds float, float* %call102, i32 2
  %34 = load float, float* %arrayidx103, align 4
  br label %cond.end108

cond.false104:                                    ; preds = %cond.end88
  %m_max105 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call106 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max105)
  %arrayidx107 = getelementptr inbounds float, float* %call106, i32 2
  %35 = load float, float* %arrayidx107, align 4
  br label %cond.end108

cond.end108:                                      ; preds = %cond.false104, %cond.true100
  %cond109 = phi float [ %34, %cond.true100 ], [ %35, %cond.false104 ]
  %m_max110 = getelementptr inbounds %class.btAABB, %class.btAABB* %this1, i32 0, i32 1
  %call111 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %m_max110)
  %arrayidx112 = getelementptr inbounds float, float* %call111, i32 2
  store float %cond109, float* %arrayidx112, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(32) %class.btAABB* @_ZN23btGImpactShapeInterface11getLocalBoxEv(%class.btGImpactShapeInterface* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btGImpactShapeInterface*, align 4
  store %class.btGImpactShapeInterface* %this, %class.btGImpactShapeInterface** %this.addr, align 4
  %this1 = load %class.btGImpactShapeInterface*, %class.btGImpactShapeInterface** %this.addr, align 4
  %m_localAABB = getelementptr inbounds %class.btGImpactShapeInterface, %class.btGImpactShapeInterface* %this1, i32 0, i32 1
  ret %class.btAABB* %m_localAABB
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.17* @_ZN18btAlignedAllocatorIiLj16EEC2Ev(%class.btAlignedAllocator.17* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  ret %class.btAlignedAllocator.17* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.16* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store i32* null, i32** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE5clearEv(%class.btAlignedObjectArray.16* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIiE4sizeEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.16* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.16* %this1)
  call void @_ZN20btAlignedObjectArrayIiE4initEv(%class.btAlignedObjectArray.16* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE7destroyEii(%class.btAlignedObjectArray.16* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %3 = load i32*, i32** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds i32, i32* %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIiE10deallocateEv(%class.btAlignedObjectArray.16* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.16*, align 4
  store %class.btAlignedObjectArray.16* %this, %class.btAlignedObjectArray.16** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.16*, %class.btAlignedObjectArray.16** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %0 = load i32*, i32** %m_data, align 4
  %tobool = icmp ne i32* %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  %2 = load i32*, i32** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.17* %m_allocator, i32* %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.16, %class.btAlignedObjectArray.16* %this1, i32 0, i32 4
  store i32* null, i32** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIiLj16EE10deallocateEPi(%class.btAlignedAllocator.17* %this, i32* %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.17*, align 4
  %ptr.addr = alloca i32*, align 4
  store %class.btAlignedAllocator.17* %this, %class.btAlignedAllocator.17** %this.addr, align 4
  store i32* %ptr, i32** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.17*, %class.btAlignedAllocator.17** %this.addr, align 4
  %0 = load i32*, i32** %ptr.addr, align 4
  %1 = bitcast i32* %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btGImpactShape.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { argmemonly nounwind willreturn }
attributes #4 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { cold noreturn nounwind }
attributes #6 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #8 = { nounwind }
attributes #9 = { nounwind readnone speculatable willreturn }
attributes #10 = { noreturn nounwind }
attributes #11 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
!2 = !{!"branch_weights", i32 1, i32 1048575}
