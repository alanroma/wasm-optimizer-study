; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btConvexShape.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionShapes/btConvexShape.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btConvexShape = type { %class.btCollisionShape }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%class.btVector3 = type { [4 x float] }
%class.btBoxShape = type { %class.btPolyhedralConvexShape }
%class.btPolyhedralConvexShape = type { %class.btConvexInternalShape, %class.btConvexPolyhedron* }
%class.btConvexInternalShape = type { %class.btConvexShape, %class.btVector3, %class.btVector3, float, float }
%class.btConvexPolyhedron = type opaque
%class.btTriangleShape = type { %class.btPolyhedralConvexShape, [3 x %class.btVector3] }
%class.btCylinderShape = type { %class.btConvexInternalShape, i32 }
%class.btCapsuleShape = type { %class.btConvexInternalShape, i32 }
%class.btConvexPointCloudShape = type { %class.btPolyhedralConvexAabbCachingShape.base, %class.btVector3*, i32 }
%class.btPolyhedralConvexAabbCachingShape.base = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8 }>
%class.btConvexHullShape = type { %class.btPolyhedralConvexAabbCachingShape.base, [3 x i8], %class.btAlignedObjectArray }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btVector3*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btSphereShape = type { %class.btConvexInternalShape }
%class.btConeShape = type { %class.btConvexInternalShape, float, float, float, [3 x i32] }
%class.btPolyhedralConvexAabbCachingShape = type <{ %class.btPolyhedralConvexShape, %class.btVector3, %class.btVector3, i8, [3 x i8] }>
%class.btSerializer = type opaque

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN16btCollisionShapeC2Ev = comdat any

$_ZN16btCollisionShapeD2Ev = comdat any

$_ZmlRK9btVector3RK11btMatrix3x3 = comdat any

$_ZNK11btTransform8getBasisEv = comdat any

$_ZNK11btTransformclERK9btVector3 = comdat any

$_ZngRK9btVector3 = comdat any

$_ZNK9btVector33dotERKS_ = comdat any

$_ZN9btVector3C2ERKfS1_S1_ = comdat any

$_ZNK21btConvexInternalShape26getImplicitShapeDimensionsEv = comdat any

$_Z6btFselfff = comdat any

$_ZNK9btVector31xEv = comdat any

$_ZNK9btVector31yEv = comdat any

$_ZNK9btVector31zEv = comdat any

$_ZNK9btVector34getXEv = comdat any

$_ZNK9btVector34getYEv = comdat any

$_ZNK9btVector34getZEv = comdat any

$_ZNK9btVector34dot3ERKS_S1_S1_ = comdat any

$_ZNK9btVector37maxAxisEv = comdat any

$_ZNK15btCylinderShape9getUpAxisEv = comdat any

$_ZN9btVector3cvPfEv = comdat any

$_ZN9btVector3C2Ev = comdat any

$_Z6btSqrtf = comdat any

$_ZNK14btCapsuleShape13getHalfHeightEv = comdat any

$_ZNK14btCapsuleShape9getUpAxisEv = comdat any

$_ZNK9btVector37length2Ev = comdat any

$_ZN9btVector38setValueERKfS1_S1_ = comdat any

$_ZN9btVector3mLERKf = comdat any

$_ZN23btConvexPointCloudShape17getUnscaledPointsEv = comdat any

$_ZNK23btConvexPointCloudShape12getNumPointsEv = comdat any

$_ZNK21btConvexInternalShape17getLocalScalingNVEv = comdat any

$_ZN17btConvexHullShape17getUnscaledPointsEv = comdat any

$_ZNK17btConvexHullShape12getNumPointsEv = comdat any

$_ZN9btVector39normalizeEv = comdat any

$_ZplRK9btVector3S1_ = comdat any

$_ZmlRKfRK9btVector3 = comdat any

$_ZNK13btSphereShape9getRadiusEv = comdat any

$_ZNK21btConvexInternalShape11getMarginNVEv = comdat any

$_ZNK11btTransform9getOriginEv = comdat any

$_ZmiRK9btVector3S1_ = comdat any

$_ZN9btVector3pLERKS_ = comdat any

$_ZNK11btMatrix3x38absoluteEv = comdat any

$_ZN11btMatrix3x3ixEi = comdat any

$_ZNK14btCapsuleShape9getRadiusEv = comdat any

$_ZNK34btPolyhedralConvexAabbCachingShape17getNonvirtualAabbERK11btTransformR9btVector3S4_f = comdat any

$_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv = comdat any

$_ZNK16btCollisionShape28calculateSerializeBufferSizeEv = comdat any

$_ZNK11btMatrix3x35tdotxERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotyERK9btVector3 = comdat any

$_ZNK11btMatrix3x35tdotzERK9btVector3 = comdat any

$_ZNK11btMatrix3x3ixEi = comdat any

$_ZNK9btVector3cvPKfEv = comdat any

$_ZmlRK9btVector3S1_ = comdat any

$_ZNK9btVector36maxDotEPKS_lRf = comdat any

$_ZN20btAlignedObjectArrayI9btVector3EixEi = comdat any

$_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv = comdat any

$_ZNK9btVector36lengthEv = comdat any

$_ZN9btVector3dVERKf = comdat any

$_ZmlRK9btVector3RKf = comdat any

$_Z6btFabsf = comdat any

$_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_ = comdat any

$_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV13btConvexShape = hidden unnamed_addr constant { [25 x i8*] } { [25 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI13btConvexShape to i8*), i8* bitcast (%class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD1Ev to i8*), i8* bitcast (void (%class.btConvexShape*)* @_ZN13btConvexShapeD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btVector3*, float*)* @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf to i8*), i8* bitcast (float (%class.btCollisionShape*)* @_ZNK16btCollisionShape20getAngularMotionDiscEv to i8*), i8* bitcast (float (%class.btCollisionShape*, float)* @_ZNK16btCollisionShape27getContactBreakingThresholdEf to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btVector3*, %class.btCollisionShape*)* @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (i32 (%class.btCollisionShape*)* @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv to i8*), i8* bitcast (i8* (%class.btCollisionShape*, i8*, %class.btSerializer*)* @_ZNK16btCollisionShape9serializeEPvP12btSerializer to i8*), i8* bitcast (void (%class.btCollisionShape*, %class.btSerializer*)* @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, float*, float*, %class.btVector3*, %class.btVector3*)* @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_ to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS13btConvexShape = hidden constant [16 x i8] c"13btConvexShape\00", align 1
@_ZTI16btCollisionShape = external constant i8*
@_ZTI13btConvexShape = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([16 x i8], [16 x i8]* @_ZTS13btConvexShape, i32 0, i32 0), i8* bitcast (i8** @_ZTI16btCollisionShape to i8*) }, align 4
@_ZTV16btCollisionShape = external unnamed_addr constant { [18 x i8*] }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btConvexShape.cpp, i8* null }]

@_ZN13btConvexShapeD1Ev = hidden unnamed_addr alias %class.btConvexShape* (%class.btConvexShape*), %class.btConvexShape* (%class.btConvexShape*)* @_ZN13btConvexShapeD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btConvexShape* @_ZN13btConvexShapeC2Ev(%class.btConvexShape* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvexShape*, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  %0 = bitcast %class.btConvexShape* %this1 to %class.btCollisionShape*
  %call = call %class.btCollisionShape* @_ZN16btCollisionShapeC2Ev(%class.btCollisionShape* %0)
  %1 = bitcast %class.btConvexShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [25 x i8*] }, { [25 x i8*] }* @_ZTV13btConvexShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  ret %class.btConvexShape* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZN16btCollisionShapeC2Ev(%class.btCollisionShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %0 = bitcast %class.btCollisionShape* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV16btCollisionShape, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  store i32 35, i32* %m_shapeType, align 4
  %m_userPointer = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 2
  store i8* null, i8** %m_userPointer, align 4
  %m_userIndex = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 3
  store i32 -1, i32* %m_userIndex, align 4
  ret %class.btCollisionShape* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btConvexShape* @_ZN13btConvexShapeD2Ev(%class.btConvexShape* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvexShape*, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  %0 = bitcast %class.btConvexShape* %this1 to %class.btCollisionShape*
  %call = call %class.btCollisionShape* @_ZN16btCollisionShapeD2Ev(%class.btCollisionShape* %0) #7
  ret %class.btConvexShape* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZN16btCollisionShapeD2Ev(%class.btCollisionShape* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  ret %class.btCollisionShape* %this1
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN13btConvexShapeD0Ev(%class.btConvexShape* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btConvexShape*, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  call void @llvm.trap() #8
  unreachable
}

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #3

; Function Attrs: noinline optnone
define hidden void @_ZNK13btConvexShape7projectERK11btTransformRK9btVector3RfS6_RS3_S7_(%class.btConvexShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %dir, float* nonnull align 4 dereferenceable(4) %min, float* nonnull align 4 dereferenceable(4) %max, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMin, %class.btVector3* nonnull align 4 dereferenceable(16) %witnesPtMax) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btConvexShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %dir.addr = alloca %class.btVector3*, align 4
  %min.addr = alloca float*, align 4
  %max.addr = alloca float*, align 4
  %witnesPtMin.addr = alloca %class.btVector3*, align 4
  %witnesPtMax.addr = alloca %class.btVector3*, align 4
  %localAxis = alloca %class.btVector3, align 4
  %vtx1 = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %vtx2 = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %ref.tmp3 = alloca %class.btVector3, align 4
  %tmp = alloca float, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4
  store %class.btVector3* %dir, %class.btVector3** %dir.addr, align 4
  store float* %min, float** %min.addr, align 4
  store float* %max, float** %max.addr, align 4
  store %class.btVector3* %witnesPtMin, %class.btVector3** %witnesPtMin.addr, align 4
  store %class.btVector3* %witnesPtMax, %class.btVector3** %witnesPtMax.addr, align 4
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %call = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %1)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %localAxis, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call)
  %2 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %3 = bitcast %class.btConvexShape* %this1 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable, i64 16
  %4 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn, align 4
  call void %4(%class.btVector3* sret align 4 %ref.tmp, %class.btConvexShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %localAxis)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %vtx1, %class.btTransform* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp)
  %5 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  call void @_ZngRK9btVector3(%class.btVector3* sret align 4 %ref.tmp3, %class.btVector3* nonnull align 4 dereferenceable(16) %localAxis)
  %6 = bitcast %class.btConvexShape* %this1 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable4 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %6, align 4
  %vfn5 = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable4, i64 16
  %7 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn5, align 4
  call void %7(%class.btVector3* sret align 4 %ref.tmp2, %class.btConvexShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp3)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %vtx2, %class.btTransform* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  %8 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4
  %call6 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %vtx1, %class.btVector3* nonnull align 4 dereferenceable(16) %8)
  %9 = load float*, float** %min.addr, align 4
  store float %call6, float* %9, align 4
  %10 = load %class.btVector3*, %class.btVector3** %dir.addr, align 4
  %call7 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %vtx2, %class.btVector3* nonnull align 4 dereferenceable(16) %10)
  %11 = load float*, float** %max.addr, align 4
  store float %call7, float* %11, align 4
  %12 = load %class.btVector3*, %class.btVector3** %witnesPtMax.addr, align 4
  %13 = bitcast %class.btVector3* %12 to i8*
  %14 = bitcast %class.btVector3* %vtx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %13, i8* align 4 %14, i32 16, i1 false)
  %15 = load %class.btVector3*, %class.btVector3** %witnesPtMin.addr, align 4
  %16 = bitcast %class.btVector3* %15 to i8*
  %17 = bitcast %class.btVector3* %vtx1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  %18 = load float*, float** %min.addr, align 4
  %19 = load float, float* %18, align 4
  %20 = load float*, float** %max.addr, align 4
  %21 = load float, float* %20, align 4
  %cmp = fcmp ogt float %19, %21
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %22 = load float*, float** %min.addr, align 4
  %23 = load float, float* %22, align 4
  store float %23, float* %tmp, align 4
  %24 = load float*, float** %max.addr, align 4
  %25 = load float, float* %24, align 4
  %26 = load float*, float** %min.addr, align 4
  store float %25, float* %26, align 4
  %27 = load float, float* %tmp, align 4
  %28 = load float*, float** %max.addr, align 4
  store float %27, float* %28, align 4
  %29 = load %class.btVector3*, %class.btVector3** %witnesPtMax.addr, align 4
  %30 = bitcast %class.btVector3* %29 to i8*
  %31 = bitcast %class.btVector3* %vtx1 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 16, i1 false)
  %32 = load %class.btVector3*, %class.btVector3** %witnesPtMin.addr, align 4
  %33 = bitcast %class.btVector3* %32 to i8*
  %34 = bitcast %class.btVector3* %vtx2 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %m) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %m.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store %class.btMatrix3x3* %m, %class.btMatrix3x3** %m.addr, align 4
  %0 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call = call float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call, float* %ref.tmp, align 4
  %2 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  store float %call2, float* %ref.tmp1, align 4
  %4 = load %class.btMatrix3x3*, %class.btMatrix3x3** %m.addr, align 4
  %5 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call4 = call float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %4, %class.btVector3* nonnull align 4 dereferenceable(16) %5)
  store float %call4, float* %ref.tmp3, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  ret %class.btMatrix3x3* %m_basis
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btTransformclERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btTransform* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %x) #2 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  %x.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  store %class.btVector3* %x, %class.btVector3** %x.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %x.addr, align 4
  %m_basis = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis, i32 0)
  %m_basis2 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis2, i32 1)
  %m_basis4 = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 0
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %m_basis4, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* %0, %class.btVector3* nonnull align 4 dereferenceable(16) %call, %class.btVector3* nonnull align 4 dereferenceable(16) %call3, %class.btVector3* nonnull align 4 dereferenceable(16) %call5)
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %m_origin)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZngRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %fneg = fneg float %1
  store float %fneg, float* %ref.tmp, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %3 = load float, float* %arrayidx3, align 4
  %fneg4 = fneg float %3
  store float %fneg4, float* %ref.tmp1, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %5 = load float, float* %arrayidx7, align 4
  %fneg8 = fneg float %5
  store float %fneg8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector33dotERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %mul = fmul float %0, %2
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %3 = load float, float* %arrayidx5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %3, %5
  %add = fadd float %mul, %mul8
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %6 = load float, float* %arrayidx10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %7, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %mul13 = fmul float %6, %8
  %add14 = fadd float %add, %mul13
  ret float %add14
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i32, i1 immarg) #4

; Function Attrs: noinline optnone
define hidden void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %localDir) #2 {
entry:
  %this.addr = alloca %class.btConvexShape*, align 4
  %localDir.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %convexShape = alloca %class.btBoxShape*, align 4
  %halfExtents = alloca %class.btVector3*, align 4
  %ref.tmp6 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %ref.tmp17 = alloca float, align 4
  %triangleShape = alloca %class.btTriangleShape*, align 4
  %dir = alloca %class.btVector3, align 4
  %vertices = alloca %class.btVector3*, align 4
  %dots = alloca %class.btVector3, align 4
  %sup = alloca %class.btVector3, align 4
  %cylShape = alloca %class.btCylinderShape*, align 4
  %halfExtents39 = alloca %class.btVector3, align 4
  %v = alloca %class.btVector3, align 4
  %cylinderUpAxis = alloca i32, align 4
  %XX = alloca i32, align 4
  %YY = alloca i32, align 4
  %ZZ = alloca i32, align 4
  %radius = alloca float, align 4
  %halfHeight = alloca float, align 4
  %tmp = alloca %class.btVector3, align 4
  %d = alloca float, align 4
  %s = alloca float, align 4
  %vec0 = alloca %class.btVector3, align 4
  %capsuleShape = alloca %class.btCapsuleShape*, align 4
  %halfHeight108 = alloca float, align 4
  %capsuleUpAxis = alloca i32, align 4
  %supVec = alloca %class.btVector3, align 4
  %ref.tmp111 = alloca float, align 4
  %ref.tmp112 = alloca float, align 4
  %ref.tmp113 = alloca float, align 4
  %maxDot = alloca float, align 4
  %vec = alloca %class.btVector3, align 4
  %lenSqr = alloca float, align 4
  %ref.tmp118 = alloca float, align 4
  %ref.tmp119 = alloca float, align 4
  %ref.tmp120 = alloca float, align 4
  %rlen = alloca float, align 4
  %vtx = alloca %class.btVector3, align 4
  %newDot = alloca float, align 4
  %pos = alloca %class.btVector3, align 4
  %ref.tmp126 = alloca float, align 4
  %ref.tmp127 = alloca float, align 4
  %ref.tmp128 = alloca float, align 4
  %pos136 = alloca %class.btVector3, align 4
  %ref.tmp137 = alloca float, align 4
  %ref.tmp138 = alloca float, align 4
  %ref.tmp139 = alloca float, align 4
  %convexPointCloudShape = alloca %class.btConvexPointCloudShape*, align 4
  %points = alloca %class.btVector3*, align 4
  %numPoints = alloca i32, align 4
  %convexHullShape = alloca %class.btConvexHullShape*, align 4
  %points157 = alloca %class.btVector3*, align 4
  %numPoints159 = alloca i32, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4
  store %class.btVector3* %localDir, %class.btVector3** %localDir.addr, align 4
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  %0 = bitcast %class.btConvexShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %0, i32 0, i32 1
  %1 = load i32, i32* %m_shapeType, align 4
  switch i32 %1, label %sw.default162 [
    i32 8, label %sw.bb
    i32 0, label %sw.bb4
    i32 1, label %sw.bb24
    i32 13, label %sw.bb38
    i32 10, label %sw.bb103
    i32 5, label %sw.bb152
    i32 4, label %sw.bb156
  ]

sw.bb:                                            ; preds = %entry
  store float 0.000000e+00, float* %ref.tmp, align 4
  store float 0.000000e+00, float* %ref.tmp2, align 4
  store float 0.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  br label %return

sw.bb4:                                           ; preds = %entry
  %2 = bitcast %class.btConvexShape* %this1 to %class.btBoxShape*
  store %class.btBoxShape* %2, %class.btBoxShape** %convexShape, align 4
  %3 = load %class.btBoxShape*, %class.btBoxShape** %convexShape, align 4
  %4 = bitcast %class.btBoxShape* %3 to %class.btConvexInternalShape*
  %call5 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape26getImplicitShapeDimensionsEv(%class.btConvexInternalShape* %4)
  store %class.btVector3* %call5, %class.btVector3** %halfExtents, align 4
  %5 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %call7 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %5)
  %6 = load float, float* %call7, align 4
  %7 = load %class.btVector3*, %class.btVector3** %halfExtents, align 4
  %call8 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %7)
  %8 = load float, float* %call8, align 4
  %9 = load %class.btVector3*, %class.btVector3** %halfExtents, align 4
  %call9 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %9)
  %10 = load float, float* %call9, align 4
  %fneg = fneg float %10
  %call10 = call float @_Z6btFselfff(float %6, float %8, float %fneg)
  store float %call10, float* %ref.tmp6, align 4
  %11 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %11)
  %12 = load float, float* %call12, align 4
  %13 = load %class.btVector3*, %class.btVector3** %halfExtents, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %13)
  %14 = load float, float* %call13, align 4
  %15 = load %class.btVector3*, %class.btVector3** %halfExtents, align 4
  %call14 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %15)
  %16 = load float, float* %call14, align 4
  %fneg15 = fneg float %16
  %call16 = call float @_Z6btFselfff(float %12, float %14, float %fneg15)
  store float %call16, float* %ref.tmp11, align 4
  %17 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %call18 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %17)
  %18 = load float, float* %call18, align 4
  %19 = load %class.btVector3*, %class.btVector3** %halfExtents, align 4
  %call19 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %19)
  %20 = load float, float* %call19, align 4
  %21 = load %class.btVector3*, %class.btVector3** %halfExtents, align 4
  %call20 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %21)
  %22 = load float, float* %call20, align 4
  %fneg21 = fneg float %22
  %call22 = call float @_Z6btFselfff(float %18, float %20, float %fneg21)
  store float %call22, float* %ref.tmp17, align 4
  %call23 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp11, float* nonnull align 4 dereferenceable(4) %ref.tmp17)
  br label %return

sw.bb24:                                          ; preds = %entry
  %23 = bitcast %class.btConvexShape* %this1 to %class.btTriangleShape*
  store %class.btTriangleShape* %23, %class.btTriangleShape** %triangleShape, align 4
  %24 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %call25 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %24)
  %25 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %25)
  %26 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %call27 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %26)
  %call28 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %dir, float* nonnull align 4 dereferenceable(4) %call25, float* nonnull align 4 dereferenceable(4) %call26, float* nonnull align 4 dereferenceable(4) %call27)
  %27 = load %class.btTriangleShape*, %class.btTriangleShape** %triangleShape, align 4
  %m_vertices1 = getelementptr inbounds %class.btTriangleShape, %class.btTriangleShape* %27, i32 0, i32 1
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_vertices1, i32 0, i32 0
  store %class.btVector3* %arrayidx, %class.btVector3** %vertices, align 4
  %28 = load %class.btVector3*, %class.btVector3** %vertices, align 4
  %arrayidx29 = getelementptr inbounds %class.btVector3, %class.btVector3* %28, i32 0
  %29 = load %class.btVector3*, %class.btVector3** %vertices, align 4
  %arrayidx30 = getelementptr inbounds %class.btVector3, %class.btVector3* %29, i32 1
  %30 = load %class.btVector3*, %class.btVector3** %vertices, align 4
  %arrayidx31 = getelementptr inbounds %class.btVector3, %class.btVector3* %30, i32 2
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %dots, %class.btVector3* %dir, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx29, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx30, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx31)
  %31 = load %class.btVector3*, %class.btVector3** %vertices, align 4
  %call32 = call i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %dots)
  %arrayidx33 = getelementptr inbounds %class.btVector3, %class.btVector3* %31, i32 %call32
  %32 = bitcast %class.btVector3* %sup to i8*
  %33 = bitcast %class.btVector3* %arrayidx33 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %32, i8* align 4 %33, i32 16, i1 false)
  %call34 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %sup)
  %call35 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %sup)
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %sup)
  %call37 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call34, float* nonnull align 4 dereferenceable(4) %call35, float* nonnull align 4 dereferenceable(4) %call36)
  br label %return

sw.bb38:                                          ; preds = %entry
  %34 = bitcast %class.btConvexShape* %this1 to %class.btCylinderShape*
  store %class.btCylinderShape* %34, %class.btCylinderShape** %cylShape, align 4
  %35 = load %class.btCylinderShape*, %class.btCylinderShape** %cylShape, align 4
  %36 = bitcast %class.btCylinderShape* %35 to %class.btConvexInternalShape*
  %call40 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape26getImplicitShapeDimensionsEv(%class.btConvexInternalShape* %36)
  %37 = bitcast %class.btVector3* %halfExtents39 to i8*
  %38 = bitcast %class.btVector3* %call40 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %37, i8* align 4 %38, i32 16, i1 false)
  %39 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %39)
  %40 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %call42 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %40)
  %41 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %call43 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %41)
  %call44 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %v, float* nonnull align 4 dereferenceable(4) %call41, float* nonnull align 4 dereferenceable(4) %call42, float* nonnull align 4 dereferenceable(4) %call43)
  %42 = load %class.btCylinderShape*, %class.btCylinderShape** %cylShape, align 4
  %call45 = call i32 @_ZNK15btCylinderShape9getUpAxisEv(%class.btCylinderShape* %42)
  store i32 %call45, i32* %cylinderUpAxis, align 4
  store i32 1, i32* %XX, align 4
  store i32 0, i32* %YY, align 4
  store i32 2, i32* %ZZ, align 4
  %43 = load i32, i32* %cylinderUpAxis, align 4
  switch i32 %43, label %sw.default [
    i32 0, label %sw.bb46
    i32 1, label %sw.bb47
    i32 2, label %sw.bb48
  ]

sw.bb46:                                          ; preds = %sw.bb38
  store i32 1, i32* %XX, align 4
  store i32 0, i32* %YY, align 4
  store i32 2, i32* %ZZ, align 4
  br label %sw.epilog

sw.bb47:                                          ; preds = %sw.bb38
  store i32 0, i32* %XX, align 4
  store i32 1, i32* %YY, align 4
  store i32 2, i32* %ZZ, align 4
  br label %sw.epilog

sw.bb48:                                          ; preds = %sw.bb38
  store i32 0, i32* %XX, align 4
  store i32 2, i32* %YY, align 4
  store i32 1, i32* %ZZ, align 4
  br label %sw.epilog

sw.default:                                       ; preds = %sw.bb38
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb48, %sw.bb47, %sw.bb46
  %call49 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents39)
  %44 = load i32, i32* %XX, align 4
  %arrayidx50 = getelementptr inbounds float, float* %call49, i32 %44
  %45 = load float, float* %arrayidx50, align 4
  store float %45, float* %radius, align 4
  %call51 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents39)
  %46 = load i32, i32* %cylinderUpAxis, align 4
  %arrayidx52 = getelementptr inbounds float, float* %call51, i32 %46
  %47 = load float, float* %arrayidx52, align 4
  store float %47, float* %halfHeight, align 4
  %call53 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %tmp)
  %call54 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %48 = load i32, i32* %XX, align 4
  %arrayidx55 = getelementptr inbounds float, float* %call54, i32 %48
  %49 = load float, float* %arrayidx55, align 4
  %call56 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %50 = load i32, i32* %XX, align 4
  %arrayidx57 = getelementptr inbounds float, float* %call56, i32 %50
  %51 = load float, float* %arrayidx57, align 4
  %mul = fmul float %49, %51
  %call58 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %52 = load i32, i32* %ZZ, align 4
  %arrayidx59 = getelementptr inbounds float, float* %call58, i32 %52
  %53 = load float, float* %arrayidx59, align 4
  %call60 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %54 = load i32, i32* %ZZ, align 4
  %arrayidx61 = getelementptr inbounds float, float* %call60, i32 %54
  %55 = load float, float* %arrayidx61, align 4
  %mul62 = fmul float %53, %55
  %add = fadd float %mul, %mul62
  %call63 = call float @_Z6btSqrtf(float %add)
  store float %call63, float* %s, align 4
  %56 = load float, float* %s, align 4
  %cmp = fcmp une float %56, 0.000000e+00
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %sw.epilog
  %57 = load float, float* %radius, align 4
  %58 = load float, float* %s, align 4
  %div = fdiv float %57, %58
  store float %div, float* %d, align 4
  %call64 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %59 = load i32, i32* %XX, align 4
  %arrayidx65 = getelementptr inbounds float, float* %call64, i32 %59
  %60 = load float, float* %arrayidx65, align 4
  %61 = load float, float* %d, align 4
  %mul66 = fmul float %60, %61
  %call67 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %62 = load i32, i32* %XX, align 4
  %arrayidx68 = getelementptr inbounds float, float* %call67, i32 %62
  store float %mul66, float* %arrayidx68, align 4
  %call69 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %63 = load i32, i32* %YY, align 4
  %arrayidx70 = getelementptr inbounds float, float* %call69, i32 %63
  %64 = load float, float* %arrayidx70, align 4
  %conv = fpext float %64 to double
  %cmp71 = fcmp olt double %conv, 0.000000e+00
  br i1 %cmp71, label %cond.true, label %cond.false

cond.true:                                        ; preds = %if.then
  %65 = load float, float* %halfHeight, align 4
  %fneg72 = fneg float %65
  br label %cond.end

cond.false:                                       ; preds = %if.then
  %66 = load float, float* %halfHeight, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %fneg72, %cond.true ], [ %66, %cond.false ]
  %call73 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %67 = load i32, i32* %YY, align 4
  %arrayidx74 = getelementptr inbounds float, float* %call73, i32 %67
  store float %cond, float* %arrayidx74, align 4
  %call75 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %68 = load i32, i32* %ZZ, align 4
  %arrayidx76 = getelementptr inbounds float, float* %call75, i32 %68
  %69 = load float, float* %arrayidx76, align 4
  %70 = load float, float* %d, align 4
  %mul77 = fmul float %69, %70
  %call78 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %71 = load i32, i32* %ZZ, align 4
  %arrayidx79 = getelementptr inbounds float, float* %call78, i32 %71
  store float %mul77, float* %arrayidx79, align 4
  %call80 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %tmp)
  %call81 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %tmp)
  %call82 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %tmp)
  %call83 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call80, float* nonnull align 4 dereferenceable(4) %call81, float* nonnull align 4 dereferenceable(4) %call82)
  br label %return

if.else:                                          ; preds = %sw.epilog
  %72 = load float, float* %radius, align 4
  %call84 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %73 = load i32, i32* %XX, align 4
  %arrayidx85 = getelementptr inbounds float, float* %call84, i32 %73
  store float %72, float* %arrayidx85, align 4
  %call86 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %v)
  %74 = load i32, i32* %YY, align 4
  %arrayidx87 = getelementptr inbounds float, float* %call86, i32 %74
  %75 = load float, float* %arrayidx87, align 4
  %conv88 = fpext float %75 to double
  %cmp89 = fcmp olt double %conv88, 0.000000e+00
  br i1 %cmp89, label %cond.true90, label %cond.false92

cond.true90:                                      ; preds = %if.else
  %76 = load float, float* %halfHeight, align 4
  %fneg91 = fneg float %76
  br label %cond.end93

cond.false92:                                     ; preds = %if.else
  %77 = load float, float* %halfHeight, align 4
  br label %cond.end93

cond.end93:                                       ; preds = %cond.false92, %cond.true90
  %cond94 = phi float [ %fneg91, %cond.true90 ], [ %77, %cond.false92 ]
  %call95 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %78 = load i32, i32* %YY, align 4
  %arrayidx96 = getelementptr inbounds float, float* %call95, i32 %78
  store float %cond94, float* %arrayidx96, align 4
  %call97 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %79 = load i32, i32* %ZZ, align 4
  %arrayidx98 = getelementptr inbounds float, float* %call97, i32 %79
  store float 0.000000e+00, float* %arrayidx98, align 4
  %call99 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %tmp)
  %call100 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %tmp)
  %call101 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %tmp)
  %call102 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call99, float* nonnull align 4 dereferenceable(4) %call100, float* nonnull align 4 dereferenceable(4) %call101)
  br label %return

sw.bb103:                                         ; preds = %entry
  %80 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %call104 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %80)
  %81 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %call105 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %81)
  %82 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %call106 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %82)
  %call107 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %vec0, float* nonnull align 4 dereferenceable(4) %call104, float* nonnull align 4 dereferenceable(4) %call105, float* nonnull align 4 dereferenceable(4) %call106)
  %83 = bitcast %class.btConvexShape* %this1 to %class.btCapsuleShape*
  store %class.btCapsuleShape* %83, %class.btCapsuleShape** %capsuleShape, align 4
  %84 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4
  %call109 = call float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %84)
  store float %call109, float* %halfHeight108, align 4
  %85 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4
  %call110 = call i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %85)
  store i32 %call110, i32* %capsuleUpAxis, align 4
  store float 0.000000e+00, float* %ref.tmp111, align 4
  store float 0.000000e+00, float* %ref.tmp112, align 4
  store float 0.000000e+00, float* %ref.tmp113, align 4
  %call114 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %supVec, float* nonnull align 4 dereferenceable(4) %ref.tmp111, float* nonnull align 4 dereferenceable(4) %ref.tmp112, float* nonnull align 4 dereferenceable(4) %ref.tmp113)
  store float 0xC3ABC16D60000000, float* %maxDot, align 4
  %86 = bitcast %class.btVector3* %vec to i8*
  %87 = bitcast %class.btVector3* %vec0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %86, i8* align 4 %87, i32 16, i1 false)
  %call115 = call float @_ZNK9btVector37length2Ev(%class.btVector3* %vec)
  store float %call115, float* %lenSqr, align 4
  %88 = load float, float* %lenSqr, align 4
  %cmp116 = fcmp olt float %88, 0x3D10000000000000
  br i1 %cmp116, label %if.then117, label %if.else121

if.then117:                                       ; preds = %sw.bb103
  store float 1.000000e+00, float* %ref.tmp118, align 4
  store float 0.000000e+00, float* %ref.tmp119, align 4
  store float 0.000000e+00, float* %ref.tmp120, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %ref.tmp118, float* nonnull align 4 dereferenceable(4) %ref.tmp119, float* nonnull align 4 dereferenceable(4) %ref.tmp120)
  br label %if.end

if.else121:                                       ; preds = %sw.bb103
  %89 = load float, float* %lenSqr, align 4
  %call122 = call float @_Z6btSqrtf(float %89)
  %div123 = fdiv float 1.000000e+00, %call122
  store float %div123, float* %rlen, align 4
  %call124 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %rlen)
  br label %if.end

if.end:                                           ; preds = %if.else121, %if.then117
  %call125 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %vtx)
  store float 0.000000e+00, float* %ref.tmp126, align 4
  store float 0.000000e+00, float* %ref.tmp127, align 4
  store float 0.000000e+00, float* %ref.tmp128, align 4
  %call129 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %pos, float* nonnull align 4 dereferenceable(4) %ref.tmp126, float* nonnull align 4 dereferenceable(4) %ref.tmp127, float* nonnull align 4 dereferenceable(4) %ref.tmp128)
  %90 = load float, float* %halfHeight108, align 4
  %call130 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pos)
  %91 = load i32, i32* %capsuleUpAxis, align 4
  %arrayidx131 = getelementptr inbounds float, float* %call130, i32 %91
  store float %90, float* %arrayidx131, align 4
  %92 = bitcast %class.btVector3* %vtx to i8*
  %93 = bitcast %class.btVector3* %pos to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %92, i8* align 4 %93, i32 16, i1 false)
  %call132 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx)
  store float %call132, float* %newDot, align 4
  %94 = load float, float* %newDot, align 4
  %95 = load float, float* %maxDot, align 4
  %cmp133 = fcmp ogt float %94, %95
  br i1 %cmp133, label %if.then134, label %if.end135

if.then134:                                       ; preds = %if.end
  %96 = load float, float* %newDot, align 4
  store float %96, float* %maxDot, align 4
  %97 = bitcast %class.btVector3* %supVec to i8*
  %98 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %97, i8* align 4 %98, i32 16, i1 false)
  br label %if.end135

if.end135:                                        ; preds = %if.then134, %if.end
  store float 0.000000e+00, float* %ref.tmp137, align 4
  store float 0.000000e+00, float* %ref.tmp138, align 4
  store float 0.000000e+00, float* %ref.tmp139, align 4
  %call140 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %pos136, float* nonnull align 4 dereferenceable(4) %ref.tmp137, float* nonnull align 4 dereferenceable(4) %ref.tmp138, float* nonnull align 4 dereferenceable(4) %ref.tmp139)
  %99 = load float, float* %halfHeight108, align 4
  %fneg141 = fneg float %99
  %call142 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %pos136)
  %100 = load i32, i32* %capsuleUpAxis, align 4
  %arrayidx143 = getelementptr inbounds float, float* %call142, i32 %100
  store float %fneg141, float* %arrayidx143, align 4
  %101 = bitcast %class.btVector3* %vtx to i8*
  %102 = bitcast %class.btVector3* %pos136 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %101, i8* align 4 %102, i32 16, i1 false)
  %call144 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %vtx)
  store float %call144, float* %newDot, align 4
  %103 = load float, float* %newDot, align 4
  %104 = load float, float* %maxDot, align 4
  %cmp145 = fcmp ogt float %103, %104
  br i1 %cmp145, label %if.then146, label %if.end147

if.then146:                                       ; preds = %if.end135
  %105 = load float, float* %newDot, align 4
  store float %105, float* %maxDot, align 4
  %106 = bitcast %class.btVector3* %supVec to i8*
  %107 = bitcast %class.btVector3* %vtx to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %106, i8* align 4 %107, i32 16, i1 false)
  br label %if.end147

if.end147:                                        ; preds = %if.then146, %if.end135
  %call148 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %supVec)
  %call149 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %supVec)
  %call150 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %supVec)
  %call151 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %call148, float* nonnull align 4 dereferenceable(4) %call149, float* nonnull align 4 dereferenceable(4) %call150)
  br label %return

sw.bb152:                                         ; preds = %entry
  %108 = bitcast %class.btConvexShape* %this1 to %class.btConvexPointCloudShape*
  store %class.btConvexPointCloudShape* %108, %class.btConvexPointCloudShape** %convexPointCloudShape, align 4
  %109 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %convexPointCloudShape, align 4
  %call153 = call %class.btVector3* @_ZN23btConvexPointCloudShape17getUnscaledPointsEv(%class.btConvexPointCloudShape* %109)
  store %class.btVector3* %call153, %class.btVector3** %points, align 4
  %110 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %convexPointCloudShape, align 4
  %call154 = call i32 @_ZNK23btConvexPointCloudShape12getNumPointsEv(%class.btConvexPointCloudShape* %110)
  store i32 %call154, i32* %numPoints, align 4
  %111 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %112 = load %class.btVector3*, %class.btVector3** %points, align 4
  %113 = load i32, i32* %numPoints, align 4
  %114 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %convexPointCloudShape, align 4
  %115 = bitcast %class.btConvexPointCloudShape* %114 to %class.btConvexInternalShape*
  %call155 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape17getLocalScalingNVEv(%class.btConvexInternalShape* %115)
  call void @_ZL17convexHullSupportRK9btVector3PS0_iS1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %111, %class.btVector3* %112, i32 %113, %class.btVector3* nonnull align 4 dereferenceable(16) %call155)
  br label %return

sw.bb156:                                         ; preds = %entry
  %116 = bitcast %class.btConvexShape* %this1 to %class.btConvexHullShape*
  store %class.btConvexHullShape* %116, %class.btConvexHullShape** %convexHullShape, align 4
  %117 = load %class.btConvexHullShape*, %class.btConvexHullShape** %convexHullShape, align 4
  %call158 = call %class.btVector3* @_ZN17btConvexHullShape17getUnscaledPointsEv(%class.btConvexHullShape* %117)
  store %class.btVector3* %call158, %class.btVector3** %points157, align 4
  %118 = load %class.btConvexHullShape*, %class.btConvexHullShape** %convexHullShape, align 4
  %call160 = call i32 @_ZNK17btConvexHullShape12getNumPointsEv(%class.btConvexHullShape* %118)
  store i32 %call160, i32* %numPoints159, align 4
  %119 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %120 = load %class.btVector3*, %class.btVector3** %points157, align 4
  %121 = load i32, i32* %numPoints159, align 4
  %122 = load %class.btConvexHullShape*, %class.btConvexHullShape** %convexHullShape, align 4
  %123 = bitcast %class.btConvexHullShape* %122 to %class.btConvexInternalShape*
  %call161 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape17getLocalScalingNVEv(%class.btConvexInternalShape* %123)
  call void @_ZL17convexHullSupportRK9btVector3PS0_iS1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %119, %class.btVector3* %120, i32 %121, %class.btVector3* nonnull align 4 dereferenceable(16) %call161)
  br label %return

sw.default162:                                    ; preds = %entry
  %124 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %125 = bitcast %class.btConvexShape* %this1 to void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)***
  %vtable = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)**, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*** %125, align 4
  %vfn = getelementptr inbounds void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vtable, i64 17
  %126 = load void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)*, void (%class.btVector3*, %class.btConvexShape*, %class.btVector3*)** %vfn, align 4
  call void %126(%class.btVector3* sret align 4 %agg.result, %class.btConvexShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %124)
  br label %return

return:                                           ; preds = %sw.default162, %sw.bb156, %sw.bb152, %if.end147, %cond.end93, %cond.end, %sw.bb24, %sw.bb4, %sw.bb
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* returned %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape26getImplicitShapeDimensionsEv(%class.btConvexInternalShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 2
  ret %class.btVector3* %m_implicitShapeDimensions
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFselfff(float %a, float %b, float %c) #1 comdat {
entry:
  %a.addr = alloca float, align 4
  %b.addr = alloca float, align 4
  %c.addr = alloca float, align 4
  store float %a, float* %a.addr, align 4
  store float %b, float* %b.addr, align 4
  store float %c, float* %c.addr, align 4
  %0 = load float, float* %a.addr, align 4
  %cmp = fcmp oge float %0, 0.000000e+00
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load float, float* %b.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %2 = load float, float* %c.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %1, %cond.true ], [ %2, %cond.false ]
  ret float %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getYEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 1
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getZEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 2
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v0, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v0.addr = alloca %class.btVector3*, align 4
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp4 = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v0, %class.btVector3** %v0.addr, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v0.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %0)
  store float %call, float* %ref.tmp, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %call3 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  store float %call3, float* %ref.tmp2, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %call5 = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %2)
  store float %call5, float* %ref.tmp4, align 4
  %call6 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp4)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVector37maxAxisEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %0 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %1 = load float, float* %arrayidx3, align 4
  %cmp = fcmp olt float %0, %1
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %2 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %3 = load float, float* %arrayidx7, align 4
  %cmp8 = fcmp olt float %2, %3
  %4 = zext i1 %cmp8 to i64
  %cond = select i1 %cmp8, i32 2, i32 1
  br label %cond.end

cond.false:                                       ; preds = %entry
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 0
  %5 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %6 = load float, float* %arrayidx12, align 4
  %cmp13 = fcmp olt float %5, %6
  %7 = zext i1 %cmp13 to i64
  %cond14 = select i1 %cmp13, i32 2, i32 0
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond15 = phi i32 [ %cond, %cond.true ], [ %cond14, %cond.false ]
  ret i32 %cond15
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK15btCylinderShape9getUpAxisEv(%class.btCylinderShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCylinderShape*, align 4
  store %class.btCylinderShape* %this, %class.btCylinderShape** %this.addr, align 4
  %this1 = load %class.btCylinderShape*, %class.btCylinderShape** %this.addr, align 4
  %m_upAxis = getelementptr inbounds %class.btCylinderShape, %class.btCylinderShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_upAxis, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZN9btVector3cvPfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btSqrtf(float %y) #1 comdat {
entry:
  %y.addr = alloca float, align 4
  store float %y, float* %y.addr, align 4
  %0 = load float, float* %y.addr, align 4
  %1 = call float @llvm.sqrt.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %0 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %0, i32 0, i32 2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_implicitShapeDimensions)
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_upAxis, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %1
  %2 = load float, float* %arrayidx, align 4
  ret float %2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_upAxis, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector37length2Ev(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  ret float %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %_x, float* nonnull align 4 dereferenceable(4) %_y, float* nonnull align 4 dereferenceable(4) %_z) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %_x.addr = alloca float*, align 4
  %_y.addr = alloca float*, align 4
  %_z.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %_x, float** %_x.addr, align 4
  store float* %_y, float** %_y.addr, align 4
  store float* %_z, float** %_z.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %_x.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  store float %1, float* %arrayidx, align 4
  %2 = load float*, float** %_y.addr, align 4
  %3 = load float, float* %2, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  store float %3, float* %arrayidx3, align 4
  %4 = load float*, float** %_z.addr, align 4
  %5 = load float, float* %4, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 2
  store float %5, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 3
  store float 0.000000e+00, float* %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %2 = load float, float* %arrayidx, align 4
  %mul = fmul float %2, %1
  store float %mul, float* %arrayidx, align 4
  %3 = load float*, float** %s.addr, align 4
  %4 = load float, float* %3, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %mul4 = fmul float %5, %4
  store float %mul4, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %m_floats5 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx6 = getelementptr inbounds [4 x float], [4 x float]* %m_floats5, i32 0, i32 2
  %8 = load float, float* %arrayidx6, align 4
  %mul7 = fmul float %8, %7
  store float %mul7, float* %arrayidx6, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN23btConvexPointCloudShape17getUnscaledPointsEv(%class.btConvexPointCloudShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  %m_unscaledPoints = getelementptr inbounds %class.btConvexPointCloudShape, %class.btConvexPointCloudShape* %this1, i32 0, i32 1
  %0 = load %class.btVector3*, %class.btVector3** %m_unscaledPoints, align 4
  ret %class.btVector3* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK23btConvexPointCloudShape12getNumPointsEv(%class.btConvexPointCloudShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConvexPointCloudShape*, align 4
  store %class.btConvexPointCloudShape* %this, %class.btConvexPointCloudShape** %this.addr, align 4
  %this1 = load %class.btConvexPointCloudShape*, %class.btConvexPointCloudShape** %this.addr, align 4
  %m_numPoints = getelementptr inbounds %class.btConvexPointCloudShape, %class.btConvexPointCloudShape* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_numPoints, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define internal void @_ZL17convexHullSupportRK9btVector3PS0_iS1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %localDirOrg, %class.btVector3* %points, i32 %numPoints, %class.btVector3* nonnull align 4 dereferenceable(16) %localScaling) #2 {
entry:
  %localDirOrg.addr = alloca %class.btVector3*, align 4
  %points.addr = alloca %class.btVector3*, align 4
  %numPoints.addr = alloca i32, align 4
  %localScaling.addr = alloca %class.btVector3*, align 4
  %vec = alloca %class.btVector3, align 4
  %maxDot = alloca float, align 4
  %ptIndex = alloca i32, align 4
  store %class.btVector3* %localDirOrg, %class.btVector3** %localDirOrg.addr, align 4
  store %class.btVector3* %points, %class.btVector3** %points.addr, align 4
  store i32 %numPoints, i32* %numPoints.addr, align 4
  store %class.btVector3* %localScaling, %class.btVector3** %localScaling.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %localDirOrg.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %localScaling.addr, align 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %vec, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  %2 = load %class.btVector3*, %class.btVector3** %points.addr, align 4
  %3 = load i32, i32* %numPoints.addr, align 4
  %call = call i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %vec, %class.btVector3* %2, i32 %3, float* nonnull align 4 dereferenceable(4) %maxDot)
  store i32 %call, i32* %ptIndex, align 4
  %4 = load %class.btVector3*, %class.btVector3** %points.addr, align 4
  %5 = load i32, i32* %ptIndex, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 %5
  %6 = load %class.btVector3*, %class.btVector3** %localScaling.addr, align 4
  call void @_ZmlRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %6)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape17getLocalScalingNVEv(%class.btConvexInternalShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 1
  ret %class.btVector3* %m_localScaling
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btVector3* @_ZN17btConvexHullShape17getUnscaledPointsEv(%class.btConvexHullShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %m_unscaledPoints, i32 0)
  ret %class.btVector3* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK17btConvexHullShape12getNumPointsEv(%class.btConvexHullShape* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btConvexHullShape*, align 4
  store %class.btConvexHullShape* %this, %class.btConvexHullShape** %this.addr, align 4
  %this1 = load %class.btConvexHullShape*, %class.btConvexHullShape** %this.addr, align 4
  %m_unscaledPoints = getelementptr inbounds %class.btConvexHullShape, %class.btConvexHullShape* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %m_unscaledPoints)
  ret i32 %call
}

; Function Attrs: noinline optnone
define hidden void @_ZNK13btConvexShape31localGetSupportVertexNonVirtualERK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, %class.btConvexShape* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %localDir) #2 {
entry:
  %this.addr = alloca %class.btConvexShape*, align 4
  %localDir.addr = alloca %class.btVector3*, align 4
  %localDirNorm = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %ref.tmp7 = alloca float, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4
  store %class.btVector3* %localDir, %class.btVector3** %localDir.addr, align 4
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %localDir.addr, align 4
  %1 = bitcast %class.btVector3* %localDirNorm to i8*
  %2 = bitcast %class.btVector3* %0 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %1, i8* align 4 %2, i32 16, i1 false)
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %localDirNorm)
  %cmp = fcmp olt float %call, 0x3D10000000000000
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  store float -1.000000e+00, float* %ref.tmp, align 4
  store float -1.000000e+00, float* %ref.tmp2, align 4
  store float -1.000000e+00, float* %ref.tmp3, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %localDirNorm, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %localDirNorm)
  call void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4 %ref.tmp5, %class.btConvexShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %localDirNorm)
  %call8 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %this1)
  store float %call8, float* %ref.tmp7, align 4
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %ref.tmp6, float* nonnull align 4 dereferenceable(4) %ref.tmp7, %class.btVector3* nonnull align 4 dereferenceable(16) %localDirNorm)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp6)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector39normalizeEv(%class.btVector3* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector36lengthEv(%class.btVector3* %this1)
  store float %call, float* %ref.tmp, align 4
  %call2 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call2
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZplRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %add = fadd float %1, %3
  store float %add, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %7
  store float %add8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %add14 = fadd float %9, %11
  store float %add14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRKfRK9btVector3(%class.btVector3* noalias sret align 4 %agg.result, float* nonnull align 4 dereferenceable(4) %s, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %s.addr = alloca float*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store float* %s, float** %s.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %1 = load float*, float** %s.addr, align 4
  call void @_ZmlRK9btVector3RKf(%class.btVector3* sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %0, float* nonnull align 4 dereferenceable(4) %1)
  ret void
}

; Function Attrs: noinline optnone
define hidden float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %this) #2 {
entry:
  %retval = alloca float, align 4
  %this.addr = alloca %class.btConvexShape*, align 4
  %sphereShape = alloca %class.btSphereShape*, align 4
  %convexShape = alloca %class.btBoxShape*, align 4
  %triangleShape = alloca %class.btTriangleShape*, align 4
  %cylShape = alloca %class.btCylinderShape*, align 4
  %conShape = alloca %class.btConeShape*, align 4
  %capsuleShape = alloca %class.btCapsuleShape*, align 4
  %convexHullShape = alloca %class.btPolyhedralConvexShape*, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  %0 = bitcast %class.btConvexShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %0, i32 0, i32 1
  %1 = load i32, i32* %m_shapeType, align 4
  switch i32 %1, label %sw.default [
    i32 8, label %sw.bb
    i32 0, label %sw.bb2
    i32 1, label %sw.bb4
    i32 13, label %sw.bb6
    i32 11, label %sw.bb8
    i32 10, label %sw.bb10
    i32 5, label %sw.bb12
    i32 4, label %sw.bb12
  ]

sw.bb:                                            ; preds = %entry
  %2 = bitcast %class.btConvexShape* %this1 to %class.btSphereShape*
  store %class.btSphereShape* %2, %class.btSphereShape** %sphereShape, align 4
  %3 = load %class.btSphereShape*, %class.btSphereShape** %sphereShape, align 4
  %call = call float @_ZNK13btSphereShape9getRadiusEv(%class.btSphereShape* %3)
  store float %call, float* %retval, align 4
  br label %return

sw.bb2:                                           ; preds = %entry
  %4 = bitcast %class.btConvexShape* %this1 to %class.btBoxShape*
  store %class.btBoxShape* %4, %class.btBoxShape** %convexShape, align 4
  %5 = load %class.btBoxShape*, %class.btBoxShape** %convexShape, align 4
  %6 = bitcast %class.btBoxShape* %5 to %class.btConvexInternalShape*
  %call3 = call float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %6)
  store float %call3, float* %retval, align 4
  br label %return

sw.bb4:                                           ; preds = %entry
  %7 = bitcast %class.btConvexShape* %this1 to %class.btTriangleShape*
  store %class.btTriangleShape* %7, %class.btTriangleShape** %triangleShape, align 4
  %8 = load %class.btTriangleShape*, %class.btTriangleShape** %triangleShape, align 4
  %9 = bitcast %class.btTriangleShape* %8 to %class.btConvexInternalShape*
  %call5 = call float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %9)
  store float %call5, float* %retval, align 4
  br label %return

sw.bb6:                                           ; preds = %entry
  %10 = bitcast %class.btConvexShape* %this1 to %class.btCylinderShape*
  store %class.btCylinderShape* %10, %class.btCylinderShape** %cylShape, align 4
  %11 = load %class.btCylinderShape*, %class.btCylinderShape** %cylShape, align 4
  %12 = bitcast %class.btCylinderShape* %11 to %class.btConvexInternalShape*
  %call7 = call float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %12)
  store float %call7, float* %retval, align 4
  br label %return

sw.bb8:                                           ; preds = %entry
  %13 = bitcast %class.btConvexShape* %this1 to %class.btConeShape*
  store %class.btConeShape* %13, %class.btConeShape** %conShape, align 4
  %14 = load %class.btConeShape*, %class.btConeShape** %conShape, align 4
  %15 = bitcast %class.btConeShape* %14 to %class.btConvexInternalShape*
  %call9 = call float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %15)
  store float %call9, float* %retval, align 4
  br label %return

sw.bb10:                                          ; preds = %entry
  %16 = bitcast %class.btConvexShape* %this1 to %class.btCapsuleShape*
  store %class.btCapsuleShape* %16, %class.btCapsuleShape** %capsuleShape, align 4
  %17 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4
  %18 = bitcast %class.btCapsuleShape* %17 to %class.btConvexInternalShape*
  %call11 = call float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %18)
  store float %call11, float* %retval, align 4
  br label %return

sw.bb12:                                          ; preds = %entry, %entry
  %19 = bitcast %class.btConvexShape* %this1 to %class.btPolyhedralConvexShape*
  store %class.btPolyhedralConvexShape* %19, %class.btPolyhedralConvexShape** %convexHullShape, align 4
  %20 = load %class.btPolyhedralConvexShape*, %class.btPolyhedralConvexShape** %convexHullShape, align 4
  %21 = bitcast %class.btPolyhedralConvexShape* %20 to %class.btConvexInternalShape*
  %call13 = call float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %21)
  store float %call13, float* %retval, align 4
  br label %return

sw.default:                                       ; preds = %entry
  %22 = bitcast %class.btConvexShape* %this1 to float (%class.btConvexShape*)***
  %vtable = load float (%class.btConvexShape*)**, float (%class.btConvexShape*)*** %22, align 4
  %vfn = getelementptr inbounds float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vtable, i64 12
  %23 = load float (%class.btConvexShape*)*, float (%class.btConvexShape*)** %vfn, align 4
  %call14 = call float %23(%class.btConvexShape* %this1)
  store float %call14, float* %retval, align 4
  br label %return

return:                                           ; preds = %sw.default, %sw.bb12, %sw.bb10, %sw.bb8, %sw.bb6, %sw.bb4, %sw.bb2, %sw.bb
  %24 = load float, float* %retval, align 4
  ret float %24
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK13btSphereShape9getRadiusEv(%class.btSphereShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSphereShape*, align 4
  store %class.btSphereShape* %this, %class.btSphereShape** %this.addr, align 4
  %this1 = load %class.btSphereShape*, %class.btSphereShape** %this.addr, align 4
  %0 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %0, i32 0, i32 2
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_implicitShapeDimensions)
  %1 = load float, float* %call, align 4
  %2 = bitcast %class.btSphereShape* %this1 to %class.btConvexInternalShape*
  %m_localScaling = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %2, i32 0, i32 1
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %m_localScaling)
  %3 = load float, float* %call2, align 4
  %mul = fmul float %1, %3
  ret float %mul
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK21btConvexInternalShape11getMarginNVEv(%class.btConvexInternalShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btConvexInternalShape*, align 4
  store %class.btConvexInternalShape* %this, %class.btConvexInternalShape** %this.addr, align 4
  %this1 = load %class.btConvexInternalShape*, %class.btConvexInternalShape** %this.addr, align 4
  %m_collisionMargin = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %this1, i32 0, i32 3
  %0 = load float, float* %m_collisionMargin, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define hidden void @_ZNK13btConvexShape17getAabbNonVirtualERK11btTransformR9btVector3S4_(%class.btConvexShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %t, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax) #2 {
entry:
  %this.addr = alloca %class.btConvexShape*, align 4
  %t.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %sphereShape = alloca %class.btSphereShape*, align 4
  %radius = alloca float, align 4
  %margin = alloca float, align 4
  %center = alloca %class.btVector3*, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp = alloca %class.btVector3, align 4
  %ref.tmp6 = alloca %class.btVector3, align 4
  %convexShape = alloca %class.btBoxShape*, align 4
  %margin8 = alloca float, align 4
  %halfExtents = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center15 = alloca %class.btVector3, align 4
  %extent17 = alloca %class.btVector3, align 4
  %ref.tmp21 = alloca %class.btVector3, align 4
  %ref.tmp22 = alloca %class.btVector3, align 4
  %triangleShape = alloca %class.btTriangleShape*, align 4
  %margin24 = alloca float, align 4
  %i = alloca i32, align 4
  %vec = alloca %class.btVector3, align 4
  %ref.tmp26 = alloca float, align 4
  %ref.tmp27 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %sv = alloca %class.btVector3, align 4
  %ref.tmp31 = alloca %class.btVector3, align 4
  %tmp = alloca %class.btVector3, align 4
  %ref.tmp40 = alloca %class.btVector3, align 4
  %ref.tmp41 = alloca %class.btVector3, align 4
  %ref.tmp42 = alloca %class.btVector3, align 4
  %capsuleShape = alloca %class.btCapsuleShape*, align 4
  %halfExtents49 = alloca %class.btVector3, align 4
  %ref.tmp50 = alloca float, align 4
  %ref.tmp52 = alloca float, align 4
  %ref.tmp54 = alloca float, align 4
  %m_upAxis = alloca i32, align 4
  %abs_b63 = alloca %class.btMatrix3x3, align 4
  %center65 = alloca %class.btVector3, align 4
  %extent67 = alloca %class.btVector3, align 4
  %ref.tmp71 = alloca %class.btVector3, align 4
  %ref.tmp72 = alloca %class.btVector3, align 4
  %convexHullShape = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  %margin74 = alloca float, align 4
  store %class.btConvexShape* %this, %class.btConvexShape** %this.addr, align 4
  store %class.btTransform* %t, %class.btTransform** %t.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  %this1 = load %class.btConvexShape*, %class.btConvexShape** %this.addr, align 4
  %0 = bitcast %class.btConvexShape* %this1 to %class.btCollisionShape*
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %0, i32 0, i32 1
  %1 = load i32, i32* %m_shapeType, align 4
  switch i32 %1, label %sw.default [
    i32 8, label %sw.bb
    i32 13, label %sw.bb7
    i32 0, label %sw.bb7
    i32 1, label %sw.bb23
    i32 10, label %sw.bb48
    i32 5, label %sw.bb73
    i32 4, label %sw.bb73
  ]

sw.bb:                                            ; preds = %entry
  %2 = bitcast %class.btConvexShape* %this1 to %class.btSphereShape*
  store %class.btSphereShape* %2, %class.btSphereShape** %sphereShape, align 4
  %3 = load %class.btSphereShape*, %class.btSphereShape** %sphereShape, align 4
  %4 = bitcast %class.btSphereShape* %3 to %class.btConvexInternalShape*
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape26getImplicitShapeDimensionsEv(%class.btConvexInternalShape* %4)
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector34getXEv(%class.btVector3* %call)
  %5 = load float, float* %call2, align 4
  store float %5, float* %radius, align 4
  %6 = load float, float* %radius, align 4
  %7 = load %class.btSphereShape*, %class.btSphereShape** %sphereShape, align 4
  %8 = bitcast %class.btSphereShape* %7 to %class.btConvexShape*
  %call3 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %8)
  %add = fadd float %6, %call3
  store float %add, float* %margin, align 4
  %9 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %call4 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %9)
  store %class.btVector3* %call4, %class.btVector3** %center, align 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %extent, float* nonnull align 4 dereferenceable(4) %margin, float* nonnull align 4 dereferenceable(4) %margin, float* nonnull align 4 dereferenceable(4) %margin)
  %10 = load %class.btVector3*, %class.btVector3** %center, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %10, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %11 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %12 = bitcast %class.btVector3* %11 to i8*
  %13 = bitcast %class.btVector3* %ref.tmp to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %12, i8* align 4 %13, i32 16, i1 false)
  %14 = load %class.btVector3*, %class.btVector3** %center, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp6, %class.btVector3* nonnull align 4 dereferenceable(16) %14, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %15 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %16 = bitcast %class.btVector3* %15 to i8*
  %17 = bitcast %class.btVector3* %ref.tmp6 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %16, i8* align 4 %17, i32 16, i1 false)
  br label %sw.epilog

sw.bb7:                                           ; preds = %entry, %entry
  %18 = bitcast %class.btConvexShape* %this1 to %class.btBoxShape*
  store %class.btBoxShape* %18, %class.btBoxShape** %convexShape, align 4
  %19 = load %class.btBoxShape*, %class.btBoxShape** %convexShape, align 4
  %20 = bitcast %class.btBoxShape* %19 to %class.btConvexShape*
  %call9 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %20)
  store float %call9, float* %margin8, align 4
  %21 = load %class.btBoxShape*, %class.btBoxShape** %convexShape, align 4
  %22 = bitcast %class.btBoxShape* %21 to %class.btConvexInternalShape*
  %call10 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK21btConvexInternalShape26getImplicitShapeDimensionsEv(%class.btConvexInternalShape* %22)
  %23 = bitcast %class.btVector3* %halfExtents to i8*
  %24 = bitcast %class.btVector3* %call10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %23, i8* align 4 %24, i32 16, i1 false)
  %call12 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp11, float* nonnull align 4 dereferenceable(4) %margin8, float* nonnull align 4 dereferenceable(4) %margin8, float* nonnull align 4 dereferenceable(4) %margin8)
  %call13 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp11)
  %25 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %call14 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %25)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call14)
  %26 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %call16 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %26)
  %27 = bitcast %class.btVector3* %center15 to i8*
  %28 = bitcast %class.btVector3* %call16 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %27, i8* align 4 %28, i32 16, i1 false)
  %call18 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call19 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call20 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent17, %class.btVector3* %halfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %call18, %class.btVector3* nonnull align 4 dereferenceable(16) %call19, %class.btVector3* nonnull align 4 dereferenceable(16) %call20)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp21, %class.btVector3* nonnull align 4 dereferenceable(16) %center15, %class.btVector3* nonnull align 4 dereferenceable(16) %extent17)
  %29 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %30 = bitcast %class.btVector3* %29 to i8*
  %31 = bitcast %class.btVector3* %ref.tmp21 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %30, i8* align 4 %31, i32 16, i1 false)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp22, %class.btVector3* nonnull align 4 dereferenceable(16) %center15, %class.btVector3* nonnull align 4 dereferenceable(16) %extent17)
  %32 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %33 = bitcast %class.btVector3* %32 to i8*
  %34 = bitcast %class.btVector3* %ref.tmp22 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %33, i8* align 4 %34, i32 16, i1 false)
  br label %sw.epilog

sw.bb23:                                          ; preds = %entry
  %35 = bitcast %class.btConvexShape* %this1 to %class.btTriangleShape*
  store %class.btTriangleShape* %35, %class.btTriangleShape** %triangleShape, align 4
  %36 = load %class.btTriangleShape*, %class.btTriangleShape** %triangleShape, align 4
  %37 = bitcast %class.btTriangleShape* %36 to %class.btConvexShape*
  %call25 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %37)
  store float %call25, float* %margin24, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %sw.bb23
  %38 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %38, 3
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  store float 0.000000e+00, float* %ref.tmp26, align 4
  store float 0.000000e+00, float* %ref.tmp27, align 4
  store float 0.000000e+00, float* %ref.tmp28, align 4
  %call29 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %vec, float* nonnull align 4 dereferenceable(4) %ref.tmp26, float* nonnull align 4 dereferenceable(4) %ref.tmp27, float* nonnull align 4 dereferenceable(4) %ref.tmp28)
  %call30 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vec)
  %39 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds float, float* %call30, i32 %39
  store float 1.000000e+00, float* %arrayidx, align 4
  %40 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %call32 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %40)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp31, %class.btVector3* nonnull align 4 dereferenceable(16) %vec, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call32)
  call void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4 %sv, %class.btConvexShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp31)
  %41 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %tmp, %class.btTransform* %41, %class.btVector3* nonnull align 4 dereferenceable(16) %sv)
  %call33 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %42 = load i32, i32* %i, align 4
  %arrayidx34 = getelementptr inbounds float, float* %call33, i32 %42
  %43 = load float, float* %arrayidx34, align 4
  %44 = load float, float* %margin24, align 4
  %add35 = fadd float %43, %44
  %45 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %call36 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %45)
  %46 = load i32, i32* %i, align 4
  %arrayidx37 = getelementptr inbounds float, float* %call36, i32 %46
  store float %add35, float* %arrayidx37, align 4
  %call38 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %vec)
  %47 = load i32, i32* %i, align 4
  %arrayidx39 = getelementptr inbounds float, float* %call38, i32 %47
  store float -1.000000e+00, float* %arrayidx39, align 4
  %48 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %49 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %call43 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %49)
  call void @_ZmlRK9btVector3RK11btMatrix3x3(%class.btVector3* sret align 4 %ref.tmp42, %class.btVector3* nonnull align 4 dereferenceable(16) %vec, %class.btMatrix3x3* nonnull align 4 dereferenceable(48) %call43)
  call void @_ZNK13btConvexShape44localGetSupportVertexWithoutMarginNonVirtualERK9btVector3(%class.btVector3* sret align 4 %ref.tmp41, %class.btConvexShape* %this1, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp42)
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %ref.tmp40, %class.btTransform* %48, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp41)
  %50 = bitcast %class.btVector3* %tmp to i8*
  %51 = bitcast %class.btVector3* %ref.tmp40 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %50, i8* align 4 %51, i32 16, i1 false)
  %call44 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %tmp)
  %52 = load i32, i32* %i, align 4
  %arrayidx45 = getelementptr inbounds float, float* %call44, i32 %52
  %53 = load float, float* %arrayidx45, align 4
  %54 = load float, float* %margin24, align 4
  %sub = fsub float %53, %54
  %55 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %call46 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %55)
  %56 = load i32, i32* %i, align 4
  %arrayidx47 = getelementptr inbounds float, float* %call46, i32 %56
  store float %sub, float* %arrayidx47, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %57 = load i32, i32* %i, align 4
  %inc = add nsw i32 %57, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %sw.epilog

sw.bb48:                                          ; preds = %entry
  %58 = bitcast %class.btConvexShape* %this1 to %class.btCapsuleShape*
  store %class.btCapsuleShape* %58, %class.btCapsuleShape** %capsuleShape, align 4
  %59 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4
  %call51 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %59)
  store float %call51, float* %ref.tmp50, align 4
  %60 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4
  %call53 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %60)
  store float %call53, float* %ref.tmp52, align 4
  %61 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4
  %call55 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %61)
  store float %call55, float* %ref.tmp54, align 4
  %call56 = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %halfExtents49, float* nonnull align 4 dereferenceable(4) %ref.tmp50, float* nonnull align 4 dereferenceable(4) %ref.tmp52, float* nonnull align 4 dereferenceable(4) %ref.tmp54)
  %62 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4
  %call57 = call i32 @_ZNK14btCapsuleShape9getUpAxisEv(%class.btCapsuleShape* %62)
  store i32 %call57, i32* %m_upAxis, align 4
  %63 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4
  %call58 = call float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %63)
  %64 = load %class.btCapsuleShape*, %class.btCapsuleShape** %capsuleShape, align 4
  %call59 = call float @_ZNK14btCapsuleShape13getHalfHeightEv(%class.btCapsuleShape* %64)
  %add60 = fadd float %call58, %call59
  %call61 = call float* @_ZN9btVector3cvPfEv(%class.btVector3* %halfExtents49)
  %65 = load i32, i32* %m_upAxis, align 4
  %arrayidx62 = getelementptr inbounds float, float* %call61, i32 %65
  store float %add60, float* %arrayidx62, align 4
  %66 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %call64 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %66)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b63, %class.btMatrix3x3* %call64)
  %67 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %call66 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %67)
  %68 = bitcast %class.btVector3* %center65 to i8*
  %69 = bitcast %class.btVector3* %call66 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %68, i8* align 4 %69, i32 16, i1 false)
  %call68 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b63, i32 0)
  %call69 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b63, i32 1)
  %call70 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b63, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent67, %class.btVector3* %halfExtents49, %class.btVector3* nonnull align 4 dereferenceable(16) %call68, %class.btVector3* nonnull align 4 dereferenceable(16) %call69, %class.btVector3* nonnull align 4 dereferenceable(16) %call70)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp71, %class.btVector3* nonnull align 4 dereferenceable(16) %center65, %class.btVector3* nonnull align 4 dereferenceable(16) %extent67)
  %70 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %71 = bitcast %class.btVector3* %70 to i8*
  %72 = bitcast %class.btVector3* %ref.tmp71 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %71, i8* align 4 %72, i32 16, i1 false)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp72, %class.btVector3* nonnull align 4 dereferenceable(16) %center65, %class.btVector3* nonnull align 4 dereferenceable(16) %extent67)
  %73 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %74 = bitcast %class.btVector3* %73 to i8*
  %75 = bitcast %class.btVector3* %ref.tmp72 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %74, i8* align 4 %75, i32 16, i1 false)
  br label %sw.epilog

sw.bb73:                                          ; preds = %entry, %entry
  %76 = bitcast %class.btConvexShape* %this1 to %class.btPolyhedralConvexAabbCachingShape*
  store %class.btPolyhedralConvexAabbCachingShape* %76, %class.btPolyhedralConvexAabbCachingShape** %convexHullShape, align 4
  %77 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %convexHullShape, align 4
  %78 = bitcast %class.btPolyhedralConvexAabbCachingShape* %77 to %class.btConvexShape*
  %call75 = call float @_ZNK13btConvexShape19getMarginNonVirtualEv(%class.btConvexShape* %78)
  store float %call75, float* %margin74, align 4
  %79 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %convexHullShape, align 4
  %80 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %81 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %82 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %83 = load float, float* %margin74, align 4
  call void @_ZNK34btPolyhedralConvexAabbCachingShape17getNonvirtualAabbERK11btTransformR9btVector3S4_f(%class.btPolyhedralConvexAabbCachingShape* %79, %class.btTransform* nonnull align 4 dereferenceable(64) %80, %class.btVector3* nonnull align 4 dereferenceable(16) %81, %class.btVector3* nonnull align 4 dereferenceable(16) %82, float %83)
  br label %sw.epilog

sw.default:                                       ; preds = %entry
  %84 = load %class.btTransform*, %class.btTransform** %t.addr, align 4
  %85 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %86 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  %87 = bitcast %class.btConvexShape* %this1 to void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)***
  %vtable = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)**, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*** %87, align 4
  %vfn = getelementptr inbounds void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vtable, i64 2
  %88 = load void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)*, void (%class.btConvexShape*, %class.btTransform*, %class.btVector3*, %class.btVector3*)** %vfn, align 4
  call void %88(%class.btConvexShape* %this1, %class.btTransform* nonnull align 4 dereferenceable(64) %84, %class.btVector3* nonnull align 4 dereferenceable(16) %85, %class.btVector3* nonnull align 4 dereferenceable(16) %86)
  br label %sw.epilog

sw.epilog:                                        ; preds = %sw.default, %sw.bb73, %sw.bb48, %for.end, %sw.bb7, %sw.bb
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btTransform9getOriginEv(%class.btTransform* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTransform*, align 4
  store %class.btTransform* %this, %class.btTransform** %this.addr, align 4
  %this1 = load %class.btTransform*, %class.btTransform** %this.addr, align 4
  %m_origin = getelementptr inbounds %class.btTransform, %class.btTransform* %this1, i32 0, i32 1
  ret %class.btVector3* %m_origin
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmiRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %sub = fsub float %1, %3
  store float %sub, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %sub8 = fsub float %5, %7
  store float %sub8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %sub14 = fsub float %9, %11
  store float %sub14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 0
  %2 = load float, float* %arrayidx3, align 4
  %add = fadd float %2, %1
  store float %add, float* %arrayidx3, align 4
  %3 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %3, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %4 = load float, float* %arrayidx5, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %5 = load float, float* %arrayidx7, align 4
  %add8 = fadd float %5, %4
  store float %add8, float* %arrayidx7, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats9 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [4 x float], [4 x float]* %m_floats9, i32 0, i32 2
  %7 = load float, float* %arrayidx10, align 4
  %m_floats11 = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx12 = getelementptr inbounds [4 x float], [4 x float]* %m_floats11, i32 0, i32 2
  %8 = load float, float* %arrayidx12, align 4
  %add13 = fadd float %8, %7
  store float %add13, float* %arrayidx12, align 4
  ret %class.btVector3* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* noalias sret align 4 %agg.result, %class.btMatrix3x3* %this) #2 comdat {
entry:
  %result.ptr = alloca i8*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp8 = alloca float, align 4
  %ref.tmp13 = alloca float, align 4
  %ref.tmp18 = alloca float, align 4
  %ref.tmp23 = alloca float, align 4
  %ref.tmp28 = alloca float, align 4
  %ref.tmp33 = alloca float, align 4
  %ref.tmp38 = alloca float, align 4
  %0 = bitcast %class.btMatrix3x3* %agg.result to i8*
  store i8* %0, i8** %result.ptr, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %1 = load float, float* %call, align 4
  %call2 = call float @_Z6btFabsf(float %1)
  store float %call2, float* %ref.tmp, align 4
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 0
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx5)
  %2 = load float, float* %call6, align 4
  %call7 = call float @_Z6btFabsf(float %2)
  store float %call7, float* %ref.tmp3, align 4
  %m_el9 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx10 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el9, i32 0, i32 0
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx10)
  %3 = load float, float* %call11, align 4
  %call12 = call float @_Z6btFabsf(float %3)
  store float %call12, float* %ref.tmp8, align 4
  %m_el14 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx15 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el14, i32 0, i32 1
  %call16 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx15)
  %4 = load float, float* %call16, align 4
  %call17 = call float @_Z6btFabsf(float %4)
  store float %call17, float* %ref.tmp13, align 4
  %m_el19 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx20 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el19, i32 0, i32 1
  %call21 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx20)
  %5 = load float, float* %call21, align 4
  %call22 = call float @_Z6btFabsf(float %5)
  store float %call22, float* %ref.tmp18, align 4
  %m_el24 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx25 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el24, i32 0, i32 1
  %call26 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx25)
  %6 = load float, float* %call26, align 4
  %call27 = call float @_Z6btFabsf(float %6)
  store float %call27, float* %ref.tmp23, align 4
  %m_el29 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx30 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el29, i32 0, i32 2
  %call31 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx30)
  %7 = load float, float* %call31, align 4
  %call32 = call float @_Z6btFabsf(float %7)
  store float %call32, float* %ref.tmp28, align 4
  %m_el34 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx35 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el34, i32 0, i32 2
  %call36 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx35)
  %8 = load float, float* %call36, align 4
  %call37 = call float @_Z6btFabsf(float %8)
  store float %call37, float* %ref.tmp33, align 4
  %m_el39 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx40 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el39, i32 0, i32 2
  %call41 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx40)
  %9 = load float, float* %call41, align 4
  %call42 = call float @_Z6btFabsf(float %9)
  store float %call42, float* %ref.tmp38, align 4
  %call43 = call %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp8, float* nonnull align 4 dereferenceable(4) %ref.tmp13, float* nonnull align 4 dereferenceable(4) %ref.tmp18, float* nonnull align 4 dereferenceable(4) %ref.tmp23, float* nonnull align 4 dereferenceable(4) %ref.tmp28, float* nonnull align 4 dereferenceable(4) %ref.tmp33, float* nonnull align 4 dereferenceable(4) %ref.tmp38)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK14btCapsuleShape9getRadiusEv(%class.btCapsuleShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCapsuleShape*, align 4
  %radiusAxis = alloca i32, align 4
  store %class.btCapsuleShape* %this, %class.btCapsuleShape** %this.addr, align 4
  %this1 = load %class.btCapsuleShape*, %class.btCapsuleShape** %this.addr, align 4
  %m_upAxis = getelementptr inbounds %class.btCapsuleShape, %class.btCapsuleShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_upAxis, align 4
  %add = add nsw i32 %0, 2
  %rem = srem i32 %add, 3
  store i32 %rem, i32* %radiusAxis, align 4
  %1 = bitcast %class.btCapsuleShape* %this1 to %class.btConvexInternalShape*
  %m_implicitShapeDimensions = getelementptr inbounds %class.btConvexInternalShape, %class.btConvexInternalShape* %1, i32 0, i32 2
  %call = call float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %m_implicitShapeDimensions)
  %2 = load i32, i32* %radiusAxis, align 4
  %arrayidx = getelementptr inbounds float, float* %call, i32 %2
  %3 = load float, float* %arrayidx, align 4
  ret float %3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK34btPolyhedralConvexAabbCachingShape17getNonvirtualAabbERK11btTransformR9btVector3S4_f(%class.btPolyhedralConvexAabbCachingShape* %this, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMax, float %margin) #2 comdat {
entry:
  %this.addr = alloca %class.btPolyhedralConvexAabbCachingShape*, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMin.addr = alloca %class.btVector3*, align 4
  %aabbMax.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  store %class.btPolyhedralConvexAabbCachingShape* %this, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4
  store %class.btVector3* %aabbMin, %class.btVector3** %aabbMin.addr, align 4
  store %class.btVector3* %aabbMax, %class.btVector3** %aabbMax.addr, align 4
  store float %margin, float* %margin.addr, align 4
  %this1 = load %class.btPolyhedralConvexAabbCachingShape*, %class.btPolyhedralConvexAabbCachingShape** %this.addr, align 4
  %m_localAabbMin = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 1
  %m_localAabbMax = getelementptr inbounds %class.btPolyhedralConvexAabbCachingShape, %class.btPolyhedralConvexAabbCachingShape* %this1, i32 0, i32 2
  %0 = load float, float* %margin.addr, align 4
  %1 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %2 = load %class.btVector3*, %class.btVector3** %aabbMin.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %aabbMax.addr, align 4
  call void @_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %m_localAabbMax, float %0, %class.btTransform* nonnull align 4 dereferenceable(64) %1, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  ret void
}

declare void @__cxa_pure_virtual() unnamed_addr

declare void @_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf(%class.btCollisionShape*, %class.btVector3* nonnull align 4 dereferenceable(16), float* nonnull align 4 dereferenceable(4)) unnamed_addr #5

declare float @_ZNK16btCollisionShape20getAngularMotionDiscEv(%class.btCollisionShape*) unnamed_addr #5

declare float @_ZNK16btCollisionShape27getContactBreakingThresholdEf(%class.btCollisionShape*, float) unnamed_addr #5

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZNK16btCollisionShape38getAnisotropicRollingFrictionDirectionEv(%class.btVector3* noalias sret align 4 %agg.result, %class.btCollisionShape* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp2 = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  store float 1.000000e+00, float* %ref.tmp, align 4
  store float 1.000000e+00, float* %ref.tmp2, align 4
  store float 1.000000e+00, float* %ref.tmp3, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp2, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionShape28calculateSerializeBufferSizeEv(%class.btCollisionShape* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  ret i32 12
}

declare i8* @_ZNK16btCollisionShape9serializeEPvP12btSerializer(%class.btCollisionShape*, i8*, %class.btSerializer*) unnamed_addr #5

declare void @_ZNK16btCollisionShape20serializeSingleShapeEP12btSerializer(%class.btCollisionShape*, %class.btSerializer*) unnamed_addr #5

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotxERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotyERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline optnone
define linkonce_odr hidden float @_ZNK11btMatrix3x35tdotzERK9btVector3(%class.btMatrix3x3* %this, %class.btVector3* nonnull align 4 dereferenceable(16) %v) #2 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %v.addr = alloca %class.btVector3*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %call = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx)
  %0 = load float, float* %call, align 4
  %1 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31xEv(%class.btVector3* %1)
  %2 = load float, float* %call2, align 4
  %mul = fmul float %0, %2
  %m_el3 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx4 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el3, i32 0, i32 1
  %call5 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx4)
  %3 = load float, float* %call5, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31yEv(%class.btVector3* %4)
  %5 = load float, float* %call6, align 4
  %mul7 = fmul float %3, %5
  %add = fadd float %mul, %mul7
  %m_el8 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx9 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el8, i32 0, i32 2
  %call10 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %arrayidx9)
  %6 = load float, float* %call10, align 4
  %7 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %call11 = call nonnull align 4 dereferenceable(4) float* @_ZNK9btVector31zEv(%class.btVector3* %7)
  %8 = load float, float* %call11, align 4
  %mul12 = fmul float %6, %8
  %add13 = fadd float %add, %mul12
  ret float %add13
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZNK11btMatrix3x3ixEi(%class.btMatrix3x3* %this, i32 %i) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %i.addr = alloca i32, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store i32 %i, i32* %i.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %0 = load i32, i32* %i.addr, align 4
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 %0
  ret %class.btVector3* %arrayidx
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.sqrt.f32(float) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float* @_ZNK9btVector3cvPKfEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  ret float* %arrayidx
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3S1_(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v1, %class.btVector3* nonnull align 4 dereferenceable(16) %v2) #2 comdat {
entry:
  %v1.addr = alloca %class.btVector3*, align 4
  %v2.addr = alloca %class.btVector3*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  store %class.btVector3* %v1, %class.btVector3** %v1.addr, align 4
  store %class.btVector3* %v2, %class.btVector3** %v2.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats1 = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 0, i32 0
  %arrayidx2 = getelementptr inbounds [4 x float], [4 x float]* %m_floats1, i32 0, i32 0
  %3 = load float, float* %arrayidx2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats4 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [4 x float], [4 x float]* %m_floats4, i32 0, i32 1
  %5 = load float, float* %arrayidx5, align 4
  %6 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %6, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 1
  %7 = load float, float* %arrayidx7, align 4
  %mul8 = fmul float %5, %7
  store float %mul8, float* %ref.tmp3, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v1.addr, align 4
  %m_floats10 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx11 = getelementptr inbounds [4 x float], [4 x float]* %m_floats10, i32 0, i32 2
  %9 = load float, float* %arrayidx11, align 4
  %10 = load %class.btVector3*, %class.btVector3** %v2.addr, align 4
  %m_floats12 = getelementptr inbounds %class.btVector3, %class.btVector3* %10, i32 0, i32 0
  %arrayidx13 = getelementptr inbounds [4 x float], [4 x float]* %m_floats12, i32 0, i32 2
  %11 = load float, float* %arrayidx13, align 4
  %mul14 = fmul float %9, %11
  store float %mul14, float* %ref.tmp9, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3, float* nonnull align 4 dereferenceable(4) %ref.tmp9)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK9btVector36maxDotEPKS_lRf(%class.btVector3* %this, %class.btVector3* %array, i32 %array_count, float* nonnull align 4 dereferenceable(4) %dotOut) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %array.addr = alloca %class.btVector3*, align 4
  %array_count.addr = alloca i32, align 4
  %dotOut.addr = alloca float*, align 4
  %maxDot1 = alloca float, align 4
  %i = alloca i32, align 4
  %ptIndex = alloca i32, align 4
  %dot = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store %class.btVector3* %array, %class.btVector3** %array.addr, align 4
  store i32 %array_count, i32* %array_count.addr, align 4
  store float* %dotOut, float** %dotOut.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  store float 0xC7EFFFFFE0000000, float* %maxDot1, align 4
  store i32 0, i32* %i, align 4
  store i32 -1, i32* %ptIndex, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load i32, i32* %array_count.addr, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btVector3*, %class.btVector3** %array.addr, align 4
  %3 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %2, i32 %3
  %call = call float @_ZNK9btVector33dotERKS_(%class.btVector3* %arrayidx, %class.btVector3* nonnull align 4 dereferenceable(16) %this1)
  store float %call, float* %dot, align 4
  %4 = load float, float* %dot, align 4
  %5 = load float, float* %maxDot1, align 4
  %cmp2 = fcmp ogt float %4, %5
  br i1 %cmp2, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load float, float* %dot, align 4
  store float %6, float* %maxDot1, align 4
  %7 = load i32, i32* %i, align 4
  store i32 %7, i32* %ptIndex, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %8 = load i32, i32* %i, align 4
  %inc = add nsw i32 %8, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %9 = load float, float* %maxDot1, align 4
  %10 = load float*, float** %dotOut.addr, align 4
  store float %9, float* %10, align 4
  %11 = load i32, i32* %ptIndex, align 4
  ret i32 %11
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN20btAlignedObjectArrayI9btVector3EixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btVector3*, %class.btVector3** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 %1
  ret %class.btVector3* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btVector3E4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK9btVector36lengthEv(%class.btVector3* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %call = call float @_ZNK9btVector37length2Ev(%class.btVector3* %this1)
  %call2 = call float @_Z6btSqrtf(float %call)
  ret float %call2
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3dVERKf(%class.btVector3* %this, float* nonnull align 4 dereferenceable(4) %s) #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  store float* %s, float** %s.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  %0 = load float*, float** %s.addr, align 4
  %1 = load float, float* %0, align 4
  %div = fdiv float 1.000000e+00, %1
  store float %div, float* %ref.tmp, align 4
  %call = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3mLERKf(%class.btVector3* %this1, float* nonnull align 4 dereferenceable(4) %ref.tmp)
  ret %class.btVector3* %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZmlRK9btVector3RKf(%class.btVector3* noalias sret align 4 %agg.result, %class.btVector3* nonnull align 4 dereferenceable(16) %v, float* nonnull align 4 dereferenceable(4) %s) #2 comdat {
entry:
  %v.addr = alloca %class.btVector3*, align 4
  %s.addr = alloca float*, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca float, align 4
  %ref.tmp5 = alloca float, align 4
  store %class.btVector3* %v, %class.btVector3** %v.addr, align 4
  store float* %s, float** %s.addr, align 4
  %0 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats = getelementptr inbounds %class.btVector3, %class.btVector3* %0, i32 0, i32 0
  %arrayidx = getelementptr inbounds [4 x float], [4 x float]* %m_floats, i32 0, i32 0
  %1 = load float, float* %arrayidx, align 4
  %2 = load float*, float** %s.addr, align 4
  %3 = load float, float* %2, align 4
  %mul = fmul float %1, %3
  store float %mul, float* %ref.tmp, align 4
  %4 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats2 = getelementptr inbounds %class.btVector3, %class.btVector3* %4, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [4 x float], [4 x float]* %m_floats2, i32 0, i32 1
  %5 = load float, float* %arrayidx3, align 4
  %6 = load float*, float** %s.addr, align 4
  %7 = load float, float* %6, align 4
  %mul4 = fmul float %5, %7
  store float %mul4, float* %ref.tmp1, align 4
  %8 = load %class.btVector3*, %class.btVector3** %v.addr, align 4
  %m_floats6 = getelementptr inbounds %class.btVector3, %class.btVector3* %8, i32 0, i32 0
  %arrayidx7 = getelementptr inbounds [4 x float], [4 x float]* %m_floats6, i32 0, i32 2
  %9 = load float, float* %arrayidx7, align 4
  %10 = load float*, float** %s.addr, align 4
  %11 = load float, float* %10, align 4
  %mul8 = fmul float %9, %11
  store float %mul8, float* %ref.tmp5, align 4
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %agg.result, float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp1, float* nonnull align 4 dereferenceable(4) %ref.tmp5)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_Z6btFabsf(float %x) #1 comdat {
entry:
  %x.addr = alloca float, align 4
  store float %x, float* %x.addr, align 4
  %0 = load float, float* %x.addr, align 4
  %1 = call float @llvm.fabs.f32(float %0)
  ret float %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btMatrix3x3* @_ZN11btMatrix3x3C2ERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* returned %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btMatrix3x3*, align 4
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  store %class.btMatrix3x3* %this1, %class.btMatrix3x3** %retval, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %array.begin = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btVector3, %class.btVector3* %array.begin, i32 3
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btVector3* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btVector3, %class.btVector3* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btVector3* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this1, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  %9 = load %class.btMatrix3x3*, %class.btMatrix3x3** %retval, align 4
  ret %class.btMatrix3x3* %9
}

; Function Attrs: nounwind readnone speculatable willreturn
declare float @llvm.fabs.f32(float) #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN11btMatrix3x38setValueERKfS1_S1_S1_S1_S1_S1_S1_S1_(%class.btMatrix3x3* %this, float* nonnull align 4 dereferenceable(4) %xx, float* nonnull align 4 dereferenceable(4) %xy, float* nonnull align 4 dereferenceable(4) %xz, float* nonnull align 4 dereferenceable(4) %yx, float* nonnull align 4 dereferenceable(4) %yy, float* nonnull align 4 dereferenceable(4) %yz, float* nonnull align 4 dereferenceable(4) %zx, float* nonnull align 4 dereferenceable(4) %zy, float* nonnull align 4 dereferenceable(4) %zz) #1 comdat {
entry:
  %this.addr = alloca %class.btMatrix3x3*, align 4
  %xx.addr = alloca float*, align 4
  %xy.addr = alloca float*, align 4
  %xz.addr = alloca float*, align 4
  %yx.addr = alloca float*, align 4
  %yy.addr = alloca float*, align 4
  %yz.addr = alloca float*, align 4
  %zx.addr = alloca float*, align 4
  %zy.addr = alloca float*, align 4
  %zz.addr = alloca float*, align 4
  store %class.btMatrix3x3* %this, %class.btMatrix3x3** %this.addr, align 4
  store float* %xx, float** %xx.addr, align 4
  store float* %xy, float** %xy.addr, align 4
  store float* %xz, float** %xz.addr, align 4
  store float* %yx, float** %yx.addr, align 4
  store float* %yy, float** %yy.addr, align 4
  store float* %yz, float** %yz.addr, align 4
  store float* %zx, float** %zx.addr, align 4
  store float* %zy, float** %zy.addr, align 4
  store float* %zz, float** %zz.addr, align 4
  %this1 = load %class.btMatrix3x3*, %class.btMatrix3x3** %this.addr, align 4
  %m_el = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el, i32 0, i32 0
  %0 = load float*, float** %xx.addr, align 4
  %1 = load float*, float** %xy.addr, align 4
  %2 = load float*, float** %xz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx, float* nonnull align 4 dereferenceable(4) %0, float* nonnull align 4 dereferenceable(4) %1, float* nonnull align 4 dereferenceable(4) %2)
  %m_el2 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx3 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el2, i32 0, i32 1
  %3 = load float*, float** %yx.addr, align 4
  %4 = load float*, float** %yy.addr, align 4
  %5 = load float*, float** %yz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx3, float* nonnull align 4 dereferenceable(4) %3, float* nonnull align 4 dereferenceable(4) %4, float* nonnull align 4 dereferenceable(4) %5)
  %m_el4 = getelementptr inbounds %class.btMatrix3x3, %class.btMatrix3x3* %this1, i32 0, i32 0
  %arrayidx5 = getelementptr inbounds [3 x %class.btVector3], [3 x %class.btVector3]* %m_el4, i32 0, i32 2
  %6 = load float*, float** %zx.addr, align 4
  %7 = load float*, float** %zy.addr, align 4
  %8 = load float*, float** %zz.addr, align 4
  call void @_ZN9btVector38setValueERKfS1_S1_(%class.btVector3* %arrayidx5, float* nonnull align 4 dereferenceable(4) %6, float* nonnull align 4 dereferenceable(4) %7, float* nonnull align 4 dereferenceable(4) %8)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_Z15btTransformAabbRK9btVector3S1_fRK11btTransformRS_S5_(%class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMin, %class.btVector3* nonnull align 4 dereferenceable(16) %localAabbMax, float %margin, %class.btTransform* nonnull align 4 dereferenceable(64) %trans, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMinOut, %class.btVector3* nonnull align 4 dereferenceable(16) %aabbMaxOut) #2 comdat {
entry:
  %localAabbMin.addr = alloca %class.btVector3*, align 4
  %localAabbMax.addr = alloca %class.btVector3*, align 4
  %margin.addr = alloca float, align 4
  %trans.addr = alloca %class.btTransform*, align 4
  %aabbMinOut.addr = alloca %class.btVector3*, align 4
  %aabbMaxOut.addr = alloca %class.btVector3*, align 4
  %localHalfExtents = alloca %class.btVector3, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp1 = alloca %class.btVector3, align 4
  %ref.tmp2 = alloca %class.btVector3, align 4
  %localCenter = alloca %class.btVector3, align 4
  %ref.tmp4 = alloca float, align 4
  %ref.tmp5 = alloca %class.btVector3, align 4
  %abs_b = alloca %class.btMatrix3x3, align 4
  %center = alloca %class.btVector3, align 4
  %extent = alloca %class.btVector3, align 4
  %ref.tmp10 = alloca %class.btVector3, align 4
  %ref.tmp11 = alloca %class.btVector3, align 4
  store %class.btVector3* %localAabbMin, %class.btVector3** %localAabbMin.addr, align 4
  store %class.btVector3* %localAabbMax, %class.btVector3** %localAabbMax.addr, align 4
  store float %margin, float* %margin.addr, align 4
  store %class.btTransform* %trans, %class.btTransform** %trans.addr, align 4
  store %class.btVector3* %aabbMinOut, %class.btVector3** %aabbMinOut.addr, align 4
  store %class.btVector3* %aabbMaxOut, %class.btVector3** %aabbMaxOut.addr, align 4
  store float 5.000000e-01, float* %ref.tmp, align 4
  %0 = load %class.btVector3*, %class.btVector3** %localAabbMax.addr, align 4
  %1 = load %class.btVector3*, %class.btVector3** %localAabbMin.addr, align 4
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp1, %class.btVector3* nonnull align 4 dereferenceable(16) %0, %class.btVector3* nonnull align 4 dereferenceable(16) %1)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localHalfExtents, float* nonnull align 4 dereferenceable(4) %ref.tmp, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp1)
  %call = call %class.btVector3* @_ZN9btVector3C2ERKfS1_S1_(%class.btVector3* %ref.tmp2, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr, float* nonnull align 4 dereferenceable(4) %margin.addr)
  %call3 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN9btVector3pLERKS_(%class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp2)
  store float 5.000000e-01, float* %ref.tmp4, align 4
  %2 = load %class.btVector3*, %class.btVector3** %localAabbMax.addr, align 4
  %3 = load %class.btVector3*, %class.btVector3** %localAabbMin.addr, align 4
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp5, %class.btVector3* nonnull align 4 dereferenceable(16) %2, %class.btVector3* nonnull align 4 dereferenceable(16) %3)
  call void @_ZmlRKfRK9btVector3(%class.btVector3* sret align 4 %localCenter, float* nonnull align 4 dereferenceable(4) %ref.tmp4, %class.btVector3* nonnull align 4 dereferenceable(16) %ref.tmp5)
  %4 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  %call6 = call nonnull align 4 dereferenceable(48) %class.btMatrix3x3* @_ZNK11btTransform8getBasisEv(%class.btTransform* %4)
  call void @_ZNK11btMatrix3x38absoluteEv(%class.btMatrix3x3* sret align 4 %abs_b, %class.btMatrix3x3* %call6)
  %5 = load %class.btTransform*, %class.btTransform** %trans.addr, align 4
  call void @_ZNK11btTransformclERK9btVector3(%class.btVector3* sret align 4 %center, %class.btTransform* %5, %class.btVector3* nonnull align 4 dereferenceable(16) %localCenter)
  %call7 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 0)
  %call8 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 1)
  %call9 = call nonnull align 4 dereferenceable(16) %class.btVector3* @_ZN11btMatrix3x3ixEi(%class.btMatrix3x3* %abs_b, i32 2)
  call void @_ZNK9btVector34dot3ERKS_S1_S1_(%class.btVector3* sret align 4 %extent, %class.btVector3* %localHalfExtents, %class.btVector3* nonnull align 4 dereferenceable(16) %call7, %class.btVector3* nonnull align 4 dereferenceable(16) %call8, %class.btVector3* nonnull align 4 dereferenceable(16) %call9)
  call void @_ZmiRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp10, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %6 = load %class.btVector3*, %class.btVector3** %aabbMinOut.addr, align 4
  %7 = bitcast %class.btVector3* %6 to i8*
  %8 = bitcast %class.btVector3* %ref.tmp10 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %7, i8* align 4 %8, i32 16, i1 false)
  call void @_ZplRK9btVector3S1_(%class.btVector3* sret align 4 %ref.tmp11, %class.btVector3* nonnull align 4 dereferenceable(16) %center, %class.btVector3* nonnull align 4 dereferenceable(16) %extent)
  %9 = load %class.btVector3*, %class.btVector3** %aabbMaxOut.addr, align 4
  %10 = bitcast %class.btVector3* %9 to i8*
  %11 = bitcast %class.btVector3* %ref.tmp11 to i8*
  call void @llvm.memcpy.p0i8.p0i8.i32(i8* align 4 %10, i8* align 4 %11, i32 16, i1 false)
  ret void
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btConvexShape.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { cold noreturn nounwind }
attributes #4 = { argmemonly nounwind willreturn }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nounwind readnone speculatable willreturn }
attributes #7 = { nounwind }
attributes #8 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
