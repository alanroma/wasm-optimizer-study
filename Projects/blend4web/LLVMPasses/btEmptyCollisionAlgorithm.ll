; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btEmptyCollisionAlgorithm.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btEmptyCollisionAlgorithm.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btEmptyAlgorithm = type { %class.btCollisionAlgorithm }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%class.btDispatcher = type { i32 (...)** }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.0, %union.anon.1, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.0 = type { float }
%union.anon.1 = type { float }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type opaque
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32, float }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%class.btAlignedObjectArray.2 = type <{ %class.btAlignedAllocator.3, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.3 = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN16btEmptyAlgorithmD2Ev = comdat any

$_ZN16btEmptyAlgorithmD0Ev = comdat any

$_ZN16btEmptyAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE = comdat any

$_ZN20btCollisionAlgorithmD2Ev = comdat any

$_ZTS20btCollisionAlgorithm = comdat any

$_ZTI20btCollisionAlgorithm = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV16btEmptyAlgorithm = hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI16btEmptyAlgorithm to i8*), i8* bitcast (%class.btEmptyAlgorithm* (%class.btEmptyAlgorithm*)* @_ZN16btEmptyAlgorithmD2Ev to i8*), i8* bitcast (void (%class.btEmptyAlgorithm*)* @_ZN16btEmptyAlgorithmD0Ev to i8*), i8* bitcast (void (%class.btEmptyAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN16btEmptyAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (float (%class.btEmptyAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)* @_ZN16btEmptyAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult to i8*), i8* bitcast (void (%class.btEmptyAlgorithm*, %class.btAlignedObjectArray.2*)* @_ZN16btEmptyAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE to i8*)] }, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS16btEmptyAlgorithm = hidden constant [19 x i8] c"16btEmptyAlgorithm\00", align 1
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTS20btCollisionAlgorithm = linkonce_odr hidden constant [23 x i8] c"20btCollisionAlgorithm\00", comdat, align 1
@_ZTI20btCollisionAlgorithm = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([23 x i8], [23 x i8]* @_ZTS20btCollisionAlgorithm, i32 0, i32 0) }, comdat, align 4
@_ZTI16btEmptyAlgorithm = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([19 x i8], [19 x i8]* @_ZTS16btEmptyAlgorithm, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI20btCollisionAlgorithm to i8*) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btEmptyCollisionAlgorithm.cpp, i8* null }]

@_ZN16btEmptyAlgorithmC1ERK36btCollisionAlgorithmConstructionInfo = hidden unnamed_addr alias %class.btEmptyAlgorithm* (%class.btEmptyAlgorithm*, %struct.btCollisionAlgorithmConstructionInfo*), %class.btEmptyAlgorithm* (%class.btEmptyAlgorithm*, %struct.btCollisionAlgorithmConstructionInfo*)* @_ZN16btEmptyAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btEmptyAlgorithm* @_ZN16btEmptyAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo(%class.btEmptyAlgorithm* returned %this, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btEmptyAlgorithm*, align 4
  %ci.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  store %class.btEmptyAlgorithm* %this, %class.btEmptyAlgorithm** %this.addr, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %ci, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %this1 = load %class.btEmptyAlgorithm*, %class.btEmptyAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btEmptyAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %ci.addr, align 4
  %call = call %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo(%class.btCollisionAlgorithm* %0, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %1)
  %2 = bitcast %class.btEmptyAlgorithm* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV16btEmptyAlgorithm, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %2, align 4
  ret %class.btEmptyAlgorithm* %this1
}

declare %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmC2ERK36btCollisionAlgorithmConstructionInfo(%class.btCollisionAlgorithm* returned, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8)) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN16btEmptyAlgorithm16processCollisionEPK24btCollisionObjectWrapperS2_RK16btDispatcherInfoP16btManifoldResult(%class.btEmptyAlgorithm* %this, %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper* %1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %2, %class.btManifoldResult* %3) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btEmptyAlgorithm*, align 4
  %.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %.addr1 = alloca %struct.btCollisionObjectWrapper*, align 4
  %.addr2 = alloca %struct.btDispatcherInfo*, align 4
  %.addr3 = alloca %class.btManifoldResult*, align 4
  store %class.btEmptyAlgorithm* %this, %class.btEmptyAlgorithm** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %.addr, align 4
  store %struct.btCollisionObjectWrapper* %1, %struct.btCollisionObjectWrapper** %.addr1, align 4
  store %struct.btDispatcherInfo* %2, %struct.btDispatcherInfo** %.addr2, align 4
  store %class.btManifoldResult* %3, %class.btManifoldResult** %.addr3, align 4
  %this4 = load %class.btEmptyAlgorithm*, %class.btEmptyAlgorithm** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden float @_ZN16btEmptyAlgorithm21calculateTimeOfImpactEP17btCollisionObjectS1_RK16btDispatcherInfoP16btManifoldResult(%class.btEmptyAlgorithm* %this, %class.btCollisionObject* %0, %class.btCollisionObject* %1, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %2, %class.btManifoldResult* %3) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btEmptyAlgorithm*, align 4
  %.addr = alloca %class.btCollisionObject*, align 4
  %.addr1 = alloca %class.btCollisionObject*, align 4
  %.addr2 = alloca %struct.btDispatcherInfo*, align 4
  %.addr3 = alloca %class.btManifoldResult*, align 4
  store %class.btEmptyAlgorithm* %this, %class.btEmptyAlgorithm** %this.addr, align 4
  store %class.btCollisionObject* %0, %class.btCollisionObject** %.addr, align 4
  store %class.btCollisionObject* %1, %class.btCollisionObject** %.addr1, align 4
  store %struct.btDispatcherInfo* %2, %struct.btDispatcherInfo** %.addr2, align 4
  store %class.btManifoldResult* %3, %class.btManifoldResult** %.addr3, align 4
  %this4 = load %class.btEmptyAlgorithm*, %class.btEmptyAlgorithm** %this.addr, align 4
  ret float 1.000000e+00
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btEmptyAlgorithm* @_ZN16btEmptyAlgorithmD2Ev(%class.btEmptyAlgorithm* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btEmptyAlgorithm*, align 4
  store %class.btEmptyAlgorithm* %this, %class.btEmptyAlgorithm** %this.addr, align 4
  %this1 = load %class.btEmptyAlgorithm*, %class.btEmptyAlgorithm** %this.addr, align 4
  %0 = bitcast %class.btEmptyAlgorithm* %this1 to %class.btCollisionAlgorithm*
  %call = call %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmD2Ev(%class.btCollisionAlgorithm* %0) #5
  ret %class.btEmptyAlgorithm* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btEmptyAlgorithmD0Ev(%class.btEmptyAlgorithm* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btEmptyAlgorithm*, align 4
  store %class.btEmptyAlgorithm* %this, %class.btEmptyAlgorithm** %this.addr, align 4
  %this1 = load %class.btEmptyAlgorithm*, %class.btEmptyAlgorithm** %this.addr, align 4
  %call = call %class.btEmptyAlgorithm* @_ZN16btEmptyAlgorithmD2Ev(%class.btEmptyAlgorithm* %this1) #5
  %0 = bitcast %class.btEmptyAlgorithm* %this1 to i8*
  call void @_ZdlPv(i8* %0) #6
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN16btEmptyAlgorithm22getAllContactManifoldsER20btAlignedObjectArrayIP20btPersistentManifoldE(%class.btEmptyAlgorithm* %this, %class.btAlignedObjectArray.2* nonnull align 4 dereferenceable(17) %manifoldArray) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btEmptyAlgorithm*, align 4
  %manifoldArray.addr = alloca %class.btAlignedObjectArray.2*, align 4
  store %class.btEmptyAlgorithm* %this, %class.btEmptyAlgorithm** %this.addr, align 4
  store %class.btAlignedObjectArray.2* %manifoldArray, %class.btAlignedObjectArray.2** %manifoldArray.addr, align 4
  %this1 = load %class.btEmptyAlgorithm*, %class.btEmptyAlgorithm** %this.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionAlgorithm* @_ZN20btCollisionAlgorithmD2Ev(%class.btCollisionAlgorithm* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionAlgorithm*, align 4
  store %class.btCollisionAlgorithm* %this, %class.btCollisionAlgorithm** %this.addr, align 4
  %this1 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %this.addr, align 4
  ret %class.btCollisionAlgorithm* %this1
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btEmptyCollisionAlgorithm.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind }
attributes #6 = { builtin nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
