; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Dynamics/btSimulationIslandManagerMt.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletDynamics/Dynamics/btSimulationIslandManagerMt.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btSimulationIslandManagerMt = type { %class.btSimulationIslandManager.base, [3 x i8], %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14, %"struct.btSimulationIslandManagerMt::Island"*, i32, i32, void (%class.btAlignedObjectArray.14*, %"struct.btSimulationIslandManagerMt::IslandCallback"*)* }
%class.btSimulationIslandManager.base = type <{ i32 (...)**, %class.btUnionFind, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.10, i8 }>
%class.btUnionFind = type { %class.btAlignedObjectArray }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %struct.btElement*, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%struct.btElement = type { i32, i32 }
%class.btAlignedObjectArray.0 = type <{ %class.btAlignedAllocator.1, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator.1 = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.3, %union.anon.4, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.3 = type { float }
%union.anon.4 = type { float }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.5, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type opaque
%class.btAlignedObjectArray.5 = type <{ %class.btAlignedAllocator.6, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.6 = type { i8 }
%class.btAlignedObjectArray.10 = type <{ %class.btAlignedAllocator.11, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.11 = type { i8 }
%class.btAlignedObjectArray.14 = type <{ %class.btAlignedAllocator.15, [3 x i8], i32, i32, %"struct.btSimulationIslandManagerMt::Island"**, i8, [3 x i8] }>
%class.btAlignedAllocator.15 = type { i8 }
%"struct.btSimulationIslandManagerMt::Island" = type <{ %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.17, i32, i8, [3 x i8] }>
%class.btAlignedObjectArray.17 = type <{ %class.btAlignedAllocator.18, [3 x i8], i32, i32, %class.btTypedConstraint**, i8, [3 x i8] }>
%class.btAlignedAllocator.18 = type { i8 }
%class.btTypedConstraint = type { i32 (...)**, %struct.btTypedObject, i32, %union.anon.20, float, i8, i8, i32, %class.btRigidBody*, %class.btRigidBody*, float, float, %struct.btJointFeedback* }
%union.anon.20 = type { i32 }
%class.btRigidBody = type { %class.btCollisionObject, %class.btMatrix3x3, %class.btVector3, %class.btVector3, float, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, i8, float, float, float, float, float, float, %class.btMotionState*, %class.btAlignedObjectArray.17, i32, i32, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, i32, i32 }
%class.btMotionState = type { i32 (...)** }
%struct.btJointFeedback = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3 }
%"struct.btSimulationIslandManagerMt::IslandCallback" = type { i32 (...)** }
%class.btSimulationIslandManager = type <{ i32 (...)**, %class.btUnionFind, %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.10, i8, [3 x i8] }>
%class.IslandBodyCapacitySortPredicate = type { i8 }
%class.btDispatcher = type { i32 (...)** }
%class.btCollisionWorld = type <{ i32 (...)**, %class.btAlignedObjectArray.10, %class.btDispatcher*, %struct.btDispatcherInfo, %class.btBroadphaseInterface*, %class.btIDebugDraw*, i8, [3 x i8] }>
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btBroadphaseInterface = type { i32 (...)** }
%class.btIDebugDraw = type opaque
%class.CProfileSample = type { i8 }
%class.IslandBatchSizeSortPredicate = type { i8 }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEC2Ev = comdat any

$_Z13calcBatchCostiii = comdat any

$_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi = comdat any

$_ZN27btSimulationIslandManagerMt6IslandD2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE6resizeEiRKS2_ = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEED2Ev = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectEixEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintEixEi = comdat any

$_ZN25btSimulationIslandManager12getUnionFindEv = comdat any

$_ZNK11btUnionFind14getNumElementsEv = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE9quickSortI31IslandBodyCapacitySortPredicateEEvRKT_ = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_ = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE9push_backERKS2_ = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE8pop_backEv = comdat any

$_ZN27btSimulationIslandManagerMt6IslandC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi = comdat any

$_ZN16btCollisionWorld23getCollisionObjectArrayEv = comdat any

$_ZN11btUnionFind10getElementEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi = comdat any

$_ZNK17btCollisionObject12getIslandTagEv = comdat any

$_ZNK17btCollisionObject18getActivationStateEv = comdat any

$_ZN17btCollisionObject19setDeactivationTimeEf = comdat any

$_ZNK17btCollisionObject8isActiveEv = comdat any

$_ZNK20btPersistentManifold8getBody0Ev = comdat any

$_ZNK20btPersistentManifold8getBody1Ev = comdat any

$_ZNK17btCollisionObject17isKinematicObjectEv = comdat any

$_ZNK17btCollisionObject18hasContactResponseEv = comdat any

$_Z11getIslandIdPK20btPersistentManifold = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi = comdat any

$_ZNK17btTypedConstraint9isEnabledEv = comdat any

$_Z23btGetConstraintIslandIdPK17btTypedConstraint = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE9quickSortI28IslandBatchSizeSortPredicateEEvRKT_ = comdat any

$_Z13calcBatchCostPKN27btSimulationIslandManagerMt6IslandE = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZN25btSimulationIslandManager15getSplitIslandsEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_ = comdat any

$_ZNK20btAlignedObjectArrayI9btElementE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayI9btElementEixEi = comdat any

$_ZNK17btTypedConstraint13getRigidBodyAEv = comdat any

$_ZNK17btTypedConstraint13getRigidBodyBEv = comdat any

$_ZN18btAlignedAllocatorIPN27btSimulationIslandManagerMt6IslandELj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4initEv = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIPN27btSimulationIslandManagerMt6IslandELj16EE10deallocateEPS2_ = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE7reserveEi = comdat any

$_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4copyEiiPS2_ = comdat any

$_ZN18btAlignedAllocatorIPN27btSimulationIslandManagerMt6IslandELj16EE8allocateEiPPKS2_ = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE17quickSortInternalI31IslandBodyCapacitySortPredicateEEvRKT_ii = comdat any

$_ZNK31IslandBodyCapacitySortPredicateclEPKN27btSimulationIslandManagerMt6IslandES3_ = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE17quickSortInternalI28IslandBatchSizeSortPredicateEEvRKT_ii = comdat any

$_ZNK28IslandBatchSizeSortPredicateclEPKN27btSimulationIslandManagerMt6IslandES3_ = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_ = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@_ZTV27btSimulationIslandManagerMt = hidden unnamed_addr constant { [14 x i8*] } { [14 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI27btSimulationIslandManagerMt to i8*), i8* bitcast (%class.btSimulationIslandManagerMt* (%class.btSimulationIslandManagerMt*)* @_ZN27btSimulationIslandManagerMtD1Ev to i8*), i8* bitcast (void (%class.btSimulationIslandManagerMt*)* @_ZN27btSimulationIslandManagerMtD0Ev to i8*), i8* bitcast (void (%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*)* @_ZN25btSimulationIslandManager21updateActivationStateEP16btCollisionWorldP12btDispatcher to i8*), i8* bitcast (void (%class.btSimulationIslandManager*, %class.btCollisionWorld*)* @_ZN25btSimulationIslandManager26storeIslandActivationStateEP16btCollisionWorld to i8*), i8* bitcast (%"struct.btSimulationIslandManagerMt::Island"* (%class.btSimulationIslandManagerMt*, i32, i32)* @_ZN27btSimulationIslandManagerMt14allocateIslandEii to i8*), i8* bitcast (void (%class.btSimulationIslandManagerMt*)* @_ZN27btSimulationIslandManagerMt15initIslandPoolsEv to i8*), i8* bitcast (void (%class.btSimulationIslandManagerMt*, %class.btCollisionWorld*)* @_ZN27btSimulationIslandManagerMt18addBodiesToIslandsEP16btCollisionWorld to i8*), i8* bitcast (void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*)* @_ZN27btSimulationIslandManagerMt21addManifoldsToIslandsEP12btDispatcher to i8*), i8* bitcast (void (%class.btSimulationIslandManagerMt*, %class.btAlignedObjectArray.17*)* @_ZN27btSimulationIslandManagerMt23addConstraintsToIslandsER20btAlignedObjectArrayIP17btTypedConstraintE to i8*), i8* bitcast (void (%class.btSimulationIslandManagerMt*)* @_ZN27btSimulationIslandManagerMt12mergeIslandsEv to i8*), i8* bitcast (void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*, %class.btAlignedObjectArray.17*, %"struct.btSimulationIslandManagerMt::IslandCallback"*)* @_ZN27btSimulationIslandManagerMt22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldR20btAlignedObjectArrayIP17btTypedConstraintEPNS_14IslandCallbackE to i8*), i8* bitcast (void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*)* @_ZN27btSimulationIslandManagerMt12buildIslandsEP12btDispatcherP16btCollisionWorld to i8*)] }, align 4
@.str = private unnamed_addr constant [28 x i8] c"islandUnionFindAndQuickSort\00", align 1
@.str.1 = private unnamed_addr constant [15 x i8] c"processIslands\00", align 1
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS27btSimulationIslandManagerMt = hidden constant [30 x i8] c"27btSimulationIslandManagerMt\00", align 1
@_ZTI25btSimulationIslandManager = external constant i8*
@_ZTI27btSimulationIslandManagerMt = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([30 x i8], [30 x i8]* @_ZTS27btSimulationIslandManagerMt, i32 0, i32 0), i8* bitcast (i8** @_ZTI25btSimulationIslandManager to i8*) }, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btSimulationIslandManagerMt.cpp, i8* null }]

@_ZN27btSimulationIslandManagerMtC1Ev = hidden unnamed_addr alias %class.btSimulationIslandManagerMt* (%class.btSimulationIslandManagerMt*), %class.btSimulationIslandManagerMt* (%class.btSimulationIslandManagerMt*)* @_ZN27btSimulationIslandManagerMtC2Ev
@_ZN27btSimulationIslandManagerMtD1Ev = hidden unnamed_addr alias %class.btSimulationIslandManagerMt* (%class.btSimulationIslandManagerMt*), %class.btSimulationIslandManagerMt* (%class.btSimulationIslandManagerMt*)* @_ZN27btSimulationIslandManagerMtD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btSimulationIslandManagerMt* @_ZN27btSimulationIslandManagerMtC2Ev(%class.btSimulationIslandManagerMt* returned %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManagerMt*, align 4
  store %class.btSimulationIslandManagerMt* %this, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %this1 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %0 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call = call %class.btSimulationIslandManager* @_ZN25btSimulationIslandManagerC2Ev(%class.btSimulationIslandManager* %0)
  %1 = bitcast %class.btSimulationIslandManagerMt* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTV27btSimulationIslandManagerMt, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_allocatedIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEC2Ev(%class.btAlignedObjectArray.14* %m_allocatedIslands)
  %m_activeIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  %call3 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEC2Ev(%class.btAlignedObjectArray.14* %m_activeIslands)
  %m_freeIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 4
  %call4 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEC2Ev(%class.btAlignedObjectArray.14* %m_freeIslands)
  %m_lookupIslandFromId = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 5
  %call5 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEC2Ev(%class.btAlignedObjectArray.14* %m_lookupIslandFromId)
  %call6 = call i32 @_Z13calcBatchCostiii(i32 0, i32 128, i32 0)
  %m_minimumSolverBatchSize = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 7
  store i32 %call6, i32* %m_minimumSolverBatchSize, align 4
  %m_batchIslandMinBodyCount = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 8
  store i32 32, i32* %m_batchIslandMinBodyCount, align 4
  %m_islandDispatch = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 9
  store void (%class.btAlignedObjectArray.14*, %"struct.btSimulationIslandManagerMt::IslandCallback"*)* @_ZN27btSimulationIslandManagerMt21defaultIslandDispatchEP20btAlignedObjectArrayIPNS_6IslandEEPNS_14IslandCallbackE, void (%class.btAlignedObjectArray.14*, %"struct.btSimulationIslandManagerMt::IslandCallback"*)** %m_islandDispatch, align 4
  %m_batchIsland = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 6
  store %"struct.btSimulationIslandManagerMt::Island"* null, %"struct.btSimulationIslandManagerMt::Island"** %m_batchIsland, align 4
  ret %class.btSimulationIslandManagerMt* %this1
}

declare %class.btSimulationIslandManager* @_ZN25btSimulationIslandManagerC2Ev(%class.btSimulationIslandManager* returned) unnamed_addr #3

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEC2Ev(%class.btAlignedObjectArray.14* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.15* @_ZN18btAlignedAllocatorIPN27btSimulationIslandManagerMt6IslandELj16EEC2Ev(%class.btAlignedAllocator.15* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4initEv(%class.btAlignedObjectArray.14* %this1)
  ret %class.btAlignedObjectArray.14* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_Z13calcBatchCostiii(i32 %bodies, i32 %manifolds, i32 %constraints) #1 comdat {
entry:
  %bodies.addr = alloca i32, align 4
  %manifolds.addr = alloca i32, align 4
  %constraints.addr = alloca i32, align 4
  %batchCost = alloca i32, align 4
  store i32 %bodies, i32* %bodies.addr, align 4
  store i32 %manifolds, i32* %manifolds.addr, align 4
  store i32 %constraints, i32* %constraints.addr, align 4
  %0 = load i32, i32* %bodies.addr, align 4
  %1 = load i32, i32* %manifolds.addr, align 4
  %mul = mul nsw i32 8, %1
  %add = add nsw i32 %0, %mul
  %2 = load i32, i32* %constraints.addr, align 4
  %mul1 = mul nsw i32 4, %2
  %add2 = add nsw i32 %add, %mul1
  store i32 %add2, i32* %batchCost, align 4
  %3 = load i32, i32* %batchCost, align 4
  ret i32 %3
}

; Function Attrs: noinline optnone
define hidden void @_ZN27btSimulationIslandManagerMt21defaultIslandDispatchEP20btAlignedObjectArrayIPNS_6IslandEEPNS_14IslandCallbackE(%class.btAlignedObjectArray.14* %islandsPtr, %"struct.btSimulationIslandManagerMt::IslandCallback"* %callback) #2 {
entry:
  %islandsPtr.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %callback.addr = alloca %"struct.btSimulationIslandManagerMt::IslandCallback"*, align 4
  %islands = alloca %class.btAlignedObjectArray.14*, align 4
  %i = alloca i32, align 4
  %island = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %manifolds = alloca %class.btPersistentManifold**, align 4
  %constraintsPtr = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedObjectArray.14* %islandsPtr, %class.btAlignedObjectArray.14** %islandsPtr.addr, align 4
  store %"struct.btSimulationIslandManagerMt::IslandCallback"* %callback, %"struct.btSimulationIslandManagerMt::IslandCallback"** %callback.addr, align 4
  %0 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %islandsPtr.addr, align 4
  store %class.btAlignedObjectArray.14* %0, %class.btAlignedObjectArray.14** %islands, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %islands, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %2)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %islands, align 4
  %4 = load i32, i32* %i, align 4
  %call1 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %3, i32 %4)
  %5 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %call1, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %5, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %6 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %manifoldArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %6, i32 0, i32 1
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %manifoldArray)
  %tobool = icmp ne i32 %call2, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.body
  %7 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %manifoldArray3 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %7, i32 0, i32 1
  %call4 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.0* %manifoldArray3, i32 0)
  br label %cond.end

cond.false:                                       ; preds = %for.body
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btPersistentManifold** [ %call4, %cond.true ], [ null, %cond.false ]
  store %class.btPersistentManifold** %cond, %class.btPersistentManifold*** %manifolds, align 4
  %8 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %constraintArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %8, i32 0, i32 2
  %call5 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %constraintArray)
  %tobool6 = icmp ne i32 %call5, 0
  br i1 %tobool6, label %cond.true7, label %cond.false10

cond.true7:                                       ; preds = %cond.end
  %9 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %constraintArray8 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %9, i32 0, i32 2
  %call9 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.17* %constraintArray8, i32 0)
  br label %cond.end11

cond.false10:                                     ; preds = %cond.end
  br label %cond.end11

cond.end11:                                       ; preds = %cond.false10, %cond.true7
  %cond12 = phi %class.btTypedConstraint** [ %call9, %cond.true7 ], [ null, %cond.false10 ]
  store %class.btTypedConstraint** %cond12, %class.btTypedConstraint*** %constraintsPtr, align 4
  %10 = load %"struct.btSimulationIslandManagerMt::IslandCallback"*, %"struct.btSimulationIslandManagerMt::IslandCallback"** %callback.addr, align 4
  %11 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %bodyArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %11, i32 0, i32 0
  %call13 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %bodyArray, i32 0)
  %12 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %bodyArray14 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %12, i32 0, i32 0
  %call15 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %bodyArray14)
  %13 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifolds, align 4
  %14 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %manifoldArray16 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %14, i32 0, i32 1
  %call17 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %manifoldArray16)
  %15 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraintsPtr, align 4
  %16 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %constraintArray18 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %16, i32 0, i32 2
  %call19 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %constraintArray18)
  %17 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %id = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %17, i32 0, i32 3
  %18 = load i32, i32* %id, align 4
  %19 = bitcast %"struct.btSimulationIslandManagerMt::IslandCallback"* %10 to void (%"struct.btSimulationIslandManagerMt::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)***
  %vtable = load void (%"struct.btSimulationIslandManagerMt::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)**, void (%"struct.btSimulationIslandManagerMt::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)*** %19, align 4
  %vfn = getelementptr inbounds void (%"struct.btSimulationIslandManagerMt::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)*, void (%"struct.btSimulationIslandManagerMt::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)** %vtable, i64 2
  %20 = load void (%"struct.btSimulationIslandManagerMt::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)*, void (%"struct.btSimulationIslandManagerMt::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)** %vfn, align 4
  call void %20(%"struct.btSimulationIslandManagerMt::IslandCallback"* %10, %class.btCollisionObject** %call13, i32 %call15, %class.btPersistentManifold** %13, i32 %call17, %class.btTypedConstraint** %15, i32 %call19, i32 %18)
  br label %for.inc

for.inc:                                          ; preds = %cond.end11
  %21 = load i32, i32* %i, align 4
  %inc = add nsw i32 %21, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btSimulationIslandManagerMt* @_ZN27btSimulationIslandManagerMtD2Ev(%class.btSimulationIslandManagerMt* returned %this) unnamed_addr #1 {
entry:
  %retval = alloca %class.btSimulationIslandManagerMt*, align 4
  %this.addr = alloca %class.btSimulationIslandManagerMt*, align 4
  %i = alloca i32, align 4
  %ref.tmp = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %ref.tmp6 = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %ref.tmp7 = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  store %class.btSimulationIslandManagerMt* %this, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %this1 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %this.addr, align 4
  store %class.btSimulationIslandManagerMt* %this1, %class.btSimulationIslandManagerMt** %retval, align 4
  %0 = bitcast %class.btSimulationIslandManagerMt* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [14 x i8*] }, { [14 x i8*] }* @_ZTV27btSimulationIslandManagerMt, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %m_allocatedIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %m_allocatedIslands)
  %cmp = icmp slt i32 %1, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_allocatedIslands2 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 2
  %2 = load i32, i32* %i, align 4
  %call3 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %m_allocatedIslands2, i32 %2)
  %3 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %call3, align 4
  %isnull = icmp eq %"struct.btSimulationIslandManagerMt::Island"* %3, null
  br i1 %isnull, label %delete.end, label %delete.notnull

delete.notnull:                                   ; preds = %for.body
  %call4 = call %"struct.btSimulationIslandManagerMt::Island"* @_ZN27btSimulationIslandManagerMt6IslandD2Ev(%"struct.btSimulationIslandManagerMt::Island"* %3) #8
  %4 = bitcast %"struct.btSimulationIslandManagerMt::Island"* %3 to i8*
  call void @_ZdlPv(i8* %4) #9
  br label %delete.end

delete.end:                                       ; preds = %delete.notnull, %for.body
  br label %for.inc

for.inc:                                          ; preds = %delete.end
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_allocatedIslands5 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 2
  store %"struct.btSimulationIslandManagerMt::Island"* null, %"struct.btSimulationIslandManagerMt::Island"** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE6resizeEiRKS2_(%class.btAlignedObjectArray.14* %m_allocatedIslands5, i32 0, %"struct.btSimulationIslandManagerMt::Island"** nonnull align 4 dereferenceable(4) %ref.tmp)
  %m_activeIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  store %"struct.btSimulationIslandManagerMt::Island"* null, %"struct.btSimulationIslandManagerMt::Island"** %ref.tmp6, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE6resizeEiRKS2_(%class.btAlignedObjectArray.14* %m_activeIslands, i32 0, %"struct.btSimulationIslandManagerMt::Island"** nonnull align 4 dereferenceable(4) %ref.tmp6)
  %m_freeIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 4
  store %"struct.btSimulationIslandManagerMt::Island"* null, %"struct.btSimulationIslandManagerMt::Island"** %ref.tmp7, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE6resizeEiRKS2_(%class.btAlignedObjectArray.14* %m_freeIslands, i32 0, %"struct.btSimulationIslandManagerMt::Island"** nonnull align 4 dereferenceable(4) %ref.tmp7)
  %m_lookupIslandFromId = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 5
  %call8 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEED2Ev(%class.btAlignedObjectArray.14* %m_lookupIslandFromId) #8
  %m_freeIslands9 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 4
  %call10 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEED2Ev(%class.btAlignedObjectArray.14* %m_freeIslands9) #8
  %m_activeIslands11 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  %call12 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEED2Ev(%class.btAlignedObjectArray.14* %m_activeIslands11) #8
  %m_allocatedIslands13 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 2
  %call14 = call %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEED2Ev(%class.btAlignedObjectArray.14* %m_allocatedIslands13) #8
  %6 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call15 = call %class.btSimulationIslandManager* @_ZN25btSimulationIslandManagerD2Ev(%class.btSimulationIslandManager* %6) #8
  %7 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %retval, align 4
  ret %class.btSimulationIslandManagerMt* %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %0 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %0, i32 %1
  ret %"struct.btSimulationIslandManagerMt::Island"** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btSimulationIslandManagerMt::Island"* @_ZN27btSimulationIslandManagerMt6IslandD2Ev(%"struct.btSimulationIslandManagerMt::Island"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %this, %"struct.btSimulationIslandManagerMt::Island"** %this.addr, align 4
  %this1 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %this.addr, align 4
  %constraintArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %this1, i32 0, i32 2
  %call = call %class.btAlignedObjectArray.17* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.17* %constraintArray) #8
  %manifoldArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.0* %manifoldArray) #8
  %bodyArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %this1, i32 0, i32 0
  %call3 = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev(%class.btAlignedObjectArray.10* %bodyArray) #8
  ret %"struct.btSimulationIslandManagerMt::Island"* %this1
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE6resizeEiRKS2_(%class.btAlignedObjectArray.14* %this, i32 %newsize, %"struct.btSimulationIslandManagerMt::Island"** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %"struct.btSimulationIslandManagerMt::Island"**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %"struct.btSimulationIslandManagerMt::Island"** %fillData, %"struct.btSimulationIslandManagerMt::Island"*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %5 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE7reserveEi(%class.btAlignedObjectArray.14* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %14 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %14, i32 %15
  %16 = bitcast %"struct.btSimulationIslandManagerMt::Island"** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %"struct.btSimulationIslandManagerMt::Island"**
  %18 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %fillData.addr, align 4
  %19 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %18, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %19, %"struct.btSimulationIslandManagerMt::Island"** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.14* @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEED2Ev(%class.btAlignedObjectArray.14* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE5clearEv(%class.btAlignedObjectArray.14* %this1)
  ret %class.btAlignedObjectArray.14* %this1
}

; Function Attrs: nounwind
declare %class.btSimulationIslandManager* @_ZN25btSimulationIslandManagerD2Ev(%class.btSimulationIslandManager* returned) unnamed_addr #5

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN27btSimulationIslandManagerMtD0Ev(%class.btSimulationIslandManagerMt* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btSimulationIslandManagerMt*, align 4
  store %class.btSimulationIslandManagerMt* %this, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %this1 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %call = call %class.btSimulationIslandManagerMt* @_ZN27btSimulationIslandManagerMtD1Ev(%class.btSimulationIslandManagerMt* %this1) #8
  %0 = bitcast %class.btSimulationIslandManagerMt* %this1 to i8*
  call void @_ZdlPv(i8* %0) #9
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN27btSimulationIslandManagerMt6Island6appendERKS0_(%"struct.btSimulationIslandManagerMt::Island"* %this, %"struct.btSimulationIslandManagerMt::Island"* nonnull align 4 dereferenceable(65) %other) #2 {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %other.addr = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  %i16 = alloca i32, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %this, %"struct.btSimulationIslandManagerMt::Island"** %this.addr, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %other, %"struct.btSimulationIslandManagerMt::Island"** %other.addr, align 4
  %this1 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %other.addr, align 4
  %bodyArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %bodyArray)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %bodyArray2 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %this1, i32 0, i32 0
  %2 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %other.addr, align 4
  %bodyArray3 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %2, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZNK20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %bodyArray3, i32 %3)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray.10* %bodyArray2, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %call4)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i32 0, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc13, %for.end
  %5 = load i32, i32* %i5, align 4
  %6 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %other.addr, align 4
  %manifoldArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %6, i32 0, i32 1
  %call7 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %manifoldArray)
  %cmp8 = icmp slt i32 %5, %call7
  br i1 %cmp8, label %for.body9, label %for.end15

for.body9:                                        ; preds = %for.cond6
  %manifoldArray10 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %this1, i32 0, i32 1
  %7 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %other.addr, align 4
  %manifoldArray11 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %7, i32 0, i32 1
  %8 = load i32, i32* %i5, align 4
  %call12 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.0* %manifoldArray11, i32 %8)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.0* %manifoldArray10, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %call12)
  br label %for.inc13

for.inc13:                                        ; preds = %for.body9
  %9 = load i32, i32* %i5, align 4
  %inc14 = add nsw i32 %9, 1
  store i32 %inc14, i32* %i5, align 4
  br label %for.cond6

for.end15:                                        ; preds = %for.cond6
  store i32 0, i32* %i16, align 4
  br label %for.cond17

for.cond17:                                       ; preds = %for.inc24, %for.end15
  %10 = load i32, i32* %i16, align 4
  %11 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %other.addr, align 4
  %constraintArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %11, i32 0, i32 2
  %call18 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %constraintArray)
  %cmp19 = icmp slt i32 %10, %call18
  br i1 %cmp19, label %for.body20, label %for.end26

for.body20:                                       ; preds = %for.cond17
  %constraintArray21 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %this1, i32 0, i32 2
  %12 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %other.addr, align 4
  %constraintArray22 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %12, i32 0, i32 2
  %13 = load i32, i32* %i16, align 4
  %call23 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZNK20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.17* %constraintArray22, i32 %13)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_(%class.btAlignedObjectArray.17* %constraintArray21, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %call23)
  br label %for.inc24

for.inc24:                                        ; preds = %for.body20
  %14 = load i32, i32* %i16, align 4
  %inc25 = add nsw i32 %14, 1
  store i32 %inc25, i32* %i16, align 4
  br label %for.cond17

for.end26:                                        ; preds = %for.cond17
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray.10* %this, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %_Val.addr = alloca %class.btCollisionObject**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store %class.btCollisionObject** %_Val, %class.btCollisionObject*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray.10* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray.10* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray.10* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %1 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %1, i32 %2
  %3 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btCollisionObject**
  %5 = load %class.btCollisionObject**, %class.btCollisionObject*** %_Val.addr, align 4
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %5, align 4
  store %class.btCollisionObject* %6, %class.btCollisionObject** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZNK20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %0, i32 %1
  ret %class.btCollisionObject** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.0* %this, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Val.addr = alloca %class.btPersistentManifold**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store %class.btPersistentManifold** %_Val, %class.btPersistentManifold*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.0* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %1 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %1, i32 %2
  %3 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btPersistentManifold**
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %_Val.addr, align 4
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %5, align 4
  store %class.btPersistentManifold* %6, %class.btPersistentManifold** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_(%class.btAlignedObjectArray.17* %this, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  %_Val.addr = alloca %class.btTypedConstraint**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  store %class.btTypedConstraint** %_Val, %class.btTypedConstraint*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.17* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi(%class.btAlignedObjectArray.17* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.17* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 4
  %1 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %1, i32 %2
  %3 = bitcast %class.btTypedConstraint** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btTypedConstraint**
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %_Val.addr, align 4
  %6 = load %class.btTypedConstraint*, %class.btTypedConstraint** %5, align 4
  store %class.btTypedConstraint* %6, %class.btTypedConstraint** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZNK20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.17* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %0, i32 %1
  ret %class.btTypedConstraint** %arrayidx
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_Z16btIsBodyInIslandRKN27btSimulationIslandManagerMt6IslandEPK17btCollisionObject(%"struct.btSimulationIslandManagerMt::Island"* nonnull align 4 dereferenceable(65) %island, %class.btCollisionObject* %obj) #2 {
entry:
  %retval = alloca i1, align 1
  %island.addr = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %obj.addr = alloca %class.btCollisionObject*, align 4
  %i = alloca i32, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %island, %"struct.btSimulationIslandManagerMt::Island"** %island.addr, align 4
  store %class.btCollisionObject* %obj, %class.btCollisionObject** %obj.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island.addr, align 4
  %bodyArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %bodyArray)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island.addr, align 4
  %bodyArray1 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %2, i32 0, i32 0
  %3 = load i32, i32* %i, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZNK20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %bodyArray1, i32 %3)
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %call2, align 4
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %obj.addr, align 4
  %cmp3 = icmp eq %class.btCollisionObject* %4, %5
  br i1 %cmp3, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %6 = load i32, i32* %i, align 4
  %inc = add nsw i32 %6, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %for.end, %if.then
  %7 = load i1, i1* %retval, align 1
  ret i1 %7
}

; Function Attrs: noinline optnone
define hidden void @_ZN27btSimulationIslandManagerMt15initIslandPoolsEv(%class.btSimulationIslandManagerMt* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManagerMt*, align 4
  %numElem = alloca i32, align 4
  %ref.tmp = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %i = alloca i32, align 4
  %ref.tmp7 = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %ref.tmp8 = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %lastCapacity = alloca i32, align 4
  %isSorted = alloca i8, align 1
  %i9 = alloca i32, align 4
  %island = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %cap = alloca i32, align 4
  %ref.tmp23 = alloca %class.IslandBodyCapacitySortPredicate, align 1
  %i25 = alloca i32, align 4
  %island31 = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %ref.tmp35 = alloca %class.btCollisionObject*, align 4
  %ref.tmp36 = alloca %class.btPersistentManifold*, align 4
  %ref.tmp37 = alloca %class.btTypedConstraint*, align 4
  store %class.btSimulationIslandManagerMt* %this, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %this1 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %0 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %0)
  %call2 = call i32 @_ZNK11btUnionFind14getNumElementsEv(%class.btUnionFind* %call)
  store i32 %call2, i32* %numElem, align 4
  %m_lookupIslandFromId = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 5
  %1 = load i32, i32* %numElem, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* null, %"struct.btSimulationIslandManagerMt::Island"** %ref.tmp, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE6resizeEiRKS2_(%class.btAlignedObjectArray.14* %m_lookupIslandFromId, i32 %1, %"struct.btSimulationIslandManagerMt::Island"** nonnull align 4 dereferenceable(4) %ref.tmp)
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %2 = load i32, i32* %i, align 4
  %m_lookupIslandFromId3 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 5
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %m_lookupIslandFromId3)
  %cmp = icmp slt i32 %2, %call4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_lookupIslandFromId5 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 5
  %3 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %m_lookupIslandFromId5, i32 %3)
  store %"struct.btSimulationIslandManagerMt::Island"* null, %"struct.btSimulationIslandManagerMt::Island"** %call6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %4 = load i32, i32* %i, align 4
  %inc = add nsw i32 %4, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_activeIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  store %"struct.btSimulationIslandManagerMt::Island"* null, %"struct.btSimulationIslandManagerMt::Island"** %ref.tmp7, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE6resizeEiRKS2_(%class.btAlignedObjectArray.14* %m_activeIslands, i32 0, %"struct.btSimulationIslandManagerMt::Island"** nonnull align 4 dereferenceable(4) %ref.tmp7)
  %m_freeIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 4
  store %"struct.btSimulationIslandManagerMt::Island"* null, %"struct.btSimulationIslandManagerMt::Island"** %ref.tmp8, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE6resizeEiRKS2_(%class.btAlignedObjectArray.14* %m_freeIslands, i32 0, %"struct.btSimulationIslandManagerMt::Island"** nonnull align 4 dereferenceable(4) %ref.tmp8)
  store i32 0, i32* %lastCapacity, align 4
  store i8 1, i8* %isSorted, align 1
  store i32 0, i32* %i9, align 4
  br label %for.cond10

for.cond10:                                       ; preds = %for.inc18, %for.end
  %5 = load i32, i32* %i9, align 4
  %m_allocatedIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 2
  %call11 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %m_allocatedIslands)
  %cmp12 = icmp slt i32 %5, %call11
  br i1 %cmp12, label %for.body13, label %for.end20

for.body13:                                       ; preds = %for.cond10
  %m_allocatedIslands14 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 2
  %6 = load i32, i32* %i9, align 4
  %call15 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %m_allocatedIslands14, i32 %6)
  %7 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %call15, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %7, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %8 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %bodyArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %8, i32 0, i32 0
  %call16 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray.10* %bodyArray)
  store i32 %call16, i32* %cap, align 4
  %9 = load i32, i32* %cap, align 4
  %10 = load i32, i32* %lastCapacity, align 4
  %cmp17 = icmp sgt i32 %9, %10
  br i1 %cmp17, label %if.then, label %if.end

if.then:                                          ; preds = %for.body13
  store i8 0, i8* %isSorted, align 1
  br label %for.end20

if.end:                                           ; preds = %for.body13
  %11 = load i32, i32* %cap, align 4
  store i32 %11, i32* %lastCapacity, align 4
  br label %for.inc18

for.inc18:                                        ; preds = %if.end
  %12 = load i32, i32* %i9, align 4
  %inc19 = add nsw i32 %12, 1
  store i32 %inc19, i32* %i9, align 4
  br label %for.cond10

for.end20:                                        ; preds = %if.then, %for.cond10
  %13 = load i8, i8* %isSorted, align 1
  %tobool = trunc i8 %13 to i1
  br i1 %tobool, label %if.end24, label %if.then21

if.then21:                                        ; preds = %for.end20
  %m_allocatedIslands22 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE9quickSortI31IslandBodyCapacitySortPredicateEEvRKT_(%class.btAlignedObjectArray.14* %m_allocatedIslands22, %class.IslandBodyCapacitySortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp23)
  br label %if.end24

if.end24:                                         ; preds = %if.then21, %for.end20
  %m_batchIsland = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 6
  store %"struct.btSimulationIslandManagerMt::Island"* null, %"struct.btSimulationIslandManagerMt::Island"** %m_batchIsland, align 4
  store i32 0, i32* %i25, align 4
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc39, %if.end24
  %14 = load i32, i32* %i25, align 4
  %m_allocatedIslands27 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 2
  %call28 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %m_allocatedIslands27)
  %cmp29 = icmp slt i32 %14, %call28
  br i1 %cmp29, label %for.body30, label %for.end41

for.body30:                                       ; preds = %for.cond26
  %m_allocatedIslands32 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 2
  %15 = load i32, i32* %i25, align 4
  %call33 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %m_allocatedIslands32, i32 %15)
  %16 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %call33, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %16, %"struct.btSimulationIslandManagerMt::Island"** %island31, align 4
  %17 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island31, align 4
  %bodyArray34 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %17, i32 0, i32 0
  store %class.btCollisionObject* null, %class.btCollisionObject** %ref.tmp35, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray.10* %bodyArray34, i32 0, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %ref.tmp35)
  %18 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island31, align 4
  %manifoldArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %18, i32 0, i32 1
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %ref.tmp36, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.0* %manifoldArray, i32 0, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %ref.tmp36)
  %19 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island31, align 4
  %constraintArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %19, i32 0, i32 2
  store %class.btTypedConstraint* null, %class.btTypedConstraint** %ref.tmp37, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.17* %constraintArray, i32 0, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %ref.tmp37)
  %20 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island31, align 4
  %id = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %20, i32 0, i32 3
  store i32 -1, i32* %id, align 4
  %21 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island31, align 4
  %isSleeping = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %21, i32 0, i32 4
  store i8 1, i8* %isSleeping, align 4
  %m_freeIslands38 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE9push_backERKS2_(%class.btAlignedObjectArray.14* %m_freeIslands38, %"struct.btSimulationIslandManagerMt::Island"** nonnull align 4 dereferenceable(4) %island31)
  br label %for.inc39

for.inc39:                                        ; preds = %for.body30
  %22 = load i32, i32* %i25, align 4
  %inc40 = add nsw i32 %22, 1
  store i32 %inc40, i32* %i25, align 4
  br label %for.cond26

for.end41:                                        ; preds = %for.cond26
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %m_unionFind = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 1
  ret %class.btUnionFind* %m_unionFind
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK11btUnionFind14getNumElementsEv(%class.btUnionFind* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %m_elements)
  ret i32 %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray.10* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE9quickSortI31IslandBodyCapacitySortPredicateEEvRKT_(%class.btAlignedObjectArray.14* %this, %class.IslandBodyCapacitySortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %CompareFunc.addr = alloca %class.IslandBodyCapacitySortPredicate*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store %class.IslandBodyCapacitySortPredicate* %CompareFunc, %class.IslandBodyCapacitySortPredicate** %CompareFunc.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.IslandBodyCapacitySortPredicate*, %class.IslandBodyCapacitySortPredicate** %CompareFunc.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE17quickSortInternalI31IslandBodyCapacitySortPredicateEEvRKT_ii(%class.btAlignedObjectArray.14* %this1, %class.IslandBodyCapacitySortPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE6resizeEiRKS1_(%class.btAlignedObjectArray.10* %this, i32 %newsize, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btCollisionObject**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btCollisionObject** %fillData, %class.btCollisionObject*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %5 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray.10* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %14 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %14, i32 %15
  %16 = bitcast %class.btCollisionObject** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btCollisionObject**
  %18 = load %class.btCollisionObject**, %class.btCollisionObject*** %fillData.addr, align 4
  %19 = load %class.btCollisionObject*, %class.btCollisionObject** %18, align 4
  store %class.btCollisionObject* %19, %class.btCollisionObject** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE6resizeEiRKS1_(%class.btAlignedObjectArray.0* %this, i32 %newsize, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btPersistentManifold**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btPersistentManifold** %fillData, %class.btPersistentManifold*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.0* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %14 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %14, i32 %15
  %16 = bitcast %class.btPersistentManifold** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btPersistentManifold**
  %18 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %fillData.addr, align 4
  %19 = load %class.btPersistentManifold*, %class.btPersistentManifold** %18, align 4
  store %class.btPersistentManifold* %19, %class.btPersistentManifold** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE6resizeEiRKS1_(%class.btAlignedObjectArray.17* %this, i32 %newsize, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %fillData) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  %newsize.addr = alloca i32, align 4
  %fillData.addr = alloca %class.btTypedConstraint**, align 4
  %curSize = alloca i32, align 4
  %i = alloca i32, align 4
  %i5 = alloca i32, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  store i32 %newsize, i32* %newsize.addr, align 4
  store %class.btTypedConstraint** %fillData, %class.btTypedConstraint*** %fillData.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %this1)
  store i32 %call, i32* %curSize, align 4
  %0 = load i32, i32* %newsize.addr, align 4
  %1 = load i32, i32* %curSize, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %2 = load i32, i32* %newsize.addr, align 4
  store i32 %2, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %curSize, align 4
  %cmp2 = icmp slt i32 %3, %4
  br i1 %cmp2, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 4
  %5 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %6 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %5, i32 %6
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  br label %if.end14

if.else:                                          ; preds = %entry
  %8 = load i32, i32* %newsize.addr, align 4
  %9 = load i32, i32* %curSize, align 4
  %cmp3 = icmp sgt i32 %8, %9
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %if.else
  %10 = load i32, i32* %newsize.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.17* %this1, i32 %10)
  br label %if.end

if.end:                                           ; preds = %if.then4, %if.else
  %11 = load i32, i32* %curSize, align 4
  store i32 %11, i32* %i5, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc11, %if.end
  %12 = load i32, i32* %i5, align 4
  %13 = load i32, i32* %newsize.addr, align 4
  %cmp7 = icmp slt i32 %12, %13
  br i1 %cmp7, label %for.body8, label %for.end13

for.body8:                                        ; preds = %for.cond6
  %m_data9 = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 4
  %14 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data9, align 4
  %15 = load i32, i32* %i5, align 4
  %arrayidx10 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %14, i32 %15
  %16 = bitcast %class.btTypedConstraint** %arrayidx10 to i8*
  %17 = bitcast i8* %16 to %class.btTypedConstraint**
  %18 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %fillData.addr, align 4
  %19 = load %class.btTypedConstraint*, %class.btTypedConstraint** %18, align 4
  store %class.btTypedConstraint* %19, %class.btTypedConstraint** %17, align 4
  br label %for.inc11

for.inc11:                                        ; preds = %for.body8
  %20 = load i32, i32* %i5, align 4
  %inc12 = add nsw i32 %20, 1
  store i32 %inc12, i32* %i5, align 4
  br label %for.cond6

for.end13:                                        ; preds = %for.cond6
  br label %if.end14

if.end14:                                         ; preds = %for.end13, %for.end
  %21 = load i32, i32* %newsize.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 2
  store i32 %21, i32* %m_size, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE9push_backERKS2_(%class.btAlignedObjectArray.14* %this, %"struct.btSimulationIslandManagerMt::Island"** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %_Val.addr = alloca %"struct.btSimulationIslandManagerMt::Island"**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store %"struct.btSimulationIslandManagerMt::Island"** %_Val, %"struct.btSimulationIslandManagerMt::Island"*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE8capacityEv(%class.btAlignedObjectArray.14* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE9allocSizeEi(%class.btAlignedObjectArray.14* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE7reserveEi(%class.btAlignedObjectArray.14* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %1 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %1, i32 %2
  %3 = bitcast %"struct.btSimulationIslandManagerMt::Island"** %arrayidx to i8*
  %4 = bitcast i8* %3 to %"struct.btSimulationIslandManagerMt::Island"**
  %5 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %_Val.addr, align 4
  %6 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %5, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %6, %"struct.btSimulationIslandManagerMt::Island"** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden %"struct.btSimulationIslandManagerMt::Island"* @_ZN27btSimulationIslandManagerMt9getIslandEi(%class.btSimulationIslandManagerMt* %this, i32 %id) #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManagerMt*, align 4
  %id.addr = alloca i32, align 4
  %island = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %i = alloca i32, align 4
  store %class.btSimulationIslandManagerMt* %this, %class.btSimulationIslandManagerMt** %this.addr, align 4
  store i32 %id, i32* %id.addr, align 4
  %this1 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %m_lookupIslandFromId = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 5
  %0 = load i32, i32* %id.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %m_lookupIslandFromId, i32 %0)
  %1 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %call, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %1, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %2 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %cmp = icmp eq %"struct.btSimulationIslandManagerMt::Island"* %2, null
  br i1 %cmp, label %if.then, label %if.end13

if.then:                                          ; preds = %entry
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %3 = load i32, i32* %i, align 4
  %m_activeIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %m_activeIslands)
  %cmp3 = icmp slt i32 %3, %call2
  br i1 %cmp3, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_activeIslands4 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  %4 = load i32, i32* %i, align 4
  %call5 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %m_activeIslands4, i32 %4)
  %5 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %call5, align 4
  %id6 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %5, i32 0, i32 3
  %6 = load i32, i32* %id6, align 4
  %7 = load i32, i32* %id.addr, align 4
  %cmp7 = icmp eq i32 %6, %7
  br i1 %cmp7, label %if.then8, label %if.end

if.then8:                                         ; preds = %for.body
  %m_activeIslands9 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  %8 = load i32, i32* %i, align 4
  %call10 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %m_activeIslands9, i32 %8)
  %9 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %call10, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %9, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then8, %for.cond
  %11 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %m_lookupIslandFromId11 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 5
  %12 = load i32, i32* %id.addr, align 4
  %call12 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %m_lookupIslandFromId11, i32 %12)
  store %"struct.btSimulationIslandManagerMt::Island"* %11, %"struct.btSimulationIslandManagerMt::Island"** %call12, align 4
  br label %if.end13

if.end13:                                         ; preds = %for.end, %entry
  %13 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  ret %"struct.btSimulationIslandManagerMt::Island"* %13
}

; Function Attrs: noinline optnone
define hidden %"struct.btSimulationIslandManagerMt::Island"* @_ZN27btSimulationIslandManagerMt14allocateIslandEii(%class.btSimulationIslandManagerMt* %this, i32 %id, i32 %numBodies) unnamed_addr #2 {
entry:
  %retval = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %this.addr = alloca %class.btSimulationIslandManagerMt*, align 4
  %id.addr = alloca i32, align 4
  %numBodies.addr = alloca i32, align 4
  %island = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %allocSize = alloca i32, align 4
  %freeIslands = alloca %class.btAlignedObjectArray.14*, align 4
  %iFound = alloca i32, align 4
  %i = alloca i32, align 4
  %iDest = alloca i32, align 4
  %iSrc = alloca i32, align 4
  store %class.btSimulationIslandManagerMt* %this, %class.btSimulationIslandManagerMt** %this.addr, align 4
  store i32 %id, i32* %id.addr, align 4
  store i32 %numBodies, i32* %numBodies.addr, align 4
  %this1 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %this.addr, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* null, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %0 = load i32, i32* %numBodies.addr, align 4
  store i32 %0, i32* %allocSize, align 4
  %1 = load i32, i32* %numBodies.addr, align 4
  %m_batchIslandMinBodyCount = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 8
  %2 = load i32, i32* %m_batchIslandMinBodyCount, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %if.then, label %if.end11

if.then:                                          ; preds = %entry
  %m_batchIsland = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 6
  %3 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %m_batchIsland, align 4
  %tobool = icmp ne %"struct.btSimulationIslandManagerMt::Island"* %3, null
  br i1 %tobool, label %if.then2, label %if.else

if.then2:                                         ; preds = %if.then
  %m_batchIsland3 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 6
  %4 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %m_batchIsland3, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %4, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %5 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %m_lookupIslandFromId = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 5
  %6 = load i32, i32* %id.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %m_lookupIslandFromId, i32 %6)
  store %"struct.btSimulationIslandManagerMt::Island"* %5, %"struct.btSimulationIslandManagerMt::Island"** %call, align 4
  %7 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %bodyArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %7, i32 0, i32 0
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %bodyArray)
  %8 = load i32, i32* %numBodies.addr, align 4
  %add = add nsw i32 %call4, %8
  %m_batchIslandMinBodyCount5 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 8
  %9 = load i32, i32* %m_batchIslandMinBodyCount5, align 4
  %cmp6 = icmp sge i32 %add, %9
  br i1 %cmp6, label %if.then7, label %if.end

if.then7:                                         ; preds = %if.then2
  %m_batchIsland8 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 6
  store %"struct.btSimulationIslandManagerMt::Island"* null, %"struct.btSimulationIslandManagerMt::Island"** %m_batchIsland8, align 4
  br label %if.end

if.end:                                           ; preds = %if.then7, %if.then2
  %10 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %10, %"struct.btSimulationIslandManagerMt::Island"** %retval, align 4
  br label %return

if.else:                                          ; preds = %if.then
  %m_batchIslandMinBodyCount9 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 8
  %11 = load i32, i32* %m_batchIslandMinBodyCount9, align 4
  %mul = mul nsw i32 %11, 2
  store i32 %mul, i32* %allocSize, align 4
  br label %if.end10

if.end10:                                         ; preds = %if.else
  br label %if.end11

if.end11:                                         ; preds = %if.end10, %entry
  %m_freeIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 4
  store %class.btAlignedObjectArray.14* %m_freeIslands, %class.btAlignedObjectArray.14** %freeIslands, align 4
  %12 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %freeIslands, align 4
  %call12 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %12)
  %cmp13 = icmp sgt i32 %call12, 0
  br i1 %cmp13, label %if.then14, label %if.end35

if.then14:                                        ; preds = %if.end11
  %13 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %freeIslands, align 4
  %call15 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %13)
  store i32 %call15, i32* %iFound, align 4
  %14 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %freeIslands, align 4
  %call16 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %14)
  %sub = sub nsw i32 %call16, 1
  store i32 %sub, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then14
  %15 = load i32, i32* %i, align 4
  %cmp17 = icmp sge i32 %15, 0
  br i1 %cmp17, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %16 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %freeIslands, align 4
  %17 = load i32, i32* %i, align 4
  %call18 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %16, i32 %17)
  %18 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %call18, align 4
  %bodyArray19 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %18, i32 0, i32 0
  %call20 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray.10* %bodyArray19)
  %19 = load i32, i32* %allocSize, align 4
  %cmp21 = icmp sge i32 %call20, %19
  br i1 %cmp21, label %if.then22, label %if.end25

if.then22:                                        ; preds = %for.body
  %20 = load i32, i32* %i, align 4
  store i32 %20, i32* %iFound, align 4
  %21 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %freeIslands, align 4
  %22 = load i32, i32* %i, align 4
  %call23 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %21, i32 %22)
  %23 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %call23, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %23, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %24 = load i32, i32* %id.addr, align 4
  %25 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %id24 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %25, i32 0, i32 3
  store i32 %24, i32* %id24, align 4
  br label %for.end

if.end25:                                         ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end25
  %26 = load i32, i32* %i, align 4
  %dec = add nsw i32 %26, -1
  store i32 %dec, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then22, %for.cond
  %27 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %tobool26 = icmp ne %"struct.btSimulationIslandManagerMt::Island"* %27, null
  br i1 %tobool26, label %if.then27, label %if.end34

if.then27:                                        ; preds = %for.end
  %28 = load i32, i32* %iFound, align 4
  store i32 %28, i32* %iDest, align 4
  %29 = load i32, i32* %iDest, align 4
  %add28 = add nsw i32 %29, 1
  store i32 %add28, i32* %iSrc, align 4
  br label %while.cond

while.cond:                                       ; preds = %while.body, %if.then27
  %30 = load i32, i32* %iSrc, align 4
  %31 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %freeIslands, align 4
  %call29 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %31)
  %cmp30 = icmp slt i32 %30, %call29
  br i1 %cmp30, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %32 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %freeIslands, align 4
  %33 = load i32, i32* %iSrc, align 4
  %inc = add nsw i32 %33, 1
  store i32 %inc, i32* %iSrc, align 4
  %call31 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %32, i32 %33)
  %34 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %call31, align 4
  %35 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %freeIslands, align 4
  %36 = load i32, i32* %iDest, align 4
  %inc32 = add nsw i32 %36, 1
  store i32 %inc32, i32* %iDest, align 4
  %call33 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %35, i32 %36)
  store %"struct.btSimulationIslandManagerMt::Island"* %34, %"struct.btSimulationIslandManagerMt::Island"** %call33, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  %37 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %freeIslands, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE8pop_backEv(%class.btAlignedObjectArray.14* %37)
  br label %if.end34

if.end34:                                         ; preds = %while.end, %for.end
  br label %if.end35

if.end35:                                         ; preds = %if.end34, %if.end11
  %38 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %cmp36 = icmp eq %"struct.btSimulationIslandManagerMt::Island"* %38, null
  br i1 %cmp36, label %if.then37, label %if.end42

if.then37:                                        ; preds = %if.end35
  %call38 = call noalias nonnull i8* @_Znwm(i32 68) #10
  %39 = bitcast i8* %call38 to %"struct.btSimulationIslandManagerMt::Island"*
  %40 = bitcast %"struct.btSimulationIslandManagerMt::Island"* %39 to i8*
  call void @llvm.memset.p0i8.i32(i8* align 16 %40, i8 0, i32 68, i1 false)
  %call39 = call %"struct.btSimulationIslandManagerMt::Island"* @_ZN27btSimulationIslandManagerMt6IslandC2Ev(%"struct.btSimulationIslandManagerMt::Island"* %39)
  store %"struct.btSimulationIslandManagerMt::Island"* %39, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %41 = load i32, i32* %id.addr, align 4
  %42 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %id40 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %42, i32 0, i32 3
  store i32 %41, i32* %id40, align 4
  %43 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %bodyArray41 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %43, i32 0, i32 0
  %44 = load i32, i32* %allocSize, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray.10* %bodyArray41, i32 %44)
  %m_allocatedIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE9push_backERKS2_(%class.btAlignedObjectArray.14* %m_allocatedIslands, %"struct.btSimulationIslandManagerMt::Island"** nonnull align 4 dereferenceable(4) %island)
  br label %if.end42

if.end42:                                         ; preds = %if.then37, %if.end35
  %45 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %m_lookupIslandFromId43 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 5
  %46 = load i32, i32* %id.addr, align 4
  %call44 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %m_lookupIslandFromId43, i32 %46)
  store %"struct.btSimulationIslandManagerMt::Island"* %45, %"struct.btSimulationIslandManagerMt::Island"** %call44, align 4
  %47 = load i32, i32* %numBodies.addr, align 4
  %m_batchIslandMinBodyCount45 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 8
  %48 = load i32, i32* %m_batchIslandMinBodyCount45, align 4
  %cmp46 = icmp slt i32 %47, %48
  br i1 %cmp46, label %if.then47, label %if.end49

if.then47:                                        ; preds = %if.end42
  %49 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %m_batchIsland48 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 6
  store %"struct.btSimulationIslandManagerMt::Island"* %49, %"struct.btSimulationIslandManagerMt::Island"** %m_batchIsland48, align 4
  br label %if.end49

if.end49:                                         ; preds = %if.then47, %if.end42
  %m_activeIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE9push_backERKS2_(%class.btAlignedObjectArray.14* %m_activeIslands, %"struct.btSimulationIslandManagerMt::Island"** nonnull align 4 dereferenceable(4) %island)
  %50 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %50, %"struct.btSimulationIslandManagerMt::Island"** %retval, align 4
  br label %return

return:                                           ; preds = %if.end49, %if.end
  %51 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %retval, align 4
  ret %"struct.btSimulationIslandManagerMt::Island"* %51
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE8pop_backEv(%class.btAlignedObjectArray.14* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %1 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %1, i32 %2
  ret void
}

; Function Attrs: nobuiltin allocsize(0)
declare nonnull i8* @_Znwm(i32) #6

; Function Attrs: argmemonly nounwind willreturn writeonly
declare void @llvm.memset.p0i8.i32(i8* nocapture writeonly, i8, i32, i1 immarg) #7

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btSimulationIslandManagerMt::Island"* @_ZN27btSimulationIslandManagerMt6IslandC2Ev(%"struct.btSimulationIslandManagerMt::Island"* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %this, %"struct.btSimulationIslandManagerMt::Island"** %this.addr, align 4
  %this1 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %this.addr, align 4
  %bodyArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %this1, i32 0, i32 0
  %call = call %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev(%class.btAlignedObjectArray.10* %bodyArray)
  %manifoldArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %this1, i32 0, i32 1
  %call2 = call %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.0* %manifoldArray)
  %constraintArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %this1, i32 0, i32 2
  %call3 = call %class.btAlignedObjectArray.17* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray.17* %constraintArray)
  ret %"struct.btSimulationIslandManagerMt::Island"* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray.10* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray.10* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray.10* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btCollisionObject**
  store %class.btCollisionObject** %2, %class.btCollisionObject*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call3, %class.btCollisionObject** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray.10* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btCollisionObject**, %class.btCollisionObject*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store %class.btCollisionObject** %4, %class.btCollisionObject*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN27btSimulationIslandManagerMt12buildIslandsEP12btDispatcherP16btCollisionWorld(%class.btSimulationIslandManagerMt* %this, %class.btDispatcher* %dispatcher, %class.btCollisionWorld* %collisionWorld) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManagerMt*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %collisionObjects = alloca %class.btAlignedObjectArray.10*, align 4
  %numElem = alloca i32, align 4
  %endIslandIndex = alloca i32, align 4
  %startIslandIndex = alloca i32, align 4
  %islandId = alloca i32, align 4
  %allSleeping = alloca i8, align 1
  %idx = alloca i32, align 4
  %i = alloca i32, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %idx41 = alloca i32, align 4
  %i45 = alloca i32, align 4
  %colObj049 = alloca %class.btCollisionObject*, align 4
  %idx65 = alloca i32, align 4
  %i69 = alloca i32, align 4
  %colObj073 = alloca %class.btCollisionObject*, align 4
  store %class.btSimulationIslandManagerMt* %this, %class.btSimulationIslandManagerMt** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %this1 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %call = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([28 x i8], [28 x i8]* @.str, i32 0, i32 0))
  %0 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call2 = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %0)
  store %class.btAlignedObjectArray.10* %call2, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %1 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call3 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %1)
  call void @_ZN11btUnionFind11sortIslandsEv(%class.btUnionFind* %call3)
  %2 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call4 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %2)
  %call5 = call i32 @_ZNK11btUnionFind14getNumElementsEv(%class.btUnionFind* %call4)
  store i32 %call5, i32* %numElem, align 4
  store i32 1, i32* %endIslandIndex, align 4
  store i32 0, i32* %startIslandIndex, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc94, %entry
  %3 = load i32, i32* %startIslandIndex, align 4
  %4 = load i32, i32* %numElem, align 4
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end95

for.body:                                         ; preds = %for.cond
  %5 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call6 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %5)
  %6 = load i32, i32* %startIslandIndex, align 4
  %call7 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call6, i32 %6)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call7, i32 0, i32 0
  %7 = load i32, i32* %m_id, align 4
  store i32 %7, i32* %islandId, align 4
  %8 = load i32, i32* %startIslandIndex, align 4
  %add = add nsw i32 %8, 1
  store i32 %add, i32* %endIslandIndex, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %9 = load i32, i32* %endIslandIndex, align 4
  %10 = load i32, i32* %numElem, align 4
  %cmp9 = icmp slt i32 %9, %10
  br i1 %cmp9, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond8
  %11 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call10 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %11)
  %12 = load i32, i32* %endIslandIndex, align 4
  %call11 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call10, i32 %12)
  %m_id12 = getelementptr inbounds %struct.btElement, %struct.btElement* %call11, i32 0, i32 0
  %13 = load i32, i32* %m_id12, align 4
  %14 = load i32, i32* %islandId, align 4
  %cmp13 = icmp eq i32 %13, %14
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond8
  %15 = phi i1 [ false, %for.cond8 ], [ %cmp13, %land.rhs ]
  br i1 %15, label %for.body14, label %for.end

for.body14:                                       ; preds = %land.end
  br label %for.inc

for.inc:                                          ; preds = %for.body14
  %16 = load i32, i32* %endIslandIndex, align 4
  %inc = add nsw i32 %16, 1
  store i32 %inc, i32* %endIslandIndex, align 4
  br label %for.cond8

for.end:                                          ; preds = %land.end
  store i8 1, i8* %allSleeping, align 1
  %17 = load i32, i32* %startIslandIndex, align 4
  store i32 %17, i32* %idx, align 4
  br label %for.cond15

for.cond15:                                       ; preds = %for.inc37, %for.end
  %18 = load i32, i32* %idx, align 4
  %19 = load i32, i32* %endIslandIndex, align 4
  %cmp16 = icmp slt i32 %18, %19
  br i1 %cmp16, label %for.body17, label %for.end39

for.body17:                                       ; preds = %for.cond15
  %20 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call18 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %20)
  %21 = load i32, i32* %idx, align 4
  %call19 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call18, i32 %21)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call19, i32 0, i32 1
  %22 = load i32, i32* %m_sz, align 4
  store i32 %22, i32* %i, align 4
  %23 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %24 = load i32, i32* %i, align 4
  %call20 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %23, i32 %24)
  %25 = load %class.btCollisionObject*, %class.btCollisionObject** %call20, align 4
  store %class.btCollisionObject* %25, %class.btCollisionObject** %colObj0, align 4
  %26 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call21 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %26)
  %27 = load i32, i32* %islandId, align 4
  %cmp22 = icmp ne i32 %call21, %27
  br i1 %cmp22, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %for.body17
  %28 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call23 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %28)
  %cmp24 = icmp ne i32 %call23, -1
  br i1 %cmp24, label %if.then, label %if.end

if.then:                                          ; preds = %land.lhs.true
  br label %if.end

if.end:                                           ; preds = %if.then, %land.lhs.true, %for.body17
  %29 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call25 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %29)
  %30 = load i32, i32* %islandId, align 4
  %cmp26 = icmp eq i32 %call25, %30
  br i1 %cmp26, label %if.then27, label %if.end36

if.then27:                                        ; preds = %if.end
  %31 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call28 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %31)
  %cmp29 = icmp eq i32 %call28, 1
  br i1 %cmp29, label %if.then30, label %if.end31

if.then30:                                        ; preds = %if.then27
  store i8 0, i8* %allSleeping, align 1
  br label %if.end31

if.end31:                                         ; preds = %if.then30, %if.then27
  %32 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call32 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %32)
  %cmp33 = icmp eq i32 %call32, 4
  br i1 %cmp33, label %if.then34, label %if.end35

if.then34:                                        ; preds = %if.end31
  store i8 0, i8* %allSleeping, align 1
  br label %if.end35

if.end35:                                         ; preds = %if.then34, %if.end31
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %if.end
  br label %for.inc37

for.inc37:                                        ; preds = %if.end36
  %33 = load i32, i32* %idx, align 4
  %inc38 = add nsw i32 %33, 1
  store i32 %inc38, i32* %idx, align 4
  br label %for.cond15

for.end39:                                        ; preds = %for.cond15
  %34 = load i8, i8* %allSleeping, align 1
  %tobool = trunc i8 %34 to i1
  br i1 %tobool, label %if.then40, label %if.else

if.then40:                                        ; preds = %for.end39
  %35 = load i32, i32* %startIslandIndex, align 4
  store i32 %35, i32* %idx41, align 4
  br label %for.cond42

for.cond42:                                       ; preds = %for.inc62, %if.then40
  %36 = load i32, i32* %idx41, align 4
  %37 = load i32, i32* %endIslandIndex, align 4
  %cmp43 = icmp slt i32 %36, %37
  br i1 %cmp43, label %for.body44, label %for.end64

for.body44:                                       ; preds = %for.cond42
  %38 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call46 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %38)
  %39 = load i32, i32* %idx41, align 4
  %call47 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call46, i32 %39)
  %m_sz48 = getelementptr inbounds %struct.btElement, %struct.btElement* %call47, i32 0, i32 1
  %40 = load i32, i32* %m_sz48, align 4
  store i32 %40, i32* %i45, align 4
  %41 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %42 = load i32, i32* %i45, align 4
  %call50 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %41, i32 %42)
  %43 = load %class.btCollisionObject*, %class.btCollisionObject** %call50, align 4
  store %class.btCollisionObject* %43, %class.btCollisionObject** %colObj049, align 4
  %44 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj049, align 4
  %call51 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %44)
  %45 = load i32, i32* %islandId, align 4
  %cmp52 = icmp ne i32 %call51, %45
  br i1 %cmp52, label %land.lhs.true53, label %if.end57

land.lhs.true53:                                  ; preds = %for.body44
  %46 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj049, align 4
  %call54 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %46)
  %cmp55 = icmp ne i32 %call54, -1
  br i1 %cmp55, label %if.then56, label %if.end57

if.then56:                                        ; preds = %land.lhs.true53
  br label %if.end57

if.end57:                                         ; preds = %if.then56, %land.lhs.true53, %for.body44
  %47 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj049, align 4
  %call58 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %47)
  %48 = load i32, i32* %islandId, align 4
  %cmp59 = icmp eq i32 %call58, %48
  br i1 %cmp59, label %if.then60, label %if.end61

if.then60:                                        ; preds = %if.end57
  %49 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj049, align 4
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %49, i32 2)
  br label %if.end61

if.end61:                                         ; preds = %if.then60, %if.end57
  br label %for.inc62

for.inc62:                                        ; preds = %if.end61
  %50 = load i32, i32* %idx41, align 4
  %inc63 = add nsw i32 %50, 1
  store i32 %inc63, i32* %idx41, align 4
  br label %for.cond42

for.end64:                                        ; preds = %for.cond42
  br label %if.end93

if.else:                                          ; preds = %for.end39
  %51 = load i32, i32* %startIslandIndex, align 4
  store i32 %51, i32* %idx65, align 4
  br label %for.cond66

for.cond66:                                       ; preds = %for.inc90, %if.else
  %52 = load i32, i32* %idx65, align 4
  %53 = load i32, i32* %endIslandIndex, align 4
  %cmp67 = icmp slt i32 %52, %53
  br i1 %cmp67, label %for.body68, label %for.end92

for.body68:                                       ; preds = %for.cond66
  %54 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call70 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %54)
  %55 = load i32, i32* %idx65, align 4
  %call71 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call70, i32 %55)
  %m_sz72 = getelementptr inbounds %struct.btElement, %struct.btElement* %call71, i32 0, i32 1
  %56 = load i32, i32* %m_sz72, align 4
  store i32 %56, i32* %i69, align 4
  %57 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %58 = load i32, i32* %i69, align 4
  %call74 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %57, i32 %58)
  %59 = load %class.btCollisionObject*, %class.btCollisionObject** %call74, align 4
  store %class.btCollisionObject* %59, %class.btCollisionObject** %colObj073, align 4
  %60 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4
  %call75 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %60)
  %61 = load i32, i32* %islandId, align 4
  %cmp76 = icmp ne i32 %call75, %61
  br i1 %cmp76, label %land.lhs.true77, label %if.end81

land.lhs.true77:                                  ; preds = %for.body68
  %62 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4
  %call78 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %62)
  %cmp79 = icmp ne i32 %call78, -1
  br i1 %cmp79, label %if.then80, label %if.end81

if.then80:                                        ; preds = %land.lhs.true77
  br label %if.end81

if.end81:                                         ; preds = %if.then80, %land.lhs.true77, %for.body68
  %63 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4
  %call82 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %63)
  %64 = load i32, i32* %islandId, align 4
  %cmp83 = icmp eq i32 %call82, %64
  br i1 %cmp83, label %if.then84, label %if.end89

if.then84:                                        ; preds = %if.end81
  %65 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4
  %call85 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %65)
  %cmp86 = icmp eq i32 %call85, 2
  br i1 %cmp86, label %if.then87, label %if.end88

if.then87:                                        ; preds = %if.then84
  %66 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4
  call void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject* %66, i32 3)
  %67 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj073, align 4
  call void @_ZN17btCollisionObject19setDeactivationTimeEf(%class.btCollisionObject* %67, float 0.000000e+00)
  br label %if.end88

if.end88:                                         ; preds = %if.then87, %if.then84
  br label %if.end89

if.end89:                                         ; preds = %if.end88, %if.end81
  br label %for.inc90

for.inc90:                                        ; preds = %if.end89
  %68 = load i32, i32* %idx65, align 4
  %inc91 = add nsw i32 %68, 1
  store i32 %inc91, i32* %idx65, align 4
  br label %for.cond66

for.end92:                                        ; preds = %for.cond66
  br label %if.end93

if.end93:                                         ; preds = %for.end92, %for.end64
  br label %for.inc94

for.inc94:                                        ; preds = %if.end93
  %69 = load i32, i32* %endIslandIndex, align 4
  store i32 %69, i32* %startIslandIndex, align 4
  br label %for.cond

for.end95:                                        ; preds = %for.cond
  %call96 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #8
  ret void
}

declare %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* returned, i8*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionWorld*, align 4
  store %class.btCollisionWorld* %this, %class.btCollisionWorld** %this.addr, align 4
  %this1 = load %class.btCollisionWorld*, %class.btCollisionWorld** %this.addr, align 4
  %m_collisionObjects = getelementptr inbounds %class.btCollisionWorld, %class.btCollisionWorld* %this1, i32 0, i32 1
  ret %class.btAlignedObjectArray.10* %m_collisionObjects
}

declare void @_ZN11btUnionFind11sortIslandsEv(%class.btUnionFind*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %this, i32 %index) #2 comdat {
entry:
  %this.addr = alloca %class.btUnionFind*, align 4
  %index.addr = alloca i32, align 4
  store %class.btUnionFind* %this, %class.btUnionFind** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btUnionFind*, %class.btUnionFind** %this.addr, align 4
  %m_elements = getelementptr inbounds %class.btUnionFind, %class.btUnionFind* %this1, i32 0, i32 0
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %m_elements, i32 %0)
  ret %struct.btElement* %call
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %0, i32 %1
  ret %class.btCollisionObject** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_islandTag1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 13
  %0 = load i32, i32* %m_islandTag1, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  %0 = load i32, i32* %m_activationState1, align 4
  ret i32 %0
}

declare void @_ZNK17btCollisionObject18setActivationStateEi(%class.btCollisionObject*, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btCollisionObject19setDeactivationTimeEf(%class.btCollisionObject* %this, float %time) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  %time.addr = alloca float, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store float %time, float* %time.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %0 = load float, float* %time.addr, align 4
  %m_deactivationTime = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 17
  store float %0, float* %m_deactivationTime, align 4
  ret void
}

; Function Attrs: nounwind
declare %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* returned) unnamed_addr #5

; Function Attrs: noinline optnone
define hidden void @_ZN27btSimulationIslandManagerMt18addBodiesToIslandsEP16btCollisionWorld(%class.btSimulationIslandManagerMt* %this, %class.btCollisionWorld* %collisionWorld) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManagerMt*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %collisionObjects = alloca %class.btAlignedObjectArray.10*, align 4
  %endIslandIndex = alloca i32, align 4
  %startIslandIndex = alloca i32, align 4
  %numElem = alloca i32, align 4
  %islandId = alloca i32, align 4
  %islandSleeping = alloca i8, align 1
  %iElem = alloca i32, align 4
  %i = alloca i32, align 4
  %colObj = alloca %class.btCollisionObject*, align 4
  %numBodies = alloca i32, align 4
  %island = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %iElem25 = alloca i32, align 4
  %i29 = alloca i32, align 4
  %colObj33 = alloca %class.btCollisionObject*, align 4
  store %class.btSimulationIslandManagerMt* %this, %class.btSimulationIslandManagerMt** %this.addr, align 4
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %this1 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %0 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %0)
  store %class.btAlignedObjectArray.10* %call, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  store i32 1, i32* %endIslandIndex, align 4
  %1 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call2 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %1)
  %call3 = call i32 @_ZNK11btUnionFind14getNumElementsEv(%class.btUnionFind* %call2)
  store i32 %call3, i32* %numElem, align 4
  store i32 0, i32* %startIslandIndex, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc39, %entry
  %2 = load i32, i32* %startIslandIndex, align 4
  %3 = load i32, i32* %numElem, align 4
  %cmp = icmp slt i32 %2, %3
  br i1 %cmp, label %for.body, label %for.end40

for.body:                                         ; preds = %for.cond
  %4 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call4 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %4)
  %5 = load i32, i32* %startIslandIndex, align 4
  %call5 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call4, i32 %5)
  %m_id = getelementptr inbounds %struct.btElement, %struct.btElement* %call5, i32 0, i32 0
  %6 = load i32, i32* %m_id, align 4
  store i32 %6, i32* %islandId, align 4
  %7 = load i32, i32* %startIslandIndex, align 4
  store i32 %7, i32* %endIslandIndex, align 4
  br label %for.cond6

for.cond6:                                        ; preds = %for.inc, %for.body
  %8 = load i32, i32* %endIslandIndex, align 4
  %9 = load i32, i32* %numElem, align 4
  %cmp7 = icmp slt i32 %8, %9
  br i1 %cmp7, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %for.cond6
  %10 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call8 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %10)
  %11 = load i32, i32* %endIslandIndex, align 4
  %call9 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call8, i32 %11)
  %m_id10 = getelementptr inbounds %struct.btElement, %struct.btElement* %call9, i32 0, i32 0
  %12 = load i32, i32* %m_id10, align 4
  %13 = load i32, i32* %islandId, align 4
  %cmp11 = icmp eq i32 %12, %13
  br label %land.end

land.end:                                         ; preds = %land.rhs, %for.cond6
  %14 = phi i1 [ false, %for.cond6 ], [ %cmp11, %land.rhs ]
  br i1 %14, label %for.body12, label %for.end

for.body12:                                       ; preds = %land.end
  br label %for.inc

for.inc:                                          ; preds = %for.body12
  %15 = load i32, i32* %endIslandIndex, align 4
  %inc = add nsw i32 %15, 1
  store i32 %inc, i32* %endIslandIndex, align 4
  br label %for.cond6

for.end:                                          ; preds = %land.end
  store i8 1, i8* %islandSleeping, align 1
  %16 = load i32, i32* %startIslandIndex, align 4
  store i32 %16, i32* %iElem, align 4
  br label %for.cond13

for.cond13:                                       ; preds = %for.inc20, %for.end
  %17 = load i32, i32* %iElem, align 4
  %18 = load i32, i32* %endIslandIndex, align 4
  %cmp14 = icmp slt i32 %17, %18
  br i1 %cmp14, label %for.body15, label %for.end22

for.body15:                                       ; preds = %for.cond13
  %19 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call16 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %19)
  %20 = load i32, i32* %iElem, align 4
  %call17 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call16, i32 %20)
  %m_sz = getelementptr inbounds %struct.btElement, %struct.btElement* %call17, i32 0, i32 1
  %21 = load i32, i32* %m_sz, align 4
  store i32 %21, i32* %i, align 4
  %22 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %23 = load i32, i32* %i, align 4
  %call18 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %22, i32 %23)
  %24 = load %class.btCollisionObject*, %class.btCollisionObject** %call18, align 4
  store %class.btCollisionObject* %24, %class.btCollisionObject** %colObj, align 4
  %25 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj, align 4
  %call19 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %25)
  br i1 %call19, label %if.then, label %if.end

if.then:                                          ; preds = %for.body15
  store i8 0, i8* %islandSleeping, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %for.body15
  br label %for.inc20

for.inc20:                                        ; preds = %if.end
  %26 = load i32, i32* %iElem, align 4
  %inc21 = add nsw i32 %26, 1
  store i32 %inc21, i32* %iElem, align 4
  br label %for.cond13

for.end22:                                        ; preds = %for.cond13
  %27 = load i8, i8* %islandSleeping, align 1
  %tobool = trunc i8 %27 to i1
  br i1 %tobool, label %if.end38, label %if.then23

if.then23:                                        ; preds = %for.end22
  %28 = load i32, i32* %endIslandIndex, align 4
  %29 = load i32, i32* %startIslandIndex, align 4
  %sub = sub nsw i32 %28, %29
  store i32 %sub, i32* %numBodies, align 4
  %30 = load i32, i32* %islandId, align 4
  %31 = load i32, i32* %numBodies, align 4
  %32 = bitcast %class.btSimulationIslandManagerMt* %this1 to %"struct.btSimulationIslandManagerMt::Island"* (%class.btSimulationIslandManagerMt*, i32, i32)***
  %vtable = load %"struct.btSimulationIslandManagerMt::Island"* (%class.btSimulationIslandManagerMt*, i32, i32)**, %"struct.btSimulationIslandManagerMt::Island"* (%class.btSimulationIslandManagerMt*, i32, i32)*** %32, align 4
  %vfn = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"* (%class.btSimulationIslandManagerMt*, i32, i32)*, %"struct.btSimulationIslandManagerMt::Island"* (%class.btSimulationIslandManagerMt*, i32, i32)** %vtable, i64 4
  %33 = load %"struct.btSimulationIslandManagerMt::Island"* (%class.btSimulationIslandManagerMt*, i32, i32)*, %"struct.btSimulationIslandManagerMt::Island"* (%class.btSimulationIslandManagerMt*, i32, i32)** %vfn, align 4
  %call24 = call %"struct.btSimulationIslandManagerMt::Island"* %33(%class.btSimulationIslandManagerMt* %this1, i32 %30, i32 %31)
  store %"struct.btSimulationIslandManagerMt::Island"* %call24, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %34 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %isSleeping = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %34, i32 0, i32 4
  store i8 0, i8* %isSleeping, align 4
  %35 = load i32, i32* %startIslandIndex, align 4
  store i32 %35, i32* %iElem25, align 4
  br label %for.cond26

for.cond26:                                       ; preds = %for.inc35, %if.then23
  %36 = load i32, i32* %iElem25, align 4
  %37 = load i32, i32* %endIslandIndex, align 4
  %cmp27 = icmp slt i32 %36, %37
  br i1 %cmp27, label %for.body28, label %for.end37

for.body28:                                       ; preds = %for.cond26
  %38 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call30 = call nonnull align 4 dereferenceable(20) %class.btUnionFind* @_ZN25btSimulationIslandManager12getUnionFindEv(%class.btSimulationIslandManager* %38)
  %39 = load i32, i32* %iElem25, align 4
  %call31 = call nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN11btUnionFind10getElementEi(%class.btUnionFind* %call30, i32 %39)
  %m_sz32 = getelementptr inbounds %struct.btElement, %struct.btElement* %call31, i32 0, i32 1
  %40 = load i32, i32* %m_sz32, align 4
  store i32 %40, i32* %i29, align 4
  %41 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %42 = load i32, i32* %i29, align 4
  %call34 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %41, i32 %42)
  %43 = load %class.btCollisionObject*, %class.btCollisionObject** %call34, align 4
  store %class.btCollisionObject* %43, %class.btCollisionObject** %colObj33, align 4
  %44 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %bodyArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %44, i32 0, i32 0
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_(%class.btAlignedObjectArray.10* %bodyArray, %class.btCollisionObject** nonnull align 4 dereferenceable(4) %colObj33)
  br label %for.inc35

for.inc35:                                        ; preds = %for.body28
  %45 = load i32, i32* %iElem25, align 4
  %inc36 = add nsw i32 %45, 1
  store i32 %inc36, i32* %iElem25, align 4
  br label %for.cond26

for.end37:                                        ; preds = %for.cond26
  br label %if.end38

if.end38:                                         ; preds = %for.end37, %for.end22
  br label %for.inc39

for.inc39:                                        ; preds = %if.end38
  %46 = load i32, i32* %endIslandIndex, align 4
  store i32 %46, i32* %startIslandIndex, align 4
  br label %for.cond

for.end40:                                        ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp = icmp ne i32 %call, 2
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %call2 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp3 = icmp ne i32 %call2, 5
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %0 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %0
}

; Function Attrs: noinline optnone
define hidden void @_ZN27btSimulationIslandManagerMt21addManifoldsToIslandsEP12btDispatcher(%class.btSimulationIslandManagerMt* %this, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManagerMt*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %maxNumManifolds = alloca i32, align 4
  %i = alloca i32, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %colObj1 = alloca %class.btCollisionObject*, align 4
  %islandId = alloca i32, align 4
  %island = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  store %class.btSimulationIslandManagerMt* %this, %class.btSimulationIslandManagerMt** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %0 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %1 = bitcast %class.btDispatcher* %0 to i32 (%class.btDispatcher*)***
  %vtable = load i32 (%class.btDispatcher*)**, i32 (%class.btDispatcher*)*** %1, align 4
  %vfn = getelementptr inbounds i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vtable, i64 9
  %2 = load i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vfn, align 4
  %call = call i32 %2(%class.btDispatcher* %0)
  store i32 %call, i32* %maxNumManifolds, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %3 = load i32, i32* %i, align 4
  %4 = load i32, i32* %maxNumManifolds, align 4
  %cmp = icmp slt i32 %3, %4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %5 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %6 = load i32, i32* %i, align 4
  %7 = bitcast %class.btDispatcher* %5 to %class.btPersistentManifold* (%class.btDispatcher*, i32)***
  %vtable2 = load %class.btPersistentManifold* (%class.btDispatcher*, i32)**, %class.btPersistentManifold* (%class.btDispatcher*, i32)*** %7, align 4
  %vfn3 = getelementptr inbounds %class.btPersistentManifold* (%class.btDispatcher*, i32)*, %class.btPersistentManifold* (%class.btDispatcher*, i32)** %vtable2, i64 10
  %8 = load %class.btPersistentManifold* (%class.btDispatcher*, i32)*, %class.btPersistentManifold* (%class.btDispatcher*, i32)** %vfn3, align 4
  %call4 = call %class.btPersistentManifold* %8(%class.btDispatcher* %5, i32 %6)
  store %class.btPersistentManifold* %call4, %class.btPersistentManifold** %manifold, align 4
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call5 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %9)
  store %class.btCollisionObject* %call5, %class.btCollisionObject** %colObj0, align 4
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call6 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %10)
  store %class.btCollisionObject* %call6, %class.btCollisionObject** %colObj1, align 4
  %11 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %tobool = icmp ne %class.btCollisionObject* %11, null
  br i1 %tobool, label %land.lhs.true, label %lor.lhs.false

land.lhs.true:                                    ; preds = %for.body
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call7 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %12)
  %cmp8 = icmp ne i32 %call7, 2
  br i1 %cmp8, label %if.then, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true, %for.body
  %13 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %tobool9 = icmp ne %class.btCollisionObject* %13, null
  br i1 %tobool9, label %land.lhs.true10, label %if.end40

land.lhs.true10:                                  ; preds = %lor.lhs.false
  %14 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call11 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %14)
  %cmp12 = icmp ne i32 %call11, 2
  br i1 %cmp12, label %if.then, label %if.end40

if.then:                                          ; preds = %land.lhs.true10, %land.lhs.true
  %15 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call13 = call zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %15)
  br i1 %call13, label %land.lhs.true14, label %if.end20

land.lhs.true14:                                  ; preds = %if.then
  %16 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call15 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %16)
  %cmp16 = icmp ne i32 %call15, 2
  br i1 %cmp16, label %if.then17, label %if.end20

if.then17:                                        ; preds = %land.lhs.true14
  %17 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call18 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %17)
  br i1 %call18, label %if.then19, label %if.end

if.then19:                                        ; preds = %if.then17
  %18 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  call void @_ZNK17btCollisionObject8activateEb(%class.btCollisionObject* %18, i1 zeroext false)
  br label %if.end

if.end:                                           ; preds = %if.then19, %if.then17
  br label %if.end20

if.end20:                                         ; preds = %if.end, %land.lhs.true14, %if.then
  %19 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call21 = call zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %19)
  br i1 %call21, label %land.lhs.true22, label %if.end29

land.lhs.true22:                                  ; preds = %if.end20
  %20 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call23 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %20)
  %cmp24 = icmp ne i32 %call23, 2
  br i1 %cmp24, label %if.then25, label %if.end29

if.then25:                                        ; preds = %land.lhs.true22
  %21 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call26 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %21)
  br i1 %call26, label %if.then27, label %if.end28

if.then27:                                        ; preds = %if.then25
  %22 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  call void @_ZNK17btCollisionObject8activateEb(%class.btCollisionObject* %22, i1 zeroext false)
  br label %if.end28

if.end28:                                         ; preds = %if.then27, %if.then25
  br label %if.end29

if.end29:                                         ; preds = %if.end28, %land.lhs.true22, %if.end20
  %23 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %24 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %25 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %26 = bitcast %class.btDispatcher* %23 to i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable30 = load i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*** %26, align 4
  %vfn31 = getelementptr inbounds i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable30, i64 7
  %27 = load i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn31, align 4
  %call32 = call zeroext i1 %27(%class.btDispatcher* %23, %class.btCollisionObject* %24, %class.btCollisionObject* %25)
  br i1 %call32, label %if.then33, label %if.end39

if.then33:                                        ; preds = %if.end29
  %28 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call34 = call i32 @_Z11getIslandIdPK20btPersistentManifold(%class.btPersistentManifold* %28)
  store i32 %call34, i32* %islandId, align 4
  %29 = load i32, i32* %islandId, align 4
  %call35 = call %"struct.btSimulationIslandManagerMt::Island"* @_ZN27btSimulationIslandManagerMt9getIslandEi(%class.btSimulationIslandManagerMt* %this1, i32 %29)
  store %"struct.btSimulationIslandManagerMt::Island"* %call35, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %30 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %tobool36 = icmp ne %"struct.btSimulationIslandManagerMt::Island"* %30, null
  br i1 %tobool36, label %if.then37, label %if.end38

if.then37:                                        ; preds = %if.then33
  %31 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %manifoldArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %31, i32 0, i32 1
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray.0* %manifoldArray, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %manifold)
  br label %if.end38

if.end38:                                         ; preds = %if.then37, %if.then33
  br label %if.end39

if.end39:                                         ; preds = %if.end38, %if.end29
  br label %if.end40

if.end40:                                         ; preds = %if.end39, %land.lhs.true10, %lor.lhs.false
  br label %for.inc

for.inc:                                          ; preds = %if.end40
  %32 = load i32, i32* %i, align 4
  %inc = add nsw i32 %32, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body0, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  %m_body1 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 3
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %m_body1, align 4
  ret %class.btCollisionObject* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 2
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 4
  %cmp = icmp eq i32 %and, 0
  ret i1 %cmp
}

declare void @_ZNK17btCollisionObject8activateEb(%class.btCollisionObject*, i1 zeroext) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_Z11getIslandIdPK20btPersistentManifold(%class.btPersistentManifold* %lhs) #1 comdat {
entry:
  %lhs.addr = alloca %class.btPersistentManifold*, align 4
  %rcolObj0 = alloca %class.btCollisionObject*, align 4
  %rcolObj1 = alloca %class.btCollisionObject*, align 4
  %islandId = alloca i32, align 4
  store %class.btPersistentManifold* %lhs, %class.btPersistentManifold** %lhs.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %lhs.addr, align 4
  %call = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %0)
  store %class.btCollisionObject* %call, %class.btCollisionObject** %rcolObj0, align 4
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %lhs.addr, align 4
  %call1 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %1)
  store %class.btCollisionObject* %call1, %class.btCollisionObject** %rcolObj1, align 4
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj0, align 4
  %call2 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %2)
  %cmp = icmp sge i32 %call2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %3 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj0, align 4
  %call3 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %3)
  br label %cond.end

cond.false:                                       ; preds = %entry
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj1, align 4
  %call4 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %4)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call3, %cond.true ], [ %call4, %cond.false ]
  store i32 %cond, i32* %islandId, align 4
  %5 = load i32, i32* %islandId, align 4
  ret i32 %5
}

; Function Attrs: noinline optnone
define hidden void @_ZN27btSimulationIslandManagerMt23addConstraintsToIslandsER20btAlignedObjectArrayIP17btTypedConstraintE(%class.btSimulationIslandManagerMt* %this, %class.btAlignedObjectArray.17* nonnull align 4 dereferenceable(17) %constraints) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManagerMt*, align 4
  %constraints.addr = alloca %class.btAlignedObjectArray.17*, align 4
  %i = alloca i32, align 4
  %constraint = alloca %class.btTypedConstraint*, align 4
  %islandId = alloca i32, align 4
  %island = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  store %class.btSimulationIslandManagerMt* %this, %class.btSimulationIslandManagerMt** %this.addr, align 4
  store %class.btAlignedObjectArray.17* %constraints, %class.btAlignedObjectArray.17** %constraints.addr, align 4
  %this1 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %constraints.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %1)
  %cmp = icmp slt i32 %0, %call
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %2 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %constraints.addr, align 4
  %3 = load i32, i32* %i, align 4
  %call2 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.17* %2, i32 %3)
  %4 = load %class.btTypedConstraint*, %class.btTypedConstraint** %call2, align 4
  store %class.btTypedConstraint* %4, %class.btTypedConstraint** %constraint, align 4
  %5 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4
  %call3 = call zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %5)
  br i1 %call3, label %if.then, label %if.end7

if.then:                                          ; preds = %for.body
  %6 = load %class.btTypedConstraint*, %class.btTypedConstraint** %constraint, align 4
  %call4 = call i32 @_Z23btGetConstraintIslandIdPK17btTypedConstraint(%class.btTypedConstraint* %6)
  store i32 %call4, i32* %islandId, align 4
  %7 = load i32, i32* %islandId, align 4
  %call5 = call %"struct.btSimulationIslandManagerMt::Island"* @_ZN27btSimulationIslandManagerMt9getIslandEi(%class.btSimulationIslandManagerMt* %this1, i32 %7)
  store %"struct.btSimulationIslandManagerMt::Island"* %call5, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %8 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %tobool = icmp ne %"struct.btSimulationIslandManagerMt::Island"* %8, null
  br i1 %tobool, label %if.then6, label %if.end

if.then6:                                         ; preds = %if.then
  %9 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %constraintArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %9, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_(%class.btAlignedObjectArray.17* %constraintArray, %class.btTypedConstraint** nonnull align 4 dereferenceable(4) %constraint)
  br label %if.end

if.end:                                           ; preds = %if.then6, %if.then
  br label %if.end7

if.end7:                                          ; preds = %if.end, %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end7
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.17* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %0, i32 %1
  ret %class.btTypedConstraint** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btTypedConstraint9isEnabledEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_isEnabled = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 5
  %0 = load i8, i8* %m_isEnabled, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z23btGetConstraintIslandIdPK17btTypedConstraint(%class.btTypedConstraint* %lhs) #2 comdat {
entry:
  %lhs.addr = alloca %class.btTypedConstraint*, align 4
  %rcolObj0 = alloca %class.btCollisionObject*, align 4
  %rcolObj1 = alloca %class.btCollisionObject*, align 4
  %islandId = alloca i32, align 4
  store %class.btTypedConstraint* %lhs, %class.btTypedConstraint** %lhs.addr, align 4
  %0 = load %class.btTypedConstraint*, %class.btTypedConstraint** %lhs.addr, align 4
  %call = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %0)
  %1 = bitcast %class.btRigidBody* %call to %class.btCollisionObject*
  store %class.btCollisionObject* %1, %class.btCollisionObject** %rcolObj0, align 4
  %2 = load %class.btTypedConstraint*, %class.btTypedConstraint** %lhs.addr, align 4
  %call1 = call nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %2)
  %3 = bitcast %class.btRigidBody* %call1 to %class.btCollisionObject*
  store %class.btCollisionObject* %3, %class.btCollisionObject** %rcolObj1, align 4
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj0, align 4
  %call2 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %4)
  %cmp = icmp sge i32 %call2, 0
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj0, align 4
  %call3 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %5)
  br label %cond.end

cond.false:                                       ; preds = %entry
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %rcolObj1, align 4
  %call4 = call i32 @_ZNK17btCollisionObject12getIslandTagEv(%class.btCollisionObject* %6)
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %call3, %cond.true ], [ %call4, %cond.false ]
  store i32 %cond, i32* %islandId, align 4
  %7 = load i32, i32* %islandId, align 4
  ret i32 %7
}

; Function Attrs: noinline optnone
define hidden void @_ZN27btSimulationIslandManagerMt12mergeIslandsEv(%class.btSimulationIslandManagerMt* %this) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManagerMt*, align 4
  %ref.tmp = alloca %class.IslandBatchSizeSortPredicate, align 1
  %destIslandIndex = alloca i32, align 4
  %i = alloca i32, align 4
  %island = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %batchSize = alloca i32, align 4
  %lastIndex = alloca i32, align 4
  %island12 = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %numBodies = alloca i32, align 4
  %numManifolds = alloca i32, align 4
  %numConstraints = alloca i32, align 4
  %firstIndex = alloca i32, align 4
  %src = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %batchCost = alloca i32, align 4
  %i42 = alloca i32, align 4
  %ref.tmp52 = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  store %class.btSimulationIslandManagerMt* %this, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %this1 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %m_activeIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE9quickSortI28IslandBatchSizeSortPredicateEEvRKT_(%class.btAlignedObjectArray.14* %m_activeIslands, %class.IslandBatchSizeSortPredicate* nonnull align 1 dereferenceable(1) %ref.tmp)
  %m_activeIslands2 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %m_activeIslands2)
  store i32 %call, i32* %destIslandIndex, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_activeIslands3 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %m_activeIslands3)
  %cmp = icmp slt i32 %0, %call4
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_activeIslands5 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  %1 = load i32, i32* %i, align 4
  %call6 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %m_activeIslands5, i32 %1)
  %2 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %call6, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %2, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %3 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island, align 4
  %call7 = call i32 @_Z13calcBatchCostPKN27btSimulationIslandManagerMt6IslandE(%"struct.btSimulationIslandManagerMt::Island"* %3)
  store i32 %call7, i32* %batchSize, align 4
  %4 = load i32, i32* %batchSize, align 4
  %m_minimumSolverBatchSize = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 7
  %5 = load i32, i32* %m_minimumSolverBatchSize, align 4
  %cmp8 = icmp slt i32 %4, %5
  br i1 %cmp8, label %if.then, label %if.end

if.then:                                          ; preds = %for.body
  %6 = load i32, i32* %i, align 4
  store i32 %6, i32* %destIslandIndex, align 4
  br label %for.end

if.end:                                           ; preds = %for.body
  br label %for.inc

for.inc:                                          ; preds = %if.end
  %7 = load i32, i32* %i, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %if.then, %for.cond
  %m_activeIslands9 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  %call10 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %m_activeIslands9)
  %sub = sub nsw i32 %call10, 1
  store i32 %sub, i32* %lastIndex, align 4
  br label %while.cond

while.cond:                                       ; preds = %for.end50, %for.end
  %8 = load i32, i32* %destIslandIndex, align 4
  %9 = load i32, i32* %lastIndex, align 4
  %cmp11 = icmp slt i32 %8, %9
  br i1 %cmp11, label %while.body, label %while.end55

while.body:                                       ; preds = %while.cond
  %m_activeIslands13 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  %10 = load i32, i32* %destIslandIndex, align 4
  %call14 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %m_activeIslands13, i32 %10)
  %11 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %call14, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %11, %"struct.btSimulationIslandManagerMt::Island"** %island12, align 4
  %12 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island12, align 4
  %bodyArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %12, i32 0, i32 0
  %call15 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %bodyArray)
  store i32 %call15, i32* %numBodies, align 4
  %13 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island12, align 4
  %manifoldArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %13, i32 0, i32 1
  %call16 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %manifoldArray)
  store i32 %call16, i32* %numManifolds, align 4
  %14 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island12, align 4
  %constraintArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %14, i32 0, i32 2
  %call17 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %constraintArray)
  store i32 %call17, i32* %numConstraints, align 4
  %15 = load i32, i32* %lastIndex, align 4
  store i32 %15, i32* %firstIndex, align 4
  br label %while.body19

while.body19:                                     ; preds = %while.body, %if.end38
  %m_activeIslands20 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  %16 = load i32, i32* %firstIndex, align 4
  %call21 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %m_activeIslands20, i32 %16)
  %17 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %call21, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %17, %"struct.btSimulationIslandManagerMt::Island"** %src, align 4
  %18 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %src, align 4
  %bodyArray22 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %18, i32 0, i32 0
  %call23 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %bodyArray22)
  %19 = load i32, i32* %numBodies, align 4
  %add = add nsw i32 %19, %call23
  store i32 %add, i32* %numBodies, align 4
  %20 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %src, align 4
  %manifoldArray24 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %20, i32 0, i32 1
  %call25 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %manifoldArray24)
  %21 = load i32, i32* %numManifolds, align 4
  %add26 = add nsw i32 %21, %call25
  store i32 %add26, i32* %numManifolds, align 4
  %22 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %src, align 4
  %constraintArray27 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %22, i32 0, i32 2
  %call28 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %constraintArray27)
  %23 = load i32, i32* %numConstraints, align 4
  %add29 = add nsw i32 %23, %call28
  store i32 %add29, i32* %numConstraints, align 4
  %24 = load i32, i32* %numBodies, align 4
  %25 = load i32, i32* %numManifolds, align 4
  %26 = load i32, i32* %numConstraints, align 4
  %call30 = call i32 @_Z13calcBatchCostiii(i32 %24, i32 %25, i32 %26)
  store i32 %call30, i32* %batchCost, align 4
  %27 = load i32, i32* %batchCost, align 4
  %m_minimumSolverBatchSize31 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 7
  %28 = load i32, i32* %m_minimumSolverBatchSize31, align 4
  %cmp32 = icmp sge i32 %27, %28
  br i1 %cmp32, label %if.then33, label %if.end34

if.then33:                                        ; preds = %while.body19
  br label %while.end

if.end34:                                         ; preds = %while.body19
  %29 = load i32, i32* %firstIndex, align 4
  %sub35 = sub nsw i32 %29, 1
  %30 = load i32, i32* %destIslandIndex, align 4
  %cmp36 = icmp eq i32 %sub35, %30
  br i1 %cmp36, label %if.then37, label %if.end38

if.then37:                                        ; preds = %if.end34
  br label %while.end

if.end38:                                         ; preds = %if.end34
  %31 = load i32, i32* %firstIndex, align 4
  %dec = add nsw i32 %31, -1
  store i32 %dec, i32* %firstIndex, align 4
  br label %while.body19

while.end:                                        ; preds = %if.then37, %if.then33
  %32 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island12, align 4
  %bodyArray39 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %32, i32 0, i32 0
  %33 = load i32, i32* %numBodies, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7reserveEi(%class.btAlignedObjectArray.10* %bodyArray39, i32 %33)
  %34 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island12, align 4
  %manifoldArray40 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %34, i32 0, i32 1
  %35 = load i32, i32* %numManifolds, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.0* %manifoldArray40, i32 %35)
  %36 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island12, align 4
  %constraintArray41 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %36, i32 0, i32 2
  %37 = load i32, i32* %numConstraints, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.17* %constraintArray41, i32 %37)
  %38 = load i32, i32* %firstIndex, align 4
  store i32 %38, i32* %i42, align 4
  br label %for.cond43

for.cond43:                                       ; preds = %for.inc48, %while.end
  %39 = load i32, i32* %i42, align 4
  %40 = load i32, i32* %lastIndex, align 4
  %cmp44 = icmp sle i32 %39, %40
  br i1 %cmp44, label %for.body45, label %for.end50

for.body45:                                       ; preds = %for.cond43
  %41 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island12, align 4
  %m_activeIslands46 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  %42 = load i32, i32* %i42, align 4
  %call47 = call nonnull align 4 dereferenceable(4) %"struct.btSimulationIslandManagerMt::Island"** @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEEixEi(%class.btAlignedObjectArray.14* %m_activeIslands46, i32 %42)
  %43 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %call47, align 4
  call void @_ZN27btSimulationIslandManagerMt6Island6appendERKS0_(%"struct.btSimulationIslandManagerMt::Island"* %41, %"struct.btSimulationIslandManagerMt::Island"* nonnull align 4 dereferenceable(65) %43)
  br label %for.inc48

for.inc48:                                        ; preds = %for.body45
  %44 = load i32, i32* %i42, align 4
  %inc49 = add nsw i32 %44, 1
  store i32 %inc49, i32* %i42, align 4
  br label %for.cond43

for.end50:                                        ; preds = %for.cond43
  %m_activeIslands51 = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  %45 = load i32, i32* %firstIndex, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* null, %"struct.btSimulationIslandManagerMt::Island"** %ref.tmp52, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE6resizeEiRKS2_(%class.btAlignedObjectArray.14* %m_activeIslands51, i32 %45, %"struct.btSimulationIslandManagerMt::Island"** nonnull align 4 dereferenceable(4) %ref.tmp52)
  %46 = load i32, i32* %firstIndex, align 4
  %sub53 = sub nsw i32 %46, 1
  store i32 %sub53, i32* %lastIndex, align 4
  %47 = load i32, i32* %destIslandIndex, align 4
  %inc54 = add nsw i32 %47, 1
  store i32 %inc54, i32* %destIslandIndex, align 4
  br label %while.cond

while.end55:                                      ; preds = %while.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE9quickSortI28IslandBatchSizeSortPredicateEEvRKT_(%class.btAlignedObjectArray.14* %this, %class.IslandBatchSizeSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %CompareFunc.addr = alloca %class.IslandBatchSizeSortPredicate*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store %class.IslandBatchSizeSortPredicate* %CompareFunc, %class.IslandBatchSizeSortPredicate** %CompareFunc.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  %cmp = icmp sgt i32 %call, 1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %0 = load %class.IslandBatchSizeSortPredicate*, %class.IslandBatchSizeSortPredicate** %CompareFunc.addr, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  %sub = sub nsw i32 %call2, 1
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE17quickSortInternalI28IslandBatchSizeSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.14* %this1, %class.IslandBatchSizeSortPredicate* nonnull align 1 dereferenceable(1) %0, i32 0, i32 %sub)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_Z13calcBatchCostPKN27btSimulationIslandManagerMt6IslandE(%"struct.btSimulationIslandManagerMt::Island"* %island) #2 comdat {
entry:
  %island.addr = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %island, %"struct.btSimulationIslandManagerMt::Island"** %island.addr, align 4
  %0 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island.addr, align 4
  %bodyArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %0, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %bodyArray)
  %1 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island.addr, align 4
  %manifoldArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %1, i32 0, i32 1
  %call1 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %manifoldArray)
  %2 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %island.addr, align 4
  %constraintArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %2, i32 0, i32 2
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %constraintArray)
  %call3 = call i32 @_Z13calcBatchCostiii(i32 %call, i32 %call1, i32 %call2)
  ret i32 %call3
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray.0* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.0* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.0* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %2, %class.btPersistentManifold*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %4, %class.btPersistentManifold*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi(%class.btAlignedObjectArray.17* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.17* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi(%class.btAlignedObjectArray.17* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btTypedConstraint**
  store %class.btTypedConstraint** %2, %class.btTypedConstraint*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %this1)
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.17* %this1, i32 0, i32 %call3, %class.btTypedConstraint** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.17* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.17* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 4
  store %class.btTypedConstraint** %4, %class.btTypedConstraint*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray.0* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: noinline optnone
define hidden void @_ZN27btSimulationIslandManagerMt22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldR20btAlignedObjectArrayIP17btTypedConstraintEPNS_14IslandCallbackE(%class.btSimulationIslandManagerMt* %this, %class.btDispatcher* %dispatcher, %class.btCollisionWorld* %collisionWorld, %class.btAlignedObjectArray.17* nonnull align 4 dereferenceable(17) %constraints, %"struct.btSimulationIslandManagerMt::IslandCallback"* %callback) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btSimulationIslandManagerMt*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %collisionWorld.addr = alloca %class.btCollisionWorld*, align 4
  %constraints.addr = alloca %class.btAlignedObjectArray.17*, align 4
  %callback.addr = alloca %"struct.btSimulationIslandManagerMt::IslandCallback"*, align 4
  %collisionObjects = alloca %class.btAlignedObjectArray.10*, align 4
  %__profile = alloca %class.CProfileSample, align 1
  %manifolds = alloca %class.btPersistentManifold**, align 4
  %maxNumManifolds = alloca i32, align 4
  %i = alloca i32, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %colObj1 = alloca %class.btCollisionObject*, align 4
  %constraintsPtr = alloca %class.btTypedConstraint**, align 4
  store %class.btSimulationIslandManagerMt* %this, %class.btSimulationIslandManagerMt** %this.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  store %class.btCollisionWorld* %collisionWorld, %class.btCollisionWorld** %collisionWorld.addr, align 4
  store %class.btAlignedObjectArray.17* %constraints, %class.btAlignedObjectArray.17** %constraints.addr, align 4
  store %"struct.btSimulationIslandManagerMt::IslandCallback"* %callback, %"struct.btSimulationIslandManagerMt::IslandCallback"** %callback.addr, align 4
  %this1 = load %class.btSimulationIslandManagerMt*, %class.btSimulationIslandManagerMt** %this.addr, align 4
  %0 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %call = call nonnull align 4 dereferenceable(17) %class.btAlignedObjectArray.10* @_ZN16btCollisionWorld23getCollisionObjectArrayEv(%class.btCollisionWorld* %0)
  store %class.btAlignedObjectArray.10* %call, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %1 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %2 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %3 = bitcast %class.btSimulationIslandManagerMt* %this1 to void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*)***
  %vtable = load void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*)**, void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*)*** %3, align 4
  %vfn = getelementptr inbounds void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*)*, void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*)** %vtable, i64 11
  %4 = load void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*)*, void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*, %class.btCollisionWorld*)** %vfn, align 4
  call void %4(%class.btSimulationIslandManagerMt* %this1, %class.btDispatcher* %1, %class.btCollisionWorld* %2)
  %call2 = call %class.CProfileSample* @_ZN14CProfileSampleC1EPKc(%class.CProfileSample* %__profile, i8* getelementptr inbounds ([15 x i8], [15 x i8]* @.str.1, i32 0, i32 0))
  %5 = bitcast %class.btSimulationIslandManagerMt* %this1 to %class.btSimulationIslandManager*
  %call3 = call zeroext i1 @_ZN25btSimulationIslandManager15getSplitIslandsEv(%class.btSimulationIslandManager* %5)
  br i1 %call3, label %if.else, label %if.then

if.then:                                          ; preds = %entry
  %6 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %7 = bitcast %class.btDispatcher* %6 to %class.btPersistentManifold** (%class.btDispatcher*)***
  %vtable4 = load %class.btPersistentManifold** (%class.btDispatcher*)**, %class.btPersistentManifold** (%class.btDispatcher*)*** %7, align 4
  %vfn5 = getelementptr inbounds %class.btPersistentManifold** (%class.btDispatcher*)*, %class.btPersistentManifold** (%class.btDispatcher*)** %vtable4, i64 11
  %8 = load %class.btPersistentManifold** (%class.btDispatcher*)*, %class.btPersistentManifold** (%class.btDispatcher*)** %vfn5, align 4
  %call6 = call %class.btPersistentManifold** %8(%class.btDispatcher* %6)
  store %class.btPersistentManifold** %call6, %class.btPersistentManifold*** %manifolds, align 4
  %9 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %10 = bitcast %class.btDispatcher* %9 to i32 (%class.btDispatcher*)***
  %vtable7 = load i32 (%class.btDispatcher*)**, i32 (%class.btDispatcher*)*** %10, align 4
  %vfn8 = getelementptr inbounds i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vtable7, i64 9
  %11 = load i32 (%class.btDispatcher*)*, i32 (%class.btDispatcher*)** %vfn8, align 4
  %call9 = call i32 %11(%class.btDispatcher* %9)
  store i32 %call9, i32* %maxNumManifolds, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %if.then
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %maxNumManifolds, align 4
  %cmp = icmp slt i32 %12, %13
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %14 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifolds, align 4
  %15 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %14, i32 %15
  %16 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx, align 4
  store %class.btPersistentManifold* %16, %class.btPersistentManifold** %manifold, align 4
  %17 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call10 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody0Ev(%class.btPersistentManifold* %17)
  store %class.btCollisionObject* %call10, %class.btCollisionObject** %colObj0, align 4
  %18 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %call11 = call %class.btCollisionObject* @_ZNK20btPersistentManifold8getBody1Ev(%class.btPersistentManifold* %18)
  store %class.btCollisionObject* %call11, %class.btCollisionObject** %colObj1, align 4
  %19 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %tobool = icmp ne %class.btCollisionObject* %19, null
  br i1 %tobool, label %land.lhs.true, label %lor.lhs.false

land.lhs.true:                                    ; preds = %for.body
  %20 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call12 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %20)
  %cmp13 = icmp ne i32 %call12, 2
  br i1 %cmp13, label %if.then18, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %land.lhs.true, %for.body
  %21 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %tobool14 = icmp ne %class.btCollisionObject* %21, null
  br i1 %tobool14, label %land.lhs.true15, label %if.end36

land.lhs.true15:                                  ; preds = %lor.lhs.false
  %22 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call16 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %22)
  %cmp17 = icmp ne i32 %call16, 2
  br i1 %cmp17, label %if.then18, label %if.end36

if.then18:                                        ; preds = %land.lhs.true15, %land.lhs.true
  %23 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call19 = call zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %23)
  br i1 %call19, label %land.lhs.true20, label %if.end26

land.lhs.true20:                                  ; preds = %if.then18
  %24 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call21 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %24)
  %cmp22 = icmp ne i32 %call21, 2
  br i1 %cmp22, label %if.then23, label %if.end26

if.then23:                                        ; preds = %land.lhs.true20
  %25 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call24 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %25)
  br i1 %call24, label %if.then25, label %if.end

if.then25:                                        ; preds = %if.then23
  %26 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  call void @_ZNK17btCollisionObject8activateEb(%class.btCollisionObject* %26, i1 zeroext false)
  br label %if.end

if.end:                                           ; preds = %if.then25, %if.then23
  br label %if.end26

if.end26:                                         ; preds = %if.end, %land.lhs.true20, %if.then18
  %27 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call27 = call zeroext i1 @_ZNK17btCollisionObject17isKinematicObjectEv(%class.btCollisionObject* %27)
  br i1 %call27, label %land.lhs.true28, label %if.end35

land.lhs.true28:                                  ; preds = %if.end26
  %28 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call29 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %28)
  %cmp30 = icmp ne i32 %call29, 2
  br i1 %cmp30, label %if.then31, label %if.end35

if.then31:                                        ; preds = %land.lhs.true28
  %29 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call32 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %29)
  br i1 %call32, label %if.then33, label %if.end34

if.then33:                                        ; preds = %if.then31
  %30 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  call void @_ZNK17btCollisionObject8activateEb(%class.btCollisionObject* %30, i1 zeroext false)
  br label %if.end34

if.end34:                                         ; preds = %if.then33, %if.then31
  br label %if.end35

if.end35:                                         ; preds = %if.end34, %land.lhs.true28, %if.end26
  br label %if.end36

if.end36:                                         ; preds = %if.end35, %land.lhs.true15, %lor.lhs.false
  br label %for.inc

for.inc:                                          ; preds = %if.end36
  %31 = load i32, i32* %i, align 4
  %inc = add nsw i32 %31, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %32 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %constraints.addr, align 4
  %call37 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %32)
  %tobool38 = icmp ne i32 %call37, 0
  br i1 %tobool38, label %cond.true, label %cond.false

cond.true:                                        ; preds = %for.end
  %33 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %constraints.addr, align 4
  %call39 = call nonnull align 4 dereferenceable(4) %class.btTypedConstraint** @_ZN20btAlignedObjectArrayIP17btTypedConstraintEixEi(%class.btAlignedObjectArray.17* %33, i32 0)
  br label %cond.end

cond.false:                                       ; preds = %for.end
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btTypedConstraint** [ %call39, %cond.true ], [ null, %cond.false ]
  store %class.btTypedConstraint** %cond, %class.btTypedConstraint*** %constraintsPtr, align 4
  %34 = load %"struct.btSimulationIslandManagerMt::IslandCallback"*, %"struct.btSimulationIslandManagerMt::IslandCallback"** %callback.addr, align 4
  %35 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %call40 = call nonnull align 4 dereferenceable(4) %class.btCollisionObject** @_ZN20btAlignedObjectArrayIP17btCollisionObjectEixEi(%class.btAlignedObjectArray.10* %35, i32 0)
  %36 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %collisionObjects, align 4
  %call41 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %36)
  %37 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %manifolds, align 4
  %38 = load i32, i32* %maxNumManifolds, align 4
  %39 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %constraintsPtr, align 4
  %40 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %constraints.addr, align 4
  %call42 = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %40)
  %41 = bitcast %"struct.btSimulationIslandManagerMt::IslandCallback"* %34 to void (%"struct.btSimulationIslandManagerMt::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)***
  %vtable43 = load void (%"struct.btSimulationIslandManagerMt::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)**, void (%"struct.btSimulationIslandManagerMt::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)*** %41, align 4
  %vfn44 = getelementptr inbounds void (%"struct.btSimulationIslandManagerMt::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)*, void (%"struct.btSimulationIslandManagerMt::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)** %vtable43, i64 2
  %42 = load void (%"struct.btSimulationIslandManagerMt::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)*, void (%"struct.btSimulationIslandManagerMt::IslandCallback"*, %class.btCollisionObject**, i32, %class.btPersistentManifold**, i32, %class.btTypedConstraint**, i32, i32)** %vfn44, align 4
  call void %42(%"struct.btSimulationIslandManagerMt::IslandCallback"* %34, %class.btCollisionObject** %call40, i32 %call41, %class.btPersistentManifold** %37, i32 %38, %class.btTypedConstraint** %39, i32 %call42, i32 -1)
  br label %if.end58

if.else:                                          ; preds = %entry
  %43 = bitcast %class.btSimulationIslandManagerMt* %this1 to void (%class.btSimulationIslandManagerMt*)***
  %vtable45 = load void (%class.btSimulationIslandManagerMt*)**, void (%class.btSimulationIslandManagerMt*)*** %43, align 4
  %vfn46 = getelementptr inbounds void (%class.btSimulationIslandManagerMt*)*, void (%class.btSimulationIslandManagerMt*)** %vtable45, i64 5
  %44 = load void (%class.btSimulationIslandManagerMt*)*, void (%class.btSimulationIslandManagerMt*)** %vfn46, align 4
  call void %44(%class.btSimulationIslandManagerMt* %this1)
  %45 = load %class.btCollisionWorld*, %class.btCollisionWorld** %collisionWorld.addr, align 4
  %46 = bitcast %class.btSimulationIslandManagerMt* %this1 to void (%class.btSimulationIslandManagerMt*, %class.btCollisionWorld*)***
  %vtable47 = load void (%class.btSimulationIslandManagerMt*, %class.btCollisionWorld*)**, void (%class.btSimulationIslandManagerMt*, %class.btCollisionWorld*)*** %46, align 4
  %vfn48 = getelementptr inbounds void (%class.btSimulationIslandManagerMt*, %class.btCollisionWorld*)*, void (%class.btSimulationIslandManagerMt*, %class.btCollisionWorld*)** %vtable47, i64 6
  %47 = load void (%class.btSimulationIslandManagerMt*, %class.btCollisionWorld*)*, void (%class.btSimulationIslandManagerMt*, %class.btCollisionWorld*)** %vfn48, align 4
  call void %47(%class.btSimulationIslandManagerMt* %this1, %class.btCollisionWorld* %45)
  %48 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %49 = bitcast %class.btSimulationIslandManagerMt* %this1 to void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*)***
  %vtable49 = load void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*)**, void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*)*** %49, align 4
  %vfn50 = getelementptr inbounds void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*)*, void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*)** %vtable49, i64 7
  %50 = load void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*)*, void (%class.btSimulationIslandManagerMt*, %class.btDispatcher*)** %vfn50, align 4
  call void %50(%class.btSimulationIslandManagerMt* %this1, %class.btDispatcher* %48)
  %51 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %constraints.addr, align 4
  %52 = bitcast %class.btSimulationIslandManagerMt* %this1 to void (%class.btSimulationIslandManagerMt*, %class.btAlignedObjectArray.17*)***
  %vtable51 = load void (%class.btSimulationIslandManagerMt*, %class.btAlignedObjectArray.17*)**, void (%class.btSimulationIslandManagerMt*, %class.btAlignedObjectArray.17*)*** %52, align 4
  %vfn52 = getelementptr inbounds void (%class.btSimulationIslandManagerMt*, %class.btAlignedObjectArray.17*)*, void (%class.btSimulationIslandManagerMt*, %class.btAlignedObjectArray.17*)** %vtable51, i64 8
  %53 = load void (%class.btSimulationIslandManagerMt*, %class.btAlignedObjectArray.17*)*, void (%class.btSimulationIslandManagerMt*, %class.btAlignedObjectArray.17*)** %vfn52, align 4
  call void %53(%class.btSimulationIslandManagerMt* %this1, %class.btAlignedObjectArray.17* nonnull align 4 dereferenceable(17) %51)
  %m_minimumSolverBatchSize = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 7
  %54 = load i32, i32* %m_minimumSolverBatchSize, align 4
  %cmp53 = icmp sgt i32 %54, 1
  br i1 %cmp53, label %if.then54, label %if.end57

if.then54:                                        ; preds = %if.else
  %55 = bitcast %class.btSimulationIslandManagerMt* %this1 to void (%class.btSimulationIslandManagerMt*)***
  %vtable55 = load void (%class.btSimulationIslandManagerMt*)**, void (%class.btSimulationIslandManagerMt*)*** %55, align 4
  %vfn56 = getelementptr inbounds void (%class.btSimulationIslandManagerMt*)*, void (%class.btSimulationIslandManagerMt*)** %vtable55, i64 9
  %56 = load void (%class.btSimulationIslandManagerMt*)*, void (%class.btSimulationIslandManagerMt*)** %vfn56, align 4
  call void %56(%class.btSimulationIslandManagerMt* %this1)
  br label %if.end57

if.end57:                                         ; preds = %if.then54, %if.else
  %m_islandDispatch = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 9
  %57 = load void (%class.btAlignedObjectArray.14*, %"struct.btSimulationIslandManagerMt::IslandCallback"*)*, void (%class.btAlignedObjectArray.14*, %"struct.btSimulationIslandManagerMt::IslandCallback"*)** %m_islandDispatch, align 4
  %m_activeIslands = getelementptr inbounds %class.btSimulationIslandManagerMt, %class.btSimulationIslandManagerMt* %this1, i32 0, i32 3
  %58 = load %"struct.btSimulationIslandManagerMt::IslandCallback"*, %"struct.btSimulationIslandManagerMt::IslandCallback"** %callback.addr, align 4
  call void %57(%class.btAlignedObjectArray.14* %m_activeIslands, %"struct.btSimulationIslandManagerMt::IslandCallback"* %58)
  br label %if.end58

if.end58:                                         ; preds = %if.end57, %cond.end
  %call59 = call %class.CProfileSample* @_ZN14CProfileSampleD1Ev(%class.CProfileSample* %__profile) #8
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN25btSimulationIslandManager15getSplitIslandsEv(%class.btSimulationIslandManager* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btSimulationIslandManager*, align 4
  store %class.btSimulationIslandManager* %this, %class.btSimulationIslandManager** %this.addr, align 4
  %this1 = load %class.btSimulationIslandManager*, %class.btSimulationIslandManager** %this.addr, align 4
  %m_splitIslands = getelementptr inbounds %class.btSimulationIslandManager, %class.btSimulationIslandManager* %this1, i32 0, i32 4
  %0 = load i8, i8* %m_splitIslands, align 4
  %tobool = trunc i8 %0 to i1
  ret i1 %tobool
}

declare void @_ZN25btSimulationIslandManager21updateActivationStateEP16btCollisionWorldP12btDispatcher(%class.btSimulationIslandManager*, %class.btCollisionWorld*, %class.btDispatcher*) unnamed_addr #3

declare void @_ZN25btSimulationIslandManager26storeIslandActivationStateEP16btCollisionWorld(%class.btSimulationIslandManager*, %class.btCollisionWorld*) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.17* @_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev(%class.btAlignedObjectArray.17* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray.17* %this1)
  ret %class.btAlignedObjectArray.17* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev(%class.btAlignedObjectArray.10* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv(%class.btAlignedObjectArray.10* %this1)
  ret %class.btAlignedObjectArray.10* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE5clearEv(%class.btAlignedObjectArray.17* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4sizeEv(%class.btAlignedObjectArray.17* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.17* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.17* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.17* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE7destroyEii(%class.btAlignedObjectArray.17* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 4
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv(%class.btAlignedObjectArray.17* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %tobool = icmp ne %class.btTypedConstraint** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 4
  %2 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.18* %m_allocator, %class.btTypedConstraint** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.17* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 4
  store %class.btTypedConstraint** null, %class.btTypedConstraint*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE10deallocateEPS1_(%class.btAlignedAllocator.18* %this, %class.btTypedConstraint** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.18*, align 4
  %ptr.addr = alloca %class.btTypedConstraint**, align 4
  store %class.btAlignedAllocator.18* %this, %class.btAlignedAllocator.18** %this.addr, align 4
  store %class.btTypedConstraint** %ptr, %class.btTypedConstraint*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.18*, %class.btAlignedAllocator.18** %this.addr, align 4
  %0 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %ptr.addr, align 4
  %1 = bitcast %class.btTypedConstraint** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #3

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.0* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.0* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray.0* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray.0* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator.1* %this, %class.btPersistentManifold** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE5clearEv(%class.btAlignedObjectArray.10* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4sizeEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray.10* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray.10* %this1)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray.10* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE7destroyEii(%class.btAlignedObjectArray.10* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv(%class.btAlignedObjectArray.10* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %tobool = icmp ne %class.btCollisionObject** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %2 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_(%class.btAlignedAllocator.11* %m_allocator, %class.btCollisionObject** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray.10* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  store %class.btCollisionObject** null, %class.btCollisionObject*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE10deallocateEPS1_(%class.btAlignedAllocator.11* %this, %class.btCollisionObject** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  %ptr.addr = alloca %class.btCollisionObject**, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4
  store %class.btCollisionObject** %ptr, %class.btCollisionObject*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  %0 = load %class.btCollisionObject**, %class.btCollisionObject*** %ptr.addr, align 4
  %1 = bitcast %class.btCollisionObject** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayI9btElementE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.10* @_ZN20btAlignedObjectArrayIP17btCollisionObjectEC2Ev(%class.btAlignedObjectArray.10* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.11* @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator.11* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP17btCollisionObjectE4initEv(%class.btAlignedObjectArray.10* %this1)
  ret %class.btAlignedObjectArray.10* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.0* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray.0* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.1* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray.0* %this1)
  ret %class.btAlignedObjectArray.0* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray.17* @_ZN20btAlignedObjectArrayIP17btTypedConstraintEC2Ev(%class.btAlignedObjectArray.17* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator.18* @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev(%class.btAlignedAllocator.18* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP17btTypedConstraintE4initEv(%class.btAlignedObjectArray.17* %this1)
  ret %class.btAlignedObjectArray.17* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.11* @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EEC2Ev(%class.btAlignedAllocator.11* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  ret %class.btAlignedAllocator.11* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.1* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator.1* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  ret %class.btAlignedAllocator.1* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.18* @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EEC2Ev(%class.btAlignedAllocator.18* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.18*, align 4
  store %class.btAlignedAllocator.18* %this, %class.btAlignedAllocator.18** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.18*, %class.btAlignedAllocator.18** %this.addr, align 4
  ret %class.btAlignedAllocator.18* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(8) %struct.btElement* @_ZN20btAlignedObjectArrayI9btElementEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %struct.btElement*, %struct.btElement** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %struct.btElement, %struct.btElement* %0, i32 %1
  ret %struct.btElement* %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyAEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbA = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 8
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbA, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(676) %class.btRigidBody* @_ZNK17btTypedConstraint13getRigidBodyBEv(%class.btTypedConstraint* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btTypedConstraint*, align 4
  store %class.btTypedConstraint* %this, %class.btTypedConstraint** %this.addr, align 4
  %this1 = load %class.btTypedConstraint*, %class.btTypedConstraint** %this.addr, align 4
  %m_rbB = getelementptr inbounds %class.btTypedConstraint, %class.btTypedConstraint* %this1, i32 0, i32 9
  %0 = load %class.btRigidBody*, %class.btRigidBody** %m_rbB, align 4
  ret %class.btRigidBody* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator.15* @_ZN18btAlignedAllocatorIPN27btSimulationIslandManagerMt6IslandELj16EEC2Ev(%class.btAlignedAllocator.15* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.15*, align 4
  store %class.btAlignedAllocator.15* %this, %class.btAlignedAllocator.15** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator.15*, %class.btAlignedAllocator.15** %this.addr, align 4
  ret %class.btAlignedAllocator.15* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4initEv(%class.btAlignedObjectArray.14* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  store %"struct.btSimulationIslandManagerMt::Island"** null, %"struct.btSimulationIslandManagerMt::Island"*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE5clearEv(%class.btAlignedObjectArray.14* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE7destroyEii(%class.btAlignedObjectArray.14* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE10deallocateEv(%class.btAlignedObjectArray.14* %this1)
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4initEv(%class.btAlignedObjectArray.14* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE7destroyEii(%class.btAlignedObjectArray.14* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %3 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE10deallocateEv(%class.btAlignedObjectArray.14* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %0 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data, align 4
  %tobool = icmp ne %"struct.btSimulationIslandManagerMt::Island"** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %2 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIPN27btSimulationIslandManagerMt6IslandELj16EE10deallocateEPS2_(%class.btAlignedAllocator.15* %m_allocator, %"struct.btSimulationIslandManagerMt::Island"** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  store %"struct.btSimulationIslandManagerMt::Island"** null, %"struct.btSimulationIslandManagerMt::Island"*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIPN27btSimulationIslandManagerMt6IslandELj16EE10deallocateEPS2_(%class.btAlignedAllocator.15* %this, %"struct.btSimulationIslandManagerMt::Island"** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.15*, align 4
  %ptr.addr = alloca %"struct.btSimulationIslandManagerMt::Island"**, align 4
  store %class.btAlignedAllocator.15* %this, %class.btAlignedAllocator.15** %this.addr, align 4
  store %"struct.btSimulationIslandManagerMt::Island"** %ptr, %"struct.btSimulationIslandManagerMt::Island"*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator.15*, %class.btAlignedAllocator.15** %this.addr, align 4
  %0 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %ptr.addr, align 4
  %1 = bitcast %"struct.btSimulationIslandManagerMt::Island"** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE7reserveEi(%class.btAlignedObjectArray.14* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %"struct.btSimulationIslandManagerMt::Island"**, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE8capacityEv(%class.btAlignedObjectArray.14* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE8allocateEi(%class.btAlignedObjectArray.14* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %"struct.btSimulationIslandManagerMt::Island"**
  store %"struct.btSimulationIslandManagerMt::Island"** %2, %"struct.btSimulationIslandManagerMt::Island"*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  %3 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4copyEiiPS2_(%class.btAlignedObjectArray.14* %this1, i32 0, i32 %call3, %"struct.btSimulationIslandManagerMt::Island"** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4sizeEv(%class.btAlignedObjectArray.14* %this1)
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE7destroyEii(%class.btAlignedObjectArray.14* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE10deallocateEv(%class.btAlignedObjectArray.14* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  store %"struct.btSimulationIslandManagerMt::Island"** %4, %"struct.btSimulationIslandManagerMt::Island"*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE8capacityEv(%class.btAlignedObjectArray.14* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE8allocateEi(%class.btAlignedObjectArray.14* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %"struct.btSimulationIslandManagerMt::Island"** @_ZN18btAlignedAllocatorIPN27btSimulationIslandManagerMt6IslandELj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.15* %m_allocator, i32 %1, %"struct.btSimulationIslandManagerMt::Island"*** null)
  %2 = bitcast %"struct.btSimulationIslandManagerMt::Island"** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4copyEiiPS2_(%class.btAlignedObjectArray.14* %this, i32 %start, i32 %end, %"struct.btSimulationIslandManagerMt::Island"** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %"struct.btSimulationIslandManagerMt::Island"**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %"struct.btSimulationIslandManagerMt::Island"** %dest, %"struct.btSimulationIslandManagerMt::Island"*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %3, i32 %4
  %5 = bitcast %"struct.btSimulationIslandManagerMt::Island"** %arrayidx to i8*
  %6 = bitcast i8* %5 to %"struct.btSimulationIslandManagerMt::Island"**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %7 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %7, i32 %8
  %9 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %arrayidx2, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %9, %"struct.btSimulationIslandManagerMt::Island"** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %"struct.btSimulationIslandManagerMt::Island"** @_ZN18btAlignedAllocatorIPN27btSimulationIslandManagerMt6IslandELj16EE8allocateEiPPKS2_(%class.btAlignedAllocator.15* %this, i32 %n, %"struct.btSimulationIslandManagerMt::Island"*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.15*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %"struct.btSimulationIslandManagerMt::Island"***, align 4
  store %class.btAlignedAllocator.15* %this, %class.btAlignedAllocator.15** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %"struct.btSimulationIslandManagerMt::Island"*** %hint, %"struct.btSimulationIslandManagerMt::Island"**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.15*, %class.btAlignedAllocator.15** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %"struct.btSimulationIslandManagerMt::Island"**
  ret %"struct.btSimulationIslandManagerMt::Island"** %1
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #3

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP17btCollisionObjectE9allocSizeEi(%class.btAlignedObjectArray.10* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray.0* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray.0* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE8capacityEv(%class.btAlignedObjectArray.17* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP17btTypedConstraintE9allocSizeEi(%class.btAlignedObjectArray.17* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE17quickSortInternalI31IslandBodyCapacitySortPredicateEEvRKT_ii(%class.btAlignedObjectArray.14* %this, %class.IslandBodyCapacitySortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %CompareFunc.addr = alloca %class.IslandBodyCapacitySortPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store %class.IslandBodyCapacitySortPredicate* %CompareFunc, %class.IslandBodyCapacitySortPredicate** %CompareFunc.addr, align 4
  store i32 %lo, i32* %lo.addr, align 4
  store i32 %hi, i32* %hi.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %0 = load i32, i32* %lo.addr, align 4
  store i32 %0, i32* %i, align 4
  %1 = load i32, i32* %hi.addr, align 4
  store i32 %1, i32* %j, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %2 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data, align 4
  %3 = load i32, i32* %lo.addr, align 4
  %4 = load i32, i32* %hi.addr, align 4
  %add = add nsw i32 %3, %4
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %2, i32 %div
  %5 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %arrayidx, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %5, %"struct.btSimulationIslandManagerMt::Island"** %x, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %6 = load %class.IslandBodyCapacitySortPredicate*, %class.IslandBodyCapacitySortPredicate** %CompareFunc.addr, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %7 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data2, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %7, i32 %8
  %9 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %arrayidx3, align 4
  %10 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %x, align 4
  %call = call zeroext i1 @_ZNK31IslandBodyCapacitySortPredicateclEPKN27btSimulationIslandManagerMt6IslandES3_(%class.IslandBodyCapacitySortPredicate* %6, %"struct.btSimulationIslandManagerMt::Island"* %9, %"struct.btSimulationIslandManagerMt::Island"* %10)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond4

while.cond4:                                      ; preds = %while.body8, %while.end
  %12 = load %class.IslandBodyCapacitySortPredicate*, %class.IslandBodyCapacitySortPredicate** %CompareFunc.addr, align 4
  %13 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %x, align 4
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %14 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data5, align 4
  %15 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %14, i32 %15
  %16 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %arrayidx6, align 4
  %call7 = call zeroext i1 @_ZNK31IslandBodyCapacitySortPredicateclEPKN27btSimulationIslandManagerMt6IslandES3_(%class.IslandBodyCapacitySortPredicate* %12, %"struct.btSimulationIslandManagerMt::Island"* %13, %"struct.btSimulationIslandManagerMt::Island"* %16)
  br i1 %call7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond4
  %17 = load i32, i32* %j, align 4
  %dec = add nsw i32 %17, -1
  store i32 %dec, i32* %j, align 4
  br label %while.cond4

while.end9:                                       ; preds = %while.cond4
  %18 = load i32, i32* %i, align 4
  %19 = load i32, i32* %j, align 4
  %cmp = icmp sle i32 %18, %19
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %20 = load i32, i32* %i, align 4
  %21 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4swapEii(%class.btAlignedObjectArray.14* %this1, i32 %20, i32 %21)
  %22 = load i32, i32* %i, align 4
  %inc10 = add nsw i32 %22, 1
  store i32 %inc10, i32* %i, align 4
  %23 = load i32, i32* %j, align 4
  %dec11 = add nsw i32 %23, -1
  store i32 %dec11, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %24 = load i32, i32* %i, align 4
  %25 = load i32, i32* %j, align 4
  %cmp12 = icmp sle i32 %24, %25
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %26 = load i32, i32* %lo.addr, align 4
  %27 = load i32, i32* %j, align 4
  %cmp13 = icmp slt i32 %26, %27
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %28 = load %class.IslandBodyCapacitySortPredicate*, %class.IslandBodyCapacitySortPredicate** %CompareFunc.addr, align 4
  %29 = load i32, i32* %lo.addr, align 4
  %30 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE17quickSortInternalI31IslandBodyCapacitySortPredicateEEvRKT_ii(%class.btAlignedObjectArray.14* %this1, %class.IslandBodyCapacitySortPredicate* nonnull align 1 dereferenceable(1) %28, i32 %29, i32 %30)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %31 = load i32, i32* %i, align 4
  %32 = load i32, i32* %hi.addr, align 4
  %cmp16 = icmp slt i32 %31, %32
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %33 = load %class.IslandBodyCapacitySortPredicate*, %class.IslandBodyCapacitySortPredicate** %CompareFunc.addr, align 4
  %34 = load i32, i32* %i, align 4
  %35 = load i32, i32* %hi.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE17quickSortInternalI31IslandBodyCapacitySortPredicateEEvRKT_ii(%class.btAlignedObjectArray.14* %this1, %class.IslandBodyCapacitySortPredicate* nonnull align 1 dereferenceable(1) %33, i32 %34, i32 %35)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK31IslandBodyCapacitySortPredicateclEPKN27btSimulationIslandManagerMt6IslandES3_(%class.IslandBodyCapacitySortPredicate* %this, %"struct.btSimulationIslandManagerMt::Island"* %lhs, %"struct.btSimulationIslandManagerMt::Island"* %rhs) #1 comdat {
entry:
  %this.addr = alloca %class.IslandBodyCapacitySortPredicate*, align 4
  %lhs.addr = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %rhs.addr = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  store %class.IslandBodyCapacitySortPredicate* %this, %class.IslandBodyCapacitySortPredicate** %this.addr, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %lhs, %"struct.btSimulationIslandManagerMt::Island"** %lhs.addr, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %rhs, %"struct.btSimulationIslandManagerMt::Island"** %rhs.addr, align 4
  %this1 = load %class.IslandBodyCapacitySortPredicate*, %class.IslandBodyCapacitySortPredicate** %this.addr, align 4
  %0 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %lhs.addr, align 4
  %bodyArray = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %0, i32 0, i32 0
  %call = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray.10* %bodyArray)
  %1 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %rhs.addr, align 4
  %bodyArray2 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island", %"struct.btSimulationIslandManagerMt::Island"* %1, i32 0, i32 0
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE8capacityEv(%class.btAlignedObjectArray.10* %bodyArray2)
  %cmp = icmp sgt i32 %call, %call3
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4swapEii(%class.btAlignedObjectArray.14* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %0 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %0, i32 %1
  %2 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %arrayidx, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %2, %"struct.btSimulationIslandManagerMt::Island"** %temp, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %3 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data2, align 4
  %4 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %3, i32 %4
  %5 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %arrayidx3, align 4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %6 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %6, i32 %7
  store %"struct.btSimulationIslandManagerMt::Island"* %5, %"struct.btSimulationIslandManagerMt::Island"** %arrayidx5, align 4
  %8 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %temp, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %9 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data6, align 4
  %10 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %9, i32 %10
  store %"struct.btSimulationIslandManagerMt::Island"* %8, %"struct.btSimulationIslandManagerMt::Island"** %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE9allocSizeEi(%class.btAlignedObjectArray.14* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP17btCollisionObjectE8allocateEi(%class.btAlignedObjectArray.10* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btCollisionObject** @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.11* %m_allocator, i32 %1, %class.btCollisionObject*** null)
  %2 = bitcast %class.btCollisionObject** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_(%class.btAlignedObjectArray.10* %this, i32 %start, i32 %end, %class.btCollisionObject** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.10*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btCollisionObject**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.10* %this, %class.btAlignedObjectArray.10** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btCollisionObject** %dest, %class.btCollisionObject*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.10*, %class.btAlignedObjectArray.10** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btCollisionObject**, %class.btCollisionObject*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %3, i32 %4
  %5 = bitcast %class.btCollisionObject** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btCollisionObject**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.10, %class.btAlignedObjectArray.10* %this1, i32 0, i32 4
  %7 = load %class.btCollisionObject**, %class.btCollisionObject*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btCollisionObject*, %class.btCollisionObject** %7, i32 %8
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %arrayidx2, align 4
  store %class.btCollisionObject* %9, %class.btCollisionObject** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btCollisionObject** @_ZN18btAlignedAllocatorIP17btCollisionObjectLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.11* %this, i32 %n, %class.btCollisionObject*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.11*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btCollisionObject***, align 4
  store %class.btAlignedAllocator.11* %this, %class.btAlignedAllocator.11** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btCollisionObject*** %hint, %class.btCollisionObject**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.11*, %class.btAlignedAllocator.11** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btCollisionObject**
  ret %class.btCollisionObject** %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE17quickSortInternalI28IslandBatchSizeSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.14* %this, %class.IslandBatchSizeSortPredicate* nonnull align 1 dereferenceable(1) %CompareFunc, i32 %lo, i32 %hi) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.14*, align 4
  %CompareFunc.addr = alloca %class.IslandBatchSizeSortPredicate*, align 4
  %lo.addr = alloca i32, align 4
  %hi.addr = alloca i32, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  %x = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  store %class.btAlignedObjectArray.14* %this, %class.btAlignedObjectArray.14** %this.addr, align 4
  store %class.IslandBatchSizeSortPredicate* %CompareFunc, %class.IslandBatchSizeSortPredicate** %CompareFunc.addr, align 4
  store i32 %lo, i32* %lo.addr, align 4
  store i32 %hi, i32* %hi.addr, align 4
  %this1 = load %class.btAlignedObjectArray.14*, %class.btAlignedObjectArray.14** %this.addr, align 4
  %0 = load i32, i32* %lo.addr, align 4
  store i32 %0, i32* %i, align 4
  %1 = load i32, i32* %hi.addr, align 4
  store i32 %1, i32* %j, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %2 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data, align 4
  %3 = load i32, i32* %lo.addr, align 4
  %4 = load i32, i32* %hi.addr, align 4
  %add = add nsw i32 %3, %4
  %div = sdiv i32 %add, 2
  %arrayidx = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %2, i32 %div
  %5 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %arrayidx, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %5, %"struct.btSimulationIslandManagerMt::Island"** %x, align 4
  br label %do.body

do.body:                                          ; preds = %do.cond, %entry
  br label %while.cond

while.cond:                                       ; preds = %while.body, %do.body
  %6 = load %class.IslandBatchSizeSortPredicate*, %class.IslandBatchSizeSortPredicate** %CompareFunc.addr, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %7 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data2, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx3 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %7, i32 %8
  %9 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %arrayidx3, align 4
  %10 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %x, align 4
  %call = call zeroext i1 @_ZNK28IslandBatchSizeSortPredicateclEPKN27btSimulationIslandManagerMt6IslandES3_(%class.IslandBatchSizeSortPredicate* %6, %"struct.btSimulationIslandManagerMt::Island"* %9, %"struct.btSimulationIslandManagerMt::Island"* %10)
  br i1 %call, label %while.body, label %while.end

while.body:                                       ; preds = %while.cond
  %11 = load i32, i32* %i, align 4
  %inc = add nsw i32 %11, 1
  store i32 %inc, i32* %i, align 4
  br label %while.cond

while.end:                                        ; preds = %while.cond
  br label %while.cond4

while.cond4:                                      ; preds = %while.body8, %while.end
  %12 = load %class.IslandBatchSizeSortPredicate*, %class.IslandBatchSizeSortPredicate** %CompareFunc.addr, align 4
  %13 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %x, align 4
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray.14, %class.btAlignedObjectArray.14* %this1, i32 0, i32 4
  %14 = load %"struct.btSimulationIslandManagerMt::Island"**, %"struct.btSimulationIslandManagerMt::Island"*** %m_data5, align 4
  %15 = load i32, i32* %j, align 4
  %arrayidx6 = getelementptr inbounds %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %14, i32 %15
  %16 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %arrayidx6, align 4
  %call7 = call zeroext i1 @_ZNK28IslandBatchSizeSortPredicateclEPKN27btSimulationIslandManagerMt6IslandES3_(%class.IslandBatchSizeSortPredicate* %12, %"struct.btSimulationIslandManagerMt::Island"* %13, %"struct.btSimulationIslandManagerMt::Island"* %16)
  br i1 %call7, label %while.body8, label %while.end9

while.body8:                                      ; preds = %while.cond4
  %17 = load i32, i32* %j, align 4
  %dec = add nsw i32 %17, -1
  store i32 %dec, i32* %j, align 4
  br label %while.cond4

while.end9:                                       ; preds = %while.cond4
  %18 = load i32, i32* %i, align 4
  %19 = load i32, i32* %j, align 4
  %cmp = icmp sle i32 %18, %19
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %while.end9
  %20 = load i32, i32* %i, align 4
  %21 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE4swapEii(%class.btAlignedObjectArray.14* %this1, i32 %20, i32 %21)
  %22 = load i32, i32* %i, align 4
  %inc10 = add nsw i32 %22, 1
  store i32 %inc10, i32* %i, align 4
  %23 = load i32, i32* %j, align 4
  %dec11 = add nsw i32 %23, -1
  store i32 %dec11, i32* %j, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %while.end9
  br label %do.cond

do.cond:                                          ; preds = %if.end
  %24 = load i32, i32* %i, align 4
  %25 = load i32, i32* %j, align 4
  %cmp12 = icmp sle i32 %24, %25
  br i1 %cmp12, label %do.body, label %do.end

do.end:                                           ; preds = %do.cond
  %26 = load i32, i32* %lo.addr, align 4
  %27 = load i32, i32* %j, align 4
  %cmp13 = icmp slt i32 %26, %27
  br i1 %cmp13, label %if.then14, label %if.end15

if.then14:                                        ; preds = %do.end
  %28 = load %class.IslandBatchSizeSortPredicate*, %class.IslandBatchSizeSortPredicate** %CompareFunc.addr, align 4
  %29 = load i32, i32* %lo.addr, align 4
  %30 = load i32, i32* %j, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE17quickSortInternalI28IslandBatchSizeSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.14* %this1, %class.IslandBatchSizeSortPredicate* nonnull align 1 dereferenceable(1) %28, i32 %29, i32 %30)
  br label %if.end15

if.end15:                                         ; preds = %if.then14, %do.end
  %31 = load i32, i32* %i, align 4
  %32 = load i32, i32* %hi.addr, align 4
  %cmp16 = icmp slt i32 %31, %32
  br i1 %cmp16, label %if.then17, label %if.end18

if.then17:                                        ; preds = %if.end15
  %33 = load %class.IslandBatchSizeSortPredicate*, %class.IslandBatchSizeSortPredicate** %CompareFunc.addr, align 4
  %34 = load i32, i32* %i, align 4
  %35 = load i32, i32* %hi.addr, align 4
  call void @_ZN20btAlignedObjectArrayIPN27btSimulationIslandManagerMt6IslandEE17quickSortInternalI28IslandBatchSizeSortPredicateEEvRKT_ii(%class.btAlignedObjectArray.14* %this1, %class.IslandBatchSizeSortPredicate* nonnull align 1 dereferenceable(1) %33, i32 %34, i32 %35)
  br label %if.end18

if.end18:                                         ; preds = %if.then17, %if.end15
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK28IslandBatchSizeSortPredicateclEPKN27btSimulationIslandManagerMt6IslandES3_(%class.IslandBatchSizeSortPredicate* %this, %"struct.btSimulationIslandManagerMt::Island"* %lhs, %"struct.btSimulationIslandManagerMt::Island"* %rhs) #2 comdat {
entry:
  %this.addr = alloca %class.IslandBatchSizeSortPredicate*, align 4
  %lhs.addr = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %rhs.addr = alloca %"struct.btSimulationIslandManagerMt::Island"*, align 4
  %lCost = alloca i32, align 4
  %rCost = alloca i32, align 4
  store %class.IslandBatchSizeSortPredicate* %this, %class.IslandBatchSizeSortPredicate** %this.addr, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %lhs, %"struct.btSimulationIslandManagerMt::Island"** %lhs.addr, align 4
  store %"struct.btSimulationIslandManagerMt::Island"* %rhs, %"struct.btSimulationIslandManagerMt::Island"** %rhs.addr, align 4
  %this1 = load %class.IslandBatchSizeSortPredicate*, %class.IslandBatchSizeSortPredicate** %this.addr, align 4
  %0 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %lhs.addr, align 4
  %call = call i32 @_Z13calcBatchCostPKN27btSimulationIslandManagerMt6IslandE(%"struct.btSimulationIslandManagerMt::Island"* %0)
  store i32 %call, i32* %lCost, align 4
  %1 = load %"struct.btSimulationIslandManagerMt::Island"*, %"struct.btSimulationIslandManagerMt::Island"** %rhs.addr, align 4
  %call2 = call i32 @_Z13calcBatchCostPKN27btSimulationIslandManagerMt6IslandE(%"struct.btSimulationIslandManagerMt::Island"* %1)
  store i32 %call2, i32* %rCost, align 4
  %2 = load i32, i32* %lCost, align 4
  %3 = load i32, i32* %rCost, align 4
  %cmp = icmp sgt i32 %2, %3
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray.0* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.1* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray.0* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.0*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.0* %this, %class.btAlignedObjectArray.0** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.0*, %class.btAlignedObjectArray.0** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  %5 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.0, %class.btAlignedObjectArray.0* %this1, i32 0, i32 4
  %7 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %7, i32 %8
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4
  store %class.btPersistentManifold* %9, %class.btPersistentManifold** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.1* %this, i32 %n, %class.btPersistentManifold*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.1*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator.1* %this, %class.btAlignedAllocator.1** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.1*, %class.btAlignedAllocator.1** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi(%class.btAlignedObjectArray.17* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btTypedConstraint** @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.18* %m_allocator, i32 %1, %class.btTypedConstraint*** null)
  %2 = bitcast %class.btTypedConstraint** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_(%class.btAlignedObjectArray.17* %this, i32 %start, i32 %end, %class.btTypedConstraint** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray.17*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btTypedConstraint**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray.17* %this, %class.btAlignedObjectArray.17** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btTypedConstraint** %dest, %class.btTypedConstraint*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray.17*, %class.btAlignedObjectArray.17** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %3, i32 %4
  %5 = bitcast %class.btTypedConstraint** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btTypedConstraint**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray.17, %class.btAlignedObjectArray.17* %this1, i32 0, i32 4
  %7 = load %class.btTypedConstraint**, %class.btTypedConstraint*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btTypedConstraint*, %class.btTypedConstraint** %7, i32 %8
  %9 = load %class.btTypedConstraint*, %class.btTypedConstraint** %arrayidx2, align 4
  store %class.btTypedConstraint* %9, %class.btTypedConstraint** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btTypedConstraint** @_ZN18btAlignedAllocatorIP17btTypedConstraintLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator.18* %this, i32 %n, %class.btTypedConstraint*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator.18*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btTypedConstraint***, align 4
  store %class.btAlignedAllocator.18* %this, %class.btAlignedAllocator.18** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btTypedConstraint*** %hint, %class.btTypedConstraint**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator.18*, %class.btAlignedAllocator.18** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btTypedConstraint**
  ret %class.btTypedConstraint** %1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btSimulationIslandManagerMt.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { nobuiltin allocsize(0) "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #7 = { argmemonly nounwind willreturn writeonly }
attributes #8 = { nounwind }
attributes #9 = { builtin nounwind }
attributes #10 = { builtin allocsize(0) }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
