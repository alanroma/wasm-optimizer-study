; ModuleID = '/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btCollisionDispatcher.cpp'
source_filename = "/data/Code/wasm-optimizer-study/Projects/blend4web/Source/uranium/bullet/src/BulletCollision/CollisionDispatch/btCollisionDispatcher.cpp"
target datalayout = "e-m:e-p:32:32-i64:64-n32:64-S128"
target triple = "wasm32-unknown-emscripten"

%struct.btInfMaskConverter = type { %union.anon }
%union.anon = type { float }
%class.btCollisionDispatcher = type { %class.btDispatcher, i32, %class.btAlignedObjectArray, %class.btManifoldResult, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)*, %class.btPoolAllocator*, %class.btPoolAllocator*, [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], %class.btCollisionConfiguration* }
%class.btDispatcher = type { i32 (...)** }
%class.btAlignedObjectArray = type <{ %class.btAlignedAllocator, [3 x i8], i32, i32, %class.btPersistentManifold**, i8, [3 x i8] }>
%class.btAlignedAllocator = type { i8 }
%class.btPersistentManifold = type { %struct.btTypedObject, [4 x %class.btManifoldPoint], %class.btCollisionObject*, %class.btCollisionObject*, i32, float, float, i32, i32, i32 }
%struct.btTypedObject = type { i32 }
%class.btManifoldPoint = type { %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, %class.btVector3, float, float, float, float, float, i32, i32, i32, i32, i8*, i32, float, float, float, float, float, %union.anon.0, %union.anon.1, float, i32, %class.btVector3, %class.btVector3 }
%union.anon.0 = type { float }
%union.anon.1 = type { float }
%class.btVector3 = type { [4 x float] }
%class.btCollisionObject = type { i32 (...)**, %class.btTransform, %class.btTransform, %class.btVector3, %class.btVector3, %class.btVector3, i32, float, %struct.btBroadphaseProxy*, %class.btCollisionShape*, i8*, %class.btCollisionShape*, i32, i32, i32, i32, i32, float, float, float, float, float, float, float, i32, i8*, i32, i32, float, float, float, i32, %class.btAlignedObjectArray.2, i32, %class.btVector3 }
%class.btTransform = type { %class.btMatrix3x3, %class.btVector3 }
%class.btMatrix3x3 = type { [3 x %class.btVector3] }
%struct.btBroadphaseProxy = type { i8*, i32, i32, i32, %class.btVector3, %class.btVector3 }
%class.btCollisionShape = type { i32 (...)**, i32, i8*, i32 }
%class.btAlignedObjectArray.2 = type <{ %class.btAlignedAllocator.3, [3 x i8], i32, i32, %class.btCollisionObject**, i8, [3 x i8] }>
%class.btAlignedAllocator.3 = type { i8 }
%class.btManifoldResult = type { %"struct.btDiscreteCollisionDetectorInterface::Result", %class.btPersistentManifold*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, i32, i32, i32, i32, float }
%"struct.btDiscreteCollisionDetectorInterface::Result" = type { i32 (...)** }
%struct.btCollisionObjectWrapper = type { %struct.btCollisionObjectWrapper*, %class.btCollisionShape*, %class.btCollisionObject*, %class.btTransform*, i32, i32 }
%struct.btBroadphasePair = type { %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy*, %class.btCollisionAlgorithm*, %union.anon.6 }
%class.btCollisionAlgorithm = type { i32 (...)**, %class.btDispatcher* }
%union.anon.6 = type { i8* }
%struct.btDispatcherInfo = type { float, i32, i32, float, i8, %class.btIDebugDraw*, i8, i8, i8, float, i8, float }
%class.btIDebugDraw = type opaque
%class.btPoolAllocator = type { i32, i32, i32, i8*, i8*, %class.btSpinMutex }
%class.btSpinMutex = type { i32 }
%struct.btCollisionAlgorithmCreateFunc = type <{ i32 (...)**, i8, [3 x i8] }>
%class.btCollisionConfiguration = type { i32 (...)** }
%struct.btCollisionAlgorithmConstructionInfo = type { %class.btDispatcher*, %class.btPersistentManifold* }
%class.btOverlappingPairCache = type { %class.btOverlappingPairCallback }
%class.btOverlappingPairCallback = type { i32 (...)** }
%class.btCollisionPairCallback = type { %struct.btOverlapCallback, %struct.btDispatcherInfo*, %class.btCollisionDispatcher* }
%struct.btOverlapCallback = type { i32 (...)** }

$_ZN18btInfMaskConverterC2Ei = comdat any

$_ZN12btDispatcherC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev = comdat any

$_ZN16btManifoldResultC2Ev = comdat any

$_ZN21btCollisionDispatcher15setNearCallbackEPFvR16btBroadphasePairRS_RK16btDispatcherInfoE = comdat any

$_ZN16btManifoldResultD2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev = comdat any

$_Z5btMinIfERKT_S2_S2_ = comdat any

$_ZNK17btCollisionObject17getCollisionShapeEv = comdat any

$_ZNK17btCollisionObject29getContactProcessingThresholdEv = comdat any

$_ZN15btPoolAllocator8allocateEi = comdat any

$_ZN20btPersistentManifoldnwEmPv = comdat any

$_ZN20btPersistentManifoldC2EPK17btCollisionObjectS2_iff = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_ = comdat any

$_ZN20btPersistentManifold13clearManifoldEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4swapEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8pop_backEv = comdat any

$_ZN15btPoolAllocator8validPtrEPv = comdat any

$_ZN15btPoolAllocator10freeMemoryEPv = comdat any

$_ZN36btCollisionAlgorithmConstructionInfoC2Ev = comdat any

$_ZNK24btCollisionObjectWrapper17getCollisionShapeEv = comdat any

$_ZNK16btCollisionShape12getShapeTypeEv = comdat any

$_ZNK17btCollisionObject18hasContactResponseEv = comdat any

$_ZNK17btCollisionObject25isStaticOrKinematicObjectEv = comdat any

$_ZNK17btCollisionObject8isActiveEv = comdat any

$_ZNK17btCollisionObject16checkCollideWithEPKS_ = comdat any

$_ZN23btCollisionPairCallbackC2ERK16btDispatcherInfoP21btCollisionDispatcher = comdat any

$_ZN23btCollisionPairCallbackD2Ev = comdat any

$_ZN17btCollisionObject17getCollisionShapeEv = comdat any

$_ZN17btCollisionObject17getWorldTransformEv = comdat any

$_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii = comdat any

$_ZNK21btCollisionDispatcher15getNumManifoldsEv = comdat any

$_ZN21btCollisionDispatcher26getManifoldByIndexInternalEi = comdat any

$_ZN21btCollisionDispatcher26getInternalManifoldPointerEv = comdat any

$_ZN21btCollisionDispatcher23getInternalManifoldPoolEv = comdat any

$_ZNK21btCollisionDispatcher23getInternalManifoldPoolEv = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev = comdat any

$_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev = comdat any

$_Z11btMutexLockP11btSpinMutex = comdat any

$_Z13btMutexUnlockP11btSpinMutex = comdat any

$_ZN13btTypedObjectC2Ei = comdat any

$_ZN15btManifoldPointC2Ev = comdat any

$_ZN9btVector3C2Ev = comdat any

$_ZNK17btCollisionObject18getActivationStateEv = comdat any

$_ZN17btOverlapCallbackC2Ev = comdat any

$_ZN23btCollisionPairCallbackD0Ev = comdat any

$_ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair = comdat any

$_ZN17btOverlapCallbackD2Ev = comdat any

$_ZN17btOverlapCallbackD0Ev = comdat any

$_ZNK21btCollisionDispatcher15getNearCallbackEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_ = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi = comdat any

$_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi = comdat any

$_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_ = comdat any

$_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_ = comdat any

$_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = comdat any

$_ZTV23btCollisionPairCallback = comdat any

$_ZTS23btCollisionPairCallback = comdat any

$_ZTS17btOverlapCallback = comdat any

$_ZTI17btOverlapCallback = comdat any

$_ZTI23btCollisionPairCallback = comdat any

$_ZTV17btOverlapCallback = comdat any

@_ZL14btInfinityMask = internal global %struct.btInfMaskConverter zeroinitializer, align 4
@gNumManifold = hidden global i32 0, align 4
@_ZTV21btCollisionDispatcher = hidden unnamed_addr constant { [18 x i8*] } { [18 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI21btCollisionDispatcher to i8*), i8* bitcast (%class.btCollisionDispatcher* (%class.btCollisionDispatcher*)* @_ZN21btCollisionDispatcherD1Ev to i8*), i8* bitcast (void (%class.btCollisionDispatcher*)* @_ZN21btCollisionDispatcherD0Ev to i8*), i8* bitcast (%class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)* @_ZN21btCollisionDispatcher13findAlgorithmEPK24btCollisionObjectWrapperS2_P20btPersistentManifold22ebtDispatcherQueryType to i8*), i8* bitcast (%class.btPersistentManifold* (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)* @_ZN21btCollisionDispatcher14getNewManifoldEPK17btCollisionObjectS2_ to i8*), i8* bitcast (void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)* @_ZN21btCollisionDispatcher15releaseManifoldEP20btPersistentManifold to i8*), i8* bitcast (void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)* @_ZN21btCollisionDispatcher13clearManifoldEP20btPersistentManifold to i8*), i8* bitcast (i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)* @_ZN21btCollisionDispatcher14needsCollisionEPK17btCollisionObjectS2_ to i8*), i8* bitcast (i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)* @_ZN21btCollisionDispatcher13needsResponseEPK17btCollisionObjectS2_ to i8*), i8* bitcast (void (%class.btCollisionDispatcher*, %class.btOverlappingPairCache*, %struct.btDispatcherInfo*, %class.btDispatcher*)* @_ZN21btCollisionDispatcher25dispatchAllCollisionPairsEP22btOverlappingPairCacheRK16btDispatcherInfoP12btDispatcher to i8*), i8* bitcast (i32 (%class.btCollisionDispatcher*)* @_ZNK21btCollisionDispatcher15getNumManifoldsEv to i8*), i8* bitcast (%class.btPersistentManifold* (%class.btCollisionDispatcher*, i32)* @_ZN21btCollisionDispatcher26getManifoldByIndexInternalEi to i8*), i8* bitcast (%class.btPersistentManifold** (%class.btCollisionDispatcher*)* @_ZN21btCollisionDispatcher26getInternalManifoldPointerEv to i8*), i8* bitcast (%class.btPoolAllocator* (%class.btCollisionDispatcher*)* @_ZN21btCollisionDispatcher23getInternalManifoldPoolEv to i8*), i8* bitcast (%class.btPoolAllocator* (%class.btCollisionDispatcher*)* @_ZNK21btCollisionDispatcher23getInternalManifoldPoolEv to i8*), i8* bitcast (i8* (%class.btCollisionDispatcher*, i32)* @_ZN21btCollisionDispatcher26allocateCollisionAlgorithmEi to i8*), i8* bitcast (void (%class.btCollisionDispatcher*, i8*)* @_ZN21btCollisionDispatcher22freeCollisionAlgorithmEPv to i8*)] }, align 4
@gContactBreakingThreshold = external global float, align 4
@_ZTVN10__cxxabiv120__si_class_type_infoE = external global i8*
@_ZTS21btCollisionDispatcher = hidden constant [24 x i8] c"21btCollisionDispatcher\00", align 1
@_ZTI12btDispatcher = external constant i8*
@_ZTI21btCollisionDispatcher = hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([24 x i8], [24 x i8]* @_ZTS21btCollisionDispatcher, i32 0, i32 0), i8* bitcast (i8** @_ZTI12btDispatcher to i8*) }, align 4
@_ZTV12btDispatcher = external unnamed_addr constant { [18 x i8*] }, align 4
@_ZTV16btManifoldResult = external unnamed_addr constant { [7 x i8*] }, align 4
@_ZTVN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden unnamed_addr constant { [7 x i8*] } { [7 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTIN36btDiscreteCollisionDetectorInterface6ResultE to i8*), i8* bitcast (%"struct.btDiscreteCollisionDetectorInterface::Result"* (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev to i8*), i8* bitcast (void (%"struct.btDiscreteCollisionDetectorInterface::Result"*)* @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@_ZTVN10__cxxabiv117__class_type_infoE = external global i8*
@_ZTSN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant [48 x i8] c"N36btDiscreteCollisionDetectorInterface6ResultE\00", comdat, align 1
@_ZTIN36btDiscreteCollisionDetectorInterface6ResultE = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([48 x i8], [48 x i8]* @_ZTSN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, i32 0) }, comdat, align 4
@_ZTV23btCollisionPairCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8*, i8* }* @_ZTI23btCollisionPairCallback to i8*), i8* bitcast (%class.btCollisionPairCallback* (%class.btCollisionPairCallback*)* @_ZN23btCollisionPairCallbackD2Ev to i8*), i8* bitcast (void (%class.btCollisionPairCallback*)* @_ZN23btCollisionPairCallbackD0Ev to i8*), i8* bitcast (i1 (%class.btCollisionPairCallback*, %struct.btBroadphasePair*)* @_ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair to i8*)] }, comdat, align 4
@_ZTS23btCollisionPairCallback = linkonce_odr hidden constant [26 x i8] c"23btCollisionPairCallback\00", comdat, align 1
@_ZTS17btOverlapCallback = linkonce_odr hidden constant [20 x i8] c"17btOverlapCallback\00", comdat, align 1
@_ZTI17btOverlapCallback = linkonce_odr hidden constant { i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv117__class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([20 x i8], [20 x i8]* @_ZTS17btOverlapCallback, i32 0, i32 0) }, comdat, align 4
@_ZTI23btCollisionPairCallback = linkonce_odr hidden constant { i8*, i8*, i8* } { i8* bitcast (i8** getelementptr inbounds (i8*, i8** @_ZTVN10__cxxabiv120__si_class_type_infoE, i32 2) to i8*), i8* getelementptr inbounds ([26 x i8], [26 x i8]* @_ZTS23btCollisionPairCallback, i32 0, i32 0), i8* bitcast ({ i8*, i8* }* @_ZTI17btOverlapCallback to i8*) }, comdat, align 4
@_ZTV17btOverlapCallback = linkonce_odr hidden unnamed_addr constant { [5 x i8*] } { [5 x i8*] [i8* null, i8* bitcast ({ i8*, i8* }* @_ZTI17btOverlapCallback to i8*), i8* bitcast (%struct.btOverlapCallback* (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD2Ev to i8*), i8* bitcast (void (%struct.btOverlapCallback*)* @_ZN17btOverlapCallbackD0Ev to i8*), i8* bitcast (void ()* @__cxa_pure_virtual to i8*)] }, comdat, align 4
@llvm.global_ctors = appending global [1 x { i32, void ()*, i8* }] [{ i32, void ()*, i8* } { i32 65535, void ()* @_GLOBAL__sub_I_btCollisionDispatcher.cpp, i8* null }]

@_ZN21btCollisionDispatcherC1EP24btCollisionConfiguration = hidden unnamed_addr alias %class.btCollisionDispatcher* (%class.btCollisionDispatcher*, %class.btCollisionConfiguration*), %class.btCollisionDispatcher* (%class.btCollisionDispatcher*, %class.btCollisionConfiguration*)* @_ZN21btCollisionDispatcherC2EP24btCollisionConfiguration
@_ZN21btCollisionDispatcherD1Ev = hidden unnamed_addr alias %class.btCollisionDispatcher* (%class.btCollisionDispatcher*), %class.btCollisionDispatcher* (%class.btCollisionDispatcher*)* @_ZN21btCollisionDispatcherD2Ev

; Function Attrs: noinline
define internal void @__cxx_global_var_init() #0 {
entry:
  %call = call %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* @_ZL14btInfinityMask, i32 2139095040)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btInfMaskConverter* @_ZN18btInfMaskConverterC2Ei(%struct.btInfMaskConverter* returned %this, i32 %mask) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btInfMaskConverter*, align 4
  %mask.addr = alloca i32, align 4
  store %struct.btInfMaskConverter* %this, %struct.btInfMaskConverter** %this.addr, align 4
  store i32 %mask, i32* %mask.addr, align 4
  %this1 = load %struct.btInfMaskConverter*, %struct.btInfMaskConverter** %this.addr, align 4
  %0 = getelementptr inbounds %struct.btInfMaskConverter, %struct.btInfMaskConverter* %this1, i32 0, i32 0
  %intmask = bitcast %union.anon* %0 to i32*
  %1 = load i32, i32* %mask.addr, align 4
  store i32 %1, i32* %intmask, align 4
  ret %struct.btInfMaskConverter* %this1
}

; Function Attrs: noinline optnone
define hidden %class.btCollisionDispatcher* @_ZN21btCollisionDispatcherC2EP24btCollisionConfiguration(%class.btCollisionDispatcher* returned %this, %class.btCollisionConfiguration* %collisionConfiguration) unnamed_addr #2 {
entry:
  %retval = alloca %class.btCollisionDispatcher*, align 4
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %collisionConfiguration.addr = alloca %class.btCollisionConfiguration*, align 4
  %i = alloca i32, align 4
  %j = alloca i32, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  store %class.btCollisionConfiguration* %collisionConfiguration, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  store %class.btCollisionDispatcher* %this1, %class.btCollisionDispatcher** %retval, align 4
  %0 = bitcast %class.btCollisionDispatcher* %this1 to %class.btDispatcher*
  %call = call %class.btDispatcher* @_ZN12btDispatcherC2Ev(%class.btDispatcher* %0) #7
  %1 = bitcast %class.btCollisionDispatcher* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV21btCollisionDispatcher, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_dispatcherFlags = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 1
  store i32 2, i32* %m_dispatcherFlags, align 4
  %m_manifoldsPtr = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray* %m_manifoldsPtr)
  %m_defaultManifoldResult = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 3
  %call3 = call %class.btManifoldResult* @_ZN16btManifoldResultC2Ev(%class.btManifoldResult* %m_defaultManifoldResult)
  %m_collisionConfiguration = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 9
  %2 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4
  store %class.btCollisionConfiguration* %2, %class.btCollisionConfiguration** %m_collisionConfiguration, align 4
  call void @_ZN21btCollisionDispatcher15setNearCallbackEPFvR16btBroadphasePairRS_RK16btDispatcherInfoE(%class.btCollisionDispatcher* %this1, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)* @_ZN21btCollisionDispatcher19defaultNearCallbackER16btBroadphasePairRS_RK16btDispatcherInfo)
  %3 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4
  %4 = bitcast %class.btCollisionConfiguration* %3 to %class.btPoolAllocator* (%class.btCollisionConfiguration*)***
  %vtable = load %class.btPoolAllocator* (%class.btCollisionConfiguration*)**, %class.btPoolAllocator* (%class.btCollisionConfiguration*)*** %4, align 4
  %vfn = getelementptr inbounds %class.btPoolAllocator* (%class.btCollisionConfiguration*)*, %class.btPoolAllocator* (%class.btCollisionConfiguration*)** %vtable, i64 3
  %5 = load %class.btPoolAllocator* (%class.btCollisionConfiguration*)*, %class.btPoolAllocator* (%class.btCollisionConfiguration*)** %vfn, align 4
  %call4 = call %class.btPoolAllocator* %5(%class.btCollisionConfiguration* %3)
  %m_collisionAlgorithmPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 5
  store %class.btPoolAllocator* %call4, %class.btPoolAllocator** %m_collisionAlgorithmPoolAllocator, align 4
  %6 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %collisionConfiguration.addr, align 4
  %7 = bitcast %class.btCollisionConfiguration* %6 to %class.btPoolAllocator* (%class.btCollisionConfiguration*)***
  %vtable5 = load %class.btPoolAllocator* (%class.btCollisionConfiguration*)**, %class.btPoolAllocator* (%class.btCollisionConfiguration*)*** %7, align 4
  %vfn6 = getelementptr inbounds %class.btPoolAllocator* (%class.btCollisionConfiguration*)*, %class.btPoolAllocator* (%class.btCollisionConfiguration*)** %vtable5, i64 2
  %8 = load %class.btPoolAllocator* (%class.btCollisionConfiguration*)*, %class.btPoolAllocator* (%class.btCollisionConfiguration*)** %vfn6, align 4
  %call7 = call %class.btPoolAllocator* %8(%class.btCollisionConfiguration* %6)
  %m_persistentManifoldPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 6
  store %class.btPoolAllocator* %call7, %class.btPoolAllocator** %m_persistentManifoldPoolAllocator, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc22, %entry
  %9 = load i32, i32* %i, align 4
  %cmp = icmp slt i32 %9, 36
  br i1 %cmp, label %for.body, label %for.end24

for.body:                                         ; preds = %for.cond
  store i32 0, i32* %j, align 4
  br label %for.cond8

for.cond8:                                        ; preds = %for.inc, %for.body
  %10 = load i32, i32* %j, align 4
  %cmp9 = icmp slt i32 %10, 36
  br i1 %cmp9, label %for.body10, label %for.end

for.body10:                                       ; preds = %for.cond8
  %m_collisionConfiguration11 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 9
  %11 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %m_collisionConfiguration11, align 4
  %12 = load i32, i32* %i, align 4
  %13 = load i32, i32* %j, align 4
  %14 = bitcast %class.btCollisionConfiguration* %11 to %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)***
  %vtable12 = load %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)**, %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)*** %14, align 4
  %vfn13 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)*, %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)** %vtable12, i64 4
  %15 = load %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)*, %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)** %vfn13, align 4
  %call14 = call %struct.btCollisionAlgorithmCreateFunc* %15(%class.btCollisionConfiguration* %11, i32 %12, i32 %13)
  %m_doubleDispatchContactPoints = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 7
  %16 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]]* %m_doubleDispatchContactPoints, i32 0, i32 %16
  %17 = load i32, i32* %j, align 4
  %arrayidx15 = getelementptr inbounds [36 x %struct.btCollisionAlgorithmCreateFunc*], [36 x %struct.btCollisionAlgorithmCreateFunc*]* %arrayidx, i32 0, i32 %17
  store %struct.btCollisionAlgorithmCreateFunc* %call14, %struct.btCollisionAlgorithmCreateFunc** %arrayidx15, align 4
  %m_collisionConfiguration16 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 9
  %18 = load %class.btCollisionConfiguration*, %class.btCollisionConfiguration** %m_collisionConfiguration16, align 4
  %19 = load i32, i32* %i, align 4
  %20 = load i32, i32* %j, align 4
  %21 = bitcast %class.btCollisionConfiguration* %18 to %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)***
  %vtable17 = load %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)**, %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)*** %21, align 4
  %vfn18 = getelementptr inbounds %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)*, %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)** %vtable17, i64 5
  %22 = load %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)*, %struct.btCollisionAlgorithmCreateFunc* (%class.btCollisionConfiguration*, i32, i32)** %vfn18, align 4
  %call19 = call %struct.btCollisionAlgorithmCreateFunc* %22(%class.btCollisionConfiguration* %18, i32 %19, i32 %20)
  %m_doubleDispatchClosestPoints = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 8
  %23 = load i32, i32* %i, align 4
  %arrayidx20 = getelementptr inbounds [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]]* %m_doubleDispatchClosestPoints, i32 0, i32 %23
  %24 = load i32, i32* %j, align 4
  %arrayidx21 = getelementptr inbounds [36 x %struct.btCollisionAlgorithmCreateFunc*], [36 x %struct.btCollisionAlgorithmCreateFunc*]* %arrayidx20, i32 0, i32 %24
  store %struct.btCollisionAlgorithmCreateFunc* %call19, %struct.btCollisionAlgorithmCreateFunc** %arrayidx21, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body10
  %25 = load i32, i32* %j, align 4
  %inc = add nsw i32 %25, 1
  store i32 %inc, i32* %j, align 4
  br label %for.cond8

for.end:                                          ; preds = %for.cond8
  br label %for.inc22

for.inc22:                                        ; preds = %for.end
  %26 = load i32, i32* %i, align 4
  %inc23 = add nsw i32 %26, 1
  store i32 %inc23, i32* %i, align 4
  br label %for.cond

for.end24:                                        ; preds = %for.cond
  %27 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %retval, align 4
  ret %class.btCollisionDispatcher* %27
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btDispatcher* @_ZN12btDispatcherC2Ev(%class.btDispatcher* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btDispatcher*, align 4
  store %class.btDispatcher* %this, %class.btDispatcher** %this.addr, align 4
  %this1 = load %class.btDispatcher*, %class.btDispatcher** %this.addr, align 4
  %0 = bitcast %class.btDispatcher* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV12btDispatcher, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %class.btDispatcher* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEC2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %call = call %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator* %m_allocator)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btManifoldResult* @_ZN16btManifoldResultC2Ev(%class.btManifoldResult* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = bitcast %class.btManifoldResult* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #7
  %1 = bitcast %class.btManifoldResult* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTV16btManifoldResult, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_closestPointDistanceThreshold = getelementptr inbounds %class.btManifoldResult, %class.btManifoldResult* %this1, i32 0, i32 8
  store float 0.000000e+00, float* %m_closestPointDistanceThreshold, align 4
  ret %class.btManifoldResult* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN21btCollisionDispatcher15setNearCallbackEPFvR16btBroadphasePairRS_RK16btDispatcherInfoE(%class.btCollisionDispatcher* %this, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)* %nearCallback) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %nearCallback.addr = alloca void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  store void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)* %nearCallback, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)** %nearCallback.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = load void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)*, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)** %nearCallback.addr, align 4
  %m_nearCallback = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 4
  store void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)* %0, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)** %m_nearCallback, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btCollisionDispatcher19defaultNearCallbackER16btBroadphasePairRS_RK16btDispatcherInfo(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %collisionPair, %class.btCollisionDispatcher* nonnull align 4 dereferenceable(10448) %dispatcher, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo) #2 {
entry:
  %collisionPair.addr = alloca %struct.btBroadphasePair*, align 4
  %dispatcher.addr = alloca %class.btCollisionDispatcher*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %colObj0 = alloca %class.btCollisionObject*, align 4
  %colObj1 = alloca %class.btCollisionObject*, align 4
  %obj0Wrap = alloca %struct.btCollisionObjectWrapper, align 4
  %obj1Wrap = alloca %struct.btCollisionObjectWrapper, align 4
  %contactPointResult = alloca %class.btManifoldResult, align 4
  %toi = alloca float, align 4
  store %struct.btBroadphasePair* %collisionPair, %struct.btBroadphasePair** %collisionPair.addr, align 4
  store %class.btCollisionDispatcher* %dispatcher, %class.btCollisionDispatcher** %dispatcher.addr, align 4
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %0 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair.addr, align 4
  %m_pProxy0 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %0, i32 0, i32 0
  %1 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy0, align 4
  %m_clientObject = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %1, i32 0, i32 0
  %2 = load i8*, i8** %m_clientObject, align 4
  %3 = bitcast i8* %2 to %class.btCollisionObject*
  store %class.btCollisionObject* %3, %class.btCollisionObject** %colObj0, align 4
  %4 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair.addr, align 4
  %m_pProxy1 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %4, i32 0, i32 1
  %5 = load %struct.btBroadphaseProxy*, %struct.btBroadphaseProxy** %m_pProxy1, align 4
  %m_clientObject1 = getelementptr inbounds %struct.btBroadphaseProxy, %struct.btBroadphaseProxy* %5, i32 0, i32 0
  %6 = load i8*, i8** %m_clientObject1, align 4
  %7 = bitcast i8* %6 to %class.btCollisionObject*
  store %class.btCollisionObject* %7, %class.btCollisionObject** %colObj1, align 4
  %8 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %dispatcher.addr, align 4
  %9 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %10 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %11 = bitcast %class.btCollisionDispatcher* %8 to i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable = load i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*** %11, align 4
  %vfn = getelementptr inbounds i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vtable, i64 6
  %12 = load i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btCollisionDispatcher*, %class.btCollisionObject*, %class.btCollisionObject*)** %vfn, align 4
  %call = call zeroext i1 %12(%class.btCollisionDispatcher* %8, %class.btCollisionObject* %9, %class.btCollisionObject* %10)
  br i1 %call, label %if.then, label %if.end32

if.then:                                          ; preds = %entry
  %13 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call2 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %13)
  %14 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %15 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %call3 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %15)
  %call4 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper* null, %class.btCollisionShape* %call2, %class.btCollisionObject* %14, %class.btTransform* nonnull align 4 dereferenceable(64) %call3, i32 -1, i32 -1)
  %16 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call5 = call %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %16)
  %17 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %18 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %call6 = call nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %18)
  %call7 = call %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* %obj1Wrap, %struct.btCollisionObjectWrapper* null, %class.btCollisionShape* %call5, %class.btCollisionObject* %17, %class.btTransform* nonnull align 4 dereferenceable(64) %call6, i32 -1, i32 -1)
  %19 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair.addr, align 4
  %m_algorithm = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %19, i32 0, i32 2
  %20 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm, align 4
  %tobool = icmp ne %class.btCollisionAlgorithm* %20, null
  br i1 %tobool, label %if.end, label %if.then8

if.then8:                                         ; preds = %if.then
  %21 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %dispatcher.addr, align 4
  %22 = bitcast %class.btCollisionDispatcher* %21 to %class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)***
  %vtable9 = load %class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)**, %class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*** %22, align 4
  %vfn10 = getelementptr inbounds %class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*, %class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)** %vtable9, i64 2
  %23 = load %class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)*, %class.btCollisionAlgorithm* (%class.btCollisionDispatcher*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %class.btPersistentManifold*, i32)** %vfn10, align 4
  %call11 = call %class.btCollisionAlgorithm* %23(%class.btCollisionDispatcher* %21, %struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper* %obj1Wrap, %class.btPersistentManifold* null, i32 1)
  %24 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair.addr, align 4
  %m_algorithm12 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %24, i32 0, i32 2
  store %class.btCollisionAlgorithm* %call11, %class.btCollisionAlgorithm** %m_algorithm12, align 4
  br label %if.end

if.end:                                           ; preds = %if.then8, %if.then
  %25 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair.addr, align 4
  %m_algorithm13 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %25, i32 0, i32 2
  %26 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm13, align 4
  %tobool14 = icmp ne %class.btCollisionAlgorithm* %26, null
  br i1 %tobool14, label %if.then15, label %if.end31

if.then15:                                        ; preds = %if.end
  %call16 = call %class.btManifoldResult* @_ZN16btManifoldResultC1EPK24btCollisionObjectWrapperS2_(%class.btManifoldResult* %contactPointResult, %struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper* %obj1Wrap)
  %27 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %m_dispatchFunc = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %27, i32 0, i32 2
  %28 = load i32, i32* %m_dispatchFunc, align 4
  %cmp = icmp eq i32 %28, 1
  br i1 %cmp, label %if.then17, label %if.else

if.then17:                                        ; preds = %if.then15
  %29 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair.addr, align 4
  %m_algorithm18 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %29, i32 0, i32 2
  %30 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm18, align 4
  %31 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %32 = bitcast %class.btCollisionAlgorithm* %30 to void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)***
  %vtable19 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)**, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*** %32, align 4
  %vfn20 = getelementptr inbounds void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vtable19, i64 2
  %33 = load void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, void (%class.btCollisionAlgorithm*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vfn20, align 4
  call void %33(%class.btCollisionAlgorithm* %30, %struct.btCollisionObjectWrapper* %obj0Wrap, %struct.btCollisionObjectWrapper* %obj1Wrap, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %31, %class.btManifoldResult* %contactPointResult)
  br label %if.end29

if.else:                                          ; preds = %if.then15
  %34 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %collisionPair.addr, align 4
  %m_algorithm21 = getelementptr inbounds %struct.btBroadphasePair, %struct.btBroadphasePair* %34, i32 0, i32 2
  %35 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %m_algorithm21, align 4
  %36 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj0, align 4
  %37 = load %class.btCollisionObject*, %class.btCollisionObject** %colObj1, align 4
  %38 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %39 = bitcast %class.btCollisionAlgorithm* %35 to float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)***
  %vtable22 = load float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)**, float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*** %39, align 4
  %vfn23 = getelementptr inbounds float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vtable22, i64 3
  %40 = load float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)*, float (%class.btCollisionAlgorithm*, %class.btCollisionObject*, %class.btCollisionObject*, %struct.btDispatcherInfo*, %class.btManifoldResult*)** %vfn23, align 4
  %call24 = call float %40(%class.btCollisionAlgorithm* %35, %class.btCollisionObject* %36, %class.btCollisionObject* %37, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %38, %class.btManifoldResult* %contactPointResult)
  store float %call24, float* %toi, align 4
  %41 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %m_timeOfImpact = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %41, i32 0, i32 3
  %42 = load float, float* %m_timeOfImpact, align 4
  %43 = load float, float* %toi, align 4
  %cmp25 = fcmp ogt float %42, %43
  br i1 %cmp25, label %if.then26, label %if.end28

if.then26:                                        ; preds = %if.else
  %44 = load float, float* %toi, align 4
  %45 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %m_timeOfImpact27 = getelementptr inbounds %struct.btDispatcherInfo, %struct.btDispatcherInfo* %45, i32 0, i32 3
  store float %44, float* %m_timeOfImpact27, align 4
  br label %if.end28

if.end28:                                         ; preds = %if.then26, %if.else
  br label %if.end29

if.end29:                                         ; preds = %if.end28, %if.then17
  %call30 = call %class.btManifoldResult* @_ZN16btManifoldResultD2Ev(%class.btManifoldResult* %contactPointResult) #7
  br label %if.end31

if.end31:                                         ; preds = %if.end29, %if.end
  br label %if.end32

if.end32:                                         ; preds = %if.end31, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN21btCollisionDispatcher27registerCollisionCreateFuncEiiP30btCollisionAlgorithmCreateFunc(%class.btCollisionDispatcher* %this, i32 %proxyType0, i32 %proxyType1, %struct.btCollisionAlgorithmCreateFunc* %createFunc) #1 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %proxyType0.addr = alloca i32, align 4
  %proxyType1.addr = alloca i32, align 4
  %createFunc.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  store i32 %proxyType0, i32* %proxyType0.addr, align 4
  store i32 %proxyType1, i32* %proxyType1.addr, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %createFunc, %struct.btCollisionAlgorithmCreateFunc** %createFunc.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %createFunc.addr, align 4
  %m_doubleDispatchContactPoints = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 7
  %1 = load i32, i32* %proxyType0.addr, align 4
  %arrayidx = getelementptr inbounds [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]]* %m_doubleDispatchContactPoints, i32 0, i32 %1
  %2 = load i32, i32* %proxyType1.addr, align 4
  %arrayidx2 = getelementptr inbounds [36 x %struct.btCollisionAlgorithmCreateFunc*], [36 x %struct.btCollisionAlgorithmCreateFunc*]* %arrayidx, i32 0, i32 %2
  store %struct.btCollisionAlgorithmCreateFunc* %0, %struct.btCollisionAlgorithmCreateFunc** %arrayidx2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN21btCollisionDispatcher31registerClosestPointsCreateFuncEiiP30btCollisionAlgorithmCreateFunc(%class.btCollisionDispatcher* %this, i32 %proxyType0, i32 %proxyType1, %struct.btCollisionAlgorithmCreateFunc* %createFunc) #1 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %proxyType0.addr = alloca i32, align 4
  %proxyType1.addr = alloca i32, align 4
  %createFunc.addr = alloca %struct.btCollisionAlgorithmCreateFunc*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  store i32 %proxyType0, i32* %proxyType0.addr, align 4
  store i32 %proxyType1, i32* %proxyType1.addr, align 4
  store %struct.btCollisionAlgorithmCreateFunc* %createFunc, %struct.btCollisionAlgorithmCreateFunc** %createFunc.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %createFunc.addr, align 4
  %m_doubleDispatchClosestPoints = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 8
  %1 = load i32, i32* %proxyType0.addr, align 4
  %arrayidx = getelementptr inbounds [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]]* %m_doubleDispatchClosestPoints, i32 0, i32 %1
  %2 = load i32, i32* %proxyType1.addr, align 4
  %arrayidx2 = getelementptr inbounds [36 x %struct.btCollisionAlgorithmCreateFunc*], [36 x %struct.btCollisionAlgorithmCreateFunc*]* %arrayidx, i32 0, i32 %2
  store %struct.btCollisionAlgorithmCreateFunc* %0, %struct.btCollisionAlgorithmCreateFunc** %arrayidx2, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define hidden %class.btCollisionDispatcher* @_ZN21btCollisionDispatcherD2Ev(%class.btCollisionDispatcher* returned %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = bitcast %class.btCollisionDispatcher* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [18 x i8*] }, { [18 x i8*] }* @_ZTV21btCollisionDispatcher, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  %m_defaultManifoldResult = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 3
  %call = call %class.btManifoldResult* @_ZN16btManifoldResultD2Ev(%class.btManifoldResult* %m_defaultManifoldResult) #7
  %m_manifoldsPtr = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %call2 = call %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray* %m_manifoldsPtr) #7
  %1 = bitcast %class.btCollisionDispatcher* %this1 to %class.btDispatcher*
  %call3 = call %class.btDispatcher* @_ZN12btDispatcherD2Ev(%class.btDispatcher* %1) #7
  ret %class.btCollisionDispatcher* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btManifoldResult* @_ZN16btManifoldResultD2Ev(%class.btManifoldResult* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btManifoldResult*, align 4
  store %class.btManifoldResult* %this, %class.btManifoldResult** %this.addr, align 4
  %this1 = load %class.btManifoldResult*, %class.btManifoldResult** %this.addr, align 4
  %0 = bitcast %class.btManifoldResult* %this1 to %"struct.btDiscreteCollisionDetectorInterface::Result"*
  %call = call %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %0) #7
  ret %class.btManifoldResult* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedObjectArray* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev(%class.btAlignedObjectArray* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray* %this1)
  ret %class.btAlignedObjectArray* %this1
}

; Function Attrs: nounwind
declare %class.btDispatcher* @_ZN12btDispatcherD2Ev(%class.btDispatcher* returned) unnamed_addr #3

; Function Attrs: noinline nounwind optnone
define hidden void @_ZN21btCollisionDispatcherD0Ev(%class.btCollisionDispatcher* %this) unnamed_addr #1 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %call = call %class.btCollisionDispatcher* @_ZN21btCollisionDispatcherD1Ev(%class.btCollisionDispatcher* %this1) #7
  %0 = bitcast %class.btCollisionDispatcher* %this1 to i8*
  call void @_ZdlPv(i8* %0) #8
  ret void
}

; Function Attrs: nobuiltin nounwind
declare void @_ZdlPv(i8*) #4

; Function Attrs: noinline optnone
define hidden %class.btPersistentManifold* @_ZN21btCollisionDispatcher14getNewManifoldEPK17btCollisionObjectS2_(%class.btCollisionDispatcher* %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1) unnamed_addr #2 {
entry:
  %retval = alloca %class.btPersistentManifold*, align 4
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %contactBreakingThreshold = alloca float, align 4
  %ref.tmp = alloca float, align 4
  %ref.tmp3 = alloca float, align 4
  %contactProcessingThreshold = alloca float, align 4
  %ref.tmp9 = alloca float, align 4
  %ref.tmp11 = alloca float, align 4
  %mem = alloca i8*, align 4
  %manifold = alloca %class.btPersistentManifold*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = load i32, i32* @gNumManifold, align 4
  %inc = add nsw i32 %0, 1
  store i32 %inc, i32* @gNumManifold, align 4
  %m_dispatcherFlags = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 1
  %1 = load i32, i32* %m_dispatcherFlags, align 4
  %and = and i32 %1, 2
  %tobool = icmp ne i32 %and, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4
  %call = call %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %2)
  %3 = load float, float* @gContactBreakingThreshold, align 4
  %4 = bitcast %class.btCollisionShape* %call to float (%class.btCollisionShape*, float)***
  %vtable = load float (%class.btCollisionShape*, float)**, float (%class.btCollisionShape*, float)*** %4, align 4
  %vfn = getelementptr inbounds float (%class.btCollisionShape*, float)*, float (%class.btCollisionShape*, float)** %vtable, i64 5
  %5 = load float (%class.btCollisionShape*, float)*, float (%class.btCollisionShape*, float)** %vfn, align 4
  %call2 = call float %5(%class.btCollisionShape* %call, float %3)
  store float %call2, float* %ref.tmp, align 4
  %6 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4
  %call4 = call %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %6)
  %7 = load float, float* @gContactBreakingThreshold, align 4
  %8 = bitcast %class.btCollisionShape* %call4 to float (%class.btCollisionShape*, float)***
  %vtable5 = load float (%class.btCollisionShape*, float)**, float (%class.btCollisionShape*, float)*** %8, align 4
  %vfn6 = getelementptr inbounds float (%class.btCollisionShape*, float)*, float (%class.btCollisionShape*, float)** %vtable5, i64 5
  %9 = load float (%class.btCollisionShape*, float)*, float (%class.btCollisionShape*, float)** %vfn6, align 4
  %call7 = call float %9(%class.btCollisionShape* %call4, float %7)
  store float %call7, float* %ref.tmp3, align 4
  %call8 = call nonnull align 4 dereferenceable(4) float* @_Z5btMinIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %ref.tmp, float* nonnull align 4 dereferenceable(4) %ref.tmp3)
  %10 = load float, float* %call8, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %11 = load float, float* @gContactBreakingThreshold, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi float [ %10, %cond.true ], [ %11, %cond.false ]
  store float %cond, float* %contactBreakingThreshold, align 4
  %12 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4
  %call10 = call float @_ZNK17btCollisionObject29getContactProcessingThresholdEv(%class.btCollisionObject* %12)
  store float %call10, float* %ref.tmp9, align 4
  %13 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4
  %call12 = call float @_ZNK17btCollisionObject29getContactProcessingThresholdEv(%class.btCollisionObject* %13)
  store float %call12, float* %ref.tmp11, align 4
  %call13 = call nonnull align 4 dereferenceable(4) float* @_Z5btMinIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %ref.tmp9, float* nonnull align 4 dereferenceable(4) %ref.tmp11)
  %14 = load float, float* %call13, align 4
  store float %14, float* %contactProcessingThreshold, align 4
  %m_persistentManifoldPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 6
  %15 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPoolAllocator, align 4
  %call14 = call i8* @_ZN15btPoolAllocator8allocateEi(%class.btPoolAllocator* %15, i32 804)
  store i8* %call14, i8** %mem, align 4
  %16 = load i8*, i8** %mem, align 4
  %cmp = icmp eq i8* null, %16
  br i1 %cmp, label %if.then, label %if.end20

if.then:                                          ; preds = %cond.end
  %m_dispatcherFlags15 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 1
  %17 = load i32, i32* %m_dispatcherFlags15, align 4
  %and16 = and i32 %17, 4
  %cmp17 = icmp eq i32 %and16, 0
  br i1 %cmp17, label %if.then18, label %if.else

if.then18:                                        ; preds = %if.then
  %call19 = call i8* @_Z22btAlignedAllocInternalmi(i32 804, i32 16)
  store i8* %call19, i8** %mem, align 4
  br label %if.end

if.else:                                          ; preds = %if.then
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %retval, align 4
  br label %return

if.end:                                           ; preds = %if.then18
  br label %if.end20

if.end20:                                         ; preds = %if.end, %cond.end
  %18 = load i8*, i8** %mem, align 4
  %call21 = call i8* @_ZN20btPersistentManifoldnwEmPv(i32 804, i8* %18)
  %19 = bitcast i8* %call21 to %class.btPersistentManifold*
  %20 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4
  %21 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4
  %22 = load float, float* %contactBreakingThreshold, align 4
  %23 = load float, float* %contactProcessingThreshold, align 4
  %call22 = call %class.btPersistentManifold* @_ZN20btPersistentManifoldC2EPK17btCollisionObjectS2_iff(%class.btPersistentManifold* %19, %class.btCollisionObject* %20, %class.btCollisionObject* %21, i32 0, float %22, float %23)
  store %class.btPersistentManifold* %19, %class.btPersistentManifold** %manifold, align 4
  %m_manifoldsPtr = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %call23 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %m_manifoldsPtr)
  %24 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  %m_index1a = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %24, i32 0, i32 9
  store i32 %call23, i32* %m_index1a, align 4
  %m_manifoldsPtr24 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray* %m_manifoldsPtr24, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %manifold)
  %25 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold, align 4
  store %class.btPersistentManifold* %25, %class.btPersistentManifold** %retval, align 4
  br label %return

return:                                           ; preds = %if.end20, %if.else
  %26 = load %class.btPersistentManifold*, %class.btPersistentManifold** %retval, align 4
  ret %class.btPersistentManifold* %26
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) float* @_Z5btMinIfERKT_S2_S2_(float* nonnull align 4 dereferenceable(4) %a, float* nonnull align 4 dereferenceable(4) %b) #1 comdat {
entry:
  %a.addr = alloca float*, align 4
  %b.addr = alloca float*, align 4
  store float* %a, float** %a.addr, align 4
  store float* %b, float** %b.addr, align 4
  %0 = load float*, float** %a.addr, align 4
  %1 = load float, float* %0, align 4
  %2 = load float*, float** %b.addr, align 4
  %3 = load float, float* %2, align 4
  %cmp = fcmp olt float %1, %3
  br i1 %cmp, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %4 = load float*, float** %a.addr, align 4
  br label %cond.end

cond.false:                                       ; preds = %entry
  %5 = load float*, float** %b.addr, align 4
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond-lvalue = phi float* [ %4, %cond.true ], [ %5, %cond.false ]
  ret float* %cond-lvalue
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4
  ret %class.btCollisionShape* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden float @_ZNK17btCollisionObject29getContactProcessingThresholdEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_contactProcessingThreshold = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 7
  %0 = load float, float* %m_contactProcessingThreshold, align 4
  ret float %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN15btPoolAllocator8allocateEi(%class.btPoolAllocator* %this, i32 %size) #2 comdat {
entry:
  %this.addr = alloca %class.btPoolAllocator*, align 4
  %size.addr = alloca i32, align 4
  %result = alloca i8*, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  %m_mutex = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 5
  call void @_Z11btMutexLockP11btSpinMutex(%class.btSpinMutex* %m_mutex)
  %m_firstFree = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  %0 = load i8*, i8** %m_firstFree, align 4
  store i8* %0, i8** %result, align 4
  %m_firstFree2 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  %1 = load i8*, i8** %m_firstFree2, align 4
  %cmp = icmp ne i8* null, %1
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_firstFree3 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  %2 = load i8*, i8** %m_firstFree3, align 4
  %3 = bitcast i8* %2 to i8**
  %4 = load i8*, i8** %3, align 4
  %m_firstFree4 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  store i8* %4, i8** %m_firstFree4, align 4
  %m_freeCount = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 2
  %5 = load i32, i32* %m_freeCount, align 4
  %dec = add nsw i32 %5, -1
  store i32 %dec, i32* %m_freeCount, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_mutex5 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 5
  call void @_Z13btMutexUnlockP11btSpinMutex(%class.btSpinMutex* %m_mutex5)
  %6 = load i8*, i8** %result, align 4
  ret i8* %6
}

declare i8* @_Z22btAlignedAllocInternalmi(i32, i32) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i8* @_ZN20btPersistentManifoldnwEmPv(i32 %0, i8* %ptr) #1 comdat {
entry:
  %.addr = alloca i32, align 4
  %ptr.addr = alloca i8*, align 4
  store i32 %0, i32* %.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  ret i8* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPersistentManifold* @_ZN20btPersistentManifoldC2EPK17btCollisionObjectS2_iff(%class.btPersistentManifold* returned %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1, i32 %0, float %contactBreakingThreshold, float %contactProcessingThreshold) unnamed_addr #2 comdat {
entry:
  %retval = alloca %class.btPersistentManifold*, align 4
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %.addr = alloca i32, align 4
  %contactBreakingThreshold.addr = alloca float, align 4
  %contactProcessingThreshold.addr = alloca float, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4
  store i32 %0, i32* %.addr, align 4
  store float %contactBreakingThreshold, float* %contactBreakingThreshold.addr, align 4
  store float %contactProcessingThreshold, float* %contactProcessingThreshold.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  store %class.btPersistentManifold* %this1, %class.btPersistentManifold** %retval, align 4
  %1 = bitcast %class.btPersistentManifold* %this1 to %struct.btTypedObject*
  %call = call %struct.btTypedObject* @_ZN13btTypedObjectC2Ei(%struct.btTypedObject* %1, i32 1025)
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %array.begin = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 0
  %arrayctor.end = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %array.begin, i32 4
  br label %arrayctor.loop

arrayctor.loop:                                   ; preds = %arrayctor.loop, %entry
  %arrayctor.cur = phi %class.btManifoldPoint* [ %array.begin, %entry ], [ %arrayctor.next, %arrayctor.loop ]
  %call2 = call %class.btManifoldPoint* @_ZN15btManifoldPointC2Ev(%class.btManifoldPoint* %arrayctor.cur)
  %arrayctor.next = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %arrayctor.cur, i32 1
  %arrayctor.done = icmp eq %class.btManifoldPoint* %arrayctor.next, %arrayctor.end
  br i1 %arrayctor.done, label %arrayctor.cont, label %arrayctor.loop

arrayctor.cont:                                   ; preds = %arrayctor.loop
  %m_body0 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 2
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4
  store %class.btCollisionObject* %2, %class.btCollisionObject** %m_body0, align 4
  %m_body1 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 3
  %3 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4
  store %class.btCollisionObject* %3, %class.btCollisionObject** %m_body1, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  store i32 0, i32* %m_cachedPoints, align 4
  %m_contactBreakingThreshold = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 5
  %4 = load float, float* %contactBreakingThreshold.addr, align 4
  store float %4, float* %m_contactBreakingThreshold, align 4
  %m_contactProcessingThreshold = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 6
  %5 = load float, float* %contactProcessingThreshold.addr, align 4
  store float %5, float* %m_contactProcessingThreshold, align 4
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %retval, align 4
  ret %class.btPersistentManifold* %6
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9push_backERKS1_(%class.btAlignedObjectArray* %this, %class.btPersistentManifold** nonnull align 4 dereferenceable(4) %_Val) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Val.addr = alloca %class.btPersistentManifold**, align 4
  %sz = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store %class.btPersistentManifold** %_Val, %class.btPersistentManifold*** %_Val.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this1)
  store i32 %call, i32* %sz, align 4
  %0 = load i32, i32* %sz, align 4
  %call2 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray* %this1)
  %cmp = icmp eq i32 %0, %call2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this1)
  %call4 = call i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray* %this1, i32 %call3)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray* %this1, i32 %call4)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %1, i32 %2
  %3 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %4 = bitcast i8* %3 to %class.btPersistentManifold**
  %5 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %_Val.addr, align 4
  %6 = load %class.btPersistentManifold*, %class.btPersistentManifold** %5, align 4
  store %class.btPersistentManifold* %6, %class.btPersistentManifold** %4, align 4
  %m_size5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %7 = load i32, i32* %m_size5, align 4
  %inc = add nsw i32 %7, 1
  store i32 %inc, i32* %m_size5, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btCollisionDispatcher13clearManifoldEP20btPersistentManifold(%class.btCollisionDispatcher* %this, %class.btPersistentManifold* %manifold) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %manifold.addr = alloca %class.btPersistentManifold*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  store %class.btPersistentManifold* %manifold, %class.btPersistentManifold** %manifold.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  call void @_ZN20btPersistentManifold13clearManifoldEv(%class.btPersistentManifold* %0)
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btPersistentManifold13clearManifoldEv(%class.btPersistentManifold* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btPersistentManifold*, align 4
  %i = alloca i32, align 4
  store %class.btPersistentManifold* %this, %class.btPersistentManifold** %this.addr, align 4
  %this1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %this.addr, align 4
  store i32 0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %0 = load i32, i32* %i, align 4
  %m_cachedPoints = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  %1 = load i32, i32* %m_cachedPoints, align 4
  %cmp = icmp slt i32 %0, %1
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_pointCache = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 1
  %2 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds [4 x %class.btManifoldPoint], [4 x %class.btManifoldPoint]* %m_pointCache, i32 0, i32 %2
  call void @_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint(%class.btPersistentManifold* %this1, %class.btManifoldPoint* nonnull align 4 dereferenceable(192) %arrayidx)
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %3 = load i32, i32* %i, align 4
  %inc = add nsw i32 %3, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  %m_cachedPoints2 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %this1, i32 0, i32 4
  store i32 0, i32* %m_cachedPoints2, align 4
  ret void
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btCollisionDispatcher15releaseManifoldEP20btPersistentManifold(%class.btCollisionDispatcher* %this, %class.btPersistentManifold* %manifold) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %manifold.addr = alloca %class.btPersistentManifold*, align 4
  %findIndex = alloca i32, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  store %class.btPersistentManifold* %manifold, %class.btPersistentManifold** %manifold.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = load i32, i32* @gNumManifold, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* @gNumManifold, align 4
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %2 = bitcast %class.btCollisionDispatcher* %this1 to void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)***
  %vtable = load void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)**, void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)*** %2, align 4
  %vfn = getelementptr inbounds void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)*, void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)** %vtable, i64 5
  %3 = load void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)*, void (%class.btCollisionDispatcher*, %class.btPersistentManifold*)** %vfn, align 4
  call void %3(%class.btCollisionDispatcher* %this1, %class.btPersistentManifold* %1)
  %4 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %m_index1a = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %4, i32 0, i32 9
  %5 = load i32, i32* %m_index1a, align 4
  store i32 %5, i32* %findIndex, align 4
  %m_manifoldsPtr = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %6 = load i32, i32* %findIndex, align 4
  %m_manifoldsPtr2 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %m_manifoldsPtr2)
  %sub = sub nsw i32 %call, 1
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4swapEii(%class.btAlignedObjectArray* %m_manifoldsPtr, i32 %6, i32 %sub)
  %7 = load i32, i32* %findIndex, align 4
  %m_manifoldsPtr3 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %8 = load i32, i32* %findIndex, align 4
  %call4 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray* %m_manifoldsPtr3, i32 %8)
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call4, align 4
  %m_index1a5 = getelementptr inbounds %class.btPersistentManifold, %class.btPersistentManifold* %9, i32 0, i32 9
  store i32 %7, i32* %m_index1a5, align 4
  %m_manifoldsPtr6 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8pop_backEv(%class.btAlignedObjectArray* %m_manifoldsPtr6)
  %10 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %m_persistentManifoldPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 6
  %11 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPoolAllocator, align 4
  %12 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %13 = bitcast %class.btPersistentManifold* %12 to i8*
  %call7 = call zeroext i1 @_ZN15btPoolAllocator8validPtrEPv(%class.btPoolAllocator* %11, i8* %13)
  br i1 %call7, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_persistentManifoldPoolAllocator8 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 6
  %14 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPoolAllocator8, align 4
  %15 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %16 = bitcast %class.btPersistentManifold* %15 to i8*
  call void @_ZN15btPoolAllocator10freeMemoryEPv(%class.btPoolAllocator* %14, i8* %16)
  br label %if.end

if.else:                                          ; preds = %entry
  %17 = load %class.btPersistentManifold*, %class.btPersistentManifold** %manifold.addr, align 4
  %18 = bitcast %class.btPersistentManifold* %17 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %18)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4swapEii(%class.btAlignedObjectArray* %this, i32 %index0, i32 %index1) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %index0.addr = alloca i32, align 4
  %index1.addr = alloca i32, align 4
  %temp = alloca %class.btPersistentManifold*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %index0, i32* %index0.addr, align 4
  store i32 %index1, i32* %index1.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %1 = load i32, i32* %index0.addr, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  %2 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx, align 4
  store %class.btPersistentManifold* %2, %class.btPersistentManifold** %temp, align 4
  %m_data2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data2, align 4
  %4 = load i32, i32* %index1.addr, align 4
  %arrayidx3 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  %5 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx3, align 4
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %6 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4
  %7 = load i32, i32* %index0.addr, align 4
  %arrayidx5 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %6, i32 %7
  store %class.btPersistentManifold* %5, %class.btPersistentManifold** %arrayidx5, align 4
  %8 = load %class.btPersistentManifold*, %class.btPersistentManifold** %temp, align 4
  %m_data6 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %9 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data6, align 4
  %10 = load i32, i32* %index1.addr, align 4
  %arrayidx7 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %9, i32 %10
  store %class.btPersistentManifold* %8, %class.btPersistentManifold** %arrayidx7, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray* %this, i32 %n) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %n.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %1 = load i32, i32* %n.addr, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %0, i32 %1
  ret %class.btPersistentManifold** %arrayidx
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8pop_backEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %0 = load i32, i32* %m_size, align 4
  %dec = add nsw i32 %0, -1
  store i32 %dec, i32* %m_size, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %1 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %m_size2 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  %2 = load i32, i32* %m_size2, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %1, i32 %2
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZN15btPoolAllocator8validPtrEPv(%class.btPoolAllocator* %this, i8* %ptr) #1 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btPoolAllocator*, align 4
  %ptr.addr = alloca i8*, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end5

if.then:                                          ; preds = %entry
  %1 = load i8*, i8** %ptr.addr, align 4
  %m_pool = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  %2 = load i8*, i8** %m_pool, align 4
  %cmp = icmp uge i8* %1, %2
  br i1 %cmp, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %3 = load i8*, i8** %ptr.addr, align 4
  %m_pool2 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 4
  %4 = load i8*, i8** %m_pool2, align 4
  %m_maxElements = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 1
  %5 = load i32, i32* %m_maxElements, align 4
  %m_elemSize = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 0
  %6 = load i32, i32* %m_elemSize, align 4
  %mul = mul nsw i32 %5, %6
  %add.ptr = getelementptr inbounds i8, i8* %4, i32 %mul
  %cmp3 = icmp ult i8* %3, %add.ptr
  br i1 %cmp3, label %if.then4, label %if.end

if.then4:                                         ; preds = %land.lhs.true
  store i1 true, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %land.lhs.true, %if.then
  br label %if.end5

if.end5:                                          ; preds = %if.end, %entry
  store i1 false, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end5, %if.then4
  %7 = load i1, i1* %retval, align 1
  ret i1 %7
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN15btPoolAllocator10freeMemoryEPv(%class.btPoolAllocator* %this, i8* %ptr) #1 comdat {
entry:
  %this.addr = alloca %class.btPoolAllocator*, align 4
  %ptr.addr = alloca i8*, align 4
  store %class.btPoolAllocator* %this, %class.btPoolAllocator** %this.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %this1 = load %class.btPoolAllocator*, %class.btPoolAllocator** %this.addr, align 4
  %0 = load i8*, i8** %ptr.addr, align 4
  %tobool = icmp ne i8* %0, null
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_mutex = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 5
  call void @_Z11btMutexLockP11btSpinMutex(%class.btSpinMutex* %m_mutex)
  %m_firstFree = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  %1 = load i8*, i8** %m_firstFree, align 4
  %2 = load i8*, i8** %ptr.addr, align 4
  %3 = bitcast i8* %2 to i8**
  store i8* %1, i8** %3, align 4
  %4 = load i8*, i8** %ptr.addr, align 4
  %m_firstFree2 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 3
  store i8* %4, i8** %m_firstFree2, align 4
  %m_freeCount = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 2
  %5 = load i32, i32* %m_freeCount, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %m_freeCount, align 4
  %m_mutex3 = getelementptr inbounds %class.btPoolAllocator, %class.btPoolAllocator* %this1, i32 0, i32 5
  call void @_Z13btMutexUnlockP11btSpinMutex(%class.btSpinMutex* %m_mutex3)
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

declare void @_Z21btAlignedFreeInternalPv(i8*) #5

; Function Attrs: noinline optnone
define hidden %class.btCollisionAlgorithm* @_ZN21btCollisionDispatcher13findAlgorithmEPK24btCollisionObjectWrapperS2_P20btPersistentManifold22ebtDispatcherQueryType(%class.btCollisionDispatcher* %this, %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper* %body1Wrap, %class.btPersistentManifold* %sharedManifold, i32 %algoType) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %body0Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %body1Wrap.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %sharedManifold.addr = alloca %class.btPersistentManifold*, align 4
  %algoType.addr = alloca i32, align 4
  %ci = alloca %struct.btCollisionAlgorithmConstructionInfo, align 4
  %algo = alloca %class.btCollisionAlgorithm*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %body0Wrap, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  store %struct.btCollisionObjectWrapper* %body1Wrap, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  store %class.btPersistentManifold* %sharedManifold, %class.btPersistentManifold** %sharedManifold.addr, align 4
  store i32 %algoType, i32* %algoType.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %call = call %struct.btCollisionAlgorithmConstructionInfo* @_ZN36btCollisionAlgorithmConstructionInfoC2Ev(%struct.btCollisionAlgorithmConstructionInfo* %ci)
  %0 = bitcast %class.btCollisionDispatcher* %this1 to %class.btDispatcher*
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %ci, i32 0, i32 0
  store %class.btDispatcher* %0, %class.btDispatcher** %m_dispatcher1, align 4
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %sharedManifold.addr, align 4
  %m_manifold = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %ci, i32 0, i32 1
  store %class.btPersistentManifold* %1, %class.btPersistentManifold** %m_manifold, align 4
  store %class.btCollisionAlgorithm* null, %class.btCollisionAlgorithm** %algo, align 4
  %2 = load i32, i32* %algoType.addr, align 4
  %cmp = icmp eq i32 %2, 1
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_doubleDispatchContactPoints = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 7
  %3 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call2 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %3)
  %call3 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %call2)
  %arrayidx = getelementptr inbounds [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]]* %m_doubleDispatchContactPoints, i32 0, i32 %call3
  %4 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call4 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %4)
  %call5 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %call4)
  %arrayidx6 = getelementptr inbounds [36 x %struct.btCollisionAlgorithmCreateFunc*], [36 x %struct.btCollisionAlgorithmCreateFunc*]* %arrayidx, i32 0, i32 %call5
  %5 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %arrayidx6, align 4
  %6 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %7 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %8 = bitcast %struct.btCollisionAlgorithmCreateFunc* %5 to %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)***
  %vtable = load %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)**, %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)*** %8, align 4
  %vfn = getelementptr inbounds %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)*, %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)** %vtable, i64 2
  %9 = load %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)*, %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)** %vfn, align 4
  %call7 = call %class.btCollisionAlgorithm* %9(%struct.btCollisionAlgorithmCreateFunc* %5, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %6, %struct.btCollisionObjectWrapper* %7)
  store %class.btCollisionAlgorithm* %call7, %class.btCollisionAlgorithm** %algo, align 4
  br label %if.end

if.else:                                          ; preds = %entry
  %m_doubleDispatchClosestPoints = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 8
  %10 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %call8 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %10)
  %call9 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %call8)
  %arrayidx10 = getelementptr inbounds [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]], [36 x [36 x %struct.btCollisionAlgorithmCreateFunc*]]* %m_doubleDispatchClosestPoints, i32 0, i32 %call9
  %11 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %call11 = call %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %11)
  %call12 = call i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %call11)
  %arrayidx13 = getelementptr inbounds [36 x %struct.btCollisionAlgorithmCreateFunc*], [36 x %struct.btCollisionAlgorithmCreateFunc*]* %arrayidx10, i32 0, i32 %call12
  %12 = load %struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmCreateFunc** %arrayidx13, align 4
  %13 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body0Wrap.addr, align 4
  %14 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %body1Wrap.addr, align 4
  %15 = bitcast %struct.btCollisionAlgorithmCreateFunc* %12 to %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)***
  %vtable14 = load %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)**, %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)*** %15, align 4
  %vfn15 = getelementptr inbounds %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)*, %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)** %vtable14, i64 2
  %16 = load %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)*, %class.btCollisionAlgorithm* (%struct.btCollisionAlgorithmCreateFunc*, %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*)** %vfn15, align 4
  %call16 = call %class.btCollisionAlgorithm* %16(%struct.btCollisionAlgorithmCreateFunc* %12, %struct.btCollisionAlgorithmConstructionInfo* nonnull align 4 dereferenceable(8) %ci, %struct.btCollisionObjectWrapper* %13, %struct.btCollisionObjectWrapper* %14)
  store %class.btCollisionAlgorithm* %call16, %class.btCollisionAlgorithm** %algo, align 4
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  %17 = load %class.btCollisionAlgorithm*, %class.btCollisionAlgorithm** %algo, align 4
  ret %class.btCollisionAlgorithm* %17
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCollisionAlgorithmConstructionInfo* @_ZN36btCollisionAlgorithmConstructionInfoC2Ev(%struct.btCollisionAlgorithmConstructionInfo* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionAlgorithmConstructionInfo*, align 4
  store %struct.btCollisionAlgorithmConstructionInfo* %this, %struct.btCollisionAlgorithmConstructionInfo** %this.addr, align 4
  %this1 = load %struct.btCollisionAlgorithmConstructionInfo*, %struct.btCollisionAlgorithmConstructionInfo** %this.addr, align 4
  %m_dispatcher1 = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %this1, i32 0, i32 0
  store %class.btDispatcher* null, %class.btDispatcher** %m_dispatcher1, align 4
  %m_manifold = getelementptr inbounds %struct.btCollisionAlgorithmConstructionInfo, %struct.btCollisionAlgorithmConstructionInfo* %this1, i32 0, i32 1
  store %class.btPersistentManifold* null, %class.btPersistentManifold** %m_manifold, align 4
  ret %struct.btCollisionAlgorithmConstructionInfo* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZNK24btCollisionObjectWrapper17getCollisionShapeEv(%struct.btCollisionObjectWrapper* %this) #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_shape, align 4
  ret %class.btCollisionShape* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK16btCollisionShape12getShapeTypeEv(%class.btCollisionShape* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionShape*, align 4
  store %class.btCollisionShape* %this, %class.btCollisionShape** %this.addr, align 4
  %this1 = load %class.btCollisionShape*, %class.btCollisionShape** %this.addr, align 4
  %m_shapeType = getelementptr inbounds %class.btCollisionShape, %class.btCollisionShape* %this1, i32 0, i32 1
  %0 = load i32, i32* %m_shapeType, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN21btCollisionDispatcher13needsResponseEPK17btCollisionObjectS2_(%class.btCollisionDispatcher* %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %hasResponse = alloca i8, align 1
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4
  %call = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %0)
  br i1 %call, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4
  %call2 = call zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %1)
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %2 = phi i1 [ false, %entry ], [ %call2, %land.rhs ]
  %frombool = zext i1 %2 to i8
  store i8 %frombool, i8* %hasResponse, align 1
  %3 = load i8, i8* %hasResponse, align 1
  %tobool = trunc i8 %3 to i1
  br i1 %tobool, label %land.rhs3, label %land.end6

land.rhs3:                                        ; preds = %land.end
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4
  %call4 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %4)
  br i1 %call4, label %lor.rhs, label %lor.end

lor.rhs:                                          ; preds = %land.rhs3
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4
  %call5 = call zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %5)
  %lnot = xor i1 %call5, true
  br label %lor.end

lor.end:                                          ; preds = %lor.rhs, %land.rhs3
  %6 = phi i1 [ true, %land.rhs3 ], [ %lnot, %lor.rhs ]
  br label %land.end6

land.end6:                                        ; preds = %lor.end, %land.end
  %7 = phi i1 [ false, %land.end ], [ %6, %lor.end ]
  %frombool7 = zext i1 %7 to i8
  store i8 %frombool7, i8* %hasResponse, align 1
  %8 = load i8, i8* %hasResponse, align 1
  %tobool8 = trunc i8 %8 to i1
  ret i1 %tobool8
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject18hasContactResponseEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 4
  %cmp = icmp eq i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject25isStaticOrKinematicObjectEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionFlags = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 12
  %0 = load i32, i32* %m_collisionFlags, align 4
  %and = and i32 %0, 3
  %cmp = icmp ne i32 %and, 0
  ret i1 %cmp
}

; Function Attrs: noinline optnone
define hidden zeroext i1 @_ZN21btCollisionDispatcher14needsCollisionEPK17btCollisionObjectS2_(%class.btCollisionDispatcher* %this, %class.btCollisionObject* %body0, %class.btCollisionObject* %body1) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %body0.addr = alloca %class.btCollisionObject*, align 4
  %body1.addr = alloca %class.btCollisionObject*, align 4
  %needsCollision = alloca i8, align 1
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  store %class.btCollisionObject* %body0, %class.btCollisionObject** %body0.addr, align 4
  store %class.btCollisionObject* %body1, %class.btCollisionObject** %body1.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  store i8 1, i8* %needsCollision, align 1
  %0 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4
  %call = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %0)
  br i1 %call, label %if.else, label %land.lhs.true

land.lhs.true:                                    ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4
  %call2 = call zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %1)
  br i1 %call2, label %if.else, label %if.then

if.then:                                          ; preds = %land.lhs.true
  store i8 0, i8* %needsCollision, align 1
  br label %if.end6

if.else:                                          ; preds = %land.lhs.true, %entry
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4
  %3 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4
  %call3 = call zeroext i1 @_ZNK17btCollisionObject16checkCollideWithEPKS_(%class.btCollisionObject* %2, %class.btCollisionObject* %3)
  br i1 %call3, label %lor.lhs.false, label %if.then5

lor.lhs.false:                                    ; preds = %if.else
  %4 = load %class.btCollisionObject*, %class.btCollisionObject** %body1.addr, align 4
  %5 = load %class.btCollisionObject*, %class.btCollisionObject** %body0.addr, align 4
  %call4 = call zeroext i1 @_ZNK17btCollisionObject16checkCollideWithEPKS_(%class.btCollisionObject* %4, %class.btCollisionObject* %5)
  br i1 %call4, label %if.end, label %if.then5

if.then5:                                         ; preds = %lor.lhs.false, %if.else
  store i8 0, i8* %needsCollision, align 1
  br label %if.end

if.end:                                           ; preds = %if.then5, %lor.lhs.false
  br label %if.end6

if.end6:                                          ; preds = %if.end, %if.then
  %6 = load i8, i8* %needsCollision, align 1
  %tobool = trunc i8 %6 to i1
  ret i1 %tobool
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject8isActiveEv(%class.btCollisionObject* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %call = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp = icmp ne i32 %call, 2
  br i1 %cmp, label %land.rhs, label %land.end

land.rhs:                                         ; preds = %entry
  %call2 = call i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this1)
  %cmp3 = icmp ne i32 %call2, 5
  br label %land.end

land.end:                                         ; preds = %land.rhs, %entry
  %0 = phi i1 [ false, %entry ], [ %cmp3, %land.rhs ]
  ret i1 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZNK17btCollisionObject16checkCollideWithEPKS_(%class.btCollisionObject* %this, %class.btCollisionObject* %co) #2 comdat {
entry:
  %retval = alloca i1, align 1
  %this.addr = alloca %class.btCollisionObject*, align 4
  %co.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  store %class.btCollisionObject* %co, %class.btCollisionObject** %co.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_checkCollideWith = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 31
  %0 = load i32, i32* %m_checkCollideWith, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load %class.btCollisionObject*, %class.btCollisionObject** %co.addr, align 4
  %2 = bitcast %class.btCollisionObject* %this1 to i1 (%class.btCollisionObject*, %class.btCollisionObject*)***
  %vtable = load i1 (%class.btCollisionObject*, %class.btCollisionObject*)**, i1 (%class.btCollisionObject*, %class.btCollisionObject*)*** %2, align 4
  %vfn = getelementptr inbounds i1 (%class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btCollisionObject*, %class.btCollisionObject*)** %vtable, i64 3
  %3 = load i1 (%class.btCollisionObject*, %class.btCollisionObject*)*, i1 (%class.btCollisionObject*, %class.btCollisionObject*)** %vfn, align 4
  %call = call zeroext i1 %3(%class.btCollisionObject* %this1, %class.btCollisionObject* %1)
  store i1 %call, i1* %retval, align 1
  br label %return

if.end:                                           ; preds = %entry
  store i1 true, i1* %retval, align 1
  br label %return

return:                                           ; preds = %if.end, %if.then
  %4 = load i1, i1* %retval, align 1
  ret i1 %4
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btCollisionDispatcher25dispatchAllCollisionPairsEP22btOverlappingPairCacheRK16btDispatcherInfoP12btDispatcher(%class.btCollisionDispatcher* %this, %class.btOverlappingPairCache* %pairCache, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btDispatcher* %dispatcher) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %pairCache.addr = alloca %class.btOverlappingPairCache*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %dispatcher.addr = alloca %class.btDispatcher*, align 4
  %collisionCallback = alloca %class.btCollisionPairCallback, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  store %class.btOverlappingPairCache* %pairCache, %class.btOverlappingPairCache** %pairCache.addr, align 4
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %class.btDispatcher* %dispatcher, %class.btDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %0 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  %call = call %class.btCollisionPairCallback* @_ZN23btCollisionPairCallbackC2ERK16btDispatcherInfoP21btCollisionDispatcher(%class.btCollisionPairCallback* %collisionCallback, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %0, %class.btCollisionDispatcher* %this1)
  %1 = load %class.btOverlappingPairCache*, %class.btOverlappingPairCache** %pairCache.addr, align 4
  %2 = bitcast %class.btCollisionPairCallback* %collisionCallback to %struct.btOverlapCallback*
  %3 = load %class.btDispatcher*, %class.btDispatcher** %dispatcher.addr, align 4
  %4 = bitcast %class.btOverlappingPairCache* %1 to void (%class.btOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)***
  %vtable = load void (%class.btOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)**, void (%class.btOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*** %4, align 4
  %vfn = getelementptr inbounds void (%class.btOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vtable, i64 12
  %5 = load void (%class.btOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)*, void (%class.btOverlappingPairCache*, %struct.btOverlapCallback*, %class.btDispatcher*)** %vfn, align 4
  call void %5(%class.btOverlappingPairCache* %1, %struct.btOverlapCallback* %2, %class.btDispatcher* %3)
  %call2 = call %class.btCollisionPairCallback* @_ZN23btCollisionPairCallbackD2Ev(%class.btCollisionPairCallback* %collisionCallback) #7
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionPairCallback* @_ZN23btCollisionPairCallbackC2ERK16btDispatcherInfoP21btCollisionDispatcher(%class.btCollisionPairCallback* returned %this, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %dispatchInfo, %class.btCollisionDispatcher* %dispatcher) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionPairCallback*, align 4
  %dispatchInfo.addr = alloca %struct.btDispatcherInfo*, align 4
  %dispatcher.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionPairCallback* %this, %class.btCollisionPairCallback** %this.addr, align 4
  store %struct.btDispatcherInfo* %dispatchInfo, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %class.btCollisionDispatcher* %dispatcher, %class.btCollisionDispatcher** %dispatcher.addr, align 4
  %this1 = load %class.btCollisionPairCallback*, %class.btCollisionPairCallback** %this.addr, align 4
  %0 = bitcast %class.btCollisionPairCallback* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackC2Ev(%struct.btOverlapCallback* %0) #7
  %1 = bitcast %class.btCollisionPairCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV23btCollisionPairCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %1, align 4
  %m_dispatchInfo = getelementptr inbounds %class.btCollisionPairCallback, %class.btCollisionPairCallback* %this1, i32 0, i32 1
  %2 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %dispatchInfo.addr, align 4
  store %struct.btDispatcherInfo* %2, %struct.btDispatcherInfo** %m_dispatchInfo, align 4
  %m_dispatcher = getelementptr inbounds %class.btCollisionPairCallback, %class.btCollisionPairCallback* %this1, i32 0, i32 2
  %3 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %dispatcher.addr, align 4
  store %class.btCollisionDispatcher* %3, %class.btCollisionDispatcher** %m_dispatcher, align 4
  ret %class.btCollisionPairCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionPairCallback* @_ZN23btCollisionPairCallbackD2Ev(%class.btCollisionPairCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionPairCallback*, align 4
  store %class.btCollisionPairCallback* %this, %class.btCollisionPairCallback** %this.addr, align 4
  %this1 = load %class.btCollisionPairCallback*, %class.btCollisionPairCallback** %this.addr, align 4
  %0 = bitcast %class.btCollisionPairCallback* %this1 to %struct.btOverlapCallback*
  %call = call %struct.btOverlapCallback* @_ZN17btOverlapCallbackD2Ev(%struct.btOverlapCallback* %0) #7
  ret %class.btCollisionPairCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btCollisionShape* @_ZN17btCollisionObject17getCollisionShapeEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_collisionShape = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 9
  %0 = load %class.btCollisionShape*, %class.btCollisionShape** %m_collisionShape, align 4
  ret %class.btCollisionShape* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden nonnull align 4 dereferenceable(64) %class.btTransform* @_ZN17btCollisionObject17getWorldTransformEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_worldTransform = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 1
  ret %class.btTransform* %m_worldTransform
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btCollisionObjectWrapper* @_ZN24btCollisionObjectWrapperC2EPKS_PK16btCollisionShapePK17btCollisionObjectRK11btTransformii(%struct.btCollisionObjectWrapper* returned %this, %struct.btCollisionObjectWrapper* %parent, %class.btCollisionShape* %shape, %class.btCollisionObject* %collisionObject, %class.btTransform* nonnull align 4 dereferenceable(64) %worldTransform, i32 %partId, i32 %index) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %parent.addr = alloca %struct.btCollisionObjectWrapper*, align 4
  %shape.addr = alloca %class.btCollisionShape*, align 4
  %collisionObject.addr = alloca %class.btCollisionObject*, align 4
  %worldTransform.addr = alloca %class.btTransform*, align 4
  %partId.addr = alloca i32, align 4
  %index.addr = alloca i32, align 4
  store %struct.btCollisionObjectWrapper* %this, %struct.btCollisionObjectWrapper** %this.addr, align 4
  store %struct.btCollisionObjectWrapper* %parent, %struct.btCollisionObjectWrapper** %parent.addr, align 4
  store %class.btCollisionShape* %shape, %class.btCollisionShape** %shape.addr, align 4
  store %class.btCollisionObject* %collisionObject, %class.btCollisionObject** %collisionObject.addr, align 4
  store %class.btTransform* %worldTransform, %class.btTransform** %worldTransform.addr, align 4
  store i32 %partId, i32* %partId.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %this.addr, align 4
  %m_parent = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 0
  %0 = load %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper** %parent.addr, align 4
  store %struct.btCollisionObjectWrapper* %0, %struct.btCollisionObjectWrapper** %m_parent, align 4
  %m_shape = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 1
  %1 = load %class.btCollisionShape*, %class.btCollisionShape** %shape.addr, align 4
  store %class.btCollisionShape* %1, %class.btCollisionShape** %m_shape, align 4
  %m_collisionObject = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 2
  %2 = load %class.btCollisionObject*, %class.btCollisionObject** %collisionObject.addr, align 4
  store %class.btCollisionObject* %2, %class.btCollisionObject** %m_collisionObject, align 4
  %m_worldTransform = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 3
  %3 = load %class.btTransform*, %class.btTransform** %worldTransform.addr, align 4
  store %class.btTransform* %3, %class.btTransform** %m_worldTransform, align 4
  %m_partId = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 4
  %4 = load i32, i32* %partId.addr, align 4
  store i32 %4, i32* %m_partId, align 4
  %m_index = getelementptr inbounds %struct.btCollisionObjectWrapper, %struct.btCollisionObjectWrapper* %this1, i32 0, i32 5
  %5 = load i32, i32* %index.addr, align 4
  store i32 %5, i32* %m_index, align 4
  ret %struct.btCollisionObjectWrapper* %this1
}

declare %class.btManifoldResult* @_ZN16btManifoldResultC1EPK24btCollisionObjectWrapperS2_(%class.btManifoldResult* returned, %struct.btCollisionObjectWrapper*, %struct.btCollisionObjectWrapper*) unnamed_addr #5

; Function Attrs: noinline optnone
define hidden i8* @_ZN21btCollisionDispatcher26allocateCollisionAlgorithmEi(%class.btCollisionDispatcher* %this, i32 %size) unnamed_addr #2 {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %size.addr = alloca i32, align 4
  %mem = alloca i8*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_collisionAlgorithmPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 5
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPoolAllocator, align 4
  %1 = load i32, i32* %size.addr, align 4
  %call = call i8* @_ZN15btPoolAllocator8allocateEi(%class.btPoolAllocator* %0, i32 %1)
  store i8* %call, i8** %mem, align 4
  %2 = load i8*, i8** %mem, align 4
  %cmp = icmp eq i8* null, %2
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %3 = load i32, i32* %size.addr, align 4
  %call2 = call i8* @_Z22btAlignedAllocInternalmi(i32 %3, i32 16)
  store i8* %call2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  %4 = load i8*, i8** %mem, align 4
  store i8* %4, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %5 = load i8*, i8** %retval, align 4
  ret i8* %5
}

; Function Attrs: noinline optnone
define hidden void @_ZN21btCollisionDispatcher22freeCollisionAlgorithmEPv(%class.btCollisionDispatcher* %this, i8* %ptr) unnamed_addr #2 {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %ptr.addr = alloca i8*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  store i8* %ptr, i8** %ptr.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_collisionAlgorithmPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 5
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPoolAllocator, align 4
  %1 = load i8*, i8** %ptr.addr, align 4
  %call = call zeroext i1 @_ZN15btPoolAllocator8validPtrEPv(%class.btPoolAllocator* %0, i8* %1)
  br i1 %call, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %m_collisionAlgorithmPoolAllocator2 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 5
  %2 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_collisionAlgorithmPoolAllocator2, align 4
  %3 = load i8*, i8** %ptr.addr, align 4
  call void @_ZN15btPoolAllocator10freeMemoryEPv(%class.btPoolAllocator* %2, i8* %3)
  br label %if.end

if.else:                                          ; preds = %entry
  %4 = load i8*, i8** %ptr.addr, align 4
  call void @_Z21btAlignedFreeInternalPv(i8* %4)
  br label %if.end

if.end:                                           ; preds = %if.else, %if.then
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i32 @_ZNK21btCollisionDispatcher15getNumManifoldsEv(%class.btCollisionDispatcher* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_manifoldsPtr = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %m_manifoldsPtr)
  ret i32 %call
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPersistentManifold* @_ZN21btCollisionDispatcher26getManifoldByIndexInternalEi(%class.btCollisionDispatcher* %this, i32 %index) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  %index.addr = alloca i32, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  store i32 %index, i32* %index.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_manifoldsPtr = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %0 = load i32, i32* %index.addr, align 4
  %call = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray* %m_manifoldsPtr, i32 %0)
  %1 = load %class.btPersistentManifold*, %class.btPersistentManifold** %call, align 4
  ret %class.btPersistentManifold* %1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPersistentManifold** @_ZN21btCollisionDispatcher26getInternalManifoldPointerEv(%class.btCollisionDispatcher* %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_manifoldsPtr = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %m_manifoldsPtr)
  %tobool = icmp ne i32 %call, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %m_manifoldsPtr2 = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 2
  %call3 = call nonnull align 4 dereferenceable(4) %class.btPersistentManifold** @_ZN20btAlignedObjectArrayIP20btPersistentManifoldEixEi(%class.btAlignedObjectArray* %m_manifoldsPtr2, i32 0)
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi %class.btPersistentManifold** [ %call3, %cond.true ], [ null, %cond.false ]
  ret %class.btPersistentManifold** %cond
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPoolAllocator* @_ZN21btCollisionDispatcher23getInternalManifoldPoolEv(%class.btCollisionDispatcher* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_persistentManifoldPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 6
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPoolAllocator, align 4
  ret %class.btPoolAllocator* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btPoolAllocator* @_ZNK21btCollisionDispatcher23getInternalManifoldPoolEv(%class.btCollisionDispatcher* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_persistentManifoldPoolAllocator = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 6
  %0 = load %class.btPoolAllocator*, %class.btPoolAllocator** %m_persistentManifoldPoolAllocator, align 4
  ret %class.btPoolAllocator* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultC2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %0 = bitcast %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [7 x i8*] }, { [7 x i8*] }* @_ZTVN36btDiscreteCollisionDetectorInterface6ResultE, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %"struct.btDiscreteCollisionDetectorInterface::Result"* @_ZN36btDiscreteCollisionDetectorInterface6ResultD2Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  ret %"struct.btDiscreteCollisionDetectorInterface::Result"* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN36btDiscreteCollisionDetectorInterface6ResultD0Ev(%"struct.btDiscreteCollisionDetectorInterface::Result"* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %"struct.btDiscreteCollisionDetectorInterface::Result"*, align 4
  store %"struct.btDiscreteCollisionDetectorInterface::Result"* %this, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  %this1 = load %"struct.btDiscreteCollisionDetectorInterface::Result"*, %"struct.btDiscreteCollisionDetectorInterface::Result"** %this.addr, align 4
  call void @llvm.trap() #9
  unreachable
}

declare void @__cxa_pure_virtual() unnamed_addr

; Function Attrs: cold noreturn nounwind
declare void @llvm.trap() #6

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z11btMutexLockP11btSpinMutex(%class.btSpinMutex* %0) #1 comdat {
entry:
  %.addr = alloca %class.btSpinMutex*, align 4
  store %class.btSpinMutex* %0, %class.btSpinMutex** %.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_Z13btMutexUnlockP11btSpinMutex(%class.btSpinMutex* %0) #1 comdat {
entry:
  %.addr = alloca %class.btSpinMutex*, align 4
  store %class.btSpinMutex* %0, %class.btSpinMutex** %.addr, align 4
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btTypedObject* @_ZN13btTypedObjectC2Ei(%struct.btTypedObject* returned %this, i32 %objectType) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btTypedObject*, align 4
  %objectType.addr = alloca i32, align 4
  store %struct.btTypedObject* %this, %struct.btTypedObject** %this.addr, align 4
  store i32 %objectType, i32* %objectType.addr, align 4
  %this1 = load %struct.btTypedObject*, %struct.btTypedObject** %this.addr, align 4
  %m_objectType = getelementptr inbounds %struct.btTypedObject, %struct.btTypedObject* %this1, i32 0, i32 0
  %0 = load i32, i32* %objectType.addr, align 4
  store i32 %0, i32* %m_objectType, align 4
  ret %struct.btTypedObject* %this1
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btManifoldPoint* @_ZN15btManifoldPointC2Ev(%class.btManifoldPoint* returned %this) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btManifoldPoint*, align 4
  store %class.btManifoldPoint* %this, %class.btManifoldPoint** %this.addr, align 4
  %this1 = load %class.btManifoldPoint*, %class.btManifoldPoint** %this.addr, align 4
  %m_localPointA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 0
  %call = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localPointA)
  %m_localPointB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 1
  %call2 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_localPointB)
  %m_positionWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 2
  %call3 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_positionWorldOnB)
  %m_positionWorldOnA = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 3
  %call4 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_positionWorldOnA)
  %m_normalWorldOnB = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 4
  %call5 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_normalWorldOnB)
  %m_userPersistentData = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 14
  store i8* null, i8** %m_userPersistentData, align 4
  %m_contactPointFlags = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 15
  store i32 0, i32* %m_contactPointFlags, align 4
  %m_appliedImpulse = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 16
  store float 0.000000e+00, float* %m_appliedImpulse, align 4
  %m_appliedImpulseLateral1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 17
  store float 0.000000e+00, float* %m_appliedImpulseLateral1, align 4
  %m_appliedImpulseLateral2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 18
  store float 0.000000e+00, float* %m_appliedImpulseLateral2, align 4
  %m_contactMotion1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 19
  store float 0.000000e+00, float* %m_contactMotion1, align 4
  %m_contactMotion2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 20
  store float 0.000000e+00, float* %m_contactMotion2, align 4
  %0 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 21
  %m_contactCFM = bitcast %union.anon.0* %0 to float*
  store float 0.000000e+00, float* %m_contactCFM, align 4
  %1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 22
  %m_contactERP = bitcast %union.anon.1* %1 to float*
  store float 0.000000e+00, float* %m_contactERP, align 4
  %m_frictionCFM = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 23
  store float 0.000000e+00, float* %m_frictionCFM, align 4
  %m_lifeTime = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 24
  store i32 0, i32* %m_lifeTime, align 4
  %m_lateralFrictionDir1 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 25
  %call6 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lateralFrictionDir1)
  %m_lateralFrictionDir2 = getelementptr inbounds %class.btManifoldPoint, %class.btManifoldPoint* %this1, i32 0, i32 26
  %call7 = call %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* %m_lateralFrictionDir2)
  ret %class.btManifoldPoint* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btVector3* @_ZN9btVector3C2Ev(%class.btVector3* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btVector3*, align 4
  store %class.btVector3* %this, %class.btVector3** %this.addr, align 4
  %this1 = load %class.btVector3*, %class.btVector3** %this.addr, align 4
  ret %class.btVector3* %this1
}

declare void @_ZN20btPersistentManifold14clearUserCacheER15btManifoldPoint(%class.btPersistentManifold*, %class.btManifoldPoint* nonnull align 4 dereferenceable(192)) #5

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK17btCollisionObject18getActivationStateEv(%class.btCollisionObject* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionObject*, align 4
  store %class.btCollisionObject* %this, %class.btCollisionObject** %this.addr, align 4
  %this1 = load %class.btCollisionObject*, %class.btCollisionObject** %this.addr, align 4
  %m_activationState1 = getelementptr inbounds %class.btCollisionObject, %class.btCollisionObject* %this1, i32 0, i32 16
  %0 = load i32, i32* %m_activationState1, align 4
  ret i32 %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btOverlapCallback* @_ZN17btOverlapCallbackC2Ev(%struct.btOverlapCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btOverlapCallback*, align 4
  store %struct.btOverlapCallback* %this, %struct.btOverlapCallback** %this.addr, align 4
  %this1 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %this.addr, align 4
  %0 = bitcast %struct.btOverlapCallback* %this1 to i32 (...)***
  store i32 (...)** bitcast (i8** getelementptr inbounds ({ [5 x i8*] }, { [5 x i8*] }* @_ZTV17btOverlapCallback, i32 0, inrange i32 0, i32 2) to i32 (...)**), i32 (...)*** %0, align 4
  ret %struct.btOverlapCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN23btCollisionPairCallbackD0Ev(%class.btCollisionPairCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionPairCallback*, align 4
  store %class.btCollisionPairCallback* %this, %class.btCollisionPairCallback** %this.addr, align 4
  %this1 = load %class.btCollisionPairCallback*, %class.btCollisionPairCallback** %this.addr, align 4
  %call = call %class.btCollisionPairCallback* @_ZN23btCollisionPairCallbackD2Ev(%class.btCollisionPairCallback* %this1) #7
  %0 = bitcast %class.btCollisionPairCallback* %this1 to i8*
  call void @_ZdlPv(i8* %0) #8
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden zeroext i1 @_ZN23btCollisionPairCallback14processOverlapER16btBroadphasePair(%class.btCollisionPairCallback* %this, %struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %pair) unnamed_addr #2 comdat {
entry:
  %this.addr = alloca %class.btCollisionPairCallback*, align 4
  %pair.addr = alloca %struct.btBroadphasePair*, align 4
  store %class.btCollisionPairCallback* %this, %class.btCollisionPairCallback** %this.addr, align 4
  store %struct.btBroadphasePair* %pair, %struct.btBroadphasePair** %pair.addr, align 4
  %this1 = load %class.btCollisionPairCallback*, %class.btCollisionPairCallback** %this.addr, align 4
  %m_dispatcher = getelementptr inbounds %class.btCollisionPairCallback, %class.btCollisionPairCallback* %this1, i32 0, i32 2
  %0 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %m_dispatcher, align 4
  %call = call void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)* @_ZNK21btCollisionDispatcher15getNearCallbackEv(%class.btCollisionDispatcher* %0)
  %1 = load %struct.btBroadphasePair*, %struct.btBroadphasePair** %pair.addr, align 4
  %m_dispatcher2 = getelementptr inbounds %class.btCollisionPairCallback, %class.btCollisionPairCallback* %this1, i32 0, i32 2
  %2 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %m_dispatcher2, align 4
  %m_dispatchInfo = getelementptr inbounds %class.btCollisionPairCallback, %class.btCollisionPairCallback* %this1, i32 0, i32 1
  %3 = load %struct.btDispatcherInfo*, %struct.btDispatcherInfo** %m_dispatchInfo, align 4
  call void %call(%struct.btBroadphasePair* nonnull align 4 dereferenceable(16) %1, %class.btCollisionDispatcher* nonnull align 4 dereferenceable(10448) %2, %struct.btDispatcherInfo* nonnull align 4 dereferenceable(40) %3)
  ret i1 false
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %struct.btOverlapCallback* @_ZN17btOverlapCallbackD2Ev(%struct.btOverlapCallback* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btOverlapCallback*, align 4
  store %struct.btOverlapCallback* %this, %struct.btOverlapCallback** %this.addr, align 4
  %this1 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %this.addr, align 4
  ret %struct.btOverlapCallback* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN17btOverlapCallbackD0Ev(%struct.btOverlapCallback* %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %struct.btOverlapCallback*, align 4
  store %struct.btOverlapCallback* %this, %struct.btOverlapCallback** %this.addr, align 4
  %this1 = load %struct.btOverlapCallback*, %struct.btOverlapCallback** %this.addr, align 4
  call void @llvm.trap() #9
  unreachable
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)* @_ZNK21btCollisionDispatcher15getNearCallbackEv(%class.btCollisionDispatcher* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btCollisionDispatcher*, align 4
  store %class.btCollisionDispatcher* %this, %class.btCollisionDispatcher** %this.addr, align 4
  %this1 = load %class.btCollisionDispatcher*, %class.btCollisionDispatcher** %this.addr, align 4
  %m_nearCallback = getelementptr inbounds %class.btCollisionDispatcher, %class.btCollisionDispatcher* %this1, i32 0, i32 4
  %0 = load void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)*, void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)** %m_nearCallback, align 4
  ret void (%struct.btBroadphasePair*, %class.btCollisionDispatcher*, %struct.btDispatcherInfo*)* %0
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden %class.btAlignedAllocator* @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EEC2Ev(%class.btAlignedAllocator* returned %this) unnamed_addr #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  ret %class.btAlignedAllocator* %this1
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data, align 4
  %m_size = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 2
  store i32 0, i32* %m_size, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 0, i32* %m_capacity, align 4
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE5clearEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE4initEv(%class.btAlignedObjectArray* %this1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray* %this, i32 %first, i32 %last) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %first.addr = alloca i32, align 4
  %last.addr = alloca i32, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %first, i32* %first.addr, align 4
  store i32 %last, i32* %last.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %first.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %last.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %5 = load i32, i32* %i, align 4
  %inc = add nsw i32 %5, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray* %this) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %tobool = icmp ne %class.btPersistentManifold** %0, null
  br i1 %tobool, label %if.then, label %if.end6

if.then:                                          ; preds = %entry
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  %1 = load i8, i8* %m_ownsMemory, align 4
  %tobool2 = trunc i8 %1 to i1
  br i1 %tobool2, label %if.then3, label %if.end

if.then3:                                         ; preds = %if.then
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %m_data4 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %2 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data4, align 4
  call void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %m_allocator, %class.btPersistentManifold** %2)
  br label %if.end

if.end:                                           ; preds = %if.then3, %if.then
  %m_data5 = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btPersistentManifold** null, %class.btPersistentManifold*** %m_data5, align 4
  br label %if.end6

if.end6:                                          ; preds = %if.end, %entry
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE10deallocateEPS1_(%class.btAlignedAllocator* %this, %class.btPersistentManifold** %ptr) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %ptr.addr = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store %class.btPersistentManifold** %ptr, %class.btPersistentManifold*** %ptr.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %ptr.addr, align 4
  %1 = bitcast %class.btPersistentManifold** %0 to i8*
  call void @_Z21btAlignedFreeInternalPv(i8* %1)
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray* %this) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  %0 = load i32, i32* %m_capacity, align 4
  ret i32 %0
}

; Function Attrs: noinline optnone
define linkonce_odr hidden void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7reserveEi(%class.btAlignedObjectArray* %this, i32 %_Count) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %_Count.addr = alloca i32, align 4
  %s = alloca %class.btPersistentManifold**, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %_Count, i32* %_Count.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %call = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE8capacityEv(%class.btAlignedObjectArray* %this1)
  %0 = load i32, i32* %_Count.addr, align 4
  %cmp = icmp slt i32 %call, %0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i32, i32* %_Count.addr, align 4
  %call2 = call i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray* %this1, i32 %1)
  %2 = bitcast i8* %call2 to %class.btPersistentManifold**
  store %class.btPersistentManifold** %2, %class.btPersistentManifold*** %s, align 4
  %call3 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this1)
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  call void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray* %this1, i32 0, i32 %call3, %class.btPersistentManifold** %3)
  %call4 = call i32 @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4sizeEv(%class.btAlignedObjectArray* %this1)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE7destroyEii(%class.btAlignedObjectArray* %this1, i32 0, i32 %call4)
  call void @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE10deallocateEv(%class.btAlignedObjectArray* %this1)
  %m_ownsMemory = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 5
  store i8 1, i8* %m_ownsMemory, align 4
  %4 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %s, align 4
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  store %class.btPersistentManifold** %4, %class.btPersistentManifold*** %m_data, align 4
  %5 = load i32, i32* %_Count.addr, align 4
  %m_capacity = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 3
  store i32 %5, i32* %m_capacity, align 4
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden i32 @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE9allocSizeEi(%class.btAlignedObjectArray* %this, i32 %size) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %cond.true, label %cond.false

cond.true:                                        ; preds = %entry
  %1 = load i32, i32* %size.addr, align 4
  %mul = mul nsw i32 %1, 2
  br label %cond.end

cond.false:                                       ; preds = %entry
  br label %cond.end

cond.end:                                         ; preds = %cond.false, %cond.true
  %cond = phi i32 [ %mul, %cond.true ], [ 1, %cond.false ]
  ret i32 %cond
}

; Function Attrs: noinline optnone
define linkonce_odr hidden i8* @_ZN20btAlignedObjectArrayIP20btPersistentManifoldE8allocateEi(%class.btAlignedObjectArray* %this, i32 %size) #2 comdat {
entry:
  %retval = alloca i8*, align 4
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %size.addr = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %size, i32* %size.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %size.addr, align 4
  %tobool = icmp ne i32 %0, 0
  br i1 %tobool, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %m_allocator = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 0
  %1 = load i32, i32* %size.addr, align 4
  %call = call %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %m_allocator, i32 %1, %class.btPersistentManifold*** null)
  %2 = bitcast %class.btPersistentManifold** %call to i8*
  store i8* %2, i8** %retval, align 4
  br label %return

if.end:                                           ; preds = %entry
  store i8* null, i8** %retval, align 4
  br label %return

return:                                           ; preds = %if.end, %if.then
  %3 = load i8*, i8** %retval, align 4
  ret i8* %3
}

; Function Attrs: noinline nounwind optnone
define linkonce_odr hidden void @_ZNK20btAlignedObjectArrayIP20btPersistentManifoldE4copyEiiPS1_(%class.btAlignedObjectArray* %this, i32 %start, i32 %end, %class.btPersistentManifold** %dest) #1 comdat {
entry:
  %this.addr = alloca %class.btAlignedObjectArray*, align 4
  %start.addr = alloca i32, align 4
  %end.addr = alloca i32, align 4
  %dest.addr = alloca %class.btPersistentManifold**, align 4
  %i = alloca i32, align 4
  store %class.btAlignedObjectArray* %this, %class.btAlignedObjectArray** %this.addr, align 4
  store i32 %start, i32* %start.addr, align 4
  store i32 %end, i32* %end.addr, align 4
  store %class.btPersistentManifold** %dest, %class.btPersistentManifold*** %dest.addr, align 4
  %this1 = load %class.btAlignedObjectArray*, %class.btAlignedObjectArray** %this.addr, align 4
  %0 = load i32, i32* %start.addr, align 4
  store i32 %0, i32* %i, align 4
  br label %for.cond

for.cond:                                         ; preds = %for.inc, %entry
  %1 = load i32, i32* %i, align 4
  %2 = load i32, i32* %end.addr, align 4
  %cmp = icmp slt i32 %1, %2
  br i1 %cmp, label %for.body, label %for.end

for.body:                                         ; preds = %for.cond
  %3 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %dest.addr, align 4
  %4 = load i32, i32* %i, align 4
  %arrayidx = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %3, i32 %4
  %5 = bitcast %class.btPersistentManifold** %arrayidx to i8*
  %6 = bitcast i8* %5 to %class.btPersistentManifold**
  %m_data = getelementptr inbounds %class.btAlignedObjectArray, %class.btAlignedObjectArray* %this1, i32 0, i32 4
  %7 = load %class.btPersistentManifold**, %class.btPersistentManifold*** %m_data, align 4
  %8 = load i32, i32* %i, align 4
  %arrayidx2 = getelementptr inbounds %class.btPersistentManifold*, %class.btPersistentManifold** %7, i32 %8
  %9 = load %class.btPersistentManifold*, %class.btPersistentManifold** %arrayidx2, align 4
  store %class.btPersistentManifold* %9, %class.btPersistentManifold** %6, align 4
  br label %for.inc

for.inc:                                          ; preds = %for.body
  %10 = load i32, i32* %i, align 4
  %inc = add nsw i32 %10, 1
  store i32 %inc, i32* %i, align 4
  br label %for.cond

for.end:                                          ; preds = %for.cond
  ret void
}

; Function Attrs: noinline optnone
define linkonce_odr hidden %class.btPersistentManifold** @_ZN18btAlignedAllocatorIP20btPersistentManifoldLj16EE8allocateEiPPKS1_(%class.btAlignedAllocator* %this, i32 %n, %class.btPersistentManifold*** %hint) #2 comdat {
entry:
  %this.addr = alloca %class.btAlignedAllocator*, align 4
  %n.addr = alloca i32, align 4
  %hint.addr = alloca %class.btPersistentManifold***, align 4
  store %class.btAlignedAllocator* %this, %class.btAlignedAllocator** %this.addr, align 4
  store i32 %n, i32* %n.addr, align 4
  store %class.btPersistentManifold*** %hint, %class.btPersistentManifold**** %hint.addr, align 4
  %this1 = load %class.btAlignedAllocator*, %class.btAlignedAllocator** %this.addr, align 4
  %0 = load i32, i32* %n.addr, align 4
  %mul = mul i32 4, %0
  %call = call i8* @_Z22btAlignedAllocInternalmi(i32 %mul, i32 16)
  %1 = bitcast i8* %call to %class.btPersistentManifold**
  ret %class.btPersistentManifold** %1
}

; Function Attrs: noinline
define internal void @_GLOBAL__sub_I_btCollisionDispatcher.cpp() #0 {
entry:
  call void @__cxx_global_var_init()
  ret void
}

attributes #0 = { noinline "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { noinline nounwind optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noinline optnone "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nobuiltin nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #6 = { cold noreturn nounwind }
attributes #7 = { nounwind }
attributes #8 = { builtin nounwind }
attributes #9 = { noreturn nounwind }

!llvm.module.flags = !{!0}
!llvm.ident = !{!1}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{!"clang version 11.0.0 (/b/s/w/ir/cache/git/chromium.googlesource.com-external-github.com-llvm-llvm--project 613c4a87ba9bb39d1927402f4dd4c1ef1f9a02f7)"}
